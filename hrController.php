<?php

namespace App\Http\Controllers\hr;

use App\Http\Controllers\Controller;
use App\models\hr\hrOperation;
use App\models\hr\Wahede_janebe;
use App\models\hr\attendanceServer;
use App\models\workplan\Report;

use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use DateTime;
use DateInterval;
use DatePeriod;
use Session;
use Mail;
use Crypt;
use Image;

class hrController extends Controller
{
	//database connection
	public static $myDb = "hr";

	public function __construct()
	{
		if(!Session::has('lang'))
		{
			Redirect::route('getLogin')->send();
		}
	}
	public function homePage()
	{
		if(canViewChart())
		{
			$data['emp_degrees'] = hrOperation::getDegreesChart();
			$degrees = hrOperation::getDegrees();
			$dg_array=array(0=>'هیچکدام');
			foreach($degrees AS $dg)
			{
				$dg_array[$dg->id]=$dg->name_dr;
			}
			$data['degrees']=$dg_array;
			$gender = hrOperation::getGenders();
			$g_array=array();
			foreach($gender AS $g)
			{
				$g_array[$g->gender]=$g->total;
			}
			$data['gender']=$g_array;
			$types = hrOperation::getEmployeeTypes();
			$t_array=array();
			foreach($types AS $t)
			{
				$t_array[$t->employee_type]=$t->total;
			}
			$data['types']=$t_array;
			//dd($t_array);
			return View::make('hr.dashboard',$data);
		}
		else
		{
			return View::make('hr.attendance.dashboard_att');
		}
	}
	//get card template
	public function cardTemplate($id=0)
	{
		if(canPrintCard('hr_cards'))
		{
			$row = DB::connection('hr')
					->table('employees AS t1')
					->select(
						't1.id AS eid',
						't1.name_dr',
						't1.name_en',
						't1.last_name',
						//'t1.position_dr',
						//'t1.position_en',
						't1.current_position_dr',
						't1.current_position_en',
						'dep.name_pa AS general_department_dr',
						'dep1.name_pa AS department_dr',
						'dep.name_en AS general_department_en',
						'dep1.name_en AS department_en',
						'dep.id AS dep_id',
						'dep1.id AS sudep_id',
						'dep1.advisor',
						'bg.name_en AS blood_group',
						't1.card_issued',
						//'ps.name_en AS position_en',
						//'ps.color AS color',
						't1.short_title_id AS position_id',
						't1.photo',
						't1.ready_to_print',
						't1.RFID',
						't1.employee_type',
						't1.dep_type',
						't1.blood_group as blood_id'
						)
					->leftjoin('auth.department AS dep','dep.id','=','t1.general_department')
					->leftjoin('auth.department AS dep1','dep1.id','=','t1.department')
					->leftjoin('auth.blood_group AS bg','bg.id','=','t1.blood_group')
					//->leftjoin('auth.position_short_title AS ps','ps.id','=','t1.short_title_id')
					->where('t1.id',$id)
					->first();
			$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			if($row->dep_type != $user_dep_type)
			{
				return showWarning();
			}
			if($row->card_issued>0)
			{
				return '<center>برای این کارمند قبلا کارت پرنت شده است.!</center>';
			}
			if($row->employee_type==4 || $row->employee_type==5 || $row->employee_type==6)
			{
				return '<center>این کارمند بالمقطع میباشد!.</center>';
			}
			// if($row->RFID !=null && $row->dep_type==2)
			// {
			// 	return '<center>این کارمند دارای کارت میباشد!.</center>';
			// }
			elseif($row->ready_to_print==0)
			{
				return '<center>کارت برای پرنت آماده نیست!.</center>';
			}
			else
			{
				if($user_dep_type == 2)
				{
                    if($row->eid==1976 || $row->eid==5130 )
                    {
                        return View::make('hr.card',array('row'=>$row));
                    }
					if($row->sudep_id==92 || $row->sudep_id==93)
					{
                        return View::make('hr.card',array('row'=>$row));
					}
					else
					{
						return View::make('hr.card',array('row'=>$row));
					}
				}
				else
				{
					if($row->blood_id==9)
					{
						return '<center>گروپ خون کارمند مشخص نشده است!.</center>';
					}
					return View::make('hr.ocs_card_new',array('row'=>$row));
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function cardTemplatePrint($id=0,$card_type='0')
	{
        if($card_type!='0')
           $card_type = Crypt::decrypt($card_type); 

		if(canPrintCard('hr_cards'))
		{
			$row = DB::connection('hr')
					->table('employees AS t1')
					->select(
						't1.id AS eid',
						't1.name_dr',
						't1.name_en',
						't1.last_name',
						//'t1.position_dr',
						//'t1.position_en',
						't1.current_position_dr',
						't1.current_position_en',
						'dep.name_pa AS general_department_dr',
						'dep1.name_pa AS department_dr',
						'dep.name_en AS general_department_en',
						'dep1.name_en AS department_en',
						'dep.id AS dep_id',
						'dep1.id AS sudep_id',
						'dep1.advisor',
						'bg.name_en AS blood_group',
						't1.card_issued',
						//'ps.name_en AS position_en',
						//'ps.color AS color',
						't1.short_title_id AS position_id',
						't1.photo',
						't1.ready_to_print',
						't1.RFID',
						't1.employee_type',
						't1.dep_type',
                        't1.blood_group as blood_id',
                        'ejc.position_short_title_id as pshort_id'
						)
					->leftjoin('auth.department AS dep','dep.id','=','t1.general_department')
					->leftjoin('auth.department AS dep1','dep1.id','=','t1.department')
					->leftjoin('auth.blood_group AS bg','bg.id','=','t1.blood_group')
                    // ->leftjoin('auth.position_short_title AS ps','ps.id','=','t1.short_title_id')
                    ->leftjoin('hr.employee_janebe_card as ejc','t1.id','=','ejc.employee_id') 
                    ->where('t1.id',$id)
                    ->first();
                                        
			$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			if($row->dep_type != $user_dep_type)
			{
				return showWarning();
			}
			// if($row->card_issued>0)
			// {
			// 	return '<center>برای این کارمند قبلا کارت پرنت شده است.!</center>';
			// }
			if($row->employee_type==4 || $row->employee_type==5 || $row->employee_type==6)
			{
				return '<center>این کارمند بالمقطع میباشد!.</center>';
			}
			// if($row->RFID !=null && $row->dep_type==2)
			// {
			// 	return '<center>این کارمند دارای کارت میباشد!.</center>';
			// }
			elseif($row->ready_to_print==0)
			{
				return '<center>کارت برای پرنت آماده نیست!.</center>';
			}
			else
			{
				hrOperation::insertRecord('emp_cards',array('employee_id'=>$id,'print_by'=>Auth::user()->id,'print_at'=>date('Y-m-d H:i:s')));
				hrOperation::update_record('employees',array('card_issued'=>date('Y-m-d H:i:s')),array('id'=>$id));
				if($user_dep_type == 2)
				{
					if($row->sudep_id==92 || $row->sudep_id==93)
					{
						if($row->eid==5130 || $row->eid==3732 || $row->eid==6960 || $row->eid==23 || $row->eid==5130 || $row->eid==2039)
						{
							return View::make('hr.card_print',array('row'=>$row));
						}
						else
						{
							return View::make('hr.card',array('row'=>$row));
						}
                    }
                    elseif($card_type=='wahid_janabi')
                    {
                        return View::make('hr.wahid_janabi_card',array('row' => $row,'print' => true)); 
                    }
					else
					{
						return View::make('hr.card_print',array('row'=>$row));
					}
				}
				else
				{
					return View::make('hr.ocs_card_new_print',array('row'=>$row));
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function cardTemplateView($id=0)
	{
		if(canPrintCard('hr_cards'))
		{
			$row = DB::connection('hr')
					->table('employees AS t1')
					->select(
						't1.id AS eid',
						't1.name_dr',
						't1.name_en',
						't1.last_name',
						't1.employee_type',
						//'t1.position_en',
						't1.current_position_dr',
						't1.current_position_en',
						'dep.name AS general_department_dr',
						'dep1.name AS department_dr',
						'dep.name_en AS general_department_en',
						'dep1.name_en AS department_en',
						'dep.id AS dep_id',
						'bg.name_en AS blood_group',
						'ps.name_dr AS position_dr',
						'ps.name_en AS position_en',
						'ps.color AS color',
                        'ps.id AS position_id',
                        'ejc.position_short_title_id as pshort_id', 
						't1.photo'
						)
					->leftjoin('auth.department AS dep','dep.id','=','t1.general_department')
					->leftjoin('auth.department AS dep1','dep1.id','=','t1.department')
					->leftjoin('auth.blood_group AS bg','bg.id','=','t1.blood_group')
                    ->leftjoin('auth.position_short_title AS ps','ps.id','=','t1.short_title_id')
                    ->leftjoin('hr.employee_janebe_card as ejc','t1.id','=','ejc.employee_id')
					->where('t1.id',$id)
                    ->first();
                    
			return View::make('hr.cardOld',array('row'=>$row,'print' => false));
		}
		else
		{
			return showWarning();
		}
	}
	public function newCardTemplate($id=0)
	{
		if(canPrintCard('hr_cards'))
		{
			$row = DB::connection('hr')
					->table('employees AS t1')
					->select(
						't1.id AS eid',
						't1.name_dr',
						't1.name_en',
						't1.last_name',
						//'t1.position_dr',
						//'t1.position_en',
						't1.current_position_dr',
						't1.current_position_en',
						'dep.name_pa AS general_department_dr',
						'dep1.name_pa AS department_dr',
						'dep.name_en AS general_department_en',
						'dep1.name_en AS department_en',
						'dep.id AS dep_id',
						'dep1.id AS sudep_id',
						'bg.name_en AS blood_group',
						'ps.name_dr AS position_dr',
						'ps.name_en AS position_en',
						'ps.color AS color',
						'ps.id AS position_id',
						't1.photo',
						't1.ready_to_print',
						't1.RFID'
						)
					->leftjoin('auth.department AS dep','dep.id','=','t1.general_department')
					->leftjoin('auth.department AS dep1','dep1.id','=','t1.department')
					->leftjoin('auth.blood_group AS bg','bg.id','=','t1.blood_group')
					->leftjoin('auth.position_short_title AS ps','ps.id','=','t1.short_title_id')
					->where('t1.id',$id)
					->first();
			if($row->RFID !=null)
			{
				return '<center>This user already has printed card</center>';
			}
			elseif($row->ready_to_print==0)
			{
				return '<center>Card is not ready for print</center>';
			}
			else
			{
				return View::make('hr.new_card',array('row'=>$row));
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function cardTemplateTemp($id=0)
	{
		if(canPrintCard('hr_cards'))
		{
			$row = DB::connection('hr')
					->table('employees AS t1')
					->select(
						't1.id AS eid',
						't1.name_dr',
						't1.name_en',
						't1.last_name',
						//'t1.position_dr',
						//'t1.position_en',
						't1.current_position_dr',
						't1.current_position_en',
						'dep.name AS general_department_dr',
						'dep1.name AS department_dr',
						'dep.name_en AS general_department_en',
						'dep1.name_en AS department_en',
						'dep.id AS dep_id',
						'dep1.id AS sudep_id',
						'bg.name_en AS blood_group',
						'ps.name_dr AS position_dr',
						'ps.name_en AS position_en',
						'ps.color AS color',
						'ps.id AS position_id',
						't1.contract_expire_date AS expire',
						't1.employee_type',
						't1.photo',
						't1.ready_to_print',
						't1.RFID',
						't1.card_issued'
						)
					->leftjoin('auth.department AS dep','dep.id','=','t1.general_department')
					->leftjoin('auth.department AS dep1','dep1.id','=','t1.department')
					->leftjoin('auth.blood_group AS bg','bg.id','=','t1.blood_group')
					->leftjoin('auth.position_short_title AS ps','ps.id','=','t1.short_title_id')
					->where('t1.id',$id)
					->first();
			if($row->card_issued>0)
			{
				return '<center>برای این کارمند قبلا کارت پرنت شده است.!</center>';
			}
			if($row->RFID >0)
			{
				dd('این کارمند دارای کارت میباشد');
			}
			elseif($row->ready_to_print==0)
			{
				dd('کارت برای پرنت آماده نیست');
			}
			elseif($row->expire==null)
			{
				return Redirect::route('getEmployeeIDs')->with("fail","تاریخ ختم قرارداد موجود نیست!");
			}
			else
			{
				hrOperation::insertRecord('emp_cards',array('employee_id'=>$id,'print_by'=>Auth::user()->id,'print_at'=>date('Y-m-d H:i:s')));
				$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
				if($user_dep_type==2)
				{
					hrOperation::update_record('employees',array('card_issued'=>date('Y-m-d H:i:s')),array('id'=>$id));
					return View::make('hr.cardTemp',array('row'=>$row));
				}
				else
				{
					return View::make('hr.cardTemp_ocs',array('row'=>$row));
				}

			}
		}
		else
		{
			return showWarning();
		}
	}
	//bring related department
	public function bringSubDepartment()
	{
		$deps = getRelatedSubDepartment(Input::get('dep_id'));

		$options = '<option value="0">همه</option>';
		foreach($deps AS $item)
		{
			$options.="<option value='".$item->id."'>".$item->name."</option>";
		}

		return $options;
	}
	//Load documents form entry view
	public function getAllEmployees()
	{
		//check roles
		if(canView('hr_employee'))
		{
			//check roles
			return View::make('hr.employee_list');
		}
		else
		{
			return showWarning();
		}
	}
	//get employee ids
	public function getEmployeeIDs()
	{
		//check roles
		if(canUpdateCard('hr_cards') || canPrintCard('hr_cards'))
		{
			$data['parentDeps'] = getMainDepartments();
			return View::make('hr.employee_ids',$data);
		}
		else
		{
			return showWarning();
		}

	}
	public function getEmployeeIDs_search()
	{
		//check roles
		if(canUpdateCard('hr_cards') || canPrintCard('hr_cards'))
		{
			$dep 		 = Input::get('dep');
            $sub_dep 	 = Input::get('sub_dep');
            $card_issued = Input::get('card_issued'); 
            $from_date   = Input::get('from_date'); 
            $to_date     = Input::get('to_date'); 
			$data['records'] = hrOperation::getDataCard_search($dep,$sub_dep,$card_issued,$from_date,$to_date);
			return View::make('hr.employee_ids_search',$data);
		}
		else
		{
			return showWarning();
		}

	}
	//get form data list
	public function getEmployeeData()
	{
		/*
		$user_deps = getHRUserDeps();
		if($user_deps)
		{
			$user_deps = explode(',',$user_deps->deps);
			//$user_deps = $user_deps->deps;
		}
		else
		{
			$user_deps = 0;
		}
		*/
		$object = hrOperation::getData('dr');//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name',
							'number_tayenat',
							'department',
							'original_province',
							'bast',
							'emp_date'
							)
				->addColumn('operations', function($option){
					$options = '';
					/*
					$options .= '<a href="'.route('getEmployeeDetails',$option->id).'" class="table-link">
														<span class="fa-stack">
										<i class="fa fa-square fa-stack-2x"></i>
										<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
									</span>
								</a>
								';
					*/
					if(canEdit('hr_recruitment') || canEdit('hr_documents'))
					{
						$options .= '<a href="'.route('getDetailsEmployee',Crypt::encrypt($option->id)).'" class="table-link" style="text-decoration:none;" title="Edit">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
					}
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	//get form data list
	public function getEmployeeCardData()
	{
		//get all data
		$object = hrOperation::getDataCard();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'father_name',
							'number_tayenat',
							'gender',
							'original_province',
							'rank',
							'emp_date'
                            )
                ->addColumn('employee_type', function($option){
					return trans('static.employee_type_'.$option->employee_type);
				})
				->addColumn('operations', function($option){
					$options = '';
						if(Auth::user()->id==174)
						{
							$options.='<a href="javascript:void()" class="table-link" data-target="#reprint_modal" data-toggle="modal" onclick="$(\'#card_id\').val('.$option->id.')">
													<span class="fa-stack" title="Release Card for Reprint">
														<span class="icon wb-replay"></span>
													</span>
												</a>';
						}
						if(canUpdateCard('hr_cards'))
						{
							if(Auth::user()->id!=203)
							{
							$options.= '<a href="'.route('getEmployeeCardDetails',$option->id).'" class="table-link" style="text-decoration:none;" title="update">
										<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
									</a>
									| ';
							}
							$options.=
									' <a href="'.route('forPrint',$option->id).'" class="table-link" style="text-decoration:none;" title="Details">
										<i class="icon fa-eye" aria-hidden="true" style="font-size: 16px;"></i>
									</a> |
									<a href="javascript:void()" class="table-link" data-target="#rfid_modal" data-toggle="modal" onclick="$(\'#record_id\').val('.$option->id.')">
										<span class="fa-stack" title="Enter RFID">
											<span class="icon wb-edit"></span>
										</span>
									</a>';
						}
						if(canPrintCard('hr_cards'))
						{
							if($option->employee_type == 4 || $option->employee_type == 5 || $option->employee_type == 6)
							{//temporary employees
								/*
								<a href="javascript:void()" class="table-link" data-target="#rfid_modal" data-toggle="modal" onclick="$(\'#record_id\').val('.$option->id.')">
										<span class="fa-stack" title="Enter RFID">
											<span class="icon wb-edit"></span>
										</span>
									</a>
								*/
								$options.= '<a target="_blank" href="'.route('getCardTemplateTemp',$option->id).'" class="table-link" style="text-decoration:none;" title="print">
										<i class="icon fa-print" aria-hidden="true" style="font-size: 16px;"></i>
									</a>';
							}
							else
							{
								/*
								<a href="javascript:void()" class="table-link" data-target="#rfid_modal" data-toggle="modal" onclick="$(\'#record_id\').val('.$option->id.')">
										<span class="fa-stack" title="Enter RFID">
											<span class="icon wb-edit"></span>
										</span>
									</a>
								*/

								$options.= '<a target="_blank" href="'.route('getCardTemplate',$option->id).'" class="table-link" style="text-decoration:none;" title="print">
										<i class="icon fa-print" aria-hidden="true" style="font-size: 16px;"></i>
									</a>
									 | <a target="_blank" href="'.route('getCardTemplate_view',$option->id).'" class="table-link" style="text-decoration:none;" title="view card">
										<i class="icon fa-eye" aria-hidden="true" style="font-size: 16px;"></i>
									</a>';
							}
						}
						return $options;
				})
				->make();
	}
	//Load documents form entry view
	public function getEmployeeForPrint()
	{
		//check roles
		//if(canView('hr_employee'))
		//{
			$data['parentDeps'] = getDepartmentWhereIn();
			//check roles
			return View::make('hr.employee_print_list',$data);
		//}
		//else
		//{
			//return showWarning();
		//}
	}
	//get form data list
	public function getEmployeeDataPrint()
	{
		//get all data
		$object = hrOperation::getData();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'department',
							'name',
							'current_position_dr'
							)
						->addColumn('name_en', function($option){

							return "";
						})
						->addColumn('position_en', function($option){

							return "";
						})

				// ->orderColumns('id','id')
				->make();
	}
	//get register form for employee
	public function getRegisterForm()
	{
		//check roles
		if(canAdd('hr_employee'))
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			//get province
			$data['provinces'] = getAllProvinces('dr');
			//load view for registring
			return View::make('hr.employee_add',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//get employee ids
	public function getEmployeeCardForm($id=0)
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$emp_dep_type = hrOperation::getEmployeeDet($id)->dep_type;
		if($user_dep_type != $emp_dep_type)
		{
			return showWarning();
		}
		//check roles
		if(canUpdateCard('hr_cards') && Auth::user()->id!=203)
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['parentDeps'] = getAllParentDepartments();
			//check roles
			if($user_dep_type==2)
			{
                $data['position_id'] = Wahede_janebe::select('position_short_title_id as position_id')->where('employee_id',$id)->first();
				return View::make('hr.employee_card_form',$data);
			}
			else
			{
				return View::make('hr.employee_card_form_ocs',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	//get register form for employee
	public function getEmployeeDetails($id=0)
	{
		//check roles
		if(canView('hr_employee'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachments'] = hrOperation::getDocuments($id);
			$data['parentDeps'] = getDepartmentWhereIn();
			//get province
			$data['provinces'] = getAllProvinces('dr');
			//load view for registring
			return View::make('hr.employee_details',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//get register form for employee
	public function forPrint($id=0)
	{
		//check roles
		if(canUpdateCard('hr_cards'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['parentDeps'] = getAllDepartments();
			//load view for registring
			return View::make('hr.for_signature',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//check data if ready to print
	public function checkCardReady()
	{
		DB::connection(self::$myDb)->table('employees')->where('id',Input::get('id'))->update(array('ready_to_print'=>Input::get('ready')));
	}
	//save rfid
	public function saveRFID()
	{
		DB::connection(self::$myDb)
		->table('employees')
		->where('id',Input::get('record_id'))
		->update(array('RFID'=>Input::get('RFID'),'att_in_date'=>date('Y-m-d')));

		hrOperation::insertLog('employees',1,'employee RFID',Input::get('record_id'));
		return Redirect::route('getEmployeeIDs')->with("success","Record updated successfully");
	}
	//release card for reprint
	public function releaseCard()
	{
		DB::connection(self::$myDb)
		->table('employees')
		->where('id',Input::get('card_id'))
		->update(array('card_issued'=>NULL));

		hrOperation::insertLog('employees_cards',1,Input::get('reprint_reason'),Input::get('card_id'));
		return Redirect::route('getEmployeeIDs')->with("success","Record updated successfully");
	}
	//Load documents form entry view
	public function postEmployee()
	{
		//check roles
		if(canAdd('hr_employee'))
		{
			if(Input::get('vacant')!=1)
			{
				//validate fields
				$validates = Validator::make(Input::all(), array(
					"name_dr"						=> "required",
					"current_position_dr"			=> "required",
					// "emp_rank"					=> "required",
					"position_dr"					=> "required"
				));
			}
			else
			{
				//validate fields
				$validates = Validator::make(Input::all(), array(
					"vacant"						=> "required"
				));
			}
			//check the validation
			if($validates->fails())
			{
				return Redirect::route('getRegisterForm')->withErrors($validates)->withInput();
			}
			else
			{
				$object = new hrOperation;

				$object->number_tayenat 						= Input::get("number_tayenat");
				$object->vacant									= Input::get("vacant");
				if(Input::get('vacant') == 1)
				{

					$object->emp_bast								= Input::get("emp_bast1");
					$object->current_position_dr					= Input::get("current_position_dr1");
					$object->general_department						= Input::get("general_department1");
					$object->department								= Input::get("sub_dep1");
					$object->ageer_rank								= Input::get("ageer_rank1");
					$object->ageer_bast								= Input::get("ageer_bast1");

				}
				else
				{
					$object->name_dr 								= Input::get("name_dr");
					//$object->name_en 								= Input::get("name_en");
					$object->father_name_dr 						= Input::get("father_name_dr");
					//$object->father_name_en 						= Input::get("father_name_en");
					$object->gender 								= Input::get("gender");
					$object->original_province 						= Input::get("original_province");
					$object->nationality 							= Input::get("nationality");
					$object->district 								= Input::get("district");
					$object->education_degree						= Input::get("education_degree");
					$object->education_field						= Input::get("education_field");
					$object->blood_group							= Input::get("blood_group");
					$object->birth_year								= Input::get("birth_year");
					$object->first_date_appointment					= Input::get("first_appointment_date");
					$object->last_date_tarfee						= Input::get("last_appointment_date");
					if(Input::get('employee_type') == 3)
					{
						$object->emp_rank								= Input::get("military_rank");
						$object->emp_bast								= Input::get("military_bast");
					}
					else
					{
						$object->emp_rank								= Input::get("emp_rank");
						$object->emp_bast								= Input::get("emp_bast");
					}

					$object->ageer_rank								= Input::get("ageer_rank");
					$object->ageer_bast								= Input::get("ageer_bast");
					$object->appointment_date_current_position		= Input::get("appointment_current_position");
					$object->phone									= Input::get("phone");
					$object->current_position_dr						= Input::get("current_position_dr");
					//$object->current_position_en						= Input::get("current_position_en");
					$object->position_dr							= Input::get("position_dr");
					//$object->position_en							= Input::get("position_en");
					$object->employee_type							= Input::get("employee_type");
					$object->free_compitition						= Input::get("free_competition");
					//$object->department_en							= Input::get("department_en");
					$object->general_department						= Input::get("general_department");
					$object->department								= Input::get("sub_dep");
					//$object->general_department_en					= Input::get("general_department_en");
					$object->education_rank							= Input::get("education_rank");
					$object->mawqif_employee						= Input::get("mawqif_employee");
					$object->email									= Input::get("email");
					$object->job_description						= Input::get("job_description");
				}

				$object->created_by 							= Auth::user()->id;

				if($object->save())
				{
					$inserted_id = $object->id;

					$this->uploadDocs($inserted_id);

					return Redirect::route('getAllEmployees')->with("success","Record saved successfully");
				}
				else
				{
					return Redirect::route("getAllEmployees")->with("fail","An error occured plase try again.");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	//update document
	public function postUpdateEmployee($doc_id=0)
	{
		if(canEdit('hr_employee'))
		{
			if(Input::get('vacant')!=1)
			{
				//validate fields
				$validates = Validator::make(Input::all(), array(
					"name_dr"						=> "required",
					"current_position_dr"			=> "required",
					// "emp_rank"					=> "required",
					"position_dr"					=> "required"
				));
			}
			else
			{
				//validate fields
				$validates = Validator::make(Input::all(), array(
					"vacant"						=> "required"
				));
			}

			//check the validation
			if($validates->fails())
			{
				return Redirect::route('getRegisterForm')->withErrors($validates)->withInput();
			}
			else
			{
				$object = hrOperation::find($doc_id);

				$object->number_tayenat 						= Input::get("number_tayenat");
				$object->vacant									= Input::get("vacant");
				if(Input::get('vacant') == 1)
				{
					$object->emp_bast								= Input::get("emp_bast1");
					$object->current_position_dr						= Input::get("current_position_dr1");
					$object->general_department						= Input::get("general_department1");
					$object->department								= Input::get("sub_dep1");
					$object->ageer_rank								= Input::get("ageer_rank1");
					$object->ageer_bast								= Input::get("ageer_bast1");
				}
				else
				{
					$object->name_dr 								= Input::get("name_dr");
					//$object->name_en 								= Input::get("name_en");
					$object->father_name_dr 						= Input::get("father_name_dr");
					//$object->father_name_en 						= Input::get("father_name_en");
					$object->gender 								= Input::get("gender");
					$object->original_province 						= Input::get("original_province");
					$object->nationality 							= Input::get("nationality");
					$object->district 								= Input::get("district");
					$object->education_degree						= Input::get("education_degree");
					$object->education_field						= Input::get("education_field");
					$object->blood_group							= Input::get("blood_group");
					$object->birth_year								= Input::get("birth_year");
					$object->first_date_appointment					= Input::get("first_appointment_date");
					$object->last_date_tarfee						= Input::get("last_appointment_date");
					if(Input::get('employee_type') == 3)
					{
						$object->emp_rank								= Input::get("military_rank");
						$object->emp_bast								= Input::get("military_bast");

					}
					else
					{
						$object->emp_rank								= Input::get("emp_rank");
						$object->emp_bast								= Input::get("emp_bast");
					}
					$object->ageer_rank								= Input::get("ageer_rank");
					$object->ageer_bast								= Input::get("ageer_bast");
					$object->appointment_date_current_position		= Input::get("appointment_current_position");
					$object->phone									= Input::get("phone");
					$object->current_position_dr						= Input::get("current_position_dr");
					//$object->current_position_en						= Input::get("current_position_en");
					$object->position_dr							= Input::get("position_dr");
					//$object->position_en							= Input::get("position_en");
					$object->employee_type							= Input::get("employee_type");
					$object->free_compitition						= Input::get("free_competition");

					//$object->department_en							= Input::get("department_en");
					$object->general_department						= Input::get("general_department");
					$object->department								= Input::get("sub_dep");
					//$object->general_department_en					= Input::get("general_department_en");

					$object->education_rank							= Input::get("education_rank");
					$object->mawqif_employee						= Input::get("mawqif_employee");
					$object->email									= Input::get("email");
					$object->job_description						= Input::get("job_description");
				}

				$object->updated_by 							= Auth::user()->id;

				if($object->save())
				{
					$inserted_id = $doc_id;

					$this->uploadDocs($inserted_id);

					return Redirect::route('getAllEmployees')->with("success","Record updated successfully");
				}
				else
				{
					return Redirect::route("getAllEmployees")->with("fail","An error occured plase try again.");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	//update document
	public function updateEmployee($doc_id=0)
	{
			//check roles
		if(canEdit('hr_recruitment'))
		{
			//validate
			if(input::get('tashkil_id')==0)
			{//dont validate if user is not active
				$validates = Validator::make(Input::all(), array(
						"name_dr"						=> "required",
						//"current_position_dr"			=> "required",
						//"first_appointment_date"		=> "required",
						"blood_group"					=> "required",
						//"general_department1"			=> "required",
						//"sub_dep1"						=> "required",
						"father_name_dr"				=> "required",
						"grand_father_name"				=> "required"

					));
			}
			else
			{
				if(input::get('employee_type')==4 || input::get('employee_type')==5 || input::get('employee_type')==6)
				{//temporary employees
					$validates = Validator::make(Input::all(), array(
						"employee_type"					=> "required",
						"name_dr"						=> "required",
						"last_date"						=> "required",
						//"first_appointment_date"		=> "required",
						"blood_group"					=> "required",
						"general_department1"			=> "required",
						"sub_dep1"						=> "required",
						"father_name_dr"				=> "required",
						"grand_father_name"				=> "required"

					));
				}
				else
				{//normal employees
					$validates = Validator::make(Input::all(), array(
						"name_dr"						=> "required",
						//"tashkil"						=> "required",
						"employee_type"					=> "required",
						"blood_group"					=> "required",
						//"general_department"			=> "required",
						//"sub_dep"						=> "required",
						"father_name_dr"				=> "required",
						"grand_father_name"				=> "required"

					));
				}
			}
			//check the validation
			if($validates->fails())
			{
				return Redirect::route('getDetailsEmployee',Crypt::encrypt($doc_id))->withErrors($validates)->withInput();
			}
			else
			{
				$object = hrOperation::find($doc_id);
				//hrOperation::update_record('employees',array('tashkil_id_old'=>$object->tashkil_id),array('id'=>$doc_id));
				//update the status of current tashkil id from used to unused because it is gonna be updated
				//hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$object->tashkil_id));
				$object->vacant									= 2;//mawjod
				$object->employee_type							= Input::get("employee_type");
				if(Input::get('employee_type') == 4 || Input::get('employee_type') == 5 || input::get('employee_type')==6)
				{//contract
					$object->contract_expire_date				= Input::get("last_date");
					$object->general_department					= Input::get("general_department1");
					$object->department							= Input::get("sub_dep1");
				}
				else
				{
					//$object->number_tayenat 					= Input::get("number_tayenat");
					//$object->general_department					= Input::get("general_department");
					//$object->department							= Input::get("sub_dep");
					if(Input::get("position_dr")==7 || Input::get("position_dr")==4)
					{//ezafa bast || wait for salary
						$object->tashkil_id							= 0;
					}
					else
					{
						//$object->tashkil_id							= Input::get('tashkil');
					}
					$object->position_dr						= Input::get("position_dr");
					if(Input::get('employee_type') == 3)
					{//military
						$object->emp_rank						= Input::get("military_rank");
						//$object->emp_bast						= Input::get("military_bast");

					}
					elseif(Input::get('employee_type') == 2)
					{//ajeer
						$object->emp_rank						= Input::get("ajeer_rank");
					}
					else
					{
						$object->emp_rank						= Input::get("emp_rank");
						//$object->emp_bast						= Input::get("emp_bast");
					}
				}

				$object->free_compitition						= Input::get("free_compitition");
				//$object->mawqif_employee						= Input::get("mawqif_employee");
				$object->first_date_appointment					= Input::get("first_appointment_date");
				$object->current_position_dr					= Input::get("current_position_dr");
				$object->name_dr 								= Input::get("name_dr");
				$object->father_name_dr 						= Input::get("father_name_dr");
				$object->nationality 							= Input::get("nationality");
				$object->gender 								= Input::get("gender");
				$object->blood_group							= Input::get("blood_group");
				$object->birth_year								= Input::get("birth_year");
				$object->original_province 						= Input::get("original_province");
				$object->district 								= Input::get("district");
				//$object->education_degree						= Input::get("education_degree");
				//$object->education_field						= Input::get("education_field");
				$object->phone									= Input::get("phone");
				$object->email									= Input::get("email");
				$object->last_name 								= Input::get("last_name");
				$object->grand_father_name 						= Input::get("grand_father_name");
				$object->id_no 									= Input::get("id_no");
				$object->id_jild								= Input::get("id_jild");
				$object->id_page								= Input::get("id_page");
				$object->id_sabt								= Input::get("id_sabt");
				$object->village 								= Input::get("village");
				$object->current_province 						= Input::get("current_province");
				$object->current_district 						= Input::get("current_district");
				$object->current_village 						= Input::get("current_village");
				//$object->education_place						= Input::get("education_place");
				//$object->graduation_year						= Input::get("graduation_year");
				//$object->updated_by 							= Auth::user()->id;

				$emp_bast = hrOperation::getEmployeeBast(Input::get('tashkil'));
				if($emp_bast)
				{
					//$object->emp_bast								= $emp_bast;
				}
				//$object->edu_location							= Input::get("edu_location");

				$object->updated_by 							= Auth::user()->id;
				$object->updated_at 							= date('Y-m-d H:i:s');

				if($object->save())
				{
					hrOperation::insertLog('employees',1,'details of given employee',$doc_id);
					if(Input::get('tashkil'))
					{//update the bast id to used
						//hrOperation::update_record('tashkilat',array('status'=>1),array('id'=>Input::get('tashkil')));
					}

					$inserted_id = $doc_id;
					if(canEdit('hr_documents'))
					{
						hrOperation::delete_record('employee_educations',array('employee_id'=>$inserted_id));
						for($e=1;$e<=2;$e++)
						{
							if(Input::get('education_degree_'.$e)!='')
							{
								$edu = array(
									"employee_id" 		=> $inserted_id,
									"education_id" 		=> Input::get('education_degree_'.$e),
									"education_field" 	=> Input::get('education_field_'.$e),
									"edu_location"		=> Input::get('edu_location_'.$e),
									"education_place" 	=> Input::get('education_place_'.$e),
									"graduation_year" 	=> Input::get('graduation_year_'.$e)
								);
								hrOperation::insertRecord('employee_educations',$edu);
							}
						}
					}
					/*
					hrOperation::delete_record('employee_experiences',array('employee_id'=>$inserted_id,'type'=>0));
					for($i=1;$i<=3;$i++)
					{
						if(Input::get('experience_position_'.$i)!='')
						{
							$experience = array(
								"employee_id" 	=> $inserted_id,
								"organization" 	=> Input::get('experience_company_'.$i),
								"position" 		=> Input::get('experience_position_'.$i),
								"bast"			=> Input::get('experience_bast_'.$i),
								"leave_reason" 	=> Input::get('experience_leave_'.$i),
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
							hrOperation::insertRecord('employee_experiences',$experience);
						}
					}
					hrOperation::delete_record('employee_languages',array('employee_id'=>$inserted_id));
					for($i=1;$i<=4;$i++)
					{
						$langs = array(
								"employee_id" 	=> $inserted_id,
								"language" 		=> Input::get('lang_'.$i),
								"writing" 		=> Input::get('writing_'.$i),
								"speaking"		=> Input::get('speaking_'.$i),
								"reading" 		=> Input::get('reading_'.$i),
								"typing"		=> Input::get('typing_'.$i),
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
						hrOperation::insertRecord('employee_languages',$langs);
					}
					hrOperation::delete_record('employee_garantee',array('employee_id'=>$inserted_id));
					for($i=1;$i<=2;$i++)
					{
						if(Input::get('garantee_name_'.$i)!='')
						{
							$garantee = array(
								"employee_id" 	=> $inserted_id,
								"name" 			=> Input::get('garantee_name_'.$i),
								"job" 			=> Input::get('garantee_job_'.$i),
								"organization"	=> Input::get('garantee_company_'.$i),
								"phone" 		=> Input::get('garantee_phone_'.$i),
								"email"			=> Input::get('garantee_email_'.$i),
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
							hrOperation::insertRecord('employee_garantee',$garantee);
						}
					}
					*/
					$this->uploadDocs($inserted_id);

					return Redirect::route('getDetailsEmployee',Crypt::encrypt($doc_id))->with("success","Record updated successfully");
				}
				else
				{
					return Redirect::route("getDetailsEmployee",Crypt::encrypt($doc_id))->with("fail","An error occured plase try again.");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function updateEmployeeContract($doc_id=0)
	{
			//check roles
		if(canEdit('hr_recruitment'))
		{
			//validate fields
			if(input::get('tashkil_id')==0)
			{//dont validate if user is not active
				$validates = Validator::make(Input::all(), array(
						"name_dr"						=> "required",
						//"current_position_dr"			=> "required",
						//"first_appointment_date"		=> "required",
						"blood_group"					=> "required",
						//"general_department1"			=> "required",
						//"sub_dep1"						=> "required",
						"father_name_dr"				=> "required",
						"grand_father_name"				=> "required"

					));
			}
			else
			{
				if(input::get('employee_type')==4 || input::get('employee_type')==5 || input::get('employee_type')==6)
				{//temporary employees
					$validates = Validator::make(Input::all(), array(
						"employee_type"					=> "required",
						"name_dr"						=> "required",
						"last_date"						=> "required",
						//"first_appointment_date"		=> "required",
						"blood_group"					=> "required",
						"general_department1"			=> "required",
						"sub_dep1"						=> "required",
						"father_name_dr"				=> "required",
						"grand_father_name"				=> "required"

					));
				}
				else
				{//normal employees
					$validates = Validator::make(Input::all(), array(
						"name_dr"						=> "required",
						"tashkil"						=> "required",
						"employee_type"					=> "required",
						"blood_group"					=> "required",
						"general_department"			=> "required",
						"sub_dep"						=> "required",
						"father_name_dr"				=> "required",
						"grand_father_name"				=> "required"

					));
				}
			}
			//check the validation
			if($validates->fails())
			{
				return Redirect::route('getContractEmployee',$doc_id)->withErrors($validates)->withInput();
			}
			else
			{
				$object = hrOperation::find($doc_id);

				//$object->number_tayenat 						= Input::get("number_tayenat");
				//$object->vacant									= Input::get("vacant");

					$object->employee_type							= Input::get("employee_type");
					$object->free_compitition						= Input::get("free_compitition");
					if(Input::get("employee_type")==4 || Input::get("employee_type")==5 || input::get('employee_type')==6)
					{
						$object->mawqif_employee						= Input::get("mawqif_employee");
						$object->general_department						= Input::get("general_department1");
						$object->department								= Input::get("sub_dep1");
					}

					else
					{
						$object->mawqif_employee						= 1;
						$object->general_department						= Input::get("general_department");
						$object->department								= Input::get("sub_dep");
					}
					$object->first_date_appointment					= Input::get("first_appointment_date");
					$object->current_position_dr					= Input::get("current_position_dr");


					//$object->position_dr							= Input::get("position_dr");
					//$object->emp_bast								= $emp_bast;
					$object->contract_expire_date					= Input::get("last_date");

					$object->name_dr 								= Input::get("name_dr");
					$object->father_name_dr 						= Input::get("father_name_dr");
					$object->last_name 								= Input::get("last_name");
					$object->grand_father_name 						= Input::get("grand_father_name");
					$object->id_no 									= Input::get("id_no");
					$object->id_jild								= Input::get("id_jild");
					$object->id_page								= Input::get("id_page");
					$object->id_sabt								= Input::get("id_sabt");
					$object->nationality 							= Input::get("nationality");
					$object->gender 								= Input::get("gender");
					$object->blood_group							= Input::get("blood_group");
					$object->birth_year								= Input::get("birth_year");
					$object->original_province 						= Input::get("original_province");
					$object->district 								= Input::get("district");
					$object->village 								= Input::get("village");
					$object->current_province 						= Input::get("current_province");
					$object->current_district 						= Input::get("current_district");
					$object->current_village 						= Input::get("current_village");
					//$object->education_degree						= Input::get("education_degree");
					//$object->education_field						= Input::get("education_field");
					//$object->education_place						= Input::get("education_place");
					//$object->graduation_year						= Input::get("graduation_year");
					//$object->edu_location							= Input::get("edu_location");
					$object->phone									= Input::get("phone");
					$object->email									= Input::get("email");

				$object->updated_by 								= Auth::user()->id;
				$object->updated_at 								= date('Y-m-d H:i:s');

				if($object->save())
				{
					hrOperation::insertLog('employees',1,'details of given employee',$doc_id);

					return Redirect::route('getRecruitment')->with("success","Record updated successfully");
				}
				else
				{
					return Redirect::route("getContractEmployee",$doc_id)->with("fail","An error occured plase try again.");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	//update document
	public function postEmployeeCard($doc_id=0)
	{   
        // $janebe_id					                = Input::get("short_title_janebe_id");
        // echo $janebe_id."sssssss";exit;
		if(canUpdateCard('hr_cards'))
		{
			$validates = Validator::make(Input::all(), array(
				"name_en"						=> "required",
				"current_position_en"			=> "required",
				"short_title_id"				=> "required"
			));

			//check the validation
			if($validates->fails())
			{
				return Redirect::route('getEmployeeCardDetails',array($doc_id))->withErrors($validates)->withInput();
			}
			else
			{
				$object = hrOperation::find($doc_id);
				$object->name_en 								= Input::get("name_en");
				//$object->father_name_dr 						= Input::get("father_name_dr");
				//$object->father_name_en 						= Input::get("father_name_en");

				//$object->current_position_dr					= Input::get("current_position_dr");
				$object->current_position_en					= Input::get("current_position_en");
				//$object->position_dr							= Input::get("position_dr");
				//$object->position_en							= Input::get("position_en");
                $object->short_title_id							= Input::get("short_title_id");
                
				if($object->save())
				{
                    if(Input::get("short_title_janebe_id")!='')
                   { 
                       $janebe_id					                = Input::get("short_title_janebe_id");
                       $user_id                                    = Auth::user()->id;
                       $wahedejanebe = Wahede_janebe::where('employee_id',$doc_id)->first();
                    
                    if($wahedejanebe){
                        $wahedejanebe->position_short_title_id = $janebe_id;
                        $wahedejanebe->employee_id    = $doc_id;
                        $wahedejanebe->user_id   = $user_id;
                        $wahedejanebe->save();
                       // Wahede_janebe::UpdateJanebeRecord($inserted_id,$user_id,$janebe_id);
                    }   
                    else{
                        $wahedejanebe            = new Wahede_janebe;
                        $wahedejanebe->position_short_title_id = $janebe_id;
                        $wahedejanebe->employee_id    = $doc_id;
                        $wahedejanebe->user_id   = $user_id;
                        $wahedejanebe->save();
                        //Wahede_janebe::InsertJanebeRecord($inserted_id,$user_id,$janebe_id);
                     } 
                    }
					$this->uploadCardPhoto($doc_id);
					if(Input::hasFile('signature'))
					{
						$this->uploadPhoto($doc_id);
					}
					hrOperation::insertLog('employees',1,'details of card',$doc_id);
					//------------------------------------------//
					return Redirect::route('getEmployeeIDs')->with("success","Card information updated successfully");
				}
				else
				{
					return Redirect::route("getEmployeeIDs")->with("fail","An error occured plase try again.");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	//upload document
	public function uploadDocs($doc_id=0)
	{
		// getting all of the post data
		$files = Input::file('files');
		$errors = "";
		$file_data = array();
		if(Input::hasFile('files'))
		{
			foreach($files as $file)
			{
		  		// validating each file.
		  		$rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
		  		$validator = Validator::make(

			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'file' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png,doc,docx,zip,rar,pdf,rtf,xlsx,xls,txt'
			        ]
			  	);

			  	if($validator->passes())
			  	{

				    // path is root/uploads
				    $destinationPath = 'documents/hr_attachments';
				    $filename = $file->getClientOriginalName();

				    $temp = explode(".", $filename);
				    $extension = end($temp);
				    $lastFileId = $doc_id.'_'.time();

				    $lastFileId++;

				    $filename = $temp[0].'_'.$lastFileId.'.'.$extension;

				    $upload_success = $file->move($destinationPath, $filename);

				    if($upload_success)
				    {
				   		$data = array(
				   					'doc_id'=>$doc_id,
				   					'file_name'=>$filename,
				   					'created_at'=>date('Y-m-d H:i:s'),
				   					'user_id'	=> Auth::user()->id
				   				);
				   		if(count($data)>0)
						{

							DB::connection('hr')->table('attachments')->insert($data);
						}

					}
					else
					{
					   $errors .= json('error', 400);
					}
				}
			  	else
			  	{
				    // redirect back with errors.
				    return Redirect::back()->withErrors($validator);
			  	}
			}
		}
	}
	//upload document
	public function uploadPhoto($doc_id=0)
	{
		if(Input::hasFile('photo'))
		{
			// getting all of the post data
			$file = Input::file('photo');
			$errors = "";
			$file_data = array();
			// validating each file.
			$rules = array('photo' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			$validator = Validator::make(

			  		[
			            'photo' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'photo' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			);

			if($validator->passes())
			{
			  	$destinationPath = 'documents/profile_pictures';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    $lastFileId = getLastDocId();

			    $lastFileId++;

			    $filename = 'profile_'.$doc_id.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success)
			    {
			    	//remove the old small profile picture
				   	$file= public_path(). "/documents/profile_pictures/small_".$filename;
				   	File::delete($file);

					DB::connection('hr')->table('employees')->where('id',$doc_id)->update(array('photo'=>$filename));
				}
				else
				{
				   $errors .= json('error', 400);
				}
			}
			else
			{
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			}
		}
		if(Input::hasFile('signature'))
		{
			// getting all of the post data
			$file = Input::file('signature');
			$errors = "";
			$file_data = array();
			// validating each file.
			$rules = array('signature' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			$validator = Validator::make(

			  		[
			            'signature' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'signature' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			);

			if($validator->passes())
			{

			    // path is root/uploads
			    $destinationPath = 'documents/signatures';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    $lastFileId = getLastDocId();

			    $lastFileId++;

			    $filename = 'signature_'.$doc_id.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success)
			    {
					DB::connection('hr')->table('employees')->where('id',$doc_id)->update(array('signature'=>$filename));
				}
				else
				{
				   $errors .= json('error', 400);
				}
			}
			else
			{
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			}
		}
	}
	//download file from server
	public function downloadDoc($file_id=0)
	{
		if(canView('hr_recruitment'))
		{
			//get file name from database
			$fileName = getEmployeeFileName($file_id);
	        //public path for file
	        $file= public_path(). "/documents/hr_attachments/".$fileName;
	        //download file
	        return Response::download($file, $fileName);
	    }
	    else
	    {
	    	return showWarning();
	    }
	}
	//remove file from folder
	public function deleteFile()
	{
		if(canEdit('hr_recruitment'))
		{
			$file_id = Input::get('doc_id');

			//get file name from database
			$fileName = getEmployeeFileName($file_id);
			$file= public_path(). "/documents/hr_attachments/".$fileName;
			if(File::delete($file))
			{
				//delete from database
				DB::connection('hr')
				->table('attachments')
				->where('id',$file_id)
				->delete();

				return "<div class='alert alert-success'>File Deleted Successfully!</div>";
			}
			else
			{
				return "<div class='alert alert-danger'>Error!</div>";
			}
		}
		else
		{
			return showWarning();
		}
	}
	//get province districtsgetAllEmployees
	public function getProvinceDistrict()
	{
		$districts = getProvinceDistrict(Input::get('province'));

		$options = "<option value=''>??????</option>";
		foreach($districts AS $item)
		{
			$options .= "<option value='".$item->id."'>".$item->name."</option>";
		}

		return $options;
	}
	//get department employees
	public function searchDepartmentEmployees()
	{
		$results = hrOperation::getDepartmentEmployees();

		$data['rows'] = $results;
		if(Input::get('sub_dep') == '')
		{
			$data['department'] = Input::get('general_department');
		}
		else
		{
			$data['department'] = Input::get('sub_dep');
		}

		return View::make('hr.print_list',$data);
	}
	//Load documents form entry view
	public function getRecruitment()
	{        
		// check roles
		if(canView('hr_recruitment') || canView('hr_documents'))
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['records'] = hrOperation::getData('dr');
			$data['records'];
			$data['active'] = 'all';
			if(Input::get('ajax') == 1)
			{
				return View::make('hr.recruitment.list_ajax',$data);
			}
			else
			{
				//load view to show searchpa result
				return View::make('hr.recruitment.list',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getRecruitment_search()
	{
		//check roles
		$data['parentDeps'] = getDepartmentWhereIn();
		$data['records'] = hrOperation::getData('dr');

		$data['active'] = 'all';
		return View::make('hr.recruitment.list_ajax',$data);
	}
	//get register form for employee
	public function addNewEmployee()
	{
		//check roles
		if(canAdd('hr_recruitment'))
		{
			$data['parentDeps'] = getMainDepartments();
			//get province
			$data['provinces'] = getAllProvinces('dr');
			//$data['tashkils'] = hrOperation::getTashkilData('dr');
			//load view for registring
			return View::make('hr.recruitment.add',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function checkEmployeeDuplicate()
	{
		if(hrOperation::check_employee_add(Input::get('name_dr'),Input::get('father_name_dr'),Input::get('grand_father_name'),Input::get('general_department'),Input::get('sub_dep')))
		{//already added
			return true;
		}
		else
		{
			return false;
		}
	}
	//Load documents form entry view
	public function postNewEmployee()
	{
		//check roles
		if(canAdd('hr_recruitment'))
		{
			//validate
			if(input::get('employee_type')==4 || input::get('employee_type')==5 || input::get('employee_type')==6)
			{//temporary employees
				$validates = Validator::make(Input::all(), array(
					"employee_type"					=> "required",
					"name_dr"						=> "required",
					"last_date"						=> "required",
					"gender"						=> "required",
					"blood_group"					=> "required",
					"general_department1"			=> "required",
					"sub_dep1"						=> "required",
					"father_name_dr"				=> "required",
					"grand_father_name"				=> "required"

				));
			}
			else
			{//normal employees
				$validates = Validator::make(Input::all(), array(
					"name_dr"						=> "required",
					"tashkil"						=> "required",
					"employee_type"					=> "required",
					"blood_group"					=> "required",
					"general_department"			=> "required",
					"sub_dep"						=> "required",
					"father_name_dr"				=> "required",
					"grand_father_name"				=> "required",
					"gender"						=> "required"
				));
			}
			$validates->after(function($validates)
			{
			    if ($this->checkEmployeeDuplicate())
			    {//check if same name,fathername,grandfathername in same general dep, sub dep
			        $validates->errors()->add('field', 'duplicate entry');
			    }
			});
			//check the validation
			if($validates->fails())
			{
				return Redirect::route('addNewEmployee')->withErrors($validates)->withInput();
			}
			else
			{
				$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
				$object = new hrOperation;

				$object->vacant									= 2;//mawjod
				$object->employee_type							= Input::get("employee_type");
				if(Input::get('employee_type') == 4 || Input::get('employee_type') == 5 || input::get('employee_type')==6)
				{//contract
					$object->contract_expire_date				= Input::get("last_date");
					$object->general_department					= Input::get("general_department1");
					$object->department							= Input::get("sub_dep1");
					$object->mawqif_employee					= 3;
				}
				else
				{
					$object->number_tayenat 					= Input::get("number_tayenat");
					$object->general_department					= Input::get("general_department");
					$object->department							= Input::get("sub_dep");
					$object->tashkil_id							= Input::get('tashkil');
					$object->position_dr						= Input::get("position_dr");
					$object->mawqif_employee					= 1;

				}

				$object->free_compitition						= Input::get("free_competition");

				$object->first_date_appointment					= Input::get("first_appointment_date");
				$object->current_position_dr					= Input::get("current_position_dr");
				$object->name_dr 								= Input::get("name_dr");
				$object->father_name_dr 						= Input::get("father_name_dr");
				$object->nationality 							= Input::get("nationality");
				$object->gender 								= Input::get("gender");
				$object->blood_group							= Input::get("blood_group");
				$object->birth_year								= Input::get("birth_year");
				$object->original_province 						= Input::get("original_province");
				$object->district 								= Input::get("district");
				//$object->education_degree						= Input::get("education_degree");
				//$object->education_field						= Input::get("education_field");
				$object->phone									= Input::get("phone");
				$object->email									= Input::get("email");
				$object->last_name 								= Input::get("last_name");
				$object->grand_father_name 						= Input::get("grand_father_name");
				$object->id_no 									= Input::get("id_no");
				$object->id_jild								= Input::get("id_jild");
				$object->id_page								= Input::get("id_page");
				$object->id_sabt								= Input::get("id_sabt");
				$object->village 								= Input::get("village");
				$object->current_province 						= Input::get("current_province");
				$object->current_district 						= Input::get("current_district");
				$object->current_village 						= Input::get("current_village");
				$object->dep_type 								= $user_dep_type;
				//$object->education_place						= Input::get("education_place");
				//$object->graduation_year						= Input::get("graduation_year");
				$object->created_by 							= Auth::user()->id;

				if($object->save())
				{
					$inserted_id = $object->id;
					//add current position to employee_experience
					if(Input::get('employee_type') == 1 || Input::get('employee_type') == 2 || Input::get('employee_type') == 3)
					{//not contracts
						$emp_det = getEmployeeDetails($inserted_id);
						if(Input::get("first_appointment_date") != '')
						{
							$sdate = explode("-", Input::get("first_appointment_date"));
							$sy = $sdate[2];
							$sm = $sdate[1];
							$sd = $sdate[0];
							$sdate = dateToMiladi($sy,$sm,$sd);
						}
						else
						{
							$sdate = null;
						}
						$experience = array(
								'employee_id'	=> $inserted_id,
								'organization'	=> $emp_det->dep_name,
								'position'	=> Input::get("current_position_dr"),
								'bast'	=> Input::get("tashkil_bast"),
								//'rank'	=> Input::get("rank"),
								'date_from'	=> $sdate,
								'type'	=> 1,
								'internal'	=> 1,
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
						hrOperation::insertRecord('employee_experiences',$experience);
					}
					/*
					for($i=1;$i<=3;$i++)
					{
						if(Input::get('experience_position_'.$i)!='')
						{
							$experience = array(
								"employee_id" 	=> $inserted_id,
								"organization" 	=> Input::get('experience_company_'.$i),
								"position" 		=> Input::get('experience_position_'.$i),
								"bast"			=> Input::get('experience_bast_'.$i),
								"leave_reason" 	=> Input::get('experience_leave_'.$i),
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
							hrOperation::insertRecord('employee_experiences',$experience);
						}
					}
					for($i=1;$i<=4;$i++)
					{
						$langs = array(
								"employee_id" 	=> $inserted_id,
								"language" 		=> Input::get('lang_'.$i),
								"writing" 		=> Input::get('writing_'.$i),
								"speaking"		=> Input::get('speaking_'.$i),
								"reading" 		=> Input::get('reading_'.$i),
								"typing"		=> Input::get('typing_'.$i),
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
						hrOperation::insertRecord('employee_languages',$langs);
					}
					for($i=1;$i<=2;$i++)
					{
						if(Input::get('garantee_name_'.$i)!='')
						{
							$garantee = array(
								"employee_id" 	=> $inserted_id,
								"name" 			=> Input::get('garantee_name_'.$i),
								"job" 			=> Input::get('garantee_job_'.$i),
								"organization"	=> Input::get('garantee_company_'.$i),
								"phone" 		=> Input::get('garantee_phone_'.$i),
								"email"			=> Input::get('garantee_email_'.$i),
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
							hrOperation::insertRecord('employee_garantee',$garantee);
						}
					}
					*/
					//$this->uploadDocs($inserted_id);
					//$this->uploadPhoto($inserted_id);
					if(Input::get('tashkil'))
					{
						hrOperation::update_record('tashkilat',array('status'=>1),array('id'=>Input::get('tashkil')));
					}

					return Redirect::route('getRecruitment')->with("success","Record saved successfully");
				}
				else
				{
					return Redirect::route("getRecruitment")->with("fail","An error occured plase try again.");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	//change task start/end date
	public function fireDate()
	{
		if(canAdd('hr_recruitment'))
		{
			$employee_id 	= Input::get('employee_id');
			$number	 		= Input::get('number');
			$sdate 			= Input::get("date");
			$update_report 	= hrOperation::find($employee_id);

			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				return \Redirect::route("getDetailsEmployee",array(Crypt::encrypt($employee_id)))->with("fail","Date must be filled");
			}
			if(Input::hasFile('fire'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'fire_'.$employee_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($employee_id,'fire');
			}
			$update_report->fire_date 		= $sdate;
			$update_report->fire_no 		= $number;

		  if(Input::get("date_hire") != '')
			{//if the hire date is set, it means the emplyee is not fire any more
				$edate = explode("-", Input::get("date_hire"));
				$ey = $edate[2];
				$em = $edate[1];
				$ed = $edate[0];
				$edate = dateToMiladi($ey,$em,$ed);
				$update_report->fire_date_hire 			= $edate;
				//$update_report->first_appointment_date	= $edate;
				$update_report->fired 					= 0;//not fired
				$update_report->mawqif_employee 		= 1;//not fired
			}
			else
			{
				$update_report->fire_date_hire 			= null;
				//$update_report->first_appointment_date	= $edate;
				$update_report->fired 					= 1;//fired
				$update_report->mawqif_employee 		= 6;//fired

				$employee_experience = hrOperation::getEmployeeExperience($employee_id);
				if($employee_experience)
				{
					hrOperation::update_record('employee_experiences',array('date_to'=>$sdate),array('id'=>$employee_experience->id));
					$from_date = $employee_experience->date_from;
				}
				else
				{
					$from_date = hrOperation::getEmployeeDetails($employee_id)->first_date_appointment;
				}

				$dep_id = hrOperation::getEmployeeDetails($employee_id)->department;
				$position = hrOperation::getEmployeeDetails($employee_id)->current_position_dr;

				//save to experience
				$dep_name = hrOperation::get_dep_name($dep_id);
				$experience = array(
						'employee_id'	=> $employee_id,
						'organization'	=> $dep_name->name,
						'position'	=> $position,
						//'bast'	=> $emp_det->bast_name,
						'leave_reason'	=> 'منفک',
						'date_to'	=> $sdate,
						'date_from'	=> $from_date,
						'type'	=> 1,
						'internal'	=> 1,
						"created_at" 	=> date('Y-m-d H:i:s'),
						"created_by" 	=> Auth::user()->id
					);
				hrOperation::insertRecord('employee_experiences',$experience);


				$rfid = hrOperation::getEmployeeDetails($employee_id)->RFID;
				$update_report->fired 			= 1;//fired
				$update_report->att_out_date 	= $sdate;
				$update_report->in_attendance 	= $rfid;
				$update_report->RFID 			= null;
				$update_report->mawqif_employee = 6;
			}
		    if($update_report->save())
		    {
		    	$emp_det =  DB::connection('hr')
								->table('employees')
								->where('id',$employee_id)
								->pluck('tashkil_id');
				if($emp_det!=0)
				{
					hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$emp_det));
				}
				hrOperation::insertLog('employees',1,'fire of given employee',$employee_id);
		    	return \Redirect::route("getDetailsEmployee",array(Crypt::encrypt($employee_id)))->with("success","Record Saved Successfully.");
		    }
		}
		else
		{
			return showWarning();
		}
	}
	public function retireDate()
	{
		if(canAdd('hr_documents'))
		{
			$employee_id 	= Input::get('employee_id');
			$number	 		= Input::get('number');
			$sdate 			= Input::get("date");
			$update_report 	= hrOperation::find($employee_id);

			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				return \Redirect::route("getDetailsEmployee",array(Crypt::encrypt($employee_id)))->with("fail","Date must be filled");
			}
			if(Input::hasFile('retire'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'retire_'.$employee_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($employee_id,'retire');
			}
			$update_report->retire_date 		= $sdate;
			$update_report->retire_no 		= $number;
			$update_report->retired 			= 1;//retired
	    	$rfid = hrOperation::getEmployeeDetails($employee_id)->RFID;
			$update_report->att_out_date 	= $sdate;
			$update_report->in_attendance 	= $rfid;
			$update_report->RFID 			= null;
			$update_report->mawqif_employee = 5;

		    if($update_report->save())
		    {
		    	$emp_det =  DB::connection('hr')
								->table('employees')
								->where('id',$employee_id)
								->pluck('tashkil_id');
				if($emp_det!=0)
				{
					hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$emp_det));
				}
				$employee_experience = hrOperation::getEmployeeExperience($employee_id);
				if($employee_experience)
				{//it has record in employee_experiences, update the record
					$experience_array = array('date_to'=>$sdate,'leave_reason'=>'تقاعد');
					hrOperation::update_record('employee_experiences',$experience_array,array('id'=>$employee_experience->id));
				}
				else
				{//add new record for experience
					//save to experience
					$dep_id = hrOperation::getEmployeeDetails($employee_id)->department;
					$position = hrOperation::getEmployeeDetails($employee_id)->current_position_dr;
					$dep_name = hrOperation::get_dep_name($dep_id);
					$experience = array(
							'employee_id'	=> $employee_id,
							'organization'	=> $dep_name->name,
							'position'	=> $position,
							//'bast'	=> $emp_det->bast_name,
							'leave_reason'	=> 'تقاعد',
							'date_to'	=> $sdate,
							'type'	=> 1,
							'internal'	=> 1,
							"created_at" 	=> date('Y-m-d H:i:s'),
							"created_by" 	=> Auth::user()->id
						);
					hrOperation::insertRecord('employee_experiences',$experience);
				}
				hrOperation::insertLog('employees',1,'retire of given employee',$employee_id);
		    	return \Redirect::route("getRecruitment")->with("success","Record Saved Successfully.");
		    }
		}
		else
		{
			return showWarning();
		}
	}
	public function tanqisDate()
	{
		if(canAdd('hr_recruitment'))
		{
			$employee_id 	= Input::get('employee_id');
			$number	 		= Input::get('number');
			$sdate 			= Input::get("date");
			$mawqif_employee= Input::get("mawqif_employee");
			$update_report 	= hrOperation::find($employee_id);

			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				return \Redirect::route("getDetailsEmployee",array(Crypt::encrypt($employee_id)))->with("fail","Date must be filled");
			}
			if($mawqif_employee=='')
			{
				return \Redirect::route("getDetailsEmployee",array(Crypt::encrypt($employee_id)))->with("fail","Date must be filled");
			}
			if($mawqif_employee==2)
			{
				$type = 'انتظاربامعاش';
			}
			if($mawqif_employee==7)
			{
				$type = 'اضافه بست';
			}
			$update_report->tanqis_date 	= $sdate;
			$update_report->tanqis_no 		= $number;
			//$update_report->retired 			= 1;//retired
	    	$rfid = hrOperation::getEmployeeDetails($employee_id)->RFID;
			$update_report->att_out_date 	= $sdate;
			$update_report->in_attendance 	= $rfid;
			$update_report->RFID 			= null;
			$update_report->mawqif_employee = $mawqif_employee;

		    if($update_report->save())
		    {
		    	$emp_det =  DB::connection('hr')
								->table('employees')
								->where('id',$employee_id)
								->pluck('tashkil_id');
				if($emp_det!=0)
				{
					hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$emp_det));
				}
				$employee_experience = hrOperation::getEmployeeExperience($employee_id);
				if($employee_experience)
				{//it has record in employee_experiences, update the record
					$experience_array = array('date_to'=>$sdate,'leave_reason'=>$type);
					hrOperation::update_record('employee_experiences',$experience_array,array('id'=>$employee_experience->id));
				}
				else
				{//add new record for experience
					//save to experience
					$dep_id = hrOperation::getEmployeeDetails($employee_id)->department;
					$position = hrOperation::getEmployeeDetails($employee_id)->current_position_dr;
					$dep_name = hrOperation::get_dep_name($dep_id);
					$experience = array(
							'employee_id'	=> $employee_id,
							'organization'	=> $dep_name->name,
							'position'	=> $position,
							//'bast'	=> $emp_det->bast_name,
							'leave_reason'	=> $type,
							'date_to'	=> $sdate,
							'type'	=> 1,
							'internal'	=> 1,
							"created_at" 	=> date('Y-m-d H:i:s'),
							"created_by" 	=> Auth::user()->id
						);
					hrOperation::insertRecord('employee_experiences',$experience);
				}
				hrOperation::insertLog('employees',1,'tanqis of given employee',$employee_id);
		    	return \Redirect::route("getRecruitment")->with("success","Record Saved Successfully.");
		    }
		}
		else
		{
			return showWarning();
		}
	}
	public function employeeResign()
	{
		if(canAdd('hr_recruitment'))
		{
			$employee_id 	= Input::get('employee_id');
			$number	 		= Input::get('number');
			$sdate 			= Input::get("date");
			$update_report 	= hrOperation::find($employee_id);

			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				return \Redirect::route("getDetailsEmployee",array(Crypt::encrypt($employee_id)))->with("fail","Date must be filled");
			}
			if(Input::hasFile('resign'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'resign_'.$employee_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($employee_id,'resign');
			}
			$update_report->resign_date 		= $sdate;
			$update_report->resign_no 		= $number;
			$update_report->resigned 			= 1;//resigned
		    $rfid = hrOperation::getEmployeeDet($employee_id)->RFID;
			$update_report->att_out_date 	= $sdate;
			$update_report->in_attendance 	= $rfid;
			$update_report->RFID 			= null;
			$update_report->mawqif_employee = 9;

		    if($update_report->save())
		    {
		    	$emp_det =  DB::connection('hr')
								->table('employees')
								->where('id',$employee_id)
								->pluck('tashkil_id');
				if($emp_det!=0)
				{
					hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$emp_det));
				}
				$employee_experience = hrOperation::getEmployeeExperience($employee_id);
				if($employee_experience)
				{//it has record in employee_experiences, update the record
					$experience_array = array('date_to'=>$sdate,'leave_reason'=>'استعفا');
					hrOperation::update_record('employee_experiences',$experience_array,array('id'=>$employee_experience->id));
				}
				else
				{//add new record for experience
					$dep_id = hrOperation::getEmployeeDetails($employee_id)->department;
					$position = hrOperation::getEmployeeDetails($employee_id)->current_position_dr;
					$dep_name = hrOperation::get_dep_name($dep_id);
					$experience = array(
							'employee_id'	=> $employee_id,
							'organization'	=> $dep_name->name,
							'position'	=> $position,
							//'bast'	=> $emp_det->bast_name,
							'leave_reason'	=> 'استعفا',
							'date_from'	=> $sdate,
							'type'	=> 1,
							'internal'	=> 1,
							"created_at" 	=> date('Y-m-d H:i:s'),
							"created_by" 	=> Auth::user()->id
						);
					hrOperation::insertRecord('employee_experiences',$experience);
				}
				hrOperation::insertLog('employees',1,'resign of given employee',$employee_id);
		    	return \Redirect::route("getDetailsEmployee",array(Crypt::encrypt($employee_id)))->with("success","Record Saved Successfully.");
		    }
		}
		else
		{
			return showWarning();
		}
	}
	public function loadEmployees()
	{
		$type = Input::get('type');
		if($type == 'changed')
		{
			return View::make('hr.recruitment.changed_list');
		}
		elseif($type == 'khedmati')
		{
			return View::make('hr.recruitment.khedmati_list');
		}
		elseif($type == 'fired')
		{
			return View::make('hr.recruitment.fired_list');
		}
		elseif($type == 'resign')
		{
			return View::make('hr.recruitment.resign_list');
		}
		elseif($type == 'retire')
		{
			return View::make('hr.recruitment.retire_list');
		}
		elseif($type == 'extra_bast')
		{//salary wait
			return View::make('hr.recruitment.extra_bast_list');
		}
		elseif($type == 'tanqis')
		{//ezafa bast
			return View::make('hr.recruitment.tanqis_list');
		}
		elseif($type == 'contracts')
		{
			return View::make('hr.recruitment.contracts_list');
		}
		//communication tabs
		elseif($type == 'maktob')
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			$users = Report::getDepUsers();
			$data['users'] = $users;
			return View::make('hr.communication.maktob_list',$data);
		}
		elseif($type == 'estelam')
		{
			return View::make('hr.communication.estelam_list');
		}
		elseif($type == 'security')
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			$users = Report::getDepUsers();
			$data['users'] = $users;
			return View::make('hr.communication.security_list',$data);
		}
		elseif($type == 'protection')
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			$users = Report::getDepUsers();
			$data['users'] = $users;
			return View::make('hr.communication.protection_list',$data);
		}
		elseif($type == 'complain')
		{
			return View::make('hr.communication.complains_list');
		}
		elseif($type == 'comitte')
		{
			return View::make('hr.communication.comitte_list');
		}
		elseif($type == 'cards')
		{
			return View::make('hr.communication.cards');
		}
	}
	public function getFiredEmployeeData()
	{
		if(canView('hr_documents'))
		{
			//get all data
			$object = hrOperation::getFiredData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name',
								'number_tayenat',
								'bast',
								'department',
								'fire_no'
								)
					->addColumn('fire_date', function($option){
						$date = $option->emp_date;
						if($date!='')
						{
							$date = explode('-',$option->emp_date);
							$date = dateToShamsi($date[0],$date[1],$date[2]);
						}

						return $date;

					})
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',Crypt::encrypt($option->id)).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getResignEmployeeData()
	{
		if(canView('hr_documents'))
		{
			//get all data
			$object = hrOperation::getResignData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name',
								'number_tayenat',
								'bast',
								'department',
								'fire_no'
								)
					->addColumn('resign_date', function($option){
						$date = $option->emp_date;
						if($date!='')
						{
							$date = explode('-',$option->emp_date);
							$date = dateToShamsi($date[0],$date[1],$date[2]);
						}

						return $date;

					})
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',Crypt::encrypt($option->id)).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getRetireEmployeeData()
	{
		if(canView('hr_documents'))
		{
			//get all data
			$object = hrOperation::getRetireData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name',
								'number_tayenat',
								'bast',
								'department',
								'retire_no'
								)
					->addColumn('retire_date', function($option){
						$date = $option->emp_date;
						if($date!='')
						{
							$date = explode('-',$option->emp_date);
							$date = dateToShamsi($date[0],$date[1],$date[2]);
						}

						return $date;

					})
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',Crypt::encrypt($option->id)).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getExtraBastEmployeeData()
	{
		if(canView('hr_documents'))
		{
			//get all data
			$object = hrOperation::getExtraBastData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name',
								'number_tayenat',
								'bast',
								'department'
								)
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',Crypt::encrypt($option->id)).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getTanqisEmployeeData()
	{
		if(canView('hr_documents'))
		{
			$object = hrOperation::getTanqistData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name',
								'number_tayenat',
								'bast',
								'department'
								)
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',Crypt::encrypt($option->id)).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getContractsEmployeeData()
	{
		if(canView('hr_documents'))
		{
			$object = hrOperation::getContractsData();
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name',
								'department'
								)
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getContractEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getChangedEmployeeData()
	{
		if(canView('hr_documents'))
		{
			//get all data
			$object = hrOperation::getChangedData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name_dr',
								'number_tayenat',
								'original_province',
								'bast',
								'rank',
								'emp_date'
								)
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',Crypt::encrypt($option->id)).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getServiceEmployeeData()
	{
		if(canView('hr_documents'))
		{
			//get all data
			$object = hrOperation::getServiceData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'number_tayenat',
								'gender',
								'original_province',
								'bast',
								'rank',
								'emp_date'
								)
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',Crypt::encrypt($option->id)).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function load_change_employee()
	{
		//get employee id
		$employee_id = Input::get('id');
		$data['details'] = hrOperation::find($employee_id);
		$data['parentDeps'] = getDepartmentWhereIn();

		return View('hr.recruitment.employee_change_detail',$data);
	}
	public function load_tashkil_det()
	{
		//get employee id
		$id = Input::get('id');
		$data['id'] = $id;
		//get employee details
		$data['details'] = hrOperation::getTashkilDetail($id);
		$data['sud_dep']=getRelatedSubDepartment($data['details']->dep_id);
		$data['parentDeps'] = getMainDepartments();

		return View('hr.tashkil.update_tashkil',$data);
	}
	public function load_promotion_employee()
	{
		//get employee id
		$employee_id = Input::get('id');
		$lastPromotion = hrOperation::getLastPromotion($employee_id);
		$data['last_promotion'] = $lastPromotion;
		$data['promotions'] = hrOperation::getEmployeePromotions($employee_id);

		return View('hr.documents.employee_promotion_details',$data);
	}
	public function load_promotion_update()
	{
		//get employee id
		$id = Input::get('id');
		$data['id'] = $id;
		//get employee details
		$data['details'] = hrOperation::getEmployeePromotionDetail($id);

		return View('hr.documents.update_promotion',$data);
	}
	public function load_makafat_update()
	{
		//get employee id
		$id = Input::get('id');
		$data['id'] = $id;
		//get employee details
		$data['details'] = hrOperation::getEmployeeMakafatDetail($id);

		return View('hr.documents.update_makafat',$data);
	}
	public function load_punish_update()
	{
		//get employee id
		$id = Input::get('id');
		$data['id'] = $id;
		//get employee details
		$data['details'] = hrOperation::getEmployeePunishDetail($id);

		return View('hr.documents.update_punish',$data);
	}
	public function load_service_employee()
	{
		//get employee id
		$employee_id = Input::get('id');

		//get employee details
		$data['details'] = hrOperation::find($employee_id);
		$data['parentDeps'] = getDepartmentWhereIn();

		return View('hr.recruitment.employee_service_detail',$data);
	}
	public function load_change_detail()
	{
		//get employee id
		$employee_id = Input::get('id');

		//get employee details
		$data['details'] = hrOperation::find($employee_id);
		$data['changes'] = hrOperation::getEmployeeChanges($employee_id,'dr');

		return View('hr.recruitment.employee_change_list',$data);
	}
	public function changeEmployeeViaAjax()
	{
		if(canAdd('hr_recruitment'))
		{
			$employee_id = \Crypt::decrypt(Input::get('employee_id'));
			//validate fields
			if(Input::get('type')==1)
			{//external
				$validates = Validator::make(Input::all(), array(
					"employee_type"		=> "required",
					"position_title"	=> "required",
					"type"				=> "required",
					"change_date"		=> "required",
					"ministry"			=> "required"
				));
			}
			else
			{
				$validates = Validator::make(Input::all(), array(
					"employee_type"		=> "required",
					"position_title"	=> "required",
					"type"				=> "required",
					"change_date"		=> "required",
					"general_department"=> "required"
				));
			}
			//check the validation
			if($validates->fails())
			{
				return Redirect::route("getDetailsEmployee",Crypt::encrypt($employee_id))->with("fail","Missing required fields.");
			}
			else
			{
				$sdate = Input::get("change_date");
				$hdate = Input::get("hokm_date");
				if($sdate != '')
				{
					$sdate = explode("-", $sdate);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);
				}
				else
				{
					$sdate = null;
				}
				if($hdate != '')
				{
					$hdate = explode("-", $hdate);
					$hy = $hdate[2];
					$hm = $hdate[1];
					$hd = $hdate[0];
					$hdate = dateToMiladi($hy,$hm,$hd);
				}
				else
				{
					$hdate = null;
				}
				if(Input::get('type') == 0)
				{//internal
					$dep = Input::get('general_department');
					$sub_dep = Input::get('sub_dep');
					$ministry = 0;
				}
				else
				{
					$dep = null;
					$sub_dep = null;
					$ministry = Input::get('ministry');
				}
				$items = array(
						"employee_id" 			=> $employee_id,
						"position_title" 		=> Input::get('position_title'),
						"employee_type" 		=> Input::get('employee_type'),
						"general_department"	=> $dep,
						"department"			=> $sub_dep,
						"ministry_id"			=> $ministry,
						'change_type'			=> Input::get('type'),
						"position_dr"			=> Input::get('position_dr'),
						"changed_date"			=> $sdate,
						"hokm_date"				=> $hdate,
						"hokm_no"				=> Input::get('hokm_no'),
						"created_at" 			=> date('Y-m-d H:i:s'),
						"created_by" 			=> Auth::user()->id
					);
				if(Input::get('employee_type') == 3)
				{
					$items['military_rank']		= Input::get("military_rank");
					$items['military_bast']		= Input::get("military_bast");

				}
				else
				{
					$items['emp_rank']			= Input::get("emp_rank");
					$items['emp_bast']			= Input::get("emp_bast");
				}
				$inserted_id = hrOperation::insertRecord_id('employee_changes',$items);
				$type = 1;//for experience record
				if(Input::get('type') == 1)
				{//if it is external, update the status to changed to out
					$type = 0;//external experience
					$tashkil_id = hrOperation::getEmployeeDetails($employee_id)->tashkil_id;
					$rfid = hrOperation::getEmployeeDetails($employee_id)->RFID;
					hrOperation::update_record('employees',array('changed'=>1,'mawqif_employee'=>8,'att_out_date'=>$sdate,'in_attendance'=>$rfid,'RFID'=>null),array('id'=>$employee_id));
					hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$tashkil_id));
				}
				$employee_experience = hrOperation::getEmployeeExperience($employee_id);//get last experience
				if($employee_experience)
				{
					hrOperation::update_record('employee_experiences',array('date_to'=>$sdate),array('id'=>$employee_experience->id));
				}
				//add record to experience

				$emp_det = getEmployeeChangesDetails($inserted_id,Input::get('type'));

				//dd($emp_det);
				$experience = array(
						'employee_id'	=> $employee_id,
						'organization'	=> $emp_det->dep_name,
						'position'	=> Input::get("position_title"),
						'bast'	=> $emp_det->bast_name,
						//'rank'	=> Input::get("rank"),
						'date_from'	=> $sdate,
						//'date_to'	=> $sdate,
						'type'	=> 1,
						'internal'	=> $type,
						"created_at" 	=> date('Y-m-d H:i:s'),
						"created_by" 	=> Auth::user()->id
					);
				hrOperation::insertRecord('employee_experiences',$experience);
				hrOperation::insertLog('employees',1,'change of given employee',$employee_id);
				return \Redirect::route("getRecruitment")->with("success","Record Saved Successfully.");
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function promoteEmployeeViaAjax()
	{
		if(canEdit('hr_documents'))
		{
			$employee_id = \Crypt::decrypt(Input::get('employee_id'));

			$sdate = Input::get("change_date");

			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"employee_id" 			=> $employee_id,
					"promotion_date"		=> $sdate,
					"qadam"					=> Input::get('qadam'),
					"qadam_year"			=> Input::get('qadam_year'),
					"qadam_month"			=> Input::get('qadam_month'),
					"qadam_day"				=> Input::get('qadam_day'),
					"employee_type"			=> Input::get('employee_type'),
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);
			if(Input::get('employee_type') == 3)
			{
				$items['military_rank']		= Input::get("military_rank");
				$items['military_bast']		= Input::get("military_bast");

			}
			elseif(Input::get('employee_type') == 2)
			{//ajir
				$items['ajir_rank']		= Input::get("ajir_rank");
				$items['ajir_bast']		= Input::get("ajir_bast");

			}
			else
			{
				$items['emp_rank']			= Input::get("emp_rank");
				$items['emp_bast']			= Input::get("emp_bast");
			}
			hrOperation::insertRecord('employee_promotions',$items);
			return \Redirect::route("getDetailsEmployee",Crypt::encrypt($employee_id))->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function promoteEmployeeUpdate()
	{
		if(canEdit('hr_documents'))
		{
			$id = \Crypt::decrypt(Input::get('id'));

			$sdate = Input::get("change_date");

			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"promotion_date"		=> $sdate,
					"employee_type"			=> Input::get('employee_type'),
					"qadam"					=> Input::get('qadam'),
					"qadam_year"			=> Input::get('qadam_year'),
					"qadam_month"			=> Input::get('qadam_month'),
					"qadam_day"				=> Input::get('qadam_day')
				);
			if(Input::get('employee_type') == 3)
			{
				$items['military_rank']		= Input::get("military_rank");
				$items['military_bast']		= Input::get("military_bast");

			}
			elseif(Input::get('employee_type') == 2)
			{//ajir
				$items['ajir_rank']		= Input::get("ajir_rank");
				$items['ajir_bast']		= Input::get("ajir_bast");

			}
			else
			{
				$items['emp_rank']			= Input::get("emp_rank");
				$items['emp_bast']			= Input::get("emp_bast");
			}
			hrOperation::update_record('employee_promotions',$items,array('id'=>$id));
			hrOperation::insertLog('employee_promotions',1,'promotion of given employee',$id);
			return \Redirect::route("getDetailsEmployee",Crypt::encrypt(Input::get('employee_id')))->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function serviceEmployeeViaAjax()
	{
		if(canEdit('hr_documents'))
		{
			$employee_id = \Crypt::decrypt(Input::get('employee_id'));

			$sdate = Input::get("change_date");
			$hdate = Input::get("hokm_date");
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				$sdate = null;
			}
			if($hdate != '')
			{
				$hdate = explode("-", $hdate);
				$hy = $hdate[2];
				$hm = $hdate[1];
				$hd = $hdate[0];
				$hdate = dateToMiladi($hy,$hm,$hd);
			}
			else
			{
				$hdate = null;
			}
			if(Input::get('type') == 0)
			{
				$dep = Input::get('general_department');
				$sub_dep = Input::get('sub_dep');
				$ministry = 0;
			}
			else
			{
				$dep = null;
				$sub_dep = null;
				$ministry = Input::get('ministry');
			}
			$items = array(
					"employee_id" 			=> $employee_id,
					"position_title" 		=> Input::get('position_title'),
					"general_department"	=> $dep,
					"department"			=> $sub_dep,
					"ministry_id"			=> $ministry,
					"service_date"			=> $sdate,
					"hokm_date"				=> $hdate,
					"hokm_no"				=> Input::get('hokm_no'),
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);

			hrOperation::insertRecord('employee_services',$items);
			return \Redirect::route("getDetailsEmployee",Crypt::encrypt($employee_id))->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	//get edit form for employee
	public function getEmployeeUpdate($id=0)
	{
		//check roles
		if(canEdit('hr_recruitment'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachments'] = hrOperation::getDocuments($id);
			$data['parentDeps'] = getDepartmentWhereIn();
			//get province
			$data['provinces'] = getAllProvinces('dr');
			//load view for registring
			return View::make('hr.recruitment.update',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getDocumentsUpdate($id=0)
	{
		//check roles
		if(canEdit('hr_documents'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['experiences'] = hrOperation::getEmployeeExperiences($id);
			$data['trainings'] = hrOperation::getEmployeeTrainings($id);

			//load view for registring
			return View::make('hr.documents.update',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//get edit form for docs
	public function getEmployeeDocs($id=0)
	{
		//check roles
		if(canView('hr_recruitment'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachments'] = hrOperation::getDocuments($id);

			return View::make('hr.recruitment.documents',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeSuggestion($id=0)
	{
		//check roles
		if(canView('hr_recruitment'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachment'] = hrOperation::getDocumentsByType($id,'suggestion');

			return View::make('hr.recruitment.suggestion',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeHire($id=0)
	{
		//check roles
		if(canView('hr_recruitment'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachment'] = hrOperation::getDocumentsByType($id,'hire');

			return View::make('hr.recruitment.hire',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeDocs($emp_id=0)
	{
		//check roles
		if(canAdd('hr_recruitment'))
		{
			if(Input::hasFile('education'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'education_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'education');
			}
			if(Input::hasFile('crime'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'crime_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'crime');
			}
			if(Input::hasFile('health'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'health_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'health');
			}
			if(Input::hasFile('job'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'job_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'job');
			}
			if(Input::hasFile('tazkira'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'tazkira_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'tazkira');
			}
			if(Input::hasFile('other'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'other_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'other');
			}
			hrOperation::delete_record('employee_filing',array('employee_id'=>$emp_id));
			$filing_data=array('rack_no'=>Input::get('rack'),
							   'floor_no'=>Input::get('floor'),
							   'file_no'=>Input::get('file_no'),
							   'employee_id'=>$emp_id
								);
			HrOperation::insertRecord('employee_filing',$filing_data);
			hrOperation::insertLog('attachments',1,'attachment of given employee',$emp_id);
			//$this->uploadPhoto($inserted_id);
			return Redirect::route('getDetailsEmployee',Crypt::encrypt($emp_id))->with("success","Record saved successfully");
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeSuggestion($emp_id=0)
	{
		//check roles
		if(canAdd('hr_recruitment'))
		{
			$object = hrOperation::find($emp_id);
			$sdate = Input::get("suggestion_date");
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			$object->suggestion_no 						= Input::get("suggestion_no");
			$object->suggestion_date 					= $sdate;
			$object->save();
			if(Input::hasFile('suggestion'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'suggestion_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'suggestion');
			}
			//$this->uploadPhoto($inserted_id);
			return Redirect::route('getDetailsEmployee',Crypt::encrypt($emp_id))->with("success","Record saved successfully");
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeHire($emp_id=0)
	{
		//check roles
		if(canAdd('hr_recruitment'))
		{
			$object = hrOperation::find($emp_id);
			$sdate = Input::get("hire_date");
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				$sdate = null;
			}
			$object->hire_no 						= Input::get("hire_no");
			$object->hire_date 					= $sdate;
			$object->save();
			if(Input::hasFile('hire'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'hire_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'hire');
			}
			hrOperation::insertLog('employees',1,'Hire of given employee',$emp_id);
			//$this->uploadPhoto($inserted_id);
			return Redirect::route('getDetailsEmployee',Crypt::encrypt($emp_id))->with("success","Record saved successfully");
		}
		else
		{
			return showWarning();
		}
	}
	public function uploadEmployeeDocs($doc_id=0,$type='')
	{
		// getting all of the post data
		$file = Input::file($type);
		$errors = "";
		$file_data = array();
		// validating each file.
		$rules = array($type => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
		$validator = Validator::make(

		  		[
		            $type => $file,
		            'extension'  => Str::lower($file->getClientOriginalExtension()),
		        ],
		        [
		            $type => 'required|max:100000',
		            'extension'  => 'required|in:jpg,jpeg,bmp,png'
		        ]
		);

		if($validator->passes())
		{
		    // path is root/uploads
		    $destinationPath = 'documents/hr_attachments';
		    $filename = $file->getClientOriginalName();

		    $temp = explode(".", $filename);
		    $extension = end($temp);
		    $lastFileId = getLastDocId();

		    $lastFileId++;

		    $filename = $type.'_'.$doc_id.'.'.$extension;

		    $upload_success = $file->move($destinationPath, $filename);

		    if($upload_success)
		    {
			   	$data = array(
			   					'doc_id'=>$doc_id,
			   					'file_name'=>$filename,
			   					'created_at'=>date('Y-m-d H:i:s'),
			   					'user_id'	=> Auth::user()->id
			   				);
			   if(count($data)>0)
				{
					DB::connection('hr')->table('attachments')->insert($data);
				}
			}
			else
			{
			   $errors .= json('error', 400);
			}
		}
		else
		{
		    // redirect back with errors.
		    return Redirect::back()->withErrors($validator);
		}
	}
	public function getMoreExperience()
	{
		$data['total'] = Input::get('total');
		$data['type'] = Input::get('type');

		return View::make('hr.documents.moreExperiences',$data);
	}
	public function postEmployeeExperiences($employee_id=0,Request $request)
	{
		if(canEdit('hr_documents'))
		{
			$experienceNo = $request['total_files'];

			hrOperation::delete_record('employee_experiences',array('employee_id'=>$employee_id,'type'=>1));

			for($i=1;$i<=$experienceNo;$i++)
			{
				if($request['experience_company_'.$i]!='')
				{
					$sdate = Input::get('date_from_'.$i);
					$edate = Input::get('date_to_'.$i);
					if($sdate != '')
					{
						$sdate = explode("-", $sdate);
						$sy = $sdate[2];
						$sm = $sdate[1];
						$sd = $sdate[0];
						$sdate = dateToMiladi($sy,$sm,$sd);
					}
					else
					{
						$sdate = null;
					}
					if($edate != '')
					{
						$edate = explode("-", $edate);
						$sy = $edate[2];
						$sm = $edate[1];
						$sd = $edate[0];
						$edate = dateToMiladi($sy,$sm,$sd);
					}
					else
					{
						$edate = null;
					}
					$internal=0;
					if(Input::get('dep_type_'.$i))
					{
						$internal = 1;
					}
					$data = array(
						'employee_id' 	=> $employee_id,
						'organization'	=> $request['experience_company_'.$i],
						'position'		=> $request['experience_position_'.$i],
						'bast'			=> $request['experience_bast_'.$i],
						'leave_reason'	=> $request['experience_leave_'.$i],
						'rank'			=> $request['experience_rank_'.$i],
						'date_from'		=> $sdate,
						'date_to'		=> $edate,
						'type'		    => 1,
						'internal'		=> $internal,
						'created_at' 	=> date('Y-m-d H:i:s'),
						'created_by' 	=> Auth::user()->id
					);
					hrOperation::insertRecord('employee_experiences',$data);
				}
			}
			hrOperation::insertLog('employee_experiences',1,'experiences of given employee',$employee_id);
			return Redirect::route('getDetailsEmployee',Crypt::encrypt($employee_id))->with("success","Record saved successfully");
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeTrainings($employee_id=0,Request $request)
	{
		if(canEdit('hr_documents'))
		{
			$trainingNo = $request['trainings_number'];

			hrOperation::delete_record('employee_trainings',array('employee_id'=>$employee_id));

			for($j=1;$j<=$trainingNo;$j++)
			{
				$trainin_validates = Validator::make(Input::all(), array(
					"training_title_".$j		=> "required"
				));
				if($trainin_validates->fails())
				{
					continue;
				}
				else
				{
					$sdate = Input::get('training_date_from_'.$j);
					$edate = Input::get('training_date_to_'.$j);
					if($sdate != '')
					{
						$sdate = explode("-", $sdate);
						$sy = $sdate[2];
						$sm = $sdate[1];
						$sd = $sdate[0];
						$sdate = dateToMiladi($sy,$sm,$sd);
					}
					else
					{
						$sdate = null;
					}
					if($edate != '')
					{
						$edate = explode("-", $edate);
						$sy = $edate[2];
						$sm = $edate[1];
						$sd = $edate[0];
						$edate = dateToMiladi($sy,$sm,$sd);
					}
					else
					{
						$edate = null;
					}
					$training_data = array(
						'employee_id' 	=> $employee_id,
						'title'			=> $request['training_title_'.$j],
						'organization'	=> $request['training_org_'.$j],
						'date_from'		=> $sdate,
						'date_to'		=> $edate,
						'created_at' 	=> date('Y-m-d H:i:s'),
						'created_by' 	=> Auth::user()->id
					);

					hrOperation::insertRecord('employee_trainings',$training_data);
				}
			}
			hrOperation::insertLog('employee_trainings',1,'trainings of given employee',$employee_id);
			return Redirect::route('getDetailsEmployee',Crypt::encrypt($employee_id))->with("success","Record saved successfully");
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeSoldier($employee_id=0,Request $request)
	{
		if(canEdit('hr_documents'))
		{
			if($request['soldier'])
			{
				$mokalafiat= 0;
				$ehtiat= 0;
				$sdate = null;
				$edate = null;
				$sdate_1 = null;
				$edate_1 = null;
				if($request['mokalafiat'])
				{
					$mokalafiat= 1;
					$sdate = Input::get('soldier_date_from');
					$edate = Input::get('soldier_date_to');
					if($sdate != '')
					{
						$sdate = explode("-", $sdate);
						$sy = $sdate[2];
						$sm = $sdate[1];
						$sd = $sdate[0];
						$sdate = dateToMiladi($sy,$sm,$sd);
					}

					if($edate != '')
					{
						$edate = explode("-", $edate);
						$sy = $edate[2];
						$sm = $edate[1];
						$sd = $edate[0];
						$edate = dateToMiladi($sy,$sm,$sd);
					}

				}
				if($request['ehtiat'])
				{
					$ehtiat= 1;
					$sdate_1 = Input::get('soldier_date_from_1');
					$edate_1 = Input::get('soldier_date_to_1');
					if($sdate_1 != '')
					{
						$sdate_1 = explode("-", $sdate_1);
						$sy_1 = $sdate_1[2];
						$sm_1 = $sdate_1[1];
						$sd_1 = $sdate_1[0];
						$sdate_1 = dateToMiladi($sy_1,$sm_1,$sd_1);
					}

					if($edate_1 != '')
					{
						$edate_1 = explode("-", $edate_1);
						$sy_1 = $edate_1[2];
						$sm_1 = $edate_1[1];
						$sd_ = $edate_1[0];
						$edate_1 = dateToMiladi($sy_1,$sm_1,$sd_1);
					}

				}
				$object = hrOperation::find($employee_id);

				$object->soldier = 1;
				$object->soldier_date_from = $sdate;
				$object->soldier_date_to = $edate;
				$object->soldier_date_from_1 = $sdate_1;
				$object->soldier_date_to_1 = $edate_1;
				$object->soldier_type_1 = $mokalafiat;
				$object->soldier_type_2 = $ehtiat;

				$object->save();
			}
			else
			{
				$object = hrOperation::find($employee_id);

				$object->soldier = 0;
				$object->soldier_date_from = null;
				$object->soldier_date_to = null;
				$object->soldier_date_from_1 = null;
				$object->soldier_date_to_1 = null;
				$object->soldier_type_1 = 0;
				$object->soldier_type_2 = 0;

				$object->save();
			}
			hrOperation::insertLog('employees',1,'soldier of given employee',$employee_id);
			return Redirect::route('getDetailsEmployee',Crypt::encrypt($employee_id))->with("success","Record saved successfully");
		}
		else
		{
			return showWarning();
		}
	}
	public function copyToAttendance()
	{
		//dd(attendanceServer::all());
		attendanceServer::delete_record();
		$data_array= array();
		$allEmployees = hrOperation::getAllEmployeesForAttendanceServer();
		$data_array[]= array('badgenumber'	=> 7893646,
						 'defaultdeptid'	=> 1,
						 'name'			=> 'Dr.Abdul Rahim',
						 'card'			=> 7893646);
		$data_array[]= array('badgenumber'	=> 7867436,
						 'defaultdeptid'	=> 1,
						 'name'			=> 'Rafiullah',
						 'card'			=> 7867436);
		$data_array[]= array('badgenumber'	=> 7869024,
						 'defaultdeptid'	=> 1,
						 'name'			=> 'Marzia Raof',
						 'card'			=> 7869024);
		/*
		foreach($allEmployees AS $item)
		{
			$rfid = str_replace(' ','',$item->RFID);
			$rfid = preg_replace('/\s+/', '', $rfid);
			$rfid = (int)$rfid;
			//$new_rfid = str_pad($item->RFID, 9, '0', STR_PAD_LEFT);
			$data= array('badgenumber'	=> $rfid,
						 'defaultdeptid'	=> 1,
						 'name'			=> $item->name_en,
						 'card'			=> $rfid);

			array_push($data_array,$data);
			$last_id = $item->id;
		}
		**/
		attendanceServer::insert($data_array);
		$last_id_in_db = getLastAttendanceId();
		if($last_id_in_db)
		{
			$to = $last_id_in_db->to_id;
		}
		else
		{
			$to = 0;
		}
		$last_array=array('from_id' => $to,'to_id' => $last_id);
		hrOperation::insertRecord('attendance_temp',$last_array);

		return Redirect::route('getRecruitment')->with("success","Records saved to attendance server");
	}
	public function exportToCsv()
	{
		//dd(attendanceServer::all());
		//attendanceServer::delete_record();

		$allEmployees = hrOperation::getAllEmployeesForAttendance();
		$last_id = hrOperation::getLastEmployeeId()->id;

		Excel::create('Att-'.$last_id, function($file) use($allEmployees){

			$file->setTitle('Attendance');

			$file->sheet('Attendance', function($sheet) use($allEmployees){
				$row = 1;
				foreach($allEmployees AS $item)
				{
					$rfid = str_replace(' ','',$item->RFID);
					$rfid = preg_replace('/\s+/', '', $rfid);
					$rfid = (int)$rfid;
					//$new_rfid = str_pad($rfid, 9, '0', STR_PAD_LEFT);

					$sheet->setCellValue('A'.$row.'',$rfid);

					$sheet->setCellValue('B'.$row.'',$rfid);

					$sheet->setCellValue('C'.$row.'',$item->name_en);

					$sheet->setCellValue('D'.$row.'','');
					$sheet->setCellValue('E'.$row.'','');
					$sheet->setCellValue('F'.$row.'','');
					$sheet->setCellValue('G'.$row.'','');
					$sheet->setCellValue('H'.$row.'','');
					$sheet->setCellValue('I'.$row.'','');
					$sheet->setCellValue('J'.$row.'','');
					$sheet->setCellValue('K'.$row.'','');
					$sheet->setCellValue('L'.$row.'',$rfid);
					$row++;
				}
    		});
		})->download('csv');
	}
	public function getDetailsEmployee($id=0)
	{

		$id = \Crypt::decrypt($id);
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$details = hrOperation::getEmployeeDetails($id);
		$year = $details->year;
		$data['year'] = $year;
		if($user_dep_type==0)
		{
			//super admin
		}
		elseif($user_dep_type != $details->dep_type)
		{
			return showWarning();
		}
		if(canView('hr_recruitment') || canView('hr_documents') || canView('hr_capacity') || canView('hr_communication'))
		{
			$data['id'] = $id;
			//getEmployeeUpdate

			$data['row'] = $details;
			$data['attachments'] = hrOperation::getDocuments($id);
			$data['parentDeps'] = getDepartmentWhereIn();

			//get province
			$data['provinces'] = getAllProvinces('dr');
			$data['org_districts'] = getProvinceDistrict($details->original_province);
			$data['cur_districts'] = getProvinceDistrict($details->current_province);
			$data['experiences'] = hrOperation::getEmployeeExperiences($id,0);
			$data['langs'] = hrOperation::getEmployeeLangs($id);
			$data['educations'] = hrOperation::getEmployeeEducations($id);
			//dd($data['educations']);
			if($details->tashkil_id==0)
			{
				$data['tashkils'] = hrOperation::getRelatedBasts($details->department,$details->tashkil_id,$year);
				$data['deps'] = getRelatedSubDepartment($details->general_department);
				$data['dep_id'] = $details->general_department;
				$data['sub_dep_id'] = $details->department;
			}
			else
			{
				$data['tashkils'] = hrOperation::getRelatedBasts($details->sub_dep_id,$details->tashkil_id,$year);
				if(count($data['tashkils'])==0)
				{//the employee does not have tashkilat at current year,then get prevouse year tashkil of the employee
					$data['tashkils'] = hrOperation::getRelatedBasts($details->sub_dep_id,$details->tashkil_id,$year-1);
					$data['year'] = $year-1;
				}
				$data['deps'] = getRelatedSubDepartment($details->dep_id);
				$data['dep_id'] = $details->dep_id;
                $data['sub_dep_id'] = $details->sub_dep_id;

			}
			$data['garantee'] = hrOperation::getEmployeeGarantee($id);
			//load view for registring
			$data['employeeUpdate']=View::make('hr.recruitment.update',$data);
            //all tabs view
			return View::make('hr.recruitment.employee_details',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getContractEmployee($id=0)
	{
		if(canView('hr_recruitment') || canView('hr_documents') || canView('hr_capacity'))
		{

			$the_date = dateToShamsi(date('Y'),date('m'),date('d'));
			$s_date = explode('-',$the_date);
			$year = $s_date[0];
			$data['year'] = $year;

			$data['id'] = $id;
			//getEmployeeUpdate
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachments'] = hrOperation::getDocuments($id);
			$data['parentDeps'] = getDepartmentWhereIn();

			//get province
			$data['provinces'] = getAllProvinces('dr');
			$data['org_districts'] = getProvinceDistrict($details->original_province);
			$data['cur_districts'] = getProvinceDistrict($details->current_province);
			$data['experiences'] = hrOperation::getEmployeeExperiences($id,0);
			$data['langs'] = hrOperation::getEmployeeLangs($id);
			$data['educations'] = hrOperation::getEmployeeEducations($id);
			if($details->tashkil_id==0)
			{
				$data['tashkils'] = hrOperation::getRelatedBasts($details->department,$details->tashkil_id);
				$data['deps'] = getRelatedSubDepartment($details->general_department);
				$data['dep_id'] = $details->general_department;
				$data['sub_dep_id'] = $details->department;
			}
			else
			{
				$data['tashkils'] = hrOperation::getRelatedBasts($details->sub_dep_id,$details->tashkil_id);
				$data['deps'] = getRelatedSubDepartment($details->dep_id);
				$data['dep_id'] = $details->dep_id;
				$data['sub_dep_id'] = $details->sub_dep_id;
			}
			$data['garantee'] = hrOperation::getEmployeeGarantee($id);
			//load view for registring
			$data['employeeUpdate']=View::make('hr.recruitment.update_contract',$data);
			//all tabs view
			return View::make('hr.recruitment.employee_details',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function loadEmployeeDetails()
	{
		if(canView('hr_recruitment') || canView('hr_documents') || canView('hr_capacity'))
		{
			$type = Input::get('type');
            $employee_id = Input::get('id');
            $lang = Session::get('lang'); 
			//get employee details
			$data['details'] = hrOperation::find($employee_id);
			$data['id'] = $employee_id;

			if($type == 'changed')
			{
				$data['changes'] = hrOperation::getEmployeeChanges($employee_id,'dr');

				return View('hr.recruitment.employee_change_list',$data);
			}
			elseif($type == 'edit')
			{
				$data['id'] = $employee_id;
				//getEmployeeUpdate
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['row'] = $details;
				$data['attachments'] = hrOperation::getDocuments($employee_id);
				$data['parentDeps'] = getDepartmentWhereIn();
				//$data['deps'] = getRelatedSubDepartment($details->general_department);
				//get province
				$data['provinces'] = getAllProvinces('dr');
				$data['org_districts'] = getProvinceDistrict($details->original_province);
				$data['cur_districts'] = getProvinceDistrict($details->current_province);
				$data['experiences'] = hrOperation::getEmployeeExperiences($employee_id,0);
				$data['educations'] = hrOperation::getEmployeeEducations($employee_id);
				$data['langs'] = hrOperation::getEmployeeLangs($employee_id);
				//$data['tashkils'] = hrOperation::getRelatedBasts($details->department,$details->tashkil_id);
				$data['garantee'] = hrOperation::getEmployeeGarantee($employee_id);
				if($details->tashkil_id==0)
				{
					$data['tashkils'] = hrOperation::getRelatedBasts($details->department,$details->tashkil_id);
					$data['deps'] = getRelatedSubDepartment($details->general_department);
					$data['dep_id'] = $details->general_department;
					$data['sub_dep_id'] = $details->department;
				}
				else
				{
					$data['tashkils'] = hrOperation::getRelatedBasts($details->sub_dep_id,$details->tashkil_id);
					$data['deps'] = getRelatedSubDepartment($details->dep_id);
					$data['dep_id'] = $details->dep_id;
					$data['sub_dep_id'] = $details->sub_dep_id;
				}
				return View::make('hr.recruitment.update',$data);
			}
			elseif($type == 'salary')
			{
				$data['row'] = hrOperation::getEmployeeSalary($employee_id);
				$data['employee_id'] = $employee_id;
				return View::make('hr.recruitment.employee_salary',$data);
			}
			elseif($type == 'khedmati')
			{
				$data['details'] = hrOperation::getEmployeeServices($employee_id);
				return View::make('hr.recruitment.employee_khedmati_list',$data);
			}
			elseif($type == 'docs')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['filing'] = hrOperation::getEmployeefiling($employee_id);
				$data['row'] = $details;
				$data['attachments'] = hrOperation::getDocuments($employee_id);

				return View::make('hr.recruitment.documents',$data);
			}
			elseif($type == 'suggestion')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['row'] = $details;
				$data['attachment'] = hrOperation::getDocumentsByType($employee_id,'suggestion');

				return View::make('hr.recruitment.suggestion',$data);
			}
			elseif($type == 'hire')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['row'] = $details;
				$data['attachment'] = hrOperation::getDocumentsByType($employee_id,'hire');

				return View::make('hr.recruitment.hire',$data);
			}
			elseif($type == 'fire')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['attachment'] = hrOperation::getDocumentsByType($employee_id,'fire');
				$data['row'] = $details;

				return View::make('hr.recruitment.fire',$data);
			}
			elseif($type == 'retire')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['attachment'] = hrOperation::getDocumentsByType($employee_id,'retire');
				$data['row'] = $details;

				return View::make('hr.recruitment.retire',$data);
			}
			elseif($type == 'resign')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['attachment'] = hrOperation::getDocumentsByType($employee_id,'resign');
				$data['row'] = $details;

				return View::make('hr.recruitment.resign',$data);
			}
			elseif($type == 'tanqis')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				//$data['attachment'] = hrOperation::getDocumentsByType($employee_id,'resign');
				$data['row'] = $details;

				return View::make('hr.recruitment.tanqis',$data);
			}
			//documents sections
			elseif($type == 'experience')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['row'] = $details;
				$data['experiences'] = hrOperation::getEmployeeExperiences($employee_id);

				//load view for registring
				return View::make('hr.documents.experience',$data);
			}
			elseif($type == 'training')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['row'] = $details;

				$data['trainings'] = hrOperation::getEmployeeTrainings($employee_id);
				$data['internal_trainings'] = hrOperation::getEmployeeAOPTrainings($employee_id);

				//load view for registring
				return View::make('hr.documents.trainings',$data);
			}
			elseif($type == 'soldier')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['row'] = $details;

				//load view for registring
				return View::make('hr.documents.soldier',$data);
			}
			elseif($type == 'promotion')
			{
				$data['last_promotion'] = hrOperation::getLastPromotion($employee_id);
				$data['promotions'] = hrOperation::getEmployeePromotions($employee_id);

				return View('hr.documents.employee_promotion_details',$data);
			}
			elseif($type == 'evaluation')
			{
				$data['evaluations'] = hrOperation::getEmployeeEvaluations($employee_id);

				return View('hr.documents.evaluations',$data);
			}
			elseif($type == 'punishment')
			{
				$data['punish'] = hrOperation::getEmployeePunishments($employee_id);

				return View('hr.documents.punishments',$data);
			}
			elseif($type == 'makafat')
			{
				$data['makafat'] = hrOperation::getEmployeeMakafat($employee_id);

				return View('hr.documents.makafat',$data);
			}
			elseif($type == 'cv')
			{
                $data['details'] = hrOperation::getEmployeeDetails($employee_id);
				$data['makafat'] = hrOperation::getEmployeeMakafat($employee_id);
				$data['punish'] = hrOperation::getEmployeePunishments($employee_id);
				$data['experiences'] = hrOperation::getEmployeeExperiences($employee_id);
				$data['promotions'] = hrOperation::getEmployeePromotions($employee_id);
				$data['trainings'] = hrOperation::getEmployeeTrainings($employee_id);
				$data['internal_trainings'] = hrOperation::getEmployeeAOPTrainings($employee_id);
				$data['educations'] = hrOperation::getEmployeeEducations($employee_id);
                $data['changes']    = hrOperation::getEmployeeLastChange($employee_id);
                $data['lang']       = $lang; 
				//dd($data['changes']);
				return View('hr.documents.cv',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function print_cv($employee_id=0,$pic_size='old')
	{
		if(canView('hr_documents'))
		{
			$data['id'] = $employee_id;
			$data['details'] = hrOperation::getEmployeeDetails($employee_id);//dd($data['details']);
			$data['makafat'] = hrOperation::getEmployeeMakafat($employee_id);
			$data['punish'] = hrOperation::getEmployeePunishments($employee_id);
			$data['experiences'] = hrOperation::getEmployeeExperiences($employee_id);
			$data['promotions'] = hrOperation::getEmployeePromotions($employee_id);
			$data['trainings'] = hrOperation::getEmployeeTrainings($employee_id);
			$data['internal_trainings'] = hrOperation::getEmployeeAOPTrainings($employee_id);
			$data['educations'] = hrOperation::getEmployeeEducations($employee_id);
            $data['changes'] = hrOperation::getEmployeeLastChange($employee_id);
            $data['pic_size'] = $pic_size;
			return View('hr.documents.print_cv',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function evaluateEmployeeViaAjax()
	{
		if(canEdit('hr_documents'))
		{
			$employee_id = \Crypt::decrypt(Input::get('employee_id'));

			$sdate = Input::get("date");

			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"employee_id" 			=> $employee_id,
					"bast"					=> Input::get('emp_bast'),
					"qadam"					=> Input::get('qadam'),
					"score"					=> Input::get('score'),
					"date"					=> $sdate,
					"result"				=> Input::get('result'),
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);

			hrOperation::insertRecord('employee_evaluations',$items);
			return \Redirect::route("getDetailsEmployee",Crypt::encrypt($employee_id))->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function punishEmployeeViaAjax()
	{
		if(canEdit('hr_documents'))
		{
			$employee_id = \Crypt::decrypt(Input::get('employee_id'));

			$sdate = Input::get("date");

			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"employee_id" 			=> $employee_id,
					"date"					=> $sdate,
					"reason"				=> Input::get('reason'),
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);

			hrOperation::insertRecord('employee_punishments',$items);
			return \Redirect::route("getDetailsEmployee",Crypt::encrypt($employee_id))->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function punishEmployeeUpdate()
	{
		if(canEdit('hr_documents'))
		{
			$id = \Crypt::decrypt(Input::get('id'));

			$sdate = Input::get("date");

			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"date"					=> $sdate,
					"reason"				=> Input::get('reason')
				);

			hrOperation::update_record('employee_punishments',$items,array('id'=>$id));
			hrOperation::insertLog('employee_punishments',1,'punishment of given employee',$id);
			return \Redirect::route("getDetailsEmployee",Crypt::encrypt(Input::get('employee_id')))->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function makafatEmployeeViaAjax()
	{
		if(canEdit('hr_documents'))
		{
			$employee_id = \Crypt::decrypt(Input::get('employee_id'));

			$sdate = Input::get("date");

			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"employee_id" 			=> $employee_id,
					"date"					=> $sdate,
					"result"				=> Input::get('result'),
					"type"					=> Input::get('type'),
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);

			hrOperation::insertRecord('employee_makafats',$items);
			return \Redirect::route("getDetailsEmployee",Crypt::encrypt($employee_id))->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function makafatEmployeeUpdate()
	{
		if(canEdit('hr_documents'))
		{
			$id = \Crypt::decrypt(Input::get('id'));

			$sdate = Input::get("date");

			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"date"					=> $sdate,
					"result"				=> Input::get('result'),
					"type"					=> Input::get('type')
				);

			hrOperation::update_record('employee_makafats',$items,array('id'=>$id));
			hrOperation::insertLog('employee_makafats',1,'makafat of given employee',$id);
			return \Redirect::route("getDetailsEmployee",Crypt::encrypt(Input::get('employee_id')))->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	//communication
	public function getEmployeeCommunication()
	{
		//check roles
		if(canView('hr_communication'))
		{
			//check roles
			return View::make('hr.communication.list');
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeComData()
	{
		//get all data
		$object = hrOperation::getComData();//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name',
							'current_position_dr',
							'department'
							)
				->addColumn('operations', function($option){
					$options = '';
					if(canView('hr_communication'))
					{
						$options .= '
								<a href="'.route('getDetailsEmployee',Crypt::encrypt($option->id)).'" class="table-link" style="text-decoration:none;" title="نمایش">
									<i class="icon fa-eye" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
					}
					if(canAdd('hr_communication'))
					{
						$options .= '

								<a href="#" onclick="$('.'\'#employee_id\''.').val('.$option->id.');$('.'\'#employee\''.').val(\''.$option->name.'\');" class="table-link" style="text-decoration:none;" title="استعلام" data-target="#estelam_modal" data-toggle="modal">
									<i class="icon fa-refresh" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
								<a href="#" onclick="$('.'\'#employee_card\''.').val('.$option->id.');$('.'\'#employee\''.').val(\''.$option->name.'\');" class="table-link" style="text-decoration:none;" title="کارت" data-target="#card_modal" data-toggle="modal">
									<i class="icon fa-plus" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
					}

					return $options;
				})
				->make();
		return $result;
	}
	public function getEmployeeComCardData()
	{
		//get all data
		$object = hrOperation::getComCardData();//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'current_position_dr',
							'department',
							'card_no',
							'card_color',
							'issue_date'
							)
				->addColumn('operations', function($option){
					$options = '';
					$options .= '
								<a href="javascript:void()" class="table-link" style="text-decoration:none;" title="نمایش">
									<i class="icon fa-eye" aria-hidden="true" style="font-size: 16px;"></i>
								</a>

								';


					return $options;
				})
				->make();
		return $result;
	}
	public function getComEmplyeeMaktobs()
	{
		//get all data
		$object = hrOperation::getComMaktobData();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'number',
							'date',
							'source'
							)
				->addColumn('operations', function($option){
					return '
							<a href="javascript:void()" onclick="" class="table-link" style="text-decoration:none;" title="Edit" data-target="#maktob_modal"" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							';
				})
				->make();
	}
	public function postComMaktob()
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$sdate = Input::get("date");

		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);
		}
		$items = array(
				"number" 				=> Input::get('number'),
				"date"					=> $sdate,
				"general_dep"			=> Input::get('general_department'),
				"sub_dep"				=> Input::get('sub_dep'),
				"dep_type"				=> $user_dep_type,
				"created_at" 			=> date('Y-m-d H:i:s'),
				"created_by" 			=> Auth::user()->id
			);

		$id = hrOperation::insertRecord_id('com_maktobs',$items);
		$employees = Input::get('employees');
		for($i=0;$i<count($employees);$i++)
		{
			$emps = array(
						'employee_id' 	=> $employees[$i],
						'maktob_id' 	=> $id,
						"created_at" 	=> date('Y-m-d H:i:s'),
						"created_by" 	=> Auth::user()->id
				);
			hrOperation::insertRecord('com_maktob_employees',$emps);
		}
		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function getComEmplyeeEstelams()
	{
		//get all data
		$object = hrOperation::getComEstelamData();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'number',
							'date',
							'name'
							)
				->addColumn('operations', function($option){
					return '
							<a href="javascript:void()" onclick="" class="table-link" style="text-decoration:none;" title="Edit" data-target="#change_date_modal"" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							';
				})
				->make();
	}
	public function postComEstelam()
	{
		$sdate = Input::get("date");

		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);
		}
		$items = array(
				"number" 				=> Input::get('number'),
				"date"					=> $sdate,
				"employee_id"			=> Input::get('employee_id'),
				"created_at" 			=> date('Y-m-d H:i:s'),
				"created_by" 			=> Auth::user()->id
			);

		hrOperation::insertRecord('com_estelams',$items);
		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function getComEmplyeeSecurity()
	{
		//get all data
		$object = hrOperation::getComSecurityData(0);//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'number',
							'date',
							'file_name'
							)
				->addColumn('operations', function($option){
					return '
							<a href="javascript:void()" onclick="" class="table-link" style="text-decoration:none;" title="Edit" data-target="#security_modal"" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							<a href="javascript:void()" onclick="$('.'\'#security_id\''.').val('.$option->id.');" class="table-link" style="text-decoration:none;" title="موافقه" data-target="#accept_modal"" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
				})
				->make();
	}
	public function postComSecurity($type=0)
	{
		$sdate = Input::get("date");

		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);
		}

		$items = array(
				"number" 				=> Input::get('number'),
				"date"					=> $sdate,
				//"file_name"				=> Input::get('employee_id'),
				"type"					=> $type,
				"created_at" 			=> date('Y-m-d H:i:s'),
				"created_by" 			=> Auth::user()->id
			);

		$id = hrOperation::insertRecord_id('com_securities',$items);

		if(Input::hasFile('scan'))
		{
			// getting all of the post data
			$file = Input::file('scan');
			$errors = "";
			$file_data = array();
			// validating each file.
			$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			$validator = Validator::make(

			  		[
			            'scan' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'scan' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			);

			if($validator->passes())
			{
			    // path is root/uploads
			    $destinationPath = 'documents/hr_attachments';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    if($type==2)
			    {
			    	$filename = 'com_protection_'.$id.'.'.$extension;
			    }
			    else
			    {
			    	$filename = 'com_security_'.$id.'.'.$extension;
				}
			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success)
			    {
					DB::connection('hr')->table('com_securities')->where('id',$id)->update(array('file_name'=>$filename));
				}
				else
				{
				   $errors .= json('error', 400);
				}
			}
			else
			{
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			}
		}
		$employees = Input::get('employees');
		for($i=0;$i<count($employees);$i++)
		{
			$emps = array(
						'employee_id' 	=> $employees[$i],
						'security_id' 	=> $id,
						"created_at" 	=> date('Y-m-d H:i:s'),
						"created_by" 	=> Auth::user()->id
				);
			hrOperation::insertRecord('com_security_employees',$emps);
		}
		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function postComEmployeeCard()
	{
		$sdate = Input::get("issue_date");
		$edate = Input::get("expiry_date");
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);
		}
		if($edate != '')
		{
			$edate = explode("-", $edate);
			$ey = $edate[2];
			$em = $edate[1];
			$ed = $edate[0];
			$edate = dateToMiladi($ey,$em,$ed);
		}
		$items = array(
				"card_no" 				=> Input::get('card_no'),
				"employee_id"			=> Input::get('employee_card'),
				"issue_date"			=> $sdate,
				"expiry_date"			=> $edate,
				//"file_name"				=> Input::get('employee_id'),
				"card_color"			=> Input::get('card_color'),
				"created_at" 			=> date('Y-m-d H:i:s'),
				"created_by" 			=> Auth::user()->id
			);

		$id = hrOperation::insertRecord_id('employee_cards',$items);

		if(Input::hasFile('scan'))
		{
			// getting all of the post data
			$file = Input::file('scan');
			$errors = "";
			$file_data = array();
			// validating each file.
			$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			$validator = Validator::make(

			  		[
			            'scan' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'scan' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			);

			if($validator->passes())
			{
			    // path is root/uploads
			    $destinationPath = 'documents/hr_attachments';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    $filename = 'com_card_'.$id.'.'.$extension;
			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success)
			    {
					DB::connection('hr')->table('employee_cards')->where('id',$id)->update(array('file_name'=>$filename));
				}
				else
				{
				   $errors .= json('error', 400);
				}
			}
			else
			{
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			}
		}

		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function postComSecurityAccept()
	{
		$sdate = Input::get("date");

		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);
		}

		$items = array(
				"number" 				=> Input::get('number'),
				"date"					=> $sdate,
				//"file_name"				=> Input::get('employee_id'),
				"type"					=> 1,//accept
				"created_at" 			=> date('Y-m-d H:i:s'),
				"created_by" 			=> Auth::user()->id
			);

		$id = hrOperation::insertRecord_id('com_securities',$items);

		if(Input::hasFile('scan'))
		{
			// getting all of the post data
			$file = Input::file('scan');
			$errors = "";
			$file_data = array();
			// validating each file.
			$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			$validator = Validator::make(

			  		[
			            'scan' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'scan' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			);

			if($validator->passes())
			{
			    // path is root/uploads
			    $destinationPath = 'documents/hr_attachments';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);

			    $filename = 'com_securityAccept_'.$id.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success)
			    {
					DB::connection('hr')->table('com_securities')->where('id',$id)->update(array('file_name'=>$filename));
				}
				else
				{
				   $errors .= json('error', 400);
				}
			}
			else
			{
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			}
		}

		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function getComEmplyeeProtection()
	{
		//get all data
		$object = hrOperation::getComSecurityData(2);//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'number',
							'date',
							'file_name'
							)
				->addColumn('operations', function($option){
					return '
							<a href="javascript:void()" onclick="" class="table-link" style="text-decoration:none;" title="Edit" data-target="#security_modal"" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							';
				})
				->make();
	}
	public function getComComplaints()
	{
		//get all data
		$object = hrOperation::getComComplaintsData();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'reg_date',
							'desc'
							)
				->addColumn('operations', function($option){
					return '
							<a href="javascript:void()" onclick="$('.'\'#complain_id\''.').val('.$option->id.');" class="table-link" style="text-decoration:none;" title="ارجاع به کمیته" data-target="#edit_complain_modal" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							';
				})
				->make();
	}
	public function getComComitte()
	{
		//get all data
		$object = hrOperation::getComComitteData();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'number',
							'date',
							'desc'
							)
				->addColumn('operations', function($option){
					return '
							<a href="javascript:void()" onclick="$('.'\'#comitte_id\''.').val('.$option->id.');" class="table-link" style="text-decoration:none;" title="فیصله کمیته" data-target="#comitte_modal" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							';
				})
				// ->orderColumns('id','id')
				->make();
	}
	public function postComComplain()
	{
		$sdate = Input::get("date");
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);
		}

		$items = array(
				"desc" 				=> Input::get('desc'),
				"reg_date"					=> $sdate,
				"dep_type"					=> $user_dep_type,
				"created_at" 			=> date('Y-m-d H:i:s'),
				"created_by" 			=> Auth::user()->id
			);

		hrOperation::insertRecord('com_complaints',$items);

		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function postComComitte()
	{
		$sdate = Input::get("date");

		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);
		}

		$items = array(
				"number" 		=> Input::get('number'),
				"date"			=> $sdate,
				"complain_id"	=> Input::get('complain_id'),
				"created_at" 	=> date('Y-m-d H:i:s'),
				"created_by" 	=> Auth::user()->id
			);

		hrOperation::insertRecord('com_comitteis',$items);

		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function postComComitteResult()
	{
		$comitte_id = Input::get('comitte_id');
		$items = array(
				"result" 		=> Input::get('result')
			);

		hrOperation::update_record('com_comitteis',$items,array('id'=>$comitte_id));

		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function getAttendance(Request $request)
	{
		//$ip = $request->ip();
		//$log = array('ip'=>$ip,'time'=>date('Y-m-d H:i:s'),'user_id'=>Auth::user()->id,'name'=>Auth::user()->first_name.' '.Auth::user()->last_name);
		//hrOperation::insertRecord('attendance_log',$log);
		//check roles
		//if(canAttHoliday() || canCheckImages() || canAttTodayAtt())
		if(canView('hr_attendance'))
		{
			//check if the user is director or deputy
            $is_manager = is_manager_att(Auth::user()->id);
			if($is_manager->position_id==2)
            {//the user is director
				$data['parentDeps'] = getMainDepartments();
				$data['details'] = $is_manager;
				$data['sub_dep']=getRelatedSubDepartment($is_manager->gen_dep);
				return View::make('hr.attendance.directorate_attendance',$data);
			}
			elseif($is_manager->position_id==3)
			{//the user is deputy
				$data['parentDeps'] = getMainDepartments();
				$data['details'] = $is_manager;
				$data['sub_dep']=getRelatedSubDepartment($is_manager->sub_dep);
				return View::make('hr.attendance.directorate_attendance',$data);
			}
			else
			{
				$data['parentDeps'] = getMainDepartments();
				return View::make('hr.attendance.list',$data);
			}
		}
		elseif(canCheckAtt())
		{//normal users
			//dd('Oops! something went wrong!!!');
			$the_date = dateToShamsi(date('Y'),date('m'),date('d'));
			$s_date = explode('-',$the_date);
			$year = $s_date[0];
			$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			if($user_dep_type==1)
			{
				if($s_date[2]>15)
				{
					if($s_date[1]<12)
					{
						$month= $s_date[1]+1;
					}
					else
					{
						$year= $s_date[0]+1;
						$month=1;
					}
				}
				else
				{
					$month= $s_date[1];
				}
			}
			else
			{
				if($s_date[2]>10)
				{
					if($s_date[1]<12)
					{
						$month= $s_date[1]+1;
					}
					else
					{
						$year= $s_date[0]+1;
						$month=1;
					}
				}
				else
				{
					$month= $s_date[1];
				}
			}
			if($month<10)
			{
				$month = '0'.$month;
			}
			$details = hrOperation::getEmployeeByUserId(Auth::user()->id);

			$rfid = $details->RFID;
			//dd($year.$month);
			$data['enc_rfid'] = Crypt::encrypt($rfid);
			if($rfid>0)
			{
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
				if($details->email==null)
				{
					return View::make('hr.attendance.no_email',$data);
				}
				$rfid_lost = lost_rfid($data['photo']->id);
				if($rfid_lost)
				{
					$rfids = array($rfid);
					$data['rfids'] = $rfid_lost;
					foreach($rfid_lost as $new_rfid)
					{
						$rfids[] =$new_rfid->rfid;
					}
					//dd($rfids);
					$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
				}
				else
				{
					$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
				}
				//$absentd = hrOperation::getEmployeeAbsentDays($rfid,$year,$month);
				//$data['absent'] = count($absentd);
				$data['rfid'] = $rfid;
				$data['emp_id'] = $details->id;
				$data['year'] = $year;
				$data['month'] = $month;
				return View::make('hr.attendance.check_att',$data);
			}
			else
			{
				$data['photo'] = $details;
				return View::make('hr.attendance.no_rfid',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeAttendanceCheck_itself($rfid=0,$year=0,$month=0)
	{
		$rfid = Crypt::decrypt($rfid);
		if($rfid>0)
		{
			$emp_details = hrOperation::getEmployeeByUserId(Auth::user()->id);
			if($emp_details->RFID!=$rfid)
			{
				return showWarning();
			}
			$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
			$rfid_lost = lost_rfid($data['photo']->id);
			if($rfid_lost)
			{
				$rfids = array($rfid);
				$data['rfids'] = $rfid_lost;
				foreach($rfid_lost as $new_rfid)
				{
					$rfids[] =$new_rfid->rfid;
				}
				//dd($rfids);
				$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
			}
			else
			{
				$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
			}
			$absentd = hrOperation::getEmployeeAbsentDays($rfid,$year,$month);
			$data['absent'] = count($absentd);
			$data['enc_rfid'] = Crypt::encrypt($rfid);
			$data['rfid'] = $rfid;
			$data['emp_id'] = $emp_details->id;
			$data['year'] = $year;
			$data['month'] = $month;

			return View::make('hr.attendance.check_att',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function leavesManager()
	{
		//check roles
		if(canAttHoliday())
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			return View::make('hr.attendance.leavesList',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function holidaysManager()
	{
		//check roles
		if(canAttHoliday())
		{
			return View::make('hr.attendance.holidays');
		}
		else
		{
			return showWarning();
		}
	}
	public function notifyManager()
	{
		$data['records'] = hrOperation::getAllHRNotifications();
		return View::make('hr.attendance.notification_board',$data);
	}
	public function editNotifyModal()
	{
		$data['id'] = Input::get('id');

		$data['details'] = hrOperation::getNotificationBoard(Input::get('id'));

		return View::make('hr.attendance.edit_notification_board',$data);
	}
	public function saveNewNotify()
	{
		$data = array(
				'content'	=>Input::get('content'),
				'dep_type'	=>getUserDepType(Auth::user()->id)->dep_type,
				"created_at" => date('Y-m-d H:i:s'),
				"created_by" => Auth::user()->id
			);
		$id = hrOperation::insertRecord_id('hr_notification_board',$data);
		hrOperation::insertLog('hr_notification_board',1,'added new notification',$id);

		return Redirect::route('notifyManager');
	}
	public function postNotifyEdit()
	{
		$data = array(
				'content'	=>Input::get('content')
			);
		hrOperation::update_record('hr_notification_board',$data,array('id'=>Input::get('id')));
		hrOperation::insertLog('hr_notification_board',0,'update notification',Input::get('id'));

		return Input::get('content');
	}
	public function publishNotify()
	{
		$id = Input::get('id');
		$data = array(
				'status'	=>Input::get('type')
			);
		hrOperation::update_record('hr_notification_board',$data,array('id'=>Input::get('id')));
		if(Input::get('type')==0)
		{
			$result = '
					<a href="javascript:void()" onclick="load_position_edit('.$id.');" class="table-link" style="text-decoration:none;" data-target="#edit_modal" data-toggle="modal">
                                        <i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
                    </a> |
                    <a href="javascript:void()" onclick="publish_notificationn('.$id.',1);" class="table-link" style="text-decoration:none;">
                        <i class="icon fa-bullhorn" aria-hidden="true" style="font-size: 16px;"></i>
                    </a>
					';
			hrOperation::insertLog('hr_notification_board',0,'publish notification',Input::get('id'));
		}
		else
		{
			$result = '
					<a href="javascript:void()" onclick="load_position_edit('.$id.');" class="table-link" style="text-decoration:none;" data-target="#edit_modal" data-toggle="modal">
                                        <i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
                    </a> |
                    <a href="javascript:void()" onclick="publish_notificationn('.$id.',0);" class="table-link" style="text-decoration:none;">
                        <i class="icon fa-ban" aria-hidden="true" style="font-size: 16px;"></i>
                    </a>
					';
			hrOperation::insertLog('hr_notification_board',0,'unPublish notification',Input::get('id'));
		}
		return $result;
	}
	public function putImagesToHr()
	{
		$ip = '10.10.0.201';
		$exec = exec( "ping -c 2 -s 64 -t 64 ".$ip, $output, $return );
		if($return)
		{
			dd('Images server is not responding... please try again later.!.');
		}
		//return View::make('hr.attendance.put_images');
		//check roles
		if(Auth::user()->id == 113 || Auth::user()->id == 174 || Auth::user()->id == 201 || Auth::user()->id == 241 || Auth::user()->id == 2038)
		{//aop

			$errors='';
			$devices = array(

					    // array('10.10.7.212','خدمات'),
						array('10.10.7.35','اداره امور طرف چپ'),
						array('10.20.3.195','تعمیراداری طرف راست'),
						array('10.10.7.34','اداره امور'),
						array('10.10.2.22','دفتر الماس'),
						//array('10.10.2.87','اداره امور منزل چهار'),
						//array('10.10.9.147','وزارت معدن'),
						//array('10.10.9.229','کارتوگرافی طرف راست'),
						//array('10.10.9.150','کارتوگرافی طرف چپ'),
						array('10.10.7.33','اداره امور طرف راست'),

						array('10.10.8.218','Attendance Station No 1'),
						array('10.10.8.212','Attendance Station No 2'),
						array('10.10.8.208','Attendance Station No 3'),
						array('10.10.8.207','Attendance Station No 4'),
						array('10.10.8.202','Attendance Station No 5'),
						array('10.10.8.182','Attendance Station No 6'),
						array('10.10.8.198','Attendance Station No 7'),
                        array('10.10.8.237','Attendance Station No 8'),
                        array('10.134.45.176','دفتر سخنگوی'),

					);
			for($i=0;$i<count($devices);$i++)
			{
				$ip = $devices[$i][0];
				$exec = exec( "ping -c 2 -s 64 -t 64 ".$ip, $output, $return );
				if($return)
				{
					$errors.='IP Address: '.$ip.',Device Location: '.$devices[$i][1].'<br/>';
				}
			}
			if($errors!='')
			{//atleast one of the devices is not connected
				echo 'ماشین های ذیل به سیستم وصل نمیباشند:';
				echo '<br/>';
				dd($errors);
			}
			else
			{
				return View::make('hr.attendance.put_images');
			}
		}
		elseif(Auth::user()->id == 715 || Auth::user()->id == 1286 || Auth::user()->id == 450)
		{//ocs
			//return View::make('hr.attendance.put_images');
			$errors='';
			$devices = array(
						array('10.134.45.82','تعمیر سنگی اول'),
						array('10.134.44.15','اجیران طعام خانه'),
						array('10.134.45.141','تعمیر سنگی'),
						array('10.20.2.82','تعمیر اداری'),
						array('10.20.3.203','ریاست امور حقوقی'),
						//array('10.20.3.207','ریاست نظارت و ارزیابی'),
						//array('10.20.3.213','ریاست تفتیش و بررسی'),
						//array('10.20.3.211','ریاست تعقیب اوامر'),
						array('10.20.3.216','سکرتریت دولتداری باز'),
						//array('10.20.2.121','ریاست مطبوعات'),
						array('10.10.8.82','اجیران معدن'),
						array('10.134.45.65','دفتر رحیمی صاحب'),
						array('10.10.3.44','اداره امور ریاست دفتر'),
						array('10.20.2.121','اجیران، تعمیر اداری'),
					);
			for($i=0;$i<count($devices);$i++)
			{
				$ip = $devices[$i][0];
				$exec = exec( "ping -c 2 -s 64 -t 64 ".$ip, $output, $return );
				if($return)
				{
					$errors.='IP Address: '.$ip.',Device Location: '.$devices[$i][1].'<br/>';
				}
			}
			if($errors!='')
			{//atleast one of the devices is not connected
				echo 'ماشین های ذیل به سیستم وصل نمیباشند:';
				echo '<br/>';
				dd($errors);
			}
			else
			{
				return View::make('hr.attendance.put_images');
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getRelatedEmployees()
	{
		if(canView('hr_attendance'))
		{
			$data['year'] = Input::get('year');//shamsi year
			$data['month'] = Input::get('month');//shamsi month
			if(Input::get('general_department')!=null)
			{
				$data['dep'] = Input::get('general_department');
			}
			else
			{
				$data['dep']=0;
			}
			if(Input::get('sub_dep')!=null)
			{
				$data['dep_id'] = Input::get('sub_dep');
			}
			else
			{
				$data['dep_id']=0;
			}

			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_dep']=getRelatedSubDepartment(Input::get('general_department'));
			//$data['employees'] = hrOperation::getRelatedEmployees(Input::get('dep_id'));
			return View::make('hr.attendance.employeeAttList',$data);
			//return View::make('hr.attendance.relatedEmployeeList',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getRelatedEmployees_get($dep_id=0,$sub_dep=0,$year,$month)
	{
		if(canView('hr_attendance'))
		{
			$data['year'] = $year;//shamsi year
			$data['month'] = $month;//shamsi month
			if($dep_id!=0)
			{
				$data['dep'] = $dep_id;
			}
			else
			{
				$data['dep']=0;
			}
			if($sub_dep!=0)
			{
				$data['dep_id'] = $sub_dep;
			}
			else
			{
				$data['dep_id']=0;
			}

			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_dep']=getRelatedSubDepartment($dep_id);
			//$data['rows'] = hrOperation::getRelatedEmployeesAjax($data['dep'],$data['dep_id'],10);
			//$data['employees'] = hrOperation::getRelatedEmployees(Input::get('dep_id'));
			return View::make('hr.attendance.employeeAttList',$data);
			//return View::make('hr.attendance.relatedEmployeeList',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getAttendanceAjax()
	{
		if(canView('hr_attendance'))
		{
			if(Input::get('date_type')==0)
			{
				$data['year'] = Input::get('year');//shamsi year
				$data['month'] = Input::get('month');//shamsi month
			}
			else
			{
				$data['year'] = Input::get('from_date');
				$data['month'] = Input::get('to_date');
			}
			$data['date_type']=Input::get('date_type');
			if(Input::get('general_department')!=null)
			{
				$data['dep'] = Input::get('general_department');
			}
			else
			{
				$data['dep']=0;
			}
			if(Input::get('sub_dep')!=null)
			{
				$data['dep_id'] = Input::get('sub_dep');
			}
			else
			{
				$data['dep_id']=0;
			}
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_dep']=getRelatedSubDepartment(Input::get('general_department'));
			$data['check'] = Input::get('check');
			$per_page=10;

			$data['rows'] = hrOperation::getRelatedEmployeesAjax($data['dep'],$data['dep_id'],$per_page,Input::get('check'));
            //dd($data['rows']);
			if(Input::get('check')==0)
			{
				return View::make('hr.attendance.attendance_ajax',$data);
			}
			else
			{//only for images checking, no need to calculate the absent and present days
				return View::make('hr.attendance.attendance_check_ajax',$data);
			}

		}
		else
		{
			return showWarning();
		}
	}
	public function getDirAttendance()
	{
		if(canView('hr_attendance'))
		{
			$data['year'] = Input::get('year');//shamsi year
			$data['month'] = Input::get('month');//shamsi month
			if(Input::get('general_department')!=null)
			{
				$data['dep'] = Input::get('general_department');
			}
			else
			{
				$data['dep']=0;
			}
			if(Input::get('sub_dep')!=null)
			{
				$data['dep_id'] = Input::get('sub_dep');
			}
			else
			{
				$data['dep_id']=0;
			}

			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_dep']=getRelatedSubDepartment(Input::get('general_department'));

			$per_page=10;

			$data['rows'] = hrOperation::getRelatedEmployeesAjax($data['dep'],$data['dep_id'],$per_page,1);
			//dd($data['rows']);
			return View::make('hr.attendance.dir_attendance_ajax',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getSearchResult()
	{
		if(canView('hr_attendance'))
		{
			$data['year'] = Input::get('year');//shamsi year
			$data['month'] = Input::get('month');//shamsi month
			if(Input::get('general_department')!=null)
			{
				$data['dep'] = Input::get('general_department');
			}
			else
			{
				$data['dep']=0;
			}
			if(Input::get('sub_dep')!=null)
			{
				$data['dep_id'] = Input::get('sub_dep');
			}
			else
			{
				$data['dep_id']=0;
			}

			$data['rows'] = hrOperation::getRelatedEmployeesAjax_search(Input::get('item'),$data['dep'],$data['dep_id'],Input::get('check'));

			//dd($data['rows']);
			if(Input::get('check')==0)
			{
				return View::make('hr.attendance.attendance_ajax_search',$data);
			}
			else
			{
				return View::make('hr.attendance.attendance_check_ajax_search',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getDirSearchResult()
	{
		if(canView('hr_attendance'))
		{
			$data['year'] = Input::get('year');//shamsi year
			$data['month'] = Input::get('month');//shamsi month
			if(Input::get('general_department')!=null)
			{
				$data['dep'] = Input::get('general_department');
			}
			else
			{
				$data['dep']=0;
			}
			if(Input::get('sub_dep')!=null)
			{
				$data['dep_id'] = Input::get('sub_dep');
			}
			else
			{
				$data['dep_id']=0;
			}

			$data['rows'] = hrOperation::getRelatedEmployeesAjax_search(Input::get('item'),$data['dep'],$data['dep_id'],1);

			//dd($data['rows']);
			return View::make('hr.attendance.dir_attendance_ajax_search',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeAttData($dep_id=0,$sub=0,$year,$month)
	{
		if(canView('hr_attendance'))
		{
			//get all data
			$object = hrOperation::getRelatedEmployees($dep_id,$sub);//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'father_name',
							'position',
							'department'
							)
				->addColumn('present', function($option) use($year,$month){

					return getEmployeePresentDays($option->RFID,$year,$month)+getPresentedDays($option->RFID,$year,$month);

				})
				->addColumn('absent', function($option) use($year,$month){
					return getEmployeeAbsentDays(getEmployeePresentDays($option->RFID,$year,$month)+getPresentedDays($option->RFID,$year,$month),$year,$month) - getEmployeeLeaveDays($option->id,$year,$month);
					//$options .= getEmployeeLeaveDays($option->id,2015,12);

				})
				->addColumn('leave', function($option) use($year,$month){
					//$options = getEmployeeAbsentDays(getEmployeePresentDays($option->RFID,2015,12),2015,12) - countHolidays(2015,12);
					return getEmployeeLeaveDays($option->id,$year,$month);

				})
				->addColumn('verify', function($option) use($year,$month){
					$options = '';
					if($option->RFID!=0)
					{
						/*
						<a href="'.route('approveEmployeeAttendance',array($option->RFID,$year,$month)).'" class="table-link" style="text-decoration:none;">
									تایید عکسها
								</a> |
						*/
						if(canCheckImages())
						{
							$options .= '
								<a href="'.route('getEmployeeAttendanceCheck',array($option->RFID,$year,$month,$option->general_department,$option->dep_id)).'" class="table-link" style="text-decoration:none;">
									بررسی عکسها
								</a>
								';
						}
						else
						{
							$options .= '
								<a href="javascript:void()" onclick="load_salary_wait('.$option->id.');" class="table-link" style="text-decoration:none;" data-target="#salary_modal" data-toggle="modal">
									معطل به معاش
								</a> |

								<a href="'.route('getEmployeeAttendance',array($option->RFID,$year,$month,$option->general_department,$option->dep_id)).'" class="table-link" style="text-decoration:none;">
									بررسی عکسها
								</a>
								';
						}
					}
					else
					{
					$options .= '
								<a href="javascript:void()" class="table-link" style="text-decoration:none;">
								بدون کارت
								</a>

								';
					}
					return $options;
				})
				->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeSalaryWait()
	{
		$data['id'] = Input::get('id');
		$data['year'] = Input::get('year');
		$data['month'] = Input::get('month');
		$data['details'] = hrOperation::getEmployeeSalaryWait(Input::get('id'),Input::get('year'),Input::get('month'));

		return View::make('hr.attendance.employeeSalaryWait',$data);
	}
	public function getLeavesEmployees()
	{
		if(canAttHoliday())
		{
			$data['dep'] = Input::get('general_department');
			if(Input::get('sub_dep'))
			{
				$data['sub_dep'] = Input::get('sub_dep');
			}
			else
			{
				$data['sub_dep'] = 0;
			}
			$data['year'] 		= Input::get('year');
			$data['month'] 		= Input::get('month');
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_deps'] 	= getRelatedSubDepartment(Input::get('general_department'));
			//$data['employees'] = hrOperation::getRelatedEmployees(1,Input::get('dep_id'));

			return View::make('hr.attendance.employeeLeaves',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getLeavesEmployeesData($dep=0,$sub_dep=0,$year,$month)
	{
		//get all data
		$object = hrOperation::getEmployees_leave($dep,$sub_dep);//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'father_name',
							'position'
							)
				->addColumn('status', function($option){
					if($option->mawqif_employee==8){$status = '<span style="color:red">تبدیلی</span>';}
                    elseif($option->mawqif_employee==6){$status = '<span style="color:red">منفک</span>';}
                    elseif($option->mawqif_employee==9){$status = '<span style="color:red">استعفا</span>';}
                    elseif($option->mawqif_employee==5){$status = '<span style="color:red">تقاعد</span>';}
                    elseif($option->mawqif_employee==2){$status = '<span style="color:red">انتظاربامعاش</span>';}
                    else{$status='کارمند برحال';}
                    return $status;
				})
				->addColumn('zarori', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,1,$year,$month);
				})
				->addColumn('tafrihi', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,2,$year,$month);
				})
				->addColumn('sick', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,3,$year,$month);
				})
				->addColumn('pregnant', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,4,$year,$month);
				})
				->addColumn('wedding', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,5,$year,$month);
				})
				->addColumn('haj', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,6,$year,$month);
				})
				->addColumn('extra_seak', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,8,$year,$month);
				})
				->addColumn('other', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,7,$year,$month);
				})
				->addColumn('total', function($option) use($year,$month){
					return getEmployeeLeaves_total($option->id,$year,$month);
				})
				->addColumn('operations', function($option) use($year,$month){
					return '
							<a href="'.route('getEmployeeLeaves',array($option->id,$year,$month)).'" class="table-link" style="text-decoration:none;">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							';
				})
				->make();
		return $result;
	}
	public function getEmployeeLeavesDep_print($dep=0,$sub_dep=0,$year,$month)
	{
		//$data['employees'] = hrOperation::getEmployeesWithRFID($dep,$sub_dep);
		$data['shamsi_months'] = array('01'=>'حمل',
							 '02'=>'ثور',
							 '03'=>'جوزا',
							 '04'=>'سرطان',
							 '05'=>'اسد',
							 '06'=>'سنبله',
							 '07'=>'میزان',
							 '08'=>'عقرب',
							 '09'=>'قوس',
							 '10'=>'جدی',
							 '11'=>'دلو',
							 '12'=>'حوت');
		$data['year'] 	= $year;
		$data['month'] 	= $month;
		$data['dep_id']	= $sub_dep;
		$data['employees'] = hrOperation::getEmployeeForAttPrint_aop($sub_dep);
		Excel::create('Leaves', function($file) use($data){

			$file->setTitle('Leaves');

			$file->sheet('Leaves', function($sheet) use($data){
				$year = $data['year'];
				$month= $data['month'];

				$sheet->setRightToLeft(true);
				$sheet->setOrientation('landscape');
				$sheet->setFontFamily('B Nazanin');
				$sheet->setFontSize(14);

				//$sheet->setAutoSize(true);
				$sheet->mergeCells('A1:L1');
				$sheet->row(1, function($row) {
					$row->setAlignment('center');
					$row->setFontWeight('bold');
					//$row->setBackground('#FBE5D6');
				});
				$sheet->setCellValue('A1','جمهوری اسلامی افغانستان');

				$sheet->mergeCells('A2:L2');
				$sheet->row(2, function($row) {
					$row->setAlignment('center');
					$row->setFontWeight('bold');
				});
				if(getUserDepType(Auth::user()->id)->dep_type==1)
				{
					$sheet->setCellValue('A2','ریاست عمومی ریاست دفتر');

					$sheet->mergeCells('A3:L3');
					$sheet->row(3, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A3','معاونیت منابع و اداره');

					$sheet->mergeCells('A4:L4');
					$sheet->row(4, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
				}
				else
				{
					$sheet->setCellValue('A2','ریاست عمومی ادراه امور ریاست جمهوری ا.ا');

					$sheet->mergeCells('A3:L3');
					$sheet->row(3, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A3','واحد عملیاتی و حمایوی مالی و اداری');

					$sheet->mergeCells('A4:L4');
					$sheet->row(4, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
				}
				$sheet->setCellValue('A4','ریاست منابع بشری');

				$sheet->mergeCells('A5:L5');
				$sheet->row(5, function($row) {
					$row->setAlignment('center');
					$row->setFontWeight('bold');
				});
				$sheet->setCellValue('A5','راپور رخصتی سال '.' '.$year);

				$sheet->mergeCells('A6:A7');
				$sheet->row(6, function($row) {
					$row->setAlignment('center');
					$row->setFontWeight('bold');
					$row->setBackground('#FBE5D6');
				});
				$sheet->setCellValue('A6','شماره');

				$sheet->mergeCells('B6:C6');
				$sheet->row(6, function($row) {
					$row->setAlignment('center');
					$row->setFontWeight('bold');
					$row->setBackground('#FBE5D6');
				});
				$sheet->setCellValue('B6','شهرت');

				$sheet->mergeCells('D6:F6');
				$sheet->row(6, function($row) {
					$row->setAlignment('center');
					$row->setFontWeight('bold');
					$row->setBackground('#FBE5D6');
				});
				$sheet->setCellValue('D6','مشخصات بست و وظیفه');

				$sheet->mergeCells('G6:L6');
				$sheet->row(6, function($row) {
					$row->setAlignment('center');
					$row->setFontWeight('bold');
					$row->setBackground('#FBE5D6');
				});
				$sheet->setCellValue('G6','رخصتی ها');

				$sheet->row(7, function($row) {
					$row->setAlignment('center');
					$row->setFontWeight('bold');
					$row->setBackground('#FBE5D6');
				});
				$sheet->setCellValue('B7','نام کامل');
				$sheet->setCellValue('C7','نام پدر');
				$sheet->setCellValue('D7','رتبه');
				$sheet->setCellValue('E7','بست');
				$sheet->setCellValue('F7','وظیفه');
				$sheet->setCellValue('G7','ضروری');
				$sheet->setCellValue('H7','مریضی');
				$sheet->setCellValue('I7','تفریحی');
				// $sheet->setCellValue('J7','خدمتی');
				// $sheet->setCellValue('K7','دیگر موارد');
				$sheet->setCellValue('J7','مجموع');



				$row = 8;
				$day_counter=1;
				foreach($data['employees'] AS $emp)
				{
					$sheet->setCellValue('A'.$row.'',$day_counter);
					$sheet->setCellValue('B'.$row.'',$emp->name);
					$sheet->setCellValue('C'.$row.'',$emp->father_name);
					$sheet->setCellValue('D'.$row.'',$emp->rank);
					$sheet->setCellValue('E'.$row.'',$emp->bast);
					$sheet->setCellValue('F'.$row.'',$emp->current_position_dr);
					$types = array('0'=>'',
	                    					 '1'=>'ضروری',
	                    					 '2'=>'تفریحی',
	                    					 '3'=>'مریضی',
	                    					 '4'=>'ولادی',
	                    					 '5'=>'عروسی',
	                    					 '6'=>'حج',
	                    					 '7'=>'دیگر(خدمتی)',
	                    					 '8'=>'اضافه رخصتی مریضی',
	                    					 '9'=>'دیگر موارد',
	                               '10'=>'شامل کتاب حاضری'
	                    );
					if($emp->RFID>0)
					{//employees included in e-attendance
						$leaves = hrOperation::getEmployeeLeaves_groupby($emp->id,$data['year']);
						$zarori=0;$tafrihi=0;$sick=0;$khedmati=0;$other=0;
						if($leaves)
						{
							foreach($leaves AS $leave)
							{
								if($leave->type==1)
								{
									$zarori=$leave->total;
								}
								elseif($leave->type==2)
								{
									$tafrihi=$leave->total;
								}
								elseif($leave->type==3)
								{
									$sick=$leave->total;
								}
								elseif($leave->type==7)
								{
									$khedmati=$leave->total;
								}
								elseif($leave->type==9)
								{
									$other=$leave->total;
								}

							}
						}

						$sheet->setCellValue('G'.$row.'',$zarori);
						$sheet->setCellValue('H'.$row.'',$sick);
						$sheet->setCellValue('I'.$row.'',$tafrihi);
						// $sheet->setCellValue('J'.$row.'',$khedmati);

						// $sheet->setCellValue('K'.$row.'',$other);
						$sheet->setCellValue('J'.$row.'',$zarori+$sick+$tafrihi);

					}
					else
					{
						$sheet->setCellValue('G'.$row.'',0);
						$sheet->setCellValue('H'.$row.'',0);
						$sheet->setCellValue('I'.$row.'',0);
						// $sheet->setCellValue('J'.$row.'',0);
						// $sheet->setCellValue('K'.$row.'',0);
						$sheet->setCellValue('J'.$row.'',0);
					}
					$row++;$day_counter++;
				}

				$sheet->cells('A6:L'.$row.'', function($cells) {
					$cells->setValignment('center');
					//$cells->setAlignment('center');
				});
/*
				$sheet->mergeCells('A1:F1');
				$sheet->setCellValue('A1','راپور رخصتی');
				$sheet->setCellValue('A2','#');
				$sheet->setCellValue('B2','نوعیت رخصتی');
				$sheet->setCellValue('C2','از تاریخ');
				$sheet->setCellValue('D2','تا تاریخ');
				$sheet->setCellValue('E2','تعداد روز(ها)');
				$sheet->setCellValue('F2','تفصیلات');
				$sheet->row(1, function($row) {
				    // call cell manipulation methods
				    $row->setBackground('#e0ebeb');
					$row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$sheet->row(2, function($row) {
					$row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$row = 3;
				$types = array('0'=>'',
                    					 '1'=>'ضروری',
                    					 '2'=>'تفریحی',
                    					 '3'=>'مریضی',
                    					 '4'=>'ولادی',
                    					 '5'=>'عروسی',
                    					 '6'=>'حج',
                    					 '7'=>'دیگر(خدمتی)',
                    					 '8'=>'اضافه رخصتی مریضی',
                    					 '9'=>'دیگر موارد',
                                         '10'=>'شامل کتاب حاضری'
                    );
				foreach($data['employees'] AS $rows)
				{
					$leave_no=0;$day_counter = 1;
					$sheet->mergeCells('A'.$row.':F'.$row);
					$sheet->setCellValue('A'.$row.'','راپور رخصتی محترم:'.' '.$rows->name.' '.$rows->last_name);
					$sheet->row($row, function($row) {
					    // call cell manipulation methods
					    $row->setBackground('#e0ebeb');
						$row->setAlignment('center');
						//$row->setValignment('middle');
						$row->setFontWeight('bold');
					});
					$leaves = hrOperation::getEmployeeLeaves($rows->id,$data['year'],$data['month']);
					$row++;
					if($leaves)
					{
						foreach($leaves AS $leave)
						{
							$sdate = $leave->date_from;$edate = $leave->date_to;
		            		if($sdate !=''){$sdate = explode('-',$sdate);$sdate=dateToShamsi($sdate[0],$sdate[1],$sdate[2]);$sdate=jalali_format($sdate);}
		            		if($edate !=''){$edate = explode('-',$edate);$edate=dateToShamsi($edate[0],$edate[1],$edate[2]);$edate=jalali_format($edate);}

							$sheet->setCellValue('A'.$row.'',$day_counter);
							$sheet->setCellValue('B'.$row.'',$types[$leave->type]);
							$sheet->setCellValue('C'.$row.'',$sdate);
							$sheet->setCellValue('D'.$row.'',$edate);
							$sheet->setCellValue('E'.$row.'',$leave->days_no);
							$sheet->setCellValue('F'.$row.'',$leave->desc);


							$row++;$day_counter++;$leave_no+=$leave->days_no;
						}
						$sheet->mergeCells('B'.$row.':D'.$row);
						$sheet->setCellValue('B'.$row.'','مجموع رخصتی های:'.' '.$rows->name.' '.$rows->last_name);
						$sheet->setCellValue('E'.$row.'',$leave_no .' روز');
						$sheet->row($row, function($row) {
						    // call cell manipulation methods
						    $row->setBackground('#e0ebeb');
							$row->setAlignment('center');
							//$row->setValignment('middle');
							$row->setFontWeight('bold');
						});
					}
					else
					{
						$sheet->mergeCells('A'.$row.':F'.$row);
						$sheet->setCellValue('A'.$row.'','ریکاردی موجود نیست!');
					}
					$row=$row+2;
				}
*/


    		});
		})->export('xlsx');
	}
	public function getAllInShifts()
	{
		$data['parentDeps'] = getDepartmentWhereIn();
		$total=hrOperation::getEmployees_inShift();
		$data['total']=count($total);
		return View::make('hr.attendance.shiftList',$data);
	}
	public function getShiftEmployees($dep=0,$sub=0,$status=0,$gender=0)
	{
		if(canAdd('hr_attendance'))
		{
			$data['dep_id'] = $dep;
			$data['sub_dep_id'] = $sub;
			$data['status'] = $status;
			$data['gender'] = $gender;
			$total=hrOperation::getEmployees_inShift($dep,$sub,$status,$gender);
			$data['total']=count($total);
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_deps']=getRelatedSubDepartment($dep);
			//$data['employees'] = hrOperation::getRelatedEmployees(Input::get('dep_id'));
			return View::make('hr.attendance.shiftList',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getShiftEmployeesData($dep=0,$sub=0,$status=0,$gender=0)
	{
		$object = hrOperation::getEmployees_inShift($dep,$sub,$status,$gender);

		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name_dr',
							'dep_name',
							'position'
							)
						->addColumn('status', function($option){
						if($option->time_in!=''){$status = 'صبح';}
	                    elseif($option->time_out!=''){$status = 'بعدازظهر';}
	                    else{$status='پنجشنبه';}
	                    return $status;
					})

				->make();
		return $result;
	}
	public function printShiftEmployees($dep=0,$sub_dep=0,$status,$gender=0)
	{
		$data['records'] = hrOperation::getEmployees_inShift($dep,$sub_dep,$status,$gender);

		Excel::create('students', function($file) use($data){

			$file->setTitle('Attendance');

			$file->sheet('Attendance', function($sheet) use($data){
				$sheet->setRightToLeft(true);
				$sheet->setCellValue('A1','#');
				$sheet->setCellValue('B1','اسم');
				$sheet->setCellValue('C1','ولد');
				$sheet->setCellValue('D1','دیپارتمنت');
				$sheet->setCellValue('E1','وظیفه');
				$sheet->setCellValue('F1','وضعیت');
				$sheet->row(1, function($row) {
				    // call cell manipulation methods
				    $row->setBackground('#e0ebeb');
					$row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$row = 2;
				$counter = 1;
				foreach($data['records'] AS $emp)
				{
					$sheet->setCellValue('A'.$row,$counter);
					$sheet->setCellValue('B'.$row,$emp->fullname);
					$sheet->setCellValue('C'.$row,$emp->father_name_dr);
					$sheet->setCellValue('D'.$row,$emp->dep_name);
					$sheet->setCellValue('E'.$row,$emp->position);
					if($emp->time_in!='')
					{
						$sheet->setCellValue('F'.$row,'صبح');
					}
					elseif($emp->time_out!='')
					{
						$sheet->setCellValue('F'.$row,'بعدازظهر');
					}
					else
					{
						$sheet->setCellValue('F'.$row,'پنجشنبه');
					}
					$row++;
					$counter++;
				}
				$sheet->setBorder('A1:E'.$row.'', 'dotted');
    		});

		})->download('xlsx');
	}
	public function getAllAttEmps()
	{
		$data['parentDeps'] = getDepartmentWhereIn();
		$data['total'] = hrOperation::getEmployees_inAtt();
		return View::make('hr.attendance.att_employees',$data);
	}
	public function getAtttEmployeesData($dep=0,$sub=0,$gender=0)
	{
		$object = hrOperation::getEmployees_inAtt($dep,$sub,$gender);
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name_dr',
							'dep_name',
							'position'
							)

				->make();
		return $result;
	}
	public function getAttEmployees($dep=0,$sub=0,$gender=0)
	{
		if(canAdd('hr_attendance'))
		{
			$data['dep_id'] = $dep;
			$data['sub_dep_id'] = $sub;
			$data['gender'] = $gender;

			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_deps']=getRelatedSubDepartment($dep);
			$data['total'] = hrOperation::getEmployees_inAtt($dep,$sub,$gender);
			return View::make('hr.attendance.att_employees',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getPayrollList($dep_id=0,$year=0,$month=0)
	{
		$data['year'] 	= $year;
		$data['month'] 	= $month;
		$data['dep_id']	= $dep_id;
        $data['employees'] = hrOperation::getEmployeePayroll($dep_id);

		return View::make('hr.attendance.payrollList',$data);
	}
	public function getAttCharts($dep_id=0,$year=0,$month=0)
	{
		$data['year'] 	= $year;
		$data['month'] 	= $month;
		$data['dep_id']	= $dep_id;
		$data['dep'] = hrOperation::getDepName($dep_id);
		$data['dep_employees'] = hrOperation::getDepEmployeeTotal($dep_id);
		return View::make('hr.attendance.attCharts',$data);
	}
	public function printDailyAttChart($dep_id=0)
	{
		$data['dep_id']	= $dep_id;
		$data['dep'] = hrOperation::getDepName($dep_id);
		$data['date'] = dateToShamsi(date('Y'),date('m'),date('d'));
		//$data['records'] = hrOperation::getEmployeesTodayAtt(1,$dep_id,0);
		return View::make('hr.attendance.dailyAttCharts',$data);
	}
	public function printPayroll($dep_id=0,$year=0,$month=0)
	{
		if(canView('hr_attendance'))
		{
			$data['year'] 	= $year;
			$data['month'] 	= $month;
			$data['dep_id']	= $dep_id;
			$data['employees'] = hrOperation::getEmployeePayroll($dep_id);
			$data['ajirs'] = hrOperation::getAjirPayroll($dep_id);

			Excel::load('excel_template/payroll.xlsx', function($file) use($data){

				$file->setTitle('Payroll');

				$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){

					$year = $data['year'];
					$month= $data['month'];
					$row = 10;
					$day_counter=1;
					$empMainSalary=0;$empProfSalary=0;$empMakol=0;$empExtraSalary=0;$empCar=0;$empBenefits=0;$empTotalBenifits=0;
					$empRetire8=0;$empTax=0;$empRob=0;$empAbsent=0;$empKsrat=0;$empTotalKsrat=0;
					foreach($data['employees'] AS $emp)
					{
						//get employee estehqaq details
						$other_details = hrOperation::getEmployeeEstehqaq($emp->id,$year,$month);
						if($other_details)
						{//if estehqaq details is inserted for this employee
							$estehqaq_total = $emp->main_salary+$emp->prof_salary+$other_details->makolat+$emp->extra_salary+$other_details->car_rent+$other_details->other_benifit;
							$robh = $other_details->robh;
							$makafat = $other_details->makolat;
							$car_rent = $other_details->car_rent;
							$other_benifit = $other_details->other_benifit;
							$other_ksor = $other_details->other_ksor;
						}
						else
						{//if not, set them to 0
							$estehqaq_total = $emp->main_salary+$emp->prof_salary+$emp->extra_salary;
							$robh = 0;
							$makafat = 0;
							$car_rent = 0;
							$other_benifit = 0;
							$other_ksor = 0;
						}

						$retire_8 = (8*($emp->main_salary+$emp->prof_salary))/100;
						$retire_3 = (3*$emp->main_salary)/100;
						$tax = calculateTax($estehqaq_total);


						//$total_salary = $emp->extra_salary+$emp->main_salary+$emp->prof_salary+$emp->kadri_salary;
						//$total_benifits = getEmployeePayrollDetTotal($emp->id,$year,$month,'in');
						//$total_ksorat = getEmployeePayrollDetTotal($emp->id,$year,$month,'out');
						//$salary_wait = getEmployeeSalaryWait($emp->id,$year,$month);

	               		$leaves = getEmployeeLeaveDays($emp->id,$year,$month);
	               		$presents = getEmployeePresentDays($emp->RFID,$year,$month);
	               		$holidays = countHolidays($year,$month);//fridays and 5shanba
	               		$paid_days = $leaves + $presents + $holidays;
	               		$monthDays = countMonthDays($year,$month);
               			$unpaid_days = $monthDays - ($paid_days);
               			$salary_perday = 0;//emtiazi
               			$main_salary_perday = 0;//asli
	               		$prof_salary_perday = 0;//maslaki
	               		$kadri_salary_perday = 0;//kadri
	               		//$extra_tax = calculateTax($emp->extra_salary);
	               		//$main_tax = calculateTax($emp->main_salary);
	               		//$prof_tax = calculateTax($emp->prof_salary);
	               		//$kadri_tax = calculateTax($emp->kadri_salary);
	               		//$total_tax = $main_tax+$extra_tax+$prof_tax+$kadri_tax;
	               		if($emp->extra_salary!=0)
	               		{
	               			$salary_perday = $emp->extra_salary/30;
	               		}
	               		if($emp->main_salary!=0)
	               		{
	               			$main_salary_perday = $emp->main_salary/30;
	               		}
	               		if($emp->prof_salary!=0)
	               		{
	               			$prof_salary_perday = $emp->prof_salary/30;
	               		}
	               		if($emp->kadri_salary!=0)
	               		{
	               			$kadri_salary_perday = $emp->kadri_salary/30;
	               		}

	               		$absent_deduction = round($unpaid_days*$salary_perday)+round($unpaid_days*$main_salary_perday)+round($unpaid_days*$prof_salary_perday)+round($unpaid_days*$kadri_salary_perday);
	               		//$ksarat_total = $retire_8+$retire_3+$tax+$robh+$absent_deduction;
	               		$ksarat_total = $retire_8+$other_ksor+$tax+$robh+$absent_deduction;

						$sheet->setCellValue('A'.$row.'',$day_counter);
						$sheet->setCellValue('B'.$row.'',$emp->name);
						$sheet->setCellValue('C'.$row.'',$emp->father_name);
						$sheet->setCellValue('D'.$row.'',$emp->employee_type);
						$sheet->setCellValue('E'.$row.'',$emp->emp_rank);
						$sheet->setCellValue('F'.$row.'',$emp->emp_bast);
						$sheet->setCellValue('G'.$row.'',$emp->main_salary);
						$sheet->setCellValue('H'.$row.'',$emp->prof_salary);
						$sheet->setCellValue('I'.$row.'',$makafat);//makol
						$sheet->setCellValue('J'.$row.'',$emp->extra_salary);
						$sheet->setCellValue('K'.$row.'',$car_rent);
						$sheet->setCellValue('L'.$row.'',$other_benifit);
						$sheet->setCellValue('M'.$row.'',$estehqaq_total);
						$sheet->setCellValue('N'.$row.'',$retire_8);
						//$sheet->setCellValue('O'.$row.'',$retire_3);
						$sheet->setCellValue('O'.$row.'',$tax);
						$sheet->setCellValue('P'.$row.'',$robh);
						$sheet->setCellValue('Q'.$row.'',$absent_deduction);
						$sheet->setCellValue('R'.$row.'',$other_ksor);
						$sheet->setCellValue('S'.$row.'',$ksarat_total);
						$sheet->setCellValue('T'.$row.'',$estehqaq_total-$ksarat_total);
						//$sheet->setCellValue('J'.$row.'',($total_salary+$total_benifits)-($emp->retirment+$total_tax+$absent_deduction+$total_ksorat));

						$row++;$day_counter++;
						$empMainSalary+=$emp->main_salary;$empProfSalary+=$emp->prof_salary;$empMakol+=$makafat;$empExtraSalary+=$emp->extra_salary;$empCar+=$car_rent;$empBenefits+=$other_benifit;$empTotalBenifits+=$estehqaq_total;
						$empRetire8+=$retire_8;$empTax+=$tax;$empRob+=$robh;$empAbsent+=$absent_deduction;$empKsrat+=$other_ksor;$empTotalKsrat+=$ksarat_total;
					}
					$border=$row;
					$sheet->mergeCells('A'.$border.':F'.$border.'');
					$sheet->row($border, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#D0CECE');
					});
					$sheet->setCellValue('A'.$border.'','مجموع مامورین رسمی');

					$sheet->setCellValue('G'.$border.'',$empMainSalary);
					$sheet->setCellValue('H'.$border.'',$empProfSalary);
					$sheet->setCellValue('I'.$border.'',$empMakol);//makol
					$sheet->setCellValue('J'.$border.'',$empExtraSalary);
					$sheet->setCellValue('K'.$border.'',$empCar);
					$sheet->setCellValue('L'.$border.'',$empBenefits);
					$sheet->setCellValue('M'.$border.'',$empTotalBenifits);
					$sheet->setCellValue('N'.$border.'',$empRetire8);
					//$sheet->setCellValue('O'.$row.'',$retire_3);
					$sheet->setCellValue('O'.$border.'',$empTax);
					$sheet->setCellValue('P'.$border.'',$empRob);
					$sheet->setCellValue('Q'.$border.'',$empAbsent);
					$sheet->setCellValue('R'.$border.'',$empKsrat);
					$sheet->setCellValue('S'.$border.'',$empTotalKsrat);
					$sheet->setCellValue('T'.$border.'',$empTotalBenifits-$empTotalKsrat);

					$border = $border+1;
					$sheet->mergeCells('A'.$border.':F'.$border.'');
					$sheet->row($border, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('A'.$border.'','اجیران');
					$border = $border+1;
					$day_counter = 1;
					foreach($data['ajirs'] AS $ajir)
					{
						$sheet->setCellValue('A'.$border.'',$day_counter);
						$sheet->setCellValue('B'.$border.'',$ajir->name);
						$sheet->setCellValue('C'.$border.'',$ajir->father_name);
						$sheet->setCellValue('D'.$border.'',$ajir->employee_type);
						$sheet->setCellValue('E'.$border.'',$ajir->emp_rank);
						$sheet->setCellValue('F'.$border.'',$ajir->emp_bast);
						$sheet->setCellValue('G'.$border.'',$ajir->main_salary);
						$sheet->setCellValue('H'.$border.'',$ajir->prof_salary);
						//$sheet->setCellValue('I'.$border.'',$makafat);//makol
						$sheet->setCellValue('J'.$border.'',$ajir->extra_salary);
						$border++;$day_counter++;
					}

					$sheet->mergeCells('A'.$border.':F'.$border.'');
					$sheet->row($border, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#D0CECE');
					});
					$sheet->setCellValue('A'.$border.'','مجموع اجیران');

					$border = $border+1;
					$sheet->mergeCells('A'.$border.':F'.$border.'');
					$sheet->row($border, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#D6DCE5');
					});
					$sheet->setCellValue('A'.$border.'','مجموع عمومی');
					// Set border for range
					$sheet->setBorder('A10:U'.$border.'', 'thin');
	    		});

			})->export('xlsx');
		}
		else
		{
			return showWarning();
		}
    }
    // Print Attendence Report
	public function printAttReport($dep_id=0,$year=0,$month=0,$date_type=0)
	{
		if(canView('hr_attendance'))
		{
			$data['shamsi_months'] = array('01'=>'حمل',
								 '02'=>'ثور',
								 '03'=>'جوزا',
								 '04'=>'سرطان',
								 '05'=>'اسد',
								 '06'=>'سنبله',
								 '07'=>'میزان',
								 '08'=>'عقرب',
								 '09'=>'قوس',
								 '10'=>'جدی',
								 '11'=>'دلو',
								 '12'=>'حوت');
			$data['year'] 	= $year;
			$data['month'] 	= $month;
			$data['date_type'] 	= $date_type;
			$data['dep_id']	= $dep_id;
			$data['month_number'] = '';
			//$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			if($date_type==0)
			{
				$sday = att_month_days(0);
				$eday = att_month_days(1);
				if($month==1)
				{
					$from = dateToMiladi($year-1,12,$sday);
				}
				else
				{
					$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
				}
				$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
			}
			else
			{
				$sh_from = explode('-',$year);
				$sh_to = explode('-',$month);
				$from = dateToMiladi($sh_from[2],$sh_from[1],$sh_from[0]);
				$to = dateToMiladi($sh_to[2],$sh_to[1],$sh_to[0]);
				// Append Month Number
			  $data['month_number'] = $sh_to[1];
			}
			if($to>date('Y-m-d'))
			{
				$to = date('Y-m-d');
			}
			$allHolidays =  DB::connection('hr')
						->table('month_holiday_dates')
						->select('date')
						->whereBetween('date', array($from, $to))
						->where('dep_type',1)
						->get();
			$allHolidays = json_decode(json_encode((array) $allHolidays), true);
			$new_array=array();
			foreach($allHolidays as $array)
			{
			    foreach($array as $val)
			    {
			        array_push($new_array, $val);
			    }
			}
			$data['holidays'] = $new_array;
			$data['employees'] = hrOperation::getEmployeeForAttPrint($dep_id);
			//dd($data['employees']);
			Excel::create('timesheet-'.$month, function($file) use($data){
			//Excel::load('excel_template/payroll.xlsx', function($file) use($data){
				$file->setTitle('timesheet');

				$file->sheet('timesheet', function($sheet) use($data){

					$year = $data['year'];
					$month= $data['month'];
					$date_type= $data['date_type'];
                    $month_number = $data['month_number'];

                      // Style of the Sheet
                    $fontStyle = array(
                                'font'  => array(
                                    'bold'  => false,
                                    'color' => array('rgb' => '000000'),
                                    'size'  => 12,
                                    'name'  => 'B Nazanin'
                                ));

                    // Wrap Text
                    $wrapText = array(
                         array(
                              'wrap'       => TRUE
                          )
                    );

					$sheet->setRightToLeft(true);
					$sheet->setOrientation('landscape');
                    // change whole page style
                    $sheet->getDefaultStyle()->applyFromArray($fontStyle);
					$sheet->mergeCells('A1:P1');
					$sheet->row(1, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						//$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('A1','جمهوری اسلامی افغانستان');

					$sheet->mergeCells('A2:P2');
					$sheet->row(2, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A2','ریاست عمومی دفتر مقام عالی ریاست جمهوری ا.ا');

					$sheet->mergeCells('A3:P3');
					$sheet->row(3, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A3','معاونیت منابع و اداره');

					$sheet->mergeCells('A4:P4');
					$sheet->row(4, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A4','ریاست منابع بشری');

					$sheet->mergeCells('A5:P5');
					$sheet->row(5, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					// Set the header based on date type
					if($date_type==0)
					  $sheet->setCellValue('A5','راپور حاضری کارکنان ریاست('.getDepartmentName($data["dep_id"]).') بابت برج('.$data["shamsi_months"][$month].')');
					else
						$sheet->setCellValue('A5','راپور حاضری کارکنان ریاست('.getDepartmentName($data["dep_id"]).') بابت برج('.$data["shamsi_months"][$month_number].')');

					$sheet->mergeCells('A6:A7');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('A6','شماره');

					$sheet->mergeCells('B6:C6');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('B6','شهرت');

					$sheet->mergeCells('D6:F6');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('D6','مشخصات بست و وظیفه');

					$sheet->mergeCells('G6:N6');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('G6','کوایف حاضری');

					$sheet->mergeCells('P6:P7');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('P6','ملاحضات');


					$sheet->row(7, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('B7','نام کامل');
					$sheet->setCellValue('C7','نام پدر');
					$sheet->setCellValue('D7','رتبه');
					$sheet->setCellValue('E7','بست');
					$sheet->setCellValue('F7','وظیفه');
					$sheet->setCellValue('G7','کسرات');
					$sheet->setCellValue('H7','غیرحاضر');
					$sheet->setCellValue('I7','رخصتی های عمومی');
					$sheet->setCellValue('J7','رخصتی ضروری');
					$sheet->setCellValue('K7','رخصتی مریضی');
					$sheet->setCellValue('L7','رخصتی تفریحی');
					$sheet->setCellValue('M7','خدمتی');
					$sheet->setCellValue('N7','ولادی/حج/عروسی');
					$sheet->setCellValue('O7','مجموع روزهای رخصتی');
                   $sheet->setSize(array(
                        'A7' => array(
                            'width'     => 3,
                            'height'    => 95
                        ),
                        'B7' => array(
                            'width'     => 12,
                            'height'    => 95
                        ),
                         'C7' => array(
                            'width'     => 10,
                            'height'    => 95
                         ),
                        'D7' => array(
                                    'width'     => 4,
                                    'height'    => 95
                        ),
                        'E7' => array(
                                    'width'     => 4,
                                    'height'    => 95
                        ),
                        'F7' => array(
                                    'width'     => 20,
                                    'height'    => 95
                        ),
                        'G7' => array(
                                    'width'     => 4,
                                    'height'    => 95
                        ),
                        'H7' => array(
                                    'width'     => 4,
                                    'height'    => 95
                        ),
                        'I7' => array(
                                    'width'     => 4,
                                    'height'    => 95
                        ),
                        'J7' => array(
                                    'width'     => 4,
                                    'height'    => 95
                        ),
                        'K7' => array(
                                    'width'     => 4,
                                    'height'    => 95
                        ),
                        'L7' => array(
                                    'width'     => 4,
                                    'height'    => 95
                        ),
                        'M7' => array(
                                    'width'     => 4,
                                    'height'    => 95
                        ),
                        'N7' => array(
                                    'width'     => 4,
                                    'height'    => 95
                        ),
                        'O7' => array(
                                    'width'     => 4,
                                    'height'    => 95
                        ),
                        'P7' => array(
                                    'width'     => 40,
                                    'height'    => 95
                        ),
                    ));



                    // Apply Alignment Style
                    $sheet->getStyle('A6')->getAlignment()->setTextRotation(90);
                    $sheet->getStyle('G7:O7')->getAlignment()->setTextRotation(90);
					$row = 8;
					$day_counter=1;
					$totalHolidays = countHolidays($year,$month,$date_type);

					foreach($data['employees'] AS $emp)
					{
						$absentd = hrOperation::getEmployeeAbsentDays($emp->RFID,$year,$month,$emp->id,$data['holidays'],$date_type);
						$absent = count($absentd);

						if($absent>0)
						{
							$ksrat = 'دارد';
						}
						else
						{
							$ksrat = 'ندارد';
						}

						$zarori = getEmployeeLeaves($emp->id,1,$year,$month,$date_type);
						$tafrihi = getEmployeeLeaves($emp->id,2,$year,$month,$date_type);
						$sick = getEmployeeLeaves($emp->id,3,$year,$month,$date_type);
						$x_sick = getEmployeeLeaves($emp->id,8,$year,$month,$date_type);
						$khedmati = getEmployeeLeaves($emp->id,7,$year,$month,$date_type);
						$pregnant = getEmployeeLeaves($emp->id,4,$year,$month,$date_type);
						$wedding = getEmployeeLeaves($emp->id,5,$year,$month,$date_type);
						$haj = getEmployeeLeaves($emp->id,6,$year,$month,$date_type);

						$sheet->setCellValue('A'.$row.'',$day_counter);
						$sheet->setCellValue('B'.$row.'',$emp->name);
						$sheet->setCellValue('C'.$row.'',$emp->father_name);
						$sheet->setCellValue('D'.$row.'',$emp->rank);
						$sheet->setCellValue('E'.$row.'',$emp->bast);
						$sheet->setCellValue('F'.$row.'',$emp->current_position_dr);

						if($emp->RFID==null || $emp->employee_type==2)
						{//manual
							//$sheet->setCellValue('G'.$row.'',$absent);
							$sheet->setCellValue('G'.$row.'','ندارد');
							$sheet->setCellValue('H'.$row.'',0);
						}
						else
						{
							//$sheet->setCellValue('G'.$row.'',$present);
							$sheet->setCellValue('G'.$row.'',$ksrat);
							$sheet->setCellValue('H'.$row.'',$absent);
						}
						$sheet->setCellValue('I'.$row.'',$totalHolidays);
						$sheet->setCellValue('J'.$row.'',$zarori);
						$sheet->setCellValue('K'.$row.'',$sick+$x_sick);
						$sheet->setCellValue('L'.$row.'',$tafrihi);
						$sheet->setCellValue('M'.$row.'',$khedmati);
						$sheet->setCellValue('N'.$row.'',$pregnant+$wedding+$haj);

						$sheet->setCellValue('O'.$row.'',$zarori+$sick+$x_sick+$tafrihi+$khedmati+$pregnant+$wedding+$haj);
						if($emp->RFID>0)
						{
							$z_leave = '';
							$s_leave = '';$x_leave = '';
							$t_leave = '';
							$k_leave = '';$p_leave = '';$w_leave = '';$h_leave = '';
							$gh_leave = '';$desc_hight=15;

							if(count($absentd)>0)
							{
								$has_absent=true;
								foreach($absentd as $absentday)
								{
									$absentday_y = substr($absentday,0,4);
									$absentday_m = substr($absentday,4,2);
									$absentday_d = substr($absentday,6,2);

									$absentday_date = dateToShamsi($absentday_y,$absentday_m,$absentday_d);
									$gh_leave.= $absentday_date.' , ';
								}
								if($has_absent)
								{
									$gh_leave.='غیرحاضر';
								}
								$desc_hight+=15;
							}
							if($zarori>0)
							{
								$z_leave.= 'به تاریخ های';
								$zaroris = getEmployeeLeaves_dates($emp->id,1,$year,$month,$date_type);
								foreach($zaroris as $z)
								{
									$sz_date = explode('-', $z->date_from);
									$sz_date = dateToShamsi($sz_date[0],$sz_date[1],$sz_date[2]);
									if($z->days_no>1)
									{
										$ez_date = explode('-', $z->date_to);
										$ez_date = dateToShamsi($ez_date[0],$ez_date[1],$ez_date[2]);
										$z_leave.= $sz_date.' الی '.$ez_date.' , ';
									}
									else
									{
										$z_leave.= $sz_date.' , ';
									}
								}
								$z_leave.='رخصتی ضروری.';
								$desc_hight+=15;
							}

							if($tafrihi>0)
							{
								$t_leave .= 'به تاریخ های';
								$tafrihis = getEmployeeLeaves_dates($emp->id,2,$year,$month,$date_type);
								foreach($tafrihis as $t)
								{
									$st_date = explode('-', $t->date_from);
									$st_date = dateToShamsi($st_date[0],$st_date[1],$st_date[2]);
									if($t->days_no>1)
									{
										$et_date = explode('-', $t->date_to);
										$et_date = dateToShamsi($et_date[0],$et_date[1],$et_date[2]);
										$t_leave.= $st_date.' الی '.$et_date.' , ';
									}
									else
									{
										$t_leave.= $st_date.' , ';
									}
								}
								$t_leave.='رخصتی تفریحی.';
								$desc_hight+=15;
							}
							if($sick>0)
							{
								$s_leave .= 'به تاریخ های';
								$sicks = getEmployeeLeaves_dates($emp->id,3,$year,$month,$date_type);
								foreach($sicks as $s)
								{
									$ss_date = explode('-', $s->date_from);
									$ss_date = dateToShamsi($ss_date[0],$ss_date[1],$ss_date[2]);
									if($s->days_no>1)
									{
										$es_date = explode('-', $s->date_to);
										$es_date = dateToShamsi($es_date[0],$es_date[1],$es_date[2]);
										$s_leave.= $ss_date.' الی '.$es_date.' , ';
									}
									else
									{
										$s_leave.= $ss_date.' , ';
									}
								}
								$s_leave.='رخصتی مریضی.';
								$desc_hight+=15;
							}
							if($x_sick>0)
							{
								$x_leave .= 'به تاریخ های';
								$x_sicks = getEmployeeLeaves_dates($emp->id,8,$year,$month,$date_type);
								foreach($x_sicks as $x)
								{
									$sx_date = explode('-', $x->date_from);
									$sx_date = dateToShamsi($sx_date[0],$sx_date[1],$sx_date[2]);
									if($x->days_no>1)
									{
										$ex_date = explode('-', $x->date_to);
										$ex_date = dateToShamsi($ex_date[0],$ex_date[1],$ex_date[2]);
										$x_leave.= $sx_date.' الی '.$ex_date.' , ';
									}
									else
									{
										$x_leave.= $sx_date.' , ';
									}
								}
								$x_leave.='رخصتی اضافه مریضی.';
								$desc_hight+=15;
							}
							if($khedmati>0)
							{
								$k_leave .= 'به تاریخ های';
								$khedmatis = getEmployeeLeaves_dates($emp->id,7,$year,$month,$date_type);
								foreach($khedmatis as $k)
								{
									$sk_date = explode('-', $k->date_from);
									$sk_date = dateToShamsi($sk_date[0],$sk_date[1],$sk_date[2]);
									if($k->days_no>1)
									{
										$ek_date = explode('-', $k->date_to);
										$ek_date = dateToShamsi($ek_date[0],$ek_date[1],$ek_date[2]);
										$k_leave.= $sk_date.' الی '.$ek_date.' '.$k->desc.' , ';
									}
									else
									{
										$k_leave.= $sk_date.' '.$k->desc.' , ';
									}
								}
								$k_leave.='خدمتی.';
								$desc_hight+=15;
							}
							if($pregnant>0)
							{
								$p_leave .= 'به تاریخ های';
								$pregnants = getEmployeeLeaves_dates($emp->id,4,$year,$month,$date_type);
								foreach($pregnants as $p)
								{
									$sp_date = explode('-', $p->date_from);
									$sp_date = dateToShamsi($sp_date[0],$sp_date[1],$sp_date[2]);
									if($p->days_no>1)
									{
										$ep_date = explode('-', $p->date_to);
										$ep_date = dateToShamsi($ep_date[0],$ep_date[1],$ep_date[2]);
										$p_leave.= $sp_date.' الی '.$ep_date.' '.$p->desc.' , ';
									}
									else
									{
										$p_leave.= $sp_date.' '.$p->desc.' , ';
									}
								}
								$p_leave.='رخصتی ولادی.';
								$desc_hight+=15;
							}
							if($wedding>0)
							{
								$w_leave .= 'به تاریخ های';
								$weddings = getEmployeeLeaves_dates($emp->id,5,$year,$month,$date_type);
								foreach($weddings as $w)
								{
									$sw_date = explode('-', $w->date_from);
									$sw_date = dateToShamsi($sw_date[0],$sw_date[1],$sw_date[2]);
									if($w->days_no>1)
									{
										$ew_date = explode('-', $w->date_to);
										$ew_date = dateToShamsi($ew_date[0],$ew_date[1],$ew_date[2]);
										$w_leave.= $sw_date.' الی '.$ew_date.' '.$w->desc.' , ';
									}
									else
									{
										$w_leave.= $sw_date.' '.$w->desc.' , ';
									}
								}
								$w_leave.='عروسی.';
								$desc_hight+=15;
							}
							if($haj>0)
							{
								$h_leave .= 'به تاریخ های';
								$hajs = getEmployeeLeaves_dates($emp->id,6,$year,$month,$date_type);
								foreach($hajs as $h)
								{
									$sh_date = explode('-', $h->date_from);
									$sh_date = dateToShamsi($sh_date[0],$sh_date[1],$sh_date[2]);
									if($h->days_no>1)
									{
										$eh_date = explode('-', $h->date_to);
										$eh_date = dateToShamsi($eh_date[0],$eh_date[1],$eh_date[2]);
										$h_leave.= $sh_date.' الی '.$eh_date.' '.$h->desc.' , ';
									}
									else
									{
										$h_leave.= $sh_date.' '.$h->desc.' , ';
									}
								}
								$h_leave.='حج.';
								$desc_hight+=15;
							}
							$sheet->setHeight($row, $desc_hight);
							$sheet->setCellValue('P'.$row.'',$z_leave.$x_leave.$t_leave.$s_leave.$k_leave.$gh_leave.$p_leave.$w_leave.$h_leave);
						}

						$row++;$day_counter++;
					}
                    $sheet->getStyle('A8:P'.$row.'')->getAlignment()->setWrapText(true);
					$sheet->setBorder('A6:P'.$row.'');
					$row = $row+2;
					$sheet->setCellValue('B'.$row.'','نوت:');
					$sheet->mergeCells('C'.$row.':P'.$row.'');
					$sheet->setCellValue('C'.$row.'','قرار شرح فوق راپور حاضری کارمندان و کارکنان خدماتی ریاست('.getDepartmentName($data["dep_id"]).')بابت برج '.$month.' سال مالی ۱۳۹۷ در قید( ) صفحه ترتیب و صحت است.');

					$row = $row+3;
					$sheet->setCellValue('B'.$row.'','ترتیب کننده:');
					$sheet->setCellValue('D'.$row.'','تصدیق کننده:');
					$sheet->setCellValue('G'.$row.'','منظور کننده:');
                    // Merge Center

                    $sheet->mergeCells('B'.$row.':C'.$row.'');
                    $sheet->mergeCells('D'.$row.':F'.$row.'');
                    $sheet->mergeCells('G'.$row.':P'.$row.'');
                    $sheet->cells('B'.$row.':P'.$row.'', function($cells){
                        $cells->setAlignment('right');
                    });
                    $sheet->cells('A6:P'.$row.'', function($cells) {
						$cells->setValignment('center');
						//$cells->setAlignment('center');
					});
	    		});

			})->download('xlsx');
		}
		else
		{
			return showWarning();
		}
	}
	public function printDailyAttReport($dep_id=0,$year=0,$month=0)
	{
		if(canView('hr_attendance'))
		{
			$data['shamsi_months'] = array('01'=>'حمل',
								 '02'=>'ثور',
								 '03'=>'جوزا',
								 '04'=>'سرطان',
								 '05'=>'اسد',
								 '06'=>'سنبله',
								 '07'=>'میزان',
								 '08'=>'عقرب',
								 '09'=>'قوس',
								 '10'=>'جدی',
								 '11'=>'دلو',
								 '12'=>'حوت');
			$data['year'] 	= $year;
			$data['month'] 	= $month;
			$data['dep_id']	= $dep_id;
			//$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			$sday = att_month_days(0);
			$eday = att_month_days(1);
			if($month == 1)
			{
				$from = dateToMiladi($year-1,12,$sday);
			}
			else
			{
				$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
			}
			$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
			if($to>date('Y-m-d'))
			{
				$to = date('Y-m-d');
			}
			$begin = new DateTime($from);
			$end = new DateTime($to);
			$interval = DateInterval::createFromDateString('1 day');
			$data['period'] = new DatePeriod($begin, $interval, $end);

			$allHolidays =  DB::connection('hr')
						->table('month_holiday_dates')
						->select('date')
						->whereBetween('date', array($from, $to))
						->where('dep_type',1)
						->get();
			$allHolidays = json_decode(json_encode((array) $allHolidays), true);
			$new_array=array();
			foreach($allHolidays as $array)
			{
			    foreach($array as $val)
			    {
			        array_push($new_array, $val);
			    }
			}
			$data['holidays'] = $new_array;
			$data['employees'] = hrOperation::getEmployeeForAttPrint($dep_id);
			$data['sday'] = $sday;
			//dd($data['employees']);
			Excel::create('timesheet-'.$month, function($file) use($data){
			//Excel::load('excel_template/payroll.xlsx', function($file) use($data){
				$file->setTitle('timesheet');

				$file->sheet('timesheet', function($sheet) use($data){

					$year = $data['year'];
					$month= $data['month'];

					$sheet->setRightToLeft(true);
					$sheet->setOrientation('landscape');

					$sheet->mergeCells('A1:P1');
					$sheet->row(1, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						//$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('A1','جمهوری اسلامی افغانستان');

					$sheet->mergeCells('A2:P2');
					$sheet->row(2, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A2','ریاست عمومی دفتر مقام عالی ریاست جمهوری ا.ا');

					$sheet->mergeCells('A3:P3');
					$sheet->row(3, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A3','معاونیت منابع و اداره');

					$sheet->mergeCells('A4:P4');
					$sheet->row(4, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A4','ریاست منابع بشری');

					$sheet->mergeCells('A5:P5');
					$sheet->row(5, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A5','راپور حاضری روزانه کارکنان ریاست('.getDepartmentName($data["dep_id"]).') بابت برج('.$data["shamsi_months"][$month].') سال '.$year);

					$sheet->mergeCells('A6:A7');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('A6','شماره');

					$sheet->mergeCells('B6:C6');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('B6','شهرت');

					$sheet->mergeCells('D6:F6');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('D6','مشخصات بست و وظیفه');

					$sheet->mergeCells('G6:AK6');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('G6','کوایف حاضری');




					$sheet->row(7, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('B7','نام کامل');
					$sheet->setCellValue('C7','نام پدر');
					$sheet->setCellValue('D7','رتبه');
					$sheet->setCellValue('E7','بست');
					$sheet->setCellValue('F7','وظیفه');
					$col = 'G';$sday = $data['sday'];
					foreach($data['period'] AS $day)
					{
					 	$the_day = $day->format( "Y-m-d" );
						$period_det = explode('-',$the_day);
						$period_day = $period_det[2];
						$period_month = $period_det[1];
						$p_year = $period_det[0];
						$sh_date = dateToShamsi($p_year, $period_month, $period_day);
						$sheet->setCellValue(''.$col.'7',$sh_date);
						$col++;$sday++;
					}

					$row = 8;
					$day_counter=1;
					$totalHolidays = countHolidays($year,$month);

					foreach($data['employees'] AS $emp)
					{
						$sheet->setCellValue('A'.$row.'',$day_counter);
						$sheet->setCellValue('B'.$row.'',$emp->name);
						$sheet->setCellValue('C'.$row.'',$emp->father_name);
						$sheet->setCellValue('D'.$row.'',$emp->rank);
						$sheet->setCellValue('E'.$row.'',$emp->bast);
						$sheet->setCellValue('F'.$row.'',$emp->current_position_dr);
						$col = 'G';
						foreach($data['period'] AS $day)
						{
						 	$the_day = $day->format( "Y-m-d" );
						 	$day_code = date("w", strtotime($the_day));//1: monday
							$isInLeave = isEmployeeInLeave_today($emp->id,$the_day);
							if($day_code==5)
							{
								$sheet->setCellValue(''.$col.$row.'','جمعه');
							}
							elseif(in_array($the_day, $data['holidays']))
							{
								$sheet->setCellValue(''.$col.$row.'','رخصتی عمومی');
							}
							elseif(!is_null($isInLeave))
							{
								$sheet->setCellValue(''.$col.$row.'','رخصتی');
							}
							elseif(checkIfEmployeeIsPresent($emp->RFID,$the_day))
							{
								$sheet->setCellValue(''.$col.$row.'','حاضر');
							}
							else
							{
								$sheet->setCellValue(''.$col.$row.'','غیرحاضر');
							}

							$col++;
						}

						$row++;$day_counter++;
					}

					$sheet->setBorder('A6:P'.$row.'');
					$row = $row+2;
					$sheet->setCellValue('B'.$row.'','نوت:');
					$sheet->mergeCells('C'.$row.':P'.$row.'');
					$sheet->setCellValue('C'.$row.'','قرار شرح فوق راپور حاضری کارمندان و کارکنان خدماتی ریاست('.getDepartmentName($data["dep_id"]).')بابت برج '.$month.' سال مالی ۱۳۹۷ در قید( ) صفحه ترتیب و صحت است.');

					$row = $row+3;
					$sheet->setCellValue('B'.$row.'','ترتیب کننده:');
					$sheet->setCellValue('D'.$row.'','تصدیق کننده:');
					$sheet->setCellValue('G'.$row.'','منظور کننده:');
					$sheet->cells('A6:P'.$row.'', function($cells) {
						$cells->setValignment('center');
						//$cells->setAlignment('center');
					});
	    		});

			})->download('xlsx');
		}
		else
		{
			return showWarning();
		}
	}

	public function printExtraReport_ajir($dep_id=0,$year=0,$month=0)
	{
		if(canView('hr_attendance'))
		{
			$data['shamsi_months'] = array('01'=>'حمل',
								 '02'=>'ثور',
								 '03'=>'جوزا',
								 '04'=>'سرطان',
								 '05'=>'اسد',
								 '06'=>'سنبله',
								 '07'=>'میزان',
								 '08'=>'عقرب',
								 '09'=>'قوس',
								 '10'=>'جدی',
								 '11'=>'دلو',
								 '12'=>'حوت');
			$data['year'] 	= $year;
			$data['month'] 	= $month;
			$data['dep_id']	= $dep_id;
			$data['employees'] = hrOperation::getAjirForAttPrint($dep_id);

			Excel::create('timesheet-'.$month, function($file) use($data){
			//Excel::load('excel_template/payroll.xlsx', function($file) use($data){
				$file->setTitle('timesheet');

				$file->sheet('timesheet', function($sheet) use($data){

					$year = $data['year'];
					$month= $data['month'];

					$sheet->setRightToLeft(true);
					$sheet->setOrientation('landscape');

					$sheet->mergeCells('A1:N1');
					$sheet->row(1, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						//$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('A1','جمهوری اسلامی افغانستان');

					$sheet->mergeCells('A2:N2');
					$sheet->row(2, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A2','ریاست عمومی دفتر مقام عالی ریاست جمهوری ا.ا');

					$sheet->mergeCells('A3:N3');
					$sheet->row(3, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A3','معاونیت منابع و اداره');

					$sheet->mergeCells('A4:N4');
					$sheet->row(4, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A4','ریاست منابع بشری');

					$sheet->mergeCells('A5:N5');
					$sheet->row(5, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A5','راپور اضافه کاری اجیران ریاست('.getDepartmentName($data["dep_id"]).') بابت برج('.$data["shamsi_months"][$month].') سال '.$year);

					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});

					$sheet->setCellValue('A6','شماره');
					$sheet->setCellValue('B6','شهرت');
					$sheet->setCellValue('C6','فرزند');
					$sheet->setCellValue('D6','رتبه');
					$sheet->setCellValue('E6','بست');
					$sheet->setCellValue('F6','وظیفه');
					$sheet->setCellValue('G6','ایام رخصتی های عمومی');
					$sheet->setCellValue('H6','ایام عادی');
					$sheet->setCellValue('I6','ایام تعطیل');
					$sheet->setCellValue('J6','مجموع ساعت ایام رسمی');
					$sheet->setCellValue('K6','مجموع ساعت ایام تعطیل');
					$sheet->setCellValue('L6','کسرات');

					$sheet->setCellValue('M6','تعداد غیر حاضری');
					$sheet->setCellValue('N6','ملاحضات');


					$row = 7;
					$day_counter=1;

					$totalHolidays = getMonthHolidays_ajirs($year,$month);
					$totalOffdays = counOffdays($year,$month);//fridays
					if($month< 7)
					{
						$totalDays = 31-($totalHolidays+$totalOffdays);//total working days
						$month_days = 31;
					}
					elseif($month<12)
					{
						$totalDays = 30-($totalHolidays+$totalOffdays);
						$month_days = 30;
					}
					else
					{
						$totalDays = 29-($totalHolidays+$totalOffdays);
						$month_days = 29;
					}
					foreach($data['employees'] AS $emp)
					{
						$absentd = hrOperation::getAjirAbsentDaysExtra($emp->RFID,$year,$month);
						$absent = count($absentd);

						$zarori = getEmployeeLeaves_ajir($emp->id,1,$year,$month);
						$tafrihi = getEmployeeLeaves_ajir($emp->id,2,$year,$month);
						$sick = getEmployeeLeaves_ajir($emp->id,3,$year,$month);
						//$khedmati = getEmployeeLeaves($emp->id,7,$year,$month);
						$toalKsrat = $zarori+$sick+$tafrihi+$absent;
						if($toalKsrat>0)
						{
							$ksrat = 'دارد';
						}
						else
						{
							$ksrat = 'ندارد';
						}
						$sheet->setCellValue('A'.$row.'',$day_counter);
						$sheet->setCellValue('B'.$row.'',$emp->name);
						$sheet->setCellValue('C'.$row.'',$emp->father_name);
						$sheet->setCellValue('D'.$row.'',$emp->rank);
						$sheet->setCellValue('E'.$row.'',$emp->bast);
						$sheet->setCellValue('F'.$row.'',$emp->current_position_dr);



						$sheet->setCellValue('G'.$row.'',$totalHolidays);//holidays
						$sheet->setCellValue('H'.$row.'',$totalDays-$toalKsrat);//working days
						$sheet->setCellValue('I'.$row.'',$totalOffdays);//off days
						$sheet->setCellValue('J'.$row.'',($totalDays-$toalKsrat)*3.5);//working days hours
						$sheet->setCellValue('K'.$row.'',$totalOffdays*3.5);//off days hours
						$sheet->setCellValue('L'.$row.'',$ksrat);
						$sheet->setCellValue('M'.$row.'',$toalKsrat);

						if($emp->RFID>0)
						{
							$z_leave = '';
							$s_leave = '';
							$t_leave = '';
							//$k_leave = '';
							$gh_leave = '';
							$desc_hight=15;

							if(count($absentd)>0)
							{
								$has_absent=true;
								foreach($absentd as $absentday)
								{
									$absentday_y = substr($absentday,0,4);
									$absentday_m = substr($absentday,4,2);
									$absentday_d = substr($absentday,6,2);

									$absentday_date = dateToShamsi($absentday_y,$absentday_m,$absentday_d);
									$gh_leave.= $absentday_date.' , ';
								}
								if($has_absent)
								{
									$gh_leave.='غیرحاضر';
								}
								$desc_hight+=15;
							}
							if($zarori>0)
							{
								$z_leave.= 'به تاریخ های';
								$zaroris = getEmployeeLeaves_dates_ajir($emp->id,1,$year,$month);
								foreach($zaroris as $z)
								{
									$sz_date = explode('-', $z->date_from);
									$sz_date = dateToShamsi($sz_date[0],$sz_date[1],$sz_date[2]);
									if($z->days_no>1)
									{
										$ez_date = explode('-', $z->date_to);
										$ez_date = dateToShamsi($ez_date[0],$ez_date[1],$ez_date[2]);
										$shamsi_sdate = explode('-', $sz_date);
										$shamsi_edate = explode('-', $ez_date);
										if($shamsi_sdate[1]==$shamsi_edate[1])
										{
											$z_leave.= $sz_date.' الی '.$ez_date.' , ';
										}
										else
										{//just include the days from current month
											$z_leave.= $sz_date.' الی '.$shamsi_edate[0].'-'.$shamsi_sdate[1].'-'.$month_days.' , ';
										}

									}
									else
									{
										$z_leave.= $sz_date.' , ';
									}
								}
								$z_leave.='رخصتی ضروری.';
								$desc_hight+=15;
							}

							if($tafrihi>0)
							{
								$t_leave .= 'به تاریخ های';
								$tafrihis = getEmployeeLeaves_dates_ajir($emp->id,2,$year,$month);
								foreach($tafrihis as $t)
								{
									$st_date = explode('-', $t->date_from);
									$st_date = dateToShamsi($st_date[0],$st_date[1],$st_date[2]);
									if($t->days_no>1)
									{
										$et_date = explode('-', $t->date_to);
										$et_date = dateToShamsi($et_date[0],$et_date[1],$et_date[2]);
										$shamsi_sdate = explode('-', $st_date);
										$shamsi_edate = explode('-', $et_date);
										if($shamsi_sdate[1]==$shamsi_edate[1])
										{
											$t_leave.= $st_date.' الی '.$et_date.' , ';
										}
										else
										{//just include the days from current month
											$t_leave.= $st_date.' الی '.$shamsi_edate[0].'-'.$shamsi_sdate[1].'-'.$month_days.' , ';
										}
									}
									else
									{
										$t_leave.= $st_date.' , ';
									}
								}
								$t_leave.='رخصتی تفریحی.';
								$desc_hight+=15;
							}
							if($sick>0)
							{
								$s_leave .= 'به تاریخ های';
								$sicks = getEmployeeLeaves_dates_ajir($emp->id,3,$year,$month);
								foreach($sicks as $s)
								{
									$ss_date = explode('-', $s->date_from);
									$ss_date = dateToShamsi($ss_date[0],$ss_date[1],$ss_date[2]);
									if($s->days_no>1)
									{
										$es_date = explode('-', $s->date_to);
										$es_date = dateToShamsi($es_date[0],$es_date[1],$es_date[2]);
										$shamsi_sdate = explode('-', $ss_date);
										$shamsi_edate = explode('-', $es_date);
										if($shamsi_sdate[1]==$shamsi_edate[1])
										{
											$s_leave.= $ss_date.' الی '.$es_date.' , ';
										}
										else
										{//just include the days from current month
											$s_leave.= $ss_date.' الی '.$shamsi_edate[0].'-'.$shamsi_sdate[1].'-'.$month_days.' , ';
										}

									}
									else
									{
										$s_leave.= $ss_date.' , ';
									}
								}
								$s_leave.='رخصتی مریضی.';
								$desc_hight+=15;
							}

							$sheet->setHeight($row, $desc_hight);
							$sheet->setCellValue('N'.$row.'',$z_leave.$t_leave.$s_leave.$gh_leave);
						}
						$row++;$day_counter++;
					}
					$sheet->setBorder('A6:N'.$row.'');
					$row = $row+2;
					$sheet->setCellValue('B'.$row.'','نوت:');
					$sheet->mergeCells('C'.$row.':O'.$row.'');
					$sheet->setCellValue('C'.$row.'','قرار شرح فوق راپور اضافه کاری اجیران ریاست('.getDepartmentName($data["dep_id"]).')بابت برج '.$month.' سال مالی ۱۳۹۷ در قید( ) صفحه ترتیب و صحت است.');

					$row = $row+3;
					$sheet->setCellValue('B'.$row.'','ترتیب کننده:');
					$sheet->setCellValue('D'.$row.'','تصدیق کننده:');
					$sheet->setCellValue('G'.$row.'','منظور کننده:');
					$sheet->cells('A6:N'.$row.'', function($cells) {
						$cells->setValignment('center');
						//$cells->setAlignment('center');
					});
	    		});

			})->download('xlsx');
		}
		else
		{
			return showWarning();
		}
	}
	public function printAttReport_ajir($dep_id=0,$year=0,$month=0,$date_type=0)
	{
		if(canView('hr_attendance'))
		{
			$data['shamsi_months'] = array('01'=>'حمل',
								 '02'=>'ثور',
								 '03'=>'جوزا',
								 '04'=>'سرطان',
								 '05'=>'اسد',
								 '06'=>'سنبله',
								 '07'=>'میزان',
								 '08'=>'عقرب',
								 '09'=>'قوس',
								 '10'=>'جدی',
								 '11'=>'دلو',
								 '12'=>'حوت');
			$data['year'] 	= $year;
			$data['month'] 	= $month;
			$data['date_type'] 	= $date_type;
			$data['dep_id']	= $dep_id;

			$data['employees'] = hrOperation::getAjirForAttPrint($dep_id);

			Excel::create('timesheet-'.$month, function($file) use($data){
			//Excel::load('excel_template/payroll.xlsx', function($file) use($data){

				$file->setTitle('timesheet');

				$file->sheet('timesheet', function($sheet) use($data){


                 // Excel Style
                 $fontStyle = array(
                                'font'  => array(
                                'bold'  => false,
                                'color' => array('rgb' => '000000'),
                                'size'  => 12,
                                'name'  => 'B Nazanin'
                            ));

                    // Wrap Text
                    $wrapText = array(
                        array(
                             'wrap'       => TRUE
                         )
                   );
                    $year = $data['year'];
					$month= $data['month'];
					$date_type = $data['date_type'];
					$sheet->setRightToLeft(true);
                    $sheet->setOrientation('landscape');
                    // change whole page style
                    $sheet->getDefaultStyle()->applyFromArray($fontStyle);                     $sheet->mergeCells('A1:P1');
					$sheet->row(1, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						//$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('A1','جمهوری اسلامی افغانستان');

					$sheet->mergeCells('A2:P2');
					$sheet->row(2, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A2','ریاست عمومی دفتر مقام عالی ریاست جمهوری ا.ا');

					$sheet->mergeCells('A3:P3');
					$sheet->row(3, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A3','معاونیت منابع و اداره');

					$sheet->mergeCells('A4:P4');
					$sheet->row(4, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A4','ریاست منابع بشری');

					$sheet->mergeCells('A5:P5');
					$sheet->row(5, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A5','راپور حاضری کارکنان ریاست('.getDepartmentName($data["dep_id"]).') بابت سال '.$year);
                    // Set the header based on date type
                    if($date_type==0)
					  $sheet->setCellValue('A5','راپور حاضری کارکنان ریاست('.getDepartmentName($data["dep_id"]).') بابت برج('.$data["shamsi_months"][$month].')');
                    else
                      $sheet->setCellValue('A5','راپور حاضری کارکنان ریاست('.getDepartmentName($data["dep_id"]).') از تاریخ('.$data["year"].' الی تاریخ '.$data['month'].')');

					$sheet->mergeCells('A6:A7');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('A6','شماره');

					$sheet->mergeCells('B6:C6');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('B6','شهرت');

					$sheet->mergeCells('D6:F6');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('D6','مشخصات بست و وظیفه');

					$sheet->mergeCells('G6:N6');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('G6','کوایف حاضری');

					$sheet->mergeCells('P6:P7');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('P6','ملاحضات');


					$sheet->row(7, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('B7','نام کامل');
					$sheet->setCellValue('C7','نام پدر');
					$sheet->setCellValue('D7','رتبه');
					$sheet->setCellValue('E7','بست');
					$sheet->setCellValue('F7','وظیفه');
					$sheet->setCellValue('G7','کسرات');
					$sheet->setCellValue('H7','غیرحاضر');
					$sheet->setCellValue('I7','رخصتی های عمومی');
					$sheet->setCellValue('J7','رخصتی ضروری');
					$sheet->setCellValue('K7','رخصتی مریضی');
					$sheet->setCellValue('L7','رخصتی تفریحی');
					$sheet->setCellValue('M7','خدمتی');
					$sheet->setCellValue('N7','ولادی/حج/عروسی');
					$sheet->setCellValue('O7','مجموع روزهای رخصتی');
					//$sheet->setCellValue('O7','مجموع');
 //Tayyeb
                    $sheet->setSize(array(
                        'A7' => array(
                            'width'     => 3,
                            'height'    => 95
                        ),
                        'B7' => array(
                            'width'     => 12,
                            'height'    => 95
                        ),
                        'C7' => array(
                            'width'     => 10,
                            'height'    => 95
                        ),
                        'D7' => array(
                            'width'     => 4,
                            'height'    => 95
                        ),
                        'E7' => array(
                            'width'     => 4,
                            'height'    => 95
                        ),
                        'F7' => array(
                            'width'     => 20,
                            'height'    => 95
                        ),
                        'G7' => array(
                            'width'     => 4,
                            'height'    => 95
                        ),
                        'H7' => array(
                            'width'     => 4,
                            'height'    => 95
                        ),
                        'I7' => array(
                            'width'     => 4,
                            'height'    => 95
                        ),
                        'J7' => array(
                            'width'     => 4,
                            'height'    => 95
                        ),
                        'K7' => array(
                            'width'     => 4,
                            'height'    => 95
                        ),
                        'L7' => array(
                            'width'     => 4,
                            'height'    => 95
                        ),
                        'M7' => array(
                            'width'     => 4,
                            'height'    => 95
                        ),
                        'N7' => array(
                            'width'     => 4,
                            'height'    => 95
                        ),
                        'O7' => array(
                            'width'     => 4,
                            'height'    => 95
                        ),
                        'P7' => array(
                            'width'     => 40,
                            'height'    => 95
                        ),
                    ));


                    // Apply Alignment Style
                    $sheet->getStyle('A6')->getAlignment()->setTextRotation(90);
                    $sheet->getStyle('G7:O7')->getAlignment()->setTextRotation(90);

					$row = 8;
					$day_counter=1;
					$totalHolidays = countHolidays($year,$month,$date_type);
					foreach($data['employees'] AS $emp)
					{
						$absentd = hrOperation::getAjirAbsentDays($emp->RFID,$year,$month,$date_type);
						$absent = count($absentd);
						if($absent>0)
						{
							$ksrat = 'دارد';
						}
						else
						{
							$ksrat = 'ندارد';
						}
						$zarori = getEmployeeLeaves($emp->id,1,$year,$month,$date_type);
						$tafrihi = getEmployeeLeaves($emp->id,2,$year,$month,$date_type);
						$sick = getEmployeeLeaves($emp->id,3,$year,$month,$date_type);
						$khedmati = getEmployeeLeaves($emp->id,7,$year,$month,$date_type);
						$x_sick = getEmployeeLeaves($emp->id,8,$year,$month,$date_type);
						$pregnant = getEmployeeLeaves($emp->id,4,$year,$month,$date_type);
						$wedding = getEmployeeLeaves($emp->id,5,$year,$month,$date_type);
						$haj = getEmployeeLeaves($emp->id,6,$year,$month,$date_type);

						$sheet->setCellValue('A'.$row.'',$day_counter);
						$sheet->setCellValue('B'.$row.'',$emp->name);
						$sheet->setCellValue('C'.$row.'',$emp->father_name);
						$sheet->setCellValue('D'.$row.'',$emp->rank);
						$sheet->setCellValue('E'.$row.'',$emp->bast);
						$sheet->setCellValue('F'.$row.'',$emp->current_position_dr);

						if($emp->RFID==null)
						{//manual
							//$sheet->setCellValue('G'.$row.'',$absent);
							$sheet->setCellValue('G'.$row.'','ندارد');
							$sheet->setCellValue('H'.$row.'',0);
						}
						else
						{
							//$sheet->setCellValue('G'.$row.'',$present);
							$sheet->setCellValue('G'.$row.'',$ksrat);
							$sheet->setCellValue('H'.$row.'',$absent);
						}
						$sheet->setCellValue('I'.$row.'',$totalHolidays);
						$sheet->setCellValue('J'.$row.'',$zarori);
						$sheet->setCellValue('K'.$row.'',$sick+$x_sick);
						$sheet->setCellValue('L'.$row.'',$tafrihi);
						$sheet->setCellValue('M'.$row.'',$khedmati);
						$sheet->setCellValue('N'.$row.'',$pregnant+$wedding+$haj);
						$sheet->setCellValue('O'.$row.'',$zarori+$sick+$x_sick+$tafrihi+$khedmati+$pregnant+$wedding+$haj);

						if($emp->RFID>0)
						{
							$z_leave = '';
							$s_leave = '';$x_leave = '';
							$t_leave = '';
							$k_leave = '';$p_leave = '';$w_leave = '';$h_leave = '';
							$gh_leave = '';$desc_hight=15;

							if(count($absentd)>0)
							{
								$has_absent=true;
								foreach($absentd as $absentday)
								{
									$absentday_y = substr($absentday,0,4);
									$absentday_m = substr($absentday,4,2);
									$absentday_d = substr($absentday,6,2);

									$absentday_date = dateToShamsi($absentday_y,$absentday_m,$absentday_d);
									$gh_leave.= $absentday_date.' , ';
								}
								if($has_absent)
								{
									$gh_leave.='غیرحاضر';
								}
								$desc_hight+=15;
							}
							if($zarori>0)
							{
								$z_leave.= 'به تاریخ های';
								$zaroris = getEmployeeLeaves_dates($emp->id,1,$year,$month,$date_type);
								foreach($zaroris as $z)
								{
									$sz_date = explode('-', $z->date_from);
									$sz_date = dateToShamsi($sz_date[0],$sz_date[1],$sz_date[2]);
									if($z->days_no>1)
									{
										$ez_date = explode('-', $z->date_to);
										$ez_date = dateToShamsi($ez_date[0],$ez_date[1],$ez_date[2]);
										$z_leave.= $sz_date.' الی '.$ez_date.' , ';
									}
									else
									{
										$z_leave.= $sz_date.' , ';
									}
								}
								$z_leave.='رخصتی ضروری.';
								$desc_hight+=15;
							}

							if($tafrihi>0)
							{
								$t_leave .= 'به تاریخ های';
								$tafrihis = getEmployeeLeaves_dates($emp->id,2,$year,$month,$date_type);
								foreach($tafrihis as $t)
								{
									$st_date = explode('-', $t->date_from);
									$st_date = dateToShamsi($st_date[0],$st_date[1],$st_date[2]);
									if($t->days_no>1)
									{
										$et_date = explode('-', $t->date_to);
										$et_date = dateToShamsi($et_date[0],$et_date[1],$et_date[2]);
										$t_leave.= $st_date.' الی '.$et_date.' , ';
									}
									else
									{
										$t_leave.= $st_date.' , ';
									}
								}
								$t_leave.='رخصتی تفریحی.';
								$desc_hight+=15;
							}
							if($sick>0)
							{
								$s_leave .= 'به تاریخ های';
								$sicks = getEmployeeLeaves_dates($emp->id,3,$year,$month,$date_type);
								foreach($sicks as $s)
								{
									$ss_date = explode('-', $s->date_from);
									$ss_date = dateToShamsi($ss_date[0],$ss_date[1],$ss_date[2]);
									if($s->days_no>1)
									{
										$es_date = explode('-', $s->date_to);
										$es_date = dateToShamsi($es_date[0],$es_date[1],$es_date[2]);
										$s_leave.= $ss_date.' الی '.$es_date.' , ';
									}
									else
									{
										$s_leave.= $ss_date.' , ';
									}
								}
								$s_leave.='رخصتی مریضی.';
								$desc_hight+=15;
							}
							if($khedmati>0)
							{
								$k_leave .= 'به تاریخ های';
								$khedmatis = getEmployeeLeaves_dates($emp->id,7,$year,$month,$date_type);
								foreach($khedmatis as $k)
								{
									$sk_date = explode('-', $k->date_from);
									$sk_date = dateToShamsi($sk_date[0],$sk_date[1],$sk_date[2]);
									if($k->days_no>1)
									{
										$ek_date = explode('-', $k->date_to);
										$ek_date = dateToShamsi($ek_date[0],$ek_date[1],$ek_date[2]);
										$k_leave.= $sk_date.' الی '.$ek_date.' '.$k->desc.' , ';
									}
									else
									{
										$k_leave.= $sk_date.' '.$k->desc.' , ';
									}
								}
								//$k_leave.='خدمتی.';
								$desc_hight+=15;
							}
							if($x_sick>0)
							{
								$x_leave .= 'به تاریخ های';
								$x_sicks = getEmployeeLeaves_dates($emp->id,8,$year,$month,$date_type);
								foreach($x_sicks as $x)
								{
									$sx_date = explode('-', $x->date_from);
									$sx_date = dateToShamsi($sx_date[0],$sx_date[1],$sx_date[2]);
									if($x->days_no>1)
									{
										$ex_date = explode('-', $x->date_to);
										$ex_date = dateToShamsi($ex_date[0],$ex_date[1],$ex_date[2]);
										$x_leave.= $sx_date.' الی '.$ex_date.' , ';
									}
									else
									{
										$x_leave.= $sx_date.' , ';
									}
								}
								$x_leave.='رخصتی اضافه مریضی.';
								$desc_hight+=15;
							}
							if($pregnant>0)
							{
								$p_leave .= 'به تاریخ های';
								$pregnants = getEmployeeLeaves_dates($emp->id,4,$year,$month,$date_type);
								foreach($pregnants as $p)
								{
									$sp_date = explode('-', $p->date_from);
									$sp_date = dateToShamsi($sp_date[0],$sp_date[1],$sp_date[2]);
									if($p->days_no>1)
									{
										$ep_date = explode('-', $p->date_to);
										$ep_date = dateToShamsi($ep_date[0],$ep_date[1],$ep_date[2]);
										$p_leave.= $sp_date.' الی '.$ep_date.' '.$p->desc.' , ';
									}
									else
									{
										$p_leave.= $sp_date.' '.$p->desc.' , ';
									}
								}
								$p_leave.='رخصتی ولادی.';
								$desc_hight+=15;
							}
							if($wedding>0)
							{
								$w_leave .= 'به تاریخ های';
								$weddings = getEmployeeLeaves_dates($emp->id,5,$year,$month,$date_type);
								foreach($weddings as $w)
								{
									$sw_date = explode('-', $w->date_from);
									$sw_date = dateToShamsi($sw_date[0],$sw_date[1],$sw_date[2]);
									if($w->days_no>1)
									{
										$ew_date = explode('-', $w->date_to);
										$ew_date = dateToShamsi($ew_date[0],$ew_date[1],$ew_date[2]);
										$w_leave.= $sw_date.' الی '.$ew_date.' '.$w->desc.' , ';
									}
									else
									{
										$w_leave.= $sw_date.' '.$w->desc.' , ';
									}
								}
								$w_leave.='عروسی.';
								$desc_hight+=15;
							}
							if($haj>0)
							{
								$h_leave .= 'به تاریخ های';
								$hajs = getEmployeeLeaves_dates($emp->id,6,$year,$month,$date_type);
								foreach($hajs as $h)
								{
									$sh_date = explode('-', $h->date_from);
									$sh_date = dateToShamsi($sh_date[0],$sh_date[1],$sh_date[2]);
									if($h->days_no>1)
									{
										$eh_date = explode('-', $h->date_to);
										$eh_date = dateToShamsi($eh_date[0],$eh_date[1],$eh_date[2]);
										$h_leave.= $sh_date.' الی '.$eh_date.' '.$h->desc.' , ';
									}
									else
									{
										$h_leave.= $sh_date.' '.$h->desc.' , ';
									}
								}
								$h_leave.='حج.';
								$desc_hight+=15;
							}
							$sheet->setHeight($row, $desc_hight);
							$sheet->setCellValue('P'.$row.'',$z_leave.$x_leave.$t_leave.$s_leave.$k_leave.$gh_leave.$p_leave.$w_leave.$h_leave);
						}
						$row++;$day_counter++;
					}
                    $sheet->getStyle('A8:P'.$row.'')->getAlignment()->setWrapText(true);
					$sheet->setBorder('A6:P'.$row.'');
					$row = $row+2;
					$sheet->setCellValue('B'.$row.'','نوت:');
					$sheet->mergeCells('C'.$row.':P'.$row.'');
					//$sheet->setCellValue('C'.$row.'','قرار شرح فوق راپور حاضری کارمندان و کارکنان خدماتی ریاست('.getDepartmentName($data["dep_id"]).')بابت برج '.$month.' سال مالی ۱۳۹۷ در قید( ) صفحه ترتیب و صحت است.');

					$row = $row+3;
					$sheet->setCellValue('B'.$row.'','ترتیب کننده:');
					$sheet->setCellValue('D'.$row.'','تصدیق کننده:');
					$sheet->setCellValue('G'.$row.'','منظور کننده:');
                    // Merger Center
                    $sheet->mergeCells('B'.$row.':C'.$row.'');
                    $sheet->mergeCells('D'.$row.':F'.$row.'');
                    $sheet->mergeCells('G'.$row.':P'.$row.'');
                    // Align right
                    $sheet->cells('B'.$row.':P'.$row.'', function($cells){
                        $cells->setAlignment('right');
                    });
                    $sheet->cells('A6:P'.$row.'', function($cells) {
						$cells->setValignment('center');
						//$cells->setAlignment('center');
					});
	    		});

			})->download('xlsx');
		}
		else
		{
			return showWarning();
		}
	}
	public function printAttReport_aop($dep_id=0,$year=0,$month=0)
	{
		if(canView('hr_attendance'))
		{
			$data['shamsi_months'] = array('01'=>'حمل',
								 '02'=>'ثور',
								 '03'=>'جوزا',
								 '04'=>'سرطان',
								 '05'=>'اسد',
								 '06'=>'سنبله',
								 '07'=>'میزان',
								 '08'=>'عقرب',
								 '09'=>'قوس',
								 '10'=>'جدی',
								 '11'=>'دلو',
								 '12'=>'حوت');
			$data['year'] 	= $year;
			$data['month'] 	= $month;
			$data['dep_id']	= $dep_id;
			$data['employees'] = hrOperation::getEmployeeForAttPrint_aop($dep_id);

			Excel::create('attendanceReport-'.$month, function($file) use($data){
			//Excel::load('excel_template/payroll.xlsx', function($file) use($data){
				$file->setTitle('timesheet');

				$file->sheet('timesheet', function($sheet) use($data){

					$year = $data['year'];
					$month= $data['month'];

					$sheet->setRightToLeft(true);
					$sheet->setOrientation('landscape');
					$sheet->setFontFamily('B Nazanin');
					$sheet->setFontSize(14);
					$sheet->setWidth(array(
					    'A'     =>  3,
					    'B'     =>  15,
					    'C'     =>  15,
					    'D'     =>  5,
					    'E'     =>  5,
					    'F'     =>  25,
					    'G'     =>  5,
					    'O'     =>  30
					));
					//$sheet->setAutoSize(true);
					$sheet->mergeCells('A1:O1');
					$sheet->row(1, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						//$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('A1','جمهوری اسلامی افغانستان');

					$sheet->mergeCells('A2:O2');
					$sheet->row(2, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A2','ریاست عمومی ادراه امور ریاست جمهوری ا.ا');

					$sheet->mergeCells('A3:O3');
					$sheet->row(3, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A3','واحد عملیاتی و حمایوی مالی و اداری');

					$sheet->mergeCells('A4:O4');
					$sheet->row(4, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A4','ریاست منابع بشری');

					$sheet->mergeCells('A5:O5');
					$sheet->row(5, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A5','راپور حاضری کارکنان ('.getDepartmentName($data["dep_id"]).') بابت برج('.$data["shamsi_months"][$month].') سال '.$year);

					$sheet->mergeCells('A6:A7');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('A6','شماره');

					$sheet->mergeCells('B6:C6');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('B6','شهرت');

					$sheet->mergeCells('D6:F6');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('D6','مشخصات بست و وظیفه');

					$sheet->mergeCells('G6:N6');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('G6','کوایف حاضری');

					$sheet->mergeCells('O6:O7');
					$sheet->row(6, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('O6','ملاحضات');


					$sheet->row(7, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('B7','نام کامل');
					$sheet->setCellValue('C7','نام پدر');
					$sheet->setCellValue('D7','رتبه');
					$sheet->setCellValue('E7','بست');
					$sheet->setCellValue('F7','وظیفه');
					$sheet->setCellValue('G7','کسرات');
					$sheet->setCellValue('H7','غیرحاضر');
					//$sheet->setCellValue('I7','رخصتی های عمومی');
					$sheet->setCellValue('I7','ضروری');
					$sheet->setCellValue('J7','مریضی');
					$sheet->setCellValue('K7','تفریحی');
					$sheet->setCellValue('L7','خدمتی');
					$sheet->setCellValue('M7','مجموع');
					$sheet->setCellValue('N7','کرایه موتر');
					//$sheet->setCellValue('O7','مجموع');


					$row = 8;
					$day_counter=1;
					foreach($data['employees'] AS $emp)
					{
						$sheet->setCellValue('A'.$row.'',$day_counter);
						$sheet->setCellValue('B'.$row.'',$emp->name);
						$sheet->setCellValue('C'.$row.'',$emp->father_name);
						$sheet->setCellValue('D'.$row.'',$emp->rank);
						$sheet->setCellValue('E'.$row.'',$emp->bast);
						$sheet->setCellValue('F'.$row.'',$emp->current_position_dr);

						if($emp->RFID>0)
						{//employees included in e-attendance
							//$totalHolidays = countHolidays($year,$month);
							//$present = getEmployeePresentDays($emp->RFID,$year,$month)+getPresentedDays($emp->RFID,$year,$month);
							//$present = getEmployeePresentDays($emp->RFID,$year,$month);
							//$absent = getEmployeeAbsentDays($present,$year,$month);
							$absentd = hrOperation::getEmployeeAbsentDays($emp->RFID,$year,$month);
							$absent = count($absentd);
							//$leaves = getEmployeeLeaveDays($emp->id,$year,$month);
							if($absent>0)
							{
								$ksrat = 'دارد';
							}
							else
							{
								$ksrat = 'ندارد';
							}
							$zarori = getEmployeeLeaves($emp->id,1,$year,$month);
							$tafrihi = getEmployeeLeaves($emp->id,2,$year,$month);
							$sick = getEmployeeLeaves($emp->id,3,$year,$month);
							$khedmati = getEmployeeLeaves($emp->id,7,$year,$month);

							$sheet->setCellValue('G'.$row.'',$ksrat);
							$sheet->setCellValue('H'.$row.'',$absent);
							$car_rent = $zarori+$sick+$tafrihi+$absent;

							//$sheet->setCellValue('I'.$row.'',$totalHolidays);
							$sheet->setCellValue('I'.$row.'',$zarori);
							$sheet->setCellValue('J'.$row.'',$sick);
							$sheet->setCellValue('K'.$row.'',$tafrihi);
							$sheet->setCellValue('L'.$row.'',$khedmati);

							$sheet->setCellValue('M'.$row.'',$zarori+$sick+$tafrihi+$khedmati);
							$sheet->setCellValue('N'.$row.'',$car_rent);
							$desc_hight = 15;
							$z_leave = '';
							$s_leave = '';
							$t_leave = '';
							$k_leave = '';
							$gh_leave = '';

							if(count($absentd)>0)
							{
								$has_absent = true;
								foreach($absentd as $absentday)
								{
									$absentday_y = substr($absentday,0,4);
									$absentday_m = substr($absentday,4,2);
									$absentday_d = substr($absentday,6,2);

									$absentday_date = dateToShamsi($absentday_y,$absentday_m,$absentday_d);
									$gh_leave.= $absentday_date.' , ';
								}
								if($has_absent)
								{
									//$gh_leave.='غیرحاضر';
									$gh_leave = ("به تاریخ های".$gh_leave."غیرحاضر\n");
								}
								$desc_hight+=20;
							}
							if($zarori>0)
							{
								//$z_leave.= 'به تاریخ های';
								$zaroris = getEmployeeLeaves_dates($emp->id,1,$year,$month);
								foreach($zaroris as $z)
								{
									$sz_date = explode('-', $z->date_from);
									$sz_date = dateToShamsi($sz_date[0],$sz_date[1],$sz_date[2]);
									if($z->days_no>1)
									{
										$ez_date = explode('-', $z->date_to);
										$ez_date = dateToShamsi($ez_date[0],$ez_date[1],$ez_date[2]);
										$z_leave.= $sz_date.' الی '.$ez_date.' , ';
									}
									else
									{
										$z_leave.= $sz_date.' , ';
									}
								}
								//$z_leave.="رخصتی ضروری.\n";
								$z_leave = ("به تاریخ های".$z_leave."رخصتی ضروری\n");
								$desc_hight+=15;
							}

							if($tafrihi>0)
							{
								//$t_leave .= 'به تاریخ های';
								$tafrihis = getEmployeeLeaves_dates($emp->id,2,$year,$month);
								foreach($tafrihis as $t)
								{
									$st_date = explode('-', $t->date_from);
									$st_date = dateToShamsi($st_date[0],$st_date[1],$st_date[2]);
									if($t->days_no>1)
									{
										$et_date = explode('-', $t->date_to);
										$et_date = dateToShamsi($et_date[0],$et_date[1],$et_date[2]);
										$t_leave.= $st_date.' الی '.$et_date.' , ';
									}
									else
									{
										$t_leave.= $st_date.' , ';
									}
								}
								//$t_leave.="رخصتی تفریحی.\n";
								$t_leave = ("به تاریخ های".$t_leave."رخصتی تفریحی\n");
								$desc_hight+=15;
							}
							if($sick>0)
							{
								//$s_leave .= 'به تاریخ های';
								$sicks = getEmployeeLeaves_dates($emp->id,3,$year,$month);
								foreach($sicks as $s)
								{
									$ss_date = explode('-', $s->date_from);
									$ss_date = dateToShamsi($ss_date[0],$ss_date[1],$ss_date[2]);
									if($s->days_no>1)
									{
										$es_date = explode('-', $s->date_to);
										$es_date = dateToShamsi($es_date[0],$es_date[1],$es_date[2]);
										$s_leave.= $ss_date.' الی '.$es_date.' , ';
									}
									else
									{
										$s_leave.= $ss_date.' , ';
									}
								}
								//$s_leave.="رخصتی مریضی.";
								$s_leave = ("به تاریخ های".$s_leave."رخصتی مریضی\n");
								$desc_hight+=15;
							}
							if($khedmati>0)
							{
								//$k_leave .= 'به تاریخ های';
								$khedmatis = getEmployeeLeaves_dates($emp->id,7,$year,$month);
								foreach($khedmatis as $k)
								{
									$sk_date = explode('-', $k->date_from);
									$sk_date = dateToShamsi($sk_date[0],$sk_date[1],$sk_date[2]);
									if($k->days_no>1)
									{
										$ek_date = explode('-', $k->date_to);
										$ek_date = dateToShamsi($ek_date[0],$ek_date[1],$ek_date[2]);
										$k_leave.= $sk_date.' الی '.$ek_date.' '.$k->desc.' , ';
									}
									else
									{
										$k_leave.= $sk_date.' '.$k->desc.' , ';
									}
								}
								//$k_leave.='خدمتی.</br>';
								$k_leave = ("به تاریخ های".$k_leave."\n");
								$desc_hight+=15;
							}

							$sheet->setHeight($row, $desc_hight);

							$sheet->setCellValue('O'.$row.'',$z_leave.$t_leave.$s_leave.$k_leave.$gh_leave);
						}
						else
						{
							$sheet->setCellValue('G'.$row.'','ندارد');
							$sheet->setCellValue('H'.$row.'',0);
							$sheet->setCellValue('I'.$row.'',0);
							$sheet->setCellValue('J'.$row.'',0);
							$sheet->setCellValue('K'.$row.'',0);
							$sheet->setCellValue('L'.$row.'',0);

							$sheet->setCellValue('M'.$row.'',0);
							$sheet->setCellValue('N'.$row.'',0);
						}
						$row++;$day_counter++;
					}
					$sheet->setBorder('A6:O'.$row.'');
					$row = $row+2;
					$sheet->setCellValue('B'.$row.'','نوت:');
					$sheet->mergeCells('C'.$row.':O'.$row.'');
					$sheet->setCellValue('C'.$row.'','قرار شرح فوق راپور حاضری کارمندان و کارکنان خدماتی ریاست('.getDepartmentName($data["dep_id"]).')بابت برج '.$month.' سال مالی ۱۳۹۷ در قید( ) صفحه ترتیب و صحت است.');

					$row = $row+3;
					$sheet->setCellValue('B'.$row.'','ترتیب کننده:');
					$sheet->setCellValue('D'.$row.'','تصدیق کننده:');
					$sheet->setCellValue('G'.$row.'','منظور کننده:');
					$sheet->cells('A6:O'.$row.'', function($cells) {
						$cells->setValignment('center');
						//$cells->setAlignment('center');
					});
	    		});

			})->download('xlsx');
		}
		else
		{
			return showWarning();
		}
	}
	public function printAbsentReport($dep_id=0,$year=0,$month=0)
	{
		if(canView('hr_attendance'))
		{
			$sday = att_month_days(0);
			$eday = att_month_days(1);
			if($month == 1)
			{
				$from = dateToMiladi($year-1,12,$sday);
			}
			else
			{
				$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
			}
			$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
			if($to>date('Y-m-d'))
			{
				$to = date('Y-m-d');
			}
			$data['year'] 	= $year;
			$data['month'] 	= $month;
			$data['dep_id']	= $dep_id;
			$data['employees'] = hrOperation::getEmployeesWithRFID(1,$dep_id);
			$allHolidays =  DB::connection('hr')
						->table('month_holiday_dates')
						->select('date')
						->whereBetween('date', array($from, $to))
						->where('dep_type',1)
						->get();
			$allHolidays = json_decode(json_encode((array) $allHolidays), true);
			$new_array=array();
			foreach($allHolidays as $array)
			{
			    foreach($array as $val)
			    {
			        array_push($new_array, $val);
			    }
			}
			$data['holidays'] = $new_array;
			Excel::create('absent report-'.$month, function($file) use($data){
			//Excel::load('excel_template/payroll.xlsx', function($file) use($data){
				$file->setTitle('absent report');

				$file->sheet('absent report', function($sheet) use($data){

					$year = $data['year'];
					$month= $data['month'];

					$sheet->setRightToLeft(true);
					//$sheet->setOrientation('landscape');

					$sheet->mergeCells('A1:C1');
					$sheet->row(1, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						//$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('A1','راپور حاضری کارکنان ریاست('.getDepartmentName($data["dep_id"]).') بابت برج('.$month.') سال '.$year.'');

					$sheet->setCellValue('A2','شماره');

					$sheet->setCellValue('B2','تاریخ غیرحاضری');
					$sheet->setCellValue('C2','فورم رخصتی');

					$row = 3;

					foreach($data['employees'] AS $emp)
					{
						$day_counter=1;
						$sheet->mergeCells('A'.$row.':C'.$row.'');
						$sheet->row(1, function($row) {
							$row->setAlignment('center');
							$row->setFontWeight('bold');
							//$row->setBackground('#FBE5D6');
						});
						$sheet->setCellValue('A'.$row.'',$emp->name);
						$row++;
						$absent = hrOperation::getEmployeeAbsentDays($emp->RFID,$year,$month,$emp->id,$data['holidays']);
						if(count($absent)>0)
						{
							foreach($absent as $absentday)
							{
								$singin_year = substr($absentday,0,4);
								$singin_m = substr($absentday,4,2);
								$singin_d = substr($absentday,6,2);
								$sh_date = dateToShamsi($singin_year, $singin_m, $singin_d);
								$sheet->setCellValue('A'.$row.'',$day_counter);
								$sheet->setCellValue('B'.$row.'',$sh_date);
								$sheet->setCellValue('C'.$row.'','');

								$row++;$day_counter++;
							}
						}
						else
						{
							$sheet->mergeCells('A'.$row.':C'.$row.'');
							$sheet->row(1, function($row) {
								$row->setAlignment('center');
								$row->setFontWeight('bold');
								//$row->setBackground('#FBE5D6');
							});
							$sheet->setCellValue('A'.$row.'','بدون غیر حاضری');
							$row++;
						}
					}
	    		});

			})->download('xlsx');
		}
		else
		{
			return showWarning();
		}
	}
	public function printAbsentReport_ajir($dep_id=0,$year=0,$month=0)
	{
		if(canView('hr_attendance'))
		{
			$data['year'] 	= $year;
			$data['month'] 	= $month;
			$data['dep_id']	= $dep_id;
			$data['employees'] = hrOperation::getAjirsWithRFID(1,$dep_id);

			Excel::create('absent report-'.$month, function($file) use($data){
			//Excel::load('excel_template/payroll.xlsx', function($file) use($data){
				$file->setTitle('absent report');

				$file->sheet('absent report', function($sheet) use($data){

					$year = $data['year'];
					$month= $data['month'];

					$sheet->setRightToLeft(true);
					//$sheet->setOrientation('landscape');

					$sheet->mergeCells('A1:C1');
					$sheet->row(1, function($row) {
						$row->setAlignment('center');
						$row->setFontWeight('bold');
						//$row->setBackground('#FBE5D6');
					});
					$sheet->setCellValue('A1','راپور حاضری کارکنان ریاست('.getDepartmentName($data["dep_id"]).') بابت برج('.$month.') سال '.$year.'');

					$sheet->setCellValue('A2','شماره');

					$sheet->setCellValue('B2','تاریخ غیرحاضری');
					$sheet->setCellValue('C2','فورم رخصتی');

					$row = 3;

					foreach($data['employees'] AS $emp)
					{
						$day_counter=1;
						$sheet->mergeCells('A'.$row.':C'.$row.'');
						$sheet->row(1, function($row) {
							$row->setAlignment('center');
							$row->setFontWeight('bold');
							//$row->setBackground('#FBE5D6');
						});
						$sheet->setCellValue('A'.$row.'',$emp->name);
						$row++;
						$absent = hrOperation::getAjirAbsentDays($emp->RFID,$year,$month);
						if(count($absent)>0)
						{
							foreach($absent as $absentday)
							{
                                $absent_year = substr($absentday,0,4);
								$absent_m = substr($absentday,4,2);
								$absent_d = substr($absentday,6,2);
                                $sh_date = dateToShamsi($absent_year, $absent_m, $absent_d);


								$sheet->setCellValue('A'.$row.'',$day_counter);
								$sheet->setCellValue('B'.$row.'',$sh_date);
								$sheet->setCellValue('C'.$row.'','');

								$row++;$day_counter++;
							}
						}
						else
						{
							$sheet->mergeCells('A'.$row.':C'.$row.'');
							$sheet->row(1, function($row) {
								$row->setAlignment('center');
								$row->setFontWeight('bold');
								//$row->setBackground('#FBE5D6');
							});
							$sheet->setCellValue('A'.$row.'','بدون غیر حاضری');
							$row++;
						}
					}
	    		});

			})->download('xlsx');
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeAttendance($rfid=0,$year=0,$month=0,$dep_id=0,$sub_dep=0,$in_attendance=0)
	{
		if(canView('hr_attendance'))
		{
			if($rfid>0)
			{
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
				$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
				if($user_dep_type==$data['photo']->dep_type)
				{
					$data['next_rfid'] = hrOperation::getNextEmployeeRfid($rfid,'next');
					$data['previous_rfid'] = hrOperation::getNextEmployeeRfid($rfid,'previous');
					$rfid_lost = lost_rfid($data['photo']->id);
					if($rfid_lost)
					{
						$rfids = array($rfid);
						$data['rfids'] = $rfid_lost;
						foreach($rfid_lost as $new_rfid)
						{
							$rfids[] =$new_rfid->rfid;
						}
						//dd($rfids);
						$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
						//$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
					}
					else
					{
						$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
						//$data['presentedDays'] = hrOperation::getEmployeePresented($rfid);
					}

					$data['rfid'] = $rfid;
					$data['year'] = $year;
					$data['month'] = $month;
					$data['dep'] = $dep_id;
					$data['sub_dep'] = $sub_dep;
					if(getUserDepType(Auth::user()->id)->dep_type==1 AND $data['photo']->employee_type==2)
					{
						$data['employees']=hrOperation::getRelatedAjirs($dep_id,$sub_dep);
					}
					else
					{
						$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
					}
					//dd($data['details']);
					return View::make('hr.attendance.verifying',$data);
				}
				else
				{
					return showWarning();
				}
			}
			else
			{//those who are not in attendance any more
				//get employee details

				$data['photo'] =  hrOperation::getPhotoByNoRfid($in_attendance);
				$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;

				if($user_dep_type==$data['photo']->dep_type)
				{
					$rfid_lost = lost_rfid($data['photo']->id);
					if($rfid_lost)
					{
						$rfids = array($in_attendance);
						$data['rfids'] = $rfid_lost;
						foreach($rfid_lost as $new_rfid)
						{
							$rfids[] =$new_rfid->rfid;
						}
						//dd($rfids);
						$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
						//$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
					}
					else
					{
						$data['details'] = hrOperation::getEmployeeImages($in_attendance,$year,$month);
						//$data['presentedDays'] = hrOperation::getEmployeePresented($in_attendance);
					}
					$data['rfid'] = $in_attendance;
					$data['year'] = $year;
					$data['month'] = $month;
					$data['dep'] = $dep_id;
					$data['sub_dep'] = $sub_dep;

					//$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
					return View::make('hr.attendance.verifying_no_rfid',$data);
				}
				else
				{
					return showWarning();
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeAttendanceCheck($rfid=0,$year=0,$month=0,$dep_id=0,$sub_dep=0,$in_attendance=0)
	{
		if(canCheckImages())
		{
			if($rfid>0)
			{
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
				$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
				if($user_dep_type==$data['photo']->dep_type)
				{
					$data['next_rfid'] = hrOperation::getNextEmployeeRfid($rfid,'next');
					$data['previous_rfid'] = hrOperation::getNextEmployeeRfid($rfid,'previous');
					$rfid_lost = lost_rfid($data['photo']->id);
					if($rfid_lost)
					{
						$rfids = array($rfid);
						$data['rfids'] = $rfid_lost;
						foreach($rfid_lost as $new_rfid)
						{
							$rfids[] =$new_rfid->rfid;
						}
						//dd($rfids);
						$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
						//$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
					}
					else
					{
						$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
						//$data['presentedDays'] = hrOperation::getEmployeePresented($rfid);
					}
					$data['rfid'] = $rfid;
					$data['year'] = $year;
					$data['month'] = $month;
					$data['dep'] = $dep_id;
					$data['sub_dep'] = $sub_dep;
					$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
					return View::make('hr.attendance.checking',$data);
				}
				else
				{
					return showWarning();
				}
			}
			else
			{//those who are not in attendance any more
				//get employee details
				$data['photo'] =  hrOperation::getPhotoByNoRfid($in_attendance);

				$rfid_lost = lost_rfid($data['photo']->id);
				if($rfid_lost)
				{
					$rfids = array($in_attendance);
					$data['rfids'] = $rfid_lost;
					foreach($rfid_lost as $new_rfid)
					{
						$rfids[] =$new_rfid->rfid;
					}
					//dd($rfids);
					$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
					//$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
				}
				else
				{
					$data['details'] = hrOperation::getEmployeeImages($in_attendance,$year,$month);
					//$data['presentedDays'] = hrOperation::getEmployeePresented($in_attendance);
				}
				$data['rfid'] = $in_attendance;
				$data['year'] = $year;
				$data['month'] = $month;
				$data['dep'] = $dep_id;
				$data['sub_dep'] = $sub_dep;
				$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
				return View::make('hr.attendance.checking_no_rfid',$data);
				//return View::make('hr.attendance.verifying_no_rfid',$data);
			}
		}
		elseif(canCheckAtt())
		{
			if($rfid>0)
			{
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
				$rfid_lost = lost_rfid($data['photo']->id);
				if($rfid_lost)
				{
					$rfids = array($rfid);
					$data['rfids'] = $rfid_lost;
					foreach($rfid_lost as $new_rfid)
					{
						$rfids[] =$new_rfid->rfid;
					}
					//dd($rfids);
					$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
					$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
				}
				else
				{
					$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
					$data['presentedDays'] = hrOperation::getEmployeePresented($rfid);
				}
				$absentd = hrOperation::getEmployeeAbsentDays($rfid,$year,$month);
				$data['absent'] = count($absentd);
				$data['rfid'] = $rfid;
				$data['year'] = $year;
				$data['month'] = $month;

				return View::make('hr.attendance.check_att',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getDirEmployee_images($enc_rfid=0,$year=0,$month=0,$enc_dep_id=0,$enc_sub_dep=0)
	{
		if(canCheckImages())
		{
			$rfid = Crypt::decrypt($enc_rfid);
			$dep_id=Crypt::decrypt($enc_dep_id);
			$sub_dep=Crypt::decrypt($enc_sub_dep);
			if($rfid!='')
			{
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);

				$rfid_lost = lost_rfid($data['photo']->id);
				if($rfid_lost)
				{
					$rfids = array($rfid);
					$data['rfids'] = $rfid_lost;
					foreach($rfid_lost as $new_rfid)
					{
						$rfids[] =$new_rfid->rfid;
					}
					//dd($rfids);
					$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
					//$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
				}
				else
				{
					$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
					//$data['presentedDays'] = hrOperation::getEmployeePresented($rfid);
				}
				$data['rfid'] = $rfid;
				$data['year'] = $year;
				$data['month'] = $month;
				$data['dep'] = $dep_id;
				$data['sub_dep'] = $sub_dep;
				$data['enc_dep'] = $enc_dep_id;
				$data['enc_sub_dep'] = $enc_sub_dep;
				//$data['enc_rfid'] = $enc_rfid;
				$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
				return View::make('hr.attendance.dir_checking',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getDirEmployeeImages_ajax()
	{
		if(canCheckImages())
		{
			$rfid = Input::get('rfid');
			$dep_id=Input::get('dep');
			$sub_dep=Input::get('sub_dep');
			$year=Input::get('year');
			$month=Input::get('month');
			if($rfid!='')
			{
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);

				$rfid_lost = lost_rfid($data['photo']->id);
				if($rfid_lost)
				{
					$rfids = array($rfid);
					$data['rfids'] = $rfid_lost;
					foreach($rfid_lost as $new_rfid)
					{
						$rfids[] =$new_rfid->rfid;
					}
					//dd($rfids);
					$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
					//$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
				}
				else
				{
					$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
					//$data['presentedDays'] = hrOperation::getEmployeePresented($rfid);
				}
				$data['rfid'] = $rfid;
				$data['year'] = $year;
				$data['month'] = $month;
				$data['dep'] = $dep_id;
				$data['sub_dep'] = $sub_dep;

				$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
				return View::make('hr.attendance.dir_checking_ajax',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function approveEmployeeAttendance($rfid=0,$year=0,$month=0)
	{
		if(canAdd('hr_attendance'))
		{
			if($rfid!='')
			{
				$data['next_rfid'] = hrOperation::getNextEmployeeRfid($rfid,'next');
				$data['previous_rfid'] = hrOperation::getNextEmployeeRfid($rfid,'previous');
				$data['rfid'] = $rfid;
				$data['year'] = $year;
				$data['month'] = $month;

				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
				$data['details'] = hrOperation::absentEmployeeImages($rfid,$year,$month);
				return View::make('hr.attendance.approve_images',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public static function verifyNextEmployee($rfid=0,$year=0,$month=0,$type='next',$dep_id=0,$sub_dep=0)
	{
		if(canView('hr_attendance'))
		{
			$next = hrOperation::getNextEmployeeRfid($rfid,$type);
			$next_rfid = $next->rfid;
			$data['photo'] =  $next;
			if($type=='next')
			{
				$data['next_rfid'] = $next;
			}
			else
			{
				$data['next_rfid'] = hrOperation::getNextEmployeeRfid($next_rfid,'next');
			}
			if($type=='previous')
			{
				$data['previous_rfid'] = $next;
			}
			else
			{
				$data['previous_rfid'] = hrOperation::getNextEmployeeRfid($next_rfid,'previous');
			}
			if($year == 0)
			{
				$year = date('Y');
			}
			if($month == 0)
			{
				$month = date('m');
			}
			$rfid_lost = lost_rfid($next->id);
			if($rfid_lost)
			{
				$rfids = array($next_rfid);
				$data['rfids'] = $rfid_lost;
				foreach($rfid_lost as $new_rfid)
				{
					$rfids[] =$new_rfid->rfid;
				}
				//dd($rfids);
				$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
				//$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
			}
			else
			{
				$data['details'] = hrOperation::getEmployeeImages($next_rfid,$year,$month);
				//$data['presentedDays'] = hrOperation::getEmployeePresented($next_rfid);
			}
			$data['rfid'] = $next_rfid;
			$data['year'] = $year;
			$data['month'] = $month;
			$data['dep'] = $dep_id;
			$data['sub_dep'] = $sub_dep;
			if(getUserDepType(Auth::user()->id)->dep_type==1 AND $next->employee_type==2)
			{
				$data['employees']=hrOperation::getRelatedAjirs($dep_id,$sub_dep);
			}
			else
			{
				$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
			}
			return View::make('hr.attendance.verifying',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public static function verifyNextEmployee_check($rfid=0,$year=0,$month=0,$type='next',$dep_id=0,$sub_dep=0)
	{
		if(canCheckImages())
		{
			//get next/prevous employee
			$next = hrOperation::getNextEmployeeRfid($rfid,$type);
			$next_rfid = $next->rfid;
			$data['photo'] =  $next;
			if($type=='next')
			{
				$data['next_rfid'] = $next;
			}
			else
			{
				$data['next_rfid'] = hrOperation::getNextEmployeeRfid($next_rfid,'next');
			}
			if($type=='previous')
			{
				$data['previous_rfid'] = $next;
			}
			else
			{
				$data['previous_rfid'] = hrOperation::getNextEmployeeRfid($next_rfid,'previous');
			}
			if($year == 0)
			{
				$year = date('Y');
			}
			if($month == 0)
			{
				$month = date('m');
			}
			$rfid_lost = lost_rfid($next->id);
			if($rfid_lost)
			{
				$rfids = array($next_rfid);
				$data['rfids'] = $rfid_lost;
				foreach($rfid_lost as $new_rfid)
				{
					$rfids[] =$new_rfid->rfid;
				}
				//dd($rfids);
				$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
				//$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
			}
			else
			{
				$data['details'] = hrOperation::getEmployeeImages($next_rfid,$year,$month);
				//$data['presentedDays'] = hrOperation::getEmployeePresented($next_rfid);
			}
			$data['rfid'] = $next_rfid;
			$data['year'] = $year;
			$data['month'] = $month;
			$data['dep'] = $dep_id;
			$data['sub_dep'] = $sub_dep;

			$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
			return View::make('hr.attendance.checking',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public static function approveNextEmployee($rfid=0,$year=0,$month=0,$type='next')
	{
		if(canView('hr_attendance'))
		{
			//get next/prevous employee
			$next_rfid = hrOperation::getNextEmployeeRfid($rfid,$type)->rfid;
			$data['next_rfid'] = hrOperation::getNextEmployeeRfid($next_rfid,'next');
			$data['previous_rfid'] = hrOperation::getNextEmployeeRfid($next_rfid,'previous');
			if($year == 0)
			{
				$year = date('Y');
			}
			if($month == 0)
			{
				$month = date('m');
			}
			$data['rfid'] = $next_rfid;
			$data['year'] = $year;
			$data['month'] = $month;
			$data['photo'] =  hrOperation::getPhotoByRfid($next_rfid);
			$data['details'] = hrOperation::absentEmployeeImages($next_rfid,$year,$month);
			return View::make('hr.attendance.approve_images',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public static function rejectImage()
	{
		if(canEdit('hr_attendance'))
		{
			$img_id = Input::get('img_id');
			$type = Input::get('type');
			$late = Input::get('late');
			if($late!='')
			{//it is late pic
				if(Input::get('late_status')!='Present')
				{//this late pic is not presented yet, make it present
					$data=array('late_status' => 0);//make it present
					$items = array(
						"RFID" 		=> Input::get('emp_id'),
						"date" 		=> Input::get('date'),
						"type"		=> $late,
						"created_at"=> date('Y-m-d H:i:s'),
						"created_by"=> Auth::user()->id
					);
					hrOperation::insertRecord('attendance_presents',$items);
				}
				else
				{//this late pic is presented, make it late again
					$data=array('late_status' => 1);//make it late again
					$items = array(
						"RFID" 		=> Input::get('emp_id'),
						"date" 		=> Input::get('date'),
						"type"		=> $late
					);
					hrOperation::delete_record('attendance_presents',$items);
				}
			}
			else
			{
				if($type=='')
				{//make it rejected
					$status = 1;
				}
				else
				{
					$status=0;
				}
				$data=array('status' => $status,'rejected_by'=>Auth::user()->id,'rejected_at'=>date('Y-m-d H:i:s'));
			}

			hrOperation::update_record('attendance_images',$data,array('id'=>$img_id));
		}
		else
		{
			return showWarning();
		}
	}
	public static function acceptImage()
	{
		if(canEdit('hr_attendance'))
		{
			$img_id = Input::get('img_id');
			$type = Input::get('type');
			if($type=='')
			{//the iamge is rejeted
				$status = 1;
			}
			else
			{
				$status=0;
			}
			$data=array('status' => $status,'rejected_by'=>Auth::user()->id,'rejected_at'=>date('Y-m-d H:i:s'));
			hrOperation::update_record('attendance_images',$data,array('id'=>$img_id));
		}
		else
		{
			return showWarning();
		}
	}
	public static function presentImage()
	{
		if(canEdit('hr_attendance') && Auth::user()->id == 174)
		{
			$date = Input::get('date');
			$type = Input::get('type');
			$present = Input::get('present');
			if($present!='Present')
			{//the iamge should be presented

				$items = array(
					"RFID" 		=> Input::get('emp_id'),
					"date" 		=> Input::get('date'),
					"type"		=> Input::get('type'),
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);
				hrOperation::insertRecord('attendance_presents',$items);
			}
			else
			{
				$items = array(
					"RFID" 		=> Input::get('emp_id'),
					"date" 		=> Input::get('date'),
					"type"		=> Input::get('type')
				);
				hrOperation::delete_record('attendance_presents',$items);
				hrOperation::insertLog('attendance_presents',2,'deletion of presented day of given employee',Input::get('emp_id'));
			}
		}

	}
	public function postLeaveDays()
	{
		if(canAttHoliday())
		{
			$items = array(
					"employee_id" 		=> Input::get('employee_id'),
					"paid_leaves" 		=> Input::get('paid'),
					"unpaid_leaves"		=> Input::get('unpaid'),
					"year"				=> Input::get('leave_year'),
					"month"				=> Input::get('leave_month'),
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);
			if(getEmployeeLeaveDays(Input::get('employee_id'),Input::get('leave_year'),Input::get('leave_month')))
			{
				hrOperation::delete_record('employee_leaves',array('employee_id'=>Input::get('employee_id'),'year'=>Input::get('leave_year'),'month'=>Input::get('leave_month')));
			}
			hrOperation::insertRecord('employee_leaves',$items);

			return \Redirect::route("getAttendance")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function postLeaves()
	{
		if(canAttHoliday())
		{
			//validate fields
			$validates = Validator::make(Input::all(), array(
				"from_date"						=> "required",
				"to_date"						=> "required",
				"type"							=> "required"
			));

			//check the validation
			if($validates->fails())
			{
				return Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->withErrors($validates)->withInput();
			}
			else
			{
				$sdate = Input::get("from_date");
				$edate = Input::get("to_date");

				if($sdate != '')
				{
					$sdate = explode("-", $sdate);
					if(count($sdate)!=3)
					{
						return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","تاریخ شروع غلط میباشد");
					}
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);
				}
				if($edate != '')
				{
					$edate = explode("-", $edate);
					if(count($edate)!=3)
					{
						return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","تاریخ ختم غلط میباشد");
					}
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);
				}
				if($sdate>$edate)
				{
					return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","تاریخ شروع نباید از تاریخ ختم بزرگتر باشد");
				}
				if(Input::get('type') == 7 || Input::get('type') == 8 || Input::get('type') == 9)
				{
					//if the type is khedmat, extra sick or other type then no issue to request leave
				}
				else
				{
					//check if before or after requested leave is off day
					$empDet = hrOperation::getEmployeeById(Input::get('employee_id'));
					$offdaycheck = checkLeaveOffday($sdate,$edate,$empDet->RFID,$empDet->id);
					if($offdaycheck==='offDayLeave')
					{//the user requested leave in a off day, reject the request
						return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","تاریخ انتخاب شده رخصتی میباشد!");
					}
					if($offdaycheck==='extraLeave')
					{//the day before the date, employee is in leave, he/she is not allowed to take leave
						return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","شما اجازه علاوه نمودن رخصتی بعد از رخصتی را ندارید!");
					}
					elseif($offdaycheck==='true')
					{//no issue to request for laeve

					}
					/*
					elseif($offdaycheck==='absent')
					{//the day before off day, employee is absent
						return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","بدلیل موجودیت روزهای رخصتی در بین غیرحاضری شما, اگر روزهای رخصت را هم فورم نیندازید, روزهای رخصت هم غیرحاضر محسوب خواهید شد!");
					}
					*/
				}
				$days = date_diff(date_create($sdate),date_create($edate));
				$noOfDays = (int)$days->format("%a")+1;
				$totalLeaves=hrOperation::getEmployeeLeaves_total(Input::get('type'),Input::get('employee_id'),$sy);
				if(Input::get('type')==2)
				{//tafrihi
					$kh_days = 0;
					$khedmat = employeeKhedmat_aop(Input::get('employee_id'));
					if($khedmat)
					{
						foreach($khedmat AS $kh)
						{
							$date1 = time();
							$date2 = time();
							if($kh->date_from!=null)
							{
								$date1=strtotime($kh->date_from);
							}
							if($kh->date_to!=null)
							{
								$date2=strtotime($kh->date_to);
							}
							$diff=$date2-$date1;
							$kh_days = $kh_days + floor($diff/(60*60*24));
						}
						if($kh_days>=330)
						{
							if($totalLeaves+$noOfDays>20)
							{//tafrihi
								$remined = 20 - $totalLeaves;
								return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail",'از رخصتی تفریحی '.$remined.' روز باقی مانده است');
							}
						}
						else
						{
							return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail",'شما مستحق رخصتی تفریحی نمیباشید');
						}
					}
					else
					{//no experience at AOP
						return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail",'شما مستحق رخصتی تفریحی نمیباشید');
					}
				}
				if($totalLeaves+$noOfDays > 10 && Input::get('type')==1)
				{
					return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","No more leaves for this type");
				}
				if($totalLeaves+$noOfDays > 20 && Input::get('type')==3)
				{
					return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","No more leaves for this type");
				}
				$items = array(
						"employee_id" 		=> Input::get('employee_id'),
						"type" 				=> Input::get('type'),
						"date_from"			=> $sdate,
						"date_to"			=> $edate,
						"days_no"			=> $noOfDays,
						"desc"				=> Input::get('desc'),
						"dir_approved"		=> 1,
						"hr_approved"		=> 1,
						"created_at" 		=> date('Y-m-d H:i:s'),
						"created_by" 		=> Auth::user()->id
					);

				$id = hrOperation::insertRecord_id('leaves',$items);
				if(Input::hasFile('scan'))
				{
					// getting all of the post data
					$file = Input::file('scan');
					$errors = "";
					$file_data = array();
					// validating each file.
					$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
					$validator = Validator::make(

					  		[
					            'scan' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension()),
					        ],
					        [
					            'scan' => 'required|max:100000',
					            'extension'  => 'required|in:jpg,jpeg,bmp,png'
					        ]
					);

					if($validator->passes())
					{
					    // path is root/uploads
					    $destinationPath = 'documents/hr_attachments';
					    $filename = $file->getClientOriginalName();

					    $temp = explode(".", $filename);
					    $extension = end($temp);

					    $filename = 'leave_'.Input::get('employee_id').'_'.$id.'.'.$extension;

					    $upload_success = $file->move($destinationPath, $filename);

					    if($upload_success)
					    {
							DB::connection('hr')->table('leaves')->where('id',$id)->update(array('file_name'=>$filename));
						}
						else
						{
						   $errors .= json('error', 400);
						}
					}
					else
					{
					    // redirect back with errors.
					    return Redirect::back()->withErrors($validator);
					}
				}
				hrOperation::insertLog('leaves',0,'leave of given employee',$id);
				return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("success","Record Saved Successfully.");
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function postLeaves_byUser()
	{
		if(canCheckAtt())
		{
			//validate fields
			$validates = Validator::make(Input::all(), array(
				"from_date"						=> "required",
				"to_date"						=> "required",
				"type"							=> "required"
			));

			//check the validation
			if($validates->fails())
			{
				return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
			}
			else
			{
				$sdate = Input::get("from_date");
				$edate = Input::get("to_date");
				if($sdate>$edate)
				{
					$validates->errors()->add('date', 'تاریخ شروع نباید از تاریخ ختم بزرگتر باشد');
					return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
				}
				if($sdate != '')
				{
					$sdate = explode("-", $sdate);
					if(count($sdate)!=3)
					{
						$validates->errors()->add('date', 'تاریخ شروع غلط میباشد');
						return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
					}
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);
				}
				if($edate != '')
				{
					$edate = explode("-", $edate);
					if(count($edate)!=3)
					{
						$validates->errors()->add('date', 'تاریخ ختم غلط میباشد');
						return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
					}
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);
				}
				if($sm!=$em)
				{
					$validates->errors()->add('date', 'تاریخ های شروع و ختم باید در عین ماه باشند');
					return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
				}
				//check if before or after requested leave is off day
				$empDet = hrOperation::getEmployeeByUserId(Auth::user()->id);
				$offdaycheck = checkLeaveOffday($sdate,$edate,$empDet->RFID,$empDet->id);
				if($offdaycheck==='offDayLeave')
				{//the user requested leave in a off day, reject the request
					$validates->errors()->add('date', 'تاریخ انتخاب شده رخصتی میباشد!');
					return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
				}
				if($offdaycheck==='extraLeave')
				{//the day before the date, employee is in leave, he/she is not allowed to take leave
					$validates->errors()->add('date', 'شما اجازه علاوه نمودن رخصتی بعد از رخصتی را ندارید!');
					return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
				}
				elseif($offdaycheck==='true')
				{//no issue to request for laeve

				}
				elseif($offdaycheck==='absent')
				{//the day before off day, employee is absent
					$validates->errors()->add('date', 'بدلیل موجودیت روزهای رخصتی در بین غیرحاضری شما, اگر روزهای رخصت را هم فورم نیندازید, روزهای رخصت هم غیرحاضر محسوب خواهید شد!');
					return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
				}
				$type = Input::get('type');
				$id = $empDet->id;
				$days = date_diff(date_create($sdate),date_create($edate));
				$noOfDays = (int)$days->format("%a")+1;
				$current_leaves = hrOperation::getEmployeeLeaves_total($type,$id,$sy);
				$total_leaves = $current_leaves+$noOfDays;

				if($type==2)
				{//tafrihi
					$kh_days = 0;
					$khedmat = employeeKhedmat_aop($id);
					if($khedmat)
					{
						foreach($khedmat AS $kh)
						{
							$date1 = time();
							$date2 = time();
							if($kh->date_from!=null)
							{
								$date1=strtotime($kh->date_from);
							}
							if($kh->date_to!=null)
							{
								$date2=strtotime($kh->date_to);
							}
							$diff=$date2-$date1;
							$kh_days = $kh_days + floor($diff/(60*60*24));
						}
						if($kh_days>=330)
						{
							if($total_leaves>20)
							{//tafrihi
								$remined = 20 - $current_leaves;
								$validates->errors()->add('date', 'از رخصتی تفریحی '.$remined.' روز باقی مانده است');
								return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
							}
						}
						else
						{
							$validates->errors()->add('date', 'شما مستحق رخصتی تفریحی نمیباشید');
							return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
						}
					}
					else
					{//no experience at AOP
						$validates->errors()->add('date', 'شما مستحق رخصتی تفریحی نمیباشید');
						return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
					}
				}
				//$date = dateToMiladi($year,01,01);//first of shamsi year
				if($total_leaves>10 && $type==1)
				{//zarori
					$remined = 10 - $current_leaves;
					$validates->errors()->add('date', 'از رخصتی ضروری '.$remined.' روز باقی مانده است');
					return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
				}
				elseif($total_leaves>20 && $type==3)
				{//sick
					$remined = 20 - $current_leaves;
					$validates->errors()->add('date', 'از رخصتی مریضی '.$remined.' روز باقی مانده است');
					return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
				}
				elseif($total_leaves>45 && $type==6)
				{//haj
					$remined = 45 - $current_leaves;
					$validates->errors()->add('date', 'از رخصتی حج '.$remined.' روز باقی مانده است');
					return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
				}
				elseif($total_leaves>90 && $type==4)
				{//pregnancy
					$remined = 90 - $current_leaves;
					$validates->errors()->add('date', 'از رخصتی ولادی '.$remined.' روز باقی مانده است');
					return Redirect::route("leaveForm_add")->withErrors($validates)->withInput();
				}
				//everything is allright
				$items = array(
						"employee_id" 		=> $id,
						"type" 				=> $type,
						"date_from"			=> $sdate,
						"date_to"			=> $edate,
						"days_no"			=> (int)$days->format("%a")+1,
						"dir_approved"		=> 0,
						"hr_approved"		=> 0,
						"created_at" 		=> date('Y-m-d H:i:s'),
						"created_by" 		=> Auth::user()->id
					);

				$inserted_rec = hrOperation::insertRecord_id('leaves',$items);

				hrOperation::insertLog('leaves',0,'employee added the leave',$inserted_rec);
				return \Redirect::route("leaveForm_add")->with("success","Record Saved Successfully.");
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function updateLeaves()
	{
		if(canAttHoliday())
		{
			//validate fields
			$validates = Validator::make(Input::all(), array(
				"from_date"						=> "required",
				"to_date"						=> "required"
			));

			//check the validation
			if($validates->fails())
			{
				return Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->withErrors($validates)->withInput();
			}
			else
			{
				$sdate = Input::get("from_date");
				$id = Input::get('id');
				if($sdate != '')
				{
					$sdate = explode("-", $sdate);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);
				}
				$edate = Input::get("to_date");

				if($edate != '')
				{
					$edate = explode("-", $edate);
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);
				}
				$days = date_diff(date_create($sdate),date_create($edate));
				$noDays = (int)$days->format("%a")+1;
				$currentDays = hrOperation::getLeaveDetails($id)->days_no;
				$noOfDays = $noDays - $currentDays;
				$currentAllDays = hrOperation::getEmployeeLeaves_total(Input::get('type'),Input::get('employee_id'),$sy);
				$totalLeaves = ($currentAllDays - $currentDays) + $noDays;
				if($totalLeaves > 10 && Input::get('type')==1)
				{
					return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","No more leaves for this type");
				}
				if($totalLeaves > 20 && Input::get('type')==2)
				{
					return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","No more leaves for this type");
				}
				if($totalLeaves > 20 && Input::get('type')==3)
				{
					return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","No more leaves for this type");
				}
				$items = array(
						"type" 				=> Input::get('type'),
						"date_from"			=> $sdate,
						"date_to"			=> $edate,
						"desc"				=> Input::get('desc'),
						"days_no"			=> (int)$days->format("%a")+1
					);

				hrOperation::update_record('leaves',$items,array('id'=>$id));
				if(Input::hasFile('scan'))
				{
					$file= public_path(). "/documents/hr_attachments/".Input::get('filename');
					File::delete($file);
					// getting all of the post data
					$file = Input::file('scan');
					$errors = "";
					$file_data = array();
					// validating each file.
					$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
					$validator = Validator::make(

					  		[
					            'scan' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension()),
					        ],
					        [
					            'scan' => 'required|max:100000',
					            'extension'  => 'required|in:jpg,jpeg,bmp,png'
					        ]
					);

					if($validator->passes())
					{
					    // path is root/uploads
					    $destinationPath = 'documents/hr_attachments';
					    $filename = $file->getClientOriginalName();

					    $temp = explode(".", $filename);
					    $extension = end($temp);

					    $filename = 'leave_'.Input::get('employee_id').'_'.$id.'.'.$extension;

					    $upload_success = $file->move($destinationPath, $filename);

					    if($upload_success)
					    {
							DB::connection('hr')->table('leaves')->where('id',$id)->update(array('file_name'=>$filename));
						}
						else
						{
						   $errors .= json('error', 400);
						}
					}
					else
					{
					    // redirect back with errors.
					    return Redirect::back()->withErrors($validator);
					}
				}
				hrOperation::insertLog('leaves',1,'leave of given employee',$id);
				return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("success","Record Saved Successfully.");
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function postAttendanceTime()
	{
		if(canEdit('hr_attendance'))
		{
			$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			hrOperation::delete_record('attendance_time',array('year'=>Input::get('time_year'),'month'=>Input::get('time_month'),'dep_type'=>$user_dep_type));
			$items = array(
					"time_in" 			=> Input::get('time_in'),
					"time_out" 			=> Input::get('time_out'),
					"year" 				=> Input::get('time_year'),
					"month" 			=> Input::get('time_month'),
					"thu"				=> 0,
					"dep_type"			=> $user_dep_type,
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);

			hrOperation::insertRecord('attendance_time',$items);
			$items_thu = array(
					"time_in" 			=> Input::get('time_in_thu'),
					"time_out" 			=> Input::get('time_out_thu'),
					"year" 				=> Input::get('time_year'),
					"month" 			=> Input::get('time_month'),
					"thu"				=> 1,
					"dep_type"			=> $user_dep_type,
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);

			hrOperation::insertRecord('attendance_time',$items_thu);
			hrOperation::insertLog('attendance_time',1,'attendance time updated',1);
		}
		else
		{
			return showWarning();
		}
	}
	public function postHolidays()
	{
		if(canAttHoliday())
		{
			$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			hrOperation::delete_record('month_holidays',array('year'=>Input::get('leave_year'),'month'=>Input::get('leave_month'),'dep_type'=>$user_dep_type));
			hrOperation::delete_holidays(Input::get('leave_year').Input::get('leave_month'));
			if(Input::get('h_number')>0)
			{
				$items = array(
						"days" 				=> Input::get('h_number'),
						"year" 				=> Input::get('leave_year'),
						"month" 			=> Input::get('leave_month'),
						"dep_type" 			=> $user_dep_type,
						"created_at" 		=> date('Y-m-d H:i:s'),
						"created_by" 		=> Auth::user()->id
					);

				$id = hrOperation::insertRecord_id('month_holidays',$items);
				$dates = array();
				for($i=1;$i<=Input::get('holiday_number');$i++)
				{
					if(Input::get('date_'.$i)!='')
					{
						$date = explode('-',Input::get('date_'.$i));
						$date = dateToMiladi($date[2],$date[1],$date[0]);
						$dates[] = array(
							"month_holidays_id" => $id,
							"code" 				=> Input::get('leave_year').Input::get('leave_month'),
							"date" 				=> $date,
							"desc" 				=> Input::get('desc_'.$i),
							"dep_type" 			=> $user_dep_type
						);
					}
				}

				hrOperation::insertRecord('month_holiday_dates',$dates);
				hrOperation::insertLog('month_holidays',1,'month holidays date updated',1);
			}
			return Redirect::route('holidaysManager');
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmergencyHolidays()
	{
		if(canEdit('hr_attendance'))
		{
			$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			hrOperation::delete_record('emergency_days',array('year'=>Input::get('emergency_year'),'month'=>Input::get('emergency_month'),'dep_type'=>$user_dep_type));
			hrOperation::delete_EmergencyHolidays(Input::get('emergency_year').Input::get('emergency_month'));
			if(Input::get('e_number')>0)
			{
				$items = array(
						"days" 				=> Input::get('e_number'),
						"year" 				=> Input::get('emergency_year'),
						"month" 			=> Input::get('emergency_month'),
						"dep_type" 			=> $user_dep_type,
						"created_at" 		=> date('Y-m-d H:i:s'),
						"created_by" 		=> Auth::user()->id
					);

				$id = hrOperation::insertRecord_id('emergency_days',$items);
				$dates = array();
				for($i=1;$i<=Input::get('emergency_number');$i++)
				{
					if(Input::get('edate_'.$i)!='')
					{
						$date = explode('-',Input::get('edate_'.$i));
						$date = dateToMiladi($date[2],$date[1],$date[0]);
						$dates[] = array(
							"emergency_id" => $id,
							"code" 				=> Input::get('emergency_year').Input::get('emergency_month'),
							"date" 				=> $date,
							"desc" 				=> Input::get('edesc_'.$i),
							"dep_type" 			=> $user_dep_type
						);
					}
				}

				hrOperation::insertRecord('month_holiday_dates',$dates);
				hrOperation::insertLog('emergency_days',1,'inserted',1);
			}
			return Redirect::route('holidaysManager');
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeSalaryWait()
	{
		if(canEdit('hr_attendance'))
		{
			hrOperation::delete_record('employee_salary_waits',array('employee_id'=>Input::get('employee_id'),'year'=>Input::get('year'),'month'=>Input::get('month')));
			$items = array(
					"employee_id" 		=> Input::get('employee_id'),
					"year" 				=> Input::get('year'),
					"month" 			=> Input::get('month'),
					"desc" 				=> Input::get('desc'),
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);

			hrOperation::insertRecord('employee_salary_waits',$items);
			hrOperation::insertLog('employee_salary_waits',1,'salary wait of given employee',Input::get('employee_id'));
		}
		else
		{
			return showWarning();
		}
	}

	public function insertImages()
	{
		//return json_encode(array('msg'=>'Oops...something went wrong!!!','status'=>0));
		if(Auth::user()->id == 113 || Auth::user()->id == 174 || Auth::user()->id == 715 || Auth::user()->id == 201 || Auth::user()->id == 1286 || Auth::user()->id == 450 || Auth::user()->id == 241 || Auth::user()->id == 2038)
		{//ali,fahim,jawadi,farkhonda,samimullah,ahmadshekib,zahidullah,ali-ocs
			$insert_status = hrOperation::checkAttImageInsertStatus();
			if(count($insert_status)>0)
			{
				return json_encode(array('msg'=>'This process is already in progress. please wait...','status'=>0));
			}
			$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			$the_date = explode('-',Input::get('the_date'));
			$shamsi_date = Input::get('the_date');
			$the_date = dateToMiladi($the_date[2],$the_date[1],$the_date[0]);
			$insert_id = hrOperation::insertRecord_id('att_images_insert_log',array('user_id'=>Auth::user()->id,
																															'att_date'=>$the_date,
																															'dep_type'=>$user_dep_type,
																															'status'=>0,
																															'created_at'=>date('Y-m-d H:i:s')
																														));
			if($the_date>=date('Y-m-d'))
			{
				hrOperation::update_record('att_images_insert_log',array('status'=>2),array('id'=>$insert_id));
				return json_encode(array('msg'=>'The date should be smaller than today','status'=>0));
			}
			if(!hrOperation::checkInsertImagesDate($the_date))
			{
				hrOperation::update_record('att_images_insert_log',array('status'=>2),array('id'=>$insert_id));
				return json_encode(array('msg'=>'The Images are already inserted for this day','status'=>0));
			}

			$m_date = explode('-',$the_date);
			$year = $m_date[0];
			$month= $m_date[1];
			$day  = $m_date[2];
			//dd('not inserted');
			$result = "";
			// create a new cURL resource
			$ch = curl_init();
			// set URL and other appropriate options
			curl_setopt($ch, CURLOPT_URL, "10.10.0.201/images.php?year=$year&month=$month&day=$day");
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// grab URL and pass it to the browser
			$result = curl_exec($ch);//dd($result);
			$result = explode("\n",$result);
			$result = array_reverse($result);
			// close cURL resource, and free up system resources
			curl_close($ch);
			$first_data = array();
			$data = array();
			//$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			if($user_dep_type==2)
			{//aop

			
                /** 
                 * Deveices Mac Address  
                 * OCN6183060799 GMAK, RSANA HAY HOKOMAT 
                 * AF4C180460341 Transport   
                 * AF4C180460343 KHADAMAT 
                 */                
			
				$machines = array("AF4C180460348","AF4C180460343","AF4C180460344","AF4C180460345"
                    ,"AF4C180460349","AF4C180460350","AF4C180460347",
					"OCN6183060800","OCN6183060805","OCN6183060804","OCN6183060796","OCN6183060823","OCN6183060798","OCN6183060802",
					"OCN6183060803","OCN6183060799","AF4C180460341"
                );

				for($i=0;$i<count($result);$i++)
				{
					$pos = strpos($result[$i], '_');
					if ($pos !== false)
					{//check if it has rfid
						$path = explode("\\",$result[$i]);
						if(count($path)==5)
						{//it should have 4 part:\machine\year\monthDay\time_rfid.jpg
							$machine = $path[1];
							if (in_array($machine, $machines))
							{
								$time_rfid = explode('_',$path[4]);
								$rfid = $time_rfid[1];
								$rfid = explode('.',$rfid);
								$rfid = $rfid[0];
								$path_replace = str_replace('\\', '/', $result[$i]);
								/*
								if($time_rfid[0]<120000)
								{
									if(!in_array(array('rfid'=>$rfid,'day'=>$path[3],'timeout'=>0),$first_data))
									{
										$first_data[]=array('rfid'=>$rfid,'day'=>$path[3],'timeout'=>0);
										$data[] = array('RFID'=>$rfid,'path'=>$path_replace);
									}
								}
								else
								{
									if(!in_array(array('rfid'=>$rfid,'day'=>$path[3],'timein'=>0),$first_data))
									{
										$first_data[]=array('rfid'=>$rfid,'day'=>$path[3],'timein'=>0);
										$data[] = array('RFID'=>$rfid,'path'=>$path_replace);
									}
								}
								*/
								$emp_dep_type = getUserDepTypeByRFID($rfid);
								if($emp_dep_type==$user_dep_type)
								{

									 $data[] = array(
													'RFID'=>$rfid,
													'path'=>$path_replace,
													'time'=>$time_rfid[0],
													'year'=> $year,
													'month'=> $month,
													'day' => $day,
													'date'=> $the_date,
													'dep_type'=>$user_dep_type
                                                );
								}
							}
						}
					}
				}
				//DB::connection('hr')->table('attendance_images_temp')->truncate();
				if(hrOperation::insertAttImages($data))
				{
					// DB::statement("DELETE t
					//     FROM hr.attendance_images AS t LEFT JOIN
					//          (SELECT RFID, max( t1.time ) AS time
					//           FROM hr.attendance_images AS t1
					//           WHERE t1.year = $year AND t1.month = $month AND t1.day = $day AND t1.dep_type = $user_dep_type AND t1.time < 120000
					//           GROUP BY t1.RFID
					//          ) keep
					//          ON t.time = keep.time AND t.RFID = keep.RFID
					// WHERE keep.time IS NULL AND
					//       t.year = $year AND t.month = $month AND t.day = $day AND t.dep_type = $user_dep_type AND t.time < 120000;");

					//       DB::statement("DELETE t
					//     FROM hr.attendance_images AS t LEFT JOIN
					//          (SELECT RFID, max( t1.time ) AS time
					//           FROM hr.attendance_images AS t1
					//           WHERE t1.year = $year AND t1.month = $month AND t1.day = $day AND t1.dep_type = $user_dep_type AND t1.time > 120000
					//           GROUP BY t1.RFID
					//          ) keep
					//          ON t.time = keep.time AND t.RFID = keep.RFID
					// WHERE keep.time IS NULL AND
					//       t.year = $year AND t.month = $month AND t.day = $day AND t.dep_type = $user_dep_type AND t.time > 120000;");

                    DB::statement("DELETE t
					    FROM hr.attendance_images AS t LEFT JOIN
					         (SELECT RFID, max( t1.time ) AS time
					          FROM hr.attendance_images AS t1
					          WHERE t1.year = $year AND t1.month = $month AND t1.day = $day AND t1.dep_type = $user_dep_type AND t1.time < 120000
					          GROUP BY t1.RFID
					         ) keep
					         ON t.time = keep.time AND t.RFID = keep.RFID
					         WHERE keep.time IS NULL AND t.RFID NOT IN (SELECT RFID FROM hr.night_shifts) AND
					         t.year = $year AND t.month = $month AND t.day = $day AND t.dep_type = $user_dep_type AND t.time < 120000;");

					DB::statement("DELETE t
					    FROM hr.attendance_images AS t LEFT JOIN
					         (SELECT RFID, max( t1.time ) AS time
					          FROM hr.attendance_images AS t1
					          WHERE t1.year = $year AND t1.month = $month AND t1.day = $day AND t1.dep_type = $user_dep_type AND t1.time > 120000
					          GROUP BY t1.RFID
					         ) keep
					    ON t.time = keep.time AND t.RFID = keep.RFID
					    WHERE keep.time IS NULL AND t.RFID NOT IN (SELECT RFID FROM hr.night_shifts) AND
					    t.year = $year AND t.month = $month AND t.day = $day AND t.dep_type = $user_dep_type AND t.time > 120000;");

					//night shifts
					DB::statement("DELETE t
					    FROM hr.attendance_images AS t LEFT JOIN
					         (SELECT RFID, min( t1.time ) AS time,max(t1.time) AS max_time
					          FROM hr.attendance_images AS t1
					          WHERE t1.year = $year AND t1.month = $month AND t1.day = $day AND t1.dep_type = $user_dep_type
					          GROUP BY t1.RFID
					         ) keep
					    ON t.time = keep.time AND t.RFID = keep.RFID
					    WHERE keep.time IS NULL AND t.RFID IN (SELECT RFID FROM hr.night_shifts) AND t.time < keep.max_time AND t.year = $year AND t.month = $month AND t.day = $day AND t.dep_type = $user_dep_type;");

					hrOperation::insertLog('attendance_images',1,'insert att images',0);
					$msg= '<span style="color:red">عکسهای تاریخ '.Input::get("the_date").' موفقانه ذخیره گردید</span>';
					hrOperation::update_record('att_images_insert_log',array('status'=>1),array('id'=>$insert_id));
					return json_encode(array('msg'=>$msg,'status'=>1));

				}
				else
				{
					hrOperation::update_record('att_images_insert_log',array('status'=>2),array('id'=>$insert_id));
					return json_encode(array('msg'=>'Woops! There was some problems! Images are Not Inserted!!!','status'=>0));
				}
			}
			else
            {//ocs

                /** 
                 * @Devices 
                 * 3929181560568 Shash Darak 
                 * 
                 */       

                // return json_encode(array('msg'=>'Oops...something went wrong!!!','status'=>0));
				$machines = array("AF4C175260124","AF4C175260116","AF1U173660010","2785872450003",
                				  "3929181560532","AF4C175060083","3929181560624","3929181560529","3929181560544","3929181560551","3929181560568");

				for($i=0;$i<count($result);$i++)
				{
					$pos = strpos($result[$i], '_');
					if ($pos !== false)
					{//check if it has rfid
						$path = explode("\\",$result[$i]);
						if(count($path)==5)
						{//it should have 4 part:\machine\year\monthDay\time_rfid.jpg
							$machine = $path[1];
							if (in_array($machine, $machines))
							{
								$time_rfid = explode('_',$path[4]);
								$rfid = $time_rfid[1];
								$rfid = explode('.',$rfid);
								$rfid = $rfid[0];
								$path_replace = str_replace('\\', '/', $result[$i]);
								/*
								if($time_rfid[0]<120000)
								{
									if(!in_array(array('rfid'=>$rfid,'day'=>$path[3],'timeout'=>0),$first_data))
									{
										$first_data[]=array('rfid'=>$rfid,'day'=>$path[3],'timeout'=>0);
										$data[] = array('RFID'=>$rfid,'path'=>$path_replace);
									}
								}
								else
								{
									if(!in_array(array('rfid'=>$rfid,'day'=>$path[3],'timein'=>0),$first_data))
									{
										$first_data[]=array('rfid'=>$rfid,'day'=>$path[3],'timein'=>0);
										$data[] = array('RFID'=>$rfid,'path'=>$path_replace);
									}
								}
								*/
                                $emp_dep_type = getUserDepTypeByRFID($rfid);
								if($emp_dep_type==$user_dep_type)
								{

									$data[] = array(
												'RFID'=>$rfid,
												'path'=>$path_replace,
												'time'=>$time_rfid[0],
												'year'=> $year,
												'month'=> $month,
												'day' => $day,
												'date'=> $the_date,
												'dep_type'=>$user_dep_type
                                            );

								}
							}
						}
					}
                }
				//DB::connection('hr')->table('attendance_images_temp')->truncate();
				if(hrOperation::insertAttImages($data))
				{

                    DB::statement("DELETE t
					    FROM hr.attendance_images AS t LEFT JOIN
					         (SELECT RFID, max( t1.time ) AS time
					          FROM hr.attendance_images AS t1
					          WHERE t1.year = $year AND t1.month = $month AND t1.day = $day AND t1.dep_type = $user_dep_type AND t1.time < 120000
					          GROUP BY t1.RFID
					         ) keep
					         ON t.time = keep.time AND t.RFID = keep.RFID
					WHERE keep.time IS NULL AND t.RFID NOT IN (SELECT RFID FROM hr.night_shifts) AND
					      t.year = $year AND t.month = $month AND t.day = $day AND t.dep_type = $user_dep_type AND t.time < 120000;");

					      DB::statement("DELETE t
					    FROM hr.attendance_images AS t LEFT JOIN
					         (SELECT RFID, max( t1.time ) AS time
					          FROM hr.attendance_images AS t1
					          WHERE t1.year = $year AND t1.month = $month AND t1.day = $day AND t1.dep_type = $user_dep_type AND t1.time > 120000
					          GROUP BY t1.RFID
					         ) keep
					         ON t.time = keep.time AND t.RFID = keep.RFID
					WHERE keep.time IS NULL AND t.RFID NOT IN (SELECT RFID FROM hr.night_shifts) AND
					      t.year = $year AND t.month = $month AND t.day = $day AND t.dep_type = $user_dep_type AND t.time > 120000;");

					//night shifts
					DB::statement("DELETE t
					    FROM hr.attendance_images AS t LEFT JOIN
					         (SELECT RFID, min( t1.time ) AS time,max(t1.time) AS max_time
					          FROM hr.attendance_images AS t1
					          WHERE t1.year = $year AND t1.month = $month AND t1.day = $day AND t1.dep_type = $user_dep_type
					          GROUP BY t1.RFID
					         ) keep
					         ON t.time = keep.time AND t.RFID = keep.RFID

					WHERE keep.time IS NULL AND t.RFID IN (SELECT RFID FROM hr.night_shifts) AND t.time < keep.max_time AND t.year = $year AND t.month = $month AND t.day = $day AND t.dep_type = $user_dep_type;");

					hrOperation::insertLog('attendance_images',1,'insert att images',0);
					$msg= '<span style="color:red">عکسهای تاریخ '.Input::get("the_date").' موفقانه ذخیره گردید</span>';
					hrOperation::update_record('att_images_insert_log',array('status'=>1),array('id'=>$insert_id));
					return json_encode(array('msg'=>$msg,'status'=>1));

				}
				else
				{
					hrOperation::update_record('att_images_insert_log',array('status'=>2),array('id'=>$insert_id));
					return json_encode(array('msg'=>'Woops! There was some problems! Images are Not Inserted!!!','status'=>0));
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	//tashkils
	public function getTashkils($year=0)
	{
		if($year==0)
		{//if no year is send, then get the current shamsi year
			$sh_date = dateToShamsi(date('Y'),date('m'),date('d'));
			$year = explode('-', $sh_date);
			$year = $year[0];
		}
		//check roles
		if(canView('hr_tashkil'))
		{
			$table = Datatable::table()
		  	->addColumn('id',
					'bast',
					'tainat',
					'title',
					'sub_dep',
					'department',
					'year',
					'actions')
		  	->setUrl(route('getTashkilData',$year))
		  	/*
		  	->setCallbacks(
			'fnRowCallback', 'function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

			        var numStart = this.fnPagingInfo().iStart;

			        var index = numStart + iDisplayIndexFull + 1;
			        $("td:first", nRow).html(index);
			        return nRow;
			    }'
			)
			*/
		  	->noScript();
			//View::make('admin.users', array('table' => $table));
			return View::make('hr.tashkil.list',array('table' => $table,'year'=>$year));
		}
		else
		{
			return showWarning();
		}
	}
	public function getTashkilData($year=0)
	{
		//get all data
		$object = hrOperation::getTashkilData($year);//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
				->showColumns(
							'id',
							'bast',
							'tainat',
							'title',
							'sub_dep',
							'department',
							'year'
				)
				->addColumn('operations', function($option){
					$options = '';
					if(canEdit('hr_tashkil'))
					{
						$options.='
							<a href="javascript:void()" onclick="load_tashkil_det('.$option->id.')" class="table-link" style="text-decoration:none;" data-target="#update_tashkil" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
					}
					if(canDelete('hr_tashkil'))
					{
						$options.='<a href="javascript:void()" onclick="deleteTashkil('.$option->id.')" class="table-link" style="text-decoration:none;">
										<i class="icon fa-trash-o" style="color:red;" aria-hidden="true" style="font-size: 16px;"></i>
									</a>';

					}
					return $options;
				})
				->searchColumns(
							'id',
							'bast',
							'tainat',
							'title',
							'sub_dep',
							'department',
							'year'
				)
				// ->orderColumns('id','id')
				->make();
	}

	public function checkTashkilDuplicate()
	{
		$dep_id = Input::get('general_department');
		$sub_dep_id = Input::get('sub_dep');
		$tainat = Input::get('tainat');
		if(hrOperation::check_tashkil($dep_id,$sub_dep_id,$tainat))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function postNewBast()
	{
		if(canAdd('hr_tashkil'))
		{
			//validate fields
			$validates = Validator::make(Input::all(), array(
				"title"						=> "required",
				"general_department"		=> "required",
				"sub_dep"					=> "required",
				"tainat"					=> "required"
			));
			/*
			$validates->after(function($validates)
			{
			    if ($this->checkTashkilDuplicate())
			    {
			        $validates->errors()->add('field', 'duplicate entry');
			    }
			});
			*/
			//check the validation
			if($validates->fails())
			{
				return Redirect::route('getTashkils')->with("fail","Duplicate Entry!!!");
			}
			else
			{
				$filename='';
				if(Input::hasFile('scan'))
				{
					// getting all of the post data
					$file = Input::file('scan');
					$errors = "";
					$file_data = array();
					// validating each file.
					$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
					$validator = Validator::make(

					  		[
					            'scan' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension()),
					        ],
					        [
					            'scan' => 'required|max:100000',
					            'extension'  => 'required|in:jpg,jpeg,bmp,png'
					        ]
					);

					if($validator->passes())
					{
					    // path is root/uploads
					    $destinationPath = 'documents/hr_attachments';
					    $filename = $file->getClientOriginalName();

					    $temp = explode(".", $filename);
					    $extension = end($temp);

					    $filename = 'job_description_'.$filename.'.'.$extension;

					    $upload_success = $file->move($destinationPath, $filename);
					}
				}
				if(Input::get('employee_type')==2)
				{
					$bast = Input::get('ajir_bast');
				}
				elseif(Input::get('employee_type')==3)
				{
					$bast = Input::get('military_bast');
				}
				else
				{
					$bast = Input::get('emp_bast');
				}

				$data=array();
				$tainats = explode(',',Input::get('tainat'));
				$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
				for($i=0;$i<Input::get('bast_number');$i++)
				{
					$data[] = array(
						"dep_id" 				=> Input::get('general_department'),
						"sub_dep_id"			=> Input::get('sub_dep'),
						"bast"					=> $bast,
						"title"					=> Input::get('title'),
						"employee_type"			=> Input::get('employee_type'),
						"year"					=> Input::get('year'),
						"tainat"				=> $tainats[$i],
						"job_desc"				=> $filename,
						"dep_type"				=> $user_dep_type,
						"created_at" 			=> date('Y-m-d H:i:s'),
						"created_by" 			=> Auth::user()->id
					);
				}
				hrOperation::insertRecord('tashkilat',$data);

				return \Redirect::route("getTashkils")->with("success","Record Saved Successfully.");
			}
		}
	}
	public function updateBast()
	{
		if(canAdd('hr_tashkil'))
		{
			//validate fields
			$validates = Validator::make(Input::all(), array(
				"title"						=> "required",
				"general_department"		=> "required",
				"sub_dep"					=> "required",
				"tainat"					=> "required"
			));

			//check the validation
			if($validates->fails())
			{
				return Redirect::route('getTashkils')->with("fail","Duplicate Entry!!!");
			}
			else
			{
				$filename=Input::get('scan_file');
				if(Input::hasFile('scan'))
				{
					$file= public_path(). "/documents/hr_attachments/".Input::get('scan_file');
					File::delete($file);
					// getting all of the post data
					$file = Input::file('scan');
					$errors = "";
					$file_data = array();
					// validating each file.
					$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
					$validator = Validator::make(

					  		[
					            'scan' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension()),
					        ],
					        [
					            'scan' => 'required|max:100000',
					            'extension'  => 'required|in:jpg,jpeg,bmp,png'
					        ]
					);

					if($validator->passes())
					{

					    // path is root/uploads
					    $destinationPath = 'documents/hr_attachments';
					    $filename = $file->getClientOriginalName();

					    $temp = explode(".", $filename);
					    $extension = end($temp);

					    $filename = 'job_description_'.Input::get('bast_id').'_'.$filename.'.'.$extension;

					    $upload_success = $file->move($destinationPath, $filename);

					}

				}
				if(Input::get('employee_type')==2)
				{
					$bast = Input::get('ajir_bast');
				}
				elseif(Input::get('employee_type')==3)
				{
					$bast = Input::get('military_bast');
				}
				else
				{
					$bast = Input::get('emp_bast');
				}


				$data = array(
					"dep_id" 				=> Input::get('general_department'),
					"sub_dep_id"			=> Input::get('sub_dep'),
					"bast"					=> $bast,
					"title"					=> Input::get('title'),
					"employee_type"			=> Input::get('employee_type'),
					"tainat"				=> Input::get('tainat'),
					"year"					=> Input::get('year'),
					"job_desc"				=> $filename,
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);

				hrOperation::update_record('tashkilat',$data,array('id'=>Input::get('bast_id')));
				hrOperation::insertLog('tashkilat',1,'tashkil updated',Input::get('bast_id'));
				//return \Redirect::route("getTashkils")->with("success","Record Saved Successfully.");
			}
		}
	}
	public static function deleteTashkil()
	{
		if(canDelete('hr_tashkil'))
		{
			$id = Input::get('id');
			if(hrOperation::isTashkilInUse($id))
			{
				return;
			}
			else
			{
				hrOperation::delete_record('tashkilat',array('id'=>$id));
				hrOperation::insertLog('tashkilat',2,'tashkil deleted',$id);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public static function deleteEmployeePromotion()
	{
		if(canDelete('hr_documents'))
		{
			$id = Input::get('id');
			hrOperation::delete_record('employee_promotions',array('id'=>$id));
			hrOperation::insertLog('employee_promotions',2,'promotion of given employee',$id);
		}
		else
		{
			return showWarning();
		}
	}
	public static function deleteEmployeeMakafat()
	{
		if(canDelete('hr_documents'))
		{
			$id = Input::get('id');
			hrOperation::delete_record('employee_makafats',array('id'=>$id));
			hrOperation::insertLog('employee_makafats',2,'makafat of given employee',$id);
		}
		else
		{
			return showWarning();
		}
	}
	public static function deleteEmployeePunish()
	{
		if(canDelete('hr_documents'))
		{
			$id = Input::get('id');
			hrOperation::delete_record('employee_punishments',array('id'=>$id));
			hrOperation::insertLog('employee_punishments',2,'punishment of given employee',$id);
		}
		else
		{
			return showWarning();
		}
	}
	public static function deleteEmployeeLeave()
	{
		if(canAttHoliday())
		{
			$id = Input::get('id');
			hrOperation::delete_record('leaves',array('id'=>$id));
			hrOperation::insertLog('leaves',2,'leave of given employee',$id);
		}
		else
		{
			return showWarning();
		}
	}
	public static function bringTashkilDetViaAjax()
	{
		$id = Input::get('id');
		$tashkil = hrOperation::getTashkilDetail($id);
		return json_encode(array('bast'=>$tashkil->bast_name,'number'=>$tashkil->tainat));
	}
	public function getCapacityBuilding()
	{
		//check roles
		if(canView('hr_capacity'))
		{
			$data['trainings'] = hrOperation::getCapacityTrainingsData();

			return View::make('hr.capacity.employeeList',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//get form data list
	public function getEmployeeCapacityData()
	{
		//get all data
		$object = hrOperation::getTrainingEmployeeData();//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name',
							'number_tayenat',
							'gender',
							'original_province',
							'bast',
							'rank',
							'emp_date'
							)
				->addColumn('education', function($option){
					$education = getEmployeeEducation($option->id);
					if($education)
					{
						return getEmployeeEducation($option->id)->name_dr;
					}
					else
					{
						return '';
					}
				})
				->addColumn('operations', function($option){
					$options = '';
					if(canView('hr_capacity'))
					{
						$options .= '
								<a href="'.route('getDetailsEmployee',Crypt::encrypt($option->id)).'" class="table-link" style="text-decoration:none;" title="نمایش" target="_blank">
									<i class="icon fa-eye" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
					}
					if(canAdd('hr_capacity'))
					{
						$options .= '
								<a href="javascript:void()" onclick="load_employee_trainings('.$option->id.')" class="table-link" style="text-decoration:none;" title="trainings" data-target="#employee_trainings" data-toggle="modal">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
								<a href="javascript:void()" onclick="$('.'\'#employee_id\''.').val('.$option->id.');" class="table-link" style="text-decoration:none;" title="Add training" data-target="#add_training" data-toggle="modal">
									<i class="icon fa-plus" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
								';
					}

					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public function getNewEmployeeCapacityData()
	{
		//get all data
		$object = hrOperation::getTrainingNewEmployeeData();//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name',
							'number_tayenat',
							'gender',
							'original_province',
							'bast',
							'rank',
							'emp_date'
							)
				->addColumn('education', function($option){
					$education = getEmployeeEducation($option->id);
					if($education)
					{
						return getEmployeeEducation($option->id)->name_dr;
					}
					else
					{
						return '';
					}
				})
				->addColumn('operations', function($option){
					$options = '';
					if(canView('hr_capacity'))
					{
						$options .= '
								<a href="'.route('getDetailsEmployee',Crypt::encrypt($option->id)).'" class="table-link" style="text-decoration:none;" title="نمایش" target="_blank">
									<i class="icon fa-eye" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
					}
					if(canAdd('hr_capacity'))
					{
						$options .= '
								<a href="javascript:void()" onclick="load_employee_trainings('.$option->id.')" class="table-link" style="text-decoration:none;" title="trainings" data-target="#newemployee_trainings" data-toggle="modal">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
								<a href="javascript:void()" onclick="$('.'\'#employee_id\''.').val('.$option->id.');" class="table-link" style="text-decoration:none;" title="Add training" data-target="#newadd_training" data-toggle="modal">
									<i class="icon fa-plus" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
								<a href="javascript:void()" onclick="employee_oriented('.$option->id.')" class="table-link" style="text-decoration:none;" title="Oriented">
									<i class="icon fa-check-square-o" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
								';
					}

					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public static function employee_oriented()
	{
		if(canAdd('hr_capacity'))
		{
			$id = Input::get('id');
			hrOperation::update_record('employees',array('training_oriented'=>1),array('id'=>$id));
		}
		else
		{
			return showWarning();
		}
	}
	public static function check_training_validity()
	{
		if(canAdd('hr_capacity'))
		{
			$id = Input::get('id');
			if(hrOperation::check_training_validity($id))
			{
				return 'true';
			}
			else
			{
				return 'false';
			}
		}
		else
		{
			return showWarning();
		}
	}
	public static function loadCapacityTrainings()
	{
		if(canView('hr_capacity'))
		{
			$type = Input::get('type');
			if($type == 'trainings')
			{//trainings
				return View('hr.capacity.trainingsList');
			}
			elseif($type == 'trainers')
			{//teachers
				return View('hr.capacity.trainersList');
			}
			else
			{
				$data['trainings'] = hrOperation::getCapacityTrainingsData();
				return View('hr.capacity.capacityNewEmoList',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getCapacityTrainingsData()
	{
		//get all data
		$object = hrOperation::getCapacityTrainingsData();//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'title',
							'location',
							'type',
							'seat_no',
							'start_date',
							'end_date'
							)
				->addColumn('operations', function($option){
					$options = '';
					$options .= '
								<a href="javascript:void()" onclick="load_training('.$option->id.')" class="table-link" style="text-decoration:none;" title="Edit" data-target="#edit_training" data-toggle="modal">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
								<a href="javascript:void()" onclick="load_training_emps('.$option->id.')" class="table-link" style="text-decoration:none;" title="employees" data-target="#training_employees" data-toggle="modal">
									<i class="icon fa-user" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';


					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public function printTrainings()
	{
		$data['employees'] = hrOperation::getAllTrainingsEmployees();

		Excel::create('trainings', function($file) use($data){

			$file->setTitle('trainings');

			$file->sheet('trainings', function($sheet) use($data){
				$sheet->setRightToLeft(true);
				$sheet->setOrientation('landscape');

				$sheet->mergeCells('A1:L1');
				$sheet->setCellValue('A1','لیست اشتراک کننده گان برنامه های آموزشی');
				$sheet->setCellValue('A2','#');
				$sheet->setCellValue('B2','اسم و تخلص');
				$sheet->setCellValue('C2','ریاست مربوطه');
				$sheet->setCellValue('D2','داخلی / خارجی');
				$sheet->setCellValue('E2','سال');
				$sheet->setCellValue('F2','تارخی شروع');
				$sheet->setCellValue('G2','تارخی ختم');
				$sheet->setCellValue('H2','نام برنامه آموزشی');
				$sheet->setCellValue('I2','محل برگزاری');
				$sheet->setCellValue('J2','تمویل کننده');
				$sheet->setCellValue('K2','شماره حکم');
				$sheet->setCellValue('L2','تارخی حکم');
				$sheet->row(1, function($row) {
				    // call cell manipulation methods
				    $row->setBackground('#e0ebeb');
					$row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$sheet->row(2, function($row) {
					$row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$row = 3;$day_counter = 1;
				foreach($data['employees'] AS $rows)
				{
					$type = 'خارجی';$sdate=$rows->start_date;$edate=$rows->end_date;$year='';
					if($rows->start_date!='')
					{
						$s_date = explode('-', $rows->start_date);
						$year = $s_date[0];
					}
					if($rows->type==0)
					{
						$type = 'داخلی';
						if($rows->start_date!='')
						{
							$sdate=dateToShamsi($s_date[0],$s_date[1],$s_date[2]);
							$year = explode('-', $sdate);
							$year = $year[0];
						}
						if($rows->end_date!='')
						{
							$edate = explode('-', $rows->end_date);
							$edate=dateToShamsi($edate[0],$edate[1],$edate[2]);
						}
					}
					$sheet->setCellValue('A'.$row.'',$day_counter);
					$sheet->setCellValue('B'.$row.'',$rows->name_dr.' '.$rows->last_name);
					$sheet->setCellValue('C'.$row.'',$rows->dep_name);
					$sheet->setCellValue('D'.$row.'',$type);
					$sheet->setCellValue('E'.$row.'',$year);
					$sheet->setCellValue('F'.$row.'',$sdate);
					$sheet->setCellValue('G'.$row.'',$edate);
					$sheet->setCellValue('H'.$row.'',$rows->title);
					$sheet->setCellValue('I'.$row.'',$rows->location);
					$sheet->setCellValue('J'.$row.'',$rows->sponser);
					$sheet->setCellValue('K'.$row.'',$rows->hokm_no);
					$sheet->setCellValue('L'.$row.'',$rows->hokm_date);


					$row++;$day_counter++;
				}



    		});
		})->export('xlsx');
	}
	public function postNewTraining()
	{
		if(canAdd('hr_capacity'))
		{
			$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			$sdate = Input::get("start_date");
			$edate = Input::get("end_date");
			if(isShamsiDate())
			{
				if($sdate != '')
				{
					$sdate = explode("-", $sdate);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);
				}
				else
				{
					$sdate = null;
				}
				if($edate != '')
				{
					$edate = explode("-", $edate);
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);
				}
				else
				{
					$edate=null;
				}
			}
			else
			{
				if($sdate != '')
				{
					$sdate = explode("/", $sdate);
					$sy = $sdate[2];
					$sm = $sdate[0];
					$sd = $sdate[1];
					$sdate = $sy."-".$sm."-".$sd;
				}
				else
				{
					$sdate = null;
				}
				if($edate != '')
				{
					$edate = explode("/", $edate);
					$ey = $edate[2];
					$em = $edate[0];
					$ed = $edate[1];
					$edate = $ey."-".$em."-".$ed;
				}
				else
				{
					$edate=null;
				}
			}
			if(Input::get('type')==0)
			{//internal
				$hokm_no = null;
				$external_type = null;
				$hdate = null;
			}
			else
			{
				$hdate = Input::get("hokm_date");
				if($hdate != '')
				{
					$hdate = explode("-", $hdate);
					$hy = $hdate[2];
					$hm = $hdate[1];
					$hd = $hdate[0];
					$hdate = dateToMiladi($hy,$hm,$hd);
				}
				else
				{
					$hdate=null;
				}
				$hokm_no = Input::get('hokm_no');
				$external_type = Input::get('external_type');
			}
			$items = array(
					"title" 			=> Input::get('title'),
					"location"			=> Input::get('location'),
					"type"				=> Input::get('type'),//exteranl or internal
					"start_date"		=> $sdate,
					"end_date"			=> $edate,
					"organizer"			=> Input::get('organizer'),
					"sponser"			=> Input::get('sponser'),
					"days_no"			=> Input::get('days_no'),
					"hokm_date"			=> $hdate,
					"hokm_no"			=> $hokm_no,
					"seat_no"			=> Input::get('seat_no'),
					"external_type"		=> $external_type,
					"dep_type"			=> $user_dep_type,
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);

			$id = hrOperation::insertRecord_id('capacity_trainings',$items);

			return \Redirect::route("getCapacityBuilding")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function editTraining()
	{
		if(canEdit('hr_capacity'))
		{
			$sdate = Input::get("start_date");
			$edate = Input::get("end_date");
			if(isShamsiDate())
			{
				if($sdate != '')
				{
					$sdate = explode("-", $sdate);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);
				}
				else
				{
					$sdate = null;
				}
				if($edate != '')
				{
					$edate = explode("-", $edate);
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);
				}
				else
				{
					$edate=null;
				}
			}
			else
			{
				if($sdate != '')
				{
					$sdate = explode("/", $sdate);
					$sy = $sdate[2];
					$sm = $sdate[0];
					$sd = $sdate[1];
					$sdate = $sy."-".$sm."-".$sd;
				}
				else
				{
					$sdate = null;
				}
				if($edate != '')
				{
					$edate = explode("/", $edate);
					$ey = $edate[2];
					$em = $edate[0];
					$ed = $edate[1];
					$edate = $ey."-".$em."-".$ed;
				}
				else
				{
					$edate=null;
				}
			}
			if(Input::get('type')==0)
			{//internal
				$hokm_no = null;
				$external_type = null;
				$hdate = null;
			}
			else
			{
				$hdate = Input::get("hokm_date");
				if($hdate != '')
				{
					$hdate = explode("-", $hdate);
					$hy = $hdate[2];
					$hm = $hdate[1];
					$hd = $hdate[0];
					$hdate = dateToMiladi($hy,$hm,$hd);
				}
				else
				{
					$hdate=null;
				}
				$hokm_no = Input::get('hokm_no');
				$external_type = Input::get('external_type');
			}
			$items = array(
					"title" 			=> Input::get('title'),
					"location"			=> Input::get('location'),
					"type"				=> Input::get('type'),//exteranl or internal
					"start_date"		=> $sdate,
					"end_date"			=> $edate,
					"organizer"			=> Input::get('organizer'),
					"sponser"			=> Input::get('sponser'),
					"days_no"			=> Input::get('days_no'),
					"hokm_date"			=> $hdate,
					"hokm_no"			=> $hokm_no,
					"seat_no"			=> Input::get('seat_no'),
					"external_type"		=> $external_type,
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);

			hrOperation::update_record('capacity_trainings',$items,array('id'=>Input::get('id')));
			$id = Input::get('id');
			/*
			if(Input::hasFile('scan'))
			{
				$fileName = hrOperation::getTraining(Input::get('id'))->file_name;
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('capacity_trainings')
					->where('id',Input::get('id'))
					->update(array('file_name'=>null));

				}
				// getting all of the post data
				$file = Input::file('scan');
				$errors = "";
				$file_data = array();
				// validating each file.
				$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
				$validator = Validator::make(

				  		[
				            'scan' => $file,
				            'extension'  => Str::lower($file->getClientOriginalExtension()),
				        ],
				        [
				            'scan' => 'required|max:100000',
				            'extension'  => 'required|in:jpg,jpeg,bmp,png'
				        ]
				);

				if($validator->passes())
				{

				    // path is root/uploads
				    $destinationPath = 'documents/hr_attachments';
				    $filename = $file->getClientOriginalName();

				    $temp = explode(".", $filename);
				    $extension = end($temp);

				    $filename = 'capacity_trainings_'.$id.'.'.$extension;

				    $upload_success = $file->move($destinationPath, $filename);

				    if($upload_success)
				    {
						DB::connection('hr')->table('capacity_trainings')->where('id',$id)->update(array('file_name'=>$filename));
					}
					else
					{
					   $errors .= json('error', 400);
					}


				}
				else
				{
				    // redirect back with errors.
				    return Redirect::back()->withErrors($validator);
				}

			}
			*/
			return \Redirect::route("getCapacityBuilding")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function getCapacityTrainersData()
	{
		//get all data
		$object = hrOperation::getCapacityTrainersData();//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'last_name',
							'org',
							'especiality'
							)
				->addColumn('operations', function($option){
					$options = '';
					$options .= '
								<a href="javascript:void()" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';


					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public function postNewTrainer()
	{
		if(canAdd('hr_capacity'))
		{
			$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			$items = array(
					"name" 				=> Input::get('name'),
					"last_name" 		=> Input::get('last_name'),
					"org"				=> Input::get('org'),
					"especiality" 		=> Input::get('especiality'),
					"phone"				=> Input::get('phone'),
					"email"				=> Input::get('email'),
					"experience"		=> Input::get('experience'),
					"dep_type"			=> $user_dep_type,
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);

			hrOperation::insertRecord('capacity_trainers',$items);

			return \Redirect::route("getCapacityBuilding")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function load_employee_trainings()
	{
		//get employee id
		$employee_id = Input::get('id');

		//get employee details
		$data['details'] = hrOperation::getEmployeeCapacityTrainings($employee_id);
		$data['id'] = $employee_id;
		return View('hr.capacity.employee_trainings',$data);
	}
	public function load_training()
	{
		//get employee id
		$id = Input::get('id');

		//get employee details
		$data['details'] = hrOperation::getTraining($id);
		$data['id'] = $id;
		return View('hr.capacity.edit_training',$data);
	}
	public function load_training_emps()
	{
		//get employee id
		$id = Input::get('id');

		//get employee details
		$data['details'] = hrOperation::getTrainingEmployees($id);
		$data['id'] = $id;
		return View('hr.capacity.training_employees',$data);
	}
	public function addTrainingToEmployee()
	{
		if(canAdd('hr_capacity'))
		{
			$items = array(
					"employee_id" 			=> Input::get('employee_id'),
					"training_id"			=> Input::get('training_id'),
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);

			hrOperation::insertRecord('capacity_employee_trainings',$items);

			return \Redirect::route("getCapacityBuilding")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeSalary($emp_id=0)
	{
		$main_salary = Input::get('main_salary');
		$salary = Input::get('salary');
		$prof_salary = Input::get('profesional_salary');
		$kadri_salary = Input::get('kadri_salary');
		if($main_salary==null)
		{
			$main_salary = 0;
			$retirment = 0;
		}
		else
		{
			$retirment = (Input::get('main_salary')*3)/100;
		}
		if($salary==null)
		{
			$salary = 0;
		}
		if($prof_salary==null)
		{
			$prof_salary = 0;
		}
		if($kadri_salary==null)
		{
			$kadri_salary = 0;
		}
		$items = array(
				"employee_id" 	=> $emp_id,
				"main_salary"	=> $main_salary,
				"extra_salary"	=> $salary,
				"prof_salary"	=> $prof_salary,
				"kadri_salary"	=> $kadri_salary,
				"grade"			=> Input::get('grade'),
				"retirment"		=> round($retirment),
				"created_at" 	=> date('Y-m-d H:i:s'),
				"created_by" 	=> Auth::user()->id
			);
		if(hrOperation::getEmployeeSalary($emp_id))
		{
			hrOperation::delete_record('employee_salary',array('employee_id'=>$emp_id));
		}
		hrOperation::insertRecord('employee_salary',$items);
		hrOperation::insertLog('employee_salary',1,'salary of given employee',$emp_id);
		return \Redirect::route("getDetailsEmployee",Crypt::encrypt($$emp_id))->with("success","Record Saved Successfully.");
	}
	public function loadEmployeePayroll()
	{
		//get employee id
		$employee_id = Input::get('id');
		$data['emp_id'] = $employee_id;

		return View('hr.attendance.employee_payroll',$data);
	}
	//bring related basts according to dep
	public function bringRelatedBastViaAjax()
	{
		$dep = Input::get('id');
		$operation = Input::get('type');
		$year = Input::get('year');
		$basts = hrOperation::getRelatedBasts($dep,$operation,$year);
		$options = '<option value="">انتخاب</option>';
		foreach($basts AS $item)
		{
			$options.="<option value='".$item->id."'>".$item->name."-".$item->tainat."</option>";
		}

		return $options;
	}
	public function bringExistBastViaAjax()
	{
		$dep = Input::get('id');
		$vacant = Input::get('vacant');
		$basts = hrOperation::getExistBasts($dep,$vacant);
		$options = '<option value="">انتخاب</option>';
		foreach($basts AS $item)
		{
			$options.="<option value='".$item->id."'>".$item->name."-".$item->tainat."</option>";
		}

		return $options;
	}
	public function editPayroll()
	{
		//get employee id
		$employee_id = Input::get('id');
		$data['year']= Input::get('year');
		$data['month']= Input::get('month');
		$data['rfid']= Input::get('rfid');
		$data['dep_id']= Input::get('dep_id');
		$data['emp_id']= $employee_id;
		//get employee details
		$data['details'] = hrOperation::getEmployeePayroll_details($employee_id,Input::get('year'),Input::get('month'));
		$data['salaries']= hrOperation::getEmployeeSalary($employee_id);
		//$data['parentDeps'] = getDepartmentWhereIn();
		return View('hr.attendance.edit_payroll',$data);
	}
	public function getEmployeeLeaves($emp_id=0,$year,$month)
	{
		if(canAttHoliday())
		{
			$emp_dep_type = hrOperation::getEmployeeDet($emp_id)->dep_type;//employee dep type
			$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;//loged in user dep type
			if($user_dep_type==$emp_dep_type)
			{
				$data['id'] = $emp_id;
				$data['year'] = $year;
				$data['month'] = $month;
				$data['rfid'] = hrOperation::getEmployeeDet($emp_id)->RFID;
				//get employee details
				$data['details'] = hrOperation::getEmployeeLeaves($emp_id,$year,$month);

				return View('hr.attendance.edit_leaves',$data);
			}
			else
			{
				return showWarning();
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeLeaves_print($id=0,$year=0,$month=0)
	{
		$data['employees'] = hrOperation::getEmployeeLeaves($id,$year,$month);
		$data['name'] = getEmployeeFullName($id);
		Excel::create('Leaves', function($file) use($data){

			$file->setTitle('Leaves');

			$file->sheet('Leaves', function($sheet) use($data){
				$sheet->mergeCells('A1:F1');
				$sheet->setCellValue('A1','راپور رخصتی محترم:'.' '.$data['name']);
				$sheet->setCellValue('A2','#');
				$sheet->setCellValue('B2','نوعیت رخصتی');
				$sheet->setCellValue('C2','از تاریخ');
				$sheet->setCellValue('D2','تا تاریخ');
				$sheet->setCellValue('E2','تعداد روز(ها)');
				$sheet->setCellValue('F2','تفصیلات');
				$sheet->row(1, function($row) {
				    // call cell manipulation methods
				    $row->setBackground('#e0ebeb');
					$row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$sheet->row(2, function($row) {
					$row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$row = 3;$day_counter = 1;$leave_no=0;
				$types = array('0'=>'',
                    					 '1'=>'ضروری',
                    					 '2'=>'تفریحی',
                    					 '3'=>'مریضی',
                    					 '4'=>'ولادی',
                    					 '5'=>'عروسی',
                    					 '6'=>'حج',
                    					 '7'=>'دیگر(خدمتی)',
                    					 '8'=>'اضافه رخصتی مریضی',
                    					 '9'=>'دیگر موارد',
                                         '10'=>'شامل کتاب حاضری'
                    );
				foreach($data['employees'] AS $rows)
				{
					$sdate = $rows->date_from;$edate = $rows->date_to;
            		if($sdate !=''){$sdate = explode('-',$sdate);$sdate=dateToShamsi($sdate[0],$sdate[1],$sdate[2]);$sdate=jalali_format($sdate);}
            		if($edate !=''){$edate = explode('-',$edate);$edate=dateToShamsi($edate[0],$edate[1],$edate[2]);$edate=jalali_format($edate);}

					$sheet->setCellValue('A'.$row.'',$day_counter);
					$sheet->setCellValue('B'.$row.'',$types[$rows->type]);
					$sheet->setCellValue('C'.$row.'',$sdate);
					$sheet->setCellValue('D'.$row.'',$edate);
					$sheet->setCellValue('E'.$row.'',$rows->days_no);
					$sheet->setCellValue('F'.$row.'',$rows->desc);


					$row++;$day_counter++;$leave_no+=$rows->days_no;
				}

				$sheet->mergeCells('B'.$row.':D'.$row);
				$sheet->setCellValue('B'.$row.'','مجموع رخصتی ها');
				$sheet->setCellValue('E'.$row.'',$leave_no .' روز');
				$sheet->row($row, function($row) {
				    // call cell manipulation methods
				    $row->setBackground('#e0ebeb');
					$row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});

    		});
		})->export('xlsx');
	}
	public function check_leave_validity()
	{
		$type = Input::get('type');
		if($type=='')
		{
			return 'true';
		}
		$id = Input::get('id');
		$year = Input::get('year');
		if($type==2)
		{//tafrihi
			$kh_days = 0;
			$khedmat = employeeKhedmat_aop($id);
			if($khedmat)
			{
				foreach($khedmat AS $kh)
				{
					$date1 = time();
					$date2 = time();
					if($kh->date_from!=null)
					{
						$date1=strtotime($kh->date_from);
					}
					if($kh->date_to!=null)
					{
						$date2=strtotime($kh->date_to);
					}
					$diff=$date2-$date1;
					$kh_days = $kh_days + floor($diff/(60*60*24));
				}
				if($kh_days>=330)
				{
					if(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=20)
					{//tafrihi
						return 'true';
					}
				}
				else
				{
					return 'tafrihi';
				}
			}
			else
			{//no experience at AOP
				return 'tafrihi';
			}
		}
		//$date = dateToMiladi($year,01,01);//first of shamsi year
		if(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=10 && $type==1)
		{//zarori
			return 'true';
		}
		elseif(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=20 && $type==3)
		{//sick
			return 'true';
		}
		elseif(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=45 && $type==6)
		{//haj
			return 'true';
		}
		elseif(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=105 && $type==4)
		{//pregnancy
			return 'true';
		}
		else
		{
			return 'false';
		}
	}
	public function check_leave_validity_byUser()
	{
		$type = Input::get('type');
		if($type=='')
		{
			return 'true';
		}
		$id = getEmployeeId(Auth::user()->id);
		$sh_date = dateToShamsi(date('Y'),date('m'),date('d'));
		$year = explode('-', $sh_date);
		$year = $year[0];//current shamsi year
		if($type==2)
		{//tafrihi
			$kh_days = 0;
			$khedmat = employeeKhedmat_aop($id);
			if($khedmat)
			{
				foreach($khedmat AS $kh)
				{
					$date1 = time();
					$date2 = time();
					if($kh->date_from!=null)
					{
						$date1=strtotime($kh->date_from);
					}
					if($kh->date_to!=null)
					{
						$date2=strtotime($kh->date_to);
					}
					$diff=$date2-$date1;
					$kh_days = $kh_days + floor($diff/(60*60*24));
				}
				if($kh_days>=330)
				{
					if(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=20)
					{//tafrihi
						return 'true';
					}
				}
				else
				{
					return 'tafrihi';
				}
			}
			else
			{//no experience at AOP
				return 'tafrihi';
			}
		}
		//$date = dateToMiladi($year,01,01);//first of shamsi year
		if(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=10 && $type==1)
		{//zarori
			return 'true';
		}
		elseif(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=20 && $type==3)
		{//sick
			return 'true';
		}
		elseif(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=45 && $type==6)
		{//haj
			return 'true';
		}
		elseif(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=105 && $type==4)
		{//pregnancy
			return 'true';
		}
		else
		{//user can add leave
			return 'false';
		}
	}
	public function check_leave_validity_date()
	{
		$type = Input::get('type');
		$id = Input::get('id');
		$empDet = hrOperation::getEmployeeDet($id);
		if($id==0)
		{//the form is submited from the user itself
			//$id = getEmployeeId(Auth::user()->id);
			$empDet = hrOperation::getEmployeeByUserId(Auth::user()->id);
			$id = $empDet->id;
		}
		$sdate = Input::get('date_from');
		$edate = Input::get('date_to');
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);
		}
		else
		{
			$sdate=date("Y-m-d");
			$sy = 0;
		}
		if($edate != '')
		{
			$edate = explode("-", $edate);
			$ey = $edate[2];
			$em = $edate[1];
			$ed = $edate[0];
			$edate = dateToMiladi($ey,$em,$ed);
		}
		else
		{
			$edate=date("Y-m-d");
		}
		if($empDet->RFID>0)
		{
			//check if before or after requested leave is off day
			$offdaycheck = checkLeaveOffday($sdate,$edate,$empDet->RFID,$empDet->id);
			if($offdaycheck==='offDayLeave')
			{//the user requested leave in a off day, reject the request
				$result = 'offday';
				return json_encode(array('result'=>$result,'leaves'=>''));
			}
			if($offdaycheck==='extraLeave')
			{//the employee is in leave the day before the date, he/she is not allowed to take more normal leaves
				$result = 'extraLeave';
				return json_encode(array('result'=>$result,'leaves'=>''));
			}
			elseif($offdaycheck==='true')
			{//no issue to request for laeve
				$result = 'false';
				//return json_encode(array('result'=>$result,'leaves'=>''));
			}
			elseif($offdaycheck==='absent')
			{//the day before off day, employee is absent
				$result = 'absent';
				return json_encode(array('result'=>$result,'leaves'=>''));
			}
		}
		//$date = dateToMiladi($sy,01,01);
		$current_leaves = hrOperation::getEmployeeLeaves_total($type,$id,$sy);
		$days = date_diff(date_create($sdate),date_create($edate));
		$requested_leaves = (int)$days->format("%a")+1;
		$total_leaves = $current_leaves+$requested_leaves;
		if($total_leaves>10 && $type==1)
		{//zarori
			$result = 'true';
		}
		elseif($total_leaves>20 && $type==2)
		{//tafrihi
			$result = 'true';
		}
		elseif($total_leaves>20 && $type==3)
		{//sick
			$result = 'true';
		}
		elseif($total_leaves>45 && $type==6)
		{//haj
			$result = 'true';
		}
		elseif($total_leaves>90 && $type==4)
		{//pregnant
			$result = 'true';
		}
		else
		{
			$result = 'false';
		}
		return json_encode(array('result'=>$result,'leaves'=>$current_leaves));
	}
	public function getEditLeaveForm()
	{
		$id = Input::get('id');
		$data['id'] = $id;
		$data['year'] = Input::get('year');
		$data['month']= Input::get('month');
		$data['details'] = hrOperation::getLeaveDetails($id);

		return View('hr.attendance.edit_leave',$data);
	}
	//download file from server
	public function downloadLeaveDoc($id=0)
	{
		if(canView('hr_attendance'))
		{
			//get file name from database
			$fileName = hrOperation::getLeaveDetails($id)->file_name;
	        //public path for file
	        $file= public_path(). "/documents/hr_attachments/".$fileName;
	        //download file
	        return Response::download($file, $fileName);
	    }
	    else
	    {
	    	return showWarning();
	    }
	}
	public function deleteLeaveFile()
	{
		if(canEdit('hr_attendance'))
		{
			$id = Input::get('doc_id');
			//get file name from database
			$fileName = hrOperation::getLeaveDetails($id)->file_name;
			$file= public_path(). "/documents/hr_attachments/".$fileName;
			if(File::delete($file))
			{
				//delete from database
				DB::connection('hr')
				->table('leaves')
				->where('id',$id)
				->update(array('file_name'=>null));

				return "<div class='alert alert-success'>File Deleted Successfully!</div>";
			}
			else
			{
				return "<div class='alert alert-danger'>Error!</div>";
			}
		}
		else
		{
			return showWarning();
		}
	}
	//test
	public function getAllAttendances($year,$month,$dep=0,$sub_dep=0)
	{
		$data['allImages'] = hrOperation::test($year,$month);
		//dd($data['allImages']['7877382_0105_1']);
		$data['allEmployees'] = hrOperation::getAllRFIDs($dep,$sub_dep);
		$data['year'] = $year;
		$data['month'] = $month;
		//echo count($data['allEmployees']);exit;
		//return View('hr.attendance.test',$data);
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month == 1)
		{
			$from = dateToMiladi($year-1,12,$sday);
		}
		else
		{
			$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
		}
		//$from = dateToMiladi($year,$month-1,15);//the start day of attendance month is 15 of prevouce month
		$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month
		$begin = new DateTime($from);
		$end = new DateTime($to);
		$interval = DateInterval::createFromDateString('1 day');
		$data['period'] = new DatePeriod($begin, $interval, $end);
		Excel::create('Att-'.$month, function($file) use($data){
			$file->getDefaultStyle()
		        ->getAlignment()
		        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$file->setTitle('Attendance');

			$file->sheet('Attendance', function($sheet) use($data){
				$sheet->setRightToLeft(true);
				$sheet->setStyle(array(
				    'font' => array(
				        'name'      => 'Calibri',
				        //'size'      =>  15,
				        //'bold'      =>  true
				    )
				));
				$emp_count = 1;
				$row = 1;
				$date_row = 2;
				$time_row = 3;
				$colIn = 'A';
				$colOut= 'B';
				$day_counter=0;
				foreach($data['allEmployees'] AS $emp)
				{
					$sheet->setCellValue('A'.$row.'',$emp_count);
					$sheet->mergeCells('B'.$row.':C'.$row);
					$sheet->setCellValue('B'.$row.'',$emp->name_dr);
					$sheet->mergeCells('D'.$row.':E'.$row);
					$sheet->setCellValue('D'.$row.'',$emp->father_name_dr);
					$sheet->mergeCells('F'.$row.':I'.$row);
					$sheet->setCellValue('F'.$row.'',$emp->current_position_dr);
					$sheet->mergeCells('J'.$row.':N'.$row);
					$sheet->setCellValue('J'.$row.'',$emp->name);
					$sheet->row($row, function($row) {
					    // call cell manipulation methods
					    $row->setBackground('#e0ebeb');
						$row->setAlignment('center');
						//$row->setValignment('middle');
						$row->setFontWeight('bold');
					});
					//$sheet->setOrientation('landscape');
					foreach($data['period'] AS $day)
					{
					 	$the_day = $day->format( "Y-m-d" );
					 	$day_code = date("w", strtotime($the_day));//1: monday
					 	$day_week = date("l", strtotime($the_day));
						$period_det = explode('-',$the_day);
						$period_day = $period_det[2];
						$period_month = $period_det[1];
						$p_year = $period_det[0];
						$shamsi_date = dateToShamsi($p_year,$period_month,$period_day);

						$period_monthDay = $period_month.$period_day;
						//$timein = '<span style="color:red">NA</span>';$timeout='<span style="color:red">NA</span>';
						$timein = '-';$timeout='-';
						if (array_key_exists($emp->RFID.'_'.$period_monthDay.'_1',$data['allImages']))
						{
							$img=$data['allImages'][$emp->RFID.'_'.$period_monthDay.'_1'];
							$path = explode("_",$img->path);
							$time = (int)substr($path[0],-6);//get the time part in path
							$month_day = substr($path[0],-11,4);//get the monthDay part in path

							if($time <= 120000)
							{
								$time=str_pad($time,6,'0',STR_PAD_LEFT);
								$time= preg_replace('/(\d{2})(\d{2})(\d{2})/', "$1:$2:$3", $time);
								$time=date("g:i a", strtotime($time));
								//$time = preg_replace('/(\d\d)(\d\d)\d\d/', '($1<12)?"$1:$2 AM":$1-12 . ":$2 PM"', $time);
								$timein= $time;

							}
							else
							{
								$time=str_pad($time,6,'0',STR_PAD_LEFT);
								$time= preg_replace('/(\d{2})(\d{2})(\d{2})/', "$1:$2:$3", $time);
								$time=date("g:i a", strtotime($time));
								$timeout = $time;

							}
						}
						if (array_key_exists($emp->RFID.'_'.$period_monthDay.'_2',$data['allImages']))
						{
							$img=$data['allImages'][$emp->RFID.'_'.$period_monthDay.'_2'];
							$path = explode("_",$img->path);
							$time = (int)substr($path[0],-6);//get the time part in path
							$month_day = substr($path[0],-11,4);//get the monthDay part in path
							if($time <= 120000)
							{
								$time=str_pad($time,6,'0',STR_PAD_LEFT);
								$time= preg_replace('/(\d{2})(\d{2})(\d{2})/', "$1:$2:$3", $time);
								$time=date("g:i a", strtotime($time));
								//$time = preg_replace('/(\d\d)(\d\d)\d\d/', '($1<12)?"$1:$2 AM":$1-12 . ":$2 PM"', $time);
								$timein= $time;

							}
							else
							{
								$time=str_pad($time,6,'0',STR_PAD_LEFT);
								$time= preg_replace('/(\d{2})(\d{2})(\d{2})/', "$1:$2:$3", $time);
								$time=date("g:i a", strtotime($time));
								$timeout = $time;

							}
						}
						//$sheet->mergeCells($colIn.$date_row.':'.$colOut.$date_row);
						//$sheet->setCellValue($colIn.$date_row,$shamsi_date.'('.$day_week.')');
						$sheet->setCellValue($colIn.$date_row,$shamsi_date);
						$sheet->setCellValue($colOut.$date_row,$day_week);
						$sheet->setCellValue($colIn.$time_row,$timein);
						$sheet->setCellValue($colOut.$time_row,$timeout);
						$day_counter++;
						$mod = $day_counter%7;
						if($mod == 0)
						{
							$colIn = 'A';
							$colOut= 'B';

							$date_row = $date_row+2;
							$time_row = $time_row+2;
						}
						else
						{
							$colIn++;
							$colOut++;
							$colIn++;
							$colOut++;
						}
					}
					$day_counter = 0;
					$colIn = 'A';
					$colOut= 'B';
					$row = $row+11;
					$date_row = $date_row+3;
					$time_row = $time_row+3;
					$emp_count++;
				}
				$sheet->setBorder('A1:N'.$time_row.'', 'dotted');
    		});

		})->download('xlsx');
	}

	public function getTodayAttendances($dep=0,$sub=0,$status=0,$type=0)
	{
		if(canAttTodayAtt())
		{
			$data['dep'] = $dep;
			$data['sub_dep'] = $sub;
			$data['status'] = $status;
			$data['type'] = $type;

			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_deps']=getRelatedSubDepartment($dep);
			//$data['employees'] = hrOperation::getRelatedEmployees(Input::get('dep_id'));
			return View::make('hr.attendance.todayAttendance',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getTodayAttData($dep_id=0,$sub=0,$status=0,$type=0)
	{
		$date = dateToShamsi(date('Y'),date('m'),date('d'));
		$date = explode('-',$date);
		$year = $date[0];
		$month = $date[1];
		$object = hrOperation::getEmployeesTodayAtt($dep_id,$sub,$status,$type);//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'father_name',
							'current_position_dr',
							'department',
							'status'
							)
				// ->orderColumns('id','id')
				->make();
	}

	public function insertImages_today()
	{
		if(canAttTodayAtt())
		{
			$year = date('Y');
			$month= date('m');
			$day =  date('d');
			$the_date = $year.'/'.$month.$day;
			if(hrOperation::checkTodayAtt($the_date))
			{
				return 'done';
			}
			$result = "";
			// create a new cURL resource
			$ch = curl_init();

			// set URL and other appropriate options
			curl_setopt($ch, CURLOPT_URL, "10.10.0.201/images.php?year=$year&month=$month&day=$day");
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			// grab URL and pass it to the browser
			$result = curl_exec($ch);
			$result = explode("\n",$result);
			$result = array_reverse($result);
			// close cURL resource, and free up system resources
			curl_close($ch);
			$first_data = array();
			$data = array();
			for($i=0;$i<count($result);$i++)
			{
				$pos = strpos($result[$i], '_');
				if ($pos !== false)
				{//check if it has rfid
					$path = explode("\\",$result[$i]);
					if(count($path)==5)
					{//it should have 4 part:\machine\year\monthDay\time_rfid
						//$year = $path[2];$monthDay=$path[3];
						$time_rfid = explode('_',$path[4]);
						$time = $time_rfid[0];
						$rfid = $time_rfid[1];
						$rfid = explode('.',$rfid);
						$rfid = $rfid[0];
						$path_replace = str_replace('\\', '/', $result[$i]);
						if(!in_array(array('rfid'=>$rfid),$first_data))
						{
							$first_data[]=array('rfid'=>$rfid);
							$data[] = array('RFID'=>$rfid,'path'=>$path_replace,'time'=>$time);
						}

						/*
						if($time_rfid[0]<120000)
						{
							if(!in_array(array('rfid'=>$rfid,'day'=>$path[3],'timeout'=>0),$first_data))
							{
								$first_data[]=array('rfid'=>$rfid,'day'=>$path[3],'timeout'=>0);
								$data[] = array('RFID'=>$rfid,'path'=>$path_replace);
							}
						}
						*/
					}
				}
			}
			//dd(count($data));
			DB::connection('hr')->table('attendance_images_today')->truncate();
			hrOperation::insertRecord('attendance_images_today',$data);
			hrOperation::insertLog('attendance_images_today',1,'todays attendance',0);

			return 'done';
		}
		else
		{
			return showWarning();
		}
	}
	public function postPayroll(request $request)
	{
		hrOperation::delete_record('employee_payrolls',array('employee_id'=>$request['employee_id'],'year'=>$request['year'],'month'=>$request['month']));
		$data = array(
			'employee_id'		=> $request['employee_id'],
			'year'				=> $request['year'],
			'month'				=> $request['month'],
			'makolat'			=> $request['makolat'],
			'car_rent'			=> $request['car_rent'],
			'prr'				=> $request['prr'],
			'khatar'			=> $request['khatar'],
			'other_benifit'		=> $request['other_benifit'],
			'robh'				=> $request['robh'],
			'tazmin'			=> $request['tazmin'],
			'makolat_ksor'		=> $request['makolat_ksor'],
			'bank'				=> $request['bank'],
			'returns'			=> $request['returns'],
			'other_ksor'		=> $request['other_ksor'],
			"created_at" 		=> date('Y-m-d H:i:s'),
			"created_by" 		=> Auth::user()->id
			);
		hrOperation::insertRecord('employee_payrolls',$data);

		return \Redirect::route("getPayrollList",array($request['dep_id'],$request['year'],$request['month']))->with("success","Record Saved Successfully.");
	}
	public function checkInsertImagesDate(request $request)
	{
		$the_date = explode('-',$request['date']);
		$the_date = dateToMiladi($the_date[2],$the_date[1],$the_date[0]);
		if($the_date>=date('Y-m-d'))
		{
			return '<span style="color:red">The date should be smaller than today</span>';
		}
		if(hrOperation::checkInsertImagesDate($the_date))
		{
			return '';
		}
		else
		{
			return '<span style="color:red">The Images are already inserted for this day</span>';
		}

	}
	public function searchTashkil()
	{
		//check roles
		if(canView('hr_tashkil'))
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			return View::make('hr.tashkil.search',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getTashkilSearch(request $request)
	{

		$data['parentDeps'] = getDepartmentWhereIn();
		$data['sub_deps']=getRelatedSubDepartment($request['general_department']);
		$data['type'] = $request['type'];
		$data['year'] = $request['year'];
		$data['dep_id'] = $request['general_department'];
		if($request['sub_dep']!='')
		{
			$data['sub_dep_id'] = $request['sub_dep'];
		}
		else
		{
			$data['sub_dep_id'] = 0;
		}
		if($request['employee_type']==2)
		{
			$data['bast'] = $request['ajir_bast'];
		}
		elseif($request['employee_type']==3)
		{
			$data['bast'] = $request['military_bast'];
		}
		else
		{
			$data['bast'] = $request['emp_bast'];
		}
		if($request['title']=='')
		{
			$data['title']	= 0;
		}
		else
		{
			$data['title']	= $request['title'];
		}

		$data['employee_type'] = $request['employee_type'];
		if($request['search'])
		{
			return View::make('hr.tashkil.searchResult',$data);
		}
		else
		{
			$data['employees'] = hrOperation::getTashkilSearch($request['general_department'],$data['sub_dep_id'],$request['type'],$data['bast'],$data['title'],$request['employee_type'],$request['year']);

			Excel::create('Tashkil', function($file) use($data){

				$file->setTitle('Tashkil');

				$file->sheet('Tashkil', function($sheet) use($data){
					$sheet->setRightToLeft(true);
					$sheet->setOrientation('landscape');

					$sheet->setCellValue('A2','#');
					$sheet->setCellValue('B2','بست');
					$sheet->setCellValue('C2','تعینات');
					$sheet->setCellValue('D2','وظیفه');
					$sheet->setCellValue('E2','ریاست');
					$sheet->setCellValue('F2','معاونیت');
					$sheet->setCellValue('G2','سال');


					$row = 3;
					$day_counter = 1;
					foreach($data['employees'] AS $emp)
					{

						$sheet->setCellValue('A'.$row.'',$day_counter);
						$sheet->setCellValue('B'.$row.'',$emp->bast);
						$sheet->setCellValue('C'.$row.'',$emp->tainat);
						$sheet->setCellValue('D'.$row.'',$emp->title);
						$sheet->setCellValue('E'.$row.'',$emp->sub_dep);
						$sheet->setCellValue('F'.$row.'',$emp->department);
						$sheet->setCellValue('G'.$row.'',$emp->year);



						$row++;$day_counter++;
					}
	    		});
			})->download('xlsx');
		}
	}
	public function getTashkilSearchData($dep=0,$sub_dep=0,$type=2,$bast=0,$title=0,$emp_type=0,$year=0)
	{
        //get all data
		$object = hrOperation::getTashkilSearch($dep,$sub_dep,$type,$bast,$title,$emp_type,$year);//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'bast',
							'tainat',
							'title',
							'sub_dep',
							'department',
							'year'
							)
						->addColumn('operations', function($option){
							$options = '';
							if(canEdit('hr_tashkil'))
							{
								$options.='
									<a href="javascript:void()" onclick="load_tashkil_det('.$option->id.')" class="table-link" style="text-decoration:none;" data-target="#update_tashkil" data-toggle="modal">
										<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
									</a>';
							}
							if(canDelete('hr_tashkil'))
							{
								if($option->status==0)
								{
									$options.='<a href="javascript:void()" onclick="deleteTashkil('.$option->id.')" class="table-link" style="text-decoration:none;">
												<i class="icon fa-trash-o" style="color:red;" aria-hidden="true" style="font-size: 16px;"></i>
											</a>';
								}
								else
								{
								 	$options.='<i onmouseover="getEmpDetailsByTashkil('.$option->id.');" id="tashkil_'.$option->id.'" class="icon fa-trash-o" aria-hidden="true" style="font-size: 16px;" title="This Tashkil is Used!"></i>';
								}

							}
							return $options;
						})
				->make();
		return $result;
	}
	public function getRecruitmentSearch(request $request)
	{  
		$data['parentDeps'] = getDepartmentWhereIn();
		$data['sub_deps']=getRelatedSubDepartment($request['general_department']);
        $records= hrOperation::getRecruitmentSearchData();
        $data['records'] = $records; 
        if($records)
        {
            $records_array = $records->toArray();  
            $data['unique_record'] = unique_multidim_array(json_decode(json_encode($records_array['data']),true),'id'); 
        }
        $data['lang'] = Session::get('lang'); 

		return View::make('hr.recruitment.search_result_list',$data);
	}

	public function printEmployees()
	{
        $records = hrOperation::getEmployeesForPrint();
        $uniq_records = unique_multidim_array(json_decode(json_encode($records),true),'id');
        $data['employees'] = json_decode(json_encode($uniq_records), FALSE);
        $lang = Session::get('lang'); 
		Excel::create('employees', function($file) use($data,$lang){

			$file->setTitle('Employees');
			$file->sheet('Employees', function($sheet) use($data,$lang){
				$sheet->setRightToLeft(true);
				$sheet->setOrientation('landscape');

				$sheet->setCellValue('A2','#');
				$sheet->setCellValue('B2','نام کامل');
				$sheet->setCellValue('C2','نام انگلیسی');
				$sheet->setCellValue('D2','نام پدر');
				$sheet->setCellValue('E2','درجه تحصیل');
				$sheet->setCellValue('F2','رشته');
				$sheet->setCellValue('G2','سال تولد');
				$sheet->setCellValue('H2','اول تقرر');
				$sheet->setCellValue('I2','رتبه');
				$sheet->setCellValue('J2','بست');
				$sheet->setCellValue('K2','درجه');
				$sheet->setCellValue('L2','بست');
				$sheet->setCellValue('M2','وظیفه فعلی');
				$sheet->setCellValue('N2','موقف');
				$sheet->setCellValue('O2','کارکنان');
				$sheet->setCellValue('P2','اداره مربوطه');
				//$sheet->setCellValue('P2','وضعیت فعلی');
				$sheet->setCellValue('Q2','خدمت');

				$row = 3;
				$day_counter = 1;
				foreach($data['employees'] AS $emp)
				{
					$emp_type = '';
					if($emp->employee_type==1){$emp_type = 'مامور';}
					if($emp->employee_type==2){$emp_type = 'اجیر';}
					if($emp->employee_type==3){$emp_type = 'نظامی';}
					$emp_mawqif = \Config::get("static.emp_status.$lang.$emp->mawqif_employee");
				

					$sheet->setCellValue('A'.$row.'',$emp->id);
					$sheet->setCellValue('B'.$row.'',$emp->fullname);
					$sheet->setCellValue('C'.$row.'',$emp->en_fullname);
					$sheet->setCellValue('D'.$row.'',$emp->father_name);
					$sheet->setCellValue('E'.$row.'',$emp->edu_degree);
					$sheet->setCellValue('F'.$row.'',$emp->education_field);
					$sheet->setCellValue('G'.$row.'',$emp->birth_year);
					$empExp=getEmployeeFirstExperience($emp->id);
					if($empExp)
					{
						$first_date = explode('-',$empExp->date_from);
						if(count($first_date)==3)
						{
							$shamsi_date = dateToShamsi($first_date[0],$first_date[1],$first_date[2]);
						}
						else
						{
							$shamsi_date=getEmployeeFirstExperience($emp->id)->date_from;
						}
						$sheet->setCellValue('H'.$row.'',$shamsi_date);
					}
					else
					{
						$sheet->setCellValue('H'.$row.'',$emp->emp_date);
					}
					if($emp->employee_type==2)
					{
						$sheet->setCellValue('I'.$row.'','');
						$sheet->setCellValue('J'.$row.'','');
						$sheet->setCellValue('K'.$row.'',$emp->rank);
						$sheet->setCellValue('L'.$row.'',$emp->bast);
					}
					else
					{
						$sheet->setCellValue('I'.$row.'',$emp->rank);
						$sheet->setCellValue('J'.$row.'',$emp->bast);
						$sheet->setCellValue('K'.$row.'','');
						$sheet->setCellValue('L'.$row.'','');
					}
					$sheet->setCellValue('M'.$row.'',$emp->current_position_dr);
					$sheet->setCellValue('N'.$row.'',$emp_mawqif);
					$sheet->setCellValue('O'.$row.'',$emp_type);
					$sheet->setCellValue('P'.$row.'',$emp->department);

					$khedmat = employeeKhedmat($emp->id);
					$kh_days = 0;
					if($khedmat)
					{
						foreach($khedmat AS $kh)
						{
							$date1 = time();
							$date2 = time();
							if($kh->date_from!=null)
							{
								$date1=strtotime($kh->date_from);
							}
							if($kh->date_to!=null)
							{
								$date2=strtotime($kh->date_to);
							}
							$diff=$date2-$date1;
							$kh_days = $kh_days + floor($diff/(60*60*24));
						}

					}
					if($kh_days>=365)
					{
						$year = (int)($kh_days/365);
						if($kh_days%365>=30)
						{
							$month = (int)(($kh_days%365)/30);
							$day = ($kh_days%365)%30;
						}
						else
						{
							$month = 0;
							$day = $kh_days%365;
						}
					}
					else
					{
						$year = 0;
						$month= (int)($kh_days/30);
						$day = $kh_days%30;
					}

					$sheet->setCellValue('Q'.$row.'',$year.'Y-'.$month.'M-'.$day.'D');

					$row++;$day_counter++;
				}
    		});
		})->export('xlsx');
	}
	public function bringYearTime()
	{
		$year = Input::get('year');
		$month= Input::get('month');
		$time = getAttendanceTime($year,$month);
		$time_thu = getAttendanceTime_thu($year,$month);
		$time_in = '';
		$time_out = '';
		$time_in_thu = '';
		$time_out_thu = '';
		if($time)
		{
			$time_in = $time->time_in;
			$time_out = $time->time_out;
		}
		if($time_thu)
		{
			$time_in_thu = $time_thu->time_in;
			$time_out_thu = $time_thu->time_out;
		}
		return json_encode(array('time_in'=>$time_in,'time_out'=>$time_out,
								'time_in_thu'=>$time_in_thu,'time_out_thu'=>$time_out_thu
							)
						);
	}
	public function bringMonthHolidays()
	{
		$data['year'] = Input::get('year');
		$data['month']= Input::get('month');
		$days = hrOperation::getMonthHolidayDays(Input::get('year'),Input::get('month'));
		if($days)
		{
			$data['days'] = $days->days;
		}
		else
		{
			$data['days'] = 0;
		}

		$data['details'] = hrOperation::getMonthHolidays(Input::get('year'),Input::get('month'));
		return json_encode(array('view'=>View::make('hr.attendance.holidays_edit',$data)->render(),'days'=>$data['days']));
		//return View::make('hr.attendance.holidays_edit',$data);
	}
	public function bringMonthEmergencyHolidays()
	{
		$data['year'] = Input::get('year');
		$data['month']= Input::get('month');
		$days = hrOperation::getMonthEmergencyDays(Input::get('year'),Input::get('month'));
		if($days)
		{
			$data['days'] = $days->days;
		}
		else
		{
			$data['days'] = 0;
		}
		$data['details'] = hrOperation::getMonthEmergencyHolidays(Input::get('year'),Input::get('month'));
		return json_encode(array('view'=>View::make('hr.attendance.emergency_edit',$data)->render(),'days'=>$data['days']));
		//return View::make('hr.attendance.holidays_edit',$data);
	}
	public function insert_cards()
	{
		$object = hrOperation::get_Cards();
		$items = array();
		foreach($object AS $emp)
		{
			$emp_id = hrOperation::check_fname($emp->name,$emp->fname);
			if($emp_id)
			{
					$items[] = array(
					"card_no" 				=> $emp->card_no,
					"employee_id"			=> $emp_id->id,
					"card_color"			=> $emp->card_color,
					"job_location"			=> $emp->job_location,
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);
			}
		}
		hrOperation::insertRecord('employee_cards',$items);
		echo 'done';
    }

    /** 
     * @Author: Update by Jamal Yousufi  
     * @Date: 2019-10-29 14:56:52 
     * @Desc: Save Employee New Tashkil  
     */    
    public function saveEmployeeNewTashkil(request $request)
	{
        $date_from       = convertDate($request->date,'to_miladi');
		$object          = hrOperation::find($request->id);
        $emp_bast        = hrOperation::getEmployeeBast($request->tashkil);
        $mawqif_employee = $request->mawqif_employee; 

		if($object->current_position_dr == $request['position'] && $object->emp_bast == $emp_bast)
		{//employee is assigned to same position, no need to update the experience

		}
		else
		{
			$employee_experience = hrOperation::getEmployeeExperience($request->id);//get last experience
			if($employee_experience && $mawqif_employee==1)
			{
				hrOperation::update_record('employee_experiences',array('date_to'=>$date_from),array('id'=>$employee_experience->id));
			}
			$employee_rank = hrOperation::getEmployeeRank($object->employee_type,$request->id);
			$employee_bast = hrOperation::getEmployeeTashkil($request->tashkil);

			//$date_from = dateToMiladi(1395,12,16);
			$data = array(
							'employee_id' 	=> $request->id,
							'organization'	=> hrOperation::getDepName($request->sub_dep)->name,
							'position'		=> $request->position,
							'bast'			=> $employee_bast->bast,
							'rank'			=> $employee_rank->rank,
							'date_from'		=> $date_from,
							'type'		    => 1,
							'internal'		=> 1,
							'created_at' 	=> date('Y-m-d H:i:s'),
							'created_by' 	=> Auth::user()->id
						);

			hrOperation::insertRecord('employee_experiences',$data);
		}
		//update the status of current tashkil id from used to unused because it is gonna be updated
		hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$object->tashkil_id));

		$object->number_tayenat 	    = $request->no;
		$object->general_department 	= $request->dep;
		$object->department				= $request->sub_dep;
		$object->tashkil_id				= $request->tashkil;
		$object->current_position_dr    = $request->position;
		$object->first_date_appointment	= $request->date;
		$object->emp_bast				= $emp_bast;
		$object->mawqif_employee		= 1;
		if($object->save())
		{
			hrOperation::insertLog('employees',1,'new tashkil for employee',$request['id']);

			if($request['tashkil'])
			{//update the bast id to used
				hrOperation::update_record('tashkilat',array('status'=>1),array('id'=>$request['tashkil']));
			}
		}
	}
	public function printDailyAtt($dep=0,$sub_dep=0,$status,$type=0)
	{
		$data['records'] = hrOperation::getEmployeesTodayAtt($dep,$sub_dep,$status,$type);

		Excel::create('TodayAtt', function($file) use($data){

			$file->setTitle('Attendance');

			$file->sheet('Attendance', function($sheet) use($data){
				$sheet->setRightToLeft(true);
				$sheet->setCellValue('A1','#');
				$sheet->setCellValue('B1','اسم');
				$sheet->setCellValue('C1','ولد');
				$sheet->setCellValue('D1','دیپارتمنت');
				$sheet->setCellValue('E1','وظیفه');
				$sheet->setCellValue('F1','وضعیت');
				$sheet->row(1, function($row) {
				    // call cell manipulation methods
				    $row->setBackground('#e0ebeb');
					$row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$row = 2;
				$counter = 1;
				foreach($data['records'] AS $emp)
				{
					$sheet->setCellValue('A'.$row,$counter);
					$sheet->setCellValue('B'.$row,$emp->name);
					$sheet->setCellValue('C'.$row,$emp->father_name);
					$sheet->setCellValue('D'.$row,$emp->department);
					$sheet->setCellValue('E'.$row,$emp->current_position_dr);
					$sheet->setCellValue('F'.$row,$emp->status);
					$row++;
					$counter++;
				}
				$sheet->setBorder('A1:E'.$row.'', 'dotted');
    		});

		})->download('xlsx');
	}
	public function getMoreEmergencyDate()
	{
		$total = Input::get('total');
		if(Input::get('type')=='e')
		{
			$result = '
				<div class="col-sm-12" id="e_'.$total.'">
					<div class="col-sm-4">
	                	<div class="col-sm-12">
	                		<label class="col-sm-12 ">تاریخ</label>
	                		<input class="form-control datepicker_farsi" readonly type="text" name="edate_'.$total.'">
	                	</div>
	                </div>
	                <div class="col-sm-6">
	                	<div class="col-sm-12">
	                		<label class="col-sm-12 ">توضیحات</label>
	                		<input class="form-control" type="text" name="edesc_'.$total.'">
	                	</div>
	                </div>
	                <div class="col-sm-2">
	                	<div class="col-sm-12">
	                		<label class="col-sm-12 ">&nbsp;</label>
	                		<input class="btn btn-danger" type="button" value=" - " onclick="remove_emergency_date('.$total.')">
	                	</div>
	                </div>
	            </div>';
		}
		else
		{
			$result = '<div class="col-sm-12" id="h_'.$total.'">
						<div class="col-sm-4">
		                	<div class="col-sm-12">
		                		<label class="col-sm-12 ">تاریخ</label>
		                		<input class="form-control datepicker_farsi" readonly type="text" name="date_'.$total.'">
		                	</div>
		                </div>
		                <div class="col-sm-6">
		                	<div class="col-sm-12">
		                		<label class="col-sm-12 ">توضیحات</label>
		                		<input class="form-control" type="text" name="desc_'.$total.'">
		                	</div>
		                </div>
		                <div class="col-sm-2">
		                	<div class="col-sm-12">
		                		<label class="col-sm-12 ">&nbsp;</label>
		                		<input class="btn btn-danger" type="button" value=" - " onclick="remove_holiday_date('.$total.')">
		                	</div>
		                </div>
		            </div>';
		}
		return $result;
	}
	public function sendAbsent_emails()
	{
		$the_date = date('Y-m-d');
		$dayOfWeek = date('D',strtotime($the_date));
  		$holiday = checkHoliday($the_date);
		$urgents = checkUrgentHoliday($the_date);
  		if($dayOfWeek=='Fri' || $holiday || $urgents)
  		{//if friday or holiday or urgent dont send email
  			return;
  		}
  		else
  		{
			$shamsi_date = dateToShamsi(date('Y'),date('m'),date('d'));
			$result = hrOperation::get_todays_absent();
			//dd($result);
			$directorates=array();
			$executives=array();
			$dirs_emails =array(
					28=>'def.sec@aop.gov.af',//defa
					33=>'bismellah.haidary@gmail.com',//kabina
					53=>'ahmadrashed.t@gmail.com',//hoqoqi
					89=>'asadullah.salehi@aop.gov.af',//moshawerin
					41=>'miagulwaseeq@gmail.com',//dini
					174=>'s.hafiz.fayaz@gmail.com',//madani
					175=>'zia.abdulrahimzai@gmail.com',//parlemani
					6=>'victoria.ghauri@aop.gov.af',//hr
					409=>'victoria.ghauri@aop.gov.af',//new hr
					45=>'fegarzada@hotmail.com',//finance
					410=>'fegarzada@hotmail.com',//new finance
					264=>'rahim.stanikzai@aop.gov.af',//it
					411=>'rahim.stanikzai@aop.gov.af',//new it
					79=>'sadaqatullah.sadiq@aop.gov.af',//procurement
					414=>'sadaqatullah.sadiq@aop.gov.af',//new procurement
					404=>'patyal.ghorzang@aop.gov.af',//npa, nezarat
					405=>'wais.rahimi@aop.gov.af',//npa, tashilat
					406=>'ahmad.naqshbendi@gmail.com',//npa, resource
					25=>'stimory@gmail.com',//ensejam deputy
					39=>'feridun.ilham@aop.gov.af',//agahi deputy
					58=>'Liaqat.Khaleeq@aop.gov.af',//npa
					44=>'a.attash@aop.gov.af',//mali deputy
					408=>'a.attash@aop.gov.af',//operation
					//ocs
					428=>'subhan.raouf@aop.gov.af',
					429=>'h.chakhansuri@gmail.com',
					430=>'whedayet@aop.gov.af',
					433=>'m.kochai@aop.gov.af',
					434=>'shafiq.ibrahimi@aop.gov.af',
					436=>'zxklmy@gmail.com',
					437=>'ahmadrashed.t@gmail.com',
					439=>'najiaanwari1@gmail.com'
								);
			$second_emails =array(
					28=>'naseer.amiri2000@gmail.com',
					33=>'hamedwally4@gmail.com',
					53=>'zakia_sabah@yahoo.com',
					89=>'sideqi2000@gmail.com',
					417=>'rahimullah.noory@gmail.com',
					41=>'hamida.hewad@gmail.com',
					174=>'ehsansaramad1370@gmail.com',
					175=>'niazmohammad.nori@gmail.com',
					6=>'deedar.stanikzay@gmail.com',
					409=>'deedar.stanikzay@gmail.com',
					45=>'abwahidpaiman@gmail.com',
					410=>'abwahidpaiman@gmail.com',
					76=>'nasim.sidiqiorg@yahoo.com',
					413=>'nasim.sidiqiorg@yahoo.com',
					79=>'h.saborqayoumi@gmail.com',
					414=>'h.saborqayoumi@gmail.com',
					264=>'wali.ahmadzai@aop.gov.af',
					411=>'wali.ahmadzai@aop.gov.af',
					269=>'abdulhamidhakimy8@gmail.com',
					412=>'abdulhamidhakimy8@gmail.com',
					404=>'mhhashimi@yahoo.com',
					405=>'najiba.nejati@aop.gov.af',
					406=>'saboor.safi@yahoo.com',
					//25=>'maryammasadiq786@gmail.com',
					58=>'nazari.arefa@yahoo.com',
					//44=>'sharifi.y@hotmail.com',
					//408=>'sharifi.y@hotmail.com',
					421=>'shabdesmusamem@gmail.com',
					422=>'wazhmaibrahimkhil2011@gmail.com',
					423=>'kazem.faiz@gmail.com',
					425=>'lailahamza08@gmail.com',
					428=>'zohal.azizy1@gmail.com',
					429=>'A.Nawabzada77@yahoo.com',
					430=>'marzia_naderi@yahoo.com',
					433=>'akram.jamshidi@aop.gov.af',
					434=>'wazhma.nasiri@aop.gov.af',
					436=>'yazdanpanah518@gmail.com',
					437=>'shabnam.zr95@gmail.com',
					439=>'minarahmani440@gmail.com'


								);
			$dep_id=0;
			$sh = explode('-',$shamsi_date);
			$sh_year = $sh[0];
			$sh_month = $sh[1];
			foreach($result as $rec)
			{
				$isInLeave = isEmployeeInLeave($rec->id,$the_date,$sh_year,$sh_month);
				if($isInLeave)
				{
					continue;
				}
				else
				{
					if (array_key_exists($rec->department,$dirs_emails))
					{
						$directorates[$rec->department]['email']=$dirs_emails[$rec->department];
					}
					else
					{
						$directorates[$rec->department]['email']='no_email';
					}
					$directorates[$rec->department][] = array('name'=>$rec->name,'last_name'=>$rec->last_name,'dep'=>$rec->department);
					//emails to excutive manager
					if (array_key_exists($rec->department,$second_emails))
					{
						$executives[$rec->department]['email']=$second_emails[$rec->department];
					}
					else
					{
						$executives[$rec->department]['email']='no_email';
					}
					$executives[$rec->department][] = array('name'=>$rec->name,'last_name'=>$rec->last_name,'dep'=>$rec->department);
					$email = explode('@',$rec->email);
					if($rec->email!='' && count($email)==2)
					{
						$user_email = array('email'=>$rec->email,'name'=>$rec->name,'last_name'=>$rec->last_name,'the_date'=>$shamsi_date);
						Mail::send('emails.absent', $user_email, function($message) use ($user_email){
								    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
									$message->to($user_email['email'])->subject('Absent Alert');
								});
					}
					else
					{
						continue;
					}
				}
			}
			if(count($directorates)>0)
			{
				foreach($directorates as $array)
				{//loop for every direcotrate
					if($array['email']=='no_email')
					{
						continue;
					}
					else
					{
						$user_email = array('email'=>$array['email'],'emps'=>$array,'the_date'=>$shamsi_date);
						Mail::send('emails.dir_absent', $user_email, function($message) use ($user_email){
							    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
								$message->to($user_email['email'])->subject('Absent Alert');
								//$message->cc($user_email['email']);
							});
					}
				}
			}
			if(count($executives)>0)
			{
				foreach($executives as $executive)
				{//loop for every direcotrate
					if($executive['email']=='no_email')
					{
						continue;
					}
					else
					{
						$user_email = array('email'=>$executive['email'],'emps'=>$executive,'the_date'=>$shamsi_date);
						Mail::send('emails.dir_absent', $user_email, function($message) use ($user_email){
							    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
								$message->to($user_email['email'])->subject('Absent Alert');
								//$message->cc($user_email['email']);
							});
					}
				}
			}
  		}
    }
    
	public function sendAfternoon_emails()
	{
            $shamsi_date = Input::get('the_date');
            $the_sh_date = explode('-',$shamsi_date);
            $the_date = dateToMiladi($the_sh_date[2],$the_sh_date[1],$the_sh_date[0]);
			$dayOfWeek = date('D',strtotime($the_date));
	  	    $holiday = checkHoliday($the_date);
			$urgents = checkUrgentHoliday($the_date);
  		if($dayOfWeek=='Fri' || $holiday || $urgents)
  		{//if friday or holiday or urgent dont send email
  			return 'به دلیل بودن رخصتی ایمیل ارسال نمیشود.';
  		}
  		else
  		{
				$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
  			    $result = hrOperation::get_yesterdays_absent($the_date,$user_dep_type);
				
				$directorates=array();
				$executives=array();
				$dirs_emails =array(
						28=>'def.sec@aop.gov.af',//defa
						33=>'halimeyar2@gmail.com',//kabina
						53=>'ahmadrashed.t@gmail.com',//hoqoqi
						89=>'asadullah.salehi@aop.gov.af',//moshawerin
						41=>'miagulwaseeq@gmail.com',//dini
						174=>'s.hafiz.fayaz@gmail.com',//madani
						175=>'zia.abdulrahimzai@gmail.com',//parlemani
						6=>'victoria.ghauri@aop.gov.af',//hr
						409=>'victoria.ghauri@aop.gov.af',//new hr
						45=>'fegarzada@hotmail.com',//finance
						410=>'fegarzada@hotmail.com',//new finance
						264=>'rahim.stanikzai@aop.gov.af',//it
						411=>'rahim.stanikzai@aop.gov.af',//new it
						79=>'sadaqatullah.sadiq@aop.gov.af',//procurement
						414=>'sadaqatullah.sadiq@aop.gov.af',//new procurement
						404=>'patyal.ghorzang@aop.gov.af',//npa, nezarat
						405=>'wais.rahimi@aop.gov.af',//npa, tashilat
						406=>'ahmad.naqshbendi@gmail.com',//npa, resource
						25=>'stimory@gmail.com',//ensejam deputy
						39=>'feridun.ilham@aop.gov.af',//agahi deputy
						58=>'Liaqat.Khaleeq@aop.gov.af',//npa
						44=>'a.attash@aop.gov.af',//mali deputy
						408=>'a.attash@aop.gov.af',//operation
						//ocs
						428=>'subhan.raouf@aop.gov.af',
						429=>'h.chakhansuri@gmail.com',
						430=>'whedayet@aop.gov.af',
						433=>'m.kochai@aop.gov.af',
						434=>'shafiq.ibrahimi@aop.gov.af',
						436=>'zxklmy@gmail.com',
						437=>'ahmadrashed.t@gmail.com',
						439=>'najiaanwari1@gmail.com'
									);
				$second_emails =array(
						28=>'naseer.amiri2000@gmail.com',
						33=>'hamedwally4@gmail.com',
						53=>'zakia_sabah@yahoo.com',
						89=>'sideqi2000@gmail.com',
						417=>'rahimullah.noory@gmail.com',
						41=>'hamida.hewad@gmail.com',
						174=>'ehsansaramad1370@gmail.com',
						175=>'niazmohammad.nori@gmail.com',
						6=>'deedar.stanikzay@gmail.com',
						409=>'deedar.stanikzay@gmail.com',
						45=>'abwahidpaiman@gmail.com',
						410=>'abwahidpaiman@gmail.com',
						76=>'nasim.sidiqiorg@yahoo.com',
						413=>'nasim.sidiqiorg@yahoo.com',
						79=>'h.saborqayoumi@gmail.com',
						414=>'h.saborqayoumi@gmail.com',
						264=>'wali.ahmadzai@aop.gov.af',
						411=>'wali.ahmadzai@aop.gov.af',
						269=>'abdulhamidhakimy8@gmail.com',
						412=>'abdulhamidhakimy8@gmail.com',
						404=>'mhhashimi@yahoo.com',
						405=>'najiba.nejati@aop.gov.af',
						406=>'saboor.safi@yahoo.com',
						//25=>'maryammasadiq786@gmail.com',
						58=>'nazari.arefa@yahoo.com',
						//44=>'sharifi.y@hotmail.com',
						//408=>'sharifi.y@hotmail.com',
						421=>'shabdesmusamem@gmail.com',
						422=>'wazhmaibrahimkhil2011@gmail.com',
						423=>'kazem.faiz@gmail.com',
						425=>'lailahamza08@gmail.com',
						428=>'zohal.azizy1@gmail.com',
						429=>'A.Nawabzada77@yahoo.com',
						430=>'marzia_naderi@yahoo.com',
						433=>'akram.jamshidi@aop.gov.af',
						434=>'wazhma.nasiri@aop.gov.af',
						436=>'yazdanpanah518@gmail.com',
						437=>'shabnam.zr95@gmail.com',
                        439=>'minarahmani440@gmail.com',
                        440=>'shir.jan@arg.gov.af',
						);

					$absent_list = array();
					foreach($result as $rec)
					{
						//is_in_thu_shift($rfid,$year,$month,0);
						$isInLeave = isEmployeeInLeave_today($rec->id,$the_date);
						if($isInLeave)
						{
							continue;
						}
						elseif($dayOfWeek == 'Thu')
						{
							if(is_in_thu_shift($rec->RFID,$the_sh_date[2],$the_sh_date[1],0))
							{
								continue;
							}
						}

						//insert absent list table
						hrOperation::checkEmployeeSeqAbsent($rec->id,$the_date);
						$absent_list[] = array('employee_id'=>$rec->id,'name'=>$rec->name,'last_name'=>$rec->last_name,'date'=>$the_date,'dep_id'=>$rec->department);

						if(array_key_exists($rec->department,$dirs_emails))
						{
							$directorates[$rec->department]['email']=$dirs_emails[$rec->department];
						}
						else
						{
							$directorates[$rec->department]['email']='no_email';
						}
						$directorates[$rec->department][] = array('name'=>$rec->name,'last_name'=>$rec->last_name,'dep'=>$rec->department);
						//emails to excutive manager
						if (array_key_exists($rec->department,$second_emails))
						{
							$executives[$rec->department]['email']=$second_emails[$rec->department];
						}
						else
						{
							$executives[$rec->department]['email']='no_email';
						}
						$executives[$rec->department][] = array('name'=>$rec->name,'last_name'=>$rec->last_name,'dep'=>$rec->department);

						$email = explode('@',$rec->email);
						if($rec->email!='' && count($email)==2)
						{
							if($user_dep_type==2)
							{//AOP
								$user_email = array('email'=>$rec->email,'name'=>$rec->name,'last_name'=>$rec->last_name,'the_date'=>$shamsi_date);
								Mail::send('emails.absent_afternoon', $user_email, function($message) use ($user_email){
										    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
											$message->to($user_email['email'])->subject('Absent Alert');
										});
							}
							else
							{//ocs
								$user_email = array('email'=>$rec->email,'name'=>$rec->name,'last_name'=>$rec->last_name,'the_date'=>$shamsi_date);
								Mail::send('emails.absent_afternoon', $user_email, function($message) use ($user_email){
										    $message->from('e.attendance.aop@gmail.com', 'OCS Attendance Department');
											$message->to($user_email['email'])->subject('Absent Alert');
										});
							}
						}
						else
						{
							continue;
						}

                    }
					// Check if we have absend today
					if(count($absent_list)>0)
					{
	                    // Insert employees in to sequence table whomea are absend today
						hrOperation::insertRecord('employee_seq_absents',$absent_list);
					}
					//sent email for sequence absent
					$seq_absent_emps = hrOperation::bringSeqAbsentEmps();
					if($seq_absent_emps)
					{
								if($user_dep_type==2)
									$mail_to = 'jamal.yousufii@gmail.com';
								else
									$mail_to = 'ssamimullah@yahoo.com';

								$user_email = array('email'=>$mail_to,'emps'=>$seq_absent_emps);
								Mail::send('emails.sequence_absent', $user_email, function($message) use ($user_email,$mail_to){
											  $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
												$message->to($mail_to)->subject('Absent Alert');
										     //$message->cc($user_email['email']);
								});

								//Update statuse of USERS whome sent email
								hrOperation::updateAbsentUserStatus($seq_absent_emps);
					}

				if(count($directorates)>0)
				{
					foreach($directorates as $array)
					{//loop for every direcotrate
						if($array['email']=='no_email')
						{
							continue;
						}
						else
						{
							$user_email = array('email'=>$array['email'],'emps'=>$array,'the_date'=>$shamsi_date);
							Mail::send('emails.dir_absent_afternoon', $user_email, function($message) use ($user_email){
								    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
									$message->to($user_email['email'])->subject('Absent Alert');
									//$message->cc($user_email['email']);
								});
						}
					}
				}
				if(count($executives)>0)
				{
					foreach($executives as $executive)
					{//loop for every direcotrate
						if($executive['email']=='no_email')
						{
							continue;
						}
						else
						{
							$user_email = array('email'=>$executive['email'],'emps'=>$executive,'the_date'=>$shamsi_date);
							Mail::send('emails.dir_absent_afternoon', $user_email, function($message) use ($user_email){
								    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
									$message->to($user_email['email'])->subject('Absent Alert');
									//$message->cc($user_email['email']);
								});
						}
					}
				}

		    return 'ایمیل به اشخاص غیر حاضر ارسال گردید.'; 		
  		}
	}
	public function sendAbsent_emails_old()
	{
		$year = date('Y');
		$month= date('m');
		$day =  date('d');
		$shamsi = dateToShamsi($year,$month,$day);
		$shamsi_month = explode('-',$shamsi);
		$s_month = $shamsi_month[1];
		$s_year = $shamsi_month[0];
		$att_time_in = getAttTime($s_year,$s_month,'in');

		$result = hrOperation::get_todays_absent_old($att_time_in);

		$directorates=array();
		$dirs_emails =array(33=>'bismellah.haidary@gmail.com',
							180=>'najiaanwari1@gmail.com',
							23=>'whedayet@arg.gov.af',
							78=>'rahim.stanikzai@aop.gov.af',
							41=>'miagulwaseeq@gmail.com',
							28=>'def.sec@aop.gov.af',
							45=>'fegarzada@hotmail.com',
							58=>'Liaqat.Khaleeq@aop.gov.af',
							54=>'shakib.oaa@gmail.com',
							226=>'malikzay@gmail.com',
							176=>'wais.rahimi@aop.gov.af',
							174=>'s.hafiz.fayaz@gmail.com'
							);
		$dep_id=0;
		foreach($result as $rec)
		{
			$time_h = substr($rec->time, 0, -4);
			$time_m = substr($rec->time, 2, -2);
			$time_s = substr($rec->time, 4, 2);
			$time = $time_h.':'.$time_m.':'.$time_s;
			if (array_key_exists($rec->department,$dirs_emails))
			{
				$directorates[$rec->department]['email']=$dirs_emails[$rec->department];
			}
			else
			{
				$directorates[$rec->department]['email']='no_email';
			}
			$directorates[$rec->department][] = array('name'=>$rec->name,'last_name'=>$rec->last_name,'time'=>$time,'dep'=>$rec->department);

			$user_email = array('email'=>$rec->email,'name'=>$rec->name,'last_name'=>$rec->last_name,'time'=>$time);
			Mail::send('emails.absent_old', $user_email, function($message) use ($user_email){
					    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
						$message->to($user_email['email'])->subject('Absent Alert');
					});
		}
		if(count($directorates)>0)
		{
			foreach($directorates as $array)
			{//loop for every direcotrate
				if($array['email']=='no_email')
				{
					continue;
				}
				else
				{
					$user_email = array('email'=>$array['email'],'emps'=>$array);
					Mail::send('emails.dir_absent_old', $user_email, function($message) use ($user_email){
						    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
							$message->to($user_email['email'])->subject('Absent Alert');
							//$message->cc($user_email['email']);
						});
				}
			}
		}
	}
	public function attendance_chart()
	{
		$data['parentDeps'] = getDepartmentWhereIn();
		return View::make('hr.attendance.charts',$data);
	}

	public function generate_att_chart()
	{
		//validate fields
		$validates = Validator::make(Input::all(), array(
			"from_date"		=> "required",
			"to_date"		=> "required"
		));

		//check the validation
		if($validates->fails())
		{
			return Redirect::route('attendance_chart')->withErrors($validates)->withInput();
		}
		else
		{
			$data['s_date'] = Input::get('from_date');
			$data['e_date'] = Input::get('to_date');
			$data['gen_dep'] = Input::get('general_department');
			$data['sub_dep'] = Input::get('sub_dep');
			$data['type'] = Input::get('type');

			$sdate = explode('-',Input::get('from_date'));
			$edate = explode('-',Input::get('to_date'));
			$month = $sdate[1];$year = $sdate[2];

			$from = dateToMiladi($sdate[2],$sdate[1],$sdate[0]);
			$to = dateToMiladi($edate[2],$edate[1],$edate[0]);
			if($to<=$from)
			{
				$validates->errors()->add('to_date', 'To Date must be bigger than From Date');
				return Redirect::route('attendance_chart')->withErrors($validates)->withInput();
			}
			$begin = new DateTime($from);
			$end = new DateTime($to);
			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);
			//create the all monthDay values for where: /2015/1216|/2015/1217
			$days = array();$day_counter=0;
			$thu_days = array();
			$allHolidays = hrOperation::getAllHolidays($from,$to);
			$new_array=array();
			foreach($allHolidays as $array)
			{
			    foreach($array as $val)
			    {
			        array_push($new_array, $val);
			    }
			}
			if(Input::get('type')==0)
			{//chart by percentage
				foreach($period As $day)
				{
					$the_day = $day->format( "Y-m-d" );
					$period_det = explode('-',$the_day);
					$period_day = $period_det[2];
					$period_month = $period_det[1];
					$period_year = $period_det[0];
					$day_code = date("w", strtotime($the_day));
					//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
					//$urgents = checkUrgentHoliday($the_day);
					$holiday = in_array($the_day, $new_array);
					if($holiday || $day_code == 5)
					{
						//$dayTotal++;//calculate the day urgent,leave or fridays as present
						continue;
					}
					else
					{
						$day_counter++;//to calculate the everage
						$days[] = $period_year.'-'.$period_month.'-'.$period_day;
					}
				}

				if(Input::get('sub_dep')!='')
				{//only one directorate
					$totalEmp_dep = hrOperation::getEmployeeDep(Input::get('sub_dep'));//number of employees who signes the att
					if($totalEmp_dep)
					{
						if($days!=null)
						{
							$total =DB::connection('hr')
								->table('attendance_images')
								->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
								->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
								//->whereIn('e.department', [41,49,174,175])
								->where('e.department',Input::get('sub_dep'))
								->where('e.employee_type','!=',2)
								->where('status',0)//present
								->whereIn('date',$days)//check the year/monthDay in image path
								->groupBy('groupByMe')
								->groupBy('attendance_images.RFID')
								->having('total', '>', 1)
								->get();
								//->toSql();dd($total);
						}

						$result = 0 ;
						if(isset($total))
						{
							$result+=count($total);
						}
						//$presents=$result+$thuTotal+$dayTotal;
						$presents = $result/$day_counter;//everage
						$present_perc = (100*$presents)/$totalEmp_dep->total;//percentage
						$data['present_totals'] = $present_perc;
						$data['absent_totals'] = 100-$present_perc;
					}
					else
					{//directorate does not have any attendance employee
						$data['present_totals'] = 0;
						$data['absent_totals'] = 100;
					}
					$direc=hrOperation::get_dep_name(Input::get('sub_dep'));
					if($direc)
					{
						$direc = $direc->name;
					}
					else
					{
						$direc = 'NA';
					}
					$direc=str_replace(",","-",$direc);
					$data['display'] = "'".$direc."',";

				}
				elseif(Input::get('sub_dep')=='' && Input::get('general_department')!='')
				{//all directorates of selected deputy
					$dirs = getRelatedSubDepartment(Input::get('general_department'));
					$present_totals= '';
					$absent_totals= '';
					$directorates='';
					foreach($dirs as $dir)
					{
						$totalEmp_dep = hrOperation::getEmployeeDep($dir->id);
						if($totalEmp_dep)
						{
							if($days!=null)
							{
								$total =DB::connection('hr')
									->table('attendance_images')
									->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
									->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
									->where('e.employee_type','!=',2)
									->where('e.department',$dir->id)
									->where('status',0)//present

									->whereIn('date',$days)
									->groupBy('groupByMe')
									->groupBy('attendance_images.RFID')
									->having('total', '>', 1)
									->get();
									//->toSql();dd($total);
							}

							$result = 0 ;
							if(isset($total))
							{
								$result+=count($total);
							}
							if(isset($thu_total))
							{
								$result+=count($thu_total);
							}
							//$presents=$result+$thuTotal+$dayTotal;
							$presents = $result/$day_counter;
							$present_perc = (100*$presents)/$totalEmp_dep->total;
							$present_totals.= $present_perc.',';
							$absent_totals.= 100-$present_perc.',';

						}
						else
						{
							$present_totals.= '0,';
							$absent_totals.= '100,';
						}
						$direc=hrOperation::get_dep_name($dir->id)->name;
						$direc=str_replace(",","-",$direc);
						$directorates.= "'".$direc."',";
					}
					$directorates = substr($directorates, 0, -1);
					$present_totals = substr($present_totals, 0, -1);
					$absent_totals = substr($absent_totals, 0, -1);
					$data['display'] = $directorates;
					$data['present_totals'] = $present_totals;
					$data['absent_totals'] = $absent_totals;
				}
				elseif(Input::get('general_department')=='')
				{//all deputies
					$dirs = getDepartmentWhereIn();
					$present_totals= '';
					$absent_totals= '';
					$directorates='';
					foreach($dirs as $dir)
					{
						$totalEmp_dep = hrOperation::getEmployeeGen_Dep($dir->id);
						if($totalEmp_dep)
						{
							if($days!=null)
							{
								$total =DB::connection('hr')
									->table('attendance_images')
									->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
									->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
									->where('e.employee_type','!=',2)
									->where('e.general_department',$dir->id)
									->where('status',0)//present

									->whereIn('date',$days)
									->groupBy('groupByMe')
									->groupBy('attendance_images.RFID')
									->having('total', '>', 1)
									->get();
									//->toSql();dd($total);
							}

							$result = 0 ;
							if(isset($total))
							{
								$result+=count($total);
							}
							if(isset($thu_total))
							{
								$result+=count($thu_total);
							}
							//$presents=$result+$thuTotal+$dayTotal;
							$presents = $result/$day_counter;
							//$totalEmp_dep = hrOperation::getEmployeeDep($dir->id);
							//dd($totalEmp_dep[0]->total);
							$present_perc = (100*$presents)/$totalEmp_dep->total;
							$present_totals.= $present_perc.',';
							$absent_totals.= 100-$present_perc.',';
						}
						else
						{
							$present_totals.= '0,';
							$absent_totals.= '100,';
						}
						$direc=hrOperation::get_dep_name($dir->id)->name;
						$direc=str_replace(",","-",$direc);
						$directorates.= "'".$direc."',";
					}
					$directorates = substr($directorates, 0, -1);
					$present_totals = substr($present_totals, 0, -1);
					$absent_totals = substr($absent_totals, 0, -1);
					$data['display'] = $directorates;
					$data['present_totals'] = $present_totals;
					$data['absent_totals'] = $absent_totals;
				}
				//dd($directorates);

				return View::make('hr.attendance.generateCharts_percentage',$data);
			}
			else
			{//charts by number
				if(Input::get('sub_dep')!='')
				{//only one directorate
					$present_totals= '';
					$absent_totals= '';
					$directorates='';
					$totalEmp_dep = hrOperation::getEmployeeDep(Input::get('sub_dep'));//number of employees who signes the att
					foreach($period As $day)
					{
					    $total=0;$thu_total=0;
						$the_day = $day->format( "Y-m-d" );
						$period_det = explode('-',$the_day);
						$period_day = $period_det[2];
						$period_month = $period_det[1];
						$period_year = $period_det[0];
						$day_code = date("w", strtotime($the_day));
						//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
						//$urgents = checkUrgentHoliday($the_day);
						$holiday = in_array($the_day, $new_array);
						if($holiday || $day_code == 5)
						{
							//$dayTotal++;//calculate the day urgent,leave or fridays as present
							continue;
						}
						else
						{//normal days
							if($totalEmp_dep)
							{

									$total =DB::connection('hr')
										->table('attendance_images')
										->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
										->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
										->where('e.employee_type','!=',2)
										->where('e.department',Input::get('sub_dep'))
										->where('status',0)//present

										->where('date',$the_day)//check the year/monthDay in image path
										->groupBy('groupByMe')
										->groupBy('attendance_images.RFID')
										->having('total', '>', 1)
										->get();
										//->toSql();dd($total);



								$result = 0 ;
								if($total!=0)
								{
									$result+=count($total);
								}

								//$presents=$result;
								//$presents = $result/$day_counter;//everage
								//$present_perc = (100*$presents)/$totalEmp_dep->total;//percentage
								$present_totals.= $result.',';
								$absent_totals.= $totalEmp_dep->total-$result.',';
							}
							else
							{//directorate does not have any attendance employee
								$present_totals.= '0,';
								$absent_totals.= '0,';
							}
							$s_date = explode('-', $the_day);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);
							$directorates.= "'".$sdate."',";
						}
					}
					$data['direc']=hrOperation::get_dep_name(Input::get('sub_dep'))->name;
					$data['total']=$totalEmp_dep;
					$directorates = substr($directorates, 0, -1);
					$present_totals = substr($present_totals, 0, -1);
					$absent_totals = substr($absent_totals, 0, -1);
					$data['display'] = $directorates;
					$data['present_totals'] = $present_totals;
					$data['absent_totals'] = $absent_totals;

				}
				elseif(Input::get('sub_dep')=='' && Input::get('general_department')!='')
				{//all directorates of selected deputy
					$dirs = getRelatedSubDepartment(Input::get('general_department'));
					$all_dirs_data = array();
					foreach($dirs as $dir)
					{
					    $present_totals= '';
						$absent_totals= '';
						$directorates='';//dates
						$totalEmp_dep = hrOperation::getEmployeeDep($dir->id);//number of employees who signes the att
						foreach($period As $day)
						{
						    $total=0;$thu_total=0;
							$the_day = $day->format( "Y-m-d" );
							$period_det = explode('-',$the_day);
							$period_day = $period_det[2];
							$period_month = $period_det[1];
							$period_year = $period_det[0];
							$day_code = date("w", strtotime($the_day));
							//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
							//$urgents = checkUrgentHoliday($the_day);
							$holiday = in_array($the_day, $new_array);
							if($holiday || $day_code == 5)
							{
								//$dayTotal++;//calculate the day urgent,leave or fridays as present
								continue;
							}
							else
							{//normal days
								if($totalEmp_dep)
								{

										$total =DB::connection('hr')
											->table('attendance_images')
											->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
											->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
											->where('e.employee_type','!=',2)
											->where('e.department',$dir->id)
											->where('status',0)//present

											->where('date',$the_day)//check the year/monthDay in image path
											->groupBy('groupByMe')
											->groupBy('attendance_images.RFID')
											->having('total', '>', 1)
											->get();
											//->toSql();dd($total);



									$result = 0 ;
									if($total!=0)
									{
										$result+=count($total);
									}

									//$presents=$result;
									//$presents = $result/$day_counter;//everage
									//$present_perc = (100*$presents)/$totalEmp_dep->total;//percentage
									$present_totals.= $result.',';
									$absent_totals.= $totalEmp_dep->total-$result.',';
								}
								else
								{//directorate does not have any attendance employee
									$present_totals.= '0,';
									$absent_totals.= '0,';
								}
								$s_date = explode('-', $the_day);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);
								$directorates.= "'".$sdate."',";
							}
						}
						$direc=hrOperation::get_dep_name($dir->id)->name;
						$data['total']=$totalEmp_dep;
						$directorates = substr($directorates, 0, -1);
						$present_totals = substr($present_totals, 0, -1);
						$absent_totals = substr($absent_totals, 0, -1);
						$all_dirs_data[]=array('dir_total'=>$totalEmp_dep,'dir'=>$direc,'display'=>$directorates,'present_totals'=>$present_totals,'absent_totals'=>$absent_totals);

						//$data['display'] = $directorates;
						//$data['present_totals'] = $present_totals;
						//$data['absent_totals'] = $absent_totals;
					}
					//dd($all_dirs_data);
					$data['all_charts']=$all_dirs_data;
				}
				elseif(Input::get('general_department')=='')
				{//all deputies
					$dirs = getDepartmentWhereIn();
					$all_dirs_data = array();
					foreach($dirs as $dir)
					{
					    $present_totals= '';
						$absent_totals= '';
						$directorates='';//dates
						$totalEmp_dep = hrOperation::getEmployeeGen_Dep($dir->id);//number of employees who signes the att
						foreach($period As $day)
						{
						    $total=0;$thu_total=0;
							$the_day = $day->format( "Y-m-d" );
							$period_det = explode('-',$the_day);
							$period_day = $period_det[2];
							$period_month = $period_det[1];
							$period_year = $period_det[0];
							$day_code = date("w", strtotime($the_day));
							//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
							//$urgents = checkUrgentHoliday($the_day);
							$holiday = in_array($the_day, $new_array);
							if($holiday || $day_code == 5)
							{
								//$dayTotal++;//calculate the day urgent,leave or fridays as present
								continue;
							}
							else
							{//normal days
								if($totalEmp_dep)
								{

										$total =DB::connection('hr')
											->table('attendance_images')
											->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
											->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
											->where('e.employee_type','!=',2)
											->where('e.general_department',$dir->id)
											->where('status',0)//present

											->where('date',$the_day)//check the year/monthDay in image path
											->groupBy('groupByMe')
											->groupBy('attendance_images.RFID')
											->having('total', '>', 1)
											->get();
											//->toSql();dd($total);



									$result = 0 ;
									if($total!=0)
									{
										$result+=count($total);
									}

									//$presents=$result;
									//$presents = $result/$day_counter;//everage
									//$present_perc = (100*$presents)/$totalEmp_dep->total;//percentage
									$present_totals.= $result.',';
									$absent_totals.= $totalEmp_dep->total-$result.',';
								}
								else
								{//directorate does not have any attendance employee
									$present_totals.= '0,';
									$absent_totals.= '0,';
								}
								$s_date = explode('-', $the_day);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);
								$directorates.= "'".$sdate."',";
							}
						}
						$direc=hrOperation::get_dep_name($dir->id)->name;
						$data['total']=$totalEmp_dep;
						$directorates = substr($directorates, 0, -1);
						$present_totals = substr($present_totals, 0, -1);
						$absent_totals = substr($absent_totals, 0, -1);
						$all_dirs_data[]=array('dir_total'=>$totalEmp_dep,'dir'=>$direc,'display'=>$directorates,'present_totals'=>$present_totals,'absent_totals'=>$absent_totals);

						//$data['display'] = $directorates;
						//$data['present_totals'] = $present_totals;
						//$data['absent_totals'] = $absent_totals;
					}
					//dd($all_dirs_data);
					$data['all_charts']=$all_dirs_data;
				}
				return View::make('hr.attendance.generateCharts',$data);
			}
		}
	}
	public function generate_att_chart_percentage()
	{
		//validate fields
		$validates = Validator::make(Input::all(), array(
			"from_date"		=> "required",
			"to_date"		=> "required"
		));

		//check the validation
		if($validates->fails())
		{
			return Redirect::route('attendance_chart')->withErrors($validates)->withInput();
		}
		else
		{
			$data['s_date'] = Input::get('from_date');
			$data['e_date'] = Input::get('to_date');
			$data['gen_dep'] = Input::get('general_department');
			$data['sub_dep'] = Input::get('sub_dep');
			$sdate = explode('-',Input::get('from_date'));
			$edate = explode('-',Input::get('to_date'));
			$month = $sdate[1];$year = $sdate[2];
			$att_time_in = getAttTime($year,$month,'in');
			$att_time_out = getAttTime($year,$month,'out');
			$att_time_in_thu = getAttTime_thu($year,$month,'in');
			$att_time_out_thu = getAttTime_thu($year,$month,'out');

			$from = dateToMiladi($sdate[2],$sdate[1],$sdate[0]);
			$to = dateToMiladi($edate[2],$edate[1],$edate[0]);
			if($to<=$from)
			{
				$validates->errors()->add('to_date', 'To Date must be bigger than From Date');
				return Redirect::route('attendance_chart')->withErrors($validates)->withInput();
			}
			$begin = new DateTime($from);
			$end = new DateTime($to);
			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);
			//create the all monthDay values for where: /2015/1216|/2015/1217
			$days = array();$day_counter=0;
			$thu_days = array();
			$allHolidays = hrOperation::getAllHolidays($from,$to);
			$new_array=array();
			foreach($allHolidays as $array)
			{
			    foreach($array as $val)
			    {
			        array_push($new_array, $val);
			    }
			}
			foreach($period As $day)
			{
				$the_day = $day->format( "Y-m-d" );
				$period_det = explode('-',$the_day);
				$period_day = $period_det[2];
				$period_month = $period_det[1];
				$period_year = $period_det[0];
				$day_code = date("w", strtotime($the_day));
				//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
				//$urgents = checkUrgentHoliday($the_day);
				$holiday = in_array($the_day, $new_array);
				if($holiday || $day_code == 5)
				{
					//$dayTotal++;//calculate the day urgent,leave or fridays as present
					continue;
				}
				else
				{
					$day_counter++;//to calculate the everage
					if($day_code != 4)//dont include thu here as the time differs
					{
						//$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
						$days[] = $period_year.'-'.$period_month.'-'.$period_day;
					}
					else
					{//thursdays
						//$thu_days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
						$thu_days[] = $period_year.'-'.$period_month.'-'.$period_day;
					}
				}
			}

			if(Input::get('sub_dep')!='')
			{//only one directorate
				$totalEmp_dep = hrOperation::getEmployeeDep(Input::get('sub_dep'));//number of employees who signes the att
				if($totalEmp_dep)
				{
					if($days!=null)
					{
						$total =DB::connection('hr')
							->table('attendance_images')
							->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
							->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
							//->whereIn('e.department', [41,49,174,175])
							->where('e.department',Input::get('sub_dep'))
							->where('status',0)//present
							->whereRaw('CASE
										WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
							//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
							//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
							->whereIn('date',$days)//check the year/monthDay in image path
							->groupBy('groupByMe')
							->groupBy('attendance_images.RFID')
							->having('total', '>', 1)
							->get();
							//->toSql();dd($total);
					}
					if($thu_days!=null)
					{
						$thu_total=DB::connection('hr')
							->table('attendance_images')
							->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
							->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
							->where('e.department',Input::get('sub_dep'))
							->where('status',0)//present
							->whereRaw('CASE
										WHEN time <= 120000 THEN time<='.$att_time_in_thu.' ELSE time>='.$att_time_out_thu.' END')
							//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
							//->whereRaw('path REGEXP "'.$thu_days.'"')//check the year/monthDay in image path
							->whereIn('date',$thu_days)
							->groupBy('groupByMe')
							->groupBy('attendance_images.RFID')
							->having('total', '>', 1)
							->get();
							//->toSql();dd($total);
					}

					$result = 0 ;
					if(isset($total))
					{
						$result+=count($total);
					}
					if(isset($thu_total))
					{
						$result+=count($thu_total);
					}
					//$presents=$result+$thuTotal+$dayTotal;
					$presents = $result/$day_counter;//everage
					$present_perc = (100*$presents)/$totalEmp_dep->total;//percentage
					$data['present_totals'] = $present_perc;
					$data['absent_totals'] = 100-$present_perc;
				}
				else
				{//directorate does not have any attendance employee
					$data['present_totals'] = 0;
					$data['absent_totals'] = 100;
				}
				$direc=hrOperation::get_dep_name(Input::get('sub_dep'))->name;
				$direc=str_replace(",","-",$direc);
				$data['display'] = "'".$direc."',";

			}
			elseif(Input::get('sub_dep')=='' && Input::get('general_department')!='')
			{//all directorates of selected deputy
				$dirs = getRelatedSubDepartment(Input::get('general_department'));
				$present_totals= '';
				$absent_totals= '';
				$directorates='';
				foreach($dirs as $dir)
				{
					$totalEmp_dep = hrOperation::getEmployeeDep($dir->id);
					if($totalEmp_dep)
					{
						if($days!=null)
						{
							$total =DB::connection('hr')
								->table('attendance_images')
								->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
								->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
								//->whereIn('e.department', [41,49,174,175])
								->where('e.department',$dir->id)
								->where('status',0)//present
								->whereRaw('CASE
											WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
								//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
								//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
								->whereIn('date',$days)
								->groupBy('groupByMe')
								->groupBy('attendance_images.RFID')
								->having('total', '>', 1)
								->get();
								//->toSql();dd($total);
						}
						if($thu_days!=null)
						{
							$thu_total=DB::connection('hr')
								->table('attendance_images')
								->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
								->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
								->where('e.department',$dir->id)
								->where('status',0)//present
								->whereRaw('CASE
											WHEN time <= 120000 THEN time<='.$att_time_in_thu.' ELSE time>='.$att_time_out_thu.' END')
								//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
								//->whereRaw('path REGEXP "'.$thu_days.'"')//check the year/monthDay in image path
								->whereIn('date',$thu_days)
								->groupBy('groupByMe')
								->groupBy('attendance_images.RFID')
								->having('total', '>', 1)
								->get();
								//->toSql();dd($total);
						}

						$result = 0 ;
						if(isset($total))
						{
							$result+=count($total);
						}
						if(isset($thu_total))
						{
							$result+=count($thu_total);
						}
						//$presents=$result+$thuTotal+$dayTotal;
						$presents = $result/$day_counter;
						$present_perc = (100*$presents)/$totalEmp_dep->total;
						$present_totals.= $present_perc.',';
						$absent_totals.= 100-$present_perc.',';

					}
					else
					{
						$present_totals.= '0,';
						$absent_totals.= '100,';
					}
					$direc=hrOperation::get_dep_name($dir->id)->name;
					$direc=str_replace(",","-",$direc);
					$directorates.= "'".$direc."',";
				}
				$directorates = substr($directorates, 0, -1);
				$present_totals = substr($present_totals, 0, -1);
				$absent_totals = substr($absent_totals, 0, -1);
				$data['display'] = $directorates;
				$data['present_totals'] = $present_totals;
				$data['absent_totals'] = $absent_totals;
			}
			elseif(Input::get('general_department')=='')
			{//all deputies
				$dirs = getDepartmentWhereIn();
				$present_totals= '';
				$absent_totals= '';
				$directorates='';
				foreach($dirs as $dir)
				{
					$totalEmp_dep = hrOperation::getEmployeeGen_Dep($dir->id);
					if($totalEmp_dep)
					{
						if($days!=null)
						{
							$total =DB::connection('hr')
								->table('attendance_images')
								->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
								->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
								//->whereIn('e.department', [41,49,174,175])
								->where('e.general_department',$dir->id)
								->where('status',0)//present
								->whereRaw('CASE
											WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
								//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
								//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
								->whereIn('date',$days)
								->groupBy('groupByMe')
								->groupBy('attendance_images.RFID')
								->having('total', '>', 1)
								->get();
								//->toSql();dd($total);
						}
						if($thu_days!=null)
						{
							$thu_total=DB::connection('hr')
								->table('attendance_images')
								->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
								->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
								->where('e.general_department',$dir->id)
								->where('status',0)//present
								->whereRaw('CASE
											WHEN time <= 120000 THEN time<='.$att_time_in_thu.' ELSE time>='.$att_time_out_thu.' END')
								//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
								//->whereRaw('path REGEXP "'.$thu_days.'"')//check the year/monthDay in image path
								->whereIn('date',$thu_days)
								->groupBy('groupByMe')
								->groupBy('attendance_images.RFID')
								->having('total', '>', 1)
								->get();
								//->toSql();dd($total);
						}

						$result = 0 ;
						if(isset($total))
						{
							$result+=count($total);
						}
						if(isset($thu_total))
						{
							$result+=count($thu_total);
						}
						//$presents=$result+$thuTotal+$dayTotal;
						$presents = $result/$day_counter;
						//$totalEmp_dep = hrOperation::getEmployeeDep($dir->id);
						//dd($totalEmp_dep[0]->total);
						$present_perc = (100*$presents)/$totalEmp_dep->total;
						$present_totals.= $present_perc.',';
						$absent_totals.= 100-$present_perc.',';
					}
					else
					{
						$present_totals.= '0,';
						$absent_totals.= '100,';
					}
					$direc=hrOperation::get_dep_name($dir->id)->name;
					$direc=str_replace(",","-",$direc);
					$directorates.= "'".$direc."',";
				}
				$directorates = substr($directorates, 0, -1);
				$present_totals = substr($present_totals, 0, -1);
				$absent_totals = substr($absent_totals, 0, -1);
				$data['display'] = $directorates;
				$data['present_totals'] = $present_totals;
				$data['absent_totals'] = $absent_totals;
			}
			//dd($directorates);

			return View::make('hr.attendance.generateCharts',$data);
		}
	}
	public function getAllPositions()
	{
		return View::make('hr.attendance.localization');
	}
	public function getAllPositionsData()
	{
		$object = hrOperation::getEmployees_positions();
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name_dr',
							'dep_name',
							'position_dr',
							'position_en'
							)
				->addColumn('operations', function($option){
					return '<a href="javascript:void()" onclick="load_position_edit('.$option->id.');" class="table-link" style="text-decoration:none;" data-target="#salary_modal" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
				})

				->make();
		return $result;
	}
	public function editPositionModal()
	{
		$data['id'] = Input::get('id');

		$data['details'] = hrOperation::getEmployeeById(Input::get('id'));

		return View::make('hr.attendance.edit_position',$data);
	}
	public function postEmployeePosition()
	{
		hrOperation::update_record('employees',array('current_position_en'=>Input::get('name_en')),array('id'=>Input::get('employee_id')));
		hrOperation::insertLog('employees',1,'Eng position of given employee',Input::get('employee_id'));
	}

	public function test_email()
	{
		$user_email = array('email'=>'ali.panahi@aop.gov.af','name'=>'ali hussain','last_name'=>'panahi','the_date'=>'1395-6-6');
		Mail::send('emails.absent_afternoon', $user_email, function($message) use ($user_email){
									    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
										$message->to($user_email['email'])->subject('Absent Alert');
									});
	}
	public function view_leaveForm()
	{
		return View::make('hr.attendance.view_leaveForm');
	}
	public function downloadLeaveForm()
	{
	    $file= public_path(). "/documents/hr_attachments/leave_form.doc";
	    //download file
	    return Response::download($file, 'leave_form.doc');
	}
	public function downloadForm($file_name='')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		if($user_dep_type==2)
		{
	    	$file= public_path(). "/documents/forms/".$file_name;
	    }
	    else
	    {
	    	$file= public_path(). "/documents/forms/ocs/".$file_name;
	    }
	    //download file
	    return Response::download($file, $file_name);
	}
	public function leaveFormAdd($notification_id=0)
	{//dd(Auth::user());
		if($notification_id!=0)
		{//if user comes from notification bar, then update the status of notification to read
			hrOperation::update_record('notifications',array('status'=>1),array('id'=>$notification_id,'to'=>Auth::user()->employee_id));
		}
		if(Auth::user()->position_id==2)
		{//director view
			//$emp_id = getEmployeeId(Auth::user()->id);
			$sh_date = dateToShamsi(date('Y'),date('m'),date('d'));
			$year = explode('-', $sh_date);
			$year = $year[0];//current shamsi year
			$data['details'] = hrOperation::getDir_pendingLeaves($year);

			return View::make('hr.attendance.add_leave_form',$data);
		}
		elseif(canAttHoliday())
		{
			$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
			$sh_date = dateToShamsi(date('Y'),date('m'),date('d'));
			$year = explode('-', $sh_date);
			$year = $year[0];//current shamsi year
			if($user_dep_type==1)
			{//ocs attendance head
				$data['details'] = hrOperation::getOCS_pendingLeaves($year);
			}
			else
			{
				$data['details'] = hrOperation::getAOP_pendingLeaves($year);
			}
			return View::make('hr.attendance.add_leave_form',$data);
		}
		elseif(canCheckAtt())
		{//normal user
			$emp_id = getEmployeeId(Auth::user()->id);
			$sh_date = dateToShamsi(date('Y'),date('m'),date('d'));
			$year = explode('-', $sh_date);
			$year = $year[0];//current shamsi year
			$data['details'] = hrOperation::getEmployeeLeaves($emp_id,$year,0);
			return View::make('hr.attendance.add_leave_form',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function processLeaveDir()
	{
		$id = Input::get('id');
		$type = Input::get('type');
		$emp_id = hrOperation::getEmpDetByLeaveID($id);
		if($type==3)
		{//attendance head approve
			hrOperation::update_record('leaves',array('hr_approved'=>1,'hr_approved_at'=>date('Y-m-d H:i:s')),array('id'=>$id));
			$data = array('to'=>$emp_id->employee_id,'type'=>3,'content'=>'درخواست رخصتی شما توسط آمریت حاضری تایید گردید','created_at'=>date('Y-m-d H:i:s'));
			hrOperation::insertRecord('notifications',$data);

			return 'موفقانه تایید گردید';
		}
		else
		{//dir approve and reject
			hrOperation::update_record('leaves',array('dir_approved'=>$type,'dir_approved_at'=>date('Y-m-d H:i:s')),array('id'=>$id));
			if($type==2)
			{//rejected
				$data = array('to'=>$emp_id->employee_id,'type'=>1,'content'=>'درخواست رخصتی شما توسط ریاست رد گردید','created_at'=>date('Y-m-d H:i:s'));
				hrOperation::insertRecord('notifications',$data);
				return 'موفقانه رد گردید';
			}
			else
			{//approved
				$data = array('to'=>$emp_id->employee_id,'type'=>2,'content'=>'درخواست رخصتی شما توسط ریاست تایید گردید','created_at'=>date('Y-m-d H:i:s'));
				hrOperation::insertRecord('notifications',$data);
				//attendance head notify
				$data = array('to'=>156,'type'=>2,'content'=>'درخواست رخصتی توسط ریاست تایید گردید','created_at'=>date('Y-m-d H:i:s'));
				hrOperation::insertRecord('notifications',$data);
				return 'موفقانه تایید گردید';
			}
		}
	}
	public function uploadCardPhoto($doc_id=0)
	{
		// getting all of the post data
		$file = Input::file('profile_pic');
		$errors = "";
		if(Input::hasFile('profile_pic'))
		{
				// validating each file.
				$rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
				$validator = Validator::make(

						[
									'file' => $file,
									'extension'  => Str::lower($file->getClientOriginalExtension()),
							],
							[
									'file' => 'required|max:500000',
									'extension'  => 'required|in:jpg,jpeg,png,gif'
							]
					);

				if($validator->passes())
				{
					$thumb = Image::make($file);

					if(Input::get('x')!=null) {
							$thumb->crop(Input::get('w'), Input::get('h'), Input::get('x'), Input::get('y'));
					}
					else if($thumb->width()>2000) {
							$thumb->resize(725, null, function ($constraint) {
									$constraint->aspectRatio();
							});
					}

					// path is root/uploads
					$destinationPath = 'documents/profile_pictures';
					$filename = $file->getClientOriginalName();

					$temp = explode(".", $filename);
					$extension = end($temp);
					// $lastFileId = getLastDocId();

					// $lastFileId++;

					$filename = 'profile_'.$doc_id.'.'.$extension;

					$upload_success = $file->move($destinationPath, $filename);
					$thumb->save($destinationPath.'/'.$filename);

					if($upload_success)
					{
						$file= public_path(). "/documents/profile_pictures/small_".$filename;
 					 	File::delete($file);

 				 		DB::connection('hr')->table('employees')->where('id',$doc_id)->update(array('photo'=>$filename));
					}
					else
					{
						 $errors .= json('error', 400);
					}


				}
				else
				{
					// redirect back with errors.
					return Redirect::back()->withErrors($validator);
				}
		}
	}

	 public function getEmpDetailsByTashkil()
	 {
			 $tashkil_id = Input::get('tashkil_id');
			 $emp_details = hrOperation::where('tashkil_id',$tashkil_id)->first();
			 return ' اسم : '.$emp_details->name_dr.'  ولد :‌ '. $emp_details->father_name_dr;
	 }

}
?>
