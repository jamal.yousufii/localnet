<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SectionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name_en'       => 'required',
            'name_dr'       => 'required',
            'name_pa'       => 'required',
            'module_id'     => 'required',
            'department_id' => 'required',
            'code'          => 'required',
            'url_route'     => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name_dr.required'      => 'نام بخش به دری ضروری میباشد*',
            'name_pa.required'      => 'نام بخش به پشتو ضروری میباشد*',
            'name_en.required'      => 'نام بخش به انگلیسی ضروری میباشد*',
            'module_id.required'    => 'انتخاب آپلیکیشن ضروری میباشد.',
            'code.required'         => 'کودبخش ضروری میباشد.',
            'department_id.required'=> 'انتخاب دیپارتمنت ضروری میباشد*',
            'url_route.required'    => 'URL ضروری میباشد.',
        ];
    }

     
}
