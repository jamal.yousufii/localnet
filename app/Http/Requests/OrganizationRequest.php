<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class OrganizationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_dr'  => 'required', 
            'name_pa'  => 'required', 
            'name_en'  => 'required', 
            'org_code' => 'required', 
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name_dr.required' => 'نام اورگان به دری ضروری میباشد.',
            'name_dr.required'  => 'نام اورگان به پشتو ضروری میباشد.',
            'name_en.required'  => 'نام اورگان به انگلیسی ضروری میباشد.',
            'org_code.required' => 'کود اورگان ضروری میباشد.',
        ];
    }
}
