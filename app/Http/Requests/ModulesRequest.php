<?php
namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class ModulesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_dr'  => 'required', 
            'name_pa'  => 'required', 
            'name_en'  => 'required', 
            'code'     => 'required', 
            'url'      => 'required', 
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name_dr.required'  => 'نام آپلیکیشن به دری ضروری میباشد.',
            'name_dr.required'  => 'نام آپلیکیشن به پشتو ضروری میباشد.',
            'name_en.required'  => 'نام آپلیکیشن به انگلیسی ضروری میباشد.',
            'code.required'     => 'کود آپلیکیشن ضروری میباشد.',
            'url.required'      => 'URL ضروری میباشد.',
        ];
    }
}
