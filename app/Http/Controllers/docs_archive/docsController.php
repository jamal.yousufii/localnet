<?php 

namespace App\Http\Controllers\docs_archive;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\models\docs_archive\docsModel;
use Illuminate\Support\Collection;
use App\library\jdatetime;
use View;
use DB;
use Validator;
use Input;
use Redirect;
use Response;
use Datatable;
use Session;
use DateTime;
use DateInterval;
use File;
use Excel;
use Crypt;		
use Illuminate\Support\Str;
use URL;
use Carbon\Carbon;

class docsController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		if(!Auth::check())
		{
			return Redirect::route('getLogin');
		}	
	}
//================================================= Archive Management Controller Functions ============================
	// get the list of documents with datatables.
	public function getDocs()
	{
		$data['records'] = docsModel::getArchivedDocuments();
		if(Input::get('ajax') == 1)
		{
			return View::make('docs_archive.docs_list_ajax',$data);
		}
		else
		{
			//load view to show searchpa result
			return View::make('docs_archive.docs_list',$data);
		}
	}

	public function getLoadInsertPage()
	{
		return view('docs_archive.insert_doc');
	}

	public function getSearchedDocs()
	{
		//dd($_POST);
		$data['records'] = docsModel::getSearchedData("");
		return view('docs_archive.search_result',$data);
	}

	public function exportDocsToExcel()
	{
		//dd($_POST);
		$results = docsModel::getSearchedData('print');
		//dd($searchInventories);
		$curr_date = date('Y-m-d');
		Excel::load('excel_template/incoming_documents_report.xlsx', function($file) use($results){
			//Excel::create('Filename', function($file) use($results){			
			$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($results){	
				$row = 4;
				$sheet->setFreeze('A4');
				if(!empty($results))
				{
					foreach($results AS $item)
					{
						$sheet->getStyle('A4:T' . $sheet->getHighestRow())->getAlignment()->setWrapText(true); 

						$summary = preg_replace("/&nbsp;/",' ',$item->summary);
						$committee_comment = preg_replace("/&nbsp;/",' ',$item->committee_comment);
						$hedayat_muqam_aali = preg_replace("/&nbsp;/",' ',$item->hedayat_muqam_aali);
						$considerations = preg_replace("/&nbsp;/",' ',$item->considerations);

						$sheet->setHeight($row, 20);

						$sheet->setCellValue('A'.$row.'',$row-3);
						$sheet->setCellValue('B'.$row.'',$item->sender);
						$sheet->setCellValue('C'.$row.'',$item->doc_number);
						$sheet->setCellValue('D'.$row.'',dmy_format(toJalali($item->doc_date)));
						$sheet->setCellValue('E'.$row.'',$item->document_type);
						$sheet->setCellValue('F'.$row.'',$item->internal_sender_related_deputy);
						$sheet->setCellValue('G'.$row.'',$item->internal_sender_related_directorate);
						$sheet->setCellValue('H'.$row.'',$item->internal_sender_related_doc_number);
						$sheet->setCellValue('I'.$row.'',dmy_format(toJalali($item->internal_sender_related_doc_date)));
						$sheet->setCellValue('J'.$row.'',$item->internal_sender_related_doc_type);
						$sheet->setCellValue('K'.$row.'',strip_tags($summary));
						$sheet->setCellValue('L'.$row.'',$item->incoming_number);
						$sheet->setCellValue('M'.$row.'',dmy_format(toJalali($item->incoming_date)));
						$sheet->setCellValue('N'.$row.'',strip_tags($committee_comment));
						$sheet->setCellValue('O'.$row.'',$item->execution);
						$sheet->setCellValue('P'.$row.'',$item->hedayat_number);
						$sheet->setCellValue('Q'.$row.'',strip_tags($hedayat_muqam_aali));
						$sheet->setCellValue('R'.$row.'',dmy_format(toJalali($item->execution_date)));
						$sheet->setCellValue('S'.$row.'',strip_tags($considerations));
						if(!empty(getDocsArchiveFileName($item->id)))
							$sheet->setCellValue('T'.$row.'',"ضمیمه دارد باید در سیستم چک شوید");
						else
							$sheet->setCellValue('T'.$row.'',"ضمیمه ندارد");
						
						$row++;
					}
				}

				$sheet->setBorder('A4:T'.($row-1).'', 'thin');
				
    		});
			
			})->export('xlsx');
	}

	public function addDoc()
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"doc_type"						=> "required",
			"doc_number"					=> "required"
			));

		if($validates->fails())
		{	
			return Redirect::route('getDocsList')->withErrors($validates)->withInput();
		}
		else
		{
	    	//dd($_POST);
		    //check the date type if it's shamsi or miladi.
			$doc_date = toGregorian(ymd_format(Input::get('doc_date')));
			$internal_sender_related_doc_date = toGregorian(ymd_format(Input::get('internal_sender_related_doc_date')));
			$incoming_date = toGregorian(ymd_format(Input::get('incoming_date')));
			$execution_date = toGregorian(ymd_format(Input::get('execution_date')));
			$execution = Input::get("execution");
			if($execution == 5){
				$hedayat_number = Input::get("hedayat_number");
				$hedayat_muqam_aali = Input::get("hedayat_muqam_aali");
			}else {
				$hedayat_number = "";
				$hedayat_muqam_aali = "";
			}

		    // get the form data.
		    $data = array(
		    		"sender"									=> Input::get('sender'),
		    		"doc_number"								=> Input::get('doc_number'),
		    		"doc_date"									=> $doc_date,
		    		"doc_type"									=> Input::get('doc_type'),
		    		"internal_sender_related_deputy"			=> Input::get('internal_sender_related_deputy'),
		    		"internal_sender_related_directorate"		=> Input::get('internal_sender_related_directorate'),
		    		"internal_sender_related_doc_number"		=> Input::get('internal_sender_related_doc_number'),
		    		"internal_sender_related_doc_date"			=> $internal_sender_related_doc_date,
		    		"internal_sender_related_doc_type"			=> Input::get('internal_sender_related_doc_type'),
		    		"summary"									=> Input::get('summary'),
		    		"incoming_number"							=> Input::get('incoming_number'),
		    		"incoming_date"								=> $incoming_date,
		    		"committee_comment"							=> Input::get('committee_comment'),
		    		"execution"									=> Input::get('execution'),
		    		"hedayat_number"							=> $hedayat_number,
		    		"hedayat_muqam_aali"						=> $hedayat_muqam_aali,
		    		"execution_date"							=> $execution_date,
		    		"considerations"							=> Input::get('considerations'),
		    		"user_id"									=> Auth::user()->id,
		    		"created_at"								=> date('Y-m-d H:i:s')
		    	);
		    $insertedRecordID = docsModel::addRecord($data);
		    if($insertedRecordID){
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 1;
				$file_data = array();

				if(Input::hasFile('files'))
				{				
					foreach($files as $file) 
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension())
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    // path is root/uploads
						    $destinationPath = 'aop_docs_archive_uploads/';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);
						    			  			    
					    	$lastFileId = $insertedRecordID;
							if($auto == 1)
						    $filename = "Attachment_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Attachment_".$lastFileId."_".$auto.".".$extension;
							
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success) 
						    {
							    $data = array(
						   					'file_name'				=> $filename,
						   					'original_file_name'	=> $original_filename,
						   					'doc_id'				=> $insertedRecordID,
						   					'user_id'				=> Auth::user()->id,
						   					"created_at"			=> date('Y-m-d H:i:s')
						   				);
						    	//call the model function to insert the data into upload table.
						    	docsModel::uploadFiles($data);
							} 
							else 
							{
							    // redirect back with errors.
					    		return Redirect::route('getDocsList')->withErrors($validator);
							}
						}
						else
						{
							// redirect back with errors.
					    	return Redirect::route('getDocsList')->withErrors($validator);
						}
						$auto ++;
					}
				}
		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'docs',
					'record_id'=>$insertedRecordID,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				docsModel::addLog($Log);
		    	return Redirect::route('getDocsList')->with("success","معلومات موفقانه در سیستم اضافه گردید !");
	        }
	        else
	        {
	            return Redirect::route('getDocsList')->with("fail","مشکل وجود دارد، لطفآ همرای مسؤل سیستم به تماس شوید.");
	        }
	    }
    }

	public function loadEditDocPage($doc_id)
	{
		$record_id = Crypt::decrypt($doc_id);
		$data['record'] = docsModel::getDocDetails($record_id);
		return view('docs_archive.edit_doc', $data);
	}

	public function updateDoc($record_id)
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"doc_type"						=> "required",
			"doc_number"					=> "required"
			));

		if($validates->fails())
		{	
			return Redirect::route('getDocsList')->withErrors($validates)->withInput();
		}
		else
		{
			//dd($_POST);
		    //check the date type if it's shamsi or miladi.
			$doc_date = toGregorian(ymd_format(Input::get('doc_date')));
			$internal_sender_related_doc_date = toGregorian(ymd_format(Input::get('internal_sender_related_doc_date')));
			$incoming_date = toGregorian(ymd_format(Input::get('incoming_date')));
			$execution_date = toGregorian(ymd_format(Input::get('execution_date')));
			$execution = Input::get("execution");
			if($execution == 5){
				$hedayat_number = Input::get("hedayat_number");
				$hedayat_muqam_aali = Input::get("hedayat_muqam_aali");
			}else {
				$hedayat_number = "";
				$hedayat_muqam_aali = "";
			}

		    $data = array(
		    		"sender"									=> Input::get('sender'),
		    		"doc_number"								=> Input::get('doc_number'),
		    		"doc_date"									=> $doc_date,
		    		"doc_type"									=> Input::get('doc_type'),
		    		"internal_sender_related_deputy"			=> Input::get('internal_sender_related_deputy'),
		    		"internal_sender_related_directorate"		=> Input::get('internal_sender_related_directorate'),
		    		"internal_sender_related_doc_number"		=> Input::get('internal_sender_related_doc_number'),
		    		"internal_sender_related_doc_date"			=> $internal_sender_related_doc_date,
		    		"internal_sender_related_doc_type"			=> Input::get('internal_sender_related_doc_type'),
		    		"summary"									=> Input::get('summary'),
		    		"incoming_number"							=> Input::get('incoming_number'),
		    		"incoming_date"								=> $incoming_date,
		    		"committee_comment"							=> Input::get('committee_comment'),
		    		"execution"									=> Input::get('execution'),
		    		"hedayat_number"							=> $hedayat_number,
		    		"hedayat_muqam_aali"						=> $hedayat_muqam_aali,
		    		"execution_date"							=> $execution_date,
		    		"considerations"							=> Input::get('considerations'),
		    		"user_id"									=> Auth::user()->id,
		    		"updated_at"				=> date('Y-m-d H:i:s')
		    	);
		    $updated = docsModel::updateRecord($data,$record_id);
		    if($updated){
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 1;
				$file_data = array();

				if(Input::hasFile('files'))
				{				
					foreach($files as $file) 
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension())
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    // path is root/uploads
						    $destinationPath = 'aop_docs_archive_uploads/';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);
						    			  			    
					    	$lastFileId = $record_id;
							$filename = "Attachment_".$lastFileId."_edited_".$auto.".".$extension;
							
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success) 
						    {
							    $data = array(
						   					'file_name'				=> $filename,
						   					'original_file_name'	=> $original_filename,
						   					'doc_id'				=> $record_id,
						   					'user_id'				=> Auth::user()->id,
						   					"created_at"			=> date('Y-m-d H:i:s')
						   				);
						    	//call the model function to insert the data into upload table.
						    	docsModel::uploadFiles($data);
							} 
							else 
							{
							    // redirect back with errors.
					    		return Redirect::route('getDocsList')->withErrors($validator);
							}
						}
						else
						{
							// redirect back with errors.
					    	return Redirect::route('getDocsList')->withErrors($validator);
						}
						$auto ++;
					}

				}
		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'docs',
					'record_id'=>$record_id,
					'user' => Auth::user()->username,
					'action' => "Updated",
					'created_at' => date('Y-m-d H:i:s')
					);
				docsModel::addLog($Log);
		    	return Redirect::route('getDocsList')->with("success","معلومات موفقانه اصلاح گردید !");
	        }
	        else
	        {
	            return Redirect::route('getDocsList')->with("fail","مشکل وجود دارد، لطفآ همرای مسؤل سیستم به تماس شوید.");
	        }
	    }
	}

	//change the status of record based on it's id to 1 if it's deleted.
	public function getDeleteDoc()
	{
		$id = Input::get('record_id');
		//$id = Crypt::decrypt($id);
		$deleted = docsModel::deleteDocInfo($id);
		//dd($deleted);
		if($deleted)
	    {
	    	$removed = docsModel::removeDocFiles($id);
    		$file_info = docsModel::getDocFilesName($id);
    		if(!empty($file_info))
    		{
	    		foreach ($file_info as $value) {
	    			$file_name = $value->file_name;
	    			File::move(public_path()."/aop_docs_archive_uploads/$file_name", public_path()."/aop_docs_archive_deleted_uploads/$file_name");
	    		}
    		}
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'docs',
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			docsModel::addLog($Log);
    		echo "<div class='alert alert-success'>جزئیات سند موفقانه حذف گردید !</div>";
        }
	}

	public function downloadDocFile($doc_id)
	{
		$file_id = Crypt::decrypt($doc_id);
		//get file name from database
		$fileName = docsModel::getFileName($file_id);
		//dd($fileName->original_file_name);
        //public path for file
        $file= public_path(). "/aop_docs_archive_uploads/".$fileName->file_name;
        //download file
        return Response::download($file, $fileName->original_file_name);
	}

	public function removeDocumentFile()
	{
		$file_id = Input::get('file_id');
		$file_name = Input::get('file_name');
		$deleted = docsModel::removeFile($file_id);
		if($deleted)
		{
			File::delete(public_path()."/aop_docs_archive_uploads/$file_name");
			return 1;
		}
	}

	// ================================================================= Document Type Management functions ===================

	public function loadDocTypeList()
	{
		return view('docs_archive.document_type_list');
	}

	public function getDocTypeList()
	{
		$doc_type = docsModel::getDocumentType();
		$collectin = new Collection($doc_type);
		return \Datatable::collection($collectin)
			->showColumns('id','name','created_at','updated_at')
			->addColumn('actions', function($option){
				$options = '';
				$record_id = Crypt::encrypt($option->id);
				if(isAdmin())
					$options .= '<a href="'.route('editDocType',$record_id).'" class="btn btn-warning" title="Edit Record"><i class="fa fa-edit"></i></a> | ';
					$options .= '<button type="button" onclick="delteDocType(this.id)" id="'.$option->id.'" class="btn btn-warning" title="Delete Record"><i class="fa fa-remove"></i></button> ';
				return $options;
			})
			//->setSearchWithAlias()
			//->searchColumns('id','full_name','father_name','username','email','position','dep_name')
			->make();
	}
	public function addDocumentType()
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"name"		=> "required"
			));

		if($validates->fails())
		{	
			return Redirect::route('loadDocTypeList')->withErrors($validates)->withInput();
		}
		else
		{
		    // get the form data.
		    $data = array(
		    		"name"						=> Input::get('name'),
		    		"user_id"					=> Auth::user()->id,
		    		"created_at"				=> date('Y-m-d H:i:s')
		    	);
		    $inserted = docsModel::addDocTypeRecord($data);
		    if($inserted)
		    {
		    	return Redirect::route('loadDocTypeList')->with('success','نوعیت سند موفقانه اضافه گردید !');
		    }
		}
	}
	public function loadEditDocTypePage($id)
	{
		$id = Crypt::decrypt($id);
		$data['record'] = docsModel::getDocType($id);
		return view('docs_archive.edit_doc_type',$data);
	}
	public function updateDocumentType($id)
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"name"		=> "required"
			));

		if($validates->fails())
		{	
			return Redirect::route('loadDocTypeList')->withErrors($validates)->withInput();
		}
		else
		{
		    // get the form data.
		    $data = array(
		    		"name"						=> Input::get('name'),
		    		"user_id"					=> Auth::user()->id,
		    		"updated_at"				=> date('Y-m-d H:i:s')
		    	);
		    $updated = docsModel::updateDocType($id,$data);
		    if($updated)
		    {
		    	return Redirect::route('loadDocTypeList')->with('success','نوعیت سند موفقانه اصلاح شد !');
		    }
		}
	}

	public function getDeleteDocType()
	{
		$id = Input::get('record_id');
		$deleted = docsModel::deleteDocType($id);
		if($deleted)
	    {
    		echo "<div class='alert alert-success'>نوعیت سند موفقانه از سیستم حذف گردید !</div>";
        }
	}

	// ================================================================= Document Category Management functions ===================

	public function loadDocSourcesList()
	{
		return view('docs_archive.sourceslist');
	}

	public function getSourcesList()
	{
		$sources = docsModel::getSources();
		$collectin = new Collection($sources);
		return \Datatable::collection($collectin)
			->showColumns('id','sender','internal_sender_related_deputy','internal_sender_related_directorate','created_at')
			->addColumn('actions', function($option){
				$options = '';
				$record_id = Crypt::encrypt($option->id);
				if(isAdmin())
					$options .= '<a href="'.route('editSources',$record_id).'" class="btn btn-warning" title="Edit Record"><i class="fa fa-edit"></i></a> | ';
					$options .= '<button type="button" onclick="deleteDocSource(this.id)" id="'.$option->id.'" class="btn btn-warning" title="Delete Record"><i class="fa fa-remove"></i></button> ';
				return $options;
			})
			//->setSearchWithAlias()
			//->searchColumns('id','full_name','father_name','username','email','position','dep_name')
			->make();
	}
	public function addDocSource()
	{
	    // get the form data.
	    $data = array(
	    		"sender"												=> Input::get('sender'),
	    		"internal_sender_related_deputy"						=> Input::get('internal_sender_related_deputy'),
	    		"internal_sender_related_directorate"					=> Input::get('internal_sender_related_directorate'),
	    		"created_at"											=> date("Y-m-d H:i:s")
	    	);
	    $inserted = docsModel::addDocSource($data);
	    if($inserted)
	    {
	    	return Redirect::route('loadSourcesList')->with('success','موفقانه در سیستم اضافه گردید !');
	    }
	}
	public function loadEditDocSourcePage($id)
	{
		$id = Crypt::decrypt($id);
		$data['record'] = docsModel::getDocSource($id);
		return view('docs_archive.editSource',$data);
	}
	public function updateDocumentSource($id)
	{
	    // get the form data.
	    $data = array(
	    		"sender"												=> Input::get('sender'),
	    		"internal_sender_related_deputy"						=> Input::get('internal_sender_related_deputy'),
	    		"internal_sender_related_directorate"					=> Input::get('internal_sender_related_directorate'),
	    		"created_at"											=> date("Y-m-d H:i:s")
	    	);
	    $updated = docsModel::updateDocSource($id,$data);
	    if($updated)
	    {
	    	return Redirect::route('loadSourcesList')->with('success','مرجع مربوطه موفقانه اصلاح گردید !');
	    }
	}

	public function getDeleteDocSource()
	{
		$id = Input::get('record_id');
		$deleted = docsModel::deleteDocSource($id);
		if($deleted)
	    {
    		echo "<div class='alert alert-success'>مرجع مربوطه موفقانه حذف گردید !</div>";
        }
	}

}

?>