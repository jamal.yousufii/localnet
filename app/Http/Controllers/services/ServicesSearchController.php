<?php 

namespace App\Http\Controllers\services;

use App\Http\Controllers\Controller;
use App\models\services\ServiceSearch;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class ServicesSearchController extends Controller
{
	
	//database connection
	public static $myDb = "services";

	//Load vehicle list view
	public function getList()
	{
				
		$data["items"] = DB::connection("services")->table("items")->get();
		return view("services.search.list",$data);
	}
	
	//get datatable json data
	public function getData()
	{

		//get data from model
		$apps = ServiceInvitation::getData();
		$collection = new Collection($apps);
		return \Datatable::collection($collection)
					->showColumns(
									'id',
									'date',
									'time',
									'type',
									'number_of_guest',
									'maktob_number',
									'location',
									'source'
									)
					->addColumn('operation',function($option)
						{
							$options = '';
							
							$options .= '<a href="'.route('getEditServiceInvitation',$option->id).'"><i class="icon wb-edit" aria-hidden="true"></i></a> &nbsp;';
						
							$options .= '|&nbsp;<a href="'.route('getDeleteServiceFees9',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="icon wb-trash" aria-hidden="true"></i></a>';
						
							return $options;
						}
					)
					
					->make();
	}
	
	/*
	getting form for inserting
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getSearchResult()
	{
		$search_category = Input::get("category_type");
		
		if($search_category == 1){
			//get search result from services employees
			$data["rows"] = ServiceSearch::getEmployeeSearchResult();
			return View::make('services.search.search_employee_result',$data);
		}else if($search_category == 2){
			//get search result from services invitations
			$data["rows"] = ServiceSearch::getInvitationSearchResult();
			return View::make('services.search.search_invitation_result',$data);
		}else if($search_category == 3){
			//get search result from services fees9
			$data["rows"] = ServiceSearch::getFees9SearchResult();
			return View::make('services.search.search_fees9_result',$data);
		}
		
	}
	
	public function insert()
	{
		//echo "<pre>";print_r($_POST);exit;
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "type" 			=> "required",
	        "maktob_number" 		=> "required"	    
	        ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateServiceInvitation")->withErrors($validates)->withInput();
	    }
	    else
	    {
	    	
	    	//echo "<pre>";print_r($_POST);exit;
	        
	        //create an object
	        $object = new ServiceInvitation();
	        $object->type = Input::get("type");
	        $object->time = Input::get("time");
	        $object->description = Input::get("description");
	        $object->location = Input::get("location");
	        $object->maktob_number = Input::get("maktob_number");
	        $object->source = Input::get("source");
	        $object->number_of_guest = Input::get("number_of_guest");
	        //check date setting
			if(isMiladiDate())
			{
				$object->date = toJalali(Input::get("date"));

			}
			else
			{
				$object->date = ymd_format(Input::get("date"));

			}
			
	        
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	        	
	            return \Redirect::route("getServiceInvitationList")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getServiceInvitationList")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0)
    {
    	
    	//get data
    	$data['row'] = ServiceInvitation::getDetails($id);
    	
    	//get employee attachments
    	$data["attachments"] = DB::connection('services')->table('attachments')->where('record_id', $id)->where('table',"service_invitation")->get();
    
    	
    	return View::make("services.invitation.edit",$data);
    	
    }
    
    public function update($id=0)
    {
    	
    	//create an object
        $object = ServiceInvitation::find($id);
        $object->type = Input::get("type");
        $object->time = Input::get("time");
        $object->description = Input::get("description");
        $object->location = Input::get("location");
        $object->maktob_number = Input::get("maktob_number");
        $object->source = Input::get("source");
        $object->number_of_guest = Input::get("number_of_guest");
        //check date setting
		if(isMiladiDate())
		{
			$object->date = toJalali(Input::get("date"));

		}
		else
		{
			$object->date = ymd_format(Input::get("date"));

		}
		
        $object->updated_at = date("Y-m-d H:i:s");
	        
        if($object->save())
        {
        	
            return \Redirect::route("getServiceInvitationList")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getServiceInvitationList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function getDelete($id=0)
    {
    	//get all record attachemts
    	$attachments = DB::connection("services")->table("attachments")->where("record_id",$id)->where("table","service_fees9")->get();
    	if($attachments)
    	{
	    	DB::connection("services")->table("fees9_items")->where("fees9_id",$id)->delete();
	    	//delete record
	    	$deleted = ServiceFees9::getDelete($id);
    	}
    	
    	if($deleted)
        {
        	
            return \Redirect::route("getServiceInvitationList")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getServiceInvitationList")->with("fail","An error occured plase try again.");
        }
    	
    }
    //upload document
	public function uploadDocs($doc_id=0)
	{
		
		// getting all of the post data
		$files = Input::file('files');
		$errors = "";
		$file_data = array();

		foreach($files as $file) 
		{
			
			if(Input::hasFile('files'))
			{
			  // validating each file.
			  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			  $validator = Validator::make(

			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'file' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png,doc,docx,zip,rar,pdf,rtf,xlsx,xls,txt'
			        ]
			  	);

			  if($validator->passes())
			  {
			  	
			    // path is root/uploads
			    $destinationPath = 'documents/services_attachments';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    
			    $lastFileId = DB::connection('services')->table('attachments')->orderBy('id', 'desc')->pluck('id');
			  
			    $lastFileId++;
			    
			    $filename = 'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
				   
				   $data = array(
				   					'record_id'=>$doc_id,
				   					'file_name'=>$filename,
				   					'created_at'=>date('Y-m-d H:i:s'),
				   					'table' => "service_invitation",
				   					'user_id'	=> Auth::user()->id
				   				);
				   

				   if(count($data)>0)
					{
						DB::connection("services")->table("attachments")->insert($data);
					}
					
					
				   //return Response::json('success', 200);
				   
				} 
				else 
				{
				   $errors .= json('error', 400);
				}

		    
			  } 
			  else 
			  {
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			  }
			}

		}
		
		return \Redirect::route("getEditServiceInvitation",$doc_id)->with("success","Attachments successfully uploaded.");
		
	}
	
	//remove file from folder
	public function deleteFile($fileName="",$fileId=0,$doc_id=0)
	{
		
		
		//get file name from database
		$file= public_path(). "/documents/services_attachments/".$fileName;
		if(File::delete($file))
		{
			//delete from database
			DB::connection('services')
			->table('attachments')
			->where('id',$fileId)
			->delete();
			
			return \Redirect::route("getEditServiceInvitation",$doc_id)->with("success","Attachments successfully deleted.");
		
		}
		else
		{
			return \Redirect::route("getEditServiceInvitation",$doc_id)->with("fail","Some error!");
		}
		
	}
	
	
}

?>