<?php 

namespace App\Http\Controllers\services;

use App\Http\Controllers\Controller;
use App\models\services\ServiceFees9;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class ServicesFees9Controller extends Controller
{
	
	//database connection
	public static $myDb = "services";

	//Load vehicle list view
	public function getList()
	{

		return view("services.fees9.list");
	}
	
	//get datatable json data
	public function getData()
	{

		//get data from model
		$apps = ServiceFees9::getData();
		$collection = new Collection($apps);
		return \Datatable::collection($collection)
					->showColumns(
									'id',
									'form_number',
									'date',
									'type',
									'item_count',
									'created_at'
									)
					->addColumn('operation',function($option)
						{
							$options = '';
							
							$options .= '<a href="'.route('getEditServiceFees9',$option->id).'"><i class="icon wb-edit" aria-hidden="true"></i></a> &nbsp;';
						
							$options .= '|&nbsp;<a href="'.route('getDeleteServiceFees9',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="icon wb-trash" aria-hidden="true"></i></a>';
						
							return $options;
						}
					)
					
					->make();
	}
	
	/*
	getting form for inserting
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreate()
	{
		
		$data["items"] = DB::connection("services")->table("items")->get();
		//load view for inserting
		return View::make("services.fees9.insert",$data);
		
	}
	
	public function insert()
	{
		//echo "<pre>";print_r($_POST);exit;
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "form_number" 			=> "required",
	        "type" 		=> "required"	    
	        ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateServiceFees9")->withErrors($validates)->withInput();
	    }
	    else
	    {
	    	
	    	//echo "<pre>";print_r($_POST);exit;
	        
	        //create an object
	        $object = new ServiceFees9();
	        $object->form_number = Input::get("form_number");
	        $object->type = Input::get("type");
	        //check date setting
			if(isMiladiDate())
			{
				$object->date = toJalali(Input::get("date"));

			}
			else
			{
				$object->date = ymd_format(Input::get("date"));

			}
			
			if(Input::has("daily_specail")){
				$object->daily_specail = Input::get("daily_specail");
			}
			if(Input::has("daily_employee")){
				$object->daily_employee = Input::get("daily_employee");
			}
			if(Input::has("daily_employee_3")){
				$object->daily_employee_3 = Input::get("daily_employee_3");
			}
			if(Input::has("other")){
				$object->other = Input::get("other");
			}
	        
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	        	$form_id = $object->id;
	        	
	        	//insert fees9 items
	        	$items = Input::get("items");
	        	$units = Input::get("unit");
	        	$amounts = Input::get("amount");
	        	
	        	$items_data = array();
	        	
	        	for($i=1;$i<count($items);$i++){
	        		
	        		$data = array(
	        				"fees9_id" => $form_id,
	        				"item_id"  => $items[$i],
	        				"amount"   => $amounts[$i],
	        				"unit"     => $units[$i]
	        			);
	        		
	        		$items_data[] = $data;
	        	}
	        	DB::connection("services")->table("fees9_items")->insert($items_data);

	            return \Redirect::route("getServiceFees9List")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getServiceFees9List")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0)
    {
    	
    	//get data
    	$data['row'] = ServiceFees9::getDetails($id);
    	
    	//get selected items
    	$data['selected_items'] = ServiceFees9::getFees9Items($id);
    	
    	//get all items
    	$data["items"] = DB::connection("services")->table("items")->get();
    	
    	//get employee attachments
    	$data["attachments"] = DB::connection('services')->table('attachments')->where('record_id', $id)->where('table',"service_fees9")->get();
    
    	
    	return View::make("services.fees9.edit",$data);
    	
    }
    
    public function update($id=0)
    {
    	
    	//create an object
        $object = ServiceFees9::find($id);
        $object->form_number = Input::get("form_number");
        $object->type = Input::get("type");
        //check date setting
		if(isMiladiDate())
		{
			$object->date = toJalali(Input::get("date"));

		}
		else
		{
			$object->date = ymd_format(Input::get("date"));

		}
		
		
		$object->daily_specail = Input::get("daily_specail");
	
		$object->daily_employee = Input::get("daily_employee");
	
		$object->daily_employee_3 = Input::get("daily_employee_3");
	
		$object->other = Input::get("other");
		
        $object->updated_at = date("Y-m-d H:i:s");
	        
        if($object->save())
        {
        	
        	$form_id = $id;
	        	
        	//insert fees9 items
        	$items = Input::get("items");
        	$units = Input::get("unit");
        	$amounts = Input::get("amount");
        	
        	$items_data = array();
        	
        	for($i=1;$i<count($items);$i++){
        		
        		$data = array(
        				"fees9_id" => $form_id,
        				"item_id"  => $items[$i],
        				"amount"   => $amounts[$i],
        				"unit"     => $units[$i]
        			);
        		
        		$items_data[] = $data;
        	}
        	
        	if(DB::connection("services")->table("fees9_items")->where("fees9_id",$id)->delete()){
        		DB::connection("services")->table("fees9_items")->insert($items_data);
        	}
	        	
            return \Redirect::route("getServiceFees9List")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getServiceFees9List")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function getDelete($id=0)
    {
    	
    	$deleted = true;
    	//get all record attachemts
    	$attachments = DB::connection("services")->table("attachments")->where("record_id",$id)->where("table","service_fees9")->get();
    	
    	DB::connection("services")->table("fees9_items")->where("fees9_id",$id)->delete();
    	//delete record
    	$deleted = ServiceFees9::getDelete($id);
    	
    	
    	if($deleted)
        {
        	
            return \Redirect::route("getServiceFees9List")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getServiceFees9List")->with("fail","An error occured plase try again.");
        }
    	
    }
    //upload document
	public function uploadDocs($doc_id=0)
	{
		
		// getting all of the post data
		$files = Input::file('files');
		$errors = "";
		$file_data = array();

		foreach($files as $file) 
		{
			
			if(Input::hasFile('files'))
			{
			  // validating each file.
			  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			  $validator = Validator::make(

			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'file' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png,doc,docx,zip,rar,pdf,rtf,xlsx,xls,txt'
			        ]
			  	);

			  if($validator->passes())
			  {
			  	
			    // path is root/uploads
			    $destinationPath = 'documents/services_attachments';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    
			    $lastFileId = DB::connection('services')->table('attachments')->orderBy('id', 'desc')->pluck('id');
			  
			    $lastFileId++;
			    
			    $filename = 'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
				   
				   $data = array(
				   					'record_id'=>$doc_id,
				   					'file_name'=>$filename,
				   					'created_at'=>date('Y-m-d H:i:s'),
				   					'table' => "service_fees9",
				   					'user_id'	=> Auth::user()->id
				   				);
				   

				   if(count($data)>0)
					{
						DB::connection("services")->table("attachments")->insert($data);
					}
					
					
				   //return Response::json('success', 200);
				   
				} 
				else 
				{
				   $errors .= json('error', 400);
				}

		    
			  } 
			  else 
			  {
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			  }
			}

		}
		
		return \Redirect::route("getEditServiceFees9",$doc_id)->with("success","Attachments successfully uploaded.");
		
	}
	
	//remove file from folder
	public function deleteFile($fileName="",$fileId=0,$doc_id=0)
	{
		
		
		//get file name from database
		$file= public_path(). "/documents/services_attachments/".$fileName;
		if(File::delete($file))
		{
			//delete from database
			DB::connection('services')
			->table('attachments')
			->where('id',$fileId)
			->delete();
			
			return \Redirect::route("getEditServiceFees9",$doc_id)->with("success","Attachments successfully deleted.");
		
		}
		else
		{
			return \Redirect::route("getEditServiceFees9",$doc_id)->with("fail","Some error!");
		}
		
	}
	
	
}

?>