<?php 

namespace App\Http\Controllers\services;

use App\Http\Controllers\Controller;
use App\models\services\ServiceEmployees;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class ServicesEmpController extends Controller
{
	
	//database connection
	public static $myDb = "services";

	//Load vehicle list view
	public function getList()
	{

		return view("services.employee.list");
	}
	
	//get datatable json data
	public function getData()
	{

		//get data from model
		$apps = ServiceEmployees::getData();
		$collection = new Collection($apps);
		return \Datatable::collection($collection)
					->showColumns(
									'id',
									'name',
									'father_name',
									'position',
									'start_date',
									'end_date',
									'employee_id',
									'palace'
									)
					->addColumn('operation',function($option)
						{
							$options = '';
							
							$options .= '<a href="'.route('getEditServiceEmployee',$option->id).'"><i class="icon wb-edit" aria-hidden="true"></i></a> &nbsp;';
						
							$options .= '|&nbsp;<a href="'.route('getDeleteServiceEmp',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="icon wb-trash" aria-hidden="true"></i></a>';
						
							return $options;
						}
					)
					
					->make();
	}
	
	/*
	getting form for inserting
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreate()
	{
		
		//get all drivers
		$data['drivers'] = DB::connection("transport")->table("drivers")->get();
		//load view for inserting vehilce
		return View::make("services.employee.insert",$data);
		
	}
	
	public function insert()
	{
		//echo "<pre>";print_r($_POST);exit;
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "name" 			=> "required",
	        "ID_card" 		=> "required",
	        "palace_id" 	=> "required"
	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateServiceEmployee")->withErrors($validates)->withInput();
	    }
	    else
	    {
	        
	        //create an object
	        $object = new ServiceEmployees();
	        $object->name = Input::get("name");
	        $object->father_name = Input::get("father_name");
	        $object->position = Input::get("position");
	        //check date setting
			if(isMiladiDate())
			{
				$object->start_date = toJalali(Input::get("start_date"));
				$object->end_date 	= toJalali(Input::get("end_date"));
				
			}
			else
			{
				$object->start_date = ymd_format(Input::get("start_date"));
				$object->end_date 	= ymd_format(Input::get("end_date"));
				
			}
	        $object->employee_id = Input::get("ID_card");
	        $object->palace_id = Input::get("palace_id");
	        if(Input::get("is_general_manager")){
	        	$object->is_general_manager = Input::get("is_general_manager");
	        }
	        if(Input::get("is_palace_manager")){
	        	$object->is_palace_manager = Input::get("is_palace_manager");
	        }
	        $object->comment = Input::get("comment");
	        
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	            return \Redirect::route("getServiceEmpList")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getServiceEmpList")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0)
    {
    	
    	//get data
    	$data['row'] = ServiceEmployees::getDetails($id);
    	
    	//get employee comments
    	$data["comments"] = ServiceEmployees::getEmpComments($id);
    	
    	//get employee attachments
    	$data["attachments"] = DB::connection('services')->table('attachments')->where('record_id', $id)->where('table',"service_employees")->get();
    
    	return View::make("services.employee.edit",$data);
    	
    }
    
    public function update($id=0)
    {
    	
    	//create an object
        $object = ServiceEmployees::find($id);
        $object->name = Input::get("name");
        $object->father_name = Input::get("father_name");
        $object->position = Input::get("position");
        //check date setting
		if(isMiladiDate())
		{
			$object->start_date = toJalali(Input::get("start_date"));
			$object->end_date 	= toJalali(Input::get("end_date"));
			
		}
		else
		{
			$object->start_date = ymd_format(Input::get("start_date"));
			$object->end_date 	= ymd_format(Input::get("end_date"));
			
		}
        $object->employee_id = Input::get("ID_card");
        $object->palace_id = Input::get("palace_id");
        if(Input::get("is_general_manager")){
        	$object->is_general_manager = Input::get("is_general_manager");
        }
        if(Input::get("is_palace_manager")){
        	$object->is_palace_manager = Input::get("is_palace_manager");
        }
        $object->comment = Input::get("comment");
        $object->updated_at = date("Y-m-d H:i:s");
	        
        if($object->save())
        {
            return \Redirect::route("getServiceEmpList")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getServiceEmpList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function getDelete($id=0)
    {
    	
    	//get all record attachemts
    	$attachments = DB::connection("services")->table("attachments")->where("record_id",$id)->where("table","service_employees")->get();
    	if($attachments)
    	{
    		
    		foreach($attachments AS $item)
    		{
		    	//get file name from database
				$file= public_path(). "/documents/services_attachments/".$item->file_name;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('services')
					->table('attachments')
					->where('id',$item->id)
					->delete();
				}
    		}
    	}
    	
    	//delete employee comments
    	DB::connection("services")->table("employee_comments")->where("emp_id",$id)->delete();
    	
    	//delete record
    	$deleted = ServiceEmployees::getDelete($id);
    	
    	if($deleted)
        {
            return \Redirect::route("getServiceEmpList")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getServiceEmpList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function saveComment()
    {
    	//get posted data
    	$data = array(
    			"emp_id" => Input::get("emp_id"),
    			"comment" => Input::get("comment"),
    			"date" => date("Y-m-d H:i:s")
    		);
    	$id = DB::connection("services")->table("employee_comments")->insertGetId($data);
    	
    	return '<div class="content well" style="text-align: right;"><a href="javascript:void()" style="float: left;color: red;" onclick="removeComment('.$id.');"><i class="icon fa-close fa-lg" aria-hidden="true"></i></a>'.Input::get('comment').' <br>
			                        <small>Posted Date: '.date('Y-m-d').'</small></div>';
    }
    public function removeComment()
    {
    	
    	DB::connection("services")->table("employee_comments")->where("id",Input::get("comment"))->delete();
    	
    	return '<div class="alert alert-success"><i class="icon fa-check-circle fa-lg" aria-hidden="true"></i> Comment Removed Successfully!</div>';
    }
    
    //upload document
	public function uploadDocs($doc_id=0)
	{
		
		// getting all of the post data
		$files = Input::file('files');
		$errors = "";
		$file_data = array();

		foreach($files as $file) 
		{
			
			if(Input::hasFile('files'))
			{
			  // validating each file.
			  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			  $validator = Validator::make(

			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'file' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png,doc,docx,zip,rar,pdf,rtf,xlsx,xls,txt'
			        ]
			  	);

			  if($validator->passes())
			  {
			  	
			    // path is root/uploads
			    $destinationPath = 'documents/services_attachments';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    
			    $lastFileId = DB::connection('services')->table('attachments')->orderBy('id', 'desc')->pluck('id');
			  
			    $lastFileId++;
			    
			    $filename = 'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
				   
				   $data = array(
				   					'record_id'=>$doc_id,
				   					'file_name'=>$filename,
				   					'created_at'=>date('Y-m-d H:i:s'),
				   					'table' => "service_employees",
				   					'user_id'	=> Auth::user()->id
				   				);
				   

				   if(count($data)>0)
					{
						DB::connection("services")->table("attachments")->insert($data);
					}
					
					
				   //return Response::json('success', 200);
				   
				} 
				else 
				{
				   $errors .= json('error', 400);
				}

		    
			  } 
			  else 
			  {
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			  }
			}

		}
		
		return \Redirect::route("getEditServiceEmployee",$doc_id)->with("success","Attachments successfully uploaded.");
		
	}
	//download file from server
	public function downloadDoc($fileName='')
	{
		
        //public path for file
        $file= public_path(). "/documents/services_attachments/".$fileName;
        //download file
        return Response::download($file, $fileName);
	    
	}

	//remove file from folder
	public function deleteFile($fileName="",$fileId=0,$doc_id=0)
	{
		
		
		//get file name from database
		$file= public_path(). "/documents/services_attachments/".$fileName;
		if(File::delete($file))
		{
			//delete from database
			DB::connection('services')
			->table('attachments')
			->where('id',$fileId)
			->delete();
			
			return \Redirect::route("getEditServiceEmployee",$doc_id)->with("success","Attachments successfully deleted.");
		
		}
		else
		{
			return \Redirect::route("getEditServiceEmployee",$doc_id)->with("fail","Some error!");
		}
		
	}
	
	
}

?>