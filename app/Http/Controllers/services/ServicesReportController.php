<?php 

namespace App\Http\Controllers\services;

use App\Http\Controllers\Controller;
use App\models\services\ServiceSearch;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class ServicesReportController extends Controller
{
	
	//database connection
	public static $myDb = "services";

	//Load vehicle list view
	public function getList()
	{
				
		$data["items"] = DB::connection("services")->table("items")->get();
		return view("services.report.form",$data);
	}
	
	/*
	getting form for inserting
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getReportResult()
	{
		
		//get search result from services fees9
		$data["rows"] = ServiceSearch::getFees9ReportResult();
		return View::make('services.report.list',$data);
		
	}
	
	    
    public function getRelated()
    {
    	
    	$items = DB::connection("services")->table("items")->where("fees9_type",Input::get('id'))->get();
    	$options = "";
    	foreach($items AS $item)
    	{
    		$options .= "<option value='".$item->id."'>".$item->item_description."</option>";
    	}
    	
    	return $options;
    	
    }
    
}

?>