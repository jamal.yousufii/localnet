<?php 

namespace App\Http\Controllers\audit;

use App\Http\Controllers\Controller;
use App\models\audit\auditModel;

use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use DateTime;
use DateInterval;
use DatePeriod;

class audit extends Controller
{
	//database connection
	public static $myDb = "audit";

	/**
    * Instantiate a new docsController instance.
    */
	public function __construct()
	{
		if(!Auth::check())
		{
			return Redirect::route('getLogin');
		}		
	}
	public function getAuditReports()
	{
		//check roles
		if(canView('audit_reports'))
		{
			$data['active'] = 'all';
			$data['parentDeps'] = getDepartmentWhereIn();
			//$data['records'] = auditModel::getAllReports(1);//get all planed reports
			//dd($data['records']);
			return View::make('audit.reports.list',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getAuditReportsData($type=1)
	{	
		//get all data 
		$object = auditModel::getAllReports($type);
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'file_name',
							'type_name',
							'report_year',
							'created_at'
							)
				->addColumn('operations', function($option){
					$options = '';
					if(canAdd('audit_reports'))
					{
						$options .= '<a href="'.route('editAuditReport',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
						$options .= '<a href="'.route('getReportDownload',$option->file_id).'" class="table-link" style="text-decoration:none;" title="Download">
								<i class="icon fa-download" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
					}
					if(canDelete('audit_reports'))
					{
						$options .= '<a href="javascript:void()" onclick="removeReport('.$option->id.')" class="table-link" style="text-decoration:none;" title="Edit">
								<i class="icon fa-remove" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
					}
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public function addNewReport()
	{
		//check roles
		if(canAdd('audit_reports'))
		{
			return View::make('audit.reports.add');
		}
		else
		{
			return showWarning();
		}
	}
	function post_audit_report()
	{
		if(canAdd('audit_reports'))
		{
			$validates = Validator::make(Input::all(), array(
				"report_type"					=> "required",
				"year"							=> "required"
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('add_new_report')->withErrors($validates);
			}
			else
			{
				$items=array(
					'type' => Input::get('report_type'),
					'report_year' => Input::get('year'),
					'created_at'=>date('Y-m-d H:i:s'),
					'created_by'=>Auth::user()->id
					);
			}
			$id = auditModel::insertRecord_id('reports',$items);
			if(Input::hasFile('attach'))
			{
				$this->uploadEmployeeDocs($id,'attach','reports');
			}
			return \Redirect::route("getAuditReports")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function uploadEmployeeDocs($doc_id=0,$type='',$table='reports')
	{
		// getting all of the post data
		$file = Input::file($type);
		$errors = "";
		$file_data = array();
		// validating each file.
		$rules = array($type => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
		$validator = Validator::make(

		  		[
		            $type => $file,
		            'extension'  => Str::lower($file->getClientOriginalExtension()),
		        ],
		        [
		            $type => 'required|max:100000',
		            'extension'  => 'required|in:doc,docx,pdf,png'
		        ]
		);

		if($validator->passes())
		{
		    // path is root/uploads
		    $destinationPath = 'documents/audit_attachments';
		    $name = $file->getClientOriginalName();

		    $temp = explode(".", $name);
		    $extension = end($temp);
		    
		    $filename = $table.'_'.$doc_id.'.'.$extension;

		    $upload_success = $file->move($destinationPath, $filename);

		    if($upload_success) 
		    {
			   	$data = array(
			   					'file_name'=>$filename,
			   					'original_name'=>$name,
			   					'table' => $table
			   				);
			   if(count($data)>0)
				{						
					$file_id = auditModel::insertRecord_id('attachments',$data);
					auditModel::update_record($table,array('file_id'=>$file_id),array('id'=>$doc_id));
				}
			} 
			else 
			{
			   $errors .= json('error', 400);
			}
		} 
		else 
		{
		    // redirect back with errors.
		    return Redirect::back()->withErrors($validator);
		}
	}
	//download file from server
	public function downloadDoc($file_id=0)
	{
		if(canView('audit_reports'))
		{
			//get file name from database
			$fileName = auditModel::getReportFileName($file_id);
	        //public path for file
	        $file= public_path(). "/documents/audit_attachments/".$fileName;
	        //download file
	        return Response::download($file, $fileName);
	    }
	    else
	    {
	    	return showWarning();
	    }
	}
	//remove file from folder
	public function deleteFile()
	{
		if(canDelete('audit_reports'))
		{
			$file_id = Input::get('doc_id');
			
			//get file name from database
			$fileName = auditModel::getReportDetails($file_id);
			$file= public_path(). "/documents/audit_attachments/".$fileName->filename;
			if(File::delete($file))
			{
				auditModel::delete_record('attachments',array('id'=>$fileName->file_id));
				auditModel::update_record('reports',array('file_id'=>0),array('id'=>$file_id));
				
				return "<div class='alert alert-success'>File Deleted Successfully!</div>";
			}
			else
			{
				return "<div class='alert alert-danger'>Error!</div>";
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function editAuditReport($id=0)
	{
		//check roles
		if(canAdd('audit_reports'))
		{
			$data['row'] = auditModel::getReportDetails($id);
			return View::make('audit.reports.edit',$data);
		}
		else
		{
			return showWarning();
		}
	}
	function postAuditReportEdit($id=0)
	{
		if(canAdd('audit_reports'))
		{
			$validates = Validator::make(Input::all(), array(
				"report_type"					=> "required",
				"year"							=> "required"
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('editAuditReport')->withErrors($validates);
			}
			else
			{
				$items=array(
					'type' => Input::get('report_type'),
					'report_year' => Input::get('year')
					);
			}
			auditModel::update_record('reports',$items,array('id'=>$id));
			if(Input::hasFile('attach'))
			{
				$this->uploadEmployeeDocs($id,'attach','reports');
			}
			return \Redirect::route("getAuditReports")->with("success","Record updated Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function removeReport()
	{
		if(canDelete('audit_reports'))
		{
			$file_id = Input::get('doc_id');
			
			//get file name from database
			$fileName = auditModel::getReportFileName($file_id);
			$file= public_path(). "/documents/audit_attachments/".$fileName;
			File::delete($file);
			auditModel::delete_record('reports',array('id'=>$file_id));
		}
		else
		{
			return showWarning();
		}
	}
	public function loadUnplanedReports()
	{
		return View::make('audit.reports.unplanedList');
	}
	public function search_report()
	{
		$data['active'] = 'search';
		$type = Input::get('type');		
		$year = Input::get('year');		
		$attach = Input::get('attach');		
		$data['records'] = auditModel::getReports_search($type,$year,$attach);
		//dd($data['records']);
		return View::make('audit.reports.search_result',$data);
		
	}
	//_________________________ recomendations ___________________________
	
	public function getAduitRecommendations()
	{
		//check roles
		if(canView('audit_tracking'))
		{
			$data['active'] = 'charts';
			$data['parentDeps'] = getDepartmentWhereIn();
			//get general dep recommandations
			$dep_rec_imp= auditModel::getGeneralChart(1);//count implemented
			$imp_array=array();
			foreach($dep_rec_imp as $imp)
			{
				$imp_array[$imp->general_dir]=$imp->total;
			}
			$dep_rec = auditModel::getGeneralChart();
			$deps_array='';$imp_totals='';$not_totals='';
			foreach($dep_rec AS $dg)
			{
				$deps_array.= "'".$dg->name."',";//display the categories x axis
				if (array_key_exists($dg->general_dir,$imp_array))
				{
					$imp_totals.= $imp_array[$dg->general_dir].',';
					$not_totals.= $dg->total - $imp_array[$dg->general_dir].',';
				}
				else
				{
					$imp_totals.= '0,';
					$not_totals.= $dg->total.',';
				}
			}
			$data['imp_totals'] = substr($imp_totals,0,-1);
			$data['not_totals'] = substr($not_totals,0,-1);
			$deps_array=substr($deps_array,0,-1);
			$data['display']=$deps_array;
			//get sub dep recommandations
			$sub_rec_imp = auditModel::getSubChart(1);
			$sub_imp_array=array();
			foreach($sub_rec_imp as $imp)
			{
				$sub_imp_array[$imp->responsible_dir]=$imp->total;
			}
			$sub_rec = auditModel::getSubChart();
			$sub_array='';$sub_imp_totals='';$sub_not_totals='';
			foreach($sub_rec AS $dg)
			{
				$sub_array.= "'".$dg->name."',";//display the categories x axis
				if (array_key_exists($dg->responsible_dir,$sub_imp_array))
				{
					$sub_imp_totals.= $sub_imp_array[$dg->responsible_dir].',';
					$sub_not_totals.= $dg->total - $sub_imp_array[$dg->responsible_dir].',';
				}
				else
				{
					$sub_imp_totals.= '0,';
					$sub_not_totals.= $dg->total.',';
				}
			}
			$data['sub_imp_totals'] = substr($sub_imp_totals,0,-1);
			$data['sub_not_totals'] = substr($sub_not_totals,0,-1);
			$sub_array=substr($sub_array,0,-1);
			$data['sdisplay']=$sub_array;
			
			return View::make('audit.tracking.list',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getRecommandationsData()
	{	
		//get all data 
		$object = auditModel::getAllRecommandations();
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
						
							'title',
							'deadline',
							
							'attorney'
							)
				->addColumn('dep', function($option){
					if($option->type==0)
					{
						return getRecommandationDep($option->sub_dep,$option->type)->name;
					}
					else
					{
						return getRecommandationDep($option->ministry,$option->type)->name_dr;
					}
				})
				->addColumn('operations', function($option){
					$options = '';
					if(canAdd('audit_tracking'))
					{
						$options .= '<a href="'.route('editRecomandation',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
								<i class="icon fa-edit" aria-hidden="true"></i>
							</a> | ';
						
						$options .= '<a href="javascript:void()" data-target="#exampleModalPrimary" onclick="add_recommendation_modal('.$option->id.')" data-toggle="modal" class="table-link" style="text-decoration:none;" title="Recommendations">
									<i class="icon fa-eye" aria-hidden="true"></i>
								</a> | ';
					}
					if(canDelete('audit_tracking'))
					{
						$options .= '<a href="javascript:void()" onclick="removeReport('.$option->id.')" class="table-link" style="text-decoration:none;" title="Delete">
								<i class="icon fa-remove" aria-hidden="true"></i>
							</a>';
					}
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public function loadRecommendationModal()
	{
		$data['id'] = Input::get('id');
		$data['type'] = Input::get('type');
		$data['record'] = DB::connection("audit")->table("recommandations")->where('id',Input::get('id'))->first();
		//load view for list
		return View::make('audit.tracking.recommendation_modal',$data);
	}
	public function loadRecommendationItemsModal()
	{
		$data['id'] = Input::get('id');
		$data['records'] = DB::connection("audit")->table("recommandation_items")->where('recommandation_id',Input::get('id'))->get();
		//load view for list
		return View::make('audit.tracking.recommendationItems_modal',$data);
	}
	public function addNewRecommendation()
	{
		//check roles
		if(canAdd('audit_tracking'))
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['reports'] = auditModel::getAllFindings();
			
			return View::make('audit.tracking.add',$data);
		}
		else
		{
			return showWarning();
		}
	}
	function postRecommandation()
	{
		if(canAdd('audit_tracking'))
		{
			$validates = Validator::make(Input::all(), array(
				"title_1"				=> "required",
				//"deadline_1"			=> "required",
				//"general_department"	=> "required",
				//"sub_dep"				=> "required",
				//"type_1"				=> "required",
				"report_title"			=> "required"
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('addNewRecommendation')->withErrors($validates);
			}
			else
			{
				$investigation_det = auditModel::getInvestigationDet(Input::get('report_title'));
				if($investigation_det->type==0)
				{
					$responsible_dir = $investigation_det->sub_dep;
					$general_dir = $investigation_det->general_dep;
				}
				else
				{
					$responsible_dir = 0;
					$general_dir = 0;
				}
				$items=array(
					'report_title_id' 	=> Input::get('report_title'),
					'deadline' 			=> $investigation_det->date,
					'responsible_dir'	=> $responsible_dir,
					'general_dir' 		=> $general_dir,
					'sent_authority' 	=> Input::get('authority'),
					'money_note' 		=> Input::get('money'),
					
					'created_at'		=>date('Y-m-d H:i:s'),
					'created_by'		=>Auth::user()->id
					);
				$id = auditModel::insertRecord_id('recommandations',$items);
				if(Input::get('money')==1)
				{
					$sdate = null;
					if(Input::get('date') != '')
					{
						$sdate = explode("-", Input::get('date'));
						$sy = $sdate[2];
						$sm = $sdate[1];
						$sd = $sdate[0];
						$sdate = dateToMiladi($sy,$sm,$sd);		
					}
					
					$money=array(
						'amount' 			=> Input::get('amount'),
						'date' 				=> $sdate,
						'recommandation_id' =>$id
						);
					$money_id = auditModel::insertRecord_id('money_notes',$money);
					$employees = Input::get('employees');
					$multi_emps = array();
		            // Loop until the number of category ids and store them in the array;
		            for($i = 0; $i < count($employees); $i ++)
		            {
		                $multi_emps[] = array('money_id' => $money_id,'employee_id' => $employees[$i]);
		            } 
		            auditModel::insertRecord('money_note_employees',$multi_emps); 
				}
				
				if(Input::hasFile('attach'))
				{
					$this->uploadEmployeeDocs($id,'attach','recommandations');
				}
				
				$recoms=array();
				for($i=1;$i<=Input::get('total_recom');$i++)
				{
					if(Input::get('title_'.$i)!='')
					{
						$recoms[]=array(
									'title'				=>Input::get('title_'.$i),
									'type'				=>Input::get('type_'.$i),
									'authority'			=>Input::get('authority_'.$i),
									'employee_number'	=>Input::get('number_'.$i),
									'deadline'			=>Input::get('deadline_'.$i),
									'recommandation_id'	=>$id);
					}
				}
				auditModel::insertRecord('recommandation_items',$recoms);
			
				return \Redirect::route("getAduitRecommendations")->with("success","Record Saved Successfully.");
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function editRecomandation($id=0)
	{
		//check roles
		if(canAdd('audit_tracking'))
		{
			$details = auditModel::getRecommandationDetails($id);
			$data['row'] = $details;
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['deps'] = getRelatedSubDepartment($details->general_dir);
			$data['employees'] = auditModel::bringDepEmployees($details->responsible_dir);
			$data['investigation_det'] = auditModel::getInvestigationDet($details->report_title_id);
			$money_emp_array=array();
			if($details->money_note==1)
			{
				$money_employees = auditModel::bringMoneyEmployees($id);
				
				if($money_employees)
				{
					foreach($money_employees as $emp)
					{
						$money_emp_array[$emp->employee_id]=$emp->employee_id;
					}
				}
				
			}
			$data['money_employees'] = $money_emp_array;
			$data['reports'] = auditModel::getAllFindings();
			$data['items'] = auditModel::getAllRecommendation_items($id);
			return View::make('audit.tracking.edit',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function downloadRecommandationDoc($file_id=0)
	{
		if(canView('audit_tracking'))
		{
			//get file name from database
			$fileName = auditModel::getReportFileName($file_id);
	        //public path for file
	        $file= public_path(). "/documents/audit_attachments/".$fileName;
	        //download file
	        return Response::download($file, $fileName);
	    }
	    else
	    {
	    	return showWarning();
	    }
	}
	function postRecommandationEdit($id=0)
	{
		if(canAdd('audit_tracking'))
		{
			$validates = Validator::make(Input::all(), array(
				"title_1"				=> "required",
				//"deadline"				=> "required",
				//"general_department"	=> "required",
				//"sub_dep"				=> "required",
				//"type_1"				=> "required",
				"report_title"			=> "required"
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('editRecomandation')->withErrors($validates);
			}
			else
			{
				$investigation_det = auditModel::getInvestigationDet(Input::get('report_title'));
				$items=array(
					'report_title_id' 	=> Input::get('report_title'),
					'deadline' 			=> $investigation_det->date,
					'responsible_dir'	=> $investigation_det->sub_dep,
					'general_dir' 		=> $investigation_det->general_dep,
					'sent_authority' 	=> Input::get('authority'),
					'money_note' 		=> Input::get('money')
					//'type' 				=> Input::get('type'),
					);
			
				auditModel::update_record('recommandations',$items,array('id'=>$id));
				if(Input::get('money')==1)
				{
					$sdate = null;
					if(Input::get('date') != '')
					{
						$sdate = explode("-", Input::get('date'));
						$sy = $sdate[2];
						$sm = $sdate[1];
						$sd = $sdate[0];
						$sdate = dateToMiladi($sy,$sm,$sd);		
					}
					
					$money=array(
						'amount' 			=> Input::get('amount'),
						'date' 				=> $sdate,
						'recommandation_id' => $id
						);
					if(Input::get('money_id')=='')
					{//new money note
						$money_id = auditModel::insertRecord_id('money_notes',$money); 
					}
					else
					{
						auditModel::update_record('money_notes',$money,array('id'=>Input::get('money_id')));
						$money_id = Input::get('money_id');
					}
					$employees = Input::get('employees');
					$multi_emps = array();
		            // Loop until the number of category ids and store them in the array;
		            for($i = 0; $i < count($employees); $i ++)
		            {
		                $multi_emps[] = array('money_id' => $money_id,'employee_id' => $employees[$i]);
		            } 
		            auditModel::delete_record('money_note_employees',array('money_id'=>$money_id));
		            auditModel::insertRecord('money_note_employees',$multi_emps); 
				}
				else
				{
					auditModel::delete_record('money_notes',array('recommandation_id'=>$id));
				}
				if(Input::hasFile('attach'))
				{
					$this->uploadEmployeeDocs($id,'attach','recommandations');
				}
				$recoms=array();
				for($i=1;$i<=Input::get('total_recom');$i++)
				{
					if(Input::get('title_'.$i)!='')
					{
						$recoms[]=array(
									'title'				=>Input::get('title_'.$i),
									'type'				=>Input::get('type_'.$i),
									'authority'			=>Input::get('authority_'.$i),
									'employee_number'	=>Input::get('number_'.$i),
									'deadline'			=>Input::get('deadline_'.$i),
									'recommandation_id'	=>$id
									);
					}
				}
				auditModel::delete_record('recommandation_items',array('recommandation_id'=>$id));
				auditModel::insertRecord('recommandation_items',$recoms);
			
				return \Redirect::route("getAduitRecommendations")->with("success","Record updated Successfully.");
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function removeRecommandation()
	{
		if(canDelete('audit_tracking'))
		{
			$file_id = Input::get('doc_id');
			
			//get file name from database
			$fileName = auditModel::getRecommandationDetails($file_id);
			if($fileName->file_id!=0)
			{
				$file= public_path(). "/documents/audit_attachments/".$fileName->filename;
				File::delete($file);
			}
			auditModel::delete_record('recommandations',array('id'=>$file_id));
			auditModel::delete_record('recommandation_items',array('recommandation_id'=>$file_id));
			
		}
		else
		{
			return showWarning();
		}
	}
	public function ImplementedRecommandation()
	{
		if(canAdd('audit_tracking'))
		{
			$id = Input::get('doc_id');
			$type = Input::get('type');
			if($type==0)
			{
				auditModel::update_record('recommandation_items',array('implemented'=>1,'implementaion_desc'=>Input::get('desc')),array('id'=>$id));
				return '<a href="javascript:void()" onclick="implementedRec('.$id.')" class="table-link" style="text-decoration:none;" title="Implemented">
							<i class="icon fa-check" aria-hidden="true"></i>
						</a>';
			}
			else
			{
				auditModel::update_record('recommandation_items',array('implemented'=>0,'implementaion_desc'=>null),array('id'=>$id));
				return '<a href="javascript:void()" onclick="show_desc('.$id.')" class="table-link" style="text-decoration:none;" title="Not Implemented">
							<i class="icon fa-archive" aria-hidden="true"></i>
						</a>';
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function removeRecommandationFile()
	{
		if(canDelete('audit_tracking'))
		{
			$file_id = Input::get('doc_id');
			
			//get file name from database
			$fileName = auditModel::getRecommandationDetails($file_id);
			$file= public_path(). "/documents/audit_attachments/".$fileName->filename;
			if(File::delete($file))
			{
				auditModel::delete_record('attachments',array('id'=>$fileName->file_id));
				auditModel::update_record('recommandations',array('file_id'=>0),array('id'=>$fileName->file_id));
				
				return "<div class='alert alert-success'>File Deleted Successfully!</div>";
			}
			else
			{
				return "<div class='alert alert-danger'>Error!</div>";
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function loadMoneyTracking()
	{
		return View::make('audit.tracking.moneyTracking');
	}
	public function getMoneyData()
	{	
		//get all data 
		$object = auditModel::getAllMoneyNotes();
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'amount',
							'general_dep',
							'sub_dep',
							'payments'
							)
				->addColumn('operations', function($option){
					$options = '';
					if(canAdd('audit_tracking'))
					{
						/*
						$options .= '<a href="'.route('editMoneyNote',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a> | ';
						*/
						$options .= '<a href="'.route('paybackMoneyNote',$option->id).'" class="table-link" style="text-decoration:none;" title="Payments">
								<i class="icon fa-recycle" aria-hidden="true" style="font-size: 16px;"></i>
							</a> ';
					}
					/*
					if(canDelete('audit_tracking'))
					{
						$options .= '<a href="javascript:void()" onclick="removeMoney('.$option->id.')" class="table-link" style="text-decoration:none;" title="Delete">
								<i class="icon fa-remove" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
					}
					*/
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public function addNewMoneyNote()
	{
		//check roles
		if(canAdd('audit_tracking'))
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			return View::make('audit.tracking.add_money_note',$data);
		}
		else
		{
			return showWarning();
		}
	}
	function postMoneyNote()
	{
		if(canAdd('audit_tracking'))
		{
			$validates = Validator::make(Input::all(), array(
				"amount"					=> "required",
				"general_department"		=> "required",
				"sub_dep"					=> "required",
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('addNewMoneyNote')->withErrors($validates);
			}
			else
			{
				$sdate = null;
				if(Input::get('date') != '')
				{
					$sdate = explode("-", Input::get('date'));
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				
				$items=array(
					'amount' => Input::get('amount'),
					'date' => $sdate,
					'sub_dir' => Input::get('sub_dep'),
					'general_dir' => Input::get('general_department'),
					'created_at'=>date('Y-m-d H:i:s'),
					'created_by'=>Auth::user()->id
					);
			}
			$id = auditModel::insertRecord_id('money_notes',$items);
			$employees = Input::get('employees');
			$multi_emps = array();
            // Loop until the number of category ids and store them in the array;
            for($i = 0; $i < count($employees); $i ++)
            {
                $multi_emps[] = array('money_id' => $id,'employee_id' => $multi_emps[$i]);
            } 
            auditModel::insertRecord('money_note_employees',$multi_emps); 
			return \Redirect::route("getAduitRecommendations")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function editMoneyNote($id=0)
	{
		//check roles
		if(canAdd('audit_tracking'))
		{
			$details = auditModel::getMoneyNoteDetails($id);
			$data['row'] = $details;
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['deps'] = getRelatedSubDepartment($details->general_dir);
			return View::make('audit.tracking.edit_money_note',$data);
		}
		else
		{
			return showWarning();
		}
	}
	function postMoneyNoteEdit($id=0)
	{
		if(canAdd('audit_tracking'))
		{
			$validates = Validator::make(Input::all(), array(
				"amount"					=> "required",
				"general_department"		=> "required",
				"sub_dep"					=> "required",
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('editRecomandation')->withErrors($validates);
			}
			else
			{
				$sdate = null;
				if(Input::get('date') != '')
				{
					$sdate = explode("-", Input::get('date'));
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				
				$items=array(
					'amount' => Input::get('amount'),
					'date' => $sdate,
					'sub_dir' => Input::get('sub_dep'),
					'general_dir' => Input::get('general_department')
					);
			}
			auditModel::update_record('money_notes',$items,array('id'=>$id));
			
			return \Redirect::route("getAduitRecommendations")->with("success","Record updated Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function removeMoneyNote()
	{
		if(canDelete('audit_tracking'))
		{
			$file_id = Input::get('doc_id');
			
			auditModel::delete_record('money_notes',array('id'=>$file_id));
			
		}
		else
		{
			return showWarning();
		}
	}
	public function removeFinding()
	{
		if(canDelete('audit_tracking'))
		{
			$file_id = Input::get('doc_id');
			
			auditModel::delete_record('findings',array('report_id'=>$file_id));
			auditModel::delete_record('finding_reports',array('id'=>$file_id));
		}
		else
		{
			return showWarning();
		}
	}
	public function paybackMoneyNote($id=0)
	{
		//check roles
		if(canAdd('audit_tracking'))
		{
			$data['records'] = auditModel::getMoneyNotePayments($id);
			$data['record'] = auditModel::getMoneyNoteDetails($id);
			$data['employees'] = auditModel::getMoneyNoteEmployees($id);
			$data['id'] = $id;
			return View::make('audit.tracking.money_payback_list',$data);
		}
		else
		{
			return showWarning();
		}
	}
	function addMoneyPayment()
	{
		if(canAdd('audit_tracking'))
		{
			$validates = Validator::make(Input::all(), array(
				"amount"					=> "required"
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('paybackMoneyNote')->withErrors($validates);
			}
			else
			{
				$sdate = null;
				if(Input::get('date') != '')
				{
					$sdate = explode("-", Input::get('date'));
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				
				$items=array(
					'amount' => Input::get('amount'),
					'payment_date' => $sdate,
					'money_id' => Input::get('id'),
					'employee_id' => Input::get('employee'),
					'created_at'=>date('Y-m-d H:i:s'),
					'created_by'=>Auth::user()->id
					);
			}
			auditModel::insertRecord_id('money_paybacks',$items);
			
			return \Redirect::route("paybackMoneyNote",array(Input::get('id')))->with("success","Record updated Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	function load_payment_edit()
	{
		$data['record'] = auditModel::getPaymentDetails(Input::get('id'));
		$data['employees'] = auditModel::getMoneyNoteEmployees(Input::get('money_id'));
		return View::make('audit.tracking.money_payback_edit',$data);
	}
	function editMoneyPayment($id=0)
	{
		if(canAdd('audit_tracking'))
		{
			$validates = Validator::make(Input::all(), array(
				"amount"					=> "required"
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('paybackMoneyNote')->withErrors($validates);
			}
			else
			{
				$sdate = null;
				if(Input::get('date') != '')
				{
					$sdate = explode("-", Input::get('date'));
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				
				$items=array(
					'amount' => Input::get('amount'),
					'payment_date' => $sdate,
					'money_id' => Input::get('id'),
					'employee_id' => Input::get('employee')
					);
			}
			auditModel::update_record('money_paybacks',$items,array('id'=>$id));
			
			return \Redirect::route("paybackMoneyNote",array(Input::get('id')))->with("success","Record updated Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function removePayment()
	{
		if(canDelete('audit_tracking'))
		{
			$file_id = Input::get('doc_id');
			
			auditModel::delete_record('money_paybacks',array('id'=>$file_id));
			
		}
		else
		{
			return showWarning();
		}
	}
	public function loadFindings()
	{
		return View::make('audit.tracking.findings');
	}
	public function getFindingsData()
	{	
		//get all data 
		$object = auditModel::getAllFindings();
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'title',
							
							'date'
							)
				->addColumn('dep', function($option){
					if($option->type==0)
					{
						if($option->sub_dep>0)
						{
							return getRecommandationDep($option->sub_dep,$option->type)->name;
						}
						else
						{
							return '';
						}
					}
					else
					{
						return getRecommandationDep($option->ministry,$option->type)->name_dr;
					}
				})
				->addColumn('operations', function($option){
					$options = '';
					if(canAdd('audit_tracking'))
					{
						$options .= '<a href="'.route('editFinding',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a> | ';
					}
					if(canDelete('audit_tracking'))
					{
						if(findingHas_rec($option->id))
						{
							$options .= '<a href="javascript:void()" disabled class="table-link" style="text-decoration:none;" title="Since this report has recommandation, you must delete the recommandations first">
								<i class="icon fa-remove" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
						}
						else
						{
							$options .= '<a href="javascript:void()" onclick="removeMoney('.$option->id.')" class="table-link" style="text-decoration:none;" title="Delete">
								<i class="icon fa-remove" aria-hidden="true" style="font-size: 16px;color:red"></i>
							</a>';
						}
					}
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public function addNewFinding()
	{
		//check roles
		if(canAdd('audit_tracking'))
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['ministrires'] = auditModel::getAllMinistries();
			return View::make('audit.tracking.add_finding',$data);
		}
		else
		{
			return showWarning();
		}
	}
	function postFinding()
	{
		if(canAdd('audit_tracking'))
		{
			if(Input::get('type')==0)
			{
				$validates = Validator::make(Input::all(), array(
					"title"					=> "required",
					"date"					=> "required",
					"general_department"	=> "required",
					"sub_dep"				=> "required",
					"finding_1"				=> "required"
					
				));
			}
			else
			{
				$validates = Validator::make(Input::all(), array(
					"title"					=> "required",
					"date"					=> "required",
					"ministry"				=> "required",
					//"sub_dep"				=> "required",
					"finding_1"				=> "required"
					
				));
			}
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('addNewFinding')->withErrors($validates)->withInput();
			}
			else
			{
				$sdate = null;
				if(Input::get('date') != '')
				{
					$sdate = explode("-", Input::get('date'));
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				
				$items=array(
					'title' => Input::get('title'),
					'date' => $sdate,
					'type' => Input::get('type'),
					//'sub_dep' => Input::get('sub_dep'),
					//'general_dep' => Input::get('general_department'),
					//'ministry' => Input::get('ministry'),
					'created_at'=>date('Y-m-d H:i:s'),
					'created_by'=>Auth::user()->id
					);
				if(Input::get('type')==0)
				{
					$items['general_dep'] = Input::get('general_department');
					$items['sub_dep'] = Input::get('sub_dep');
				}
				else
				{
					$items['ministry'] = Input::get('ministry');
				}
			}
			$id = auditModel::insertRecord_id('finding_reports',$items);
			$findings=array();
			for($i=1;$i<=Input::get('total_findings');$i++)
			{
				if(Input::get('finding_'.$i)!='')
				{
					$findings=array('title'=>Input::get('finding_'.$i),'report_id'=>$id);
					$finding_id = auditModel::insertRecord_id('findings',$findings);
					$subs=array();
					for($j=1;$j<=Input::get('total_sub_'.$i);$j++)
					{
						if(Input::get('subfinding_'.$j.'_'.$i)!='')
						{
							$subs[]=array('title'=>Input::get('subfinding_'.$j.'_'.$i),'finding_id'=>$finding_id);
						}
					}
					auditModel::insertRecord('sub_findings',$subs);
				}
			}
			
			
			return \Redirect::route("getAduitRecommendations")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function editFinding($id=0)
	{
		//check roles
		if(canAdd('audit_tracking'))
		{
			$details = auditModel::getFindingDetails($id);
			$data['findings'] = auditModel::getReportFindings($id);
			$data['subfindings'] = auditModel::getReportSubFindings($id);
			$data['row'] = $details;
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['deps'] = getRelatedSubDepartment($details->general_dep);
			$data['ministrires'] = auditModel::getAllMinistries();
			return View::make('audit.tracking.edit_finding',$data);
		}
		else
		{
			return showWarning();
		}
	}
	function postEditFinding($id=0)
	{
		if(canAdd('audit_tracking'))
		{
			if(Input::get('type')==0)
			{
				$validates = Validator::make(Input::all(), array(
					"title"					=> "required",
					"date"					=> "required",
					"general_department"	=> "required",
					"sub_dep"				=> "required",
					"finding_1"				=> "required"
					
				));
			}
			else
			{
				$validates = Validator::make(Input::all(), array(
					"title"					=> "required",
					"date"					=> "required",
					//"general_department"	=> "required",
					"ministry"				=> "required",
					"finding_1"				=> "required"
					
				));
			}
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('editFinding',$id)->withErrors($validates)->withInput();
			}
			else
			{
				$sdate = null;
				if(Input::get('date') != '')
				{
					$sdate = explode("-", Input::get('date'));
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				
				$items=array(
					'title' => Input::get('title'),
					'date' => $sdate,
					'type' => Input::get('type')
					
					);
				if(Input::get('type')==0)
				{
					$items['sub_dep'] = Input::get('sub_dep');
					$items['general_dep'] = Input::get('general_department');
				}
				else
				{
					$items['ministry'] = Input::get('ministry');
				}
			}
			auditModel::update_record('finding_reports',$items,array('id'=>$id));
			auditModel::delete_record('findings',array('report_id'=>$id));
			
			$findings=array();
			for($i=1;$i<=Input::get('total_findings');$i++)
			{
				auditModel::delete_record('sub_findings',array('finding_id'=>Input::get('finding_id_'.$i)));
				if(Input::get('finding_'.$i)!='')
				{
					$findings=array('title'=>Input::get('finding_'.$i),'report_id'=>$id);
					$finding_id = auditModel::insertRecord_id('findings',$findings);
					$subs=array();
					for($j=1;$j<=Input::get('total_sub_'.$i);$j++)
					{
						if(Input::get('subfinding_'.$j.'_'.$i)!='')
						{
							$subs[]=array('title'=>Input::get('subfinding_'.$j.'_'.$i),'finding_id'=>$finding_id);
						}
					}
					auditModel::insertRecord('sub_findings',$subs);
				}
			}
			//auditModel::insertRecord('findings',$findings);
			
			return \Redirect::route("getAduitRecommendations")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function loadRecommandations()
	{
		return View::make('audit.tracking.recommandationList');
	}
	public function getMoreFinding()
	{
		$data['total'] = Input::get('total');
		return View::make('audit.tracking.more_findings',$data);
	}
	public function getMoreSubFinding()
	{
		$data['total'] = Input::get('total');
		$data['finding'] = Input::get('finding');
		return View::make('audit.tracking.more_subfindings',$data);
	}
	public function getMoreRecommendation()
	{
		$data['total'] = Input::get('total');
		return View::make('audit.tracking.more_recommendation',$data);
	}
	public function getInvestigationDet()
	{
		$data = auditModel::getInvestigationDet(Input::get('id'));
		if($data->type==0)
		{
		$result = '
			<div class="col-sm-3">
            	<div class="col-sm-12">
            		<label class="col-sm-12 ">'._("general_department").'</label>
                    <input class="form-control" readonly type="text" value="'.$data->gen_dir.'">
            	</div>
            </div>
            <div class="col-sm-3">
            	<div class="col-sm-12">
            		<label class="col-sm-12 ">'._("sub_department").'</label>
                    <input class="form-control" readonly type="text" value="'.$data->sub_dir.'">
            </div>
            <input type="hidden" value="'.$data->sub_dep.'" id="sub_department">
		';
		}
		else
		{
			$result = '
				<div class="col-sm-3">
	            	<div class="col-sm-12">
	            		<label class="col-sm-12 ">'._("ministry").'</label>
	                    <input class="form-control" readonly type="text" value="'.$data->ministry_name.'">
	            	</div>
	            </div>
	            
	            <input type="hidden" value="0" id="sub_department">
			';
		}
		return $result;
	}
	/////////////////////////////////////// other attahcments
	public function getAllAttachments()
	{
		//check roles
		if(canView('audit_reports'))
		{
			$data['active'] = 'all';
			$data['parentDeps'] = getDepartmentWhereIn();
			return View::make('audit.attachments.list',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getAllAttachmentsData()
	{	
		//get all data 
		$object = auditModel::getAllAttachs();
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'original_name',
							'category',
							'created_at'
							)
				->addColumn('operations', function($option){
					$options = '';
					if(canAdd('audit_reports'))
					{
						$options .= '<a href="'.route('editAuditReport',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
						$options .= '<a href="'.route('getReportDownload',$option->id).'" class="table-link" style="text-decoration:none;" title="Download">
								<i class="icon fa-download" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
					}
					if(canDelete('audit_reports'))
					{
						$options .= '<a href="javascript:void()" onclick="removeReport('.$option->id.')" class="table-link" style="text-decoration:none;" title="Edit">
								<i class="icon fa-remove" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
					}
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public function attach_new_file()
	{
		//check roles
		if(canAdd('audit_reports'))
		{
			$data['categories'] = auditModel::getAllCategories();
			return View::make('audit.attachments.add',$data);
		}
		else
		{
			return showWarning();
		}
	}
	function post_new_file()
	{
		if(canAdd('audit_reports'))
		{
			$validates = Validator::make(Input::all(), array(
				"category"					=> "required",
				"attach"					=> "required"
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('attach_new_file')->withErrors($validates);
			}
			else
			{
				$items=array(
					
					'category_id' => Input::get('category'),
					'created_at'=>date('Y-m-d H:i:s'),
					'created_by'=>Auth::user()->id
					);
			}
			$id = auditModel::insertRecord_id('attachments',$items);
			if(Input::hasFile('attach'))
			{
				$file = Input::file('attach');
				$errors = "";
				$file_data = array();
				// validating each file.
				$rules = array('attach' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
				$validator = Validator::make(
		
				  		[
				            'attach' => $file,
				            'extension'  => Str::lower($file->getClientOriginalExtension()),
				        ],
				        [
				            'attach' => 'required|max:100000',
				            'extension'  => 'required|in:doc,docx,pdf,png,jpg,jpeg'
				        ]
				);
		
				if($validator->passes())
				{
				    // path is root/uploads
				    $destinationPath = 'documents/audit_attachments';
				    $filename = $file->getClientOriginalName();
		
				    $temp = explode(".", $filename);
				    $extension = end($temp);
				    
				    $filename = 'attachment_'.$id.'.'.$extension;
		
				    $upload_success = $file->move($destinationPath, $filename);
		
				    if($upload_success) 
				    {
					   	$data = array(
					   					'file_name'=>$filename,
					   					'original_name'=>$file->getClientOriginalName()
					   				);
					   if(count($data)>0)
						{						
							auditModel::update_record('attachments',$data,array('id'=>$id));
						}
					} 
					else 
					{
					   $errors .= json('error', 400);
					}
				} 
				else 
				{
				    // redirect back with errors.
				    return Redirect::back()->withErrors($validator);
				}
			}
			return \Redirect::route("getAllAttachments")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function bringDepEmployees()
	{
		$dep_id = Input::get('dep_id');
		$result = '<option value="">'._('select').'</option>';
		if($dep_id!=0)
		{
			$records = auditModel::bringDepEmployees($dep_id);
			
			if($records)
			{
				foreach($records as $rec)
				{
					$result .= '<option value="'.$rec->id.'">'.$rec->name.' '.$rec->last_name.'</option>';
				}
			}
		}
		return $result;
	}
}
?>