<?php 
namespace App\  Http\Controllers\docscom_doc_tracking;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\docscom_doc_tracking\Doc_Tb;
use App\models\docscom_doc_tracking\Deputy;
use App\models\docscom_doc_tracking\Doc_Tb_Log;
use App\models\docscom_doc_tracking\Executing_Tb; 
use App\models\docscom_doc_tracking\Sender;
use App\models\docscom_doc_tracking\LogActivity;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Response;
use Excel;
use Carbon;
use Validator;


class main_controller extends Controller {

//----------------------------Select Main Page-------------------------------

  public function loadRecordsList(){

//     // return "Salam";
//     $deputy     =DB::connection('docscom_document_tracking')->table("deputy_tb")->get(); 
//     // $doc_tb     =DB::connection('docscom_document_tracking')->table("doc_tb")->get();
//     $executing  =DB::connection('docscom_document_tracking')->table("executing_tb")->get(); 
//     $sender     =DB::connection('docscom_document_tracking')->table("sender")->get(); 
    
//     $doc_tb    =DB::connection('docscom_document_tracking')->table('doc_tb')->select('doc_tb.*','deputy_tb.dup_name','executing_tb.exe_name','sender.sender_name')
//       ->leftjoin('executing_tb','doc_tb.executive_id', '=', 'executing_tb.id')
//       ->leftjoin('sender','doc_tb.sender_id','=','sender.id')
//       ->leftjoin('deputy_tb','doc_tb.deputy_id','=','deputy_tb.id')
//       // ->leftjoin('uploads','doc_tb.id','=','uploads.file_id')->groupBy('file_id')
//       ->where('status','=','0')
//       ->get();
// // return($doc_tb);
    return view('docscom_doc_tracking.list');

  }
  public function loadimportrecards(){

    // return "Salam";
    $deputy     =DB::connection('docscom_document_tracking')->table("deputy_tb")->get(); 
    // $doc_tb     =DB::connection('docscom_document_tracking')->table("doc_tb")->get();
    $executing  =DB::connection('docscom_document_tracking')->table("executing_tb")->get(); 
    $sender     =DB::connection('docscom_document_tracking')->table("sender")->get(); 
    
    $doc_tb    =DB::connection('docscom_document_tracking')->table('doc_tb')->select('doc_tb.*','deputy_tb.dup_name','executing_tb.exe_name','sender.sender_name')
      ->leftjoin('executing_tb','doc_tb.executive_id', '=', 'executing_tb.id')
      ->leftjoin('sender','doc_tb.sender_id','=','sender.id')
      ->leftjoin('deputy_tb','doc_tb.deputy_id','=','deputy_tb.id')
      // ->leftjoin('uploads','doc_tb.id','=','uploads.file_id')->groupBy('file_id')
      ->where('status','=','0')
      ->get();
// return($doc_tb);
    return view('docscom_doc_tracking.implist',array('deputy'=>$deputy,'executing'=>$executing,'sender'=>$sender,'doc_tb'=>$doc_tb));

  }
  public function loadexportrecards(){

    // return "Salam";
    $deputy     =DB::connection('docscom_document_tracking')->table("deputy_tb")->get(); 
    // $doc_tb     =DB::connection('docscom_document_tracking')->table("doc_tb")->get();
    $executing  =DB::connection('docscom_document_tracking')->table("executing_tb")->get(); 
    $sender     =DB::connection('docscom_document_tracking')->table("sender")->get(); 
    
    $doc_tb    =DB::connection('docscom_document_tracking')->table('doc_tb')->select('doc_tb.*','deputy_tb.dup_name','executing_tb.exe_name','sender.sender_name')
      ->leftjoin('executing_tb','doc_tb.executive_id', '=', 'executing_tb.id')
      ->leftjoin('sender','doc_tb.sender_id','=','sender.id')
      ->leftjoin('deputy_tb','doc_tb.deputy_id','=','deputy_tb.id')
      // ->leftjoin('uploads','doc_tb.id','=','uploads.file_id')->groupBy('file_id')
      ->where('status','=','0')
      ->get();
// return($doc_tb);
    return view('docscom_doc_tracking.explist',array('deputy'=>$deputy,'executing'=>$executing,'sender'=>$sender,'doc_tb'=>$doc_tb));

  }
  public function loadfallowrecards(){

    // return "Salam";
    $deputy     =DB::connection('docscom_document_tracking')->table("deputy_tb")->get(); 
    // $doc_tb     =DB::connection('docscom_document_tracking')->table("doc_tb")->get();
    $executing  =DB::connection('docscom_document_tracking')->table("executing_tb")->get(); 
    $sender     =DB::connection('docscom_document_tracking')->table("sender")->get(); 
    
    $doc_tb    =DB::connection('docscom_document_tracking')->table('doc_tb')->select('doc_tb.*','deputy_tb.dup_name','executing_tb.exe_name','sender.sender_name')
      ->leftjoin('executing_tb','doc_tb.executive_id', '=', 'executing_tb.id')
      ->leftjoin('sender','doc_tb.sender_id','=','sender.id')
      ->leftjoin('deputy_tb','doc_tb.deputy_id','=','deputy_tb.id')
      // ->leftjoin('uploads','doc_tb.id','=','uploads.file_id')->groupBy('file_id')
      ->where('status','=','0')
      ->where('condition','=','2')
      ->get();
// return($doc_tb);
    return view('docscom_doc_tracking.fallowlist',array('deputy'=>$deputy,'executing'=>$executing,'sender'=>$sender,'doc_tb'=>$doc_tb));

  }


  


//-----------------------Insert Data--------------------------------------

//---------------Insert Import document  Information 
  public function load_impdoc_form(){
     // return "Salam";
    $doc_tb     = DB::connection('docscom_document_tracking')->table('doc_tb')->get();
    $deputy_tb  = DB::connection('docscom_document_tracking')->table('deputy_tb')->get();
    $sender     = DB::connection('docscom_document_tracking')->table('sender')->get();
    $executing  =DB::connection('docscom_document_tracking')->table("executing_tb")->get();
   
    return view('docscom_doc_tracking.insert_impdoc_info',array('doc_tb'=>$doc_tb,'deputy_tb'=>$deputy_tb,'sender'=>$sender,'executing'=>$executing));
  }
       
 public function insert_impdoc_info(Request $request)
 {

  // validate the input fields
    $validates   = Validator::make(Input::all(), array
      (
             "sender_id" => "required",
             "executive_id"   => "required"
      ));
 
        if($validates->fails())
            {
                return Redirect::route('listOfdocument')->withErrors($validates)->withInput();
            }
        else
           {

                $insert_doc_data=new Doc_Tb;
                $max=Doc_Tb::max('id');
                $id =$max + 1;
                $insert_doc_data->doc_type=Input::get('doc_type');
                $insert_doc_data->doc_no=Input::get('doc_no');
                $insert_doc_data->doc_date= toGregorian(gregorian_format(Input::get('doc_date')));
                $insert_doc_data->import_no=Input::get('import_no');
                $insert_doc_data->import_date= toGregorian(gregorian_format(Input::get('import_date')));
                $insert_doc_data->sender_id=Input::get('sender_id');
                $insert_doc_data->deputy_id=Input::get('deputy_id');
                $insert_doc_data->executive_id=Input::get('executive_id');
                $insert_doc_data->execution=Input::get('execution');
                $insert_doc_data->condition=Input::get('condition');
                $insert_doc_data->summary=Input::get('summary');
                $insert_doc_data->save();

    // getLog('employee_infos',$id,'inserted record');
    // return Redirect::route("phoneSimcardList")->with("success","معلومات معافقانه ثبت  ګردید.");


        if($insert_doc_data){
                // getting all of the post data
                $files = Input::file('files');
                //print_r($files);exit;
               
                $errors = "";
                $auto = 1;
                $file_data = array();
 
                if(Input::hasFile('files'))
                  
                {
                    foreach($files as $file)
                    {
                        // validating each file.
                        $validator = Validator::make(
                            [
                                'file' => $file
                            ],
                            [
                                'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
                            ]
                        );
                        if($validator->passes())
                        {
                            // path is root/uploads
                            $destinationPath ='docscom_doc_uploads/';
                            $original_filename = $file->getClientOriginalName();
                            $temp = explode(".", $original_filename);
                            $extension = end($temp);
 
                            $lastid = DB::connection('docscom_document_tracking')->table('doc_tb')->where('id',$id)->pluck('id');
 
 
                            $lastFileId = $lastid;
                            if($auto == 1)
                            $filename = "photo_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
                            else
                            $filename = "photo_".$lastFileId."_".$auto.".".$extension;
                            if(!file_exists($destinationPath)) {
                              File::makeDirectory($destinationPath);
                            }
                            $upload_success = $file->move($destinationPath, $filename);
 
                            if($upload_success)
                            {
                                $data = array(
                                            'file_name'             => $filename,
                                            'file_id'             => $lastid,
                                        );
                                //call the model function to insert the data into upload table.
                            $insertfile=DB::connection('docscom_document_tracking')->table("uploads")->insert($data);
                            }
                            else
                            {
                                // redirect back with errors.
                                return Redirect::route('listOfdocument')->withErrors($validator);
                            }
                        }
                        $auto ++;
 
 
                    }
                }
                // get the the log data and insert it into the log table.
 
                return back()->with("success","اسناد موافقانه ثبت گردید.");
            }
            else
            {
                // return Redirect::route('edit_doc')->with("fail","An error occured please try again or contact system developer.");
            }
          }
        }

                       //---------------Insert Export document  Information 
   public function load_expdoc_form(){
     // return "Salam";
    $doc_tb     = DB::connection('docscom_document_tracking')->table('doc_tb')->get();
    $deputy_tb  = DB::connection('docscom_document_tracking')->table('deputy_tb')->get();
    $sender     = DB::connection('docscom_document_tracking')->table('sender')->get();
    $executing  =DB::connection('docscom_document_tracking')->table("executing_tb")->get();
   
    return view('docscom_doc_tracking.insert_expdoc_info',array('doc_tb'=>$doc_tb,'deputy_tb'=>$deputy_tb,'sender'=>$sender,'executing'=>$executing));
  }
  public function insert_expdoc_info(Request $request){
    $insert_expdoc_data=new Doc_Tb;
    $max=Doc_Tb::max('id');
    $id =$max + 1;
    $insert_expdoc_data->doc_type=Input::get('doc_type');
    $insert_expdoc_data->export_no=Input::get('export_no');
    $insert_expdoc_data->export_date= toGregorian(gregorian_format(Input::get('export_date')));
    $insert_expdoc_data->sender_id=Input::get('sender_id');
    $insert_expdoc_data->deputy_id=Input::get('deputy_id');
    $insert_expdoc_data->executive_id=Input::get('executive_id');
    $insert_expdoc_data->execution=Input::get('execution');
    $insert_expdoc_data->condition=Input::get('condition');
    $insert_expdoc_data->summary=Input::get('summary');
    $insert_expdoc_data->save();
   return back()->with("success","اسناد موافقانه ثبت گردید.");

  }
          //-------------------------Update  Import Document information

    public function load_editimpdoc_form($id){
      // return "Salam";
    $edit_doc_data             = Doc_Tb::find($id);
    $edit_doc                  = DB::connection('docscom_document_tracking')->table('doc_tb')->get();
    $edit_deputy               = DB::connection('docscom_document_tracking')->table('deputy_tb')->get();
    $edit_sender               = DB::connection('docscom_document_tracking')->table('sender')->get();
    $edit_executing            =DB::connection('docscom_document_tracking')->table("executing_tb")->get();
    return view('docscom_doc_tracking.update_impdoc_info',array('edit_deputy'=>$edit_deputy,'edit_sender'=>$edit_sender,'edit_executing'=>$edit_executing,'edit_doc_data'=>$edit_doc_data,'edit_doc'=>$edit_doc));
    }
    public function insert_editimpdoc_data(Request $request,$id){
    // return "Salam";
    $edit_doc_data               =new Doc_Tb;
    $edit_doc_data               = Doc_Tb::find($id);
    $edit_doc_data->import_no    =$request->import_no;
    $edit_doc_data->condition    =$request->condition;
    $edit_doc_data->import_date  = toGregorian(gregorian_format(Input::get('import_date')));
    $diff = Carbon\Carbon::parse($edit_doc_data->import_date)->diffInDays();
    $edit_doc_data->passed_day=$diff;
    $edit_doc_data->save();
    // return view('docscom_doc_tracking.list')->with("success","معلومات معافقانه ثبت  ګردید.");
    return Redirect::route("listOfdocument")->with("success","معلومات معافقانه ثبت  ګردید.");
    }

    public function load_editexpdoc_form($id){
       // return "Salam";
    $edit_expdoc_data             = Doc_Tb::find($id);
    $edit_doc                  = DB::connection('docscom_document_tracking')->table('doc_tb')->get();
    $edit_deputy               = DB::connection('docscom_document_tracking')->table('deputy_tb')->get();
    $edit_sender               = DB::connection('docscom_document_tracking')->table('sender')->get();
    $edit_executing            =DB::connection('docscom_document_tracking')->table("executing_tb")->get();
    return view('docscom_doc_tracking.update_expdoc_info',array('edit_deputy'=>$edit_deputy,'edit_sender'=>$edit_sender,'edit_executing'=>$edit_executing,'edit_expdoc_data'=>$edit_expdoc_data,'edit_doc'=>$edit_doc));
    }
    public function insert_editexpdoc_data(Request $request,$id){
    // return "Salam";
    $edit_expdoc_data               =new Doc_Tb;
    $edit_expdoc_data               = Doc_Tb::find($id);
    $edit_expdoc_data->export_no    =$request->export_no;
    $edit_expdoc_data->condition    =$request->condition;
    $edit_expdoc_data->export_date  = toGregorian(gregorian_format(Input::get('export_date')));
    $diff = Carbon\Carbon::parse($edit_expdoc_data->import_date)->diffInDays();
    $edit_expdoc_data->passed_day=$diff;
    $edit_expdoc_data->save();
    // return view('docscom_doc_tracking.list')->with("success","معلومات معافقانه ثبت  ګردید.");
    return Redirect::route("listOfdocument")->with("success","معلومات معافقانه ثبت  ګردید.");
    }

  
     
  //---------------Insert deputy  Information 


  public function load_deputy_form(){
    // return "Salam";
    
    $deputy = DB::connection('docscom_document_tracking')->table('deputy_tb')->get();
    // return ($deputy);
  return view('docscom_doc_tracking.insert_deputy_info',array('deputy'=>$deputy));

  }

          
 public function insert_deputy_info(Request $request)
 {
  
    $deputy=new Deputy;
    
    $deputy->dup_name=Input::get('dup_name');
    $deputy->save();

   
return back()->with("success","معلومات معافقانه ثبت  ګردید.");
        }


public function load_executing_form()
{
  // return "Salam";
   $executing=DB::connection('docscom_document_tracking')->table("executing_tb")->get();
   return view('docscom_doc_tracking.insert_executing_info',array('executing'=>$executing));

}

public function insert_executing_info(Request $request)
 {
  // return "Salam";
    $executing=new Executing_Tb;
    $executing->exe_name=Input::get('exe_name');
    $executing->save();
return back()->with("success","معلومات معافقانه ثبت  ګردید.");
}

public function load_sender_form()
{
  // return "Salam";
    $deputy     =DB::connection('docscom_document_tracking')->table("deputy_tb")->get(); 
    $executing  =DB::connection('docscom_document_tracking')->table("executing_tb")->get(); 
    $sender     =DB::connection('docscom_document_tracking')->table("sender")->get();
   return view('docscom_doc_tracking.insert_sender_info',array('sender'=>$sender,'deputy'=>$deputy,'executing'=>$executing));

}

public function insert_sender_info(Request $request)
 {
  
    $sender=new Sender;
    
    $sender->sender_name=Input::get('sender_name');
    $sender->save();

   
    return back()->with("success","معلومات معافقانه ثبت  ګردید.");
  }

                                //-----------Delete Record

   function delete_doc(Request $request,$id){
     // return "Salam";
   $doc_log        = Doc_Tb::find($id);
   $doc_log->status=1;
   $doc_log->save();

    return Redirect::route("listOfdocument")->with("success","معلومات معافقانه ثحذف گردید");
    }
//-------------------Download File 
    public function file_download($id){
     $files=DB::connection('docscom_document_tracking')->table('uploads')->select('uploads.*','doc_tb.id')
     ->leftjoin('doc_tb','uploads.file_id','=','doc_tb.id')
     ->where('file_id',$id)
      ->get();
      return view::make('docscom_doc_tracking.page_for_down')->with('files',$files);
}
    //-----------------------Live Search---------------------
    public function search_impdoc_live(){
// return "Salam";
     $deputy     =DB::connection('docscom_document_tracking')->table("deputy_tb")->get(); 
     $doc_tb     =DB::connection('docscom_document_tracking')->table("doc_tb")->get();
     $executing  =DB::connection('docscom_document_tracking')->table("executing_tb")->get(); 
     $sender     =DB::connection('docscom_document_tracking')->table("sender")->get();
     $search_string=trim(Input::get('search_string'));
     $rows=DB::connection('docscom_document_tracking')->table('doc_tb')->select('doc_tb.*','deputy_tb.dup_name','executing_tb.exe_name','sender.sender_name')
     ->where('doc_tb.status',0)
     ->leftjoin('executing_tb','doc_tb.executive_id', '=', 'executing_tb.id')
     ->leftjoin('sender','doc_tb.sender_id','=','sender.id')
     ->leftjoin('deputy_tb','doc_tb.deputy_id','=','deputy_tb.id')
     ->Where('doc_tb.import_no', '=',$search_string)
     ->get();
    // dd($search_string);
     return view('docscom_doc_tracking.search_imp',array('deputy'=>$deputy,'executing'=>$executing,'sender'=>$sender,'rows'=>$rows));
     } 
     public function search_expdoc_live(){
// return "Salam";
     $deputy     =DB::connection('docscom_document_tracking')->table("deputy_tb")->get(); 
     $doc_tb     =DB::connection('docscom_document_tracking')->table("doc_tb")->get();
     $executing  =DB::connection('docscom_document_tracking')->table("executing_tb")->get(); 
     $sender     =DB::connection('docscom_document_tracking')->table("sender")->get();
     $search_string=trim(Input::get('search_string'));
     $rows=DB::connection('docscom_document_tracking')->table('doc_tb')->select('doc_tb.*','deputy_tb.dup_name','executing_tb.exe_name','sender.sender_name')
     ->where('doc_tb.status',0)
     ->leftjoin('executing_tb','doc_tb.executive_id', '=', 'executing_tb.id')
     ->leftjoin('sender','doc_tb.sender_id','=','sender.id')
     ->leftjoin('deputy_tb','doc_tb.deputy_id','=','deputy_tb.id')
     ->Where('doc_tb.export_no', '=',$search_string)
     ->get();
    // dd($search_string);
     return view('docscom_doc_tracking.search_exp',array('deputy'=>$deputy,'executing'=>$executing,'sender'=>$sender,'rows'=>$rows));
     } 

        

        //----------Multipale Searching

        public function load_data_forsearch()
        {

          $deputy     =DB::connection('docscom_document_tracking')->table("deputy_tb")->get(); 
          $doc_tb     =DB::connection('docscom_document_tracking')->table("doc_tb")->get();
          $executing  =DB::connection('docscom_document_tracking')->table("executing_tb")->get(); 
          $sender     =DB::connection('docscom_document_tracking')->table("sender")->get(); 
          $search_data    =DB::connection('docscom_document_tracking')->table('doc_tb')->select('doc_tb.*','deputy_tb.dup_name','executing_tb.exe_name','sender.sender_name','uploads.file_name')
            ->leftjoin('executing_tb','doc_tb.executive_id', '=', 'executing_tb.id')
            ->leftjoin('sender','doc_tb.sender_id','=','sender.id')
            ->leftjoin('deputy_tb','doc_tb.deputy_id','=','deputy_tb.id')
            ->leftjoin('uploads','doc_tb.id','=','uploads.file_id')
            // ->groupBy('file_id')
            ->where('status','=','0')
            ->get();
// return($search_data);
    return view('docscom_doc_tracking.search_doc',array('deputy'=>$deputy,'executing'=>$executing,'sender'=>$sender,'search_data'=>$search_data));
        }




     public function doc_search_engin(Request $request){
      // dd($_POST);
      // $frome_date=$Input('frome_date');
      // if($request->Input('from_date')!=""){
      //   $from_date        =toGregorian(gregorian_format($request->Input('from_date')));
      // }
      // if($request->Input('to_date')!=""){
      //   $to_date          =toGregorian(gregorian_format($request->Input('to_date')));
      // }
        $deputy     =DB::connection('docscom_document_tracking')->table("deputy_tb")->get(); 
        $doc_tb     =DB::connection('docscom_document_tracking')->table("doc_tb")->get();
        $executing  =DB::connection('docscom_document_tracking')->table("executing_tb")->get(); 
        $sender     =DB::connection('docscom_document_tracking')->table("sender")->get(); 
        $import_no        =$request->Input('import_no');
        $doc_no           =$request->Input('doc_no');
        $order_no         =$request->Input('order_no');
        $export_no        =$request->Input('export_no');
        $executive_id     =$request->Input('exe_name');
        $sender_id        =$request->Input('sender_name');
        $deputy_id        =$request->Input('dup_name');
        $condition        =$request->Input('condition');

        $rows=DB::connection('docscom_document_tracking')->table('doc_tb')->select('doc_tb.*','deputy_tb.dup_name','executing_tb.exe_name','sender.sender_name','uploads.file_name')
                ->where('doc_tb.status',0)
                ->leftjoin('executing_tb','doc_tb.executive_id', '=', 'executing_tb.id')
                ->leftjoin('sender','doc_tb.sender_id','=','sender.id')
                ->leftjoin('deputy_tb','doc_tb.deputy_id','=','deputy_tb.id')
                ->leftjoin('uploads','doc_tb.id','=','uploads.file_id');
                // ->groupBy('file_id')
                     
        if ($request->input('from_date') && $request->input('to_date'))
          {    
            $start = date("Y-m-d",strtotime(toGregorian(gregorian_format($request->Input('from_date')))));
            $end = date("Y-m-d",strtotime(toGregorian(gregorian_format($request->Input('to_date'))."+1 day")));
            $rows->whereBetween('import_date',[$start,$end]);
           }

        if($import_no!="")
           $rows->where("import_no" , $import_no);
                
        if($doc_no!="")
           $rows->where("doc_no" , $doc_no);
                
        if($order_no!="")
           $rows->where("order_no" , $order_no);

        if($export_no!="")
           $rows->where("export_no" , $export_no);

        if($executive_id !="")
           $rows->where("executive_id" , $executive_id);

        if($sender_id !="")
           $rows->where("sender_id" , $sender_id);

        if($deputy_id !="")
           $rows->where("deputy_id" , $deputy_id);

        if($condition !="")
           $rows->where("condition" , $condition);
         
        $data['rows'] = $rows->get();
             
        return view("docscom_doc_tracking.search_engin", $data);
               
        }


                         //---------- Download data to Excel sheet----------

    public function exportExcel(){
        $executive_id =Input::get('exe_name');
        $sender_id    =Input::get('sender_name');
        $deputy_id  =Input::get('dup_name');
        $condition =Input::get('condition');
        $all_data=DB::connection('docscom_document_tracking')->table('doc_tb')->select('doc_tb.*','deputy_tb.dup_name','executing_tb.exe_name','sender.sender_name','uploads.file_name')
                ->where('doc_tb.status',0)
                ->leftjoin('executing_tb','doc_tb.executive_id', '=', 'executing_tb.id')
                ->leftjoin('sender','doc_tb.sender_id','=','sender.id')
                ->leftjoin('deputy_tb','doc_tb.deputy_id','=','deputy_tb.id')
                ->leftjoin('uploads','doc_tb.id','=','uploads.file_id');
                // ->groupBy('file_id')
        if($executive_id!="")
           $all_data->where("executive_id" , $executive_id);
                
        if($sender_id!="")
           $all_data->where("sender_id" , $sender_id);
                
        if($deputy_id!="")
           $all_data->where("deputy_id" , $deputy_id);

        if($condition!="")
           $all_data->where("condition" , $condition);
           $data= $all_data->get();
           $curr_date = date('Y-m-d');
        Excel::load('docscom_doc_excel_template/doc_trucking.xlsx', function($file) use($data){
          //Excel::create('Filename', function($file) use($data){        
           $file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){   
           $row = 3;
                //$sheet->setFreeze('A3');
               
        foreach($data AS $item){
                $sheet->getStyle('A2:R' . $sheet->getHighestRow())->getAlignment()->setWrapText(true);
             if($item->condition ==1)
                $condition = "حفظ شده";
         elseif($item->condition ==2)
                $condition ="تحت کار";
           else $condition ="ازحیث ارتباط";

                    $date_day=$item->condition == '2' ? $diff = Carbon\Carbon::parse($item->import_date)->diffInDays() : $item->passed_day ;
                    $sheet->setHeight($row, 20);
                    $sheet->setCellValue('A'.$row.'',$item->reasons);
                    $sheet->setCellValue('B'.$row.'',$condition);
                    $sheet->setCellValue('C'.$row.'',checkEmptyDate($item->export_date));
                    $sheet->setCellValue('D'.$row.'',$item->export_no);
                    $sheet->setCellValue('E'.$row.'',checkEmptyDate($item->order_date));
                    $sheet->setCellValue('F'.$row.'',$item->order_no);
                    $sheet->setCellValue('G'.$row.'',$item->summary);
                    $sheet->setCellValue('H'.$row.'',$item->execution);
                    $sheet->setCellValue('I'.$row.'',$item->exe_name);
                    $sheet->setCellValue('J'.$row.'',$item->dup_name);
                    $sheet->setCellValue('K'.$row.'',$item->sender_name);
                    $sheet->setCellValue('L'.$row.'',checkEmptyDate($item->import_date));
                    $sheet->setCellValue('M'.$row.'',$date_day );
                    $sheet->setCellValue('N'.$row.'',$item->import_no);
                    $sheet->setCellValue('O'.$row.'',checkEmptyDate($item->doc_date));
                    $sheet->setCellValue('P'.$row.'',$item->doc_no);
                    $sheet->setCellValue('Q'.$row.'',$item->doc_type);
                    $sheet->setCellValue('R'.$row.'',$row-2);
                    
                   
                    $row++;
                }
 
                $sheet->setBorder('A3:R'.($row-1).'', 'thin');
               
            });
           
            })->export('xlsx');
    }


      }
