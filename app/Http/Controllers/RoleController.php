<?php namespace App\Http\Controllers;
/*
@desc: Manipulates the entities roles
@Author: Gul Muhammad Akbari (gm.akbari27@gmail.com)
@Created At: 23 Feb 2015
@version: 1.0
*/

use Auth;
use App\models\RoleX;
use Illuminate\Support\Collection;
use App\models\Section;
use View;
use Input;
use Redirect;
use Validator;

class RoleController extends Controller
{

	/*
	Getting all records
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getAll($section_id=0)
	{
		//check role
		if(!canView('auth_role'))
		{
			return showWarning();
		}
		else
		{
			//load view for list
			return \View::make("auth.role.role_list")->with('section_id',$section_id);
		}
	}
	//get data for datatable
	public function getData($sectionId=0)
	{
		//check role
		if(!canView('auth_role'))
		{
			return showWarning();
		}
		else
		{
			
			

			//get data from model
			$roles = RoleX::getData($sectionId);
			$collection = new Collection($roles);
			return \Datatable::collection($collection)
						->showColumns('id','code','name','section','module')
						->addColumn('operation', function($option){
							//get options in list
							$options = '';
							if(canEdit('auth_role'))
							{
								$options .= '<a href="'.route('getUpdateRole',$option->id).'"><i class="fa fa-edit"></i> Edit</a> &nbsp;';
							}
							if(canDelete('auth_role'))
							{
								$options .= '|&nbsp;<a href="'.route('getDeleteRole',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="fa fa-trash-o"></i> Delete</a>';
							}

							return $options;
						})
						->make();
		}
	}
	/*
	getting form for inserting module's Role
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreateRole()
	{
		//check role
		if(!canAdd('auth_role'))
		{
			return showWarning();
		}
		else
		{
		
			//get all applications
			$apps = Section::getAll();
			//load view for users list
			return View::make("auth.role.role_create")->with('apps',$apps);
		}
	}

	//get update module's Role
	public function getUpdateRole($id=0)
	{
		//check role
		if(!canEdit('auth_role'))
		{
			return showWarning();
		}
		else
		{

			//get all applications
			$apps = Section::getAll();

			//get details
			$details = RoleX::getDetails($id);
			
			//load view for editing 
			return View::make('auth.role.role_edit')->with('details',$details)->with('apps',$apps);
		}
	}

	/*
	Inserting filled form to module's role
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function postCreateRole()
	{
		//check role
		if(!canAdd('auth_role'))
		{
			return showWarning();
		}
		else
		{
			//validate the input fields
		    $validates = Validator::make(Input::all(),array(
		        "code" => "required",
		        "name" => "required",
		        "section"=>'required'
		    ));
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return Redirect::route("getCreateRole")->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        //create an object from Role class
		        $object = new RoleX();
		        $object->code = Input::get("code");
		        $object->name = Input::get("name");
		        $object->entity_id = Input::get("section");
		        $object->user_id = Auth::user()->id;

		        if($object->save())
		        {
		            return Redirect::route("getAllRoles")->with("success","You successfuly created new Role.");
		        }
		        else
		        {
		            return Redirect::route("getAllRoles")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		
	}
	public function postUpdateRole($id=0)
	{
		//check role
		if(!canEdit('auth_role'))
		{
			return showWarning();
		}
		else
		{
			//validate the input fields
		    $validates = \Validator::make(Input::all(),array(
		        "code" => "required",
		        "name" => "required",
		        "section" => "required"
		    ));
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return Redirect::route("getUpdateRole",$id)->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        //create an object from class
		        $dep = RoleX::find($id);
		        $dep->code = Input::get("code");
		        $dep->name = Input::get("name");
		        $dep->entity_id = Input::get("section");
		        
		        if($dep->save())
		        {
		            return Redirect::route("getAllRoles")->with("success","You successfuly updated record.");
		        }
		        else
		        {
		            return Redirect::route("getAllRoles")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		
	}
	//delete department
	public function getDeleteRole($id=0)
	{
		//check role
		if(!canDelete('auth_role'))
		{
			return showWarning();
		}
		else
		{
			$dep = RoleX::find($id);
			if($dep->delete())
	        {
	            return Redirect::route("getAllRoles")->with("success","You successfuly deleted record, ID: <span style='color:red;font_weight:bold;'>{{$id}}</span>");
	        }
	        else
	        {
	            return Redirect::route("getAllRoles")->with("fail","An error occured plase try again.");
	        }
	    }
	}

}


?>