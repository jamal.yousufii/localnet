<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use App\Notifications\NewProject;
use App\models\Requests;
use App\User;
use App\Surveyor;
use Validator;
use Redirect;

class SurveyController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    // Get Language
    $lang = get_language();
    // Get Data
    $record = array();
    // Load view to show result
    return view('egov/survey/view',compact('lang',$record));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $lang = get_language();
    $lang  = $lang;
    // Get equipments
    $equipments = getStaticData('equipments');
    // Get emails
    $emails = getStaticData('emails');
    // Get websites
    $websites = getStaticData('website');
    // Get efficiency
    $efficiency = getStaticData('efficiency');
     // Get yesNo
     $yesNo = getStaticData('yesNo');
     // Get applications
     $applications = getStaticData('application');
    // Load view to show result
    return view('egov/survey/create',compact('lang','equipments','emails','websites','efficiency','yesNo','applications'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
        'name'      => 'required',
        'job'      => 'required',
        'education' => 'required',
        'organization_id'  => 'required',
    ]);
    // Language
    $lang = get_language();
    // Generate URN
    $urn = GenerateURN('requests','urn');
    // Insert Record
    $record = new Requests;
    $record->urn           = $urn;
    $record->request_date  = dateProvider($request->request_date,$lang);
    $record->doc_type      = $request->doc_type;
    $record->department_id = $request->department_id;
    $record->doc_number    = $request->request_number;
    $record->goals         = $request->goals;
    $record->description   = $request->description;
    $record->created_at    = date('Y-m-d H:i:s');
    $record->created_by    = userid();
    $record->department    = departmentid();
    $record->save();
    if($record->id>0)
    {
    
      Session()->flash('success', __("global.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Dycript the id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get Departments
    $data['deps']  = HRDepartments::select('id','name_'.$lang.' as name')->get();
    // Get Document Type
    $data['types'] = Documents::select('id','name_'.$lang.' as name')->get();
    // Get Record
    $data['record'] = Requests::find($id);
    // Load view to show result
    return view('pmis/requests/edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    $lang = get_language();
    $id = decrypt(Input::get('enc_id'));
    $req = Requests::find($id);
    $req->request_date  = dateProvider($request->request_date,$lang);
    $req->department_id = $request->department_id;
    $req->doc_type      = $request->doc_type;
    $req->doc_number    = $request->request_number;
    $req->goals         = $request->goals;
    $req->description   = $request->description;
    $req->updated_by    = userid();
    $updated = $req->save();
    if($updated)
    {
      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  // Bring more website
  public function more_websites()
  {
      // Get Language
      $lang = get_language();
      // Get Counter Number
      $number = Input::post('number');
      // Get websites
      $websites = getStaticData('website');
      // Load view
      return view('egov/survey/websites',compact('number','lang','websites')); 
  }
  // Bring more application
  public function more_applications()
  {
      // Get Language
      $lang = get_language();
      // Get Counter Number
      $number = Input::post('number');
      // Get applications
      $applications = getStaticData('applications');
      // Load view
      return view('egov/survey/applications',compact('number','lang','applications')); 
  }

}
?>