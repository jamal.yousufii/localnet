<?php namespace App\Http\Controllers;

use Redirect;
use Illuminate\Http\Request;
use Auth;
use Input;
use Session;
use DB;
use LaravelGettext;
use Saml2;

        
class calendarController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/
	
	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	
	public function saveCalendarEvent()
	{
		
		$type = Input::get('type');

		if($type == 'new')
		{
			$startdate = Input::get('startdate').'+'.Input::get('zone');
			$title = Input::get('title');
			
			$data = array(
						"title" => $title,
						"startdate" => $startdate,
						"enddate" => $startdate,
						"allDay" => 'false',
						"user_id" => Auth::user()->id
				);
				
			// $insert = mysqli_query($con,"INSERT INTO calendar(`title`, `startdate`, `enddate`, `allDay`) VALUES('$title','$startdate','$startdate','false')");
			// $lastid = mysqli_insert_id($con);
			
			$lastid = DB::table('calendar')->insertGetId($data);
			
			return json_encode(array('status'=>'success','eventid'=>$lastid));
		}
		
		if($type == 'changetitle')
		{
			$eventid = Input::get('eventid');
			$title = Input::get('title');
			
			$data = array(
						"title" => $title
				);
			
			$update = DB::table('calendar')->where('id',$eventid)->update($data);
			
			if($update)
				return json_encode(array('status'=>'success'));
			else
				return json_encode(array('status'=>'failed'));
		}
		
		if($type == 'resetdate')
		{
			$title = Input::get('title');
			$startdate = Input::get('start');
			$enddate = Input::get('end');
			$eventid = Input::get('eventid');
			
			$data = array(
						"title" => $title,
						"startdate" => $startdate,
						"enddate" => $enddate
				);
				
			$update = DB::table('calendar')->where('id',$eventid)->update($data);
			
			if($update)
				return json_encode(array('status'=>'success'));
			else
				return json_encode(array('status'=>'failed'));
		}
		
		if($type == 'remove')
		{
			$eventid = Input::get('eventid');
			
			$delete = DB::table('calendar')->where('id',$eventid)->delete();
			if($delete)
				return json_encode(array('status'=>'success'));
			else
				return json_encode(array('status'=>'failed'));
		}
		
		if($type == 'fetch')
		{
			$events = array();
			$query = DB::table('calendar')->where('user_id',Auth::user()->id)->get();
			//$query = mysqli_query($con, "SELECT * FROM calendar");
			foreach($query AS $fetch)
			{
				$e = array();
				
			    $e['id'] = $fetch->id;
			    $e['title'] = $fetch->title;
			    $e['start'] = $fetch->startdate;
			    $e['end'] = $fetch->enddate;
			
			    $allday = ($fetch->allDay == "true") ? true : false;
			    
			    $e['allDay'] = $allday;
			
			    array_push($events, $e);
			}
			return json_encode($events);
		}		
			
	}
	
}
