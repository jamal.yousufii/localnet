<?php 

namespace App\Http\Controllers\workplan;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Input;
use DB;
use Datatable;
use App\models\workplan\It_task_model;
use App\models\workplan\It_taskGroup;
use App\models\User;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\Config;
use Validator;
use Illuminate\Support\Str;
use Response;
use Excel;

use App\models\DepartmentX;
use Illuminate\Support\Collection;

class It_task extends Controller
{
	//get report list - all
	public function getReport($week=0,$mode='all',$year=0,$week_no=0)
	{
		if($week == 0)
		{
			$week = date('W');
		}
		if($year == 0)
		{
			$year = date('Y');
		}

		$data['week_no'] = $week_no;//for tooltip
		$data['week'] = $week;
		$data['year'] = $year;
		
		$data['mode'] = $mode;
		return View::make('it_task.tasks.create_workplan',$data);		
		
	}
	//get report list - by group
	public function getReportGroup($id=0,$mode='all')
	{
		$data['id'] = $id;
		$data['mode'] = $mode;
		if($id != 0)
		{
			$data['details'] = It_taskGroup::find($id);
			
			$t2 = DB::connection('helpdesk')
					->table('tasks AS t')
					->select(
						't.title',
						't.id AS task_id',
						't.user_id',
						't.start_date',
						't.end_date',
						't.created_at'
						)
					->where('task_group_id',$id)
					->where('parent_task',0)
					->where('percentage','!=','100');
					//->where(DB::raw('YEAR(start_date)'),$year);
					
				if(!isAdmin())
				{
					if($mode == 'my')
					{
						$t2->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
						//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
						$t2->where(function($q){
							$q->whereRaw('(ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2)');		
							$q->orWhereIN('t.id',function($query)
				           {
				                $query->select('tasks.parent_task')
									->from('tasks')
				                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
									->where('tasks.parent_task','!=',0)
				                    ->whereRaw('(sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2)');
				           });
						});
					}
					elseif($mode == 'out')
					{
						$t2->where(function($q){
							$q->Where('t.user_id',Auth::user()->id);
							$q->orWhereIN('t.id',function($query)
				           {
				                $query->select('tasks.parent_task')
									->from('tasks')
				                    //->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
									->where('tasks.parent_task','!=',0)
				                    ->Where('tasks.user_id',Auth::user()->id);
				           });
						});
					}
					else
					{
						$t2->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
						//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
						$t2->where(function($q){
							$q->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');		
							$q->orWhereIN('t.id',function($query)
				           {
				                $query->select('tasks.parent_task')
									->from('tasks')
				                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
									->where('tasks.parent_task','!=',0)
				                    ->whereRaw('((sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2) or tasks.user_id = '.Auth::user()->id.')');
				           });
						});
					}
				}
	
			$data['pages'] = round((count($t2->get()))/Config::get('myConfig.record_per_page'));
		}
		else
		{//completed tasks
			$data['details'] = '';
			
			$t2 = DB::connection('helpdesk')
					->table('tasks AS t')
					->select(
						't.title',
						't.id AS task_id',
						't.user_id',
						't.start_date',
						't.end_date',
						't.created_at'
						)
					//->where('task_group_id',$id)
					->where('parent_task',0)
					->where('completed',1);
					//->where(DB::raw('YEAR(start_date)'),$year);
					
				if(!isAdmin())
				{
					$t2->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
					//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
					$t2->where(function($q){
						$q->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');		
						$q->orWhereIN('t.id',function($query)
			           {
			                $query->select('tasks.parent_task')
								->from('tasks')
			                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
								->where('tasks.parent_task','!=',0)
			                    ->whereRaw('((sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2) or tasks.user_id = '.Auth::user()->id.')');
			           });
					});
					
				}
			$data['pages'] = round((count($t2->get()))/Config::get('myConfig.record_per_page'));
		}
		
		return View::make('it_task.task_group.list_by_group',$data);		
		
	}
	//create report task
	public function getReportCreate()
	{
		$taskGroup = It_taskGroup::getAll();
		$users = It_task_model::getDepUsers();
		$managers = It_task_model::getDepUsers(true);
		$data['users'] = $users;
		$data['managers'] = $managers;
		$data['taskGroup'] = $taskGroup;

		//get all reports from database
		$data['reports'] = It_task_model::getReport();
		//get tasks assigned to me
		$data['assigned_to_me'] = It_task_model::getTasksAssignedToMe();

		return View::make('it_task.tasks.create_workplan',$data);
	}

		//save new task via ajax
	public function saveNewTask()
	{
		$report = new It_task_model;
		$report->title 						= Input::get("title");
		$report->task_group_id				= Input::get("task_group");
		$report->parent_task				= Input::get("parent");
		$report->user_id 					= Auth::user()->id;
		$report->start_date					= date('Y-m-d');
		$report->end_date					= date('Y-m-d');
		
		if($report->save())
		{
			$insertedId = $report->id;
			
			//get insert item
			$row = It_task_model::find($insertedId)->toArray();
			//--- li for ajax response --------------------------//
			
			if(Input::get('from_dash'))
			{
				$ul = '<li class="list-group-item li-item" title="'.Input::get('title').'">
                  <i class="wb-check red-600 margin-right-10" aria-hidden="true"></i>
                  <a href="javascript:void()" data-target="#task_detail" data-toggle="modal" onclick="load_task_detail(\'task_detail\','.$row['id'].')">
                  '.Input::get('title').'
                  </a>
                </li>';
                return $ul;
			}
			$end_date = date('Y-m-d');
			
			$from=date_create($end_date);
			$to=date_create(date('Y-m-d'));
			
			$diff=date_diff($to,$from);
			
			$deadline = $diff->format('%R%a');
			$d=$deadline;

	        $label_status = "";
	        $days;

	        switch (true) {
	        	case ($d < 0):
	        		$days='Due';
	        		break;
	        	case ($d == 0):
	        		$days='Today';
	        		break;
	        	case ($d == 1):
	        		$days='Tomorrow';
	        		break;
	        	case ($d > 1 and $d < 7):
	        		$days=$d.' Days';
	        		break;
	        	case ($d > 6 and $d < 15):
	        		$days='Next Week';
	        		break;
	        	case ($d > 14 and $d < 22):
	        		$days='Two Weeks';
	        		break;
	        	case ($d > 21 and $d < 29):
	        		$days='Three Weeks';
	        		break;
	        	case ($d > 28 and $d < 56):
	        		$days='Next Month';
	        		break;
	        	case ($d > 55 and $d < 336):
	        		$nm=round($d/30);
	        		$days=$nm.' Months';
	        		break;
	        	case ($d > 335):
	        		$ny=round($d/365);
	        		$days=$ny.' Year(s)';
	        		break;
	        	default:
	        		$days='select date';
	        		break;
	        }

	        if($d>0)
	        {
	            $label_status = "label-success";
	      }
	        else if($d == 0)
	        {
	            $label_status = "label-warning";
	      }
	        else
	        {
	            $label_status = "label-danger";
	      }

			$progress = getMainTaskProgress_it($row['id']);
			
			$s_date = '00-00-0000';
			$e_date = '00-00-0000';
	
			$progress = getMainTaskProgress_it($row['id']);
			
			if(Input::get("parent")!='')
			{//sub task from task detail view
				$ul = '<li>
							<div class="checkbox-custom checkbox-inline" style="margin-top:-1em;">
								<input type="checkbox" disabled>
								<label>&nbsp;</label>
							</div>
							<a href="javascript:void()" onclick="load_task_detail(\'sub_task_detail\',\''.$row['id'].'\')">
								<div class="task_title big_title" style="margin-top:0.3em;">'.$row['title'].'</div>
							</a>
							<span id="label_'.$row['id'].'" style="margin-right: 170px;direction:rtl;float: right;position:relative;">
								<a href="javascript:void()" onclick="load_task_date_sub(\'sub_task_detail\','.$row['id'].')" id="label_updated_'.$row['id'].'">
									<span class="label '.$label_status.' date_middle"> '.$days.'</span>
								</a>
							</span>
					   </li>';
			}
			else
			{
				$ul = '<li class="0 list_main_task" id="list_'.$row['id'].'">
							<div class="checkbox-custom checkbox-inline" style="margin-top:-1em;">
								<input type="checkbox" disabled>
								<label>&nbsp;</label>
							</div>
							<a href="#task_detail" data-toggle="modal" onclick="load_task_detail(\'task_detail\',\''.$row['id'].'\')">
								<div class="task_title big_title" style="margin-top:0.3em;">'.$row['title'].'</div>
							</a>
							<span id="label_'.$row['id'].'" style="margin-right: 170px;direction:rtl;float: right;position:relative;">
								<a data-toggle="modal" href="#task_detail" onclick="load_task_date(\'task_detail\','.$row['id'].')" id="label_updated_'.$row['id'].'">
									<span class="label '.$label_status.' date_middle"> '.$days.'</span>
								</a>
							</span>
							<div class="pull-right">                     
		                        <div class="assignees" style="display:inline;">
			                        <a data-toggle="modal" href="#task_detail" onclick="load_task_assignee('.$row['id'].')">
										<img src="/img/default.png" class="project-img-owner">
									</a>
								</div>';
		                	                        
		        $ul .=    '<div onclick="loadModal('.$row['id'].')" class="progress sub_progress" data-modal="progress_modal" title="'.$progress.'% Completed" style="display:inline-block;">
		                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="'.$progress.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$progress.'%;">
		                                <span class="sr-only">'.$progress.'% Complete</span>
		                            </div>
		                        </div>
		                    </div>
						</li>';
			}
			$ul .= '</li>';
			return $ul;
		}

	}

	//get task details
	public function loadDetailsViaAjax()
	{
		//get task id
		$report_id = Input::get('task_id');
		$data['progress'] = getMainTaskProgress_it($report_id);
		$taskGroup = It_taskGroup::getAll();
		$users = It_task_model::getDepUsers();
		$managers = It_task_model::getDepUsers(true);
		$data['users'] = $users;
		$data['managers'] = $managers;

		$data['taskGroup'] = $taskGroup;
		//get task assigned to
		$assignees = It_task_model::getTaskAssignee($report_id);
		
		$assign_arr = array();
		foreach($assignees AS $item)
		{
			$assign_arr[] = $item->assigned_to;
		}

		$data['assignees'] = $assign_arr;
		
		//get task details
		$data['task_details'] = It_task_model::getTaskDetails($report_id);
		//get the attachments
		$data['files'] = It_task_model::get_task_files($report_id);
		//get sub tasks 
		$data['sub_tasks']	= It_task_model::getSubTasks($report_id);
		$data['type'] = Input::get('type');
		return View::make('it_task.tasks.edit_workplan',$data);

	}
	
	public function updateReportViaAjax()
	{
		$report_id = \Crypt::decrypt(Input::get('task_id'));
		if(!isTaskCreator_it($report_id))
		{
			return \Redirect::route("getReport_it")->with("fail",_('you_can_not_update_this_task'));
		}
		//$report_id = Input::get('task_id');
		$validates = \Validator::make(Input::all(),array(
	        "title"		=> "required"
	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("ViewReport_it", $report_id)->withErrors($validates)->withInput();
	    }
	 	else
	 	{
		    $update_report 	= It_task_model::find($report_id);
			
			$sdate = Input::get("start_date");
			$edate = Input::get("end_date");
			
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			if($edate != '')
			{
				$edate = explode("-", $edate);
				$ey = $edate[2];
				$em = $edate[1];
				$ed = $edate[0];
				$edate = dateToMiladi($ey,$em,$ed);		
			}
			
		    $update_report->title 					 	= Input::get("title");
		    $update_report->description 			 	= Input::get("description");
		    //$update_report->quantity_of_deliverables 	= Input::get("quantity_of_deliverables");
		    //$update_report->indicator 				 			= Input::get("indicator");
		    //$update_report->metric 					 			= Input::get("metric");
		    $update_report->task_group_id			 	= Input::get("task_group");
		    //$update_report->expected_outcome 		 	= Input::get("expected_outcome");
			$update_report->start_date 					= $sdate;
		    $update_report->end_date 				 	= $edate;
		    $update_report->notify_by_email 		 	= Input::get("by_email");
			//$update_report->notify_by_sms 			 		= Input::get("by_sms");
			$update_report->subordinates 			 	= 1;
		    
		    if($update_report->save())
		    {
		    	$task_id = $report_id;
				
		    	$assign_with = Input::get("assign_with1");
		    	
				$new_assignees = array();
				$old_assignees = array();
				$all_assignees = It_task_model::getTaskAssignee($task_id);	
				if($all_assignees)
				{
					foreach($all_assignees AS $assignee)
					{
						$old_assignees[$assignee->assigned_to] = 1;
					}
				}
			
				if(count($assign_with)>0)
				{
					$items = array();
					$notification = array();
					
					for($i=0;$i<count($assign_with);$i++)
					{
						if(!is_assigned_before_it($assign_with[$i],$task_id))
						{//check if this user was assigned to this task before, if yes no need to assign him again
							$task_status = getTaskStatusByUser_it($assign_with[$i],$task_id);
							if($task_status)
							{//check if user has rejected the task before
								//update the task_assign_to recored from rejected
								It_task_model::updateRecord('task_assigned_to',array('status'=>0),$task_status->id);
								//add new notification
								$notification = array(
									"task_id" 			=> $task_id,
									"from" 				=> Auth::user()->id,
									"to" 				=> $assign_with[$i],
									"notification_type" => 1,//means notification type is asigned task
									"created_at" 		=> date('Y-m-d H:i:s')
									);
							}	
							else
							{//add new records for new assignees
								$items = array(
										"task_id" => $task_id,
										"assigned_to" => $assign_with[$i],
										"assigned_by" => Auth::user()->id,
										"assigned_date" => date('Y-m-d H:i:s')
									);
						
								$notification = array(
									"task_id" 			=> $task_id,
									"from" 				=> Auth::user()->id,
									"to" 				=> $assign_with[$i],
									"notification_type" => 1,//means notification type is asigned task
									"created_at" 		=> date('Y-m-d H:i:s')
									);
							}
						}
						$new_assignees[$assign_with[$i]] = 1;
						//$notifications[] = $notification;
						$userEmail = User::find($assign_with[$i])->email;	
						$assigned_by_first = User::find(Auth::user()->id)->first_name;
						$assigned_by_last = User::find(Auth::user()->id)->last_name;
						$assigned_to_first = User::find($assign_with[$i])->first_name;
						//$assigned_to_last = User::find($assign_with[$i])->last_name;
						$by_email = 0;
						if(Input::get('by_email'))
						{
							$by_email = 1;
						}
						
						if($by_email == 1 && $userEmail != '')
						{
							$user_email = array('email'=>$userEmail,'name_by'=>$assigned_by_first.' '.$assigned_by_last,'name_to'=>$assigned_to_first);
							if(count($old_assignees)>0)
							{//if there is any saved assignee already
								if(array_key_exists($assign_with[$i], $old_assignees))	
								{//check if the user already exists
									if($old_assignees[$assign_with[$i]] == 0)
									{//check if email was not sent to him, send him email
										if(Mail::send('emails.task_assigned',$user_email, function($message) use ($user_email){
										    $message->from('e.attendance.aop@gmail.com', 'Task Management');
											$message->to($user_email['email'])->subject('New Task');
										}))
										{
											$items['email_notify']=1;
										}
										else
										{
											$items['email_notify'] = 0;
										}		
									}
									else
									{//if email has been sent
										$items['email_notify']=1;
									}
								}
								else
								{//the ueser is new assigned to this task, send him email
									if(Mail::send('emails.task_assigned', $user_email, function($message) use ($user_email){
									    $message->from('e.attendance.aop@gmail.com', 'Task Management');
										$message->to($user_email['email'])->subject('New Task');
									}))
									{
										$items['email_notify']=1;
									}
									else
									{
										$items['email_notify'] = 0;
									}	
								}
								
							}
							else
							{//if no assignee is assigned before, send him email
								if(Mail::send('emails.task_assigned', $user_email, function($message) use ($user_email){
								    $message->from('e.attendance.aop@gmail.com', 'Task Management');
									$message->to($user_email['email'])->subject('New Task');
								}))
								{
									$items['email_notify']=1;
								}
								else
								{
									$items['email_notify'] = 0;
								}
							}
						}
						//$data[] = $items;
					}
					//remove the old records
			    	DB::connection('helpdesk')->table('task_assigned_to')->where('task_id',$task_id)->delete();
					DB::connection('helpdesk')->table('notifications')->where('task_id',$task_id)->delete();			
					//insert assigned info

					if(count($items)>0)
					{
						It_task_model::insertBatch('task_assigned_to',$items);
					}
					if(count($notification)>0)
					{
						It_task_model::insertBatch('notifications',$notification);
					}
					
				}
				else
				{
					//remove the old records
			    	DB::connection('helpdesk')->table('task_assigned_to')->where('task_id',$task_id)->delete();
					DB::connection('helpdesk')->table('notifications')->where('task_id',$task_id)->delete();
				}
				/*
				//check if any assignee is removed from this task
				$removed_assignees = array_diff_assoc($old_assignees, $new_assignees);	
				
				if(count($removed_assignees)>0)
				{		
					foreach($removed_assignees as $x => $x_value)
					{
						$userEmail = User::find($x)->email;	
						if($userEmail != '')
						{
							Mail::send('emails.task_assigned', array('email'=>$userEmail,'name'=>''), function($message) use($userEmail){
						    	//$message->from('p.alihussain@gmail.com', 'Laravel');
								$message->to($userEmail);
							});
						}
					}
				}
				*/
				//save the attachments
				$this->uploadDocs($task_id);
				//calculation for tooltip
				$week_edate = getFirstWeek_it(date('Y'),date('W'),'e');
				$week_no = date("W", strtotime($sdate));
				if($week_edate < $sdate)
				{//if the start date of task is out of current week the show the tooltip
					return \Redirect::route("getReportTooltip_it",array(date('W'),'all',date('Y'),$week_no))->with("success","Record Saved Successfully.");
				}
				else
				{
					//\Session::flash('success','Record Saved Successfully.');
					return \Redirect::route("getReport_it")->with("success","Record Saved Successfully.");
					//return json_encode(array('cond'=>'true','week_no'=>$week_no));
				}
			}
			else
			{
				//\Session::flash('fail','Not Saved.');
				return \Redirect::route("getReport_it")->with("success","Not Saved.");
				//return json_encode(array('cond'=>'false'));
	
			}
	 	}

	}
	public function updateAssigneeViaAjax()
	{
	 	$task_id = Input::get('task_id');
	   
    	if(Input::get('subordinates'))
    	{
    		$assign_with = Input::get("assign_with");
			$sub = 0;
    	}
    	else
    	{
    		$assign_with = Input::get("assign_with1");
			$sub = 1;
    	}
		
		$new_assignees = array();
		$old_assignees = array();
		$all_assignees = It_task_model::getTaskAssignee($task_id);	
		if($all_assignees)
		{
			foreach($all_assignees AS $assignee)
			{
				$old_assignees[$assignee->assigned_to] = $assignee->email_notify;
			}
		}
		
		if(count($assign_with)>0)
		{
			$update_report 	= It_task_model::find($task_id);
			$update_report->subordinates 	= $sub;
			$update_report->save();
			
			$items = array();
			$notification = array();	
			for($i=0;$i<count($assign_with);$i++)
			{
				if(!is_assigned_before_it($assign_with[$i],$task_id))
				{//check if this user was assigned to this task before, if yes no need to assign him again
					$task_status = getTaskStatusByUser_it($assign_with[$i],$task_id);
					if($task_status)
					{//check if user has rejected the task before
						//update the task_assign_to recored from rejected
						It_task_model::updateRecord('task_assigned_to',array('status'=>0),$task_status->id);
						//add new notification
						$notification = array(
							"task_id" 			=> $task_id,
							"from" 				=> Auth::user()->id,
							"to" 				=> $assign_with[$i],
							"notification_type" => 1,//means notification type is asigned task
							"created_at" 		=> date('Y-m-d H:i:s')
							);
					}	
					else
					{//add new records for new assignees
						$items = array(
								"task_id" => $task_id,
								"assigned_to" => $assign_with[$i],
								"assigned_by" => Auth::user()->id,
								"assigned_date" => date('Y-m-d H:i:s')
							);
				
						$notification = array(
							"task_id" 			=> $task_id,
							"from" 				=> Auth::user()->id,
							"to" 				=> $assign_with[$i],
							"notification_type" => 1,//means notification type is asigned task
							"created_at" 		=> date('Y-m-d H:i:s')
							);
					}
				}
				$new_assignees[$assign_with[$i]] = 1;
				
				$userEmail = User::find($assign_with[$i])->email;	
				$by_email = 0;
				if(Input::get('by_email'))
				{
					$by_email = 1;
				}
				if($by_email == 1 && $userEmail != '')
				{
					$user_email = array('email'=>$userEmail,'name'=>$assign_with[$i]);
					if(count($old_assignees)>0)
					{//if there is any saved assignee already
						if(array_key_exists($assign_with[$i], $old_assignees))	
						{//check if the user already exists
							if($old_assignees[$assign_with[$i]] == 0)
							{//check if email was not sent to him, send him email
								if(Mail::send('emails.task_assigned',$user_email, function($message) use ($user_email){
								    //$message->from('p.alihussain@gmail.com', 'Laravel');
									$message->to($user_email['email']);
								}))
								{
									$items['email_notify']=1;
								}
								else
								{
									$items['email_notify'] = 0;
								}		
							}
							else
							{//if email has been sent
								$items['email_notify']=1;
							}
						}
						else
						{//the ueser is new assigned to this task, send him email
							if(Mail::send('emails.task_assigned', $user_email, function($message) use ($user_email){
							    //$message->from('p.alihussain@gmail.com', 'Laravel');
								$message->to($user_email['email']);
							}))
							{
								$items['email_notify']=1;
							}
							else
							{
								$items['email_notify'] = 0;
							}	
						}
						
					}
					else
					{//if no assignee is assigned before, send him email
						if(Mail::send('emails.task_assigned', $user_email, function($message) use ($user_email){
						    //$message->from('p.alihussain@gmail.com', 'Laravel');
							$message->to($user_email['email']);
						}))
						{
							$items['email_notify']=1;
						}
						else
						{
							$items['email_notify'] = 0;
						}
					}
				}
				//$data[] = $items;
			}
			
			//remove the old records
	    	//DB::connection('workplan')->table('task_assigned_to')->where('task_id',$task_id)->delete();
			//DB::connection('workplan')->table('notifications')->where('task_id',$task_id)->delete();			
			//insert assigned info
			if(count($items)>0)
			{
				It_task_model::insertBatch('task_assigned_to',$items);
			}
			if(count($notification)>0)
			{
				It_task_model::insertBatch('notifications',$notification);
			}			
		}
		else
		{
			//remove the old records
	    	DB::connection('helpdesk')->table('task_assigned_to')->where('task_id',$task_id)->delete();
			DB::connection('helpdesk')->table('notifications')->where('task_id',$task_id)->delete();
		}
		//check if any assignee is removed from this task
		$removed_assignees = array_diff_assoc($old_assignees, $new_assignees);	
		if(count($removed_assignees)>0)
		{		
			foreach($removed_assignees as $x => $x_value)
			{
				$userEmail = User::find($x)->email;	
				if($userEmail != '')
				{
					Mail::send('emails.task_assigned', array('email'=>$userEmail,'name'=>''), function($message) use($userEmail){
				    	//$message->from('p.alihussain@gmail.com', 'Laravel');
						$message->to($userEmail);
					});
				}
			}
		}
		return json_encode(array('cond'=>'true'));
	

	}
	//change task start/end date
	public function changeTaskDate()
	{	
		$report_id 		= Input::get('task_id');
	    $update_report 	= It_task_model::find($report_id);
	    $sdate 			= Input::get("start_date");
		$edate 			= Input::get("end_date");
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);		
		}
		if($edate != '')
		{
			$edate = explode("-", $edate);
			$ey = $edate[2];
			$em = $edate[1];
			$ed = $edate[0];
			$edate = dateToMiladi($ey,$em,$ed);		
		}
		$update_report->start_date 		= $sdate;
	    $update_report->end_date 		= $edate;
	    
	    if($update_report->save())
	    {
	    	//get insert item
			$row = It_task_model::find($report_id)->toArray();
			
			//--- li for ajax response --------------------------//
			//convert shamsi to meladi
			if($row['end_date'] != '0000-00-00')
			{
				$end_date = $row['end_date'];
				//convert shamsi to meladi
				//$end_date = explode("-", $row['end_date']);
				//$end_date = dateToMiladi($end_date[0],$end_date[1],$end_date[2]);
			}
			else
			{
				$end_date = date('Y-m-d');
			}
			
			$from=date_create($end_date);
			$to=date_create(date('Y-m-d'));
			
			$diff=date_diff($to,$from);
			
			$deadline = $diff->format('%R%a');
			$d=$deadline;

	        $label_status = "";
	        $days;

	        switch (true) {
	        	case ($d < 0):
	        		$days='Due';
	        		break;
	        	case ($d == 0):
	        		$days='Today';
	        		break;
	        	case ($d == 1):
	        		$days='Tomorrow';
	        		break;
	        	case ($d > 1 and $d < 7):
	        		$days=abs($d).' Days';
	        		break;
	        	case ($d > 6 and $d < 15):
	        		$days='Next Week';
	        		break;
	        	case ($d > 14 and $d < 22):
	        		$days='Two Weeks';
	        		break;
	        	case ($d > 21 and $d < 29):
	        		$days='Three Weeks';
	        		break;
	        	case ($d > 28 and $d < 56):
	        		$days='Next Month';
	        		break;
	        	case ($d > 55 and $d < 336):
	        		$nm=round($d/30);
	        		$days=$nm.' Months';
	        		break;
	        	case ($d > 335):
	        		$ny=round($d/365);
	        		$days=$ny.' Year(s)';
	        		break;
	        	default:
	        		$days='select date';
	        		break;
	        }

	        if($d>0)
	        {
	            $label_status = "label-success";
	        }
	        else if($d == 0)
	        {
	            $label_status = "label-warning";
	      }
	        else
	        {
	            $label_status = "label-danger";
	      }
			
	    	//return date("W", strtotime($edate));
	    	//return $days;
	    	echo '<span class="label '.$label_status.' date_middle"> '.$days.'</span>';
	    }

	}
	
	//create report task
	public function postReportCreate()
	{
		$sdate = Input::get("start_date");
		$edate = Input::get("end_date");
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);		
		}
		if($edate != '')
		{
			$edate = explode("-", $edate);
			$ey = $edate[2];
			$em = $edate[1];
			$ed = $edate[0];
			$edate = dateToMiladi($ey,$em,$ed);		
		}
		$report = new It_task_model;
		
		$report->title 						= Input::get("title");
		$report->description 				= Input::get("description");
		//$report->quantity_of_deliverables 	= Input::get("quantity_of_deliverables");
		//$report->indicator 					= Input::get("indicator");
		//$report->metric 					= Input::get("metric");
		//$report->expected_outcome 			= Input::get("expected_outcome");
		$report->task_group_id				= Input::get("task_group");
		$report->notify_by_email 			= Input::get("by_email");
		//$report->notify_by_sms 				= Input::get("by_sms");
		$report->user_id 					= Auth::user()->id;
		$report->start_date 				= $sdate;
		$report->end_date 					= $edate;
		
		if($report->save())
		{
			$task_id = $report->id;

			if(Input::get('subordinates'))
	    	{
	    		$assign_with = Input::get("assign_with1");
	    	}
	    	else
	    	{
	    		$assign_with = Input::get("assign_with");
	    	}
			
			if(count($assign_with)>0)
			{
				$data = array();
				$notifications = array();

				for($i=0;$i<count($assign_with);$i++)
				{
					//get item
					$items = array(
									"task_id" => $task_id,
									"assigned_to" => $assign_with[$i],
									"assigned_by" => Auth::user()->id,
									"assigned_date" => date('Y-m-d H:i:s')
								);
					$data[] = $items;

					$notification = array(
							"task_id" 			=> $task_id,
							"from" 				=> Auth::user()->id,
							"to" 				=> $assign_with[$i],
							"notification_type" => 1,//means notification type is asigned task
							"created_at" 		=> date('Y-m-d H:i:s')
							);
					$notifications[] = $notification;
				}

				//insert assigned info
				It_task_model::insertBatch('task_assigned_to',$data);
				It_task_model::insertBatch('notifications',$notifications);
			}

			

			return json_encode(array('cond'=>'true','id'=>$task_id));
		}
		else
        {
            return json_encode(array('cond'=>'false','id'=>0));
        }
		
	}
	//create report task
	public function postSubTaskCreate()
	{
			//print_r($_POST);exit;
			$report = new It_task_model;
			
			$report->title 						= Input::get("title");
			$report->description 				= Input::get("description");
			//$report->quantity_of_deliverables 	= Input::get("quantity_of_deliverables");
			//$report->indicator 					= Input::get("indicator");
			//$report->metric 					= Input::get("metric");
			//$report->expected_outcome 			= Input::get("expected_outcome");
			$report->task_group_id				= Input::get("task_group");
			if(Input::get("by_email")){
				$report->notify_by_email 			= Input::get("by_email");
			}
			if(Input::get("by_sms")){
				$report->notify_by_sms 				= Input::get("by_sms");
			}
			$report->user_id 					= Auth::user()->id;
			$report->start_date 				= Input::get("start_date");
			$report->end_date 					= Input::get("end_date");
			$report->parent_task 				= Input::get("parent_task");
			$report->percentage 				= Input::get("percentage");

			if($report->save())
			{
				$task_id = $report->id;

				$assign_with = Input::get("assign_with");
				/*
				if(count($assign_with)>0)
				{
					$data = array();

					for($i=0;$i<count($assign_with);$i++)
					{
						//get item
						$items = array(
										"task_id" => $task_id,
										"assigned_to" => $assign_with[$i],
										"assigned_by" => Auth::user()->id,
										"assigned_date" => date('Y-m-d H:i:s')
									);
						$data[] = $items;
					}

					//insert assigned info
					It_task_model::insertBatch('task_assigned_to',$data);
				}
				*/

				//get item
				$data = array(
								"task_id" => $task_id,
								"assigned_to" => $assign_with,
								"assigned_by" => Auth::user()->id,
								"assigned_date" => date('Y-m-d H:i:s')
							);

				//insert assigned info
				It_task_model::insertBatch('task_assigned_to',$data);

				//redirect to the view page
				return "success";
		}
	}

	// Get the Tasks based on its id for dataTable.
	public function GetReportData($report_id = 0)
	{
		//get all Reports
		$report = It_task_model::getReport($report_id);//print_r($report);exit;
		//print_r($report->get());exit;
		return Datatables::of($report)->make();
	}

	// Get the subtasks based on its id for dataTable.
	public function GetSubtasksData($report_id = 0)
	{
		//get subtasks data from model
		$subtasks = SubTasks::getSubtasks($report_id);
		return Datatables::of($subtasks)->make();
	}

	// load the reports and subtasks view
	public function ViewReport($report_id = 0,$notification_id=0)
	{
		$taskGroup = It_taskGroup::getAll();
		$data['taskGroup'] = $taskGroup;
		$managers = It_task_model::getDepUsers(true);
		$data['managers'] = $managers;
		$users = It_task_model::getDepUsers();
		$data['users'] = $users;
		
		//check if notification id is not zero -----//
		if($notification_id!=0)
		{
			$noti_data = array('status'=>1);
			//update view of notification
			DB::connection('helpdesk')->table('notifications')->where('id',$notification_id)->update($noti_data);
		}
		//-----------------------------------------//
		//get task assigned to
		$assignees = It_task_model::getTaskAssignee($report_id);
		$assign_arr = array();
		foreach($assignees AS $item)
		{
			$assign_arr[] = $item->assigned_to;
		}

		$data['assignees'] = $assign_arr;
		//get task details
		$data['task_item'] = It_task_model::getTaskDetails($report_id);
		//get sum task
		$data['sub_tasks'] = It_task_model::getSubTasks($report_id);
		//get the attachments
		$data['files'] = It_task_model::get_task_files($report_id);
		//$data['assignees'] = It_task_model::getTaskAssignee($report_id);
		//load view for viewing the report and its subtasks in detail.
		return View::make('it_task.tasks.view_workplan',$data)->with('report_id', $report_id);

	}

	// Load the edit_report page with the particular report's data in fields.
	public function EditReport($report_id = 0)
	{

		$taskGroup = It_taskGroup::getAll();
		$users = It_task_model::getDepUsers();
		$managers = It_task_model::getDepUsers(true);
		$data['users'] = $users;
		$data['managers'] = $managers;

		$data['taskGroup'] = $taskGroup;
		//get task assigned to
		$assignees = It_task_model::getTaskAssignee($report_id);
		$assign_arr = array();
		foreach($assignees AS $item)
		{
			$assign_arr[] = $item->assigned_to;
		}

		$data['assignees'] = $assign_arr;
		
		//get task details
		$data['task_details'] = It_task_model::getTaskDetails($report_id);

		return View::make('it_task.tasks.edit_workplan',$data);

	}

	//Update the editted data after form submission and send the session flash data back to the repor_list page.
	public function UpdateReport($report_id = 0)
	{   
	    $validates = \Validator::make(Input::all(),array(
	        "title"							=> "required",
			//"description"					=> "required",
			//"quantity_of_deliverables" 		=> "required",
			//"indicator"						=> "required",
			//"expected_outcome"				=> "required",
			// "department"					=> "required"
	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("ViewReport_it", $report_id)->withErrors($validates)->withInput();
	   }
	    else
	    {
	        $sdate = Input::get("start_date");
			$edate = Input::get("end_date");
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			if($edate != '')
			{
				$edate = explode("-", $edate);
				$ey = $edate[2];
				$em = $edate[1];
				$ed = $edate[0];
				$edate = dateToMiladi($ey,$em,$ed);		
			}
			if(Input::get('subordinates'))
			{
				$sub = 0;
			}
			else
			{
				$sub = 1;
			}
	        $update_report = It_task_model::find($report_id);
	        $update_report->title 					 	= Input::get("title");
		   $update_report->description 			 	= Input::get("description");
		   //$update_report->quantity_of_deliverables 	= Input::get("quantity_of_deliverables");
		   //$update_report->indicator 				 	= Input::get("indicator");
		   //$update_report->metric 					 	= Input::get("metric");
		   $update_report->task_group_id			 	= Input::get("task_group");
		   //$update_report->expected_outcome 		 	= Input::get("expected_outcome");
			$update_report->start_date 					= $sdate;
		   $update_report->end_date 				 	= $edate;
		   $update_report->notify_by_email 		 	= Input::get("by_email");
			//$update_report->notify_by_sms 			 	= Input::get("by_sms");
			$update_report->subordinates					= $sub;
	      if($update_report->save())
	       {
		    	$task_id = $report_id;
	
		    	if(Input::get('subordinates'))
		    	{
		    		$assign_with = Input::get("assign_with");
		    	}
		    	else
		    	{
		    		$assign_with = Input::get("assign_with1");
		    	}
				$new_assignees = array();
				$old_assignees = array();
				$all_assignees = It_task_model::getTaskAssignee($task_id);	
				if($all_assignees)
				{
					foreach($all_assignees AS $assignee)
					{
						$old_assignees[$assignee->assigned_to] = 1;
					}
				}
				
				if(count($assign_with)>0)
				{
					$items = array();
					$notification = array();
					
					for($i=0;$i<count($assign_with);$i++)
					{
						if(!is_assigned_before_it($assign_with[$i],$task_id))
						{//check if this user was assigned to this task before, if yes no need to assign him again
							$task_status = getTaskStatusByUser_it($assign_with[$i],$task_id);
							
							if($task_status)
							{//check if user has rejected the task before
								//update the task_assign_to recored from rejected
								It_task_model::updateRecord('task_assigned_to',array('status'=>0),$task_status->id);
								//add new notification
								$notification = array(
									"task_id" 			=> $task_id,
									"from" 				=> Auth::user()->id,
									"to" 				=> $assign_with[$i],
									"notification_type" => 1,//means notification type is asigned task
									"created_at" 		=> date('Y-m-d H:i:s')
									);
									
							}	
							else
							{//add new records for new assignees
								$items[] = array(
										"task_id" => $task_id,
										"assigned_to" => $assign_with[$i],
										"assigned_by" => Auth::user()->id,
										"assigned_date" => date('Y-m-d H:i:s')
									);
						
								$notification[] = array(
									"task_id" 			=> $task_id,
									"from" 				=> Auth::user()->id,
									"to" 				=> $assign_with[$i],
									"notification_type" => 1,//means notification type is asigned task
									"created_at" 		=> date('Y-m-d H:i:s')
									);
							}
						}
						$new_assignees[$assign_with[$i]] = 1;
						$userEmail = User::find($assign_with[$i])->email;
						$by_email = 0;
						if(Input::get('by_email'))
						{
							$by_email = 1;
						}
						
						if($by_email == 1 && $userEmail != '')
						{
							$user_email = array('email'=>$userEmail,'name'=>$assign_with[$i]);
							if(count($old_assignees)>0)
							{//if there is any saved assignee already
								if(array_key_exists($assign_with[$i], $old_assignees))	
								{//check if the user already exists
									if($old_assignees[$assign_with[$i]] == 0)
									{//check if email was not sent to him, send him email
										if(Mail::send('emails.task_assigned',$user_email, function($message) use ($user_email){
										    //$message->from('p.alihussain@gmail.com', 'Laravel');
											$message->to($user_email['email']);
										}))
										{
											$items['email_notify']=1;
										}
										else
										{
											$items['email_notify'] = 0;
										}		
									}
									else
									{//if email has been sent
										$items['email_notify']=1;
									}
								}
								else
								{//the ueser is new assigned to this task, send him email
									if(Mail::send('emails.task_assigned', $user_email, function($message) use ($user_email){
									    //$message->from('p.alihussain@gmail.com', 'Laravel');
										$message->to($user_email['email']);
									}))
									{
										$items['email_notify']=1;
									}
									else
									{
										$items['email_notify'] = 0;
									}	
								}
								
							}
							else
							{//if no assignee is assigned before, send him email
								if(Mail::send('emails.task_assigned', $user_email, function($message) use ($user_email){
								    //$message->from('p.alihussain@gmail.com', 'Laravel');
									$message->to($user_email['email']);
								}))
								{
									$items['email_notify']=1;
								}
								else
								{
									$items['email_notify'] = 0;
								}
							}
						}
						
					}
					
					//remove the old records
			    	//DB::connection('workplan')->table('task_assigned_to')->where('task_id',$task_id)->delete();
					//DB::connection('workplan')->table('notifications')->where('task_id',$task_id)->delete();			
					//insert assigned info
					if(count($items)>0)
					{
						It_task_model::insertBatch('task_assigned_to',$items);
					}
					if(count($notification)>0)
					{
						It_task_model::insertBatch('notifications',$notification);
					}
					
				}
				else
				{
					//remove the old records
			    	DB::connection('helpdesk')->table('task_assigned_to')->where('task_id',$task_id)->delete();
					DB::connection('helpdesk')->table('notifications')->where('task_id',$task_id)->delete();
				}
				//check if any assignee is removed from this task
				$removed_assignees = array_diff_assoc($old_assignees, $new_assignees);	
				if(count($removed_assignees)>0)
				{		
					foreach($removed_assignees as $x => $x_value)
					{
						$userEmail = User::find($x)->email;	
						if($userEmail != '')
						{
							Mail::send('emails.task_assigned', array('email'=>$userEmail,'name'=>''), function($message) use($userEmail){
						    	//$message->from('p.alihussain@gmail.com', 'Laravel');
								$message->to($userEmail);
							});
						}
					}
				}
				//save the attachments
				$this->uploadDocs($task_id);
				\Session::flash('success','Record Saved Successfully.');
				return \Redirect::route("ViewReport_it",$report_id)->with("success","Task has been successfuly updated.");
			}
			else
			{
				\Session::flash('fail','Not Saved.');
				return \Redirect::route("ViewReport_it",$report_id)->with("fail","An error occured please try again.");
			} 
		}
	}

	// Delete Tasks based on its particular id and the subtasks matching the id.
	public function DeleteReport($id=0)
	{
		if(isTaskCreator_it($id))
		{
			$deleted = It_task_model::deleteTask($id);
	
			if($deleted)
	        {
	        	if(has_subtask_it($id))
	        	{
	        		$subs 	= DB::connection('helpdesk')->table('tasks')->where('parent_task',$id)->get();
	        		foreach($subs AS $sub)
	        		{
	        			It_task_model::deleteTask($sub->id);
	        		}
	        	}
	        	It_task_model::insertRecord('task_logs',array('task_id'=>$id,'deleted_by'=>Auth::user()->id,'deleted_at'=>date('Y-m-d H:i:s')));
	            return \Redirect::route("getReport_it")->with("success",_('you_have_successfully_deleted_the_task_and_its_subtasks'));
	        }
	        else
	        {
	            return \Redirect::route("getReport_it")->with("fail",_('an_error_occured_please_try_again'));
	        }
		}
		else
		{
			return \Redirect::route("getReport_it")->with("fail",_('you_can_not_delete_this_task'));
		}
		

	}
	public function DeleteSubtask($id=0,$parent=0)
	{
		$deleted = It_task_model::deleteTask($id);
	
		if($deleted)
        {
            return \Redirect::route("ViewReport_it",$parent)->with("success","You have successfully deleted the subtasks, ID: <span style='color:red;font_weight:bold;'>{{$id}}</span>");
        }
        else
        {
            return \Redirect::route("ViewReport_it",$parent)->with("fail","An error occured please try again.");
        }
	}
	//load modal for subtask edit
	public function loadEditModal($sub=0)
	{

		$sub = Input::get('id');

		$taskGroup = It_taskGroup::getAll();
		$users = It_task_model::getDepUsers();
		$data['users'] = $users;
		$data['taskGroup'] = $taskGroup;
		//get task assigned to
		$assignees = It_task_model::getTaskAssignee($sub);
		$assign_arr = array();
		foreach($assignees AS $item)
		{
			$assign_arr[] = $item->assigned_to;
		}

		$data['assignees'] = $assign_arr;

		//get task details
		$data['task_details'] = It_task_model::getTaskDetails($sub);

		return View::make('it_task.tasks.edit_sub',$data);


	}
	//load modal for edit task progress
	public function loadModal()
	{
		$data['task_id'] = Input::get('id');
		if(Input::get('main')==1)
		{
			$data['progress'] = getMainTaskProgress_it(Input::get('id'));
		}
		else
		{
			$data['progress'] = getTaskProgress_it(Input::get('id'));
		}

		return View::make('it_task.tasks.edit_progress',$data);
	}
	public function loadApprovalModal()
	{
		$data['task_id'] = Input::get('id');
		
		//$data['progress'] = getTaskProgress(Input::get('id'));
		
		return View::make('it_task.tasks.task_approval',$data);
	}
	//save task approval
	public function postSaveApproval()
	{

		//get task id
		$task_id = Input::get('task_id');

		//comment details
		$data = array(

				"task_id" 			=> Input::get("task_id"),
				"comment" 			=> Input::get("reason"),
				"type" 				=> 1,
				"user_id" 			=> Auth::user()->id,
				"created_at" 		=> date('Y-m-d H:i:s')

			);

		

		DB::connection("helpdesk")->table("task_comments")->insert($data);
		
		//--- get users who included to this task -------------//
		
		$allUsers = getIncludedUsers_it($task_id);
		//notification details
		$notiData = array();
		for($i=0;$i<count($allUsers);$i++)
		{
			$data = array(
				"task_id" 			=> Input::get("task_id"),
				"from" 				=> Auth::user()->id,
				"to" 				=> $allUsers[$i],
				"notification_type" => 2,//means notification type is reject task
				"created_at" 		=> date('Y-m-d H:i:s')
				);
			//push row in array
			array_push($notiData, $data);
		}

		DB::connection("helpdesk")->table("notifications")->insert($notiData);

		//--- get users who included to this task -------------//
		
		// $update = array("status"=>2);
		// DB::connection("workplan")
		// 			->table("task_assigned_to")
		// 			->where("task_id",Input::get("task_id"))
		// 			->where("assigned_to",Auth::user()->id)
		// 			->update($update);
		return "success";
	}
	//save task comments
	public function postComment()
	{
		$data = array(
				"task_id" 			=> Input::get("task_id"),
				"comment" 			=> Input::get("comment"),
				"type" 				=> 2,
				"user_id" 			=> Auth::user()->id,
				"created_at" 		=> date('Y-m-d H:i:s')

			);
		$comment_id = DB::connection("helpdesk")->table("task_comments")->insertGetId($data);
		
		//--- get users who included to this task -------------//
		
		$allUsers = getIncludedUsers_it(Input::get("task_id"));
		//notification details
		$notiData = array();
		for($i=0;$i<count($allUsers);$i++)
		{
			$data = array(
				"task_id" 			=> Input::get("task_id"),
				"from" 				=> Auth::user()->id,
				"to" 				=> $allUsers[$i],
				"notification_type" => 4,//means notification type is commented on task
				"created_at" 		=> date('Y-m-d H:i:s')
				);
			//push row in array
			array_push($notiData, $data);
		}

		DB::connection("helpdesk")->table("notifications")->insert($notiData);

		//--- get users who included to this task -------------//
		
		//get last inserted comment
		$commentObject = DB::connection('helpdesk')->table('task_comments')->where('id',$comment_id)->get();
		
		$comments = '<div class="conversation-item item-left clearfix">';
		//foreach comment for showing on view
		foreach($commentObject AS $item)
		{
			$comments .= '<div class="conversation-user">';
			$photo 	   = getProfilePicture($item->user_id);
			$comments .= \HTML::image('/img/'.$photo,'',array('class' => 'project-img-owner comment'));
			$comments .= '</div>';
			$comments .= '<div class="conversation-body">';
			$comments .= '<div class="name">';
			$comments .= getUserFullName_it($item->user_id);
			$comments .= '</div>';
			$comments .= '<div class="time hidden-xs">';
			$comments .= $item->created_at;
			$comments .= '</div>';
			$comments .= '<div class="text">';
			$comments .= $item->comment;
			$comments .= '</div>';
			$comments .= '</div>';
		}

		return $comments;
	}
	//save task approval
	public function approveTask()
	{

		//--- get users who included to this task -------------//
		
		$allUsers 	= DB::connection('helpdesk')->table('task_assigned_to')->where('task_id',Input::get('task_id'))->get();
		
		//notification details
		$notiData = array();
		foreach($allUsers AS $item)
		{
			$data = array(
				"task_id" 			=> Input::get("task_id"),
				"from" 				=> Auth::user()->id,
				"to" 				=> $item->assigned_by,
				"notification_type" => 3,//means notification type is task approved
				"created_at" 		=> date('Y-m-d H:i:s')
				);
			//push row in array
			array_push($notiData, $data);
		}

		DB::connection("helpdesk")->table("notifications")->insert($notiData);

		//--- get users who included to this task -------------//
		
		$update = array("status"=>1);
		DB::connection("helpdesk")
					->table("task_assigned_to")
					->where("task_id",Input::get("task_id"))
					->where("assigned_to",Auth::user()->id)
					->update($update);

		$update = array("status"=>1);
		DB::connection("helpdesk")
					->table("notifications")
					->where("task_id",Input::get("task_id"))
					->where("to",Auth::user()->id)
					->update($update);

		return "success";
	}
	public function rejectTask()
	{
		$edate = Input::get("end_date");
		if($edate != '')
		{
			$edate = explode("-", $edate);
			$sy = $edate[2];
			$sm = $edate[1];
			$sd = $edate[0];
			$endDate = dateToMiladi($sy,$sm,$sd);	
			$edate = strtotime($endDate);	
		}
		$reject_new_date = null;
		$reject_comment = Input::get('comment');
		if(date('Y-m-d',$edate) != Input::get('enddate'))
		{//if user changed the date
			$reject_new_date = $endDate;
			$reject_comment = Input::get('comment').'[New Date: '.Input::get('end_date').']';
		}
		
		$allUsers 	= DB::connection('helpdesk')->table('tasks')->where('id',Input::get('task_id'))->first();
		
			$data = array(
				"task_id" 			=> Input::get("task_id"),
				"from" 				=> Auth::user()->id,
				"to" 				=> $allUsers->user_id,
				"notification_type" => 2,//means notification type is task reject
				"created_at" 		=> date('Y-m-d H:i:s')
				);
			
		DB::connection("helpdesk")->table("notifications")->insert($data);

		$update = array("status"=>2,'reject_new_date'=>$reject_new_date);//task is rejected
		DB::connection("helpdesk")
					->table("task_assigned_to")
					->where("task_id",Input::get("task_id"))
					->where("assigned_to",Auth::user()->id)
					->update($update);

		DB::connection("helpdesk")
					->table("notifications")
					->where("task_id",Input::get("task_id"))
					->where("to",Auth::user()->id)
					->update(array("status"=>1));
					
		$reject_reason = array('task_id' => Input::get('task_id'),
								'user_id' => Auth::user()->id,
								'comment' => $reject_comment,
								'status' => 0,
								'type' => 1,
								'created_at' => date('Y-m-d H:i:s')
								);
		DB::connection("helpdesk")->table("task_comments")->insert($reject_reason);
		return "success";
	}
	public function approveCompletedTask()
	{
		$task_id = Input::get('task_id');
/*
		$subTasks = DB::select("
								SELECT t1.title,t1.id AS task_id, IFNULL( t2.progress, 0 ) AS progress
								FROM workplan.tasks AS t1
								LEFT JOIN workplan.task_progress AS t2 ON t2.task_id = t1.id
								LEFT JOIN workplan.task_assigned_to AS t3 ON t3.assigned_to = t2.user_id
								WHERE t3.assigned_to = t2.user_id
								AND t2.status = 0
								AND t1.parent_task = ".$task_id
							);

*/		
		//print_r($subTasks);exit;
		if(has_subtask_it($task_id))
		{
/*
			$ul = '<ul class="graph-stats">';

			//foreach uncompleted tasks
			foreach($subTasks AS $item):

				$page = route('ViewReport',$item->task_id);
				
				$ul .= '
						<a href="'.$page.'" target="_blank">
						<li>
							<div class="clearfix">
								<div class="title pull-left">
									'.$item->title.'
								</div>
								<div class="value pull-right" title="" data-toggle="tooltip" data-original-title="10% down">
									'.$item->progress.'% 
								</div>
							</div>
							<div class="progress">
								<div style="width: '.$item->progress.'%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="'.$item->progress.'" role="progressbar" class="progress-bar">
									<span class="sr-only">'.$item->progress.'% Complete</span>
								</div>
							</div>
						</li>
						</a>
						';
			endforeach;

			$ul .= '</ul>';

			$data = array('cond'=>'true','data'=>$ul);
			return json_encode($data);

			foreach($subTasks AS $item)
			{
				$update = array("status"=>1);
				DB::connection("workplan")
							->table("task_progress")
							->where("task_id",$item->task_id)
							->update($update);
			}
			$task_creator 	= DB::connection('workplan')->table('tasks')->where('id',$task_id)->pluck('user_id');

			$data = array(
							"task_id" 			=> $task_id,
							"from" 				=> Auth::user()->id,
							"to" 				=> $task_creator,
							"notification_type" => 5,//means notification type is task completed
							"created_at" 		=> date('Y-m-d H:i:s')
						);

			It_task_model::insertBatch('notifications',$data);
*/
			if(isMainTaskCompleted_withSubtasks_it(Input::get("task_id")))
			{
				$update = array("percentage"=>100);
				DB::connection("helpdesk")
							->table("tasks")
							->where("id",Input::get("task_id"))
							->update($update);
			
				$element = '<div class="alert alert-success">
	                                    <i class="fa fa-check-circle fa-fw fa-lg"></i>
	                                    <strong>Well done!</strong> Task Approved
	                                </div>';
			}
			else
			{
				$element = '<div class="alert alert-success">
	                                    <i class="fa fa-check-circle fa-fw fa-lg"></i>
	                                    <strong>The task is not completed yet</strong>
	                                </div>';
			}
	        $data = array('cond'=>'false','data'=>$element);
			return json_encode($data);
		}
		else
		{
			$update = array("status"=>1);
			DB::connection("helpdesk")
						->table("task_progress")
						->where("task_id",Input::get("task_id"))
						->update($update);
			DB::connection("helpdesk")
						->table("tasks")
						->where("id",Input::get("task_id"))
						->update(array("completed"=>1));

			$update = array("status"=>1);
			DB::connection("helpdesk")
						->table("notifications")
						->where("task_id",Input::get("task_id"))
						->where("to",Auth::user()->id)
						->where("notification_type",5)
						->update($update);

			$element = '<div class="alert alert-success">
	                                    <i class="fa fa-check-circle fa-fw fa-lg"></i>
	                                    <strong>Well done!</strong> Task Completed
	                                </div>';
	        $data = array('cond'=>'false','data'=>$element);
			return json_encode($data);
	    }
	}
	//save progress
	public function saveProgress()
	{
		if(Input::get("percentage") == 0 || Input::get("progress_summary") == '')
		{
			return \Redirect::route("ViewReport_it",Input::get("task_id"))->with("fail","An error occured please try again.");
		}
		$task_creator 	= DB::connection('helpdesk')->table('tasks')->where('id',Input::get('task_id'))->pluck('user_id');
		if(Input::get("percentage") == 100 && Auth::user()->id == $task_creator)
		{// i am the creator of task, no need for notificatoin
			$data = array(

				"task_id" 			=> Input::get("task_id"),
				"progress" 			=> Input::get("percentage"),
				"progress_summary" 	=> Input::get("progress_summary"),
				"status" 			=> 1,//since i am the doer and creator of task, it would be approved directly
				"user_id" 			=> Auth::user()->id,
				"created_at" 		=> date('Y-m-d H:i:s')

			);
		}
		else
		{
			$data = array(

				"task_id" 			=> Input::get("task_id"),
				"progress" 			=> Input::get("percentage"),
				"progress_summary" 	=> Input::get("progress_summary"),
				"user_id" 			=> Auth::user()->id,
				"created_at" 		=> date('Y-m-d H:i:s')

			);
		}
		DB::connection("helpdesk")->table("task_progress")->insert($data);
		$report = It_task_model::find(Input::get('task_id'));
		$report->percentage = Input::get("percentage");
		$report->save();
		
		//check for notifications
		if(Input::get('percentage') == 100)
		{
			if($report->parent_task == 0 && Auth::user()->id != $task_creator)
			{//add notification for completed main tasks without sub if doer is not ther creator
				if(Auth::user()->id != $task_creator)
				{
					$data = array(
							"task_id" 			=> Input::get('task_id'),
							"from" 				=> Auth::user()->id,
							"to" 				=> $task_creator,
							"notification_type" => 5,//means notification type is task completed
							"created_at" 		=> date('Y-m-d H:i:s')
						);
					It_task_model::insertRecord('notifications',$data);
				}
			}
			if($report->parent_task != 0)
			{
				//$task_creator 	= DB::connection('workplan')->table('tasks')->where('id',$report->parent_task)->pluck('user_id');
				$subtask_creator 	= DB::connection('helpdesk')->table('tasks')->where('id',Input::get('task_id'))->pluck('user_id');
				if(isMainTaskCompleted_withSubtasks_it($report->parent_task))
				{//since all sub tasks are completed then send notification
					$data = array(
							"task_id" 			=> $report->parent_task,
							"from" 				=> Auth::user()->id,
							"to" 				=> $task_creator,
							"notification_type" => 5,//means notification type is task completed
							"created_at" 		=> date('Y-m-d H:i:s')
						);
					It_task_model::insertRecord('notifications',$data);
				}
				
				if(Auth::user()->id != $subtask_creator)
				{//send notification the subtask itself is completed AND the doer is not task creator
					$data = array(
								"task_id" 			=> Input::get('task_id'),
								"from" 				=> Auth::user()->id,
								"to" 				=> $task_creator,
								"notification_type" => 5,//means notification type is task completed
								"created_at" 		=> date('Y-m-d H:i:s')
							);
	
					It_task_model::insertRecord('notifications',$data);
				}
				
			}
		}
		$comments = '<div class="conversation-item item-left clearfix">';
		$comments .= '<div class="conversation-user">';
		$photo 	   = getProfilePicture(Auth::user()->id);
		$comments .= \HTML::image('/img/'.$photo,'',array('class' => 'project-img-owner comment'));
		$comments .= '</div>';
		$comments .= '<div class="conversation-body">';
		$comments .= '<div class="name">';
		$comments .= getUserFullName_it(Auth::user()->id);
		$comments .= '</div>';
		$comments .= '<div class="time hidden-xs">';
		$comments .= date('Y-m-d H:i:s');
		$comments .= '</div>';
		$comments .= '<div class="text">';
		$comments .= Input::get("progress_summary");
		$comments .= '<br><br>';
		$progress = Input::get('percentage');
		$comments .= '<div class="progress" title="'.$progress.'% Completed">';
		$comments .= '<div class="progress-bar" role="progressbar" aria-valuenow="'.$progress.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$progress.'%;">';
		$comments .= '<span class="sr-only">'.$progress.'% Complete</span>';
		$comments .= '</div></div></div></div>';
		
		//\Session::flash('success', 'The Record Saved Successfully.');
		//return \Redirect::route('getRep')->with("success","The Record Saved Successfully.");
		return $comments;
	}
	//create report task
	public function postSubTaskUpdate()
	{
			$report = It_task_model::find(Input::get('task_id'));
			
			$report->title 						= Input::get("title");
			$report->description 				= Input::get("description");
			//$report->quantity_of_deliverables 	= Input::get("quantity_of_deliverables");
			//$report->indicator 					= Input::get("indicator");
			//$report->metric 					= Input::get("metric");
			//$report->expected_outcome 			= Input::get("expected_outcome");
			$report->task_group_id				= Input::get("task_group");
			if(Input::get("by_email")){
				$report->notify_by_email 			= Input::get("by_email");
			}
			if(Input::get("by_sms")){
				$report->notify_by_sms 				= Input::get("by_sms");
			}
			$report->user_id 					= Auth::user()->id;
			$report->start_date 				= Input::get("start_date");
			$report->end_date 					= Input::get("end_date");
			$report->parent_task 				= Input::get("parent_task");
			$report->percentage 				= Input::get("percentage");

			if($report->save())
			{

				$task_id = Input::get('task_id');
				DB::connection('helpdesk')->table('task_assigned_to')->where('task_id', $task_id)->delete();

				$assign_with = Input::get("assign_with");
				if(count($assign_with)>0)
				{
					$data = array();

					for($i=0;$i<count($assign_with);$i++)
					{
						//get item
						$items = array(
										"task_id" => $task_id,
										"assigned_to" => $assign_with[$i],
										"assigned_by" => Auth::user()->id,
										"assigned_date" => date('Y-m-d H:i:s')
									);
						$data[] = $items;
					}

					//insert assigned info
					It_task_model::insertBatch('task_assigned_to',$data);
				}

				return "success";
		
		}
	}
	//get report list - my
	public function get_myTasks($week = 0,$year=0)
	{
		if($week == 0)
		{
			$week = date('W');
		}
		if($year == 0)
		{
			$year = date('Y');
		}
		$data['week_no'] = 0;//for tooltip
		$data['week'] = $week;
		$data['year'] = $year;
		//get all reports from database
		//$data['reports'] = It_task_model::getReport();
		//get tasks assigned to me
		//$data['assigned_to_me'] = It_task_model::getTasksAssignedToMe();
		$data['mode'] = 'my';
		return View::make('it_task.tasks.create_workplan',$data);		
		
	}
	//get report list - outgoing
	public function get_outTasks($week = 0,$year=0)
	{
		if($week == 0)
		{
			$week = date('W');
		}
		if($year == 0)
		{
			$year = date('Y');
		}
		$data['week_no'] = 0;//for tooltip
		$data['week'] = $week;
		$data['year'] = $year;
		
		$data['mode'] = 'out';
		return View::make('it_task.tasks.create_workplan',$data);		
		
	}
	public function load_task_detail(Request $request)
	{
		//get task id
		$report_id = $request['id'];
		$data['progress'] = getMainTaskProgress_it($report_id);
		$taskGroup = It_taskGroup::getAll_myGroups(Auth::user()->id);
		$users = It_task_model::getDepUsers();
		//$managers = It_task_model::getDepUsers(true);
		$data['users'] = $users;
		//$data['managers'] = $managers;
		
		$data['taskGroup'] = $taskGroup;
		//get task assigned to
		$assignees = It_task_model::getTaskAssignee($report_id);
		$assign_arr = array();
		foreach($assignees AS $item)
		{
			$assign_arr[] = $item->assigned_to;
		}

		$data['assignees'] = $assign_arr;
		//get task details
		$data['details'] = It_task_model::getTaskDetails($report_id);
		$data['files'] = It_task_model::get_task_files($report_id);
		//get sub tasks 
		$data['sub_tasks']	= It_task_model::getSubTasks($report_id);
		$data['last_progress']	= It_task_model::getLastProgress($report_id);

		//return View::make('task.tasks.edit_workplan',$data);
		//details for main task or sub tasks from task details
		$data['type'] = $request['type'];
		return View('it_task.tasks.task_details',$data);
	}
	public function load_task_detail1(Request $request)
	{
		//get task id
		$report_id = $request['id'];
		$data['progress'] = getMainTaskProgress_it($report_id);
		$taskGroup = It_taskGroup::getAll();
		$users = It_task_model::getDepUsers();
		$managers = It_task_model::getDepUsers(true);
		$data['users'] = $users;
		$data['managers'] = $managers;
		
		$data['taskGroup'] = $taskGroup;
		//get task assigned to
		$assignees = It_task_model::getTaskAssignee($report_id);
		$assign_arr = array();
		foreach($assignees AS $item)
		{
			$assign_arr[] = $item->assigned_to;
		}

		$data['assignees'] = $assign_arr;
		//get task details
		$data['details'] = It_task_model::getTaskDetails($report_id);
		$data['files'] = It_task_model::get_task_files($report_id);
		//get sub tasks 
		$data['sub_tasks']	= It_task_model::getSubTasks($report_id);

		//return View::make('task.tasks.edit_workplan',$data);
		//details for main task or sub tasks from task details
		$data['type'] = $request['type'];
		return View('it_task.tasks.task_details1',$data);
	}
	public function load_task_assignee(Request $request)
	{
		//get task id
		$report_id = $request['id'];
		$data['type'] = 'task_detail';
		if(isset($request['type']))
		{
			$data['type'] = $request['type'];
		}
		$taskGroup = It_taskGroup::getAll();
		$users = It_task_model::getDepUsers();
		$managers = It_task_model::getDepUsers(true);
		$data['users'] = $users;
		$data['managers'] = $managers;

		$data['taskGroup'] = $taskGroup;
		//get task assigned to
		$assignees = It_task_model::getTaskAssignee($report_id);
		$assign_arr = array();
		foreach($assignees AS $item)
		{
			$assign_arr[] = $item->assigned_to;
		}

		$data['assignees'] = $assign_arr;
		
		//get task details
		$data['details'] = It_task_model::getTaskDetails($report_id);
		
		//get sub tasks 
		$data['sub_tasks']	= It_task_model::getSubTasks($report_id);
		
		//return View::make('task.tasks.edit_workplan',$data);
		
		return View('it_task.tasks.task_assignee',$data);
	}
	public function load_task_date(Request $request)
	{
		//get task id
		$report_id = $request['id'];
		$type = $request['type'];
		
		$details = It_task_model::getTaskDetails($report_id);
		$s_date = $details->start_date;
		$e_date = $details->end_date;
		if($s_date != '0000-00-00')
		{
			$sdate = explode("-", $s_date);
			$sy = $sdate[0];
			$sm = $sdate[1];
			$sd = $sdate[2];
			$s_date = dateToShamsi($sy,$sm,$sd);		
		}
		if($e_date != '0000-00-00')
		{
			$edate = explode("-", $e_date);
			$ey = $edate[0];
			$em = $edate[1];
			$ed = $edate[2];
			$e_date = dateToShamsi($ey,$em,$ed);		
		}
		$data['s_date'] = $s_date;
		$data['e_date'] = $e_date;
		$data['report_id'] = $report_id;
		$data['parent_id'] = $details->parent_task;
		$data['type'] = $type;
		return View('it_task.tasks.task_date',$data);
	}
	public function paginateTasks()
	{
		//return the result of helper function
		return getTaskGroup_it(Input::get('task_group'),Input::get('page'));
	}
	public function getAssignee(Request $request)
	{
		//get task id
		$report_id = $request['id'];

		//get task assigned to
		$assignees = It_task_model::getTaskAssignee($report_id);
		
		$data['assignees'] = $assignees;
		$data['id'] = $report_id;
		echo View('it_task.tasks.assignee_tooltip',$data);
	}
	public function getWeeklyReport()
	{
		if(canView('it_task_report'))
		{
			$data['deps'] = getAllDepartments();
			$data['dep_id'] = 0;
			$data['excel'] = false;
			return View::make('it_task.task_group.weekly_report',$data);		
		}
		else
		{
			return showWarning();
		}
	}
	public function generateWeeklyReport(Request $request)
	{
		//validate the input fields
		$this->validate($request,[
		        "start_date" => "required",
		        "end_date" => 'required'
		        //"dep" => 'required'		        
		        ]
		    );
		//validation true
		$data['result'] = It_task_model::get_weekly_report($request);
		$data['deps'] = getAllDepartments();
		$data['sdate'] = $request['start_date'];
		$data['edate'] = $request['end_date'];
		$data['dep_id'] = $request['dep'];
		$data['excel'] = true;
		return View::make('it_task.task_group.weekly_report',$data);
	}
	//upload document
	public function uploadDocs($doc_id=0)
	{
		$files = Input::file('files');
		$errors = "";
		$file_data = array();
		foreach($files as $file) 
		{
			if(Input::hasFile('files'))
			{
			  // validating each file.
			  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			  $validator = \Validator::make(

			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'file' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,png,doc,docx,zip,pdf,xlsx,xls,txt'
			        ]
			  	);

			  if($validator->passes())
			  {	
			  	// path is root/uploads
			    $destinationPath = 'documents/tasks';
			    $name = $file->getClientOriginalName();

			    $temp = explode(".", $name);
			    $extension = end($temp);
			    //$lastFileId = getLastDocId();
			    
			    //$lastFileId++;
			    
			    $filename = $temp[0].'-'.$doc_id.'-'.time().'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
				   
				   $data = array(
				   					'task_id'=>$doc_id,
				   					'file_name'=>$filename
				   				);
				   if(count($data)>0)
					{
						It_task_model::insertRecord('attachments',$data);
					}
				   //return Response::json('success', 200);
				   
				} 
				else 
				{
				   $errors .= json('error', 400);
				}

		    
			  } 
			  else 
			  {
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			  }
			}
		}	
	}
	//download file from server
	public function downloadDoc($file_id=0)
	{	
		//get file name from database
		//$fileName = getDocscomFileName($file_id);
        //public path for file
        $file= public_path(). "/documents/tasks/".$file_id;
        //download file
        return Response::download($file, $file_id);
	    
	}
	//remove file from folder
	public function deleteFile(Request $request)
	{
		$fileName = $request['name'];
		$id = $request['doc_id'];
		//get file name from database
		//$fileName = getDocscomFileName($file_id);
		$file= public_path(). "/documents/tasks/".$fileName;
		if(\File::delete($file))
		{
			//delete from database
			
			It_task_model::delete_record('attachments',array('id'=>$id));
			return "<div class='alert alert-success'>File Deleted Successfully!</div>";
		}
		else
		{
			return "<div class='alert alert-danger'>Error!</div>";
		}	
	}
	
	public function weeklyReport_excel($sdate,$edate)
	{
		$request = array('start_date'=>$sdate,'end_date'=>$edate);
		
		//get data from model
		$results = It_task_model::get_weekly_report($request);
		$head = 'Report From '.$request['start_date'].' to '.$request['end_date'];
		
		//Excel::load('excel_template/received.xlsx', function($file) use($results){
		Excel::create('HelpDesk-Daily-Report', function($file) use($results,$head,$request){
			
			$file->setTitle('HelpDesk-Daily-Report');
		
			$file->sheet('HelpDesk-Daily-Report', function($sheet) use($results,$head,$request){
			//$sheet = $file -> getActiveSheetIndex(1);	
				$sheet->setWidth(array(
				    'A'     => 3,
				    'B'     => 30,
				    'C'		=> 20,
				    'D'		=> 20,
				    'E'		=> 50,
				    'F'		=> 50,
				    'G'		=> 20
				));
				$sheet->mergeCells('A1:G1');
				$sheet->row(1, function($row) {
				    // call cell manipulation methods
				    //$row->setBackground('#f9ec36');
				    $row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$sheet->setCellValue('A1','راپور اجـــــراآت روزانــــه کارمنــدان خدمات تکنالــوژی معلوماتـــی( داخـل ارگ)');
				$sheet->mergeCells('A2:G2');
				$sheet->row(2, function($row) {
				    // call cell manipulation methods
				    //$row->setBackground('#f9ec36');
				    $row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$head_2 = 'الیوم تاریخ: '.$request['start_date'];
				$sheet->setCellValue('A2',$head_2);
				$sheet->mergeCells('A4:G4');
				$sheet->row(4, function($row) {
				    // call cell manipulation methods
				    //$row->setBackground('#f9ec36');
				    //$row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$sheet->setCellValue('A4',$head);
				$sheet->setCellValue('A5','NO');
				$sheet->setCellValue('B5','Title');
				$sheet->setCellValue('C5','Start time');
				$sheet->setCellValue('D5','Assign to');
				$sheet->setCellValue('E5','Summary');
				$sheet->setCellValue('F5','Progress');
				$sheet->setCellValue('G5','End time');
				$row = 6;$main_no = 1;
				foreach($results AS $item)
				{
					$sheet->setCellValue('A'.$row.'',$main_no);
					$sheet->setCellValue('B'.$row.'',$item->title);
					$sheet->setCellValue('C'.$row.'',$item->created_at);
					$assignees = getTaskAssignees_it($item->id);
					$assign = '';
					if($assignees)
					{
						foreach($assignees AS $assignee)
						{
							$assign.= getUserFullName_it($assignee->assigned_to).',';
						}
					}
					$sheet->setCellValue('D'.$row.'',$assign);
					
					$desc = '';
					$progress = getTaskAllProgress_it($item->id);
					$end_time='';
					if($progress)
					{
						foreach($progress AS $prog)
						{
							$desc.= $prog->progress_summary.',';
							if($prog->progress==100)
							{
								$end_time = $prog->created_at;
							}
						}
					}
					
					$sheet->setCellValue('E'.$row.'',$item->description);
					$sheet->setCellValue('F'.$row.'',$desc);
					$sheet->setCellValue('G'.$row.'',$end_time);
					
					$main_no++;$row++;
				}
				$border = $row-1;
				$footer = $row+3;
				$footer_value = $row+4;
				// Set border for range
				$sheet->setBorder('A4:G'.$border.'', 'thin');
				$sheet->setCellValue('C'.$footer.'','ملاحضه کننده');
				$sheet->setCellValue('C'.$footer_value.'','رئیس تکنالوژی معلوماتی');
				$sheet->setCellValue('E'.$footer.'','تصدیق کننده');
				$sheet->setCellValue('E'.$footer_value.'','آمر شبکه تکنالوژی');
				$sheet->setCellValue('F'.$footer.'','ترتیب کننده');
				$sheet->setCellValue('F'.$footer_value.'','محمد آصف عظیمی');

    		});
		
		})->export('xls');
	}
	//------------------------------------------------------- groups ------------------------------------------------

	//get task group list
	public function getTaskGroups()
	{
		return View::make("it_task.task_group.list");
	}

	// Get the Tasks based on its id for dataTable.
	public function getTaskGroupData()
	{
		//get all data 
		$object = It_taskGroup::getData();
		$collection = new Collection($object);
		return Datatable::collection($collection)
							->showColumns('id','title','description','creator')
							->addColumn('operation',function($option){
								$options = '';
								if($option->user_id == Auth::user()->id OR isAdmin()){
									$options .= '<a href="'.route('getTaskGroupUpdate_it',$option->id).'"><i class="fa fa-edit"></i> Edit</a> &nbsp;';
									if(checkGroup_task_it($option->id))
									{
										$options .= '|&nbsp;<a href="'.route('getDeleteTaskGroup_it',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="fa fa-trash-o"></i> Delete</a>';
									}	
									else
									{
										$options .= '|&nbsp;<a href="javascript:void()" onclick="alert(\'You can not delete this task since it has some tasks!\')" style="color:red"><i class="fa fa-trash-o"></i> Delete</a>';
									}			
								}
								return $options;
		
							})
							->make();
	}

	//get create form for task group
	public function getTaskGroupCreate()
	{
		//get departments
		$data['deps'] = DepartmentX::getAll();
		//get users
		//$data['users'] = User::getAll();

		$users = It_task_model::getDepUsers();
		$managers = It_task_model::getDepUsers(true);
		$data['users'] = $users;

		$data['managers'] = $managers;

		return View::make('it_task.task_group.create',$data);
	}

	//get edit form for task group
	public function getTaskGroupUpdate($id=0)
	{
		if(isGroupCreator_it($id))
		{
			//get departments
			$data['deps'] = DepartmentX::getAll();
			//get users
			$data['users'] = User::getAll();
			//get task group details
			$data['taskGroupDetails'] = It_taskGroup::getTaskGroupDetails($id);
			
			//get task group shared department ---------------------------------------//
			$SharedDeps = It_taskGroup::getTaskGroupShares('group_shares_department',$id);
			$SharedDepsArr = array();
			foreach($SharedDeps AS $item)
			{
				//push item to array
				array_push($SharedDepsArr, $item->department_id);
			}
			$data['shared_deps'] = $SharedDepsArr;
			//------------------------------------------------------------------------//
	
			//get task group shared users --------------------------------------------//
			$SharedUsers = It_taskGroup::getTaskGroupShares('group_shares_user',$id);
			$SharedUsersArr = array();
			foreach($SharedUsers AS $item)
			{
				//push item to array
				array_push($SharedUsersArr, $item->user_id);
			}
			$data['shared_users'] = $SharedUsersArr;
			//------------------------------------------------------------------------//
	
			return View::make('it_task.task_group.edit',$data);
		}
		else
		{
			return showWarning();
		}
	}
	
	//create new record task group
	public function postTaskGroupCreate()
	{
		//validate fields
		$validates = \Validator::make(Input::all(), array(

			"title"							=> "required",
			//"description"					=> "required"
			
		));

		//check the validation
		if($validates->fails())
		{	
			return \Redirect::route('getTaskGroupCreate_it')->withErrors($validates)->withInput();
		}
		else
		{		
			$object = new It_taskGroup;
			
			$object->title = Input::get("title");
			$object->description = Input::get("description");
			$object->access_level = Input::get("access");
			/*
			if(isManager())
			{
				$object->access_level = Input::get("access");
			}
			else
			{
				$object->access_level = 2;//personal
			}
			*/
			$object->user_id = Auth::user()->id;
			$object->dep_id = Auth::user()->department_id;
			if(Input::get('in_report'))
			{
				$object->in_report = Input::get('in_report');
			}
			
			if($object->save())
			{
				/*
				$record_id = $object->id;
				//check if shared with some one or share with some departments
	        	$shared = Input::get('share_with');
	        	if(count($shared)>0)
	        	{
	        		$users = array();
	        		$deps = array();
	        		
	        		for($i=0;$i<count($shared);$i++)
	        		{
	        			$shared_with = $shared[$i];
	        			//explode shared
	        			$shared_with = explode("_", $shared_with);
	        			//check if shared with user
	        			if($shared_with[0] == 'u')
	        			{
	        				$data = array(
	        						'user_id' => $shared_with[1],
	        						'task_group_id' => $record_id
	        					);
	        				//push to user array
	        				array_push($users, $data);
	        			}//check if shared with department
	        			else if($shared_with[0] == 'd')
	        			{
	        				$data = array(
	        						'department_id' => $shared_with[1],
	        						'task_group_id' => $record_id
	        					);
	        				//push to department array
	        				array_push($deps, $data);
	        			}
	        		}

	        		//then, insert document shared
	        		if(count($users)>0)
	        		{
	        			It_taskGroup::insertBatch('group_shares_user',$users);
	        		}
	        		if(count($deps)>0)
	        		{
	        			It_taskGroup::insertBatch('group_shares_department',$deps);
	        		}
	        	}
				*/
				return \Redirect::route('getTaskGroups_it')->with("success","The Record Saved Successfully.");

			}
			else
	        {
	            return \Redirect::route("getTaskGroups_it")->with("fail","An error occured plase try again.");
	        }
		}
	}

	//Update Task group details
	public function postTaskGroupUpdate($id=0)
	{
		if(isGroupCreator_it($id))
		{
			//validate fields
			$validates = \Validator::make(Input::all(), array(
	
				"title"							=> "required",
				//"description"					=> "required"
				
			));
	
			//check the validation
			if($validates->fails())
			{	
				return \Redirect::route('getTaskGroupUpdate_it',$id)->withErrors($validates)->withInput();
			}
			else
			{
				
				$object = It_taskGroup::find($id);
				
				$object->title = Input::get("title");
				$object->description = Input::get("description");
				if(isManager())
				{
					$object->access_level = Input::get("access");
				}
				else
				{
					$object->access_level = 2;//personal
				}
				
				if(Input::get('in_report'))
				{
					$object->in_report = Input::get('in_report');
				}
				
				if($object->save())
				{
					/*
					$record_id = $id;
					//remove the old related task group
					It_taskGroup::removeTaskGroupRelated('group_shares_department',$record_id);
					It_taskGroup::removeTaskGroupRelated('group_shares_user',$record_id);
	
					//check if shared with some one or share with some departments
		        	$shared = Input::get('share_with');
		        	if(count($shared)>0 && Input::get('access')==2)
		        	{
		        		$users = array();
		        		$deps = array();
		        		
		        		for($i=0;$i<count($shared);$i++)
		        		{
		        			$shared_with = $shared[$i];
		        			//explode shared
		        			$shared_with = explode("_", $shared_with);
		        			//check if shared with user
		        			if($shared_with[0] == 'u')
		        			{
		        				$data = array(
		        						'user_id' => $shared_with[1],
		        						'task_group_id' => $record_id
		        					);
		        				//push to user array
		        				array_push($users, $data);
		        			}//check if shared with department
		        			else if($shared_with[0] == 'd')
		        			{
		        				$data = array(
		        						'department_id' => $shared_with[1],
		        						'task_group_id' => $record_id
		        					);
		        				//push to department array
		        				array_push($deps, $data);
		        			}
		        		}
	
		        		//then, insert document shared
		        		if(count($users)>0)
		        		{
		        			It_taskGroup::insertBatch('group_shares_user',$users);
		        		}
		        		if(count($deps)>0)
		        		{
		        			It_taskGroup::insertBatch('group_shares_department',$deps);
		        		}
		        	}
					*/
					return \Redirect::route('getTaskGroups_it')->with("success","The Record Saved Successfully.");
	
				}
				else
		        {
		            return \Redirect::route("getTaskGroups_it")->with("fail","An error occured plase try again.");
		        }
			}
		}
		else
		{
			return showWarning();
		}
	}
	//delete task group
	public function deleteTaskGroup($id=0)
	{
		if(isGroupCreator_it($id))
		{
			//delete related
			It_taskGroup::deleteRelated($id);
	
			$dep = It_taskGroup::find($id);
			if($dep->delete())
	        {
	            return \Redirect::route("getTaskGroups_it")->with("success","You successfuly deleted record, ID: <span style='color:red;font_weight:bold;'>{{$id}}</span>");
	        }
	        else
	        {
	            return \Redirect::route("getTaskGroups_it")->with("fail","An error occured plase try again.");
	        }
		}
		else
		{
			return showWarning();
		}
	}
	//is task group included in report or not
	public function isTaskGroupInReport()
	{
		$task_group = Input::get('task_group');
		$in_report = DB::connection('helpdesk')->table('task_group')->where('id',$task_group)->pluck('in_report');
		//check if included or not
		if($in_report == 1)
		{
			return json_encode(array('condition'=>'true'));
		}
		else
		{
			return json_encode(array('condition'=>'false'));
		}
	}
	public static function getAll()
	{
		return DB::connection('helpdesk')->table('task_group')->get();
	}
	public static function getAll_myGroups()
	{
		return DB::connection('helpdesk')->table('task_group')
		//->whereRaw('(access_level = 1 AND dep_id = '.\Auth::user()->department_id.') OR (access_level = 1 AND user_id = '.\Auth::user()->id.') OR user_id = '.\Auth::user()->id.'')
		->where('access_level',1)
		->get();
	}
	public static function checkGroup_task($id=0)
	{
		return DB::connection('helpdesk')->table('tasks')->where('task_group_id',$id)->first();
	}
}
?>
