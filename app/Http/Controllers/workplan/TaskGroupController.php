<?php 

namespace App\Http\Controllers\workplan;
use App\Http\Controllers\Controller;
use App\models\workplan\TaskGroup;
use App\models\DepartmentX;
use App\models\workplan\Report;
use App\models\User;
use Auth;
use View;
use Input;
use Illuminate\Support\Collection;
use Datatable;
use DB;

class TaskGroupController extends Controller
{
	//get task group list
	public function getTaskGroups()
	{
		return View::make("task.task_group.list");
	}

	// Get the Tasks based on its id for dataTable.
	public function getTaskGroupData()
	{
		//get all data 
		$object = TaskGroup::getData();
		$collection = new Collection($object);
		return Datatable::collection($collection)
							->showColumns('id','title','description','creator')
							->addColumn('operation',function($option){
								$options = '';
								if($option->user_id == Auth::user()->id OR isAdmin()){
									$options .= '<a href="'.route('getTaskGroupUpdate',$option->id).'"><i class="fa fa-edit"></i> Edit</a> &nbsp;';
									if(checkGroup_task($option->id))
									{
										$options .= '|&nbsp;<a href="'.route('getDeleteTaskGroup',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="fa fa-trash-o"></i> Delete</a>';
									}	
									else
									{
										$options .= '|&nbsp;<a href="javascript:void()" onclick="alert(\'You can not delete this task since it has some tasks!\')" style="color:red"><i class="fa fa-trash-o"></i> Delete</a>';
									}			
								}
								return $options;
		
							})
							->make();
	}

	//get create form for task group
	public function getTaskGroupCreate()
	{
		//get departments
		$data['deps'] = DepartmentX::getAll();
		//get users
		//$data['users'] = User::getAll();

		$users = Report::getDepUsers();
		$managers = Report::getDepUsers(true);
		$data['users'] = $users;

		$data['managers'] = $managers;

		return View::make('task.task_group.create',$data);
	}

	//get edit form for task group
	public function getTaskGroupUpdate($id=0)
	{
		if(isGroupCreator($id))
		{
			//get departments
			$data['deps'] = DepartmentX::getAll();
			//get users
			$data['users'] = User::getAll();
			//get task group details
			$data['taskGroupDetails'] = TaskGroup::getTaskGroupDetails($id);
			
			//get task group shared department ---------------------------------------//
			$SharedDeps = TaskGroup::getTaskGroupShares('group_shares_department',$id);
			$SharedDepsArr = array();
			foreach($SharedDeps AS $item)
			{
				//push item to array
				array_push($SharedDepsArr, $item->department_id);
			}
			$data['shared_deps'] = $SharedDepsArr;
			//------------------------------------------------------------------------//
	
			//get task group shared users --------------------------------------------//
			$SharedUsers = TaskGroup::getTaskGroupShares('group_shares_user',$id);
			$SharedUsersArr = array();
			foreach($SharedUsers AS $item)
			{
				//push item to array
				array_push($SharedUsersArr, $item->user_id);
			}
			$data['shared_users'] = $SharedUsersArr;
			//------------------------------------------------------------------------//
	
			return View::make('task.task_group.edit',$data);
		}
		else
		{
			return showWarning();
		}
	}
	
	//create new record task group
	public function postTaskGroupCreate()
	{
		//validate fields
		$validates = \Validator::make(Input::all(), array(

			"title"							=> "required",
			//"description"					=> "required"
			
		));

		//check the validation
		if($validates->fails())
		{	
			return \Redirect::route('getTaskGroupCreate')->withErrors($validates)->withInput();
		}
		else
		{		
			$object = new TaskGroup;
			
			$object->title = Input::get("title");
			$object->description = Input::get("description");
			$object->access_level = Input::get("access");
			/*
			if(isManager())
			{
				$object->access_level = Input::get("access");
			}
			else
			{
				$object->access_level = 2;//personal
			}
			*/
			$object->user_id = Auth::user()->id;
			$object->dep_id = Auth::user()->department_id;
			if(Input::get('in_report'))
			{
				$object->in_report = Input::get('in_report');
			}
			
			if($object->save())
			{
				/*
				$record_id = $object->id;
				//check if shared with some one or share with some departments
	        	$shared = Input::get('share_with');
	        	if(count($shared)>0)
	        	{
	        		$users = array();
	        		$deps = array();
	        		
	        		for($i=0;$i<count($shared);$i++)
	        		{
	        			$shared_with = $shared[$i];
	        			//explode shared
	        			$shared_with = explode("_", $shared_with);
	        			//check if shared with user
	        			if($shared_with[0] == 'u')
	        			{
	        				$data = array(
	        						'user_id' => $shared_with[1],
	        						'task_group_id' => $record_id
	        					);
	        				//push to user array
	        				array_push($users, $data);
	        			}//check if shared with department
	        			else if($shared_with[0] == 'd')
	        			{
	        				$data = array(
	        						'department_id' => $shared_with[1],
	        						'task_group_id' => $record_id
	        					);
	        				//push to department array
	        				array_push($deps, $data);
	        			}
	        		}

	        		//then, insert document shared
	        		if(count($users)>0)
	        		{
	        			TaskGroup::insertBatch('group_shares_user',$users);
	        		}
	        		if(count($deps)>0)
	        		{
	        			TaskGroup::insertBatch('group_shares_department',$deps);
	        		}
	        	}
				*/
				return \Redirect::route('getTaskGroups')->with("success","The Record Saved Successfully.");

			}
			else
	        {
	            return \Redirect::route("getTaskGroups")->with("fail","An error occured plase try again.");
	        }
		}
	}

	//Update Task group details
	public function postTaskGroupUpdate($id=0)
	{
		if(isGroupCreator($id))
		{
			//validate fields
			$validates = \Validator::make(Input::all(), array(
	
				"title"							=> "required",
				"description"					=> "required"
				
			));
	
			//check the validation
			if($validates->fails())
			{	
				return \Redirect::route('getTaskGroupUpdate',$id)->withErrors($validates)->withInput();
			}
			else
			{
				
				$object = TaskGroup::find($id);
				
				$object->title = Input::get("title");
				$object->description = Input::get("description");
				if(isManager())
				{
					$object->access_level = Input::get("access");
				}
				else
				{
					$object->access_level = 2;//personal
				}
				
				if(Input::get('in_report'))
				{
					$object->in_report = Input::get('in_report');
				}
				
				if($object->save())
				{
					/*
					$record_id = $id;
					//remove the old related task group
					TaskGroup::removeTaskGroupRelated('group_shares_department',$record_id);
					TaskGroup::removeTaskGroupRelated('group_shares_user',$record_id);
	
					//check if shared with some one or share with some departments
		        	$shared = Input::get('share_with');
		        	if(count($shared)>0 && Input::get('access')==2)
		        	{
		        		$users = array();
		        		$deps = array();
		        		
		        		for($i=0;$i<count($shared);$i++)
		        		{
		        			$shared_with = $shared[$i];
		        			//explode shared
		        			$shared_with = explode("_", $shared_with);
		        			//check if shared with user
		        			if($shared_with[0] == 'u')
		        			{
		        				$data = array(
		        						'user_id' => $shared_with[1],
		        						'task_group_id' => $record_id
		        					);
		        				//push to user array
		        				array_push($users, $data);
		        			}//check if shared with department
		        			else if($shared_with[0] == 'd')
		        			{
		        				$data = array(
		        						'department_id' => $shared_with[1],
		        						'task_group_id' => $record_id
		        					);
		        				//push to department array
		        				array_push($deps, $data);
		        			}
		        		}
	
		        		//then, insert document shared
		        		if(count($users)>0)
		        		{
		        			TaskGroup::insertBatch('group_shares_user',$users);
		        		}
		        		if(count($deps)>0)
		        		{
		        			TaskGroup::insertBatch('group_shares_department',$deps);
		        		}
		        	}
					*/
					return \Redirect::route('getTaskGroups')->with("success","The Record Saved Successfully.");
	
				}
				else
		        {
		            return \Redirect::route("getTaskGroups")->with("fail","An error occured plase try again.");
		        }
			}
		}
		else
		{
			return showWarning();
		}
	}
	//delete task group
	public function deleteTaskGroup($id=0)
	{
		if(isGroupCreator($id))
		{
			//delete related
			TaskGroup::deleteRelated($id);
	
			$dep = TaskGroup::find($id);
			if($dep->delete())
	        {
	            return \Redirect::route("getTaskGroups")->with("success","You successfuly deleted record, ID: <span style='color:red;font_weight:bold;'>{{$id}}</span>");
	        }
	        else
	        {
	            return \Redirect::route("getTaskGroups")->with("fail","An error occured plase try again.");
	        }
		}
		else
		{
			return showWarning();
		}
	}
	//is task group included in report or not
	public function isTaskGroupInReport()
	{
		$task_group = Input::get('task_group');
		$in_report = DB::connection('workplan')->table('task_group')->where('id',$task_group)->pluck('in_report');
		//check if included or not
		if($in_report == 1)
		{
			return json_encode(array('condition'=>'true'));
		}
		else
		{
			return json_encode(array('condition'=>'false'));
		}
	}
}
?>
