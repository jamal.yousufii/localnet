<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use App\Notifications\NewProject;
use App\models\Requests;
use App\Surveyor;
use App\User;
use Validator;
use Redirect;

class SurveyorController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    // Get Language
    $lang = get_language();
    if(is_admin())
      $surveyors = Surveyor::all(); 
    else
      $surveyors = Surveyor::where('user_id',userid())->get(); 

    // Load view to show result
    return view('egov/surveyor/view',compact('lang','surveyors'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $lang = get_language();
    // Load view to show result
    return view('egov/surveyor/create',compact('lang'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // Validate the request...
       // Validate the request...
    $validates = $request->validate([
        'name'      => 'required',
        'job'      => 'required',
        'education' => 'required',
        'organization_id'  => 'required',
    ]);

    // Language
    $lang = get_language();
    // Insert Record
    $surveyor = Surveyor::create($request->all());   
    if($surveyor)
    {

      Session()->flash('success', __("global.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Dycript the id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get Departments
    $data['deps']  = HRDepartments::select('id','name_'.$lang.' as name')->get();
    // Get Document Type
    $data['types'] = Documents::select('id','name_'.$lang.' as name')->get();
    // Get Record
    $data['record'] = Requests::find($id);
    // Load view to show result
    return view('pmis/requests/edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    $lang = get_language();
    $id = decrypt(Input::get('enc_id'));
    $req = Requests::find($id);
    $req->request_date  = dateProvider($request->request_date,$lang);
    $req->department_id = $request->department_id;
    $req->doc_type      = $request->doc_type;
    $req->doc_number    = $request->request_number;
    $req->goals         = $request->goals;
    $req->description   = $request->description;
    $req->updated_by    = userid();
    $updated = $req->save();
    if($updated)
    {
      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

}
?>
