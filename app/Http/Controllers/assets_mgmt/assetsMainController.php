<?php 

namespace App\Http\Controllers\assets_mgmt;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\models\assets_mgmt\assetsMgmtModel;
use Illuminate\Support\Collection;
use App\library\jdatetime;
use View;
use DB;
use Validator;
use Input;
use Redirect;
use Response;
use Datatable;
use Session;
use DateTime;
use DateInterval;
use File;
use Excel;
use Crypt;		
use Illuminate\Support\Str;
use URL;
use Carbon\Carbon;

class assetsMainController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		if(!Auth::check())
		{
			return Redirect::route('getLogin');
		}	
	}
//================================================= Assets Management Notification Controller Functions ============================
// get the list Items.
public function viewTheNotification($table = "",$record_id = 0,$notification_id = 0)
{
	$record_id = Crypt::encrypt($record_id);
	if($table=="purchase")
	{
		// update the is_viewed field.
		assetsMgmtModel::updateNotification($notification_id);
		return $this->assetPurchaseDetails($record_id);
	}
	elseif($table=="inward")
	{
		// update the is_viewed field.
		assetsMgmtModel::updateNotification($notification_id);
		return $this->assetInwardDetails($record_id);
	}
	elseif($table=="asset_identification")
	{
		// update the is_viewed field.
		assetsMgmtModel::updateNotification($notification_id);
		return $this->assetIdentityDetails($record_id);
	}
	elseif($table=="allotment")
	{
		// update the is_viewed field.
		assetsMgmtModel::updateNotification($notification_id);
		return $this->assetAllotDetails($record_id);
	}
	elseif($table=="allotment_return")
	{
		// update the is_viewed field.
		assetsMgmtModel::updateNotification($notification_id);
		return $this->allotmentReturnDetails($record_id);
	}
	elseif($table=="allotment_to_individual")
	{
		// update the is_viewed field.
		assetsMgmtModel::updateNotification($notification_id);
		return $this->allotmentToIndividualDetails($record_id);
	}
	elseif($table=="allotment_to_storekeeper")
	{
		// update the is_viewed field.
		assetsMgmtModel::updateNotification($notification_id);
		return $this->allotmentToStoreKeeperDetails($record_id);
	}
}
//================================================= Authorize or Reject Recirds Controller Functions ============================

	public function authorizeOrRejectRecordDetail()
	{
		$record_id = Crypt::decrypt(Input::get('record_id'));
		$table = Input::get('table');
		$reject_reason = Input::get('reject_reason');
		if($reject_reason != "")
		{
			$rejectData = array("reject_reason" => $reject_reason);
			$rejected = assetsMgmtModel::getRejectAssetRecord($table,$record_id,$rejectData);
			if($rejected){

				// Now insert the notification details.
				$notification = array(
					'subject' => "$table Rejected",
					'table_name' => $table,
					'table_id' => $record_id,
					'user_id' => Auth::user()->username,
					'created_at' => date('Y-m-d H:i:s')
				);
				assetsMgmtModel::addNotification($notification);

				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => "$table",
					'record_id'=>$record_id,
					'user' => Auth::user()->username,
					'action' => "Rejected",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>$table Detailed Information Successfully Rejected / جزئیات جنس رد گردید</div>";
			}
			else
			echo "<div class='alert alert-danger'>Something was wrong / مشکل وجود دارد</div>";
		}
		else{
			$authorized = assetsMgmtModel::authorizeAssetRecord($table,$record_id);
			if($authorized) 
			{
				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => "$table",
					'record_id'=>$record_id,
					'user' => Auth::user()->username,
					'action' => "Authorized",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>$table Detailed Information Successfully Authorized / جزئیات جنس تائید گردید</div>";
			}
			else
			echo "<div class='alert alert-danger'>Something was wrong / مشکل وجود دارد</div>";
		}
	}

//================================================= Assets Master Record Management or Settings Controller Functions ============================

	// get the list Items.
	public function assetsMgmtItemList()
	{
		//load view
		return View::make('assets_mgmt.settings.itemsList');
	}

	// get items nature
	public function getAssetNature()
	{
		$data['records'] = assetsMgmtModel::getAssetsNature();
		return view::make('assets_mgmt.settings.asset_nature',$data);
	}

	public function insertAssetNature()
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"asset_nature_name"					=> "required"
			));

		if($validates->fails())
		{	
			// return Redirect::route('getAssetsMgmtItemsList')->withErrors($validates)->withInput();
			echo "<div class='alert alert-danger'>Please fill the required fields</div>";
		}
		else
		{
			$asset_nature_data = array('name' => Input::get('asset_nature_name'));
			$inserted = assetsMgmtModel::addAssetNature($asset_nature_data);
		    if($inserted){
				
				// Now insert the notification details.
				// $notification = array(
				// 	'subject' => 'New Asset Nature added',
				// 	'table_name' => "asset_nature",
				// 	'table_id' => $inserted,
				// 	'user_id' => Auth::user()->username,
				// 	'created_at' => date('Y-m-d H:i:s')
				// );
				//assetsMgmtModel::addNotification($notification);

		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'asset_nature',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
		    	echo "<div class='alert alert-success'>Asset Nature Successfully Added</div>";
	        }
	        else
	        {
	            echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
	        }
		}
	}

	public function deleteAssetNature()
	{
		$id = Input::get('record_id');
		//$id = Crypt::decrypt($id);
		$deleted = DB::connection("assets_mgmt")->table("asset_nature")->where("id", $id)->update(array("deleted" => 1));
		//dd($deleted);
		if($deleted)
	    {
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => "asset_nature",
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
    		echo "<div class='alert alert-success'>Asset Nature Successfully Deleted</div>";
        }
	}
	// get main items
	public function getAssetType()
	{
		$data['records'] = assetsMgmtModel::getAssetsType();
		return view::make('assets_mgmt.settings.asset_type',$data);
	}

	public function insertAssetType()
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"asset_type_name"					=> "required"
			));

		if($validates->fails())
		{	
			// return Redirect::route('getAssetsMgmtItemsList')->withErrors($validates)->withInput();
			echo "<div class='alert alert-danger'>Please fill the required fields</div>";
		}
		else
		{
			$asset_type_data = array('name' => Input::get('asset_type_name'));
			$inserted = assetsMgmtModel::addAssetType($asset_type_data);
		    if($inserted){
				
				// Now insert the notification details.
				// $notification = array(
				// 	'subject' => "New Asset Type added",
				// 	'table_name' => "asset_type",
				// 	'table_id' => $inserted,
				// 	'user_id' => Auth::user()->username,
				// 	'created_at' => date('Y-m-d H:i:s')
				// );
				// assetsMgmtModel::addNotification($notification);

		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'asset_type',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
		    	echo "<div class='alert alert-success'>Asset Type Successfully Added</div>";
	        }
	        else
	        {
	            echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
	        }
		}
	}

	public function deleteAssetType()
	{
		$id = Input::get('record_id');
		//$id = Crypt::decrypt($id);
		$deleted = DB::connection("assets_mgmt")->table("asset_type")->where("id", $id)->update(array("deleted" => 1));
		//dd($deleted);
		if($deleted)
	    {
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => "asset_type",
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
    		echo "<div class='alert alert-success'>Asset Type Successfully Deleted</div>";
        }
	}
	// get main items
	public function getAssetMainItems()
	{
		$data['records'] = assetsMgmtModel::getMainItems();
		return view::make('assets_mgmt.settings.main_items',$data);
	}
	// get the main item code based on its id.
	public function mainItemCode()
	{
		$id = Input::get("main_item_id");
		$main_item_code = assetsMgmtModel::getMainItemCode($id);
		echo $main_item_code;
	}

	// get the sub item code based on its id.
	public function subItemCode()
	{
		$id = Input::get("sub_item_id");
		$sub_item_code = assetsMgmtModel::getSubItemCode($id);
		echo $sub_item_code;
	}
	
	// get the end item code based on its id.
	public function endItemCode()
	{
		$id = Input::get("end_item_id");
		$end_item_code = assetsMgmtModel::getEndItemCode($id);
		echo $end_item_code;
	}

	// get sub items
	public function getAssetSubItems()
	{
		$data['records'] = assetsMgmtModel::getSubItems();
		return view::make('assets_mgmt.settings.sub_items',$data);
	}
	// get end items
	public function getAssetEndItems()
	{
		$data['records'] = assetsMgmtModel::getEndItems();
		return view::make('assets_mgmt.settings.end_items',$data);
	}
	// get item detail
	public function getAssetItemDetail()
	{
		$data['records'] = assetsMgmtModel::getItemDetail();
		return view::make('assets_mgmt.settings.item_detail',$data);
	}
// ================================================================== Asset Sanctions =====================================================
	public function getAssetsMgmtForm()
	{
		return view('assets_mgmt.itemsList');
	}

	public function getAssetSanctions()
	{
		$data['parentDeps'] = getDepartmentWhereIn();
		$data['records'] = assetsMgmtModel::getAssetSanctions();
		return view::make('assets_mgmt.settings.sanctions',$data);
	}

	public function insertAssetSanction()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"code"			=> "required|unique:assets_management.sanction",
			"department"	=> "required",
			"name"			=> "required"
		));

		if($validates->fails())
		{	
			// return Redirect::route('getAssetsMgmtItemsList')->withErrors($validates)->withInput();
			echo "error";
		}
		else
		{
	    	//dd($_POST);
			// get the form data.
			$data = array(
				"code"				=> Input::get("code"),
				"name"				=> Input::get("name"),
				"department_id"		=> Input::get("department"),
				"deleted"			=> 0,
				"user_id"			=> Auth::user()->id,
				"created_at"		=> date('Y-m-d H:i:s')
			);

		    $inserted = assetsMgmtModel::addSanction($data);
		    if($inserted){
				
				// Now insert the notification details.
				// $notification = array(
				// 	'subject' => "New Sanction added",
				// 	'table_name' => "sanction",
				// 	'table_id' => $inserted,
				// 	'user_id' => Auth::user()->username,
				// 	'created_at' => date('Y-m-d H:i:s')
				// );
				// assetsMgmtModel::addNotification($notification);

		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'sanction',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
		    	echo "<div class='alert alert-success'>Sanction Successfully Added</div>";
	        }
	        else
	        {
	            echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
	        }
	    }
	}

	public function deleteAssetSanction()
	{
		$id = Input::get('record_id');
		$table = Input::get('table');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deleteSanction($table,$id);
		//dd($deleted);
		if($deleted)
	    {
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => $table,
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
    		echo "<div class='alert alert-success'>Sanction Successfully Deleted</div>";
        }
	}
// ===================================================== Asset Vendors ==========================================================
	public function getAssetVendors()
	{
		$data['records'] = assetsMgmtModel::getAssetVendors();
		return view::make('assets_mgmt.settings.vendors',$data);
	}

	public function insertAssetVendor()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"code"		=> "required|unique:assets_management.vendor",
			"name"		=> "required"
		));

		if($validates->fails())
		{	
			echo "error";
		}
		else
		{
	    	//dd($_POST);
			// get the form data.
			$data = array(
				"code"				=> Input::get("code"),
				"name"				=> Input::get("name"),
				"deleted"			=> 0,
				"user_id"			=> Auth::user()->id,
				"created_at"		=> date('Y-m-d H:i:s')
			);

		    $inserted = assetsMgmtModel::addVendor($data);
		    if($inserted){
				
				// Now insert the notification details.
				// $notification = array(
				// 	'subject' => "New Vendor added",
				// 	'table_name' => "vendor",
				// 	'table_id' => $inserted,
				// 	'user_id' => Auth::user()->username,
				// 	'created_at' => date('Y-m-d H:i:s')
				// );
				// assetsMgmtModel::addNotification($notification);

		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'vendor',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
		    	echo "<div class='alert alert-success'>Vendor Successfully Added</div>";
	        }
	        else
	        {
	            echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
	        }
	    }
	}

	public function deleteAssetVendor()
	{
		$id = Input::get('record_id');
		$table = Input::get('table');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deleteVendor($table,$id);
		//dd($deleted);
		if($deleted)
	    {
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => $table,
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
    		echo "<div class='alert alert-success'>Vendor Successfully Deleted</div>";
        }
	}
// ======================================================== Asset Locations ===================================================================
	public function getAssetLocations()
	{
		$data['records'] = assetsMgmtModel::getAssetLocations();
		$data['parentDeps'] = getDepartmentWhereIn();
		return view::make('assets_mgmt.settings.locations',$data);
	}

	public function insertAssetLocation()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"code"							=> "required|unique:assets_management.location",
			"exact_location"				=> "required",
			"general_dept"					=> "required",
		));

		if($validates->fails())
		{	
			// return Redirect::route('getAssetsMgmtItemsList')->withErrors($validates)->withInput();
			echo "error";
		}
		else
		{
	    	//dd($_POST);
			// get the form data.
			$data = array(
				"code"					=> Input::get("code"),
				"exact_location"		=> Input::get("exact_location"),
				"general_dept"			=> Input::get("general_dept"),
				"sub_dept"				=> Input::get("sub_dept"),
				"deleted"				=> 0,
				"user_id"				=> Auth::user()->id,
				"created_at"			=> date('Y-m-d H:i:s')
			);

		    $inserted = assetsMgmtModel::addLocation($data);
		    if($inserted){
				
				// Now insert the notification details.
				// $notification = array(
				// 	'subject' => "New Location added",
				// 	'table_name' => "location",
				// 	'table_id' => $inserted,
				// 	'user_id' => Auth::user()->username,
				// 	'created_at' => date('Y-m-d H:i:s')
				// );
				// assetsMgmtModel::addNotification($notification);

		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'location',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
		    	echo "<div class='alert alert-success'>Location Successfully Added</div>";
	        }
	        else
	        {
	            echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
	        }
	    }
	}
	public function deleteAssetLocation()
	{
		$id = Input::get('record_id');
		$table = Input::get('table');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deleteLocation($table,$id);
		//dd($deleted);
		if($deleted)
		{
			// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => $table,
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
			echo "<div class='alert alert-success'>Location Successfully Deleted</div>";
		}
	}
	// ======================================================== End of Asset Locations ===================================================================
// ======================================================== Asset Currency ===================================================================
	public function getAssetCurrency()
	{
		$data['records'] = assetsMgmtModel::getAssetCurrency();
		return view::make('assets_mgmt.settings.currency',$data);
	}

	public function insertAssetCurrency()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"code"					=> "required|unique:assets_management.currency",
			"name"					=> "required"
		));

		if($validates->fails())
		{	
			// return Redirect::route('getAssetsMgmtItemsList')->withErrors($validates)->withInput();
			echo "error";
		}
		else
		{
	    	//dd($_POST);
			// get the form data.
			$data = array(
				"code"					=> Input::get("code"),
				"name"					=> Input::get("name"),
				"deleted"				=> 0,
				"user_id"				=> Auth::user()->id,
				"created_at"			=> date('Y-m-d H:i:s')
			);

		    $inserted = assetsMgmtModel::addCurrency($data);
		    if($inserted){
				
				// Now insert the notification details.
				// $notification = array(
				// 	'subject' => "New Currency Added",
				// 	'table_name' => "currency",
				// 	'table_id' => $inserted,
				// 	'user_id' => Auth::user()->username,
				// 	'created_at' => date('Y-m-d H:i:s')
				// );
				// assetsMgmtModel::addNotification($notification);

		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'currency',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
		    	echo "<div class='alert alert-success'>Currency Successfully Added</div>";
	        }
	        else
	        {
	            echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
	        }
	    }
	}
	public function deleteAssetCurrency()
	{
		$id = Input::get('record_id');
		$table = Input::get('table');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deleteCurrency($table,$id);
		//dd($deleted);
		if($deleted)
		{
			// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => $table,
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
			echo "<div class='alert alert-success'>Currency Successfully Deleted</div>";
		}
	}
// ======================================================== End of Asset Currency ===================================================================
// ======================================================== Asset Store Keeper ===================================================================
	public function getAssetStoreKeeper()
	{
		$data['records'] = assetsMgmtModel::getAssetStoreKeeper();
		return view::make('assets_mgmt.settings.store_keeper',$data);
	}

	public function insertAssetStoreKeeper()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"code"					=> "required|unique:assets_management.store_keeper",
			"name"					=> "required"
		));

		if($validates->fails())
		{	
			// return Redirect::route('getAssetsMgmtItemsList')->withErrors($validates)->withInput();
			echo "error";
		}
		else
		{
	    	//dd($_POST);
			// get the form data.
			$data = array(
				"code"					=> Input::get("code"),
				"name"					=> Input::get("name"),
				"deleted"				=> 0,
				"user_id"				=> Auth::user()->id,
				"created_at"			=> date('Y-m-d H:i:s')
			);

		    $inserted = assetsMgmtModel::addStoreKeeper($data);
		    if($inserted){
				
				// Now insert the notification details.
				// $notification = array(
				// 	'subject' => "New Store Keeper Added",
				// 	'table_name' => "store_keeper",
				// 	'table_id' => $inserted,
				// 	'user_id' => Auth::user()->username,
				// 	'created_at' => date('Y-m-d H:i:s')
				// );
				// assetsMgmtModel::addNotification($notification);

		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'store_keeper',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
		    	echo "<div class='alert alert-success'>Store Keeper Successfully Added</div>";
	        }
	        else
	        {
	            echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
	        }
	    }
	}
	public function deleteAssetStoreKeeper()
	{
		$id = Input::get('record_id');
		$table = Input::get('table');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deleteStoreKeeper($table,$id);
		//dd($deleted);
		if($deleted)
		{
			// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => $table,
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
			echo "<div class='alert alert-success'>Store Keeper Successfully Deleted</div>";
		}
	}
// ======================================================== End of Asset Store Keeper Controller Functions ===================================================================
// ======================================================== Main,Sub,End and Item details Controller Functions ===================================================================

	public function addItems($table="")
	{
		if($table == "main_item")
		{
			//validate the input fields
			$validates = Validator::make(Input::all(), array(
				"main_item_code"		=> "required|unique:assets_management.main_item",
				"main_item_name"		=> "required"
			));
		}
		elseif($table == "sub_item")
		{
			//validate the input fields
			$validates = Validator::make(Input::all(), array(
				"main_item_code"		=> "required",
				"main_item_name"		=> "required",
				"sub_item_code"			=> "required|unqiue:assets_management.sub_item",
				"sub_item_name"			=> "required"
			));
		}
		elseif($table == "end_item")
		{
			//validate the input fields
			$validates = Validator::make(Input::all(), array(
				"main_item_code"		=> "required",
				"sub_item_code"			=> "required",
				"end_item_code"			=> "required|unqiue:assets_management.end_item",
				"main_item_name"		=> "required",
				"sub_item_name"			=> "required",
				"end_item_name"			=> "required"
			));
		}
		elseif($table == "item_detail")
		{
			//validate the input fields
			$validates = Validator::make(Input::all(), array(
				"main_item_code"		=> "required",
				"sub_item_code"			=> "required",
				"end_item_code"			=> "required",
				"item_detail_code"		=> "required|unqiue:assets_management.item_detail",
				"main_item_name"		=> "required",
				"sub_item_name"			=> "required",
				"end_item_name"			=> "required",
				"item_detail_name"		=> "required"
			));
		}
		

		if($validates->fails())
		{	
			// return Redirect::route('getAssetsMgmtItemsList')->withErrors($validates)->withInput();
			echo "error";
		}
		else
		{
	    	//dd($_POST);
			if(Input::get('main_item_code') != "" || Input::get('sub_item_code') != "" || Input::get('end_item_code') != "" || Input::get('item_detail_code') != "" || Input::get('main_item_name') != "" || Input::get('sub_item_name') != "" || Input::get('end_item_name') != "" || Input::get('item_detail_name') != "")
		    {
		    	$main_item_code = Input::get('main_item_code');
				$sub_item_code = Input::get('sub_item_code');
				$end_item_code = Input::get('end_item_code');
				$item_detail_code = Input::get('item_detail_code');
				$main_item_name = Input::get('main_item_name');
				$sub_item_name = Input::get('sub_item_name');
				$end_item_name = Input::get('end_item_name');
				$item_detail_name = Input::get('item_detail_name');
		    }
		    else
		    { 
		    	$main_item_code = "";
				$sub_item_code = "";
				$end_item_code = "";
				$item_detail_code = "";
				$main_item_name = "";
				$sub_item_name = "";
				$end_item_name = "";
				$item_detail_name = "";
		    }			

		    if($table == "main_item")
		    {
		    	$asset_nature = Input::get("asset_nature");
		    	// get the form data.
		    	$data = array(
		    		"main_item_code"				=> $main_item_code,
		    		"main_item_name"				=> $main_item_name,
		    		"asset_nature"					=> $asset_nature,
		    		"deleted"						=> 0,
		    		"user_id"						=> Auth::user()->id,
		    		"created_at"					=> date('Y-m-d H:i:s')
				);
		    }
		    elseif ($table == "sub_item") {
		    	$asset_type = Input::get("asset_type");
		    	// get the form data.
		    	$data = array(
		    		"sub_item_code"				=> $main_item_code."-".$sub_item_code,
		    		"sub_item_name"				=> $sub_item_name,
		    		"asset_type"				=> $asset_type,
		    		"deleted"					=> 0,
		    		"user_id"					=> Auth::user()->id,
		    		"main_item_code"			=> $main_item_code,
		    		"created_at"				=> date('Y-m-d H:i:s')
		    	);
		    }
		    elseif ($table == "end_item") {
		    	// get the form data.
		    	$data = array(
		    		"end_item_code"				=> $sub_item_code."-".$end_item_code,
					"end_item_name"				=> $end_item_name,
					"main_item_code"			=> $main_item_code,
					"sub_item_code"				=> $sub_item_code,
		    		"deleted"					=> 0,
		    		"user_id"					=> Auth::user()->id,
		    		"created_at"				=> date('Y-m-d H:i:s')
		    	);
			}
			elseif ($table == "item_detail") {
		    	// get the form data.
		    	$data = array(
					"main_item_code"	=> $main_item_code,
					"sub_item_code"		=> $sub_item_code,
					"end_item_code"		=> $end_item_code,
					"item_detail_code"	=> $end_item_code."-".$item_detail_code,
					"item_detail_name"	=> $item_detail_name,
		    		"deleted"			=> 0,
		    		"user_id"			=> Auth::user()->id,
		    		"created_at"		=> date('Y-m-d H:i:s')
		    	);
		    }

		    $inserted = assetsMgmtModel::addItems($table,$data);
		    if($inserted){
				
				// Now insert the notification details.
				// $notification = array(
				// 	'subject' => "New $table added",
				// 	'table_name' => $table,
				// 	'table_id' => $inserted,
				// 	'user_id' => Auth::user()->username,
				// 	'created_at' => date('Y-m-d H:i:s')
				// );
				// assetsMgmtModel::addNotification($notification);
				
		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => $table,
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
		    	echo "<div class='alert alert-success'>".$table." Successfully Added</div>";
	        }
	        else
	        {
	            echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
	        }
	    }
    }

	// delete the items based on sent parameter table.
	public function deleteItem()
	{
		$id = Input::get('record_id');
		$table = Input::get('table');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deleteItem($id,$table);
		//dd($deleted);
		if($deleted)
	    {
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => $table,
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
    		echo "<div class='alert alert-success'>Item Successfully Deleted</div>";
        }
	}

	// =================================================================== Purchase Controller Functions =====================================

	public function assetsMgmtPurchaseList()
	{
		$data['records'] = assetsMgmtModel::getAllPurchases();
		if(Input::get('ajax') == 1)
		{
			return View::make('assets_mgmt.purchase.purchase_list_ajax',$data);
		}
		else
		{
			//load view to show searchpa result
			return View::make('assets_mgmt.purchase.purchase_list',$data);
		}
	}
	public function loadAssetPurchaseEntryPage()
	{
		return view::make('assets_mgmt.purchase.purchase_form');
	}
	public function getItemDetailInformation()
	{
		$item_detail_id = Input::get("item_detail_id");
		$data['records'] = assetsMgmtModel::getItemDetailInformation($item_detail_id);
		return View::make('assets_mgmt.purchase.item_detail_table',$data);
	}
	public function insertAssetPurchase()
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"item_detail_code"				=> "required",
			"sanction_by"					=> "required",
			"peshnehad_number"				=> "required",
			"order_date"					=> "required"
			));

		if($validates->fails())
		{	
			return Redirect::route('assetPurchaseEntryPage')->withErrors($validates)->withInput();
			//echo "<div class='alert alert-danger'>Please fill the required fields</div>";
		}
		else
		{
			//dd($_POST);
			$order_date = convertToGregorian(ymd_format(Input::get('order_date')));
			$purchaseData = array(
				'item_detail_id' 		=> Input::get('item_detail_code'),
				'sanction_by' 			=> Input::get('sanction_by'),
				'peshnehad_number' 		=> Input::get('peshnehad_number'),
				'order_date' 			=> $order_date,
				'remarks' 				=> Input::get('remarks'),
				'number_of_items' 		=> Input::get('number_of_items'),
				'unit_price' 			=> Input::get('unit_price'),
				'total_cost' 			=> Input::get('total_cost'),
				'currency' 				=> Input::get('currency'),
				'user_id' 				=> Auth::user()->id,
				'created_at' 			=> date('Y-m-d H:i:s')
			);
			$inserted = assetsMgmtModel::addPurchaseData($purchaseData);
		    if($inserted){
				
				// Now insert the notification details.
				$notification = array(
					'subject' => "New Purchase added",
					'table_name' => "purchase",
					'table_id' => $inserted,
					'user_id' => Auth::user()->username,
					'created_at' => date('Y-m-d H:i:s')
				);
				assetsMgmtModel::addNotification($notification);

		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'purchase',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				return Redirect::route('getAssetsMgmtPurchaseList')->with("success","Purchase Details Successfully Added");
	        }
	        else
	        {
				return Redirect::route('getAssetsMgmtPurchaseList')->with("fail","Something was wrong, please consult with system developer");
	        }
		}
	}
	public function getLoadPurchaseEditForm()
	{
		$purchase_id = Input::get("id");
		
		$data["row"] = assetsMgmtModel::getPurchaseDetails($purchase_id);
		//load view for editing the purchase details.
		return View::make("assets_mgmt.purchase.edit_purchase",$data);
	}
	public function getUpdateAssetPurchase()
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"sanction_by"					=> "required",
			"peshnehad_number"				=> "required",
			"order_date"					=> "required"
			));

		if($validates->fails())
		{	
			echo "error";
		}
		else
		{
			//dd($_POST);
			$purchase_id = Crypt::decrypt(Input::get('purchase_id'));
			$order_date = convertToGregorian(ymd_format(Input::get('order_date')));
			$object = assetsMgmtModel::find($purchase_id);
					
			$object->sanction_by 			= Input::get("sanction_by");
			$object->peshnehad_number 		= Input::get("peshnehad_number");
			$object->order_date 			= $order_date;
			$object->remarks 				= Input::get("remarks");
			$object->number_of_items 		= Input::get("number_of_items");
			$object->unit_price 			= Input::get("unit_price");
			$object->total_cost 			= Input::get("total_cost");
			$object->currency 				= Input::get("currency");
			$object->user_id 				= Auth::user()->id;
			$object->updated_at 			= date('Y-m-d H:i:s');

			if($object->save()){
		    	
		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'purchase',
					'record_id'=>$purchase_id,
					'user' => Auth::user()->username,
					'action' => "Updated",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Purchase Details Successfully Updated</div>";
	        }
	        else
	        {
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
	        }
		}
	}
	// this function is called from viewTheNotification function as well with the parameter set to it.
	public function assetPurchaseDetails($asset_purchase_id=0)
	{
		$asset_purchase_id = Crypt::decrypt($asset_purchase_id);
		$data['parentDeps'] = getDepartmentWhereIn();
		$data["row"] = assetsMgmtModel::getPurchaseDetails($asset_purchase_id);
		//load view for editing the Asset Identity details.
		return View::make("assets_mgmt.purchase.purchase_details",$data);
	}
	public function getDeleteAssetPurchase()
	{
		$purchase_id = Input::get('record_id');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deletePurchase($purchase_id);
		//dd($deleted);
		if($deleted)
	    {
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => "purchase",
				'record_id'=>$purchase_id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
    		echo "<div class='alert alert-success'>Purchase Successfully Deleted</div>";
        }
	}
// ======================================================== End of Purchase Functions ===================================================================
// ======================================================== Asset Inward for Puchases Functions ===================================================================

	public function purchaseInwardList()
	{
		$data['records'] = assetsMgmtModel::getAllPurchaseInwards();
		if(Input::get('ajax') == 1)
		{
			return View::make('assets_mgmt.inward.inward_list_ajax',$data);
		}
		else
		{
			//load view to show the result
			return View::make('assets_mgmt.inward.inward_list',$data);
		}
	}
	// get the details of item and purchase based on the ids.
	public function itemAndPurchaseInfo()
	{
		$purchase_id = Input::get("purchase_id");

		$item_detail_id = getNameBasedOnId("purchase","item_detail_id",$purchase_id);
		$data['item_detail_info'] = assetsMgmtModel::getItemDetailInformation($item_detail_id);
		
		$data["purchase_details"] = assetsMgmtModel::getPurchaseDetails($purchase_id);
		//Load the Purchase and Inward Details Page.
		return View::make("assets_mgmt.inward.item_and_purchase_details",$data);
	}
	// get the details of the purchase and load the inward form in the model
	public function loadPurchaseInwardForm()
	{
		$purchase_id = Input::get("id");
		
		$data["row"] = assetsMgmtModel::getPurchaseDetails($purchase_id);
		//load the inward form.
		return View::make("assets_mgmt.inward.inward_form",$data);
	}
	public function addPurchaseInwardData()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"inward_date"				=> "required",
			"m7_number"					=> "required",
			"store_keeper"				=> "required",
			"warranty"					=> "required"
			));

		if($validates->fails())
		{	
			echo "validation_error";
		}
		else
		{
			//dd($_POST);
			$inward_date = convertToGregorian(ymd_format(Input::get('inward_date')));
			$purchase_id = Crypt::decrypt(Input::get("purchase_id"));
			$inwardData = array(
				'inward_date' 			=> $inward_date,
				'm7_number' 			=> Input::get('m7_number'),
				'store_keeper' 			=> Input::get('store_keeper'),
				'warranty' 				=> Input::get('warranty'),
				"purchase_id"			=> $purchase_id,
				"deleted"				=> "0",
				'user_id' 				=> Auth::user()->id,
				'created_at' 			=> date('Y-m-d H:i:s')
			);
			$inserted = assetsMgmtModel::addInward($inwardData);
			if($inserted){
				
                // Now insert the notification details.
				$notification = array(
					'subject' => "New Inward added",
					'table_name' => "inward",
					'table_id' => $inserted,
					'user_id' => Auth::user()->username,
					'created_at' => date('Y-m-d H:i:s')
				);
				assetsMgmtModel::addNotification($notification);

				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'inward',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Inward Details Successfully Added / معلومات موفقانه اضافه شد</div>";
			}
			else
			{
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer / مشکل وجود دارد، لطفآ همرای مسئول سیستم به تماس شوید</div>";
			}
		}
	}
	public function loadInwardEditPage()
	{
		$inward_id = Input::get("inward_id");
		
		$data["row"] = assetsMgmtModel::getInwardDetails($inward_id);
		//load view for editing the inward details.
		return View::make("assets_mgmt.inward.inward_edit_form",$data);
	}
	public function updateAssetInward()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"inward_date"				=> "required",
			"m7_number"					=> "required",
			"store_keeper"				=> "required",
			"warranty"					=> "required"
			));

		if($validates->fails())
		{	
			echo "error";
		}
		else
		{
			//dd($_POST);
			$inward_id = Crypt::decrypt(Input::get('inward_id'));
			$inward_date = convertToGregorian(ymd_format(Input::get('inward_date')));

			$inward_data = array(
				"inward_date"			=> $inward_date,
				"m7_number" 			=> Input::get("m7_number"),
				"store_keeper" 			=> Input::get("store_keeper"),
				"warranty" 				=> Input::get("warranty"),
				"user_id" 				=> Auth::user()->id,
				"updated_at" 			=> date('Y-m-d H:i:s')
			);

			$updated = assetsMgmtModel::updateInward($inward_id, $inward_data);

			if($updated){
				
				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'inward',
					'record_id'=>$inward_id,
					'user' => Auth::user()->username,
					'action' => "Updated",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Inward Details Successfully Updated</div>";
			}
			else
			{
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
			}
		}
	}
	// this function is called from viewTheNotification function as well with the parameter set to it.
	public function assetInwardDetails($asset_inward_id=0)
	{
		$asset_inward_id = Crypt::decrypt($asset_inward_id);
		$data['parentDeps'] = getDepartmentWhereIn();
		$data["row"] = assetsMgmtModel::getInwardDetails($asset_inward_id);
		//load view for editing the Asset Identity details.
		return View::make("assets_mgmt.inward.inward_details",$data);
	}
	public function deletePurchaseInward()
	{
		$inward_id = Input::get('record_id');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deleteInward($inward_id);
		//dd($deleted);
		if($deleted)
		{
			// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => "inward",
				'record_id'=>$inward_id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
			echo "<div class='alert alert-success'>Inward Successfully Deleted</div>";
		}
	}

// ======================================================== End of Inward Functions =====================================================================
// ======================================================== Asset Identification Controller Functions ===================================================

	public function assetIdentityList()
	{
		$data['records'] = assetsMgmtModel::getAllAssetIdentities();
		if(Input::get('ajax') == 1)
		{
			return View::make('assets_mgmt.asset_identification.asset_identity_list_ajax',$data);
		}
		else
		{
			//load view to show the result
			return View::make('assets_mgmt.asset_identification.asset_identity_list',$data);
		}
	}
	// get the details of the purchase, inward and load the asset identification form in the model
	public function loadAssetIdentityPage()
	{
		$purchase_id = Input::get("purchase_id");
		$inward_id = Input::get("inward_id");
		
		$data["purchase_details"] = assetsMgmtModel::getPurchaseDetails($purchase_id);
		$data["inward_details"] = assetsMgmtModel::getInwardDetails($inward_id);
		// get item detail id from purchase table based on purchase id.
		$item_detail_id = getNameBasedOnId("purchase","item_detail_id",$purchase_id);
		// get item detail code based on item detail id.
		$data['item_detail_code'] = getNameBasedOnId("item_detail","item_detail_code",$item_detail_id);
		//load the Asset Identity form.
		return View::make("assets_mgmt.asset_identification.asset_identity_form",$data);
	}
	// get the details of purchase and inward based on the ids.
	public function purchaseAndInwardInfo()
	{
		$purchase_id = Input::get("purchase_id");
		$inward_id = Input::get("inward_id");

		$item_detail_id = getNameBasedOnId("purchase","item_detail_id",$purchase_id);
		$data['item_detail_info'] = assetsMgmtModel::getItemDetailInformation($item_detail_id);
		
		$data["purchase_details"] = assetsMgmtModel::getPurchaseDetails($purchase_id);
		$data["inward_details"] = assetsMgmtModel::getInwardDetails($inward_id);
		//Load the Purchase and Inward Details Page.
		return View::make("assets_mgmt.asset_identification.purchase_inward_details",$data);
	}

	public function insertAssetIdentification()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"item_location"				=> "required",
			"asset_number"				=> "required"
			));

		if($validates->fails())
		{	
			echo "validation_error";
		}
		else
		{
			//dd($_POST);
			$inward_id = Crypt::decrypt(Input::get("inward_id"));
			$assetIdentityData = array(
				'item_location' 			=> Input::get("item_location"),
				'asset_number' 				=> Input::get('asset_number'),
				"inward_id"					=> $inward_id,
				"deleted"					=> "0",
				'user_id' 					=> Auth::user()->id,
				'created_at' 				=> date('Y-m-d H:i:s')
			);
			$inserted = assetsMgmtModel::addAssetIdentityData($assetIdentityData);
			if($inserted){
				
                // Now insert the notification details.
				$notification = array(
					'subject' => "New Asset Identification added",
					'table_name' => "asset_identification",
					'table_id' => $inserted,
					'user_id' => Auth::user()->username,
					'created_at' => date('Y-m-d H:i:s')
				);
				assetsMgmtModel::addNotification($notification);

				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'asset_identification',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Asset Identification Details Successfully Added</div>";
			}
			else
			{
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
			}
		}
	}
	public function loadAssetIdentityEditPage()
	{
		$asset_identity_id = Input::get("asset_identity_id");
		$data["row"] = assetsMgmtModel::getAssetIdentityDetails($asset_identity_id);
		//load view for editing the Asset Identity details.
		return View::make("assets_mgmt.asset_identification.asset_identity_edit_form",$data);
	}
	public function updateAssetIdentity()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"item_location"				=> "required",
			"asset_number"				=> "required"
			));

		if($validates->fails())
		{	
			echo "error";
		}
		else
		{
			//dd($_POST);
			$asset_identity_id = Crypt::decrypt(Input::get("asset_identity_id"));
			$assetIdentityData = array(
				'item_location' 			=> Input::get("item_location"),
				'asset_number' 				=> Input::get('asset_number'),
				'user_id' 					=> Auth::user()->id,
				'created_at' 				=> date('Y-m-d H:i:s')
			);
			$updated = assetsMgmtModel::updateAssetIdentityData($asset_identity_id,$assetIdentityData);
			if($updated){
				
				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'asset_identification',
					'record_id'=>$asset_identity_id,
					'user' => Auth::user()->username,
					'action' => "Updated",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Asset Identification Details Successfully Updated</div>";
			}
			else
			{
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
			}
		}
	}
	// this function is called from viewTheNotification function as well with the parameter set to it.
	public function assetIdentityDetails($record_id=0)
	{
		$asset_identity_id = Crypt::decrypt($record_id);
		$data["row"] = assetsMgmtModel::getAssetIdentityDetails($asset_identity_id);
		//dd($data['row']);
		//load view for Viewing the Asset Identity details.
		return View::make("assets_mgmt.asset_identification.asset_identity_details",$data);
	}
	public function deleteAssetIdentity()
	{
		$asset_identity_id = Input::get('record_id');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deleteAssetIdentity($asset_identity_id);
		//dd($deleted);
		if($deleted)
		{
			// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => "asset_identification",
				'record_id'=>$asset_identity_id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
			echo "<div class='alert alert-success'>Asset Identification Successfully Deleted</div>";
		}
	}

// ======================================================== End of Asset Identification Controller Functions ===================================================

// ======================================================== Asset Allotation Controller Functions ===================================================

	public function assetAllotList()
	{
		$data['records'] = assetsMgmtModel::getAllAssetAllots();
		if(Input::get('ajax') == 1)
		{
			return View::make('assets_mgmt.allotment.allot_list_ajax',$data);
		}
		else
		{
			//load view to show the result
			return View::make('assets_mgmt.allotment.allot_list',$data);
		}
	}

	// get the designation or position of the HR employee based on id.
	public function getAssetEmployeePosition()
	{
		$emp_id = Input::get("emp_id");
		$employee_position = getAssetEmployeeDesignation($emp_id);
		return $employee_position;
	}

	// get the HR Employees based on department id.
	public function getEmpBasedOnDept()
	{
		$department_id = Input::get("department_id");
		$emps = getDepartmentEmployeeList($department_id);
		return $emps;
	}
	// get the details of the purchase, inward and load the asset identification form in the model
	public function loadAssetAllotPage()
	{		
		$data['parentDeps'] = getDepartmentWhereIn();
		//load the Asset Identity form.
		return View::make("assets_mgmt.allotment.allot_form",$data);
	}

	public function insertAssetAllot()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"general_dept"				=> "required",
			"sub_dept"					=> "required",
			"allottee_name"				=> "required",
			"item_detail_id"			=> "required",
			"allotment_date"			=> "required",
			"store_keeper"				=> "required",
			"fes5"						=> "required",
			"sanction_by"				=> "required"
			));

		if($validates->fails())
		{	
			echo "validation_error";
		}
		else
		{
			//dd($_POST);
			$allotment_date = convertToGregorian(ymd_format(Input::get('allotment_date')));
			$item_detail_id = Input::get('item_detail_id');
			$assetAllotData = array(
				'general_dept' 				=> Input::get("general_dept"),
				'sub_dept' 					=> Input::get('sub_dept'),
				'allottee_name' 			=> Input::get('allottee_name'),
				'allottee_designation' 		=> Input::get('allottee_designation'),
				'item_detail_id' 			=> $item_detail_id,
				'remark' 					=> Input::get('remark'),
				'allotment_date' 			=> $allotment_date,
				'store_keeper' 				=> Input::get('store_keeper'),
				'fes5' 						=> Input::get('fes5'),
				'sanction_by' 				=> Input::get('sanction_by'),
				'user_id' 					=> Auth::user()->id,
				'created_at' 				=> date('Y-m-d H:i:s')
			);
			$inserted = assetsMgmtModel::addAssetAllotData($assetAllotData);
			if($inserted){
				// now update the alloted field of the item detail table.
				DB::connection("assets_mgmt")->table("item_detail")->where("id",$item_detail_id)->update(array('allotted' => 1));

                // Now insert the notification details.
				$notification = array(
					'subject' => "New Allotment added",
					'table_name' => "allotment",
					'table_id' => $inserted,
					'user_id' => Auth::user()->username,
					'created_at' => date('Y-m-d H:i:s')
				);
				assetsMgmtModel::addNotification($notification);

				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'allotment',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Asset Allotment Details Successfully Added</div>";
			}
			else
			{
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
			}
		}
	}
	public function loadAssetAllotEditPage()
	{
		$asset_allot_id = Input::get("asset_allot_id");
		$data['parentDeps'] = getDepartmentWhereIn();
		$data["row"] = assetsMgmtModel::getAssetAllotDetails($asset_allot_id);
		//load view for editing the Asset Identity details.
		return View::make("assets_mgmt.allotment.allot_edit_form",$data);
	}
	public function updateAssetAllot()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"general_dept"				=> "required",
			"sub_dept"					=> "required",
			"allottee_name"				=> "required",
			"item_detail_id"			=> "required",
			"allotment_date"			=> "required",
			"store_keeper"				=> "required",
			"fes5"						=> "required",
			"sanction_by"				=> "required"
			));

		if($validates->fails())
		{	
			echo "error";
		}
		else
		{
			$asset_allot_id = Crypt::decrypt(Input::get("asset_allot_id"));
			$allotment_date = convertToGregorian(ymd_format(Input::get('allotment_date')));
			$item_detail_id = Input::get('item_detail_id');
			$assetAllotData = array(
				'general_dept' 				=> Input::get("general_dept"),
				'sub_dept' 					=> Input::get('sub_dept'),
				'allottee_name' 			=> Input::get('allottee_name'),
				'allottee_designation' 		=> Input::get('allottee_designation'),
				'item_detail_id' 			=> $item_detail_id,
				'remark' 					=> Input::get('remark'),
				'allotment_date' 			=> $allotment_date,
				'store_keeper' 				=> Input::get('store_keeper'),
				'fes5' 						=> Input::get('fes5'),
				'sanction_by' 				=> Input::get('sanction_by'),
				'user_id' 					=> Auth::user()->id,
				'updated_at' 				=> date('Y-m-d H:i:s')
			);
			$updated = assetsMgmtModel::updateAssetAllotData($asset_allot_id,$assetAllotData);
			if($updated){
				
				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'allotment',
					'record_id'=>$asset_allot_id,
					'user' => Auth::user()->username,
					'action' => "Updated",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Asset Allotment Details Successfully Updated</div>";
			}
			else
			{
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer</div>";
			}
		}
	}
	// this function is called from viewTheNotification function as well with the parameter set to it.
	public function assetAllotDetails($asset_allot_id=0)
	{
		$asset_allot_id = Crypt::decrypt($asset_allot_id);
		$data['parentDeps'] = getDepartmentWhereIn();
		$data["row"] = assetsMgmtModel::getAssetAllotDetails($asset_allot_id);
		//load view for editing the Asset Identity details.
		return View::make("assets_mgmt.allotment.allot_details",$data);
	}
	public function deleteAssetAllot()
	{
		$asset_allot_id = Input::get('record_id');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deleteAssetAllot($asset_allot_id);
		//dd($deleted);
		if($deleted)
		{
			$item_detail_id = getNameBasedOnId("allotment",'item_detail_id',$asset_allot_id);
			DB::connection('assets_mgmt')->table('item_detail')->where('id',$item_detail_id)->update(array("allotted" => 0));
			// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => "allotment",
				'record_id'=>$asset_allot_id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
			echo "<div class='alert alert-success'>Asset Allotment Successfully Deleted</div>";
		}
	}

// ======================================================== End of Asset Allot Controller Functions ===================================================

// ======================================================== Asset Allotment Return Controller Functions ===================================================

	public function assetAllotmentReturnList()
	{
		$data['records'] = assetsMgmtModel::getAllAssetAllotmentReturn();
		if(Input::get('ajax') == 1)
		{
			return View::make('assets_mgmt.allotment_return.allot_list_ajax',$data);
		}
		else
		{
			//load view to show the result
			return View::make('assets_mgmt.allotment_return.allot_list',$data);
		}
	}

	public function loadAssetAllotmentReturnPage()
	{		
		$data['parentDeps'] = getDepartmentWhereIn();
		//load the Asset Identity form.
		return View::make("assets_mgmt.allotment_return.allot_form",$data);
	}

	public function insertAssetAllotmentReturn()
	{
		//dd($_POST);
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"general_dept"				=> "required",
			"sub_dept"					=> "required",
			"from_person"				=> "required",
			"item_detail_id"			=> "required",
			"allotment_date"			=> "required",
			"return_date"				=> "required",
			"sanction_by"				=> "required",
			"received_by"				=> "required"
			));

		if($validates->fails())
		{	
			echo "error";
		}
		else
		{
			$allotment_date = convertToGregorian(ymd_format(Input::get('allotment_date')));
			$return_date = convertToGregorian(ymd_format(Input::get('return_date')));
			$item_detail_id = Input::get('item_detail_id');
			$allotment_return_data = array(
				'general_dept' 				=> Input::get("general_dept"),
				'sub_dept' 					=> Input::get('sub_dept'),
				'from_person' 				=> Input::get('from_person'),
				'allotment_number' 			=> Input::get('allotment_number'),
				'allotment_date' 			=> $allotment_date,
				'item_detail_id' 			=> $item_detail_id,
				'sanction_by' 				=> Input::get('sanction_by'),
				'return_date' 				=> $return_date,
				'received_by' 				=> Input::get('received_by'),
				'remarks' 					=> Input::get('remarks'),
				'user_id' 					=> Auth::user()->id,
				'updated_at' 				=> date('Y-m-d H:i:s')
			);
			$inserted = assetsMgmtModel::addAssetAllotmentReturnData($allotment_return_data);
			if($inserted){
				// now update the alloted field of the item detail table.
				DB::connection("assets_mgmt")->table("item_detail")->where("id",$item_detail_id)->update(array('allotted' => 0));

                // Now insert the notification details.
				$notification = array(
					'subject' => "New Allotment Return added",
					'table_name' => "allotment_return",
					'table_id' => $inserted,
					'user_id' => Auth::user()->username,
					'created_at' => date('Y-m-d H:i:s')
				);
				assetsMgmtModel::addNotification($notification);

				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'allotment_return',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Allotment Return Details Successfully Added / معلومات تسلیمی جنس موفقانه در سیستم اضافه گردید</div>";
			}
			else
			{
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer / مشکل وجود دارد، لطفآ با مسئول سیستم به تماس شوید</div>";
			}
		}
	}
	public function loadAssetAllotmentReturnEditPage()
	{
		$asset_allotment_return_id = Input::get("asset_allotment_return_id");
		$data['parentDeps'] = getDepartmentWhereIn();
		$data["row"] = assetsMgmtModel::getAllotmentReturnDetails($asset_allotment_return_id);
		//dd($data['row']);
		//load view for editing the Asset Identity details.
		return View::make("assets_mgmt.allotment_return.allot_edit_form",$data);
	}
	public function updateAssetAllotmentReturn()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"general_dept"				=> "required",
			"sub_dept"					=> "required",
			"from_person"				=> "required",
			"item_detail_id"			=> "required",
			"allotment_date"			=> "required",
			"return_date"				=> "required",
			"sanction_by"				=> "required",
			"received_by"				=> "required"
		));
		
		if($validates->fails())
		{	
			echo "error";
		}
		else
		{
			$allotment_return_id = Input::get("allotment_return_id");
			$allotment_date = convertToGregorian(ymd_format(Input::get('allotment_date')));
			$return_date = convertToGregorian(ymd_format(Input::get('return_date')));
			$item_detail_id = Input::get('item_detail_id');
			$allotment_return_data = array(
				'general_dept' 				=> Input::get("general_dept"),
				'sub_dept' 					=> Input::get('sub_dept'),
				'from_person' 				=> Input::get('from_person'),
				'allotment_number' 			=> Input::get('allotment_number'),
				'allotment_date' 			=> $allotment_date,
				'item_detail_id' 			=> $item_detail_id,
				'sanction_by' 				=> Input::get('sanction_by'),
				'return_date' 				=> $return_date,
				'received_by' 				=> Input::get('received_by'),
				'remarks' 					=> Input::get('remarks'),
				'user_id' 					=> Auth::user()->id,
				'updated_at' 				=> date('Y-m-d H:i:s')
			);
			$updated = assetsMgmtModel::updateAllotmentReturnData($allotment_return_id,$allotment_return_data);
			//dd($updated);
			if($updated){
				
				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'allotment_return',
					'record_id'=>$allotment_return_id,
					'user' => Auth::user()->username,
					'action' => "Updated",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Allotment Return Details Successfully Updated / معلومات موفقانه تجدید گردید</div>";
			}
			else
			{
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer / مشکل وجود دارد، لطفآ همرای مسئول سیستم به تماس شوید</div>";
			}
		}
	}
	// this function is called from viewTheNotification function as well with the parameter set to it.
	public function allotmentReturnDetails($allotment_return_id=0)
	{
		$allotment_return_id = Crypt::decrypt($allotment_return_id);
		$data['parentDeps'] = getDepartmentWhereIn();
		$data["row"] = assetsMgmtModel::getAssetAllotmentReturnDetails($allotment_return_id);
		//load view for editing.
		return View::make("assets_mgmt.allotment_return.allot_details",$data);
	}
	public function deleteAssetAllotmentReturn()
	{
		$allotment_return_id = Input::get('record_id');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deleteAssetAllotmentReturn($allotment_return_id);
		//dd($deleted);
		if($deleted)
		{
			// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => "allotment_return",
				'record_id'=>$allotment_return_id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
			echo "<div class='alert alert-success'>Allotment Return Successfully Deleted / موفقانه حذف گردید</div>";
		}
	}

// ======================================================== End of Asset Allotment Return Controller Functions ===================================================

// ======================================================== Asset Allotment to Storekeeper Controller Functions ===================================================

	public function assetAllotmentToStoreKeeperList()
	{
		$data['records'] = assetsMgmtModel::getAllAssetAllotmentToStoreKeeper();
		if(Input::get('ajax') == 1)
		{
			return View::make('assets_mgmt.allotment_to_storekeeper.allot_list_ajax',$data);
		}
		else
		{
			//load view to show the result
			return View::make('assets_mgmt.allotment_to_storekeeper.allot_list',$data);
		}
	}

	public function loadAssetAllotmentToStoreKeeperPage()
	{		
		$data['parentDeps'] = getDepartmentWhereIn();
		return View::make("assets_mgmt.allotment_to_storekeeper.allot_form",$data);
	}

	public function insertAssetAllotmentToStoreKeeper()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"allotment_number"				=> "required",
			"storekeeper"					=> "required",
			"item_detail_id"				=> "required",
			"allotment_return_date"			=> "required",
			"sanction_by"					=> "required"
			));

		if($validates->fails())
		{	
			echo "error";
		}
		else
		{
			$allotment_return_date = convertToGregorian(ymd_format(Input::get('allotment_return_date')));
			$item_detail_id = Input::get('item_detail_id');
			$allotment_to_storekeeper_data = array(
				'allotment_number' 				=> Input::get('allotment_number'),
				'storekeeper' 					=> Input::get('storekeeper'),
				'item_detail_id' 				=> $item_detail_id,
				'sanction_by' 					=> Input::get('sanction_by'),
				'allotment_return_date' 		=> $allotment_return_date,
				'remarks' 						=> Input::get('remarks'),
				'user_id' 						=> Auth::user()->id,
				'created_at' 					=> date('Y-m-d H:i:s')
			);
			$inserted = assetsMgmtModel::addAssetAllotmentToStoreKeeperData($allotment_to_storekeeper_data);
			if($inserted){
				// now update the alloted field of the item detail table.
				DB::connection("assets_mgmt")->table("item_detail")->where("id",$item_detail_id)->update(array('allotted' => 1));

                // Now insert the notification details.
				$notification = array(
					'subject' => "New Allotment To StoreKeeper added",
					'table_name' => "allotment_to_storekeeper",
					'table_id' => $inserted,
					'user_id' => Auth::user()->username,
					'created_at' => date('Y-m-d H:i:s')
				);
				assetsMgmtModel::addNotification($notification);

				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'allotment_to_storekeeper',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Allotment to Storekeeper Details Successfully Added / معلومات موفقانه در سیستم اضافه گردید</div>";
			}
			else
			{
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer / مشکل وجود دارد، لطفآ با مسئول سیستم به تماس شوید</div>";
			}
		}
	}
	public function loadAssetAllotmentToStoreKeeperEditPage()
	{
		$allotment_to_storekeeper_id = Input::get("allotment_to_storekeeper_id");
		$data["row"] = assetsMgmtModel::getAllotmentToStorekeeperDetails($allotment_to_storekeeper_id);
		return View::make("assets_mgmt.allotment_to_storekeeper.allot_edit_form",$data);
	}
	public function updateAssetAllotmentToStoreKeeper()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"allotment_number"				=> "required",
			"storekeeper"					=> "required",
			"item_detail_id"				=> "required",
			"allotment_return_date"			=> "required",
			"sanction_by"					=> "required"
			));

		if($validates->fails())
		{	
			echo "error";
		}
		else
		{
			$allotment_return_date = convertToGregorian(ymd_format(Input::get('allotment_return_date')));
			$allotment_to_storekeeper_id = Input::get('allotment_to_storekeeper_id');
			$item_detail_id = Input::get('item_detail_id');
			$allotment_to_storekeeper_data = array(
				'allotment_number' 				=> Input::get('allotment_number'),
				'storekeeper' 					=> Input::get('storekeeper'),
				'item_detail_id' 				=> $item_detail_id,
				'sanction_by' 					=> Input::get('sanction_by'),
				'allotment_return_date' 		=> $allotment_return_date,
				'remarks' 						=> Input::get('remarks'),
				'user_id' 						=> Auth::user()->id,
				'updated_at' 					=> date('Y-m-d H:i:s')
			);
			$updated = assetsMgmtModel::updateAllotmentToStoreKeeperData($allotment_to_storekeeper_id,$allotment_to_storekeeper_data);
			if($updated){
				
				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'allotment_to_storekeeper',
					'record_id'=>$allotment_to_storekeeper_id,
					'user' => Auth::user()->username,
					'action' => "Updated",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Allotment to Storekeeper Details Successfully Updated / معلومات موفقانه تجدید گردید</div>";
			}
			else
			{
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer / مشکل وجود دارد، لطفآ همرای مسئول سیستم به تماس شوید</div>";
			}
		}
	}
	// this function is called from viewTheNotification function as well with the parameter set to it.
	public function allotmentToStoreKeeperDetails($allotment_to_storekeeper_id=0)
	{
		$allotment_to_storekeeper_id = Crypt::decrypt($allotment_to_storekeeper_id);
		$data["row"] = assetsMgmtModel::getAllotmentToStorekeeperDetails($allotment_to_storekeeper_id);
		return View::make("assets_mgmt.allotment_to_storekeeper.allot_details",$data);
	}
	public function deleteAssetAllotmentToStoreKeeper()
	{
		$allotment_to_storekeeper_id = Input::get('record_id');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deleteAssetAllotmentToStoreKeeper($allotment_to_storekeeper_id);
		//dd($deleted);
		if($deleted)
		{
			$item_detail_id = getNameBasedOnId("allotment_to_storekeeper",'item_detail_id',$allotment_to_storekeeper_id);
			DB::connection('assets_mgmt')->table('item_detail')->where('id',$item_detail_id)->update(array("allotted" => 0));
			// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => "allotment_to_storekeeper",
				'record_id'=>$allotment_to_storekeeper_id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
			echo "<div class='alert alert-success'>Allotment to Storekeeper Successfully Deleted / موفقانه حذف گردید</div>";
		}
	}

// ======================================================== End of Asset Allotment to Storekeeper Controller Functions ===================================================

// ======================================================== Asset Allotment to Individual Controller Functions ===================================================

	public function assetAllotmentToIndividualList()
	{
		$data['records'] = assetsMgmtModel::getAllAssetAllotmentToIndividual();
		if(Input::get('ajax') == 1)
		{
			return View::make('assets_mgmt.allotment_to_individual.allot_list_ajax',$data);
		}
		else
		{
			//load view to show the result
			return View::make('assets_mgmt.allotment_to_individual.allot_list',$data);
		}
	}

	public function loadAssetAllotmentToIndividualPage()
	{		
		$data['parentDeps'] = getDepartmentWhereIn();
		return View::make("assets_mgmt.allotment_to_individual.allot_form",$data);
	}

	public function insertAssetAllotmentToIndividual()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"general_dept"				=> "required",
			"sub_dept"					=> "required",
			"allottee_name"				=> "required",
			"item_detail_id"			=> "required",
			"allotment_date"			=> "required",
			"sanction_by"				=> "required"
			));

		if($validates->fails())
		{	
			echo "error";
		}
		else
		{
			//dd($_POST);
			$allotment_date = convertToGregorian(ymd_format(Input::get('allotment_date')));
			$item_detail_id = Input::get('item_detail_id');
			$allotment_to_individual_data = array(
				'general_dept' 				=> Input::get("general_dept"),
				'sub_dept' 					=> Input::get('sub_dept'),
				'allottee_name' 			=> Input::get('allottee_name'),
				'allottee_designation' 		=> Input::get('allottee_designation'),
				'item_detail_id' 			=> $item_detail_id,
				'allotment_number' 			=> Input::get('allotment_number'),
				'allotment_date' 			=> $allotment_date,
				'sanction_by' 				=> Input::get('sanction_by'),
				'remarks' 					=> Input::get('remarks'),
				'user_id' 					=> Auth::user()->id,
				'created_at' 				=> date('Y-m-d H:i:s')
			);
			$inserted = assetsMgmtModel::addAssetAllotmentToIndividualData($allotment_to_individual_data);
			if($inserted){
				// now update the alloted field of the item detail table.
				DB::connection("assets_mgmt")->table("item_detail")->where("id",$item_detail_id)->update(array('allotted' => 1));

                // Now insert the notification details.
				$notification = array(
					'subject' => "New Allotment To Individual added",
					'table_name' => "allotment_to_individual",
					'table_id' => $inserted,
					'user_id' => Auth::user()->username,
					'created_at' => date('Y-m-d H:i:s')
				);
				assetsMgmtModel::addNotification($notification);

				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'allotment_to_individual',
					'record_id'=>$inserted,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Allotment to Individual Details Successfully Added / معلومات موفقانه در سیستم اضافه گردید</div>";
			}
			else
			{
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer / مشکل وجود دارد، لطفآ با مسئول سیستم به تماس شوید</div>";
			}
		}
	}
	public function loadAssetAllotmentToIndividualEditPage()
	{
		$allotment_to_individual_id = Input::get("allotment_to_individual_id");
		$data['parentDeps'] = getDepartmentWhereIn();
		$data["row"] = assetsMgmtModel::getAllotmentToIndividualDetails($allotment_to_individual_id);
		return View::make("assets_mgmt.allotment_to_individual.allot_edit_form",$data);
	}
	public function updateAssetAllotmentToIndividual()
	{
		//validate the input fields
		$validates = Validator::make(Input::all(), array(
			"general_dept"				=> "required",
			"sub_dept"					=> "required",
			"allottee_name"				=> "required",
			"item_detail_id"			=> "required",
			"allotment_date"			=> "required",
			"sanction_by"				=> "required"
		));

		if($validates->fails())
		{	
			echo "error";
		}
		else
		{
			//dd($_POST);
			$allotment_date = convertToGregorian(ymd_format(Input::get('allotment_date')));
			$allotment_to_individual_id = Input::get('allotment_to_individual_id');
			$item_detail_id = Input::get('item_detail_id');
			$allotment_to_individual_data = array(
				'general_dept' 				=> Input::get("general_dept"),
				'sub_dept' 					=> Input::get('sub_dept'),
				'allottee_name' 			=> Input::get('allottee_name'),
				'allottee_designation' 		=> Input::get('allottee_designation'),
				'item_detail_id' 			=> $item_detail_id,
				'allotment_number' 			=> Input::get('allotment_number'),
				'allotment_date' 			=> $allotment_date,
				'sanction_by' 				=> Input::get('sanction_by'),
				'remarks' 					=> Input::get('remarks'),
				'user_id' 					=> Auth::user()->id,
				'updated_at' 				=> date('Y-m-d H:i:s')
			);
			$updated = assetsMgmtModel::updateAllotmentToIndividualData($allotment_to_individual_id,$allotment_to_individual_data);
			if($updated){
				
				// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'allotment_to_individual',
					'record_id'=>$allotment_to_individual_id,
					'user' => Auth::user()->username,
					'action' => "Updated",
					'created_at' => date('Y-m-d H:i:s')
					);
				assetsMgmtModel::addLog($Log);
				echo "<div class='alert alert-success'>Allotment to Individual Details Successfully Updated / معلومات موفقانه تجدید گردید</div>";
			}
			else
			{
				echo "<div class='alert alert-danger'>Something was wrong, please consult with system developer / مشکل وجود دارد، لطفآ همرای مسئول سیستم به تماس شوید</div>";
			}
		}
	}
	// this function is called from viewTheNotification function as well with the parameter set to it.
	public function allotmentToIndividualDetails($allotment_to_individual_id=0)
	{
		$allotment_to_individual_id = Crypt::decrypt($allotment_to_individual_id);
		$data['parentDeps'] = getDepartmentWhereIn();
		$data["row"] = assetsMgmtModel::getAllotmentToIndividualDetails($allotment_to_individual_id);
		return View::make("assets_mgmt.allotment_to_individual.allot_details",$data);
	}
	public function deleteAssetAllotmentToIndividual()
	{
		$allotment_to_individual_id = Input::get('record_id');
		//$id = Crypt::decrypt($id);
		$deleted = assetsMgmtModel::deleteAssetAllotmentToIndividual($allotment_to_individual_id);
		//dd($deleted);
		if($deleted)
		{
			$item_detail_id = getNameBasedOnId("allotment_to_individual",'item_detail_id',$allotment_to_individual_id);
			DB::connection('assets_mgmt')->table('item_detail')->where('id',$item_detail_id)->update(array("allotted" => 0));
			// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => "allotment_to_individual",
				'record_id'=>$allotment_to_individual_id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			assetsMgmtModel::addLog($Log);
			echo "<div class='alert alert-success'>Allotment to Individual Successfully Deleted / موفقانه حذف گردید</div>";
		}
	}

// ======================================================== End of Asset Allotment to Individual Controller Functions ===================================================

}

?>