<?php namespace App\Http\Controllers;

use Redirect;
use Illuminate\Http\Request;
use Auth;
use Input;
use Session;
use DB;
use LaravelGettext;
use Saml2;
use Mail;
use Crypt; 
use Hash;
use App\models\User; 
use App\models\hr\Employee; 
use App\models\hr\UserRequestLog; 

        
class LoginController extends NoLoginController {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function getLogin()
	{
		//return Saml2::login();
		return view('auth.user.login');
	}
	public function postLogin(Request $request)
	{
			
			$validates = $this->validate($request,array(
		        "username"  => "required",
		        "password"  => "required"
		    ));
		   
		    //check the validation
		    if($validates)
		    {
		        return \Redirect::route("getLogin")->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        $remember = ($request->has('remember')) ? true:false;
		        
		        $auth = Auth::attempt(array(
		            "username" => $request->get("username"),
		            "password" => $request->get("password"),
		            "status"   => "1"
		        ),$remember);
		        /**
		        *Check if login success and then set the roles
		        */
		        if($auth)
		        {   
		        	
		        	//call function helper to login in webmail
		        	//loginWebmail(); 
				    $last_locale = DB::table("user_session")
				    				->where('user_id',Auth::user()->id)
				    				->where('session_type','lang')
				    				->pluck('session_value');
				   
				    //get date setting
					$date_setting = DB::table("user_session")
				    				->where('user_id',Auth::user()->id)
				    				->where('session_type','date')
				    				->pluck('session_value');
				    
				    Session::put('date_setting',$date_setting);
				    
				    if($last_locale != NULL && $last_locale != '' && $last_locale != false)
				    {
				    	LaravelGettext::setLocale($last_locale);
				    	Session::put('lang',getLangShortcut());
				    }	
				    else
				    {		
			        	LaravelGettext::setLocale("en_US");
			        	Session::put('lang',getLangShortcut());
			        }


		        	//set lang session
		        	//Session::put('lang','en');
		        	//set roles to session
		        	if(setSessionRoles())
		        	{
		        		return \Redirect::intended("/")->with("success","You are logged in successfuly.");
		        	}
		        	else
		        	{
		        		return \Redirect::intended("/")->with("fail","Access denied!.");
		        	}
		            
		            
		        }
		        else
		        {   
		            return \Redirect::route("getLogin")->with("fail","یوزر یا پسورد شما اشتباه میباشد");
		        }
            }
            
            
    }
    
    /** 
     * @Author: Jamal & Ehsan  
    * @Date: 2019-09-04 14:30:25 
    * @Desc: Create Account  
	*/ 
	
    public function createAccount()
    {
        return view('auth.user.account.index'); 
	}

	public function postCreateAccount(Request $request)
	{
        $email = $request->email; 
        $employee = Employee::where('email',$email)->first(); 
        $user = false;
        if($employee){
            $user = User::where('employee_id',$employee->id)->first(); 
        }
        // echo "<pre>"; print_r($employee); exit;
        $message = ''; 
		if($user)
		{
            return \Redirect::route("create-account")->with("fail","با این ایمیل آدرس اکونت از قبل موجود است، برای بازیابی اکونت خود رمز خویش را تغیر دهید.");
			//$message = 'با این ایمیل آدرس اکونت از قبل موجود است، برای بازیابی اکونت خود رمز خویش را تغیر دهید.';  
		}
		else
		{
			if($employee)
			{	
				// Create new draft				
				$new_user = new UserRequestLog;
				$new_user->logip = \Request::ip();
				$new_user->agent = $request->header('user-agent');
				$new_user->token = Crypt::encrypt($email);
				$new_user->email = $email;
				$new_user->employee_id = $employee->id;
				$record = $new_user->save();
				if($record)
				{
                    // Send link through email
					$user_email = array('email'=>$email,'name'=>$employee->name_dr,'last_name'=>$employee->last_name);
					Mail::send('emails.account_request', $user_email, function($message) use ($user_email){
								$message->from('mis.user@aop.gov.af', 'AOP Attendance Department');
								$message->to($user_email['email'])->subject('ایجاد اکونت');
                            });
                    
                    return \Redirect::route("create-account")->with("success","لینک ایجاد اکونت در ایمیل شما ارسال گردید،‌ لطفاً ایمیل خویش را چک نمائید.");
                    //$message = 'لینک ایجاد اکونت در ایمیل شما ارسال گردید،‌ لطفاً ایمیل خویش را چک نمائید.';	
                }
			}
			else
			{
                return \Redirect::route("create-account")->with("fail","ایمیل شما ثبت سیستم نمیباشد،‌ برای معلومات بیشتر به آمریت استخدام ریاست منابع بشری به تماس شوید.");
				//$message = 'ایمیل شما ثبت سیستم نمیباشد،‌ برای معلومات بیشتر به آمریت استخدام ریاست منابع بشری به تماس شوید.'; 
			}

		}
	}

	public function getAccountRegister($email="")
	{
        # GET EMAIL...
        $email = Crypt::decrypt($email); 
		$record = UserRequestLog::where('email',$email)->first();
		$data['record'] = $record;
		//check if log exist
		if($record)
		{
			$employee = Employee::where('email',$email)->first();
            $data['employee'] = $employee;
            //print_r($employee->department_data); exit;
            $data['email'] = $email;
			return view('auth.user.account.register',$data); 
		}
		
    }
    
     /** 
     * @Author: Jamalludin Yousufi  
     * @Date: 2019-09-11 16:03:59 
     * @Desc:  
     */    
    public function storeAccount(Request $request)
	{
    
        //validate the input fields
        $username = Crypt::decrypt($request->username);
        $validates = \Validator::make($request->except(['username']),array(
            "password"    		=> "required|min:6",
            "confirm_password"  => "required|same:password",
        ));
        
        //check the validation
        if($validates->fails())
        {                    
            return redirect()->back()->withErrors($validates)->withInput();
        }
        else
        {
            // Check if user is alredy exist in user table
            $account = User::where('username',$username)->orWhere('email',$username)->first();
            if(!$account)
            {
                // Check if user is alredy exist in user table
                $employee = Employee::where('email',$username)->first();
                //dd($request->all()); 
                $user = new User();
                $user->first_name 		= $employee->name_dr;
                $user->last_name 		= $employee->last_name;
                $user->father_name 		= $employee->father_name_dr;
                $user->email 			= $username;
                $user->department_id 	= $employee->department;
                $user->username 		= $username;
                $user->employee_id 		= $employee->id;
                $user->password 		= Hash::make(Input::get("password"));
                $user->status 			= '1';
                $user->photo 			= ($employee->photo==''?'/img/default.jpeg':$employee->photo);
                $user->is_admin         = '0';
                $user->is_manager       = '0';
                
                if($user->save())
                {
                    $userId = $user->id;
                    //user modules ---------------------------
                    $modData = array(
                        'module_id' => '6',
                        'user_id'   => $userId
                    );
                    //insert user modules
                    User::insertUserRelated('user_module',$modData);
                   
                    //user entities --------------------------
                    $allData = array(
                        'entity_id' => '52',
                        'user_id'	=> $userId
                    );
                    //insert user entities
                    User::insertUserRelated('user_entity',$allData);

                    //user roles--- --------------------------
                    $allData = array(
                        'role_id'   => '140',
                        'user_id'	=> $userId
                    );
                    //insert user roles
                    User::insertUserRelated('user_role',$allData);

                    //return \Redirect::route("getAllUsers")->with("success","You successfuly created user and now you can login.");
                    return \Redirect::route("getAccountRegister",$request->username)->with("success","You successfuly created user and now you can login.");
                }
                else
                {
                    //return \Redirect::route("getAllUsers")->with("fail","An error occured plase try again.");
                    return \Redirect::route("getAccountRegister",$request->username)->with("fail","An error occured plase try again.");
                }
            }
            else
            {
                return \Redirect::route("getAccountRegister",$request->username)->with("fail","با این ایمیل آدرس اکونت از قبل موجود است، برای بازیابی اکونت خود رمز خویش را تغیر دهید.");
            }

        }
    }

    /** 
     * @Author: Ehsan Mojadadi 
     * @Date: 2019-09-18 14:13:30 
     * @Desc:  
     */    
    public function forgotPasswordIndex()
    {
        return view('auth.user.account.forgot'); 
    }

    /** 
     * @Author: Ehsan Mojadadi 
     * @Date: 2019-09-18 14:13:30 
     * @Desc:  
     */    
    public function postChangePassword(Request $request)
    {
        $email = $request->email; 
        $employee = Employee::where('email',$email)->count();
        if($employee and $employee > 1)
        {
            return \Redirect::route("forgotPasswordIndex")->with("fail","با این ایمیل آدرس بیشتر از یک اکونت در سیستم ثبت گردیده است،‌ لطفاً با مدیر سیستم به تماس شوید.");
		}
		elseif($employee and $employee == 1)
		{
            $account = User::where('username',$email)->orWhere('email',$email)->count();

            // Create new draft				
            if($account and $account == 1)
            {
                $employee1 = Employee::where('email',$email)->first();
                // Send link through email
                $user_email = array('email'=>$email,'name'=>$employee1->name_dr,'last_name'=>$employee1->last_name);
                Mail::send('emails.change_password', $user_email, function($message) use ($user_email){
                            $message->from('mis.user@aop.gov.af', 'AOP Attendance Department');
                            $message->to($user_email['email'])->subject('تغیر پسورد');
                        });
                return \Redirect::route("forgotPasswordIndex")->with("success","لینک تغیر پسورد در ایمیل شما ارسال گردید،‌ لطفاً ایمیل خویش را چک نمائید.");
            }
            elseif($account and $account > 1)
            {
                return \Redirect::route("forgotPasswordIndex")->with("fail","با این ایمیل آدرس بیشتر از یک اکونت در سیستم ثبت گردیده است،‌ لطفاً با مدیر سیستم به تماس شوید.");
            }
            else
            {
                return \Redirect::route("forgotPasswordIndex")->with("fail","ایمیل شما ثبت سیستم نمیباشد،‌ برای معلومات بیشتر به آمریت استخدام ریاست منابع بشری به تماس شوید.");
            }
        }
        else
        {
            return \Redirect::route("forgotPasswordIndex")->with("fail","ایمیل شما ثبت سیستم نمیباشد،‌ برای معلومات بیشتر به آمریت استخدام ریاست منابع بشری به تماس شوید.");
        }
	}

    public function getResetPassword($email="")
	{
        # GET EMAIL...
        $email = Crypt::decrypt($email); 
		$employee = Employee::where('email',$email)->first();
        $data['employee'] = $employee;
        //print_r($employee->department_data); exit;
        $data['email'] = $email;
        return view('auth.user.account.reset',$data);
		
    }

    public function resetPassword(Request $request)
    {
    
        //validate the input fields
        $username = Crypt::decrypt($request->username);
        $validates = $this->validate($request,array(
            "password"    		=> "required|min:6",
            "confirm_password"  => "required|same:password",
        ));
        
        // Check if user is alredy exist in user table
        $user = User::where('username',$username)->orWhere('email',$username)->first();
        if($user)
        {
            $password = Input::get("password");
            // echo $username; exit;
            $user->username = $username;
            $user->password = Hash::make($password);
            if($user->save())
            {
                $employee = Employee::where('email',$username)->first();
                // Send link through email
                $user_email = array('email'=>$username,'name'=>$employee->name_dr,'last_name'=>$employee->last_name,'password'=>$password);
                Mail::send('emails.confirm_password', $user_email, function($message) use ($user_email){
                            $message->from('mis.user@aop.gov.af', 'AOP Attendance Department');
                            $message->to($user_email['email'])->subject('مشخصات اکونت سیستم HRMIS');
                });
                return \Redirect::route("getResetPassword",$request->username)->with("success","پسورد شما تجدید گردید،‌ یوزر و پسورد به ایمل شما ارسال شد.");
            }
            else
            {
                return \Redirect::route("getResetPassword",$request->username)->with("fail","عملیه موفقانه انجام نشد، لطفاً دوباره کوشش نمائید.");
            }
        }
        else
        {
            return \Redirect::route("getResetPassword",$request->username)->with("fail","با این ایمیل آدرس اکونت از قبل موجود است، برای بازیابی اکونت خود رمز خویش را تغیر دهید.");
        }        
    }

}
