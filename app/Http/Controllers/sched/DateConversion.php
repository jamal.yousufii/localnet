<?php

namespace App\Http\Controllers\sched;
use App\Http\Controllers\Controller;
//use View;
use Auth;
use Illuminate\Http\Request;
use App\models\sched\Meeting;
use App\models\sched\ManageSpecialDays;
use App\library\jdatetime;
use View;
use DB;
use Validator;
use Input;
use Redirect;
use Datatables;
use DateTime;
use DateInterval;
use URL;
use Carbon\Carbon;

class DateConversion extends Controller
{

	/**
     	* Instantiate a new UserController instance.
     	*/
	public function __construct()
	{
		if(!Auth::check())
		{
			return Redirect::route('getLogin');
		}		
	}	


	public function convertToJalali()
    {

        $gregorian_date = explode("-", Input::get("date"));
        //print_r($gregorian_date);exit;
        $g_y = $gregorian_date[0];
        $g_m = $gregorian_date[1];
        $g_d = $gregorian_date[2];
        //echo $g_d."-".$g_m."-".$g_y;
        // calling the function in Dateconverter library to change the gregorian date to jalali.
        $jalali_date = jdatetime::toJalali($g_y, $g_m, $g_d);
        //$jalali_numbers_date = jdatetime::convertNumbers($jalali_date);
        //print_r($jalali_numbers);exit;
        $j_y = $jalali_date[0];
        $j_m = $jalali_date[1];
        $j_d = $jalali_date[2];
        
        $jalali_date = $j_d."-".$j_m."-".$j_y;
        echo $jalali_date;
        //print_r($jalali_date);
        //$jalali_date = date("d-m-Y", strtotime($jalali_date));

        //echo $jalali_date;

    }

    // find the gregorian day from given date and convert it to jalali week day.
    public function getJalaliWeekDay()
    {

        $date = Input::get("date");
        $timestamp = strtotime($date);
        $day = date('D', $timestamp);
        
        echo $dayname = jdatetime::getDayNames($day);
    }

    // find the gregorian day from given date and convert it to jalali week day.
    public function getCurrentWeekDay()
    {

        $date = Input::get("date");
        $date = gregorian_format($date);
        $date = convertToGregorian($date);
        
        $timestamp = strtotime($date);
        $day = date('D', $timestamp);
        
        echo $dayname = jdatetime::getDayNames($day);
        // send the number value to the response to set the selected value of the select box of the day selection.
        // switch ($dayname) {
        //     case 'شنبه':
        //         echo $dayname = 1;
        //         break;
        //     case 'یکشنبه':
        //         echo $dayname = 2;
        //         break;
        //     case 'دوشنبه':
        //         echo $dayname = 3;
        //         break;
        //     case 'سه شنبه':
        //         echo $dayname = 4;
        //         break;
        //     case 'چهارشنبه':
        //         echo $dayname = 5;
        //         break;
        //     case 'پنجشنبه':
        //         echo $dayname = 6;
        //         break;
            
        //     default:
        //         echo $dayname = 7;
        //         break;
        // }
    }

    public function getJalaliMonthName()
    {

        $date = Input::get("date");
        $timestamp = strtotime($date);
        $day = date('D', $timestamp);
        
        $dayname = jdatetime::getDayNames($day);
        //$date = Input::get("date");
        $gregorian_date = explode("-", Input::get("date"));
        //print_r($gregorian_date);exit;
        $g_y = $gregorian_date[0];
        $g_m = $gregorian_date[1];
        $g_d = $gregorian_date[2];
        //echo $g_d."-".$g_m."-".$g_y;
        // calling the function in Dateconverter library to change the gregorian date to jalali.
        $jalali_date = jdatetime::toJalali($g_y, $g_m, $g_d);
        $j_y = $jalali_date[0];
        $j_m = $jalali_date[1];
        $j_d = $jalali_date[2];
        // add leading zero to month and day of the date.
        $j_m_pad = str_pad($j_m,2,0, STR_PAD_LEFT);
        $j_d_pad = str_pad($j_d,2,0, STR_PAD_LEFT);

        $g_date_numbers = array($j_y, $j_m_pad, $j_d_pad);

        $jalali_numbers_date = jdatetime::convertNumbers($g_date_numbers);
        
        //print_r($jalali_numbers);exit;
        $j_y = $jalali_numbers_date[0];
        $j_m = $jalali_numbers_date[1];
        $j_d = $jalali_numbers_date[2];
        
        //echo $j_d."-".$j_m."-".$j_y;

        $date = Input::get("date");
        $timestamp = strtotime($date);
        $g_monthName = date('F', $timestamp);
        
        $j_monthName = jdatetime::getMonthNames($j_m);

        echo "<h4 align='center' dir='rtl' style='margin-top:20px' class='title'>".$dayname." ".$j_d." ".$j_monthName." ".$j_y." مطابق <i dir='ltr'>".$g_y." ".$g_monthName." ".$g_d."</i></h4>";

    }

    public function getMonthDifference()
    {

        $start_date = Input::get("start_date");
        $end_date = Input::get("end_date");
        
        $j_start_date = explode("-", $start_date);

        $g_y = $j_start_date[2];
        $g_m = $j_start_date[1];
        $g_d = $j_start_date[0];

        $j_end_date = explode("-", $end_date);

        $g_y_end = $j_end_date[2];
        $g_m_end = $j_end_date[1];
        $g_d_end = $j_end_date[0];

        // calling the function in jdatetime library to change the jalali date to gregorian.
        $g_start_date = jdatetime::toGregorian($g_y, $g_m, $g_d);

        $grg_y = $g_start_date[0];
        $grg_m = $g_start_date[1];
        $grg_d = $g_start_date[2];

        $gregorian_start_date = $grg_y."-".$grg_m."-".$grg_d;
        
        // get gregorian month name.
        $timestamp = strtotime($gregorian_start_date);
        $g_s_monthName = date('F', $timestamp);

        $g_end_date = jdatetime::toGregorian($g_y_end, $g_m_end, $g_d_end);

        $grg_y_end = $g_end_date[0];
        $grg_m_end = $g_end_date[1];
        $grg_d_end = $g_end_date[2];

        $gregorian_end_date = $grg_y_end."-".$grg_m_end."-".$grg_d_end;
        // get gregorian month name.
        $timestamp = strtotime($gregorian_end_date);
        $g_e_monthName = date('F', $timestamp);

        // calling the function in jdatetime library to change the gregorian numbers to jalali.
        $start_date_jalali_numbers = jdatetime::convertNumbers($start_date);
        $end_date_jalali_numbers = jdatetime::convertNumbers($end_date);

        $start_date_jalali_numbers = explode("-", $start_date_jalali_numbers);
        $end_date_jalali_numbers = explode("-", $end_date_jalali_numbers);
        //print_r($start_date_jalali_numbers);exit;
        $j_y = $start_date_jalali_numbers[2];
        $j_m = $start_date_jalali_numbers[1];
        $j_d = $start_date_jalali_numbers[0];
        
        // $timestamp = strtotime($start_date_jalali_numbers);
        // $s_month = date('m', $timestamp);
        // echo $start_date_month = jdatetime::convertNumbers($s_month);

        // $timestamp = strtotime($end_date_jalali_numbers);
        // $e_month = date('m', $timestamp);
        // echo $end_date_month = jdatetime::convertNumbers($e_month);

        $e_j_y = $end_date_jalali_numbers[2];
        $e_j_m = $end_date_jalali_numbers[1];
        $e_j_d = $end_date_jalali_numbers[0];
        //echo $j_d."-".$j_m."-".$j_y;

        $start_date_monthName = jdatetime::getMonthNames($j_m);
        $end_date_monthName = jdatetime::getMonthNames($e_j_m);

        echo "<h4 align='center' dir='rtl' style='margin-top:20px' class='title'>( ".$j_d." ".$start_date_monthName." - ".$e_j_d." ".$end_date_monthName." ".$j_y." ) مطابق <i dir='ltr'> ( ".$grg_y_end." ".$g_e_monthName." ".$grg_d_end." - ".$g_s_monthName." ".$grg_d." ) </i></h4>";

    }

}

?>