<?php 

namespace App\Http\Controllers\sched;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\models\sched\ManageSpecialDays;
use App\library\jdatetime;
use View;
use DB;
use Validator;
use Input;
use Redirect;
use Datatables;
use DateTime;
use DateInterval;
use URL;
use Carbon\Carbon;
//use Dateconverter;

class SpecialDays extends Controller
{

	/**
     	* Instantiate a new UserController instance.
     	*/
	public function __construct()
	{
		if(!Auth::check())
		{
			return Redirect::route('getLogin');
		}		
	}	


	// Manage special days.
    public function loadView()
    {
        return View::make('sched.specialDays');
    }

    // now add the special days into it's table.
    public function add_special_days(Request $request)
    {
        //print_r($_POST);exit;
        //validate the input fields
        $this->validate($request,[
            "title"     => "required",
            //"meeting_end"     => "required",
            "date"      => "required",
            ]
        );

        $date = convertToGregorian(gregorian_format(Input::get('date')));

        // get the last inserted records to check if the duplicate date is not being inserted.
        $object = DB::connection('sched')->table('special_days')
                    ->select('date')->where('date', $date)->first();

        if($object != '')
        {
            if(strtotime($date) == strtotime($object->date))
            {
                return Redirect::route("SD_List")->with("fail","این روز در سیستم موجود است، روز خاص دیگری را اضافه کنید !");
            }
        }

        // get the the data and insert it into the table.
        $data = array(

                "date"              => $date,
                "title"             => Input::get('title'),
                "description"       => Input::get('description'),
                "type"              => Input::get('type'),
                "user_id"           => Auth::user()->id

        );

        //Insert Special days form data
        $object = ManageSpecialDays::insertFormData($data);

        if($object)
        {
            // get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'special_days',
				'record_id'=>$object,
				'user' => Auth::user()->username,
				'action' => "Inserted",
				'created_at'=>date('Y-m-d H:i:s')
				);
            //Insert Special days logged data
            ManageSpecialDays::addLogData($Log);
            return Redirect::route("SD_List")->with("success","موفقانه اضافه گردید");
        }
        else
        {
            return Redirect::route("SD_List")->with("fail","در اضافه کردن مشکل وجود دارد، لطفآ دوباره کوشش کنید");
        }

    }

    // load special edit view.
    public function loadSDEditView($id = 0)
    {
        $record = ManageSpecialDays::getSpicificSD($id);
        return View::make('sched.specialDaysEditForm')->with('record',$record);
    }

    // update the special day contents.
    public function update_special_day(Request $request,$id = 0)
    {
        //validate the input fields
        $this->validate($request,[
            "title"     => "required",
            //"meeting_end"     => "required",
            "date"      => "required",
            ]
        );

        $corrected_date_format = convertToGregorian(gregorian_format(Input::get('date')));
        // update the editted special day based on the id.
        $update = ManageSpecialDays::find($id);
        $update->date           = $corrected_date_format;
        $update->title          = Input::get('title');
        $update->description    = Input::get('description');
        $update->type           = Input::get('type');
        // if the particular meeting has been updated successfully then do the rest.
        if($update->save())
        {
            // get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'special_days',
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Updated",
				'created_at'=>date('Y-m-d H:i:s')
				);
            //Insert Special days logged data
            ManageSpecialDays::addLogData($Log);
            return Redirect::route("SD_List")->with("success","موفقانه اصلاح گردید");
        }
        else
        {
            return Redirect::route("SD_List")->with("fail","اصلاح نگردید، مشکل وجود دارد، لطفآ دوباره کوشش کنید");
        }
    }

    // delete special days.
    public function deleteSpecialDay($id = 0)
    {
       $deleted = ManageSpecialDays::deleteSpecialDay($id);
        
        if($deleted)
        {
        	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'special_days',
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
            //Insert Special days logged data
            ManageSpecialDays::addLogData($Log);	
            return Redirect::route("SD_List")->with("success","موفقانه حذف گردید , ID: <span style='color:red;font_weight:bold;'>$id</span>");         
        }
        else
        {
            return Redirect::route("SD_List")->with("fail","در حذف مشکل وجود دارد، لطفآ با مسئول دیتابیس در میان بگذارید");
        }
    }

    // load list of all special days;
    public function all_special_days()
    {
        $sd_list = ManageSpecialDays::getAllSDList();
        return View::make('sched.specialDays_list')->with('sd_list', $sd_list);
    }

    // check the special days whether the selected date is included or not.
    public function check_days()
    {

        //get the date sent by ajax.
        $date = Input::get('date');
        $date = convertToGregorian(gregorian_format($date));
        $result = ManageSpecialDays::checkSpecialDays($date);
        if(!$result == '')
        {

            foreach ($result as $row) {
                $d = $row->date;
                $title = $row->title;
                $desc = $row->description;
            }

            echo "<div style='padding: 50px' class='alert alert-danger'><p style='font-size: 18px;font-weight:bold;text-align:center'>روز خاص سال </p><br /> تاریخ : ".$d."<br /><br /> عنوان : ".$title."<br /><br /> شرح بیشتر : ".$desc."</div>";
        
        }
        else
        {
            echo "";
        }

    }

}

?>