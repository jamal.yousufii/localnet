<?php 

namespace App\Http\Controllers\sched;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\models\sched\Meeting;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use App\library\jdatetime;
use View;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use DateTime;
use DateInterval;
use URL;
use Crypt;
use Excel;
use Response;
use Carbon\Carbon;

class FormController extends Controller
{

	/**
     	* Instantiate a new UserController instance.
     	*/
	public function __construct()
	{
		if(!Auth::check())
		{
			return Redirect::route('getLogin');
		}		
	}

	//Load report form
	public function meetingForm()
	{
		$data['week_days'] = Meeting::getMeetingWeekDays();
		$data['locations'] = Meeting::getMeetingLocations();
		$data['sectors'] = Meeting::getMeetingSectors();
		$data['meeting_type'] = Meeting::getMeetingType();
		return View::make('sched.meeting_form', $data);
	}

	// Insert president meeting form data
	public function addMeetingForm(Request $request)
    {
		//validate the input fields
	    $this->validate($request,[
	        "meeting_start"		=> "required",
			"meeting_end"		=> "required",
            //"meeting_with" 		=> "required",
            // "meeting_title"         => "required",
			// "meeting_subject"	=> "required",
			// "week_day"			=> "required",
	        ]
	    );

		//print_r($_POST);exit;

		// get the start and end times.
		if(!Input::get('meeting_start') == '')
		{
			$startTime_format = DATE("H:i", STRTOTIME(Input::get('meeting_start')));
		}

		if(!Input::get('meeting_end') == '')
		{

			$endTime_format = DATE("H:i", STRTOTIME(Input::get('meeting_end')));
		
	    }
	    else
	    {
	    	$endTime_format = '';
	    }
	    
    	$today_date = convertToGregorian(gregorian_format(Input::get('date')));
    	$week_day = getDayNum(Input::get('week_day'));
		// get the last meeting (which is not recurring) end time based on today's date.
		$today_last_end_time = getMeetingLastEndTimeOfToday($today_date); 
		
		// check if the start time of new entry is not before the end time of the last meeting at the same date.
		if(strtotime($today_last_end_time) > strtotime($startTime_format))
		{
			//echo $today_last_end_time."<br />".$startTime_format;exit;
			return Redirect::route('MeetingForm')->with('failed', "لطفآ وقت آغاز جلسه را بعد از زمان ختم جلسه قبلی که $today_last_end_time است انتخاب کنید ! ");
			die();
		}

		// get the current time.
		$current_time = date("h:i");
		//get the current date in jalali.
		$day_date = date('Y-m-d');
		// check that the user is not entering the date before the current time on that day.
    	// if((strtotime($current_time) > strtotime($startTime_format)) && (strtotime($day_date) == strtotime($today_date)))
		// {
		// 	return Redirect::route('MeetingForm')->with('failed', "لطفآ وقت آغاز جلسه را بعد از وقت فعلی که $current_time است انتخاب کنید ! ");
		// 	die();
		// }

    	// check the recuring meetings whether it's daily, weekly or after number of weeks.
		$object = DB::connection("sched")->table('meeting_schedule')->select(array('meeting_start','meeting_end','recurring','recure_type'))
			->where('meeting_start', '=', $startTime_format)->where('meeting_end', '=', $endTime_format)->where("deleted",0)->get();
		//print_r($object);exit;
		if(!empty($object))
		{
			foreach ($object as $row) {
				if(($row->recurring == 1 && $row->recure_type == 1) || ($row->recure_type == 2 || $row->recure_type == 3))
				{
					// now check the meeting start and end time on this particular day.
					if((strtotime($row->meeting_start) <= strtotime($startTime_format)) && (strtotime($row->meeting_end) >= strtotime($endTime_format)))
					{
						return Redirect::route('MeetingForm')->with('failed', "یاداشت : این وقت برای جلسه که باید در هر روز تکرار گردد اختصاص داده شده است !");
						die();
					}
				}
			}
		}


		// check if in the particular date the start and end time of the meeting already exists.
		// if(checkRecordExistence('meeting_schedule', 'meeting_start', $startTime_format) && checkRecordExistence('meeting_schedule', 'meeting_end', $endTime_format))
		// {
		// 	return Redirect::route('MeetingForm')->with('failed', "لطفآ وقت آغاز جلسه را بعد $startTime_format و وقت ختم را بعد از $endTime_format انتخاب کنید ! ");
		// 	die();
		// }
		// check if in the particular date the start time of the meeting already exists.
		// elseif(checkRecordExistence('meeting_schedule', 'meeting_start', $startTime_format))
		// {
		// 	return Redirect::route('MeetingForm')->with('failed', "لطفآ وقت آغاز جلسه را بعد از $startTime_format انتخاب کنید ! ");
		// 	die();
		// }
		// check if in the particular date the end time of the meeting already exists.
		// elseif(checkRecordExistence('meeting_schedule', 'meeting_end', $endTime_format))
		// {
		// 	return Redirect::route('MeetingForm')->with('failed', "لطفآ وقت آغاز جلسه را بعد از $endTime_format انتخاب کنید ! ");
		// 	die();
		// }

		// check if the location is being selected by dropdown or manually.
		if(Input::get('manual_loc') != '')
		{
			// if the user wants to insert manual location then, insert it to the locations table as well.
			//check if the location does exist then don't insert it again, otherwise insert it.
			if(!checkRecordExistence('locations','name',Input::get('manual_loc')))
			{
				$manual_location = Input::get('manual_loc');
				$location = DB::connection("sched")
					->table('locations')
					->insertGetID(array('name' => $manual_location));
			}
			else{$location = getFieldValue("locations","id","name",Input::get('manual_loc'));}
		}
		else
		{
			if(Input::get('location') != '')
			{
				$location = Input::get('location');
			}
			else{$location = '';}
		}
		
		// check if the sector is being selected by dropdown or manually.
		if(Input::get('manual_sector') != '')
		{
			
			// if the user wants to insert manual sector then, insert it to the sectors table as well.
			//check if the sector does exist then don't insert it again, otherwise insert it.
			if(!checkRecordExistence('sectors','name',Input::get('manual_sector')))
			{
				$manual_sector = Input::get('manual_sector');
				$sector = DB::connection("sched")
					->table('sectors')
					->insertGetID(array('name' => $manual_sector));
			}
			else{$sector = getFieldValue("sectors","id","name",Input::get('manual_sector'));}
			
		}
		else
		{
			if(Input::get('sector') != '')
			{
				$sector = Input::get('sector');
			}
			else{$sector = '';}
		}
		
		// check if the meeting type is being selected by dropdown or manually.
		if(Input::get('manual_meeting_type') != '')
		{
			// if the user wants to insert manual meeting_type then, insert it to the meeting_type table as well.
			//check if the meeting_type does exist then don't insert it again, otherwise insert it.
			if(!checkRecordExistence('meeting_type','name',Input::get('manual_meeting_type')))
			{
				$manual_meeting_type = Input::get('manual_meeting_type');
				$meeting_type = DB::connection("sched")
					->table('meeting_type')
					->insertGetID(array('name' => $manual_meeting_type));
			}
			else{$meeting_type = getFieldValue("meeting_type","id","name",Input::get('manual_meeting_type'));}
		}
		else
		{
			if(Input::get('meeting_type') != '')
			{
				$meeting_type = Input::get('meeting_type');
			}
			else{$meeting_type = '';}
		}
		
		// check if the use want the meeting to be recured or not then set the variable to 1 or 0.
		if(Input::get('recure') == 'on')
		{ $recure = 1; }
		else
		{ $recure = 0; }

		if(Input::get('delay') == "on") 
			$delay = 1;
		else $delay = 0;
		if(Input::get('arg_media') == "on") 
			$arg_media = 1;
		else $arg_media = 0;

		$start_recurrence = convertToGregorian(ymd_format(Input::get('date')));

		if(Input::get('daily_recure_end_date') != "" && Input::get('recure_type') == 1)
	    {
	    	$daily_recure_end_date = convertToGregorian(ymd_format(Input::get('daily_recure_end_date')));
	    	$mData = array();
			while($start_recurrence <= $daily_recure_end_date)
			{
                // get the data and insert it into meeting_schedule table.
                // Tayyeb Changes
				$mData[] = array(

						"meeting_start"			=> $startTime_format,
                        "meeting_end"			=> $endTime_format,
                        // "meeting_title"         => Input::get('meeting_title'),
						"meeting_subject"		=> Input::get('meeting_subject'),
						"location"				=> $location,
						"sector"				=> $sector,
						"meeting_type"			=> $meeting_type,
						"arg_media"				=> $arg_media,
						"date"					=> $start_recurrence,
						"recurring"				=> $recure,
						"recure_type"			=> Input::get('recure_type'),
						"end_recure"			=> $daily_recure_end_date,
						"delay"					=> $delay,
						"user_id"				=> Auth::user()->id

				);

				$start_recurrence = date('Y-m-d', strtotime("+1 day", strtotime($start_recurrence)));
			}
			//echo "<pre>";print_r($mData);exit;
			//get the last record id of the meetings table.
		    $last_record_id = getLastMeetingID('meeting_schedule');
            $inserted = Meeting::addRecurredMeetingData($mData);
			//get the inserted records id which is greater than the last record id or newly inserted records.
	    	$inserted_ids = DB::connection('sched')->table('meeting_schedule')->select('id')->where('id','>',$last_record_id)->get();
	    	$multi_ids = array();
	    	foreach ($inserted_ids as $ids) {
				// add every individual meeting's log into its table.
				Meeting::addScheduleLog($ids->id,"Inserted");
	    		$multi_ids[] = $ids->id; 
	    	}
	    }
	    elseif(Input::get('weekly_recure_end_date') != "" && Input::get('recure_type') == 2)
	    {
	    	$weekly_recure_end_date = convertToGregorian(ymd_format(Input::get('weekly_recure_end_date')));
	    	$mData = array();
			while($start_recurrence < $weekly_recure_end_date)
			{
				// get the numeric week day number of gregorian date. e.g: Monday = 1 and so on
				$start_day = date("N", strtotime($start_recurrence));
				if(in_array($start_day, Input::get('weekly_recure_day')))
				{
                    // Tayyeb Changes
					// get the the data and insert it into meeting_schedule table.
					$mData[] = array(

							"meeting_start"			=> $startTime_format,
                            "meeting_end"			=> $endTime_format,
                            // "meeting_title"         => Input::get('meeting_title'),
							"meeting_subject"		=> Input::get('meeting_subject'),
							"location"				=> $location,
							"sector"				=> $sector,
							"meeting_type"			=> $meeting_type,
							"arg_media"				=> $arg_media,
							"date"					=> $start_recurrence,
							"recurring"				=> $recure,
							"recure_type"			=> Input::get('recure_type'),
							"end_recure"			=> $weekly_recure_end_date,
							"delay"					=> $delay,
							"user_id"				=> Auth::user()->id

					);
				}

				$start_recurrence = date('Y-m-d', strtotime("+1 day", strtotime($start_recurrence)));
			}
			//dd($mData);
			//get the last record id of the meetings table.
		    $last_record_id = getLastMeetingID('meeting_schedule');
			$inserted = Meeting::addRecurredMeetingData($mData);
			//get the inserted records id which is greater than the last record id or newly inserted records.
	    	$inserted_ids = DB::connection('sched')->table('meeting_schedule')->select('id')->where('id','>',$last_record_id)->get();
	    	$multi_ids = array();
	    	foreach ($inserted_ids as $ids) {
				// add every individual meeting's log into its table.
				Meeting::addScheduleLog($ids->id,"Inserted");
	    		$multi_ids[] = $ids->id; 
	    	}

			$week_days = array();
			for ($mid=0; $mid < count($multi_ids) ; $mid++) {
				for ($i=0;$i<count(Input::get('weekly_recure_day'));$i++) { 
					$week_days[] = array("week_day" => Input::get('weekly_recure_day')[$i], "meeting_id" => $multi_ids[$mid]);
				}
			}

	    	if(!empty($multi_ids))
	    	{
				//now insert the week recure days in its table.
				DB::connection('sched')->table('weekly_recurrence')->insert($week_days);
	    	}
		}
		// add the number of weeks to the current date and iterate until the after weeks end date.
		elseif(Input::get('after_weeks_recure') != "" && Input::get('recure_type') == 3)
		{
			$after_num_of_weeks_recure = Input::get('after_weeks_recure');
			$after_weeks_end_date = convertToGregorian(ymd_format(Input::get('after_weeks_end_date')));
	    	$mData = array();
			while($start_recurrence < $after_weeks_end_date)
			{   
                // Tayyeb Changes
				// get the the data and insert it into meeting_schedule table.
				$mData[] = array(

						"meeting_start"			=> $startTime_format,
                        "meeting_end"			=> $endTime_format,
                        // "meeting_title"         => Input::get('meeting_title'),
						"meeting_subject"		=> Input::get('meeting_subject'),
						"location"				=> $location,
						"sector"				=> $sector,
						"meeting_type"			=> $meeting_type,
						"arg_media"				=> $arg_media,
						"date"					=> $start_recurrence,
						"recurring"				=> $recure,
						"recure_type"			=> Input::get('recure_type'),
						"num_of_weeks"			=> $after_num_of_weeks_recure,
						"end_recure"			=> $after_weeks_end_date,
						"delay"					=> $delay,
						"user_id"				=> Auth::user()->id

				);

				$start_recurrence = date('Y-m-d', strtotime("+$after_num_of_weeks_recure weeks", strtotime($start_recurrence)));
			}	
			//dd($mData);
			//get the last record id of the meetings table.
		    $last_record_id = getLastMeetingID('meeting_schedule');
			$inserted = Meeting::addRecurredMeetingData($mData);
			//get the inserted records id which is greater than the last record id or newly inserted records.
	    	$inserted_ids = DB::connection('sched')->table('meeting_schedule')->select('id')->where('id','>',$last_record_id)->get();
	    	$multi_ids = array();
	    	foreach ($inserted_ids as $ids) {
				// add every individual meeting's log into its table.
				Meeting::addScheduleLog($ids->id,"Inserted");
	    		$multi_ids[] = $ids->id; 
	    	}
		}
		else
		{
			// if the meeting is not intended to be recurred.
			$mData = array(

					"meeting_start"			=> $startTime_format,
                    "meeting_end"			=> $endTime_format,
                    // "meeting_title"         => Input::get('meeting_title'),
					"meeting_subject"		=> Input::get('meeting_subject'),
					"location"				=> $location,
					"sector"				=> $sector,
					"meeting_type"			=> $meeting_type,
					"arg_media"				=> $arg_media,
					"date"					=> $today_date,
					"delay"					=> $delay,
					"user_id"				=> Auth::user()->id
			);

			//Insert President daily Meetings form data
			$inserted = Meeting::addMeetingData($mData);
			// add individual meeting's log into its table.
			Meeting::addScheduleLog($inserted,"Inserted");
		}
		if($inserted)
		{
			// getting all of the post data
			$files = Input::file('files');
			//print_r($files);exit;
			$errors = "";
			$auto = 1;
			$file_data = array();

			if(Input::hasFile('files'))
			{				
				foreach($files as $file) 
				{
					// validating each file.
					$validator = Validator::make(
						  [
							'file' => $file,
							'extension'  => Str::lower($file->getClientOriginalExtension())
						],
						[
							'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
						]
					  );
					  if($validator->passes())
					  {
						$destinationPath = 'documents/scheduling_attachments';
						$original_filename = $file->getClientOriginalName();
						$temp = explode(".", $original_filename);
						$extension = end($temp);
										
						if(!empty($multi_ids))			  
							$lastFileId = getLastMeetingID("meeting_schedule");
						else
							$lastFileId = $inserted;

						if($auto == 1)
						$filename = "Attachment_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
						else
						$filename = "Attachment_".$lastFileId."_".$auto.".".$extension;
						
						$upload_success = $file->move($destinationPath, $filename);

						if($upload_success) 
						{
							// if there is a recurrence in meetings then insert the multiple records.
							if(!empty($multi_ids))
							{
								$upload_data = array();
						    	// now loop based on the number of records inserted and store the upload table required fields into the array.
						    	for($j = 0; $j < count($multi_ids); $j++)
						    	{
							    	$upload_data[] = array(
					   					'file_name'				=> $filename,
					   					'original_file_name'	=> $original_filename,
					   					'meeting_id'			=> $multi_ids[$j],
					   					'user_id'				=> Auth::user()->id,
					   					"created_at"			=> date('Y-m-d H:i:s')
					   				);
							   	}
							}
							else
							{
								$upload_data = array(
				   					'file_name'				=> $filename,
				   					'original_file_name'	=> $original_filename,
				   					'meeting_id'			=> $inserted,
				   					'user_id'				=> Auth::user()->id,
				   					"created_at"			=> date('Y-m-d H:i:s')
				   				);
							}
							//call the model function to insert the data into upload table.
							Meeting::uploadFiles($upload_data);
						} 
						else 
						{
							// delete the inserted records if the file upload had problems.
							DB::connection('sched')->table('meeting_schedule')->where('id', $insertedRecordID)->delete();
							// redirect back with errors.
							return Redirect::route('MeetingForm')->withErrors($validator);
						}
					}
					else
					{
						// delete the inserted records if the file upload had problems.
						DB::connection('sched')->table('meeting_schedule')->where('id', $insertedRecordID)->delete();
						// redirect back with errors.
						return Redirect::route('MeetingForm')->withErrors($validator);
					}
					$auto ++;
				}
			}
			if($delay == 1)
			{
				return Redirect::route("getDelayedMeetings")->with("success","جلسه موفقانه اضافه گردید");
			}
			else{
				return Redirect::route("getMeetings")->with("success","جلسه موفقانه اضافه گردید");
			}
		}
		else
		{
			return Redirect::route("MeetingForm")->with("fail","در اضافه کردن جلسه مشکل وجود دارد، لطفآ دوباره کوشش کنید");
		}

	}

	// Load the edit_meeting page with the particular meeting's data in fields.
	public function editMeetings($id = 0,$hint)
	{
		$data['record'] = Meeting::getSpecificMeeting($id);
		$week_day = Meeting::getMeetingWeeklyRecurringDays($id); 
		$week_day_recure = array();
		foreach ($week_day as $item) {
			$week_day_recure[] = $item->week_day;
		}
		$data['weekly_recure_days'] = $week_day_recure;
		$data['hint'] = $hint;
		$data['id'] = $id;
		$data['week_days'] = Meeting::getMeetingWeekDays();
		$data['locations'] = Meeting::getMeetingLocations();
		$data['sectors'] = Meeting::getMeetingSectors();
		$data['meeting_type'] = Meeting::getMeetingType();
		//dd($data['weekly_recure_days']);
		//load view for editing the Meeting.
		return View::make('sched.edit_form', $data);
	}

	public function updateMeetings(Request $request,$id = 0,$hint)
	{
		//dd($_POST);
		//validate the input fields
	    $this->validate($request,[
	        "meeting_start"		=> "required",
			// "meeting_subject"	=> "required",
	        ]
	    );

    	// get the start and end times.
		if(!Input::get('meeting_start') == '')
		{
			$startTime_format = DATE("H:i", STRTOTIME(Input::get('meeting_start')));
		}

		if(!Input::get('meeting_end') == '')
		{

			$endTime_format = DATE("H:i", STRTOTIME(Input::get('meeting_end')));
		
	    }
	    else
	    {
	    	$endTime_format = '';
	    }

    	// get the current times before updating it based on its id.
    	$current_times = DB::connection('sched')
    							->table('meeting_schedule')
    							->select(array("meeting_start","meeting_end","date"))
    							->where('id', $id)->where("deleted",0)
    							->get();

		if(!empty($current_times))
    	{

    		//foreach the current_times array and get the values.
	    	foreach($current_times AS $items)
	    	{
	    		$current_start_time = $items->meeting_start;
	    		$current_end_time = $items->meeting_end;
	    		$current_date = $items->date;
	    	}

		}
		else{
			$current_start_time = "";
			$current_end_time = "";
			$current_date = "";
		}

    	// change the current times to minutes.
    	$current_start_minutes = hoursToMinutes($current_start_time);
    	$current_end_minutes = hoursToMinutes($current_end_time);

    	// change the update times to minutes.
    	$update_start_minutes = hoursToMinutes($startTime_format);
    	$update_end_minutes = hoursToMinutes($endTime_format);

    	// now get the difference between the current and update times.
    	$update_diff_start_time = $current_start_minutes - $update_start_minutes;
    	$update_diff_end_time = $current_end_minutes - $update_end_minutes;
 
    	//echo $update_diff_start_time ." ". $update_diff_end_time."<br />";

    	// get the meetings which is after the editted meeting.
		$meetings = DB::connection('sched')
							->table('meeting_schedule')
							->select('*')
							->where('meeting_start', '>' , $current_start_time)
							->where('date', '=', $current_date)->where("deleted",0)
							->get();
    		
		// foreach the meetings which is after the editted meeting.
    	foreach($meetings AS $items)
    	{
    		$meeting_id = $items->id;
    		$meeting_start = $items->meeting_start;
    		$meeting_end = $items->meeting_end;
    		//echo $id." ".$meeting_start ." ". $meeting_end."<br />";
    		$meeting_start_minutes = hoursToMinutes($meeting_start);
    		$meeting_end_minutes = hoursToMinutes($meeting_end);
    		//echo $meeting_start_minutes ." ". $meeting_end_minutes."<br />";
    		if(($update_diff_start_time != 0) && ($update_diff_end_time == 0))
    		{
    			$update_meeting_start = abs($meeting_start_minutes + (-$update_diff_start_time));
    			$update_meeting_end = abs($meeting_end_minutes + (-$update_diff_start_time));
    		}
    		elseif (($update_diff_start_time == 0) && ($update_diff_end_time != 0)) 
    		{
    			$update_meeting_start = abs($meeting_start_minutes + (-$update_diff_end_time));
    			$update_meeting_end = abs($meeting_end_minutes + (-$update_diff_end_time));
    		}
    		else
    		{
    			$update_meeting_start = abs($meeting_start_minutes + (-$update_diff_start_time));
    			$update_meeting_end = abs($meeting_end_minutes + (-$update_diff_end_time));
    		}

    		//echo $update_meeting_start ." ". $update_meeting_end."<br />";
    		$meeting_start_time_format = minutesToHours($update_meeting_start);
    		$meeting_end_time_format = minutesToHours($update_meeting_end);
    		if($endTime_format != '' AND $current_end_time == '00:00:00' OR $endTime_format == '')
    		{
    			break;
    		}
    		// if the meeting_end is empty then just update the start time.
    		if($meeting_end == '00:00:00')
    		{
    			DB::connection('sched')
    			->table('meeting_schedule')
    			->where('id', '=' ,$meeting_id)
    			->where('meeting_start', '>=' ,$current_start_time)
    			->update(array('meeting_start' => $meeting_start_time_format));
    			continue;
    		}
    		//echo $meeting_start_time_format ." ". $meeting_end_time_format."<br />";
    		//update the start and end meeting times which is after the current updated times.
    		DB::connection('sched')
    			->table('meeting_schedule')
    			->where('id', '=' ,$meeting_id)
    			->where('meeting_start', '>=' ,$current_start_time)
    			->update(array('meeting_start' => $meeting_start_time_format, 'meeting_end' => $meeting_end_time_format));
    	}
    	//exit;

    	$week_day = getDayNum(Input::get('week_day'));

		// check if the location is being selected by dropdown or manually.
		if(Input::get('manual_loc') != '')
		{
			// if the user wants to insert manual location then, insert it to the locations table as well.
			//check if the location does exist then don't insert it again, otherwise insert it.
			if(!checkRecordExistence('locations','name',Input::get('manual_loc')))
			{
				$manual_location = Input::get('manual_loc');
				$location = DB::connection("sched")
					->table('locations')
					->insertGetID(array('name' => $manual_location));
			}
			else{$location = getFieldValue("locations","id","name",Input::get('manual_loc'));}
		}
		else
		{
			if(Input::get('location') != '')
			{
				$location = Input::get('location');
			}
			else{$location = '';}
		}
		
		// check if the sector is being selected by dropdown or manually.
		if(Input::get('manual_sector') != '')
		{
			
			// if the user wants to insert manual sector then, insert it to the sectors table as well.
			//check if the sector does exist then don't insert it again, otherwise insert it.
			if(!checkRecordExistence('sectors','name',Input::get('manual_sector')))
			{
				$manual_sector = Input::get('manual_sector');
				$sector = DB::connection("sched")
					->table('sectors')
					->insertGetID(array('name' => $manual_sector));
			}
			else{$sector = getFieldValue("sectors","id","name",Input::get('manual_sector'));}
			
		}
		else
		{
			if(Input::get('sector') != '')
			{
				$sector = Input::get('sector');
			}
			else{$sector = '';}
		}
		
		// check if the meeting type is being selected by dropdown or manually.
		if(Input::get('manual_meeting_type') != '')
		{
			// if the user wants to insert manual meeting_type then, insert it to the meeting_type table as well.
			//check if the meeting_type does exist then don't insert it again, otherwise insert it.
			if(!checkRecordExistence('meeting_type','name',Input::get('manual_meeting_type')))
			{
				$manual_meeting_type = Input::get('manual_meeting_type');
				$meeting_type = DB::connection("sched")
					->table('meeting_type')
					->insertGetID(array('name' => $manual_meeting_type));
			}
			else{$meeting_type = getFieldValue("meeting_type","id","name",Input::get('manual_meeting_type'));}
		}
		else
		{
			if(Input::get('meeting_type') != '')
			{
				$meeting_type = Input::get('meeting_type');
			}
			else{$meeting_type = '';}
		}
		
		// since the user is not allowed to uncheck the recurred meeting, it is assigned to recure or 1;
		$recure = 1;
		if(Input::get('delay') == "on") $delay = 1;
		else $delay = 0;
		if(Input::get('arg_media') == "on") $arg_media = 1;
		else $arg_media = 0;

		$start_recurrence = convertToGregorian(gregorian_format(Input::get("date")));
		// get the meeting end_recure date based on its id.
		$existing_end_recure_date = checkRecurredMeetingExistence("end_recure", $id);
		$current_start_time = checkRecurredMeetingExistence("meeting_start", $id);
		$current_end_time = checkRecurredMeetingExistence("meeting_end", $id);

		if(Input::get('daily_recure_end_date') != "" && Input::get('recure_type') == 1)
	    {
			$daily_recure_end_date = convertToGregorian(gregorian_format(Input::get('daily_recure_end_date')));
			// check the recure end date whehter it is changed. If it is changed then repeat the recurring from today date. Otherwise just update this particular meeting based on id.
			$lastRecurredEndDate = checkLastRecurreEndDate($id, 1);
			if(!empty($lastRecurredEndDate) && $lastRecurredEndDate != $daily_recure_end_date)
			{
				if($start_recurrence < $daily_recure_end_date)
				{
					// delete the meeting based on id which is exist if it is edited to be recured.
					Meeting::deleteExistingNonRecureMeeting($id);
					// Delete the old recurring ones.
					Meeting::deleteOldRecurringMeetings($current_start_time, $current_end_time, $start_recurrence, $existing_end_recure_date, 1);	
					$mData = array();
					while($start_recurrence <= $daily_recure_end_date)
					{
                        // Tayyeb Changes
						// get the the data and insert it into meeting_schedule table.
						$mData[] = array(

								"meeting_start"			=> $startTime_format,
                                "meeting_end"			=> $endTime_format,
                                // "meeting_title"         => Input::get('meeting_title'),
								"meeting_subject"		=> Input::get('meeting_subject'),
								"location"				=> $location,
								"sector"				=> $sector,
								"meeting_type"			=> $meeting_type,
								"arg_media"				=> $arg_media,
								"date"					=> $start_recurrence,
								"recurring"				=> $recure,
								"recure_type"			=> Input::get('recure_type'),
								"end_recure"			=> $daily_recure_end_date,
								"delay"					=> $delay,
								"user_id"				=> Auth::user()->id,
								"updated_at"			=> date("Y-m-d H:i:s")

						);

						$start_recurrence = date('Y-m-d', strtotime("+1 day", strtotime($start_recurrence)));
					}
					//echo "<pre>";print_r($mData);exit;
					//get the last record id of the meetings table.
					$last_record_id = getLastMeetingID('meeting_schedule');
					$inserted = Meeting::addRecurredMeetingData($mData);
					//get the inserted records id which is greater than the last record id or newly inserted records.
					$inserted_ids = DB::connection('sched')->table('meeting_schedule')->select('id')->where('id','>',$last_record_id)->get();
					$multi_ids = array();
					foreach ($inserted_ids as $ids) {
						// add every individual meeting's log into its table.
						Meeting::addScheduleLog($ids->id,"Inserted");
						$multi_ids[] = $ids->id; 
					}
					// get the attachment information based on meeting id.
					$uploadedFileDetails = getMeetingFileName($id);
					if($uploadedFileDetails != "")
					{
						foreach ($uploadedFileDetails as $item) {
							// if there is a recurrence in meetings then insert the multiple records.
							if(!empty($multi_ids))
							{
								$upload_data = array();
								// now loop based on the number of records inserted and store the upload table required fields into the array.
								for($j = 0; $j < count($multi_ids); $j++)
								{
									$upload_data[] = array(
										'file_name'				=> $item->file_name,
										'original_file_name'	=> $item->original_file_name,
										'meeting_id'			=> $multi_ids[$j],
										'user_id'				=> Auth::user()->id,
										"created_at"			=> date('Y-m-d H:i:s')
									);
								}
							}
						}
						//call the model function to insert the data into upload table.
						Meeting::uploadFiles($upload_data);
					}
				}
				else
				{
					return Redirect::route("EditMeetings", array($id,"day"))->with("fail","تاریخ جلسه باید کوچکتر از تاریخ ختم تکرار جلسه باشد");
				}
			}
			else{
                // Tayyeb Changes
				// add meeting's log into its table.
				Meeting::addScheduleLog($id,"Updated");

				$mData = array(
					"meeting_start"			=> $startTime_format,
                    "meeting_end"			=> $endTime_format,
                    // "meeting_title"         => Input::get('meeting_title'),
					"meeting_subject"		=> Input::get('meeting_subject'),
					"location"				=> $location,
					"sector"				=> $sector,
					"meeting_type"			=> $meeting_type,
					"arg_media"				=> $arg_media,
					"date"					=> $start_recurrence,
					"recurring"				=> $recure,
					"recure_type"			=> Input::get('recure_type'),
					"end_recure"			=> $daily_recure_end_date,
					"delay"					=> $delay,
					"user_id"				=> Auth::user()->id,
					"updated_at"			=> date("Y-m-d H:i:s")
				);
				//update the current meeting based on id.
				$updated = Meeting::updateMeeting($mData, $id);
				if($updated){
					if($delay == 1)
					{
						return Redirect::route("getDelayedMeetings")->with("success","جلسه موفقانه اصلاح گردید");
					}
					else{
						return Redirect::route("getMeetings")->with("success","جلسه موفقانه اصلاح گردید");
					}
				}
				else
				{
					return Redirect::route("EditMeetings")->with("fail","جلسه اصلاح نگردید، مشکل وجود دارد، لطفآ با مسئول سیستم به تماس شوید");
				}
			}
	    }
	    elseif(Input::get('weekly_recure_end_date') != "" || Input::get('recure_type') == 2)
	    {
			$weekly_recure_end_date = convertToGregorian(gregorian_format(Input::get('weekly_recure_end_date')));
			// check the recure end date whehter it is changed. If it is changed then repeat the recurring from today date. Otherwise just update this particular meeting based on id.
			$lastRecurredEndDate = checkLastRecurreEndDate($id, 2);
			$lastRecurredWeekDays = checkLastRecurredWeekDays($id);
			// make the lastRecurredWeekDays array a correct array.
			$weekDays = array();
			$auto = 0;
			foreach($lastRecurredWeekDays as $wd)
			{
				$weekDays[$auto] = $wd->week_day;
				$auto++;
			}
			//echo "<pre>";print_r(Input::get("weekly_recure_day"));
			//dd($weekDays);
			// check if the recure end date and week days are changed.
			if($lastRecurredEndDate != $weekly_recure_end_date || $weekDays != Input::get("weekly_recure_day"))
			{
				if($start_recurrence < $weekly_recure_end_date)
				{
					// get the weekly recurring meetings id based on sent parameters.
					$weeklyRecurredMeetingsID = getWeeklyRecurringMeetingsID($current_start_time, $current_end_time, $existing_end_recure_date, 2);
					$records = array();
					foreach($weeklyRecurredMeetingsID as $wrmd)
					{
						$records[] = $wrmd->id;
					}
					if(!empty($weeklyRecurredMeetingsID))
					{
						// now delete all records of meetings which are recurring weekly from weekly_recurrence table.
						DB::connection('sched')->table('weekly_recurrence')->whereIn('meeting_id',$records)->delete();
					}
					// delete the meeting based on id which is exist if it is edited to be recured.
					Meeting::deleteExistingNonRecureMeeting($id);
					
					// Delete the old recurring ones.
					Meeting::deleteOldRecurringMeetings($current_start_time, $current_end_time, $start_recurrence, $existing_end_recure_date, 2);	
					$mData = array();
					while($start_recurrence < $weekly_recure_end_date)
					{
						// get the numeric week day number of gregorian date. e.g: Monday = 1 and so on
						$start_day = date("N", strtotime($start_recurrence));
						if(in_array($start_day, Input::get('weekly_recure_day')))
						{
                            // Tayyeb Changes
							// get the the data and insert it into meeting_schedule table.
							$mData[] = array(

									"meeting_start"			=> $startTime_format,
                                    "meeting_end"			=> $endTime_format,
                                    // "meeting_title"         => Input::get('meeting_title'),
									"meeting_subject"		=> Input::get('meeting_subject'),
									"location"				=> $location,
									"sector"				=> $sector,
									"meeting_type"			=> $meeting_type,
									"arg_media"				=> $arg_media,
									"date"					=> $start_recurrence,
									"recurring"				=> $recure,
									"recure_type"			=> Input::get('recure_type'),
									"end_recure"			=> $weekly_recure_end_date,
									"delay"					=> $delay,
									"user_id"				=> Auth::user()->id,
									"updated_at"			=> date("Y-m-d H:i:s")

							);
						}

						$start_recurrence = date('Y-m-d', strtotime("+1 day", strtotime($start_recurrence)));
					}
					//dd($mData);
					//get the last record id of the meetings table.
					$last_record_id = getLastMeetingID('meeting_schedule');
					if(empty($last_record_id))
					{ $last_record_id = 0;}
					$inserted = Meeting::addRecurredMeetingData($mData);
					//get the inserted records id which is greater than the last record id or newly inserted records.
					$inserted_ids = DB::connection('sched')->table('meeting_schedule')->select('id')->where('id','>',$last_record_id)->get();
					$multi_ids = array();
					foreach ($inserted_ids as $ids) {
						// add every individual meeting's log into its table.
						Meeting::addScheduleLog($ids->id,"Inserted");
						$multi_ids[] = $ids->id; 
					}

					// get the attachment information based on meeting id.
					$uploadedFileDetails = getMeetingFileName($id);
					if($uploadedFileDetails != "")
					{
						foreach ($uploadedFileDetails as $item) {
							// if there is a recurrence in meetings then insert the multiple records.
							if(!empty($multi_ids))
							{
								$upload_data = array();
								// now loop based on the number of records inserted and store the upload table required fields into the array.
								for($j = 0; $j < count($multi_ids); $j++)
								{
									$upload_data[] = array(
										'file_name'				=> $item->file_name,
										'original_file_name'	=> $item->original_file_name,
										'meeting_id'			=> $multi_ids[$j],
										'user_id'				=> Auth::user()->id,
										"created_at"			=> date('Y-m-d H:i:s')
									);
								}
							}
						}
						//call the model function to insert the data into upload table.
						Meeting::uploadFiles($upload_data);
					}

					$week_days = array();
					for ($mid=0; $mid < count($multi_ids) ; $mid++) {
						for ($i=0;$i<count(Input::get('weekly_recure_day'));$i++) { 
							$week_days[] = array("week_day" => Input::get('weekly_recure_day')[$i], "meeting_id" => $multi_ids[$mid]);
						}
					}

					if(!empty($multi_ids))
					{
						// before adding new week day in the weekly recurrence table, the previous data based on its id should be deleted.
						DB::connection('sched')->table('weekly_recurrence')->whereIn('meeting_id', $multi_ids)->delete();
						//now insert the week recure days in its table.
						DB::connection('sched')->table('weekly_recurrence')->insert($week_days);
					}
				}
				else
				{
					return Redirect::route("EditMeetings", array($id,"week"))->with("fail","تاریخ جلسه باید کوچکتر از تاریخ ختم تکرار جلسه باشد");
				}
			}
			else{
                
                // Tayyeb Changes
				// add meeting's log into its table.
				Meeting::addScheduleLog($id,"Updated");

				$mData = array(

					"meeting_start"			=> $startTime_format,
                    "meeting_end"			=> $endTime_format,
                    // "meeting_title"         => Input::get('meeting_title'),
					"meeting_subject"		=> Input::get('meeting_subject'),
					"location"				=> $location,
					"sector"				=> $sector,
					"meeting_type"			=> $meeting_type,
					"arg_media"				=> $arg_media,
					"date"					=> $start_recurrence,
					"recurring"				=> $recure,
					"recure_type"			=> Input::get('recure_type'),
					"end_recure"			=> $weekly_recure_end_date,
					"delay"					=> $delay,
					"user_id"				=> Auth::user()->id,
					"updated_at"			=> date("Y-m-d H:i:s")
				);

				//update the current meeting based on id.
				$updated = Meeting::updateMeeting($mData, $id);
				//dd($updated);
				$inserted = "";
				$week_days = array();
				for ($i=0;$i<count(Input::get('weekly_recure_day'));$i++) { 
					$week_days[] = array("week_day" => Input::get('weekly_recure_day')[$i], "meeting_id" => $id);
				}
				// before adding new week day in the weekly recurrence table, the previous data based on its id should be deleted.
				DB::connection('sched')->table('weekly_recurrence')->where('meeting_id', $id)->delete();
				//now insert the week recure days in its table.
				DB::connection('sched')->table('weekly_recurrence')->insert($week_days);
				//update the current meeting based on id.
				if($updated){
					if($delay == 1)
					{
						return Redirect::route("getDelayedMeetings")->with("success","جلسه موفقانه اصلاح گردید");
					}
					else{
						return Redirect::route("getMeetings")->with("success","جلسه موفقانه اصلاح گردید");
					}
				}
				else
				{
					return Redirect::route("EditMeetings")->with("fail","جلسه اصلاح نگردید، مشکل وجود دارد، لطفآ با مسئول سیستم به تماس شوید");
				}
			}
		}
		// add the number of weeks to the current date and iterate until the after weeks end date.
		elseif(Input::get('after_weeks_recure') != "" && Input::get('recure_type') == 3)
		{
			$after_weeks_end_date = convertToGregorian(gregorian_format(Input::get('after_weeks_end_date')));
			
			// check the recure end date whehter it is changed. If it is changed then repeat the recurring from today date. Otherwise just update this particular meeting based on id.
			$lastRecurredEndDate = checkLastRecurreEndDate($id, 3);
			$lastRecurredNumOfWeeks = checkLastRecurredNumOfWeeks($id, 3);
			
			if(!empty($lastRecurredEndDate) && $lastRecurredEndDate != $after_weeks_end_date || $lastRecurredNumOfWeeks != Input::get('after_weeks_recure'))
			{
				if($start_recurrence < $after_weeks_end_date)
				{
					$after_num_of_weeks_recure = Input::get('after_weeks_recure');
					// delete the meeting based on id which is exist if it is edited to be recured.
					Meeting::deleteExistingNonRecureMeeting($id);
					// Delete the old recurring ones.
					Meeting::deleteOldRecurringMeetings($current_start_time, $current_end_time, $start_recurrence, $existing_end_recure_date, 3);	
					$mData = array();
					while($start_recurrence < $after_weeks_end_date)
					{
                        // Tayyeb Changes
						// get the the data and insert it into meeting_schedule table.
						$mData[] = array(

								"meeting_start"			=> $startTime_format,
                                "meeting_end"			=> $endTime_format,
                                // "meeting_title"         => Input::get('meeting_title'),
								"meeting_subject"		=> Input::get('meeting_subject'),
								"location"				=> $location,
								"sector"				=> $sector,
								"meeting_type"			=> $meeting_type,
								"arg_media"				=> $arg_media,
								"date"					=> $start_recurrence,
								"recurring"				=> $recure,
								"recure_type"			=> Input::get('recure_type'),
								"num_of_weeks"			=> $after_num_of_weeks_recure,
								"end_recure"			=> $after_weeks_end_date,
								"delay"					=> $delay,
								"user_id"				=> Auth::user()->id,
								"updated_at"			=> date("Y-m-d H:i:s")

						);

						$start_recurrence = date('Y-m-d', strtotime("+$after_num_of_weeks_recure weeks", strtotime($start_recurrence)));
					}	
					//dd($mData);
					//get the last record id of the meetings table.
					$last_record_id = getLastMeetingID('meeting_schedule');
					$inserted = Meeting::addRecurredMeetingData($mData);
					//get the inserted records id which is greater than the last record id or newly inserted records.
					$inserted_ids = DB::connection('sched')->table('meeting_schedule')->select('id')->where('id','>',$last_record_id)->get();
					$multi_ids = array();
					foreach ($inserted_ids as $ids) {
						// add every individual meeting's log into its table.
						Meeting::addScheduleLog($ids->id,"Inserted");
						$multi_ids[] = $ids->id; 
					}

					// get the attachment information based on meeting id.
					$uploadedFileDetails = getMeetingFileName($id);
					if($uploadedFileDetails != "")
					{
						foreach ($uploadedFileDetails as $item) {
							// if there is a recurrence in meetings then insert the multiple records.
							if(!empty($multi_ids))
							{
								$upload_data = array();
								// now loop based on the number of records inserted and store the upload table required fields into the array.
								for($j = 0; $j < count($multi_ids); $j++)
								{
									$upload_data[] = array(
										'file_name'				=> $item->file_name,
										'original_file_name'	=> $item->original_file_name,
										'meeting_id'			=> $multi_ids[$j],
										'user_id'				=> Auth::user()->id,
										"created_at"			=> date('Y-m-d H:i:s')
									);
								}
							}
						}
						//call the model function to insert the data into upload table.
						Meeting::uploadFiles($upload_data);
					}
				}
				else
				{
					return Redirect::route("EditMeetings", array($id,"week"))->with("fail","تاریخ جلسه باید کوچکتر از تاریخ ختم تکرار جلسه باشد");
				}
			}
			else{

                // Tayyeb Changes
				// add meeting's log into its table.
				Meeting::addScheduleLog($id,"Updated");

				$mData = array(

					"meeting_start"			=> $startTime_format,
                    "meeting_end"			=> $endTime_format,
                    // "meeting_title"         => Input::get('meeting_title'),
					"meeting_subject"		=> Input::get('meeting_subject'),
					"location"				=> $location,
					"sector"				=> $sector,
					"meeting_type"			=> $meeting_type,
					"arg_media"				=> $arg_media,
					"date"					=> $start_recurrence,
					"recurring"				=> $recure,
					"recure_type"			=> Input::get('recure_type'),
					"end_recure"			=> $after_weeks_end_date,
					"delay"					=> $delay,
					"user_id"				=> Auth::user()->id,
					"updated_at"			=> date("Y-m-d H:i:s")
				);
				//dd($mData);
				//update the current meeting based on id.
				$updated = Meeting::updateMeeting($mData, $id);
				if($updated){
					if($delay == 1)
					{
						return Redirect::route("getDelayedMeetings")->with("success","جلسه موفقانه اصلاح گردید");
					}
					else{
						return Redirect::route("getMeetings")->with("success","جلسه موفقانه اصلاح گردید");
					}
				}
				else
				{
					return Redirect::route("EditMeetings")->with("fail","جلسه اصلاح نگردید، مشکل وجود دارد، لطفآ با مسئول سیستم به تماس شوید");
				}
			}
		}
		else
		{
            // Tayyeb Changes
			// if the meeting is not intended to be recurred.
			$mData = array(

					"meeting_start"			=> $startTime_format,
                    "meeting_end"			=> $endTime_format,
                    // "meeting_title"         => Input::get('meeting_title'),
					"meeting_subject"		=> Input::get('meeting_subject'),
					"location"				=> $location,
					"sector"				=> $sector,
					"meeting_type"			=> $meeting_type,
					"arg_media"				=> $arg_media,
					"date"					=> convertToGregorian(gregorian_format(Input::get("date"))),
					"delay"					=> $delay,
					"user_id"				=> Auth::user()->id,
					"updated_at"			=> date("Y-m-d H:i:s")
			);

			//update the current meeting based on id.
			$updated = Meeting::updateMeeting($mData, $id);
			$inserted = "";
		}

    	// if the particular meeting has been updated successfully then do the rest.
    	if($inserted || $updated)
    	{
			// add meeting's log into its table.
			Meeting::addScheduleLog($id,"Updated");
    		if($delay == 1)
			{
				return Redirect::route("getDelayedMeetings")->with("success","جلسه موفقانه اصلاح گردید");
			}
			else{
				return Redirect::route("getMeetings")->with("success","جلسه موفقانه اصلاح گردید");
			}
		}
		else
		{
			return Redirect::route("EditMeetings")->with("fail","جلسه اصلاح نگردید، مشکل وجود دارد، لطفآ با مسئول سیستم به تماس شوید");
		}

	}
	
	// load search page.
	public function loadSearchPage()
	{
		$data['sectors'] = Meeting::getMeetingSectors();
		$data['meeting_type'] = Meeting::getMeetingType();
		return view('sched.searchMeetings', $data);
	}
	
	// search the meetings based on posted values. 
	public function searchMeetings()
	{
		//dd($_POST);
		if(Input::get('s_date') == "" && Input::get('e_date') == "" && Input::get('meeting_type') == "" && Input::get('sector') == "")
		{
			return "<div style='text-align:center;margin-top:30px;color:red;font-family:tahoma;font-size16px;'>لطفآ تاریخ آغاز یا ختم و یا هم نوع جلسه یا سکتور را جهت جستجو انتخاب کنید </div>";
		}	
		if(Input::get('sector') != "")
		{
			$data['sector'] = 1;
		}
		else { $data['sector'] = ""; }
		$data['records'] = Meeting::postSearchMeetings("");
		return view('sched.search_result', $data);
	}
	
	// Export the search result in excel.
	public function exportToExcel()
	{
		dd($_POST);
		$results = Meeting::postSearchMeetings('print');
		
		Excel::create('Laravel Excel', function($excel) {

		    $excel->sheet('Excel sheet', function($sheet) {
		
		        $sheet->setOrientation('landscape');
		
		    });
		
		})->export('xls');
		//dd($results);
		// Excel::load('excel_template/schedule_meeting_report.xlsx', function($file) use($results){
		// 	$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($results){	
		// 		$row = 3;
		// 		$sheet->setFreeze('A3');
		// 		$total_time_spent = "";
		// 		foreach($results AS $item)
		// 		{
		// 			$day = jdatetime::getDayNames(date_format(date_create(toGregorian($item->date)),'l'));
					
		// 			$start = date("g:i A", strtotime($item->meeting_start));
		// 		    $end = date("g:i A", strtotime($item->meeting_end));
		// 		    $s_time = jdatetime::date($start,true,true,'Asia/Kabul');
		// 		    $e_time = jdatetime::date($end,true,true,'Asia/Kabul');
		// 		    $start_time = strtotime($start);
		// 			$end_time = strtotime($end);
					
		// 			$sheet->setHeight($row, 20);
					
		// 			$sheet->setCellValue('A'.$row.'',$day);
		// 			$sheet->setCellValue('B'.$row.'',$start_time);
		// 			$sheet->setCellValue('C'.$row.'',$end_time);
		// 			$sheet->setCellValue('D'.$row.'',round(abs($start_time - $end_time) / 60,2). " دقیقه");
		// 			$sheet->setCellValue('E'.$row.'',$item->meeting_type);
		// 			$sheet->setCellValue('F'.$row.'',$item->sector);
		// 			$total_time_spent += round(abs($start_time - $end_time) / 60,2);
		// 			$sheet->setCellValue('G'.$row.'',$total_time_spent);
		// 			// $sheet->setCellValue('H'.$row.'',$item->description);
		// 			// $sheet->setCellValue('I'.$row.'',$item->model);
		// 			// $sheet->setCellValue('J'.$row.'',$item->serial_number);
		// 			// $sheet->setCellValue('K'.$row.'',$item->vendor_name);
		// 			// $sheet->setCellValue('L'.$row.'',$item->voucher_number);
		// 			// $sheet->setCellValue('M'.$row.'',$item->donor_name);
		// 			// $sheet->setCellValue('N'.$row.'',$item->asset_cost_afn);
		// 			// $sheet->setCellValue('O'.$row.'',$item->asset_cost_usd);
		// 			// $sheet->setCellValue('P'.$row.'',$item->purchase_date);
		// 			// $sheet->setCellValue('Q'.$row.'',$item->purchased_by);
		// 			// $sheet->setCellValue('R'.$row.'',$item->last_physical_check_date);
		// 			// $sheet->setCellValue('S'.$row.'',$item->physical_checked_by);
		// 			// $sheet->setCellValue('T'.$row.'',$item->next_physical_check_date);
		// 			// $sheet->setCellValue('U'.$row.'',$item->condition);
		// 			// $sheet->setCellValue('V'.$row.'',$item->remarks);
					
		// 			$row++;
		// 		}
				
		// 		$sheet->setBorder('A3:V'.($row-1).'', 'thin');
				
  //  		});
			
		// 	})->export('xlsx');
			
	}

	//Load President's daily meeting list view
	public function dailyMeetings()
	{
	
        //call the helper function to return the jalali date.
	    $jalali_date = convertToJalali(date("Y-m-d"));
	    $top_dates = shamsi_gregorian_date($jalali_date);
	    $date = "<input type='hidden' id='date' value='".date("Y-m-d")."' />";
        //get Meeting's data from model
		$meetings = Meeting::getDailyMeetings(date("Y-m-d"));
		// $mSearch = array();
		// $mData=array();
		// if(!empty($meeings))
		// {
		// 	foreach ($meetings as $value) {
		// 		$mSearch[] = $value->meeting_start;
		// 		$mData[$value->meeting_start] = array(
		// 			'id'=>$value->id,
		// 			'meeting_start'=>$value->meeting_start,
		// 			'meeting_end'=>$value->meeting_end,
		// 			'meeting_subject'=>$value->meeting_subject,
		// 			'location'=>$value->location,
		// 			'sector' => $value->sector,
		// 			'meeting_type' => $value->meeting_type,
		// 			'arg_media' => $value->arg_media,
		// 			'date' => $value->date,
		// 			'recurring' => $value->recurring,
		// 			'recure_type' => $value->recure_type,
		// 		); 
		// 	}
		// }

		//echo "<pre>";print_r($meetings);echo "</pre>";
		// get the last updated record date and time
		// $last_updated = Meeting::getLastUpdatedRecordInfo();
		// if(!empty($last_updated))
		// {
		// 	$last_updated = explode(" ", $last_updated->updated_at);
		// 	$last_updated_date = $last_updated[0];
		// 	$last_updated_date = convertToJalali($last_updated_date);
		// 	$last_updated_time = $last_updated[1];
		// 	$time = explode(":", $last_updated_time);
		// 	$last_updated_time = $time[0].":".$time[1];
		// 	$last_updated_time = date("g:i A", strtotime($last_updated_time));
		// 	$last_updated_date = jdatetime::date($last_updated_date,true,true);
		// 	$last_updated_date = shamsi_date_format($last_updated_date);
		// 	$last_updated_time_and_date = "<span dir='rtl'>".$last_updated_date."  &nbsp;<span dir='ltr'>".$last_updated_time."</span></span>";
		// }
		// else
		// {
		// 	$last_updated_time_and_date = "";
		// }
		//print_r($meetings->get());exit;
		//$data['last_update'] = $last_updated_time_and_date;
		$data['top_dates'] = $top_dates;
		$data['meetings'] = $meetings;
		$data['date'] = $date;
		//$data['mSearch'] = $mSearch;
		//$data['mData'] = $mData;
		return View::make('sched.dailyMeetings', $data);

	}

	// Get all President's daily meetings for dataTable.
	public function dailyMeetingsPrevNext()
	{	
		$dayHint = Input::get('day');
		$current_date = Input::get('date');
		//exit;
		if($dayHint == "prev")
		{

			/* subtract 1 day on each click of the previous button and convert 
			*the date to jalali then query based on the date and return the result to the contorller.
			*/
			$date = date_create($current_date);
			date_sub($date,date_interval_create_from_date_string("1 days"));
			$prev_date = date_format($date,"Y-m-d");
			$c_date = $prev_date;
			//call the helper function to return the jalali date.
	        $jalali_date = convertToJalali($prev_date);
	        $date = shamsi_gregorian_date($jalali_date);
	        //get Meeting's data from model
			$meetings = Meeting::getDailyMeetings($prev_date);
			// $mSearch = array();
			// $mData=array();
			// foreach ($meetings as $value) {
			// 	$mSearch[] = $value->meeting_start;
			// 	$mData[$value->meeting_start] = array(
			// 		'id'=>$value->id,
			// 		'meeting_start'=>$value->meeting_start,
			// 		'meeting_end'=>$value->meeting_end,
			// 		'meeting_subject'=>$value->meeting_subject,
			// 		'location'=>$value->location,
			// 		'sector' => $value->sector,
			// 		'meeting_type' => $value->meeting_type,
			// 		'arg_media' => $value->arg_media,
			// 		'date' => $value->date,
			// 		'recurring' => $value->recurring,
			// 		'recure_type' => $value->recure_type,
			// 	); 
			// }
			//print_r($meetings);

		}
		else
		{

			/* add 1 day on each click of the next button and convert 
			*the date to jalali then query based on the date and return the result to the contorller.
			*/
			$date = date_create($current_date);
			date_add($date,date_interval_create_from_date_string("1 days"));
			$next_date = date_format($date,"Y-m-d");
			$c_date = $next_date;
			//call the jdatetime library's function to convert the date to shamsi/jalali.
	        $jalali_date = convertToJalali($next_date);
	        $date = shamsi_gregorian_date($jalali_date);
	        //get Meeting's data from model
			$meetings = Meeting::getDailyMeetings($next_date);
			// $mSearch = array();
			// $mData=array();
			// foreach ($meetings as $value) {
			// 	$mSearch[] = $value->meeting_start;
			// 	$mData[$value->meeting_start] = array(
			// 		'id'=>$value->id,
			// 		'meeting_start'=>$value->meeting_start,
			// 		'meeting_end'=>$value->meeting_end,
			// 		'meeting_subject'=>$value->meeting_subject,
			// 		'location'=>$value->location,
			// 		'sector' => $value->sector,
			// 		'meeting_type' => $value->meeting_type,
			// 		'arg_media' => $value->arg_media,
			// 		'date' => $value->date,
			// 		'recurring' => $value->recurring,
			// 		'recure_type' => $value->recure_type,
			// 	); 
			// }
			//echo "<pre>";print_r($meetings);exit;

		}
		
		$data['meetings'] = $meetings;
		//$data['mSearch'] = $mSearch;
		//$data['mData'] = $mData;
		$data['date'] = $date;
		$data['c_date'] = $c_date;
		$data['jalali_date'] = $jalali_date;
		return View::make('sched.dailyMeetingsPrevNext', $data);

	}

	// load the search weekly meetings page.
	public function weeklyMeetings()
	{

		if(date('D') != 'Sat')
		{
		    $static_start = date('Y-m-d',strtotime('last Saturday'));
		    $static_finish = date('Y-m-d',strtotime('next Friday'));

		    if(date('D') == 'Fri')
		    {
		    	$static_finish = date('Y-m-d',strtotime('today'));
		    }
		}else
		{
		    @$static_start = date('Y-m-d', strtotime('today'));
		    @$static_finish = date('Y-m-d',strtotime('next Friday'));
		}

		$start_date = convertToJalali($static_start);
		$end_date = convertToJalali($static_finish);

		// now call the model function to search the Meetings between the given dates.
		$result = Meeting::getWeeklyMeetings($static_start, $static_finish);
		//echo "<pre>";print_r($result);exit;
		$meetings = array(); 

		$a = array();
		$b = array();
		$c = array();
		$d = array();
		$e = array();
		$f = array();
		$g = array();

		foreach($result AS $item)
		{

			$start_time = date("g:i a", strtotime($item->meeting_start));
			$end_time = date("g:i a", strtotime($item->meeting_end));

			$edit = '<td class="noprint" width="6%"><a href='.URL::route('EditMeetings',$item->id).'> اصلاح </a></td>';
			$delete = '<td class="noprint" width="6%"><a href='.URL::route('DeleteMeetings',$item->id).' onclick="javascript:return confirm(\'آیا مطمين هستید ؟\');"> حذف</a></td>';

         	if ($item->date == $static_start) {

				array_push($a, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
			}
			elseif ($item->date == addDaysToDate($static_start, 1)) {
			
				array_push($b, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
			}
			elseif ($item->date == addDaysToDate($static_start, 2)) {

				array_push($c, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
			}
			elseif ($item->date == addDaysToDate($static_start, 3)) {

				array_push($d, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
			}
			elseif ($item->date == addDaysToDate($static_start, 4)) {

				array_push($e, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
			}
			elseif ($item->date == addDaysToDate($static_start, 5)) {
					
				array_push($f, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
			}
			elseif ($item->date == $static_finish) {
				array_push($g, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
			}
		}
		
		$meetings[1] = $a;
		$meetings[2] = $b;
		$meetings[3] = $c;
		$meetings[4] = $d;
		$meetings[5] = $e;
		$meetings[6] = $f;
		$meetings[7] = $g;
		
		// change the format of the jalai date to be shown with the week days.
		$j_day_date = jdatetime::date($start_date,true,true);
		$j_day_date = shamsi_date_format($j_day_date);
		$g_day_date = addGregorianDays($start_date,'0');

		//echo "<pre>";print_r($meetings);exit;
    	$data['start_date'] = jalali_format($start_date);
    	$data['end_date'] = jalali_format($end_date);
    	$data['weeklyMeetings'] = $meetings; 
    	$data['j_day_date'] = $j_day_date;
    	$data['g_day_date'] = $g_day_date;

		return View::make('sched.weeklyMeetings', $data);

		//return View::make('sched.search');

	}

	// get the previous and next week's meetings;
	public function weeklyMeetingsPrevNext()
	{

		$weekHint = Input::get('week');
		$s_date = Input::get('s_date');
		$e_date = Input::get('e_date');
		//echo "<pre>";print_r($_POST);
		if($weekHint == "prev")
		{

			$date = strtotime($s_date);
		    $date = strtotime("-7 day", $date);
		    $start_date = date('Y-m-d', $date);
			
			// now convert the given gregorian date to jalali to show the particular month start and end date and year at top of the table
			$j_start_date = convertToJalali($start_date);	

			$date = strtotime($s_date);
		    $date = strtotime("-1 day", $date);
		    $end_date = date('Y-m-d', $date);

		    // get the start_date gregorian to send back as an hidden field to the view for sending back on click of prev button.
			$s_date = $start_date;	
			// get the end_date gregorian to send back as an hidden field to the view for sending back on click of prev button.
			$e_date = $end_date;
			// now convert the given gregorian date to jalali to show the particular month start and end date and year at top of the table
			$j_end_date = convertToJalali($end_date);

		}		
		else
		{

			$date = strtotime($e_date);
		    $date = strtotime("+1 day", $date);
		    $start_date = date('Y-m-d', $date);
			// now convert the given gregorian date to jalali to show the particular month start and end date and year at top of the table
			$j_start_date = convertToJalali($start_date);		
	
			// get the end_date gregorian to send back as an hidden field to the view for sending back on click of prev button.
			$date = strtotime($e_date);
		    $date = strtotime("+7 day", $date);
		    $end_date = date('Y-m-d', $date);

		    // get the start_date gregorian to send back as an hidden field to the view for sending back on click of prev button.
			$s_date = $start_date;	
			// get the end_date gregorian to send back as an hidden field to the view for sending back on click of prev button.
			$e_date = $end_date;
			// now convert the given gregorian date to jalali to show the particular month start and end date and year at top of the table
			$j_end_date = convertToJalali($end_date);	


		}
		// now call the model function to search the Meetings between the given dates.
		$result = Meeting::whereBetween('date', array($start_date, $end_date))
				//->orWhere('recurring', 1)
				//->where('recure_type', 2)
				->orderBy('meeting_start')
				->get();
		//echo "<pre>";print_r($result);exit;
		$meetings = array(); 

		$a = array();
		$b = array();
		$c = array();
		$d = array();
		$e = array();
		$f = array();
		$g = array();

		if(!empty($result))
		{
			foreach($result AS $item)
			{

				$start_time = date("g:i a", strtotime($item->meeting_start));
				$end_time = date("g:i a", strtotime($item->meeting_end));

				$edit = '<td class="noprint" width="6%"><a href='.URL::route('EditMeetings',$item->id).'> اصلاح </a></td>';
				$delete = '<td class="noprint" width="6%"><a href='.URL::route('DeleteMeetings',$item->id).' onclick="javascript:return confirm(\'آیا مطمين هستید ؟\');"> حذف</a></td>';

	         	if ($item->date == $start_date) {

					array_push($a, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
				}
				elseif ($item->date == addDaysToDate($start_date, 1)) {
				
					array_push($b, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
				}
				elseif ($item->date == addDaysToDate($start_date, 2)) {

					array_push($c, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
				}
				elseif ($item->date == addDaysToDate($start_date, 3)) {

					array_push($d, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
				}
				elseif ($item->date == addDaysToDate($start_date, 4)) {

					array_push($e, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
				}
				elseif ($item->date == addDaysToDate($start_date, 5)) {
						
					array_push($f, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
				}
				elseif ($item->date == $end_date) {
					array_push($g, '<a href='.URL::route('EditMeetings',array($item->id,"week")).' title="اصلاح"><span style="font-size:14px">'.$start_time." - ".$end_time.'</span></a><br /><span style="font-size:14px;font-family:tahoma" title="'.nl2br($item->meeting_subject).'">'.nl2br($item->meeting_subject)."</span>");
				}

			}
		}
		
		$meetings[0] = $a;
		$meetings[1] = $b;
		$meetings[2] = $c;
		$meetings[3] = $d;
		$meetings[4] = $e;
		$meetings[5] = $f;
		$meetings[6] = $g;

		// change the format of the jalai date to be shown with the week days.
		$j_day_date = jdatetime::date($j_start_date,true,true);
		$j_day_date = shamsi_date_format($j_day_date);
		$g_day_date = addGregorianDays($j_start_date,'0');
		
    	$data['date'] = $date; 
    	$data['j_day_date'] = $j_day_date; 
    	$data['g_day_date'] = $g_day_date; 
    	$data['s_date'] = $s_date; 
    	$data['e_date'] = $e_date; 
    	$data['j_start_date'] = jalali_format($j_start_date); 
    	$data['j_end_date'] = jalali_format($j_end_date); 
    	$data['weeklyMeetings'] = $meetings; 

		return View::make('sched.weeklyMeetingsPrevNext', $data);

	}

	// load the search weekly meetings page.
	public function monthlyMeetings()
	{

		// get current month's start and end date and convert it into jalali.
		//$start_date = date("Y-m-01",strtotime(date("Y-m-d")));
		//$c_date = $start_date;
		//$end_date = date("Y-m-t", strtotime(date("Y-m-d")));
		$current_date = date("Y-m-d");
		//echo $start_date." ".$end_date."<br />";
		$start_date = jdatetime::date("Y-m-01", strtotime($current_date),false);
		$end_date = jdatetime::date("Y-m-t", strtotime($current_date),false);
		// now call the model function to search the Meetings between the given dates.
		$result = Meeting::whereBetween('date', array($start_date, $end_date))->get();

		$meetings = array(); 

		$a = array();
		$b = array();
		$c = array();
		$d = array();
		$e = array();
		$f = array();
		$g = array();

		foreach($result AS $item)
		{

			$s_time = date("g:i A", strtotime($item->meeting_start));
			$e_time = date("g:i A", strtotime($item->meeting_end));
			$start_time = jdatetime::date($s_time,true,true,'Asia/Kabul');
        	$end_time = jdatetime::date($e_time,true,true,'Asia/Kabul');

        	$edit = '<td class="noprint" width="6%"><a href='.URL::route('EditMeetings',$item->id,"week").'> اصلاح </a></td>';
			$delete = '<td class="noprint" width="6%"><a href='.URL::route('DeleteMeetings',$item->id).' onclick="javascript:return confirm(\'آیا مطمين هستید ؟\');"> حذف</a></td>';

        	$date = jdatetime::date($item->date,true,true);
        	$date = shamsi_date_format($date);

			switch ($item->week_day) {
				case 1:
					array_push($a, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
				case 2:
					array_push($b, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
				case 3:
					array_push($c, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
				case 4:
					array_push($d, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
				case 5:
					array_push($e, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
				case 6:
					array_push($f, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
				case 7:
					array_push($g, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
			}
		}
		
		$meetings[1] = $a;
		$meetings[2] = $b;
		$meetings[3] = $c;
		$meetings[4] = $d;
		$meetings[5] = $e;
		$meetings[6] = $f;
		$meetings[7] = $g;

		//echo "<pre>";print_r($meetings);exit;
		if($result)
        {
        	$start_date = jalali_format($start_date);
        	$end_date = jalali_format($end_date);
			return View::make('sched.monthlyMeetings')->with('start_date', $start_date)->with('end_date', $end_date)->with('monthlyMeetings', $meetings);
        }
        else
        {
			return View::make('sched.monthlyMeetings')->with('fail', "موردی پیدا نشد");
        }

		//return View::make('sched.search');

	}

	// find the current month's previous and next months and then return to the view.
	public function monthlyMeetingsPrevNext()
	{


		$monthHint = Input::get('month');
		$current_date = Input::get('date');

		if($monthHint == "prev")
		{
			$start_date = date("Y-m-01",strtotime($current_date.' -1 month'));
			$c_date = $start_date;
			$end_date = date("Y-m-t", strtotime($start_date));
			// convert the dates to Jalali
			$start_date = jdatetime::date("Y-m-01", strtotime($start_date.' +1 month'),false);
			$end_date = jdatetime::date("Y-m-t", strtotime($end_date),false);

		}
		else
		{
			
			// $j=convertToJalali($current_date);

			// $jd=explode("-", $j);

			// $m=$jd[1]+1;
			// if($m > 12)
			//  {
			//  	$year = $jd[0] + 1;
			//  	$m='1';
			//  } else {
			//  $year = $jd[0];
			//  }
			// $sd=$year.'-0'.$m.'-01';
			// //$m=$m + 1;
			// $ed=$year.'-0'.$m.'-01';
			// $gd=convertToGregorian($ed);
			// $c_date = $gd;
			// $gdn= strtotime($gd.' +1 month');
			// $gd2=date('Y-m-d', $gdn);
			// $gdn= strtotime($gd2.' -1 day');
			// $gd=date('Y-m-d', $gdn);

			// $ed=convertToJalali($gd);
			// $start_date = $sd;
			// $end_date = $ed;
			// echo $sd.' - '.$ed; 
			$start_date = date("Y-m-01",strtotime($current_date.' +1 month'));
			$c_date = $start_date;
			$end_date = date("Y-m-t", strtotime($current_date));
			// convert the dates to Jalali
			$start_date = jdatetime::date("Y-m-01", strtotime($current_date.' +1 month'),false);
			$end_date = jdatetime::date("Y-m-t", strtotime($current_date.' +1 month'),false);
			
		}
		

		// now call the model function to search the Meetings between the given dates.
		$result = Meeting::whereBetween('date', array($start_date, $end_date))->get();
		//print_r($result);
		$meetings = array(); 

		$a = array();
		$b = array();
		$c = array();
		$d = array();
		$e = array();
		$f = array();
		$g = array();

		foreach($result AS $item)
		{

			$start_time = date("g:i A", STRTOTIME($item->meeting_start));
			$end_time = date("g:i A", STRTOTIME($item->meeting_end));
			if($item->meeting_end == "00:00:00")
			{
				$end_time = '';
			}

			$edit = '<td class="noprint" width="6%"><a href='.URL::route('EditMeetings',$item->id).'> اصلاح </a></td>';
			$delete = '<td class="noprint" width="6%"><a href='.URL::route('DeleteMeetings',$item->id).' onclick="javascript:return confirm(\'آیا مطمين هستید ؟\');"> حذف</a></td>';
			
			$date = jdatetime::date($item->date,true,true);
        	$date = shamsi_date_format($date);

			switch ($item->week_day) {
				case 1:
					array_push($a, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
				case 2:
					array_push($b, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
				case 3:
					array_push($c, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
				case 4:
					array_push($d, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
				case 5:
					array_push($e, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
				case 6:
					array_push($f, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
				case 7:
					array_push($g, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time.'_@_'.$edit.'_@_'.$delete);
					break;
			}
		}
		
		$meetings[1] = $a;
		$meetings[2] = $b;
		$meetings[3] = $c;
		$meetings[4] = $d;
		$meetings[5] = $e;
		$meetings[6] = $f;
		$meetings[7] = $g;
		//echo $start_date." ".$end_date;exit;
		// get the month difference date in jalali and gregorian.
		$date = shamsi_gregorian_months($start_date, $end_date);
           
		//echo "<pre>";print_r($meetings);exit;
		// if($result)
  //       {
		// 	return View::make('sched.weeklyMeetingsPrevNext')->with('date', $date)->with('c_date', $c_date)->with('weeklyMeetings', $meetings);
  //       }
  //       else
  //       {
		// 	return View::make('sched.weeklyMeetings')->with('fail', "موردی پیدا نشد");
  //       }

		$monthlyMeetings = array_filter($meetings);

		if(!$monthlyMeetings == '')
		{

			$out = "<i>".$date."</i>";
			$out .= "<input type='hidden' id='date' value='".$c_date."' />";
			$out .= '<table class="table table-bordered" style="margin-top 15px" dir="rtl" id="list_of_weeklyMeetings">';
            $out .= "<thead><tr>";
              
            $out .= "<th>تاریخ</th><th>شنبه</th><th>یکشنبه</th>";
            $out .= "<th>دوشنبه</th><th>سه شنبه</th><th>چهارشنبه</th><th>پنجشنبه</th><th>جمعه</th><th colspan='2'>عملیات</th>";  
              
            $out .= "</tr></thead><tbody>";
			$counter = 0;
			foreach($meetings AS $item)
			{
	                
	            $counter++;
	            if(count($meetings[$counter])==0)
	            {
	                continue;
	            }
	            
	            $meetings_count = $meetings[$counter];

	            for($i=0;$i<count($meetings_count);$i++)
	            {
	                
	                $row = $meetings_count[$i];
	                $row = explode("_@_", $row);
	 
	                $out .= "<tr>";
	                    
	                    $out .= "<td class='fixed'>".jdatetime::date($row[1],true,true,'Asia/Kabul')."</td>";
	                    // $out .= "<td class='fixed'>".jdatetime::date($row[2],true,true,'Asia/Kabul')."</td>";
	                    // $out .= "<td class='fixed'>".jdatetime::date($row[3],true,true)."</td>";
	                    
	                    if($counter == 1)
	                    {
	                        $out .= "<td class='fixed'>".$row[0]."</td>";
	                        $out .= "<td></td><td></td><td></td><td></td><td></td><td></td>";
	                    }  
	                    elseif($counter == 2)
	                        $out .= "<td></td><td class='fixed'>".$row[0]."</td><td></td><td></td><td></td><td></td><td></td>";
	                        
	                    elseif($counter == 3)
	                    	$out .= "<td></td><td></td><td class='fixed'>".$row[0]."</td><td></td><td></td><td></td><td></td>";
	                        
	                    elseif($counter == 4)
	                    	$out .= "<td></td><td></td><td></td><td class='fixed'>".$row[0]."</td><td></td><td></td><td></td>";
	                        
	                    elseif($counter == 5)
	                        $out .= "<td></td><td></td><td></td><td></td><td class='fixed'>".$row[0]."</td><td></td><td></td>";
	                       
	                    elseif($counter == 6)
	                    	$out .= "<td></td><td></td><td></td><td></td><td></td><td class='fixed'>".$row[0]."</td><td></td>";
	                        
	                    else
	                        $out .= "<td></td><td></td><td></td><td></td><td></td><td></td><td class='fixed'>".$row[0]."</td>";

	                    $out .= $row[4];
	                    $out .= $row[5];
	                $out .= "</tr>";
	            } 
	            
	        }
	        $out .= "</tbody></table>";
	        echo $out;
    	}
        else
        {
        	$out = "<i>".$date."</i>";
        	$out .= "<input type='hidden' id='date' value='".$c_date."' />";
			$out .= '<table class="table table-bordered" style="margin-top 15px" dir="rtl" id="list_of_weeklyMeetings">';
            $out .= "<thead><tr>";
              
                $out .= "<th>تاریخ</th><th>شنبه</th><th>یکشنبه</th>";
                $out .= "<th>دوشنبه</th><th>سه شنبه</th><th>چهار شنبه</th><th>پنج شنبه</th><th>جمعه</th><th colspan='2'>عملیات</th>";  
              
            $out .= "</tr></thead><tbody><tr>";
                $out .= "<td colspan='10'><div class='alert alert-danger span6' style='text-align:center'>جلسات برای این ماه در دیتابیس اضافه نگردیده است !</div></td>";
            $out .= "</tr></tbody></table>";
	        echo $out;
        }

	}


	// search meetings between specific week and return the output.
	public function searchResult()
	{

		//validate the input fields
	    $validates = Validator::make(Input::all(),array(

	        "from"		=> "required",
			"to"		=> "required"

	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return Redirect::route("weeklyMeetingsSearch")->withErrors($validates)->withInput();
	    }
	    else
	    {
	    	//print_r($_POST);exit;
			$start_date = Input::get('from');
			$end_date = Input::get('to');

			// change the default format which is in jalali to gregorian using helper functions.
			$jalali_start_date = gregorian_format($start_date);
			$jalali_end_date = gregorian_format($end_date);

			// now call the model function to search the Meetings between the given dates.
			//$result = Meetings::getWeeklyMeetings($start_date, $end_date);
			$result = Meeting::whereBetween('date', array($jalali_start_date, $jalali_end_date))->get();
			// $queries = DB::getQueryLog();
			// $last_query = end($result);
			//print_r($result);exit;

			$meetings = array(); 

			$a = array();
			$b = array();
			$c = array();
			$d = array();
			$e = array();
			$f = array();
			$g = array();

			foreach($result AS $item)
			{

				$start_time = date("g:i A", STRTOTIME($item->meeting_start));
				$end_time = date("g:i A", STRTOTIME($item->meeting_end));

				$date = jalali_format($item->date);

				switch ($item->week_day) {
					case 1:
						array_push($a, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time);
						break;
					case 2:
						array_push($b, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time);
						break;
					case 3:
						array_push($c, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time);
						break;
					case 4:
						array_push($d, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time);
						break;
					case 5:
						array_push($e, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time);
						break;
					case 6:
						array_push($f, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time);
						break;
					case 7:
						array_push($g, $item->meeting_with.'_@_'.$date.'_@_'.$start_time.'_@_'.$end_time);
						break;
				}
			}
			
			$meetings[1] = $a;
			$meetings[2] = $b;
			$meetings[3] = $c;
			$meetings[4] = $d;
			$meetings[5] = $e;
			$meetings[6] = $f;
			$meetings[7] = $g;

			//echo "<pre>";print_r($meetings);exit;
			if($result)
	        {
				return View::make('sched.weeklyMeetings')->with('start_date', $start_date)->with('end_date', $end_date)->with('weeklyMeetings', $meetings);
	        }
	        else
	        {
				return View::make('sched.weeklyMeetings')->with('fail', "موردی پیدا نشد");
	        }
		}
	}

	//Load President's all meetings log list view
	public function getMeetingsLog()
	{
		$data['records'] = Meeting::getAllLogs();
		if(Input::get('ajax') == 1)
		{
			return View::make('sched.meeting_log.meetings_log_ajax',$data);
		}
		else
		{
			//load view to show searchpa result
			return View::make('sched.meeting_log.meetings_log',$data);
		}
	}

	// Get all President's meetings log for dataTable.
	public function searchMeetingLog()
	{

        // Tayyeb Changes
		//get Meeting's data from model
		$object = Meeting::getAllLogs();
		dd($object);
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'meeting_start',
                            'meeting_end',
                            // 'meeting_title',
							'meeting_subject',
							'meeting_with',
							'location',
							'date',
							'action',
							'user',
							'updated_at'
							)->make();
		//print_r($logs->get());exit;
	}

	//Load President's all meetings in calendar view
	public function showMeetingsInCalendar()
	{
		$meetings = Meeting::getMeetingsForCalendar();
		//echo "<pre>";print_r($meetings);exit;
		$records = array();
		foreach ($meetings as $item) 
		{

			$date = date('d M Y',strtotime($item->date));
			$start_time = $item->meeting_start;
			$end_time = $item->meeting_end;
			$end_time = date('g:i a', strtotime($end_time));
			$end_time = explode(" ", $end_time);
			if($end_time[1] == "am")
			{
				$end_time = $end_time[0]."a";
			}
			else
			{
				$end_time = $end_time[0]."p";
            }
            // Tayyeb Changes
            // $meeting_title = $item->meeting_title;
			$meeting_subject = $item->meeting_subject;
			$g_date = $item->date;
			// $date = explode("-", $date);

			// $g_date = jdatetime::toGregorian($date[0], $date[1], $date[2]);

			// $year = $g_date[0];
			// $month = $g_date[1];
			// $day = $g_date[2];

			// // add leading zero to month and day of the date.
	  //       $month = str_pad($month,2,0, STR_PAD_LEFT);
	  //       $day = str_pad($day,2,0, STR_PAD_LEFT);

			// $g_date = $year."-".$month."-".$day;
			//$label = "label-success";
            // Tayyeb Changes
            // $record
			$record['title'] = $date.'  |  '.$end_time." - \n".$meeting_subject;
            $record['start'] = $g_date.' '.$start_time;//$g_date.', '.$start_time[0].', '.$start_time[1];
			//dd($record['start']);
            //$record['end'] = $g_date.' '.$end_time;
			//$record['className'] = $label;
			$record['allDay'] = false;

			array_push($records, $record);
		}
		$meetings_data = json_encode($records);
		//echo "<pre>";print_r($meetings_data);exit;
		return View::make('sched.calendar')->with('meetings_data', $meetings_data);
	}

	public function deleteMeetings($id = 0)
	{
		$deleted = Meeting::getDeleteMeeting($id);
		//dd($deleted);
		if($deleted)
	    {
			return Redirect::route("getMeetings")->with("success","جلسه موفقانه حذف گردید");   	
        }
        else
        {
            return Redirect::route("getMeetings")->with("fail","در حذف جلسه مشکل وجود دارد، لطفآ با مسئول دیتابیس در میان بگذارید");
        }

	}
	// delete the meetings which has the recurrence option.
	public function deleteRecurredMeetings($id = 0)
	{
		$deleted = Meeting::getDeleteReccuredMeeting($id);
		//dd($deleted);
		if($deleted)
	    {
			return Redirect::route("getMeetings")->with("success","جلسه موفقانه حذف گردید");   	
        }
        else
        {
            return Redirect::route("getMeetings")->with("fail","در حذف جلسه مشکل وجود دارد، لطفآ با مسئول دیتابیس در میان بگذارید");
        }

	}

	public function getDownloadMeetingAttachment($file_id)
	{
		$file_id = Crypt::decrypt($file_id);
		//get file name from database
		$fileName = Meeting::getFileName($file_id);
		//dd($fileName);
        //public path for file
        $file = public_path(). "/documents/scheduling_attachments/".$fileName->file_name;
        //download file
        return Response::download($file, $fileName->original_file_name);
	}

	public function removeSchedulingFile()
	{
		$file_id = Input::get('file_id');
		$file_name = Input::get('file_name');
		$deleted = Meeting::removeFile($file_id);
		if($deleted)
		{
			File::delete(public_path()."/scheduling_attachments/$file_name");
			return 1;
		}
	}

	//========================================================= Delayed Meetings Controller Functions ==================================================

	public function delayedMeetingsList()
	{
		$data['records'] = Meeting::getAllDelayedMeetings();
		if(Input::get('ajax') == 1)
		{
			return View::make('sched.delayed_meetings.delayed_meetings_ajax',$data);
		}
		else
		{
			//load view to show searchpa result
			return View::make('sched.delayed_meetings.delayed_meetings',$data);
		}
	}

}

?>