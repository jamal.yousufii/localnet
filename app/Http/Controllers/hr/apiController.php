<?php

namespace App\Http\Controllers\hr;

use App\Http\Controllers\Controller;
use DB;
use Response;
use Input;
use Auth;
use Session;
use Cookie;

class apiController extends Controller
{
  	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
  }
  
  public function getDepartments() {
    // return Input::get('user_name');
    // 'username' => 'ali.panahi',
    //         'password' => 'test@123'
    $user = Input::get('username');
    $password = Input::get('password');
    
    if(Auth::attempt(['username' => $user, 'password' => $password])) {
      return Response::json(DB::connection('mysql')->table('department')->where('unactive', 0)->where('name', '!=', '')->get(), 200); // Status code here
    }
  }


}