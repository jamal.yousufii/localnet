<?php 

namespace App\Http\Controllers\procurement;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\models\procurement\procurementModel;
use Illuminate\Support\Collection;
use App\library\jdatetime;
use View;
use DB;
use Validator;
use Input;
use Redirect;
use Response;
use Datatable;
use Session;
use DateTime;
use DateInterval;
use File;
use Illuminate\Support\Str;
use URL;
use Carbon\Carbon;

class procurementController extends Controller
{
		
	//================================================== Goods Purchase Controller Functions =====================================

	//Load Procurement Dashboard.
	public function homePage()
	{
		return View::make('procurement.dashboard');
	}
	//Load Purchase list view
	public function loadPurchaseList()
	{
		return View::make('procurement.purchaseList');
	}
	//get purchase data for datatable.
	public function getAllPurchases()
	{
		//get all data 
		$object = procurementModel::getPurchaseData("");//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		if(goodsRoleCheck('procurement_purchase_list'))
		{
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'date_of_preparation',
								'executive_department',
								'goods_detail',
								'request_no',
								'date_of_submission_to_the_purchasing_committee',
								'lowest_price',
								'purchasing_team',
								'winner_company',
								'm3_no',
								'date_of_submission_to_the_finance_department',
								'm3_no_sent_to_finance',
								'amount_sent_to_finance'
								)
					->addColumn('date_of_preparation', function($option){
						$date_of_preparation = checkEmptyDate($option->date_of_preparation);
						return $date_of_preparation;
					})
					->addColumn('date_of_submission_to_the_finance_department', function($option){
						$date_of_submission_to_the_finance_department = checkEmptyDate($option->date_of_submission_to_the_finance_department);
						return $date_of_submission_to_the_finance_department;
					})
					->addColumn('date_of_submission_to_the_purchasing_committee', function($option){
						$date_of_submission_to_the_purchasing_committee = checkEmptyDate($option->date_of_submission_to_the_purchasing_committee);
						return $date_of_submission_to_the_purchasing_committee;
					})
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('procurement_purchase_list'))
							$options .= '<a href="'.route('getLoadPurchaseEditForm',$option->id).'" title="Edit Purchase"><i class="fa fa-edit"></i></a> | '; 
						if(canDelete('procurement_purchase_list'))
							$options .= '<a href="'.route('postPurchaseDelete',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Purchase"><i class="fa fa-trash-o"></i></a> | ';
						if(canView('procurement_purchase_list')){
							$options .= '<a href="'.route('loadPurchaseDetails',$option->id).'" title="View Purchase details"><i class="fa fa-eye"></i></a>';
						}
						return $options;
					})
					// ->orderColumns('id','id')
					->make();
		}
		elseif(financeGoodsRoleCheck('procurement_purchase_list'))
		{
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'm3_no',
								'm3_no_sent_to_finance',
								'amount_sent_to_finance',
								'date_of_sent_to_finance'
								)
					->addColumn('date_of_sent_to_finance', function($option){
						$date_of_sent_to_finance = checkEmptyDate($option->date_of_sent_to_finance);
						return $date_of_sent_to_finance;
					})
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('procurement_purchase_list'))
							$options .= '<a href="'.route('getLoadPurchaseEditForm',$option->id).'" title="Edit Purchase"><i class="fa fa-edit"></i></a> | '; 
						if(canDelete('procurement_purchase_list'))
							$options .= '<a href="'.route('postPurchaseDelete',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Purchase"><i class="fa fa-trash-o"></i></a> | ';
						if(canView('procurement_purchase_list')){
							$options .= '<a href="'.route('loadPurchaseDetails',$option->id).'" title="View Purchase details"><i class="fa fa-eye"></i></a>';
						}
						return $options;
					})
					// ->orderColumns('id','id')
					->make();
		}
		else
		{
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'date_of_preparation',
								'executive_department',
								'goods_detail',
								'request_no',
								'purchasing_team',
								'lowest_price',
								'winner_company',
								'm3_no',
								'no_of_letter_sent_to_stock'
								)
					->addColumn('date_of_preparation', function($option){
						$date_of_preparation = checkEmptyDate($option->date_of_preparation);
						return $date_of_preparation;
					})
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('procurement_purchase_list'))
						{
							$options .= '<a href="'.route('getLoadPurchaseEditForm',$option->id).'" title="Edit Purchase"><i class="fa fa-edit"></i></a> | '; 
						}
						if(canDelete('procurement_purchase_list'))
						{
							$options .= '<a href="'.route('postPurchaseDelete',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Purchase"><i class="fa fa-trash-o"></i></a> | ';
						}
						if(canView('procurement_purchase_list'))
						{
							$options .= '<a href="'.route('loadPurchaseDetails',$option->id).'" title="View Purchase details"><i class="fa fa-eye"></i></a>';
						}
						return $options;
					})
					// ->orderColumns('id','id')
					->make();
		}
	}
	//load the searched purchases based on purchasing team.
	public function loadSearchedPurchaseList()
	{
		//get the value of the purchasing team dropdown if it's been chosen.
		$data['purchasing_team'] = Input::get('purchasing_team');
		return View::make('procurement.searchedPurchaseList', $data);
	}
	//If the purchasing team has been chosed from dropdown get the purchase details based on that.
	public function getSearchedPurchases($purchasing_team)
	{
		//get all data 
		$object = procurementModel::getPurchaseData($purchasing_team);//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		if(goodsRoleCheck('procurement_purchase_list'))
		{
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'date_of_preparation',
								'executive_department',
								'goods_detail',
								'request_no',
								'date_of_submission_to_the_purchasing_committee',
								'lowest_price',
								'purchasing_team',
								'winner_company',
								'm3_no',
								'date_of_submission_to_the_finance_department',
								'm3_no_sent_to_finance',
								'amount_sent_to_finance'
								)
					// ->addColumn('date_of_arrival', function($option){
					// 	$date_of_arrival = checkEmptyDate($option->date_of_arrival);
					// 	return $date_of_arrival;
					// })
					->addColumn('date_of_preparation', function($option){
						$date_of_preparation = checkEmptyDate($option->date_of_preparation);
						return $date_of_preparation;
					})
					->addColumn('date_of_submission_to_the_finance_department', function($option){
						$date_of_submission_to_the_finance_department = checkEmptyDate($option->date_of_submission_to_the_finance_department);
						return $date_of_submission_to_the_finance_department;
					})
					->addColumn('date_of_submission_to_the_purchasing_committee', function($option){
						$date_of_submission_to_the_purchasing_committee = checkEmptyDate($option->date_of_submission_to_the_purchasing_committee);
						return $date_of_submission_to_the_purchasing_committee;
					})
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('procurement_purchase_list'))
							$options .= '<a href="'.route('getLoadPurchaseEditForm',$option->id).'" title="Edit Purchase"><i class="fa fa-edit"></i></a> | '; 
						if(canDelete('procurement_purchase_list'))
							$options .= '<a href="'.route('postPurchaseDelete',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Purchase"><i class="fa fa-trash-o"></i></a> | ';
						if(canView('procurement_purchase_list')){
							$options .= '<a href="'.route('loadPurchaseDetails',$option->id).'" title="View Purchase details"><i class="fa fa-eye"></i></a>';
						}
						return $options;
					})
					// ->orderColumns('id','id')
					->make();
		}
		else
		{
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'date_of_preparation',
								'executive_department',
								'goods_detail',
								'request_no',
								'purchasing_team',
								'lowest_price',
								'winner_company',
								'm3_no',
								'no_of_letter_sent_to_stock'
								)
					// ->addColumn('date_of_arrival', function($option){
					// 	$date_of_arrival = checkEmptyDate($option->date_of_arrival);
					// 	return $date_of_arrival;
					// })
					->addColumn('date_of_preparation', function($option){
						$date_of_preparation = checkEmptyDate($option->date_of_preparation);
						return $date_of_preparation;
					})
					// ->addColumn('date_of_submission_to_the_finance_department', function($option){
					// 	$date_of_submission_to_the_finance_department = checkEmptyDate($option->date_of_submission_to_the_finance_department);
					// 	return $date_of_submission_to_the_finance_department;
					// })
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('procurement_purchase_list'))
						{
							$options .= '<a href="'.route('getLoadPurchaseEditForm',$option->id).'" title="Edit Purchase"><i class="fa fa-edit"></i></a> | '; 
						}
						if(canDelete('procurement_purchase_list'))
						{
							$options .= '<a href="'.route('postPurchaseDelete',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Purchase"><i class="fa fa-trash-o"></i></a> | ';
						}
						if(canView('procurement_purchase_list'))
						{
							$options .= '<a href="'.route('loadPurchaseDetails',$option->id).'" title="View Purchase details"><i class="fa fa-eye"></i></a>';
						}
						return $options;
					})
					// ->orderColumns('id','id')
					->make();
		}
	}
	// load the Purchase entry form.
	public function loadPurchaseForm()
	{
		return View::make('procurement.purchaseForm');
	}

	// insert the Goods Purchase form data to the table.
	public function addPurchaseFormData()
	{
		//dd($_POST);
		if(canAdd('procurement_purchase_list'))
		{
		    if(Input::get('date_of_preparation') != '')
		    	@$date_of_preparation = toGregorian(ymd_format(Input::get('date_of_preparation')));
		    else
		    	$date_of_preparation = "";
		    if(Input::get('date_of_arrival') != '')
		    	@$date_of_arrival = toGregorian(ymd_format(Input::get('date_of_arrival')));
		    else
		    	$date_of_arrival = "";
		    if(Input::get('date_of_submission_to_the_purchasing_committee') != '')
		    	@$date_of_submission_to_the_purchasing_committee = toGregorian(ymd_format(Input::get('date_of_submission_to_the_purchasing_committee')));
		    else
		    	$date_of_submission_to_the_purchasing_committee = "";
		    if(Input::get('date_of_m3') != '')
		    	@$date_of_m3 = toGregorian(ymd_format(Input::get('date_of_m3')));
		    else
		    	$date_of_m3 = "";
		    if(Input::get('date_of_submission_to_stock') != '')
		    	@$date_of_submission_to_stock = toGregorian(ymd_format(Input::get('date_of_submission_to_stock')));
		    else
		    	$date_of_submission_to_stock = "";
			
		    $date_of_submission_to_the_finance_department	= '';
		    $m3_no_sent_to_finance				= '';

		    // get the form data in array;
		    $purchaseData = array(
		    			'date_of_preparation'								=> $date_of_preparation,
		    			'end_user'											=> Input::get('end_user'),
		    			'goods_detail'										=> Input::get('goods_detail'),
		    			'request_no'										=> Input::get('request_no'),
		    			'date_of_arrival'									=> $date_of_arrival,
		    			'date_of_submission_to_the_purchasing_committee'	=> $date_of_submission_to_the_purchasing_committee,
		    			'purchasing_team'									=> Input::get('purchasing_team'),
		    			'lowest_price'										=> Input::get('lowest_price'),
		    			'winner_company'									=> Input::get('winner_company'),
		    			'm3_no'												=> Input::get('m3_no'),
		    			'date_of_m3'										=> $date_of_m3,
		    			'no_of_letter_sent_to_stock'						=> Input::get('no_of_letter_sent_to_stock'),
		    			'date_of_submission_to_stock'						=> $date_of_submission_to_stock,
						'date_of_submission_to_the_finance_department'		=> $date_of_submission_to_the_finance_department,
						'm3_no_sent_to_finance'								=> $m3_no_sent_to_finance,
		    			'remarks'											=> Input::get('remarks'),
		    			'user_id'											=> Auth::user()->id
		    	);

		    // now insert the form data to its table through calling the model function.
		    $object = procurementModel::addPurchaseDetails($purchaseData);

		    if($object)
		    {
		    	// get the the log data and insert it into the log table.
				$purchaseLog = array(

						'date_of_preparation'								=> $date_of_preparation,
		    			'end_user'											=> Input::get('end_user'),
		    			'goods_detail'										=> Input::get('goods_detail'),
		    			'request_no'										=> Input::get('request_no'),
		    			'date_of_arrival'									=> $date_of_arrival,
		    			'date_of_submission_to_the_purchasing_committee'	=> $date_of_submission_to_the_purchasing_committee,
		    			'purchasing_team'									=> Input::get('purchasing_team'),
		    			'lowest_price'										=> Input::get('lowest_price'),
		    			'winner_company'									=> Input::get('winner_company'),
		    			'm3_no'												=> Input::get('m3_no'),
		    			'date_of_m3'										=> $date_of_m3,
		    			'no_of_letter_sent_to_stock'						=> Input::get('no_of_letter_sent_to_stock'),
		    			'date_of_submission_to_stock'						=> $date_of_submission_to_stock,
						'date_of_submission_to_the_finance_department'		=> $date_of_submission_to_the_finance_department,
						'm3_no_sent_to_finance'								=> $m3_no_sent_to_finance,
		    			'remarks'											=> Input::get('remarks'),
		    			'user'												=> Auth::user()->username,
		    			'action'											=> 'Purchase Added'

					);
				procurementModel::addLogData('goods_purchase_log', $purchaseLog);
		    	return Redirect::route("goodsPurchaseList")->with("success","The Purchase details successfully added.");
	        }
	        else
	        {
	            return Redirect::route("goodsPurchaseList")->with("fail","An error occured please try again or contact system developer.");
	        }
	    }
	    else{
	    	return showWarning();
	    }
	}

	// load the Purchase Edit form.
	public function loadPurchaseEditForm($id=0)
	{
		$data['purchase'] = procurementModel::getSpecificPurchaseData($id);
		return View::make('procurement.editPurchase', $data);
	}

	// update the Goods Purchase form data.
	public function postEditPurchase($id=0)
	{
		//dd($_POST);
		if(canEdit('procurement_purchase_list'))
		{
		    if(Input::get('date_of_preparation') != '')
		    	@$date_of_preparation = toGregorian(ymd_format(Input::get('date_of_preparation')));
		    else
		    	$date_of_preparation = "";
		    if(Input::get('date_of_arrival') != '')
		    	@$date_of_arrival = toGregorian(ymd_format(Input::get('date_of_arrival')));
		    else
		    	$date_of_arrival = "";
		    if(Input::get('date_of_submission_to_the_purchasing_committee') != '')
		    	@$date_of_submission_to_the_purchasing_committee = toGregorian(ymd_format(Input::get('date_of_submission_to_the_purchasing_committee')));
		    else
		    	$date_of_submission_to_the_purchasing_committee = "";
		    if(Input::get('date_of_m3') != '')
		    	@$date_of_m3 = toGregorian(ymd_format(Input::get('date_of_m3')));
		    else
		    	$date_of_m3 = "";
		    if(Input::get('date_of_submission_to_stock') != '')
		    	@$date_of_submission_to_stock = toGregorian(ymd_format(Input::get('date_of_submission_to_stock')));
		    else
		    	$date_of_submission_to_stock = "";
		    if(Input::get('date_of_submission_to_the_finance_department') != '')
		    	@$date_of_submission_to_the_finance_department = toGregorian(ymd_format(Input::get('date_of_submission_to_the_finance_department')));
		    else
		    	$date_of_submission_to_the_finance_department = "";
		    if(Input::get('date_of_sent_to_finance') != '')
		    	@$date_of_sent_to_finance = toGregorian(ymd_format(Input::get('date_of_sent_to_finance')));
		    else
		    	$date_of_sent_to_finance = "";

		    if(purchaseRoleCheck('procurement_purchase_list') && goodsRoleCheck('procurement_purchase_list'))
			{
				// get the form data in array;
		    	$purchaseData = array(
		    			'date_of_preparation'								=> $date_of_preparation,
		    			'end_user'											=> Input::get('end_user'),
		    			'goods_detail'										=> Input::get('goods_detail'),
		    			'request_no'										=> Input::get('request_no'),
		    			'date_of_arrival'									=> $date_of_arrival,
		    			'date_of_submission_to_the_purchasing_committee'	=> $date_of_submission_to_the_purchasing_committee,
		    			'purchasing_team'									=> Input::get('purchasing_team'),
		    			'lowest_price'										=> Input::get('lowest_price'),
		    			'winner_company'									=> Input::get('winner_company'),
		    			'm3_no'												=> Input::get('m3_no'),
		    			'date_of_m3'										=> $date_of_m3,
		    			'no_of_letter_sent_to_stock'						=> Input::get('no_of_letter_sent_to_stock'),
		    			'date_of_submission_to_stock'						=> $date_of_submission_to_stock,
		    			'date_of_submission_to_the_finance_department'		=> $date_of_submission_to_the_finance_department,
		    			'm3_no_sent_to_finance' 							=> Input::get('m3_no_sent_to_finance'),			    			
		    			'remarks'											=> Input::get('remarks'),
		    			'user_id'											=> Auth::user()->id
		    	);
			}
		    elseif(purchaseRoleCheck('procurement_purchase_list')){
		    	// get the form data in array;
		    	$purchaseData = array(
		    			'date_of_preparation'								=> $date_of_preparation,
		    			'end_user'											=> Input::get('end_user'),
		    			'goods_detail'										=> Input::get('goods_detail'),
		    			'request_no'										=> Input::get('request_no'),
		    			'date_of_arrival'									=> $date_of_arrival,
		    			'date_of_submission_to_the_purchasing_committee'	=> $date_of_submission_to_the_purchasing_committee,
		    			'purchasing_team'									=> Input::get('purchasing_team'),
		    			'lowest_price'										=> Input::get('lowest_price'),
		    			'winner_company'									=> Input::get('winner_company'),
		    			'm3_no'												=> Input::get('m3_no'),
		    			'date_of_m3'										=> $date_of_m3,
		    			'no_of_letter_sent_to_stock'						=> Input::get('no_of_letter_sent_to_stock'),
		    			'date_of_submission_to_stock'						=> $date_of_submission_to_stock,	    			
		    			'remarks'											=> Input::get('remarks'),
		    			'user_id'											=> Auth::user()->id
		    	);
		    }
		    elseif(goodsRoleCheck('procurement_purchase_list')){
		    	// get the form data in array;
		    	$purchaseData = array(
		    			'date_of_submission_to_the_finance_department'		=> $date_of_submission_to_the_finance_department,
		    			'm3_no_sent_to_finance' 							=> Input::get('m3_no_sent_to_finance'),
		    			'amount_sent_to_finance' 							=> Input::get('amount_sent_to_finance'),
		    			'user_id'											=> Auth::user()->id
		    	);
			}
			elseif(financeGoodsRoleCheck('procurement_purchase_list')){
		    	// get the form data in array;
		    	$purchaseData = array(
		    			'date_of_sent_to_finance' 							=> $date_of_sent_to_finance,
		    			'user_id'											=> Auth::user()->id
		    	);
			}
		    // now insert the form data to its table through calling the model function.
		    $object = procurementModel::editPurchaseDetails($id, $purchaseData);

		    if($object)
		    {
		    	if(purchaseRoleCheck('procurement_purchase_list') && goodsRoleCheck('procurement_purchase_list'))
		    	{
		    		// get the the log data and insert it into the log table.
					$purchaseLog = array(

							'date_of_preparation'								=> $date_of_preparation,
			    			'end_user'											=> Input::get('end_user'),
			    			'goods_detail'										=> Input::get('goods_detail'),
			    			'request_no'										=> Input::get('request_no'),
			    			'date_of_arrival'									=> $date_of_arrival,
			    			'date_of_submission_to_the_purchasing_committee'	=> $date_of_submission_to_the_purchasing_committee,
			    			'purchasing_team'									=> Input::get('purchasing_team'),
			    			'purchasing_date'									=> $purchasing_date,
			    			'lowest_price'										=> Input::get('lowest_price'),
			    			'winner_company'									=> Input::get('winner_company'),
			    			'm3_no'												=> Input::get('m3_no'),
			    			'date_of_m3'										=> $date_of_m3,
			    			'no_of_letter_sent_to_stock'						=> Input::get('no_of_letter_sent_to_stock'),
			    			'date_of_submission_to_stock'						=> $date_of_submission_to_stock,
		    				'date_of_submission_to_the_finance_department'		=> $date_of_submission_to_the_finance_department,
		    				'm3_no_sent_to_finance' 							=> Input::get('m3_no_sent_to_finance'),				    			
			    			'remarks'											=> Input::get('remarks'),
			    			'user'												=> Auth::user()->username,
			    			'action'											=> 'Purchase Edited'

					);

		    	}
		    	elseif(purchaseRoleCheck('procurement_purchase_list')){
			    	// get the the log data and insert it into the log table.
					$purchaseLog = array(

							'date_of_preparation'								=> $date_of_preparation,
			    			'end_user'											=> Input::get('end_user'),
			    			'goods_detail'										=> Input::get('goods_detail'),
			    			'request_no'										=> Input::get('request_no'),
			    			'date_of_arrival'									=> $date_of_arrival,
			    			'date_of_submission_to_the_purchasing_committee'	=> $date_of_submission_to_the_purchasing_committee,
			    			'purchasing_team'									=> Input::get('purchasing_team'),
			    			'purchasing_date'									=> $purchasing_date,
			    			'lowest_price'										=> Input::get('lowest_price'),
			    			'winner_company'									=> Input::get('winner_company'),
			    			'm3_no'												=> Input::get('m3_no'),
			    			'date_of_m3'										=> $date_of_m3,
			    			'no_of_letter_sent_to_stock'						=> Input::get('no_of_letter_sent_to_stock'),
			    			'date_of_submission_to_stock'						=> $date_of_submission_to_stock,		    			
			    			'remarks'											=> Input::get('remarks'),
			    			'user'												=> Auth::user()->username,
			    			'action'											=> 'Purchase Edited'

					);
				}	
				elseif(goodsRoleCheck('procurement_purchase_list')){
					// get the the log data and insert it into the log table.
					$purchaseLog = array(

		    			'date_of_submission_to_the_finance_department'		=> $date_of_submission_to_the_finance_department,
		    			'm3_no_sent_to_finance' 							=> Input::get('m3_no_sent_to_finance'),
		    			'amount_sent_to_finance' 							=> Input::get('amount_sent_to_finance'),
		    			'user'												=> Auth::user()->username,
		    			'action'											=> 'Purchase Edited'

					);
				}
				elseif(financeGoodsRoleCheck('procurement_purchase_list')){
			    	// get the form data in array;
			    	$purchaseLog = array(
			    			'date_of_sent_to_finance' 							=> $date_of_sent_to_finance,
			    			'user'												=> Auth::user()->username,
		    				'action'											=> 'Purchase Edited'
			    	);
				}
				procurementModel::addLogData('goods_purchase_log', $purchaseLog);
		    	return Redirect::route("goodsPurchaseList")->with("success","The Purchase details successfully edited.");
	        }
	        else
	        {
	            return Redirect::route("goodsPurchaseList")->with("fail","An error occured or the form has not been changed, please try again or contact system developer.");
	        }
	    }
	    else{
	    	return showWarning();
	    }
    }
    //deleting the purchase based on id.
    public function postDeletePurchase($id=0)
    {
    	if(canDelete('procurement_purchase_list'))
    	{
	    	// delete the record based on id.
			$deleted = procurementModel::deletePurchaseData($id);
			
			if($deleted)
	        {
	    		return Redirect::route("goodsPurchaseList")->with("success","Purchase successfully deleted , ID: <span style='color:red;font_weight:bold;'>$id</span>");        	
	        }
	        else
	        {
	            return Redirect::route("goodsPurchaseList")->with("fail","There was a problem while deleting, please contact system developer");
	        }
	    }
    }

    // load the Purchase detailed information.
	public function loadPurchaseDetails($id=0)
	{
		if(canView('procurement_purchase_list'))
		{
			$data['purchase'] = procurementModel::getSpecificPurchaseData($id);
			return View::make('procurement.viewPurchase', $data);
		}
		else{
			return showWarning();
		}
	}
	
	//================================================== Procurement Plan Functions ===========================================================
	//load the list of procurement plans.
	public function loadProcurementPlanList()
	{
		return View::make('procurement.procurementPlanList');
	}
	//get procurement plan data for datatable.
	public function getAllProcurementPlan()
	{
		//get all data 
		$object = procurementModel::getProcurementPlanData();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'sector',
							'procurement_entity',
							'budget_type',
							'budget_code',
							'year',
							// 'name_of_project_in_approved_budget_plan',
							'donor',
							'procurement_description',
							'type_of_contract',
							'contract_number'
							)
				// ->addColumn('start_letter_date', function($option){
				// 	$start_letter_date = checkEmptyDate($option->start_letter_date);
				// 	return $start_letter_date;
				// })
				->addColumn('operations', function($option){
					
					$options = '';
					if(canEdit('procurement_plan_list'))
						$options .= '<a href="'.route('getLoadProcurementPlanEditForm',$option->id).'" title="Edit Record"><i class="fa fa-edit"></i></a> | ';
					//if(canDelete('procurement_plan_list'))
						//$options .= '<a href="'.route('postProcurementPlanDelete',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Record"><i class="fa fa-trash-o"></i></a> | ';
					if(canView('procurement_plan_list')){
						$options .= '<a href="'.route('loadProcurementPlanDetails',$option->id).'" title="View details"><i class="fa fa-eye"></i></a>';
					}
					if(canAdd('procurement_proc_list'))
						$options .= ' | <a href="'.route('getLoadProcurementForm',$option->id).'" title="Add Procurement"><i class="fa fa-plus"></i></a>';
					return $options;

				})
				// ->orderColumns('id','id')
				->make();
		
	}
	// load the Procurement form for entry.
	public function loadProcurementPlanForm()
	{
		if(canAdd('procurement_plan_list'))
		{
			return View::make('procurement.procurementPlanForm');
		}
		else
		{
			return showWarning();
		}
	}
	// insert the Procurement Plan form data to the table.
	public function addProcurementPlanFormData()
	{
		//dd($_POST);
		if(canAdd('procurement_plan_list'))
		{
		    if(Input::get('date_of_budget_agreement_by_donor_or_mof') != '')
		    	$date_of_budget_agreement_by_donor_or_mof = toGregorian(ymd_format(Input::get('date_of_budget_agreement_by_donor_or_mof')));
		    else
		    	$date_of_budget_agreement_by_donor_or_mof = '';
		    if(Input::get('estimated_date_of_procurement_initiation') != '')
		    	$estimated_date_of_procurement_initiation = toGregorian(ymd_format(Input::get('estimated_date_of_procurement_initiation')));
		    else
		    	$estimated_date_of_procurement_initiation = '';
		    if(Input::get('estimated_date_of_project_announcement') != '')
		    	$estimated_date_of_project_announcement = toGregorian(ymd_format(Input::get('estimated_date_of_project_announcement')));
		    else
		    	$estimated_date_of_project_announcement = '';
		    if(Input::get('estimated_date_of_bid_opening') != '')
		    	$estimated_date_of_bid_opening = toGregorian(ymd_format(Input::get('estimated_date_of_bid_opening')));
		    else
		    	$estimated_date_of_bid_opening = '';
		    if(Input::get('date_of_bid_evaluation') != '')
		    	$date_of_bid_evaluation = toGregorian(ymd_format(Input::get('date_of_bid_evaluation')));
		    else
		    	$date_of_bid_evaluation = '';
		    if(Input::get('date_of_completion_of_the_bid_evaluation') != '')
		    	$date_of_completion_of_the_bid_evaluation = toGregorian(ymd_format(Input::get('date_of_completion_of_the_bid_evaluation')));
		    else
		    	$date_of_completion_of_the_bid_evaluation = '';
		    if(Input::get('date_of_the_evaluation_report_submission') != '')
		    	$date_of_the_evaluation_report_submission = toGregorian(ymd_format(Input::get('date_of_the_evaluation_report_submission')));
		    else
		    	$date_of_the_evaluation_report_submission = '';
		    if(Input::get('estimated_date_of_the_public_award') != '')
		    	$estimated_date_of_the_public_award = toGregorian(ymd_format(Input::get('estimated_date_of_the_public_award')));
		    else
		    	$estimated_date_of_the_public_award = '';
		    if(Input::get('estimation_date_of_contract_sign') != '')
		    	$estimation_date_of_contract_sign = toGregorian(ymd_format(Input::get('estimation_date_of_contract_sign')));
		    else
		    	$estimation_date_of_contract_sign = '';
		    if(Input::get('estimation_date_of_contract_completion') != '')
		    	$estimation_date_of_contract_completion = toGregorian(ymd_format(Input::get('estimation_date_of_contract_completion')));
		    else
		    	$estimation_date_of_contract_completion = '';

		    // get the form data in array;
		    $procurementPlanData = array(
		    			'sector'											=> Input::get('sector'),
		    			'procurement_entity'								=> Input::get('procurement_entity'),
		    			'budget_type'										=> Input::get('budget_type'),
		    			'budget_code'										=> Input::get('budget_code'),
		    			'year'												=> Input::get('year'),
		    			'name_of_project_in_approved_budget_plan'			=> Input::get('name_of_project_in_approved_budget_plan'),
		    			'donor'												=> Input::get('donor'),
		    			'procurement_description'							=> Input::get('procurement_description'),
		    			'type_of_contract'									=> Input::get('type_of_contract'),
		    			'procurement_preference_from_national_resources'	=> Input::get('procurement_preference_from_national_resources'),
		    			'contract_number'									=> Input::get('contract_number'),
		    			'date_of_budget_agreement_by_donor_or_mof'			=> $date_of_budget_agreement_by_donor_or_mof,
		    			'procurement_method'								=> Input::get('procurement_method'),
		    			'estimated_cost_of_the_project_in_afg'				=> Input::get('estimated_cost_of_the_project_in_afg'),
		    			'estimated_date_of_procurement_initiation'			=> $estimated_date_of_procurement_initiation,
		    			'estimated_date_of_project_announcement'			=> $estimated_date_of_project_announcement,
		    			'estimated_date_of_bid_opening'						=> $estimated_date_of_bid_opening,
		    			'date_of_bid_evaluation'							=> $date_of_bid_evaluation,
		    			'date_of_completion_of_the_bid_evaluation'			=> $date_of_completion_of_the_bid_evaluation,
		    			'date_of_the_evaluation_report_submission'			=> $date_of_the_evaluation_report_submission,
		    			'estimated_date_of_the_public_award'				=> $estimated_date_of_the_public_award,
		    			'estimation_date_of_contract_sign'					=> $estimation_date_of_contract_sign,
		    			'guarantee_period_of_works'							=> Input::get('guarantee_period_of_works'),
		    			'estimation_date_of_contract_completion'			=> $estimation_date_of_contract_completion,
		    			'remarks'											=> Input::get('remarks'),
		    			'user_id'											=> Auth::user()->id,
		    			'created_at'										=> date('Y-m-d H:i:s')
		    	);

		    // now insert the form data to its table through calling the model function.
		    $object = procurementModel::addProcurementPlanDetails($procurementPlanData);

		    if($object)
		    {
		    	// get the the log data and insert it into the log table.
				$procurementPlanLog = array(

						'sector'											=> Input::get('sector'),
		    			'procurement_entity'								=> Input::get('procurement_entity'),
		    			'budget_type'										=> Input::get('budget_type'),
		    			'budget_code'										=> Input::get('budget_code'),
		    			'year'												=> Input::get('year'),
		    			'name_of_project_in_approved_budget_plan'			=> Input::get('name_of_project_in_approved_budget_plan'),
		    			'donor'												=> Input::get('donor'),
		    			'procurement_description'							=> Input::get('procurement_description'),
		    			'type_of_contract'									=> Input::get('type_of_contract'),
		    			'procurement_preference_from_national_resources'	=> Input::get('procurement_preference_from_national_resources'),
		    			'contract_number'									=> Input::get('contract_number'),
		    			'date_of_budget_agreement_by_donor_or_mof'			=> $date_of_budget_agreement_by_donor_or_mof,
		    			'procurement_method'								=> Input::get('procurement_method'),
		    			'estimated_cost_of_the_project_in_afg'				=> Input::get('estimated_cost_of_the_project_in_afg'),
		    			'estimated_date_of_procurement_initiation'			=> $estimated_date_of_procurement_initiation,
		    			'estimated_date_of_project_announcement'			=> $estimated_date_of_project_announcement,
		    			'estimated_date_of_bid_opening'						=> $estimated_date_of_bid_opening,
		    			'date_of_bid_evaluation'							=> $date_of_bid_evaluation,
		    			'date_of_completion_of_the_bid_evaluation'			=> $date_of_completion_of_the_bid_evaluation,
		    			'date_of_the_evaluation_report_submission'			=> $date_of_the_evaluation_report_submission,
		    			'estimated_date_of_the_public_award'				=> $estimated_date_of_the_public_award,
		    			'estimation_date_of_contract_sign'					=> $estimation_date_of_contract_sign,
		    			'guarantee_period_of_works'							=> Input::get('guarantee_period_of_works'),
		    			'estimation_date_of_contract_completion'			=> $estimation_date_of_contract_completion,
		    			'remarks'											=> Input::get('remarks'),
		    			'user'												=> Auth::user()->username,
		    			'action'											=> 'Procurement Plan Added',
		    			'updated_at'										=> date('Y-m-d H:i:s')

					);
				procurementModel::addLogData('procurement_plan_log', $procurementPlanLog);
		    	return Redirect::route("procurementPlanList")->with("success","The Procurement Plan details successfully added.");
	        }
	        else
	        {
	            return Redirect::route("procurementPlanList")->with("fail","An error occured please try again or contact system developer.");
	        }
	    }
	    else{
	    	return showWarning();
	    }

	}
	// load the Procurement Edit form.
	public function loadProcurementPlanEditForm($id=0)
	{
		$data['plan'] = procurementModel::getSpecificProcurementPlanData($id);
		return View::make('procurement.editProcurementPlan', $data);
	}
	// update the Procurement Plan form data.
	public function postEditProcurementPlan($id=0)
	{
		//dd($_POST);
		if(canEdit('procurement_plan_list'))
		{
		    if(Input::get('date_of_budget_agreement_by_donor_or_mof') != '')
		    	$date_of_budget_agreement_by_donor_or_mof = toGregorian(ymd_format(Input::get('date_of_budget_agreement_by_donor_or_mof')));
		    else
		    	$date_of_budget_agreement_by_donor_or_mof = '';
		    if(Input::get('estimated_date_of_procurement_initiation') != '')
		    	$estimated_date_of_procurement_initiation = toGregorian(ymd_format(Input::get('estimated_date_of_procurement_initiation')));
		    else
		    	$estimated_date_of_procurement_initiation = '';
		    if(Input::get('estimated_date_of_project_announcement') != '')
		    	$estimated_date_of_project_announcement = toGregorian(ymd_format(Input::get('estimated_date_of_project_announcement')));
		    else
		    	$estimated_date_of_project_announcement = '';
		    if(Input::get('estimated_date_of_bid_opening') != '')
		    	$estimated_date_of_bid_opening = toGregorian(ymd_format(Input::get('estimated_date_of_bid_opening')));
		    else
		    	$estimated_date_of_bid_opening = '';
		    if(Input::get('date_of_bid_evaluation') != '')
		    	$date_of_bid_evaluation = toGregorian(ymd_format(Input::get('date_of_bid_evaluation')));
		    else
		    	$date_of_bid_evaluation = '';
		    if(Input::get('date_of_completion_of_the_bid_evaluation') != '')
		    	$date_of_completion_of_the_bid_evaluation = toGregorian(ymd_format(Input::get('date_of_completion_of_the_bid_evaluation')));
		    else
		    	$date_of_completion_of_the_bid_evaluation = '';
		    if(Input::get('date_of_the_evaluation_report_submission') != '')
		    	$date_of_the_evaluation_report_submission = toGregorian(ymd_format(Input::get('date_of_the_evaluation_report_submission')));
		    else
		    	$date_of_the_evaluation_report_submission = '';
		    if(Input::get('estimated_date_of_the_public_award') != '')
		    	$estimated_date_of_the_public_award = toGregorian(ymd_format(Input::get('estimated_date_of_the_public_award')));
		    else
		    	$estimated_date_of_the_public_award = '';
		    if(Input::get('estimation_date_of_contract_sign') != '')
		    	$estimation_date_of_contract_sign = toGregorian(ymd_format(Input::get('estimation_date_of_contract_sign')));
		    else
		    	$estimation_date_of_contract_sign = '';
		    if(Input::get('estimation_date_of_contract_completion') != '')
		    	$estimation_date_of_contract_completion = toGregorian(ymd_format(Input::get('estimation_date_of_contract_completion')));
		    else
		    	$estimation_date_of_contract_completion = '';
		    // get the form data in array;
		    $procurementPlanData = array(
		    			'sector'											=> Input::get('sector'),
		    			'procurement_entity'								=> Input::get('procurement_entity'),
		    			'budget_type'										=> Input::get('budget_type'),
		    			'budget_code'										=> Input::get('budget_code'),
		    			'year'												=> Input::get('year'),
		    			'name_of_project_in_approved_budget_plan'			=> Input::get('name_of_project_in_approved_budget_plan'),
		    			'donor'												=> Input::get('donor'),
		    			'procurement_description'							=> Input::get('procurement_description'),
		    			'type_of_contract'									=> Input::get('type_of_contract'),
		    			'procurement_preference_from_national_resources'	=> Input::get('procurement_preference_from_national_resources'),
		    			'contract_number'									=> Input::get('contract_number'),
		    			'date_of_budget_agreement_by_donor_or_mof'			=> $date_of_budget_agreement_by_donor_or_mof,
		    			'procurement_method'								=> Input::get('procurement_method'),
		    			'estimated_cost_of_the_project_in_afg'				=> Input::get('estimated_cost_of_the_project_in_afg'),
		    			'estimated_date_of_procurement_initiation'			=> $estimated_date_of_procurement_initiation,
		    			'estimated_date_of_project_announcement'			=> $estimated_date_of_project_announcement,
		    			'estimated_date_of_bid_opening'						=> $estimated_date_of_bid_opening,
		    			'date_of_bid_evaluation'							=> $date_of_bid_evaluation,
		    			'date_of_completion_of_the_bid_evaluation'			=> $date_of_completion_of_the_bid_evaluation,
		    			'date_of_the_evaluation_report_submission'			=> $date_of_the_evaluation_report_submission,
		    			'estimated_date_of_the_public_award'				=> $estimated_date_of_the_public_award,
		    			'estimation_date_of_contract_sign'					=> $estimation_date_of_contract_sign,
		    			'guarantee_period_of_works'							=> Input::get('guarantee_period_of_works'),
		    			'estimation_date_of_contract_completion'			=> $estimation_date_of_contract_completion,
		    			'remarks'											=> Input::get('remarks'),
		    			'user_id'											=> Auth::user()->id,
		    			'created_at'										=> date('Y-m-d H:i:s')
		    	);
		    // update the changes in procurement plan.
		    $object = procurementModel::updateProcurementPlan($id,$procurementPlanData);
		    if($object)
		    {
		    	// get the the log data and insert it into the log table.
				$procurementPlanLog = array(

						'sector'											=> Input::get('sector'),
		    			'procurement_entity'								=> Input::get('procurement_entity'),
		    			'budget_type'										=> Input::get('budget_type'),
		    			'budget_code'										=> Input::get('budget_code'),
		    			'year'												=> Input::get('year'),
		    			'name_of_project_in_approved_budget_plan'			=> Input::get('name_of_project_in_approved_budget_plan'),
		    			'donor'												=> Input::get('donor'),
		    			'procurement_description'							=> Input::get('procurement_description'),
		    			'type_of_contract'									=> Input::get('type_of_contract'),
		    			'procurement_preference_from_national_resources'	=> Input::get('procurement_preference_from_national_resources'),
		    			'contract_number'									=> Input::get('contract_number'),
		    			'date_of_budget_agreement_by_donor_or_mof'			=> $date_of_budget_agreement_by_donor_or_mof,
		    			'procurement_method'								=> Input::get('procurement_method'),
		    			'estimated_cost_of_the_project_in_afg'				=> Input::get('estimated_cost_of_the_project_in_afg'),
		    			'estimated_date_of_procurement_initiation'			=> $estimated_date_of_procurement_initiation,
		    			'estimated_date_of_project_announcement'			=> $estimated_date_of_project_announcement,
		    			'estimated_date_of_bid_opening'						=> $estimated_date_of_bid_opening,
		    			'date_of_bid_evaluation'							=> $date_of_bid_evaluation,
		    			'date_of_completion_of_the_bid_evaluation'			=> $date_of_completion_of_the_bid_evaluation,
		    			'date_of_the_evaluation_report_submission'			=> $date_of_the_evaluation_report_submission,
		    			'estimated_date_of_the_public_award'				=> $estimated_date_of_the_public_award,
		    			'estimation_date_of_contract_sign'					=> $estimation_date_of_contract_sign,
		    			'guarantee_period_of_works'							=> Input::get('guarantee_period_of_works'),
		    			'estimation_date_of_contract_completion'			=> $estimation_date_of_contract_completion,
		    			'remarks'											=> Input::get('remarks'),
		    			'user'												=> Auth::user()->username,
		    			'action'											=> 'Procurement Plan Updated',
		    			'updated_at'										=> date('Y-m-d H:i:s')

					);
				procurementModel::addLogData('procurement_plan_log', $procurementPlanLog);
		    	return Redirect::route("procurementPlanList")->with("success","The Procurement Plan successfully updated.");
	        }
	        else
	        {
	            return Redirect::route("procurementPlanList")->with("fail","An error occured or the form has not been changed, please try again or contact system developer.");
	        }
	    }
	    else{
	    	return showWarning();
	    }
    }
	//deleting the procurement plan data based on id.
    public function postDeleteProcurementPlan($id=0)
    {
    	if(canDelete('procurement_plan_list'))
    	{
	    	// delete the record based on id.
			$deleted = procurementModel::deleteProcurementPlanData($id);
			
			if($deleted)
	        {
	    		return Redirect::route("procurementPlanList")->with("success","Procurement successfully deleted , ID: <span style='color:red;font_weight:bold;'>$id</span>");        	
	        }
	        else
	        {
	            return Redirect::route("procurementPlanList")->with("fail","There was a problem while deleting, please contact system developer");
	        }
	   	}
	   	else
	   	{
	   		return showWarning();
	   	}
    }
	// load the Procurement Plan detailed information.
	public function viewProcurementPlanDetails($id=0)
	{
		if(canView('procurement_plan_list'))
		{
			$data['procurement_plan'] = procurementModel::getProcurementPlanDetails($id);
			//dd($data['procurement']);
			return View::make('procurement.viewProcurementPlan', $data);
		}
		else
		{
			return showWarning();
		}
	}
	//================================================== Procurement Functions ===========================================================
	
	//Load procurement list view
	public function loadProcurementList()
	{
		return View::make('procurement.procurementList');
	}

	//get procurement data for datatable.
	public function getAllProcurement()
	{

		//get all data 
		$object = procurementModel::getProcurementData();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'procurement_plan_id',
							'procurement_details',
							'executive_department',
							'procurement_method',
							'announcement_date',
							'date_of_pre_qualification_meeting',
							'date_of_bid_opening',
							'evaluation_completion_date',
							'date_of_procurement_award',
							'date_of_npc_approval',
							'date_of_notice_of_procurement_award',
							'start_letter_date',
							'winner_bidder'
							)
				->addColumn('announcement_date', function($option){
					$announcement_date = checkEmptyDate($option->announcement_date);
					return $announcement_date;
				})
				->addColumn('date_of_pre_qualification_meeting', function($option){
					$date_of_pre_qualification_meeting = checkEmptyDate($option->date_of_pre_qualification_meeting);
					return $date_of_pre_qualification_meeting;
				})
				->addColumn('date_of_bid_opening', function($option){
					$date_of_bid_opening = checkEmptyDate($option->date_of_bid_opening);
					return $date_of_bid_opening;
				})
				->addColumn('evaluation_completion_date', function($option){
					$evaluation_completion_date = checkEmptyDate($option->evaluation_completion_date);
					return $evaluation_completion_date;
				})
				->addColumn('date_of_procurement_award', function($option){
					$date_of_procurement_award = checkEmptyDate($option->date_of_procurement_award);
					return $date_of_procurement_award;
				})
				->addColumn('date_of_npc_approval', function($option){
					$date_of_npc_approval = checkEmptyDate($option->date_of_npc_approval);
					return $date_of_npc_approval;
				})
				->addColumn('date_of_notice_of_procurement_award', function($option){
					$date_of_notice_of_procurement_award = checkEmptyDate($option->date_of_notice_of_procurement_award);
					return $date_of_notice_of_procurement_award;
				})
				->addColumn('start_letter_date', function($option){
					$start_letter_date = checkEmptyDate($option->start_letter_date);
					return $start_letter_date;
				})
				->addColumn('operations', function($option){
					
					$options = '';
					if(canView('procurement_proc_list')){
						$options .= '<a href="'.route('loadProcurementDetails',$option->id).'" title="View Procurement details"><i class="fa fa-eye"></i></a>';
					}
					if(canEdit('procurement_proc_list'))
						$options .= ' | <a href="'.route('getLoadProcurementEditForm',$option->id).'" title="Edit Procurement"><i class="fa fa-edit"></i></a>';
					//if(canDelete('procurement_proc_list'))
						//$options .= '<a href="'.route('postProcurementDelete',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Procurement"><i class="fa fa-trash-o"></i></a> | ';
					if(canArchive('procurement_contracts_list')){
						if($option->archived == 1){
							$options .= " | <span style='color:red'>Archived</span>";
						}else{
							$options .= ' | <a href="#" onclick="archiveContract('.$option->id.')" title="Archive the Procurement Payments"><i class="fa fa-archive"></i></a>';
						}
					}
					if(canAdd('procurement_contracts_list')){
						$options .= ' | <a href="'.route('getLoadContractForm',$option->id).'" title="Add Payment"><i class="fa fa-plus"></i></a>';
					}
					return $options;

				})
				// ->orderColumns('id','id')
				->make();
		
	}
	// load the Procurement form for entry.
	public function loadProcurementForm($procurement_plan_id=0)
	{
		//dd($data['procurement_method']);
		$data['procurement_plan_id'] = $procurement_plan_id;
		return View::make('procurement.procurementForm', $data);
	}
	// insert the Procurement form data to the table.
	public function addProcurementFormData()
	{
		//dd($_POST);
		if(canAdd('procurement_proc_list'))
		{

		    if(Input::get('project_submission_date_to_procurement_entity') != '')
		    	$project_submission_date_to_procurement_entity = toGregorian(ymd_format(Input::get('project_submission_date_to_procurement_entity')));
		    else
		    	$project_submission_date_to_procurement_entity = '';
		    if(Input::get('date_of_procurement_approval') != '')
		    	$date_of_procurement_approval = toGregorian(ymd_format(Input::get('date_of_procurement_approval')));
		    else
		    	$date_of_procurement_approval = '';
		    if(Input::get('announcement_date') != '')
		    	$announcement_date = toGregorian(ymd_format(Input::get('announcement_date')));
		    else
		    	$announcement_date = '';
		    if(Input::get('date_of_pre_qualification_meeting') != '')
		    	$date_of_pre_qualification_meeting = toGregorian(ymd_format(Input::get('date_of_pre_qualification_meeting')));
		    else
		    	$date_of_pre_qualification_meeting = '';
		    if(Input::get('date_of_bid_opening') != '')
		    	$date_of_bid_opening = toGregorian(ymd_format(Input::get('date_of_bid_opening')));
		    else
		    	$date_of_bid_opening = '';
		    if(Input::get('evaluation_completion_date') != '')
		    	$evaluation_completion_date = toGregorian(ymd_format(Input::get('evaluation_completion_date')));
		    else
		    	$evaluation_completion_date = '';
		    if(Input::get('date_of_procurement_award') != '')
		    	$date_of_procurement_award = toGregorian(ymd_format(Input::get('date_of_procurement_award')));
		    else
		    	$date_of_procurement_award = '';
		    if(Input::get('date_of_npc_approval') != '')
		    	$date_of_npc_approval = toGregorian(ymd_format(Input::get('date_of_npc_approval')));
		    else
		    	$date_of_npc_approval = '';
		    if(Input::get('date_of_notice_of_procurement_award') != '')
		    	$date_of_notice_of_procurement_award = toGregorian(ymd_format(Input::get('date_of_notice_of_procurement_award')));
		    else
		    	$date_of_notice_of_procurement_award = '';
		    if(Input::get('start_letter_date') != '')
		    	$start_letter_date = toGregorian(ymd_format(Input::get('start_letter_date')));
		    else
		    	$start_letter_date = '';

		    // get the form data in array;
		    $procurementData = array(
						'procurement_plan_id'								=> Input::get('procurement_plan_id'),
		    			'budget_code'										=> Input::get('budget_code'),
		    			'estimation_cost'									=> Input::get('estimation_cost'),
		    			'procurement_details'								=> Input::get('procurement_details'),
		    			'end_user'											=> Input::get('end_user'),
		    			'contract_number'									=> Input::get('contract_number'),
		    			'procurement_type'									=> Input::get('procurement_type'),
		    			'procurement_method'								=> Input::get('procurement_method'),
		    			'project_submission_date_to_procurement_entity'		=> $project_submission_date_to_procurement_entity,
		    			'date_of_procurement_approval'						=> $date_of_procurement_approval,
		    			'announcement_date'									=> $announcement_date,
		    			'date_of_pre_qualification_meeting'					=> $date_of_pre_qualification_meeting,
		    			'date_of_bid_opening'								=> $date_of_bid_opening,
		    			'evaluation_completion_date'						=> $evaluation_completion_date,
		    			'date_of_procurement_award'							=> $date_of_procurement_award,
		    			'date_of_npc_approval'								=> $date_of_npc_approval,
		    			'date_of_notice_of_procurement_award'				=> $date_of_notice_of_procurement_award,
		    			'start_letter_date'									=> $start_letter_date,
		    			'winner_bidder'										=> Input::get('winner_bidder'),
		    			'remarks'											=> Input::get('remarks'),
		    			'user_id'											=> Auth::user()->id
		    	);

		    // now insert the form data to its table through calling the model function.
		    $object = procurementModel::addProcurementDetails($procurementData);

		    if($object)
		    {
		    	// get the the log data and insert it into the log table.
				$procurementLog = array(
						'procurement_plan_id'								=> Input::get('procurement_plan_id'),
						'budget_code'										=> Input::get('budget_code'),
		    			'estimation_cost'									=> Input::get('estimation_cost'),
		    			'procurement_details'								=> Input::get('procurement_details'),
		    			'end_user'											=> Input::get('end_user'),
		    			'contract_number'									=> Input::get('contract_number'),
		    			'procurement_type'									=> Input::get('procurement_type'),
		    			'procurement_method'								=> Input::get('procurement_method'),
		    			'project_submission_date_to_procurement_entity'		=> $project_submission_date_to_procurement_entity,
		    			'date_of_procurement_approval'						=> $date_of_procurement_approval,
		    			'announcement_date'									=> $announcement_date,
		    			'date_of_pre_qualification_meeting'					=> $date_of_pre_qualification_meeting,
		    			'date_of_bid_opening'								=> $date_of_bid_opening,
		    			'evaluation_completion_date'						=> $evaluation_completion_date,
		    			'date_of_procurement_award'							=> $date_of_procurement_award,
		    			'date_of_npc_approval'								=> $date_of_npc_approval,
		    			'date_of_notice_of_procurement_award'				=> $date_of_notice_of_procurement_award,
		    			'start_letter_date'									=> $start_letter_date,
		    			'winner_bidder'										=> Input::get('winner_bidder'),
		    			'remarks'											=> Input::get('remarks'),
		    			'user'												=> Auth::user()->username,
		    			'action'											=> 'Procurement Added'

					);
				procurementModel::addLogData('procurement_log', $procurementLog);
		    	return Redirect::route("procurementList")->with("success","The Procurement details successfully added.");
	        }
	        else
	        {
	            return Redirect::route("procurementList")->with("fail","An error occured please try again or contact system developer.");
	        }
	    }
	    else{
	    	return showWarning();
	    }

	}
	// load the Procurement Edit form.
	public function loadProcurementEditForm($id=0)
	{
		$data['procurement'] = procurementModel::getSpecificProcurementData($id);
		return View::make('procurement.editProcurement', $data);
	}
	// update the Procurement form data.
	public function postEditProcurement($id=0)
	{
		//dd($_POST);
		if(canEdit('procurement_proc_list'))
		{
		
		    if(Input::get('project_submission_date_to_procurement_entity') != '')
		    	$project_submission_date_to_procurement_entity = toGregorian(ymd_format(Input::get('project_submission_date_to_procurement_entity')));
		    else
		    	$project_submission_date_to_procurement_entity = '';
		    if(Input::get('date_of_procurement_approval') != '')
		    	$date_of_procurement_approval = toGregorian(ymd_format(Input::get('date_of_procurement_approval')));
		    else
		    	$date_of_procurement_approval = '';
		    if(Input::get('announcement_date') != '')
		    	$announcement_date = toGregorian(ymd_format(Input::get('announcement_date')));
		    else
		    	$announcement_date = '';
		    if(Input::get('date_of_pre_qualification_meeting') != '')
		    	$date_of_pre_qualification_meeting = toGregorian(ymd_format(Input::get('date_of_pre_qualification_meeting')));
		    else
		    	$date_of_pre_qualification_meeting = '';
		    if(Input::get('date_of_bid_opening') != '')
		    	$date_of_bid_opening = toGregorian(ymd_format(Input::get('date_of_bid_opening')));
		    else
		    	$date_of_bid_opening = '';
		    if(Input::get('evaluation_completion_date') != '')
		    	$evaluation_completion_date = toGregorian(ymd_format(Input::get('evaluation_completion_date')));
		    else
		    	$evaluation_completion_date = '';
		    if(Input::get('date_of_procurement_award') != '')
		    	$date_of_procurement_award = toGregorian(ymd_format(Input::get('date_of_procurement_award')));
		    else
		    	$date_of_procurement_award = '';
		    if(Input::get('date_of_npc_approval') != '')
		    	$date_of_npc_approval = toGregorian(ymd_format(Input::get('date_of_npc_approval')));
		    else
		    	$date_of_npc_approval = '';
		    if(Input::get('date_of_notice_of_procurement_award') != '')
		    	$date_of_notice_of_procurement_award = toGregorian(ymd_format(Input::get('date_of_notice_of_procurement_award')));
		    else
		    	$date_of_notice_of_procurement_award = '';
		    if(Input::get('start_letter_date') != '')
		    	$start_letter_date = toGregorian(ymd_format(Input::get('start_letter_date')));
		    else
		    	$start_letter_date = '';

		    // get the form data in array;
		    $procurementData = array(
						'procurement_plan_id'								=> Input::get('procurement_plan_id'),
		    			'budget_code'										=> Input::get('budget_code'),
		    			'estimation_cost'									=> Input::get('estimation_cost'),
		    			'procurement_details'								=> Input::get('procurement_details'),
		    			'end_user'											=> Input::get('end_user'),
		    			'contract_number'									=> Input::get('contract_number'),
		    			'procurement_type'									=> Input::get('procurement_type'),
		    			'procurement_method'								=> Input::get('procurement_method'),
		    			'project_submission_date_to_procurement_entity'		=> $project_submission_date_to_procurement_entity,
		    			'date_of_procurement_approval'						=> $date_of_procurement_approval,
		    			'announcement_date'									=> $announcement_date,
		    			'date_of_pre_qualification_meeting'					=> $date_of_pre_qualification_meeting,
		    			'date_of_bid_opening'								=> $date_of_bid_opening,
		    			'evaluation_completion_date'						=> $evaluation_completion_date,
		    			'date_of_procurement_award'							=> $date_of_procurement_award,
		    			'date_of_npc_approval'								=> $date_of_npc_approval,
		    			'date_of_notice_of_procurement_award'				=> $date_of_notice_of_procurement_award,
		    			'start_letter_date'									=> $start_letter_date,
		    			'winner_bidder'										=> Input::get('winner_bidder'),
		    			'remarks'											=> Input::get('remarks'),
		    			'user_id'											=> Auth::user()->id
		    	);

		    // now insert the form data to its table through calling the model function.
		    $object = procurementModel::editProcurementDetails($id, $procurementData);

		    if($object)
		    {
		    	// get the the log data and insert it into the log table.
				$procurementLog = array(
						'procurement_plan_id'								=> Input::get('procurement_plan_id'),
						'budget_code'										=> Input::get('budget_code'),
		    			'estimation_cost'									=> Input::get('estimation_cost'),
		    			'procurement_details'								=> Input::get('procurement_details'),
		    			'end_user'											=> Input::get('end_user'),
		    			'contract_number'									=> Input::get('contract_number'),
		    			'procurement_type'									=> Input::get('procurement_type'),
		    			'procurement_method'								=> Input::get('procurement_method'),
		    			'project_submission_date_to_procurement_entity'		=> $project_submission_date_to_procurement_entity,
		    			'date_of_procurement_approval'						=> $date_of_procurement_approval,
		    			'announcement_date'									=> $announcement_date,
		    			'date_of_pre_qualification_meeting'					=> $date_of_pre_qualification_meeting,
		    			'date_of_bid_opening'								=> $date_of_bid_opening,
		    			'evaluation_completion_date'						=> $evaluation_completion_date,
		    			'date_of_procurement_award'							=> $date_of_procurement_award,
		    			'date_of_npc_approval'								=> $date_of_npc_approval,
		    			'date_of_notice_of_procurement_award'				=> $date_of_notice_of_procurement_award,
		    			'start_letter_date'									=> $start_letter_date,
		    			'winner_bidder'										=> Input::get('winner_bidder'),
		    			'remarks'											=> Input::get('remarks'),
		    			'user'												=> Auth::user()->username,
		    			'action'											=> 'Procurement Edited'

					);
				procurementModel::addLogData('procurement_log', $procurementLog);
		    	return Redirect::route("procurementList")->with("success","The Procurement details successfully edited.");
	        }
	        else
	        {
	            return Redirect::route("procurementList")->with("fail","An error occured or the form has not been changed, please try again or contact system developer.");
	        }
	    }
	    else{
	    	return showWarning();
	    }
    }
    //deleting the purchase based on id.
    public function postDeleteProcurement($id=0)
    {
    	if(canDelete('procurement_proc_list'))
    	{
	    	// delete the record based on id.
			$deleted = procurementModel::deleteProcurementData($id);
			
			if($deleted)
	        {
	    		return Redirect::route("procurementList")->with("success","Procurement successfully deleted , ID: <span style='color:red;font_weight:bold;'>$id</span>");        	
	        }
	        else
	        {
	            return Redirect::route("procurementList")->with("fail","There was a problem while deleting, please contact system developer");
	        }
	   	}
	   	else
	   	{
	   		return showWarning();
	   	}
    }
    // load the Procurement detailed information.
	public function loadProcurementDetails($id=0)
	{
		if(canView('procurement_proc_list'))
		{
			$data['procurement'] = procurementModel::getProcurementDetails($id);
			//dd($data['procurement']);
			return View::make('procurement.viewProcurement', $data);
		}
		else
		{
			return showWarning();
		}
	}

	//================================================== Contract Functions =====================================

	//Load contracts list view
	public function loadContractList()
	{
		return View::make('procurement.contractsList');
	}

	//get contracts data for datatable.
	public function getAllContracts()
	{

		//get all data 
		$object = procurementModel::getContractData();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		if(contractGoodsRoleCheck('procurement_contracts_list'))
		{
			return Datatable::collection($collection)
				->showColumns(
					'procurement_id',
					'contract_name',
					'start_letter_date',
					'contract_completion_date',
					'm3_no',
					'total_m3_price',
					'date_of_m3',
					'date_of_m3_to_stock',
					'date_of_submission_letter_to_finance_department',
					'm3_number_sent_to_finance',
					'amount_sent_to_finance'
					)
					->addColumn('start_letter_date', function($option){
						$start_letter_date = checkEmptyDate($option->start_letter_date);
						return $start_letter_date;
					})
					->addColumn('contract_completion_date', function($option){
						$contract_completion_date = checkEmptyDate($option->contract_completion_date);
						return $contract_completion_date;
					})
					->addColumn('date_of_m3', function($option){
						$date_of_m3 = checkEmptyDate($option->date_of_m3);
						return $date_of_m3;
					})
					->addColumn('date_of_m3_to_stock', function($option){
						$date_of_m3_to_stock = checkEmptyDate($option->date_of_m3_to_stock);
						return $date_of_m3_to_stock;
					})
					->addColumn('date_of_submission_letter_to_finance_department', function($option){
						$date_of_submission_letter_to_finance_department = checkEmptyDate($option->date_of_submission_letter_to_finance_department);
						return $date_of_submission_letter_to_finance_department;
					})
					->addColumn('operations', function($option){
			
							$options = '';
							if(canView('procurement_contracts_list')){
								$options .= '<a href="'.route('loadContractDetails',array('id' => $option->procurement_id, 'field' => $option->total_remained_contract_price)).'" title="View Contract Details"><i class="fa fa-eye"></i></a> | ';
							}
							if(canEdit('procurement_contracts_list'))
								$options .= '<a href="'.route('getLoadContractEditForm',array('id' => $option->procurement_id, 'field' => $option->total_remained_contract_price,'main_id'=>$option->id)).'" title="Edit Contract"><i class="fa fa-edit"></i></a>';
							//if(canDelete('procurement_contracts_list'))
								//$options .= '<a href="'.route('deleteContract',array('id' => $option->procurement_id, 'field' => $option->total_remained_contract_price)).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Contract"><i class="fa fa-trash-o"></i></a>';
							return $options;
					})
					// ->orderColumns('id','id')
					->make();
		}
		elseif(financeContractRoleCheck('procurement_contracts_list'))
		{
			return Datatable::collection($collection)
				->showColumns(
					'procurement_id',
					'm3_no',
					'date_of_submission_letter_to_finance_department',
					'm3_number_sent_to_finance',
					'amount_sent_to_finance',
					'date_of_sent_to_finance'
					)
					->addColumn('date_of_submission_letter_to_finance_department', function($option){
						$date_of_submission_letter_to_finance_department = checkEmptyDate($option->date_of_submission_letter_to_finance_department);
						return $date_of_submission_letter_to_finance_department;
					})
					->addColumn('date_of_sent_to_finance', function($option){
						$date_of_sent_to_finance = checkEmptyDate($option->date_of_sent_to_finance);
						return $date_of_sent_to_finance;
					})
					->addColumn('operations', function($option){
			
							$options = '';
							if(canView('procurement_contracts_list')){
								$options .= '<a href="'.route('loadContractDetails',array('id' => $option->procurement_id, 'field' => $option->total_remained_contract_price)).'" title="View Contract Details"><i class="fa fa-eye"></i></a> | ';
							}
							if(canEdit('procurement_contracts_list'))
								$options .= '<a href="'.route('getLoadContractEditForm',array('id' => $option->procurement_id, 'field' => $option->total_remained_contract_price,'main_id'=>$option->id)).'" title="Edit Contract"><i class="fa fa-edit"></i></a>';
							//if(canDelete('procurement_contracts_list'))
								//$options .= '<a href="'.route('deleteContract',array('id' => $option->procurement_id, 'field' => $option->total_remained_contract_price)).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Contract"><i class="fa fa-trash-o"></i></a> | ';
							// if(canArchive('procurement_contracts_list')){
							// 	$options .= '<a href="#" onclick="archiveContract('.$option->id.')" title="Archive the Contract"><i class="fa fa-archive"></i></a>';
							// }
							return $options;
					})
					// ->orderColumns('id','id')
					->make();
		}
		else
		{
			return Datatable::collection($collection)
				->showColumns(
					'procurement_id',
					'contract_name',
					'start_letter_date',
					'contract_completion_date',
					'm3_no',
					'total_m3_price',
					'date_of_m3',
					'm3_no_to_stock',
					'date_of_m3_to_stock',
					'date_of_submission_letter_to_finance_department'
					)
					->addColumn('start_letter_date', function($option){
						$start_letter_date = checkEmptyDate($option->start_letter_date);
						return $start_letter_date;
					})
					->addColumn('contract_completion_date', function($option){
						$contract_completion_date = checkEmptyDate($option->contract_completion_date);
						return $contract_completion_date;
					})
					->addColumn('date_of_m3', function($option){
						$date_of_m3 = checkEmptyDate($option->date_of_m3);
						return $date_of_m3;
					})
					->addColumn('date_of_m3_to_stock', function($option){
						$date_of_m3_to_stock = checkEmptyDate($option->date_of_m3_to_stock);
						return $date_of_m3_to_stock;
					})
					->addColumn('date_of_submission_letter_to_finance_department', function($option){
						$date_of_submission_letter_to_finance_department = checkEmptyDate($option->date_of_submission_letter_to_finance_department);
						return $date_of_submission_letter_to_finance_department;
					})
					->addColumn('operations', function($option){
			
							$options = '';
							if(canView('procurement_contracts_list')){
								$options .= '<a href="'.route('loadContractDetails',array('id' => $option->procurement_id, 'field' => $option->total_remained_contract_price)).'" title="View Contract Details"><i class="fa fa-eye"></i></a> | ';
							}
							if(canEdit('procurement_contracts_list'))
								$options .= '<a href="'.route('getLoadContractEditForm',array('id' => $option->procurement_id, 'field' => $option->total_remained_contract_price,'main_id'=>$option->id)).'" title="Edit Contract"><i class="fa fa-edit"></i></a>';
							//if(canDelete('procurement_contracts_list'))
								//$options .= '<a href="'.route('deleteContract',array('id' => $option->procurement_id, 'field' => $option->total_remained_contract_price)).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Contract"><i class="fa fa-trash-o"></i></a> | ';
							// if(canArchive('procurement_contracts_list')){
							// 	$options .= '<a href="#" onclick="archiveContract('.$option->id.')" title="Archive the Contract"><i class="fa fa-archive"></i></a>';
							// }
							return $options;
					})
					// ->orderColumns('id','id')
					->make();
		}
	}
	// change the dmy jalali date to ymd gregorian date.
	public function changeDate()
	{
		$date = toGregorian(ymd_format(Input::get('date')));
		echo $date;
	}

	// load the contract form for entry.
	public function loadContractForm($id=0)
	{
		//get the CONTRACT TOTAL PRICE and M3 TOTAL AMOUNT and then substruct them to find the TOTAL REMAINING PRICE.
		$response = procurementModel::getContractById($id);
		if($response){
			$total_contract_price = $response->total_contract_price;
			$total_m3_price = $response->total_m3_price;
			//subtract the total_m3_price from total_contract_price to get the total_remaining_price.
			$total_remaining_price = $total_contract_price - $total_m3_price;
			if($total_remaining_price == 0){
				return Redirect::route("procurementList")->with("fail","You can't add more contracts for this procurement, because its ended.");
			}
			$data['id'] = $id;
			$data['total_contract_price'] = $total_contract_price;
			//$data['total_m3_price'] = $total_m3_price;
			$data['total_remained_contract_price'] = $total_remaining_price;

			return View::make('procurement.contractForm', $data);
		}
		else{
			$data['id'] = $id;
			return View::make('procurement.contractForm', $data);
		}
	}

	// insert the contract form data to the table.
	public function addContractFormData()
	{
		//dd($_POST);
		if(canAdd('procurement_contracts_list'))
		{
		    if(Input::get('start_letter_date') != '')
		    	@$start_letter_date = toGregorian(ymd_format(Input::get('start_letter_date')));
		    else
		    	$start_letter_date = "";
		    if(Input::get('contract_completion_date') != '')
		    	@$contract_completion_date = toGregorian(ymd_format(Input::get('contract_completion_date')));
		    else
		    	$contract_completion_date = "";
		    if(Input::get('request_date') != '')
		    	@$request_date = toGregorian(ymd_format(Input::get('request_date')));
		    else
		    	$request_date = "";
		    if(Input::get('date_of_preparation_of_m3') != '')
		    	@$date_of_preparation_of_m3 = toGregorian(ymd_format(Input::get('date_of_preparation_of_m3')));
		    else
		    	$date_of_preparation_of_m3 = "";
		    if(Input::get('date_of_m3') != '')
		    	@$date_of_m3 = toGregorian(ymd_format(Input::get('date_of_m3')));
		    else
		    	$date_of_m3 = "";
		    if(Input::get('date_of_m3_to_stock') != '')
		    	@$date_of_m3_to_stock = toGregorian(ymd_format(Input::get('date_of_m3_to_stock')));
		    else
		    	$date_of_m3_to_stock = "";
		    if(Input::get('date_of_contract_termination') != '')
		    	@$date_of_contract_termination = toGregorian(ymd_format(Input::get('date_of_contract_termination')));
		    else
		    	$date_of_contract_termination = "";


		    $date_of_submission_letter_to_finance_department	= '';
		    $m3_number_sent_to_finance			= '';

		    // if the total_m3_price is greater than contract_total_contract_price then redirect with error msg.
		    if(Input::get('total_m3_price') > Input::get('total_contract_price'))
	        {
	            return Redirect::route("getLoadContractForm")->with("fail","لطفآ قیمت مجموعی م ۳ را از قیمت مجموعی قرار داد کرده زیاد تر ننویسید !");
	        }

		    // get the form data in array;
		    $contractData = array(
		    			'procurement_id'									=> Input::get('procurement_id'),
		    			'contract_name'										=> Input::get('contract_name'),
		    			'contract_no'										=> Input::get('contract_no'),
		    			'start_letter_date'									=> @$start_letter_date,
		    			'contract_completion_date'							=> @$contract_completion_date,
		    			'winner_bidder'										=> Input::get('winner_bidder'),
		    			'total_contract_price'								=> Input::get('total_contract_price'),
		    			'request'											=> Input::get('request'),
		    			'request_date'										=> @$request_date,
		    			'date_of_preparation_of_m3'							=> @$date_of_preparation_of_m3,
		    			'm3_no'												=> Input::get('m3_no'),
		    			'total_m3_price'									=> Input::get('total_m3_price'),
		    			'date_of_m3'										=> @$date_of_m3,
		    			'm3_no_to_stock'									=> Input::get('m3_no_to_stock'),
		    			'date_of_m3_to_stock'								=> @$date_of_m3_to_stock,
						'date_of_submission_letter_to_finance_department'	=> $date_of_submission_letter_to_finance_department,
						'm3_number_sent_to_finance'							=> $m3_number_sent_to_finance,
		    			'contract_modification_price'						=> Input::get('contract_modification_price'),
		    			'total_remained_contract_price'						=> Input::get('total_remained_contract_price'),
		    			'remained_contract_time'							=> Input::get('remained_contract_time'),
		    			'date_of_contract_termination'						=> @$date_of_contract_termination,
		    			'termination_reason'								=> Input::get('termination_reason'),
		    			'remarks'											=> Input::get('remarks'),
		    			'user_id'											=> Auth::user()->id
		    	);

		    // now insert the form data to its table through calling the model function.
		    $object = procurementModel::addContractDetails($contractData);

		    if($object)
		    {
		    	// get the the log data and insert it into the log table.
				$contractLog = array(
						'procurement_id'									=> Input::get('procurement_id'),
		    			'contract_name'										=> Input::get('contract_name'),
		    			'contract_no'										=> Input::get('contract_no'),
		    			'start_letter_date'									=> @$start_letter_date,
		    			'contract_completion_date'							=> @$contract_completion_date,
		    			'winner_bidder'										=> Input::get('winner_bidder'),
		    			'total_contract_price'								=> Input::get('total_contract_price'),
		    			'request'											=> Input::get('request'),
		    			'request_date'										=> @$request_date,
		    			'date_of_preparation_of_m3'							=> @$date_of_preparation_of_m3,
		    			'm3_no'												=> Input::get('m3_no'),
		    			'total_m3_price'									=> Input::get('total_m3_price'),
		    			'date_of_m3'										=> @$date_of_m3,
		    			'm3_no_to_stock'									=> Input::get('m3_no_to_stock'),
		    			'date_of_m3_to_stock'								=> @$date_of_m3_to_stock,
						'date_of_submission_letter_to_finance_department'	=> $date_of_submission_letter_to_finance_department,
						'm3_number_sent_to_finance'							=> $m3_number_sent_to_finance,
		    			'contract_modification_price'						=> Input::get('contract_modification_price'),
		    			'total_remained_contract_price'						=> Input::get('total_remained_contract_price'),
		    			'remained_contract_time'							=> Input::get('remained_contract_time'),
		    			'date_of_contract_termination'						=> @$date_of_contract_termination,
		    			'termination_reason'								=> Input::get('termination_reason'),
		    			'remarks'											=> Input::get('remarks'),
		    			'user'												=> Auth::user()->username,
		    			'action'											=> 'Contract Added'

					);
				procurementModel::addLogData('contract_log', $contractLog);
		    	return Redirect::route("contractsList")->with("success","The Contract details successfully added.");
	        }
	        else
	        {
	            return Redirect::route("contractsList")->with("fail","An error occured please try again or contact system developer.");
	        }
	    }
	    else
		{
			return showWarning();
		}    

	}

	// load the Procurement Edit form.
	public function loadContractEditForm($id=0, $field="",$main_id)
	{
		$data['contract'] = procurementModel::getSpecificContractData($id, $field);
		$data['id'] = $id;
		$data['main_id'] = $main_id;
		$data['field'] = $field;
		return View::make('procurement.editContract', $data);
	}

	// edit the contract form data to the table.
	public function postEditContract($main_id=0)
	{
		//dd($_POST);
		if(canEdit('procurement_contracts_list'))
		{
		    if(Input::get('start_letter_date') != '')
		    	@$start_letter_date = toGregorian(ymd_format(Input::get('start_letter_date')));
		    else
		    	$start_letter_date = "";
		    if(Input::get('contract_completion_date') != '')
		    	@$contract_completion_date = toGregorian(ymd_format(Input::get('contract_completion_date')));
		    else
		    	$contract_completion_date = "";
		    if(Input::get('request_date') != '')
		    	@$request_date = toGregorian(ymd_format(Input::get('request_date')));
		    else
		    	$request_date = "";
		    if(Input::get('date_of_preparation_of_m3') != '')
		    	@$date_of_preparation_of_m3 = toGregorian(ymd_format(Input::get('date_of_preparation_of_m3')));
		    else
		    	$date_of_preparation_of_m3 = "";
		    if(Input::get('date_of_m3') != '')
		    	@$date_of_m3 = toGregorian(ymd_format(Input::get('date_of_m3')));
		    else
		    	$date_of_m3 = "";
		    if(Input::get('date_of_m3_to_stock') != '')
		    	@$date_of_m3_to_stock = toGregorian(ymd_format(Input::get('date_of_m3_to_stock')));
		    else
		    	$date_of_m3_to_stock = "";
		    if(Input::get('date_of_submission_letter_to_finance_department') != '')
		    	@$date_of_submission_letter_to_finance_department = toGregorian(ymd_format(Input::get('date_of_submission_letter_to_finance_department')));
		    else
		    	$date_of_submission_letter_to_finance_department = "";
		    if(Input::get('date_of_contract_termination') != '')
		    	@$date_of_contract_termination = toGregorian(ymd_format(Input::get('date_of_contract_termination')));
		    else
		    	$date_of_contract_termination = "";
			if(Input::get('date_of_sent_to_finance') != '')
		    	@$date_of_sent_to_finance = toGregorian(ymd_format(Input::get('date_of_sent_to_finance')));
		    else
		    	$date_of_sent_to_finance = "";
		    // if the total_m3_price is greater than contract_total_contract_price then redirect with error msg.
		    if(Input::get('total_m3_price') > Input::get('total_contract_price'))
	        {
	            return Redirect::route("getLoadContractForm")->with("fail","لطفآ قیمت مجموعی م ۳ را از قیمت مجموعی قرار داد کرده زیاد تر ننویسید !");
	        }
	        if(contractRoleCheck('procurement_contracts_list') && contractGoodsRoleCheck('procurement_contracts_list')){
				// get the form data in array;
			    $contractData = array(
			    			'procurement_id'									=> Input::get('procurement_id'),
			    			'contract_name'										=> Input::get('contract_name'),
			    			'contract_no'										=> Input::get('contract_no'),
			    			'start_letter_date'									=> @$start_letter_date,
			    			'contract_completion_date'							=> @$contract_completion_date,
			    			'winner_bidder'										=> Input::get('winner_bidder'),
			    			'total_contract_price'								=> Input::get('total_contract_price'),
			    			'request'											=> Input::get('request'),
			    			'request_date'										=> @$request_date,
			    			'date_of_preparation_of_m3'							=> @$date_of_preparation_of_m3,
			    			'm3_no'												=> Input::get('m3_no'),
			    			'total_m3_price'									=> Input::get('total_m3_price'),
			    			'date_of_m3'										=> @$date_of_m3,
			    			'm3_no_to_stock'									=> Input::get('m3_no_to_stock'),
			    			'date_of_m3_to_stock'								=> @$date_of_m3_to_stock,
			    			'date_of_submission_letter_to_finance_department'	=> @$date_of_submission_letter_to_finance_department,
			    			'm3_number_sent_to_finance' 						=> Input::get('m3_number_sent_to_finance'),			    			
			    			'contract_modification_price'						=> Input::get('contract_modification_price'),
			    			'total_remained_contract_price'						=> Input::get('total_remained_contract_price'),
			    			'remained_contract_time'							=> Input::get('remained_contract_time'),
			    			'date_of_contract_termination'						=> @$date_of_contract_termination,
			    			'termination_reason'								=> Input::get('termination_reason'),
			    			'remarks'											=> Input::get('remarks'),
			    			'user_id'											=> Auth::user()->id
			    );
			}
	        elseif(contractRoleCheck('procurement_contracts_list')){
			    // get the form data in array;
			    $contractData = array(
			    			'procurement_id'									=> Input::get('procurement_id'),
			    			'contract_name'										=> Input::get('contract_name'),
			    			'contract_no'										=> Input::get('contract_no'),
			    			'start_letter_date'									=> @$start_letter_date,
			    			'contract_completion_date'							=> @$contract_completion_date,
			    			'winner_bidder'										=> Input::get('winner_bidder'),
			    			'total_contract_price'								=> Input::get('total_contract_price'),
			    			'request'											=> Input::get('request'),
			    			'request_date'										=> @$request_date,
			    			'date_of_preparation_of_m3'							=> @$date_of_preparation_of_m3,
			    			'm3_no'												=> Input::get('m3_no'),
			    			'total_m3_price'									=> Input::get('total_m3_price'),
			    			'date_of_m3'										=> @$date_of_m3,
			    			'm3_no_to_stock'									=> Input::get('m3_no_to_stock'),
			    			'date_of_m3_to_stock'								=> @$date_of_m3_to_stock,
			    			'contract_modification_price'						=> Input::get('contract_modification_price'),
			    			'total_remained_contract_price'						=> Input::get('total_remained_contract_price'),
			    			'remained_contract_time'							=> Input::get('remained_contract_time'),
			    			'date_of_contract_termination'						=> @$date_of_contract_termination,
			    			'termination_reason'								=> Input::get('termination_reason'),
			    			'remarks'											=> Input::get('remarks'),
			    			'user_id'											=> Auth::user()->id
			    );
			}
			elseif(contractGoodsRoleCheck('procurement_contracts_list')){
				// get the form data in array;
			    $contractData = array(
			    			'procurement_id'									=> Input::get('procurement_id'),
			    			'date_of_submission_letter_to_finance_department'	=> @$date_of_submission_letter_to_finance_department,
			    			'm3_number_sent_to_finance' 						=> Input::get('m3_number_sent_to_finance'),
			    			'amount_sent_to_finance' 							=> Input::get('amount_sent_to_finance'),
			    			'user_id'											=> Auth::user()->id
			    );
			}
			elseif(financeContractRoleCheck('procurement_contracts_list')){
				// get the form data in array;
			    $contractData = array(
			    			'date_of_sent_to_finance' 							=> $date_of_sent_to_finance,
			    			'user_id'											=> Auth::user()->id
			    );
			}
		    // now edit the form data to its table through calling the model function.
		    $object = procurementModel::editContractDetails($main_id, Input::get('field'), $contractData);
		    if($object)
		    {
		    	if(contractRoleCheck('procurement_contracts_list') && contractGoodsRoleCheck('procurement_contracts_list')){
					// get the the log data and insert it into the log table.
					$contractLog = array(
							'procurement_id'									=> Input::get('procurement_id'),
			    			'contract_name'										=> Input::get('contract_name'),
			    			'contract_no'										=> Input::get('contract_no'),
			    			'start_letter_date'									=> @$start_letter_date,
			    			'contract_completion_date'							=> @$contract_completion_date,
			    			'winner_bidder'										=> Input::get('winner_bidder'),
			    			'total_contract_price'								=> Input::get('total_contract_price'),
			    			'request'											=> Input::get('request'),
			    			'request_date'										=> @$request_date,
			    			'date_of_preparation_of_m3'							=> @$date_of_preparation_of_m3,
			    			'm3_no'												=> Input::get('m3_no'),
			    			'total_m3_price'									=> Input::get('total_m3_price'),
			    			'date_of_m3'										=> @$date_of_m3,
			    			'm3_no_to_stock'									=> Input::get('m3_no_to_stock'),
			    			'date_of_m3_to_stock'								=> @$date_of_m3_to_stock,
			    			'date_of_submission_letter_to_finance_department'	=> @$date_of_submission_letter_to_finance_department,
			    			'm3_number_sent_to_finance' 						=> Input::get('m3_number_sent_to_finance'),				    			
			    			'contract_modification_price'						=> Input::get('contract_modification_price'),
			    			'total_remained_contract_price'						=> Input::get('total_remained_contract_price'),
			    			'remained_contract_time'							=> Input::get('remained_contract_time'),
			    			'date_of_contract_termination'						=> @$date_of_contract_termination,
			    			'termination_reason'								=> Input::get('termination_reason'),
			    			'remarks'											=> Input::get('remarks'),
			    			'user'												=> Auth::user()->username,
			    			'action'											=> 'Contract Edited'

					);
				}
		    	elseif(contractRoleCheck('procurement_contracts_list')){
			    	// get the the log data and insert it into the log table.
					$contractLog = array(
							'procurement_id'									=> Input::get('procurement_id'),
			    			'contract_name'										=> Input::get('contract_name'),
			    			'contract_no'										=> Input::get('contract_no'),
			    			'start_letter_date'									=> @$start_letter_date,
			    			'contract_completion_date'							=> @$contract_completion_date,
			    			'winner_bidder'										=> Input::get('winner_bidder'),
			    			'total_contract_price'								=> Input::get('total_contract_price'),
			    			'request'											=> Input::get('request'),
			    			'request_date'										=> @$request_date,
			    			'date_of_preparation_of_m3'							=> @$date_of_preparation_of_m3,
			    			'm3_no'												=> Input::get('m3_no'),
			    			'total_m3_price'									=> Input::get('total_m3_price'),
			    			'date_of_m3'										=> @$date_of_m3,
			    			'm3_no_to_stock'									=> Input::get('m3_no_to_stock'),
			    			'date_of_m3_to_stock'								=> @$date_of_m3_to_stock,		    			
			    			'contract_modification_price'						=> Input::get('contract_modification_price'),
			    			'total_remained_contract_price'						=> Input::get('total_remained_contract_price'),
			    			'remained_contract_time'							=> Input::get('remained_contract_time'),
			    			'date_of_contract_termination'						=> @$date_of_contract_termination,
			    			'termination_reason'								=> Input::get('termination_reason'),
			    			'remarks'											=> Input::get('remarks'),
			    			'user'												=> Auth::user()->username,
			    			'action'											=> 'Contract Edited'

					);
				}
				elseif(contractGoodsRoleCheck('procurement_contracts_list')){
					// get the the log data and insert it into the log table.
					$contractLog = array(
							'procurement_id'									=> Input::get('procurement_id'),
			    			'date_of_submission_letter_to_finance_department'	=> @$date_of_submission_letter_to_finance_department,
			    			'm3_number_sent_to_finance' 						=> Input::get('m3_number_sent_to_finance'),
			    			'amount_sent_to_finance' 							=> Input::get('amount_sent_to_finance'),
			    			'user'												=> Auth::user()->username,
			    			'action'											=> 'Contract Edited'

					);
				}
				elseif(financeContractRoleCheck('procurement_contracts_list')){
					// get the the log data and insert it into the log table.
					$contractLog = array(
			    			'date_of_sent_to_finance' 							=> $date_of_sent_to_finance,
			    			'user'												=> Auth::user()->username,
			    			'action'											=> 'Contract Edited'
					);
				}
				procurementModel::addLogData('contract_log', $contractLog);
		    	return Redirect::route("contractsList")->with("success","The Contract details successfully edited.");
	        }
	        else
	        {
	            return Redirect::route("contractsList")->with("fail","An error occured please try again or contact system developer.");
	        }
	    }
	    else
		{
			return showWarning();
		}    

	}
	// Archive the contracts.
	public function postArchiveContract()
	{
		if(canArchive('procurement_contracts_list'))
    	{
    		$record_id = Input::get('record_id');
    		$archived = procurementModel::archiveContractData($record_id);
    	}
    	else
    	{
    		return showWarning();
    	}
	}
	//deleting the Contract based on id.
    public function postDeleteContract($id=0, $field="")
    {
    	if(canDelete('procurement_contracts_list'))
    	{
	    	// delete the record based on id.
			$deleted = procurementModel::deleteContractData($id, $field);
			
			if($deleted)
	        {
	    		return Redirect::route("contractsList")->with("success","Contract successfully deleted , ID: <span style='color:red;font_weight:bold;'>$id</span>");        	
	        }
	        else
	        {
	            return Redirect::route("contractsList")->with("fail","There was a problem while deleting, please contact system developer");
	        }
	   	}
	   	else
	   	{
	   		return showWarning();
	   	}
    }
	// load the contract detailed information.
	public function loadContractDetails($id, $field="")
	{
		if(canView('procurement_contracts_list'))
		{
			$data['contract'] = procurementModel::getSpecificContractDetails($id, $field);
			//dd($data['contract']);
			return View::make('procurement.viewContract', $data);
		}
		else
		{
			return showWarning();
		}
	}
	//================================================== Archived Contract Functions =====================================

	//Load archived contracts list view
	public function loadArchivedContractList()
	{
		return View::make('procurement.archivedContractList');
	}
	//get archived contracts data for datatable.
	public function getArchivedContractData()
	{
		//get all data 
		$object = procurementModel::getArchivedContracts();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		if(contractGoodsRoleCheck('procurement_contracts_list'))
		{
			return Datatable::collection($collection)
							->showColumns(
								'procurement_id',
								'contract_name',
								'start_letter_date',
								'contract_completion_date',
								'm3_no',
								'total_m3_price',
								'date_of_m3',
								'date_of_m3_to_stock',
								'date_of_submission_letter_to_finance_department',
								'm3_number_sent_to_finance',
								'amount_sent_to_finance'
								)
					->addColumn('start_letter_date', function($option){
						$start_letter_date = checkEmptyDate($option->start_letter_date);
						return $start_letter_date;
					})
					->addColumn('contract_completion_date', function($option){
						$contract_completion_date = checkEmptyDate($option->contract_completion_date);
						return $contract_completion_date;
					})
					->addColumn('date_of_m3', function($option){
						$date_of_m3 = checkEmptyDate($option->date_of_m3);
						return $date_of_m3;
					})
					->addColumn('date_of_m3_to_stock', function($option){
						$date_of_m3_to_stock = checkEmptyDate($option->date_of_m3_to_stock);
						return $date_of_m3_to_stock;
					})
					->addColumn('date_of_submission_letter_to_finance_department', function($option){
						$date_of_submission_letter_to_finance_department = checkEmptyDate($option->date_of_submission_letter_to_finance_department);
						return $date_of_submission_letter_to_finance_department;
					})
					->addColumn('operations', function($option){
							$operation = '';
							$operation = "<span style='color:red'><b>Archived</b></span> | ";
							if(canView('procurement_contracts_list')){
								$operation .= '<a href="'.route('loadContractDetails',array('id' => $option->procurement_id, 'field' => $option->total_remained_contract_price)).'">Show Details</a>';
							};
							return $operation;
					})
					// ->orderColumns('id','id')
					->make();
		}else{
			return Datatable::collection($collection)
							->showColumns(
								'procurement_id',
								'contract_name',
								'start_letter_date',
								'contract_completion_date',
								'm3_no',
								'total_m3_price',
								'date_of_m3',
								'm3_no_to_stock',
								'date_of_m3_to_stock',
								'date_of_submission_letter_to_finance_department'
								)
					->addColumn('start_letter_date', function($option){
						$start_letter_date = checkEmptyDate($option->start_letter_date);
						return $start_letter_date;
					})
					->addColumn('contract_completion_date', function($option){
						$contract_completion_date = checkEmptyDate($option->contract_completion_date);
						return $contract_completion_date;
					})
					->addColumn('date_of_m3', function($option){
						$date_of_m3 = checkEmptyDate($option->date_of_m3);
						return $date_of_m3;
					})
					->addColumn('date_of_m3_to_stock', function($option){
						$date_of_m3_to_stock = checkEmptyDate($option->date_of_m3_to_stock);
						return $date_of_m3_to_stock;
					})
					->addColumn('date_of_submission_letter_to_finance_department', function($option){
						$date_of_submission_letter_to_finance_department = checkEmptyDate($option->date_of_submission_letter_to_finance_department);
						return $date_of_submission_letter_to_finance_department;
					})
					->addColumn('operations', function($option){
							$operation = '';
							$operation = "<span style='color:red'><b>Archived</b></span> | ";
							if(canView('procurement_contracts_list')){
								$operation .= '<a href="'.route('loadContractDetails',array('id' => $option->procurement_id, 'field' => $option->total_remained_contract_price)).'">Show Details</a>';
							};
							return $operation;
					})
					// ->orderColumns('id','id')
					->make();
		}
		
	}

}

?>