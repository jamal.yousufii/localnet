<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use Input;
use Hash;
use Illuminate\Support\Facades\Session;
use App\models\User;
use App\models\DepartmentX;
use Illuminate\Support\Collection;

use App\models\Section;
use App\models\Application;
use App\models\RoleX;
use App\models\DocTree;
use App\models\Document;
use App\models\DepartmentModule;
use HTML;
use Illuminate\Support\Str;
use File;
/**
* Controlling and manipulting user information and operations
* @author Gul Muhammad Akbari (gm.akbari27@gmail.com)
* @version 1.0
* @date 21 Feb 2015
*/


class UserController extends Controller
{

	//get all userssend
	public function getUsers($department=0)
	{
		//check roles
		if(canView('auth_user'))
		{
			//load view for users list
			return View::make("auth.user.user_list");
		}
		else
		{
			return showWarning();
		}

	}
	//get data for datatable
	public function getData()
	{
		//check roles
		if(canView('auth_user'))
		{
			//get data from model
			$users = User::getData();
			$collectin = new Collection($users);
			return \Datatable::collection($collectin)
						->showColumns('id','full_name','father_name','username','email','position','dep_name')
						->addColumn('operation', function($option){
							$options = '';
							if(canEdit('auth_user'))
							{
								$options .= '<a href="'.route('getUpdate',$option->id).'"><i class="fa fa-edit"></i> Edit</a> &nbsp;';
							}
							if(canDelete('auth_user'))
							{
								$options .= '|&nbsp;<a href="'.route('getDelete',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="fa fa-trash-o"></i> Delete</a>';
							}
							return $options;
						})
						->make();
		}
		else
		{
			return showWarning();
		}
	}
	//get form registration for user
	public function getCreate()
	{
		//check roles
		if(canAdd('auth_user'))
		{
			//get all departments for user
			$dep = DB::table('department');
			if(Auth::user()->is_manager == 1 && Auth::user()->is_admin == 0)
			{
				$dep->whereIn('id',getAllSubDepartmentsWithParent(Auth::user()->department_id));
			}
			$dep = $dep->get();
			//get all positions for user
			$positions = DB::table('position')->get();
			//load view for user registration
			return View::make('auth.user.register')->with("departments",$dep)->with('positions',$positions);
		}
		else
		{
			return showWarning();
		}
	}
	//get all users
	public function getUpdate($id=0)
	{
		//check roles
		if(canEdit('auth_user'))
		{

			//get data from model
			$users = User::getUserDetails($id);

			$auth_user_id = Auth::user()->id; 
			//get all user modules 
			$userModules = User::getUserRelated('user_module',$id);
			$selectedModules = array(0);
			foreach($userModules AS $item)
			{
				$selectedModules[] = $item->module_id;
			}
			//get all user entities
			$userEntities = User::getUserRelated('user_entity',$id);
			$selectedEntities = array();
			foreach($userEntities AS $item)
			{
				$selectedEntities[] = $item->entity_id;
			}
			//get all user roles
			$userRoles = User::getUserRelated('user_role',$id);

			//get department for user
			$department_id = DB::table('users')->where('id', $id)->pluck('department_id');
			
			$dep = DB::table('department');
			if(Auth::user()->is_manager == 1 && Auth::user()->is_admin == 0)
			{
				$dep->whereIn('id',getAllSubDepartmentsWithParent(Auth::user()->department_id));
			}
			$dep = $dep->get();
			
			$pos = DB::table('position')->get();
			$modules = DepartmentX::getDepartmentApp($department_id,$auth_user_id);
			$entities = DB::table('entity')->whereIn('module_id',$selectedModules)->get();
			$roles = DB::table('role')->whereIn('entity_id',$selectedEntities)->get();
			
			$data['dep_employees'] = DepartmentX::getDepartmentEmployees($department_id);
			//set all data to an array--------------
			$data['user_data'] = $users;
			$data['departments'] = $dep;
			$data['positions']	= $pos;
			$data['userModules'] = $userModules;
			$data['userEntities'] = $userEntities;
			$data['userRoles']	= $userRoles;
			
			$data['modules'] = $modules;
			$data['entities'] = $entities;
			$data['roles']	= $roles;
			//--------------------------------------
			//load view for users list
			return View::make("auth.user.edit",$data);
		}
		else
		{
			return showWarning();
		}

	}
	
	public function getLogin()
	{
		return view('auth.user.login');
	}
	
	public function postCreate()
	{
		//check roles
		if(canAdd('auth_user'))
		{
			//validate the input fields
		    $validates = \Validator::make(Input::all(),array(
		        "first_name" 	=> "required",
		        "father_name" 	=> "required",
		        "department" 	=> "required",
		        //"position" 		=> "required",
		        "username" 		=> "required|min:4|unique:users",
		        "pass1"    		=> "required|min:6",
		        "pass2"    		=> "required|same:pass1",
		    ));
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return \Redirect::route("getCreate")->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        $user = new User();
		        $user->first_name 		= Input::get("first_name");
		        $user->last_name 		= Input::get("last_name");
		        $user->father_name 		= Input::get("father_name");
		        $user->email 			= Input::get("email");
		        //$user->position_id 		= Input::get("position");
		        $user->department_id 	= Input::get("department");
		        $user->username 		= Input::get("username");
		        $user->employee_id 		= Input::get("employee");
		        $user->password 		= Hash::make(Input::get("pass1"));
		        $user->status 			= '1';
		        $user->photo 			= Input::get("username").'.jpg';

		        if(Input::get('is_admin'))
		        {
		        	$user->is_admin = '1';
		        }
		        if(Input::get('is_manager'))
		        {
		        	$user->is_manager = '1';
		        }
		        
		        if($user->save())
		        {
		        	$userId = $user->id;
		        	
		        	//user modules ---------------------------
		        	$modules = Input::get('user_module');
		        	if(count($modules)>0)
		        	{
		        		$modData = array();
		        		for($i=0;$i<count($modules);$i++)
		        		{
		        			$row = array(
		        					'module_id' => $modules[$i],
		        					'user_id'	  => $userId
		        				);
		        			//push in array
		        			array_push($modData, $row);
		        		}

		        		//insert user modules
		        		User::insertUserRelated('user_module',$modData);
		        	}
		        	//----------------------------------------

		        	//user entities --------------------------
		        	$entities = Input::get('user_entity');
		        	if(count($entities)>0)
		        	{
		        		$allData = array();
		        		for($i=0;$i<count($entities);$i++)
		        		{
		        			$row = array(
		        					'entity_id' => $entities[$i],
		        					'user_id'	  => $userId
		        				);
		        			//push in array
		        			array_push($allData, $row);
		        		}

		        		//insert user entities
		        		User::insertUserRelated('user_entity',$allData);
		        	}
		        	//----------------------------------------

		        	//user roles--- --------------------------
		        	$roles = Input::get('user_role');
		        	if(count($roles)>0)
		        	{
		        		$allData = array();
		        		for($i=0;$i<count($roles);$i++)
		        		{
		        			$row = array(
		        					'role_id' => $roles[$i],
		        					'user_id'	  => $userId
		        				);
		        			//push in array
		        			array_push($allData, $row);
		        		}

		        		//insert user roles
		        		User::insertUserRelated('user_role',$allData);
		        	}
		        	//----------------------------------------
		            
		            return \Redirect::route("getAllUsers")->with("success","You successfuly created user and now you can login.");
		        }
		        else
		        {
		            return \Redirect::route("getAllUsers")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}
	}
	public function postUpdate($id)
	{
		//check roles
		if(canEdit('auth_user'))
		{
		
			//validate the input fields
		    $posts = array(
			        "first_name" => "required",
			        "father_name" => "required",
			        "department" => "required",
			        "username" => "required|min:4",
			        //"position"	=> "required"
			    );
		    if(Input::get('pass1') && Input::get('pass2'))
		    {
		    	$posts['pass1'] = "required|min:6";
		    	$posts['pass2'] = "required|same:pass1";
		    }

		    $validates = \Validator::make(Input::all(),$posts);
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return \Redirect::route("getUpdate",$id)->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        $user = User::find($id);
		        $user->first_name = Input::get("first_name");
		        $user->last_name = Input::get("last_name");
		        $user->father_name = Input::get("father_name");
		        $user->department_id = Input::get("department");
		        $user->username = Input::get("username");
		        $user->employee_id = Input::get("employee");
		        $user->photo = Input::get("username").'.jpg';
		        //$user->position_id = Input::get("position");
		        $user->email = Input::get("email");

		        if(Input::get('pass1'))
			    {
			    	$user->password = Hash::make(Input::get('pass1'));
			    }
		        
		        //$user->password = Hash::make(Input::get("pass1"));
		        //$user->status = '1';
		        if(Input::get('is_admin'))
		        {
		        	$user->is_admin = '1';
		        }
		        else
		        {
		        	$user->is_admin = 0;
		        }

		        if(Input::get('is_manager'))
		        {
		        	$user->is_manager = '1';
		        }
		        else
		        {
		        	$user->is_manager = 0;
		        }
		        
		        if($user->save())
		        {
		            
		            //user modules ---------------------------
		        	$modules = Input::get('user_module');
		        	//remove old user module
		        	User::removeUserRelated('user_module',$id);
		        	
		        	if(count($modules)>0)
		        	{
		        		$modData = array();
		        		for($i=0;$i<count($modules);$i++)
		        		{
		        			$row = array(
		        					'module_id' => $modules[$i],
		        					'user_id'	  => $id
		        				);
		        			//push in array
		        			array_push($modData, $row);
		        		}

		        		//insert user modules
		        		User::insertUserRelated('user_module',$modData);
		        	}
		        	//----------------------------------------

		        	//user entities --------------------------
		        	$entities = Input::get('user_entity');
		        	//remove old user module
		        	User::removeUserRelated('user_entity',$id);

		        	if(count($entities)>0)
		        	{
		        		
		        		$allData = array();
		        		for($i=0;$i<count($entities);$i++)
		        		{
		        			$row = array(
		        					'entity_id' => $entities[$i],
		        					'user_id'	  => $id
		        				);
		        			//push in array
		        			array_push($allData, $row);
		        		}

		        		//insert user entities
		        		User::insertUserRelated('user_entity',$allData);
		        	}
		        	//----------------------------------------

		        	//user roles--- --------------------------
		        	$roles = Input::get('user_role');
		        	//remove old user module
		        	User::removeUserRelated('user_role',$id);

		        	if(count($roles)>0)
		        	{
		        		
		        		$allData = array();
		        		for($i=0;$i<count($roles);$i++)
		        		{
		        			$row = array(
		        					'role_id' => $roles[$i],
		        					'user_id'	  => $id
		        				);
		        			//push in array
		        			array_push($allData, $row);
		        		}

		        		//insert user roles
		        		User::insertUserRelated('user_role',$allData);
		        	}
		        	//----------------------------------------

		            return \Redirect::route("getAllUsers")->with("success","The record updated successfully.");
		        }
		        else
		        {
		            return \Redirect::route("getAllUsers")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}
	}
	public function changePassword()
	{
		  
        $user = User::find(Auth::user()->id);
        
        if(Input::get('new_password'))
	    {
	    	$user->password = Hash::make(Input::get('new_password'));
	    	//$user->original_pass = Input::get('new_password');
	    }

        if($user->save())
        {
            return "Your password changed, You need to logout and login again!";
        }
        else
        {
            return "Error!";
        }
	}
	public function checkOldPassword()
	{

		$users = User::getUserDetails(Auth::user()->id);

		$old_pass="";
		foreach($users AS $item)
		{
			$old_pass = $item->password;
		}
		
		echo $old_pass.'<br>'.Hash::make(Input::get("old_password"));exit;
		
		if(Hash::make(Input::get('old_password')) == $old_pass)
		{
			return json_encode(array('cond'=>'true'));
		}
		else
		{
			return json_encode(array('cond'=>'false'));
		}

	}
	public function getDelete($id)
	{
		//check roles
		if(canDelete('auth_user') && Auth::user()->is_admin == 1)
		{
			
			$user = User::find($id);
			if($user->delete())
	        {
	            return \Redirect::route("getAllUsers")->with("success","You successfuly deleted record, ID: <span style='color:red;font_weight:bold;'>{{$id}}</span>");
	        }
	        else
	        {
	            return \Redirect::route("getAllUsers")->with("fail","An error occured plase try again.");
	        }
	    }
	    else
	    {
	    	return showWarning();
	    }
	}
	
	// public function postLogin(Request $request)
	// {
	// 	$validates = $this->validate($request,array(
	// 	        "username"  => "required",
	// 	        "password"  => "required"
	// 	    ));
		    
	// 	    //check the validation
	// 	    if($validates)
	// 	    {
	// 	        return \Redirect::route("getLogin")->withErrors($validates)->withInput();
	// 	    }
	// 	    else
	// 	    {
		        
	// 	        $remember = ($request->has('remember')) ? true:false;
		        
	// 	        $auth = Auth::attempt(array(
	// 	            "username" => $request->get("username"),
	// 	            "password" => $request->get("password")
	// 	        ),$remember);
	// 	        /**
	// 	        *Check if login success and then set the roles
	// 	        */
	// 	        if($auth)
	// 	        {   
		        	
	// 	        	//call function helper to login in webmail
	// 	        	//loginWebmail(); 
				    
	// 	        	//set lang session
	// 	        	Session::put('lang','en');
	// 	        	//set roles to session
	// 	        	if(setSessionRoles())
	// 	        	{
	// 	        		return \Redirect::intended("/")->with("success","You are logged in successfuly.");
	// 	        	}
	// 	        	else
	// 	        	{
	// 	        		return \Redirect::intended("/")->with("fail","Access denied!.");
	// 	        	}
		            
		            
	// 	        }
	// 	        else
	// 	        {   
	// 	            return \Redirect::route("getLogin")->with("fail","Your login information is not currect. Please try again.");
	// 	        }
	// 	    }
	// }
	// //get logout the user
    public function getLogout()
    {
        Auth::logout();
        //logoutWebmail();
        Session::forget('lang');
        return \Redirect::route("getLogin");
    }

    /*
	Getting all records from modules
	@param: department id
	@Accessiblity: public
	@return: Object
	*/
	public function getDepApp()
	{
		if(canAdd('auth_user') || canEdit('auth_user'))
		{
            $dep_id = Input::get('id');
            //get data from model
			$apps = DepartmentX::getDepartmentApp($dep_id);
			$emps = DepartmentX::getDepartmentEmployees($dep_id);
			// var_dump($emps);exit;
			$options = "";
			$empOptions = "<option value=''>Select</option>";

			foreach($apps AS $item)
			{
				$options .= "<option value='".$item->mid."'>".$item->mname."</option>";
			}

			foreach($emps AS $employee)
			{
				$empOptions .= "<option value='".$employee->id."'>".$employee->name."</option>";
			}
			return json_encode(array('apps'=>$options,'emps'=>$empOptions));
		}
		else
		{
			return showWarning();
		}
	}
	/*
	Getting all records from modules
	@param: department id
	@Accessiblity: public
	@return: Object
	*/
	public function getAppSection()
	{
		if(canAdd('auth_user') || canEdit('auth_user'))
		{

			//get all selected sections
			$selectedSections = Section::getUserSections(Input::get('userid'));
			$selectedSectionArr = array();
			foreach($selectedSections AS $item)
			{
				$selectedSectionArr[] = $item->entity_id;
			}

			$codes = Input::get('codes');
			//get data from model
			$sections = Section::getAllByModId($codes);

			$page = route('getUserSectionRole');

			$options = '<label class="col-sm-2 control-label">Sections</label>';
			$options .= '<div class="col-sm-4">';
			$options .= '<select style="height:120px;" multiple="multiple" name="user_entity[]" id="user_entity" onchange="bringMultiRelated(\''.$page.'\',\'user_entity\',\'entity_role\',\'&userid='.Input::get('userid').'\');" class="form-control">';
			
			$moduleId = '';
			$moduleName = '';
			foreach($sections AS $item)
			{
				$moduleId = $item->module_id;
				$moduleName = $item->module;
				break;
			}

			$options .= "<optgroup label='".$moduleName." Sections'>";
			foreach($sections AS $item)
			{
				if($item->module_id != $moduleId)
				{
					$options .= "</optgroup>";
					$options .= "<optgroup label='".$item->module." Sections'>";
				}
				$lang = getLang();
				$section_name = "name_".$lang;
				if(in_array($item->id, $selectedSectionArr))
				{
					$options .= "<option selected value='".$item->id."'>".$item->$section_name."</option>";
				}
				else
				{
					$options .= "<option value='".$item->id."'>".$item->$section_name."</option>";
				}

				$moduleId = $item->module_id;

			}
			$options .= "</select></div>";
			return $options;
		}
		else
		{
			return showWarning();
		}
	}
	/*
	Getting all records
	@param: department id
	@Accessiblity: public
	@return: Object
	*/
	public function getSectionRole()
	{
		if(canAdd('auth_user') || canEdit('auth_user'))
		{

			//get all selected roles
			$selectedRoles = Section::getUserRoles(Input::get('userid'));
			$selectedRoleArr = array();
			foreach($selectedRoles AS $item)
			{
				$selectedRoleArr[] = $item->role_id;
			}

			$codes = Input::get('codes');
			//get data from model
			$sections = RoleX::getAllBySecId($codes);
			
			$options = '<label class="col-sm-2 control-label">Roles</label>';
			$options .= '<div class="col-sm-4">';
			$options .= '<select style="height:120px;" multiple="multiple" name="user_role[]" id="user_role" class="form-control">';
			
			$entityId = '';
			$entityName = '';
			foreach($sections AS $item)
			{
				$entityId = $item->entity_id;
				$entityName = $item->section;
				break;
			}

			$options .= "<optgroup label='".$entityName." Roles'>";

			foreach($sections AS $item)
			{
				if($item->entity_id != $entityId)
				{
					$options .= "</optgroup>";
					$options .= "<optgroup label='".$item->section." Roles'>";
				}

				if(in_array($item->id, $selectedRoleArr))
				{
					$options .= "<option selected value='".$item->id."'>".$item->name."</option>";
				}
				else
				{
					$options .= "<option value='".$item->id."'>".$item->name."</option>";
				}
				$entityId = $item->entity_id;
			}
			$options .= "</select></div>";
			return $options;
		}
		else
		{
			return showWarning();
		}
	}

	public function getEmployeeDetails()
	{
		if(canAdd('auth_user') || canEdit('auth_user'))
		{
			
			//get all selected roles
			$details = DepartmentX::getEmployeeDetails(Input::get('id'));
			$elements = array();
			foreach($details AS $item)
			{
			    $elements['first_name'] = $item->name;
				$elements['last_name'] = $item->last_name;
                $elements['father_name'] = $item->father_name;
				$elements['name_en'] = $item->name_en;
                $elements['userid'] = $item->dep_no.'_'.$item->userid;
				$elements['position'] = $item->position;
				$elements['email'] = $item->email;
				$elements['photo'] = HTML::image("/documents/profile_pictures/".$item->photo,'',array('class' => 'profile-img img-responsive center-block','data-original-title'=>'','data-toggle'=>'tooltip'));
			}

			return json_encode($elements);

			
		}
		else
		{
			return showWarning();
		}
	}
	//change module
	public function changeModule(Request $request,$moduleCode='auth')
	{
		//$moduleCode = Input::get('code');
		//set to default module session the new code
		//Session::put('default_module',$moduleCode);
		//redirect to new module dashboard
		//$name = $request->path();var_dump($name);
		//var_dump($request->segment(1));
		switch ($moduleCode) {
			case 'auth':
				return \Redirect::route('getAllDepartments');
				break;
			case 'task':
				return \Redirect::route('getReport');
				break;
			case 'sched':
				return \Redirect::route('MeetingForm');
				break;
			case 'docscom':
				return \Redirect::route('docsGetForm');
				break;
			case 'finance':
				return \Redirect::route('getFinanceList');
				break;
			case 'hr':
				return \Redirect::route('hrHomePage');
				break;
			case 'ocshr':
				return \Redirect::route('hrHomePage-ocs');
				break;	
			case 'icom':
				return \Redirect::route('getLoadDepList');
				break;
			case 'contacts':
				return \Redirect::route('arg.net');
				break;
			case 'procurement':
				return \Redirect::route('procurementHomePage');
				break;
			case 'casemgmt':
				return \Redirect::route('caseForm');
				break;
			case 'evaluation':
				return \Redirect::route('evaluationHomePage');
				break;
			case 'const_maintenance':
				return \Redirect::route('getEstateRegistryList');
				break;
			case 'procurement_inventory':
				return \Redirect::route('procInventoryList');
				break;
			case 'library':
				return \Redirect::route('libraryList');
				break;
			case 'specification':
				return \Redirect::route('recordsList');
				break;
			case 'executive_management':
				return \Redirect::route('recordsList_executive');
				break;
			case 'document_tracking':
				return \Redirect::route('getIncomingDocsList');
				break;
			case 'followup_document_tracking':
				return \Redirect::route('followupGetIncomingDocsList');
				break;
			default:
				return \Redirect::route('home');
				break;
		}
		
	}

	public function showWarning()
	{
		return View::make('layouts.warning');
	}

	//get webmail interface
	public function getEmails()
	{
		return View::make('layouts.webmail');
	}
	//change user profile picture
	public function updateUserProfilePicture()
	{
		
		// getting all of the post data
		$files = Input::file('files');
		$errors = "";
		$file_data = array();
		
		foreach($files as $file) 
		{
			
			if(Input::hasFile('files'))
			{
			
			  // validating each file.
			  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			  $validator = \Validator::make(

			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'file' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			  	);

			  if($validator->passes())
			  {
			  	
			    // path is root/uploads
			    $destinationPath = 'documents/profile_pictures';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    
			    
			    $filename = 'custom_profile_'.Auth::user()->employee_id.'_'.Auth::user()->id.'.'.$extension;
				
				//first delete the old custom profile picture
				$this->deleteFile();
			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
				   
				   $data = array(
				   					'photo'=>$filename
				   				);
				   
				   if(count($data)>0)
					{
						DB::table("users")->where("id",Auth::user()->id)->update($data);
					}
				   
				   return \HTML::image('/documents/profile_pictures/'.$filename, '', array('class' => 'profile_pic','style'=>'width:220px;border-radius:10px;'));
				   
				} 
				else 
				{
				   return "<div class='alert alert-danger'>Error with file</div>";
				}
					
				
			  } 
			  else 
			  {
			    // redirect back with errors.
			    return "<div class='alert alert-danger'>Validation Error</div>";
			  }
			}

		}
			
	}
	//change photo type
	public function changePhotoType()
	{
		
		if(DB::table("users")->where("id",Auth::user()->id)->update(array("use_custom_photo"=>Input::get('value'))))
		{
			return "changed";
		}
		else
		{
			return "Error";
		}
			
			
		
	}
	//remove file from folder
	public function deleteFile()
	{
		
			$photo = DB::table("users")->where("id",Auth::user()->id)->pluck("photo");
			
			$file= public_path(). "/documents/profile_pictures/".$photo;
			$thumb= public_path(). "/documents/profile_pictures/small_".$photo;
			
			if(File::delete($file) && File::delete($thumb))
			{
				
				return true;
			}
			else
			{
				return false;
			}
		
	}
	
}

?>