<?php namespace App\Http\Controllers;
/*
@desc: Localizes the modules in Pashto, Dari and English.
@Author: Gul Muhammad Akbari(gm.akbari27@gmail.com)
@Created At: 10 Aug 2015
@version: 1.0
*/
use Auth;
use View;
use Input;
use App\models\Localization;
use Redirect;


class LocalizationController extends Controller
{


	public function getAllApps()
	{
		
		//get application for localization
		$data['apps'] = Localization::getApps();
		//load view for list
		return View::make('auth.localization.appList',$data);//->with('records',$apps);
		
	}

	//scan application folder for gettext field
	public function scanApplicationFolder($appCode='',$appId=0)
	{

	    //print_r(scanAppDirectory($appCode));

	    //get application database key fields
	    $fieldsInDb = Localization::getLangFields($appId);

	    $dbFields = array();
	    //loop the database fields
	    foreach($fieldsInDb AS $f)
	    {
	    	//collect to an array
	    	$dbFields[] = $f->key;
	    }

	    //get scanned fields
	    $scannedFields = scanAppDirectory($appCode);

	    //make records for insertion
	    $rows = array();
	    for($i=0;$i<count($scannedFields);$i++)
	    {
	    	if(!in_array($scannedFields[$i], $dbFields))
	    	{
	    		$data = array(
	    				"module_id"=>$appId,
	    				"key"=>$scannedFields[$i],
	    				"created_at"=>date('Y-m-d H:i:s')
	    			);
	    		//push to row array
	    		$rows[] = $data;
	    	}
	    }

	    if(!empty($rows))
	    {
		    //insert new key fields
		    Localization::insertKeys($rows);
		}

		//get latest fields
	    $data['fields'] = Localization::getLangFields($appId);
	    $data['appId'] = $appId;
	    $data['appCode'] = $appCode;

	    return View::make('auth.localization/field_list',$data);



	}

	//update localized fields according application
	public function updateLocalizedFields()
	{
		
		$keys = Input::get('key');
		$en = Input::get('english');
		$dr = Input::get('dari');
		$pa = Input::get('pashto');

		$rows = array();

		$allKeys = array();

		for($i=0;$i<count($keys);$i++)
		{
			$allKeys[] = $keys[$i];

			$data = array(
					"module_id"		=> Input::get('appId'),
					"key" 			=> $keys[$i],
					"english" 		=> $en[$keys[$i]],
					"dari" 			=> $dr[$keys[$i]],
					"pashto" 		=> $pa[$keys[$i]],
					"created_at"	=> date('Y-m-d H:i:s'),
					"updated_at"	=> date('Y-m-d H:i:s')
				);

			$rows[] = $data;
		}

		//first remove all old records for this application
		Localization::removeOldRecords(Input::get('appId'),$allKeys);

		//insert translated fields for this application
		Localization::insertKeys($rows);

		//redirect to translated form
		return Redirect::route('scanApplicationFolder',array(Input::get('appCode'),Input::get('appId')))->with("success","Form Translated Successfully!");;

	}

	/**
	*Generating .po file using php arrays
	*@param: application id
	*@param: application code
	*@return: file object
	*@access: public
	*/
	public function generatingMoFile($appId=0,$appCode='')
	{
		//get all application fields
		$fields = Localization::getLangFields($appId);

		generateMoFile($fields,$appCode);

		//redirect to translated form
		return Redirect::route('scanApplicationFolder',array($appCode,$appId))->with("success","MO File Generated Successfully!");


	}

	//translate fields save and next
	public function translateFields($appCode='',$appId=0,$current=0)
	{
		//print_r($_POST);exit;
		if($current ==0)
		{
			$state = "next";
		}
		else
		{
			$state = '';
		}
		//get latest fields
	    $data['fields'] = Localization::getFieldLimit($appId,$current,$state);
	    $data['appId'] = $appId;
	    $data['appCode'] = $appCode;
	    $data['all'] = Localization::getLangFields($appId,true);
	    return View::make('auth.localization.saveAndNext',$data);

	}

	public function saveAndNext()
	{

		$keys 	= Input::get('key');
		$en 	= Input::get('english');
		$dr 	= Input::get('dari');
		$pa 	= Input::get('pashto');

		for($i=0;$i<count($keys);$i++)
		{
			
			$data = array(
					"english" 		=> $en[$keys[$i]],
					"dari" 			=> $dr[$keys[$i]],
					"pashto" 		=> $pa[$keys[$i]],
				);

			
		}

		
		//update fields
		Localization::updateKey(Input::get('current'),$data);

		//get latest fields
	    $d['fields'] 	= Localization::getFieldLimit(Input::get('appId'),Input::get('current'),Input::get('state'));
	    $d['appId'] 	= Input::get('appId');
	    $d['appCode'] 	= Input::get('appCode');



		return View::make('auth.localization.ajax_response',$d);
	}

	public function getPageForm()
	{
		//get latest fields
	    $d['fields'] 	= Localization::getFieldLimit(Input::get('appId'),Input::get('current'));
	    $d['appId'] 	= Input::get('appId');
	    $d['appCode'] 	= Input::get('appCode');



		return View::make('auth.localization.ajax_response',$d);

	}

	public function getFieldLimit()
	{
		$data['appId'] 		= Input::get('appId');
		$data['appCode'] 		= Input::get('appCode');
		$data['current'] 	= Input::get('current');
		$data['state'] 		= Input::get('state');
		$data['counter'] 	= Input::get('counter');

		$data['rows'] = Localization::getNextPagination($data);
		
		return View::make('auth.localization.page_numbers',$data);

	}



}