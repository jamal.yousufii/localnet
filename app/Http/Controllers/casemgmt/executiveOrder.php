<?php

// getting the table information without authentication.

namespace App\Http\Controllers\casemgmt;
use App\Http\Controllers\NoLoginController;
use Illuminate\Support\Collection;
use App\models\casemgmt\caseModel;
use Datatable;
use DB;
use View;
use Auth;
use Illuminate\Http\Request;
use App\library\jdatetime;
use Validator;
use Input;
use Redirect;
use Response;
use Session;
use DateTime;
use DateInterval;
use File;

class executiveOrder extends NoLoginController{
	//load the agency list page.
	public function loadExecOrderView()
	{
		return View::make('casemgmt.executiveOrderList');
	}
	// get the list of the agencies
	public function execOrderData()
	{
		//get the cases list for datatable.
		$sql = "SELECT
				    c.*, task_temp.status,cat.name AS task_category, trl.name as task_request_level
				FROM
				    casemgmt.cases AS c
				
				    LEFT JOIN ( select * from (SELECT * from casemgmt.task_progress order by id desc) t group by case_id ) as task_temp
				
				    ON (c.id = task_temp.case_id)
					
					LEFT JOIN casemgmt.category AS cat ON cat.id = c.task_category
					LEFT JOIN casemgmt.task_request_level AS trl ON trl.id = c.task_request_level
					WHERE
					c.deleted = 0";
		// $sql = "SELECT
		// 		    c.*, task_temp.status
		// 		FROM
		// 		    casemgmt.cases AS c
				
		// 		    LEFT JOIN ( SELECT p1.*
		// 				FROM casemgmt.task_progress as p1
		// 				LEFT JOIN casemgmt.task_progress AS p2
		// 				     ON p1.case_id = p2.case_id AND p1.created_at < p2.created_at
		// 				WHERE p2.case_id IS NULL ) as task_temp
				
		// 		    ON (c.id = task_temp.case_id)";
		$object = DB::select($sql);
		
		//$object = caseModel::getData();
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'created_at',
							'task_title',
							'status',
							'task_assigned_to',
							'deadline'
							)
				->addColumn("task_title",function($option){
					if($option->task_title == "")
					{
						$datetime1 = strtotime($option->created_at);
						$datetime2 = strtotime(date("Y-m-d H:i:s"));
						$interval  = abs($datetime2 - $datetime1);
						$minutes   = round($interval / 60);
						//return "";
						if($minutes <= 60)
						{
							$m = $minutes*60*1000;
							$element = "<span id='clock".$option->id."'></span>
										<script type='text/javascript'>
										   var fiveSeconds = new Date().getTime() + ".$m.";
											  $('#clock".$option->id."').countdown(fiveSeconds, {elapse: true})
											  .on('update.countdown', function(event) {
											      $(this).html(event.strftime('<span style=\'color:red\'>%M:%S</span> - Pending transcription'));
											 });
										 </script>
										";
							return $element;
						}
						else
						{
							return "Pending transcription";
						}
					}
					else
					{
						return $option->task_title;
					}
				})
				->addColumn('task_assigned_to', function($option){
					if($option->assigned_to_option == "agency"){
						$lang = getLangShortcut();
						//get the name of agency based on id.
						$agency = DB::connection('casemgmt')->table('agencies');
						if($lang == "dr"){
							$agency->select('name_dr')->where('id', $option->task_assigned_to);
							$task_agency = $agency->first();
							if($task_agency != null){return $task_agency->name_dr;}else{return "";}
						}else{
							$agency->select('name_en')->where('id', $option->task_assigned_to);
							$task_agency = $agency->first();
							//dd($task_agency->name_en);
							if($task_agency != null){return $task_agency->name_en;}else{return "";}
						}
					}
					return "";
				})
				->addColumn('created_at', function($option){
					$r='<input type=hidden class="cases_id" value="'.route('caseDetails',$option->id).'">';
					if(isMiladiDate()){
						$date = $option->created_at;
						$date = explode(" ", $date);
						$g_date = $date[0];
						$time = $date[1];
						$gregorianDate = $g_date;
						return $gregorianDate.$r;
					}else{
						$date = $option->created_at;
						$date = explode(" ", $date);
						$j_date = $date[0];
						$time = $date[1];
						$jalali_date = dmy_format(toJalali($j_date));
						$jalali_datetime = $jalali_date;
						return $jalali_datetime.$r;
					}
				})
				->addColumn('status', function($option){
					if($option->status == "0"){
						return "<div style='width: 10px;height: 10px;display: inline-block;background-color: red'></div>&nbsp; Emergency";
					}elseif($option->status == "1"){
						return "<div style='width: 10px;height: 10px;display: inline-block;background-color: #d3d3d3'></div>&nbsp; Not Started";
					}
					elseif($option->status == "2"){
						return "<div style='width: 10px;height: 10px;display: inline-block;background-color: orange'></div>&nbsp; Started";
					}
					elseif($option->status == "3"){
						return "<div style='width: 10px;height: 10px;display: inline-block;background-color: green'></div>&nbsp; Finished";
					}
					elseif($option->status == "4"){
						return "<div style='width: 10px;height: 10px;display: inline-block;background-color: blue'></div>&nbsp; Feedback";
					}
					else{
						return "<div style='width: 10px;height: 10px;display: inline-block;background-color: #d3d3d3'></div>&nbsp; Not Started";
					}
				})
				->addColumn('deadline', function($option){
					if(isMiladiDate()){
						if($option->deadline == "0000-00-00" || $option->deadline == ""){
							return "";
						}else{
							return $option->deadline;
						}
					}else{
						$deadline = dmy_format(toJalali($option->deadline));
						return $deadline;
					}
				})
				->addColumn('Open', function($option){
					return '<a href="'.route('caseDetails',$option->id).'" title="View Record Details"><img width="48px" height="48px" src="/img/info1.ico"></i></a>'; 
				})
				->make();
	}
	//get load the task detail page for president.
	public function taskDetailsForPresident($id=0)
	{
		$data['task_details'] = caseModel::getCaseDetails($id);
		$data['task_attachments'] = caseModel::getCaseAttachments($id);
		$data['task_assigned_to'] = "";
		$data['name'] = "";
		$data['department'] = "";
		//get lang shortcut
		$lang = getLangShortcut();
		$assigned_to_option = DB::connection("casemgmt")->table("cases")->select("assigned_to_option","task_assigned_to")->where('id', $id)->first();
		if($assigned_to_option->assigned_to_option == "in_house"){
			$object = DB::connection("casemgmt")->table("category")->select('name')->where('id', $assigned_to_option->task_assigned_to)->first(); 
			if($object != null){
				$data['task_assigned_to'] = $object->name;
			}
		}
		if($assigned_to_option->assigned_to_option == "agency"){
			$object = DB::connection("casemgmt")->table("agencies")->select('name_'.$lang)->where('id', $assigned_to_option->task_assigned_to)->first(); 
			if($object != null){
				if($lang == "dr")
				$data['task_assigned_to'] = $object->name_dr;
				else
				$data['task_assigned_to'] = $object->name_en;
			}
		}
		if($assigned_to_option->assigned_to_option == "external"){
			$person = DB::connection("casemgmt")->table("person")->select('name')->where('case_id', $id)->first(); 
			$department = DB::connection("casemgmt")->table("department")->select('name')->where('case_id', $id)->first(); 
			if($person != null){$data['name'] = $person->name;}
			if($department != null){$data['department'] = $department->name;}
		}
		
		return View::make("casemgmt.taskDetailsForPresident", $data);
	}
	//get the list of all progresses.
	public function getCaseProgressData($id=0)
	{
		//get all data 
		$object = caseModel::getCaseProgressData($id);//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'created_at',
							'progress_level',
							'progress_description',
							'status',
							'user',
							'file_name'
							)
				->addColumn('status', function($option){
					if($option->status == "0"){
						return "Emergency";
					}elseif($option->status == "1"){
						return "Not Started";
					}
					elseif($option->status == "2"){
						return "Started";
					}
					elseif($option->status == "3"){
						return "Finished";
					}
					elseif($option->status == "4"){
						return "Feedback";
					}
					else{
						return "";
					}
				})
				->addColumn('created_at', function($option){
					if(isMiladiDate()){
						$date = $option->created_at;
						$date = explode(" ", $date);
						$g_date = $date[0];
						$time = $date[1];
						$gregorianDate = $g_date;
						return $gregorianDate;
					}else{
						$date = $option->created_at;
						$date = explode(" ", $date);
						$j_date = $date[0];
						$time = $date[1];
						$jalali_date = dmy_format(toJalali($j_date));
						$jalali_datetime = $jalali_date;
						return $jalali_datetime;
					}
				})
				->addColumn('file_name', function($option){
					if($option->file_name != null)
					{
						return "<a href='".route('getCaseProgressDownload',array($option->id))."' style='color:black;text-align:center;text-decoration:none' class='table-link success'>
		              		".$option->file_name."&nbsp;&nbsp;<i class='fa fa-cloud-download' style='color:#03a9f4;' title='download file'></i>
		              	</a>";
					}
				})
				->make();
	}
	//get download the task progress attachmetns based on id.
	public function downloadCaseProgressFile($progress_id=0)
	{
		//get file name from database
		$fileName = caseModel::getCaseProgressFileName($progress_id);
        //public path for file
        $file= public_path(). "/documents/case_attachments/".$fileName;
        //download file
        return Response::download($file, $fileName);
	}
	//get download the task attachmetns based on id.
	public function downloadCaseFile($file_id=0, $field_name='')
	{
		//get file name from database
		$fileName = caseModel::getCaseFileName($file_id,$field_name);
        //public path for file
        $file= public_path(). "/documents/case_attachments/".$fileName;
        //download file
        return Response::download($file, $fileName);
	}
	// update the case notification.
	public function postUpdateCaseNotification()
	{
		$notification_id = Input::get('id');
		$approved = Input::get('approved');
		$viewed = Input::get('viewed');
		//insert the notification details.
		$notification_data = array( 
			'approved' 				=> $approved,
			'viewed' 				=> $viewed,
			'updated_at'			=> date('Y-m-d h:i:s')
			);
		//update the notification data based on it's id.
		$update = caseModel::updateCaseNotification($notification_id,$notification_data);
	}
	// get the notification lists.
	public function getNotificationList()
	{
		$notification_list = caseModel::getNotificationList();
		//dd($notification_list);
		$cnt=count($notification_list);
		$notification = "You have ($cnt) notifications";
		foreach($notification_list as $item)
		{
			$notification .= "\n".$item->type.",".$item->subject.",".$item->description.",".$item->status.",".$item->case_id.",".$item->approved.",".$item->viewed.",".$item->user_id.",".$item->created_at.",".$item->updated_at;
		}
		//return json_encode($rows);
		return $notification;
	}
	
	// Add Case / Task form data sent from delphi application.
	public function addCaseSentData()
	{
	    $start_date = case_ymd_format(Input::get('start_date'));
		$deadline = case_ymd_format(Input::get('end_date'));
		
		$agency_name_en = Input::get('agency');
		$agency_id = DB::connection('casemgmt')->table('agencies')->select('id')->where('name_en', $agency_name_en)->first();

		if($agency_id != null){
			$task_assigned_to=$agency_id->id;
		}else{
			//insert the agency english name if it's not exist and get it's id.
			$agency_id = DB::connection('casemgmt')->table('agencies')->insertGetId(array('name_en'=>$agency_name_en));
			$task_assigned_to = $agency_id;

		}
	    // get the form data.
	    $data = array(
	    		"task_title"				=> Input::get('title'),
	    		"start_date"				=> $start_date,
	    		"task_request_level"		=> 1,
	    		"deadline"					=> $deadline,
	    		"assigned_to_option"		=> "agency",
	    		"task_assigned_to"			=> $task_assigned_to,
	    		"requesting_person"			=> 'Mohammad Ashraf Ghani',
	    		"department"				=> 'H.E. The President',
	    		'agency_manager'			=> Input::get('agency_manager'),
	    		"task_description_en"		=> Input::get('description'),
	    		"created_at"				=> date('Y-m-d H:i:s')
	    	);
	    $object_id = caseModel::addCase($data);
	    $audio_file = Input::file('audio_file');
		$video_file = Input::file('video_file');
		$file1='';
		$file2='';
		//print_r($files);exit;
		$errors = "";
		$file_data = array();
		$upload_success=false;

		if(Input::hasFile('audio_file'))
		{

		    // path is root/uploads
		    $destinationPath = 'documents/case_attachments/';
		    $filename = $audio_file->getClientOriginalName();

		    $temp = explode(".", $filename);
		    $extension = end($temp);
		    $lastFileId = $object_id;
		    
		    $lastFileId++;
		    
		    $filename = $temp[0].'_'.$lastFileId.'.'.$extension;

		    $upload_success = $audio_file->move($destinationPath, $filename);
		    $file1=$filename;
		    if (!$upload_success) $object_id='upload error';

		}

		if(Input::hasFile('video_file'))
		{

		    // path is root/uploads
		    $destinationPath = 'documents/case_attachments/';
		    $filename = $video_file->getClientOriginalName();

		    $temp = explode(".", $filename);
		    $extension = end($temp);
		    $lastFileId = $object_id;
		    
		    $lastFileId++;
		    
		    $filename = $temp[0].'_'.$lastFileId.'.'.$extension;

		    $upload_success = $video_file->move($destinationPath, $filename);
		    $file2=$filename;
		    if (!$upload_success) $object_id='upload error';

		}		

	    if($upload_success) 
	    {
		    $file_data = array(
		   					'audio_name'			=> $file1,
		   					'video_name'			=> $file2,
		   					'case_id'				=> $object_id
		   				);
		    $object = DB::connection('casemgmt')->table('uploads')->insert($file_data);
		} 
		return $object_id;

	}
	
	
}

?>