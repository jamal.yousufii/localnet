<?php 

namespace App\Http\Controllers\casemgmt;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\models\casemgmt\caseModel;
use Illuminate\Support\Collection;
use App\library\jdatetime;
use View;
use DB;
use Validator;
use Input;
use Redirect;
use Response;
use Datatable;
use Session;
use DateTime;
use DateInterval;
use File;
use Illuminate\Support\Str;
use URL;
use Carbon\Carbon;

class caseController extends Controller
{
		
//================================================= Records Management Controller Functions =======================================
	
	//Load management form
	public function loadDeptPersonMgmtForm()
	{
		return View::make('casemgmt.requestDepsManagementForm');
	}
	// Add the person name and department.
	public function addCaseDeptPerson(Request $request)
	{
		//dd($_POST);
		if(canAdd('casemgmt_dept_management'))
		{
			if(Input::get('department') != ""){
		    	//validate the input fields
			    $this->validate($request,[
			        "department" 			=> 'required'
			        ]
			    );
			    $checkDept = checkAutocompleteExistence('department','name',Input::get('department'));
			    if(!$checkDept){
			    	// get the form data.
				    $department_data = array(
				    		"name"			=> Input::get('department'),
				    		"user_id"		=> Auth::user()->id
				    	);
				   	$object = caseModel::addCaseDept($department_data);
				    if($object){
				    	// get the the log data and insert it into the log table.
						$caseLog = array('action_table' => "department",'action_by' => Auth::user()->username,'action_type' => "Added");
						caseModel::addCaseLog($caseLog);
			    		return Redirect::route("manageRecords")->with("success","Information successfully Added.");
			        }
			        else
			        {
			            return Redirect::route("manageRecords")->with("fail","An error occured please try again or contact system developer.");
			        }		    	
			    }
			    else{
			   		return Redirect::route("manageRecords")->with("fail","The department name already exists in the system !");
			    }
			}
			else{
				//validate the input fields
			    $this->validate($request,[
			        "name" 						=> "required",
			        "related_department" 		=> 'required'
			        ]
			    );
			    //check if the duplicate name is not being inserted in table.
			    $checkPerson = checkAutocompleteExistence('person','name',Input::get('name'));
		    	if(!$checkPerson){
		    		// get the form data.
				    $person_data = array(
				    		"name"				=> Input::get('name'),
				    		"department"		=> Input::get('related_department'),
				    		"user_id"			=> Auth::user()->id
				    	);
					$object = caseModel::addCasePerson($person_data);
				    if($object){
				    	// get the the log data and insert it into the log table.
						$caseLog = array('action_table' => "person",'action_by' => Auth::user()->username,'action_type' => "Added");
						caseModel::addCaseLog($caseLog);	
				    	return Redirect::route("manageRecords")->with("success","Information successfully Added.");
			        }
			        else
			        {
			            return Redirect::route("manageRecords")->with("fail","An error occured please try again or contact system developer.");
			        }		    	
			    }
			    else{
			   		return Redirect::route("manageRecords")->with("fail","The person name already exists in the system !");
			    }
			}
		}
		else{
			showWarning();
		}
	}
	//load request and department edit form.
	public function caseEditRequestAndDep($dept_id=0, $person_id=0)
	{
		$data['department'] = caseModel::getDeptDetails($dept_id);
		$data['person'] = caseModel::getPersonDetails($person_id);
		return View::make('casemgmt.requestDepEditForm', $data);
	}
	//update the request and department form.
	public function postEditRequestAndDep(Request $request, $id)
	{
		if(canEdit('casemgmt_dept_management'))
		{
		    if(Input::get('department') != ""){
		    	//validate the input fields
			    $this->validate($request,[
			        "department" 			=> 'required'
			        ]
			    );
		    	// get the form data.
			    $department_data = array(
			    		"name"			=> Input::get('department'),
			    		"user_id"		=> Auth::user()->id
			    	);
			   	$dept_updated = caseModel::editCaseDept($case_id=0,$id,$department_data);
			   	if($dept_updated)
			   	{
		    		// get the the log data and insert it into the log table.
					$caseLog = array('action_table' => "department",'action_by' => Auth::user()->username,'action_type' => "Edited");
					caseModel::addCaseLog($caseLog);
					return Redirect::route("manageRecords")->with("success","Information successfully Edited.");
		        }
		        else
		        {
		            return Redirect::route("manageRecords")->with("fail","An error occured please try again or contact system developer.");
		        }
		    }
		    else{
	    		//validate the input fields
			    $this->validate($request,[
			        "name" 							=> "required",
			        "related_department" 			=> 'required'
			        ]
			    );
	    		// get the form data.
			    $person_data = array(
			    		"name"			=> Input::get('name'),
			    		"department"	=> Input::get('related_department'),
			    		"user_id"		=> Auth::user()->id
			    	);
				$person_updated = caseModel::editCasePerson($case_id=0,$id,$person_data);
			    if($person_updated){
			    	// get the the log data and insert it into the log table.
					$caseLog = array('action_table' => "person",'action_by' => Auth::user()->username,'action_type' => "Edited");
					caseModel::addCaseLog($caseLog);	
			    	return Redirect::route("manageRecords")->with("success","Information successfully Edited.");
		        }
		        else
		        {
		            return Redirect::route("manageRecords")->with("fail","An error occured please try again or contact system developer.");
		        }		    	
		    }
		    
		}	
		else{
			showWarning();
		}
	}
	public function getDepartments()
	{
		$object = caseModel::getDeps();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
			->showColumns(
				'id',
				'name'
			)
			->addColumn('operations', function($option){
				return '<a href="'.route('caseEditRequestAndDep',$option->id).'" title="Edit Record"><i class="fa fa-edit"></i></a> | 
						<a href="'.route('getDeleteRequestDep',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Record"><i class="fa fa-trash-o"></i></a>';
			})
			->make();
	}
	public function getPersons()
	{
		$object = caseModel::getPersons();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
			->showColumns(
				'id',
				'name',
				'department'
			)
			->addColumn('operations', function($option){
				return '<a href="'.route('caseEditRequestAndDep',array(0, $option->id)).'" title="Edit Record"><i class="fa fa-edit"></i></a> | 
						<a href="'.route('getDeleteRequestDep',array(0, $option->id)).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Record"><i class="fa fa-trash-o"></i></a>';
			})
			->make();
	}
	// get delete the person and department data based on id.
	public function deletePersonAndDept($dept_id=0, $person_id=0)
	{
		if($dept_id != 0){
			$deleted = DB::connection('casemgmt')->table('department')->where('id', $dept_id)->delete();
			if($deleted)
			{
				// get the the log data and insert it into the log table.
				$caseLog = array('action_table' => "department",'action_by' => Auth::user()->username,'action_type' => "Deleted");
				caseModel::addCaseLog($caseLog);	
		    	return Redirect::route("manageRecords")->with("success","Information successfully Deleted.");
	        }
	        else
	        {
	            return Redirect::route("manageRecords")->with("fail","An error occured please try again or contact system developer.");
	        }
		}
		if($person_id != 0){
			$deleted = DB::connection('casemgmt')->table('person')->where('id', $person_id)->delete();
			if($deleted)
			{
				// get the the log data and insert it into the log table.
				$caseLog = array('action_table' => "person",'action_by' => Auth::user()->username,'action_type' => "Deleted");
				caseModel::addCaseLog($caseLog);	
		    	return Redirect::route("manageRecords")->with("success","Information successfully Deleted.");
	        }
	        else
	        {
	            return Redirect::route("manageRecords")->with("fail","An error occured please try again or contact system developer.");
	        }
		}
	}
	public function loadAgencyManagementForm()
	{
		return View::make('casemgmt.agencyManagementForm');
	}
	// Add Agency form data.
	public function addAgencyData(Request $request)
	{
		if(canAdd('casemgmt_management'))
		{
			//validate the input fields
		    $this->validate($request,[
		        "name_en" 			=> "required",
		        "name_dr" 			=> 'required'
		        ]
		    );
		    // get the form data.
		    $data = array(
		    		"name_en"			=> Input::get('name_en'),
		    		"name_dr"			=> Input::get('name_dr'),
		    		"head_en"			=> Input::get('head_en'),
		    		"head_dr"			=> Input::get('head_dr'),
		    		"contact1"			=> Input::get('contact1'),
		    		"contact2"			=> Input::get('contact2'),
		    		"contact3"			=> Input::get('contact3')
		    	);
		    $object = caseModel::addAgencyData($data);
		    if($object){

		    	// get the the log data and insert it into the log table.
				$caseLog = array('action_table' => "agencies",'record_id' => $id,'action_by' => Auth::user()->username,'action_type' => "Added");
				caseModel::addCaseLog($caseLog);	
		    	return Redirect::route("manageAgencies")->with("success","Information successfully Added.");
	        }
	        else
	        {
	            return Redirect::route("manageAgencies")->with("fail","An error occured please try again or contact system developer.");
	        }
		    
		}
	}
	public function getAgencyListData()
	{
		$object = caseModel::getAgencyData();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
			->showColumns(
				'id',
				'name_en',
				'name_dr',
				'head_en',
				'head_dr',
				'contact1',
				'contact2',
				'contact3'
			)
			->addColumn('operations', function($option){
				return '<a href="'.route('caseEditAgency',$option->id).'" title="Edit Record"><i class="fa fa-edit"></i></a> | 
						<a href="'.route('getDeleteAgency',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Record"><i class="fa fa-trash-o"></i></a>';
			})
			->make();
	}
	//load the agency edit form.
	public function caseEditAgencyForm($id=0)
	{
		$data['records'] = caseModel::getCaseAgencyDetails($id);
		return View::make('casemgmt.agencyEditForm', $data);
	}
	public function postEditCaseAgency(Request $request, $id)
	{
		$this->validate($request,[
	        "name_en" 				=> "required",
	        "name_dr" 				=> 'required'
	        ]
	    );
	    // get the form data.
	    $data = array(
	    			"name_en"			=> Input::get('name_en'),
		    		"name_dr"			=> Input::get('name_dr'),
		    		"head_en"			=> Input::get('head_en'),
		    		"head_dr"			=> Input::get('head_dr'),
		    		"contact1"			=> Input::get('contact1'),
		    		"contact2"			=> Input::get('contact2'),
		    		"contact3"			=> Input::get('contact3')
	    	);
	    $object = caseModel::updateAgencyData($id, $data);
	    if($object){
	    	// get the the log data and insert it into the log table.
			$caseLog = array('action_table' => "agencies",'record_id' => $id,'action_by' => Auth::user()->username,'action_type' => "Edited");
			caseModel::addCaseLog($caseLog);	
	    	return Redirect::route("manageAgencies")->with("success","Information successfully Edited.");
        }
        else
        {
            return Redirect::route("manageAgencies")->with("fail","Either you didn't change the form or an error occured, please try again or contact system developer.");
        }
	}
	// get delete the agency data based on id.
	public function deleteAgency($id=0)
	{
		$deleted = DB::connection('casemgmt')->table('agencies')->where('id', $id)->delete();
		if($deleted)
		{
			// get the the log data and insert it into the log table.
			$caseLog = array('action_table' => "agencies",'record_id' => $id,'action_by' => Auth::user()->username,'action_type' => "Deleted");
			caseModel::addCaseLog($caseLog);	
	    	return Redirect::route("manageAgencies")->with("success","Information successfully Deleted.");
        }
        else
        {
            return Redirect::route("manageAgencies")->with("fail","An error occured please try again or contact system developer.");
        }
	}
	
//================================================= Autocomplete Functions =======================================================
	//get the data list based on table name and field name for autocomplete fields.
	public function getAutoCompleteListData($table="", $field="")
	{
        $q = strtolower(Input::get('term'));
		
		if (!$q) return;
		$options = DB::connection("casemgmt")->table($table)->select($field)->get();
		if (!$options) return;
		$json_array = array();
		foreach ($options as $key) 
		{
			if (strpos(strtolower($key->$field), $q) !== false) 
			{
				//echo "$key->company_name\n";
				array_push($json_array, $key->$field);
			}
		}
		echo json_encode($json_array);
	}
	//get the related department based on person name.
	public function getRelatedCaseDepartment()
	{
		$user_id = Input::get('user_id');
		$dept_id = caseModel::getRelatedDept($user_id);
		$departments = getCaseDepartments($dept_id);
		//print_r($departments);exit;
		return $departments;
	}
//================================================= Main Page Controller Functions =================================================

	//load the case management main page.
	public function loadMainPage()
	{
		//get the notifications for the manager.
		$data['notification'] = caseModel::getNotification();
		return View::make('casemgmt.main_page', $data);
	}
	// get the case management log list.
	public function getLogsList()
	{
		//get all data 
		$object = caseModel::getLogList();
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'action_by',
							'action_type',
							'action_datetime'
							)
				->addColumn('action_datetime', function($option){
					if(isMiladiDate()){
						$date = $option->action_datetime;
						$date = explode(" ", $date);
						$g_date = $date[0];
						$time = $date[1];
						$gregorianDate = $g_date;
						return $gregorianDate;
					}else{
						$date = $option->action_datetime;
						$date = explode(" ", $date);
						$j_date = $date[0];
						$time = $date[1];
						$jalali_date = dmy_format(toJalali($j_date));
						$jalali_datetime = $jalali_date;
						return $jalali_datetime;
					}
				})
				->addColumn('operation', function($option){
					if($option->action_table == "cases" || $option->action_table == "task_progress")
					{
						if($option->action_type == "Deleted"){return "";}
						return '<a href="'.route('viewCase',$option->record_id).'" title="View Details"><i class="fa fa-2x fa-eye"></i></a>';	
					}
					elseif($option->action_table == "notification")
					{
						if($option->action_type == "Deleted"){return "";}
						return '<a href="'.route('notificationDetails',array($option->action_table,$option->record_id)).'" title="View Details"><i class="fa fa-2x fa-eye"></i></a>';
					}
					elseif($option->action_table == "notify_to_manager")
					{
						if($option->action_type == "Deleted"){return "";}
						return '<a href="'.route('viewNotificationDetails',array($option->action_table,$option->record_id)).'" title="View Details"><i class="fa fa-2x fa-eye"></i></a>';
					}
					elseif($option->action_table == "agencies")
					{
						if($option->action_type == "Deleted"){return "";}
						return '<a href="'.route('logAgencyDetails',$option->record_id).'" title="View Details"><i class="fa fa-2x fa-eye"></i></a>';
					}
					else
					{
						return "";
					}
				})
				->make();
	}
	//get the details of the notification based on record id which is sent from logs table.
	public function viewNotificationDetails($table_name, $record_id)
	{
		$data['notification'] = caseModel::getSpecificNotificationLogDetails($table_name,$record_id);
		//dd($data['notification']);
		return view('casemgmt.notificationLogDetails', $data);
	}
	//get the details of the agency based on record id which is sent from logs table.
	public function viewLogAgencyDetails($record_id)
	{
		$data['agency'] = caseModel::getSpecificAgencyLogDetails($record_id);
		//dd($data['notification']);
		return view('casemgmt.agencyLogDetails', $data);
	}
	// approve the finished task which is done by the manager
	public function getApproveFinishedTask()
	{
		$case_id = Input::get('case_id');
		$approvedFinishedTask = caseModel::getApprovedFinishedTask($case_id);
		if($approvedFinishedTask)
		{
			echo "<div class='alert alert-success'>The finished task successfully approved</div>";
		}
		else
		{
			echo "<div class='alert alert-danger'>Sorry, something was wrong !</div>";
		}
	}

//================================================= Task / Case Management Controller Functions ===================================
	
	//Load task/case form
	public function loadCaseList()
	{
		// if(canEdit('casemgmt_task_list'))
		// {
		// 	return Redirect::route('caseForm');
		// }else{
		// 	return View::make('casemgmt.caseList');
		// }
		return View::make('casemgmt.caseList');
	}
	//get form data list
	public function getAllCases()
	{
		//get all data 
		$object = caseModel::getData();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'task_title',
							'priority',
							'requesting_person',
							'department',
							'task_assigned_to',
							'task_request_level',
							'deadline',
							'task_category',
							'status'
							//'task_assigned_to'
							)
				->addColumn('task_assigned_to', function($option){
					if($option->assigned_to_option == "agency"){
						$lang = getLangShortcut();
						//get the name of agency based on id.
						$agency = DB::connection('casemgmt')->table('agencies');
						if($lang == "dr"){
							$agency->select('name_dr')->where('id', $option->task_assigned_to);
							$task_agency = $agency->first();
							if($task_agency != null){return $task_agency->name_dr;}else{return "";}
						}else{
							$agency->select('name_en')->where('id', $option->task_assigned_to);
							$task_agency = $agency->first();
							//dd($task_agency->name_en);
							if($task_agency != null){return $task_agency->name_en;}else{return "";}
						}
					}
					return "";
				})
				->addColumn('priority', function($option){
					if($option->priority == 2){
						return "High";
					}else{
						return "Low";
					}
				})
				->addColumn('status', function($option){
					if($option->status == "0"){
						return "Emergency";
					}elseif($option->status == "1"){
						return "Not Started";
					}
					elseif($option->status == "2"){
						return "Started";
					}
					elseif($option->status == "3"){
						return "Finished";
					}
					elseif($option->status == "4"){
						return "Feedback";
					}
					else{
						return "";
					}
				})
				->addColumn('deadline', function($option){
					if(isMiladiDate()){
						return $option->deadline;
					}else{
						$deadline = dmy_format(toJalali($option->deadline));
						return $deadline;
					}
				})
				->addColumn('operations', function($option){
					$options = '';
					if(canEdit('casemgmt_task_list'))
						$options .= '<a href="'.route('caseEditForm',$option->id).'" title="Edit Task"><i class="fa fa-edit"></i></a> |';
					if(canView('casemgmt_task_list')){	
						$options .= ' <a href="'.route('viewCase',$option->id).'" title="View Task Details"><i class="fa fa-eye"></i></a> | ';
					}
						$options .= '<a href="'.route('getDeleteCase',$option->id).'" onclick="javascript:return confirm(\'Are you sure ?\')" title="Delete Record"><i class="fa fa-trash-o"></i></a>';
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		
	}
	
	//Load task/case form
	public function loadCaseForm()
	{
		if(canAdd('casemgmt_form')){
			return View::make('casemgmt.case_form');
		}
		else{
			showWarning();
		}
	}
	// Add Case / Task form data.
	public function addCaseFormData(Request $request)
	{
		//dd($_POST);
		if(canAdd('casemgmt_form'))
		{
			//validate the input fields
		    $this->validate($request,[
		        "task_title" 					=> "required"
		        ]
		    );
		    
		    //check the date type if it's shamsi or miladi.
			if(isMiladiDate()){
				$start_date = Input::get('start_date');
				$deadline = Input::get('deadline');
			}else{
				$start_date = toGregorian(ymd_format(Input::get('start_date')));
				$deadline = toGregorian(ymd_format(Input::get('deadline')));
			}
			if(Input::get("task_recurring") == "on" || Input::get("recurring_type") != ""){
				$task_recurring = 1;
				$recurring_type = Input::get('recurring_type');
			}
			else{
				$task_recurring = 0;
				$recurring_type = "";
			}
			if(Input::get('assigned_to_option') == "in_house"){
				$task_assigned_to = Input::get("assigned_to_category");
			}
			elseif(Input::get('assigned_to_option') == "agency"){
				$task_assigned_to = Input::get("assigned_to_agency");
			}
			else{
				$task_assigned_to = "";
			}
			//check if the category already exists don't insert in category table or if not then insert it by calling the helper function.
			getCheckCategory(Input::get('task_category'));
			$priority = Input::get('priority');
			
		    // get the form data.
		    $data = array(
		    		"task_title"				=> Input::get('task_title'),
		    		"priority"					=> $priority,
		    		"requesting_person"			=> Input::get('requesting_person'),
		    		"department"				=> Input::get('department'),
		    		"start_date"				=> $start_date,
		    		"task_request_level"		=> Input::get('task_request_level'),
		    		"deadline"					=> $deadline,
		    		"deadline_option"			=> Input::get('deadline_option'),
		    		"task_requrring"			=> $task_recurring,
		    		"recurring_type"			=> $recurring_type,
		    		"alarm"						=> Input::get('alarm'),
		    		"task_category"				=> Input::get('task_category'),
		    		"assigned_to_option"		=> Input::get('assigned_to_option'),
		    		"task_assigned_to"			=> $task_assigned_to,
		    		"task_description_en"		=> Input::get('task_description_en'),
		   			"task_description_dr_ps"	=> Input::get('task_description_dr_ps'),
		    		"user_id"					=> Auth::user()->id,
		    		"created_at"				=> date('Y-m-d H:i:s')
		    	);
		    $object_id = caseModel::addCase($data);
		    if($object_id){
		    	
		    	//if the priority is high, then insert a notification for manager.
				if($priority == 2)
				{
					$notify_data = array(
								'case_id'		=> $object_id,
								'table_name'	=> "cases",
								"created_at"	=> date('Y-m-d H:i:s')
						);
					caseModel::addNotifyToManagerData($notify_data);
				}
		    	
		    	if(Input::get('assigned_to_option') == "external"){
		    		if(Input::get('person_name') != '' && Input::get('related_department') != '')
				    {
				    	//check if the duplicate name is not being inserted in table.
					    $checkPerson = checkAutocompleteExistence('person','name',Input::get('person_name'));
				    	if(!$checkPerson){
				    		// get the form data.
						    $person_data = array(
						    		"name"			=> Input::get('name'),
						    		"department"	=> Input::get('related_department'),
						    		"case_id"		=> $object_id,
						    		"user_id"		=> Auth::user()->id
						    	);
							$object = caseModel::addCasePerson($person_data);
						    if($object){
						    	// get the the log data and insert it into the log table.
								// $caseLog = array('action_table' => "person",'action_by' => Auth::user()->username,'action_type' => "Added");
								// caseModel::addCaseLog($caseLog);	
						    }
				    	}
				    	else{
					    	// if the person name does exist, then update and set the case_id.
					    	DB::connection('casemgmt')->table('person')->where('name', Input::get('person_name'))
					    				->update(array('case_id' => $object_id));
					    }
				    }
				}
		    	// get the the log data and insert it into the log table.
		    	$caseLog = array('action_table' => "cases",'record_id'=>$object_id,'action_by' => Auth::user()->username,'action_type' => "Added");
				caseModel::addCaseLog($caseLog);
		    	return Redirect::route("caseList")->with("success","Information successfully Added.");
	        }
	        else
	        {
	            return Redirect::route("caseList")->with("fail","An error occured please try again or contact system developer.");
	        }
		    
		}
		else{
			showWarning();
		}
	}
	//loading the task edit page.
	public function loadCaseEditForm($id=0)
	{
		if(canEdit('casemgmt_task_list'))
		{
			$data['task'] = caseModel::getSpecifiCaseDetails($id);
			//dd($data['task']);
			$data['name'] = "";
			$data['department'] = "";
			$assigned_to_option = DB::connection("casemgmt")->table("cases")->select("assigned_to_option")->where('id', $id)->first();
			if($assigned_to_option->assigned_to_option == "external"){
				$dept = DB::connection("casemgmt")->table("department")->select('id','name')->where('case_id', $id)->first();
				$person = DB::connection("casemgmt")->table("person")->select('name')->where('case_id', $id)->first(); 
				if($dept != null){
					$data['department'] = $dept->id;
				}
				if($person != null){
					$data['name'] = $person->name;
				}
			}
			return View::make('casemgmt.caseEdit', $data);	
		}
		else{
	    	return showWarning();
	    }
	}
	// updating the changes in case table.
	public function postEditCaseForm(Request $request,$id)
	{
		//dd($_POST);
		if(canEdit('casemgmt_task_list'))
		{
			//validate the input fields
		    $this->validate($request,[
		        "task_title" 					=> "required",
		        "task_description_en"			=> "required"
		        ]
		    );
		    //check the date type if it's shamsi or miladi.
			if(isMiladiDate()){
				$start_date = Input::get('start_date');
				$deadline = Input::get('deadline');
			}else{
				$start_date = toGregorian(ymd_format(Input::get('start_date')));
				$deadline = toGregorian(ymd_format(Input::get('deadline')));
			}
			if(Input::get("task_recurring") == "on" || Input::get("recurring_type") != ""){
				$task_recurring = 1;
				$recurring_type = Input::get('recurring_type');
			}
			else{
				$task_recurring = 0;
				$recurring_type = "";
			}
			if(Input::get('assigned_to_option') == "in_house"){
				$task_assigned_to = Input::get("assigned_to_category");
			}
			elseif(Input::get('assigned_to_option') == "agency"){
				$task_assigned_to = Input::get("assigned_to_agency");
			}
			else{
				$task_assigned_to = "";
			}
		    // get the form data.
		    $data = array(
		    		"task_title"				=> Input::get('task_title'),
		    		"priority"					=> Input::get('priority'),
		    		"requesting_person"			=> Input::get('requesting_person'),
		    		"department"				=> Input::get('department'),
		    		"start_date"				=> $start_date,
		    		"task_request_level"		=> Input::get('task_request_level'),
		    		"deadline"					=> $deadline,
		    		"deadline_option"			=> Input::get('deadline_option'),
		    		"task_requrring"			=> $task_recurring,
		    		"recurring_type"			=> $recurring_type,
		    		"alarm"						=> Input::get('alarm'),
		    		"task_category"				=> Input::get('task_category'),
		    		"assigned_to_option"		=> Input::get('assigned_to_option'),
		    		"task_assigned_to"			=> $task_assigned_to,
		    		"task_description_en"		=> Input::get('task_description_en'),
		   			"task_description_dr_ps"	=> Input::get('task_description_dr_ps'),
		    		"user_id"					=> Auth::user()->id
		    	);
		    $case_updated = caseModel::editCase($id, $data);
	    	// get the the log data and insert it into the log table.
			$caseLog = array('action_table' => "cases",'record_id'=>$id,'action_by' => Auth::user()->username,'action_type' => "Edited");
			caseModel::addCaseLog($caseLog);
	    	if(Input::get('assigned_to_option') == "external")
	    	{
	    		if(Input::get('person_name') != '' && Input::get('related_department') != '')
			    {
		    		// get the form data.
				    $person_data = array(
				    		"name"			=> Input::get('person_name'),
				    		"department"	=> Input::get('related_department'),
				    		"case_id"		=> $id,
				    		"user_id"		=> Auth::user()->id
				    	);
					$person_updated = caseModel::editCasePerson($id,$person_id=0,$person_data);
				    if($person_updated){
				    	// get the the log data and insert it into the log table.
						// $caseLog = array('action_table' => "person",'action_by' => Auth::user()->username,'action_type' => "Edited");
						// caseModel::addCaseLog($caseLog);	
				    }
			    }
			}
		    if($case_updated OR $person_updated)
		    {
		    	return Redirect::route("caseList")->with("success","Information successfully Edited.");
	        }
	        else
	        {
	            return Redirect::route("caseList")->with("fail","Either you didn't change the form or something is wrong, please try again or contact system developer.");
	        }
		    
		}
		else{
	    	return showWarning();
	    }
		
	}
	//change the status of the case based on it's id to 1 if it's deleted.
	public function deleteCase($id=0)
	{
		$deleted = caseModel::getDeleteCase($id);
		//dd($deleted);
		if($deleted)
	    {
	    	// get the the log data and insert it into the log table.
			$caseLog = array('action_table' => "cases",'record_id'=>$id,'action_by' => Auth::user()->username,'action_type' => "Deleted");
			caseModel::addCaseLog($caseLog);
	    	return Redirect::route("caseList")->with("success","Information successfully Deleted.");
        }
        else
        {
            return Redirect::route("caseList")->with("fail","Something is wrong, please try again or contact system developer.");
        }
	}
	// viewing the task details based on id.
	public function viewCaseDetails($id=0, $notify="")
	{
		//change the is_viewed of the notify to manager table, means the notification has visited.
		if($notify == "visited")
		{
			caseModel::notificationVisited($id);
		}
		if(canView('casemgmt_task_list')){
			$data['task_details'] = caseModel::getCaseDetails($id);
			$data['task_progress'] = caseModel::getCaseProgress($id);
			//get the approved field of the notify to manager table to insure that the notification has been approved.
			$data['approved_finished_task'] = caseModel::getApprovedFinishedCases($id);
			//dd($data['approved_finished_task']);
			$data['task_attachments'] = caseModel::getCaseAttachments($id);
			$data['task_description'] = caseModel::getTaskDescription($id);
			$data['task_assigned_to'] = "";
			$data['name'] = "";
			$data['department'] = "";
			//get lang shortcut
			$lang = getLangShortcut();
			$assigned_to_option = DB::connection("casemgmt")->table("cases")->select("assigned_to_option","task_assigned_to")->where('id', $id)->first();
			if($assigned_to_option->assigned_to_option == "in_house"){
				$object = DB::connection("casemgmt")->table("category")->select('name')->where('id', $assigned_to_option->task_assigned_to)->first(); 
				if($object != null){
					$data['task_assigned_to'] = $object->name;
				}
			}
			if($assigned_to_option->assigned_to_option == "agency"){
				$object = DB::connection("casemgmt")->table("agencies")->select('name_'.$lang)->where('id', $assigned_to_option->task_assigned_to)->first(); 
				if($object != null){
					if($lang == "dr")
					$data['task_assigned_to'] = $object->name_dr;
					else
					$data['task_assigned_to'] = $object->name_en;
				}
			}
			if($assigned_to_option->assigned_to_option == "external"){
				$person = DB::connection("casemgmt")->table("person")->select('name')->where('case_id', $id)->first(); 
				$department = DB::connection("casemgmt")->table("department")->select('name')->where('case_id', $id)->first(); 
				if($person != null){$data['name'] = $person->name;}
				if($department != null){$data['department'] = $department->name;}
			}
			
			return View::make("casemgmt.viewTaskDetails", $data);
		}
		else{
	    	return showWarning();
	    }
	}
	//get download the task attachmetns based on id.
	public function downloadCaseFile($file_id=0, $field_name='')
	{
		//get file name from database
		$fileName = caseModel::getCaseFileName($file_id,$field_name);
        //public path for file
        $file= public_path(). "/documents/case_attachments/".$fileName;
        //download file
        return Response::download($file, $fileName);
	}

// ========================================================= Task Execution Order Controller Functions ================================================

	//load the execution order form.
	public function loadExecOrderForm()
	{
		return View::make('casemgmt.execOrderForm');
	}
	// get load the execution order list
	public function loadCaseExecOrderList()
	{
		return View::make('casemgmt.execOrderList');	
	}
	//get the execution order data list.
	public function getExecOrderData()
	{
		$object = caseModel::getExecOrderData();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
			->showColumns(
				'id',
				'agency',
				'description',
				'priority',
				'deadline'
			)
			->addColumn('deadline', function($option){
				if(isMiladiDate()){
					if($option->deadline == "0000-00-00" || $option->deadline == ""){
						return "";
					}else{
						return $option->deadline;
					}
				}else{
					$deadline = dmy_format(toJalali($option->deadline));
					return $deadline;
				}
			})
			->addColumn('operations', function($option){
				return '<a href="'.route('editExecOrder',$option->id).'" title="Edit Record"><i class="fa fa-edit"></i></a> | 
						<a href="'.route('deleteExecOrder',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Record"><i class="fa fa-trash-o"></i></a>';
			})
			->make();
	}
	// add the execution order form data.
	public function postAddExec()
	{
		//echo "under construction";exit;
		//dd($_POST);
		if(Input::get('deadline') != "")
		{
			$deadline_date = Input::get('deadline');
			if($deadline_date == "24 hrs"){
				$curr_date = strtotime(date("Y-m-d"));
				$deadline = date("Y-m-d", strtotime("+1 day", $curr_date));
			}
			elseif($deadline_date == "3 days"){
				$curr_date = strtotime(date("Y-m-d"));
				$deadline = date("Y-m-d", strtotime("+3 day", $curr_date));
			}
			elseif($deadline_date == "1 week"){
				$curr_date = strtotime(date("Y-m-d"));
				$deadline = date("Y-m-d", strtotime("+7 day", $curr_date));
			}
			elseif($deadline_date == "1 month"){
				$curr_date = strtotime(date("Y-m-d"));
				$deadline = date("Y-m-d", strtotime("+1 month", $curr_date));
			}
			else{
				//check the date type if it's shamsi or miladi.
				if(isMiladiDate()){
					$deadline = Input::get('deadline');
				}else{
					$deadline = toGregorian(ymd_format(Input::get('deadline')));
				}
			}
			
		}
		//get the data
		$exec_data = array(
					"description"			=> Input::get('description'),
					"agency"				=> Input::get('agency'),
					"priority"				=> Input::get('priority'),
					"deadline"				=> $deadline
			);
		$object = caseModel::addExecOrderData($exec_data);
		if($object){
			return Redirect::route("execOrderList")->with("success","Information successfully saved.");
        }
        else
        {
            return Redirect::route("execOrderList")->with("fail","Something is wrong, please try again or contact system developer.");
        }

	}
	//get load the executive edit form
	public function loadExecOrderEditForm($id=0)
	{
		//get the executive order detailed info based on id.
		$data['execOrderData'] = caseModel::getExecData($id);
		return View::make('casemgmt.execOrderEditForm', $data);
	}
	// update the executive order data.
	public function postEditExec($id)
	{
		if(Input::get('deadline') != "")
		{
			$deadline_date = Input::get('deadline');
			if($deadline_date == "24 hrs"){
				$curr_date = strtotime(date("Y-m-d"));
				$deadline = date("Y-m-d", strtotime("+1 day", $curr_date));
			}
			elseif($deadline_date == "3 days"){
				$curr_date = strtotime(date("Y-m-d"));
				$deadline = date("Y-m-d", strtotime("+3 day", $curr_date));
			}
			elseif($deadline_date == "1 week"){
				$curr_date = strtotime(date("Y-m-d"));
				$deadline = date("Y-m-d", strtotime("+7 day", $curr_date));
			}
			elseif($deadline_date == "1 month"){
				$curr_date = strtotime(date("Y-m-d"));
				$deadline = date("Y-m-d", strtotime("+1 month", $curr_date));
			}
			else{
				//check the date type if it's shamsi or miladi.
				if(isMiladiDate()){
					$deadline = Input::get('deadline');
				}else{
					$deadline = toGregorian(ymd_format(Input::get('deadline')));
				}
			}
			
		}
		//get the data
		$exec_data = array(
					"description"			=> Input::get('description'),
					"agency"				=> Input::get('agency'),
					"priority"				=> Input::get('priority'),
					"deadline"				=> $deadline
			);
		$object = caseModel::updateExecOrderData($id, $exec_data);
		if($object){
			return Redirect::route("execOrderList")->with("success","Information successfully updated.");
        }
        else
        {
            return Redirect::route("execOrderList")->with("fail","Something is wrong, please try again or contact system developer.");
        }
	}
	//get delete the executive edit form
	public function deleteExecOrder()
	{
		echo "Under Contstruction";
	}
	//get the list of all progresses.
	public function getCaseProgressData($id=0)
	{
		//get all data 
		$object = caseModel::getCaseProgressData($id);
		dd($object);
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'created_at',
							'progress_level',
							'progress_description',
							'status',
							'user',
							'file_name'
							)
				->addColumn('created_at', function($option){
					if(isMiladiDate()){
						$date = $option->created_at;
						$date = explode(" ", $date);
						$g_date = $date[0];
						$time = $date[1];
						$gregorianDate = $g_date;
						return $gregorianDate;
					}else{
						$date = $option->created_at;
						$date = explode(" ", $date);
						$j_date = $date[0];
						$time = $date[1];
						$jalali_date = dmy_format(toJalali($j_date));
						$jalali_datetime = $jalali_date;
						return $jalali_datetime;
					}
				})
				->addColumn('status', function($option){
					if($option->status == "0"){
						return "Emergency";
					}elseif($option->status == "1"){
						return "Not Started";
					}
					elseif($option->status == "2"){
						return "Started";
					}
					elseif($option->status == "3"){
						return "Finished";
					}
					elseif($option->status == "4"){
						return "Feedback";
					}
					else{
						return "";
					}
				})
				->addColumn('file_name', function($option){
					if($option->file_name != '')
					{
						return "<a href='".route('getCaseDownload',array($option->id))."' style='color:black;text-align:center;text-decoration:none' class='table-link success'>
		              		".$option->file_name."&nbsp;&nbsp;<i class='fa fa-cloud-download' style='color:#03a9f4;' title='download file'></i>
		              	</a>";
					}
				})
				->make();
	}
	// inserting or updating task progress, case_id sent as parameter.
	public function postUpdateTaskProgress($id=0)
	{
		//dd($_POST);
		//get the data.
		$progress_level = Input::get('progress_level');
		$status = Input::get('status');
		$progress_description = Input::get('progress_description');
		//check if the task status has been changed then add the notification data.
		$check = caseModel::checkTaskStatus($id);
		//dd($check);
		$data = array( 
			'progress_level' 		=> $progress_level,
			'status' 				=> $status,
			'progress_description' 	=> $progress_description, 
			'case_id' 				=> $id,
			'user_id'				=> Auth::user()->id,
			'created_at'			=> date('Y-m-d h:i:s')
			);
		//add the progress data and get the inserted record id.
		$ProgressInserted = caseModel::addProgress($data);
		if(is_object($check)){
			//if the status is 3 or finished, then insert a notification for manager.
			if(($check->status != $status) && $status == '3' || $status == '0')
			{
				$notify_data = array(
							'case_id'			=> $id,
							'table_name'		=> "task_progress",
							"created_at"		=> date('Y-m-d H:i:s')
					);
				caseModel::addNotifyToManagerData($notify_data);
			}
			if(($check->status != $status) && $ProgressInserted && $status == '2')
			{
				//delete duplicate notification based on case_id.
				caseModel::deleteDuplicateNotification($id);
				//insert the notification details.
				$notification_data = array( 
				'type' 					=> "case",
				'subject' 				=> Input::get('task_title'),
				'status'				=> $status,
				'case_id' 				=> $id,
				'approved'				=> 0,
				'viewed'				=> 0,
				'user_id'				=> Auth::user()->id,
				'created_at'			=> date('Y-m-d h:i:s')
				);
				//add the notification data into it's table.
				$added = caseModel::addCaseNotification($notification_data);
				// get the the log data and insert it into the log table.
				$notificationLog = array('action_table' => "notification",'action_by' => Auth::user()->username,'action_type' => "Notification Added");
				caseModel::addCaseLog($notificationLog);
			}	
		}
		// getting all of the post data
		$files = Input::file('files');
		//print_r($files);exit;
		$errors = "";
		$file_data = array();

		if(Input::hasFile('files'))
		{
			
			foreach($files as $file) 
			{
			
			    // validating each file.
			    $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			    $validator = Validator::make(
			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension())
			        ],
			        [
			            'file' => 'required|max:10000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png,pdf,doc,docx'
			        ]
			  	);
			  	if($validator->passes())
			  	{
				    // path is root/uploads
				    $destinationPath = 'documents/case_attachments';
				    $filename = $file->getClientOriginalName();

				    $temp = explode(".", $filename);
				    $extension = end($temp);
				    $lastFileId = $id;
				    
				    $lastFileId++;
				    
				    $filename = $temp[0].'_'.$lastFileId.'.'.$extension;

				    $upload_success = $file->move($destinationPath, $filename);

				    if($upload_success) 
				    {
					    $data = array(
					   					'file_name'			=> $filename,
					   					'progress_id'		=> $ProgressInserted,
					   					'case_id'			=> $id,
					   					'created_at'		=> date('Y-m-d h:i:s')
					   				);
					    $object = DB::connection('casemgmt')->table('uploads')->insert($data);
					    if($object)
						{
							// get the the log data and insert it into the log table.
							$caseLog = array('action_table' => "task_progress",'record_id'=>$id,'action_by' => Auth::user()->username,'action_type' => "Progress Revision");
							caseModel::addCaseLog($caseLog);
							return Redirect::route("viewCase",$id)->with("success","Progress successfully saved.");
				        }
				        else
				        {
				            return Redirect::route("viewCase",$id)->with("fail","Something is wrong, please try again or contact system developer.");
				        }
					} 
					else 
					{
					    // redirect back with errors.
						return Redirect::back()->withErrors($validator);
					}
				}
				else
				{
					// redirect back with errors.
					return Redirect::back()->withErrors($validator);
				}

			}
			
		}
		if($ProgressInserted)
		{
			// get the the log data and insert it into the log table.
			$caseLog = array('action_table' => "task_progress",'record_id'=>$id,'action_by' => Auth::user()->username,'action_type' => "Progress Revision");
			caseModel::addCaseLog($caseLog);
			return Redirect::route("viewCase",$id)->with("success","Progress successfully saved.");
        }
        else
        {
            return Redirect::route("viewCase",$id)->with("fail","Something is wrong, please try again or contact system developer.");
        }
	}
	//load the case notification page.
	public function loadCaseNotification()
	{
		return view('casemgmt.notification_form');
	}
	//Add the notification data into the table.
	public function postAddCaseNotification()
	{
		if(Input::get('urgency') == 1){$type = "Info";}else{$type = "Urgent";}
		//insert the notification details.
		$notification_data = array( 
			'type' 					=> $type,
			'subject' 				=> Input::get('subject'),
			'description' 			=> Input::get('description'),
			'approved'				=> 0,
			'viewed'				=> 0,
			'user_id'				=> Auth::user()->id,
			'created_at'			=> date('Y-m-d h:i:s')
			);
		//add the notification data into it's table.
		$inserted_record_id = caseModel::addCaseNotification($notification_data);
		if($inserted_record_id)
		{
			// get the the log data and insert it into the log table.
			$notificationLog = array('action_table' => "notification",'record_id'=>$inserted_record_id,'action_by' => Auth::user()->username,'action_type' => "Notification Added");
			caseModel::addCaseLog($notificationLog);
			return Redirect::route("caseNotification")->with("success","Notification successfully added !");
		}
		else
		{
			return Redirect::route("caseNotification")->with("fail","An error occured please try again or contact system developer.");
		}
		
	}
	//load the notification list page.
	public function loadCaseNotificationList()
	{
		return view('casemgmt.notificationList');
	}
	// get the notification lists.
	public function notificationListData()
	{
		
		//get all data 
		$object = caseModel::getNotificationList();
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'type',
							'subject',
							'description',
							'status',
							'approval',
							'viewed',
							'created_at'
							)
				->addColumn('status', function($option){
					if($option->status == "0"){
						return "Emergency";
					}elseif($option->status == "1"){
						return "Not Started";
					}
					elseif($option->status == "2"){
						return "Started";
					}
					elseif($option->status == "3"){
						return "Finished";
					}
					elseif($option->status == "4"){
						return "Feedback";
					}
					else{
						return "";
					}
				})
				->addColumn('created_at', function($option){
					if(isMiladiDate()){
						$date = $option->created_at;
						$date = explode(" ", $date);
						$g_date = $date[0];
						$time = $date[1];
						$gregorianDate = $g_date;
						return $gregorianDate;
					}else{
						$date = $option->created_at;
						$date = explode(" ", $date);
						$j_date = $date[0];
						$time = $date[1];
						$jalali_date = dmy_format(toJalali($j_date));
						$jalali_datetime = $jalali_date;
						return $jalali_datetime;
					}
				})
				->addColumn('approval', function($option){
					if($option->approved == "0"){
						return "Not Approved";
					}
					else{
						return "Approved";
					}
				})
				->addColumn('viewed', function($option){
					if($option->viewed == "0"){
						return "Not Viewed";
					}
					else{
						return "Viewed";
					}
				})
				->addColumn('operation', function($option){
						return '<a href="'.route('getDeleteCaseNotification',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Record"><i class="fa fa-trash-o"></i></a>';
				})
				->make();
	}
	//delete case notification based on id.
	public function deleteCaseNotification($id=0)
	{
		$deleted = DB::connection('casemgmt')->table('notification')->where('id', $id)->delete();
		if($deleted)
		{
			// get the the log data and insert it into the log table.
			$caseLog = array('action_table' => "notification",'action_by' => Auth::user()->username,'action_type' => "Deleted");
			caseModel::addCaseLog($caseLog);	
	    	return Redirect::route("loadNotificationList")->with("success","Notification successfully Deleted.");
        }
        else
        {
            return Redirect::route("loadNotificationList")->with("fail","An error occured please try again or contact system developer.");
        }
	}

}

?>