<?php

// getting the table information without authentication.

namespace App\Http\Controllers\casemgmt;
use App\Http\Controllers\NoLoginController;
use Illuminate\Support\Collection;
use Datatable;
use DB;
class agenciesController extends NoLoginController{
	
	//get case agencies for the route without authentication.
	public function getCaseAgencies()
	{
		$rows = DB::connection('casemgmt')->table('agencies')->select('id','name_en','name_dr')->get();
		$agencies = "";
		foreach($rows as $item)
		{
			$agencies .= $item->name_en.",".$item->name_dr."<br />";
		}
		//return json_encode($rows);
		return $agencies;
	}
	//load the agency list page.
	
	
}

?>