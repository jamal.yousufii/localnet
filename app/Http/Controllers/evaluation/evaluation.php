<?php 

namespace App\Http\Controllers\evaluation;

use App\Http\Controllers\Controller;
use App\models\evaluation\evaluationModel;

use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use DateTime;
use DateInterval;
use DatePeriod;

class evaluation extends Controller
{
	//database connection
	public static $myDb = "evaluation";

	/**
    * Instantiate a new docsController instance.
    */
	public function __construct()
	{
		if(!Auth::check())
		{
			Redirect::route('getLogin')->send();
		}		
	}
	public function homePage($year=0)
	{
		$date = dateToShamsi(date('Y'),date('m'),date('d'));
		$sh_date = explode('-',$date);
		$data['sh_year'] = $sh_date[0];
		if($year==0)
		{
			$sh_year = $sh_date[0];
		}
		else
		{
			$sh_year = $year;
		}
		$data['year'] = $sh_year;
		$data['medals'] = evaluationModel::getTypeChart(1,$sh_year);
		$medals_items = evaluationModel::getSubTypes(1);
		$dg_array=array(0=>'هیچکدام');
		foreach($medals_items AS $dg)
		{
			$dg_array[$dg->id]=$dg->name_dr;
		}
		$data['medals_items']=$dg_array;
		//signs
		$data['signs'] = evaluationModel::getTypeChart(2,$sh_year);
		$signs_items = evaluationModel::getSubTypes(2);
		$dg_array=array(0=>'هیچکدام');
		foreach($signs_items AS $dg)
		{
			$dg_array[$dg->id]=$dg->name_dr;
		}
		$data['signs_items']=$dg_array;
		//تحسین نامه
		$data['tahsins'] = evaluationModel::getTypeChart(3,$sh_year);
		$tahsins_items = evaluationModel::getSubTypes(3);
		$dg_array=array(0=>'هیچکدام');
		foreach($tahsins_items AS $dg)
		{
			$dg_array[$dg->id]=$dg->name_dr;
		}
		$data['tahsins_items']=$dg_array;
		//تقدیرنامه
		$data['taqdirs'] = evaluationModel::getTypeChart(4,$sh_year);
		$taqdirs_items = evaluationModel::getSubTypes(4);
		$dg_array=array(0=>'هیچکدام');
		foreach($taqdirs_items AS $dg)
		{
			$dg_array[$dg->id]=$dg->name_dr;
		}
		$data['taqdirs_items']=$dg_array;
		
		$types = evaluationModel::getMedalTypes();
		$t_array=array();
		$t_array[1]=0;$t_array[2]=0;$t_array[3]=0;$t_array[4]=0;
		foreach($types AS $t)
		{
			$t_array[$t->approved_type]=$t->total;
		}
		$data['types']=$t_array;
		return View::make('evaluation.dashboard',$data);
	}
	public function getAllReceivedDocs($year=0)
	{
		//check roles
		if(canView('evaluation_receivedDocs'))
		{
			$date = dateToShamsi(date('Y'),date('m'),date('d'));
			$sh_date = explode('-',$date);
			$data['sh_year'] = $sh_date[0];
			if($year==0)
			{
				$sh_year = $sh_date[0];
			}
			else
			{
				$sh_year = $year;
			}
			$data['year'] = $sh_year;
			$data['doc_type'] = 0;
			$data['doc_no'] = 0;
			//$data['parentDeps'] = getDepartmentWhereIn();
			$data['doc_total'] = evaluationModel::getDocsTotal($sh_year);
			$data['docEmp_total'] = evaluationModel::getDocEmpsTotal(null,$sh_year);
			$data['docEmp_total_not'] = evaluationModel::getDocEmpsTotal(0,$sh_year);
			return View::make('evaluation.received_docs',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getAllReceivedDocs_post()
	{
		//check roles
		if(canView('evaluation_receivedDocs'))
		{
			$date = dateToShamsi(date('Y'),date('m'),date('d'));
			$sh_date = explode('-',$date);
			$data['sh_year'] = $sh_date[0];
			$sh_year = Input::get('year');
			$doc_type = Input::get('doc_type');
			$doc_no = Input::get('doc_no');
	
			$data['year'] = $sh_year;
			$data['doc_type'] = $doc_type;
			$data['doc_no'] = $doc_no;
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['doc_total'] = evaluationModel::getDocsTotal($sh_year,$doc_type,$doc_no);
			$data['docEmp_total'] = evaluationModel::getDocEmpsTotal(null,$sh_year,$doc_type,$doc_no);
			$data['docEmp_total_not'] = evaluationModel::getDocEmpsTotal(0,$sh_year,$doc_type,$doc_no);
			return View::make('evaluation.received_docs',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getAllDocs($year=0,$doc_type=0,$doc_no=0)
	{
		//get all data 
		$object = evaluationModel::getData($year,$doc_type,$doc_no);
		
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'doc_type',
							'doc_no')
						->addColumn('date', function($option){
							$sdate = $option->doc_date;
							if($sdate !=null)
							{
								$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);
								return jalali_format($sdate);
							}
							
						})
						->showColumns(
							'dep',
							'desc',
							'year',
							'file_name'
							)
				
				->addColumn('operations', function($option){
					$options = '';
					if(canEdit('evaluation_receivedDocs'))
					{
						$options.= '<a href="'.route('edit_Doc',$option->id).'" title="ویرایش" target="_blank"><i class="fa fa-edit"></i></a> | ';
					}
					if(canDelete('evaluation_receivedDocs'))
					{
						$options.='<a href="'.route('postDocDelete',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="حذف"><i class="fa fa-trash-o"></i></a> |';
					}
					$options.='<a href="'.route('viewDoc',$option->id).'" title="نمایش"><i class="fa fa-eye"></i></a>';
					
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		
	}
	public function search_docs(Request $request)
	{
		$per_page=10;
		$data['rows'] = evaluationModel::getDocsAjax($request,$per_page);
		//dd($data['rows']);
		return View::make('evaluation.docs_ajax',$data);
		
	}
	public function getAllDocEmployees($year=0)
	{
		//check roles
		if(canView('evaluation_receivedDocs'))
		{
			$date = dateToShamsi(date('Y'),date('m'),date('d'));
			$sh_date = explode('-',$date);
			$data['sh_year'] = $sh_date[0];
			if($year==0)
			{
				$sh_year = $sh_date[0];
			}
			else
			{
				$sh_year = $year;
			}
			$data['year'] = $sh_year;
			$data['type'] = 0;
			$data['item'] = 0;
			$data['status'] = 0;
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['medals'] = evaluationModel::medals_totol(1,$sh_year);//medals
			$data['signs'] = evaluationModel::medals_totol(2,$sh_year);//signs
			$data['taqdir'] = evaluationModel::medals_totol(4,$sh_year);//taqdir
			$data['tahsin'] = evaluationModel::medals_totol(3,$sh_year);//tahsin
			//$data['rejected'] = evaluationModel::getDocEmpsTotal(2,$sh_year);//rejected
			$data['docEmp_total'] = evaluationModel::getDocEmpsTotal(null,$sh_year);
			return View::make('evaluation.doc_employees',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getAllDocEmployees_post(Request $request)
	{
		//check roles
		if(canView('evaluation_receivedDocs'))
		{
			$date = dateToShamsi(date('Y'),date('m'),date('d'));
			$sh_date = explode('-',$date);
			$data['sh_year'] = $sh_date[0];
			$sh_year =$request->year;
			$type =$request->type;
			$item =$request->item;
			$data['year'] = $sh_year;
			$data['type'] = $type;
			$data['item'] = $item;
			$data['status'] = $request->status;
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['medals'] = evaluationModel::medals_totol(1,$sh_year,$type,$item);//medals
			$data['signs'] = evaluationModel::medals_totol(2,$sh_year,$type,$item);//signs
			$data['taqdir'] = evaluationModel::medals_totol(4,$sh_year,$type,$item);//taqdir
			$data['tahsin'] = evaluationModel::medals_totol(3,$sh_year,$type,$item);//tahsin
			$data['rejected'] = evaluationModel::getDocEmpsTotal(2,$sh_year);//rejected
			$data['docEmp_total'] = evaluationModel::getDocEmpsTotal($request->status,$sh_year);
			return View::make('evaluation.doc_employees',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getAllEmployeesData($year=0,$type=0,$item=0,$status=0)
	{
		//get all data 
		$object = evaluationModel::getEmployeesData($year,$type,$item,$status);
		
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'doc_no',
							'name',
							'f_name',
							'nationality',
							'suggested_type',
							'suggested_type_item',
							'bast',
							'rank',
							'job_title',
							'dep',
							'status_title'
							)
				
				->addColumn('operations', function($option){
					$options = '';
					$options.= '<a href="'.route('depEmpEdit',$option->id).'" target="_blank" title="ویرایش"><i class="fa fa-edit"></i></a> | ';
					if(canDelete('evaluation_receivedDocs'))
					{
						if($option->status==0)
						{
							$options.='<a href="'.route('postEmployeeDelete',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="حذف"><i class="fa fa-trash-o"></i></a> |';
						}
						elseif($option->status==1)
						{//cant be deleted as it is approved or rejected
							$options.='<a href="javascript:void()" onclick="alert(\'you cant delete this employee since it is approved\')" title="اجرا شده"><i class="fa fa-trash-o"></i></a> |';
						}
						elseif($option->status==2)
						{//cant be deleted as it is approved or rejected
							$options.='<a href="javascript:void()" onclick="alert(\'you cant delete this employee since it is rejected\')" title="اجرا نشده"><i class="fa fa-trash-o"></i></a> |';
						}
					}		
					if($option->status==0)
					{
						/*
						$options.='
							<a href="'.route('loadRejectDoc',$option->id).'" title="رد"><i class="fa fa-ban"></i></a> | 
							<a href="'.route('approve_doc',$option->id).'" title="تایید"><i class="fa fa-check"></i></a>
							';
						*/
						$options.='<a href="'.route('approve_doc',$option->id).'" title="تایید"><i class="fa fa-check"></i></a>';
					}
					elseif($option->status==1)
					{//approved
						$options.='<a href="'.route('approve_doc',$option->id).'">اجرا شده</a>';
					}
					else
					{//rejected
						//$options.='<a href="'.route('loadRejectDoc',$option->id).'">اجرا نشده</a>';
					}
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		
	}
	public function addNewDoc()
	{
		//check roles
		if(canAdd('evaluation_receivedDocs'))
		{
			$date = dateToShamsi(date('Y'),date('m'),date('d'));
			$sh_date = explode('-',$date);
			$data['year'] = $sh_date[0];
			
			$data['parentDeps'] = getReallyAllDepartments();
			$data['ministrires'] = evaluationModel::getAllMinistries();
			return View::make('evaluation.add_doc',$data);
		}
		else
		{
			return showWarning();
		}
	}
	function postDoc()
	{
		if(canAdd('evaluation_receivedDocs'))
		{
			$validates = Validator::make(Input::all(), array(
				//"doc_type"					=> "required",
				//"ministry"					=> "required",
				//"doc_no"					=> "required",
				//"date"						=> "required",
				"name_1"					=> "required",
				//"f_name_1"					=> "required"
				"suggested_type_1"			=> "required",
				"suggested_type_item_1"		=> "required"

				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('addNewDoc')->withErrors($validates)->withInput();
			}
			else
			{
				if(Input::get('date') != '')
				{
					$sdate = explode("-", Input::get('date'));
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				else
				{
					$sdate = null;
				}
				
				$items=array(
					'doc_type' 		=> Input::get('doc_type'),
					'ministry' 		=> Input::get('ministry'),
					'doc_no' 		=> Input::get('doc_no'),
					'doc_date' 		=> $sdate,
					'desc' 			=> Input::get('desc'),
					'year' 			=> Input::get('year'),
					'created_at'	=>date('Y-m-d H:i:s'),
					'created_by'	=>Auth::user()->id
					);
			}
			$id = evaluationModel::insertRecord_id('received_docs',$items);
			$emps = array();
			for($i=1;$i<=Input::get('total_emp');$i++)
			{
				$emp_bast = 0;$military_bast=0;$emp_rank = 0;$military_rank=0;
				if(Input::get('job_'.$i)==1)
				{
					$emp_bast = Input::get('emp_bast_'.$i);
					$emp_rank = Input::get('emp_rank_'.$i);
				}
				else
				{
					$military_bast = Input::get('military_bast_'.$i);
					$military_rank = Input::get('military_rank_'.$i);
				}
				if(Input::get('name_'.$i)!='')
				{
					$emps[]=array(
						'nationality' => Input::get('nationality_'.$i),
						'name' => Input::get('name_'.$i),
						'f_name' => Input::get('f_name_'.$i),
						'suggested_type' => Input::get('suggested_type_'.$i),
						'suggested_type_item' => Input::get('suggested_type_item_'.$i),
						'job_title' => Input::get('job_title_'.$i),
						'emp_type' => Input::get('job_'.$i),
						'emp_bast' => $emp_bast,
						'emp_rank' => $emp_rank,
						'military_bast' => $military_bast,
						'military_rank' => $military_rank,
						'ministry' 		=> Input::get('emp_ministry_'.$i),
						'doc_id' => $id,
						'status' => 1
					);
				}
			}
			evaluationModel::insertRecord('doc_employees',$emps);
			if(Input::hasFile('attach'))
			{
				$this->uploadEmployeeDocs($id,'attach','received_docs');
			}
			return \Redirect::route("getAllReceivedDocs")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function edit_doc($id=0)
	{
		//check roles
		if(canEdit('evaluation_receivedDocs'))
		{
			$date = dateToShamsi(date('Y'),date('m'),date('d'));
			$sh_date = explode('-',$date);
			$data['year'] = $sh_date[0];
			$data['parentDeps'] = getReallyAllDepartments();
			$data['ministrires'] = evaluationModel::getAllMinistries();
			$data['details'] = evaluationModel::getDocDetails($id);
			$data['emps'] = evaluationModel::getDocEmployees($id);
			$data['id'] = $id;
			return View::make('evaluation.edit_doc',$data);
		}
		else
		{
			return showWarning();
		}
	}
	function postEditDoc()
	{
		if(canEdit('evaluation_receivedDocs'))
		{
			$validates = Validator::make(Input::all(), array(
				"doc_type"					=> "required",
				"ministry"					=> "required",
				"doc_no"					=> "required",
				"date"						=> "required",
				// "name_1"					=> "required",
				// "f_name_1"					=> "required"
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('edit_Doc',Input::get('id'))->withErrors($validates)->withInput();
			}
			else
			{
				if(Input::get('date') != '')
				{
					$sdate = explode("-", Input::get('date'));
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				else
				{
					$sdate = null;
				}
				
				$items=array(
					'doc_type' => Input::get('doc_type'),
					'ministry' => Input::get('ministry'),
					'doc_no' => Input::get('doc_no'),
					'doc_date' => $sdate,
					'desc' => Input::get('desc'),
					'year' => Input::get('year')
					
					);
			}
			$id = Input::get('id');
			evaluationModel::update_record('received_docs',$items,array('id'=>$id));
			$emps = array();
			for($i=1;$i<=Input::get('total_emp');$i++)
			{
				$emp_bast = 0;$military_bast=0;$emp_rank = 0;$military_rank=0;
				if(Input::get('job_'.$i)==1)
				{
					$emp_bast = Input::get('emp_bast_'.$i);
					$emp_rank = Input::get('emp_rank_'.$i);
				}
				else
				{
					$military_bast = Input::get('military_bast_'.$i);
					$military_rank = Input::get('military_rank_'.$i);
				}
				if(Input::get('name_'.$i)!='')
				{
					$emps[]=array(
						'nationality' 		=> Input::get('nationality_'.$i),
						'name' 				=> Input::get('name_'.$i),
						'f_name' 			=> Input::get('f_name_'.$i),
						'suggested_type' 	=> Input::get('suggested_type_'.$i),
						'suggested_type_item' => Input::get('suggested_type_item_'.$i),
						'emp_type' 			=> Input::get('job_'.$i),
						'job_title' 		=> Input::get('job_title_'.$i),
						'emp_bast' 			=> $emp_bast,
						'emp_rank' 			=> $emp_rank,
						'military_bast' 	=> $military_bast,
						'military_rank' 	=> $military_rank,
						'ministry' 			=> Input::get('emp_ministry_'.$i),
						'doc_id' 			=> $id
					);
				}
			}
			//dd($emps);
			//evaluationModel::delete_record('doc_employees',array('doc_id'=>$id));
			evaluationModel::insertRecord('doc_employees',$emps);
			if(Input::hasFile('attach'))
			{
				$fileName =  DB::connection('evaluation')
								->table('received_docs')
								->where('file_name', 'like', 'Doc_'.$id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/evaluation_attachments/".$fileName;
				if(File::delete($file))
				{
					evaluationModel::update_record('received_docs',array('full_name'=>null),array('id'=>$id));
				}
				$this->uploadEmployeeDocs($id,'attach','received_docs');
			}
			return \Redirect::route("getAllReceivedDocs")->with("success","Record updated Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function viewDoc($id=0)
	{
		//check roles
		if(canView('evaluation_receivedDocs'))
		{
			$data['parentDeps'] = getReallyAllDepartments();
			$data['ministrires'] = evaluationModel::getAllMinistries();
			$data['details'] = evaluationModel::getDocDetails($id);
			$data['emps'] = evaluationModel::getDocEmployees($id);
			$data['id'] = $id;
			return View::make('evaluation.viewDoc',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function postDocDelete($id=0)
	{
		if(canDelete('evaluation_receivedDocs'))
		{
			$fileName =  DB::connection('evaluation')
							->table('received_docs')
							->where('file_name', 'like', 'Doc_'.$id.'.%')
							->pluck('file_name');
			$file= public_path(). "/documents/evaluation_attachments/".$fileName;
			File::delete($file);
			
			evaluationModel::delete_employee_det($id);
			evaluationModel::delete_record('received_docs',array('id'=>$id));
			evaluationModel::delete_record('doc_employees',array('doc_id'=>$id));
			
			return \Redirect::route("getAllReceivedDocs")->with("success","Record deleted Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function postDocViaAjax()
	{
		if(Input::get('date') != '')
		{
			$sdate = explode("-", Input::get('date'));
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);		
		}
		else
		{
			$sdate = null;
		}
		
		$items=array(
			'doc_type' => Input::get('doc_type'),
			'ministry' => Input::get('ministry'),
			'doc_no' => Input::get('doc_no'),
			'doc_date' => $sdate,
			'desc' => Input::get('desc'),
			'year' => Input::get('year')
			
			);
		
		$doc_id = Input::get('doc_id');
		evaluationModel::update_record('received_docs',$items,array('id'=>$doc_id));
	}
	public function depEmpEdit($emp_id=0)
	{
		//check roles
		if(canEdit('evaluation_receivedDocs') || canView('evaluation_receivedDocs'))
		{
			$date = dateToShamsi(date('Y'),date('m'),date('d'));
			$sh_date = explode('-',$date);
			$data['year'] = $sh_date[0];
			$data['parentDeps'] = getReallyAllDepartments();
			$data['ministrires'] = evaluationModel::getAllMinistries();
			$data['emp'] = evaluationModel::getEmpDetails($emp_id);
			$data['items'] = evaluationModel::getItems($data['emp']->suggested_type);
			$data['details'] = evaluationModel::getDocDetails($data['emp']->doc_id);
			//$data['emps'] = evaluationModel::getDocEmployees($id);
			$data['id'] = $emp_id;
			return View::make('evaluation.edit_employee',$data);
		}
		else
		{
			return showWarning();
		}
	}
	function postEditEmployee()
	{
		if(canEdit('evaluation_receivedDocs'))
		{
			$validates = Validator::make(Input::all(), array(
				"name"					=> "required",			
				"f_name"				=> "required",			
				"job"					=> "required"			
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('depEmpEdit',Input::get('id'))->withErrors($validates)->withInput();
			}
			else
			{
				$id = Input::get('id');
				$emp_bast = 0;$military_bast=0;$emp_rank = 0;$military_rank=0;
				if(Input::get('job')==1)
				{
					$emp_bast = Input::get('emp_bast');
					$emp_rank = Input::get('emp_rank');
				}
				else
				{
					$military_bast = Input::get('military_bast');
					$military_rank = Input::get('military_rank');
				}
				$emps=array(
							'nationality' => Input::get('nationality'),
							'name' => Input::get('name'),
							'f_name' => Input::get('f_name'),
							'suggested_type' => Input::get('suggested_type'),
							'suggested_type_item' => Input::get('suggested_type_item'),
							'job_title' => Input::get('job_title'),
							'emp_type' => Input::get('job'),
							'emp_bast' => $emp_bast,
							'emp_rank' => $emp_rank,
							'military_rank' => $military_rank,
							'military_bast' => $military_bast,
							'ministry' => Input::get('emp_ministry')
							//'doc_id' => $id
							);
				evaluationModel::update_record('doc_employees',$emps,array('id'=>$id));
				
				//return \Redirect::route("getAllDocEmployees")->with("success","Record updated Successfully.");
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeDelete($id=0)
	{
		if(canDelete('evaluation_receivedDocs'))
		{
			evaluationModel::delete_record('approved_employees',array('emp_id'=>$id));
			evaluationModel::delete_record('rejected_employees',array('emp_id'=>$id));
			evaluationModel::delete_record('doc_employees',array('id'=>$id));
			
			return \Redirect::route("getAllDocEmployees")->with("success","Record deleted Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function uploadEmployeeDocs($doc_id=0,$type='',$table='')
	{
		// getting all of the post data
		$file = Input::file($type);
		$errors = "";
		$file_data = array();
		// validating each file.
		$rules = array($type => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
		$validator = Validator::make(

		  		[
		            $type => $file,
		            'extension'  => Str::lower($file->getClientOriginalExtension()),
		        ],
		        [
		            $type => 'required|max:100000',
		            'extension'  => 'required|in:doc,docx,pdf,png,jpg'
		        ]
		);

		if($validator->passes())
		{
		    // path is root/uploads
		    $destinationPath = 'documents/evaluation_attachments';
		    $filename = $file->getClientOriginalName();
			
		    $temp = explode(".", $filename);
		    $extension = end($temp);
		    if($table=='approved_employees')
		    {
		    	$filename = 'approved_'.$doc_id.'.'.$extension;
		    }
		    elseif($table=='rejected_employees')
		    {
		    	$filename = 'rejected_'.$doc_id.'.'.$extension;
		    }
		    
		    else
		    {
		    	$filename = 'Doc_'.$doc_id.'.'.$extension;
			}
		    $upload_success = $file->move($destinationPath, $filename);

		    if($upload_success) 
		    {
			   	$data = array(
			   					'file_name'=>$filename
			   				);
			   if(count($data)>0)
				{						
					evaluationModel::update_record($table,$data,array('id'=>$doc_id));
				}
			} 
			else 
			{
			   $errors .= json('error', 400);
			}
		} 
		else 
		{
		    // redirect back with errors.
		    return Redirect::back()->withErrors($validator);
		}
	}
	public function loadRejectDoc($id=0)
	{
		if(canEdit('evaluation_receivedDocs') || canView('evaluation_receivedDocs'))
		{
			$data['id'] = $id;//emp_id
			$data['parentDeps'] = getReallyAllDepartments();
			$data['ministrires'] = evaluationModel::getAllMinistries();
			$data['details'] = evaluationModel::getEmpRejectedDet($id);
			if($data['details'])
			{//already rejected
				$data['send_tos']=evaluationModel::getSentDeps_rejected($id);
				return View::make('evaluation.rejected_doc',$data);
			}
			else
			{
				if(canEdit('evaluation_receivedDocs'))
				{
					return View::make('evaluation.rejectDoc',$data);
				}
			}
		}
		else
		{
			return showWarning();
		}	
	}
	public function postRejectDoc()
	{
		if(canAdd('evaluation_receivedDocs'))
		{
			if(Input::get('issued_date') != '')
			{
				$sdate = explode("-", Input::get('issued_date'));
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			$data = array(
					"emp_id" 				=> Input::get('id'),
					"issued_no" 			=> Input::get('issued_no'),
					"issued_date" 			=> $sdate,
					"ministry" 				=> Input::get('ministry'),
					"subject" 				=> Input::get('subject'),
					'created_at'			=>date('Y-m-d H:i:s'),
					'created_by'			=>Auth::user()->id
				);
			$rejected_id = evaluationModel::insertRecord_id('rejected_employees',$data);
			
			if(Input::hasFile('attach'))
			{
				$this->uploadEmployeeDocs($rejected_id,'attach','rejected_employees');
			}
			
			$items = array(
					"status" 			=> 2
				);
			
			evaluationModel::update_record('doc_employees',$items,array('id'=>Input::get('id')));
			return \Redirect::route("getAllDocEmployees")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}	
	}
	public function postRejectedDoc()
	{
		if(canEdit('evaluation_receivedDocs'))
		{
			if(Input::get('issued_date') != '')
			{
				$sdate = explode("-", Input::get('issued_date'));
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			
			$data = array(
					//"emp_id" 				=> Input::get('id'),
					"issued_no" 			=> Input::get('issued_no'),
					"issued_date" 			=> $sdate,
					"ministry" 				=> Input::get('ministry'),
					//"sent_to_dep" 			=> Input::get('send_to'),
					"subject" 				=> Input::get('subject')
				);
			$id = Input::get('id');//rejected id
			evaluationModel::update_record('rejected_employees',$data,array('id'=>$id));
			
			if(Input::hasFile('attach'))
			{
				$fileName =  DB::connection('evaluation')
								->table('rejected_employees')
								->where('file_name', 'like', 'rejected_'.$id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/evaluation_attachments/".$fileName;
				if(File::delete($file))
				{
					evaluationModel::update_record('rejected_employees',array('full_name'=>null),array('id'=>$id));
				}
				
				$this->uploadEmployeeDocs($id,'attach','rejected_employees');
			}
			
			return \Redirect::route("getAllDocEmployees")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}	
	}
	public function unRejectDoc()
	{
		if(canEdit('evaluation_receivedDocs'))
		{
			//evaluationModel::update_record('received_docs',array('employee_id'=>Input::get('employee_id'),'year'=>Input::get('year'),'month'=>Input::get('month')));
			$items = array(
					"status" 			=> 0,
					//"reject_reason" 	=> null
				);
			$emp_id =Input::get('emp_id');//emp_id
			$id =Input::get('id');//rejected_id
			evaluationModel::update_record('doc_employees',$items,array('id'=>$emp_id));
			$fileName =  DB::connection('evaluation')
							->table('rejected_employees')
							->where('file_name', 'like', 'rejected_'.$id.'.%')
							->pluck('file_name');
			$file= public_path(). "/documents/evaluation_attachments/".$fileName;
			File::delete($file);
	
			evaluationModel::delete_record('rejected_employees',array('emp_id'=>$emp_id));
			evaluationModel::delete_record('rejected_send_to',array('rejected_id'=>$id));
			
			//hrOperation::insertLog('employee_salary_waits',1,'salary wait of given employee',Input::get('employee_id'));
		}
		else
		{
			return showWarning();
		}	
	}
	public function unApproveEmp()
	{
		if(canEdit('evaluation_receivedDocs'))
		{
			//evaluationModel::update_record('received_docs',array('employee_id'=>Input::get('employee_id'),'year'=>Input::get('year'),'month'=>Input::get('month')));
			$items = array(
					"status" 			=> 0,
					//"reject_reason" 	=> null
				);
			$emp_id =Input::get('emp_id');//emp_id
			$id =Input::get('id');//rejected_id
			
			evaluationModel::update_record('doc_employees',$items,array('id'=>$emp_id));
			$fileName =  DB::connection('evaluation')
							->table('approved_employees')
							->where('file_name', 'like', 'approved_'.$id.'.%')
							->pluck('file_name');
			$file= public_path(). "/documents/evaluation_attachments/".$fileName;
			File::delete($file);
	
			evaluationModel::delete_record('approved_employees',array('emp_id'=>$emp_id));
			evaluationModel::delete_record('approved_send_to',array('approved_id'=>$id));
			
			//hrOperation::insertLog('employee_salary_waits',1,'salary wait of given employee',Input::get('employee_id'));
		}
		else
		{
			return showWarning();
		}	
	}
	public function approveDoc($id=0)
	{
		if(canEdit('evaluation_receivedDocs') || canView('evaluation_receivedDocs'))
		{
			$data['id'] = $id;
			$employee = evaluationModel::getEmployeeDoc($id);
			$data['parentDeps'] = getReallyAllDepartments();
			$data['ministrires'] = evaluationModel::getAllMinistries();
			$data['details'] = evaluationModel::getEmpApptovedDet($id);
			// Added after the changes to show inserted data 
			$data['doc_details'] = evaluationModel::getDocDetails($employee->doc_id);
			$data['emp'] = evaluationModel::getDocEmployees($employee->doc_id,true);
			if($data['details'])
			{
				$data['items'] = evaluationModel::getItems($data['details']->approved_type);
				$data['sub_deps']=getRelatedSubDepartment($data['details']->sender_gen_dep);
				$data['send_tos']=evaluationModel::getSentDeps_approved($id);
				return View::make('evaluation.approved_form',$data);
			}
			else
			{
				if(canEdit('evaluation_receivedDocs'))
				{
					return View::make('evaluation.approve_form',$data);
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function postApproveDoc()
	{
		if(canAdd('evaluation_receivedDocs'))
		{
			$validates = Validator::make(Input::all(), array(
				"approved_type"					=> "required",
				"approved_type_item"			=> "required"
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('approve_doc',array(Input::get('id')))->withErrors($validates);
			}
			else
			{
				if(Input::get('issued_date') != '')
				{
					$sdate = explode("-", Input::get('issued_date'));
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				else
				{
					$sdate = null;
				}
				if(Input::get('hokm_date') != '')
				{
					$hdate = explode("-", Input::get('hokm_date'));
					$hy = $hdate[2];
					$hm = $hdate[1];
					$hd = $hdate[0];
					$hdate = dateToMiladi($hy,$hm,$hd);		
				}
				else
				{
					$hdate = null;
				}
				if(Input::get('farman_date') != '')
				{
					$fdate = explode("-", Input::get('farman_date'));
					$fy = $fdate[2];
					$fm = $fdate[1];
					$fd = $fdate[0];
					$fdate = dateToMiladi($fy,$fm,$fd);		
				}
				else
				{
					$fdate = null;
				}
				
				$data = array(
						"approved_type" 		=> Input::get('approved_type'),
						"approved_type_item" 	=> Input::get('approved_type_item'),
						"title" 				=> Input::get('title'),
						"hokm_no" 				=> Input::get('hokm_no'),
						"hokm_date" 			=> $hdate,
						"farman_no" 			=> Input::get('farman_no'),
						"farman_date" 			=> $fdate,
						"emp_id" 				=> Input::get('id'),
						"issued_no" 			=> Input::get('issued_no'),
						"issued_date" 			=> $sdate,
						"subject" 				=> Input::get('subject'),
						"how" 					=> Input::get('how'),
						"ministry" 				=> Input::get('ministry'),//مرسل الیه
						'created_at'			=>date('Y-m-d H:i:s'),
						'created_by'			=>Auth::user()->id
					);
				$rejected_id = evaluationModel::insertRecord_id('approved_employees',$data);
				
				if(Input::hasFile('attach'))
				{
					$this->uploadEmployeeDocs($rejected_id,'attach','approved_employees');
				}
				
				$items = array(
						"status" 			=> 1
					);
				
				evaluationModel::update_record('doc_employees',$items,array('id'=>Input::get('id')));
				return \Redirect::route("getAllDocEmployees")->with("success","Record Saved Successfully.");
			}		
		}
		else
		{
			return showWarning();
		}	
	}
	public function postApprovedDoc()
	{
		if(canEdit('evaluation_receivedDocs'))
		{
			$validates = Validator::make(Input::all(), array(
				"approved_type"					=> "required",
				"approved_type_item"			=> "required"
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('approve_doc',array(Input::get('id')))->withErrors($validates);
			}
			else
			{
				if(Input::get('issued_date') != '')
				{
					$sdate = explode("-", Input::get('issued_date'));
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				else
				{
					$sdate = null;
				}
				if(Input::get('hokm_date') != '')
				{
					$hdate = explode("-", Input::get('hokm_date'));
					$hy = $hdate[2];
					$hm = $hdate[1];
					$hd = $hdate[0];
					$hdate = dateToMiladi($hy,$hm,$hd);		
				}
				else
				{
					$hdate = null;
				}
				if(Input::get('farman_date') != '')
				{
					$fdate = explode("-", Input::get('farman_date'));
					$fy = $fdate[2];
					$fm = $fdate[1];
					$fd = $fdate[0];
					$fdate = dateToMiladi($fy,$fm,$fd);		
				}
				else
				{
					$fdate = null;
				}
				
				$data = array(
						"approved_type" 		=> Input::get('approved_type'),
						"approved_type_item" 	=> Input::get('approved_type_item'),
						"title" 				=> Input::get('title'),
						"hokm_no" 				=> Input::get('hokm_no'),
						"hokm_date" 			=> $hdate,
						"farman_no" 			=> Input::get('farman_no'),
						"farman_date" 			=> $fdate,
						//"emp_id" 				=> Input::get('id'),
						"issued_no" 			=> Input::get('issued_no'),
						"issued_date" 			=> $sdate,
						"how" 					=> Input::get('how'),
						"ministry" 				=> Input::get('ministry'),
						"subject" 				=> Input::get('subject')
					);
				$id = Input::get('id');//approved_id
				evaluationModel::update_record('approved_employees',$data,array('id'=>$id));
				
				if(Input::hasFile('attach'))
				{
					$fileName =  DB::connection('evaluation')
									->table('approved_employees')
									->where('file_name', 'like', 'approved_'.$id.'.%')
									->pluck('file_name');
					$file= public_path(). "/documents/evaluation_attachments/".$fileName;
					if(File::delete($file))
					{
						evaluationModel::update_record('approved_employees',array('full_name'=>null),array('id'=>$id));
					}
					
					$this->uploadEmployeeDocs($id,'attach','approved_employees');
				}
				
				return \Redirect::route("getAllDocEmployees")->with("success","Record Saved Successfully.");
		
			}		
		}
		else
		{
			return showWarning();
		}	
	}
	public function getMoreEmployees()
	{
		$data['total'] = Input::get('total');
		$data['ministrires'] = evaluationModel::getAllMinistries();
		return View::make('evaluation.more_employees',$data);
	}
	public function bring_items()
	{
		$type = Input::get('type');
		$deps = evaluationModel::getItems($type);

		$options = '<option value="0">یک گزینه را انتخاب کنید</option>';
		foreach($deps AS $item)
		{
			$options.="<option value='".$item->id."'>".$item->name_dr."</option>";
		}
		$options.="<option value='0'>هیچکدام</option>";
		return $options;
	}
	public function getSearch()
	{
		$data['parentDeps'] = getReallyAllDepartments();
		$data['ministrires'] = evaluationModel::getAllMinistries();
		$data['active']='';
		$data['result'] = false;
		$date = dateToShamsi(date('Y'),date('m'),date('d'));
		$sh_date = explode('-',$date);
		$data['year'] = $sh_date[0];
		return View::make('evaluation.search_form',$data);
	}
	public function getEvaluationSearch(request $request)
	{
		$data['active'] = 'search';
		$data['result'] = evaluationModel::getEvaluationSearchData($request,false);
		$data['total'] = evaluationModel::getEvaluationSearchData($request,true);
		
		return View::make('evaluation.search_result',$data);
	}
	public function getEvaluationSearch_ajax(request $request)
	{
		$data['active'] = 'search';
		$data['pageno'] = $request['pageno'];
		$data['result'] = evaluationModel::getEvaluationSearchData_ajax($request);
		
		return View::make('evaluation.search_result_ajax',$data);
	}
	public function downloadDoc($file_id=0)
	{
		if(canView('evaluation_receivedDocs'))
		{
			//get file name from database
			$fileName = evaluationModel::getFileName($file_id,'approved_employees');
	        //public path for file
	        $file= public_path(). "/documents/evaluation_attachments/".$fileName;
	        //download file
	        return Response::download($file, $fileName);
	    }
	    else
	    {
	    	return showWarning();
	    }
	}
	public function downloadDocument($file_id=0)
	{
		if(canView('evaluation_receivedDocs'))
		{
			//get file name from database
			$fileName = evaluationModel::getFileName($file_id,'received_docs');
	        //public path for file
	        $file= public_path(). "/documents/evaluation_attachments/".$fileName;
	        //download file
	        return Response::download($file, $fileName);
	    }
	    else
	    {
	    	return showWarning();
	    }
	}
	public function downloadRejectedDoc($file_id=0)
	{
		if(canView('evaluation_receivedDocs'))
		{
			//get file name from database
			$fileName = evaluationModel::getFileName($file_id,'rejected_employees');
	        //public path for file
	        $file= public_path(). "/documents/evaluation_attachments/".$fileName;
	        //download file
	        return Response::download($file, $fileName);
	    }
	    else
	    {
	    	return showWarning();
	    }
	}
	public function removeFile()
	{
		if(canDelete('evaluation_receivedDocs'))
		{
			$file_id = Input::get('doc_id');
			$type = Input::get('type');
			
			//get file name from database
			$fileName = evaluationModel::getFileName($file_id,$type);
			$file= public_path(). "/documents/evaluation_attachments/".$fileName;
			File::delete($file);
			
			evaluationModel::update_record($type,array('file_name'=>null),array('id'=>$file_id));
				
			return "<div class='alert alert-success'>File Deleted Successfully!</div>";
			
		}
		else
		{
			return showWarning();
		}
	}
	public function docFullView($doc_id=0,$emp_id=0)
	{
		//check roles
		if(canView('evaluation_receivedDocs'))
		{
			$date = dateToShamsi(date('Y'),date('m'),date('d'));
			$sh_date = explode('-',$date);
			$data['year'] = $sh_date[0];
			$data['parentDeps'] = getReallyAllDepartments();
			$data['ministrires'] = evaluationModel::getAllMinistries();
			$data['details'] = evaluationModel::getDocDetails($doc_id);
			$data['approved_details'] = evaluationModel::getEmpApptovedDet($emp_id);
			$data['emp'] = evaluationModel::getEmpDetails($emp_id);
			$data['sent_to_sub_deps']=getRelatedSubDepartment($data['approved_details']->sent_to_gen_dep);
			$data['id'] = $doc_id;
			if($data['emp']->status==1)
			{//approved
				$data['approved'] = evaluationModel::getEmpApptovedDet($emp_id);
				
				$data['items'] = evaluationModel::getItems($data['approved']->approved_type);
				$data['approved_sub_deps']=getRelatedSubDepartment($data['approved']->sender_gen_dep);
				$data['send_tos']=evaluationModel::getSentDeps_approved($emp_id);
			}
			return View::make('evaluation.full_view',$data);
			
		}
		else
		{
			return showWarning();
		}
	}
	public function getEvaluationExcel(request $request)
	{
		//get data from model
		$data['ministrires'] = evaluationModel::getAllMinistries();
		$data['results'] = evaluationModel::getEvaluationSearchData($request,true);
		//dd($data['results']);
		Excel::load('excel_template/medals_report.xls', function($file) use($data){
		//Excel::create('Filename', function($file) use($results){
			
			$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){
			//$sheet = $file -> getActiveSheetIndex(1);	
				$row = 3;$i=1;
				foreach($data['results'] AS $item)
				{
					$sheet->setCellValue('A'.$row.'',$i);
					$sheet->setCellValue('B'.$row.'',$item->doc_type);
					$sheet->setCellValue('C'.$row.'',$item->dep);
					$sheet->setCellValue('D'.$row.'',$item->doc_no);
					$sdate = $item->doc_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}
						$sheet->setCellValue('E'.$row.'',$sdate);
					
					$sheet->setCellValue('F'.$row.'',$item->desc);
					//$sheet->mergeCells('F'.$row.':H'.$row.'');
					//$sheet->setCellValue('F'.$row.'',$item->date);
					//$sheet->setCellValue('G'.$row.'',$item->status_title);
					//$sheet->setCellValue('H'.$row.'',$item->nationality);
					$sheet->setCellValue('G'.$row.'',$item->nationality);
					$sheet->setCellValue('H'.$row.'',$item->name);
					//$sheet->mergeCells('M'.$row.':O'.$row.'');
					$sheet->setCellValue('I'.$row.'',$item->f_name);
					$sheet->setCellValue('J'.$row.'',$item->emptype);

					$sheet->setCellValue('K'.$row.'',$item->bast);
					//$sheet->mergeCells('R'.$row.':T'.$row.'');
					$sheet->setCellValue('L'.$row.'',$item->job_title);
					$sheet->setCellValue('M'.$row.'',$item->emp_dir);
					//$sheet->setCellValue('S'.$row.'',$item->desc);
					$sheet->setCellValue('N'.$row.'',$item->farman_no);
					$sdate = $item->farman_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}
					$sheet->setCellValue('O'.$row.'',$sdate);
					$sheet->setCellValue('P'.$row.'',$item->hokm_no);
					$sdate = $item->hokm_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}
						$sheet->setCellValue('Q'.$row.'',$sdate);
					
					
					$sheet->setCellValue('R'.$row.'',$item->title_name);
					//$sheet->setCellValue('U'.$row.'',$item->suggested_type);
					$sheet->setCellValue('S'.$row.'',$item->approved_type_item);
					$sheet->setCellValue('T'.$row.'',$item->how);
					$sheet->setCellValue('U'.$row.'','ریاست نظارت و ارزیابی');
					$sheet->setCellValue('V'.$row.'',$item->elayhe);
					
					
					$sheet->setCellValue('W'.$row.'',$item->issued_no);
					$sheet->setCellValue('X'.$row.'',$item->issued_date);
					$row++;$i++;
				}
				$border = $row-1;
				// Set border for range
				$sheet->setBorder('A5:AB'.$border.'', 'thin');

    		});
		
		})->export('xls');
	}
}
?>