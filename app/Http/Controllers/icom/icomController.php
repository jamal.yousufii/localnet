<?php 

namespace App\Http\Controllers\icom;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\models\icom\icomModel;
use Illuminate\Support\Collection;
use App\library\jdatetime;
use View;
use DB;
use Validator;
use Input;
use Redirect;
use Response;
use Datatable;
use Session;
use DateTime;
use DateInterval;
use File;
use Illuminate\Support\Str;
use URL;
use Carbon\Carbon;

class icomController extends Controller
{
		

	//Load returns list view
	public function depList()
	{

		return View::make('icom.depList');
	}

	//get form data list
	public function getAllDepartures()
	{

		//get all data 
		$object = icomModel::getData();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'job',
							'office',
							'travel_date',
							'country',
							'return_date',
							'statement_number',
							'chart_number'
							)
				->addColumn('travel_date', function($option){
					if(isMiladiDate()){
						return $option->travel_date;
					}else{
						$travel_date = dmy_format(toJalali($option->travel_date));
						return $travel_date;
					}
				})
				->addColumn('return_date', function($option){
					if(isMiladiDate()){
						return $option->return_date;
					}else{
						$return_date = dmy_format(toJalali($option->return_date));
						return $return_date;
					}
				})
				->addColumn('operations', function($option){
					return '<a href="'.route('depEdit',$option->id).'" title="Edit Travel"><i class="fa fa-edit"></i></a> | 
							<a href="'.route('postDepDelete',$option->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="Delete Travel"><i class="fa fa-trash-o"></i></a> | 
							<a href="'.route('viewComment',$option->id).'" title="View Travel Comment">
								<i class="fa fa-eye"></i>
							</a>';
				})
				// ->orderColumns('id','id')
				->make();
		
	}

	// insert the departure data to the table.
	public function addDepForm()
	{
		return View::make('icom.depForm');
	}

	// insert the departure data to the table.
	public function addDeparture(Request $request)
	{
		//echo "<pre>";print_r($_POST);exit;
		if(canAdd('icom_dep_list'))
		{
			//validate the input fields
		    $this->validate($request,[
		        "name" 					=> "required",
		        "travel_date" 			=> 'required',
		        "return_date" 			=> 'required',
		        "statement_number"		=> 'required'		        
		        ]
		    );
			//check the date type if it's shamsi or miladi.
			if(isMiladiDate()){
				$travel_date = Input::get('travel_date');
				$return_date = Input::get('return_date');
			}else{
				$travel_date = toGregorian(ymd_format(Input::get('travel_date')));
				$return_date = toGregorian(ymd_format(Input::get('return_date')));
			}
			
		    // get the form data in array;
		    $form_data = array(
		    			'name'					=> Input::get('name'),
		    			'job'					=> Input::get('job'),
		    			'office'				=> Input::get('office'),
		    			'travel_date'			=> $travel_date,
		    			'country'				=> Input::get('travel_country'),
		    			'return_date'			=> $return_date,
		    			'statement_number'		=> Input::get('statement_number'),
		    			'chart_number'			=> Input::get('chart_number'),
		    			'user_id'				=> Auth::user()->id
		    	);

		    // now insert the form data to its table through calling the model function.
		    $object = icomModel::insertDepartures($form_data);

		    if($object)
		    {
		    	// get the the log data and insert it into the log table.
				$depLog = array(

						'name'					=> Input::get('name'),
		    			'job'					=> Input::get('job'),
		    			'office'				=> Input::get('office'),
		    			'travel_date'			=> $travel_date,
		    			'country'				=> Input::get('travel_country'),
		    			'return_date'			=> $return_date,
		    			'statement_number'		=> Input::get('statement_number'),
		    			'chart_number'			=> Input::get('chart_number'),
		    			'user'					=> Auth::user()->username,
		    			'action'				=> 'Added'

					);
				icomModel::addDepartureLog($depLog);
		    	return Redirect::route("getLoadDepList")->with("success","The Departure successfully added.");
	        }
	        else
	        {
	            return Redirect::route("getLoadDepList")->with("fail","An error occured plase try again or contact system developer.");
	        }

		}
		else
		{
			return showWarning();
		} 
	}

	// Load the edit page with the particular departure's data in fields.
	public function editDeparture($id = 0)
	{
		//get the Departure based on id
		$data['deps'] = icomModel::getSpecificDep($id);
		//print_r($record);exit;
		//load view for editing the Meeting.
		return View::make('icom.depEditForm', $data);
	}

	public function postEditDeparture(Request $request,$id = 0)
	{
		//echo "<pre>";print_r($_POST);exit;
		if(canEdit('icom_dep_list'))
		{
			//validate the input fields
		    $this->validate($request,[
		        "name" 					=> "required",
		        "travel_date" 			=> 'required',
		        "return_date" 			=> 'required',
		        "statement_number"		=> 'required'		        
		        ]
		    );
			
			//check the date type if it's shamsi or miladi.
			if(isMiladiDate()){
				$travel_date = Input::get('travel_date');
				$return_date = Input::get('return_date');
			}else{
				$travel_date = toGregorian(ymd_format(Input::get('travel_date')));
				$return_date = toGregorian(ymd_format(Input::get('return_date')));
			}
		    // get the form data in array;
		    $form_data = array(
		    			'name'					=> Input::get('name'),
		    			'job'					=> Input::get('job'),
		    			'office'				=> Input::get('office'),
		    			'travel_date'			=> $travel_date,
		    			'country'				=> Input::get('travel_country'),
		    			'return_date'			=> $return_date,
		    			'statement_number'		=> Input::get('statement_number'),
		    			'chart_number'			=> Input::get('chart_number'),
		    			'user_id'				=> Auth::user()->id
		    	);

		    // now insert the form data to its table through calling the model function.
		    $object = icomModel::updateDepartures($form_data, $id);

		    if($object)
		    {
		    	// get the the log data and insert it into the log table.
				$depLog = array(

						'name'					=> Input::get('name'),
		    			'job'					=> Input::get('job'),
		    			'office'				=> Input::get('office'),
		    			'travel_date'			=> $travel_date,
		    			'country'				=> Input::get('travel_country'),
		    			'return_date'			=> $return_date,
		    			'statement_number'		=> Input::get('statement_number'),
		    			'chart_number'			=> Input::get('chart_number'),
		    			'user'					=> Auth::user()->username,
		    			'action'				=> 'Updated'

					);
				icomModel::addDepartureLog($depLog);
		    	return Redirect::route("getLoadDepList")->with("success","The Departure successfully updated.");
	        }
	        else
	        {
	            return Redirect::route("getLoadDepList")->with("fail","Either you didn't change the form or something is wrong, plase try again or contact system developer.");
	        }
		}
		else
		{
			return showWarning();
		} 
	}

	public function postDeleteDeparture($id = 0)
	{
		if(canDelete('icom_dep_list'))
		{
			// add the deleted record to the log table.
			icomModel::addDeletedDepsLog($id);
	    
	    	// delete the record based on id.
			$deleted = DB::connection('icom')->table('departure')->where('id', $id)->delete();
			
			if($deleted)
	        {
	    		return Redirect::route("getLoadDepList")->with("success","Departure successfully deleted , ID: <span style='color:red;font_weight:bold;'>$id</span>");        	
	        }
	        else
	        {
	            return Redirect::route("getLoadDepList")->with("fail","An error occured plase try again or contact system developer.");
	        }
	    }
	}

	//Load returns list view
	public function returnList()
	{

		return View::make('icom.returnList');
	}

	//get form data list
	public function getReturns()
	{

		//get all data 
		$object = icomModel::getReturns();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'job',
							'office',
							'return_date',
							'return_country',
							'passport_number',
							'airline_name',
							'email'
							)
				->addColumn('return_date', function($option){
					if(isMiladiDate()){
						return $option->return_date;
					}else{
						$return_date = dmy_format(toJalali($option->return_date));
						return $return_date;
					}
				})
				->addColumn('operations', function($option){
					$options = '';
					if($option->status == '')
					{
						$options .= '<div class="return'.$option->id.'"><a href="#" style="text-align:center;display:block;text-decoration:none" onclick="approveReturn(\'return\', '.$option->id.')">Approve</a></div>';
					}
					else
					{
						$options .= '<span style="color:green;text-align:center;display:block;font-weight:bold;">Approved</span>';
					}
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		
	}

	// load the filtering page.
	public function loadFilterPage()
	{
		return View::make('icom.depFiltering');
	}

	// get last week departures.
	public function getLastWeekDepartures()
	{
		// find out last week's start and end date.
		$s_date = strtotime(date('Y-m-d'));
	    $s_date = strtotime("-7 day", $s_date);
	    $start_date = date('Y-m-d', $s_date);
	    $e_date = strtotime(date('Y-m-d'));
	    $e_date = strtotime("-1 day", $e_date);
	    $end_date = date('Y-m-d', $e_date);
		
		// now call the model function to search the departures between the given dates.
		$object = icomModel::getLastWeekDeps($start_date, $end_date);//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'job',
							'office',
							'travel_date',
							'country',
							'return_date',
							'statement_number',
							'chart_number',
							'comment'
							)
				->addColumn('travel_date', function($option){
					if(isMiladiDate()){
						return $option->travel_date;
					}else{
						$travel_date = dmy_format(toJalali($option->travel_date));
						return $travel_date;
					}
				})
				->addColumn('return_date', function($option){
					if(isMiladiDate()){
						return $option->return_date;
					}else{
						$return_date = dmy_format(toJalali($option->return_date));
						return $return_date;
					}
				})
				->make();
	}

	//get form data
	public function getFilteredDepartures()
	{
		
		//check the date type if it's shamsi or miladi.
		if(isMiladiDate()){
			$start_date = Input::get('start_date');
			$end_date = Input::get('end_date');
		}else{
			$start_date = toGregorian(ymd_format(Input::get('start_date')));
			$end_date = toGregorian(ymd_format(Input::get('end_date')));
		}
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		//echo $data['start_date'];exit;
		//load view to show search result
		return View::make('icom.depSearchResult', $data);
	}

	// get the result of the search for data tables;
	public function getFilteredDeparturesData()
	{

		$object = icomModel::getFilteredResultData();
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'job',
							'office',
							'travel_date',
							'country',
							'return_date',
							'statement_number',
							'chart_number',
							'comment'
							)
				->addColumn('return_date', function($option){
					if(isMiladiDate()){
						return $option->return_date;
					}else{
						$return_date = dmy_format(toJalali($option->return_date));
						return $return_date;
					}
				})
				->make();
	}

	// load the filtering page.
	public function loadReturnFilterPage()
	{
		return View::make('icom.returnFiltering');
	}

	// get last week's returns.
	public function getLastWeekReturns()
	{
		// find out last week's start and end date.
		$s_date = strtotime(date('Y-m-d'));
	    $s_date = strtotime("-7 day", $s_date);
	    $start_date = date('Y-m-d', $s_date);
	    $e_date = strtotime(date('Y-m-d'));
	    $e_date = strtotime("-1 day", $e_date);
	    $end_date = date('Y-m-d', $e_date);
		
		// now call the model function to search the returns between the given dates.
		$object = icomModel::getLastWeekReturns($start_date, $end_date);//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'job',
							'office',
							'return_date',
							'return_country',
							'passport_number',
							'airline_name',
							'email'							
							)
				->addColumn('return_date', function($option){
					if(isMiladiDate()){
						return $option->return_date;
					}else{
						$return_date = dmy_format(toJalali($option->return_date));
						return $return_date;
					}
				})
				->make();
	}

	//get form data
	public function getFilteredReturns()
	{
		
		//check the date type if it's shamsi or miladi.
		if(isMiladiDate()){
			$start_date = Input::get('start_date');
			$end_date = Input::get('end_date');
		}else{
			$start_date = toGregorian(ymd_format(Input::get('start_date')));
			$end_date = toGregorian(ymd_format(Input::get('end_date')));
		}
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		//echo $data['start_date'];exit;
		//load view to show search result
		return View::make('icom.returnSearchResult', $data);
	}

	// get the result of the search for data tables;
	public function getFilteredReturnsData()
	{

		$object = icomModel::getReturnFilteredResultData();
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'job',
							'office',
							'return_date',
							'return_country',
							'passport_number',
							'airline_name',
							'email'
							)
				->addColumn('return_date', function($option){
					if(isMiladiDate()){
						return $option->return_date;
					}else{
						$return_date = dmy_format(toJalali($option->return_date));
						return $return_date;
					}
				})
				->make();
	}

	// load the airport checkup page.
	public function loadAirportCheckupPage()
	{
		return View::make('icom.airportCheckUp');
	}

	// get current day's VIP travels list for airport users.
	public function getTravelsList()
	{

		$object = icomModel::getTravelsData();
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'travel_date',
							'return_date',
							'statement_number',
							'chart_number'
							)
				->addColumn('travel_date', function($option){
					if(isMiladiDate()){
						return $option->travel_date;
					}else{
						$travel_date = dmy_format(toJalali($option->travel_date));
						return $travel_date;
					}
				})
				->addColumn('return_date', function($option){
					if(isMiladiDate()){
						return $option->return_date;
					}else{
						$return_date = dmy_format(toJalali($option->return_date));
						return $return_date;
					}
				})
				->addColumn('operation',function($option){
			    	$options = '';
					if(!isset($option->comment))
					{
						$options .= '<a href="'.route('getLoadCommentPage',$option->id).'" align="center" class="fa fa-plus">Add Comment</a>';
					}
					else
					{
						$options .= '<a href="'.route('viewComment',$option->id).'" align="center" class="fa fa-eye">View Comment</a>';
					}
					return $options;
			    })
				// ->orderColumns('id','id')
				->make();
	}

	// load the comments page by airport checup.
	public function loadCommentPage($id = 0)
	{
		return View::make('icom.depComments')->with('id', $id);
	}

	// now add the comments and attachments to the particular departure.
	public function addComments($id = 0)
	{
		// getting all of the post data
		$files = Input::file('files');
		//print_r($files);exit;
		$errors = "";
		$file_data = array();

		if(Input::hasFile('files'))
		{
			
			foreach($files as $file) 
			{
			
			    // validating each file.
			    $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			    $validator = Validator::make(

			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension())
			        ],
			        [
			            'file' => 'required|max:10000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			  	);

			  	if($validator->passes())
			  	{
			  	
				    // path is root/uploads
				    $destinationPath = 'documents/icom_attachments';
				    $filename = $file->getClientOriginalName();

				    $temp = explode(".", $filename);
				    $extension = end($temp);
				    $lastFileId = getLastDocId();
				    
				    $lastFileId++;
				    
				    $filename = $temp[0].'_'.$lastFileId.'.'.$extension;

				    $upload_success = $file->move($destinationPath, $filename);

				    if($upload_success) 
				    {
					   
					    $data = array(
					   					'file_name'	=> $filename,
					   					'dep_id'	=> $id,
					   					'user_id'	=> Auth::user()->id
					   				);
					    DB::connection('icom')->table('attachments')->insert($data);

					    
			    
					} 
					else 
					{
					    // redirect back with errors.
						return Redirect::back()->withErrors($validator);
					}
				}
				else
				{
					// redirect back with errors.
					return Redirect::back()->withErrors($validator);
				}

			}
			//return Redirect::route("getLoadAirportPage")->with("success","Successful"); 
		}

		$data = array(
	   					'comment' 	=> Input::get('comment'),
	   					'dep_id'	=> $id,
	   					'user_id'	=> Auth::user()->id
	   				);
		$object = DB::connection('icom')->table('comments')->insert($data);
		if($object)
		{
			return Redirect::route("getLoadAirportPage")->with("success","Successful");
		}
		else
		{
			return Redirect::route("getLoadAirportPage")->with("fail","An error occured plase try again or contact system developer.");
		}       	

	}

	// load the view page nd display the comments on a particular departure.
	public function viewDepComment($id = 0)
	{
		//get the Departure based on id
		$data['deps'] = icomModel::getSpecificDep($id);
		$data['depComment'] = icomModel::getDepComment($id);
		$data['depAttachments'] = icomModel::getDepAttachments($id);

		return View::make('icom.viewDepComment', $data);
	}

	// download the uploaded file of the particular departure.
	public function downloadIComFile($id=0)
	{
	
		//get file name from database
		$fileName = icomModel::getIComFileName($id);
        $file = "http://aop.gov.af/travel/uploads/".$fileName; 

		header("Content-Description: File Transfer"); 
		header("Content-Type: application/octet-stream"); 
		header("Content-Disposition: attachment; filename=\"$file\""); 

		readfile ($file);

		//public path for file
		//$file= file_get_contents("http://aop.gov.af/uploads/".$fileName);
		//download file

		//return Response::download($file, $fileName);
	}

	// approve the particular return based on its id.
	public function approveReturn()
	{
		$id = Input::get('return_id');
		$data = array('status' => 'Approved');
		$object = icomModel::approveReturn($id, $data);
		if($object)
		{
			echo "<span style='color: green;text-align:center;display:block;font-weight:bold;'>Approved</span>";
		}
		else
		{
			echo "<span style='color:red;text-align:center;display:block;font-weight:bold;'>Not approved</span>";
		}
	}

	// load approved returns form
	public function loadApprovedReturnForm()
	{
		return View::make('icom.approvedReturnList');
	}

	//get approved returns data
	public function getApprovedReturnsData()
	{

		//get approved returns data 
		$object = icomModel::getApprovedReturns();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
					->showColumns(
						'id',
						'name',
						'job',
						'office',
						'return_date',
						'return_country',
						'passport_number',
						'airline_name',
						'email'
						)
				->addColumn('return_date', function($option){
					if(isMiladiDate()){
						return $option->return_date;
					}else{
						$return_date = dmy_format(toJalali($option->return_date));
						return $return_date;
					}
				})
				->make();
		
	}

}

?>