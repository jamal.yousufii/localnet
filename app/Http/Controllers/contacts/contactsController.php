<?php 

namespace App\Http\Controllers\contacts;

use App\Http\Controllers\Controller;
use App\models\contacts\contactsModel;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Session;

class contactsController extends Controller
{

	//database connection
	public static $myDb = "hr";

	//Load documents form entry view
	public function getAllContacts()
	{
		//if(Auth::guest()){return View('user.login');}
		$data['parentDeps'] = getDepartmentWhereIn();
		return View::make('contacts.contacts_list', $data);
	}

	//get form data list
	public function getContactsData()
	{

		//get all data 
		$object = contactsModel::getData();//echo "<pre>";print_r($object);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							//'department',
							'name',
							'current_position_dr',
							'ext2',
							'ext1',
							'ext3'							
						)
				->addColumn('operations', function($option){
					if(canEdit('cons_list')){
						return '<a href="'.route('loadEdit',$option->id).'" target="_blank">Update</a>';
					}
					
				})
				// ->orderColumns('id','id')
				->make();
		
	}

	//update form
	public function loadEditPage($id = 0)
	{
		if(canEdit('cons_list'))
		{
			$object = contactsModel::getContacts($id);
			//echo "<pre>";print_r($object);exit;
			return View::make('contacts.editList')->with('contacts', $object);
		}
		else
		{
			return showWarning();
		}
	}

	//update form
	public function updateContacts($id = 0)
	{
		// update 
    	$data = array(

    					//'phone' => Input::get('phone'),
    			 		//'email' => Input::get('email'),
    					'ext1' => Input::get('zemins'),
    					'ext2' => Input::get('cisco'),
    			 		'ext3' => Input::get('fax')
    			 	);

    	$object = contactsModel::updateData($id, $data);

    	if($object)
    	{
			return Redirect::route("arg.net")->with("success","موفقانه اصلاح گردید");
		}
		else
		{
			return Redirect::route("arg.net")->with("fail","تغییرات آورده نشد !");
		}
	}

	//bring related department
	public function bringSubDepartments()
	{

		$deps = getRelatedSubDepartment(Input::get('dep_id'));

		$options = '<select name="sub_dep" id="sub_dep" class="form-control">';
		$options .= '<option value="">انتخاب اداره فرعی</option>';
		foreach($deps AS $item)
		{
			$options.="<option value='".$item->id."'>".$item->name."</option>";
		}
		$options.="</select>";

		return $options;
	}

	//get search result
	public function getSearchResult()
	{
		$data['general_dep'] = Input::get('general_dep');
		$data['sub_dep'] = Input::get('sub_dep');
		//load view to show search result
		return View::make('contacts.search_result', $data);
	}

	// get the result of the search for data tables;
	public function getSearchResultData()
	{

		$object = contactsModel::getSearchResult();
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							//'department',
							'name',
							'current_position_dr',
							'ext2',
							'ext1',
							'ext3',
							'phone'
							)
				->addColumn('operations', function($option){
					if(canEdit('cons_list')){
					return '<a href="'.route('loadEdit',$option->id).'" target="_blank">Update</a>';
					}
				})
				// ->orderColumns('id','id')
				->make();
	}

}

?>