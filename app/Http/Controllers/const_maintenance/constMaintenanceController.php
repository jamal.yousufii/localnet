<?php 

namespace App\Http\Controllers\const_maintenance;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\models\const_maintenance\const_maintenance_model;
use Illuminate\Support\Collection;
use App\library\jdatetime;
use View;
use DB;
use Validator;
use Input;
use Redirect;
use Response;
use Datatable;
use Session;
use DateTime;
use DateInterval;
use File;
use Illuminate\Support\Str;
use URL;
use Carbon\Carbon;

class constMaintenanceController extends Controller
{
//================================================= Estate Registration Controller Functions ======================================
	//Load management form
	public function loadEstateRegistryForm()
	{
		return View::make('const_maintenance.estate_registry.insert');
	}
	// Add Inventory form data.
	public function addEstateRegistryFormData(Request $request)
	{
		//dd($_FILES);
		//validate the input fields
	    $this->validate($request,["code" => "required|unique:construction_maintenance.locations,code"]);
	    
	    //check the date type if it's shamsi or miladi.
		if(isMiladiDate()){
			$date = Input::get('date');
		}else{
			$date = toGregorian(ymd_format(Input::get('date')));
		}

	    // get the form data.
	    $data = array(
	    		"code"						=> Input::get('code'),
	    		"location"					=> Input::get('location'),
	    		"agriculture_verdancy"		=> Input::get('agriculture_verdancy'),
	    		"general_information"		=> Input::get('general_information'),
	    		"date"						=> $date,
	    		"user_id"					=> Auth::user()->id,
	    		"created_at"				=> date('Y-m-d H:i:s')
	    	);
	    $insertedRecordID = const_maintenance_model::addRecord("locations",$data);
	    if($insertedRecordID){
	    	// getting all of the post data
			$files = Input::file('files');
			//print_r($files);exit;
			$errors = "";
			$auto = 1;
			$file_data = array();

			if(Input::hasFile('files'))
			{				
				foreach($files as $file) 
				{
				    // validating each file.
				    $validator = Validator::make(
				  		[
				            'file' => $file,
				            'extension'  => Str::lower($file->getClientOriginalExtension())
				        ],
				        [
				            'file' => 'required|max:10000|image|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
				        ]
				  	);
				  	if($validator->passes())
				  	{
					    // path is root/uploads
					    $destinationPath = 'documents/const_maintenance_attachments';
					    $original_filename = $file->getClientOriginalName();
					    $temp = explode(".", $original_filename);
				    	$extension = end($temp);
					    			  			    
				    	$lastFileId = $insertedRecordID;
						if($auto == 1)
					    $filename = "Attachment_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
						else
						$filename = "Attachment_".$lastFileId."_".$auto.".".$extension;
						
					    $upload_success = $file->move($destinationPath, $filename);

					    if($upload_success) 
					    {
						    $data = array(
					   					'file_name'				=> $filename,
					   					'original_file_name'	=> $original_filename,
					   					'location_id'			=> $insertedRecordID,
					   					'user_id'				=> Auth::user()->id,
					   					"created_at"			=> date('Y-m-d H:i:s')
					   				);
					    	//call the model function to insert the data into upload table.
					    	const_maintenance_model::uploadFiles($data);
						} 
						else 
						{
						    // redirect back with errors.
				    		return Redirect::route('getEstateRegistryForm')->withErrors($validator);
						}
					}
					else
					{
						// redirect back with errors.
				    	return Redirect::route('getEstateRegistryForm')->withErrors($validator);
					}
					$auto ++;
				}
			}
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'locations',
				'record_id'=>$insertedRecordID,
				'user' => Auth::user()->username,
				'action' => "Added",
				'created_at' => date('Y-m-d H:i:s')
				);
			const_maintenance_model::addLog($Log);
	    	return Redirect::route("getEstateRegistryList")->with("success","Information successfully Added.");
        }
        else
        {
            return Redirect::route("getEstateRegistryList")->with("fail","An error occured please try again or contact system developer.");
        }
	}
	// get load the list of added locations.
	public function loadEstateRegistryList()
	{
		$data['records'] = const_maintenance_model::getEstateRegistryData();
		if(Input::get('ajax') == 1)
		{
			return View::make('const_maintenance.estate_registry.list_ajax',$data);
		}
		else
		{
			//load view to show search result
			return View::make('const_maintenance.estate_registry.list', $data);
		}
	}
	//loading Estate Registry edit page.
	public function loadEditPage($id=0)
	{
		$data['record'] = const_maintenance_model::getSpecificRecordDetails($id);
		//dd($data['inventory']);
		return View::make('const_maintenance.estate_registry.edit', $data);	
	}
	// updating the changes.
	public function updateEstateRegistryRecord(Request $request,$id)
	{
		//dd($_POST);
		//validate the input fields
	    $this->validate($request,[
	    	"code" 					=> "required"
	        ]
	    );
	    //check the date type if it's shamsi or miladi.
		if(isMiladiDate()){
			$date = Input::get('date');
		}else{
			$date = toGregorian(ymd_format(Input::get('date')));
		}

	    // get the form data.
	    $data = array(
	    		"code"						=> Input::get('code'),
	    		"location"					=> Input::get('location'),
	    		"agriculture_verdancy"		=> Input::get('agriculture_verdancy'),
	    		"general_information"		=> Input::get('general_information'),
	    		"date"						=> $date,
	    		"user_id"					=> Auth::user()->id,
	    		"updated_at"				=> date('Y-m-d H:i:s')
	    	);
	    $updated = const_maintenance_model::updateRecordData($id,$data);
	    if($updated){
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'locations',
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Edited",
				'created_at' => date('Y-m-d H:i:s')
				);
			const_maintenance_model::addLog($Log);
	    	return Redirect::route("getEstateRegistryList")->with("success","Information successfully Edited.");
        }
        else
        {
            return Redirect::route("getEstateRegistryList")->with("fail","An error occured please try again or contact system developer.");
        }
		
	}
	
	function searchRecords()
	{
		//dd($_POST);
		$data['records'] = const_maintenance_model::getSearchData();
		if(Input::get('ajax') == 1)
		{
			return View::make('const_maintenance.estate_registry.searched_ajax',$data);
		}
		else
		{
			return View::make('const_maintenance.estate_registry.searched_records', $data);
		}
	}
	
	function getDetails()
	{
		$record_id = Input::get('id');
		$data['record'] = const_maintenance_model::getDetails($record_id);
		//dd($data['inventory']);
		$data['files'] = const_maintenance_model::getFiles($record_id);
		return View::make('const_maintenance.estate_registry.details_ajax',$data);
	}
	function deleteEstateRegistry()
	{
		$record_id = Input::get('record_id');
		$deleted = const_maintenance_model::deleteEstateRegistryRecord($record_id);
		if($deleted)
		{
			// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'locations',
				'record_id'=>$record_id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at' => date('Y-m-d H:i:s')
				);
			const_maintenance_model::addLog($Log);
			const_maintenance_model::deleteAttachment($record_id);
		}
	}
}

?>