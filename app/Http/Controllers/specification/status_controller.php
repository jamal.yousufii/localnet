<?php 
namespace App\Http\Controllers\specification;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\specification\product;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;

use Response;

class status_controller extends Controller {

	public function show_form($id){
		$product=product::find($id);

		return view::make('specification.status_page',['product'=>$product]);

	}

	public function update_status($id){

		$product=product::find($id);
		$product->status=Input::get('status');
		$product->status_description=Input::get('status_description');
		$product->status_date= toGregorian(gregorian_format(Input::get('status_date')));

			if($product->save())
        {
            return \Redirect::route("recordsList")->with("success","You have successfuly Examination the product <span style='color:red;font_weight:bold;'></span>");


        }
        else
        {
            return \Redirect::route("select_row")->with("fail","An error occured please try again.");
        }
	}

	public function reject_accept($id){

		$product=product::find($id);

		return view::make('specification.update_status_reject_accept',['product'=>$product]); 
	}

	public function update_status_re($id){

		$product=product::find($id);

		$product->status=Input::get('status');


			if($product->save())
        {
            return \Redirect::route("recordsList")->with("success","You have successfuly update the product <span style='color:red;font_weight:bold;'></span>");


        }
        else
        {
            return \Redirect::route("select_row")->with("fail","An error occured please try again.");
        }


	}
	}

?>