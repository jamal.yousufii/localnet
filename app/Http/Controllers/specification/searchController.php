<?php 
namespace App\Http\Controllers\specification;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\specification\product;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;

use Response;

class searchController extends Controller {

	public function search_feceno(){

		$search_string = trim(Input::get('search_string'));

		$date=explode('-', $search_string);
			if(count($date)==3){
		$search_string= toGregorian(gregorian_format(Input::get('search_string')));
		$rows=\DB::table('specification.feceno as fe')->select('fe.*','dept.name as dept_name')->leftjoin('specification.department AS dept','fe.department_id','=','dept.id')
		->whereRaw("fe.fe_status =0 AND (fe.date like '%".$search_string."%')")
		->orderby('fe.id','desc')
		->get();
		$data['rows'] = $rows;
		return view::make('specification.serach_feceno',$data);	
		}

		
		$rows =$rows=\DB::table('specification.feceno as fe')->select('fe.*','dept.name as dept_name')->leftjoin('specification.department AS dept','fe.department_id','=','dept.id')
		->whereRaw("fe.fe_status =0 AND (fe.number like '%".$search_string."%'
			or
			fe.description like '%".$search_string."%'
			or
			dept.name like '%".$search_string."%'
		)")

		->orderby('fe.id','desc')
		->get();
		$data['rows'] = $rows;
		return view::make('specification.serach_feceno',$data);	
	}

	public function search_feceno_reject(){

		$search_string = trim(Input::get('search_string_r'));
		$date=explode('-', $search_string);
			if(count($date)==3){
		$search_string= toGregorian(gregorian_format(Input::get('search_string_r')));
		$rows=\DB::table('specification.feceno as fe')->select('fe.*','dept.name as dept_name')->leftjoin('specification.department AS dept','fe.department_id','=','dept.id')
		->whereRaw("fe.fe_status =1 AND (fe.date like '%".$search_string."%')")
		->get();
		$data['rows'] = $rows;
		return view::make('specification.serach_feceno',$data);	
		}

		
		$rows =$rows=\DB::table('specification.feceno as fe')->select('fe.*','dept.name as dept_name')->leftjoin('specification.department AS dept','fe.department_id','=','dept.id')
			->whereRaw("fe.fe_status =1 AND (fe.number like '%".$search_string."%'
			or
			fe.description like '%".$search_string."%'
			or
			dept.name like '%".$search_string."%'
		)")
		->orderby('fe.id','desc')
		->get();
		$data['rows'] = $rows;
		return view::make('specification.serach_feceno',$data);	

	}
	public function searchProduct(){

			$search_string2=trim(Input::get('search_string2'));
			$date=explode('-', $search_string2);
			if(count($date)==3){
			$search_string2= toGregorian(gregorian_format(Input::get('search_string2')));
			$rows=\DB::table('specification.products')
			->select('products.*','dept.name','feceno.number')
			->leftjoin('specification.feceno','products.feceno_id','=','feceno.id')
			->leftjoin('specification.department as dept','products.department_id','=','dept.id')
			->whereRaw("status = 0 AND (products.date like '%".$search_string2."%')")
			->get();
				$data['rows'] = $rows;
				return view::make('specification.search_product',$data);
					}

		$rows=\DB::table('specification.products')
		->select('products.*','dept.name','feceno.number')
		->leftjoin('specification.feceno','products.feceno_id','=','feceno.id')
		->leftjoin('specification.department as dept','products.department_id','=','dept.id')
			  ->whereRaw(
			    "status = 0 AND (product_name like '%".$search_string2."%' 
		   		OR moshakhasat_id like '%".$search_string2."%' OR guarantee like '%".$search_string2."%'
			    OR product_description like '%".$search_string2."%' OR number_device like '%".$search_string2."%' OR unit like '%".$search_string2."%'
			    OR feceno.number like '%".$search_string2."%'
			    OR dept.name like '%".$search_string2."%'
			    )")->get();
 
				$data['rows'] = $rows;
				return view::make('specification.search_product',$data);

	}
			public function search_approve(){
			
			$search_string3=trim(Input::get('search_string3'));
			$date = explode('-', $search_string3);
			if(count($date)==3)
			{
				$search_string3= toGregorian(gregorian_format(Input::get('search_string3')));
					$rows=\DB::table('specification.products')
					->select('products.*','dept.name','feceno.number')
					->leftjoin('specification.feceno','products.feceno_id','=','feceno.id')
					->leftjoin('specification.department as dept','products.department_id','=','dept.id')
					->whereRaw("status = 1 AND (products.date like '%".$search_string3."%')")
					->get();

					$data['rows'] = $rows;
						
				return view::make('specification.search_approve_product',$data);
			}

			$rows=\DB::table('specification.products')
			->select('products.*','dept.name','feceno.number')
			->leftjoin('specification.feceno','products.feceno_id','=','feceno.id')
			->leftjoin('specification.department as dept','products.department_id','=','dept.id')
			->whereRaw(
					"status = 1 AND (product_name like '%".$search_string3."%' 
					OR moshakhasat_id like '%".$search_string3."%' OR guarantee like '%".$search_string3."%'
					OR product_description like '%".$search_string3."%' OR number_device like '%".$search_string3."%' OR unit like '%".$search_string3."%'
				    OR feceno.number like '%".$search_string3."%'
					OR dept.name like '%".$search_string3."%'
					 	)")->get();

				$data['rows'] = $rows;

	return view::make('specification.search_approve_product',$data);

		
	}
	
	public function search_reject(){
			
			
			$search_string4=trim(Input::get('search_string4'));
			
			$date = explode('-', $search_string4);
			if(count($date)==3)
			{
				$search_string4= toGregorian(gregorian_format(Input::get('search_string4')));
					$rows=\DB::table('specification.products')
					->select('products.*','dept.name','feceno.number')
					->leftjoin('specification.feceno','products.feceno_id','=','feceno.id')
					->leftjoin('specification.department as dept','products.department_id','=','dept.id')
					->whereRaw("status = 2 AND (products.date like '%".$search_string4."%' OR 	status_date like '%".$search_string4."%')")
					->get();

					$data['rows'] = $rows;
						
				return view::make('specification.search_reject',$data);
			}

			$rows=\DB::table('specification.products')
				->select('products.*','dept.name','feceno.number')->leftjoin('specification.feceno','products.feceno_id','=','feceno.id')->leftjoin('specification.department as dept','products.department_id','=','dept.id')
				->whereRaw(
					"status = 2 AND (product_name like '%".$search_string4."%' 
					OR moshakhasat_id like '%".$search_string4."%' OR guarantee like '%".$search_string4."%'
					OR product_description like '%".$search_string4."%' OR number_device like '%".$search_string4."%' OR unit like '%".$search_string4."%'
				    OR feceno.number like '%".$search_string4."%'
				    OR dept.name like '%".$search_string4."%'

					  )")->get();

				  $data['rows'] = $rows;

				return view::make('specification.search_reject',$data);

		
	
	}

}

?>