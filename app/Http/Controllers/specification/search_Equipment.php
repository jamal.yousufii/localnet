<?php 
namespace App\Http\Controllers\specification;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\specification\main_model;
use App\models\specification\employes_equipment_model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;

use Response;

class search_Equipment extends Controller {

	public function search_destribut_equipment(){

		$search_string = trim(Input::get('search_string'));


			$rows=\DB::table('hr.employees as emp')
			->select('emp.name_dr AS em_name','emp.father_name_dr AS last_name','dept.name AS dep_name','cat.*','equip.*','emp_equip.id AS em_eq_id')
			->leftjoin('specification.department as dept','dept.id','=','emp.department')
			->leftjoin('specification.employes_equipment as emp_equip','emp_equip.employes_id','=','emp.id')
			->leftjoin('specification.equipments as equip','emp_equip.equipment_id','=','equip.id')
			->leftjoin('specification.categories as cat','equip.categories_id','=','cat.id')

			->whereRaw("emp_equip.status= 0 AND emp_equip.delete_status=0 AND
				(emp.name_dr like '%".$search_string."%' 
				OR emp.father_name_dr like '%".$search_string."%' 
				OR dept.name like '%".$search_string."%'
			    OR cat.name like '%".$search_string."%' 
			    OR model like '%".$search_string."%' 
			    OR serial_number like '%".$search_string."%'
			    )")->get();
				
				$data['rows'] = $rows;
				return view::make('specification.search_destribute',$data);

	}
	public function search_Equipment(){

			$search_string2 = trim(Input::get('search_string2'));

			$rows=\DB::connection('specification')->table('equipments','categories.name')
			->select('equipments.*','categories.name')
			->leftjoin('categories','equipments.categories_id','=','categories.id')
			->where('name', 'like', '%'.$search_string2.'%')
			->orwhere('model','like','%'.$search_string2.'%')
			->orwhere('serial_number','like','%'.$search_string2.'%')
			->orwhere('equipments.description','like','%'.$search_string2.'%')
			->get();

			$data['rows'] = $rows;
			return view::make('specification.search_equipment',$data);


	}
	public function search_given_eq(){

			$search_string3 = trim(Input::get('search_string3'));

			$rows=\DB::table('hr.employees as emp')
			->select('emp.name_dr AS em_name','emp.father_name_dr AS last_name','dept.name AS dep_name','cat.*','equip.*','emp_equip.id AS em_eq_id')
			->leftjoin('specification.department as dept','dept.id','=','emp.department')
			->leftjoin('specification.employes_equipment as emp_equip','emp_equip.employes_id','=','emp.id')
			->leftjoin('specification.equipments as equip','emp_equip.equipment_id','=','equip.id')
			->leftjoin('specification.categories as cat','equip.categories_id','=','cat.id')

			->whereRaw("emp_equip.status= 1 AND emp_equip.delete_status=0 AND
				(emp.name_dr like '%".$search_string3."%' 
				OR emp.father_name_dr like '%".$search_string3."%' 
				OR dept.name like '%".$search_string3."%'
			    OR cat.name like '%".$search_string3."%' 
			    OR model like '%".$search_string3."%' 
			    OR serial_number like '%".$search_string3."%'
			    )")->get();
				
				$data['rows'] = $rows;
				return view::make('specification.search_geven_equipment',$data);


	}
	public function search_delete(){

			$search_string4= trim(Input::get('search_string4'));

			$rows=\DB::table('hr.employees as emp')
			->select('emp.name_dr AS em_name','emp.father_name_dr AS last_name','dept.name AS dep_name','cat.*','equip.*','emp_equip.id AS em_eq_id')
			->leftjoin('specification.department as dept','dept.id','=','emp.department')
			->leftjoin('specification.employes_equipment as emp_equip','emp_equip.employes_id','=','emp.id')
			->leftjoin('specification.equipments as equip','emp_equip.equipment_id','=','equip.id')
			->leftjoin('specification.categories as cat','equip.categories_id','=','cat.id')

			->whereRaw("emp_equip.status= 1 AND emp_equip.delete_status=1 AND
				(emp.name_dr like '%".$search_string4."%' 
				OR emp.father_name_dr like '%".$search_string4."%' 
				OR dept.name like '%".$search_string4."%'
			    OR cat.name like '%".$search_string4."%' 
			    OR model like '%".$search_string4."%' 
			    OR serial_number like '%".$search_string4."%'
			    )")->get();

				$data['rows'] = $rows;
				return view::make('specification.search_deleted',$data);



	}

}

?>