<?php 
namespace App\Http\Controllers\specification;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;

use App\models\specification\product;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;

use Response;

class productController extends Controller {

	public function insert_page(){

		$feceno=product::get_feceno();
		$department=product::get_department();

		return view::make('specification.insert_product',array('feceno'=>$feceno,'department'=>$department));
	}
	public function insert_row(Request $Request){



			$product=new product;

			$product->product_name=Input::get('product_name');
			$product->moshakhasat_id=Input::get('moshakhasat_id');
			$product->date= toGregorian(gregorian_format(Input::get('date')));
			$product->product_description=Input::get('product_description');
			$product->number_device=Input::get('number_device');
			$product->unit=Input::get('unit');
			$product->guarantee=Input::get('guarantee');
			$product->feceno_id=Input::get('feceno_id');
			$product->department_id=Input::get('department_id');
			$product->created_by=Auth::user()->id;

		
		if($product->save())
        {
            return \Redirect::route("insert_pro")->with("success","You have successfuly inserted the record <span style='color:red;font_weight:bold;'></span>");


        }
        else
        {
            return \Redirect::route("insert_pro")->with("fail","An error occured please try again.");
        }
		

		
	}
	public function delete($id){
		$product=product::find($id);
		$product->delete();
		if($product)
        {
            return \Redirect::route("recordsList")->with("success","You have successfuly Remove the record <span style='color:red;font_weight:bold;'></span>");
        }
        else
        {
            return \Redirect::route("recordsList")->with("fail","An error occured please try again.");
        }
	
		

	}

	public function selectRow($id){

		$product=product::find($id);
		$feceno=product::get_feceno();
		$department=product::get_department();

		return view::make('specification.update_product',array('product'=>$product,'feceno'=>$feceno,'department'=>$department));
	}

	public function update($id){

			$product=new product;
			$product=product::find($id);

			$product->product_name=Input::get('product_name');
			$product->moshakhasat_id=Input::get('moshakhasat_id');
			$product->date= toGregorian(gregorian_format(Input::get('date')));
			$product->product_description=Input::get('product_description');
			$product->number_device=Input::get('number_device');
			$product->unit=Input::get('unit');
			$product->guarantee=Input::get('guarantee');
			$product->feceno_id=Input::get('feceno_id');
			$product->department_id=Input::get('department_id');
			$product->updated_by=Auth::user()->id;



			if($product->save())
        {
            return \Redirect::route("recordsList")->with("success","You have successfuly updated the record <span style='color:red;font_weight:bold;'></span>");


        }
        else
        {
            return \Redirect::route("select_row")->with("fail","An error occured please try again.");
        }
		
	}

}

?>