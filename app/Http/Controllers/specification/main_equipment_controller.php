<?php 
namespace App\Http\Controllers\specification;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\specification\main_model;
use App\models\specification\employes_equipment_model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Validator;
use Response;
use File;

class main_equipment_controller extends Controller {


	public function load_equipment(){

		$first_list=main_model::get_all_distribute_equipment();
		$secand_list=main_model::list_of_all_equipment();
		$third=main_model::get_all_distribute_equipment_not_releted();
		$fourth=main_model::list_of_all_categories();
		$delet_list=main_model::get_all_distribute_equipment_delete();


		
		return view::make('specification.Equipment_page',array('first_list' => $first_list,'secand_list'=>$secand_list ,'third'=>$third ,'fourth'=>$fourth,'delet_list'=>$delet_list));
	}
	public function distribute_equipment(){
		$equipment=main_model::get_equipment();
		$employee=main_model::get_employee();
		return view::make('specification.distribute_equipment',array('equipment'=>$equipment,'employee'=>$employee));

	}
	public function insert_rows(){
		$row=new main_model;
		$employes_equipment=new employes_equipment_model;
		$employes_equipment->employes_id=Input::get('employes_id');
		$employes_equipment->equipment_id=Input::get('equipment_id');
		$used_id=Input::get('equipment_id');
		$row=main_model::find($used_id);
		$row->used='1';
		$row->save();


			if($employes_equipment->save())
        {
            return \Redirect::route("load_insert")->with("success","You have successfuly inserted the record <span style='color:red;font_weight:bold;'></span>");


        }
        else
        {
            return \Redirect::route("load_insert")->with("fail","An error occured please try again.");
        }
	}

	public function delete_destribut($id){
		$row=new main_model;
		$employes_equipment=new employes_equipment_model;
		$row=employes_equipment_model::find($id);
		//$rows=main_model::find($row->equipment_id);
		$row->status='1';
		
		$row->delete_status='1';
			$row->save();
		
		if($row)
        {
            return \Redirect::route("load_equipment")->with("success","You have successfuly Remove the record <span style='color:red;font_weight:bold;'></span>");
        }
        else
        {
            return \Redirect::route("load_equipment")->with("fail","An error occured please try again.");
        }

	}
	public function updateDastribut($id){
	$row=new main_model;
	$employes_equipment=employes_equipment_model::find($id);			
	$equipment_select_id=$employes_equipment->equipment_id;

	/**
	$employes_equipment=employes_equipment_model::find($id);			
	$employes_equipment->employes_id=Input::get('employes_id');
	$employes_equipment->equipment_id=Input::get('equipment_id');**/	
	$employes_equipment->employes_id=Input::get('employes_id');

	$employes_equipment->status=Input::get('status');

	$row=main_model::find($equipment_select_id);

	if(Input::get('status')=='1'){		

	$row->used='0';

	$row->save();

	}else{

	$row->used='1';

	$row->save();
	}	
	if($employes_equipment->save())
        {
            return \Redirect::route("load_equipment")->with("success","You have successfuly updated the record <span style='color:red;font_weight:bold;'></span>");


        }
        else
        {
            return \Redirect::route("select_update")->with("fail","An error occured please try again.");
        }
	}
	public function select_row_destribut($id){

		$equipment=main_model::get_equipment_dest();
		$employee=main_model::get_employee();
		$employes_equipment=employes_equipment_model::find($id);			
		return view::make('specification.update_destribut_equipment',array('equipment'=>$equipment,'employee'=>$employee,'employes_equipment'=>$employes_equipment));

	}




}