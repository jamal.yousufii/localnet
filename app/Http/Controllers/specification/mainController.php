<?php
namespace App\Http\Controllers\specification;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;

use App\models\specification\feceno;
use App\models\specification\department;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Validator;
use Response;
use File;

class mainController extends Controller {

	public function loadRecordsList(){

		$feceno_data=feceno::get_feceno();
		$feceno_data_re=feceno::get_feceno_reject();
		$product=feceno::select_product();
		$ap_products=feceno::approve_products();
		$reject_products=feceno::reject_products();
		return view::make('specification.specification_list',array('feceno_data'=> $feceno_data,'product'=> $product,'ap_products'=>$ap_products,'reject_products'=>$reject_products,'feceno_data_re'=>$feceno_data_re));
	}
	public function loadpage(){


		$department=department::get_department();



		return view::make('specification.insert_f_c_9',array('department' =>$department));
	}
	public function dataRange(){

		$date=input::get('form_date');

		 $from_date = getCurrentJalaliYearStartDateMaliYear($date-1);
		 $to_date = getCurrentJalaliYearEndDateMaliYear($date);

		$data=\DB::table('specification.data_range')->update(
				    ['from_date' => $from_date, 'to_date' => $to_date,'year'=>$date]
				);

 		return Redirect("/specification/specificationList");

	}


	public function insert_f_c_9(Request $request){


		// getting all of the post data
		$file = Input::file('file');
		if(Input::hasFile('file'))
		{
		  // validating each file.
		  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
		  $validator = Validator::make(

		  		[
		            'file' => $file,
		        ],
		        [
		            'file' => 'required|max:10000|image|mimes:jpeg,png,jpg,gif,docx,pdf'
		        ]
		  	);

			if($validator->passes())
			{

			    // path is root/uploads
			    $destinationPath = 'uploads/';
			    $original_filename = $file->getClientOriginalName();
			    $temp = explode(".", $original_filename);
		    	$extension = end($temp);

		    	$lastFileId = DB::connection('specification')->table('feceno')->orderBy('id', 'desc')->pluck('id');

		   	 	$lastFileId++;

			    $filename = "photo_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success)
			    {
				   if($date=Input::get('date'))
			$date = toGregorian(gregorian_format($date));

				   $data = array(
	   					'number'				=> Input::get('number'),
	   					'date'					=> $date,
	   					'image_fe'				=> $filename,
	   					'description'			=> Input::get('description'),
	   					'department_id'			=> Input::get('department_id'),
	   					'fe_status'			    => Input::get('fe_status'),
	   					'created_by'			=> Auth::user()->id,
	   				);

					DB::connection('specification')->table("feceno")->insert($data);

					return Redirect::route("insertForm")->with("success","Photo details successfully Uploaded.");
				}
				else
				{
					// send back to the page with the input data and errors
					return Redirect::route('insertForm')->withInput($validator)->withErrors();
				}

			}
			else
			{
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			}

		}


	}

	public function delete($id){

	$row=feceno::find($id);

	  $image = \DB::table('feceno')->where('id', $id)->first();
        $file= $image->image_fe;
        $filename = public_path().'/uploads/'.$file;
        \File::delete($filename);

	$row->delete();

		if($row)
        {
            return \Redirect::route("recordsList")->with("success","You have successfuly deleted the record <span style='color:red;font_weight:bold;'></span>");
        }
        else
        {
            return \Redirect::route("recordsList")->with("fail","An error occured please try again.");
        }

		}

		public function select_row($id){

			$data=feceno::find($id);
			$department=department::get_department();

			return view::make('specification.feceno_update',array('data'=>$data,'department'=>$department));

		}
		public function update(Request $request, $id){

			$feceno= new feceno;
			$feceno=feceno::find($id);

			$file = Input::file('file');
		if(Input::hasFile('file'))
		{
			  File::delete('uploads/'.$feceno->image_fe);
		  // validating each file.


			    // path is root/uploads
			    $destinationPath = 'uploads/';
			    $original_filename = $file->getClientOriginalName();
			    $temp = explode(".", $original_filename);
		    	$extension = end($temp);

		    	$lastFileId = $feceno->id;

		   	 //	$lastFileId++;

			    $filename = "photo_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success)
			    {

					$feceno->number=Input::get('number');
					if($date=Input::get('date'))
					$date = toGregorian(gregorian_format($date));
					$feceno->date =$date;
					$feceno->image_fe=$filename ;
					$feceno->description=Input::get('description');
					$feceno->fe_status=Input::get('fe_status');
					$feceno->department_id=Input::get('department_id');
					$feceno->updated_by=Auth::user()->id;
				}}else{

					$feceno->number=Input::get('number');
					if($date=Input::get('date'))
					$date = toGregorian(gregorian_format($date));
					$feceno->date =$date;
					$feceno->fe_status=Input::get('fe_status');
					$feceno->description=Input::get('description');
					$feceno->department_id=Input::get('department_id');
					$feceno->updated_by=Auth::user()->id;


				}



				if($feceno->save())
        {
            return \Redirect::route("recordsList")->with("success","You have successfuly Update the record <span style='color:red;font_weight:bold;'></span>");
        }
        else
        {
            return \Redirect::route("recordsList")->with("fail","An error occured please try again.");


		}}

		public function show_feceno_file($id){
			//$rows=feceno::get_feceno($id);

			$rows=DB::connection('specification')->table('feceno')->select('image_fe')->where('id',$id)->get();


			return view::make('specification.get_feceno_file',array('rows'=>$rows));



		}
		public function insertDepartment(Request $request){

			$validates = Validator::make(Input::all(), array(
			"name"						=> "required|unique:department",
			));
			if($validates->fails())
			{
				return Redirect::route('recordsList')->withErrors($validates)->withInput();
			}else{
			$data=array(
				'name'=>Input::get('name'),
				'name_en'=>Input::get('name_en'),
				'parent'=>0,
				'unactive'=>0,
				'user_id'=>Auth::user()->id
			);

			$department=\DB::connection('specification')->table("department")->insert($data);
			return Redirect::route('recordsList')->with("success","Information successfully Added.");

		}
	}



	}



?>
