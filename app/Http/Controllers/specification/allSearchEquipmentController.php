<?php 
namespace App\Http\Controllers\specification;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\specification\main_model;
use App\models\specification\employee_equipment_model;
use App\models\specification\equipment_model;
use App\models\specification\department;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Excel;
use Response;

class allSearchEquipmentController extends Controller {

	public function search_data(){
		$equipment=main_model::get_equipment_dest();
		$department=department::get_department();
		$categories=equipment_model::get_categories();
		$employee=main_model::get_employee();


		return view::make('specification.all_search_equipment',array('equipment'=>$equipment,'department'=>$department,'categories'=>$categories,'employee'=>$employee));

	}
	public function search_eq_data(Request $request){



	
		$department_id=$request->input('department_id');
		$employes_id=$request->input('employes_id');
		$categories_id=$request->input('categories_id');

			$rows=\DB::table('hr.employees as emp')
			->select('emp.name_dr AS em_name','emp.father_name_dr AS last_name','dept.name AS dep_name','cat.*','equip.*','emp_equip.id AS em_eq_id')
			->leftjoin('specification.department as dept','dept.id','=','emp.department')
			->leftjoin('specification.employes_equipment as emp_equip','emp_equip.employes_id','=','emp.id')
			->leftjoin('specification.equipments as equip','emp_equip.equipment_id','=','equip.id')
			->leftjoin('specification.categories as cat','equip.categories_id','=','cat.id')
			->whereRaw("emp_equip.status= 0 AND emp_equip.delete_status=0 AND
				(
				 	 cat.id= ".$categories_id."
				 	or 
				 	dept.id= ".$department_id."
				 	or 
				 	emp.id= ".$employes_id."
			   
			 		 )")->orderby('emp_equip.id','desc');
				$data['records']=$rows->get();
			return view('specification.search_all_equipment',$data);


	
	
	}

	public function export_eq(Request $request){

		$department_id=$request->input('department_id');
		$employes_id=$request->input('employes_id');
		$categories_id=$request->input('categories_id');

			$rows=\DB::table('hr.employees as emp')
			->select('emp.name_dr AS em_name','emp.father_name_dr AS last_name','dept.name AS dep_name','cat.*','equip.*','emp_equip.id AS em_eq_id')
			->leftjoin('specification.department as dept','dept.id','=','emp.department')
			->leftjoin('specification.employes_equipment as emp_equip','emp_equip.employes_id','=','emp.id')
			->leftjoin('specification.equipments as equip','emp_equip.equipment_id','=','equip.id')
			->leftjoin('specification.categories as cat','equip.categories_id','=','cat.id')
			->whereRaw("emp_equip.status= 0 AND emp_equip.delete_status=0 AND
				(
				 	 cat.id= ".$categories_id."
				 	or 
				 	dept.id= ".$department_id."
				 	or 
				 	emp.id= ".$employes_id."
			   
			 		 )")->orderby('emp_equip.id','desc');
				$data=$rows->get();

				$curr_date = date('Y-m-d');
		Excel::load('excel_template/sp_equipment_destribute.xlsx', function($file) use($data){
			//Excel::create('Filename', function($file) use($data){			
			$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){	
				$row = 3;
				$sheet->setFreeze('A3');
				
				foreach($data AS $item)
				{
					$sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true); 

					//$summary = preg_replace("/&nbsp;/",' ',$item->summary);

					$sheet->setHeight($row, 20);
					$sheet->setCellValue('H'.$row.'',$row-2);
					$sheet->setCellValue('G'.$row.'',$item->em_name);
					$sheet->setCellValue('F'.$row.'',$item->last_name);
					$sheet->setCellValue('E'.$row.'',$item->dep_name);
					$sheet->setCellValue('D'.$row.'',$item->name);
					$sheet->setCellValue('C'.$row.'',$item->model);
					$sheet->setCellValue('B'.$row.'',$item->serial_number);
					$sheet->setCellValue('A'.$row.'',$item->description);
				
					//$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));
					
					$row++;
				}

				$sheet->setBorder('A3:I'.($row-1).'', 'thin');
				
    		});
			
			})->export('xlsx');
	}

	//search data bye employes and print 
		public function search_eq_data_print(Request $request){



	
		$employes_id=$request->input('employes_id');

			$rows=\DB::table('hr.employees as emp')
			->select('emp.name_dr AS em_name','emp.father_name_dr AS last_name','dept.name AS dep_name','cat.*','equip.*','emp_equip.id AS em_eq_id')
			->leftjoin('specification.department as dept','dept.id','=','emp.department')
			->leftjoin('specification.employes_equipment as emp_equip','emp_equip.employes_id','=','emp.id')
			->leftjoin('specification.equipments as equip','emp_equip.equipment_id','=','equip.id')
			->leftjoin('specification.categories as cat','equip.categories_id','=','cat.id')
			->whereRaw("emp_equip.status= 0 AND emp_equip.delete_status=0 AND
				(
				 	
				 	emp.id= ".$employes_id."
			   
			 		 )")->orderby('emp_equip.id','desc');
				$data['records']=$rows->get();
			return view('specification.search_all_equipment',$data);


	
	
	}
	public function print_empolyes_product(){

			$employes_id=input::get('employes_id');

				$rows=\DB::table('hr.employees as emp')
			->select('emp.name_dr AS em_name','emp.father_name_dr AS last_name','dept.name AS dep_name','cat.*','equip.*','emp_equip.id AS em_eq_id')
			->leftjoin('specification.department as dept','dept.id','=','emp.department')
			->leftjoin('specification.employes_equipment as emp_equip','emp_equip.employes_id','=','emp.id')
			->leftjoin('specification.equipments as equip','emp_equip.equipment_id','=','equip.id')
			->leftjoin('specification.categories as cat','equip.categories_id','=','cat.id')
			->whereRaw("emp_equip.status= 0 AND emp_equip.delete_status=0 AND
				(
				 	
				 	emp.id= ".$employes_id."
			   
			 		 )")->orderby('emp_equip.id','desc');

					$data=$rows->get();

			$row=\DB::table('hr.employees as emp')
			->select('emp.name_dr AS em_name','emp.father_name_dr AS last_name','dept.name AS dep_name','cat.*','equip.*','emp_equip.id AS em_eq_id')
			->leftjoin('specification.department as dept','dept.id','=','emp.department')
			->leftjoin('specification.employes_equipment as emp_equip','emp_equip.employes_id','=','emp.id')
			->leftjoin('specification.equipments as equip','emp_equip.equipment_id','=','equip.id')
			->leftjoin('specification.categories as cat','equip.categories_id','=','cat.id')
			->whereRaw("emp_equip.status= 0 AND emp_equip.delete_status=0 AND
				(
				 	
				 	emp.id= ".$employes_id."
			   
			 		 )")->orderby('emp_equip.id','desc')->limit(1);
				$data2=$row->get();

			return view::make('specification.print_employes_product',array('data' =>$data,'data2' =>$data2));

	}


}

?>