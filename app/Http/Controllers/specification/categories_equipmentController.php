<?php 
namespace App\Http\Controllers\specification;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\specification\equipment_model;
use App\models\specification\categories_model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Validator;
use Response;
use File;

class categories_equipmentController extends Controller {

	public function load_inset_page(){
		$categories=equipment_model::get_categories();
		return view::make('specification.insert_equipment',['categories'=>$categories]);

	}
	public function insert_equipment(Request $request){

			    $validates = \Validator::make(Input::all(),array(
		        "categories_id" => "required",
		        "status" 		=> "required",
		        "serial_number"	=> "required|unique:specification.equipments",
		        
		    ));
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return \Redirect::route("insert_equipment")->withErrors($validates)->withInput();
		    }
		    else
		    {
		    //dd($_POST);
		$equipments=new equipment_model;
		$equipments->categories_id=Input::get('categories_id');
		$equipments->status=Input::get('status');
		$equipments->model=Input::get('model');
		$equipments->serial_number=Input::get('serial_number');
		$equipments->description=Input::get('description');
		if($equipments->save())

        	{
            		return \Redirect::route("insert_equipment")->with("success","You have successfuly inserted the record <span style='color:red;font_weight:bold;'></span>");
        	}
       
		}
	}
	public function deleterow($id){
		$row=new equipment_model;
		$row=equipment_model::find($id);
		$row->delete_equipment='1';
		$row->save();
		if($row)
        {
            return \Redirect::route("load_equipment")->with("success","You have successfuly Remove the record <span style='color:red;font_weight:bold;'></span>");
        }
        else
        {
            return \Redirect::route("load_equipment")->with("fail","An error occured please try again.");
        }}
	
	public function select_row_eq($id){
		$rows=equipment_model::find($id);	
		$categories=equipment_model::get_categories();
		return view::make('specification.update_equipment',array('rows' => $rows,'categories' => $categories ));
	}

	public function updateEquipment($id){
		$equipments=new equipment_model;
		$equipments=equipment_model::find($id);
		$equipments->categories_id=Input::get('categories_id');
		$equipments->status=Input::get('status');
		$equipments->model=Input::get('model');
		$equipments->serial_number=Input::get('serial_number');
		$equipments->description=Input::get('description');

			if($equipments->save())
        {
            return \Redirect::route("load_equipment")->with("success","You have successfuly updated the record <span style='color:red;font_weight:bold;'></span>");


        }
        else
        {
            return \Redirect::route("select_update")->with("fail","An error occured please try again.");
        }

	}
	
	//categories function 

	public function load_insert_categores(){

		return view::make('specification.insert_categories');
	}

	public function insert_categories(){
		$categories=new categories_model;
		$categories->name=Input::get('name');
		$categories->description=Input::get('description');
		if($categories->save())
        {
            return \Redirect::route("load_cat")->with("success","You have successfuly inserted the record <span style='color:red;font_weight:bold;'></span>");


        }else
        {
            return \Redirect::route("load_cat")->with("fail","An error occured please try again.");
        }

	}
	public function deletecat($id){
		$categories=categories_model::find($id);
		$categories->delete();
		if($categories)
        {
            return \Redirect::route("load_equipment")->with("success","You have successfuly Remove the record <span style='color:red;font_weight:bold;'></span>");
        }
        else
        {
            return \Redirect::route("load_equipment")->with("fail","An error occured please try again.");
        }
	}
	public function updateCat($id){

		$categories=categories_model::find($id);
		return view::make('specification.update_categories',['categories'=>$categories]);


	}
	public function update_Cat($id){
		$categories=categories_model::find($id);
		$categories->name=Input::get('name');
		$categories->description=Input::get('description');
		if($categories->save())
        {
            return \Redirect::route("load_cat")->with("success","You have successfuly updated the record <span style='color:red;font_weight:bold;'></span>");


        }else
        {
            return \Redirect::route("load_cat")->with("fail","An error occured please try again.");
        }
	}


}