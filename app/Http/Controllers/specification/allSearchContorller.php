<?php 
namespace App\Http\Controllers\specification;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\specification\product;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Excel;

use Response;

class allSearchContorller extends Controller {

	public function search_data(){
		$feceno=product::get_feceno();
		$department=product::get_department();
		$moshakhasat_id=product::get_idmoshakhasat();

		return view::make('specification.all_search',array('feceno'=>$feceno,'department'=>$department,'moshakhasat_id'=>$moshakhasat_id));

	}

	public function feceno_search(Request $request){
		// return $request->all();
			$number=$request->input('number');
			$fe_status=$request->input('fe_status');
			$department_id=$request->input('department_id');
		
			if($date = $request->input('date'))
			$date = toGregorian(gregorian_format($date));

			$rows=\DB::table('specification.feceno as fe')->select('fe.*','dept.name as dept_name')->leftjoin('specification.department AS dept','fe.department_id','=','dept.id');
			// return $request->all();
			if($date)
				$rows->where('fe.date','=',$date);
			if($number)
				$rows->orwhere('fe.number', '=', $number);
			if($fe_status)
				$rows->orwhere('fe.fe_status', '=', $fe_status);
			if($department_id)
				$rows->orwhere('fe.department_id', '=', $department_id);
				$data2['records']=$rows->get();

			return view('specification.search_feceno',$data2);
			
	}
	
	public function search_product(Request $request){

		$product_name=$request->input('product_name');
		$moshakhasat_id=$request->input('moshakhasat_id');
		if($date = $request->input('date'))
		$date = toGregorian(gregorian_format($date));
		$feceno_id=$request->input('feceno_id');
		$department_id=$request->input('department_id');
		$status=$request->input('status');


		$rows=\DB::table('specification.products as prdt')->select('prdt.*','dept.name','f9.number')
		->leftjoin('specification.feceno as f9','prdt.feceno_id','=','f9.id')
		->leftjoin('specification.department as dept','prdt.department_id','=','dept.id');

			if($status !="")
			$rows->where('prdt.status', $status);
			if($product_name !="")
			$rows->orwhere('prdt.product_name', '=',$product_name);
			if($moshakhasat_id !="")
			$rows->orwhere('prdt.moshakhasat_id','=',$moshakhasat_id);
			if($date !="")
			$rows->orwhere('prdt.date', '=',$date);
			if($feceno_id !="")
			$rows->orwhere('prdt.feceno_id', '=',$feceno_id);
			if($department_id !="")
			$rows->orwhere('prdt.department_id', '=',$department_id);
			if($status !="")
			$rows->orwhere('prdt.status', '=',$status);
			$data['records']=$rows->get();
			return view('specification.search_allPruduct',$data);

	}

	public function export_ex(Request $request){
		

		$product_name=$request->input('product_name');
		$moshakhasat_id=$request->input('moshakhasat_id');
		if($date = $request->input('date'))
		$date = toGregorian(gregorian_format($date));
		$feceno_id=$request->input('feceno_id');
		$department_id=$request->input('department_id');
		$status=$request->input('status');


		$rows=\DB::table('specification.products as prdt')->select('prdt.*','dept.name','f9.number')
		->leftjoin('specification.feceno as f9','prdt.feceno_id','=','f9.id')
		->leftjoin('specification.department as dept','prdt.department_id','=','dept.id');

			if($product_name !="")
			$rows->where('prdt.product_name', '=',$product_name);
			if($moshakhasat_id !="")
			$rows->where('prdt.moshakhasat_id', '=',$moshakhasat_id);
			if($date !="")
			$rows->orwhere('prdt.date', '=',$date);
			if($feceno_id !="")
			$rows->orwhere('prdt.feceno_id', '=',$feceno_id);
			if($department_id !="")
			$rows->orwhere('prdt.department_id', '=',$department_id);
			if($status !="")
			$rows->orwhere('prdt.status', '=',$status);
			$data=$rows->get();


		//dd($searchInventories);
		$curr_date = date('Y-m-d');
		Excel::load('excel_template/product_print.xlsx', function($file) use($data){
			//Excel::create('Filename', function($file) use($data){			
			$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){	
				$row = 3;
				$sheet->setFreeze('A3');
				
				foreach($data AS $item)
				{

					$summary = preg_replace("/&nbsp;/",' ',$item->product_description);
					$text = preg_replace("/&ndash;/",'-',$summary);

					$sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true); 

					$summary = preg_replace("/&nbsp;/",' ',$item->product_description);
					$sheet->setHeight($row, 20);
					$sheet->setCellValue('N'.$row.'',$item->number_device);
					$sheet->setCellValue('M'.$row.'',$item->unit);
					$sheet->setCellValue('O'.$row.'',$item->guarantee);
					$sheet->setCellValue('H'.$row.'',checkEmptyDate($item->status_date));
					$sheet->setCellValue('G'.$row.'',strip_tags($text));
					$sheet->setCellValue('F'.$row.'',$item->product_name);
					$sheet->setCellValue('E'.$row.'',checkEmptyDate($item->date));
					$sheet->setCellValue('D'.$row.'',$item->name);
					$sheet->setCellValue('C'.$row.'',$item->moshakhasat_id);
					$sheet->setCellValue('B'.$row.'',$item->number);
					$sheet->setCellValue('A'.$row.'',$row-2);
				
					//$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));
					
					$row++;
					
				}

				$sheet->setBorder('A3:O'.($row-1).'', 'thin');
				
    		});

			})->export('xlsx');

	     

}
public function speci_report(){

	$date_from=Input::get('from_date');
	$date_to=Input::get('to_date');
	$status=Input::get('status');
	$all_data=Input::get('all_data');
	$department_id=Input::get('department_id');
	$product_name=Input::get('product_name');

	
	if($date_from and $date_to !=''){
	 	$from_date = toGregorian(gregorian_format($date_from));
		$to_date = toGregorian(gregorian_format($date_to));
	$rows=\DB::table('specification.products as prdt')->select('prdt.*','dept.name','f9.number')
		->leftjoin('specification.feceno as f9','prdt.feceno_id','=','f9.id')
		->leftjoin('specification.department as dept','prdt.department_id','=','dept.id');

		if($status !=" ")
		$rows->where('prdt.status','=',$status);
		if($department_id !='')
	    $rows->where('prdt.department_id','=',$department_id);
		if($product_name !='')
		$rows->where('prdt.product_name','like', "%".$product_name."%");
		$rows->whereBetween('prdt.date',[$from_date,$to_date]);
		$data['records']=$rows->get();
			return view('specification.search_product_report',$data);


	  }elseif($department_id !='' or $product_name !='' ){
 
		$rows=\DB::table('specification.products as prdt')->select('prdt.*','dept.name','f9.number')
		->leftjoin('specification.feceno as f9','prdt.feceno_id','=','f9.id')
		->leftjoin('specification.department as dept','prdt.department_id','=','dept.id');
		if($department_id !='')
		$rows->where('prdt.department_id','=',$department_id);
		if($product_name !='')
		$rows->where('prdt.product_name','like', "%".$product_name."%");
		$data['records']=$rows->get();
			return view('specification.search_product_report',$data);

	
	}elseif($all_data == '1'){
		$rows=\DB::table('specification.products as prdt')->select('prdt.*','dept.name','f9.number')
		->leftjoin('specification.feceno as f9','prdt.feceno_id','=','f9.id')
		->leftjoin('specification.department as dept','prdt.department_id','=','dept.id');

		if($status !=" ")
		$rows->where('status','=',$status);
		
		$data['records']=$rows->get();
			return view('specification.search_product_report',$data);
		
	}}
	public function export_report(){

	
		$date_from=Input::get('from_date');
		$date_to=Input::get('to_date');
		$status=Input::get('status');
		$all_data=Input::get('all_data');
		$department_id=Input::get('department_id');
		$product_name=Input::get('product_name');

		if($date_from and $date_to !=''){
		$from_date = toGregorian(gregorian_format($date_from));
		$to_date = toGregorian(gregorian_format($date_to));

		$rows=\DB::table('specification.products as prdt')->select('prdt.*','dept.name','f9.number')
			->leftjoin('specification.feceno as f9','prdt.feceno_id','=','f9.id')
			->leftjoin('specification.department as dept','prdt.department_id','=','dept.id');

			if($status !=" ")
			$rows->where('prdt.status','=',$status);
			if($department_id !='')
		    $rows->where('prdt.department_id','=',$department_id);
			if($product_name !='')
			$rows->where('prdt.product_name','like', "%".$product_name."%");
			$rows->whereBetween('prdt.date',[$from_date,$to_date]);

			$data=$rows->get();
			$curr_date = date('Y-m-d'); 
			Excel::load('excel_template/product_print.xlsx', function($file) use($data){
				//Excel::create('Filename', function($file) use($data){			
				$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){	
					$row = 3;
					//$sheet->setFreeze('A3');
					
					foreach($data AS $item)
					{
						$sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true); 

						$summary = preg_replace("/&nbsp;/",' ',$item->product_description);
						$text = preg_replace("/&ndash;/",'-',$summary);

						$sheet->setHeight($row, 20);
						$sheet->setCellValue('N'.$row.'',$item->number_device);
						$sheet->setCellValue('M'.$row.'',$item->unit);
						$sheet->setCellValue('O'.$row.'',$item->guarantee);
						$sheet->setCellValue('H'.$row.'',checkEmptyDate($item->status_date));
						$sheet->setCellValue('G'.$row.'',strip_tags($text));
						$sheet->setCellValue('F'.$row.'',$item->product_name);
						$sheet->setCellValue('E'.$row.'',checkEmptyDate($item->date));
						$sheet->setCellValue('D'.$row.'',$item->name);
						$sheet->setCellValue('C'.$row.'',$item->moshakhasat_id);
						$sheet->setCellValue('B'.$row.'',$item->number);
						$sheet->setCellValue('A'.$row.'',$row-2);
					
						//$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));
						
						$row++;
					}

					$sheet->setBorder('A3:O'.($row-1).'', 'thin');
					
	    		});
				
				})->export('xlsx');

			}elseif($department_id !='' or $product_name !=''){

				$rows=\DB::table('specification.products as prdt')->select('prdt.*','dept.name','f9.number')
				->leftjoin('specification.feceno as f9','prdt.feceno_id','=','f9.id')
				->leftjoin('specification.department as dept','prdt.department_id','=','dept.id');

				if($department_id !='')
				$rows->where('prdt.department_id','=',$department_id);
				if($product_name !='')
				$rows->where('prdt.product_name','like', "%".$product_name."%");
				$data=$rows->get();
				$curr_date = date('Y-m-d'); 
				Excel::load('excel_template/product_print.xlsx', function($file) use($data){
				//Excel::create('Filename', function($file) use($data){			
				$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){	
				$row = 3;
				//$sheet->setFreeze('A3');
				
				foreach($data AS $item)
				{
					$sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true); 

					$summary = preg_replace("/&nbsp;/",' ',$item->product_description);
					$text = preg_replace("/&ndash;/",'-',$summary);

					$sheet->setHeight($row, 20);
					$sheet->setCellValue('N'.$row.'',$item->number_device);
					$sheet->setCellValue('M'.$row.'',$item->unit);
					$sheet->setCellValue('O'.$row.'',$item->guarantee);
					$sheet->setCellValue('H'.$row.'',checkEmptyDate($item->status_date));
					$sheet->setCellValue('G'.$row.'',strip_tags($text));
					$sheet->setCellValue('F'.$row.'',$item->product_name);
					$sheet->setCellValue('E'.$row.'',checkEmptyDate($item->date));
					$sheet->setCellValue('D'.$row.'',$item->name);
					$sheet->setCellValue('C'.$row.'',$item->moshakhasat_id);
					$sheet->setCellValue('B'.$row.'',$item->number);
					$sheet->setCellValue('A'.$row.'',$row-2);
				
					//$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));
					
					$row++;
				}

				$sheet->setBorder('A3:O'.($row-1).'', 'thin');
				
    		});
			
			})->export('xlsx');
			}
			
		if($all_data=='1'){

			$rows=\DB::table('specification.products as prdt')->select('prdt.*','dept.name','f9.number')
				->leftjoin('specification.feceno as f9','prdt.feceno_id','=','f9.id')
				->leftjoin('specification.department as dept','prdt.department_id','=','dept.id');

			if($status !=" ")
			$rows->where('status','=',$status);
			$data=$rows->get();
			
			
			
			$curr_date = date('Y-m-d');
			Excel::load('excel_template/product_print.xlsx', function($file) use($data){
				//Excel::create('Filename', function($file) use($data){			
				$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){	
					$row = 3;
					//$sheet->setFreeze('A3');
					
					foreach($data AS $item)
					{
						$sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true); 

						$summary = preg_replace("/&nbsp;/",' ',$item->product_description);
						$text = preg_replace("/&ndash;/",'-',$summary);

						$sheet->setHeight($row, 20);
						$sheet->setCellValue('N'.$row.'',$item->number_device);
						$sheet->setCellValue('M'.$row.'',$item->unit);
						$sheet->setCellValue('O'.$row.'',$item->guarantee);
						$sheet->setCellValue('H'.$row.'',checkEmptyDate($item->status_date));
						$sheet->setCellValue('G'.$row.'',strip_tags($text));
						$sheet->setCellValue('F'.$row.'',$item->product_name);
						$sheet->setCellValue('E'.$row.'',checkEmptyDate($item->date));
						$sheet->setCellValue('D'.$row.'',$item->name);
						$sheet->setCellValue('C'.$row.'',$item->moshakhasat_id);
						$sheet->setCellValue('B'.$row.'',$item->number);
						$sheet->setCellValue('A'.$row.'',$row-2);
					
						//$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));
						
						$row++;
					}

					$sheet->setBorder('A3:O'.($row-1).'', 'thin');
					
	    		});
				
				})->export('xlsx');





	}}




	public function export_feceno(Request $request){

			$number=$request->input('number');
			$fe_status=$request->input('fe_status');
			$department_id=$request->input('department_id');
		
			if($date = $request->input('date'))
			$date = toGregorian(gregorian_format($date));

			$rows=\DB::table('specification.feceno as fe')->select('fe.*','dept.name as dept_name')->leftjoin('specification.department AS dept','fe.department_id','=','dept.id');
			
			if($date !="");
				$rows->where('fe.date','=',$date);
			if($number !="");
				$rows->orwhere('fe.number', '=', $number);
			if($fe_status !="");
				$rows->orwhere('fe.fe_status', '=', $fe_status);
			if($department_id !="");
				$rows->orwhere('fe.department_id', '=', $department_id);
			$data=$rows->get();

			$curr_date = date('Y-m-d');
			
			Excel::load('excel_template/export_feceno.xlsx', function($file) use($data){
			//Excel::create('Filename', function($file) use($data){			
			$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){	
				$row = 3;
				//$sheet->setFreeze('A3');
				
				foreach($data AS $item)
				{
					$sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true); 

					
					$sheet->setHeight($row, 20);
					$sheet->setCellValue('E'.$row.'',$item->description);
					$sheet->setCellValue('D'.$row.'',$item->dept_name);
					$sheet->setCellValue('C'.$row.'',checkEmptyDate($item->date));
					$sheet->setCellValue('B'.$row.'',$item->number);
					$sheet->setCellValue('A'.$row.'',$row-2);
				
					//$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));
					
					$row++;
				}

				$sheet->setBorder('A3:E'.($row-1).'', 'thin');
				
    		});
			
			})->export('xlsx');



	 }
	}



	



?>