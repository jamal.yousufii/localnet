<?php
namespace App\Http\Controllers\library;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\library\libraryModel;
use App\models\library\CustomerModel;
use App\models\library\DistributeModel;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Response;
use Excel;
class DistributeController extends Controller {

 public function distributeBook(){

    $customers=CustomerModel::orderby('id','desc')->get();
    $distribute_book=DistributeModel::getDistributedBook();

    return view::make('library.distribute_book.distribute_book')->with('customers',$customers)->with('distribute_book',$distribute_book);
 }
 public function insertCustomer(){

        $validates = \Validator::make(Input::all(),array(
        "name" => "required",
        "lastname"        => "required",
        "number" => "required|unique:library_system.customers",
        "email" => "required",

        ));

        //check the validation
        if($validates->fails())
        {
            return \Redirect::route("distribute_book")->withErrors($validates)->withInput();
        }
        else
        {
        //dd($_POST);
        $customer= new CustomerModel;
        $customer->name=Input::get('name');
        $customer->lastname=Input::get('lastname');
        $customer->number=Input::get('number');
        $customer->email=Input::get('email');
        $customer->office=Input::get('office');
        $customer->position=Input::get('position');
        if($customer->save())

            {
                    return \Redirect::route("distribute_book")->with("success","You have successfuly inserted the record <span style='color:red;font_weight:bold;'></span>");
            }

         }
        }

 public function selectCustomerRow($id){

        $customers=CustomerModel::find($id);

        return view::make('library.distribute_book.update_customer')->with('customers',$customers);
     }

 public function updateCustomer($id){

     $validates = \Validator::make(Input::all(),array(
        "name"          => "required",
        "lastname"      => "required",
        "number"        => "required",
        "email"         => "required",

        ));

        //check the validation
        if($validates->fails())
        {
            return \Redirect::route("distribute_book")->withErrors($validates)->withInput();
        }
        else
        {
        //dd($_POST);
        $customer=CustomerModel::find($id);
        $customer->name=Input::get('name');
        $customer->lastname=Input::get('lastname');
        $customer->number=Input::get('number');
        $customer->email=Input::get('email');
        $customer->office=Input::get('office');
        $customer->position=Input::get('position');
        $customer->created_by=Auth::user()->id;
        if($customer->save())

            {
                    return \Redirect::route("distribute_book")->with("success","You have successfuly update the record <span style='color:red;font_weight:bold;'></span>");
            }

         }

 }

 public function distributeForm(){

    $customer=CustomerModel::orderby('id','desc')->get();
    $book=libraryModel::all();
    $categories = DB::connection('library')->table('categories')->get();

    return view::make('library.distribute_book.distribute_form')->with('customer',$customer)->with('book',$book)->with('categories',$categories);
 }

 public function distributeInsert(){

 $validates = \Validator::make(Input::all(),array(
        "book_id"          => "required",
        "customer_id"      => "required",
        "from_date"        => "required",
        "to_date"         => "required",

        ));

        //check the validation
        if($validates->fails())
        {
            return \Redirect::route("distribute_form")->withErrors($validates)->withInput();
        }
        else
        {
        //dd($_POST);
        $from_date = toGregorian(gregorian_format(Input::get('from_date')));
        $to_date = toGregorian(gregorian_format(Input::get('to_date')));

        $distribute=new DistributeModel();
        $distribute->book_id=Input::get('book_id');
        $distribute->customer_id=Input::get('customer_id');
        $distribute->from_date=$from_date;
        $distribute->to_date=$to_date;
        $distribute->description=Input::get('description');
        $distribute->created_by=Auth::user()->id;
        if($distribute->save())

            {
                    return \Redirect::route("distribute_form")->with("success","You have successfuly Insert the record <span style='color:red;font_weight:bold;'></span>");
            }

         }

 }

 public function distributeRow($id){

      $customer=CustomerModel::all();
      $book=libraryModel::all();
      $distribute_book=DistributeModel::find($id);

    // $rows=\DB::table('distribute_book')->select('book.*','categories.*','customers.*','distribute_book.*')
    //     ->leftJoin('book','distribute_book.book_id','=','book.id')
    //     ->leftJoin('categories', 'book.book_type', '=', 'categories.id')
    //     ->leftJoin('customers','distribute_book.customer_id','=','customers.id')
    //     ->where('distribute_book.id',$id)->get();

        return view::make('library.distribute_book.update_distribute')->with('distribute_book',$distribute_book)->with('customer',$customer)->with('book',$book);
 }

  public function distributeRowUpdate($id){

    $validates = \Validator::make(Input::all(),array(
        "book_id"          => "required",
        "customer_id"      => "required",
        "from_date"        => "required",
        "to_date"         => "required",

        ));

        //check the validation
        if($validates->fails())
        {
            return \Redirect::route("distribute_select_row",$id)->withErrors($validates)->withInput();
        }
        else
        {
        //dd($_POST);
        $from_date = toGregorian(gregorian_format(Input::get('from_date')));
        $to_date = toGregorian(gregorian_format(Input::get('to_date')));

        $distribute=DistributeModel::find($id);
        $distribute->book_id=Input::get('book_id');
        $distribute->customer_id=Input::get('customer_id');
        $distribute->from_date=$from_date;
        $distribute->to_date=$to_date;
        $distribute->description=Input::get('description');
        $distribute->updated_by=Auth::user()->id;
        if($distribute->save())

            {
                    return \Redirect::route("distribute_book")->with("success","You have successfuly Insert the record <span style='color:red;font_weight:bold;'></span>");
            }

         }
  }



  public function returnDate(){

     $validates = \Validator::make(Input::all(),array(
            "return_date"          => "required",
         ));

    //check the validation
    if(!$validates->fails())
    {
        $id=Input::get('get_id');
        $return_date = toGregorian(gregorian_format(Input::get('return_date')));

        $distribute=DistributeModel::find($id);

        $distribute->return_date=$return_date;
        $distribute->save();
         if($distribute->save())

            {
                    return \Redirect::route("distribute_book")->with("success","You have successfuly Updated return date the record <span style='color:red;font_weight:bold;'></span>");
            }

    }
        return \Redirect::route("distribute_book")->withErrors($validates)->withInput();


  }

  public function bookType($id){

    $row=\DB::connection('library')->table('book')->select('book.id','book.book_name')->where('book_type',$id)->get();

    return $row;
  }

    }

?>
