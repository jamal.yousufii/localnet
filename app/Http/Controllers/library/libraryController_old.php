<?php 
namespace App\Http\Controllers\library;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\library\libraryModel;
use Illuminate\Support\Collection;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Response;

class libraryController extends Controller {

	public function loadRecordsList()
	{

		$data=libraryModel::gettab2();
		$data1=libraryModel::gettab1();
		$data2=libraryModel::gettab3();
		$data3=libraryModel::gettab4();
		$data4=libraryModel::gettab5();
		$data5=libraryModel::gettab6();
		$data6=libraryModel::gettab7();
		$data7=libraryModel::gettab8();
		$data8=libraryModel::gettab9();
		$data9=libraryModel::gettab10();
		$data10=libraryModel::gettab11();
		$data11=libraryModel::gettab12();
		$data12=libraryModel::gettab13();
	    $data13=libraryModel::gettab14();

		
		return view::make('library.book_list1',array('data' => $data,'data1' => $data1,'data2' => $data2,'data3' => $data3,'data4' => $data4,'data5' => $data5,'data6' => $data6,'data7' => $data7,'data8' => $data8,'data9' => $data9,'data10' => $data10,'data11' => $data11,'data12' => $data12,'data13' => $data13 ));

}
public function insert_rows(){

	$row=\DB::connection('library')->table('categories')->get();

		return view::make('library.insert_book1',['row'=>$row]);
}

	public function create_row(Request $request){

		$library = new libraryModel;
	$library->book_name=Input::get('book_name');
	$library->author_name=Input::get('author_name');
	$library->serial_number=Input::get('serial_number');
	$library->book_type=Input::get('book_type');
	$library->cabinet_number=Input::get('cabinet_number');
	$library->description=Input::get('description');


		if($library->save())
        {
            return \Redirect::route("insertData")->with("success","You have successfuly inserted the record <span style='color:red;font_weight:bold;'></span>");


        }
        else
        {
            return \Redirect::route("createData")->with("fail","An error occured please try again.");
        }
		
		

	}

	public function delete_row($id){

		$row=libraryModel::find($id);

		$row->delete();

		if($row)
        {
            return \Redirect::route("recordsList")->with("success","You have successfuly deleted the record <span style='color:red;font_weight:bold;'></span>");
        }
        else
        {
            return \Redirect::route("recordsList")->with("fail","An error occured please try again.");
        }
		
		}

	public function select_id($id){

		$data=libraryModel::find($id);
		$row=\DB::connection('library')->table('categories')->get();

		return view::make('library.update',array('data'=>$data,'row'=>$row));

	}
	public function update(Request $request,$id){

		$data= new libraryModel;
		$data=libraryModel::find($id);
		$data->book_name=Input::get('book_name');
		$data->author_name=Input::get('author_name');

		$data->serial_number=Input::get('serial_number');

		$data->book_type=Input::get('book_type');

		$data->cabinet_number=Input::get('cabinet_number');

		$data->description=Input::get('description');

			if($data->save())
        {
            return \Redirect::route("recordsList")->with("success","You have successfuly Update the record <span style='color:red;font_weight:bold;'></span>");
        }
        else
        {
            return \Redirect::route("selectupdate")->with("fail","An error occured please try again.");
        }

	}

	public function highSearch(){

		$row=\DB::connection('library')->table('categories')->get();

		return view::make('library.high_search',['row'=>$row]);
	}
	public function select_search(Request $request){

		$book_name=$request->Input('book_name');
		$author_name=$request->Input('author_name');
		$book_type=$request->Input('book_type');
		$cabinet_number=$request->Input('cabinet_number');

		$rows=\DB::connection('library')->table('book')->select('book.*','categories.cat_name')->leftJoin('categories', 'book.book_type', '=', 'categories.id');

		if($book_name !=" ");
			$rows->where('book_name','=',$book_name);
		if($author_name !=" ");
			$rows->orwhere('author_name','=',$author_name);
		
		if($book_type !=" ");
			$rows->orwhere('book_type','=',$book_type);
		if($cabinet_number !=" ");
			$rows->orwhere('cabinet_number','=',$cabinet_number);
		
		    $data['records'] = $rows->get();

		 return view('library.search_result', $data);		





	}
	public function searchfromtable(){

		$search=trim(Input::get('field_value'));

		$cat_id=trim(Input::get('cat_id'));
		$rows=\DB::connection('library')->table('book')->select('book.*','categories.cat_name')->leftJoin('categories', 'book.book_type', '=', 'categories.id')
		->where('book_type','=',$cat_id)->where('book_name','like','%'.$search.'%')->get();
		$data['rows'] = $rows;
		return view::make('library.search_haqoq',$data);

		

		
	}
public function last_serial_num(){

			$book_type=trim(Input::get('search_value'));

			$rows=\DB::connection('library')->table('book')->select('serial_number')->where('book_type','=',$book_type)->orderby('serial_number','desc')->limit(1);

			$data['records'] = $rows->get();
			return view::make('library.serial_number',$data);

			

}


}

?>