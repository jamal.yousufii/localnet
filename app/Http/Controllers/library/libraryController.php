<?php
namespace App\Http\Controllers\library;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\library\libraryModel;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Response;
use Excel;
class LibraryController extends Controller {
    public function loadlibraryList(request $request) {
        $data1 = LibraryModel::gettab1();
        $data2 = LibraryModel::gettab2();
        $data3 = LibraryModel::gettab3();
        $data4 = LibraryModel::gettab4();
        $data5 = LibraryModel::gettab5();
        $data6 = LibraryModel::gettab6();
        $data7 = LibraryModel::gettab7();
        $data8 = LibraryModel::gettab8();
        $data9 = LibraryModel::gettab9();
        $data10 = LibraryModel::gettab10();
        $data11 = LibraryModel::gettab11();
        $data12 = LibraryModel::gettab12();
        $data13 = LibraryModel::gettab13();
        $data14 = LibraryModel::gettab14();
        $categories = DB::connection('library')->table('categories')->get();
        return view::make('library.book_list1', array('data1' => $data1, 'data2' => $data2, 'data3' => $data3, 'data4' => $data4, 'data5' => $data5, 'data6' => $data6, 'data7' => $data7, 'data8' => $data8, 'data9' => $data9, 'data10' => $data10, 'data11' => $data11, 'data12' => $data12, 'data13' => $data13, 'data14' => $data14, 'categories' => $categories));
    }
    public function insert_rows() {
        $row = \DB::connection('library')->table('categories')->get();
        return view::make('library.insert_book1', ['row' => $row]);
    }
    public function create_row(Request $request) {
        $library_model = new LibraryModel;
        $library_model->book_name = Input::get('book_name');
        $library_model->author_name = Input::get('author_name');
        $library_model->serial_number = Input::get('serial_number');
        $library_model->book_type = Input::get('book_type');
        $library_model->cabinet_number = Input::get('cabinet_number');
        $library_model->description = Input::get('description');
        if ($library_model->save()) {
            return \Redirect::route("insertLibraryData")->with("success", "You have successfuly inserted the record <span style='color:red;font_weight:bold;'></span>");
        } else {
            return \Redirect::route("createData")->with("fail", "An error occured please try again.");
        }
    }
    public function delete_row($id) {
        $row = LibraryModel::find($id);
        $row->delete();
        if ($row) {
            return \Redirect::route("libraryList")->with("success", "You have successfuly deleted the record <span style='color:red;font_weight:bold;'></span>");
        } else {
            return \Redirect::route("libraryList")->with("fail", "An error occured please try again.");
        }
    }
    public function select_id($id) {
        $data = LibraryModel::find($id);
        $row = \DB::connection('library')->table('categories')->get();
        return view::make('library.update', array('data' => $data, 'row' => $row));
    }
    public function update(Request $request, $id) {
        $data = new LibraryModel;
        $data = LibraryModel::find($id);
        $data->book_name = Input::get('book_name');
        $data->author_name = Input::get('author_name');
        $data->serial_number = Input::get('serial_number');
        $data->book_type = Input::get('book_type');
        $data->cabinet_number = Input::get('cabinet_number');
        $data->description = Input::get('description');
        if ($data->save()) {
            return \Redirect::route("libraryList")->with("success", "You have successfuly Update the record <span style='color:red;font_weight:bold;'></span>");
        } else {
            return \Redirect::route("selectupdate")->with("fail", "An error occured please try again.");
        }
    }
    public function highSearch() {
        $row = \DB::connection('library')->table('categories')->get();
        return view::make('library.high_search', ['row' => $row]);
    }
    public function select_search(Request $request) {
        $book_name = $request->Input('book_name');
        $author_name = $request->Input('author_name');
        $book_type = $request->Input('book_type');
        $cabinet_number = $request->Input('cabinet_number');
        $rows = \DB::connection('library')->table('book')->select('book.*', 'categories.cat_name')->leftJoin('categories', 'book.book_type', '=', 'categories.id');

        if ($book_name != "")
        $rows->where('book_name', '=', $book_name);
        if ($author_name != "")
        $rows->orwhere('author_name', '=', $author_name);
        if ($book_type != " ")
        $rows->orwhere('book_type', '=', $book_type);
        if ($cabinet_number != "")
        $rows->orwhere('cabinet_number', '=', $cabinet_number);
        $data['records'] = $rows->get();
        return view('library.search_result', $data);
    }

    public function searchfromtable() {
        $search = trim(Input::get('field_value'));
        $cat_id = trim(Input::get('cat_id'));
        $rows = \DB::connection('library')->table('book')->select('book.*', 'categories.cat_name')->leftJoin('categories', 'book.book_type', '=', 'categories.id')->where('book_type', '=', $cat_id)->where('book_name', 'like', '%' . $search . '%')->get();
        $data['rows'] = $rows;
        return view::make('library.search_haqoq', $data);
    }

    public function last_serial_num() {
        $book_type = trim(Input::get('search_value'));
        $rows = \DB::connection('library')->table('book')->select('serial_number')->where('book_type', '=', $book_type)->orderby('serial_number', 'desc')->limit(1);
        $data['records'] = $rows->get();
        return view::make('library.serial_number', $data);
    }

    //laravel pagination by ajax
    public function get_paginationData() {
        $pagination_data = \DB::connection('library')->table('book')->select('book.*', 'categories.cat_name')->leftJoin('categories', 'book.book_type', '=', 'categories.id')->where('book_type', '1')->paginate(4);
        return view::make('library.pagination_data', array('pagination_data' => $pagination_data));
    }

    public function exportBook(Request $request){

    	$book_name = $request->Input('book_name');
        $author_name = $request->Input('author_name');
        $book_type = $request->Input('book_type');
        $cabinet_number = $request->Input('cabinet_number');
        $rows = \DB::connection('library')->table('book')->select('book.*', 'categories.cat_name')->leftJoin('categories', 'book.book_type', '=', 'categories.id');
        if ($book_name != "")
        $rows->where('book_name', '=', $book_name);
        if ($author_name != "")
        $rows->orwhere('author_name', '=', $author_name);
        if ($book_type != " ")
        $rows->orwhere('book_type', '=', $book_type);
        if ($cabinet_number != "")
        $rows->orwhere('cabinet_number', '=', $cabinet_number);
        $data= $rows->get();
      	$curr_date = date('Y-m-d');

		Excel::load('excel_template/Library_book_excel.xlsx', function($file) use($data){
			//Excel::create('Filename', function($file) use($data){			
			$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){	
				$row = 3;
				$sheet->setFreeze('A3');
				
				foreach($data AS $item)
				{

					$sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true); 

					$sheet->setHeight($row, 20);
					$sheet->setCellValue('A'.$row.'',$item->description);
					$sheet->setCellValue('B'.$row.'',$item->cabinet_number);
					$sheet->setCellValue('C'.$row.'',$item->author_name);
					$sheet->setCellValue('D'.$row.'',$item->serial_number);
					$sheet->setCellValue('E'.$row.'',$item->cat_name);
					$sheet->setCellValue('F'.$row.'',$item->book_name);
					$sheet->setCellValue('G'.$row.'',$row-2);
				
					//$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));
					
					$row++;
				}

				$sheet->setBorder('A3:G'.($row-1).'', 'thin');
				
    		});
			
			})->export('xlsx');

	     

    }
}
?>