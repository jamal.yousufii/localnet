<?php namespace App\Http\Controllers;

/*
@desc: insert,upadate,delete and manager other document properties
@Author: Gul Muhammad Akbari (gm.akbari27@gmail.com)
@Created At: 14 March 2015
@version: 1.0
*/

use Auth;
use View;
use Input;
use Illuminate\Support\Str;
use App\models\Document;
use App\models\DocTree;
use App\models\DepartmentX;
use App\models\User;
class DocumentController extends Controller
{


	public function getAll()
	{
		//check roles
		if(canView('auth_document'))
		{
			//get data from database
			$records = Document::getAll();
			$data['records'] = $records;
			$data['trees'] = DocTree::getOnlyParentTrees();
			//load view for list
			return View::make("auth.document.doc_list",$data);//->with('records',$apps);
		}
		else
		{
			return showWarning();
		}
	}
	/*
	getting form for inserting new document
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreateTree($parent = 0)
	{
		//check roles
		if(canAdd('auth_document'))
		{

			$data['trees'] = DocTree::getAllTrees();
			$data['parent'] = $parent;
			//load view for users list
			return View::make("auth.document.create_tree",$data);
		}
		else
		{
			return showWarning();
		}
	}
	/*
	getting form for inserting new document
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreateDoc($folder=0)
	{
		//check roles
		if(canAdd('auth_document'))
		{

			//get departments
			$data['deps'] = DepartmentX::getAll();
			//get users
			$data['users'] = User::getAll();
			$data['folder'] = $folder;
			
			//load view for users list
			return View::make("auth.document.doc_create",$data);
		}
		else
		{
			return showWarning();
		}
	}
	/*
	Inserting filled form to document
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function postCreateDoc()
	{
		
		//check roles
		if(canAdd('auth_document'))
		{
			//validate the input fields
		    $validates = \Validator::make(Input::all(),array(
		        "name" => "required"
		    ));
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return \Redirect::route("getCreateDoc")->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        //create an object from department class
		        $object = new Document();
		        $object->name = Input::get("name");
		        $object->keyword = Input::get("keyword");
		        $object->description = Input::get("desc");
		        $object->date = date('Y-m-d H:i:s');
		        $object->user_id = Auth::user()->id;
		        $object->tree_id = Input::get('tree_id');

		        if($object->save())
		        {
		        	$record_id = $object->id;
		        	$this->uploadDocs($record_id);
		        	//check if shared with some one or share with some departments
		        	$shared = Input::get('share_with');
		        	if(count($shared)>0)
		        	{
		        		$users = array();
		        		$deps = array();
		        		
		        		for($i=0;$i<count($shared);$i++)
		        		{
		        			$shared_with = $shared[$i];
		        			//explode shared
		        			$shared_with = explode("_", $shared_with);
		        			//check if shared with user
		        			if($shared_with[0] == 'u')
		        			{
		        				$data = array(
		        						'user_id' => $shared_with[1],
		        						'document_id' => $record_id
		        					);
		        				//push to user array
		        				array_push($users, $data);
		        			}//check if shared with department
		        			else if($shared_with[0] == 'd')
		        			{
		        				$data = array(
		        						'department_id' => $shared_with[1],
		        						'document_id' => $record_id
		        					);
		        				//push to department array
		        				array_push($deps, $data);
		        			}
		        		}

		        		//then, insert document shared
		        		if(count($users)>0)
		        		{
		        			Document::insertBatch('document_shares_user',$users);
		        		}
		        		if(count($deps)>0)
		        		{
		        			Document::insertBatch('document_shares_department',$deps);
		        		}
		        	}
		            return \Redirect::route("getAllDocuments")->with("success","One record added successfully.");
		        }
		        else
		        {
		            return \Redirect::route("getAllDocuments")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}
		
	}

	/*
	getting form for updating new document
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getUpdateDoc($doc_id=0)
	{
		//check roles
		if(canEdit('auth_document'))
		{

			//get departments
			$data['deps'] = DepartmentX::getAll();
			//get users
			$data['users'] = User::getAll();
			$data['folder'] = $doc_id;

			//get document details
			$data['doc_details'] = Document::getDetails($doc_id);
			//get shared users
			$sharesUser = Document::getSharedDetails($doc_id,'u');
			$shared_users = array();
			foreach($sharesUser AS $item)
			{
				$shared_users[] = $item->user_id;
			}
			$data['shared_users'] = $shared_users;

			//get shared department
			$sharesDeps = Document::getSharedDetails($doc_id,'d');
			$shared_deps = array();
			foreach($sharesDeps AS $item)
			{
				$shared_deps[] = $item->department_id;
			}
			$data['shared_deps'] = $shared_deps;

			//get document files
			$data['doc_files'] = Document::getSharedDetails($doc_id,'f'); 


			//load view for users list
			return View::make("auth.document.doc_update",$data);
		}
		else
		{
			return showWarning();
		}
	}

	/*
	update filled form to document
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function postUpdateDoc($doc_id=0)
	{
		
		//check roles
		if(canEdit('auth_document'))
		{
			//validate the input fields
		    $validates = \Validator::make(Input::all(),array(
		        "name" => "required"
		    ));
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return \Redirect::route("getUpdateDoc")->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        //create an object from department class
		        $object = Document::find($doc_id);
		        $object->name = Input::get("name");
		        $object->keyword = Input::get("keyword");
		        $object->description = Input::get("desc");
		        //$object->date = date('Y-m-d H:i:s');
		        //$object->user_id = Auth::user()->id;
		        $object->tree_id = Input::get('tree_id');

		        if($object->save())
		        {
		        	
		        	//first remove shared with users and departments
		        	Document::removeShared($doc_id);
		        	$this->uploadDocs($doc_id);
		        	//check if shared with some one or share with some departments
		        	$shared = Input::get('share_with');
		        	if(count($shared)>0)
		        	{
		        		$users = array();
		        		$deps = array();
		        		
		        		for($i=0;$i<count($shared);$i++)
		        		{
		        			$shared_with = $shared[$i];
		        			//explode shared
		        			$shared_with = explode("_", $shared_with);
		        			//check if shared with user
		        			if($shared_with[0] == 'u')
		        			{
		        				$data = array(
		        						'user_id' => $shared_with[1],
		        						'document_id' => $doc_id
		        					);
		        				//push to user array
		        				array_push($users, $data);
		        			}//check if shared with department
		        			else if($shared_with[0] == 'd')
		        			{
		        				$data = array(
		        						'department_id' => $shared_with[1],
		        						'document_id' => $doc_id
		        					);
		        				//push to department array
		        				array_push($deps, $data);
		        			}
		        		}

		        		//then, insert document shared
		        		if(count($users)>0)
		        		{
		        			Document::insertBatch('document_shares_user',$users);
		        		}
		        		if(count($deps)>0)
		        		{
		        			Document::insertBatch('document_shares_department',$deps);
		        		}
		        	}
		            return \Redirect::route("getAllDocuments")->with("success","One record added successfully.");
		        }
		        else
		        {
		            return \Redirect::route("getAllDocuments")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}
		
	}

	/*
	Inserting filled form to document
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function postCreateTree()
	{
		
		//check roles
		if(canAdd('auth_document'))
		{
			//validate the input fields
		    $validates = \Validator::make(Input::all(),array(
		        "name" => "required"
		    ));
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return \Redirect::route("getCreateTree")->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        //create an object from department class
		        $object = new DocTree();
		        $object->name = Input::get("name");
		        $object->type = Input::get("type");
		        if(Input::get("parent")!='')
		        {
		        	$object->parent = Input::get("parent");
		        }
		        else
		        {
		        	$object->parent = 0;
		        }
		        $object->user_id = Auth::user()->id;

		        if($object->save())
		        {
		        	
		            return \Redirect::route("getAllDocuments")->with("success","Tree Created successfully.");
		        }
		        else
		        {
		            return \Redirect::route("getAllDocuments")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}
		
	}

	public function getUpdateTree($tree_id=0)
	{
		//check roles
		if(canEdit('auth_document'))
		{

			$data['trees'] = DocTree::getAllTrees();
			$data['tree_details'] = DocTree::getTreeDetails($tree_id);
			//load view for users list
			return View::make("auth.document.edit_tree",$data);
		}
		else
		{
			return showWarning();
		}
	}

	/*
	Inserting filled form to document
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function postUpdateTree($id=0)
	{
		
		//check roles
		if(canEdit('auth_document'))
		{
			//validate the input fields
		    $validates = \Validator::make(Input::all(),array(
		        "name" => "required"
		    ));
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return \Redirect::route("getUpdateTree")->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        //create an object from department class
		        $object = DocTree::find($id);
		        $object->name = Input::get("name");
		        $object->type = Input::get("type");

		        if(Input::get("parent")!='')
		        {
		        	$object->parent = Input::get("parent");
		        }
		        else
		        {
		        	$object->parent = 0;
		        }
		        
		        if($object->save())
		        {
		        	
		            return \Redirect::route("getAllDocuments")->with("success","Tree Created successfully.");
		        }
		        else
		        {
		            return \Redirect::route("getAllDocuments")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}
		
	}

	//upload document
	public function uploadDocs($doc_id=0)
	{

		// getting all of the post data
		$files = Input::file('files');
		$errors = "";
		$file_data = array();

		foreach($files as $file) 
		{
			
			// if($_FILES[$file]['size']>0)
			// {
			  // validating each file.
			  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			  $validator = \Validator::make(

			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'file' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png,doc,docx,zip,rar,pdf,rtf,xlsx,xls,txt'
			        ]
			  	);

			  if($validator->passes())
			  {
			  	
			    // path is root/uploads
			    $destinationPath = 'documents';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    $lastFileId = getLastTableId('document_files');
			    if($lastFileId=='')
			    {
			    	$lastFileId = 0;
			    }
			    else
			    {
			    	$lastFileId++;
			    }
			    $filename = $temp[0].'_'.$lastFileId.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
				   
				   $data = array('document_id'=>$doc_id,'file_name'=>$filename,'type'=>1);
				   if(count($data)>0)
					{
						Document::insertBatch('document_files',$data);
					}
				   //return Response::json('success', 200);
				   
				} 
				else 
				{
				   $errors .= json('error', 400);
				}

		    
			  } 
			  else 
			  {
			    // redirect back with errors.
			    return \Redirect::back()->withErrors($validator);
			  }
			//}

		}
	}

	//download file from server
	public function downloadDoc($file_id=0)
	{
		//get file name from database
		$fileName = getFileName($file_id);
        //public path for file
        $file= public_path(). "/documents/".$fileName;
        //download file
        return \Response::download($file, $fileName);
	}

	//remove file from folder
	public function deleteFile($file_id=0)
	{
		//get file name from database
		$fileName = getFileName($file_id);
		$file= public_path(). "/documents/".$fileName;
		if(File::delete($file))
		{
			return "<div class='alert alert-success'>File Deleted Successfully!</div>";
		}
		else
		{
			return "<script>alert(\"<div class='alert alert-danger'>Error!</div>\")</script>";
		}
	}


}
?>