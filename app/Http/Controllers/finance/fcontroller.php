<?php
namespace app\Http\Controllers\finance;

use app\Http\Controllers\Controller;
use App\models\finance\financeNew;
use View;
use Input;
use Redirect;
use Auth;
use Illuminate\Support\Collection;

class FController extends Controller
{
    /**
     * Instantiate a new docsController instance.
     */
    public function __construct()
    {
        if(!Auth::check())
        {
            return Redirect::route('getLogin');
        }
    }

    public function new_info()
    {
        return View::make('finance.new_info');
    }

    public function insert_info()
    {
        $object = new financeNew;
        $object->serial = Input::get('serial');
        $object->marji_marbota = Input::get('marji_marbota');
        $object->hokum_no = Input::get('hokum_no');
        $object->hokum_date = ymd_format(Input::get('hokum_date'));
        $object->maktob_no = Input::get('maktob_no');
        $object->maktob_date = ymd_format(Input::get('maktob_date'));
        $object->sadira_warida = Input::get('sadira_warida');
        $object->sadira_warida_date = ymd_format(Input::get('sadira_warida_date'));
        $object->mursal = Input::get('mursal');
        $object->mursal_elaih = Input::get('mursal_elaih');
        $object->rasidat_no = Input::get('rasidat_no');
        $object->rasidat_date = ymd_format(Input::get('rasidat_date'));
        $object->mulahizat = Input::get('mulahizat');

        function generateRandomString($length = 14) {
            return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
        }
        $object->ide = generateRandomString();

        if($object->save()){
            return Redirect::to('/finance/new_info')
                ->with('success', '<div class="alert alert-success" role="alert">success !</div>');
        }
        else{
            return Redirect::to('finance/new_info')
                ->with('failed', '<div class="alert alert-danger" role="alert">Error</div>');
        }
    }


    public function listAll()
    {
        return View::make("finance.info_list");
    }

    public function getAllData()
    {
        $records = financeNew::listAll();
        $collection = new Collection($records);
        return \Datatable::collection($collection)
                            ->showColumns('serial','warida_sadira','marji_marbota','hokum_date','mursal','mursal_elaih')
                            ->addColumn('more',function($option){
                                return '<a style="font-weight:bold;" href="'.route('details',$option->ide).'">باز</a>';
                            })
                            ->make();

    }


    public function details($id=0)
    {
        $data['record'] = financeNew::details($id);
        return View::make('finance.details', $data);
    }
}