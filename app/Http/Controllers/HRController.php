<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\session;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use App\Notifications\NewProject;
use App\models\Requests;
use App\User;
use Validator;
use Redirect;

class HRController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    // Get Language
    $lang = get_language();
    $data['lang'] = $lang;
    // Get Data
    $data['records'] = array();
    if(Input::get('ajax') == 1)
    {
        // Load view to show result
        return view('egov/hr/list_ajax',$data);
    }
    else
    {
        // Load view to show result
        return view('egov/hr/list',$data);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $lang = get_language();
    $lang  = $lang;
    // Get Provinces
    $provinces = getAllProvinces();
    // Get Provinces
    $degree = getAllEducationDegrees();
    // Load view to show result
    return view('egov/hr/create',compact('lang','provinces','degree'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // Validate the request...
    $validates = $request->validate([
        'doc_type'      => 'required',
        'department_id' => 'required',
        'request_date'  => 'required',
    ]);
    // Language
    $lang = get_language();
    // Generate URN
    $urn = GenerateURN('requests','urn');
    // Insert Record
    $record = new Requests;
    $record->urn           = $urn;
    $record->request_date  = dateProvider($request->request_date,$lang);
    $record->doc_type      = $request->doc_type;
    $record->department_id = $request->department_id;
    $record->doc_number    = $request->request_number;
    $record->goals         = $request->goals;
    $record->description   = $request->description;
    $record->created_at    = date('Y-m-d H:i:s');
    $record->created_by    = userid();
    $record->department    = departmentid();
    $record->save();
    if($record->id>0)
    {
    
      Session()->flash('success', __("global.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($enc_id)
  {
    session(['current_section' => 'pmis_request']);
    // Get language
    $lang = get_language();
    // Dycript the id
    $id = decrypt($enc_id);
    //Get Record
    $record = Requests::find($id);
    $summary = $record;
    // Check if record is shared 
    $shared = $record->share()->where('share_from_code','pmis_request')->whereNull('deleted_at')->first();
     // Get Attachments
    $attachments = getAttachments('requests_attachments',0,$id,'request');
    $section = 'request';
    //Pass Attachments table
    $table = 'requests_attachments';
    // Get Allowed Sections for Sharing
    $sections = SectionSharings::where('code','pmis_request')->get();
    // Load view to show result
    return view('pmis/requests/view',compact('lang','enc_id','record','summary','shared','attachments','section','table','sections'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($enc_id)
  {
    $lang = get_language();
    $data['lang'] = $lang;
    // Dycript the id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get Departments
    $data['deps']  = HRDepartments::select('id','name_'.$lang.' as name')->get();
    // Get Document Type
    $data['types'] = Documents::select('id','name_'.$lang.' as name')->get();
    // Get Record
    $data['record'] = Requests::find($id);
    // Load view to show result
    return view('pmis/requests/edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(RequestRequest $request)
  {
    $lang = get_language();
    $id = decrypt(Input::get('enc_id'));
    $req = Requests::find($id);
    $req->request_date  = dateProvider($request->request_date,$lang);
    $req->department_id = $request->department_id;
    $req->doc_type      = $request->doc_type;
    $req->doc_number    = $request->request_number;
    $req->goals         = $request->goals;
    $req->description   = $request->description;
    $req->updated_by    = userid();
    $updated = $req->save();
    if($updated)
    {
      Session()->flash('success', __("global.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("global.failed_msg"));
    }
  }

}
?>