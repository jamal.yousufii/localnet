<?php
namespace App\Http\Controllers\document_managment;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\http\Controllers\document_managment\Helpers;
use App\models\document_managment\department;
use App\models\document_managment\document_model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Validator;
use Response;
use File;
use Excel;
// echo "This System is Down use the new system which we have emailed you"; exit;  
// echo "<h3>سیستم مدیریت اسناد از را از آدرس زیر قابل دسترس میباشد<h3> <a hrfe='odf.aop.gov.af'>odf.aop.gov.af<a/> <br> در صورت داشتن مشکل به این شماره به تماس شوید. 93749068280";  exit;  


class search_doc_controller extends Controller
{
	public function search_live()
	{
		$search = trim(Input::get('field_value'));
		$cat_id = trim(Input::get('cat_id'));
		$type_id = trim(Input::get('type_id'));


			//search ahkam and farament data
		if ($cat_id == '1' OR $cat_id == '4') {
			$date = explode('-', $search);
			if (count($date) == 3) {
				$date_val = toGregorian(gregorian_format(Input::get('field_value')));

				$rows = \DB::table('document_managment.executive_documents as document')
					->select('document.*', 'dept.name as dept_name')
					->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
					->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
					->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
					->where('emp.department', getMyDepartmentId())
					->whereRaw(
						" document.status= 0 AND document.document_type = " . $cat_id . " AND emp.department=" . getMyDepartmentId() . " AND (document.date like '%" . $date_val . "%'

					)"
					)
					->orderby('document.id', 'desc')->get();
				$data['rows'] = $rows;
				return view::make('document_managment.search_hakom_farman', $data);
			}

			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
				->whereRaw(
					" document.status= 0 AND document.document_type = " . $cat_id . " AND emp.department=" . getMyDepartmentId() . " AND (document.number_documents like '%" . $search . "%'
					OR dept.name like '%" . $search . "%'
					OR document.description like '%" . $search . "%'
					OR document.marji like '%" . $search . "%'
					)"
				)
				->orderby('document.id', 'desc')->get();
			$data['rows'] = $rows;
			return view::make('document_managment.search_hakom_farman', $data);


						//get all search of makotob and farman
		} elseif ($cat_id == '2' OR $cat_id == '3' OR $cat_id == '16') {
			$search = trim(Input::get('field_value'));
			$cat_id = trim(Input::get('cat_id'));

			$date = explode('-', $search);
			if (count($date) == 3) {
				$date_val = toGregorian(gregorian_format(Input::get('field_value')));

				$rows = \DB::table('document_managment.executive_documents as document')
					->select('document.*', 'dept.name as dept_name')
					->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
					->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
					->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
					->whereRaw(
						" document.status= 0 AND document.document_type = " . $cat_id . " AND emp.department=" . getMyDepartmentId() . " AND (document.date like '%" . $date_val . "%'

					)"
					)
					->orderby('document.id', 'desc')->get();
				$data['rows'] = $rows;

				return view::make('document_managment.search_maktob_peshnahad', $data);
			}

			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
				->whereRaw(
					" document.status= 0 AND document.document_type = " . $cat_id . " AND emp.department=" . getMyDepartmentId() . "  AND (document.number_documents like '%" . $search . "%'
					OR dept.name like '%" . $search . "%'
					OR document.description like '%" . $search . "%'
					OR document.result_remark like '%" . $search . "%'
					OR document.marji like '%" . $search . "%'
					)"
				)
				->orderby('document.id', 'desc')->get();
			$data['rows'] = $rows;
			return view::make('document_managment.search_maktob_peshnahad', $data);


					//search f c 9 and istalam
		} elseif ($cat_id == '5' || $cat_id == '6' || $cat_id == '10') {
			$date = explode('-', $search);
			if (count($date) == 3) {
				$date_val = toGregorian(gregorian_format(Input::get('field_value')));

				$rows = \DB::table('document_managment.executive_documents as document')
					->select('document.*', 'dept.name as dept_name')
					->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
					->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
					->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
					->whereRaw(
						" document.status= 0 AND document.document_type = " . $cat_id . " AND emp.department=" . getMyDepartmentId() . " AND (document.date like '%" . $date_val . "%'

									)"
					)
					->orderby('document.id', 'desc')->get();
				$data['rows'] = $rows;
				return view::make('document_managment.search_fecen_istalam', $data);
			}

			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
				->whereRaw(
					" document.status= 0 AND document.document_type = " . $cat_id . " AND emp.department=" . getMyDepartmentId() . " AND (document.number_documents like '%" . $search . "%'
									OR dept.name like '%" . $search . "%'
									OR document.description like '%" . $search . "%'
									OR document.result_remark like '%" . $search . "%'
									OR document.name like '%" . $search . "%'
									OR document.head_of_office like '%" . $search . "%'
									)"
				)
				->orderby('document.id', 'desc')->get();
			$data['rows'] = $rows;
			return view::make('document_managment.search_fecen_istalam', $data);
		} elseif ($cat_id == '13') {
			$date = explode('-', $search);
			if (count($date) == 3) {
				$date_val = toGregorian(gregorian_format(Input::get('field_value')));

				$rows = \DB::table('document_managment.executive_documents as document')
					->select('document.*', 'dept.name as dept_name')
					->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
					->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
					->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
					->whereRaw(
						" document.status= 0 AND document.document_type = " . $cat_id . " AND emp.department=" . getMyDepartmentId() . " AND (document.date like '%" . $date_val . "%'

											)"
					)
					->orderby('document.id', 'desc')->get();
				$data['rows'] = $rows;
				return view::make('document_managment.search_copy_som', $data);
			}

			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
				->whereRaw(
					" document.status= 0 AND document.document_type = " . $cat_id . " AND emp.department=" . getMyDepartmentId() . " AND (document.number_documents like '%" . $search . "%'
											OR dept.name like '%" . $search . "%'
											OR document.description like '%" . $search . "%'
											OR document.result_remark like '%" . $search . "%'
											OR document.name like '%" . $search . "%'
											OR document.head_of_office like '%" . $search . "%'
											)"
				)
				->orderby('document.id', 'desc')->get();
			$data['rows'] = $rows;
			return view::make('document_managment.search_copy_som', $data);
		} elseif ($cat_id == '15') {
			$date = explode('-', $search);
			if (count($date) == 3) {
				$date_val = toGregorian(gregorian_format(Input::get('field_value')));

				$rows = \DB::table('document_managment.executive_documents as document')
					->select('document.*', 'dept.name as dept_name')
					->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
					->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
					->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
					->whereRaw(
						" document.status= 0 AND document.document_type = " . $cat_id . " AND emp.department=" . getMyDepartmentId() . " AND (document.date like '%" . $date_val . "%'

										)"
					)
					->orderby('document.id', 'desc')->get();
				$data['rows'] = $rows;
				return view::make('document_managment.search_hakom_farman', $data);
			}

			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
				->whereRaw(
					" document.status= 0 AND document.document_type = " . $cat_id . " AND emp.department=" . getMyDepartmentId() . " AND (document.number_documents like '%" . $search . "%'
										OR dept.name like '%" . $search . "%'
										OR document.description like '%" . $search . "%'
										OR document.marji like '%" . $search . "%'
										)"
				)
				->orderby('document.id', 'desc')->get();
			$data['rows'] = $rows;
			return view::make('document_managment.search_hakom_farman', $data);
		}
	}
		//peshnahad and maktob sadira and warida search
	public function search_sadera_warida()
	{
		$search = trim(Input::get('field_value'));
		$cat_id = trim(Input::get('cat_id'));
		$type_id = trim(Input::get('type_id'));
		$date = explode('-', $search);
		if (count($date) == 3) {
			$date_val = toGregorian(gregorian_format(Input::get('field_value')));

			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
				->whereRaw(
					" document.status= 0 AND document.document_type = " . $cat_id . " AND emp.department=" . getMyDepartmentId() . " AND document.type_id=" . $type_id . " AND (document.date like '%" . $date_val . "%'


				)"
				)
				->orderby('document.id', 'desc')->get();
			$data['rows'] = $rows;
			return view::make('document_managment.search_maktob_peshnahad', $data);
		}

		$rows = \DB::table('document_managment.executive_documents as document')
			->select('document.*', 'dept.name as dept_name')
			->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
			->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
			->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
			->whereRaw(
				" document.status= 0 AND document.document_type = " . $cat_id . " AND emp.department=" . getMyDepartmentId() . " AND document.type_id=" . $type_id . " AND (document.number_documents like '%" . $search . "%'
				OR dept.name like '%" . $search . "%'
				OR document.name like '%" . $search . "%'
				OR document.head_of_office like '%" . $search . "%'
				OR document.description like '%" . $search . "%'
				OR document.result_remark like '%" . $search . "%'
				)"
			)
			->orderby('document.id', 'desc')->get();
		$data['rows'] = $rows;
		return view::make('document_managment.search_maktob_peshnahad', $data);
	}

	public function get_all_document()
	{
		$name = trim(Input::get('name'));
		$head_of_office = trim(Input::get('head_of_office'));
		$all_search = trim(Input::get('all_search'));
		$result = trim(Input::get('result'));

		if ($all_search == '') {
			if ($result != "") {
				$rows = \DB::table('document_managment.executive_documents as document')
					->select('document.*', 'dept.name as dept_name')
					->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
					->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
					->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
					->whereRaw(
						" document.status= 0 AND emp.department=" . getMyDepartmentId() . " AND ( document.result = '" . $result . "'
						)"
					)
					->orderby('document.id', 'desc');
				$data['records'] = $rows->get();
				return view::make('document_managment.search_all_document', $data);
			}

			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
				->whereRaw(
					" document.status= 0 AND emp.department=" . getMyDepartmentId() . " AND (document.name like '%" . $name . "%'
						AND document.head_of_office like '%" . $head_of_office . "%'
						)"
				)
				->orderby('document.id', 'desc');
			$data['records'] = $rows->get();
			return view::make('document_managment.search_all_document', $data);
		} else {
			$date = explode('-', $all_search);
			if (count($date) == 3) {
				$date_val = toGregorian(gregorian_format(Input::get('all_search')));

				$rows = \DB::table('document_managment.executive_documents as document')
					->select('document.*', 'dept.name as dept_name')
					->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
					->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
					->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
					->whereRaw(
						" document.status= 0 AND emp.department=" . getMyDepartmentId() . " AND

						 document.date ='" . $date_val . "'
						 OR document.result_date ='" . $date_val . "'

						"
					)
					->orderby('document.id', 'desc')->get();
				$data['records'] = $rows;
				return view::make('document_managment.search_all_document', $data);
			} else {
				$rows = \DB::table('document_managment.executive_documents as document')
					->select('document.*', 'dept.name as dept_name')
					->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
					->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
					->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
					->whereRaw(
						" document.status= 0 AND emp.department=" . getMyDepartmentId() . " AND (document.number_documents like '%" . $all_search . "%'
						OR dept.name like '%" . $all_search . "%'
						OR document.name like '%" . $all_search . "%'
						OR document.head_of_office like '%" . $all_search . "%'
						OR document.related_to like '%" . $all_search . "%'
						OR document.receiver like '%" . $all_search . "%'
						OR document.marji like '%" . $all_search . "%'
						OR document.result_remark like '%" . $all_search . "%'
						OR document.description like '%" . $all_search . "%'
						)"
					)
					->orderby('document.id', 'desc')->get();
				$data['records'] = $rows;
				return view::make('document_managment.search_all_document', $data);
			}
		}
	}//hadayat document function
	public function search_live_hadayat()
	{
		$search = trim(Input::get('field_value'));
		$cat_id = trim(Input::get('cat_id'));
		$date = explode('-', $search);
		if (count($date) == 3) {
			$date_val = toGregorian(gregorian_format(Input::get('field_value')));

			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
				->whereRaw(
					" document.status= 0 AND emp.department=" . getMyDepartmentId() . " AND document.document_type = " . $cat_id . " AND (document.date like '%" . $date_val . "%' OR document.result_date like '%" . $date_val . "%'


						)"
				)
				->orderby('document.id', 'desc')->get();
			$data['rows'] = $rows;
			return view::make('document_managment.search_hadayat_doc', $data);
		}

		$rows = \DB::table('document_managment.executive_documents as document')
			->select('document.*', 'dept.name as dept_name')
			->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
			->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
			->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
			->whereRaw(
				" document.status= 0 AND emp.department=" . getMyDepartmentId() . " AND document.document_type = " . $cat_id . " AND (document.number_documents like '%" . $search . "%'
						OR dept.name like '%" . $search . "%'
						OR document.receiver like '%" . $search . "%'
						OR document.related_to like '%" . $search . "%'
						OR document.description like '%" . $search . "%'
						)"
			)
			->orderby('document.id', 'desc')->get();
		$data['rows'] = $rows;
		return view::make('document_managment.search_hadayat_doc', $data);
	}

	public function get_all_yearly_document()
	{
		$date_from = Input::get('from_year');
		$date_to = Input::get('to_year');
		$doc_type = trim(Input::get('doc_type'));
		$result = trim(Input::get('result'));
		$department_id = trim(Input::get('department'));

		if ($date_from and $date_to != "") {
			$from_date = toGregorian(gregorian_format($date_from));
			$to_date = toGregorian(gregorian_format($date_to));

			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id');
			$rows->where('emp.department', getMyDepartmentId());
			$rows->where('document.status', '0');
			$rows->whereBetween('date', [$from_date, $to_date]);
			if ($result != "") $rows->where('result', $result);
			if ($doc_type != "") $rows->where('document_type', $doc_type);
			if ($department_id != "") $rows->where('document.department_id', $department_id);

			$data['records'] = $rows->get();
			return view::make('document_managment.search_all_document', $data);
		} else {
			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id');
			$rows->where('emp.department', getMyDepartmentId());
			$rows->where('document.status', '0');
			if ($result != "") $rows->where('result', $result);
			if ($doc_type != "") $rows->where('document_type', $doc_type);
			if ($department_id != "") $rows->where('document.department_id', $department_id);

			$data['records'] = $rows->get();
			return view::make('document_managment.search_all_document', $data);
		}
	}

			//waraq darkhasti search
	public function search_live_waraq_darkhasti()
	{
		$search = trim(Input::get('field_value'));
		$cat_id = trim(Input::get('cat_id'));
		$date = explode('-', $search);
		if (count($date) == 3) {
			$date_val = toGregorian(gregorian_format(Input::get('field_value')));

			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
				->whereRaw(
					" document.status= 0  AND emp.department=" . getMyDepartmentId() . " AND document.document_type = " . $cat_id . " AND (document.date like '%" . $date_val . "%' OR document.result_date like '%" . $date_val . "%'


						)"
				)
				->orderby('document.id', 'desc')->get();
			$data['rows'] = $rows;
			return view::make('document_managment.search_waraq_darkhasti', $data);
		}

		$rows = \DB::table('document_managment.executive_documents as document')
			->select('document.*', 'dept.name as dept_name')
			->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
			->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
			->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id')
			->whereRaw(
				" document.status= 0  AND emp.department=" . getMyDepartmentId() . " AND document.document_type = " . $cat_id . " AND (document.name like '%" . $search . "%'
						OR dept.name like '%" . $search . "%'
						OR document.result_remark like '%" . $search . "%'
						OR document.description like '%" . $search . "%'
						)"
			)
			->orderby('document.id', 'desc')->get();
		$data['rows'] = $rows;
		return view::make('document_managment.search_waraq_darkhasti', $data);
	}
	public function export_excel()
	{
		$date_from = Input::get('from_year');
		$date_to = Input::get('to_year');
		$doc_type = trim(Input::get('doc_type'));
		$result = trim(Input::get('result'));
		$department_id = trim(Input::get('department'));

		if ($date_from and $date_to != "") {
			$from_date = toGregorian(gregorian_format($date_from));
			$to_date = toGregorian(gregorian_format($date_to));

			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id');
			$rows->where('emp.department', getMyDepartmentId());
			$rows->where('document.status', '0');
			$rows->whereBetween('date', [$from_date, $to_date]);
			if ($result != "") $rows->where('result', $result);
			if ($doc_type != "") $rows->where('document_type', $doc_type);
			if ($department_id != "") $rows->where('document.department_id', $department_id);

			$data = $rows->get();

			$curr_date = date('Y-m-d');
			Excel::load(
				'excel_template/document_data.xlsx',
				function($file) use ($data) {
			//Excel::create('Filename', function($file) use($data){
					$file->sheet(
						$file->getActiveSheetIndex(1),
						function($sheet) use ($data) {
							$row = 3;
							$sheet->setFreeze('A3');

							foreach ($data AS $item) {
								$sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true);
					//
					// $summary = preg_replace("/&nbsp;/",' ',$item->product_description);
					// $text = preg_replace("/&ndash;/",'-',$summary);
								if ($item->document_type == '1') {
									$document_type = "حکم";
								} elseif ($item->document_type == '2') {
									$document_type = "مکتوب";
								} elseif ($item->document_type == '3') {
									$document_type = "پشنهاد";
								} elseif ($item->document_type == '4') {
									$document_type = "فرمان";
								} elseif ($item->document_type == '5') {
									$document_type = "استعلام";
								} elseif ($item->document_type == '6') {
									$document_type = "ف س ۹";
								} elseif ($item->document_type == '7') {
									$document_type = "مکتوب هدایتی";
								} elseif ($item->document_type == '8') {
									$document_type = "پیشنهاد هدایتی";
								} elseif ($item->document_type == '9') {
									$document_type = "ف س ۹ هدایتی";
								} elseif ($item->document_type == '10') {
									$document_type = "تعيينات";
								} elseif ($item->document_type == '11') {
									$document_type = "مکتوب وارده";
								} elseif ($item->document_type == '12') {
									$document_type = "استعلام هدایتی";
								} elseif ($item->document_type == '13') {
									$document_type = "کاپی سوم";
								} elseif ($item->document_type == '14') {
									$document_type = "ورقه درخواستی";
								} elseif ($item->document_type == '15') {
									$document_type = "مصوبات";
								} elseif ($item->document_type == '16') {
									$document_type = "هدایت";
								}
					//set value of sadira and warida
								if ($item->type_id == '1') {
									$type_name = 'صادره';
								} elseif ($item->type_id == '2') {
									$type_name = 'وارده';
								} elseif ($item->type_id == '') {
									$type_name = '';
								}

					//set value of ajra na ajra and hafzya
								if ($item->result == '1') {
									$doc_result = 'اجراه';
								} elseif ($item->result == '2') {
									$doc_result = 'نااجراه';
								} elseif ($item->result == '3') {
									$doc_result = 'حفظیه';
								} elseif ($item->result == '') {
									$doc_result = '';
								}

								$sheet->setHeight($row, 20);
								$sheet->setCellValue('P' . $row . '', $row - 2);
								$sheet->setCellValue('O' . $row . '', $item->number_documents);
								$sheet->setCellValue('N' . $row . '', $document_type);
								$sheet->setCellValue('M' . $row . '', $item->name);
								$sheet->setCellValue('L' . $row . '', $item->head_of_office);
								$sheet->setCellValue('K' . $row . '', checkEmptyDate($item->date));
								$sheet->setCellValue('J' . $row . '', $type_name);
								$sheet->setCellValue('I' . $row . '', $item->dept_name);
								$sheet->setCellValue('H' . $row . '', $item->marji);
								$sheet->setCellValue('G' . $row . '', $item->description);
								$sheet->setCellValue('F' . $row . '', $item->related_to);
								$sheet->setCellValue('E' . $row . '', $item->receiver);
								$sheet->setCellValue('D' . $row . '', $doc_result);
								$sheet->setCellValue('C' . $row . '', $item->result_remark);
								$sheet->setCellValue('B' . $row . '', checkEmptyDate($item->result_date));
								$sheet->setCellValue('A' . $row . '', $item->file_address);


					//$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));

								$row++;
							}

							$sheet->setBorder('A3:P' . ($row - 1) . '', 'thin');
						}
					);
				}
			)->export('xlsx');
		} else {
			$rows = \DB::table('document_managment.executive_documents as document')
				->select('document.*', 'dept.name as dept_name')
				->leftjoin('document_managment.department AS dept', 'document.department_id', '=', 'dept.id')
				->leftjoin('auth.users AS users', 'users.id', '=', 'document.created_by')
				->leftjoin('hr.employees AS emp', 'emp.id', '=', 'users.employee_id');
			$rows->where('emp.department', getMyDepartmentId());
			$rows->where('document.status', '0');
			if ($result != "") $rows->where('result', $result);
			if ($doc_type != "") $rows->where('document_type', $doc_type);
			if ($department_id != "") $rows->where('document.department_id', $department_id);

			$data = $rows->get();

			$curr_date = date('Y-m-d');
			Excel::load(
				'excel_template/document_data.xlsx',
				function($file) use ($data) {
	  		//Excel::create('Filename', function($file) use($data){
					$file->sheet(
						$file->getActiveSheetIndex(1),
						function($sheet) use ($data) {
							$row = 3;
							$sheet->setFreeze('A3');

							foreach ($data AS $item) {
								$sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true);
					//
					// $summary = preg_replace("/&nbsp;/",' ',$item->product_description);
					// $text = preg_replace("/&ndash;/",'-',$summary);
								if ($item->document_type == '1') {
									$document_type = "حکم";
								} elseif ($item->document_type == '2') {
									$document_type = "مکتوب";
								} elseif ($item->document_type == '3') {
									$document_type = "پشنهاد";
								} elseif ($item->document_type == '4') {
									$document_type = "فرمان";
								} elseif ($item->document_type == '5') {
									$document_type = "استعلام";
								} elseif ($item->document_type == '6') {
									$document_type = "ف س ۹";
								} elseif ($item->document_type == '7') {
									$document_type = "مکتوب هدایتی";
								} elseif ($item->document_type == '8') {
									$document_type = "پیشنهاد هدایتی";
								} elseif ($item->document_type == '9') {
									$document_type = "ف س ۹ هدایتی";
								} elseif ($item->document_type == '10') {
									$document_type = "تعيينات";
								} elseif ($item->document_type == '11') {
									$document_type = "مکتوب وارده";
								} elseif ($item->document_type == '12') {
									$document_type = "استعلام هدایتی";
								} elseif ($item->document_type == '13') {
									$document_type = "کاپی سوم";
								} elseif ($item->document_type == '14') {
									$document_type = "ورقه درخواستی";
								} elseif ($item->document_type == '15') {
									$document_type = "مصوبات";
								} elseif ($item->document_type == '16') {
									$document_type = "هدایت";
								}
					//set value of sadira and warida
								if ($item->type_id == '1') {
									$type_name = 'صادره';
								} elseif ($item->type_id == '2') {
									$type_name = 'وارده';
								} elseif ($item->type_id == '') {
									$type_name = '';
								}

					//set value of ajra na ajra and hafzya
								if ($item->result == '1') {
									$doc_result = 'اجراه';
								} elseif ($item->result == '2') {
									$doc_result = 'نااجراه';
								} elseif ($item->result == '3') {
									$doc_result = 'حفظیه';
								} elseif ($item->result == '') {
									$doc_result = '';
								}

								$sheet->setHeight($row, 20);
								$sheet->setCellValue('P' . $row . '', $row - 2);
								$sheet->setCellValue('O' . $row . '', $item->number_documents);
								$sheet->setCellValue('N' . $row . '', $document_type);
								$sheet->setCellValue('M' . $row . '', $item->name);
								$sheet->setCellValue('L' . $row . '', $item->head_of_office);
								$sheet->setCellValue('K' . $row . '', checkEmptyDate($item->date));
								$sheet->setCellValue('J' . $row . '', $type_name);
								$sheet->setCellValue('I' . $row . '', $item->dept_name);
								$sheet->setCellValue('H' . $row . '', $item->marji);
								$sheet->setCellValue('G' . $row . '', $item->description);
								$sheet->setCellValue('F' . $row . '', $item->related_to);
								$sheet->setCellValue('E' . $row . '', $item->receiver);
								$sheet->setCellValue('D' . $row . '', $doc_result);
								$sheet->setCellValue('C' . $row . '', $item->result_remark);
								$sheet->setCellValue('B' . $row . '', checkEmptyDate($item->result_date));
								$sheet->setCellValue('A' . $row . '', $item->file_address);


					//$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));

								$row++;
							}

							$sheet->setBorder('A3:P' . ($row - 1) . '', 'thin');
						}
					);
				}
			)->export('xlsx');
		}
	}
}

