<?php
namespace App\Http\Controllers\document_managment;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\document_managment\department;
use App\models\document_managment\document_model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Validator;
use Response;
use File;
use Str;
use App\Http\Controllers\document_managment\Helpers;
use Excel;
// echo "<h3>سیستم مدیریت اسناد از را از آدرس زیر قابل دسترس میباشد<h3> <a hrfe='odf.aop.gov.af'>odf.aop.gov.af<a/> <br> در صورت داشتن مشکل به این شماره به تماس شوید. 93749068280";  exit;  

class EmployeesContrller extends Controller {

	public function get_all_employees(){


		$employee =\DB::connection('hr')->table('hr.employees as emp')->select('emp.*')
		->where('emp.department',getMyDepartmentId())
		->get();
				// get and return the department id from employee record
				return view::make('document_managment.get_employees',array('employee'=>$employee));


}
public function search(){

	$search=trim(Input::get('field_value'));
		$employee =\DB::connection('hr')->table('hr.employees as emp')->select('emp.*')
		->whereRaw(" emp.department=".getMyDepartmentId()." AND (emp.name_dr like '%".$search."%'
			OR emp.father_name_dr like '%".$search."%'
			OR emp.current_position_dr like '%".$search."%'
			OR emp.email like '%".$search."%'
			OR emp.phone like '%".$search."%'
			OR emp.ext1 like '%".$search."%'
			)")
		->get();
		return view::make('document_managment.search_employee',array('employee'=>$employee));


}
public function export_employee(){

	$employee =\DB::connection('hr')->table('hr.employees as emp')->select('emp.*')
	->where('emp.department',getMyDepartmentId());

	$data = $employee->get();
	$curr_date = date('Y-m-d');
	Excel::load('excel_template/employee_list.xlsx', function($file) use($data){
	//Excel::create('Filename', function($file) use($data){
	$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){
	$row = 3;
	$sheet->setFreeze('A3');

	foreach($data AS $item)
	{
		$sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true);

		$sheet->setHeight($row, 20);
		$sheet->setCellValue('G'.$row.'',$row-2);
		$sheet->setCellValue('F'.$row.'',$item->name_dr);
		$sheet->setCellValue('E'.$row.'',$item->father_name_dr);
		$sheet->setCellValue('D'.$row.'',$item->current_position_dr);
		$sheet->setCellValue('C'.$row.'',$item->email);
		$sheet->setCellValue('B'.$row.'',$item->phone);
		$sheet->setCellValue('A'.$row.'',$item->ext2);


		//$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));

		$row++;
	}

	$sheet->setBorder('A3:G'.($row-1).'', 'thin');

	});

})->export('xlsx');


}
}
