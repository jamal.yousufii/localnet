<?php
namespace App\Http\Controllers\document_managment;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\document_managment\department;
use App\models\document_managment\document_model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Validator;
use Response;
use File;
use Str;
// echo "This System is Down, use the new system which we have emailed you"; exit;  
// echo "<h3>سیستم مدیریت اسناد از را از آدرس زیر قابل دسترس میباشد<h3> <a hrfe='odf.aop.gov.af'>odf.aop.gov.af<a/> <br> در صورت داشتن مشکل به این شماره به تماس شوید. 93749068280";  exit;  


class waraq_darkhasti_controller extends Controller {


public function load_insert(){

	$department=department::get_department();

		return view::make("document_managment.insert_waraq_darkhasti",['department'=>$department]);
}

public function insert_waraq_darkhasti(){

		  $validates = Validator::make(Input::all(), array(
			"document_type"						=> "required",
			"name"				        		=> "required",
			// "department_id"						=> "required"

			));

		if($validates->fails())
		{
			return Redirect::route('load_waraq_darkhasti_doc')->withErrors($validates)->withInput();
		}
		else
		{

		    //check the date type if it's shamsi or miladi.
			$date = toGregorian(gregorian_format(Input::get('date')));
			// $result_date = toGregorian(gregorian_format(Input::get('result_date')));

		    // get the form data.
		    $data = array(
		    		"name"	    				=> Input::get('name'),
		    		"date"							=> $date,
		    		"description"				=> Input::get('description'),
		    		"document_type"			=> Input::get('document_type'),
		    		"result"		    	 	=> Input::get('result'),
		    		"result_remark"		 	=> Input::get('result_remark'),
		    		"file_address"		 	=> Input::get('file_address'),
		    		// "result_date"		    => $result_date,
		    		"created_by"				=> Auth::user()->id,


		    	);
			$insertedRecordID=\DB::connection('document_managment')->table("executive_documents")->insert($data);
		    if($insertedRecordID){
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 1;
				$file_data = array();

				if(Input::hasFile('files'))
				{
					foreach($files as $file)
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    // path is root/uploads
								$destinationPath ='uploads_doc/'.getMyDepartmentId().'/';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);

						    $lastid = DB::connection('document_managment')->table('executive_documents')->orderBy('id', 'desc')->pluck('id');


					    	$lastFileId = $lastid;
							if($auto == 1)
						    $filename = "Attachment_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Attachment_".$lastFileId."_".$auto.".".$extension;
							if(!file_exists($destinationPath)) {
							File::makeDirectory($destinationPath);
							}
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success)
						    {
						   $lastid = DB::connection('document_managment')->table('executive_documents')->orderBy('id', 'desc')->pluck('id');
							    $data = array(
						   					'file_name'				=> $filename,
						   					'ex_doc_id'				=> $lastid,
						   				);
						    	//call the model function to insert the data into upload table.
						    $insertfile=\DB::connection('document_managment')->table("uploads")->insert($data);
							}
							else
							{
							    // redirect back with errors.
					    		return Redirect::route('load_waraq_darkhasti_doc')->withErrors($validator);
							}
						}$auto ++;


					}
				}
		    	// get the the log data and insert it into the log table.

		    	return Redirect::route('load_waraq_darkhasti_doc')->with("success","Information successfully Added.");
	        }
	        else
	        {
	            return Redirect::route('load_waraq_darkhasti_doc')->with("fail","An error occured please try again or contact system developer.");
	        }}
	}

	public function delete_waraq_darkhasti_doc($id){

		$row=document_model::find($id);

			$row->status='1';

		$row->save();

			if($row)
	        {
	            return \Redirect::route("document_recordsList")->with("success","You have successfuly deleted the record <span style='color:red;font_weight:bold;'></span>");
	        }
	        else
	        {
	            return \Redirect::route("document_recordsList")->with("fail","An error occured please try again.");
	        }
				}

	public function select_row_doc_waraq_darkhasti($id){
	$data=document_model::find($id);

		$department=department::get_department();

		$files=\DB::connection('document_managment')->table('uploads')->select('uploads.*','uploads.id as file_id')->where('ex_doc_id',$id)->get();

			return view::make('document_managment.update_waraq_darkhasti',array('data'=>$data,'department'=>$department,'files'=>$files));

	}

	public function update_waraq_darkhasti($id){

			$document= new document_model;
			$document=document_model::find($id);
			$validates = Validator::make(Input::all(), array(
			"document_type"						=> "required",
			"name"				        	=> "required",
			));

		if($validates->fails())
		{
			return Redirect::route('select_waraq_darkhasti_doc')->withErrors($validates)->withInput();
		}
		else
		{

		    //check the date type if it's shamsi or miladi.
			$date = toGregorian(gregorian_format(Input::get('date')));
			// $result_date = toGregorian(gregorian_format(Input::get('result_date')));


		    // get the form data.
		    $data = array(
					"name"	    				=> Input::get('name'),
					"date"							=> $date,
					"description"				=> Input::get('description'),
					"document_type"			=> Input::get('document_type'),
					"result"		    	 	=> Input::get('result'),
					"result_remark"		 	=> Input::get('result_remark'),
					"file_address"		 	=> Input::get('file_address'),

		    	// "result_date"		    => $result_date,
		    	"edit_by"		     	=> Auth::user()->id,

		    	);
			$insertedRecordID=\DB::connection('document_managment')->table("executive_documents")->where('id',$id)->update($data);
		    if($insertedRecordID){
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 50;
				$file_data = array();

				if(Input::hasFile('files'))
				{
					foreach($files as $file)
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    // path is root/uploads
								$destinationPath ='uploads_doc/'.getMyDepartmentId().'/';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);

						    $lastid = DB::connection('document_managment')->table('executive_documents')->orderBy('id', 'desc')->pluck('id');
						    $file_id = DB::connection('document_managment')->table('uploads')->orderBy('id', 'desc')->pluck('id');
						   $file_id++;

					    	$lastFileId = $lastid;
							if($auto == 50)
						    $filename = "Attachment_".$lastFileId."_".$file_id.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Attachment_".$lastFileId."_".$file_id.".".$extension;
							if(!file_exists($destinationPath)) {
								File::makeDirectory($destinationPath);
							}
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success)
						    {
						   $lastid = $id;
							    $data = array(
						   					'file_name'				=> $filename,
						   					'ex_doc_id'				=> $lastid,
						   				);
						    	//call the model function to insert the data into upload table.
						    $insertfile=\DB::connection('document_managment')->table("uploads")->insert($data);
							}
							else
							{
							    // redirect back with errors.
					    		return Redirect::route('select_waraq_darkhasti_doc')->withErrors($validator);
							}
						}$auto ++;


					}
				}
		    	// get the the log data and insert it into the log table.

		    	return Redirect::route('document_recordsList')->with("success","Information successfully updeted.");
	        }
	        else
	        {
	            return Redirect::route('select_waraq_darkhasti_doc')->with("fail","An error occured please try again or contact system developer.");
	        }}
	    }



}
