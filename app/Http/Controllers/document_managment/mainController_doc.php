<?php
namespace App\Http\Controllers\document_managment;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\document_managment\department;
use App\models\document_managment\document_model;
use App\http\Controllers\document_managment\Helpers;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Validator;
use Response;
use File;
use Str;
// echo "This System is Down use the new system which we have emailed you"; exit;  

// echo "<h3>سیستم مدیریت اسناد از را از آدرس زیر قابل دسترس میباشد<h3> <a hrfe='odf.aop.gov.af'>odf.aop.gov.af<a/> <br> در صورت داشتن مشکل به این شماره به تماس شوید. 93749068280";  exit;  

class mainController_doc extends Controller {

	public function loadRecordsList(){

		return view::make('document_managment.managment_list');
	}

	public function get_hakom(request $request){

	 $data=document_model::get_document_hakom();

		if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_hakom',$data);
		}
		else
		{
			//load view to show searchpa result
				return view::make('document_managment.get_hakom',$data);
		}
	}

	public function get_maktob(){

		$data=document_model::get_document_maktob();
		if(Input::get('ajax') == 1)
		{

				return view::make('document_managment.paginate_maktob',$data);
		}
		else
		{
				return view::make('document_managment.get_maktob',$data);

			}}
	public function get_copy_som(){

		$data=document_model::get_document_copy_som();
		if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_copy3',$data);
		}
		else
		{
				return view::make('document_managment.get_copy_som',$data);

			}}


	public function get_mateb_warada(){

		$data=document_model::get_makatab_warada();
		if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_maktob_warida',$data);
		}
		else
		{
				return view::make('document_managment.get_makatab_warada',$data);

			}}

	public function get_maktob_sadara(){

		$data=document_model::get_maktob_sadara();

			if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_maktob_sadira',$data);
		}
		else
		{
				return view::make('document_managment.get_maktob_sadara',$data);

			}}

	public function get_pashnahad(){

				$data=document_model::get_document_pashnahad();


			if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_pashnahad',$data);
		}
		else
		{


				return view::make('document_managment.get_pashnahad',$data);
			}
		}

	public function get_pashnahad_sadira(){

				$data=document_model::get_pashnahad_sadira();


			if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_pashnahad_sadira',$data);
		}
		else
		{


				return view::make('document_managment.get_pashnahad_sadira',$data);
			}
		}
	public function get_pashnahad_warida(){

				$data=document_model::get_pashnahad_warida();


			if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_pashnahad_warida ',$data);
		}
		else
		{


				return view::make('document_managment.get_pashnahad_warida',$data);
			}
		}

	public function get_farman(){

		$data=document_model::get_document_farman();


			if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_farman',$data);
		}
		else
		{


				return view::make('document_managment.get_farman',$data);

			}
		}

	public function get_istalam(){

		$data=document_model::get_document_istalam();


			if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_istalam',$data);
		}
		else
		{


				return view::make('document_managment.get_istalam',$data);

			}
		}

	public function get_feceno(){

		$data=document_model::get_document_feceno();


			if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_feceno',$data);
		}
		else
		{


				return view::make('document_managment.get_feceno',$data);

			}
		}

			//get get_waraqa_darkhasti data
	public function get_waraqa_darkhasti(){



		$data=document_model::get_document_waraqa_darkhasti();

			if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_waraqa_darkhasti',$data);
		}
		else
		{
				return view::make('document_managment.get_waraqa_darkhasti',$data);

			}}

	public function get_pahshnahad_hedayati(){

		$data=document_model::get_document_pashnahad_he();
				if(Input::get('ajax') == 1)
					{
							return view::make('document_managment.paginate_hadayati_data',$data);
					}
					else
					{

				return view::make('document_managment.get_pashnahad_hadayati',$data);

			}}

	public function get_feceno_hadayati(){

		$data=document_model::get_document_feceno_he();
			if(Input::get('ajax') == 1)
				{
						return view::make('document_managment.paginate_feceno_hedayati',$data);
				}
				else
				{

				return view::make('document_managment.get_feceno_hadayati',$data);

			}}
	public function get_istalam_hadayati_data(){

		$data=document_model::get_document_istalam_he();
			if(Input::get('ajax') == 1)
				{
						return view::make('document_managment.paginate_istalm_hedayati',$data);
				}
				else
				{

				return view::make('document_managment.get_istalam_hidayati',$data);

			}}

	public function get_tayenat(){

		$data=document_model::get_document_tayenat();
				if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_tayenat',$data);
		}
		else
		{


				return view::make('document_managment.get_tayenat',$data);

			}
			}
	public function get_mosawibat(){

		$data=document_model::get_document_mosawibat();
				if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_mosawibat',$data);
		}
		else
		{


				return view::make('document_managment.get_mosawibat',$data);

			}
			}
			//get get_hadayat
	public function get_hadayat(){

		$data=document_model::get_document_hadayat();
				if(Input::get('ajax') == 1)
		{
				return view::make('document_managment.paginate_hadayat',$data);
		}
		else
		{


				return view::make('document_managment.get_hadayat',$data);

			}
			}
	public function get_all_search(){


				return view::make('document_managment.get_all_search');

			}
	public function get_yearly_search(){

		$department=department::get_department();
				return view::make('document_managment.yearly_search',['department'=>$department]);

			}
	public function loadpage_insert(){
		$department=department::get_department();

		return view::make('document_managment.insert_document',['department'=>$department]);
	}

	public function insert_doc(Request $request){
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"document_type"						=> "required",
			"number"									=> "required"
			));

		if($validates->fails())
		{
			return Redirect::route('insertForm_document')->withErrors($validates)->withInput();
		}
		else
		{

		    //check the date type if it's shamsi or miladi.
			$date = toGregorian(gregorian_format(Input::get('date')));

		    // get the form data.
		    $data = array(
		    		"number_documents"	    => Input::get('number'),
		    		"date"									=> $date,
		    		"type_id"								=> Input::get('type_doc'),
		    		"department_id"					=> Input::get('department_id'),
		    		"description"						=> Input::get('description'),
		    		"document_type"					=> Input::get('document_type'),
		    		"name"									=> Input::get('name'),
		    		"result"								=> Input::get('result'),
		    		"marji"									=> Input::get('marji'),
		    		"file_address"					=> Input::get('file_address'),
		    		"result_remark"					=> Input::get('result_remark'),
		    		"head_of_office"				=> Input::get('head_of_office'),
		    		"created_by"						=> Auth::user()->id,
		    		"created_at"						=> \Carbon\Carbon::now(),

		    	);
			$insertedRecordID=\DB::connection('document_managment')->table("executive_documents")->insert($data);
		    if($insertedRecordID){
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 1;
				$file_data = array();

				if(Input::hasFile('files'))
				{
					foreach($files as $file)
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    // path is root/uploads
						    $destinationPath ='uploads_doc/'.getMyDepartmentId().'/';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);

						    $lastid = DB::connection('document_managment')->table('executive_documents')->orderBy('id', 'desc')->pluck('id');


					    	$lastFileId = $lastid;
							if($auto == 1)
						    $filename = "Attachment_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Attachment_".$lastFileId."_".$auto.".".$extension;
							if(!file_exists($destinationPath)) {
		            File::makeDirectory($destinationPath);
		          }
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success)
						    {
						   $lastid = DB::connection('document_managment')->table('executive_documents')->orderBy('id', 'desc')->pluck('id');
							    $data = array(
						   					'file_name'				=> $filename,
						   					'ex_doc_id'				=> $lastid,
						   				);
						    	//call the model function to insert the data into upload table.
						    $insertfile=\DB::connection('document_managment')->table("uploads")->insert($data);
							}
							else
							{
							    // redirect back with errors.
					    		return Redirect::route('insertForm_document')->withErrors($validator);
							}
						}$auto ++;


					}
				}
		    	// get the the log data and insert it into the log table.

		    	return Redirect::route('insertForm_document')->with("success","Information successfully Added.");
	        }
	        else
	        {
	            return Redirect::route('insertForm_document')->with("fail","An error occured please try again or contact system developer.");
	        }}
	    }




	public function delete($id){

		$row=document_model::find($id);
			$row->status='1';

		$row->save();

			if($row)
	        {
	            return \Redirect::route("document_recordsList")->with("success","You have successfuly deleted the record <span style='color:red;font_weight:bold;'></span>");
	        }
	        else
	        {
	            return \Redirect::route("document_recordsList")->with("fail","An error occured please try again.");
	        }

		}

		public function select_row_doc($id){

			$data=document_model::find($id);

			$department=department::get_department();

			$files=\DB::connection('document_managment')->table('uploads')->select('uploads.*','uploads.id as file_id')->where('ex_doc_id',$id)->get();

			return view::make('document_managment.document_update',array('data'=>$data,'department'=>$department,'files'=>$files));

		}
		public function update(Request $request, $id){

			$document= new document_model;
			$document=document_model::find($id);
			$validates = Validator::make(Input::all(), array(
			"document_type"						=> "required",
			"number"					=> "required"
			));

		if($validates->fails())
		{
			return Redirect::route('select_data_doc')->withErrors($validates)->withInput();
		}
		else
		{

		    //check the date type if it's shamsi or miladi.
			$date = toGregorian(gregorian_format(Input::get('date')));

		    // get the form data.
		    $data = array(
					"number_documents"	    => Input::get('number'),
					"date"									=> $date,
					"type_id"								=> Input::get('type_doc'),
					"department_id"					=> Input::get('department_id'),
					"description"						=> Input::get('description'),
					"document_type"					=> Input::get('document_type'),
					"name"									=> Input::get('name'),
					"result"								=> Input::get('result'),
					"marji"									=> Input::get('marji'),
					"file_address"					=> Input::get('file_address'),
					"result_remark"					=> Input::get('result_remark'),
					"head_of_office"				=> Input::get('head_of_office'),
					"edit_by"						=> Auth::user()->id,
					"updated_at"						=> \Carbon\Carbon::now(),


		    	);
			$insertedRecordID=\DB::connection('document_managment')->table("executive_documents")->where('id',$id)->update($data);
		    if($insertedRecordID){
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 50;
				$file_data = array();

				if(Input::hasFile('files'))
				{
					foreach($files as $file)
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    // path is root/uploads
								$destinationPath ='uploads_doc/'.getMyDepartmentId().'/';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);

						    $lastid = DB::connection('document_managment')->table('executive_documents')->orderBy('id', 'desc')->pluck('id');
						    $file_id = DB::connection('document_managment')->table('uploads')->orderBy('id', 'desc')->pluck('id');
						   $file_id++;

					    	$lastFileId = $lastid;
							if($auto == 50)
						    $filename = "Attachment_".$lastFileId."_".$file_id.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Attachment_".$lastFileId."_".$file_id.".".$extension;
							if(!file_exists($destinationPath)) {
								File::makeDirectory($destinationPath);
							}

						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success)
						    {
						   $lastid = $id;
							    $data = array(
						   					'file_name'				=> $filename,
						   					'ex_doc_id'				=> $lastid,
						   				);
						    	//call the model function to insert the data into upload table.
						    $insertfile=\DB::connection('document_managment')->table("uploads")->insert($data);
							}
							else
							{
							    // redirect back with errors.
					    		return Redirect::route('select_data_doc')->withErrors($validator);
							}
						}$auto ++;


					}
				}
		    	// get the the log data and insert it into the log table.

		    	return Redirect::route('document_recordsList')->with("success","Information successfully updeted.");
	        }
	        else
	        {
	            return Redirect::route('select_data_doc')->with("fail","An error occured please try again or contact system developer.");
	        }}
	    }


		public function show_file_toprint($id){



				$row=\DB::connection('document_managment')->table('uploads')->select('uploads.*')->where('ex_doc_id',$id)->get();


				return view::make('document_managment.show_file',['row'=>$row]);


		}

		public function delete_images(){


			$img_id=trim(Input::get('img_id'));

			$row=\DB::connection('document_managment')->table('uploads')->where('id',$img_id)->get();
			foreach ($row as $val ) {

				if( File::delete('uploads_doc/'.getMyDepartmentId().'/'.$val->file_name))

					 $row=\DB::connection('document_managment')->table('uploads')->where('id',$val->id)->delete();


		}

	}

	public function change_result(Request $request){


		$id=Input::get('get_id');
		$row=document_model::find($id);

		$this->validate($request,[
				'result'=>'required'

		]);

			$row->result=Input::get('result');
			$row->result_remark=Input::get('result_remark');
			$row->save();
		if($row){

			return Redirect::route('document_recordsList')->with("success","Information successfully updeted.");
			}
			else
			{
					return Redirect::route('document_recordsList')->with("fail","An error occured please try again or contact system developer.");
			}
		}

		public function insertDepartment(Request $request){

			$validates = Validator::make(Input::all(), array(
			"name"						=> "required|unique:department",
			));
			if($validates->fails())
			{
				return Redirect::route('document_recordsList')->withErrors($validates)->withInput();
			}else{
			$data=array(
				'name'=>Input::get('name'),
				'name_en'=>Input::get('name_en'),
				'parent'=>0,
				'unactive'=>0,
				'user_id'=>Auth::user()->id
			);

			$department=\DB::connection('document_managment')->table("department")->insert($data);
			return Redirect::route('document_recordsList')->with("success","Information successfully Added.");

		}
	}
		}





?>
