<?php
namespace App\Http\Controllers\docscom;

use App\Http\Controllers\Controller;
use App\models\docscom\Docscom;
use App\models\docscom\AopDocs;
use App\models\docscom\DocAnswers;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;


class docAnswerController extends Controller
{


	//database connection
	public static $myDb = "docscom";

	/**
    * Instantiate a new docsController instance.
    */
	public function __construct()
	{
		if(!Auth::check())
		{
			return Redirect::route('getLogin');
		}



	}	

	//Load documents form entry view
	public function getDocAnswersForms()
	{
		if(!canView('docscom_docscom'))
		{
			return showWarning();
		}
		else
		{
			//check roles
			return View::make('docscom.answer_form_list');
		}
	}

	//get form data list
	public function getDocAnswersData()
	{

		//get all data 
		$object = DocAnswers::getData();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
									'doc_id',
									'barcode',
									'source_organization',
									'is_answer',
									'executive_department',
									'status'
									)
						->addColumn('operations', function($option){
							return '<a href="'.route('getDocDetails',array('docs_answers',$option->doc_id)).'" class="table-link">
																<span class="fa-stack">
																	<i class="fa fa-square fa-stack-2x"></i>
																	<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
																</span>
															</a>';
						})
						//->set_row_class('@if($status==0) danger @elseif($status == 1) success @endif')
						//->set_row_data('Test','{{$status}}')
						->make();
		
	}
}

?>