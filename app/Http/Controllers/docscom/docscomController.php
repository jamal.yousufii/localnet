<?php 

namespace App\Http\Controllers\docscom;

use App\Http\Controllers\Controller;
use App\models\docscom\Docscom;
use App\models\docscom\AopDocs;
use App\models\docscom\DocAnswers;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class docscomController extends Controller
{


	//database connection
	public static $myDb = "docscom";

	//Load documents form entry view
	public function getForm()
	{

		//get departments
		$data['departments'] = getAllDocsComDepartments();
		//check roles
		return View::make('docscom.form',$data);
	}

	//load search forms
	public function searchForm()
	{
		//check roles
		if(canView('docscom_docscom'))
		{

			$data['parentDeps'] = getDocsComDepartmentWhereIn();
			//check roles
			return View::make('docscom.search_form',$data);
		}
		else
		{
			return showWarning();
		}
	}

	//bring related department
	public function bringRelatedSubDepartment()
	{
		$deps = getRelatedSubDepartment(Input::get('dep_id'),true);
		if(Input::get("element_id"))
		{
			$options = '<select name="'.Input::get('element_id').'" id="'.Input::get('element_id').'" class="form-control">';
		}
		else
		{
			$options = '<select name="sub_dep" id="sub_dap" class="form-control">';
		}
		$options .= '<option value="">انتخاب</option>';
		foreach($deps AS $key => $value)
		{
			$options.="<option value='".$key."'>".$value."</option>";
		}
		$options.="</select>";

		return $options;
	}

	//get search result
	public function getSearchResult()
	{
		//dd(Input::all());
		$data['start_date'] = Input::get('start_date');
		$data['end_date'] = Input::get('end_date');
		$data['condition'] = Input::get('condition');
		$data['parent_dep'] = Input::get('parent_dep');
		$data['sub_dep'] = Input::get('sub_dep');

		$data['app'] = 0;
		$data['document'] = 0;

		if(Input::get('app') || Input::get('document'))
		{
			if(Input::get('app') == 1)
			{
				$data['app'] = 1;
			}
			if(Input::get('document') == 2)
			{
				$data['document'] = 2;
			}
		}
		
		
		$per_page=1;
		
		
		
		$data['rows'] = Docscom::getSearchResult();
		
		if(Input::get('ajax') == 1)
		{
			return View::make('docscom.search_result_ajax',$data);
		}
		else
		{
			//load view to show search result
			return View::make('docscom.search_result',$data);
		}
	}


	public function searchResultToExcel(request $request)
	{
		if (isset($request)) 
		{
			
			//get data from model
			$results = Docscom::getSearchResult(array('toExcel' => true));
			Excel::load('reports/docscom_search_result.xlsx', function($file) use($results){
			//Excel::create('Filename', function($file) use($results){			
			$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($results){
			//$sheet = $file->getActiveSheetIndex(0);	
				$row = 3;
				$sheet->setFreeze('A3');
				
				foreach($results AS $item)
				{
					$sheet->setHeight($row, 30);

					$sheet->setCellValue('A'.$row.'',$row-2);
					$sheet->setCellValue('B'.$row.'',$item->barcode);
					$sheet->setCellValue('C'.$row.'',$item->external_number);
					$sheet->setCellValue('D'.$row.'',$item->internal_number);
					$sheet->setCellValue('E'.$row.'',$item->internal_date);
					$sheet->setCellValue('F'.$row.'',$item->executive_department);
					$sheet->setCellValue('G'.$row.'',$item->organization);
					$sheet->setCellValue('H'.$row.'',$item->status);
					$sheet->setCellValue('I'.$row.'',$item->approved_by);
					$sheet->setCellValue('J'.$row.'',getDocumentDurration($item->id,'docs',true));
					$sheet->setCellValue('K'.$row.'',$item->doc_summary);
					
					$row++;
				}

				$sheet->setBorder('A3:K'.($row-1).'', 'thin');
				
    		});
			
			})->export('xlsx');
		}
	}
	//get search result
	public function getSearchResultDataTable()
	{
		print_r($_POST);
		//$data=Input::get('0');
		//print_r($data);
		//echo Input::get('condition');
		//exit;
		//get result from database
		$result = Docscom::getSearchResult();
		$collection = new collection($result);
		return Datatable::collection($collection)
							->showColumns(
										'barcode',
										'external_number',
										'internal_number',
										'internal_date',
										'executive_department',
										'organization',
										'status',
										'approved_by'
										)
							->addColumn('internal_date', function($option){
								if(isMiladiDate())
								{
									$date = explode('-',$option->internal_date);
								
									return dateToMiladi($date[0],$date[1],$date[2]);
								}
								else
								{
									return $option->internal_date;
								}
							})
							->addColumn('duration', function($option){
								return getDocumentDurration($option->id,'docs');
							})
							
							->addColumn('operations', function($option){
								return '<a href="'.route('getDocDetails',array('docs',$option->id)).'" class="table-link">
																	<span class="fa-stack">
																		<i class="fa fa-square fa-stack-2x"></i>
																		<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
																	</span>
																</a>';
							})
							//->set_row_class('@if($status==0) danger @elseif($status == 1) success @endif')
							//->set_row_data('Test','{{$status}}')
							->make();
	}

	//get form data list
	public function getListData()
	{
		//check roles
		if(canView('docscom_docscom'))
		{

			//get all data 
			$object = Docscom::getData();
			$collection = new Collection($object);
			// dd($object);
			return Datatable::collection($collection)
							->showColumns(
										'barcode',
										'external_number',
										'internal_number',
										'internal_date',
										'executive_department',
										'organization',
										'status',
										'approved_by',
										'days'
										)
							->addColumn('internal_date', function($option){
								if(isMiladiDate())
								{
									$date = explode('-',$option->internal_date);
								
									return dateToMiladi($date[0],$date[1],$date[2]);
								}
								else
								{
									return $option->internal_date;
								}
							})
							->addColumn('operations', function($option){
								return '<a href="'.route('getDocDetails',array('docs',$option->doc_id)).'" class="table-link">
									<span class="fa-stack">
										<i class="fa fa-square fa-stack-2x"></i>
										<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
									</span>
								</a>';
							})
							->orderColumns('doc_id')
							->make();
		}
		else
		{
			return showWarning();
		}
	}

	//search and scan barcode
	public function getFormByBarcode()
	{
		if(canSearch('docscom_docscom'))
		{

			return View::make('docscom.formByBarcode');
		}
		else
		{
			return showWarning();
		}
	}

	//checkout document
	public function getCheckoutForm()
	{
		
		return View::make('docscom.check_out');
		
	}

	//Load documents form entry view
	public function postForm()
	{

		if(canAdd('docscom_docscom'))
		{
			//echo "<pre>";print_r($_POST);exit;

			if(Input::get('app_type') == 1 || Input::get('export_import') == 1)
			{
				//validate fields
				$validates = Validator::make(Input::all(), array(
					"barcode"						=> "required|unique:docscom.docs,barcode",
					"executive_department"			=> "required"
				));
			}
			else if(Input::get('app_type') == 2)
			{
				//validate fields
				$validates = Validator::make(Input::all(), array(
					"barcode"						=> "required|unique:docscom.docs_answers,barcode",
					"doc_executive_department"			=> "required"
				));
			}
			else if(Input::get('export_import') == 2)
			{
				//validate fields
				$validates = Validator::make(Input::all(), array(
					"barcode"						=> "required|unique:docscom.aop_docs,barcode",
					"aop_executive_department"			=> "required"
				));

			}

			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('docsGetForm')->withErrors($validates)->withInput();
			}
			else
			{

				$table_name = "";

				if(Input::get('app_type') == 1 || Input::get('export_import') == 1)
				{

					$table_name = "docs";
					//get doc access type from logged in user
					$doc_access = 0;
					if(isConfidential('docscom_docscom'))
					{
						$doc_access = 1;
					}
					elseif(isNonConfidential('docscom_docscom'))
					{
						$doc_access = 2;
					}
					elseif(isConfidential('docscom_docscom') && Input::get('access'))
					{
						$doc_access = 3;
					}

					$object = new Docscom;
				
					
					$object->barcode 				= trim(Input::get("barcode"));
					$object->organization 			= Input::get("organization");
					$object->external_number 		= Input::get("external_number");
					//check date setting
					if(isMiladiDate())
					{
						$object->external_date 			= toJalali(Input::get("external_date"));
						$object->internal_date 			= toJalali(Input::get("internal_date"));
					}
					else
					{
						$object->external_date 			= ymd_format(Input::get("external_date"));
						$object->internal_date 			= ymd_format(Input::get("internal_date"));
					}
					
					//$object->email 					= Input::get("email");
					//$object->phone 					= Input::get("phone");
					$object->internal_number 		= Input::get("internal_number");
					
					$object->doc_summary 			= Input::get("summary");
					$object->executive_department 	= Input::get("executive_department");
					$object->doc_type 				= Input::get("doc_type");
					//$object->access_type 			= Input::get("access");
					$object->access_type 			= $doc_access;
					$object->user_id 				= Auth::user()->id;
					$object->created_at 			= date('Y-m-d H:i:s');
				}
				else if(Input::get('app_type') == 2)
				{

					$table_name = "aop_docs";
					$object = new AopDocs;
					
					$object->barcode 				= Input::get("barcode");
					$object->doc_type 				= Input::get("doc_type");
					$object->number 				= Input::get("aop_number");
					if(isMiladiDate())
					{
						$object->date 					= toJalali(Input::get("aop_date"));
					}
					else
					{
						$object->date 					= ymd_format(Input::get("aop_date"));
					}
					
					$object->applicant_department 	= Input::get('aop_app_department');
					$object->applicantion_subject 	= Input::get('aop_app_subject');
					$object->executive_department 	= Input::get('aop_executive_department');
					$object->user_id 				= Auth::user()->id;

				}
				else if(Input::get('export_import') == 2)
				{
					$table_name = "docs_answers";

					$object = new DocAnswers;
					
					$object->barcode 				= Input::get("barcode");
					$object->doc_type 				= Input::get("doc_type");
					$object->source_organization 	= Input::get("doc_source");
					$object->doc_summary 			= Input::get("doc_summary");
					$object->executive_department 	= Input::get("doc_executive_department");
					$object->is_answer 				= Input::get("doc_answer");
					if(isMiladiDate())
					{
						$object->date 					= toJalali(Input::get("doc_external_date"));
					}
					else
					{
						$object->date 					= ymd_format(Input::get("doc_external_date"));
					}
					$object->form_number			= Input::get("doc_yes");
					$object->user_id 				= Auth::user()->id;

				}

				if($object->save())
				{
					$inserted_id = $object->id;

					$this->uploadDocs($inserted_id,$table_name);

					//insert log entry ------------------------//
					$data = array(
							"checked_in_by" 		=> Auth::user()->id,
							"status"  				=> 1,//check in
							"check_in" 				=> date('Y-m-d H:i:s'),
							"doc_id"   				=> $inserted_id,
							"table_name"			=> $table_name,
							"created_at" 			=> date('Y-m-d H:i:s')
						);

					Docscom::insertRecord('log',$data);
					//------------------------------------------//
					return Redirect::route('docsGetForm')->with("success","Record saved successfully");
				}
				else
				{
					return Redirect::route("docsGetForm")->with("fail","An error occured plase try again.");
				}

			}
		}
		else
		{
			return showWarning();
		}

	}

	//update document
	public function postUpdateForm($table_name='docs',$doc_id=0)
	{

		if(canEdit('docscom_docscom'))
		{

			//validate fields
			$validates = Validator::make(Input::all(), array(
				"doc_type"						=> "required"
			));

			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('getDocDetails',$doc_id)->withErrors($validates)->withInput();
			}
			else
			{

				
				if($table_name == 'docs')
				{
					$object = Docscom::find($doc_id);;
					
					//$object->barcode 				= Input::get("barcode");
					$object->organization 			= Input::get("organization");
					$object->external_number 		= Input::get("external_number");
					if(isMiladiDate())
					{
						$object->external_date 			= toJalali(Input::get("external_date"));
						$object->internal_date 			= toJalali(Input::get("internal_date"));
					}
					else
					{
						$object->external_date 			= ymd_format(Input::get("external_date"));
						$object->internal_date 			= ymd_format(Input::get("internal_date"));
					}
					//$object->email 					= Input::get("email");
					//$object->phone 					= Input::get("phone");
					$object->internal_number 		= Input::get("internal_number");
					
					$object->doc_summary 			= Input::get("summary");
					$object->executive_department 	= Input::get("executive_department");
					$object->doc_type 				= Input::get("doc_type");
					if(isConfidential('docscom_docscom') && Input::get('access'))
					{
						$object->access_type = Input::get('access');
					}
					//$object->access_type 			= Input::get("access");
					//$object->user_id 				= Auth::user()->id;
					$object->updated_at 			= date('Y-m-d H:i:s');
				}
				else if($table_name == 'aop_docs')
				{

					$object = AopDocs::find($doc_id);
					$object->doc_type 				= Input::get("doc_type");
					$object->number 				= Input::get("aop_number");
					
					if(isMiladiDate())
					{
						$object->date 					= toJalali(Input::get("aop_date"));
					}
					else
					{
						$object->date 					= ymd_format(Input::get("aop_date"));
					}
					
					$object->applicant_department 	= Input::get('aop_app_department');
					$object->applicantion_subject 	= Input::get('aop_app_subject');
					$object->executive_department 	= Input::get('aop_executive_department');
					

				}
				else if($table_name == 'docs_answers')
				{
					
					$object = DocAnswers::find($doc_id);
					$object->doc_type 				= Input::get("doc_type");
					$object->source_organization 	= Input::get("doc_source");
					$object->doc_summary 			= Input::get("doc_summary");
					$object->executive_department 	= Input::get("doc_executive_department");
					$object->is_answer 				= Input::get("doc_answer");
					
					if(isMiladiDate())
					{
						$object->date 					= toJalali(Input::get("doc_external_date"));
					}
					else
					{
						$object->date 					= ymd_format(Input::get("doc_external_date"));
					}
					$object->form_number			= Input::get("doc_yes");

				}

				if($object->save())
				{
					$inserted_id = $doc_id;
					if (Input::hasFile('files'))
					{
						$this->uploadDocs($inserted_id,$table_name);
					}

					// //insert log entry ------------------------//
					// $data = array(
					// 		"checked_in_by" 		=> Auth::user()->id,
					// 		"status"  		=> 1,//check in
					// 		"check_in" 		=> date('Y-m-d H:i:s'),
					// 		"doc_id"   		=> $inserted_id,
					// 		"created_at" 	=> date('Y-m-d H:i:s')
					// 	);
					// Docscom::insertRecord('log',$data);
					//------------------------------------------//
					return Redirect::route('getDocDetails',array($table_name,$doc_id))->with("success","Record updated successfully");
				}
				else
				{
					return Redirect::route('getDocDetails',array($table_name,$doc_id))->with("fail","An error occured plase try again.");
				}

			}
		}
		else
		{
			return showWarning();
		}

	}

	//validate barcode in database
	public function validateForm()
	{
		$barcode = Input::get('barcode');

		$docs 		= DB::connection(self::$myDb)->table('docs')->where('barcode',$barcode)->get();
		$docs_answers = DB::connection(self::$myDb)->table('docs_answers')->where('barcode',$barcode)->get();
		$aop_docs 	= DB::connection(self::$myDb)->table('aop_docs')->where('barcode',$barcode)->get();
		//if record found, barcode registered in database
		if(count($docs)>0 || count($docs_answers)>0 || count($aop_docs)>0)
		{
			return json_encode(array('cond'=>'true'));
		}
		else
		{
			return json_encode(array('cond'=>'false'));
		}
	}

	//get form details
	public function getDocDetails($table_name='',$doc_id=0,$update_notify=0)
	{

		if(canView('docscom_docscom'))
		{
			//update notification
			if($update_notify!=0)
			{
				DB::connection(self::$myDb)->table('comments')
				->where('id',$update_notify)
				->where('table_name',$table_name)
				->update(array('status'=>1,'type'=>0));
			}
			//get details from model
			$object = Docscom::getDocDetails($doc_id,$table_name);
			$doc_comments = Docscom::getDocComments($doc_id,$table_name);
			$doc_attachs = Docscom::getDocAttachs($doc_id,$table_name);
			$doc_log = Docscom::getDocLog($doc_id,$table_name);
			
			$data['answers'] = false;

			//check if original form
			if($table_name == 'docs')
			{
				//get the original form answer
				$form_answers = Docscom::getFormAnswers($doc_id);
				$data['answers'] = $form_answers;
			}
			
			$data['records'] = $object;
			$data['comments'] = $doc_comments;
			$data['attachments'] = $doc_attachs;
			$data['logs'] = $doc_log;
			$data['doc_id'] = $doc_id;
			//get departments
			$data['departments'] = getAllDocsComDepartments();
			//get last log record
			$data['checked_in_by'] = Docscom::getLastLogRecord($doc_id,$table_name);
			
			$data['table_name'] = $table_name;
			//load view
			return View::make('docscom.doc_view_new',$data);
		}
		else
		{
			return showWarning();
		}
		

	}

	//validate barcode in database
	public function findBarcodeDetails()
	{

		$barcode = Input::get('barcode');
		
		$table_name = "docs";

		$object = false;	

		//check if request from checkout_form
		if(Input::get('checkout'))
		{
			
			$object = Docscom::findBarcode($barcode,$table_name);
			//check if barcode not found in docs table
			if(count($object)==0)
			{
				//set new table(aop_docs) table to find the barcode
				$table_name = "aop_docs";
				$object = Docscom::findBarcode($barcode,$table_name);

				//check if barcode not found in aop_docs table
				if(count($object)==0)
				{
					//find barcode from docs_answers table
					$table_name = "docs_answers";
					$object = Docscom::findBarcode($barcode,$table_name);
				}
			}

			$object = Docscom::findBarcode($barcode,$table_name,true);
		}
		else
		{
			//find barcode from docs table
			$object = Docscom::findBarcode($barcode,$table_name);
			//check if barcode not found in docs table
			if(count($object)==0)
			{
				//set new table(aop_docs) table to find the barcode
				$table_name = "aop_docs";
				$object = Docscom::findBarcode($barcode,$table_name);

				//check if barcode not found in aop_docs table
				if(count($object)==0)
				{
					//find barcode from docs_answers table
					$table_name = "docs_answers";
					$object = Docscom::findBarcode($barcode,$table_name);
				}
			}
		}
		
		//get form id
		$form_id = 0;
		foreach($object AS $item)
		{
			$form_id = $item->id;
		}

		//check in pending docs --------------------------------------//
		if(!Input::get('checkout'))
		{
			$pending = DB::connection(self::$myDb)
						->table("pending_docs")
						->where("table",$table_name)
						->where("doc_id",$form_id)
						->where("to_dep",getMyDepartmentId())
						->get();

			//get record from my department if exist
			$inMyDep = Docscom::findBarcodeInMyDep($barcode,$table_name);


			if(count($pending)>0)
			{

				$doc_ob = Docscom::find($form_id);
						
				//$object->barcode 				= Input::get("barcode");
				$doc_ob->executive_department = getMyDepartmentId();

				if($doc_ob->save())
				{
					$pending = DB::connection(self::$myDb)
						->table("pending_docs")
						->where("table",$table_name)
						->where("doc_id",$form_id)
						->where("to_dep",getMyDepartmentId())
						->delete();

				}

			}
			elseif(count($inMyDep) == 0 && count($pending) == 0)
			{
				//set null conditions for ajax request
				$data = array(
								'cond'=>'false',
								'details'=>'',
								'comments'=>'',
								'checkOuted'=>''
							);
				//return json format
				return json_encode($data);

			}
		}
		//check in pending docs --------------------------------------//

		//check requested from checkout form
		if(Input::get('checkout'))
		{

			//if object found
			if(count($object)>0)
			{
				//set value for ajax response
				$data = array(
								'cond'=>'false',
								'doc_id'=>$form_id,
								'checkOuted'=>'false',
								'approved'=>'true',
								'table'	=> $table_name
							);
				//check if document approved or not
				if(isDocumentApproved($form_id,$table_name))
				{
					//check is doc checked out or not
					$checkedOut = Docscom::docChekedOut($form_id,$table_name);
					
					//if form checked out
					if($checkedOut)
					{
						//then, return it, it already checked out
						$data['checkOuted'] = "true";
						$data['doc_id'] = $form_id;
						$data['cond'] = "true";

					}
					else//form checked out right now
					{
						$data['doc_id'] = $form_id;
						$data['cond'] = "true";
					}
				}
				else//if document approved tell user to approve it first
				{
					//ajax response condition data
					$data = array(
									'cond'=>'true',
									'doc_id'=>$form_id,
									'checkOuted'=>'false',
									'approved'=>'false'
								);

				}
			}
			else//record not found
			{
				//not found condition for ajax response
				$data = array(
								'cond'=>'false',
								'doc_id'=>$form_id,
								'checkOuted'=>'false',
								'approved'=>'true',
								'table'	=> $table_name
							);

			}
			//return for ajax request
			return json_encode($data);

		}

		//get all form comments
		$comments = DB::connection(self::$myDb)->table('comments')->where('doc_id',$form_id)->get();
		
		//if record found, barcode registered in database
		if(count($object)>0)
		{
			//if form checked out successfully
			$isCheckOuted = Docscom::isCheckOuted($form_id);

			//check if form check outed or not
			if(!$isCheckOuted)
			{
				//update checkout from current user
				$checkOut = array(
								'checked_out_by' 	=> Auth::user()->id,
								'check_out'			=>date('Y-m-d H:i:s'),
								'status' 			=> 2,
								'table_name'		=>$table_name
							  );

				//update checked out log to log table
				DB::connection(self::$myDb)->table('log')
											->where('doc_id',$form_id)
											->where('status',1)
											->update($checkOut);

				//insert log to log table
				$checkIn = array(
								'checked_in_by' => Auth::user()->id,
								'doc_id'  		=> $form_id,
								'status'  		=> 1,
								'table_name'	=>$table_name,
								'check_in'		=> date('Y-m-d H:i:s'),
								'created_at'	=> date('Y-m-d H:i:s')
							);
				
				//insert new record to the log table
				Docscom::insertRecord('log',$checkIn);
			}

			//get departments
			$data['departments'] = getAllDocsComDepartments();
			//set record for view
			$data['record']		 = $object;
			
			//render view for form details
			if($table_name == 'docs')
			{
				$details = View::make('docscom.form_details',$data)->render();
			}
			elseif($table_name == 'aop_docs')
			{
				$details = View::make('docscom.aop_docs',$data)->render();
			}
			elseif ($table_name == 'docs_answers') 
			{
				$details = View::make('docscom.docs_answers',$data)->render();
			}

			//render view for form's comments
			$comment = View::make('docscom.comment_view',array('comments'=>$comments))->render();
			
			//set form details condition for ajax request
			$data = array(
							'cond'=>'true',
							'details'=>$details,
							'comments'=>$comment,
							'checkOuted' => 'false'
						 );
			//if checked out, set condition to true
			if($isCheckOuted)
			{
				$data['checkOuted'] = 'true';
			}

			//return form details in json format
			return json_encode($data);
		}
		else//record not found
		{
			//set null conditions for ajax request
			$data = array(
							'cond'=>'false',
							'details'=>'',
							'comments'=>'',
							'checkOuted'=>''
						);
			//return json format
			return json_encode($data);
		}
	}

	//save document checkout
	public function saveCheckOut()
	{

		if(canCheckOut('docscom_docscom'))
		{
			$table_name = Input::get("table_name");
			
			if($table_name == 'docs')
			{
				$object = Docscom::find(Input::get('doc_id'));
			}
			elseif($table_name == 'aop_docs')
			{
				$object = AopDocs::find(Input::get('doc_id'));
			}
			else
			{
				$object = DocAnswers::find(Input::get('doc_id'));
			}
					
			//$object->barcode 				= Input::get("barcode");
			$object->exported_number 			= Input::get("checkout_number");
			if(isMiladiDate())
			{
				$object->exported_date 				= toJalali(Input::get("checkout_date"));
			}
			else
			{
				$object->exported_date 				= ymd_format(Input::get("checkout_date"));
			}
			
			$object->exported_by 				= Auth::user()->id;
			$object->status						= 4;
			
			if($object->save())
			{

				//update checkout from current user
				$checkOut = array(
								'checked_out_by' => Auth::user()->id,
								'check_out'=>date('Y-m-d H:i:s'),
								'status' => 2,
								'table_name' => $table_name
							  );

				DB::connection(self::$myDb)->table('log')
											->where('doc_id',Input::get('doc_id'))
											->where('status',1)
											->update($checkOut);
				//insert log to log table
				$checkIn = array(
								'checked_in_by' => Auth::user()->id,
								'checked_out_by' => Auth::user()->id,
								'doc_id'  		=> Input::get('doc_id'),
								'status'  		=> 2,
								'table_name' => $table_name,
								'check_in'		=> date('Y-m-d H:i:s'),
								'check_out'=>date('Y-m-d H:i:s')
							);
				Docscom::insertRecord('log',$checkIn);

				//upload scanned file
				if (Input::hasFile('files'))
				{
					$this->uploadDocs(Input::get('doc_id'),$table_name,true);
				}

				return Redirect::route('getCheckoutForm')->with("success","Document checked out successfully");
			}
			else
			{
				return Redirect::route('getCheckoutForm')->with("fail","An error occured plase try again.");
			}
		}
		else
		{
			return showWarning();
		}

	}

	//save task comments
	public function postDocComment()
	{
		if(canComment('docscom_docscom'))
		{
			
			$data = array(

					"doc_id" 			=> Input::get("doc_id"),
					"comment" 			=> Input::get("comment"),
					"table_name"		=> Input::get("table_name"),
					"user_id" 			=> Auth::user()->id,
					"created_at" 		=> date('Y-m-d H:i:s')
				);
			$comment_id = DB::connection(self::$myDb)->table("comments")->insertGetId($data);
			
			
			//get last inserted comment
			$commentObject = DB::connection(self::$myDb)
							 ->table('comments')
							 ->where('table_name',Input::get("table_name"))
							 ->where('id',$comment_id)
							 ->get();
			
			$comments = '<div class="conversation-item item-left clearfix">';
			//foreach comment for showing on view
			foreach($commentObject AS $item)
			{
				$comments .= '<div class="conversation-user">';
				$photo 	   = getProfilePicture($item->user_id);
				$comments .= HTML::image('/img/'.$photo,'',array('class' => 'project-img-owner comment'));
				$comments .= '</div>';
				$comments .= '<div class="conversation-body">';
				$comments .= '<div class="name">';
				$comments .= getUserFullName($item->user_id);
				$comments .= '</div>';
				$comments .= '<div class="time hidden-xs">';
				$comments .= $item->created_at;
				$comments .= '</div>';
				$comments .= '<div class="text">';
				$comments .= nl2br($item->comment);
				$comments .= '</div>';
				$comments .= '</div>';
			}

			return $comments;
		}
		else
		{
			return "Access deniad!";
		}
	}

	//approve document
	public function approveDocument()
	{

		$table_name = Input::get('table_name');

		if(canFinalApprove('docscom_docscom') && Input::get('approve')==1)
		{
			//
			$doc_id = Input::get('doc_id');

			DB::connection(self::$myDb)
								->table($table_name)
								->where('id',$doc_id)
								->update(array('status'=>1,'approved_by'=>Auth::user()->id));
			return json_encode(array('cond'=>'true'));
		}
		elseif(canApprove('docscom_docscom') && Input::get('proceed')==1)
		{
			//
			$doc_id = Input::get('doc_id');
			DB::connection(self::$myDb)
								->table('log')
								->where('doc_id',$doc_id)
								->where('table_name',$table_name)
								->where('checked_in_by',Auth::user()->id)
								->update(array('status'=>3));

			return json_encode(array('cond'=>'true'));

		}
		elseif(canApprove('docscom_docscom') && Input::get('save')==1)
		{
			//
			$doc_id = Input::get('doc_id');
			$data = array("status"=>3);
			
			if(Input::get('undo'))
			{
				$data['status'] = 0;
			}

			DB::connection(self::$myDb)
								->table($table_name)
								->where('id',$doc_id)
								->update($data);

			return json_encode(array('cond'=>'true'));

		}
		else
		{
			return json_encode(array('cond'=>'false'));
		}

	}

	//reject document
	public function rejectDocument()
	{
		if(canReturn('docscom_docscom') && Input::get('type1')==1)
		{

			$data = array(

					"doc_id" 			=> Input::get("doc_id1"),
					"comment" 			=> Input::get("comment1"),
					"type" 				=> 1,
					"old_type" 				=> 1,
					"table_name"		=> Input::get("table_name"),
					"user_id" 			=> Auth::user()->id,
					"notify_to" 		=> Input::get('notify_to1'),
					"created_at" 		=> date('Y-m-d H:i:s')
				);
			DB::connection(self::$myDb)->table("comments")->insertGetId($data);

			//redirect to view details
			return Redirect::route("getDocDetails",array(Input::get("table_name"),Input::get('doc_id1')));
		}
		elseif(canReject('docscom_docscom') && Input::get('type2')==2)
		{
			$data = array(

					"doc_id" 			=> Input::get("doc_id2"),
					"comment" 			=> Input::get("comment2"),
					"type" 				=> 2,
					"old_type" 				=> 2,
					"table_name"		=> Input::get("table_name"),
					"user_id" 			=> Auth::user()->id,
					"notify_to" 		=> Input::get('notify_to2'),
					"created_at" 		=> date('Y-m-d H:i:s')
				);
			DB::connection(self::$myDb)->table("comments")->insertGetId($data);

			//redirect to view details
			return Redirect::route("getDocDetails",array(Input::get("table_name"),Input::get('doc_id2')));
		}
		else
		{
			return showWarning();
		}

	}
	//reject document
	public function changeDepartment()
	{
		if(canChangeDepartment('docscom_docscom') || canEdit('docscom_docscom'))
		{


			$data = array(
					"doc_id" 	=> Input::get("doc_id"),
					"from_dep" 	=> Input::get("from_dep"),
					"to_dep" 	=> Input::get("dep"),
					"table" 	=> Input::get("table_name"),
					"created_at" 	=> date("Y-m-d H:i:s"),
					"user_id"	=> Auth::user()->id
				);

			//first delete old record
			DB::connection(self::$myDb)
			->table("pending_docs")
			->where('doc_id',Input::get("doc_id"))
			->where('table',Input::get("table_name"))
			->where('from_dep',Input::get("from_dep"))
			->delete();

			//insert new record
			DB::connection(self::$myDb)
			->table("pending_docs")
			->insert($data);

			return json_encode(array('cond'=>'true','msg'=>'<div class="alert alert-success"><i class="fa fa-check"></i> اداره اجرا کننده تجدید گردید.</div>'));
		}
		else
		{
			return json_encode(array('cond'=>'denied','msg'=>'<div class="alert alert-danger"><i class="fa fa-cancel"></i> You do not have role for this operation, please contact database admin!</div>'));
			
		}
		

	}

	//upload document
	public function uploadDocs($doc_id=0,$table_name='',$is_final=false)
	{
		if(canAdd('docscom_docscom') || canEdit('docscom_docscom') || canCheckOut('docscom_docscom'))
		{

			// getting all of the post data
			$files = Input::file('files');
			$errors = "";
			$file_data = array();

			foreach($files as $file) 
			{
				
				if(Input::hasFile('files'))
				{
				  // validating each file.
				  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
				  $validator = Validator::make(

				  		[
				            'file' => $file,
				            'extension'  => Str::lower($file->getClientOriginalExtension()),
				        ],
				        [
				            'file' => 'required|max:100000',
				            'extension'  => 'required|in:jpg,jpeg,bmp,png,doc,docx,zip,rar,pdf,rtf,xlsx,xls,txt'
				        ]
				  	);

				  if($validator->passes())
				  {
				  	
				    // path is root/uploads
				    $destinationPath = 'documents/docscom_attachments';
				    $filename = $file->getClientOriginalName();

				    $temp = explode(".", $filename);
				    $extension = end($temp);
				    $lastFileId = getLastDocId();
				    
				    $lastFileId++;
				    
				    $filename = $temp[0].'_'.$lastFileId.'.'.$extension;

				    $upload_success = $file->move($destinationPath, $filename);

				    if($upload_success) 
				    {
					   
					   $data = array(
					   					'doc_id'=>$doc_id,
					   					'file_name'=>$filename,
					   					'created_at'=>date('Y-m-d H:i:s'),
					   					'table_name' => $table_name,
					   					'user_id'	=> Auth::user()->id
					   				);
					   if($is_final)
					   {
					   		$data['sadira'] = 1;
					   }

					   if(count($data)>0)
						{
							Docscom::insertRecord('attachments',$data);
						}
					   //return Response::json('success', 200);
					   
					} 
					else 
					{
					   $errors .= json('error', 400);
					}

			    
				  } 
				  else 
				  {
				    // redirect back with errors.
				    return Redirect::back()->withErrors($validator);
				  }
				}

			}
		}
		else
		{
			return showWarning();
		}
	}
	//download file from server
	public function downloadDoc($table_name='docs',$file_id=0)
	{
		if(canView('docscom_docscom'))
		{
			//get file name from database
			$fileName = getDocscomFileName($file_id,$table_name);
	        //public path for file
	        $file= public_path(). "/documents/docscom_attachments/".$fileName;
	        //download file
	        return Response::download($file, $fileName);
	    }
	    else
	    {
	    	return showWarning();
	    }
	}

	//remove file from folder
	public function deleteFile()
	{
		if(canEdit('docscom_docscom'))
		{
			$file_id = Input::get('doc_id');
			$table_name = Input::get('table_name');

			//get file name from database
			$fileName = getDocscomFileName($file_id,$table_name);
			$file= public_path(). "/documents/docscom_attachments/".$fileName;
			if(File::delete($file))
			{
				//delete from database
				DB::connection('docscom')
				->table('attachments')
				->where('table_name',$table_name)
				->where('id',$file_id)
				->delete();
				
				return "<div class='alert alert-success'>File Deleted Successfully!</div>";
			}
			else
			{
				return "<div class='alert alert-danger'>Error!</div>";
			}
		}
		else
		{
			return showWarning();
		}
	}


	//get report form for showing the form
	public function getReportForm()
	{
		//check roles
		if(canView('docscom_docscom'))
		{

			$data['parentDeps'] = getDocsComDepartmentWhereIn();
			//check roles
			return View::make('docscom.form_report',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//get search result
	public function getReportList()
	{

		//$data['start_date'] = Input::get('start_date');
		//$data['end_date'] = Input::get('end_date');
		//$data['condition'] = Input::get('condition');
		$data['parent_dep'] = Input::get('parent_dep');
		$data['sub_dep'] = Input::get('sub_dep');
		$data['records'] = Docscom::getReport();
		//load view to show search result
		return View::make('docscom.report_result',$data);
	}

	//get report log
	public function getDocLog()
	{

		//$data['start_date'] = Input::get('start_date');
		//$data['end_date'] = Input::get('end_date');
		//$data['condition'] = Input::get('condition');
		$data['parentDeps'] = getDocsComDepartmentWhereIn();
		$data['sub_dep'] = Input::get('sub_dep');
		
		//load view to show search result
		return View::make('docscom.report_log',$data);
	}

	public function getReportLogList()
	{

		//$data['start_date'] = Input::get('start_date');
		//$data['end_date'] = Input::get('end_date');
		//$data['condition'] = Input::get('condition');
		$data['parent_dep'] = Input::get('parent_dep');
		$data['sub_dep'] = Input::get('sub_dep');
		$data['records'] = Docscom::getReportLog();
		//load view to show search result
		return View::make('docscom.report_log_result',$data);
	}

	//get search result
	public function getReportData()
	{
		
		//get result from database
		$result = Docscom::getReport();
		$collection = new collection($result);
		return Datatable::collection($collection)
							->showColumns('internal_number','executive_department','status','days')
							->addColumn('operations', function($option){
								return '<a href="'.route('getDocDetails',array('docs',$option->id)).'" class="table-link">
																	<span class="fa-stack">
																		<i class="fa fa-square fa-stack-2x"></i>
																		<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
																	</span>
																</a>';
							})
							//->set_row_class('@if($status==0) danger @elseif($status == 1) success @endif')
							//->set_row_data('Test','{{$status}}')
							->make();
	}



}

?>