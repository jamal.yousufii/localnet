<?php namespace App\Http\Controllers;
/*
@desc: Manipulates the department operations
@Author: Gul Muhammad Akbari (gm.akbari27@gmail.com)
@Created At: 20 Feb 2015
@version: 1.0
*/
use App\models\DepartmentX;
use App\models\Application;
use App\models\DepartmentModule;
use Auth;
use View;
use Input;
use LaravelGettext;
class DepartmentController extends Controller
{

	// public function __construct()
	// {
	// 	if(!Auth::check())
	// 	{
	// 		return \Redirect::to('user/login');
	// 	}

	// 	LaravelGettext::setDomain('auth');
	// }

	/*
	Getting all records from department
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getAllDepartment()
	{

		//check roles
		if(canView('auth_department'))
		{

			//get data from model
			//$deps = DepartmentX::getAllParent();
			//$depAll = DepartmentX::getAll();

            //load view for users list
			return View::make("auth.department.dep_list");
		}
		else
		{
			return showWarning();
		}
	}

	public function departmentOrgChart()
	{

		//check roles
		if(canView('auth_department'))
		{

			//get data from model
			$deps = DepartmentX::getAllParent();
			$depAll = DepartmentX::getAll();

			//load view for users list
			return View::make("auth.department.test_chart")->with('deps',$deps)->with('all',$depAll);
		}
		else
		{
			return showWarning();
		}
	}

	/*
	getting form for inserting department
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreateDepartment()
	{
		//check roles
		if(canAdd('auth_department'))
		{
			//get all departments for user
			$dep = \DB::table('department');
			if(Auth::user()->is_manager == 1 && Auth::user()->is_admin == 0)
			{
				$dep->whereIn('id',getAllSubDepartmentsWithParent(Auth::user()->department_id));
			}
			$deps = $dep->get();
			//get all application for department
			$apps = Application::getAll();
			//load view for users list
			return View::make("auth.department.dep_create")->with('deps',$deps)->with('apps',$apps);
		}
		else
		{
			return showWarning();
		}
	}

	//get update department
	public function getUpdateDepartment($id=0)
	{
		//check roles
		if(canEdit('auth_department'))
		{

			//get department details
			$details = DepartmentX::getDetails($id);

			//get all departments for user
			$dep = \DB::table('department');
			if(Auth::user()->is_manager == 1 && Auth::user()->is_admin == 0)
			{
				$dep->whereIn('id',getAllSubDepartmentsWithParent(Auth::user()->department_id));
			}
			$deps = $dep->get();
			//get all application for department
			$apps = Application::getAll();
			//get department module
			$depApps = DepartmentX::getDepartmentApp($id);
			$data['deps'] = $deps;
			$data['details'] = $details;
			$data['apps'] = $apps;
			$data['depApps'] = $depApps;

			//load view for editing department
			return View::make('auth.department.dep_edit',$data);
		}
		else
		{
			return showWarning();
		}
	}

	/*
	Inserting filled form to department
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function postCreateDepartment()
	{
		//check roles
		if(canAdd('auth_department'))
		{
			//validate the input fields
		    $validates = \Validator::make(Input::all(),array(
		        "name" => "required"
		    ));

		    //check the validation
		    if($validates->fails())
		    {
		        return \Redirect::route("getCreateDepartment")->withErrors($validates)->withInput();
		    }
		    else
		    {

		        //create an object from department class
		        $dep = new DepartmentX();
		        $dep->name = Input::get("name");
		        $dep->name_en = Input::get("name_en");
		        $dep->name_pa = Input::get("name_pa");
		        $dep->location = Input::get("location");
		        $dep->user_id = 1;//Auth::user()->id;
				//$dep->advisor = 1;

		        if(Input::get('parent_dep'))
		        {
		        	$dep->parent = Input::get('parent_dep');
		        }

		        if($dep->save())
		        {
		            //get last dep id
		            $dep_id = $dep->id;
		            //check if moduel selected for department
		            if(count(Input::get('dep_app'))>0)
		            {
		            	$module = Input::get('dep_app');
		            	//an array for getting department module data
		            	$depModuleData = array();
		            	for($i=0;$i<count($module);$i++)
		            	{
		            		//row for array
		            		$row = array('department_id'=>$dep_id,'module_id'=>$module[$i]);
		            		//push row in array
		            		array_push($depModuleData, $row);
		            	}

		            	//insert batch to module department table
		            	DepartmentModule::insert($depModuleData);

		            }

		            return \Redirect::route("getAllDepartments")->with("success","You successfuly created new department.");
		        }
		        else
		        {
		            return \Redirect::route("getAllDepartments")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}

	}
	public function postUpdateDepartment($id=0)
	{
		//check roles
		if(canEdit('auth_department'))
		{
			//validate the input fields
		    $validates = \Validator::make(Input::all(),array(
		        "name" => "required"
		    ));

		    //check the validation
		    if($validates->fails())
		    {
		        return \Redirect::route("getUpdateDepartment")->withErrors($validates)->withInput();
		    }
		    else
		    {

		        //create an object from department class
		        $dep = DepartmentX::find($id);
		        $dep->name = Input::get("name");
		        $dep->name_en = Input::get("name_en");
		        $dep->name_pa = Input::get("name_pa");
		        $dep->location = Input::get("location");

		        if(Input::get('parent_dep'))
		        {
		        	$dep->parent = Input::get('parent_dep');
		        }

		        if($dep->save())
		        {

		            //first remove the old module for this department
		            DepartmentModule::deleteModuleDepartment($id);

		            //check if moduel selected for department
		            if(count(Input::get('dep_app'))>0)
		            {

		            	$module = Input::get('dep_app');
		            	//an array for getting department module data
		            	$depModuleData = array();
		            	for($i=0;$i<count($module);$i++)
		            	{
		            		//row for array
		            		$row = array('department_id'=>$id,'module_id'=>$module[$i]);
		            		//push row in array
		            		array_push($depModuleData, $row);
		            	}

		            	//insert batch to module department table
		            	DepartmentModule::insert($depModuleData);

		            }

		            return \Redirect::route("getAllDepartments")->with("success","You successfuly updated department.");
		        }
		        else
		        {
		            return \Redirect::route("getAllDepartments")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}

	}
	//delete department
	public function getDeleteDepartment($id=0)
	{
		//check roles
		if(canDelete('auth_department'))
		{
			if(DepartmentX::deleteDepartmentRelation($id))
			{
				$dep = DepartmentX::find($id);
				if($dep->delete())
		        {
		            return \Redirect::route("getAllDepartments")->with("success","You successfuly deleted record, ID: <span style='color:red;font_weight:bold;'>{{$id}}</span>");
		        }
		        else
		        {
		            return \Redirect::route("getAllDepartments")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}
	}

	/*
	Getting all records from modules
	@param: department id
	@Accessiblity: public
	@return: Object
	*/
	public function getDepartmentApp($dep_id=0)
	{
		//get data from model
		$apps = DepartmentX::getDepartmentApp($dep_id);

		//load view for list
		return View::make("auth.department.dep_module")->with('records',$apps);
	}

}


?>
