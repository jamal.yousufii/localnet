<?php namespace App\Http\Controllers;
/*
@desc: Manipulates the modules operations
@Author: Gul Muhammad Akbari (gm.akbari27@gmail.com)
@Created At: 23 Feb 2015
@version: 1.0
*/

use Auth;
use View;
use Input;
use App\models\Application;
use Illuminate\Support\Collection;
class ApplicationController extends Controller
{


	/*
	Getting all records from modules
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getAll()
	{
		//check roles
		if(canView('auth_application'))
		{
			//load view for list
			return View::make("auth.application.app_list");//->with('records',$apps);
		}
		else
		{
			return showWarning();
		}
	}
	//get data for datatable
	public function getData()
	{
		//check roles
		if(canView('auth_application'))
		{
			//get options in list
			
			//get data from model
			$apps = Application::getData();
			$collection = new Collection($apps);
			return \Datatable::collection($collection)
						->showColumns('id','code','name','description')
						->addColumn('operation',function($option)
							{
								$options = '';
								if(canEdit('auth_application'))
								{
									$options .= '<a href="'.route('getUpdateApp',$option->id).'"><i class="fa fa-edit"></i> Edit</a> &nbsp;';
								}
								if(canDelete('auth_application'))
								{
									$options .= '|&nbsp;<a href="'.route('getDeleteApp',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="fa fa-trash-o"></i> Delete</a>';
								}
								if(canView('auth_section'))
								{
									$options .= '&nbsp;|&nbsp; <a href="'.route('getAllSections',$option->id).'"><i class="fa fa-files-o"></i> Sections</a>';
								}
								return $options;
							}
						)
						->make();
		}
		else
		{
			return showWarning();
		}
	}

	/*
	getting form for inserting module
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreateApp()
	{
		//check roles
		if(canAdd('auth_application'))
		{
		
			//load view for users list
			return View::make("auth.application.app_create");
		}
		else
		{
			return showWarning();
		}
	}

	//get update module
	public function getUpdateApp($id=0)
	{

		//check roles
		if(canEdit('auth_application'))
		{

			//get details
			$details = Application::getDetails($id);
			
			//load view for editing 
			return View::make('auth.application.app_edit')->with('details',$details);
		}
		else
		{
			return showWarning();
		}

	}

	/*
	Inserting filled form to module
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function postCreateApp()
	{
		//check roles
		if(canAdd('auth_application'))
		{
			//validate the input fields
		    $validates = \Validator::make(Input::all(),array(
		        "code" => "required|unique:module",
		        "name" => "required"
		    ));
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return \Redirect::route("getCreateApp")->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        //create an object from department class
		        $object = new Application();
		        $object->code = Input::get("code");
		        $object->name = Input::get("name");
		        $object->description = Input::get("desc");
		        $object->user_id = Auth::user()->id;

		        if($object->save())
		        {
		            return \Redirect::route("getAllApplications")->with("success","You successfuly created new application.");
		        }
		        else
		        {
		            return \Redirect::route("getAllApplications")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}
		
	}
	public function postUpdateApp($id=0)
	{
		//check roles
		if(canEdit('auth_application'))
		{
			//validate the input fields
		    $validates = \Validator::make(Input::all(),array(
		        "code" => "required|unique:module",
		        "name" => "required"
		    ));
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return \Redirect::route("getUpdateApp",$id)->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        //create an object from department class
		        $dep = Application::find($id);
		        $dep->code = Input::get("code");
		        $dep->name = Input::get("name");
		        $dep->description = Input::get("desc");
		        
		        if($dep->save())
		        {
		            return \Redirect::route("getAllApplications")->with("success","You successfuly updated record.");
		        }
		        else
		        {
		            return \Redirect::route("getAllApplications")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}
		
	}
	//delete department
	public function getDeleteApp($id=0)
	{
		//check roles
		if(canDelete('auth_application'))
		{
			$dep = Application::find($id);
			if($dep->delete())
	        {
	            return \Redirect::route("getAllApplications")->with("success","You successfuly deleted record, ID: <span style='color:red;font_weight:bold;'>{{$id}}</span>");
	        }
	        else
	        {
	            return \Redirect::route("getAllApplications")->with("fail","An error occured plase try again.");
	        }
	    }
	    else
	    {
	    	return showWarning();
	    }
	}
	/*
	Getting all records from modules
	@param: department id
	@Accessiblity: public
	@return: Object
	*/
	public function getAppSection($id=0)
	{
		//get entity for module
		$sections = Section::getAll($id);
		
		//load view for list
		return View::make("auth.section.section_list")->with('records',$sections);
	}

}


?>