<?php 

namespace App\Http\Controllers\ocshr;

use App\Http\Controllers\Controller;
use App\models\hr\hrOperation;
use App\models\hr\attendanceServer;
use App\models\workplan\Report;

use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use DateTime;
use DateInterval;
use DatePeriod;
use Session;
use Mail;
use Crypt;

class hrController extends Controller
{
	//database connection
	public static $myDb = "hr";

	public function __construct()
	{
		if(!Session::has('lang'))
		{
			Redirect::route('getLogin')->send();
		}	
	}	
	public function homePage()
	{
		if(canViewChart())
		{
			$data['emp_degrees'] = hrOperation::getDegreesChart();
			$degrees = hrOperation::getDegrees();
			$dg_array=array(0=>'هیچکدام');
			foreach($degrees AS $dg)
			{
				$dg_array[$dg->id]=$dg->name_dr;
			}
			$data['degrees']=$dg_array;
			$gender = hrOperation::getGenders();
			$g_array=array();
			foreach($gender AS $g)
			{
				$g_array[$g->gender]=$g->total;
			}
			$data['gender']=$g_array;
			$types = hrOperation::getEmployeeTypes();
			$t_array=array();
			foreach($types AS $t)
			{
				$t_array[$t->employee_type]=$t->total;
			}
			$data['types']=$t_array;
			//dd($t_array);
			return View::make('hr.dashboard',$data);
		}
		else
		{
			return View::make('hr.attendance.dashboard_att');
		}
	}
	//get card template
	public function cardTemplate($id=0)
	{
		if(canPrintCard('hr_cards'))
		{
			$row = DB::connection('hr')
					->table('employees AS t1')
					->select(
						't1.id AS eid',
						't1.name_dr',
						't1.name_en',
						't1.last_name',
						//'t1.position_dr',
						//'t1.position_en',
						't1.current_position_dr',
						't1.current_position_en',
						'dep.name_pa AS general_department_dr',
						'dep1.name_pa AS department_dr',
						'dep.name_en AS general_department_en',
						'dep1.name_en AS department_en',
						'dep.id AS dep_id',
						'dep1.id AS sudep_id',
						'bg.name_en AS blood_group',
						'ps.name_dr AS position_dr',
						'ps.name_en AS position_en',
						'ps.color AS color',
						'ps.id AS position_id',
						't1.photo',
						't1.ready_to_print',
						't1.RFID'
						)
					->leftjoin('auth.department AS dep','dep.id','=','t1.general_department')
					->leftjoin('auth.department AS dep1','dep1.id','=','t1.department')
					->leftjoin('auth.blood_group AS bg','bg.id','=','t1.blood_group')
					->leftjoin('auth.position_short_title AS ps','ps.id','=','t1.short_title_id')
					->where('t1.id',$id)
					->first();
			if($row->ready_to_print==0 || $row->RFID !=null)
			{
				return showWarning();
			}
			else
			{
				return View::make('hr.card',array('row'=>$row));
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function cardTemplateTemp($id=0)
	{
		if(canPrintCard('hr_cards'))
		{
			$row = DB::connection('hr')
					->table('employees AS t1')
					->select(
						't1.id AS eid',
						't1.name_dr',
						't1.name_en',
						't1.last_name',
						//'t1.position_dr',
						//'t1.position_en',
						't1.current_position_dr',
						't1.current_position_en',
						'dep.name AS general_department_dr',
						'dep1.name AS department_dr',
						'dep.name_en AS general_department_en',
						'dep1.name_en AS department_en',
						'dep.id AS dep_id',
						'bg.name_en AS blood_group',
						'ps.name_dr AS position_dr',
						'ps.name_en AS position_en',
						'ps.color AS color',
						'ps.id AS position_id',
						't1.contract_expire_date AS expire',
						't1.employee_type',
						't1.photo',
						't1.ready_to_print',
						't1.RFID'
						)
					->leftjoin('auth.department AS dep','dep.id','=','t1.general_department')
					->leftjoin('auth.department AS dep1','dep1.id','=','t1.department')
					->leftjoin('auth.blood_group AS bg','bg.id','=','t1.blood_group')
					->leftjoin('auth.position_short_title AS ps','ps.id','=','t1.short_title_id')
					->where('t1.id',$id)
					->first();
			if($row->ready_to_print==0 || $row->RFID !=null)
			{
				return showWarning();
			}
			elseif($row->expire==null)
			{
				return Redirect::route('getEmployeeIDs')->with("fail","user's card does not have expiry date");
			}
			else
			{
				return View::make('hr.cardTemp',array('row'=>$row));
			}
		}
		else
		{
			return showWarning();
		}
	}
	//bring related department
	public function bringSubDepartment()
	{
		$deps = getRelatedSubDepartment(Input::get('dep_id'));

		$options = '<option value="0">همه</option>';
		foreach($deps AS $item)
		{
			$options.="<option value='".$item->id."'>".$item->name."</option>";
		}
	
		return $options;
	}
	//Load documents form entry view
	public function getAllEmployees()
	{
		//check roles
		if(canView('hr_employee'))
		{
			//check roles
			return View::make('hr.employee_list');
		}
		else
		{
			return showWarning();
		}
	}
	//get employee ids
	public function getEmployeeIDs()
	{
		//check roles
		if(canUpdateCard('hr_cards') || canPrintCard('hr_cards'))
		{
			//check roles
			return View::make('hr.employee_ids');
		}
		else
		{
			return showWarning();
		}

	}
	//get form data list
	public function getEmployeeData()
	{	
		$user_deps = getHRUserDeps();
		if($user_deps)
		{
			$user_deps = explode(',',$user_deps->deps);
			//$user_deps = $user_deps->deps;
		}
		else
		{
			$user_deps = 0;
		}
		
		$object = hrOperation::getData('dr',$user_deps);//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name',
							'number_tayenat',
							'department',
							'original_province',
							'bast',
							'emp_date'
							)
				->addColumn('operations', function($option){
					$options = '';
					/*
					$options .= '<a href="'.route('getEmployeeDetails',$option->id).'" class="table-link">
														<span class="fa-stack">
										<i class="fa fa-square fa-stack-2x"></i>
										<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
									</span>
								</a>
								';
					*/
					if(canEdit('hr_recruitment') || canEdit('hr_documents'))
					{
						$options .= '<a href="'.route('getDetailsEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
					}
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	//get form data list
	public function getEmployeeCardData()
	{
		//get all data 
		$object = hrOperation::getDataCard();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'father_name',
							'number_tayenat',
							'gender',
							'original_province',
							'rank',
							'emp_date'
							)
				->addColumn('operations', function($option){
					$options = '';
						if(canUpdateCard('hr_cards'))
						{
							if(Auth::user()->id!=203)
							{
							$options.= '<a href="'.route('getEmployeeCardDetails',$option->id).'" class="table-link" style="text-decoration:none;" title="update">
										<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
									</a>
									| ';
							}
							$options.=
									' <a href="'.route('forPrint',$option->id).'" class="table-link" style="text-decoration:none;" title="Details">
										<i class="icon fa-eye" aria-hidden="true" style="font-size: 16px;"></i>
									</a> | ';
						}
						if(canPrintCard('hr_cards'))
						{
							if($option->employee_type == 4 || $option->employee_type == 5)
							{//temporary employees
								/*
								<a href="javascript:void()" class="table-link" data-target="#rfid_modal" data-toggle="modal" onclick="$(\'#record_id\').val('.$option->id.')">
										<span class="fa-stack" title="Enter RFID">
											<span class="icon wb-edit"></span>
										</span>
									</a>
								*/
								$options.= '<a target="_blank" href="'.route('getCardTemplateTemp',$option->id).'" class="table-link" style="text-decoration:none;" title="print">
										<i class="icon fa-print" aria-hidden="true" style="font-size: 16px;"></i>
									</a>
									| 
									<a href="javascript:void()" class="table-link" onclick="sendBack('.$option->id.')">
										<span class="fa-stack" title="Send Back For Update">
											<span class="icon wb-replay"></span>
										</span>
									</a>															
															';
							}
							else
							{
								/*
								<a href="javascript:void()" class="table-link" data-target="#rfid_modal" data-toggle="modal" onclick="$(\'#record_id\').val('.$option->id.')">
										<span class="fa-stack" title="Enter RFID">
											<span class="icon wb-edit"></span>
										</span>
									</a>
								*/
								
								$options.= '<a target="_blank" href="'.route('getCardTemplate',$option->id).'" class="table-link" style="text-decoration:none;" title="print">
										<i class="icon fa-print" aria-hidden="true" style="font-size: 16px;"></i>
									</a>
									 | 
									<a href="javascript:void()" class="table-link" onclick="sendBack('.$option->id.')">
										<span class="fa-stack" title="Send Back For Update">
											<span class="icon wb-replay"></span>
										</span>
									</a>															
															';
							}
						}
						return $options;
				})
				->make();
	}
	//Load documents form entry view
	public function getEmployeeForPrint()
	{
		//check roles
		//if(canView('hr_employee'))
		//{
			$data['parentDeps'] = getDepartmentWhereIn();
			//check roles
			return View::make('hr.employee_print_list',$data);
		//}
		//else
		//{
			//return showWarning();
		//}
	}
	//get form data list
	public function getEmployeeDataPrint()
	{
		//get all data 
		$object = hrOperation::getData();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'department',
							'name',
							'current_position_dr'
							)
						->addColumn('name_en', function($option){

							return "";
						})
						->addColumn('position_en', function($option){

							return "";
						})
				
				// ->orderColumns('id','id')
				->make();
	}
	//get register form for employee
	public function getRegisterForm()
	{
		//check roles
		if(canAdd('hr_employee'))
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			//get province
			$data['provinces'] = getAllProvinces('dr');
			//load view for registring
			return View::make('hr.employee_add',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//get employee ids
	public function getEmployeeCardForm($id=0)
	{
		//check roles
		if(canUpdateCard('hr_cards') && Auth::user()->id!=203)
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['parentDeps'] = getAllParentDepartments();
			//check roles
			return View::make('hr.employee_card_form',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//get register form for employee
	public function getEmployeeDetails($id=0)
	{
		//check roles
		if(canView('hr_employee'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachments'] = hrOperation::getDocuments($id);
			$data['parentDeps'] = getDepartmentWhereIn();
			//get province
			$data['provinces'] = getAllProvinces('dr');
			//load view for registring
			return View::make('hr.employee_details',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//get register form for employee
	public function forPrint($id=0)
	{
		//check roles
		if(canUpdateCard('hr_cards'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['parentDeps'] = getAllDepartments();
			//load view for registring
			return View::make('hr.for_signature',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//check data if ready to print
	public function checkCardReady()
	{
		DB::connection(self::$myDb)->table('employees')->where('id',Input::get('id'))->update(array('ready_to_print'=>Input::get('ready')));
	}
	//save rfid
	public function saveRFID()
	{
		DB::connection(self::$myDb)
		->table('employees')
		->where('id',Input::get('record_id'))
		->update(array('RFID'=>Input::get('RFID')));

		return Redirect::route('getEmployeeIDs')->with("success","Record updated successfully");
	}
	//Load documents form entry view
	public function postEmployee()
	{
		//check roles
		if(canAdd('hr_employee'))
		{
			if(Input::get('vacant')!=1)
			{
				//validate fields
				$validates = Validator::make(Input::all(), array(
					"name_dr"						=> "required",
					"current_position_dr"			=> "required",
					// "emp_rank"					=> "required",
					"position_dr"					=> "required"
				));
			}
			else
			{
				//validate fields
				$validates = Validator::make(Input::all(), array(
					"vacant"						=> "required"
				));
			}
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('getRegisterForm')->withErrors($validates)->withInput();
			}
			else
			{
				$object = new hrOperation;
				
				$object->number_tayenat 						= Input::get("number_tayenat");
				$object->vacant									= Input::get("vacant");
				if(Input::get('vacant') == 1)
				{

					$object->emp_bast								= Input::get("emp_bast1");
					$object->current_position_dr					= Input::get("current_position_dr1");
					$object->general_department						= Input::get("general_department1");
					$object->department								= Input::get("sub_dep1");
					$object->ageer_rank								= Input::get("ageer_rank1");
					$object->ageer_bast								= Input::get("ageer_bast1");
				
				}
				else
				{
					$object->name_dr 								= Input::get("name_dr");
					//$object->name_en 								= Input::get("name_en");
					$object->father_name_dr 						= Input::get("father_name_dr");
					//$object->father_name_en 						= Input::get("father_name_en");
					$object->gender 								= Input::get("gender");
					$object->original_province 						= Input::get("original_province");
					$object->nationality 							= Input::get("nationality");
					$object->district 								= Input::get("district");
					$object->education_degree						= Input::get("education_degree");
					$object->education_field						= Input::get("education_field");
					$object->blood_group							= Input::get("blood_group");
					$object->birth_year								= Input::get("birth_year");
					$object->first_date_appointment					= Input::get("first_appointment_date");
					$object->last_date_tarfee						= Input::get("last_appointment_date");
					if(Input::get('employee_type') == 3)
					{
						$object->emp_rank								= Input::get("military_rank");
						$object->emp_bast								= Input::get("military_bast");
					}
					else
					{
						$object->emp_rank								= Input::get("emp_rank");
						$object->emp_bast								= Input::get("emp_bast");
					}
					
					$object->ageer_rank								= Input::get("ageer_rank");
					$object->ageer_bast								= Input::get("ageer_bast");
					$object->appointment_date_current_position		= Input::get("appointment_current_position");
					$object->phone									= Input::get("phone");
					$object->current_position_dr						= Input::get("current_position_dr");
					//$object->current_position_en						= Input::get("current_position_en");
					$object->position_dr							= Input::get("position_dr");
					//$object->position_en							= Input::get("position_en");
					$object->employee_type							= Input::get("employee_type");
					$object->free_compitition						= Input::get("free_competition");
					//$object->department_en							= Input::get("department_en");
					$object->general_department						= Input::get("general_department");
					$object->department								= Input::get("sub_dep");
					//$object->general_department_en					= Input::get("general_department_en");
					$object->education_rank							= Input::get("education_rank");
					$object->mawqif_employee						= Input::get("mawqif_employee");
					$object->email									= Input::get("email");
					$object->job_description						= Input::get("job_description");
				}

				$object->created_by 							= Auth::user()->id;
			
				if($object->save())
				{
					$inserted_id = $object->id;

					$this->uploadDocs($inserted_id);
					
					return Redirect::route('getAllEmployees')->with("success","Record saved successfully");
				}
				else
				{
					return Redirect::route("getAllEmployees")->with("fail","An error occured plase try again.");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	//update document
	public function postUpdateEmployee($doc_id=0)
	{
		if(canEdit('hr_employee'))
		{
			if(Input::get('vacant')!=1)
			{
				//validate fields
				$validates = Validator::make(Input::all(), array(
					"name_dr"						=> "required",
					"current_position_dr"			=> "required",
					// "emp_rank"					=> "required",
					"position_dr"					=> "required"
				));
			}
			else
			{
				//validate fields
				$validates = Validator::make(Input::all(), array(
					"vacant"						=> "required"
				));
			}

			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('getRegisterForm')->withErrors($validates)->withInput();
			}
			else
			{
				$object = hrOperation::find($doc_id);
				
				$object->number_tayenat 						= Input::get("number_tayenat");
				$object->vacant									= Input::get("vacant");
				if(Input::get('vacant') == 1)
				{
					$object->emp_bast								= Input::get("emp_bast1");
					$object->current_position_dr						= Input::get("current_position_dr1");
					$object->general_department						= Input::get("general_department1");
					$object->department								= Input::get("sub_dep1");
					$object->ageer_rank								= Input::get("ageer_rank1");
					$object->ageer_bast								= Input::get("ageer_bast1");
				}
				else
				{
					$object->name_dr 								= Input::get("name_dr");
					//$object->name_en 								= Input::get("name_en");
					$object->father_name_dr 						= Input::get("father_name_dr");
					//$object->father_name_en 						= Input::get("father_name_en");
					$object->gender 								= Input::get("gender");
					$object->original_province 						= Input::get("original_province");
					$object->nationality 							= Input::get("nationality");
					$object->district 								= Input::get("district");
					$object->education_degree						= Input::get("education_degree");
					$object->education_field						= Input::get("education_field");
					$object->blood_group							= Input::get("blood_group");
					$object->birth_year								= Input::get("birth_year");
					$object->first_date_appointment					= Input::get("first_appointment_date");
					$object->last_date_tarfee						= Input::get("last_appointment_date");
					if(Input::get('employee_type') == 3)
					{
						$object->emp_rank								= Input::get("military_rank");
						$object->emp_bast								= Input::get("military_bast");

					}
					else
					{
						$object->emp_rank								= Input::get("emp_rank");
						$object->emp_bast								= Input::get("emp_bast");
					}
					$object->ageer_rank								= Input::get("ageer_rank");
					$object->ageer_bast								= Input::get("ageer_bast");
					$object->appointment_date_current_position		= Input::get("appointment_current_position");
					$object->phone									= Input::get("phone");
					$object->current_position_dr						= Input::get("current_position_dr");
					//$object->current_position_en						= Input::get("current_position_en");
					$object->position_dr							= Input::get("position_dr");
					//$object->position_en							= Input::get("position_en");
					$object->employee_type							= Input::get("employee_type");
					$object->free_compitition						= Input::get("free_competition");
					
					//$object->department_en							= Input::get("department_en");
					$object->general_department						= Input::get("general_department");
					$object->department								= Input::get("sub_dep");
					//$object->general_department_en					= Input::get("general_department_en");
					
					$object->education_rank							= Input::get("education_rank");
					$object->mawqif_employee						= Input::get("mawqif_employee");
					$object->email									= Input::get("email");
					$object->job_description						= Input::get("job_description");
				}
				
				$object->updated_by 							= Auth::user()->id;

				if($object->save())
				{
					$inserted_id = $doc_id;

					$this->uploadDocs($inserted_id);

					return Redirect::route('getAllEmployees')->with("success","Record updated successfully");
				}
				else
				{
					return Redirect::route("getAllEmployees")->with("fail","An error occured plase try again.");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	//update document
	public function updateEmployee($doc_id=0)
	{
			//check roles
		if(canEdit('hr_recruitment'))
		{
			//validate
			if(input::get('tashkil_id')==0)
			{//dont validate if user is not active
				$validates = Validator::make(Input::all(), array(
						"name_dr"						=> "required",
						//"current_position_dr"			=> "required",
						//"first_appointment_date"		=> "required",
						"blood_group"					=> "required",
						//"general_department1"			=> "required",
						//"sub_dep1"						=> "required",
						"father_name_dr"				=> "required",
						"grand_father_name"				=> "required"
						
					));
			}
			else
			{
				if(input::get('employee_type')==4 || input::get('employee_type')==5)
				{//temporary employees
					$validates = Validator::make(Input::all(), array(
						"employee_type"					=> "required",
						"name_dr"						=> "required",
						"last_date"						=> "required",
						//"first_appointment_date"		=> "required",
						"blood_group"					=> "required",
						"general_department1"			=> "required",
						"sub_dep1"						=> "required",
						"father_name_dr"				=> "required",
						"grand_father_name"				=> "required"
						
					));
				}
				else
				{//normal employees
					$validates = Validator::make(Input::all(), array(
						"name_dr"						=> "required",
						"tashkil"						=> "required",
						"employee_type"					=> "required",
						"blood_group"					=> "required",
						"general_department"			=> "required",
						"sub_dep"						=> "required",
						"father_name_dr"				=> "required",
						"grand_father_name"				=> "required"
						
					));
				}
			}
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('getDetailsEmployee',$doc_id)->withErrors($validates)->withInput();
			}
			else
			{	
				$object = hrOperation::find($doc_id);
				hrOperation::update_record('employees',array('tashkil_id_old'=>$object->tashkil_id),array('id'=>$doc_id));
				//update the status of current tashkil id from used to unused because it is gonna be updated
				hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$object->tashkil_id));
				$object->vacant									= 2;//mawjod
				$object->employee_type							= Input::get("employee_type");
				if(Input::get('employee_type') == 4 || Input::get('employee_type') == 5)
				{//contract
					$object->contract_expire_date				= Input::get("last_date");
					$object->general_department					= Input::get("general_department1");
					$object->department							= Input::get("sub_dep1");
				}
				else
				{
					$object->number_tayenat 					= Input::get("number_tayenat");
					$object->general_department					= Input::get("general_department");
					$object->department							= Input::get("sub_dep");
					if(Input::get("position_dr")==7 || Input::get("position_dr")==4)
					{//ezafa bast || wait for salary
						$object->tashkil_id							= 0;
					}
					else
					{
						$object->tashkil_id							= Input::get('tashkil');
					}
					$object->position_dr						= Input::get("position_dr");
					if(Input::get('employee_type') == 3)
					{//military
						$object->emp_rank						= Input::get("military_rank");
						//$object->emp_bast						= Input::get("military_bast");
	
					}
					elseif(Input::get('employee_type') == 2)
					{//ajeer
						$object->emp_rank						= Input::get("ajeer_rank");
					}
					else
					{
						$object->emp_rank						= Input::get("emp_rank");
						//$object->emp_bast						= Input::get("emp_bast");
					}
				}
				if(Input::get("mawqif_employee")==1)
				{//if user is active employee, unset all unactive feilds
					$object->fired					= 0;
					$object->resigned				= 0;
					$object->vacant					= 2;//mawjod
					$object->changed				= 0;
					$object->retired				= 0;
				}
				elseif(Input::get("mawqif_employee")==5)
				{
					$object->retired				= 1;
				}
				elseif(Input::get("mawqif_employee")==6)
				{
					$object->fired				= 1;
				}
				elseif(Input::get("mawqif_employee")==7)
				{//ezafa bast
					$object->tashkil_id				= 0;
				}
				elseif(Input::get("mawqif_employee")==8)
				{
					$object->changed				= 1;
				}
				$object->free_compitition						= Input::get("free_compitition");
				$object->mawqif_employee						= Input::get("mawqif_employee");
				$object->first_date_appointment					= Input::get("first_appointment_date");
				$object->current_position_dr					= Input::get("current_position_dr");
				$object->name_dr 								= Input::get("name_dr");
				$object->father_name_dr 						= Input::get("father_name_dr");
				$object->nationality 							= Input::get("nationality");
				$object->gender 								= Input::get("gender");
				$object->blood_group							= Input::get("blood_group");
				$object->birth_year								= Input::get("birth_year");
				$object->original_province 						= Input::get("original_province");
				$object->district 								= Input::get("district");
				//$object->education_degree						= Input::get("education_degree");
				//$object->education_field						= Input::get("education_field");
				$object->phone									= Input::get("phone");
				$object->email									= Input::get("email");
				$object->last_name 								= Input::get("last_name");
				$object->grand_father_name 						= Input::get("grand_father_name");
				$object->id_no 									= Input::get("id_no");
				$object->id_jild								= Input::get("id_jild");
				$object->id_page								= Input::get("id_page");
				$object->id_sabt								= Input::get("id_sabt");
				$object->village 								= Input::get("village");
				$object->current_province 						= Input::get("current_province");
				$object->current_district 						= Input::get("current_district");
				$object->current_village 						= Input::get("current_village");
				//$object->education_place						= Input::get("education_place");
				//$object->graduation_year						= Input::get("graduation_year");
				//$object->updated_by 							= Auth::user()->id;
				
				$emp_bast = hrOperation::getEmployeeBast(Input::get('tashkil'));
				if($emp_bast)
				{
					$object->emp_bast								= $emp_bast;
				}
				//$object->edu_location							= Input::get("edu_location");
				
				$object->updated_by 							= Auth::user()->id;
				$object->updated_at 							= date('Y-m-d H:i:s');
				
				if($object->save())
				{
					hrOperation::insertLog('employees',1,'details of given employee',$doc_id);
					if(Input::get('tashkil'))
					{//update the bast id to used
						hrOperation::update_record('tashkilat',array('status'=>1),array('id'=>Input::get('tashkil')));
					}
					
					$inserted_id = $doc_id;
					if(canEdit('hr_documents'))
					{
						hrOperation::delete_record('employee_educations',array('employee_id'=>$inserted_id));
						for($e=1;$e<=2;$e++)
						{
							if(Input::get('education_degree_'.$e)!='')
							{
								$edu = array(
									"employee_id" 		=> $inserted_id,
									"education_id" 		=> Input::get('education_degree_'.$e),
									"education_field" 	=> Input::get('education_field_'.$e),
									"edu_location"		=> Input::get('edu_location_'.$e),
									"education_place" 	=> Input::get('education_place_'.$e),
									"graduation_year" 	=> Input::get('graduation_year_'.$e)
								);
								hrOperation::insertRecord('employee_educations',$edu);
							}
						}
					}
					hrOperation::delete_record('employee_experiences',array('employee_id'=>$inserted_id,'type'=>0));
					for($i=1;$i<=3;$i++)
					{
						if(Input::get('experience_position_'.$i)!='')
						{
							$experience = array(
								"employee_id" 	=> $inserted_id,
								"organization" 	=> Input::get('experience_company_'.$i),
								"position" 		=> Input::get('experience_position_'.$i),
								"bast"			=> Input::get('experience_bast_'.$i),
								"leave_reason" 	=> Input::get('experience_leave_'.$i),
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
							hrOperation::insertRecord('employee_experiences',$experience);
						}
					}
					hrOperation::delete_record('employee_languages',array('employee_id'=>$inserted_id));
					for($i=1;$i<=4;$i++)
					{
						$langs = array(
								"employee_id" 	=> $inserted_id,
								"language" 		=> Input::get('lang_'.$i),
								"writing" 		=> Input::get('writing_'.$i),
								"speaking"		=> Input::get('speaking_'.$i),
								"reading" 		=> Input::get('reading_'.$i),
								"typing"		=> Input::get('typing_'.$i),
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
						hrOperation::insertRecord('employee_languages',$langs);
					}
					hrOperation::delete_record('employee_garantee',array('employee_id'=>$inserted_id));
					for($i=1;$i<=2;$i++)
					{
						if(Input::get('garantee_name_'.$i)!='')
						{
							$garantee = array(
								"employee_id" 	=> $inserted_id,
								"name" 			=> Input::get('garantee_name_'.$i),
								"job" 			=> Input::get('garantee_job_'.$i),
								"organization"	=> Input::get('garantee_company_'.$i),
								"phone" 		=> Input::get('garantee_phone_'.$i),
								"email"			=> Input::get('garantee_email_'.$i),
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
							hrOperation::insertRecord('employee_garantee',$garantee);
						}
					}
					$this->uploadDocs($inserted_id);

					return Redirect::route('getDetailsEmployee',$doc_id)->with("success","Record updated successfully");
				}
				else
				{
					return Redirect::route("getDetailsEmployee",$doc_id)->with("fail","An error occured plase try again.");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function updateEmployeeContract($doc_id=0)
	{
			//check roles
		if(canEdit('hr_recruitment'))
		{
			//validate fields
			$validates = Validator::make(Input::all(), array(
				"name_dr"						=> "required",
				"last_date"						=> "required",
				//"first_appointment_date"		=> "required",
				"blood_group"					=> "required",
				"general_department"			=> "required",
				"sub_dep"						=> "required",
				"father_name_dr"				=> "required",
				"grand_father_name"				=> "required"
				
			));
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('getContractEmployee',$doc_id)->withErrors($validates)->withInput();
			}
			else
			{	
				$object = hrOperation::find($doc_id);
				
				//$object->number_tayenat 						= Input::get("number_tayenat");
				//$object->vacant									= Input::get("vacant");
				
					$object->employee_type							= Input::get("employee_type");
					$object->free_compitition						= Input::get("free_compitition");
					$object->mawqif_employee						= Input::get("mawqif_employee");
					$object->first_date_appointment					= Input::get("first_appointment_date");
					$object->current_position_dr					= Input::get("current_position_dr");
					
					$object->general_department						= Input::get("general_department");
					$object->department								= Input::get("sub_dep");
					//$object->position_dr							= Input::get("position_dr");
					//$object->emp_bast								= $emp_bast;
					$object->contract_expire_date					= Input::get("last_date");
					
					$object->name_dr 								= Input::get("name_dr");
					$object->father_name_dr 						= Input::get("father_name_dr");
					$object->last_name 								= Input::get("last_name");
					$object->grand_father_name 						= Input::get("grand_father_name");
					$object->id_no 									= Input::get("id_no");
					$object->id_jild								= Input::get("id_jild");
					$object->id_page								= Input::get("id_page");
					$object->id_sabt								= Input::get("id_sabt");
					$object->nationality 							= Input::get("nationality");
					$object->gender 								= Input::get("gender");
					$object->blood_group							= Input::get("blood_group");
					$object->birth_year								= Input::get("birth_year");
					$object->original_province 						= Input::get("original_province");
					$object->district 								= Input::get("district");
					$object->village 								= Input::get("village");
					$object->current_province 						= Input::get("current_province");
					$object->current_district 						= Input::get("current_district");
					$object->current_village 						= Input::get("current_village");
					//$object->education_degree						= Input::get("education_degree");
					//$object->education_field						= Input::get("education_field");
					//$object->education_place						= Input::get("education_place");
					//$object->graduation_year						= Input::get("graduation_year");
					//$object->edu_location							= Input::get("edu_location");
					$object->phone									= Input::get("phone");
					$object->email									= Input::get("email");
					
				$object->updated_by 								= Auth::user()->id;
				$object->updated_at 								= date('Y-m-d H:i:s');
			
				if($object->save())
				{
					hrOperation::insertLog('employees',1,'details of given employee',$doc_id);
					if(canEdit('hr_documents'))
					{
						hrOperation::delete_record('employee_educations',array('employee_id'=>$doc_id));
						for($e=1;$e<=2;$e++)
						{
							if(Input::get('education_degree_'.$e)!='')
							{
								$edu = array(
									"employee_id" 		=> $doc_id,
									"education_id" 		=> Input::get('education_degree_'.$e),
									"education_field" 	=> Input::get('education_field_'.$e),
									"edu_location"		=> Input::get('edu_location_'.$e),
									"education_place" 	=> Input::get('education_place_'.$e),
									"graduation_year" 	=> Input::get('graduation_year_'.$e)
								);
								hrOperation::insertRecord('employee_educations',$edu);
							}
						}
					}
					return Redirect::route('getRecruitment')->with("success","Record updated successfully");
				}
				else
				{
					return Redirect::route("getContractEmployee",$doc_id)->with("fail","An error occured plase try again.");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	//update document
	public function postEmployeeCard($doc_id=0)
	{
		if(canUpdateCard('hr_cards'))
		{
			$validates = Validator::make(Input::all(), array(
				"name_en"						=> "required",
				//"current_position_en"			=> "required"
				// "position_en"					=> "required"
			));

			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('getRegisterForm')->withErrors($validates)->withInput();
			}
			else
			{
				$object = hrOperation::find($doc_id);
				$object->name_en 								= Input::get("name_en");
				//$object->father_name_dr 						= Input::get("father_name_dr");
				$object->father_name_en 						= Input::get("father_name_en");
				
				//$object->current_position_dr					= Input::get("current_position_dr");
				//$object->current_position_en					= Input::get("current_position_en");
				//$object->position_dr							= Input::get("position_dr");
				$object->position_en							= Input::get("position_en");
		
				$object->short_title_id							= Input::get("short_title_id");
				
				if($object->save())
				{
					$inserted_id = $doc_id;
					
					$this->uploadPhoto($inserted_id);
					//------------------------------------------//
					return Redirect::route('getEmployeeIDs')->with("success","Card information updated successfully");
				}
				else
				{
					return Redirect::route("getEmployeeIDs")->with("fail","An error occured plase try again.");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	//upload document
	public function uploadDocs($doc_id=0)
	{
		// getting all of the post data
		$files = Input::file('files');
		$errors = "";
		$file_data = array();
		if(Input::hasFile('files'))
		{
			foreach($files as $file) 
			{
		  		// validating each file.
		  		$rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
		  		$validator = Validator::make(

			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'file' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png,doc,docx,zip,rar,pdf,rtf,xlsx,xls,txt'
			        ]
			  	);

			  	if($validator->passes())
			  	{
			  	
				    // path is root/uploads
				    $destinationPath = 'documents/hr_attachments';
				    $filename = $file->getClientOriginalName();
	
				    $temp = explode(".", $filename);
				    $extension = end($temp);
				    $lastFileId = $doc_id.'_'.time();
				    
				    $lastFileId++;
				    
				    $filename = $temp[0].'_'.$lastFileId.'.'.$extension;
	
				    $upload_success = $file->move($destinationPath, $filename);

				    if($upload_success) 
				    {
				   		$data = array(
				   					'doc_id'=>$doc_id,
				   					'file_name'=>$filename,
				   					'created_at'=>date('Y-m-d H:i:s'),
				   					'user_id'	=> Auth::user()->id
				   				);
				   		if(count($data)>0)
						{
							
							DB::connection('hr')->table('attachments')->insert($data);
						}
						   
					} 
					else 
					{
					   $errors .= json('error', 400);
					}
				} 
			  	else 
			  	{
				    // redirect back with errors.
				    return Redirect::back()->withErrors($validator);
			  	}
			}
		}
	}
	//upload document
	public function uploadPhoto($doc_id=0)
	{
		if(Input::hasFile('photo'))
		{
			// getting all of the post data
			$file = Input::file('photo');
			$errors = "";
			$file_data = array();
			// validating each file.
			$rules = array('photo' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			$validator = Validator::make(

			  		[
			            'photo' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'photo' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			);

			if($validator->passes())
			{
			  	$destinationPath = 'documents/profile_pictures';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    $lastFileId = getLastDocId();
			    
			    $lastFileId++;
			    
			    $filename = 'profile_'.$doc_id.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
			    	//remove the old small profile picture
				   	$file= public_path(). "/documents/profile_pictures/small_".$filename;
				   	File::delete($file);
				   	
					DB::connection('hr')->table('employees')->where('id',$doc_id)->update(array('photo'=>$filename));
				} 
				else 
				{
				   $errors .= json('error', 400);
				}
			} 
			else 
			{
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			}
		}
		if(Input::hasFile('signature'))
		{
			// getting all of the post data
			$file = Input::file('signature');
			$errors = "";
			$file_data = array();
			// validating each file.
			$rules = array('signature' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			$validator = Validator::make(

			  		[
			            'signature' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'signature' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			);

			if($validator->passes())
			{
			  	
			    // path is root/uploads
			    $destinationPath = 'documents/signatures';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    $lastFileId = getLastDocId();
			    
			    $lastFileId++;
			    
			    $filename = 'signature_'.$doc_id.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
					DB::connection('hr')->table('employees')->where('id',$doc_id)->update(array('signature'=>$filename));
				} 
				else 
				{
				   $errors .= json('error', 400);
				}
			} 
			else 
			{
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			}
		}
	}
	//download file from server
	public function downloadDoc($file_id=0)
	{
		if(canView('hr_recruitment'))
		{
			//get file name from database
			$fileName = getEmployeeFileName($file_id);
	        //public path for file
	        $file= public_path(). "/documents/hr_attachments/".$fileName;
	        //download file
	        return Response::download($file, $fileName);
	    }
	    else
	    {
	    	return showWarning();
	    }
	}
	//remove file from folder
	public function deleteFile()
	{
		if(canEdit('hr_recruitment'))
		{
			$file_id = Input::get('doc_id');
			
			//get file name from database
			$fileName = getEmployeeFileName($file_id);
			$file= public_path(). "/documents/hr_attachments/".$fileName;
			if(File::delete($file))
			{
				//delete from database
				DB::connection('hr')
				->table('attachments')
				->where('id',$file_id)
				->delete();
				
				return "<div class='alert alert-success'>File Deleted Successfully!</div>";
			}
			else
			{
				return "<div class='alert alert-danger'>Error!</div>";
			}
		}
		else
		{
			return showWarning();
		}
	}
	//get province districtsgetAllEmployees
	public function getProvinceDistrict()
	{
		$districts = getProvinceDistrict(Input::get('province'));

		$options = "<option value=''>??????</option>";
		foreach($districts AS $item)
		{
			$options .= "<option value='".$item->id."'>".$item->name."</option>";
		}

		return $options;
	}
	//get department employees
	public function searchDepartmentEmployees()
	{
		$results = hrOperation::getDepartmentEmployees();

		$data['rows'] = $results;
		if(Input::get('sub_dep') == '')
		{
			$data['department'] = Input::get('general_department');
		}
		else
		{
			$data['department'] = Input::get('sub_dep');
		}

		return View::make('hr.print_list',$data);
	}
	//Load documents form entry view
	public function getRecruitment()
	{
		//check roles
		if(canView('hr_recruitment') || canView('hr_documents'))
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['active'] = 'all';
			return View::make('hr.recruitment.list',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//get register form for employee
	public function addNewEmployee()
	{
		//check roles
		if(canAdd('hr_recruitment'))
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			//get province
			$data['provinces'] = getAllProvinces('dr');
			//$data['tashkils'] = hrOperation::getTashkilData('dr');
			//load view for registring
			return View::make('hr.recruitment.add',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function checkEmployeeDuplicate()
	{
		if(hrOperation::check_employee_add(Input::get('name_dr'),Input::get('father_name_dr'),Input::get('grand_father_name'),Input::get('general_department'),Input::get('sub_dep')))
		{//already added
			return true;
		}
		else
		{
			return false;
		}
	}
	//Load documents form entry view
	public function postNewEmployee()
	{
		//check roles
		if(canAdd('hr_recruitment'))
		{
			//validate
			if(input::get('employee_type')==4 || input::get('employee_type')==5)
			{//temporary employees
				$validates = Validator::make(Input::all(), array(
					"employee_type"					=> "required",
					"name_dr"						=> "required",
					"last_date"						=> "required",
					//"first_appointment_date"		=> "required",
					"blood_group"					=> "required",
					"general_department1"			=> "required",
					"sub_dep1"						=> "required",
					"father_name_dr"				=> "required",
					"grand_father_name"				=> "required"
					
				));
			}
			else
			{//normal employees
				$validates = Validator::make(Input::all(), array(
					"name_dr"						=> "required",
					"tashkil"						=> "required",
					"employee_type"					=> "required",
					"blood_group"					=> "required",
					"general_department"			=> "required",
					"sub_dep"						=> "required",
					"father_name_dr"				=> "required",
					"grand_father_name"				=> "required"
					
				));
			}
			$validates->after(function($validates)
			{
			    if ($this->checkEmployeeDuplicate())
			    {//check if same name,fathername,grandfathername in same general dep, sub dep
			        $validates->errors()->add('field', 'duplicate entry');
			    }
			});
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('addNewEmployee')->withErrors($validates)->withInput();
			}
			else
			{
				$object = new hrOperation;
				
				$object->vacant									= 2;//mawjod
				$object->employee_type							= Input::get("employee_type");
				if(Input::get('employee_type') == 4 || Input::get('employee_type') == 5)
				{//contract
					$object->contract_expire_date				= Input::get("last_date");
					$object->general_department					= Input::get("general_department1");
					$object->department							= Input::get("sub_dep1");
				}
				else
				{
					$object->number_tayenat 					= Input::get("number_tayenat");
					$object->general_department					= Input::get("general_department");
					$object->department							= Input::get("sub_dep");
					$object->tashkil_id							= Input::get('tashkil');
					$object->position_dr						= Input::get("position_dr");
				
				}
				
				$object->free_compitition						= Input::get("free_competition");
				$object->mawqif_employee						= Input::get("mawqif_employee");
				$object->first_date_appointment					= Input::get("first_appointment_date");
				$object->current_position_dr					= Input::get("current_position_dr");
				$object->name_dr 								= Input::get("name_dr");
				$object->father_name_dr 						= Input::get("father_name_dr");
				$object->nationality 							= Input::get("nationality");
				$object->gender 								= Input::get("gender");
				$object->blood_group							= Input::get("blood_group");
				$object->birth_year								= Input::get("birth_year");
				$object->original_province 						= Input::get("original_province");
				$object->district 								= Input::get("district");
				//$object->education_degree						= Input::get("education_degree");
				//$object->education_field						= Input::get("education_field");
				$object->phone									= Input::get("phone");
				$object->email									= Input::get("email");
				$object->last_name 								= Input::get("last_name");
				$object->grand_father_name 						= Input::get("grand_father_name");
				$object->id_no 									= Input::get("id_no");
				$object->id_jild								= Input::get("id_jild");
				$object->id_page								= Input::get("id_page");
				$object->id_sabt								= Input::get("id_sabt");
				$object->village 								= Input::get("village");
				$object->current_province 						= Input::get("current_province");
				$object->current_district 						= Input::get("current_district");
				$object->current_village 						= Input::get("current_village");
				//$object->emp_bast 								= Input::get("tashkil_bast");
				//$object->education_place						= Input::get("education_place");
				//$object->graduation_year						= Input::get("graduation_year");
				$object->created_by 							= Auth::user()->id;
				
			
				if($object->save())
				{
					$inserted_id = $object->id;
					//add current position to employee_experience
					if(Input::get('employee_type') == 1 || Input::get('employee_type') == 2 || Input::get('employee_type') == 3)
					{//not contracts
						$emp_det = getEmployeeDetails($inserted_id);
						if(Input::get("first_appointment_date") != '')
						{
							$sdate = explode("-", Input::get("first_appointment_date"));
							$sy = $sdate[2];
							$sm = $sdate[1];
							$sd = $sdate[0];
							$sdate = dateToMiladi($sy,$sm,$sd);		
						}
						else
						{
							$sdate = null;
						}
						$experience = array(
								'employee_id'	=> $inserted_id,
								'organization'	=> $emp_det->dep_name,
								'position'	=> Input::get("current_position_dr"),
								'bast'	=> Input::get("tashkil_bast"),
								//'rank'	=> Input::get("rank"),
								'date_from'	=> $sdate,
								'type'	=> 1,
								'internal'	=> 1,
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
						hrOperation::insertRecord('employee_experiences',$experience);
					}
					/*
					for($i=1;$i<=3;$i++)
					{
						if(Input::get('experience_position_'.$i)!='')
						{
							$experience = array(
								"employee_id" 	=> $inserted_id,
								"organization" 	=> Input::get('experience_company_'.$i),
								"position" 		=> Input::get('experience_position_'.$i),
								"bast"			=> Input::get('experience_bast_'.$i),
								"leave_reason" 	=> Input::get('experience_leave_'.$i),
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
							hrOperation::insertRecord('employee_experiences',$experience);
						}
					}
					for($i=1;$i<=4;$i++)
					{
						$langs = array(
								"employee_id" 	=> $inserted_id,
								"language" 		=> Input::get('lang_'.$i),
								"writing" 		=> Input::get('writing_'.$i),
								"speaking"		=> Input::get('speaking_'.$i),
								"reading" 		=> Input::get('reading_'.$i),
								"typing"		=> Input::get('typing_'.$i),
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
						hrOperation::insertRecord('employee_languages',$langs);
					}
					for($i=1;$i<=2;$i++)
					{
						if(Input::get('garantee_name_'.$i)!='')
						{
							$garantee = array(
								"employee_id" 	=> $inserted_id,
								"name" 			=> Input::get('garantee_name_'.$i),
								"job" 			=> Input::get('garantee_job_'.$i),
								"organization"	=> Input::get('garantee_company_'.$i),
								"phone" 		=> Input::get('garantee_phone_'.$i),
								"email"			=> Input::get('garantee_email_'.$i),
								"created_at" 	=> date('Y-m-d H:i:s'),
								"created_by" 	=> Auth::user()->id
							);
							hrOperation::insertRecord('employee_garantee',$garantee);
						}
					}
					*/
					$this->uploadDocs($inserted_id);
					//$this->uploadPhoto($inserted_id);
					if(Input::get('tashkil'))
					{
						hrOperation::update_record('tashkilat',array('status'=>1),array('id'=>Input::get('tashkil')));
					}
					
					return Redirect::route('getRecruitment')->with("success","Record saved successfully");
				}
				else
				{
					return Redirect::route("getRecruitment")->with("fail","An error occured plase try again.");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	//change task start/end date
	public function fireDate()
	{	
		if(canAdd('hr_recruitment'))
		{
			$employee_id 	= Input::get('employee_id');
			$number	 		= Input::get('number');
			$sdate 			= Input::get("date");
			$update_report 	= hrOperation::find($employee_id);
			
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			if(Input::hasFile('fire'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'fire_'.$employee_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($employee_id,'fire');
			}
			$update_report->fire_date 		= $sdate;
			$update_report->fire_no 		= $number;
			
		    if(Input::get("date_hire") != '')
			{//if the hire date is set, it means the emplyee is not fire any more
				$edate = explode("-", Input::get("date_hire"));
				$ey = $edate[2];
				$em = $edate[1];
				$ed = $edate[0];
				$edate = dateToMiladi($ey,$em,$ed);		
				$update_report->fire_date_hire 		= $edate;
				$update_report->fired 			= 0;//not fired
			}
			else
			{
				$rfid = hrOperation::getEmployeeDetails($employee_id)->RFID;
				$update_report->fired 			= 1;//fired
				$update_report->att_out_date 	= $sdate;
				$update_report->in_attendance 	= $rfid;
				$update_report->RFID 			= null;
			}
		    if($update_report->save())
		    {
		    	$emp_det =  DB::connection('hr')
								->table('employees')
								->where('id',$employee_id)
								->pluck('tashkil_id');
				if($emp_det!=0)
				{
					hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$emp_det));
				}
				hrOperation::insertLog('employees',1,'fire of given employee',$employee_id);
		    	return \Redirect::route("getRecruitment")->with("success","Record Saved Successfully.");
		    }
		}
		else
		{
			return showWarning();
		}
	}
	public function retireDate()
	{	
		if(canAdd('hr_recruitment'))
		{
			$employee_id 	= Input::get('employee_id');
			$number	 		= Input::get('number');
			$sdate 			= Input::get("date");
			$update_report 	= hrOperation::find($employee_id);
			
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			if(Input::hasFile('retire'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'retire_'.$employee_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($employee_id,'retire');
			}
			$update_report->retire_date 		= $sdate;
			$update_report->retire_no 		= $number;
			$update_report->retired 			= 1;//retired
	    	$rfid = hrOperation::getEmployeeDetails($employee_id)->RFID;
			$update_report->att_out_date 	= $sdate;
			$update_report->in_attendance 	= $rfid;
			$update_report->RFID 			= null;
			
		    if($update_report->save())
		    {
		    	$emp_det =  DB::connection('hr')
								->table('employees')
								->where('id',$employee_id)
								->pluck('tashkil_id');
				if($emp_det!=0)
				{
					hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$emp_det));
				}
				hrOperation::insertLog('employees',1,'retire of given employee',$employee_id);
		    	return \Redirect::route("getRecruitment")->with("success","Record Saved Successfully.");
		    }
		}
		else
		{
			return showWarning();
		}
	}
	public function employeeResign()
	{	
		if(canAdd('hr_recruitment'))
		{
			$employee_id 	= Input::get('employee_id');
			$number	 		= Input::get('number');
			$sdate 			= Input::get("date");
			$update_report 	= hrOperation::find($employee_id);
			
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			if(Input::hasFile('resign'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'resign_'.$employee_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($employee_id,'resign');
			}
			$update_report->resign_date 		= $sdate;
			$update_report->resign_no 		= $number;
			$update_report->resigned 			= 1;//resigned
		    $rfid = hrOperation::getEmployeeDet($employee_id)->RFID;
			$update_report->att_out_date 	= $sdate;
			$update_report->in_attendance 	= $rfid;
			$update_report->RFID 			= null;
		
		    if($update_report->save())
		    {
		    	$emp_det =  DB::connection('hr')
								->table('employees')
								->where('id',$employee_id)
								->pluck('tashkil_id');
				if($emp_det!=0)
				{
					hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$emp_det));
				}
				hrOperation::insertLog('employees',1,'resign of given employee',$employee_id);
		    	return \Redirect::route("getRecruitment")->with("success","Record Saved Successfully.");
		    }
		}
		else
		{
			return showWarning();
		}
	}
	public function loadEmployees()
	{
		$type = Input::get('type');
		if($type == 'changed')
		{
			return View::make('hr.recruitment.changed_list');
		}
		elseif($type == 'khedmati')
		{
			return View::make('hr.recruitment.khedmati_list');
		}
		elseif($type == 'fired')
		{
			return View::make('hr.recruitment.fired_list');
		}
		elseif($type == 'resign')
		{
			return View::make('hr.recruitment.resign_list');
		}
		elseif($type == 'retire')
		{
			return View::make('hr.recruitment.retire_list');
		}
		elseif($type == 'extra_bast')
		{//salary wait
			return View::make('hr.recruitment.extra_bast_list');
		}
		elseif($type == 'tanqis')
		{//ezafa bast
			return View::make('hr.recruitment.tanqis_list');
		}
		elseif($type == 'contracts')
		{
			return View::make('hr.recruitment.contracts_list');
		}
		//communication tabs
		elseif($type == 'maktob')
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			$users = Report::getDepUsers();
			$data['users'] = $users;
			return View::make('hr.communication.maktob_list',$data);
		}
		elseif($type == 'estelam')
		{
			return View::make('hr.communication.estelam_list');
		}
		elseif($type == 'security')
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			$users = Report::getDepUsers();
			$data['users'] = $users;
			return View::make('hr.communication.security_list',$data);
		}
		elseif($type == 'protection')
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			$users = Report::getDepUsers();
			$data['users'] = $users;
			return View::make('hr.communication.protection_list',$data);
		}
		elseif($type == 'complain')
		{
			return View::make('hr.communication.complains_list');
		}
		elseif($type == 'comitte')
		{
			return View::make('hr.communication.comitte_list');
		}
		elseif($type == 'cards')
		{
			return View::make('hr.communication.cards');
		}
	}
	public function getFiredEmployeeData()
	{		
		if(canView('hr_documents'))
		{
			//get all data 
			$object = hrOperation::getFiredData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name',
								'number_tayenat',
								'bast',
								'department',
								'fire_no'
								)
					->addColumn('fire_date', function($option){
						$date = $option->emp_date;
						if($date!='')
						{
							$date = explode('-',$option->emp_date);
							$date = dateToShamsi($date[0],$date[1],$date[2]);
						}
						
						return $date;
						
					})
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getResignEmployeeData()
	{		
		if(canView('hr_documents'))
		{
			//get all data 
			$object = hrOperation::getResignData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name',
								'number_tayenat',
								'bast',
								'department',
								'fire_no'
								)
					->addColumn('resign_date', function($option){
						$date = $option->emp_date;
						if($date!='')
						{
							$date = explode('-',$option->emp_date);
							$date = dateToShamsi($date[0],$date[1],$date[2]);
						}
						
						return $date;
						
					})
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getRetireEmployeeData()
	{		
		if(canView('hr_documents'))
		{
			//get all data 
			$object = hrOperation::getRetireData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name',
								'number_tayenat',
								'bast',
								'department',
								'retire_no'
								)
					->addColumn('retire_date', function($option){
						$date = $option->emp_date;
						if($date!='')
						{
							$date = explode('-',$option->emp_date);
							$date = dateToShamsi($date[0],$date[1],$date[2]);
						}
						
						return $date;
						
					})
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getExtraBastEmployeeData()
	{		
		if(canView('hr_documents'))
		{
			//get all data 
			$object = hrOperation::getExtraBastData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name',
								'number_tayenat',
								'bast',
								'department'
								)
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getTanqisEmployeeData()
	{	
		if(canView('hr_documents'))
		{
			$object = hrOperation::getTanqistData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name',
								'number_tayenat',
								'bast',
								'department'
								)
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getContractsEmployeeData()
	{	
		if(canView('hr_documents'))
		{
			$object = hrOperation::getContractsData();
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name',
								'department'
								)
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getContractEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getChangedEmployeeData()
	{		
		if(canView('hr_documents'))
		{
			//get all data 
			$object = hrOperation::getChangedData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'father_name_dr',
								'number_tayenat',
								'original_province',
								'bast',
								'rank',
								'emp_date'
								)
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getServiceEmployeeData()
	{		
		if(canView('hr_documents'))
		{
			//get all data 
			$object = hrOperation::getServiceData();//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
							->showColumns(
								'id',
								'name',
								'number_tayenat',
								'gender',
								'original_province',
								'bast',
								'rank',
								'emp_date'
								)
					->addColumn('operations', function($option){
						$options = '';
						if(canEdit('hr_recruitment') || canEdit('hr_documents'))
						{
							$options = '<a href="'.route('getDetailsEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
						}
						return $options;
					})
					->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function load_change_employee()
	{
		//get employee id
		$employee_id = Input::get('id');
		$data['details'] = hrOperation::find($employee_id);
		$data['parentDeps'] = getDepartmentWhereIn();
		
		return View('hr.recruitment.employee_change_detail',$data);
	}
	public function load_tashkil_det()
	{
		//get employee id
		$id = Input::get('id');
		$data['id'] = $id;
		//get employee details
		$data['details'] = hrOperation::getTashkilDetail($id);
		$data['sud_dep']=getRelatedSubDepartment($data['details']->dep_id);
		$data['parentDeps'] = getDepartmentWhereIn();
		
		return View('hr.tashkil.update_tashkil',$data);
	}
	public function load_promotion_employee()
	{
		//get employee id
		$employee_id = Input::get('id');
		$lastPromotion = hrOperation::getLastPromotion($employee_id);
		$data['last_promotion'] = $lastPromotion;
		$data['promotions'] = hrOperation::getEmployeePromotions($employee_id);

		return View('hr.documents.employee_promotion_details',$data);
	}
	public function load_promotion_update()
	{
		//get employee id
		$id = Input::get('id');
		$data['id'] = $id;
		//get employee details
		$data['details'] = hrOperation::getEmployeePromotionDetail($id);
		
		return View('hr.documents.update_promotion',$data);
	}
	public function load_makafat_update()
	{
		//get employee id
		$id = Input::get('id');
		$data['id'] = $id;
		//get employee details
		$data['details'] = hrOperation::getEmployeeMakafatDetail($id);
		
		return View('hr.documents.update_makafat',$data);
	}
	public function load_punish_update()
	{
		//get employee id
		$id = Input::get('id');
		$data['id'] = $id;
		//get employee details
		$data['details'] = hrOperation::getEmployeePunishDetail($id);
		
		return View('hr.documents.update_punish',$data);
	}
	public function load_service_employee()
	{
		//get employee id
		$employee_id = Input::get('id');
		
		//get employee details
		$data['details'] = hrOperation::find($employee_id);
		$data['parentDeps'] = getDepartmentWhereIn();
		
		return View('hr.recruitment.employee_service_detail',$data);
	}
	public function load_change_detail()
	{
		//get employee id
		$employee_id = Input::get('id');
		
		//get employee details
		$data['details'] = hrOperation::find($employee_id);
		$data['changes'] = hrOperation::getEmployeeChanges($employee_id,'dr');
		
		return View('hr.recruitment.employee_change_list',$data);
	}
	public function changeEmployeeViaAjax()
	{
		if(canAdd('hr_recruitment'))
		{
			$employee_id = \Crypt::decrypt(Input::get('employee_id'));
			//validate fields
			if(Input::get('type')==1)
			{//external
				$validates = Validator::make(Input::all(), array(
					"employee_type"		=> "required",
					"position_title"	=> "required",
					"type"				=> "required",
					"change_date"		=> "required",
					"ministry"			=> "required"
				));
			}
			else
			{
				$validates = Validator::make(Input::all(), array(
					"employee_type"		=> "required",
					"position_title"	=> "required",
					"type"				=> "required",
					"change_date"		=> "required",
					"general_department"=> "required"
				));
			}
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route("getDetailsEmployee",$employee_id)->with("fail","Missing required fields.");
			}
			else
			{
				$sdate = Input::get("change_date");
				$hdate = Input::get("hokm_date");
				if($sdate != '')
				{
					$sdate = explode("-", $sdate);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				else
				{
					$sdate = null;
				}
				if($hdate != '')
				{
					$hdate = explode("-", $hdate);
					$hy = $hdate[2];
					$hm = $hdate[1];
					$hd = $hdate[0];
					$hdate = dateToMiladi($hy,$hm,$hd);		
				}
				else
				{
					$hdate = null;
				}
				if(Input::get('type') == 0)
				{//internal
					$dep = Input::get('general_department');
					$sub_dep = Input::get('sub_dep');
					$ministry = 0;
				}
				else
				{
					$dep = null;
					$sub_dep = null;
					$ministry = Input::get('ministry');
				}
				$items = array(
						"employee_id" 			=> $employee_id,
						"position_title" 		=> Input::get('position_title'),
						"employee_type" 		=> Input::get('employee_type'),
						"general_department"	=> $dep,
						"department"			=> $sub_dep,
						"ministry_id"			=> $ministry,
						'change_type'			=> Input::get('type'),
						"position_dr"			=> Input::get('position_dr'),
						"changed_date"			=> $sdate,
						"hokm_date"				=> $hdate,
						"hokm_no"				=> Input::get('hokm_no'),
						"created_at" 			=> date('Y-m-d H:i:s'),
						"created_by" 			=> Auth::user()->id
					);
				if(Input::get('employee_type') == 3)
				{
					$items['military_rank']		= Input::get("military_rank");
					$items['military_bast']		= Input::get("military_bast");
					
				}
				else
				{
					$items['emp_rank']			= Input::get("emp_rank");
					$items['emp_bast']			= Input::get("emp_bast");
				}
				$inserted_id = hrOperation::insertRecord_id('employee_changes',$items);
				$type = 1;//for experience record
				if(Input::get('type') == 1)
				{//if its external update the status to changed to out
					$type = 0;//external experience
					$tashkil_id = hrOperation::getEmployeeDetails($employee_id)->tashkil_id;
					$rfid = hrOperation::getEmployeeDetails($employee_id)->RFID;
					hrOperation::update_record('employees',array('changed'=>1,'att_out_date'=>$sdate,'in_attendance'=>$rfid,'RFID'=>null),array('id'=>$employee_id));
					hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$tashkil_id));
				}
				//add record to experience
				
				$emp_det = getEmployeeChangesDetails($inserted_id,Input::get('type'));
				//dd($emp_det);
				$experience = array(
						'employee_id'	=> $employee_id,
						'organization'	=> $emp_det->dep_name,
						'position'	=> Input::get("position_title"),
						'bast'	=> $emp_det->bast_name,
						//'rank'	=> Input::get("rank"),
						'date_from'	=> $sdate,
						'type'	=> 1,
						'internal'	=> $type,
						"created_at" 	=> date('Y-m-d H:i:s'),
						"created_by" 	=> Auth::user()->id
					);
				hrOperation::insertRecord('employee_experiences',$experience);

				return \Redirect::route("getRecruitment")->with("success","Record Saved Successfully.");
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function promoteEmployeeViaAjax()
	{
		if(canEdit('hr_documents'))
		{
			$employee_id = \Crypt::decrypt(Input::get('employee_id'));
	
			$sdate = Input::get("change_date");
				
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"employee_id" 			=> $employee_id,
					"promotion_date"		=> $sdate,
					"qadam"					=> Input::get('qadam'),
					"qadam_year"			=> Input::get('qadam_year'),
					"qadam_month"			=> Input::get('qadam_month'),
					"qadam_day"				=> Input::get('qadam_day'),
					"employee_type"			=> Input::get('employee_type'),
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);
			if(Input::get('employee_type') == 3)
			{
				$items['military_rank']		= Input::get("military_rank");
				$items['military_bast']		= Input::get("military_bast");
	
			}
			elseif(Input::get('employee_type') == 2)
			{//ajir
				$items['ajir_rank']		= Input::get("ajir_rank");
				$items['ajir_bast']		= Input::get("ajir_bast");
	
			}
			else
			{
				$items['emp_rank']			= Input::get("emp_rank");
				$items['emp_bast']			= Input::get("emp_bast");
			}
			hrOperation::insertRecord('employee_promotions',$items);
			return \Redirect::route("getDetailsEmployee",$employee_id)->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function promoteEmployeeUpdate()
	{
		if(canEdit('hr_documents'))
		{
			$id = \Crypt::decrypt(Input::get('id'));
	
			$sdate = Input::get("change_date");
				
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"promotion_date"		=> $sdate,
					"employee_type"			=> Input::get('employee_type'),
					"qadam"					=> Input::get('qadam'),
					"qadam_year"			=> Input::get('qadam_year'),
					"qadam_month"			=> Input::get('qadam_month'),
					"qadam_day"				=> Input::get('qadam_day')
				);
			if(Input::get('employee_type') == 3)
			{
				$items['military_rank']		= Input::get("military_rank");
				$items['military_bast']		= Input::get("military_bast");
	
			}
			elseif(Input::get('employee_type') == 2)
			{//ajir
				$items['ajir_rank']		= Input::get("ajir_rank");
				$items['ajir_bast']		= Input::get("ajir_bast");
	
			}
			else
			{
				$items['emp_rank']			= Input::get("emp_rank");
				$items['emp_bast']			= Input::get("emp_bast");
			}
			hrOperation::update_record('employee_promotions',$items,array('id'=>$id));
			hrOperation::insertLog('employee_promotions',1,'promotion of given employee',$id);
			return \Redirect::route("getDetailsEmployee",Input::get('employee_id'))->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function serviceEmployeeViaAjax()
	{
		if(canEdit('hr_documents'))
		{
			$employee_id = \Crypt::decrypt(Input::get('employee_id'));
	
			$sdate = Input::get("change_date");
			$hdate = Input::get("hokm_date");
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			if($hdate != '')
			{
				$hdate = explode("-", $hdate);
				$hy = $hdate[2];
				$hm = $hdate[1];
				$hd = $hdate[0];
				$hdate = dateToMiladi($hy,$hm,$hd);		
			}
			else
			{
				$hdate = null;
			}
			if(Input::get('type') == 0)
			{
				$dep = Input::get('general_department');
				$sub_dep = Input::get('sub_dep');
				$ministry = 0;
			}
			else
			{
				$dep = null;
				$sub_dep = null;
				$ministry = Input::get('ministry');
			}
			$items = array(
					"employee_id" 			=> $employee_id,
					"position_title" 		=> Input::get('position_title'),
					"general_department"	=> $dep,
					"department"			=> $sub_dep,
					"ministry_id"			=> $ministry,
					"service_date"			=> $sdate,
					"hokm_date"				=> $hdate,
					"hokm_no"				=> Input::get('hokm_no'),
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);
			
			hrOperation::insertRecord('employee_services',$items);
			return \Redirect::route("getDetailsEmployee",$employee_id)->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	//get edit form for employee
	public function getEmployeeUpdate($id=0)
	{
		//check roles
		if(canEdit('hr_recruitment'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachments'] = hrOperation::getDocuments($id);
			$data['parentDeps'] = getDepartmentWhereIn();
			//get province
			$data['provinces'] = getAllProvinces('dr');
			//load view for registring
			return View::make('hr.recruitment.update',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getDocumentsUpdate($id=0)
	{
		//check roles
		if(canEdit('hr_documents'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['experiences'] = hrOperation::getEmployeeExperiences($id);
			$data['trainings'] = hrOperation::getEmployeeTrainings($id);
			
			//load view for registring
			return View::make('hr.documents.update',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//get edit form for docs
	public function getEmployeeDocs($id=0)
	{
		//check roles
		if(canView('hr_recruitment'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachments'] = hrOperation::getDocuments($id);
			
			return View::make('hr.recruitment.documents',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeSuggestion($id=0)
	{
		//check roles
		if(canView('hr_recruitment'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachment'] = hrOperation::getDocumentsByType($id,'suggestion');
			
			return View::make('hr.recruitment.suggestion',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeHire($id=0)
	{
		//check roles
		if(canView('hr_recruitment'))
		{
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachment'] = hrOperation::getDocumentsByType($id,'hire');
			
			return View::make('hr.recruitment.hire',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeDocs($emp_id=0)
	{
		//check roles
		if(canAdd('hr_recruitment'))
		{
			if(Input::hasFile('education'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'education_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'education');
			}
			if(Input::hasFile('crime'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'crime_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'crime');
			}
			if(Input::hasFile('health'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'health_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'health');
			}
			if(Input::hasFile('job'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'job_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'job');
			}
			if(Input::hasFile('tazkira'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'tazkira_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'tazkira');
			}
			if(Input::hasFile('other'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'other_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'other');
			}
			hrOperation::delete_record('employee_filing',array('employee_id'=>$emp_id));
			$filing_data=array('rack_no'=>Input::get('rack'),
							   'floor_no'=>Input::get('floor'),
							   'file_no'=>Input::get('file_no'),
							   'employee_id'=>$emp_id
								);
			HrOperation::insertRecord('employee_filing',$filing_data);
			hrOperation::insertLog('attachments',1,'attachment of given employee',$emp_id);
			//$this->uploadPhoto($inserted_id);
			return Redirect::route('getDetailsEmployee',$emp_id)->with("success","Record saved successfully");
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeSuggestion($emp_id=0)
	{
		//check roles
		if(canAdd('hr_recruitment'))
		{
			$object = hrOperation::find($emp_id);
			$sdate = Input::get("suggestion_date");
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			$object->suggestion_no 						= Input::get("suggestion_no");
			$object->suggestion_date 					= $sdate;
			$object->save();		
			if(Input::hasFile('suggestion'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'suggestion_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'suggestion');
			}
			//$this->uploadPhoto($inserted_id);
			return Redirect::route('getDetailsEmployee',$emp_id)->with("success","Record saved successfully");
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeHire($emp_id=0)
	{
		//check roles
		if(canAdd('hr_recruitment'))
		{
			$object = hrOperation::find($emp_id);
			$sdate = Input::get("hire_date");
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			$object->hire_no 						= Input::get("hire_no");
			$object->hire_date 					= $sdate;
			$object->save();		
			if(Input::hasFile('hire'))
			{
				$fileName =  DB::connection('hr')
								->table('attachments')
								->where('file_name', 'like', 'hire_'.$emp_id.'.%')
								->pluck('file_name');
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('attachments')
					->where('file_name',$fileName)
					->delete();
				}
				$this->uploadEmployeeDocs($emp_id,'hire');
			}
			hrOperation::insertLog('employees',1,'Hire of given employee',$emp_id);
			//$this->uploadPhoto($inserted_id);
			return Redirect::route('getDetailsEmployee',$emp_id)->with("success","Record saved successfully");
		}
		else
		{
			return showWarning();
		}
	}
	public function uploadEmployeeDocs($doc_id=0,$type='')
	{
		// getting all of the post data
		$file = Input::file($type);
		$errors = "";
		$file_data = array();
		// validating each file.
		$rules = array($type => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
		$validator = Validator::make(

		  		[
		            $type => $file,
		            'extension'  => Str::lower($file->getClientOriginalExtension()),
		        ],
		        [
		            $type => 'required|max:100000',
		            'extension'  => 'required|in:jpg,jpeg,bmp,png'
		        ]
		);

		if($validator->passes())
		{
		    // path is root/uploads
		    $destinationPath = 'documents/hr_attachments';
		    $filename = $file->getClientOriginalName();

		    $temp = explode(".", $filename);
		    $extension = end($temp);
		    $lastFileId = getLastDocId();
		    
		    $lastFileId++;
		    
		    $filename = $type.'_'.$doc_id.'.'.$extension;

		    $upload_success = $file->move($destinationPath, $filename);

		    if($upload_success) 
		    {
			   	$data = array(
			   					'doc_id'=>$doc_id,
			   					'file_name'=>$filename,
			   					'created_at'=>date('Y-m-d H:i:s'),
			   					'user_id'	=> Auth::user()->id
			   				);
			   if(count($data)>0)
				{						
					DB::connection('hr')->table('attachments')->insert($data);
				}
			} 
			else 
			{
			   $errors .= json('error', 400);
			}
		} 
		else 
		{
		    // redirect back with errors.
		    return Redirect::back()->withErrors($validator);
		}
	}
	public function getMoreExperience()
	{
		$data['total'] = Input::get('total');
		$data['type'] = Input::get('type');
	
		return View::make('hr.documents.moreExperiences',$data);
	}
	public function postEmployeeExperiences($employee_id=0,Request $request)
	{
		if(canEdit('hr_documents'))
		{
			$experienceNo = $request['experiences_number'];
			
			hrOperation::delete_record('employee_experiences',array('employee_id'=>$employee_id,'type'=>1));
			
			for($i=1;$i<=$experienceNo;$i++)
			{
				$validates = Validator::make(Input::all(), array(
					"experience_company_".$i		=> "required"
				));
				if($validates->fails())
				{	
					continue;
				}
				else
				{
					$sdate = Input::get('date_from_'.$i);
					$edate = Input::get('date_to_'.$i);
					if($sdate != '')
					{
						$sdate = explode("-", $sdate);
						$sy = $sdate[2];
						$sm = $sdate[1];
						$sd = $sdate[0];
						$sdate = dateToMiladi($sy,$sm,$sd);		
					}
					else
					{
						$sdate = null;
					}
					if($edate != '')
					{
						$edate = explode("-", $edate);
						$sy = $edate[2];
						$sm = $edate[1];
						$sd = $edate[0];
						$edate = dateToMiladi($sy,$sm,$sd);		
					}
					else
					{
						$edate = null;
					}
					$data = array(
						'employee_id' 	=> $employee_id,
						'organization'	=> $request['experience_company_'.$i],
						'position'		=> $request['experience_position_'.$i],
						'bast'			=> $request['experience_bast_'.$i],
						'leave_reason'	=> $request['experience_leave_'.$i],
						'rank'			=> $request['experience_rank_'.$i],
						'date_from'		=> $sdate,
						'date_to'		=> $edate,
						'type'		    => 1,
						'created_at' 	=> date('Y-m-d H:i:s'),
						'created_by' 	=> Auth::user()->id
					);
					hrOperation::insertRecord('employee_experiences',$data);
				}
			}
			hrOperation::insertLog('employee_experiences',1,'experiences of given employee',$employee_id);
			return Redirect::route('getDetailsEmployee',$employee_id)->with("success","Record saved successfully");
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeTrainings($employee_id=0,Request $request)
	{
		if(canEdit('hr_documents'))
		{
			$trainingNo = $request['trainings_number'];
			
			hrOperation::delete_record('employee_trainings',array('employee_id'=>$employee_id));
			
			for($j=1;$j<=$trainingNo;$j++)
			{
				$trainin_validates = Validator::make(Input::all(), array(
					"training_title_".$j		=> "required"
				));
				if($trainin_validates->fails())
				{	
					continue;
				}
				else
				{
					$sdate = Input::get('training_date_from_'.$j);
					$edate = Input::get('training_date_to_'.$j);
					if($sdate != '')
					{
						$sdate = explode("-", $sdate);
						$sy = $sdate[2];
						$sm = $sdate[1];
						$sd = $sdate[0];
						$sdate = dateToMiladi($sy,$sm,$sd);		
					}
					else
					{
						$sdate = null;
					}
					if($edate != '')
					{
						$edate = explode("-", $edate);
						$sy = $edate[2];
						$sm = $edate[1];
						$sd = $edate[0];
						$edate = dateToMiladi($sy,$sm,$sd);		
					}
					else
					{
						$edate = null;
					}
					$training_data = array(
						'employee_id' 	=> $employee_id,
						'title'			=> $request['training_title_'.$j],
						'organization'	=> $request['training_org_'.$j],
						'date_from'		=> $sdate,
						'date_to'		=> $edate,
						'created_at' 	=> date('Y-m-d H:i:s'),
						'created_by' 	=> Auth::user()->id
					);
					
					hrOperation::insertRecord('employee_trainings',$training_data);
				}
			}
			hrOperation::insertLog('employee_trainings',1,'trainings of given employee',$employee_id);
			return Redirect::route('getDetailsEmployee',$employee_id)->with("success","Record saved successfully");
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeSoldier($employee_id=0,Request $request)
	{
		if(canEdit('hr_documents'))
		{
			if($request['soldier'])
			{
				$mokalafiat= 0;
				$ehtiat= 0;
				$sdate = null;
				$edate = null;
				$sdate_1 = null;
				$edate_1 = null;
				if($request['mokalafiat'])
				{
					$mokalafiat= 1;
					$sdate = Input::get('soldier_date_from');
					$edate = Input::get('soldier_date_to');
					if($sdate != '')
					{
						$sdate = explode("-", $sdate);
						$sy = $sdate[2];
						$sm = $sdate[1];
						$sd = $sdate[0];
						$sdate = dateToMiladi($sy,$sm,$sd);		
					}
					
					if($edate != '')
					{
						$edate = explode("-", $edate);
						$sy = $edate[2];
						$sm = $edate[1];
						$sd = $edate[0];
						$edate = dateToMiladi($sy,$sm,$sd);		
					}
					
				}
				if($request['ehtiat'])
				{
					$ehtiat= 1;
					$sdate_1 = Input::get('soldier_date_from_1');
					$edate_1 = Input::get('soldier_date_to_1');
					if($sdate_1 != '')
					{
						$sdate_1 = explode("-", $sdate_1);
						$sy_1 = $sdate_1[2];
						$sm_1 = $sdate_1[1];
						$sd_1 = $sdate_1[0];
						$sdate_1 = dateToMiladi($sy_1,$sm_1,$sd_1);		
					}
					
					if($edate_1 != '')
					{
						$edate_1 = explode("-", $edate_1);
						$sy_1 = $edate_1[2];
						$sm_1 = $edate_1[1];
						$sd_ = $edate_1[0];
						$edate_1 = dateToMiladi($sy_1,$sm_1,$sd_1);		
					}
					
				}
				$object = hrOperation::find($employee_id);
				
				$object->soldier = 1;
				$object->soldier_date_from = $sdate;
				$object->soldier_date_to = $edate;
				$object->soldier_date_from_1 = $sdate_1;
				$object->soldier_date_to_1 = $edate_1;
				$object->soldier_type_1 = $mokalafiat;
				$object->soldier_type_2 = $ehtiat;
				
				$object->save();
			}
			else
			{
				$object = hrOperation::find($employee_id);
				
				$object->soldier = 0;
				$object->soldier_date_from = null;
				$object->soldier_date_to = null;
				$object->soldier_date_from_1 = null;
				$object->soldier_date_to_1 = null;
				$object->soldier_type_1 = 0;
				$object->soldier_type_2 = 0;
				
				$object->save();
			}
			hrOperation::insertLog('employees',1,'soldier of given employee',$employee_id);
			return Redirect::route('getDetailsEmployee',$employee_id)->with("success","Record saved successfully");
		}
		else
		{
			return showWarning();
		}
	}
	public function copyToAttendance()
	{
		//dd(attendanceServer::all());
		attendanceServer::delete_record();
		$data_array= array();
		$allEmployees = hrOperation::getAllEmployeesForAttendanceServer();
		$data_array[]= array('badgenumber'	=> 7893646,
						 'defaultdeptid'	=> 1,
						 'name'			=> 'Dr.Abdul Rahim',
						 'card'			=> 7893646);
		$data_array[]= array('badgenumber'	=> 7867436,
						 'defaultdeptid'	=> 1,
						 'name'			=> 'Rafiullah',
						 'card'			=> 7867436);
		$data_array[]= array('badgenumber'	=> 7869024,
						 'defaultdeptid'	=> 1,
						 'name'			=> 'Marzia Raof',
						 'card'			=> 7869024);
		/* 
		foreach($allEmployees AS $item)
		{
			$rfid = str_replace(' ','',$item->RFID);
			$rfid = preg_replace('/\s+/', '', $rfid);
			$rfid = (int)$rfid;
			//$new_rfid = str_pad($item->RFID, 9, '0', STR_PAD_LEFT);
			$data= array('badgenumber'	=> $rfid,
						 'defaultdeptid'	=> 1,
						 'name'			=> $item->name_en,
						 'card'			=> $rfid);
						 
			array_push($data_array,$data);
			$last_id = $item->id;
		}
		**/
		attendanceServer::insert($data_array);
		$last_id_in_db = getLastAttendanceId();
		if($last_id_in_db)
		{
			$to = $last_id_in_db->to_id;
		}
		else
		{
			$to = 0;
		}
		$last_array=array('from_id' => $to,'to_id' => $last_id);
		hrOperation::insertRecord('attendance_temp',$last_array);
		
		return Redirect::route('getRecruitment')->with("success","Records saved to attendance server");
	}
	public function exportToCsv()
	{
		//dd(attendanceServer::all());
		//attendanceServer::delete_record();
		
		$allEmployees = hrOperation::getAllEmployeesForAttendance();
		$last_id = hrOperation::getLastEmployeeId()->id;
		
		Excel::create('Att-'.$last_id, function($file) use($allEmployees){
			
			$file->setTitle('Attendance');
		
			$file->sheet('Attendance', function($sheet) use($allEmployees){
				$row = 1;
				foreach($allEmployees AS $item)
				{
					$rfid = str_replace(' ','',$item->RFID);
					$rfid = preg_replace('/\s+/', '', $rfid);
					$rfid = (int)$rfid;
					//$new_rfid = str_pad($rfid, 9, '0', STR_PAD_LEFT);
					
					$sheet->setCellValue('A'.$row.'',$rfid);
					
					$sheet->setCellValue('B'.$row.'',$rfid);
					
					$sheet->setCellValue('C'.$row.'',$item->name_en);
					
					$sheet->setCellValue('D'.$row.'','');
					$sheet->setCellValue('E'.$row.'','');
					$sheet->setCellValue('F'.$row.'','');
					$sheet->setCellValue('G'.$row.'','');
					$sheet->setCellValue('H'.$row.'','');
					$sheet->setCellValue('I'.$row.'','');
					$sheet->setCellValue('J'.$row.'','');
					$sheet->setCellValue('K'.$row.'','');
					$sheet->setCellValue('L'.$row.'',$rfid);
					$row++;
				}
    		});
		})->download('csv');
	}
	public function getDetailsEmployee($id=0)
	{
		if(canView('hr_recruitment') || canView('hr_documents') || canView('hr_capacity'))
		{
			$data['id'] = $id;
			//getEmployeeUpdate
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachments'] = hrOperation::getDocuments($id);
			$data['parentDeps'] = getDepartmentWhereIn();
			
			//get province
			$data['provinces'] = getAllProvinces('dr');
			$data['org_districts'] = getProvinceDistrict($details->original_province);
			$data['cur_districts'] = getProvinceDistrict($details->current_province);
			$data['experiences'] = hrOperation::getEmployeeExperiences($id,0);
			$data['langs'] = hrOperation::getEmployeeLangs($id);
			$data['educations'] = hrOperation::getEmployeeEducations($id);
			//dd($data['educations']);
			if($details->tashkil_id==0)
			{
				$data['tashkils'] = hrOperation::getRelatedBasts($details->department,$details->tashkil_id);
				$data['deps'] = getRelatedSubDepartment($details->general_department);
				$data['dep_id'] = $details->general_department;
				$data['sub_dep_id'] = $details->department;
			}
			else
			{
				$data['tashkils'] = hrOperation::getRelatedBasts($details->sub_dep_id,$details->tashkil_id);
				$data['deps'] = getRelatedSubDepartment($details->dep_id);
				$data['dep_id'] = $details->dep_id;
				$data['sub_dep_id'] = $details->sub_dep_id;
			}
			$data['garantee'] = hrOperation::getEmployeeGarantee($id);
			//load view for registring
			$data['employeeUpdate']=View::make('hr.recruitment.update',$data);
			//all tabs view
			return View::make('hr.recruitment.employee_details',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getContractEmployee($id=0)
	{
		if(canView('hr_recruitment') || canView('hr_documents') || canView('hr_capacity'))
		{
			$data['id'] = $id;
			//getEmployeeUpdate
			$details = hrOperation::getEmployeeDetails($id);
			$data['row'] = $details;
			$data['attachments'] = hrOperation::getDocuments($id);
			$data['parentDeps'] = getDepartmentWhereIn();
			
			//get province
			$data['provinces'] = getAllProvinces('dr');
			$data['org_districts'] = getProvinceDistrict($details->original_province);
			$data['cur_districts'] = getProvinceDistrict($details->current_province);
			$data['experiences'] = hrOperation::getEmployeeExperiences($id,0);
			$data['langs'] = hrOperation::getEmployeeLangs($id);
			$data['educations'] = hrOperation::getEmployeeEducations($id);
			if($details->tashkil_id==0)
			{
				$data['tashkils'] = hrOperation::getRelatedBasts($details->department,$details->tashkil_id);
				$data['deps'] = getRelatedSubDepartment($details->general_department);
				$data['dep_id'] = $details->general_department;
				$data['sub_dep_id'] = $details->department;
			}
			else
			{
				$data['tashkils'] = hrOperation::getRelatedBasts($details->sub_dep_id,$details->tashkil_id);
				$data['deps'] = getRelatedSubDepartment($details->dep_id);
				$data['dep_id'] = $details->dep_id;
				$data['sub_dep_id'] = $details->sub_dep_id;
			}
			$data['garantee'] = hrOperation::getEmployeeGarantee($id);
			//load view for registring
			$data['employeeUpdate']=View::make('hr.recruitment.update_contract',$data);
			//all tabs view
			return View::make('hr.recruitment.employee_details',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function loadEmployeeDetails()
	{
		if(canView('hr_recruitment') || canView('hr_documents') || canView('hr_capacity'))
		{
			$type = Input::get('type');
			$employee_id = Input::get('id');
			//get employee details
			$data['details'] = hrOperation::find($employee_id);
			$data['id'] = $employee_id;
			
			if($type == 'changed')
			{
				$data['changes'] = hrOperation::getEmployeeChanges($employee_id,'dr');
				
				return View('hr.recruitment.employee_change_list',$data);
			}
			elseif($type == 'edit')
			{
				$data['id'] = $employee_id;
				//getEmployeeUpdate
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['row'] = $details;
				$data['attachments'] = hrOperation::getDocuments($employee_id);
				$data['parentDeps'] = getDepartmentWhereIn();
				//$data['deps'] = getRelatedSubDepartment($details->general_department);
				//get province
				$data['provinces'] = getAllProvinces('dr');
				$data['org_districts'] = getProvinceDistrict($details->original_province);
				$data['cur_districts'] = getProvinceDistrict($details->current_province);
				$data['experiences'] = hrOperation::getEmployeeExperiences($employee_id,0);
				$data['educations'] = hrOperation::getEmployeeEducations($employee_id);
				$data['langs'] = hrOperation::getEmployeeLangs($employee_id);
				//$data['tashkils'] = hrOperation::getRelatedBasts($details->department,$details->tashkil_id);
				$data['garantee'] = hrOperation::getEmployeeGarantee($employee_id);
				if($details->tashkil_id==0)
				{
					$data['tashkils'] = hrOperation::getRelatedBasts($details->department,$details->tashkil_id);
					$data['deps'] = getRelatedSubDepartment($details->general_department);
					$data['dep_id'] = $details->general_department;
					$data['sub_dep_id'] = $details->department;
				}
				else
				{
					$data['tashkils'] = hrOperation::getRelatedBasts($details->sub_dep_id,$details->tashkil_id);
					$data['deps'] = getRelatedSubDepartment($details->dep_id);
					$data['dep_id'] = $details->dep_id;
					$data['sub_dep_id'] = $details->sub_dep_id;
				}
				return View::make('hr.recruitment.update',$data);
			}
			elseif($type == 'salary')
			{
				$data['row'] = hrOperation::getEmployeeSalary($employee_id);
				$data['employee_id'] = $employee_id;
				return View::make('hr.recruitment.employee_salary',$data);
			}
			elseif($type == 'khedmati')
			{
				$data['details'] = hrOperation::getEmployeeServices($employee_id);
				return View::make('hr.recruitment.employee_khedmati_list',$data);
			}
			elseif($type == 'docs')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['filing'] = hrOperation::getEmployeefiling($employee_id);
				$data['row'] = $details;
				$data['attachments'] = hrOperation::getDocuments($employee_id);
				
				return View::make('hr.recruitment.documents',$data);
			}
			elseif($type == 'suggestion')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['row'] = $details;
				$data['attachment'] = hrOperation::getDocumentsByType($employee_id,'suggestion');
				
				return View::make('hr.recruitment.suggestion',$data);
			}
			elseif($type == 'hire')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['row'] = $details;
				$data['attachment'] = hrOperation::getDocumentsByType($employee_id,'hire');
				
				return View::make('hr.recruitment.hire',$data);
			}
			elseif($type == 'fire')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['attachment'] = hrOperation::getDocumentsByType($employee_id,'fire');
				$data['row'] = $details;
				
				return View::make('hr.recruitment.fire',$data);
			}
			elseif($type == 'retire')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['attachment'] = hrOperation::getDocumentsByType($employee_id,'retire');
				$data['row'] = $details;
				
				return View::make('hr.recruitment.retire',$data);
			}
			elseif($type == 'resign')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['attachment'] = hrOperation::getDocumentsByType($employee_id,'resign');
				$data['row'] = $details;
				
				return View::make('hr.recruitment.resign',$data);
			}
			//documents sections
			elseif($type == 'experience')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['row'] = $details;
				$data['experiences'] = hrOperation::getEmployeeExperiences($employee_id);
			
				//load view for registring
				return View::make('hr.documents.experience',$data);
			}
			elseif($type == 'training')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['row'] = $details;
			
				$data['trainings'] = hrOperation::getEmployeeTrainings($employee_id);
				
				//load view for registring
				return View::make('hr.documents.trainings',$data);
			}
			elseif($type == 'soldier')
			{
				$details = hrOperation::getEmployeeDetails($employee_id);
				$data['row'] = $details;
			
				//load view for registring
				return View::make('hr.documents.soldier',$data);
			}
			elseif($type == 'promotion')
			{
				$data['last_promotion'] = hrOperation::getLastPromotion($employee_id);
				$data['promotions'] = hrOperation::getEmployeePromotions($employee_id);
				
				return View('hr.documents.employee_promotion_details',$data);
			}
			elseif($type == 'evaluation')
			{
				$data['evaluations'] = hrOperation::getEmployeeEvaluations($employee_id);
				
				return View('hr.documents.evaluations',$data);
			}
			elseif($type == 'punishment')
			{
				$data['punish'] = hrOperation::getEmployeePunishments($employee_id);
				
				return View('hr.documents.punishments',$data);
			}
			elseif($type == 'makafat')
			{
				$data['makafat'] = hrOperation::getEmployeeMakafat($employee_id);
				
				return View('hr.documents.makafat',$data);
			}
			elseif($type == 'cv')
			{
				$data['details'] = hrOperation::getEmployeeDetails($employee_id);
				$data['makafat'] = hrOperation::getEmployeeMakafat($employee_id);
				$data['punish'] = hrOperation::getEmployeePunishments($employee_id);
				$data['experiences'] = hrOperation::getEmployeeExperiences($employee_id);
				$data['promotions'] = hrOperation::getEmployeePromotions($employee_id);
				$data['trainings'] = hrOperation::getEmployeeTrainings($employee_id);
				$data['educations'] = hrOperation::getEmployeeEducations($employee_id);
				$data['changes'] = hrOperation::getEmployeeLastChange($employee_id);
				//dd($data['changes']);
				return View('hr.documents.cv',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function print_cv($employee_id=0)
	{
		if(canView('hr_documents'))
		{
			$data['id'] = $employee_id;
			$data['details'] = hrOperation::getEmployeeDetails($employee_id);//dd($data['details']);
			$data['makafat'] = hrOperation::getEmployeeMakafat($employee_id);
			$data['punish'] = hrOperation::getEmployeePunishments($employee_id);
			$data['experiences'] = hrOperation::getEmployeeExperiences($employee_id);
			$data['promotions'] = hrOperation::getEmployeePromotions($employee_id);
			$data['trainings'] = hrOperation::getEmployeeTrainings($employee_id);
			$data['educations'] = hrOperation::getEmployeeEducations($employee_id);
			$data['changes'] = hrOperation::getEmployeeLastChange($employee_id);
			return View('hr.documents.print_cv',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function evaluateEmployeeViaAjax()
	{
		if(canEdit('hr_documents'))
		{
			$employee_id = \Crypt::decrypt(Input::get('employee_id'));
	
			$sdate = Input::get("date");
				
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"employee_id" 			=> $employee_id,
					"bast"					=> Input::get('emp_bast'),
					"qadam"					=> Input::get('qadam'),
					"score"					=> Input::get('score'),
					"date"					=> $sdate,
					"result"				=> Input::get('result'),
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);
			
			hrOperation::insertRecord('employee_evaluations',$items);
			return \Redirect::route("getDetailsEmployee",$employee_id)->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function punishEmployeeViaAjax()
	{
		if(canEdit('hr_documents'))
		{
			$employee_id = \Crypt::decrypt(Input::get('employee_id'));
	
			$sdate = Input::get("date");
				
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"employee_id" 			=> $employee_id,
					"date"					=> $sdate,
					"reason"				=> Input::get('reason'),
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);
			
			hrOperation::insertRecord('employee_punishments',$items);
			return \Redirect::route("getDetailsEmployee",$employee_id)->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function punishEmployeeUpdate()
	{
		if(canEdit('hr_documents'))
		{
			$id = \Crypt::decrypt(Input::get('id'));
	
			$sdate = Input::get("date");
				
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"date"					=> $sdate,
					"reason"				=> Input::get('reason')
				);
			
			hrOperation::update_record('employee_punishments',$items,array('id'=>$id));
			hrOperation::insertLog('employee_punishments',1,'punishment of given employee',$id);
			return \Redirect::route("getDetailsEmployee",Input::get('employee_id'))->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}	
	}
	public function makafatEmployeeViaAjax()
	{
		if(canEdit('hr_documents'))
		{
			$employee_id = \Crypt::decrypt(Input::get('employee_id'));
	
			$sdate = Input::get("date");
				
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"employee_id" 			=> $employee_id,
					"date"					=> $sdate,
					"result"				=> Input::get('result'),
					"type"					=> Input::get('type'),
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);
			
			hrOperation::insertRecord('employee_makafats',$items);
			return \Redirect::route("getDetailsEmployee",$employee_id)->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function makafatEmployeeUpdate()
	{
		if(canEdit('hr_documents'))
		{
			$id = \Crypt::decrypt(Input::get('id'));
	
			$sdate = Input::get("date");
				
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			$items = array(
					"date"					=> $sdate,
					"result"				=> Input::get('result'),
					"type"					=> Input::get('type')
				);
			
			hrOperation::update_record('employee_makafats',$items,array('id'=>$id));
			hrOperation::insertLog('employee_makafats',1,'makafat of given employee',$id);
			return \Redirect::route("getDetailsEmployee",Input::get('employee_id'))->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}	
	}
	//communication
	public function getEmployeeCommunication()
	{
		//check roles
		if(canView('hr_communication'))
		{
			//check roles
			return View::make('hr.communication.list');
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeComData()
	{		
		//get all data 
		$object = hrOperation::getComData();//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name',
							'current_position_dr',
							'department'
							)
				->addColumn('operations', function($option){
					$options = '';
					if(canView('hr_communication'))
					{
						$options .= '
								<a href="'.route('getDetailsEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="نمایش">
									<i class="icon fa-eye" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
					}
					if(canAdd('hr_communication'))
					{
						$options .= '
								
								<a href="#" onclick="$('.'\'#employee_id\''.').val('.$option->id.');$('.'\'#employee\''.').val(\''.$option->name.'\');" class="table-link" style="text-decoration:none;" title="استعلام" data-target="#estelam_modal" data-toggle="modal">
									<i class="icon fa-refresh" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
								<a href="#" onclick="$('.'\'#employee_card\''.').val('.$option->id.');$('.'\'#employee\''.').val(\''.$option->name.'\');" class="table-link" style="text-decoration:none;" title="کارت" data-target="#card_modal" data-toggle="modal">
									<i class="icon fa-plus" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
					}
					
					return $options;
				})
				->make();
		return $result;
	}
	public function getEmployeeComCardData()
	{		
		//get all data 
		$object = hrOperation::getComCardData();//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'current_position_dr',
							'department',
							'card_no',
							'card_color',
							'issue_date'
							)
				->addColumn('operations', function($option){
					$options = '';
					$options .= '
								<a href="javascript:void()" class="table-link" style="text-decoration:none;" title="نمایش">
									<i class="icon fa-eye" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
								
								';
					
					
					return $options;
				})
				->make();
		return $result;
	}
	public function getComEmplyeeMaktobs()
	{	
		//get all data 
		$object = hrOperation::getComMaktobData();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'number',
							'date',
							'source'
							)
				->addColumn('operations', function($option){
					return '
							<a href="javascript:void()" onclick="" class="table-link" style="text-decoration:none;" title="Edit" data-target="#maktob_modal"" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							';
				})
				->make();
	}
	public function postComMaktob()
	{
		$sdate = Input::get("date");
			
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);		
		}
		$items = array(
				"number" 				=> Input::get('number'),
				"date"					=> $sdate,
				"general_dep"			=> Input::get('general_department'),
				"sub_dep"				=> Input::get('sub_dep'),
				"created_at" 			=> date('Y-m-d H:i:s'),
				"created_by" 			=> Auth::user()->id
			);
		
		$id = hrOperation::insertRecord_id('com_maktobs',$items);
		$employees = Input::get('employees');
		for($i=0;$i<count($employees);$i++)
		{
			$emps = array(
						'employee_id' 	=> $employees[$i],
						'maktob_id' 	=> $id,
						"created_at" 	=> date('Y-m-d H:i:s'),
						"created_by" 	=> Auth::user()->id
				);
			hrOperation::insertRecord('com_maktob_employees',$emps);
		}
		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function getComEmplyeeEstelams()
	{	
		//get all data 
		$object = hrOperation::getComEstelamData();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'number',
							'date',
							'name'
							)
				->addColumn('operations', function($option){
					return '
							<a href="javascript:void()" onclick="" class="table-link" style="text-decoration:none;" title="Edit" data-target="#change_date_modal"" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							';
				})
				->make();
	}
	public function postComEstelam()
	{
		$sdate = Input::get("date");
			
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);		
		}
		$items = array(
				"number" 				=> Input::get('number'),
				"date"					=> $sdate,
				"employee_id"			=> Input::get('employee_id'),
				"created_at" 			=> date('Y-m-d H:i:s'),
				"created_by" 			=> Auth::user()->id
			);
		
		hrOperation::insertRecord('com_estelams',$items);
		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function getComEmplyeeSecurity()
	{	
		//get all data 
		$object = hrOperation::getComSecurityData(0);//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'number',
							'date',
							'file_name'
							)
				->addColumn('operations', function($option){
					return '
							<a href="javascript:void()" onclick="" class="table-link" style="text-decoration:none;" title="Edit" data-target="#security_modal"" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							<a href="javascript:void()" onclick="$('.'\'#security_id\''.').val('.$option->id.');" class="table-link" style="text-decoration:none;" title="موافقه" data-target="#accept_modal"" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
				})
				->make();
	}
	public function postComSecurity($type=0)
	{
		$sdate = Input::get("date");
			
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);		
		}
		
		$items = array(
				"number" 				=> Input::get('number'),
				"date"					=> $sdate,
				//"file_name"				=> Input::get('employee_id'),
				"type"					=> $type,
				"created_at" 			=> date('Y-m-d H:i:s'),
				"created_by" 			=> Auth::user()->id
			);
		
		$id = hrOperation::insertRecord_id('com_securities',$items);
		
		if(Input::hasFile('scan'))
		{
			// getting all of the post data
			$file = Input::file('scan');
			$errors = "";
			$file_data = array();
			// validating each file.
			$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			$validator = Validator::make(

			  		[
			            'scan' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'scan' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			);

			if($validator->passes())
			{
			    // path is root/uploads
			    $destinationPath = 'documents/hr_attachments';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    if($type==2)
			    {
			    	$filename = 'com_protection_'.$id.'.'.$extension;
			    }
			    else
			    {
			    	$filename = 'com_security_'.$id.'.'.$extension;
				}
			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
					DB::connection('hr')->table('com_securities')->where('id',$id)->update(array('file_name'=>$filename));
				} 
				else 
				{
				   $errors .= json('error', 400);
				}
			} 
			else 
			{
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			}
		}
		$employees = Input::get('employees');
		for($i=0;$i<count($employees);$i++)
		{
			$emps = array(
						'employee_id' 	=> $employees[$i],
						'security_id' 	=> $id,
						"created_at" 	=> date('Y-m-d H:i:s'),
						"created_by" 	=> Auth::user()->id
				);
			hrOperation::insertRecord('com_security_employees',$emps);
		}
		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function postComEmployeeCard()
	{
		$sdate = Input::get("issue_date");
		$edate = Input::get("expiry_date");	
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);		
		}
		if($edate != '')
		{
			$edate = explode("-", $edate);
			$ey = $edate[2];
			$em = $edate[1];
			$ed = $edate[0];
			$edate = dateToMiladi($ey,$em,$ed);		
		}
		$items = array(
				"card_no" 				=> Input::get('card_no'),
				"employee_id"			=> Input::get('employee_card'),
				"issue_date"			=> $sdate,
				"expiry_date"			=> $edate,
				//"file_name"				=> Input::get('employee_id'),
				"card_color"			=> Input::get('card_color'),
				"created_at" 			=> date('Y-m-d H:i:s'),
				"created_by" 			=> Auth::user()->id
			);
		
		$id = hrOperation::insertRecord_id('employee_cards',$items);
		
		if(Input::hasFile('scan'))
		{
			// getting all of the post data
			$file = Input::file('scan');
			$errors = "";
			$file_data = array();
			// validating each file.
			$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			$validator = Validator::make(

			  		[
			            'scan' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'scan' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			);

			if($validator->passes())
			{
			    // path is root/uploads
			    $destinationPath = 'documents/hr_attachments';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    $filename = 'com_card_'.$id.'.'.$extension;
			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
					DB::connection('hr')->table('employee_cards')->where('id',$id)->update(array('file_name'=>$filename));
				} 
				else 
				{
				   $errors .= json('error', 400);
				}
			} 
			else 
			{
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			}
		}
		
		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function postComSecurityAccept()
	{
		$sdate = Input::get("date");
			
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);		
		}
		
		$items = array(
				"number" 				=> Input::get('number'),
				"date"					=> $sdate,
				//"file_name"				=> Input::get('employee_id'),
				"type"					=> 1,//accept
				"created_at" 			=> date('Y-m-d H:i:s'),
				"created_by" 			=> Auth::user()->id
			);
		
		$id = hrOperation::insertRecord_id('com_securities',$items);
		
		if(Input::hasFile('scan'))
		{
			// getting all of the post data
			$file = Input::file('scan');
			$errors = "";
			$file_data = array();
			// validating each file.
			$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			$validator = Validator::make(

			  		[
			            'scan' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'scan' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			);

			if($validator->passes())
			{
			    // path is root/uploads
			    $destinationPath = 'documents/hr_attachments';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    
			    $filename = 'com_securityAccept_'.$id.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
					DB::connection('hr')->table('com_securities')->where('id',$id)->update(array('file_name'=>$filename));
				} 
				else 
				{
				   $errors .= json('error', 400);
				}
			} 
			else 
			{
			    // redirect back with errors.
			    return Redirect::back()->withErrors($validator);
			}
		}
	
		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function getComEmplyeeProtection()
	{	
		//get all data 
		$object = hrOperation::getComSecurityData(2);//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'number',
							'date',
							'file_name'
							)
				->addColumn('operations', function($option){
					return '
							<a href="javascript:void()" onclick="" class="table-link" style="text-decoration:none;" title="Edit" data-target="#security_modal"" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							';
				})
				->make();
	}
	public function getComComplaints()
	{	
		//get all data 
		$object = hrOperation::getComComplaintsData();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'reg_date',
							'desc'
							)
				->addColumn('operations', function($option){
					return '
							<a href="javascript:void()" onclick="$('.'\'#complain_id\''.').val('.$option->id.');" class="table-link" style="text-decoration:none;" title="ارجاع به کمیته" data-target="#edit_complain_modal" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							';
				})
				->make();
	}
	public function getComComitte()
	{	
		//get all data 
		$object = hrOperation::getComComitteData();//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'number',
							'date',
							'desc'
							)
				->addColumn('operations', function($option){
					return '
							<a href="javascript:void()" onclick="$('.'\'#comitte_id\''.').val('.$option->id.');" class="table-link" style="text-decoration:none;" title="فیصله کمیته" data-target="#comitte_modal" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							';
				})
				// ->orderColumns('id','id')
				->make();
	}
	public function postComComplain()
	{
		$sdate = Input::get("date");
			
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);		
		}
		
		$items = array(
				"desc" 				=> Input::get('desc'),
				"reg_date"					=> $sdate,
				"created_at" 			=> date('Y-m-d H:i:s'),
				"created_by" 			=> Auth::user()->id
			);
		
		hrOperation::insertRecord('com_complaints',$items);
	
		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function postComComitte()
	{
		$sdate = Input::get("date");
			
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);		
		}
		
		$items = array(
				"number" 		=> Input::get('number'),
				"date"			=> $sdate,
				"complain_id"	=> Input::get('complain_id'),
				"created_at" 	=> date('Y-m-d H:i:s'),
				"created_by" 	=> Auth::user()->id
			);
		
		hrOperation::insertRecord('com_comitteis',$items);
	
		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function postComComitteResult()
	{
		$comitte_id = Input::get('comitte_id');
		$items = array(
				"result" 		=> Input::get('result')
			);
		
		hrOperation::update_record('com_comitteis',$items,array('id'=>$comitte_id));
	
		return \Redirect::route("getEmployeeCommunication")->with("success","Record Saved Successfully.");
	}
	public function getAttendance(Request $request)
	{
		$ip = $request->ip();
		$log = array('ip'=>$ip,'time'=>date('Y-m-d H:i:s'),'user_id'=>Auth::user()->id,'name'=>Auth::user()->first_name.' '.Auth::user()->last_name);
		hrOperation::insertRecord('attendance_log',$log);
		//check roles
		//if(canAttHoliday() || canCheckImages() || canAttTodayAtt())
		if(canView('hr_attendance'))
		{
			//check if the user is director or deputy
			$is_manager = is_manager_att(Auth::user()->id);
			if($is_manager->position_id==2)
			{//the user is director
				$data['parentDeps'] = getDepartmentWhereIn();
				$data['details'] = $is_manager;
				$data['sub_dep']=getRelatedSubDepartment($is_manager->gen_dep);
				return View::make('hr.attendance.directorate_attendance',$data);
			}
			elseif($is_manager->position_id==3)
			{//the user is deputy
				$data['parentDeps'] = getDepartmentWhereIn();
				$data['details'] = $is_manager;
				$data['sub_dep']=getRelatedSubDepartment($is_manager->sub_dep);
				return View::make('hr.attendance.directorate_attendance',$data);
			}
			else
			{
				$data['parentDeps'] = getDepartmentWhereIn();
				return View::make('hr.attendance.list',$data);
			}
		}
		elseif(canCheckAtt())
		{//normal users
			$the_date = dateToShamsi(date('Y'),date('m'),date('d'));
			$s_date = explode('-',$the_date);
			$year = $s_date[0];
			if($s_date[2]>15)
			{
				$month= $s_date[1]+1;
			}
			else
			{
				$month= $s_date[1];
			}
			
			$details = hrOperation::getEmployeeByUserId(Auth::user()->id);
			
			$rfid = $details->RFID;
			$data['enc_rfid'] = Crypt::encrypt($rfid);
			if($rfid>0)
			{
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
				$rfid_lost = lost_rfid($data['photo']->id);
				if($rfid_lost)
				{
					$rfids = array($rfid);
					$data['rfids'] = $rfid_lost;
					foreach($rfid_lost as $new_rfid)
					{
						$rfids[] =$new_rfid->rfid; 
					}
					//dd($rfids);
					$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
				}
				else
				{
					$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
				}
				$data['rfid'] = $rfid;
				$data['year'] = $year;
				$data['month'] = $month;
				
				return View::make('hr.attendance.check_att',$data);
			}
			else
			{
				return showWarning();
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeAttendanceCheck_itself($rfid=0,$year=0,$month=0)
	{
		$rfid = Crypt::decrypt($rfid);
		if($rfid>0)
		{
			$emp_details = hrOperation::getEmployeeByUserId(Auth::user()->id);
			if($emp_details->RFID!=$rfid)
			{
				return showWarning();
			}
			$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
			$rfid_lost = lost_rfid($data['photo']->id);
			if($rfid_lost)
			{
				$rfids = array($rfid);
				$data['rfids'] = $rfid_lost;
				foreach($rfid_lost as $new_rfid)
				{
					$rfids[] =$new_rfid->rfid; 
				}
				//dd($rfids);
				$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
			}
			else
			{
				$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
			}
			$data['enc_rfid'] = Crypt::encrypt($rfid);
			$data['rfid'] = $rfid;
			$data['year'] = $year;
			$data['month'] = $month;
			
			return View::make('hr.attendance.check_att',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function leavesManager()
	{
		//check roles
		if(canAttHoliday())
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			return View::make('hr.attendance.leavesList',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function holidaysManager()
	{
		//check roles
		if(canAttHoliday())
		{
			return View::make('hr.attendance.holidays');
		}
		else
		{
			return showWarning();
		}
	}
	public function putImagesToHr()
	{
		//check roles
		if(Auth::user()->id == 113 || Auth::user()->id == 174 || Auth::user()->id == 201)
		{
			$errors='';
			$devices = array(
						//array('10.10.7.241','شعبه حاضری طرف راست'),
					    array('10.10.7.240','حاضری محصلین صبح'),
					    array('10.10.7.212','خدمات منزل بالا'),
						//array('10.10.7.211','خدمات منزل بالا'),
						//array('10.10.3.147','دفتر ضیا مسعود'),
						array('10.10.3.140','منزل دوم اداره امور'),
						//array('10.134.45.65','آشپزخانه خاص'),
						array('10.20.3.195','تعمیراداری طرف راست'),
						array('10.134.45.82','تعمیر سنگی'),
						array('10.10.3.138','منزل اول اداره امور'),
						//array('10.10.3.138','منزل اول اداره امور'),
						//array('10.134.46.82','دفتر خانم اول'),
						//array('10.10.3.139','منزل سوم اداره امور'),
						array('10.20.3.213','شعبه فهیم'),
						array('10.20.3.194','تعمیر اداری طرف چپ'),
						//array('10.134.45.40','دفتر سلام رحیمی'),
						array('10.10.7.241','حاضری محصلین بعدازظهر'),
						array('10.10.9.150','وزارت معدن'),
						array('10.10.9.155','کارتوگرافی'),
						array('10.20.3.206','شعبه فهیم محصلین')
					);
			for($i=0;$i<count($devices);$i++)
			{
				$ip = $devices[$i][0];
				$exec = exec( "ping -c 2 -s 64 -t 64 ".$ip, $output, $return );
				if($return)
				{
					$errors.='IP Address: '.$ip.',Device Location: '.$devices[$i][1].'<br/>';
				}
			}
			if($errors!='')
			{//atleast one of the devices is not connected
				echo 'ماشین های ذیل به سیستم وصل نمیباشد:';
				echo '<br/>';
				dd($errors);
			}
			else
			{
				if(Auth::user()->id == 113 || Auth::user()->id == 174)
				{
					return View::make('hr.attendance.put_images');
				}
				else
				{
					dd("<h3>تمام ماشین های حاضری به سیستم وصل میباشد.</h3>");
				}
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getRelatedEmployees()
	{
		if(canView('hr_attendance'))
		{
			$data['year'] = Input::get('year');//shamsi year
			$data['month'] = Input::get('month');//shamsi month
			if(Input::get('general_department')!=null)
			{
				$data['dep'] = Input::get('general_department');
			}
			else
			{
				$data['dep']=0;
			}
			if(Input::get('sub_dep')!=null)
			{
				$data['dep_id'] = Input::get('sub_dep');
			}
			else
			{
				$data['dep_id']=0;
			}
			
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_dep']=getRelatedSubDepartment(Input::get('general_department'));
			//$data['employees'] = hrOperation::getRelatedEmployees(Input::get('dep_id'));
			return View::make('hr.attendance.employeeAttList',$data);
			//return View::make('hr.attendance.relatedEmployeeList',$data);
		}
		else
		{
			return showWarning();
		}	
	}
	public function getRelatedEmployees_get($dep_id=0,$sub_dep=0,$year,$month)
	{
		if(canView('hr_attendance'))
		{
			$data['year'] = $year;//shamsi year
			$data['month'] = $month;//shamsi month
			if($dep_id!=0)
			{
				$data['dep'] = $dep_id;
			}
			else
			{
				$data['dep']=0;
			}
			if($sub_dep!=0)
			{
				$data['dep_id'] = $sub_dep;
			}
			else
			{
				$data['dep_id']=0;
			}
			
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_dep']=getRelatedSubDepartment($dep_id);
			//$data['rows'] = hrOperation::getRelatedEmployeesAjax($data['dep'],$data['dep_id'],10);
			//$data['employees'] = hrOperation::getRelatedEmployees(Input::get('dep_id'));
			return View::make('hr.attendance.employeeAttList',$data);
			//return View::make('hr.attendance.relatedEmployeeList',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getAttendanceAjax()
	{
		if(canView('hr_attendance'))
		{
			$data['year'] = Input::get('year');//shamsi year
			$data['month'] = Input::get('month');//shamsi month
			if(Input::get('general_department')!=null)
			{
				$data['dep'] = Input::get('general_department');
			}
			else
			{
				$data['dep']=0;
			}
			if(Input::get('sub_dep')!=null)
			{
				$data['dep_id'] = Input::get('sub_dep');
			}
			else
			{
				$data['dep_id']=0;
			}
			
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_dep']=getRelatedSubDepartment(Input::get('general_department'));
			
			$per_page=10;
			
			$data['rows'] = hrOperation::getRelatedEmployeesAjax($data['dep'],$data['dep_id'],$per_page);
			//dd($data['rows']);
			return View::make('hr.attendance.attendance_ajax',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getDirAttendance()
	{
		if(canView('hr_attendance'))
		{
			$data['year'] = Input::get('year');//shamsi year
			$data['month'] = Input::get('month');//shamsi month
			if(Input::get('general_department')!=null)
			{
				$data['dep'] = Input::get('general_department');
			}
			else
			{
				$data['dep']=0;
			}
			if(Input::get('sub_dep')!=null)
			{
				$data['dep_id'] = Input::get('sub_dep');
			}
			else
			{
				$data['dep_id']=0;
			}
			
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_dep']=getRelatedSubDepartment(Input::get('general_department'));
			
			$per_page=10;
			
			$data['rows'] = hrOperation::getRelatedEmployeesAjax($data['dep'],$data['dep_id'],$per_page);
			//dd($data['rows']);
			return View::make('hr.attendance.dir_attendance_ajax',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getSearchResult()
	{
		if(canView('hr_attendance'))
		{
			$data['year'] = Input::get('year');//shamsi year
			$data['month'] = Input::get('month');//shamsi month
			if(Input::get('general_department')!=null)
			{
				$data['dep'] = Input::get('general_department');
			}
			else
			{
				$data['dep']=0;
			}
			if(Input::get('sub_dep')!=null)
			{
				$data['dep_id'] = Input::get('sub_dep');
			}
			else
			{
				$data['dep_id']=0;
			}
			
			$data['rows'] = hrOperation::getRelatedEmployeesAjax_search(Input::get('item'),$data['dep'],$data['dep_id']);
			
			//dd($data['rows']);
			return View::make('hr.attendance.attendance_ajax_search',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getDirSearchResult()
	{
		if(canView('hr_attendance'))
		{
			$data['year'] = Input::get('year');//shamsi year
			$data['month'] = Input::get('month');//shamsi month
			if(Input::get('general_department')!=null)
			{
				$data['dep'] = Input::get('general_department');
			}
			else
			{
				$data['dep']=0;
			}
			if(Input::get('sub_dep')!=null)
			{
				$data['dep_id'] = Input::get('sub_dep');
			}
			else
			{
				$data['dep_id']=0;
			}
			
			$data['rows'] = hrOperation::getRelatedEmployeesAjax_search(Input::get('item'),$data['dep'],$data['dep_id']);
			
			//dd($data['rows']);
			return View::make('hr.attendance.dir_attendance_ajax_search',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeAttData($dep_id=0,$sub=0,$year,$month)
	{	
		if(canView('hr_attendance'))
		{
			//get all data 
			$object = hrOperation::getRelatedEmployees($dep_id,$sub);//print_r($report);exit;
			$collection = new Collection($object);
			return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'father_name',
							'position',
							'department'
							)
				->addColumn('present', function($option) use($year,$month){
					
					return getEmployeePresentDays($option->RFID,$year,$month)+getPresentedDays($option->RFID,$year,$month);
					
				})
				->addColumn('absent', function($option) use($year,$month){
					return getEmployeeAbsentDays(getEmployeePresentDays($option->RFID,$year,$month)+getPresentedDays($option->RFID,$year,$month),$year,$month) - getEmployeeLeaveDays($option->id,$year,$month);
					//$options .= getEmployeeLeaveDays($option->id,2015,12);
					
				})
				->addColumn('leave', function($option) use($year,$month){
					//$options = getEmployeeAbsentDays(getEmployeePresentDays($option->RFID,2015,12),2015,12) - countHolidays(2015,12);
					return getEmployeeLeaveDays($option->id,$year,$month);
					
				})
				->addColumn('verify', function($option) use($year,$month){
					$options = '';
					if($option->RFID!=0)
					{
						/*
						<a href="'.route('approveEmployeeAttendance',array($option->RFID,$year,$month)).'" class="table-link" style="text-decoration:none;">
									تایید عکسها
								</a> | 
						*/
						if(canCheckImages())
						{
							$options .= '
								<a href="'.route('getEmployeeAttendanceCheck',array($option->RFID,$year,$month,$option->general_department,$option->dep_id)).'" class="table-link" style="text-decoration:none;">
									بررسی عکسها
								</a>
								';
						}
						else
						{
							$options .= '
								<a href="javascript:void()" onclick="load_salary_wait('.$option->id.');" class="table-link" style="text-decoration:none;" data-target="#salary_modal" data-toggle="modal">
									معطل به معاش
								</a> |
								
								<a href="'.route('getEmployeeAttendance',array($option->RFID,$year,$month,$option->general_department,$option->dep_id)).'" class="table-link" style="text-decoration:none;">
									بررسی عکسها
								</a>
								';
						}
					}
					else
					{
					$options .= '
								<a href="javascript:void()" class="table-link" style="text-decoration:none;">
								بدون کارت
								</a>
							
								';	
					}
					return $options;
				})
				->make();
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeSalaryWait()
	{
		$data['id'] = Input::get('id');
		$data['year'] = Input::get('year');
		$data['month'] = Input::get('month');
		$data['details'] = hrOperation::getEmployeeSalaryWait(Input::get('id'),Input::get('year'),Input::get('month'));
		
		return View::make('hr.attendance.employeeSalaryWait',$data);
	}
	public function getLeavesEmployees()
	{
		if(canAttHoliday())
		{
			$data['dep'] = Input::get('general_department');
			if(Input::get('sub_dep'))
			{
				$data['sub_dep'] = Input::get('sub_dep');
			}
			else
			{
				$data['sub_dep'] = 0;
			}
			$data['year'] 		= Input::get('year');
			$data['month'] 		= Input::get('month');
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_deps'] 	= getRelatedSubDepartment(Input::get('general_department'));
			//$data['employees'] = hrOperation::getRelatedEmployees(1,Input::get('dep_id'));
			
			return View::make('hr.attendance.employeeLeaves',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getLeavesEmployeesData($dep=0,$sub_dep=0,$year,$month)
	{		
		//get all data 
		$object = hrOperation::getEmployees_leave($dep,$sub_dep);//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'father_name',
							'position'
							)
				->addColumn('status', function($option){
					if($option->changed==1){$status = '<span style="color:red">تبدیلی</span>';}
                    elseif($option->fired==1){$status = '<span style="color:red">منفک</span>';}
                    elseif($option->resigned==1){$status = '<span style="color:red">استعفا</span>';}
                    elseif($option->retired==1){$status = '<span style="color:red">تقاعد</span>';}
                    elseif($option->position_dr==4){$status = '<span style="color:red">انتظاربامعاش</span>';}
                    else{$status='کارمند برحال';}
                    return $status;
				})
				->addColumn('zarori', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,1,$year,$month);
				})
				->addColumn('tafrihi', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,2,$year,$month);
				})
				->addColumn('sick', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,3,$year,$month);
				})
				->addColumn('pregnant', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,4,$year,$month);
				})
				->addColumn('wedding', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,5,$year,$month);
				})
				->addColumn('haj', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,6,$year,$month);
				})
				->addColumn('extra_seak', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,8,$year,$month);
				})
				->addColumn('other', function($option) use($year,$month){
					return getEmployeeLeaves($option->id,7,$year,$month);
				})
				->addColumn('total', function($option) use($year,$month){
					return getEmployeeLeaves_total($option->id,$year,$month);
				})
				->addColumn('operations', function($option) use($year,$month){
					return '
							<a href="'.route('getEmployeeLeaves',array($option->id,$year,$month)).'" class="table-link" style="text-decoration:none;">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							';
				})
				->make();
		return $result;
	}
	public function getAllInShifts()
	{
		$data['parentDeps'] = getDepartmentWhereIn();
		$total=hrOperation::getEmployees_inShift();
		$data['total']=count($total);
		return View::make('hr.attendance.shiftList',$data);
	}
	public function getShiftEmployees($dep=0,$sub=0,$status=0,$gender=0)
	{
		if(canAdd('hr_attendance'))
		{
			$data['dep_id'] = $dep;
			$data['sub_dep_id'] = $sub;
			$data['status'] = $status;
			$data['gender'] = $gender;
			$total=hrOperation::getEmployees_inShift($dep,$sub,$status,$gender);
			$data['total']=count($total);
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_deps']=getRelatedSubDepartment($dep);
			//$data['employees'] = hrOperation::getRelatedEmployees(Input::get('dep_id'));
			return View::make('hr.attendance.shiftList',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getShiftEmployeesData($dep=0,$sub=0,$status=0,$gender=0)
	{
		$object = hrOperation::getEmployees_inShift($dep,$sub,$status,$gender);
		
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name_dr',
							'dep_name',
							'position'
							)
						->addColumn('status', function($option){
						if($option->time_in!=''){$status = 'صبح';}
	                    elseif($option->time_out!=''){$status = 'بعدازظهر';}
	                    else{$status='پنجشنبه';}
	                    return $status;
					})
				
				->make();
		return $result;
	}
	public function printShiftEmployees($dep=0,$sub_dep=0,$status,$gender=0)
	{
		$data['records'] = hrOperation::getEmployees_inShift($dep,$sub_dep,$status,$gender);
		
		Excel::create('students', function($file) use($data){
			
			$file->setTitle('Attendance');
		
			$file->sheet('Attendance', function($sheet) use($data){
				$sheet->setRightToLeft(true);
				$sheet->setCellValue('A1','#');
				$sheet->setCellValue('B1','اسم');
				$sheet->setCellValue('C1','ولد');
				$sheet->setCellValue('D1','دیپارتمنت');
				$sheet->setCellValue('E1','وظیفه');
				$sheet->setCellValue('F1','وضعیت');
				$sheet->row(1, function($row) {
				    // call cell manipulation methods
				    $row->setBackground('#e0ebeb');
					$row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$row = 2;
				$counter = 1;
				foreach($data['records'] AS $emp)
				{
					$sheet->setCellValue('A'.$row,$counter);
					$sheet->setCellValue('B'.$row,$emp->fullname);
					$sheet->setCellValue('C'.$row,$emp->father_name_dr);
					$sheet->setCellValue('D'.$row,$emp->dep_name);
					$sheet->setCellValue('E'.$row,$emp->position);
					if($emp->time_in!='')
					{
						$sheet->setCellValue('F'.$row,'صبح');
					}
					elseif($emp->time_out!='')
					{
						$sheet->setCellValue('F'.$row,'بعدازظهر');
					}
					else
					{
						$sheet->setCellValue('F'.$row,'پنجشنبه');
					}
					$row++;
					$counter++;
				}
				$sheet->setBorder('A1:E'.$row.'', 'dotted');
    		});
		
		})->download('xlsx');
	}
	public function getAllAttEmps()
	{
		$data['parentDeps'] = getDepartmentWhereIn();
		$data['total'] = hrOperation::getEmployees_inAtt();
		return View::make('hr.attendance.att_employees',$data);
	}
	public function getAtttEmployeesData($dep=0,$sub=0,$gender=0)
	{
		$object = hrOperation::getEmployees_inAtt($dep,$sub,$gender);
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name_dr',
							'dep_name',
							'position'
							)
				
				->make();
		return $result;
	}
	public function getAttEmployees($dep=0,$sub=0,$gender=0)
	{
		if(canAdd('hr_attendance'))
		{
			$data['dep_id'] = $dep;
			$data['sub_dep_id'] = $sub;
			$data['gender'] = $gender;
			
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_deps']=getRelatedSubDepartment($dep);
			$data['total'] = hrOperation::getEmployees_inAtt($dep,$sub,$gender);
			return View::make('hr.attendance.att_employees',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getPayrollList($dep_id=0,$year=0,$month=0)
	{
		$data['year'] 	= $year;
		$data['month'] 	= $month;
		$data['dep_id']	= $dep_id;
		$data['employees'] = hrOperation::getEmployeePayroll($dep_id);
		
		return View::make('hr.attendance.payrollList',$data);
	}
	public function getAttCharts($dep_id=0,$year=0,$month=0)
	{
		$data['year'] 	= $year;
		$data['month'] 	= $month;
		$data['dep_id']	= $dep_id;
		$data['dep'] = hrOperation::getDepName($dep_id);
		$data['dep_employees'] = hrOperation::getDepEmployeeTotal($dep_id);
		return View::make('hr.attendance.attCharts',$data);
	}
	public function printDailyAttChart($dep_id=0)
	{
		$data['dep_id']	= $dep_id;
		$data['dep'] = hrOperation::getDepName($dep_id);
		$data['date'] = dateToShamsi(date('Y'),date('m'),date('d'));
		//$data['records'] = hrOperation::getEmployeesTodayAtt(1,$dep_id,0);
		return View::make('hr.attendance.dailyAttCharts',$data);
	}
	public function printPayroll($dep_id=0,$year=0,$month=0)
	{
		if(canView('hr_attendance'))
		{
			$data['year'] 	= $year;
			$data['month'] 	= $month;
			$data['dep_id']	= $dep_id;
			$data['employees'] = hrOperation::getEmployeePayroll($dep_id);
			
			Excel::create('Payroll-'.$month, function($file) use($data){
				
				$file->setTitle('Payroll');
			
				$file->sheet('Payroll', function($sheet) use($data){
					$sheet->setCellValue('A2','#');
					$sheet->setCellValue('B2','نام کامل');
					$sheet->setCellValue('C2','نام پدر');
					$sheet->setCellValue('D2','معاش');
					$sheet->setCellValue('E2','جمله امتیازات');
					$sheet->setCellValue('F2','جمله کسرات');
					$sheet->setCellValue('G2','تقاعد');
					$sheet->setCellValue('H2','غیرحاضر');
					$sheet->setCellValue('I2','مالیات');
					$sheet->setCellValue('J2','مبلغ قابل پرداخت');
						
					$year = $data['year'];
					$month= $data['month'];
					$row = 3;
					$day_counter=1;
					foreach($data['employees'] AS $emp)
					{
						$total_salary = $emp->extra_salary+$emp->main_salary+$emp->prof_salary+$emp->kadri_salary;
						$total_benifits = getEmployeePayrollDetTotal($emp->id,$year,$month,'in');
						$total_ksorat = getEmployeePayrollDetTotal($emp->id,$year,$month,'out');
						$salary_wait = getEmployeeSalaryWait($emp->id,$year,$month);
						
	               		$leaves = getEmployeeLeaveDays($emp->id,$year,$month);
	               		$presents = getEmployeePresentDays($emp->RFID,$year,$month);
	               		$holidays = countHolidays($year,$month);//fridays and 5shanba
	               		$paid_days = $leaves + $presents + $holidays;
	               		$monthDays = countMonthDays($year,$month);
	               		$unpaid_days = $monthDays - ($paid_days);
	               		$salary_perday = 0;//emtiazi
	               		$main_salary_perday = 0;//asli
	               		$prof_salary_perday = 0;//maslaki
	               		$kadri_salary_perday = 0;//kadri
	               		$extra_tax = calculateTax($emp->extra_salary);
	               		$main_tax = calculateTax($emp->main_salary);
	               		$prof_tax = calculateTax($emp->prof_salary);
	               		$kadri_tax = calculateTax($emp->kadri_salary);
	               		$total_tax = $main_tax+$extra_tax+$prof_tax+$kadri_tax;
	               		if($emp->extra_salary!=0)
	               		{
	               			$salary_perday = $emp->extra_salary/30;
	               		}
	               		if($emp->main_salary!=0)
	               		{
	               			$main_salary_perday = $emp->main_salary/30;
	               		}
	               		if($emp->prof_salary!=0)
	               		{
	               			$prof_salary_perday = $emp->prof_salary/30;
	               		}
	               		if($emp->kadri_salary!=0)
	               		{
	               			$kadri_salary_perday = $emp->kadri_salary/30;
	               		}
	               		
	               		$absent_deduction = round($unpaid_days*$salary_perday)+round($unpaid_days*$main_salary_perday)+round($unpaid_days*$prof_salary_perday)+round($unpaid_days*$kadri_salary_perday);
	                 	
						$sheet->setCellValue('A'.$row.'',$day_counter);
						$sheet->setCellValue('B'.$row.'',$emp->name);
						$sheet->setCellValue('C'.$row.'',$emp->father_name);
						$sheet->setCellValue('D'.$row.'',$total_salary);
						$sheet->setCellValue('E'.$row.'',$total_benifits);
						$sheet->setCellValue('F'.$row.'',$total_ksorat);
						$sheet->setCellValue('G'.$row.'',$emp->retirment);
						$sheet->setCellValue('H'.$row.'',$absent_deduction);
						$sheet->setCellValue('I'.$row.'',$total_tax);
						$sheet->setCellValue('J'.$row.'',($total_salary+$total_benifits)-($emp->retirment+$total_tax+$absent_deduction+$total_ksorat));
						
						$row++;$day_counter++;
					}
	    		});
			
			})->download('xlsx');
		}
		else
		{
			return showWarning();
		}
	}
	public function printAttReport($dep_id=0,$year=0,$month=0)
	{
		if(canView('hr_attendance'))
		{
			$data['year'] 	= $year;
			$data['month'] 	= $month;
			$data['dep_id']	= $dep_id;
			$data['employees'] = hrOperation::getEmployeeForAttPrint($dep_id);
			
			Excel::create('Payroll-'.$month, function($file) use($data){
				
				$file->setTitle('Payroll');
			
				$file->sheet('Payroll', function($sheet) use($data){
					$sheet->setRightToLeft(true);
					$sheet->setOrientation('landscape');
					$sheet->setCellValue('A1','#');
					$sheet->setCellValue('B1','نام کامل');
					$sheet->setCellValue('C1','نام پدر');
					$sheet->setCellValue('D1','رتبه');
					$sheet->setCellValue('E1','بست');
					$sheet->setCellValue('F1','وظیفه');
					$sheet->setCellValue('G1','کسرات');
					$sheet->setCellValue('H1','حاضر');
					$sheet->setCellValue('I1','غیرحاضر');
					$sheet->setCellValue('J1','رخصتی ضروری');
					$sheet->setCellValue('K1','رخصتی مریضی');
					$sheet->setCellValue('L1','رخصتی تفریحی');
					$sheet->setCellValue('M1','ملاحضات');
					
					$sheet->mergeCells('A2:M2');
					$sheet->row(2, function($row) {
					    // call cell manipulation methods
					    $row->setBackground('#e0ebeb');
						$row->setAlignment('center');
						//$row->setValignment('middle');
						$row->setFontWeight('bold');
					});
					$sheet->setCellValue('A2',getDepartmentName($data['dep_id']));
					
					$year = $data['year'];
					$month= $data['month'];
					$row = 3;
					$day_counter=1;
					foreach($data['employees'] AS $emp)
					{
						$present = getEmployeePresentDays($emp->RFID,$year,$month)+getPresentedDays($emp->RFID,$year,$month)+countHolidays($year,$month);
						$absent = getEmployeeAbsentDays($present,$year,$month) - getEmployeeLeaveDays($emp->id,$year,$month);
						if($absent>0)
						{
							$ksrat = 'دارد';
						}
						else
						{
							$ksrat = 'ندارد';
						}
						$zarori = getEmployeeLeaves($emp->id,1,$year,$month);
						$tafrihi = getEmployeeLeaves($emp->id,2,$year,$month);
						$sick = getEmployeeLeaves($emp->id,3,$year,$month);
						
						$sheet->setCellValue('A'.$row.'',$day_counter);
						$sheet->setCellValue('B'.$row.'',$emp->name);
						$sheet->setCellValue('C'.$row.'',$emp->father_name);
						$sheet->setCellValue('D'.$row.'',$emp->rank);
						$sheet->setCellValue('E'.$row.'',$emp->bast);
						$sheet->setCellValue('F'.$row.'',$emp->current_position_dr);
						$sheet->setCellValue('G'.$row.'',$ksrat);
						if($emp->RFID==null || $emp->employee_type==2)
						{//manual
							$sheet->setCellValue('H'.$row.'',$absent+countHolidays($year,$month));
							$sheet->setCellValue('I'.$row.'',0);
						}
						else
						{
							$sheet->setCellValue('H'.$row.'',$present);
							$sheet->setCellValue('I'.$row.'',$absent);
						}
						$sheet->setCellValue('J'.$row.'',$zarori);
						$sheet->setCellValue('K'.$row.'',$tafrihi);
						$sheet->setCellValue('L'.$row.'',$sick);
						$sheet->setCellValue('M'.$row.'','');
						
						$row++;$day_counter++;
					}
					$sheet->setBorder('A1:M'.$row.'');
	    		});
			
			})->download('xlsx');
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeAttendance($rfid=0,$year=0,$month=0,$dep_id=0,$sub_dep=0,$in_attendance=0)
	{
		if(canView('hr_attendance'))
		{
			if($rfid>0)
			{
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
				$data['next_rfid'] = hrOperation::getNextEmployeeRfid($rfid,'next');
				$data['previous_rfid'] = hrOperation::getNextEmployeeRfid($rfid,'previous');
				$rfid_lost = lost_rfid($data['photo']->id);
				if($rfid_lost)
				{
					$rfids = array($rfid);
					$data['rfids'] = $rfid_lost;
					foreach($rfid_lost as $new_rfid)
					{
						$rfids[] =$new_rfid->rfid; 
					}
					//dd($rfids);
					$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
					$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
				}
				else
				{
					$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
					$data['presentedDays'] = hrOperation::getEmployeePresented($rfid);
				}
				
				$data['rfid'] = $rfid;
				$data['year'] = $year;
				$data['month'] = $month;
				$data['dep'] = $dep_id;
				$data['sub_dep'] = $sub_dep;
				
				$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
				return View::make('hr.attendance.verifying',$data);
			}
			else
			{//those who are not in attendance any more
				//get employee details
				$data['photo'] =  hrOperation::getPhotoByNoRfid($in_attendance);
				
				$rfid_lost = lost_rfid($data['photo']->id);
				if($rfid_lost)
				{
					$rfids = array($in_attendance);
					$data['rfids'] = $rfid_lost;
					foreach($rfid_lost as $new_rfid)
					{
						$rfids[] =$new_rfid->rfid; 
					}
					//dd($rfids);
					$data['details'] = hrOperation::getEmployeeImages_lost($in_attendance,$year,$month);
					$data['presentedDays'] = hrOperation::getEmployeePresented_lost($in_attendance);
				}
				else
				{
					$data['details'] = hrOperation::getEmployeeImages($in_attendance,$year,$month);
					$data['presentedDays'] = hrOperation::getEmployeePresented($in_attendance);
				}
				$data['rfid'] = $in_attendance;
				$data['year'] = $year;
				$data['month'] = $month;
				$data['dep'] = $dep_id;
				$data['sub_dep'] = $sub_dep;
				
				//$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
				return View::make('hr.attendance.verifying_no_rfid',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getEmployeeAttendanceCheck($rfid=0,$year=0,$month=0,$dep_id=0,$sub_dep=0)
	{
		if(canCheckImages())
		{
			if($rfid!='')
			{
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
				$data['next_rfid'] = hrOperation::getNextEmployeeRfid($rfid,'next');
				$data['previous_rfid'] = hrOperation::getNextEmployeeRfid($rfid,'previous');
				$rfid_lost = lost_rfid($data['photo']->id);
				if($rfid_lost)
				{
					$rfids = array($rfid);
					$data['rfids'] = $rfid_lost;
					foreach($rfid_lost as $new_rfid)
					{
						$rfids[] =$new_rfid->rfid; 
					}
					//dd($rfids);
					$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
					$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
				}
				else
				{
					$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
					$data['presentedDays'] = hrOperation::getEmployeePresented($rfid);
				}
				$data['rfid'] = $rfid;
				$data['year'] = $year;
				$data['month'] = $month;
				$data['dep'] = $dep_id;
				$data['sub_dep'] = $sub_dep;
				$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
				return View::make('hr.attendance.checking',$data);
			}
		}
		elseif(canCheckAtt())
		{
			if($rfid>0)
			{
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
				$rfid_lost = lost_rfid($data['photo']->id);
				if($rfid_lost)
				{
					$rfids = array($rfid);
					$data['rfids'] = $rfid_lost;
					foreach($rfid_lost as $new_rfid)
					{
						$rfids[] =$new_rfid->rfid; 
					}
					//dd($rfids);
					$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
					$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
				}
				else
				{
					$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
					$data['presentedDays'] = hrOperation::getEmployeePresented($rfid);
				}
				$data['rfid'] = $rfid;
				$data['year'] = $year;
				$data['month'] = $month;
				
				return View::make('hr.attendance.check_att',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getDirEmployee_images($enc_rfid=0,$year=0,$month=0,$enc_dep_id=0,$enc_sub_dep=0)
	{
		if(canCheckImages())
		{
			$rfid = Crypt::decrypt($enc_rfid);
			$dep_id=Crypt::decrypt($enc_dep_id);
			$sub_dep=Crypt::decrypt($enc_sub_dep);
			if($rfid!='')
			{
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
				
				$rfid_lost = lost_rfid($data['photo']->id);
				if($rfid_lost)
				{
					$rfids = array($rfid);
					$data['rfids'] = $rfid_lost;
					foreach($rfid_lost as $new_rfid)
					{
						$rfids[] =$new_rfid->rfid; 
					}
					//dd($rfids);
					$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
					//$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
				}
				else
				{
					$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
					//$data['presentedDays'] = hrOperation::getEmployeePresented($rfid);
				}
				$data['rfid'] = $rfid;
				$data['year'] = $year;
				$data['month'] = $month;
				$data['dep'] = $dep_id;
				$data['sub_dep'] = $sub_dep;
				$data['enc_dep'] = $enc_dep_id;
				$data['enc_sub_dep'] = $enc_sub_dep;
				//$data['enc_rfid'] = $enc_rfid;
				$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
				return View::make('hr.attendance.dir_checking',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getDirEmployeeImages_ajax()
	{
		if(canCheckImages())
		{
			$rfid = Input::get('rfid');
			$dep_id=Input::get('dep');
			$sub_dep=Input::get('sub_dep');
			$year=Input::get('year');
			$month=Input::get('month');
			if($rfid!='')
			{
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
				
				$rfid_lost = lost_rfid($data['photo']->id);
				if($rfid_lost)
				{
					$rfids = array($rfid);
					$data['rfids'] = $rfid_lost;
					foreach($rfid_lost as $new_rfid)
					{
						$rfids[] =$new_rfid->rfid; 
					}
					//dd($rfids);
					$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
					//$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
				}
				else
				{
					$data['details'] = hrOperation::getEmployeeImages($rfid,$year,$month);
					//$data['presentedDays'] = hrOperation::getEmployeePresented($rfid);
				}
				$data['rfid'] = $rfid;
				$data['year'] = $year;
				$data['month'] = $month;
				$data['dep'] = $dep_id;
				$data['sub_dep'] = $sub_dep;
				
				$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
				return View::make('hr.attendance.dir_checking_ajax',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function approveEmployeeAttendance($rfid=0,$year=0,$month=0)
	{
		if(canAdd('hr_attendance'))
		{
			if($rfid!='')
			{
				$data['next_rfid'] = hrOperation::getNextEmployeeRfid($rfid,'next');
				$data['previous_rfid'] = hrOperation::getNextEmployeeRfid($rfid,'previous');
				$data['rfid'] = $rfid;
				$data['year'] = $year;
				$data['month'] = $month;
				
				$data['photo'] =  hrOperation::getPhotoByRfid($rfid);
				$data['details'] = hrOperation::absentEmployeeImages($rfid,$year,$month);
				return View::make('hr.attendance.approve_images',$data);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public static function verifyNextEmployee($rfid=0,$year=0,$month=0,$type='next',$dep_id=0,$sub_dep=0)
	{
		if(canView('hr_attendance'))
		{
			//get next/prevous employee
			//dd(hrOperation::getNextEmployeeRfid($rfid,$type));
			$next_rfid = hrOperation::getNextEmployeeRfid($rfid,$type)->rfid;
			$data['photo'] =  hrOperation::getPhotoByRfid($next_rfid);
			$data['next_rfid'] = hrOperation::getNextEmployeeRfid($next_rfid,'next');
			$data['previous_rfid'] = hrOperation::getNextEmployeeRfid($next_rfid,'previous');
			if($year == 0)
			{
				$year = date('Y');
			}
			if($month == 0)
			{
				$month = date('m');
			}
			$rfid_lost = lost_rfid($data['photo']->id);
			if($rfid_lost)
			{
				$rfids = array($next_rfid);
				$data['rfids'] = $rfid_lost;
				foreach($rfid_lost as $new_rfid)
				{
					$rfids[] =$new_rfid->rfid; 
				}
				//dd($rfids);
				$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
				$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
			}
			else
			{
				$data['details'] = hrOperation::getEmployeeImages($next_rfid,$year,$month);
				$data['presentedDays'] = hrOperation::getEmployeePresented($next_rfid);
			}
			$data['rfid'] = $next_rfid;
			$data['year'] = $year;
			$data['month'] = $month;
			$data['dep'] = $dep_id;
			$data['sub_dep'] = $sub_dep;
			$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
			return View::make('hr.attendance.verifying',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public static function verifyNextEmployee_check($rfid=0,$year=0,$month=0,$type='next',$dep_id=0,$sub_dep=0)
	{
		if(canCheckImages())
		{
			//get next/prevous employee
			$next_rfid = hrOperation::getNextEmployeeRfid($rfid,$type)->rfid;
			$data['photo'] =  hrOperation::getPhotoByRfid($next_rfid);
			$data['next_rfid'] = hrOperation::getNextEmployeeRfid($next_rfid,'next');
			$data['previous_rfid'] = hrOperation::getNextEmployeeRfid($next_rfid,'previous');
			if($year == 0)
			{
				$year = date('Y');
			}
			if($month == 0)
			{
				$month = date('m');
			}
			$rfid_lost = lost_rfid($data['photo']->id);
			if($rfid_lost)
			{
				$rfids = array($next_rfid);
				$data['rfids'] = $rfid_lost;
				foreach($rfid_lost as $new_rfid)
				{
					$rfids[] =$new_rfid->rfid; 
				}
				//dd($rfids);
				$data['details'] = hrOperation::getEmployeeImages_lost($rfids,$year,$month);
				$data['presentedDays'] = hrOperation::getEmployeePresented_lost($rfids);
			}
			else
			{
				$data['details'] = hrOperation::getEmployeeImages($next_rfid,$year,$month);
				$data['presentedDays'] = hrOperation::getEmployeePresented($next_rfid);
			}
			$data['rfid'] = $next_rfid;
			$data['year'] = $year;
			$data['month'] = $month;
			$data['dep'] = $dep_id;
			$data['sub_dep'] = $sub_dep;
			
			$data['employees']=hrOperation::getRelatedEmployees($dep_id,$sub_dep);
			return View::make('hr.attendance.checking',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public static function approveNextEmployee($rfid=0,$year=0,$month=0,$type='next')
	{
		if(canView('hr_attendance'))
		{
			//get next/prevous employee
			$next_rfid = hrOperation::getNextEmployeeRfid($rfid,$type)->rfid;
			$data['next_rfid'] = hrOperation::getNextEmployeeRfid($next_rfid,'next');
			$data['previous_rfid'] = hrOperation::getNextEmployeeRfid($next_rfid,'previous');
			if($year == 0)
			{
				$year = date('Y');
			}
			if($month == 0)
			{
				$month = date('m');
			}
			$data['rfid'] = $next_rfid;
			$data['year'] = $year;
			$data['month'] = $month;
			$data['photo'] =  hrOperation::getPhotoByRfid($next_rfid);
			$data['details'] = hrOperation::absentEmployeeImages($next_rfid,$year,$month);
			return View::make('hr.attendance.approve_images',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public static function rejectImage()
	{
		if(canEdit('hr_attendance'))
		{
			$img_id = Input::get('img_id');
			$type = Input::get('type');
			$late = Input::get('late');
			if($late!='')
			{//it is late pic
				if(Input::get('late_status')!='Present')
				{//this late pic is not presented yet, make it present
					$data=array('late_status' => 0);//make it present
					$items = array(
						"RFID" 		=> Input::get('emp_id'),
						"date" 		=> Input::get('date'),
						"type"		=> $late,
						"created_at"=> date('Y-m-d H:i:s'),
						"created_by"=> Auth::user()->id
					);
					hrOperation::insertRecord('attendance_presents',$items);
				}
				else
				{//this late pic is presented, make it late again
					$data=array('late_status' => 1);//make it late again
					$items = array(
						"RFID" 		=> Input::get('emp_id'),
						"date" 		=> Input::get('date'),
						"type"		=> $late
					);
					hrOperation::delete_record('attendance_presents',$items);
				}
			}
			else
			{
				if($type=='')
				{//make it rejected
					$status = 1;
				}
				else
				{
					$status=0;
				}
				$data=array('status' => $status,'rejected_by'=>Auth::user()->id,'rejected_at'=>date('Y-m-d H:i:s'));
			}
			
			hrOperation::update_record('attendance_images',$data,array('id'=>$img_id));
		}
		else
		{
			return showWarning();
		}
	}
	public static function acceptImage()
	{
		if(canEdit('hr_attendance'))
		{
			$img_id = Input::get('img_id');
			$type = Input::get('type');
			if($type=='')
			{//the iamge is rejeted
				$status = 1;
			}
			else
			{
				$status=0;
			}
			$data=array('status' => $status,'rejected_by'=>Auth::user()->id,'rejected_at'=>date('Y-m-d H:i:s'));
			hrOperation::update_record('attendance_images',$data,array('id'=>$img_id));
		}
		else
		{
			return showWarning();
		}
	}
	public static function presentImage()
	{
		if(canEdit('hr_attendance'))
		{
			$date = Input::get('date');
			$type = Input::get('type');
			$present = Input::get('present');
			if($present!='Present')
			{//the iamge should be presented
			
				$items = array(
					"RFID" 		=> Input::get('emp_id'),
					"date" 		=> Input::get('date'),
					"type"		=> Input::get('type'),
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);
				hrOperation::insertRecord('attendance_presents',$items);
			}
			else
			{
				$items = array(
					"RFID" 		=> Input::get('emp_id'),
					"date" 		=> Input::get('date'),
					"type"		=> Input::get('type')
				);
				hrOperation::delete_record('attendance_presents',$items);
				hrOperation::insertLog('attendance_presents',2,'deletion of presented day of given employee',Input::get('emp_id'));
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function postLeaveDays()
	{
		if(canAttHoliday())
		{
			$items = array(
					"employee_id" 		=> Input::get('employee_id'),
					"paid_leaves" 		=> Input::get('paid'),
					"unpaid_leaves"		=> Input::get('unpaid'),
					"year"				=> Input::get('leave_year'),
					"month"				=> Input::get('leave_month'),
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);
			if(getEmployeeLeaveDays(Input::get('employee_id'),Input::get('leave_year'),Input::get('leave_month')))
			{
				hrOperation::delete_record('employee_leaves',array('employee_id'=>Input::get('employee_id'),'year'=>Input::get('leave_year'),'month'=>Input::get('leave_month')));
			}
			hrOperation::insertRecord('employee_leaves',$items);
		
			return \Redirect::route("getAttendance")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function postLeaves()
	{
		if(canAttHoliday())
		{
			//validate fields
			$validates = Validator::make(Input::all(), array(
				"from_date"						=> "required",
				"to_date"						=> "required"
			));
	
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->withErrors($validates)->withInput();
			}
			else
			{
				$sdate = Input::get("from_date");
					
				if($sdate != '')
				{
					$sdate = explode("-", $sdate);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				$edate = Input::get("to_date");
					
				if($edate != '')
				{
					$edate = explode("-", $edate);
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);		
				}
				$days = date_diff(date_create($sdate),date_create($edate));
				$noOfDays = (int)$days->format("%a")+1;
				if((hrOperation::getEmployeeLeaves_total(Input::get('type'),Input::get('employee_id'),$sy)+$noOfDays) > 10 && Input::get('type')==1)
				{
					return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","No more leaves for this type");
				}
				if((hrOperation::getEmployeeLeaves_total(Input::get('type'),Input::get('employee_id'),$sy)+$noOfDays) > 20 && Input::get('type')==2)
				{
					return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","No more leaves for this type");
				}
				if((hrOperation::getEmployeeLeaves_total(Input::get('type'),Input::get('employee_id'),$sy)+$noOfDays) > 20 && Input::get('type')==3)
				{
					return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","No more leaves for this type");
				}
				$items = array(
						"employee_id" 		=> Input::get('employee_id'),
						"type" 				=> Input::get('type'),
						"date_from"			=> $sdate,
						"date_to"			=> $edate,
						"days_no"			=> (int)$days->format("%a")+1,
						"desc"				=> Input::get('desc'),
						"created_at" 		=> date('Y-m-d H:i:s'),
						"created_by" 		=> Auth::user()->id
					);
				
				$id = hrOperation::insertRecord_id('leaves',$items);
				if(Input::hasFile('scan'))
				{
					// getting all of the post data
					$file = Input::file('scan');
					$errors = "";
					$file_data = array();
					// validating each file.
					$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
					$validator = Validator::make(
		
					  		[
					            'scan' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension()),
					        ],
					        [
					            'scan' => 'required|max:100000',
					            'extension'  => 'required|in:jpg,jpeg,bmp,png'
					        ]
					);
		
					if($validator->passes())
					{
					    // path is root/uploads
					    $destinationPath = 'documents/hr_attachments';
					    $filename = $file->getClientOriginalName();
		
					    $temp = explode(".", $filename);
					    $extension = end($temp);
					    
					    $filename = 'leave_'.Input::get('employee_id').'_'.$id.'.'.$extension;
		
					    $upload_success = $file->move($destinationPath, $filename);
		
					    if($upload_success) 
					    {
							DB::connection('hr')->table('leaves')->where('id',$id)->update(array('file_name'=>$filename));
						} 
						else 
						{
						   $errors .= json('error', 400);
						}
					} 
					else 
					{
					    // redirect back with errors.
					    return Redirect::back()->withErrors($validator);
					}
				}
				hrOperation::insertLog('leaves',0,'leave of given employee',$id);
				return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("success","Record Saved Successfully.");
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function updateLeaves()
	{
		if(canAttHoliday())
		{
			//validate fields
			$validates = Validator::make(Input::all(), array(
				"from_date"						=> "required",
				"to_date"						=> "required"
			));
	
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->withErrors($validates)->withInput();
			}
			else
			{
				$sdate = Input::get("from_date");
				$id = Input::get('id');
				if($sdate != '')
				{
					$sdate = explode("-", $sdate);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);		
				}
				$edate = Input::get("to_date");
					
				if($edate != '')
				{
					$edate = explode("-", $edate);
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);		
				}
				$days = date_diff(date_create($sdate),date_create($edate));
				$noDays = (int)$days->format("%a")+1;
				$currentDays = hrOperation::getLeaveDetails($id)->days_no;
				$noOfDays = $noDays - $currentDays;
				if((hrOperation::getEmployeeLeaves_total(Input::get('type'),Input::get('employee_id'),$sy)+$noOfDays) > 10 && Input::get('type')==1)
				{
					return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","No more leaves for this type");
				}
				if((hrOperation::getEmployeeLeaves_total(Input::get('type'),Input::get('employee_id'),$sy)+$noOfDays) > 20 && Input::get('type')==2)
				{
					return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","No more leaves for this type");
				}
				if((hrOperation::getEmployeeLeaves_total(Input::get('type'),Input::get('employee_id'),$sy)+$noOfDays) > 20 && Input::get('type')==3)
				{
					return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("fail","No more leaves for this type");
				}
				$items = array(
						"type" 				=> Input::get('type'),
						"date_from"			=> $sdate,
						"date_to"			=> $edate,
						"desc"				=> Input::get('desc'),
						"days_no"			=> (int)$days->format("%a")+1
					);
				
				hrOperation::update_record('leaves',$items,array('id'=>$id));
				if(Input::hasFile('scan'))
				{
					$file= public_path(). "/documents/hr_attachments/".Input::get('filename');
					File::delete($file);
					// getting all of the post data
					$file = Input::file('scan');
					$errors = "";
					$file_data = array();
					// validating each file.
					$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
					$validator = Validator::make(
		
					  		[
					            'scan' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension()),
					        ],
					        [
					            'scan' => 'required|max:100000',
					            'extension'  => 'required|in:jpg,jpeg,bmp,png'
					        ]
					);
		
					if($validator->passes())
					{
					    // path is root/uploads
					    $destinationPath = 'documents/hr_attachments';
					    $filename = $file->getClientOriginalName();
		
					    $temp = explode(".", $filename);
					    $extension = end($temp);
					    
					    $filename = 'leave_'.Input::get('employee_id').'_'.$id.'.'.$extension;
		
					    $upload_success = $file->move($destinationPath, $filename);
		
					    if($upload_success) 
					    {
							DB::connection('hr')->table('leaves')->where('id',$id)->update(array('file_name'=>$filename));
						} 
						else 
						{
						   $errors .= json('error', 400);
						}
					} 
					else 
					{
					    // redirect back with errors.
					    return Redirect::back()->withErrors($validator);
					}
				}
				hrOperation::insertLog('leaves',1,'leave of given employee',$id);
				return \Redirect::route("getEmployeeLeaves",array(Input::get('employee_id'),Input::get('year'),Input::get('month')))->with("success","Record Saved Successfully.");
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function postAttendanceTime()
	{
		if(canEdit('hr_attendance'))
		{
			hrOperation::delete_record('attendance_time',array('year'=>Input::get('time_year'),'month'=>Input::get('time_month')));
			$items = array(
					"time_in" 			=> Input::get('time_in'),
					"time_out" 			=> Input::get('time_out'),
					"year" 				=> Input::get('time_year'),
					"month" 			=> Input::get('time_month'),
					"thu"				=> 0,
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);
			
			hrOperation::insertRecord('attendance_time',$items);
			$items_thu = array(
					"time_in" 			=> Input::get('time_in_thu'),
					"time_out" 			=> Input::get('time_out_thu'),
					"year" 				=> Input::get('time_year'),
					"month" 			=> Input::get('time_month'),
					"thu"				=> 1,
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);
			
			hrOperation::insertRecord('attendance_time',$items_thu);
			hrOperation::insertLog('attendance_time',1,'attendance time updated',1);
		}
		else
		{
			return showWarning();
		}
	}
	public function postHolidays()
	{
		if(canEdit('hr_attendance'))
		{
			hrOperation::delete_record('month_holidays',array('year'=>Input::get('leave_year'),'month'=>Input::get('leave_month')));
			hrOperation::delete_holidays(Input::get('leave_year').Input::get('leave_month'));
			if(Input::get('h_number')>0)
			{
				$items = array(
						"days" 				=> Input::get('h_number'),
						"year" 				=> Input::get('leave_year'),
						"month" 			=> Input::get('leave_month'),
						"created_at" 		=> date('Y-m-d H:i:s'),
						"created_by" 		=> Auth::user()->id
					);
				
				$id = hrOperation::insertRecord_id('month_holidays',$items);
				$dates = array();
				for($i=1;$i<=Input::get('holiday_number');$i++)
				{
					if(Input::get('date_'.$i)!='')
					{
						$date = explode('-',Input::get('date_'.$i));
						$date = dateToMiladi($date[2],$date[1],$date[0]);
						$dates[] = array(
							"month_holidays_id" => $id,
							"code" 				=> Input::get('leave_year').Input::get('leave_month'),
							"date" 				=> $date,
							"desc" 				=> Input::get('desc_'.$i)
						);
					}
				}
				
				hrOperation::insertRecord('month_holiday_dates',$dates);
				hrOperation::insertLog('month_holidays',1,'month holidays time updated',1);
			}
			return Redirect::route('holidaysManager');
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmergencyHolidays()
	{
		if(canEdit('hr_attendance'))
		{
			hrOperation::delete_record('emergency_days',array('year'=>Input::get('emergency_year'),'month'=>Input::get('emergency_month')));
			hrOperation::delete_EmergencyHolidays(Input::get('emergency_year').Input::get('emergency_month'));
			if(Input::get('e_number')>0)
			{
				$items = array(
						"days" 				=> Input::get('e_number'),
						"year" 				=> Input::get('emergency_year'),
						"month" 			=> Input::get('emergency_month'),
						"created_at" 		=> date('Y-m-d H:i:s'),
						"created_by" 		=> Auth::user()->id
					);
				
				$id = hrOperation::insertRecord_id('emergency_days',$items);
				$dates = array();
				for($i=1;$i<=Input::get('emergency_number');$i++)
				{
					if(Input::get('edate_'.$i)!='')
					{
						$date = explode('-',Input::get('edate_'.$i));
						$date = dateToMiladi($date[2],$date[1],$date[0]);
						$dates[] = array(
							"emergency_id" => $id,
							"code" 				=> Input::get('emergency_year').Input::get('emergency_month'),
							"date" 				=> $date,
							"desc" 				=> Input::get('edesc_'.$i)
						);
					}
				}
				
				hrOperation::insertRecord('month_holiday_dates',$dates);
				hrOperation::insertLog('emergency_days',1,'inserted',1);
			}
			return Redirect::route('holidaysManager');
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeSalaryWait()
	{
		if(canEdit('hr_attendance'))
		{
			hrOperation::delete_record('employee_salary_waits',array('employee_id'=>Input::get('employee_id'),'year'=>Input::get('year'),'month'=>Input::get('month')));
			$items = array(
					"employee_id" 		=> Input::get('employee_id'),
					"year" 				=> Input::get('year'),
					"month" 			=> Input::get('month'),
					"desc" 				=> Input::get('desc'),
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);
			
			hrOperation::insertRecord('employee_salary_waits',$items);
			hrOperation::insertLog('employee_salary_waits',1,'salary wait of given employee',Input::get('employee_id'));
		}
		else
		{
			return showWarning();
		}	
	}
	public function insertImages()
	{
		if(Auth::user()->id == 113 || Auth::user()->id == 174)
		{
			$the_date = explode('-',Input::get('the_date'));
			$shamsi_date = Input::get('the_date');
			$the_date = dateToMiladi($the_date[2],$the_date[1],$the_date[0]);
			
			if($the_date>=date('Y-m-d'))
			{
				dd('The date should be smaller than today');
			}
			if(!hrOperation::checkInsertImagesDate($the_date))
			{
				dd('The Images are already inserted for this day');
			}
			$m_date = explode('-',$the_date);
			$year = $m_date[0];
			$month= $m_date[1];
			$day  = $m_date[2];
			//dd('not inserted');
			$result = "";
			// create a new cURL resource
			$ch = curl_init();
			// set URL and other appropriate options
			curl_setopt($ch, CURLOPT_URL, "10.134.45.19/images.php?year=$year&month=$month&day=$day");
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// grab URL and pass it to the browser
			$result = curl_exec($ch);
			$result = explode("\n",$result);
			$result = array_reverse($result);
			// close cURL resource, and free up system resources
			curl_close($ch);
			$first_data = array();
			$data = array();
			for($i=0;$i<count($result);$i++)
			{
				$pos = strpos($result[$i], '_');
				if ($pos !== false) 
				{//check if it has rfid
					$path = explode("\\",$result[$i]);
					if(count($path)==5)
					{//it should have 4 part:\machine\year\monthDay\time_rfid
						$time_rfid = explode('_',$path[4]);
						$rfid = $time_rfid[1];
						$rfid = explode('.',$rfid);
						$rfid = $rfid[0];
						$path_replace = str_replace('\\', '/', $result[$i]);
						/*
						if($time_rfid[0]<120000)
						{
							if(!in_array(array('rfid'=>$rfid,'day'=>$path[3],'timeout'=>0),$first_data))
							{
								$first_data[]=array('rfid'=>$rfid,'day'=>$path[3],'timeout'=>0);
								$data[] = array('RFID'=>$rfid,'path'=>$path_replace);
							}
						}
						else
						{
							if(!in_array(array('rfid'=>$rfid,'day'=>$path[3],'timein'=>0),$first_data))
							{
								$first_data[]=array('rfid'=>$rfid,'day'=>$path[3],'timein'=>0);
								$data[] = array('RFID'=>$rfid,'path'=>$path_replace);
							}
						}
						*/
						$data[] = array(
										'RFID'=>$rfid,
										'path'=>$path_replace,
										'time'=>$time_rfid[0],
										'year'=> $year,
										'month'=> $month,
										'day' => $day,
										'date'=> $the_date
									);
					}
				}
			}
			if(hrOperation::insertAttImages($data))
			{
				DB::statement("DELETE t
				    FROM hr.attendance_images AS t LEFT JOIN
				         (SELECT RFID, max( t1.time ) AS time
				          FROM hr.attendance_images AS t1
				          WHERE t1.year = $year AND t1.month = $month AND t1.day = $day AND t1.time < 120000
				          GROUP BY t1.RFID
				         ) keep
				         ON t.time = keep.time AND t.RFID = keep.RFID
				WHERE keep.time IS NULL AND
				      t.year = $year AND t.month = $month AND t.day = $day AND t.time < 120000;");
				      
				      DB::statement("DELETE t
				    FROM hr.attendance_images AS t LEFT JOIN
				         (SELECT RFID, max( t1.time ) AS time
				          FROM hr.attendance_images AS t1
				          WHERE t1.year = $year AND t1.month = $month AND t1.day = $day AND t1.time > 120000
				          GROUP BY t1.RFID
				         ) keep
				         ON t.time = keep.time AND t.RFID = keep.RFID
				WHERE keep.time IS NULL AND
				      t.year = $year AND t.month = $month AND t.day = $day AND t.time > 120000;");
				      
				
				return '<span style="color:red">The Images was successfuly inserted for the day:'.Input::get("the_date").'</span>';
	      		
			}
			else
			{
				dd('There was some problems, Images are Not Inserted!!!');
			}
		}
		else
		{
			return showWarning();
		}
	}
	//tashkils
	public function getTashkils($year=0)
	{
		if($year==0)
		{//if no year is send, then get the current shamsi year
			$year = date('Y')-621;
		}
		//check roles
		if(canView('hr_tashkil'))
		{
			$table = Datatable::table()
		  	->addColumn('id',
					'bast',
					'tainat',
					'title',
					'sub_dep',
					'department',
					'year',
					'actions')
		  	->setUrl(route('getTashkilData',$year))
		  	/*
		  	->setCallbacks(
			'fnRowCallback', 'function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			
			        var numStart = this.fnPagingInfo().iStart;
			
			        var index = numStart + iDisplayIndexFull + 1;
			        $("td:first", nRow).html(index);
			        return nRow;
			    }'
			)
			*/
		  	->noScript();
			//View::make('admin.users', array('table' => $table));
			return View::make('hr.tashkil.list',array('table' => $table,'year'=>$year));
		}
		else
		{
			return showWarning();
		}
	}
	public function getTashkilData($year=0)
	{	
		//get all data 
		$object = hrOperation::getTashkilData($year);//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
				->showColumns(
							'id',
							'bast',
							'tainat',
							'title',
							'sub_dep',
							'department',
							'year'
				)
				->addColumn('operations', function($option){
					$options = '';
					if(canEdit('hr_tashkil'))
					{
						$options.='
							<a href="javascript:void()" onclick="load_tashkil_det('.$option->id.')" class="table-link" style="text-decoration:none;" data-target="#update_tashkil" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
					}
					if(canDelete('hr_tashkil'))
					{
						$options.='<a href="javascript:void()" onclick="deleteTashkil('.$option->id.')" class="table-link" style="text-decoration:none;">
										<i class="icon fa-trash-o" style="color:red;" aria-hidden="true" style="font-size: 16px;"></i>
									</a>';
						
					}
					return $options;
				})
				->searchColumns(
							'id',
							'bast',
							'tainat',
							'title',
							'sub_dep',
							'department',
							'year'
				)
				// ->orderColumns('id','id')
				->make();
	}
	
	public function checkTashkilDuplicate()
	{
		$dep_id = Input::get('general_department');
		$sub_dep_id = Input::get('sub_dep');
		$tainat = Input::get('tainat');
		if(hrOperation::check_tashkil($dep_id,$sub_dep_id,$tainat))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function postNewBast()
	{
		if(canAdd('hr_tashkil'))
		{
			//validate fields
			$validates = Validator::make(Input::all(), array(
				"title"						=> "required",
				"general_department"		=> "required",
				"sub_dep"					=> "required",
				"tainat"					=> "required"
			));
			/*
			$validates->after(function($validates)
			{
			    if ($this->checkTashkilDuplicate())
			    {
			        $validates->errors()->add('field', 'duplicate entry');
			    }
			});
			*/
			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('getTashkils')->with("fail","Duplicate Entry!!!");
			}
			else
			{
				$filename='';
				if(Input::hasFile('scan'))
				{
					// getting all of the post data
					$file = Input::file('scan');
					$errors = "";
					$file_data = array();
					// validating each file.
					$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
					$validator = Validator::make(
		
					  		[
					            'scan' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension()),
					        ],
					        [
					            'scan' => 'required|max:100000',
					            'extension'  => 'required|in:jpg,jpeg,bmp,png'
					        ]
					);
		
					if($validator->passes())
					{
					    // path is root/uploads
					    $destinationPath = 'documents/hr_attachments';
					    $filename = $file->getClientOriginalName();
		
					    $temp = explode(".", $filename);
					    $extension = end($temp);
					    
					    $filename = 'job_description_'.$filename.'.'.$extension;
		
					    $upload_success = $file->move($destinationPath, $filename);
					} 
				}
				if(Input::get('employee_type')==2)
				{
					$bast = Input::get('ajir_bast');
				}
				elseif(Input::get('employee_type')==3)
				{
					$bast = Input::get('military_bast');
				}
				else
				{
					$bast = Input::get('emp_bast');
				}
				
				$data=array();
				$tainats = explode(',',Input::get('tainat'));
				
				for($i=0;$i<Input::get('bast_number');$i++)
				{
					$data[] = array(
						"dep_id" 				=> Input::get('general_department'),
						"sub_dep_id"			=> Input::get('sub_dep'),
						"bast"					=> $bast,
						"title"					=> Input::get('title'),
						"employee_type"			=> Input::get('employee_type'),
						"year"					=> Input::get('year'),
						"tainat"				=> $tainats[$i],
						"job_desc"				=> $filename,
						"created_at" 			=> date('Y-m-d H:i:s'),
						"created_by" 			=> Auth::user()->id
					);
				}
				hrOperation::insertRecord('tashkilat',$data);
				
				return \Redirect::route("getTashkils")->with("success","Record Saved Successfully.");
			}
		}
	}
	public function updateBast()
	{
		if(canAdd('hr_tashkil'))
		{
			//validate fields
			$validates = Validator::make(Input::all(), array(
				"title"						=> "required",
				"general_department"		=> "required",
				"sub_dep"					=> "required",
				"tainat"					=> "required"
			));

			//check the validation
			if($validates->fails())
			{	
				return Redirect::route('getTashkils')->with("fail","Duplicate Entry!!!");
			}
			else
			{
				$filename=Input::get('scan_file');
				if(Input::hasFile('scan'))
				{
					$file= public_path(). "/documents/hr_attachments/".Input::get('scan_file');
					File::delete($file);
					// getting all of the post data
					$file = Input::file('scan');
					$errors = "";
					$file_data = array();
					// validating each file.
					$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
					$validator = Validator::make(
		
					  		[
					            'scan' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension()),
					        ],
					        [
					            'scan' => 'required|max:100000',
					            'extension'  => 'required|in:jpg,jpeg,bmp,png'
					        ]
					);
		
					if($validator->passes())
					{
					  	
					    // path is root/uploads
					    $destinationPath = 'documents/hr_attachments';
					    $filename = $file->getClientOriginalName();
		
					    $temp = explode(".", $filename);
					    $extension = end($temp);
					    
					    $filename = 'job_description_'.Input::get('bast_id').'_'.$filename.'.'.$extension;
		
					    $upload_success = $file->move($destinationPath, $filename);
				    
					} 
		
				}
				if(Input::get('employee_type')==2)
				{
					$bast = Input::get('ajir_bast');
				}
				elseif(Input::get('employee_type')==3)
				{
					$bast = Input::get('military_bast');
				}
				else
				{
					$bast = Input::get('emp_bast');
				}
				
				
				$data = array(
					"dep_id" 				=> Input::get('general_department'),
					"sub_dep_id"			=> Input::get('sub_dep'),
					"bast"					=> $bast,
					"title"					=> Input::get('title'),
					"employee_type"			=> Input::get('employee_type'),
					"tainat"				=> Input::get('tainat'),
					"year"					=> Input::get('year'),
					"job_desc"				=> $filename,
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);
				
				hrOperation::update_record('tashkilat',$data,array('id'=>Input::get('bast_id')));
				hrOperation::insertLog('tashkilat',1,'tashkil updated',Input::get('bast_id'));
				return \Redirect::route("getTashkils")->with("success","Record Saved Successfully.");
			}
		}
	}
	public static function deleteTashkil()
	{
		if(canDelete('hr_tashkil'))
		{
			$id = Input::get('id');
			if(hrOperation::isTashkilInUse($id))
			{
				return;
			}
			else
			{
				hrOperation::delete_record('tashkilat',array('id'=>$id));
				hrOperation::insertLog('tashkilat',2,'tashkil deleted',$id);
			}
		}
		else
		{
			return showWarning();
		}
	}
	public static function deleteEmployeePromotion()
	{
		if(canDelete('hr_documents'))
		{
			$id = Input::get('id');
			hrOperation::delete_record('employee_promotions',array('id'=>$id));
			hrOperation::insertLog('employee_promotions',2,'promotion of given employee',$id);
		}
		else
		{
			return showWarning();
		}
	}
	public static function deleteEmployeeMakafat()
	{
		if(canDelete('hr_documents'))
		{
			$id = Input::get('id');
			hrOperation::delete_record('employee_makafats',array('id'=>$id));
			hrOperation::insertLog('employee_makafats',2,'makafat of given employee',$id);
		}
		else
		{
			return showWarning();
		}
	}
	public static function deleteEmployeePunish()
	{
		if(canDelete('hr_documents'))
		{
			$id = Input::get('id');
			hrOperation::delete_record('employee_punishments',array('id'=>$id));
			hrOperation::insertLog('employee_punishments',2,'punishment of given employee',$id);
		}
		else
		{
			return showWarning();
		}
	}
	public static function deleteEmployeeLeave()
	{
		if(canAttHoliday())
		{
			$id = Input::get('id');
			hrOperation::delete_record('leaves',array('id'=>$id));
			hrOperation::insertLog('leaves',2,'leave of given employee',$id);
		}
		else
		{
			return showWarning();
		}
	}
	public static function bringTashkilDetViaAjax()
	{
		$id = Input::get('id');
		$tashkil = hrOperation::getTashkilDetail($id);
		return json_encode(array('bast'=>$tashkil->bast_name,'number'=>$tashkil->tainat));
	}
	public function getCapacityBuilding()
	{
		//check roles
		if(canView('hr_capacity'))
		{
			$data['trainings'] = hrOperation::getCapacityTrainingsData();
			
			return View::make('hr.capacity.employeeList',$data);
		}
		else
		{
			return showWarning();
		}
	}
	//get form data list
	public function getEmployeeCapacityData()
	{		
		//get all data 
		$object = hrOperation::getData();//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name',
							'number_tayenat',
							'gender',
							'original_province',
							'bast',
							'rank',
							'emp_date'
							)
				->addColumn('education', function($option){
					$education = getEmployeeEducation($option->id);
					if($education)
					{
						return getEmployeeEducation($option->id)->name_dr;
					}
					else
					{
						return '';
					}
				})
				->addColumn('operations', function($option){
					$options = '';
					if(canView('hr_capacity'))
					{
						$options .= '
								<a href="'.route('getDetailsEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="نمایش">
									<i class="icon fa-eye" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
					}
					if(canAdd('hr_capacity'))
					{
						$options .= '
								<a href="javascript:void()" onclick="load_employee_trainings('.$option->id.')" class="table-link" style="text-decoration:none;" title="trainings" data-target="#employee_trainings" data-toggle="modal">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
								<a href="javascript:void()" onclick="$('.'\'#employee_id\''.').val('.$option->id.');" class="table-link" style="text-decoration:none;" title="Add training" data-target="#add_training" data-toggle="modal">
									<i class="icon fa-plus" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
								';
					}
					
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public static function loadCapacityTrainings()
	{
		if(canView('hr_capacity'))
		{
			$type = Input::get('type');
			if($type == 'trainings')
			{//trainings
				return View('hr.capacity.trainingsList');
			}
			else
			{//teachers
				return View('hr.capacity.trainersList');
			}
		}
		else
		{
			return showWarning();
		}
	}
	public function getCapacityTrainingsData()
	{		
		//get all data 
		$object = hrOperation::getCapacityTrainingsData();//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'title',
							'location',
							'type',
							'seat_no',
							'start_date',
							'end_date'
							)
				->addColumn('operations', function($option){
					$options = '';
					$options .= '
								<a href="javascript:void()" onclick="load_training('.$option->id.')" class="table-link" style="text-decoration:none;" title="Edit" data-target="#edit_training" data-toggle="modal">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
					
					
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public function postNewTraining()
	{
		if(canAdd('hr_capacity'))
		{
			$sdate = Input::get("start_date");
			$edate = Input::get("end_date");	
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate = null;
			}
			if($edate != '')
			{
				$edate = explode("-", $edate);
				$ey = $edate[2];
				$em = $edate[1];
				$ed = $edate[0];
				$edate = dateToMiladi($ey,$em,$ed);		
			}
			else
			{
				$edate=null;
			}
			if(Input::get('type')==0)
			{//internal
				$hokm_no = null;
				$external_type = null;
				$hdate = null;
			}
			else
			{
				$hdate = Input::get("hokm_date");	
				if($hdate != '')
				{
					$hdate = explode("-", $hdate);
					$hy = $hdate[2];
					$hm = $hdate[1];
					$hd = $hdate[0];
					$hdate = dateToMiladi($hy,$hm,$hd);		
				}
				else
				{
					$hdate=null;
				}
				$hokm_no = Input::get('hokm_no');
				$external_type = Input::get('external_type');
			}
			$items = array(
					"title" 			=> Input::get('title'),
					"location"			=> Input::get('location'),
					"type"				=> Input::get('type'),//exteranl or internal
					"start_date"		=> $sdate,
					"end_date"			=> $edate,
					"organizer"			=> Input::get('organizer'),
					"sponser"			=> Input::get('sponser'),
					"days_no"			=> Input::get('days_no'),
					"hokm_date"			=> $hdate,
					"hokm_no"			=> $hokm_no,
					"seat_no"			=> Input::get('seat_no'),
					"external_type"		=> $external_type,
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);
			
			$id = hrOperation::insertRecord_id('capacity_trainings',$items);
			if(Input::hasFile('scan'))
			{
				// getting all of the post data
				$file = Input::file('scan');
				$errors = "";
				$file_data = array();
				// validating each file.
				$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
				$validator = Validator::make(
	
				  		[
				            'scan' => $file,
				            'extension'  => Str::lower($file->getClientOriginalExtension()),
				        ],
				        [
				            'scan' => 'required|max:100000',
				            'extension'  => 'required|in:jpg,jpeg,bmp,png'
				        ]
				);
				if($validator->passes())
				{
				    // path is root/uploads
				    $destinationPath = 'documents/hr_attachments';
				    $filename = $file->getClientOriginalName();
	
				    $temp = explode(".", $filename);
				    $extension = end($temp);
				    
				    $filename = 'capacity_trainings_'.$id.'.'.$extension;
	
				    $upload_success = $file->move($destinationPath, $filename);
	
				    if($upload_success) 
				    {
						DB::connection('hr')->table('capacity_trainings')->where('id',$id)->update(array('file_name'=>$filename));
					} 
					else 
					{
					   $errors .= json('error', 400);
					}
				} 
				else 
				{
				    // redirect back with errors.
				    return Redirect::back()->withErrors($validator);
				}
			}
			return \Redirect::route("getCapacityBuilding")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function editTraining()
	{
		if(canEdit('hr_capacity'))
		{
			$sdate = Input::get("start_date");
			$edate = Input::get("end_date");	
			if($sdate != '')
			{
				$sdate = explode("-", $sdate);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		
			}
			else
			{
				$sdate=null;
			}
			if($edate != '')
			{
				$edate = explode("-", $edate);
				$ey = $edate[2];
				$em = $edate[1];
				$ed = $edate[0];
				$edate = dateToMiladi($ey,$em,$ed);		
			}
			else
			{
				$edate=null;
			}
			if(Input::get('type')==0)
			{//internal
				$hokm_no = null;
				$external_type = null;
				$hdate = null;
			}
			else
			{
				$hdate = Input::get("hokm_date");	
				if($hdate != '')
				{
					$hdate = explode("-", $hdate);
					$hy = $hdate[2];
					$hm = $hdate[1];
					$hd = $hdate[0];
					$hdate = dateToMiladi($hy,$hm,$hd);		
				}
				else
				{
					$hdate=null;
				}
				$hokm_no = Input::get('hokm_no');
				$external_type = Input::get('external_type');
			}
			$items = array(
					"title" 			=> Input::get('title'),
					"location"			=> Input::get('location'),
					"type"				=> Input::get('type'),//exteranl or internal
					"start_date"		=> $sdate,
					"end_date"			=> $edate,
					"organizer"			=> Input::get('organizer'),
					"sponser"			=> Input::get('sponser'),
					"days_no"			=> Input::get('days_no'),
					"hokm_date"			=> $hdate,
					"hokm_no"			=> $hokm_no,
					"seat_no"			=> Input::get('seat_no'),
					"external_type"		=> $external_type,
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);
			
			hrOperation::update_record('capacity_trainings',$items,array('id'=>Input::get('id')));
			$id = Input::get('id');
			if(Input::hasFile('scan'))
			{
				$fileName = hrOperation::getTraining(Input::get('id'))->file_name;
				$file= public_path(). "/documents/hr_attachments/".$fileName;
				if(File::delete($file))
				{
					//delete from database
					DB::connection('hr')
					->table('capacity_trainings')
					->where('id',Input::get('id'))
					->update(array('file_name'=>null));
					
				}
				// getting all of the post data
				$file = Input::file('scan');
				$errors = "";
				$file_data = array();
				// validating each file.
				$rules = array('scan' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
				$validator = Validator::make(
	
				  		[
				            'scan' => $file,
				            'extension'  => Str::lower($file->getClientOriginalExtension()),
				        ],
				        [
				            'scan' => 'required|max:100000',
				            'extension'  => 'required|in:jpg,jpeg,bmp,png'
				        ]
				);
	
				if($validator->passes())
				{
				  	
				    // path is root/uploads
				    $destinationPath = 'documents/hr_attachments';
				    $filename = $file->getClientOriginalName();
	
				    $temp = explode(".", $filename);
				    $extension = end($temp);
				    
				    $filename = 'capacity_trainings_'.$id.'.'.$extension;
	
				    $upload_success = $file->move($destinationPath, $filename);
	
				    if($upload_success) 
				    {
						DB::connection('hr')->table('capacity_trainings')->where('id',$id)->update(array('file_name'=>$filename));
					} 
					else 
					{
					   $errors .= json('error', 400);
					}
	
			    
				} 
				else 
				{
				    // redirect back with errors.
				    return Redirect::back()->withErrors($validator);
				}
	
			}
			return \Redirect::route("getCapacityBuilding")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function getCapacityTrainersData()
	{		
		//get all data 
		$object = hrOperation::getCapacityTrainersData();//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'last_name',
							'org',
							'especiality'
							)
				->addColumn('operations', function($option){
					$options = '';
					$options .= '
								<a href="javascript:void()" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>';
					
					
					return $options;
				})
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public function postNewTrainer()
	{
		if(canAdd('hr_capacity'))
		{
			$items = array(
					"name" 				=> Input::get('name'),
					"last_name" 		=> Input::get('last_name'),
					"org"				=> Input::get('org'),
					"especiality" 		=> Input::get('especiality'),
					"phone"				=> Input::get('phone'),
					"email"				=> Input::get('email'),
					"experience"		=> Input::get('experience'),
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);
			
			hrOperation::insertRecord('capacity_trainers',$items);
		
			return \Redirect::route("getCapacityBuilding")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function load_employee_trainings()
	{
		//get employee id
		$employee_id = Input::get('id');
		
		//get employee details
		$data['details'] = hrOperation::getEmployeeCapacityTrainings($employee_id);
		$data['id'] = $employee_id;
		return View('hr.capacity.employee_trainings',$data);
	}
	public function load_training()
	{
		//get employee id
		$id = Input::get('id');
		
		//get employee details
		$data['details'] = hrOperation::getTraining($id);
		$data['id'] = $id;
		return View('hr.capacity.edit_training',$data);
	}
	public function addTrainingToEmployee()
	{
		if(canAdd('hr_capacity'))
		{
			$items = array(
					"employee_id" 			=> Input::get('employee_id'),
					"training_id"			=> Input::get('training_id'),
					"created_at" 		=> date('Y-m-d H:i:s'),
					"created_by" 		=> Auth::user()->id
				);
			
			hrOperation::insertRecord('capacity_employee_trainings',$items);
		
			return \Redirect::route("getCapacityBuilding")->with("success","Record Saved Successfully.");
		}
		else
		{
			return showWarning();
		}
	}
	public function postEmployeeSalary($emp_id=0)
	{
		$main_salary = Input::get('main_salary');
		$salary = Input::get('salary');
		$prof_salary = Input::get('profesional_salary');
		$kadri_salary = Input::get('kadri_salary');
		if($main_salary==null)
		{
			$main_salary = 0;
			$retirment = 0;
		}
		else
		{
			$retirment = (Input::get('main_salary')*3)/100;
		}
		if($salary==null)
		{
			$salary = 0;
		}
		if($prof_salary==null)
		{
			$prof_salary = 0;
		}
		if($kadri_salary==null)
		{
			$kadri_salary = 0;
		}
		$items = array(
				"employee_id" 	=> $emp_id,
				"main_salary"	=> $main_salary,
				"extra_salary"	=> $salary,
				"prof_salary"	=> $prof_salary,
				"kadri_salary"	=> $kadri_salary,
				"grade"			=> Input::get('grade'),
				"retirment"		=> round($retirment),
				"created_at" 	=> date('Y-m-d H:i:s'),
				"created_by" 	=> Auth::user()->id
			);
		if(hrOperation::getEmployeeSalary($emp_id))
		{
			hrOperation::delete_record('employee_salary',array('employee_id'=>$emp_id));
		}
		hrOperation::insertRecord('employee_salary',$items);
		hrOperation::insertLog('employee_salary',1,'salary of given employee',$emp_id);
		return \Redirect::route("getDetailsEmployee",$emp_id)->with("success","Record Saved Successfully.");
	}
	public function loadEmployeePayroll()
	{
		//get employee id
		$employee_id = Input::get('id');
		$data['emp_id'] = $employee_id;
		
		return View('hr.attendance.employee_payroll',$data);
	}
	//bring related basts according to dep
	public function bringRelatedBastViaAjax()
	{
		$dep = Input::get('id');
		$operation = Input::get('type');
		$basts = hrOperation::getRelatedBasts($dep,$operation);
		$options = '<option value="">انتخاب</option>';
		foreach($basts AS $item)
		{
			$options.="<option value='".$item->id."'>".$item->name."-".$item->tainat."</option>";
		}
		
		return $options;
	}
	public function bringExistBastViaAjax()
	{
		$dep = Input::get('id');
		$vacant = Input::get('vacant');
		$basts = hrOperation::getExistBasts($dep,$vacant);
		$options = '<option value="">انتخاب</option>';
		foreach($basts AS $item)
		{
			$options.="<option value='".$item->id."'>".$item->name."-".$item->tainat."</option>";
		}
		
		return $options;
	}
	public function editPayroll()
	{
		//get employee id
		$employee_id = Input::get('id');
		$data['year']= Input::get('year');
		$data['month']= Input::get('month');
		$data['rfid']= Input::get('rfid');
		$data['dep_id']= Input::get('dep_id');
		$data['emp_id']= $employee_id;
		//get employee details
		$data['details'] = hrOperation::getEmployeePayroll_details($employee_id,Input::get('year'),Input::get('month'));
		$data['salaries']= hrOperation::getEmployeeSalary($employee_id);
		//$data['parentDeps'] = getDepartmentWhereIn();
		return View('hr.attendance.edit_payroll',$data);
	}
	public function getEmployeeLeaves($emp_id=0,$year,$month)
	{
		$data['id'] = $emp_id;
		$data['year'] = $year;
		$data['month'] = $month;
		//get employee details
		$data['details'] = hrOperation::getEmployeeLeaves($emp_id,$year,$month);
		
		return View('hr.attendance.edit_leaves',$data);
	}
	public function check_leave_validity()
	{
		$type = Input::get('type');
		$id = Input::get('id');
		$year = Input::get('year');
		//$date = dateToMiladi($year,01,01);//first of shamsi year
		if(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=10 && $type==1)
		{//zarori
			return 'true';
		}
		elseif(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=20 && $type==2)
		{//tafrihi
			return 'true';
		}
		elseif(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=20 && $type==3)
		{//sick
			return 'true';
		}
		elseif(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=45 && $type==6)
		{//haj
			return 'true';
		}
		elseif(hrOperation::getEmployeeLeaves_total($type,$id,$year)>=105 && $type==4)
		{//pregnancy
			return 'true';
		}
		else
		{
			return 'false';
		}
	}
	public function check_leave_validity_date()
	{
		$type = Input::get('type');
		$id = Input::get('id');
		$sdate = Input::get('date_from');
		$edate = Input::get('date_to');
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);		
		}
		else
		{
			$sdate=date("Y-m-d");
			$sy = 0;
		}
		if($edate != '')
		{
			$edate = explode("-", $edate);
			$ey = $edate[2];
			$em = $edate[1];
			$ed = $edate[0];
			$edate = dateToMiladi($ey,$em,$ed);		
		}
		else
		{
			$edate=date("Y-m-d");
		}
		//$date = dateToMiladi($sy,01,01);
		$current_leaves = hrOperation::getEmployeeLeaves_total($type,$id,$sy);
		$days = date_diff(date_create($sdate),date_create($edate));
		$requested_leaves = (int)$days->format("%a")+1;
		$total_leaves = $current_leaves+$requested_leaves;
		if($total_leaves>10 && $type==1)
		{//zarori
			$result = 'true';
		}
		elseif($total_leaves>20 && $type==2)
		{//tafrihi
			$result = 'true';
		}
		elseif($total_leaves>20 && $type==3)
		{//sick
			$result = 'true';
		}
		else
		{
			$result = 'false';
		}
		return json_encode(array('result'=>$result,'leaves'=>$current_leaves));
	}
	public function getEditLeaveForm()
	{
		$id = Input::get('id');
		$data['id'] = $id;
		$data['year'] = Input::get('year');
		$data['month']= Input::get('month');
		$data['details'] = hrOperation::getLeaveDetails($id);
		
		return View('hr.attendance.edit_leave',$data);
	}
	//download file from server
	public function downloadLeaveDoc($id=0)
	{
		if(canView('hr_attendance'))
		{
			//get file name from database
			$fileName = hrOperation::getLeaveDetails($id)->file_name;
	        //public path for file
	        $file= public_path(). "/documents/hr_attachments/".$fileName;
	        //download file
	        return Response::download($file, $fileName);
	    }
	    else
	    {
	    	return showWarning();
	    }
	}
	public function deleteLeaveFile()
	{
		if(canEdit('hr_attendance'))
		{
			$id = Input::get('doc_id');
			//get file name from database
			$fileName = hrOperation::getLeaveDetails($id)->file_name;
			$file= public_path(). "/documents/hr_attachments/".$fileName;
			if(File::delete($file))
			{
				//delete from database
				DB::connection('hr')
				->table('leaves')
				->where('id',$id)
				->update(array('file_name'=>null));
				
				return "<div class='alert alert-success'>File Deleted Successfully!</div>";
			}
			else
			{
				return "<div class='alert alert-danger'>Error!</div>";
			}
		}
		else
		{
			return showWarning();
		}
	}
	//test
	public function getAllAttendances($year,$month,$dep=0,$sub_dep=0)
	{
		$data['allImages'] = hrOperation::test($year,$month);
		//dd($data['allImages']['7877382_0105_1']);
		$data['allEmployees'] = hrOperation::getAllRFIDs($dep,$sub_dep);
		$data['year'] = $year;
		$data['month'] = $month;
		//echo count($data['allEmployees']);exit;
		//return View('hr.attendance.test',$data);
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month == 1)
		{
			$from = dateToMiladi($year-1,12,$sday);
		}
		else
		{
			$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
		}
		//$from = dateToMiladi($year,$month-1,15);//the start day of attendance month is 15 of prevouce month
		$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month
		$begin = new DateTime($from);
		$end = new DateTime($to);
		$interval = DateInterval::createFromDateString('1 day');
		$data['period'] = new DatePeriod($begin, $interval, $end);
		Excel::create('Att-'.$month, function($file) use($data){
			$file->getDefaultStyle()
		        ->getAlignment()
		        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$file->setTitle('Attendance');
		
			$file->sheet('Attendance', function($sheet) use($data){
				$sheet->setRightToLeft(true);
				$sheet->setStyle(array(
				    'font' => array(
				        'name'      => 'Calibri',
				        //'size'      =>  15,
				        //'bold'      =>  true
				    )
				));
				$emp_count = 1;
				$row = 1;
				$date_row = 2;
				$time_row = 3;
				$colIn = 'A';
				$colOut= 'B';
				$day_counter=0;
				foreach($data['allEmployees'] AS $emp)
				{
					$sheet->setCellValue('A'.$row.'',$emp_count);
					$sheet->mergeCells('B'.$row.':C'.$row);
					$sheet->setCellValue('B'.$row.'',$emp->name_dr);
					$sheet->mergeCells('D'.$row.':E'.$row);
					$sheet->setCellValue('D'.$row.'',$emp->father_name_dr);
					$sheet->mergeCells('F'.$row.':I'.$row);
					$sheet->setCellValue('F'.$row.'',$emp->current_position_dr);
					$sheet->mergeCells('J'.$row.':N'.$row);
					$sheet->setCellValue('J'.$row.'',$emp->name);
					$sheet->row($row, function($row) {
					    // call cell manipulation methods
					    $row->setBackground('#e0ebeb');
						$row->setAlignment('center');
						//$row->setValignment('middle');
						$row->setFontWeight('bold');
					});
					//$sheet->setOrientation('landscape');
					foreach($data['period'] AS $day)
					{
					 	$the_day = $day->format( "Y-m-d" );
					 	$day_code = date("w", strtotime($the_day));//1: monday
					 	$day_week = date("l", strtotime($the_day));
						$period_det = explode('-',$the_day);
						$period_day = $period_det[2];
						$period_month = $period_det[1];
						$p_year = $period_det[0];
						$shamsi_date = dateToShamsi($p_year,$period_month,$period_day);
						
						$period_monthDay = $period_month.$period_day;
						//$timein = '<span style="color:red">NA</span>';$timeout='<span style="color:red">NA</span>';
						$timein = '-';$timeout='-';
						if (array_key_exists($emp->RFID.'_'.$period_monthDay.'_1',$data['allImages']))
						{
							$img=$data['allImages'][$emp->RFID.'_'.$period_monthDay.'_1'];
							$path = explode("_",$img->path);
							$time = (int)substr($path[0],-6);//get the time part in path
							$month_day = substr($path[0],-11,4);//get the monthDay part in path
							
							if($time <= 120000)
							{
								$time=str_pad($time,6,'0',STR_PAD_LEFT);
								$time= preg_replace('/(\d{2})(\d{2})(\d{2})/', "$1:$2:$3", $time);
								$time=date("g:i a", strtotime($time));
								//$time = preg_replace('/(\d\d)(\d\d)\d\d/', '($1<12)?"$1:$2 AM":$1-12 . ":$2 PM"', $time);
								$timein= $time;
								
							}
							else
							{
								$time=str_pad($time,6,'0',STR_PAD_LEFT);
								$time= preg_replace('/(\d{2})(\d{2})(\d{2})/', "$1:$2:$3", $time);
								$time=date("g:i a", strtotime($time));
								$timeout = $time;
								
							}
						}
						if (array_key_exists($emp->RFID.'_'.$period_monthDay.'_2',$data['allImages']))
						{
							$img=$data['allImages'][$emp->RFID.'_'.$period_monthDay.'_2'];
							$path = explode("_",$img->path);
							$time = (int)substr($path[0],-6);//get the time part in path
							$month_day = substr($path[0],-11,4);//get the monthDay part in path
							if($time <= 120000)
							{
								$time=str_pad($time,6,'0',STR_PAD_LEFT);
								$time= preg_replace('/(\d{2})(\d{2})(\d{2})/', "$1:$2:$3", $time);
								$time=date("g:i a", strtotime($time));
								//$time = preg_replace('/(\d\d)(\d\d)\d\d/', '($1<12)?"$1:$2 AM":$1-12 . ":$2 PM"', $time);
								$timein= $time;
								
							}
							else
							{
								$time=str_pad($time,6,'0',STR_PAD_LEFT);
								$time= preg_replace('/(\d{2})(\d{2})(\d{2})/', "$1:$2:$3", $time);
								$time=date("g:i a", strtotime($time));
								$timeout = $time;
								
							}
						}
						//$sheet->mergeCells($colIn.$date_row.':'.$colOut.$date_row);
						//$sheet->setCellValue($colIn.$date_row,$shamsi_date.'('.$day_week.')');
						$sheet->setCellValue($colIn.$date_row,$shamsi_date);
						$sheet->setCellValue($colOut.$date_row,$day_week);
						$sheet->setCellValue($colIn.$time_row,$timein);
						$sheet->setCellValue($colOut.$time_row,$timeout);
						$day_counter++;
						$mod = $day_counter%7;
						if($mod == 0)
						{
							$colIn = 'A';
							$colOut= 'B';	
							
							$date_row = $date_row+2;
							$time_row = $time_row+2;
						}
						else
						{
							$colIn++;
							$colOut++;
							$colIn++;
							$colOut++;
						}
					}
					$day_counter = 0;
					$colIn = 'A';
					$colOut= 'B';
					$row = $row+11;
					$date_row = $date_row+3;
					$time_row = $time_row+3;
					$emp_count++;
				}
				$sheet->setBorder('A1:N'.$time_row.'', 'dotted');
    		});
		
		})->download('xlsx');
	}
	
	public function getTodayAttendances($dep=0,$sub=0,$status=0,$type=0)
	{
		if(canAttTodayAtt())
		{
			$data['dep'] = $dep;
			$data['sub_dep'] = $sub;
			$data['status'] = $status;
			$data['type'] = $type;
			
			$data['parentDeps'] = getDepartmentWhereIn();
			$data['sub_deps']=getRelatedSubDepartment($dep);
			//$data['employees'] = hrOperation::getRelatedEmployees(Input::get('dep_id'));
			return View::make('hr.attendance.todayAttendance',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getTodayAttData($dep_id=0,$sub=0,$status=0,$type=0)
	{	
		$date = dateToShamsi(date('Y'),date('m'),date('d'));
		$date = explode('-',$date);
		$year = $date[0];
		$month = $date[1];
		$object = hrOperation::getEmployeesTodayAtt($dep_id,$sub,$status,$type);//print_r($report);exit;
		$collection = new Collection($object);
		return Datatable::collection($collection)
						->showColumns(
							'id',
							'name',
							'father_name',
							'current_position_dr',
							'department',
							'status'
							)
				// ->orderColumns('id','id')
				->make();
	}	
	
	public function insertImages_today()
	{
		if(canAttTodayAtt())
		{
			$year = date('Y');
			$month= date('m');
			$day =  date('d');
			$result = "";
			// create a new cURL resource
			$ch = curl_init();
			
			// set URL and other appropriate options
			curl_setopt($ch, CURLOPT_URL, "10.134.45.19/images.php?year=$year&month=$month&day=$day");
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			// grab URL and pass it to the browser
			$result = curl_exec($ch);
			$result = explode("\n",$result);
			$result = array_reverse($result);
			// close cURL resource, and free up system resources
			curl_close($ch);
			$first_data = array();
			$data = array();
			for($i=0;$i<count($result);$i++)
			{
				$pos = strpos($result[$i], '_');
				if ($pos !== false) 
				{//check if it has rfid
					$path = explode("\\",$result[$i]);
					if(count($path)==5)
					{//it should have 4 part:\machine\year\monthDay\time_rfid
						//$year = $path[2];$monthDay=$path[3];
						$time_rfid = explode('_',$path[4]);
						$time = $time_rfid[0];
						$rfid = $time_rfid[1];
						$rfid = explode('.',$rfid);
						$rfid = $rfid[0];
						$path_replace = str_replace('\\', '/', $result[$i]);
						if(!in_array(array('rfid'=>$rfid),$first_data))
						{
							$first_data[]=array('rfid'=>$rfid);
							$data[] = array('RFID'=>$rfid,'path'=>$path_replace,'time'=>$time);
						}
						
						/*
						if($time_rfid[0]<120000)
						{
							if(!in_array(array('rfid'=>$rfid,'day'=>$path[3],'timeout'=>0),$first_data))
							{
								$first_data[]=array('rfid'=>$rfid,'day'=>$path[3],'timeout'=>0);
								$data[] = array('RFID'=>$rfid,'path'=>$path_replace);
							}
						}
						*/
					}
				}
			}
			//dd(count($data));
			DB::connection('hr')->table('attendance_images_today')->truncate();
			hrOperation::insertRecord('attendance_images_today',$data);
			hrOperation::insertLog('attendance_images_today',1,'todays attendance',0);
			
			return 'done';
		}
		else
		{
			return showWarning();
		}
	}
	public function postPayroll(request $request)
	{
		hrOperation::delete_record('employee_payrolls',array('employee_id'=>$request['employee_id'],'year'=>$request['year'],'month'=>$request['month']));
		$data = array(
			'employee_id'		=> $request['employee_id'],
			'year'				=> $request['year'],
			'month'				=> $request['month'],
			'makolat'			=> $request['makolat'],
			'car_rent'			=> $request['car_rent'],
			'prr'				=> $request['prr'],
			'khatar'			=> $request['khatar'],
			'other_benifit'		=> $request['other_benifit'],
			'robh'				=> $request['robh'],
			'tazmin'			=> $request['tazmin'],
			'makolat_ksor'		=> $request['makolat_ksor'],
			'bank'				=> $request['bank'],
			'returns'			=> $request['returns'],
			'other_ksor'		=> $request['other_ksor'],
			"created_at" 		=> date('Y-m-d H:i:s'),
			"created_by" 		=> Auth::user()->id
			);
		hrOperation::insertRecord('employee_payrolls',$data);
		
		return \Redirect::route("getPayrollList",array($request['dep_id'],$request['year'],$request['month']))->with("success","Record Saved Successfully.");
	}
	public function checkInsertImagesDate(request $request)
	{
		$the_date = explode('-',$request['date']);
		$the_date = dateToMiladi($the_date[2],$the_date[1],$the_date[0]);
		if($the_date>=date('Y-m-d'))
		{
			return '<span style="color:red">The date should be smaller than today</span>';
		}
		if(hrOperation::checkInsertImagesDate($the_date))
		{
			return '';
		}
		else
		{
			return '<span style="color:red">The Images are already inserted for this day</span>';
		}
		
	}
	public function searchTashkil()
	{
		//check roles
		if(canView('hr_tashkil'))
		{
			$data['parentDeps'] = getDepartmentWhereIn();
			return View::make('hr.tashkil.search',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getTashkilSearch(request $request)
	{
		$data['parentDeps'] = getDepartmentWhereIn();
		$data['sub_deps']=getRelatedSubDepartment($request['general_department']);
		$data['type'] = $request['type'];
		$data['year'] = $request['year'];
		$data['dep_id'] = $request['general_department'];
		if($request['sub_dep']!='')
		{
			$data['sub_dep_id'] = $request['sub_dep'];
		}
		else
		{
			$data['sub_dep_id'] = 0;
		}
		if($request['employee_type']==2)
		{
			$data['bast'] = $request['ajir_bast'];
		}
		elseif($request['employee_type']==3)
		{
			$data['bast'] = $request['military_bast'];
		}
		else
		{
			$data['bast'] = $request['emp_bast'];
		}
		if($request['title']=='')
		{
			$data['title']	= 0;
		}
		else
		{
			$data['title']	= $request['title'];
		}
		
		$data['employee_type'] = $request['employee_type'];
		
		return View::make('hr.tashkil.searchResult',$data);
	}
	public function getTashkilSearchData($dep=0,$sub_dep=0,$type=2,$bast=0,$title=0,$emp_type=0,$year=0)
	{	
		//get all data 
		$object = hrOperation::getTashkilSearch($dep,$sub_dep,$type,$bast,$title,$emp_type,$year);//print_r($report);exit;
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'bast',
							'tainat',
							'title',
							'sub_dep',
							'department',
							'year'
							)
						->addColumn('operations', function($option){
							$options = '';
							if(canEdit('hr_tashkil'))
							{
								$options.='
									<a href="javascript:void()" onclick="load_tashkil_det('.$option->id.')" class="table-link" style="text-decoration:none;" data-target="#update_tashkil" data-toggle="modal">
										<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
									</a>';
							}
							if(canDelete('hr_tashkil'))
							{
								if($option->status==0)
								{
									$options.='<a href="javascript:void()" onclick="deleteTashkil('.$option->id.')" class="table-link" style="text-decoration:none;">
												<i class="icon fa-trash-o" style="color:red;" aria-hidden="true" style="font-size: 16px;"></i>
											</a>';
								}
								else
								{
									$options.='<i class="icon fa-trash-o" aria-hidden="true" style="font-size: 16px;" title="This Tashkil is Used!"></i>';
								}
								
							}
							return $options;
						})
				->make();
		return $result;
	}
	public function getRecruitmentSearch(request $request)
	{
		$data['parentDeps'] = getDepartmentWhereIn();
		$data['sub_deps']=getRelatedSubDepartment($request['general_department']);
		
		$data['dep_id'] = $request['general_department'];
		if($request['sub_dep'])
		{
			$data['sub_dep_id'] = $request['sub_dep'];
		}
		else
		{
			$data['sub_dep_id'] = 0;
		}
		if($request['employee_type']==2)
		{
			$data['bast'] = $request['ajir_bast'];
		}
		elseif($request['employee_type']==3)
		{
			$data['bast'] = $request['military_bast'];
		}
		else
		{
			$data['bast'] = $request['emp_bast'];
		}
		if($request['docs'])
		{
			$data['docs'] = 1;
		}
		else
		{
			$data['docs'] = 0;
		}
		$data['employee_type'] = $request['employee_type'];
		$data['typee'] = $request['type'];
		$data['gender'] = $request['gender'];
		$data['education'] = $request['education_degree'];
		$data['birth_year'] = $request['birth_year'];
		$data['full_name'] = $request['name'];
		$data['active'] = 'search';
		
		$table = Datatable::table()
		  	->addColumn(
		  			'id',
					'fullname',
					'father_name',
					'number_tayenat',
					'current_position_dr',
					'department',
					'bast',
					'emp_date',
					'phone',
					'current_status',
					'action'
					)
		  	->setUrl(route('getRecruitmentSearchData',array(
		  								'dep'=>$request['general_department'],
		  								'sub_dep'=>$request['sub_dep'],
		  							    'bast'=>$data['bast'],
		  								'type'=>$request['employee_type'],
		  							    'doc'=>$data['docs'],
		  								'typee'=>$request['type'],
		  								'gender'=>$request['gender'],
		  								'edu'=>$request['education_degree'],
		  								'year'=>$request['birth_year'],
		  								'name'=>$request['name']
		  					)))
		  	/*
		  	->setCallbacks(
			'fnRowCallback', 'function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			
			        var numStart = this.fnPagingInfo().iStart;
			
			        var index = numStart + iDisplayIndexFull + 1;
			        $("td:first", nRow).html(index);
			        return nRow;
			    }'
			)
			*/
		  	->noScript();
			//View::make('admin.users', array('table' => $table));
			$data['table']=$table;
			//return View::make('hr.recruitment.list',array('table' => $table,'year'=>$year));
		return View::make('hr.recruitment.list',$data);
	}
	public function getRecruitmentSearchData($dep=0,$sub_dep=0,$bast=0,$type=0,$doc=0,$typee=0,$gender=0,$edu=0,$year=0,$name='')
	{
		$user_deps = getHRUserDeps();
		if($user_deps)
		{
			$user_deps = explode(',',$user_deps->deps);
			//$user_deps = $user_deps->deps;
		}
		else
		{
			$user_deps = 0;
		}
		//get all data 
		/*
		$dep=Input::get('dep');
		$sub_dep=Input::get('sub_dep');
		$bast=Input::get('bast');
		$type=Input::get('type');
		$doc=Input::get('doc');
		$typee=Input::get('typee');
		$gender=Input::get('dender');
		$edu=Input::get('edu');
		$year=Input::get('year');
		$name=Input::get('name');
		*/
		
		$object = hrOperation::getRecruitmentSearchData($user_deps,$dep,$sub_dep,$bast,$type,$doc,$typee,$gender,$edu,$year,$name);//print_r($report);exit;
		//dd(count($object));
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name',
							'number_tayenat',
							'current_position_dr',
							'department',
							'bast',
							'emp_date',
							'phone'
							)
				->addColumn('status', function($option){
					$status = '';
					if($option->changed==1)
					{
						$status = 'تبدیلی';
					}
					elseif($option->fired==1)
					{
						$status = 'منفکی';
					}
					elseif($option->resigned==1)
					{
						$status = 'استعفا';
					}
					elseif($option->retired==1)
					{
						$status = 'تقاعد';
					}
					elseif($option->position_dr==4)
					{
						$status = 'انتظاربه معاش';
					}
					elseif($option->tashkil_id==0)
					{
						$status = 'اضافه بست';
					}
					else
					{
						$status = 'کارمند برحال';
					}
					return $status;
				})
				->addColumn('operations', function($option){
					$options = '';
					if(canEdit('hr_recruitment') || canEdit('hr_documents'))
					{
						$options .= '<a target="_blank" href="'.route('getDetailsEmployee',$option->id).'" class="table-link" style="text-decoration:none;" title="Edit">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
					}
					return $options;
				})
				->searchColumns(
							'id',
							'fullname',
							'father_name',
							'number_tayenat',
							'current_position_dr',
							'department',
							'bast',
							'emp_date'
				)
				// ->orderColumns('id','id')
				->make();
		return $result;
	}
	public function printEmployees($dep=0,$sub_dep=0,$bast=0,$type=0,$doc=0,$typee=0,$gender=0,$edu=0,$year=0,$name='')
	{
		$data['employees'] = hrOperation::getEmployeesForPrint($dep,$sub_dep,$bast,$type,$doc,$typee,$gender,$edu,$year,$name);
		
		Excel::create('employees', function($file) use($data){
			
			$file->setTitle('Employees');
		
			$file->sheet('Employees', function($sheet) use($data){
				$sheet->setCellValue('A2','#');
				$sheet->setCellValue('B2','نام کامل');
				$sheet->setCellValue('C2','نام پدر');
				$sheet->setCellValue('D2','درجه تحصیل');
				$sheet->setCellValue('E2','رشته');
				$sheet->setCellValue('F2','سال تولد');
				$sheet->setCellValue('G2','اول تقرر');
				$sheet->setCellValue('H2','رتبه');
				$sheet->setCellValue('I2','بست');
				$sheet->setCellValue('J2','درجه اجیر');
				$sheet->setCellValue('K2','بست اجیر');
				$sheet->setCellValue('L2','وظیفه فعلی');
				$sheet->setCellValue('M2','موقف');
				$sheet->setCellValue('N2','کارکنان');
				$sheet->setCellValue('O2','اداره مربوطه');
				$sheet->setCellValue('P2','وضعیت فعلی');
				$sheet->setCellValue('Q2','خدمت');
				
				$row = 3;
				$day_counter = 1;
				foreach($data['employees'] AS $emp)
				{
					$emp_type = '';
					if($emp->employee_type==1){$emp_type = 'مامور';}
					if($emp->employee_type==2){$emp_type = 'اجیر';}
					if($emp->employee_type==3){$emp_type = 'نظامی';}
					$emp_mawqif = '';
					if($emp->mawqif_employee==1){$emp_mawqif = 'برحال';}
					if($emp->mawqif_employee==2){$emp_mawqif = 'انتظاربه معاش';}
					if($emp->mawqif_employee==3){$emp_mawqif = 'بالمقطع';}
					if($emp->mawqif_employee==4){$emp_mawqif = 'درجریان تحصیل';}
	
					$sheet->setCellValue('A'.$row.'',$day_counter);
					$sheet->setCellValue('B'.$row.'',$emp->fullname);
					$sheet->setCellValue('C'.$row.'',$emp->father_name);
					$sheet->setCellValue('D'.$row.'',$emp->edu_degree);
					$sheet->setCellValue('E'.$row.'',$emp->education_field);
					$sheet->setCellValue('F'.$row.'',$emp->birth_year);
					$sheet->setCellValue('G'.$row.'',$emp->emp_date);
					if($emp->employee_type==2)
					{
						$sheet->setCellValue('H'.$row.'','');
						$sheet->setCellValue('I'.$row.'','');
						$sheet->setCellValue('J'.$row.'',$emp->rank);
						$sheet->setCellValue('K'.$row.'',$emp->bast);
					}
					else
					{
						$sheet->setCellValue('H'.$row.'',$emp->rank);
						$sheet->setCellValue('I'.$row.'',$emp->bast);
						$sheet->setCellValue('J'.$row.'','');
						$sheet->setCellValue('K'.$row.'','');
					}
					$sheet->setCellValue('L'.$row.'',$emp->current_position_dr);
					$sheet->setCellValue('M'.$row.'',$emp_mawqif);
					$sheet->setCellValue('N'.$row.'',$emp_type);
					$sheet->setCellValue('O'.$row.'',$emp->department);
					$status = '';
					if($emp->changed==1)
					{
						$status = 'تبدیلی';
					}
					elseif($emp->fired==1)
					{
						$status = 'منفکی';
					}
					elseif($emp->resigned==1)
					{
						$status = 'استعفا';
					}
					elseif($emp->retired==1)
					{
						$status = 'تقاعد';
					}
					elseif($emp->position_dr==4)
					{
						$status = 'انتظاربه معاش';
					}
					elseif($emp->tashkil_id==0)
					{
						$status = 'اضافه بست';
					}
					else
					{
						$status = 'کارمند برحال';
					}
					$sheet->setCellValue('P'.$row.'',$status);
					$khedmat = employeeKhedmat($emp->id);
					$kh_days = 0;
					if($khedmat)
					{
						foreach($khedmat AS $kh)
						{
							$date1 = time();
							$date2 = time();
							if($kh->date_from!=null)
							{
								$date1=strtotime($kh->date_from);
							}
							if($kh->date_to!=null)
							{
								$date2=strtotime($kh->date_to);
							}
							$diff=$date2-$date1;
							$kh_days = $kh_days + floor($diff/(60*60*24));
						}
						
					}
					if($kh_days>=365)
					{
						$year = (int)($kh_days/365);
						if($kh_days%365>=30)
						{
							$month = (int)(($kh_days%365)/30);
							$day = ($kh_days%365)%30;
						}
						else
						{
							$month = 0;
							$day = $kh_days%365;
						}
					}
					else
					{
						$year = 0;
						$month= (int)($kh_days/30);
						$day = $kh_days%30;
					}
					
					$sheet->setCellValue('Q'.$row.'',$year.'Y-'.$month.'M-'.$day.'D');
					
					$row++;$day_counter++;
				}
    		});
		})->download('xlsx');
	}
	public function bringYearTime()
	{
		$year = Input::get('year');
		$month= Input::get('month');
		$time = getAttendanceTime($year,$month);
		$time_thu = getAttendanceTime_thu($year,$month);
		$time_in = '';
		$time_out = '';
		$time_in_thu = '';
		$time_out_thu = '';
		if($time)
		{
			$time_in = $time->time_in;
			$time_out = $time->time_out;
		}
		if($time_thu)
		{
			$time_in_thu = $time_thu->time_in;
			$time_out_thu = $time_thu->time_out;
		}
		return json_encode(array('time_in'=>$time_in,'time_out'=>$time_out,
								'time_in_thu'=>$time_in_thu,'time_out_thu'=>$time_out_thu
							)
						);
	}
	public function bringMonthHolidays()
	{
		$data['year'] = Input::get('year');
		$data['month']= Input::get('month');
		$days = hrOperation::getMonthHolidayDays(Input::get('year'),Input::get('month'));
		if($days)
		{
			$data['days'] = $days->days;
		}
		else
		{
			$data['days'] = 0;
		}
		$data['details'] = hrOperation::getMonthHolidays(Input::get('year'),Input::get('month'));
		return json_encode(array('view'=>View::make('hr.attendance.holidays_edit',$data)->render(),'days'=>$data['days']));
		//return View::make('hr.attendance.holidays_edit',$data);
	}
	public function bringMonthEmergencyHolidays()
	{
		$data['year'] = Input::get('year');
		$data['month']= Input::get('month');
		$days = hrOperation::getMonthEmergencyDays(Input::get('year'),Input::get('month'));
		if($days)
		{
			$data['days'] = $days->days;
		}
		else
		{
			$data['days'] = 0;
		}
		$data['details'] = hrOperation::getMonthEmergencyHolidays(Input::get('year'),Input::get('month'));
		return json_encode(array('view'=>View::make('hr.attendance.emergency_edit',$data)->render(),'days'=>$data['days']));
		//return View::make('hr.attendance.holidays_edit',$data);
	}
	public function insert_cards()
	{
		$object = hrOperation::get_Cards();
		$items = array();
		foreach($object AS $emp)
		{
			$emp_id = hrOperation::check_fname($emp->name,$emp->fname);
			if($emp_id)
			{
					$items[] = array(
					"card_no" 				=> $emp->card_no,
					"employee_id"			=> $emp_id->id,
					"card_color"			=> $emp->card_color,
					"job_location"			=> $emp->job_location,
					"created_at" 			=> date('Y-m-d H:i:s'),
					"created_by" 			=> Auth::user()->id
				);
			}
		}
		hrOperation::insertRecord('employee_cards',$items);
		echo 'done';
	}
	public function saveEmployeeNewTashkil(request $request)
	{
		$object = hrOperation::find($request['id']);
		$employee_rank = hrOperation::getEmployeeRank($object->employee_type,$request['id']);
		$employee_bast = hrOperation::getEmployeeTashkil($request['tashkil']);
		$date_from = dateToMiladi($request->date);
		$data = array(
						'employee_id' 	=> $request['id'],
						'organization'	=> hrOperation::getDepName($request["sub_dep"])->name,
						'position'		=> $request['position'],
						'bast'			=> $employee_bast->bast,
						'leave_reason'	=> '',
						'rank'			=> $employee_rank->rank,
						'date_from'		=> $date_from,
						//'date_to'		=> date('Y-m-d'),
						'type'		    => 1,
						'internal'		=> 1,
						'created_at' 	=> date('Y-m-d H:i:s'),
						'created_by' 	=> Auth::user()->id
					);
					
		hrOperation::insertRecord('employee_experiences',$data);
		//update the status of current tashkil id from used to unused because it is gonna be updated
	//	hrOperation::update_record('tashkilat',array('status'=>0),array('id'=>$object->tashkil_id));
		$emp_bast = hrOperation::getEmployeeBast($request['tashkil']);
		$object->number_tayenat 	= $request["no"];
		$object->general_department = $request["dep"];
		$object->department			= $request["sub_dep"];
		$object->tashkil_id			= $request['tashkil'];
		$object->current_position_dr= $request['position'];
		$object->emp_bast			= $emp_bast;
		if($object->save())
		{
			hrOperation::insertLog('employees',1,'new tashkil for employee',$request['id']);
			
			if($request['tashkil'])
			{//update the bast id to used
				hrOperation::update_record('tashkilat',array('status'=>1),array('id'=>$request['tashkil']));
			}
		}
	}
	public function printDailyAtt($dep=0,$sub_dep=0,$status,$type=0)
	{
		$data['records'] = hrOperation::getEmployeesTodayAtt($dep,$sub_dep,$status,$type);
		
		Excel::create('TodayAtt', function($file) use($data){
			
			$file->setTitle('Attendance');
		
			$file->sheet('Attendance', function($sheet) use($data){
				$sheet->setRightToLeft(true);
				$sheet->setCellValue('A1','#');
				$sheet->setCellValue('B1','اسم');
				$sheet->setCellValue('C1','ولد');
				$sheet->setCellValue('D1','دیپارتمنت');
				$sheet->setCellValue('E1','وظیفه');
				$sheet->setCellValue('F1','وضعیت');
				$sheet->row(1, function($row) {
				    // call cell manipulation methods
				    $row->setBackground('#e0ebeb');
					$row->setAlignment('center');
					//$row->setValignment('middle');
					$row->setFontWeight('bold');
				});
				$row = 2;
				$counter = 1;
				foreach($data['records'] AS $emp)
				{
					$sheet->setCellValue('A'.$row,$counter);
					$sheet->setCellValue('B'.$row,$emp->name);
					$sheet->setCellValue('C'.$row,$emp->father_name);
					$sheet->setCellValue('D'.$row,$emp->department);
					$sheet->setCellValue('E'.$row,$emp->current_position_dr);
					$sheet->setCellValue('F'.$row,$emp->status);
					$row++;
					$counter++;
				}
				$sheet->setBorder('A1:E'.$row.'', 'dotted');
    		});
		
		})->download('xlsx');
	}
	public function getMoreEmergencyDate()
	{
		$total = Input::get('total');
		if(Input::get('type')=='e')
		{
			$result = '
				<div class="col-sm-12" id="e_'.$total.'">
					<div class="col-sm-4">
	                	<div class="col-sm-12">
	                		<label class="col-sm-12 ">تاریخ</label>
	                		<input class="form-control datepicker_farsi" readonly type="text" name="edate_'.$total.'">
	                	</div>
	                </div>
	                <div class="col-sm-6">
	                	<div class="col-sm-12">
	                		<label class="col-sm-12 ">توضیحات</label>
	                		<input class="form-control" type="text" name="edesc_'.$total.'">
	                	</div>
	                </div>
	                <div class="col-sm-2">
	                	<div class="col-sm-12">
	                		<label class="col-sm-12 ">&nbsp;</label>
	                		<input class="btn btn-danger" type="button" value=" - " onclick="remove_emergency_date('.$total.')">
	                	</div>
	                </div>
	            </div>';
		}
		else
		{
			$result = '<div class="col-sm-12" id="h_'.$total.'">
						<div class="col-sm-4">
		                	<div class="col-sm-12">
		                		<label class="col-sm-12 ">تاریخ</label>
		                		<input class="form-control datepicker_farsi" readonly type="text" name="date_'.$total.'">
		                	</div>
		                </div>
		                <div class="col-sm-6">
		                	<div class="col-sm-12">
		                		<label class="col-sm-12 ">توضیحات</label>
		                		<input class="form-control" type="text" name="desc_'.$total.'">
		                	</div>
		                </div>
		                <div class="col-sm-2">
		                	<div class="col-sm-12">
		                		<label class="col-sm-12 ">&nbsp;</label>
		                		<input class="btn btn-danger" type="button" value=" - " onclick="remove_holiday_date('.$total.')">
		                	</div>
		                </div>
		            </div>';
		}
		return $result;
	}
	public function sendAbsent_emails()
	{
		$the_date = date('Y-m-d');
		$dayOfWeek = date('D',strtotime($the_date));
  		$holiday = checkHoliday($the_date);
		$urgents = checkUrgentHoliday($the_date);
  		if($dayOfWeek=='Fri' || $holiday || $urgents)
  		{//if friday or holiday or urgent dont send email
  			return;
  		}
  		else
  		{
			$shamsi_date = dateToShamsi(date('Y'),date('m'),date('d'));
			$result = hrOperation::get_todays_absent();
			//dd($result);
			$directorates=array();
			$executives=array();
			$dirs_emails =array(33=>'bismellah.haidary@gmail.com',
								180=>'najiaanwari1@gmail.com',
								23=>'whedayet@aop.gov.af',
								264=>'rahim.stanikzai@aop.gov.af',
								269=>'akbar_sarwari@hotmail.com',
								41=>'miagulwaseeq@gmail.com',
								28=>'def.sec@aop.gov.af',
								45=>'fegarzada@hotmail.com',
								58=>'Liaqat.Khaleeq@aop.gov.af',
								54=>'shakib.oaa@gmail.com',
								//226=>'malikzay@gmail.com',
								176=>'wais.rahimi@aop.gov.af',
								174=>'s.hafiz.fayaz@gmail.com',
								79=>'sadaqatullah.sadiq@aop.gov.af',
								57=>'shafiq.ibrahimi@aop.gov.af',
								6=>'victoria.ghauri@aop.gov.af',
								175=>'zia.abdulrahimzai@gmail.com',
								25=>'h.chakhansuri@gmail.com',
								89=>'asadullah.salehi@aop.gov.af',
								53=>'ahmadrashed.t@gmail.com',
								39=>'feridun.ilham@aop.gov.af',
								59=>'abdullah.mohammadi@aop.gov.af',
								76=>'a.attash@aop.gov.af',
								55=>'subhan.raouf@aop.gov.af',
								56=>'m.kochai@aop.gov.af',
								268=>'zxklmy@gmail.com'
								);
			$second_emails =array(49=>'sultan_ahmadzai@hotmail.com',
								  264=>'wali.ahmadzai@aop.gov.af',
								  44=>'mrahim.sharifi@gmail.com',
								  45=>'maruf.hamkar@aop.gov.af',
								  76=>'nasim.sidiqi@yahoo.com',
								  79=>'h.saborqayoumi@gmail.com',
								  77=>'nawidullah.amin2016@gmail.com',
								  33=>'hamedwally4@gmail.com',
								  53=>'mansoornaebkhil@yahoo.com',
								  268=>'yazdanpanah518@gmail.com',
								  89=>'sideqi2000@gmail.com',
								  39=>'eid.abdulrahimzai@gmail.com',
								  41=>'hamida.hewad@gmail.com',
								  175=>'niazmohammad.nori@gmail.com',
								  174=>'Abdullatif.tashqorghani@gmail.com',
								  55=>'rahim.karim@aop.gov.af',
								  56=>'akram.jamshidi@aop.gov.af',
								  54=>'marzia_naderi@yahoo.com',
								  57=>'wazhma.nasiri@aop.gov.af',
								  58=>'Liaqat.Khaleeq@aop.gov.af',
								  59=>'najia.yousofi@gmail.com',
								  271=>'behzad.sabeti@aop.gov.af',
								  272=>'maryam.hakimi@aop.gov.af',
								  61=>'s.nejat.npa@gmail.com',
								  6=>'jamshid.barakzai@aop.gov.af',
								  233=>'hamidyousafi@gmail.com'
								  
								);
			$dep_id=0;
			$sh = explode('-',$shamsi_date);
			$sh_year = $sh[0];
			$sh_month = $sh[1];
			foreach($result as $rec)
			{
				$isInLeave = isEmployeeInLeave($rec->id,$the_date,$sh_year,$sh_month);
				if($isInLeave)
				{
					continue;
				}
				else
				{
					if (array_key_exists($rec->department,$dirs_emails))
					{
						$directorates[$rec->department]['email']=$dirs_emails[$rec->department];
					}
					else
					{
						$directorates[$rec->department]['email']='no_email';
					}
					$directorates[$rec->department][] = array('name'=>$rec->name,'last_name'=>$rec->last_name,'dep'=>$rec->department);
					//emails to excutive manager
					if (array_key_exists($rec->department,$second_emails))
					{
						$executives[$rec->department]['email']=$second_emails[$rec->department];
					}
					else
					{
						$executives[$rec->department]['email']='no_email';
					}
					$executives[$rec->department][] = array('name'=>$rec->name,'last_name'=>$rec->last_name,'dep'=>$rec->department);
					$email = explode('@',$rec->email);
					if($rec->email!='' && count($email)==2)
					{
						$user_email = array('email'=>$rec->email,'name'=>$rec->name,'last_name'=>$rec->last_name,'the_date'=>$shamsi_date);
						Mail::send('emails.absent', $user_email, function($message) use ($user_email){
								    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
									$message->to($user_email['email'])->subject('Absent Alert');
								});
					}
					else
					{
						continue;
					}
				}
			}
			if(count($directorates)>0)
			{
				foreach($directorates as $array)
				{//loop for every direcotrate
					if($array['email']=='no_email')
					{
						continue;
					}
					else
					{
						$user_email = array('email'=>$array['email'],'emps'=>$array,'the_date'=>$shamsi_date);
						Mail::send('emails.dir_absent', $user_email, function($message) use ($user_email){
							    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
								$message->to($user_email['email'])->subject('Absent Alert');
								//$message->cc($user_email['email']);
							});
					}
				}
			}
			if(count($executives)>0)
			{
				foreach($executives as $executive)
				{//loop for every direcotrate
					if($executive['email']=='no_email')
					{
						continue;
					}
					else
					{
						$user_email = array('email'=>$executive['email'],'emps'=>$executive,'the_date'=>$shamsi_date);
						Mail::send('emails.dir_absent', $user_email, function($message) use ($user_email){
							    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
								$message->to($user_email['email'])->subject('Absent Alert');
								//$message->cc($user_email['email']);
							});
					}
				}
			}
  		}
	}
	public function sendAfternoon_emails()
	{
		$the_date = explode('-',Input::get('the_date'));
		$shamsi_date = Input::get('the_date');
		$the_date = dateToMiladi($the_date[2],$the_date[1],$the_date[0]);
		$dayOfWeek = date('D',strtotime($the_date));
  		$holiday = checkHoliday($the_date);
		$urgents = checkUrgentHoliday($the_date);
  		if($dayOfWeek=='Fri' || $holiday || $urgents)
  		{//if friday or holiday or urgent dont send email
  			return;
  		}
  		else
  		{
			$result = hrOperation::get_yesterdays_absent($the_date);
			//dd($result);
			$directorates=array();
			$executives=array();
			$dirs_emails =array(33=>'bismellah.haidary@gmail.com',
							180=>'najiaanwari1@gmail.com',
							23=>'whedayet@aop.gov.af',
							264=>'rahim.stanikzai@aop.gov.af',
							269=>'akbar_sarwari@hotmail.com',
							41=>'miagulwaseeq@gmail.com',
							28=>'def.sec@aop.gov.af',
							45=>'fegarzada@hotmail.com',
							58=>'Liaqat.Khaleeq@aop.gov.af',
							54=>'shakib.oaa@gmail.com',
							//226=>'malikzay@gmail.com',
							176=>'wais.rahimi@aop.gov.af',
							174=>'s.hafiz.fayaz@gmail.com',
							79=>'sadaqatullah.sadiq@aop.gov.af',
							57=>'shafiq.ibrahimi@aop.gov.af',
							6=>'victoria.ghauri@aop.gov.af',
							175=>'zia.abdulrahimzai@gmail.com',
							25=>'h.chakhansuri@gmail.com',
							89=>'asadullah.salehi@aop.gov.af',
							53=>'ahmadrashed.t@gmail.com',
							39=>'feridun.ilham@aop.gov.af',
							59=>'abdullah.mohammadi@aop.gov.af',
							76=>'a.attash@aop.gov.af',
							55=>'subhan.raouf@aop.gov.af',
							56=>'m.kochai@aop.gov.af',
							268=>'zxklmy@gmail.com'
							);
			$second_emails =array(49=>'sultan_ahmadzai@hotmail.com',
					  264=>'wali.ahmadzai@aop.gov.af',
					  44=>'mrahim.sharifi@gmail.com',
					  45=>'maruf.hamkar@aop.gov.af',
					  76=>'nasim.sidiqi@yahoo.com',
					  79=>'h.saborqayoumi@gmail.com',
					  77=>'nawidullah.amin2016@gmail.com',
					  33=>'hamedwally4@gmail.com',
					  53=>'mansoornaebkhil@yahoo.com',
					  268=>'yazdanpanah518@gmail.com',
					  89=>'sideqi2000@gmail.com',
					  39=>'eid.abdulrahimzai@gmail.com',
					  41=>'hamida.hewad@gmail.com',
					  175=>'niazmohammad.nori@gmail.com',
					  174=>'Abdullatif.tashqorghani@gmail.com',
					  55=>'rahim.karim@aop.gov.af',
					  56=>'akram.jamshidi@aop.gov.af',
					  54=>'marzia_naderi@yahoo.com',
					  57=>'wazhma.nasiri@aop.gov.af',
					  58=>'Liaqat.Khaleeq@aop.gov.af',
					  59=>'najia.yousofi@gmail.com',
					  271=>'behzad.sabeti@aop.gov.af',
					  272=>'maryam.hakimi@aop.gov.af',
					  61=>'s.nejat.npa@gmail.com',
					  6=>'jamshid.barakzai@aop.gov.af',
					  233=>'hamidyousafi@gmail.com'
					  
					);
				foreach($result as $rec)
				{
					//is_in_thu_shift($rfid,$year,$month,0);
					$isInLeave = isEmployeeInLeave_today($rec->id,$the_date);
					if($isInLeave)
					{
						continue;
					}
					else
					{
						if (array_key_exists($rec->department,$dirs_emails))
						{
							$directorates[$rec->department]['email']=$dirs_emails[$rec->department];
						}
						else
						{
							$directorates[$rec->department]['email']='no_email';
						}
						$directorates[$rec->department][] = array('name'=>$rec->name,'last_name'=>$rec->last_name,'dep'=>$rec->department);
						//emails to excutive manager
						if (array_key_exists($rec->department,$second_emails))
						{
							$executives[$rec->department]['email']=$second_emails[$rec->department];
						}
						else
						{
							$executives[$rec->department]['email']='no_email';
						}
						$executives[$rec->department][] = array('name'=>$rec->name,'last_name'=>$rec->last_name,'dep'=>$rec->department);
				
						$email = explode('@',$rec->email);
						if($rec->email!='' && count($email)==2)
						{
							$user_email = array('email'=>$rec->email,'name'=>$rec->name,'last_name'=>$rec->last_name,'the_date'=>$shamsi_date);
							Mail::send('emails.absent_afternoon', $user_email, function($message) use ($user_email){
									    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
										$message->to($user_email['email'])->subject('Absent Alert');
									});
						}
						else
						{
							continue;
						}
					}
			}
			if(count($directorates)>0)
			{
				foreach($directorates as $array)
				{//loop for every direcotrate
					if($array['email']=='no_email')
					{
						continue;
					}
					else
					{
						$user_email = array('email'=>$array['email'],'emps'=>$array,'the_date'=>$shamsi_date);
						Mail::send('emails.dir_absent_afternoon', $user_email, function($message) use ($user_email){
							    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
								$message->to($user_email['email'])->subject('Absent Alert');
								//$message->cc($user_email['email']);
							});
					}
				}
			}
			if(count($executives)>0)
			{
				foreach($executives as $executive)
				{//loop for every direcotrate
					if($executive['email']=='no_email')
					{
						continue;
					}
					else
					{
						$user_email = array('email'=>$executive['email'],'emps'=>$executive,'the_date'=>$shamsi_date);
						Mail::send('emails.dir_absent_afternoon', $user_email, function($message) use ($user_email){
							    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
								$message->to($user_email['email'])->subject('Absent Alert');
								//$message->cc($user_email['email']);
							});
					}
				}
			}
  		}
	}
	public function sendAbsent_emails_old()
	{
		$year = date('Y');
		$month= date('m');
		$day =  date('d');
		$shamsi = dateToShamsi($year,$month,$day);
		$shamsi_month = explode('-',$shamsi);
		$s_month = $shamsi_month[1];
		$s_year = $shamsi_month[0];
		$att_time_in = getAttTime($s_year,$s_month,'in');
		
		$result = hrOperation::get_todays_absent_old($att_time_in);
		
		$directorates=array();
		$dirs_emails =array(33=>'bismellah.haidary@gmail.com',
							180=>'najiaanwari1@gmail.com',
							23=>'whedayet@arg.gov.af',
							78=>'rahim.stanikzai@aop.gov.af',
							41=>'miagulwaseeq@gmail.com',
							28=>'def.sec@aop.gov.af',
							45=>'fegarzada@hotmail.com',
							58=>'Liaqat.Khaleeq@aop.gov.af',
							54=>'shakib.oaa@gmail.com',
							226=>'malikzay@gmail.com',
							176=>'wais.rahimi@aop.gov.af',
							174=>'s.hafiz.fayaz@gmail.com'
							);
		$dep_id=0;
		foreach($result as $rec)
		{
			$time_h = substr($rec->time, 0, -4);
			$time_m = substr($rec->time, 2, -2);
			$time_s = substr($rec->time, 4, 2);
			$time = $time_h.':'.$time_m.':'.$time_s;
			if (array_key_exists($rec->department,$dirs_emails))
			{
				$directorates[$rec->department]['email']=$dirs_emails[$rec->department];
			}
			else
			{
				$directorates[$rec->department]['email']='no_email';
			}
			$directorates[$rec->department][] = array('name'=>$rec->name,'last_name'=>$rec->last_name,'time'=>$time,'dep'=>$rec->department);
			
			$user_email = array('email'=>$rec->email,'name'=>$rec->name,'last_name'=>$rec->last_name,'time'=>$time);
			Mail::send('emails.absent_old', $user_email, function($message) use ($user_email){
					    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
						$message->to($user_email['email'])->subject('Absent Alert');
					});
		}
		if(count($directorates)>0)
		{
			foreach($directorates as $array)
			{//loop for every direcotrate
				if($array['email']=='no_email')
				{
					continue;
				}
				else
				{
					$user_email = array('email'=>$array['email'],'emps'=>$array);
					Mail::send('emails.dir_absent_old', $user_email, function($message) use ($user_email){
						    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
							$message->to($user_email['email'])->subject('Absent Alert');
							//$message->cc($user_email['email']);
						});
				}
			}
		}
	}
	public function attendance_chart()
	{
		$data['parentDeps'] = getDepartmentWhereIn();
		return View::make('hr.attendance.charts',$data);
	}
	
	public function generate_att_chart()
	{
		//validate fields
		$validates = Validator::make(Input::all(), array(
			"from_date"		=> "required",
			"to_date"		=> "required"
		));
		
		//check the validation
		if($validates->fails())
		{	
			return Redirect::route('attendance_chart')->withErrors($validates)->withInput();
		}
		else
		{
			$data['s_date'] = Input::get('from_date');
			$data['e_date'] = Input::get('to_date');
			$data['gen_dep'] = Input::get('general_department');
			$data['sub_dep'] = Input::get('sub_dep');
			$data['type'] = Input::get('type');
			
			$sdate = explode('-',Input::get('from_date'));
			$edate = explode('-',Input::get('to_date'));
			$month = $sdate[1];$year = $sdate[2];
			$att_time_in = getAttTime($year,$month,'in');
			$att_time_out = getAttTime($year,$month,'out');
			$att_time_in_thu = getAttTime_thu($year,$month,'in');
			$att_time_out_thu = getAttTime_thu($year,$month,'out');
			
			$from = dateToMiladi($sdate[2],$sdate[1],$sdate[0]);
			$to = dateToMiladi($edate[2],$edate[1],$edate[0]);
			if($to<=$from)
			{
				$validates->errors()->add('to_date', 'To Date must be bigger than From Date');
				return Redirect::route('attendance_chart')->withErrors($validates)->withInput();
			}
			$begin = new DateTime($from);
			$end = new DateTime($to);
			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);
			//create the all monthDay values for where: /2015/1216|/2015/1217
			$days = array();$day_counter=0;
			$thu_days = array();
			$allHolidays = hrOperation::getAllHolidays($from,$to);
			$new_array=array();
			foreach($allHolidays as $array)
			{
			    foreach($array as $val)
			    {
			        array_push($new_array, $val);
			    }    
			}
			if(Input::get('type')==0)
			{//chart by percentage
				foreach($period As $day)
				{
					$the_day = $day->format( "Y-m-d" );
					$period_det = explode('-',$the_day);
					$period_day = $period_det[2];
					$period_month = $period_det[1];
					$period_year = $period_det[0];
					$day_code = date("w", strtotime($the_day));
					//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
					//$urgents = checkUrgentHoliday($the_day);
					$holiday = in_array($the_day, $new_array);
					if($holiday || $day_code == 5)
					{
						//$dayTotal++;//calculate the day urgent,leave or fridays as present
						continue;
					}
					else
					{
						$day_counter++;//to calculate the everage
						if($day_code != 4)//dont include thu here as the time differs
						{
							//$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
							$days[] = $period_year.'-'.$period_month.'-'.$period_day;
						}
						else
						{//thursdays
							//$thu_days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
							$thu_days[] = $period_year.'-'.$period_month.'-'.$period_day;
						}
					}
				}
				
				if(Input::get('sub_dep')!='')
				{//only one directorate
					$totalEmp_dep = hrOperation::getEmployeeDep(Input::get('sub_dep'));//number of employees who signes the att
					if($totalEmp_dep)
					{
						if($days!=null)
						{
							$total =DB::connection('hr')
								->table('attendance_images')
								->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
								->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
								//->whereIn('e.department', [41,49,174,175])
								->where('e.department',Input::get('sub_dep'))
								->where('status',0)//present
								->whereRaw('CASE 
											WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
								//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
								//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
								->whereIn('date',$days)//check the year/monthDay in image path
								->groupBy('groupByMe')
								->groupBy('attendance_images.RFID')
								->having('total', '>', 1)
								->get();
								//->toSql();dd($total);
						}
						if($thu_days!=null)
						{
							$thu_total=DB::connection('hr')
								->table('attendance_images')
								->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
								->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
								->where('e.department',Input::get('sub_dep'))
								->where('status',0)//present
								->whereRaw('CASE 
											WHEN time <= 120000 THEN time<='.$att_time_in_thu.' ELSE time>='.$att_time_out_thu.' END')
								//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
								//->whereRaw('path REGEXP "'.$thu_days.'"')//check the year/monthDay in image path
								->whereIn('date',$thu_days)
								->groupBy('groupByMe')
								->groupBy('attendance_images.RFID')
								->having('total', '>', 1)
								->get();
								//->toSql();dd($total);
						}
						
						$result = 0 ;
						if(isset($total))
						{
							$result+=count($total);
						}
						if(isset($thu_total))
						{
							$result+=count($thu_total);
						}
						//$presents=$result+$thuTotal+$dayTotal;
						$presents = $result/$day_counter;//everage
						$present_perc = (100*$presents)/$totalEmp_dep->total;//percentage
						$data['present_totals'] = $present_perc;
						$data['absent_totals'] = 100-$present_perc;
					}
					else
					{//directorate does not have any attendance employee
						$data['present_totals'] = 0;
						$data['absent_totals'] = 100;
					}
					$direc=hrOperation::get_dep_name(Input::get('sub_dep'))->name;
					$direc=str_replace(",","-",$direc);
					$data['display'] = "'".$direc."',";
					
				}
				elseif(Input::get('sub_dep')=='' && Input::get('general_department')!='')
				{//all directorates of selected deputy
					$dirs = getRelatedSubDepartment(Input::get('general_department'));
					$present_totals= '';
					$absent_totals= '';
					$directorates='';
					foreach($dirs as $dir)
					{
						$totalEmp_dep = hrOperation::getEmployeeDep($dir->id);
						if($totalEmp_dep)
						{
							if($days!=null)
							{
								$total =DB::connection('hr')
									->table('attendance_images')
									->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
									->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
									//->whereIn('e.department', [41,49,174,175])
									->where('e.department',$dir->id)
									->where('status',0)//present
									->whereRaw('CASE 
												WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
									//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
									//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
									->whereIn('date',$days)
									->groupBy('groupByMe')
									->groupBy('attendance_images.RFID')
									->having('total', '>', 1)
									->get();
									//->toSql();dd($total);
							}
							if($thu_days!=null)
							{
								$thu_total=DB::connection('hr')
									->table('attendance_images')
									->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
									->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
									->where('e.department',$dir->id)
									->where('status',0)//present
									->whereRaw('CASE 
												WHEN time <= 120000 THEN time<='.$att_time_in_thu.' ELSE time>='.$att_time_out_thu.' END')
									//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
									//->whereRaw('path REGEXP "'.$thu_days.'"')//check the year/monthDay in image path
									->whereIn('date',$thu_days)
									->groupBy('groupByMe')
									->groupBy('attendance_images.RFID')
									->having('total', '>', 1)
									->get();
									//->toSql();dd($total);
							}
							
							$result = 0 ;
							if(isset($total))
							{
								$result+=count($total);
							}
							if(isset($thu_total))
							{
								$result+=count($thu_total);
							}
							//$presents=$result+$thuTotal+$dayTotal;
							$presents = $result/$day_counter;
							$present_perc = (100*$presents)/$totalEmp_dep->total;
							$present_totals.= $present_perc.',';
							$absent_totals.= 100-$present_perc.',';
							
						}
						else
						{
							$present_totals.= '0,';
							$absent_totals.= '100,';
						}
						$direc=hrOperation::get_dep_name($dir->id)->name;
						$direc=str_replace(",","-",$direc);
						$directorates.= "'".$direc."',";
					}
					$directorates = substr($directorates, 0, -1);
					$present_totals = substr($present_totals, 0, -1);
					$absent_totals = substr($absent_totals, 0, -1);
					$data['display'] = $directorates;
					$data['present_totals'] = $present_totals;
					$data['absent_totals'] = $absent_totals;
				}
				elseif(Input::get('general_department')=='')
				{//all deputies
					$dirs = getDepartmentWhereIn();
					$present_totals= '';
					$absent_totals= '';
					$directorates='';
					foreach($dirs as $dir)
					{
						$totalEmp_dep = hrOperation::getEmployeeGen_Dep($dir->id);
						if($totalEmp_dep)
						{
							if($days!=null)
							{
								$total =DB::connection('hr')
									->table('attendance_images')
									->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
									->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
									//->whereIn('e.department', [41,49,174,175])
									->where('e.general_department',$dir->id)
									->where('status',0)//present
									->whereRaw('CASE 
												WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
									//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
									//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
									->whereIn('date',$days)
									->groupBy('groupByMe')
									->groupBy('attendance_images.RFID')
									->having('total', '>', 1)
									->get();
									//->toSql();dd($total);
							}
							if($thu_days!=null)
							{
								$thu_total=DB::connection('hr')
									->table('attendance_images')
									->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
									->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
									->where('e.general_department',$dir->id)
									->where('status',0)//present
									->whereRaw('CASE 
												WHEN time <= 120000 THEN time<='.$att_time_in_thu.' ELSE time>='.$att_time_out_thu.' END')
									//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
									//->whereRaw('path REGEXP "'.$thu_days.'"')//check the year/monthDay in image path
									->whereIn('date',$thu_days)
									->groupBy('groupByMe')
									->groupBy('attendance_images.RFID')
									->having('total', '>', 1)
									->get();
									//->toSql();dd($total);
							}
							
							$result = 0 ;
							if(isset($total))
							{
								$result+=count($total);
							}
							if(isset($thu_total))
							{
								$result+=count($thu_total);
							}
							//$presents=$result+$thuTotal+$dayTotal;
							$presents = $result/$day_counter;
							//$totalEmp_dep = hrOperation::getEmployeeDep($dir->id);
							//dd($totalEmp_dep[0]->total);
							$present_perc = (100*$presents)/$totalEmp_dep->total;
							$present_totals.= $present_perc.',';
							$absent_totals.= 100-$present_perc.',';
						}
						else
						{
							$present_totals.= '0,';
							$absent_totals.= '100,';
						}
						$direc=hrOperation::get_dep_name($dir->id)->name;
						$direc=str_replace(",","-",$direc);
						$directorates.= "'".$direc."',";
					}
					$directorates = substr($directorates, 0, -1);
					$present_totals = substr($present_totals, 0, -1);
					$absent_totals = substr($absent_totals, 0, -1);
					$data['display'] = $directorates;
					$data['present_totals'] = $present_totals;
					$data['absent_totals'] = $absent_totals;
				}
				//dd($directorates);
				
				return View::make('hr.attendance.generateCharts_percentage',$data);
			}
			else
			{//charts by number
				if(Input::get('sub_dep')!='')
				{//only one directorate
					$present_totals= '';
					$absent_totals= '';
					$directorates='';
					$totalEmp_dep = hrOperation::getEmployeeDep(Input::get('sub_dep'));//number of employees who signes the att
					foreach($period As $day)
					{
					    $total=0;$thu_total=0;
						$the_day = $day->format( "Y-m-d" );
						$period_det = explode('-',$the_day);
						$period_day = $period_det[2];
						$period_month = $period_det[1];
						$period_year = $period_det[0];
						$day_code = date("w", strtotime($the_day));
						//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
						//$urgents = checkUrgentHoliday($the_day);
						$holiday = in_array($the_day, $new_array);
						if($holiday || $day_code == 5)
						{
							//$dayTotal++;//calculate the day urgent,leave or fridays as present
							continue;
						}
						else
						{//normal days
							if($totalEmp_dep)
							{
								if($day_code != 4)//dont include thu here as the time differs
								{
									$total =DB::connection('hr')
										->table('attendance_images')
										->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
										->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
										//->whereIn('e.department', [41,49,174,175])
										->where('e.department',Input::get('sub_dep'))
										->where('status',0)//present
										->whereRaw('CASE 
													WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
										//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
										//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
										->where('date',$the_day)//check the year/monthDay in image path
										->groupBy('groupByMe')
										->groupBy('attendance_images.RFID')
										->having('total', '>', 1)
										->get();
										//->toSql();dd($total);
								}
								else
								{//thu
									$thu_total=DB::connection('hr')
										->table('attendance_images')
										->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
										->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
										->where('e.department',Input::get('sub_dep'))
										->where('status',0)//present
										->whereRaw('CASE 
													WHEN time <= 120000 THEN time<='.$att_time_in_thu.' ELSE time>='.$att_time_out_thu.' END')
										//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
										//->whereRaw('path REGEXP "'.$thu_days.'"')//check the year/monthDay in image path
										->where('date',$the_day)
										->groupBy('groupByMe')
										->groupBy('attendance_images.RFID')
										->having('total', '>', 1)
										->get();
										//->toSql();dd($total);
								}
								
								$result = 0 ;
								if($total!=0)
								{
									$result+=count($total);
								}
								if($thu_total!=0)
								{
									$result+=count($thu_total);
								}
								//$presents=$result;
								//$presents = $result/$day_counter;//everage
								//$present_perc = (100*$presents)/$totalEmp_dep->total;//percentage
								$present_totals.= $result.',';
								$absent_totals.= $totalEmp_dep->total-$result.',';
							}
							else
							{//directorate does not have any attendance employee
								$present_totals.= '0,';
								$absent_totals.= '0,';
							}
							$s_date = explode('-', $the_day);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);
							$directorates.= "'".$sdate."',";
						}
					}
					$data['direc']=hrOperation::get_dep_name(Input::get('sub_dep'))->name;
					$data['total']=$totalEmp_dep;
					$directorates = substr($directorates, 0, -1);
					$present_totals = substr($present_totals, 0, -1);
					$absent_totals = substr($absent_totals, 0, -1);
					$data['display'] = $directorates;
					$data['present_totals'] = $present_totals;
					$data['absent_totals'] = $absent_totals;
					
				}
				elseif(Input::get('sub_dep')=='' && Input::get('general_department')!='')
				{//all directorates of selected deputy
					$dirs = getRelatedSubDepartment(Input::get('general_department'));
					$all_dirs_data = array();
					foreach($dirs as $dir)
					{
					    $present_totals= '';
						$absent_totals= '';
						$directorates='';//dates
						$totalEmp_dep = hrOperation::getEmployeeDep($dir->id);//number of employees who signes the att
						foreach($period As $day)
						{
						    $total=0;$thu_total=0;
							$the_day = $day->format( "Y-m-d" );
							$period_det = explode('-',$the_day);
							$period_day = $period_det[2];
							$period_month = $period_det[1];
							$period_year = $period_det[0];
							$day_code = date("w", strtotime($the_day));
							//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
							//$urgents = checkUrgentHoliday($the_day);
							$holiday = in_array($the_day, $new_array);
							if($holiday || $day_code == 5)
							{
								//$dayTotal++;//calculate the day urgent,leave or fridays as present
								continue;
							}
							else
							{//normal days
								if($totalEmp_dep)
								{
									if($day_code != 4)//dont include thu here as the time differs
									{
										$total =DB::connection('hr')
											->table('attendance_images')
											->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
											->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
											//->whereIn('e.department', [41,49,174,175])
											->where('e.department',$dir->id)
											->where('status',0)//present
											->whereRaw('CASE 
														WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
											//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
											//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
											->where('date',$the_day)//check the year/monthDay in image path
											->groupBy('groupByMe')
											->groupBy('attendance_images.RFID')
											->having('total', '>', 1)
											->get();
											//->toSql();dd($total);
									}
									else
									{//thu
										$thu_total=DB::connection('hr')
											->table('attendance_images')
											->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
											->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
											->where('e.department',$dir->id)
											->where('status',0)//present
											->whereRaw('CASE 
														WHEN time <= 120000 THEN time<='.$att_time_in_thu.' ELSE time>='.$att_time_out_thu.' END')
											//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
											//->whereRaw('path REGEXP "'.$thu_days.'"')//check the year/monthDay in image path
											->where('date',$the_day)
											->groupBy('groupByMe')
											->groupBy('attendance_images.RFID')
											->having('total', '>', 1)
											->get();
											//->toSql();dd($total);
									}
									
									$result = 0 ;
									if($total!=0)
									{
										$result+=count($total);
									}
									if($thu_total!=0)
									{
										$result+=count($thu_total);
									}
									//$presents=$result;
									//$presents = $result/$day_counter;//everage
									//$present_perc = (100*$presents)/$totalEmp_dep->total;//percentage
									$present_totals.= $result.',';
									$absent_totals.= $totalEmp_dep->total-$result.',';
								}
								else
								{//directorate does not have any attendance employee
									$present_totals.= '0,';
									$absent_totals.= '0,';
								}
								$s_date = explode('-', $the_day);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);
								$directorates.= "'".$sdate."',";
							}
						}
						$direc=hrOperation::get_dep_name($dir->id)->name;
						$data['total']=$totalEmp_dep;
						$directorates = substr($directorates, 0, -1);
						$present_totals = substr($present_totals, 0, -1);
						$absent_totals = substr($absent_totals, 0, -1);
						$all_dirs_data[]=array('dir_total'=>$totalEmp_dep,'dir'=>$direc,'display'=>$directorates,'present_totals'=>$present_totals,'absent_totals'=>$absent_totals);
				
						//$data['display'] = $directorates;
						//$data['present_totals'] = $present_totals;
						//$data['absent_totals'] = $absent_totals;
					}
					//dd($all_dirs_data);
					$data['all_charts']=$all_dirs_data;
				}
				elseif(Input::get('general_department')=='')
				{//all deputies
					$dirs = getDepartmentWhereIn();
					$all_dirs_data = array();
					foreach($dirs as $dir)
					{
					    $present_totals= '';
						$absent_totals= '';
						$directorates='';//dates
						$totalEmp_dep = hrOperation::getEmployeeGen_Dep($dir->id);//number of employees who signes the att
						foreach($period As $day)
						{
						    $total=0;$thu_total=0;
							$the_day = $day->format( "Y-m-d" );
							$period_det = explode('-',$the_day);
							$period_day = $period_det[2];
							$period_month = $period_det[1];
							$period_year = $period_det[0];
							$day_code = date("w", strtotime($the_day));
							//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
							//$urgents = checkUrgentHoliday($the_day);
							$holiday = in_array($the_day, $new_array);
							if($holiday || $day_code == 5)
							{
								//$dayTotal++;//calculate the day urgent,leave or fridays as present
								continue;
							}
							else
							{//normal days
								if($totalEmp_dep)
								{
									if($day_code != 4)//dont include thu here as the time differs
									{
										$total =DB::connection('hr')
											->table('attendance_images')
											->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
											->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
											//->whereIn('e.department', [41,49,174,175])
											->where('e.general_department',$dir->id)
											->where('status',0)//present
											->whereRaw('CASE 
														WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
											//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
											//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
											->where('date',$the_day)//check the year/monthDay in image path
											->groupBy('groupByMe')
											->groupBy('attendance_images.RFID')
											->having('total', '>', 1)
											->get();
											//->toSql();dd($total);
									}
									else
									{//thu
										$thu_total=DB::connection('hr')
											->table('attendance_images')
											->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
											->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
											->where('e.general_department',$dir->id)
											->where('status',0)//present
											->whereRaw('CASE 
														WHEN time <= 120000 THEN time<='.$att_time_in_thu.' ELSE time>='.$att_time_out_thu.' END')
											//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
											//->whereRaw('path REGEXP "'.$thu_days.'"')//check the year/monthDay in image path
											->where('date',$the_day)
											->groupBy('groupByMe')
											->groupBy('attendance_images.RFID')
											->having('total', '>', 1)
											->get();
											//->toSql();dd($total);
									}
									
									$result = 0 ;
									if($total!=0)
									{
										$result+=count($total);
									}
									if($thu_total!=0)
									{
										$result+=count($thu_total);
									}
									//$presents=$result;
									//$presents = $result/$day_counter;//everage
									//$present_perc = (100*$presents)/$totalEmp_dep->total;//percentage
									$present_totals.= $result.',';
									$absent_totals.= $totalEmp_dep->total-$result.',';
								}
								else
								{//directorate does not have any attendance employee
									$present_totals.= '0,';
									$absent_totals.= '0,';
								}
								$s_date = explode('-', $the_day);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);
								$directorates.= "'".$sdate."',";
							}
						}
						$direc=hrOperation::get_dep_name($dir->id)->name;
						$data['total']=$totalEmp_dep;
						$directorates = substr($directorates, 0, -1);
						$present_totals = substr($present_totals, 0, -1);
						$absent_totals = substr($absent_totals, 0, -1);
						$all_dirs_data[]=array('dir_total'=>$totalEmp_dep,'dir'=>$direc,'display'=>$directorates,'present_totals'=>$present_totals,'absent_totals'=>$absent_totals);
				
						//$data['display'] = $directorates;
						//$data['present_totals'] = $present_totals;
						//$data['absent_totals'] = $absent_totals;
					}
					//dd($all_dirs_data);
					$data['all_charts']=$all_dirs_data;
				}
				return View::make('hr.attendance.generateCharts',$data);
			}
		}
	}
	public function generate_att_chart_percentage()
	{
		//validate fields
		$validates = Validator::make(Input::all(), array(
			"from_date"		=> "required",
			"to_date"		=> "required"
		));
		
		//check the validation
		if($validates->fails())
		{	
			return Redirect::route('attendance_chart')->withErrors($validates)->withInput();
		}
		else
		{
			$data['s_date'] = Input::get('from_date');
			$data['e_date'] = Input::get('to_date');
			$data['gen_dep'] = Input::get('general_department');
			$data['sub_dep'] = Input::get('sub_dep');
			$sdate = explode('-',Input::get('from_date'));
			$edate = explode('-',Input::get('to_date'));
			$month = $sdate[1];$year = $sdate[2];
			$att_time_in = getAttTime($year,$month,'in');
			$att_time_out = getAttTime($year,$month,'out');
			$att_time_in_thu = getAttTime_thu($year,$month,'in');
			$att_time_out_thu = getAttTime_thu($year,$month,'out');
			
			$from = dateToMiladi($sdate[2],$sdate[1],$sdate[0]);
			$to = dateToMiladi($edate[2],$edate[1],$edate[0]);
			if($to<=$from)
			{
				$validates->errors()->add('to_date', 'To Date must be bigger than From Date');
				return Redirect::route('attendance_chart')->withErrors($validates)->withInput();
			}
			$begin = new DateTime($from);
			$end = new DateTime($to);
			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($begin, $interval, $end);
			//create the all monthDay values for where: /2015/1216|/2015/1217
			$days = array();$day_counter=0;
			$thu_days = array();
			$allHolidays = hrOperation::getAllHolidays($from,$to);
			$new_array=array();
			foreach($allHolidays as $array)
			{
			    foreach($array as $val)
			    {
			        array_push($new_array, $val);
			    }    
			}
			foreach($period As $day)
			{
				$the_day = $day->format( "Y-m-d" );
				$period_det = explode('-',$the_day);
				$period_day = $period_det[2];
				$period_month = $period_det[1];
				$period_year = $period_det[0];
				$day_code = date("w", strtotime($the_day));
				//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
				//$urgents = checkUrgentHoliday($the_day);
				$holiday = in_array($the_day, $new_array);
				if($holiday || $day_code == 5)
				{
					//$dayTotal++;//calculate the day urgent,leave or fridays as present
					continue;
				}
				else
				{
					$day_counter++;//to calculate the everage
					if($day_code != 4)//dont include thu here as the time differs
					{
						//$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
						$days[] = $period_year.'-'.$period_month.'-'.$period_day;
					}
					else
					{//thursdays
						//$thu_days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
						$thu_days[] = $period_year.'-'.$period_month.'-'.$period_day;
					}
				}
			}
			
			if(Input::get('sub_dep')!='')
			{//only one directorate
				$totalEmp_dep = hrOperation::getEmployeeDep(Input::get('sub_dep'));//number of employees who signes the att
				if($totalEmp_dep)
				{
					if($days!=null)
					{
						$total =DB::connection('hr')
							->table('attendance_images')
							->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
							->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
							//->whereIn('e.department', [41,49,174,175])
							->where('e.department',Input::get('sub_dep'))
							->where('status',0)//present
							->whereRaw('CASE 
										WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
							//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
							//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
							->whereIn('date',$days)//check the year/monthDay in image path
							->groupBy('groupByMe')
							->groupBy('attendance_images.RFID')
							->having('total', '>', 1)
							->get();
							//->toSql();dd($total);
					}
					if($thu_days!=null)
					{
						$thu_total=DB::connection('hr')
							->table('attendance_images')
							->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
							->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
							->where('e.department',Input::get('sub_dep'))
							->where('status',0)//present
							->whereRaw('CASE 
										WHEN time <= 120000 THEN time<='.$att_time_in_thu.' ELSE time>='.$att_time_out_thu.' END')
							//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
							//->whereRaw('path REGEXP "'.$thu_days.'"')//check the year/monthDay in image path
							->whereIn('date',$thu_days)
							->groupBy('groupByMe')
							->groupBy('attendance_images.RFID')
							->having('total', '>', 1)
							->get();
							//->toSql();dd($total);
					}
					
					$result = 0 ;
					if(isset($total))
					{
						$result+=count($total);
					}
					if(isset($thu_total))
					{
						$result+=count($thu_total);
					}
					//$presents=$result+$thuTotal+$dayTotal;
					$presents = $result/$day_counter;//everage
					$present_perc = (100*$presents)/$totalEmp_dep->total;//percentage
					$data['present_totals'] = $present_perc;
					$data['absent_totals'] = 100-$present_perc;
				}
				else
				{//directorate does not have any attendance employee
					$data['present_totals'] = 0;
					$data['absent_totals'] = 100;
				}
				$direc=hrOperation::get_dep_name(Input::get('sub_dep'))->name;
				$direc=str_replace(",","-",$direc);
				$data['display'] = "'".$direc."',";
				
			}
			elseif(Input::get('sub_dep')=='' && Input::get('general_department')!='')
			{//all directorates of selected deputy
				$dirs = getRelatedSubDepartment(Input::get('general_department'));
				$present_totals= '';
				$absent_totals= '';
				$directorates='';
				foreach($dirs as $dir)
				{
					$totalEmp_dep = hrOperation::getEmployeeDep($dir->id);
					if($totalEmp_dep)
					{
						if($days!=null)
						{
							$total =DB::connection('hr')
								->table('attendance_images')
								->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
								->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
								//->whereIn('e.department', [41,49,174,175])
								->where('e.department',$dir->id)
								->where('status',0)//present
								->whereRaw('CASE 
											WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
								//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
								//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
								->whereIn('date',$days)
								->groupBy('groupByMe')
								->groupBy('attendance_images.RFID')
								->having('total', '>', 1)
								->get();
								//->toSql();dd($total);
						}
						if($thu_days!=null)
						{
							$thu_total=DB::connection('hr')
								->table('attendance_images')
								->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
								->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
								->where('e.department',$dir->id)
								->where('status',0)//present
								->whereRaw('CASE 
											WHEN time <= 120000 THEN time<='.$att_time_in_thu.' ELSE time>='.$att_time_out_thu.' END')
								//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
								//->whereRaw('path REGEXP "'.$thu_days.'"')//check the year/monthDay in image path
								->whereIn('date',$thu_days)
								->groupBy('groupByMe')
								->groupBy('attendance_images.RFID')
								->having('total', '>', 1)
								->get();
								//->toSql();dd($total);
						}
						
						$result = 0 ;
						if(isset($total))
						{
							$result+=count($total);
						}
						if(isset($thu_total))
						{
							$result+=count($thu_total);
						}
						//$presents=$result+$thuTotal+$dayTotal;
						$presents = $result/$day_counter;
						$present_perc = (100*$presents)/$totalEmp_dep->total;
						$present_totals.= $present_perc.',';
						$absent_totals.= 100-$present_perc.',';
						
					}
					else
					{
						$present_totals.= '0,';
						$absent_totals.= '100,';
					}
					$direc=hrOperation::get_dep_name($dir->id)->name;
					$direc=str_replace(",","-",$direc);
					$directorates.= "'".$direc."',";
				}
				$directorates = substr($directorates, 0, -1);
				$present_totals = substr($present_totals, 0, -1);
				$absent_totals = substr($absent_totals, 0, -1);
				$data['display'] = $directorates;
				$data['present_totals'] = $present_totals;
				$data['absent_totals'] = $absent_totals;
			}
			elseif(Input::get('general_department')=='')
			{//all deputies
				$dirs = getDepartmentWhereIn();
				$present_totals= '';
				$absent_totals= '';
				$directorates='';
				foreach($dirs as $dir)
				{
					$totalEmp_dep = hrOperation::getEmployeeGen_Dep($dir->id);
					if($totalEmp_dep)
					{
						if($days!=null)
						{
							$total =DB::connection('hr')
								->table('attendance_images')
								->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
								->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
								//->whereIn('e.department', [41,49,174,175])
								->where('e.general_department',$dir->id)
								->where('status',0)//present
								->whereRaw('CASE 
											WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
								//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
								//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
								->whereIn('date',$days)
								->groupBy('groupByMe')
								->groupBy('attendance_images.RFID')
								->having('total', '>', 1)
								->get();
								//->toSql();dd($total);
						}
						if($thu_days!=null)
						{
							$thu_total=DB::connection('hr')
								->table('attendance_images')
								->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(attendance_images.id) As total'))//return the monthDay part of path
								->leftjoin('employees AS e','e.RFID','=','attendance_images.RFID')
								->where('e.general_department',$dir->id)
								->where('status',0)//present
								->whereRaw('CASE 
											WHEN time <= 120000 THEN time<='.$att_time_in_thu.' ELSE time>='.$att_time_out_thu.' END')
								//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
								//->whereRaw('path REGEXP "'.$thu_days.'"')//check the year/monthDay in image path
								->whereIn('date',$thu_days)
								->groupBy('groupByMe')
								->groupBy('attendance_images.RFID')
								->having('total', '>', 1)
								->get();
								//->toSql();dd($total);
						}
						
						$result = 0 ;
						if(isset($total))
						{
							$result+=count($total);
						}
						if(isset($thu_total))
						{
							$result+=count($thu_total);
						}
						//$presents=$result+$thuTotal+$dayTotal;
						$presents = $result/$day_counter;
						//$totalEmp_dep = hrOperation::getEmployeeDep($dir->id);
						//dd($totalEmp_dep[0]->total);
						$present_perc = (100*$presents)/$totalEmp_dep->total;
						$present_totals.= $present_perc.',';
						$absent_totals.= 100-$present_perc.',';
					}
					else
					{
						$present_totals.= '0,';
						$absent_totals.= '100,';
					}
					$direc=hrOperation::get_dep_name($dir->id)->name;
					$direc=str_replace(",","-",$direc);
					$directorates.= "'".$direc."',";
				}
				$directorates = substr($directorates, 0, -1);
				$present_totals = substr($present_totals, 0, -1);
				$absent_totals = substr($absent_totals, 0, -1);
				$data['display'] = $directorates;
				$data['present_totals'] = $present_totals;
				$data['absent_totals'] = $absent_totals;
			}
			//dd($directorates);
			
			return View::make('hr.attendance.generateCharts',$data);
		}
	}
	public function getAllPositions()
	{
		return View::make('hr.attendance.localization');
	}
	public function getAllPositionsData()
	{
		$object = hrOperation::getEmployees_positions();
		$collection = new Collection($object);
		$result = Datatable::collection($collection)
						->showColumns(
							'id',
							'fullname',
							'father_name_dr',
							'dep_name',
							'position_dr',
							'position_en'
							)
				->addColumn('operations', function($option){
					return '<a href="javascript:void()" onclick="load_position_edit('.$option->id.');" class="table-link" style="text-decoration:none;" data-target="#salary_modal" data-toggle="modal">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>';
				})		
				
				->make();
		return $result;
	}
	public function editPositionModal()
	{
		$data['id'] = Input::get('id');
		
		$data['details'] = hrOperation::getEmployeeById(Input::get('id'));
		
		return View::make('hr.attendance.edit_position',$data);
	}
	public function postEmployeePosition()
	{
		hrOperation::update_record('employees',array('current_position_en'=>Input::get('name_en')),array('id'=>Input::get('employee_id')));
		hrOperation::insertLog('employees',1,'Eng position of given employee',Input::get('employee_id'));
	}
	public function test_email()
	{
		$user_email = array('email'=>'ali.panahi@aop.gov.af','name'=>'ali hussain','last_name'=>'panahi','the_date'=>'1395-6-6');
		Mail::send('emails.absent_afternoon', $user_email, function($message) use ($user_email){
									    $message->from('e.attendance.aop@gmail.com', 'AOP Attendance Department');
										$message->to($user_email['email'])->subject('Absent Alert');
									});
	}
}
?>