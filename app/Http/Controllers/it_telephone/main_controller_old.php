<?php 
namespace App\Http\Controllers\phone_dep;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\phone_dep\Equipment;
use App\models\phone_dep\Employee_info;
use App\models\phone_dep\Directorat;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Response;
use Excel;
use Validator;


class main_controller extends Controller {

//----------------------------Select Data-------------------------------

  public function loadRecordsList(){
        return view('phone_dep.list');
  }
public function loadreport(){
        
    $company=\DB::table("companys")->get(); 
    $equipt=\DB::table("equipments")->get(); 
    $equptype=DB::table("equipment_types")->get(); 
    


       // $direc=\DB::table("directorates")->get();



    

    if($equptype =="PostPaid")
      $postpaid = "1";
    elseif($equptype == "PrePaid")
      $postpaid = "2";
    else
      $postpaid = "";
    

     
      //  $all_data=Equipment::paginate(6);

 

      $salam=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','Salam')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->get();

      $roshan=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','Roshan')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->get();

      $awcc=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','AWCC')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->get();

      $mtn=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','MTN')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->get();

      $etesalat=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','Etesalat')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->get();

      $w_telecom=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','Wasel Telecom')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->get();

      $a_telecom=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','Afghan Telecom')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->get();

      $activation=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('activation','=','2')
      ->get();

      $storage=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('storage','=','1')
      ->get();

      $usbdongle=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('equipment_id','=','2')
      ->get();
      $phone020=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('equipment_id','=','3')
      ->get();
      $siemensPhone=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('equipment_id','=','4')
      ->get();



    //   $data['salam']=$salam;
    //   $data['roshan']=$roshan;
    //   $data['a_telecom']=$a_telecom;
    //   $data['company']=$company;
    //   $data['equptype']=$equptype;
    
    // return view('phone_dep.list1',$data);
      return view("phone_dep.list1", array('salam' =>$salam,'roshan' =>$roshan,'a_telecom' =>$a_telecom,'mtn' =>$mtn,'awcc' =>$awcc,'etesalat' =>$etesalat,'w_telecom' =>$w_telecom,'activation' =>$activation,'company'=>$company,'equipt'=>$equipt,'equptype'=>$equptype,'storage'=>$storage,'usbdongle'=>$usbdongle,'phone020'=>$phone020,'siemensPhone'=>$siemensPhone));
    
}
//dd($_POST);

//------------------------Download file

public function file_download($file_name=""){

    //dd($file_name);
  // print_r($file_name);exit;
        //public path for file
        $file= public_path(). "/uploads/".$file_name;
        //download file
        return Response::download($file, $file_name);
}

//-----------------------Insert Data--------------------------------------

//---------------Insert Employee Information 
  public function load_emp_form($phone=0,$sim_id=0){
    $data['equipments'] = DB::table('equipments')->get();
    $data['employee_infos'] = DB::table('employee_infos')->get();
    $data['directorates'] = DB::table('directorates')->get();
    $data['phone'] = $phone;
    $data['sim_id'] = $sim_id;
    return view("phone_dep.insert_employee_info",$data);
  }

  public function insert_emp_info()
    {

$file = Input::file('file');
    if(Input::hasFile('file'))
    {
      // validating each file.
      $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
      $validator = Validator::make(

          [
                'file' => $file,
            ],
            [
                'file' => 'required|mimes:jpeg,png,jpg,gif,docx,pdf'
            ]
        );

      if($validator->passes())
      {
          
          // path is root/uploads
          $destinationPath = 'uploads/';
          $original_filename = $file->getClientOriginalName();
          $temp = explode(".", $original_filename);
          $extension = end($temp);
                            
          $lastFileId = DB::table('employee_infos')->orderBy('id', 'desc')->pluck('id');
      
          $lastFileId++;
                            
          $filename = "photo_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;

          $upload_success = $file->move($destinationPath, $filename);

          if($upload_success) 
          {
           if($date=Input::get('date'))
      $date = toGregorian(gregorian_format($date));
          $emp_records = array(
                 // "id" => Input::get('id'),
                  "cardno" => Input::get('cardno'),
                  "name" => Input::get('name'),
                  "lastname" => Input::get('lastname'),
                  "gender" => Input::get('gender'),
                  "address" =>Input::get('address'),
                  "email_id" => Input::get('email_id'),
                  "office_no" => Input::get('office_no'),
                  "directorate" => Input::get('directorate'),
                  "department" => Input::get('department'),
                  "position" => Input::get('position'),
                  "sim_id" => Input::get('sim_id'),
                  "remark" => Input::get('remark'),
                  "file" => $filename,
                  // "file2" => Input::get('file2'),
                  );
         $employeedata = Employee_info::insert_emp_info($emp_records);


          return Redirect::route("list")->with("success","معلومات معافقانه ثبت  ګردید.");        
        } 
        else 
        {
          // send back to the page with the input data and errors
          return Redirect::route('list')->withInput($validator)->withErrors();
        }
        
      } 
      else 
      {
          // redirect back with errors.
          return Redirect::back()->withErrors($validator);
      }
        
    }

}  

  //------------- Insert SIM Information 

	public function load_insert_form()
	{

   

     $all_data=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','equipments.remark')
     
     ->leftjoin('companys','equipments.company_id','=','companys.id')
     ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
     ->paginate(10);


     $selectequp=\DB::table("equipments")->get();
    $company=\DB::table("companys")->get(); 
     $equptype=DB::table("equipment_types")->get(); 
     $empinfo=\DB::table("employee_infos")->orderBy('id','desc')->get();
    return view("phone_dep.insert_sim_info",array('selectequp'=>$selectequp,'company'=>$company,'equptype'=>$equptype,'empinfo'=>$empinfo,'all_data'=>$all_data));
		

	}
	
	public function insert_mo_data(Request $request)
	{	

    $insertdata=new Equipment;
    //$insertdata->emp_id=Input::get('name');
    $insertdata->company_id=Input::get('company');
    $insertdata->equipment_id= Input::get('equipment');
   // $insertdata->equipment_range=Input::get('equipment_range') ;
    $insertdata->equipment_serialno=Input::get('equipment_serial');
    $insertdata->simcard_number=Input::get('phonno');
    //$insertdata->MB_credit=Input::get('MB_credit');
    $insertdata->storage=Input::get('storage');
    $insertdata->register_date= toGregorian(gregorian_format(Input::get('rig_date')));
    $insertdata->remark=Input::get('remark');
    //$insertdata->activation=Input::get('activation');
    
   // $insertdata->postpaid=Input::get('postpaid');
    // $insertdata->issue_date= toGregorian(gregorian_format(Input::get('issue_date')));
    $insertdata->save();
 
		return Redirect::route("insert_row")->with("success","معلومات معافقانه ثبت  ګردید.");

		
	}

  //-----------------Insert USB Dongle info

  public function load_dong_form()
  {

  // return "salam";

     $all_data1=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','equipments.remark')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->paginate(10);


     $selectequp1=\DB::table("equipments")->get();
    $company1=\DB::table("companys")->get(); 
     $equptype1=DB::table("equipment_types")->get(); 
     $empinfo1=\DB::table("employee_infos")->orderBy('id','desc')->get();
    return view("phone_dep.insert_dongle_info",array('selectequp1'=>$selectequp1,'company1'=>$company1,'equptype1'=>$equptype1,'empinfo1'=>$empinfo1,'all_data1'=>$all_data1));
    

  }
  
  public function insert_dong_data(Request $request)
  { 

    $insertdata1=new Equipment;
    //$insertdata1->emp_id=Input::get('name');
    $insertdata1->company_id=Input::get('company');
    $insertdata1->equipment_id= Input::get('equipment');
   // $insertdata1->equipment_range=Input::get('equipment_range') ;
    $insertdata1->equipment_serialno=Input::get('equipment_serial');
    $insertdata1->simcard_number=Input::get('phonno');
    //$insertdata1->MB_credit=Input::get('MB_credit');
    $insertdata1->storage=Input::get('storage');
    $insertdata1->register_date= toGregorian(gregorian_format(Input::get('rig_date')));
    $insertdata1->remark=Input::get('remark');
    //$insertdata1->activation=Input::get('activation');
    
   // $insertdata1->postpaid=Input::get('postpaid');
    
    $insertdata1->save();
 
    return Redirect::route("lod_dong")->with("success","معلومات معافقانه ثبت  ګردید.");

    
  }

  //------------------------Insert 020 info

   public function load_020_form()
  {

  // return "salam";

     $all_data2=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','equipments.remark')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->paginate(10);


     $selectequp2=\DB::table("equipments")->get();
    $company2=\DB::table("companys")->get(); 
     $equptype2=DB::table("equipment_types")->get(); 
     $empinfo2=\DB::table("employee_infos")->orderBy('id','desc')->get();
    return view("phone_dep.insert_020_info",array('selectequp2'=>$selectequp2,'company2'=>$company2,'equptype2'=>$equptype2,'empinfo2'=>$empinfo2,'all_data2'=>$all_data2));
    

  }
  
  public function insert_020_data(Request $request)
  { 

    $insertdata2=new Equipment;
    //$insertdata2->emp_id=Input::get('name');
    $insertdata2->company_id=Input::get('company');
    $insertdata2->equipment_id= Input::get('equipment');
   // $insertdata2->equipment_range=Input::get('equipment_range') ;
    $insertdata2->equipment_serialno=Input::get('equipment_serial');
    $insertdata2->simcard_number=Input::get('phonno');
    //$insertdata2->MB_credit=Input::get('MB_credit');
    $insertdata2->storage=Input::get('storage');
    //$insertdata2->register_date= toGregorian(gregorian_format(Input::get('rig_date')));
    $insertdata2->remark=Input::get('remark');
    //$insertdata2->activation=Input::get('activation');
    
   // $insertdata2->postpaid=Input::get('postpaid');
    $insertdata2->register_date= toGregorian(gregorian_format(Input::get('rig_date')));
    $insertdata2->save();
 
    return Redirect::route("lod_020")->with("success","معلومات معافقانه ثبت  ګردید.");

    
  }

  //---------------------------Insert Directorat


public function lod_dir_form(){
// return "Salam Alikom";

  $all_data3=\DB::table("directorates")->get();

  return view("phone_dep.insert_directorat",array('all_data3'=>$all_data3));
}

public function insert_dir_data(Request $request)
  { 

    $insertdata3=new Directorat;
    //$insertdata2->emp_id=Input::get('name');
    $insertdata3->directorate=Input::get('directorate');
    $insertdata3->save();
 
    return back()->with("success","معلومات معافقانه ثبت  ګردید.");

}
  //----------------------------Update Data-----------------------------
  

public function load_editequp_form($id){
     $edit_equp = Equipment::find($id);
     $emp_info =\DB::table("employee_infos")->get();
     $company=\DB::table("companys")->get(); 
     $equptype=DB::table("equipment_types")->get(); 
     return view("phone_dep.update_equp",array('edit_uqup'=>$edit_equp,'company'=>$company,'equptype'=>$equptype,'emp_info'=>$emp_info));
    }
  
  public function edit_mo_data(Request $request,$id){ 

    $edit_equp=Equipment::find($id);
    $edit_equp->id=$request->id;
    // $edit_equp->emp_id=$request->name;
    $edit_equp->company_id=$request->company;
    $edit_equp->equipment_id= $request->equipment;
    $edit_equp->equipment_range=$request->equipment_range;
    $edit_equp->equipment_serialno=$request->equipment_serial;
    $edit_equp->simcard_number=$request->phonno;
    $edit_equp->storage=$request->storage;
    $edit_equp->activation=$request->activation;
    $edit_equp->issue_date= toGregorian(gregorian_format(Input::get('issue_date')));
    $edit_equp->postpaid=$request->postpaid;
    $edit_equp->remark=$request->remark;
    $edit_equp->save();

     return Redirect::route("load_emp",array($request->phonno,$request->id))->with("success","معلومات معافقانه ثبت  ګردید.");
    }
 
   //----------------Update USB dongle

    public function load_editdongle_form($id){

      
     $edit_equp1 = Equipment::find($id);
     $emp_info1 =\DB::table("employee_infos")->get();
     $company1=\DB::table("companys")->get(); 
     $equptype1=DB::table("equipment_types")->get(); 
     return view("phone_dep.update_dongle",array('edit_equp1'=>$edit_equp1,'company1'=>$company1,'equptype1'=>$equptype1,'emp_info1'=>$emp_info1));
    }
  
  public function edit_usb_data(Request $request,$id){ 
// dd($_POST);
    $edit_equp1=Equipment::find($id);
    $edit_equp1->id=$request->id;
    // $edit_equp1->emp_id=$request->name;
    $edit_equp1->company_id=$request->company;
    $edit_equp1->equipment_id= $request->equipment;
  //  $edit_equp1->equipment_range=$request->equipment_range;
    $edit_equp1->equipment_serialno=$request->equipment_serial;
    $edit_equp1->simcard_number=$request->phonno;
    $edit_equp1->storage=$request->storage;
    $edit_equp1->MB_credit=$request->MB_credit;
    $edit_equp1->issue_date= toGregorian(gregorian_format(Input::get('issue_date')));
   // $edit_equp1->postpaid=$request->postpaid;
    $edit_equp1->remark=$request->remark;
    $edit_equp1->save();
     return Redirect::route("load_emp",array($request->phonno,$request->id))->with("success","معلومات معافقانه ثبت  ګردید.");
    }

    //----------------Update 020

    public function load_edit020_form($id){
     $edit_equp2 = Equipment::find($id);
     $emp_info2 =\DB::table("employee_infos")->get();
     $company2=\DB::table("companys")->get(); 
     $equptype2=DB::table("equipment_types")->get(); 
     return view("phone_dep.update_020",array('edit_equp2'=>$edit_equp2,'company2'=>$company2,'equptype2'=>$equptype2,'emp_info2'=>$emp_info2));
    }
  
  public function edit_020_data(Request $request,$id){ 
    $edit_equp2=Equipment::find($id);
    $edit_equp2->id=$request->id;
    // $edit_equp2->emp_id=$request->name;
    $edit_equp2->company_id=$request->company;
    $edit_equp2->equipment_id= $request->equipment;
    $edit_equp2->type_call=$request->type_call;
    $edit_equp2->equipment_serialno=$request->equipment_serial;
    $edit_equp2->simcard_number=$request->phonno;
    $edit_equp2->storage=$request->storage;
    $edit_equp2->activation=$request->activation;
    $edit_equp2->issue_date= toGregorian(gregorian_format(Input::get('issue_date')));
   // $edit_equp2->postpaid=$request->postpaid;
    $edit_equp2->remark=$request->remark;
    $edit_equp2->save();
     return Redirect::route("load_emp",array($request->phonno,$request->id))->with("success","معلومات معافقانه ثبت  ګردید.");
    }

   //--------------------Delete Record-------------

     function delete_equp($id){
      $delete_equp=DB::table("equipments")->where("id","=",$id);
      $delete_equp->delete();
      return back();
    }

    //------------------------------------Search Data---------------------
    public function search_sim(){
         $company_id=Input::get('company');
         $SIMCard=Input::get('SIMCard');

      $all_data=\DB::table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.remark')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->whereRaw("(company_id=".$company_id." AND equipment_id =".$SIMCard.")")
      ->get();
      return view("phone_dep.search_for_print",array('all_data'=>$all_data));
    }
     public function search_engin(Request $request){
      //dd($_POST);
       
         $company_id=Input::get('company');
         $equipment_range=Input::get('equipment_range');
         $postpaid=Input::get('postpaid');
          $storage=Input::get('storage');
          $equipment_id=Input::get('equipment_type');

           $all_data=\DB::table('equipments')->select('equipments.*','companys.com_name','employee_infos.name','employee_infos.file','equipment_types.equipment_name')
            ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
            ->leftjoin('companys','equipments.company_id','=','companys.id')
            ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id');  
            
                if($company_id != "")
                {
                  $all_data->where("company_id", $company_id);
                }
                if($equipment_range!="")
                {
                  $all_data->where("equipment_range", $equipment_range);
                }
                if($postpaid!="")
                {
                  $all_data->where("postpaid",$postpaid);
                }
                if($storage!="")
                {
                  $all_data->where("storage",$storage);
                }
                if($equipment_id!="")
                {
                  $all_data->where("equipment_id",$equipment_id);
                }
                // ->whereRaw("(company_id=".$company_id." AND equipment_id =".$SIMCard.")")
                $data['all_data'] = $all_data->orderBy('com_name','asc')->get();
             
                return view("phone_dep.search_engin", $data);
               
        }


    //-----------------------Live Search---------------------
    public function search_sim_live(){

     $search_string=trim(Input::get('search_string'));
    if($search_string == "Limited")
      $equipment_range = "1";
    elseif($search_string == "Unlimited")
      $equipment_range = "2";
    else
      $equipment_range = "";

    if($search_string =="PostPaid")
      $postpaid = "1";
    elseif($search_string == "PrePaid")
      $postpaid = "2";
    else
      $postpaid = "";
    if($search_string == "Active")
      $activation="1";
    elseif($search_string == "Deactive")
      $activation="2";
    else
      $activation="";
 

       $rows=\DB::table('equipments')
       ->select('equipments.*','equipments.activation','companys.com_name','equipment_types.equipment_name','employee_infos.remark')
      
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
       ->where('companys.com_name', 'like', '%'.$search_string.'%')
       ->orwhere('equipments.equipment_serialno', 'like', '%'.$search_string.'%')
       ->orwhere('equipments.simcard_number','like','%'.$search_string.'%')
       ->orwhere('equipments.equipment_range',$equipment_range)
       ->orwhere('equipments.postpaid',$postpaid)
       ->orwhere('employee_infos.name','like','%'.$search_string.'%')
       ->orwhere('employee_infos.directorate','like','%'.$search_string.'%')
       ->orwhere('equipments.activation',$activation)
       ->get();
      $data['rows'] = $rows;
       return view::make('phone_dep.search_live',$data); 
  } 
  public function exportExcel(){

    $company_id=Input::get('company');
     $equipment_range=Input::get('equipment_range');
     $postpaid=Input::get('postpaid');
      $storage=Input::get('storage');

       $all_data=\DB::table('equipments')->select('equipments.*','companys.com_name','employee_infos.name')
        ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
        ->leftjoin('companys','equipments.company_id','=','companys.id');  
            if($company_id != "")
            {
              $all_data->where("company_id", $company_id);
            }
            if($equipment_range!="")
            {
              $all_data->where("equipment_range", $equipment_range);
            }
            if($postpaid!="")
            {
              $all_data->where("postpaid",$postpaid);
            }
            if($storage!="")
            {
              $all_data->where("storage",$storage);
            }
            // ->whereRaw("(company_id=".$company_id." AND equipment_id =".$SIMCard.")")
            $data= $all_data->orderBy('com_name','asc')->get();
         

            $curr_date = date('Y-m-d');
            Excel::load('excel_template/phone_dep.xlsx', function($file) use($data){
            //Excel::create('Filename', function($file) use($data){        
            $file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){   
                $row = 3;
                //$sheet->setFreeze('A3');
               
                foreach($data AS $item)
                {
                    $sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true);

                      if($item->equipment_range ==1 ){ 
                        $equipment_range = "Limited";
                      }else{ 

                      $equipment_range ="Unlimited";
                      }

                      if($item->postpaid ==1){ 
                        $postpaid = "PostPaid";
                      }else{ 

                      $postpaid ="PrePaid";
                      }

                      if($item->activation ==1){ 
                        $activation = "Active";
                      }else{ 

                      $activation ="Deactive";
                      }


                    $sheet->setHeight($row, 20);
                    $sheet->setCellValue('A'.$row.'',checkEmptyDate($item->issue_date));
                    $sheet->setCellValue('B'.$row.'',$activation);
                    $sheet->setCellValue('C'.$row.'',$postpaid);
                    $sheet->setCellValue('D'.$row.'',$equipment_range);
                    $sheet->setCellValue('E'.$row.'',$item->simcard_number);
                    $sheet->setCellValue('F'.$row.'',$item->equipment_serialno);
                    $sheet->setCellValue('G'.$row.'',$item->com_name);
                    $sheet->setCellValue('H'.$row.'',$item->name);
                    $sheet->setCellValue('I'.$row.'',$row-2);
                    //$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));
                   
                    $row++;
                }
 
                $sheet->setBorder('A3:I'.($row-1).'', 'thin');
               
            });
           
            })->export('xlsx');
    }
  }

    

