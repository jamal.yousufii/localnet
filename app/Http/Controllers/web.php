<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

/*** Home ***/
Route::get('/home/{mod?}', 'HomeController@index')->name('home');
Route::get('/bringSections/{id}', 'HomeController@bringSections')->name('bringSections');
Route::get('/bringContractors/{id}', 'HomeController@bringContractors')->name('bringContractors');
Route::get('/bringContractorSections/{id}/{con_id}', 'HomeController@bringContractorSections')->name('bringContractorSections');
Route::post('/deleteFile', 'HomeController@deleteFile')->name('deleteFile');
Route::get('/DownloadDoc/{id},{table},{folder}', 'HomeController@DownloadDoc')->name('DownloadDoc');
Route::post('/bringTypes', 'HomeController@bringProjectTypes')->name('bringTypes');
Route::post('/bringDistricts', 'HomeController@bringDistricts')->name('bringDistricts');
Route::post('/bringVillages', 'HomeController@bringVillages')->name('bringVillages');
Route::post('/bringUsers', 'HomeController@bringUsers')->name('bringUsers');
Route::post('/completeRecords', 'HomeController@completeRecords')->name('completeRecords');
Route::get('/deleteUpdatedFile/{id}/{table}', 'HomeController@deleteUpdatedFile')->name('deleteUpdatedFile');
/*** Attachments ***/
Route::post('/bringMoreAttachments', 'HomeController@bringMoreAttachments')->name('bringMoreAttachments');
Route::get('/attachments_list/{id},{table}', 'HomeController@attachments_list')->name('attachments_list');
Route::post('/store_attachments', 'HomeController@store_attachments')->name('store_attachments');
Route::get('/DownloadAttachments/{id},{table}', 'HomeController@DownloadAttachments')->name('DownloadAttachments');
/*** Localization Routes ***/
Route::get('/language/{locale?}', 'Lang\LanguageController@changeLanguage')->name('language');

/*** Requests ***/
Route::resource('requests','RequestController');
Route::get('/request/{id?}', 'RequestController@index')->name('request');
Route::post('/filterRequest', 'RequestController@filterRequest')->name('filterRequest');
Route::post('/approve_request', 'RequestController@approve_request')->name('approve_request');
Route::get('/requests_notification/{id?}', 'RequestController@requests_notification')->name('requests_notification');

/*** Projects ***/
Route::resource('plans','PlanController');
Route::get('/plan/{id?}', 'PlanController@index')->name('plan');
Route::post('/filterPlan', 'PlanController@filterPlan')->name('filterPlan');
Route::get('/plan_notification/{id?}', 'PlanController@plan_notification')->name('plan_notification');
Route::post('/store_location', 'PlanController@store_location')->name('store_location');
Route::get('/show_ajax', 'PlanController@show_ajax')->name('show_ajax');
Route::post('/update_location', 'PlanController@update_location')->name('update_location');

/*** Surveys ***/
Route::resource('surveys','SurveyController');
Route::get('/survey/{id?}', 'SurveyController@index')->name('survey');
Route::get('/list_survey/{id?}', 'SurveyController@list')->name('list_survey');
Route::post('/survey_update', 'SurveyController@update_data')->name('survey_update');
Route::post('/more_measurement', 'SurveyController@more_measurement')->name('more_measurement');
Route::post('/more_beneficairy', 'SurveyController@more_beneficairy')->name('more_beneficairy');
Route::post('/more_document', 'SurveyController@more_document')->name('more_document');
Route::post('/filter_survey', 'SurveyController@filter_survey')->name('filter_survey');
Route::get('/SurveyEntities', 'SurveyController@SurveyEntities')->name('SurveyEntities');
Route::get('/storeSurveyEntities', 'SurveyController@storeSurveyEntities')->name('storeSurveyEntities');
Route::get('/survey_notification/{id?}', 'SurveyController@survey_notification')->name('survey_notification');

/*** Designs ***/
Route::resource('designs','DesignController')->except(['index']);
Route::get('/design/{id?}', 'DesignController@index')->name('design');
Route::get('/filter_design', 'DesignController@filter_design')->name('filter_design');

Route::get('/design_notification/{id?}', 'DesignController@design_notification')->name('design_notification');
Route::get('/send_estimation', 'DesignController@send_estimation')->name('send_estimation');
Route::post('/view_design', 'DesignController@view_design')->name('view_design');
/*** Designs->Architecture ***/
Route::resource('architecture','ArchitectureController')->except(['create','edit','update']);
Route::post('/architecture/create','ArchitectureController@create')->name('architecture.create');
Route::post('/architecture/approve', 'ArchitectureController@approveArchitecture')->name('architecture.approve');
Route::post('/architecture/do/approve','ArchitectureController@doApproveArchitecture')->name('architecture.doapprove');
Route::post('/architecture/view','ArchitectureController@view')->name('architecture.view');
Route::post('/architecture/edit','ArchitectureController@edit')->name('architecture.edit');
Route::post('/architecture/update','ArchitectureController@update')->name('architecture.update');
Route::post('/architecture/version','ArchitectureController@showVersion')->name('architecure.version');

/*** Designs->Structure ***/
Route::resource('structure','StructureController')->except(['create','edit','update']);
Route::post('/structure/create','StructureController@create')->name('structure.create');
Route::post('/structure/approve', 'StructureController@approveStructure')->name('structure.approve');
Route::post('/structure/do/approve','StructureController@doApproveStructure')->name('structure.doapprove');
Route::post('/structure/view','StructureController@view')->name('structure.view');
Route::post('/structure/edit','StructureController@edit')->name('structure.edit');
Route::post('/structure/update','StructureController@update')->name('structure.update');
Route::post('/structure/version','StructureController@showVersion')->name('structure.version');

/*** Designs->Electricity ***/
Route::resource('electricity','ElectricityController')->except(['create','edit','update']);
Route::post('/electricity/create','ElectricityController@create')->name('electricity.create');
Route::post('/electricity/approve', 'ElectricityController@approveElectricity')->name('electricity.approve');
Route::post('/electricity/do/approve','ElectricityController@doApproveElectricity')->name('electricity.doapprove');
Route::post('/electricity/view','ElectricityController@view')->name('electricity.view');
Route::post('/electricity/edit','ElectricityController@edit')->name('electricity.edit');
Route::post('/electricity/update','ElectricityController@update')->name('electricity.update');
Route::post('/electricity/version','ElectricityController@showVersion')->name('electricity.version');

/*** Designs->Water ***/
Route::resource('water','WaterController')->except(['create','edit','update']);
Route::post('/water/create','WaterController@create')->name('water.create');
Route::post('/water/approve', 'WaterController@approveWater')->name('water.approve');
Route::post('/water/do/approve','WaterController@doApproveWater')->name('water.doapprove');
Route::post('/water/view','WaterController@view')->name('water.view');
Route::post('/water/edit','WaterController@edit')->name('water.edit');
Route::post('/water/update','WaterController@update')->name('water.update');
Route::post('/water/version','WaterController@showVersion')->name('water.version');
/*** Designs->Mechanic ***/
Route::resource('mechanic','MechanicController')->except(['create','edit','update']);
Route::post('/mechanic/create','MechanicController@create')->name('mechanic.create');
Route::post('/mechanic/approve', 'MechanicController@approveMechanic')->name('mechanic.approve');
Route::post('/mechanic/do/approve','MechanicController@doApproveMechanic')->name('mechanic.doapprove');
Route::post('/mechanic/view','MechanicController@view')->name('mechanic.view');
Route::post('/mechanic/edit','MechanicController@edit')->name('mechanic.edit');
Route::post('/mechanic/update','MechanicController@update')->name('mechanic.update');
Route::post('/mechanic/version','MechanicController@showVersion')->name('mechanic.version');

/*** Designs->Visa ***/
Route::resource('visa','VisaController');

/*** Estimations ***/
Route::resource('estimations','EstimationController')->except(['index']);
Route::get('/estimation/{id?}', 'EstimationController@index')->name('estimation');
Route::get('/filter_estimation/{id?}', 'EstimationController@filter_estimation')->name('filter_estimation');
Route::post('/store_bill', 'EstimationController@store_bill')->name('store_bill');
Route::post('/update_bill', 'EstimationController@update_bill')->name('update_bill');
Route::get('/estimation/billquantity/list/{id}/{loc_id}', 'EstimationController@locationBillQuantityList')->name('location_bill_quantity.list');
Route::post('importExcel', 'EstimationController@importExcel')->name('importExcel');
Route::get('printExport/{id}/{loc_id}', 'EstimationController@printExport')->name('printExport');



/*** Implementing Plan ***/
Route::resource('implements','ImplementsController')->except(['index','update']);
Route::get('/implement/{id?}', 'ImplementsController@index')->name('implement');
Route::post('/more_attachment', 'ImplementsController@more_attachment')->name('more_attachment');
Route::post('/implements/update', 'ImplementsController@update')->name('implements/update');


Route::post('/approve_implement', 'ImplementsController@approve_implement')->name('approve_implement');
Route::get('/filter_implement', 'ImplementsController@filter_implement')->name('filter_implement');


/*** Procurements ***/
Route::get('/procurement/{id?}', 'ProcurementController@index')->name('procurement');
Route::get('/filter_procurement', 'ProcurementController@filter_procurement')->name('filter_procurement');
// Executive
Route::get('/executive/list/{id?}', 'ProcurementController@executive_list')->name('executive/list');
Route::get('/executive/create', 'ProcurementController@executive_create')->name('executive/create');
Route::post('/executive/store', 'ProcurementController@executive_store')->name('executive/store');
Route::get('/executive/show/{id?}', 'ProcurementController@executive_show')->name('executive/show');
Route::get('/executive/edit/{id?}', 'ProcurementController@executive_edit')->name('executive/edit');
Route::post('/executive/update', 'ProcurementController@executive_update')->name('executive/update');
// Planning
Route::get('/planning/list/{id?}', 'ProcurementController@planning_list')->name('planning/list');
Route::get('/planning/create', 'ProcurementController@planning_create')->name('planning/create');
Route::post('/planning/store', 'ProcurementController@planning_store')->name('planning/store');
Route::get('/planning/show/{id?}', 'ProcurementController@planning_show')->name('planning/show');
Route::get('/planning/edit/{id?}', 'ProcurementController@planning_edit')->name('planning/edit');
Route::post('/planning/update', 'ProcurementController@planning_update')->name('planning/update');
// Construction
Route::get('/construction/list/{id?}', 'ProcurementController@construction_list')->name('construction/list');
Route::get('/construction/create', 'ProcurementController@construction_create')->name('construction/create');
Route::post('/construction/store', 'ProcurementController@construction_store')->name('construction/store');
Route::get('/construction/show/{rec_id},{plan_id}', 'ProcurementController@construction_show')->name('construction/show');
Route::get('/construction/edit/{id?}', 'ProcurementController@construction_edit')->name('construction/edit');
Route::post('/construction/update', 'ProcurementController@construction_update')->name('construction/update');

/*** Contracts ***/
Route::get('/contracts', 'ContractsController@index')->name('contracts');
Route::get('/contract_notification/{id?}', 'ContractsController@contract_notification')->name('contract_notification');
Route::get('/filter_contracts', 'ContractsController@filter_contracts')->name('filter_contracts');
Route::get('/progression/{id?}', 'ContractsController@progression')->name('progression');
Route::get('/modification/{id?}', 'ContractsController@modification')->name('modification');
Route::get('/estimate/{id?}', 'ContractsController@estimate')->name('estimate');
Route::get('/termination/{id?}', 'ContractsController@termination')->name('termination');
/*** Contracts->Progression ***/
Route::get('/loadProgression', 'ContractsController@loadProgression')->name('loadProgression');
Route::post('/addProgression', 'ContractsController@addProgression')->name('addProgression');
Route::post('/doAddProgression', 'ContractsController@doAddProgression')->name('doAddProgression');
Route::post('/viewProgression', 'ContractsController@viewProgression')->name('viewProgression');
Route::post('/editProgression', 'ContractsController@editProgression')->name('editProgression');
Route::post('/doEditProgression', 'ContractsController@doEditProgression')->name('doEditProgression');
/*** Contracts->Modification ***/
Route::get('/loadModification', 'ContractsController@loadModification')->name('loadModification');
Route::post('/addModification', 'ContractsController@addModification')->name('addModification');
Route::post('/doAddModification', 'ContractsController@doAddModification')->name('doAddModification');
Route::post('/viewModification', 'ContractsController@viewModification')->name('viewModification');
Route::post('/editModification', 'ContractsController@editModification')->name('editModification');
Route::post('/doEditModification', 'ContractsController@doEditModification')->name('doEditModification');
/*** Contracts->Estimate ***/
Route::get('/loadEstimate', 'ContractsController@loadEstimate')->name('loadEstimate');
Route::post('/addEstimate', 'ContractsController@addEstimate')->name('addEstimate');
Route::post('/doAddEstimate', 'ContractsController@doAddEstimate')->name('doAddEstimate');
Route::post('/viewEstimate', 'ContractsController@viewEstimate')->name('viewEstimate');
Route::post('/editEstimate', 'ContractsController@editEstimate')->name('editEstimate');
Route::post('/doEditEstimate', 'ContractsController@doEditEstimate')->name('doEditEstimate');
/*** Contracts->Termination ***/
Route::get('/loadTermination', 'ContractsController@loadTermination')->name('loadTermination');
Route::post('/addTermination', 'ContractsController@addTermination')->name('addTermination');
Route::post('/doAddTermination', 'ContractsController@doAddTermination')->name('doAddTermination');
Route::post('/viewTermination', 'ContractsController@viewTermination')->name('viewTermination');
Route::post('/editTermination', 'ContractsController@editTermination')->name('editTermination');
Route::post('/doEditTermination', 'ContractsController@doEditTermination')->name('doEditTermination');

/*** Progress->Daily Reports ***/
// Route::get('/daily_report/{id?}', 'ProgressController@daily_report')->name('daily_report');
// Route::get('/load_daily_report', 'ProgressController@load_daily_report')->name('load_daily_report');
// Route::post('/add_daily_report', 'ProgressController@add_daily_report')->name('add_daily_report');
// Route::post('/store_daily_report', 'ProgressController@store_daily_report')->name('store_daily_report');
// Route::post('/view_daily_report', 'ProgressController@view_daily_report')->name('view_daily_report');
// Route::post('/edit_daily_report', 'ProgressController@edit_daily_report')->name('edit_daily_report');
// Route::post('/doEdit_daily_report', 'ProgressController@doEdit_daily_report')->name('doEdit_daily_report');
Route::post('/bringOtherStaff', 'ProgressController@bringOtherStaff')->name('bringOtherStaff');
Route::post('/bringOtherWorkers', 'ProgressController@bringOtherWorkers')->name('bringOtherWorkers');
Route::post('/bringOtherMachines', 'ProgressController@bringOtherMachines')->name('bringOtherMachines');
Route::post('/bringOtherPreActivities', 'ProgressController@bringOtherPreActivities')->name('bringOtherPreActivities');
Route::post('/bringOtherPlaActivities', 'ProgressController@bringOtherPlaActivities')->name('bringOtherPlaActivities');
Route::post('/bringOtherMaterials', 'ProgressController@bringOtherMaterials')->name('bringOtherMaterials');
Route::post('/bringOtherVisitors', 'ProgressController@bringOtherVisitors')->name('bringOtherVisitors');
/*** Progress->Necessities ***/
Route::get('/necessities/{id?}', 'ProgressController@necessities')->name('necessities');
Route::get('/loadNecessities', 'ProgressController@loadNecessities')->name('loadNecessities');
Route::post('/addNecessities', 'ProgressController@addNecessities')->name('addNecessities');
Route::post('/storeNecessities', 'ProgressController@storeNecessities')->name('storeNecessities');
Route::post('/viewNecessities', 'ProgressController@viewNecessities')->name('viewNecessities');
Route::post('/editNecessities', 'ProgressController@editNecessities')->name('editNecessities');
Route::post('/storeEditNecessities', 'ProgressController@storeEditNecessities')->name('storeEditNecessities');
/*** Progress->Documents ***/
Route::get('/documents/{id?}', 'ProgressController@documents')->name('documents');
Route::post('/store_documents', 'ProgressController@store_documents')->name('store_documents');
Route::get('/DownloadReportDoc/{id},{table}', 'ProgressController@DownloadReportDoc')->name('DownloadReportDoc');
/*** Progress->Suspended ***/
Route::get('/suspended/{id?}', 'ProgressController@suspended')->name('suspended');
Route::get('/loadSuspended', 'ProgressController@loadSuspended')->name('loadSuspended');
Route::post('/addSuspended', 'ProgressController@addSuspended')->name('addSuspended');
Route::post('/storeSuspended', 'ProgressController@storeSuspended')->name('storeSuspended');
Route::post('/viewSuspended', 'ProgressController@viewSuspended')->name('viewSuspended');
Route::post('/editSuspended', 'ProgressController@editSuspended')->name('editSuspended');
Route::post('/storeEditSuspended', 'ProgressController@storeEditSuspended')->name('storeEditSuspended');
/*** Progress->Installments ***/
Route::get('/installments/{id?}', 'ProgressController@installments')->name('installments');
Route::get('/loadInstallments', 'ProgressController@loadInstallments')->name('loadInstallments');
Route::post('/addInstallments', 'ProgressController@addInstallments')->name('addInstallments');
Route::post('/storeInstallments', 'ProgressController@storeInstallments')->name('storeInstallments');
Route::post('/viewInstallments', 'ProgressController@viewInstallments')->name('viewInstallments');
Route::post('/editInstallments', 'ProgressController@editInstallments')->name('editInstallments');
Route::post('/storeEditInstallments', 'ProgressController@storeEditInstallments')->name('storeEditInstallments');

/*** Progress->Project status ***/
Route::get('/change_status', 'ProgressController@change_status')->name('change_status');
/*** Completed Projects ***/
Route::get('/completed', 'CompletedController@index')->name('completed');
Route::get('/filter_completed', 'CompletedController@filter_completed')->name('filter_completed');
Route::get('/com_report/{id?}', 'CompletedController@com_report')->name('com_report');
Route::get('/com_report_list', 'CompletedController@com_report_list')->name('com_report_list');
Route::post('/com_report_view', 'CompletedController@com_report_view')->name('com_report_view');
/*** Completed Projects->Necessities ***/
Route::get('/com_necessities/{id?}', 'CompletedController@com_necessities')->name('com_necessities');
Route::get('/com_necessities_list', 'CompletedController@com_necessities_list')->name('com_necessities_list');
Route::post('/com_necessities_view', 'CompletedController@com_necessities_view')->name('com_necessities_view');
/*** Completed Projects->Documents ***/
Route::get('/com_documents/{id?}', 'CompletedController@com_documents')->name('com_documents');
Route::get('/com_documents_download/{id},{table}', 'CompletedController@com_documents_download')->name('com_documents_download');
/*** Completed Projects->Suspended ***/
Route::get('/com_suspended/{id?}', 'CompletedController@com_suspended')->name('com_suspended');
Route::get('/com_suspended_list', 'CompletedController@com_suspended_list')->name('com_suspended_list');
Route::post('/com_suspended_view', 'CompletedController@com_suspended_view')->name('com_suspended_view');
/*** Completed Projects->Installments ***/
Route::get('/com_installments/{id?}', 'CompletedController@com_installments')->name('com_installments');
Route::get('/com_installments_list', 'CompletedController@com_installments_list')->name('com_installments_list');
Route::post('/com_installments_view', 'CompletedController@com_installments_view')->name('com_installments_view');
/*** Completed Projects->BQChart ***/
Route::get('/com_BQChart/{id?}', 'CompletedController@com_BQChart')->name('com_BQChart');

/*** Suspended Projects ***/
Route::get('/pro_suspended', 'SuspendedController@index')->name('pro_suspended');
Route::get('/filter_pro_suspended', 'SuspendedController@filter_pro_suspended')->name('filter_pro_suspended');
Route::get('/sus_report/{id?}', 'SuspendedController@sus_report')->name('sus_report');
Route::get('/sus_report_list', 'SuspendedController@sus_report_list')->name('sus_report_list');
Route::post('/sus_report_view', 'SuspendedController@sus_report_view')->name('sus_report_view');
/*** Suspended Projects->Necessities ***/
Route::get('/sus_necessities/{id?}', 'SuspendedController@sus_necessities')->name('sus_necessities');
Route::get('/sus_necessities_list', 'SuspendedController@sus_necessities_list')->name('sus_necessities_list');
Route::post('/sus_necessities_view', 'SuspendedController@sus_necessities_view')->name('sus_necessities_view');
/*** Suspended Projects->Documents ***/
Route::get('/sus_documents/{id?}', 'SuspendedController@sus_documents')->name('sus_documents');
Route::get('/sus_documents_download/{id},{table}', 'SuspendedController@sus_documents_download')->name('sus_documents_download');
/*** Suspended Projects->Suspended ***/
Route::get('/sus_suspended/{id?}', 'SuspendedController@sus_suspended')->name('sus_suspended');
Route::get('/sus_suspended_list', 'SuspendedController@sus_suspended_list')->name('sus_suspended_list');
Route::post('/sus_suspended_view', 'SuspendedController@sus_suspended_view')->name('sus_suspended_view');
/*** Suspended Projects->Installments ***/
Route::get('/sus_installments/{id?}', 'SuspendedController@sus_installments')->name('sus_installments');
Route::get('/sus_installments_list', 'SuspendedController@sus_installments_list')->name('sus_installments_list');
Route::post('/sus_installments_view', 'SuspendedController@sus_installments_view')->name('sus_installments_view');
/*** Suspended Projects->BQChart ***/
Route::get('/sus_BQChart/{id?}', 'SuspendedController@sus_BQChart')->name('sus_BQChart');

/*** Terminated Projects ***/
Route::get('/terminated', 'TerminatedController@index')->name('terminated');
Route::get('/filter_terminated', 'TerminatedController@filter_terminated')->name('filter_terminated');
Route::get('/ter_report/{id?}', 'TerminatedController@ter_report')->name('ter_report');
Route::get('/ter_report_list', 'TerminatedController@ter_report_list')->name('ter_report_list');
Route::post('/ter_report_view', 'TerminatedController@ter_report_view')->name('ter_report_view');
/*** Terminated Projects->Necessities ***/
Route::get('/ter_necessities/{id?}', 'TerminatedController@ter_necessities')->name('ter_necessities');
Route::get('/ter_necessities_list', 'TerminatedController@ter_necessities_list')->name('ter_necessities_list');
Route::post('/ter_necessities_view', 'TerminatedController@ter_necessities_view')->name('ter_necessities_view');
/*** Terminated Projects->Documents ***/
Route::get('/ter_documents/{id?}', 'TerminatedController@ter_documents')->name('ter_documents');
Route::get('/ter_documents_download/{id},{table}', 'TerminatedController@ter_documents_download')->name('ter_documents_download');
/*** Terminated Projects->Suspended ***/
Route::get('/ter_suspended/{id?}', 'TerminatedController@ter_suspended')->name('ter_suspended');
Route::get('/ter_suspended_list', 'TerminatedController@ter_suspended_list')->name('ter_suspended_list');
Route::post('/ter_suspended_view', 'TerminatedController@ter_suspended_view')->name('ter_suspended_view');
/*** Terminated Projects->Installments ***/
Route::get('/ter_installments/{id?}', 'TerminatedController@ter_installments')->name('ter_installments');
Route::get('/ter_installments_list', 'TerminatedController@ter_installments_list')->name('ter_installments_list');
Route::post('/ter_installments_view', 'TerminatedController@ter_installments_view')->name('ter_installments_view');
/*** Terminated Projects->BQChart ***/
Route::get('/ter_BQChart/{id?}', 'TerminatedController@ter_BQChart')->name('ter_BQChart');

/*** Notification ***/
Route::get('/markAsRead','HomeController@markAsRead')->name('markAsRead');
/*** Settings ***/
Route::get('/statics', 'Settings\Statics\StaticsController@index')->name('statics');
Route::get('/viewStatics', 'Settings\Statics\StaticsController@viewStatics')->name('viewStatics');
/*** Static table of Categories ***/
Route::post('/addCategory', 'Settings\Statics\CategoriesController@addCategory')->name('addCategory');
Route::post('/storeCategory', 'Settings\Statics\CategoriesController@storeCategory')->name('storeCategory');
Route::post('/viewCategory', 'Settings\Statics\CategoriesController@viewCategory')->name('viewCategory');
Route::post('/editCategory', 'Settings\Statics\CategoriesController@editCategory')->name('editCategory');
Route::post('/doEditCategory', 'Settings\Statics\CategoriesController@doEditCategory')->name('doEditCategory');
Route::post('/deleteCategory', 'Settings\Statics\CategoriesController@deleteCategory')->name('deleteCategory');
/*** Static table of doc_type ***/
Route::post('/addDocument', 'Settings\Statics\DocumentsController@addDocument')->name('addDocument');
Route::post('/storeDocument', 'Settings\Statics\DocumentsController@storeDocument')->name('storeDocument');
Route::post('/viewDocument', 'Settings\Statics\DocumentsController@viewDocument')->name('viewDocument');
Route::post('/editDocument', 'Settings\Statics\DocumentsController@editDocument')->name('editDocument');
Route::post('/doEditDocument', 'Settings\Statics\DocumentsController@doEditDocument')->name('doEditDocument');
Route::post('/deleteDocument', 'Settings\Statics\DocumentsController@deleteDocument')->name('deleteDocument');
/*** Static table of project_types ***/
Route::post('/addProject', 'Settings\Statics\ProjectsController@addProject')->name('addProject');
Route::post('/storeProject', 'Settings\Statics\ProjectsController@storeProject')->name('storeProject');
Route::post('/viewProject', 'Settings\Statics\ProjectsController@viewProject')->name('viewProject');
Route::post('/editProject', 'Settings\Statics\ProjectsController@editProject')->name('editProject');
Route::post('/doEditProject', 'Settings\Statics\ProjectsController@doEditProject')->name('doEditProject');
Route::post('/deleteProject', 'Settings\Statics\ProjectsController@deleteProject')->name('deleteProject');
/*** Static table of units ***/
Route::post('/addUnits', 'Settings\Statics\UnitsController@addUnits')->name('addUnits');
Route::post('/storeUnits', 'Settings\Statics\UnitsController@storeUnits')->name('storeUnits');
Route::post('/viewUnits', 'Settings\Statics\UnitsController@viewUnits')->name('viewUnits');
Route::post('/editUnits', 'Settings\Statics\UnitsController@editUnits')->name('editUnits');
Route::post('/doEditUnits', 'Settings\Statics\UnitsController@doEditUnits')->name('doEditUnits');
Route::post('/deleteUnits', 'Settings\Statics\UnitsController@deleteUnits')->name('deleteUnits');
/*** Static table of project_financial_source ***/
Route::post('/addFinancial_source', 'Settings\Statics\Financial_sourceController@addFinancial_source')->name('addFinancial_source');
Route::post('/storeFinancial_source', 'Settings\Statics\Financial_sourceController@storeFinancial_source')->name('storeFinancial_source');
Route::post('/viewFinancial_source', 'Settings\Statics\Financial_sourceController@viewFinancial_source')->name('viewFinancial_source');
Route::post('/editFinancial_source', 'Settings\Statics\Financial_sourceController@editFinancial_source')->name('editFinancial_source');
Route::post('/doEditFinancial_source', 'Settings\Statics\Financial_sourceController@doEditFinancial_source')->name('doEditFinancial_source');
Route::post('/deleteFinancial_source', 'Settings\Statics\Financial_sourceController@deleteFinancial_source')->name('deleteFinancial_source');
/*** Static table of employees ***/
Route::post('/addEmployee', 'Settings\Statics\EmployeesController@addEmployee')->name('addEmployee');
Route::post('/storeEmployee', 'Settings\Statics\EmployeesController@storeEmployee')->name('storeEmployee');
Route::post('/viewEmployee', 'Settings\Statics\EmployeesController@viewEmployee')->name('viewEmployee');
Route::post('/editEmployee', 'Settings\Statics\EmployeesController@editEmployee')->name('editEmployee');
Route::post('/doEditEmployee', 'Settings\Statics\EmployeesController@doEditEmployee')->name('doEditEmployee');
Route::post('/deleteEmployee', 'Settings\Statics\EmployeesController@deleteEmployee')->name('deleteEmployee');
/*** Static table of roof ***/
Route::post('/addRoof', 'Settings\Statics\RoofController@addRoof')->name('addRoof');
Route::post('/storeRoof', 'Settings\Statics\RoofController@storeRoof')->name('storeRoof');
Route::post('/viewRoof', 'Settings\Statics\RoofController@viewRoof')->name('viewRoof');
Route::post('/editRoof', 'Settings\Statics\RoofController@editRoof')->name('editRoof');
Route::post('/doEditRoof', 'Settings\Statics\RoofController@doEditRoof')->name('doEditRoof');
Route::post('/deleteRoof', 'Settings\Statics\RoofController@deleteRoof')->name('deleteRoof');
/*** Static table of fear ***/
Route::post('/addFear', 'Settings\Statics\FearController@addFear')->name('addFear');
Route::post('/storeFear', 'Settings\Statics\FearController@storeFear')->name('storeFear');
Route::post('/viewFear', 'Settings\Statics\FearController@viewFear')->name('viewFear');
Route::post('/editFear', 'Settings\Statics\FearController@editFear')->name('editFear');
Route::post('/doEditFear', 'Settings\Statics\FearController@doEditFear')->name('doEditFear');
Route::post('/deleteFear', 'Settings\Statics\FearController@deleteFear')->name('deleteFear');
/*** Static table of foundation ***/
Route::post('/addFoundation', 'Settings\Statics\FoundationController@addFoundation')->name('addFoundation');
Route::post('/storeFoundation', 'Settings\Statics\FoundationController@storeFoundation')->name('storeFoundation');
Route::post('/viewFoundation', 'Settings\Statics\FoundationController@viewFoundation')->name('viewFoundation');
Route::post('/editFoundation', 'Settings\Statics\FoundationController@editFoundation')->name('editFoundation');
Route::post('/doEditFoundation', 'Settings\Statics\FoundationController@doEditFoundation')->name('doEditFoundation');
Route::post('/deleteFoundation', 'Settings\Statics\FoundationController@deleteFoundation')->name('deleteFoundation');
/*** Static table of stair ***/
Route::post('/addStair', 'Settings\Statics\StairController@addStair')->name('addStair');
Route::post('/storeStair', 'Settings\Statics\StairController@storeStair')->name('storeStair');
Route::post('/viewStair', 'Settings\Statics\StairController@viewStair')->name('viewStair');
Route::post('/editStair', 'Settings\Statics\StairController@editStair')->name('editStair');
Route::post('/doEditStair', 'Settings\Statics\StairController@doEditStair')->name('doEditStair');
Route::post('/deleteStair', 'Settings\Statics\StairController@deleteStair')->name('deleteStair');
/*** Static table of wall ***/
Route::post('/addWall', 'Settings\Statics\WallController@addWall')->name('addWall');
Route::post('/storeWall', 'Settings\Statics\WallController@storeWall')->name('storeWall');
Route::post('/viewWall', 'Settings\Statics\WallController@viewWall')->name('viewWall');
Route::post('/editWall', 'Settings\Statics\WallController@editWall')->name('editWall');
Route::post('/doEditWall', 'Settings\Statics\WallController@doEditWall')->name('doEditWall');
Route::post('/deleteWall', 'Settings\Statics\WallController@deleteWall')->name('deleteWall');
/*** Static table of Wall_shape ***/
Route::post('/addWall_shape', 'Settings\Statics\Wall_shapeController@addWall_shape')->name('addWall_shape');
Route::post('/storeWall_shape', 'Settings\Statics\Wall_shapeController@storeWall_shape')->name('storeWall_shape');
Route::post('/viewWall_shape', 'Settings\Statics\Wall_shapeController@viewWall_shape')->name('viewWall_shape');
Route::post('/editWall_shape', 'Settings\Statics\Wall_shapeController@editWall_shape')->name('editWall_shape');
Route::post('/doEditWall_shape', 'Settings\Statics\Wall_shapeController@doEditWall_shape')->name('doEditWall_shape');
Route::post('/deleteWall_shape', 'Settings\Statics\Wall_shapeController@deleteWall_shape')->name('deleteWall_shape');
/*** Static table of column ***/
Route::post('/addColumn', 'Settings\Statics\ColumnController@addColumn')->name('addColumn');
Route::post('/storeColumn', 'Settings\Statics\ColumnController@storeColumn')->name('storeColumn');
Route::post('/viewColumn', 'Settings\Statics\ColumnController@viewColumn')->name('viewColumn');
Route::post('/editColumn', 'Settings\Statics\ColumnController@editColumn')->name('editColumn');
Route::post('/doEditColumn', 'Settings\Statics\ColumnController@doEditColumn')->name('doEditColumn');
Route::post('/deleteColumn', 'Settings\Statics\ColumnController@deleteColumn')->name('deleteColumn');
/*** Static table of column_shape ***/
Route::post('/addColumn_shape', 'Settings\Statics\Column_shapeController@addColumn_shape')->name('addColumn_shape');
Route::post('/storeColumn_shape', 'Settings\Statics\Column_shapeController@storeColumn_shape')->name('storeColumn_shape');
Route::post('/viewColumn_shape', 'Settings\Statics\Column_shapeController@viewColumn_shape')->name('viewColumn_shape');
Route::post('/editColumn_shape', 'Settings\Statics\Column_shapeController@editColumn_shape')->name('editColumn_shape');
Route::post('/doEditColumn_shape', 'Settings\Statics\Column_shapeController@doEditColumn_shape')->name('doEditColumn_shape');
Route::post('/deleteColumn_shape', 'Settings\Statics\Column_shapeController@deleteColumn_shape')->name('deleteColumn_shape');
/*** Static table of brick ***/
Route::post('/addBrick', 'Settings\Statics\BrickController@addBrick')->name('addBrick');
Route::post('/storeBrick', 'Settings\Statics\BrickController@storeBrick')->name('storeBrick');
Route::post('/viewBrick', 'Settings\Statics\BrickController@viewBrick')->name('viewBrick');
Route::post('/editBrick', 'Settings\Statics\BrickController@editBrick')->name('editBrick');
Route::post('/doEditBrick', 'Settings\Statics\BrickController@doEditBrick')->name('doEditBrick');
Route::post('/deleteBrick', 'Settings\Statics\BrickController@deleteBrick')->name('deleteBrick');

/*** Labor Clasification ***/
Route::resource('clasification','Settings\Statics\ClasificationController');
/*** Machinery ***/
Route::resource('machinery','Settings\Statics\MachineryController');

/***** Authentication Module Routes *****/

/*** Modules ***/
Route::resource('modules','Authentication\ModulesController');
Route::get('/modules', 'Authentication\ModulesController@index')->name('modules');
Route::get('/filterModule', 'Authentication\ModulesController@filterModule')->name('filterModule');

/*** Organizations ***/
Route::resource('organizations','Authentication\OrganizationsController');
Route::get('/organizations', 'Authentication\OrganizationsController@index')->name('organizations');
Route::get('/filterOrganization', 'Authentication\OrganizationsController@filterOrganization')->name('filterOrganization');

/*** Department ***/
Route::resource('departments','Authentication\DepartmentsController');
Route::get('/departments', 'Authentication\DepartmentsController@index')->name('departments');
Route::get('/filterDepartment', 'Authentication\DepartmentsController@filterDepartment')->name('filterDepartment');

/*** Sections ***/
Route::resource('sections','Authentication\SectionsController');
Route::get('/sections','Authentication\SectionsController@index')->name('sections');
Route::get('/filterSection', 'Authentication\SectionsController@filterSection')->name('filterSection');

/*** Roles ***/
Route::resource('roles','Authentication\RolesController');
Route::get('roles','Authentication\RolesController@index')->name('roles');
Route::get('/filterRole','Authentication\RolesController@filterRole')->name('filterRole');

/*** Users ***/
Route::resource('users','Authentication\UsersController');
Route::get('/users', 'Authentication\UsersController@index')->name('users');
Route::get('/filterUser', 'Authentication\UsersController@filterUser')->name('filterUser');
Route::post('/uploadPic', 'Authentication\UsersController@uploadPic')->name('uploadPic');
Route::post('/getModulesByDepartment', 'Authentication\UsersController@getModulesByDepartment')->name('getModulesByDepartment');
Route::post('/getSectionsByModule', 'Authentication\UsersController@getSectionsByModule')->name('getSectionsByModule');
Route::post('/getRolesBySections', 'Authentication\UsersController@getRolesBySections')->name('getRolesBySections');
Route::post('/editPassword', 'Authentication\UsersController@editPassword')->name('editPassword');

/*** Searcch ***/
Route::get('/report', 'ReportController@index')->name('report');
Route::get('/viewReport', 'ReportController@viewReport')->name('viewReport');
Route::get('/report_request', 'ReportController@report_request')->name('report_request');
Route::get('/report_plan', 'ReportController@report_plan')->name('report_plan');
Route::post('/bringProjectTypesReport', 'ReportController@bringProjectTypesReport')->name('bringProjectTypesReport');
Route::get('/report_survey', 'ReportController@report_survey')->name('report_survey');
Route::get('/report_design', 'ReportController@report_design')->name('report_design');


/*** Request Share Route ***/
// Route::resource('/request_share','RequestShareController')->except(['update']);

/*** Share Route ***/
Route::resource('/share','ShareController')->except(['update']);

/** Attachments */
Route::post('attachment/destroy','HomeController@destoryAttachment')->name('attachment.destroy');
Route::post('attachment/update','HomeController@editAttachment')->name('attachment.edit');
Route::post('attachment/create','HomeController@createAttachment')->name('attachment.create');
Route::get('attachment/view/{id},{table}', 'HomeController@viewAttachment')->name('attachment.view');

/** Documents */
Route::post('document/update','HomeController@editDocument')->name('document.edit');
Route::post('document/create','HomeController@createDocument')->name('document.create');
Route::post('document/store','HomeController@storeDocument')->name('document.store');


/*** Summary Route ***/
Route::resource('/summary','SummaryController');
Route::get('summary/show_survey/{rec_id},{parent_id}', 'SummaryController@show_survey')->name('summary.show_survey');
Route::get('tab_architecture/{id?}', 'SummaryController@tab_architecture')->name('tab_architecture');
Route::get('tab_structure/{id?}', 'SummaryController@tab_structure')->name('tab_structure');
Route::get('tab_electricity/{id?}', 'SummaryController@tab_electricity')->name('tab_electricity');
Route::get('tab_water/{id?}', 'SummaryController@tab_water')->name('tab_water');
Route::get('tab_mechanic/{id?}', 'SummaryController@tab_mechanic')->name('tab_mechanic');
Route::get('summary/show_BQ/{id?}', 'SummaryController@show_BQ')->name('summary.show_BQ');

/** Contractors */
Route::resource('contractors','Settings\ContractorController');
Route::get('/contractors', 'Settings\ContractorController@index')->name('contractors');
Route::get('/filterContractors', 'Settings\ContractorController@filterContractors')->name('filterContractors');

/*** Project Setting Routes ***/
Route::resource('projects','Settings\ProjectsController');
Route::get('/bringProjects/{id}', 'Settings\ProjectsController@bringProjects')->name('bringProjects');
Route::get('/projects/list/{id}', 'Settings\ProjectsController@list')->name('projects.list');
Route::get('/filter_projects', 'Settings\ProjectsController@filter_projects')->name('filter_projects');
Route::get('/projects/create_staff/{id}', 'Settings\ProjectsController@create_staff')->name('projects.create_staff');
Route::post('/projects/store_staff', 'Settings\ProjectsController@store_staff')->name('projects.store_staff');


/** Contractors Staff */
Route::resource('contractor_staff', 'Settings\ContractorStaffController');
Route::get('/contractor_staff', 'Settings\ContractorStaffController@index')->name('contractor_staff');
Route::get('/filter_contractor_staff', 'Settings\ContractorStaffController@filter_contractor_staff')->name('filter_contractor_staff');
Route::post('/contractor_modulesByDepartment', 'Settings\ContractorStaffController@modulesByDepartment')->name('contractor_modulesByDepartment');
Route::post('/contractor_sectionsByModule', 'Settings\ContractorStaffController@sectionsByModule')->name('contractor_sectionsByModule');
Route::post('/contractor_rolesBySections', 'Settings\ContractorStaffController@rolesBySections')->name('contractor_rolesBySections');

/*** Routes of Contractor's Project ***/
Route::resource('contractor_projects','ContractorProjectController')->except(['index']);
Route::get('/contractor_project/{con_id}/{dep_id}','ContractorProjectController@index')->name('contractor_project');
Route::get('/contractor_projects/view/{id}/{dep_id}','ContractorProjectController@view')->name('contractor_projects.view');
Route::get('/filter_contractor_project','ContractorProjectController@filter_contractor_project')->name('filter_contractor_project');

/*** Routes of Work Categroy Controller ***/
Route::resource('work_category','Settings\WorkCategoryController');
Route::get('/work_category','Settings\WorkCategoryController@index')->name('work_category');
Route::get('/filterWorkCategory','Settings\WorkCategoryController@filterWorkCategory')->name('filterWorkCategory');

/*** Routes of Feature of Work  Controller ***/
Route::resource('feature_of_work','Settings\FeatureOfWorkController');
Route::get('/feature_of_work','Settings\FeatureOfWorkController@index')->name('feature_of_work');
Route::get('/filterFeatureOfWork','Settings\FeatureOfWorkController@filterFeatureOfWork')->name('filterFeatureOfWork');


/*** Routes of Inspection  Controller ***/
Route::resource('inspection','Settings\InspectionController');
Route::get('/inspection','Settings\InspectionController@index')->name('inspection');
Route::get('/filterInspection','Settings\InspectionController@filterInspection')->name('filterInspection');
Route::post('/more_inspection_sub','Settings\InspectionController@more_inspection_sub')->name('more_inspection_sub');
Route::get('/getBillOfQuantity/{id}/{dep_id}','Settings\InspectionController@getBillOfQuantity')->name('getBillOfQuantity');

/*** Routes of Inspection  Controller ***/
Route::resource('three_phase_Inspection','ThreePhaseInspectionController')->except(['index','show']);
Route::get('/three_phase_Inspection/show/{pro_id}/{bq_id}/{loc_id}','ThreePhaseInspectionController@show')->name('three_phase_Inspection/show');
Route::get('/three_phase_Inspection/{id}/{dep_id}','ThreePhaseInspectionController@index')->name('three_phase_Inspection');
Route::get('/three_phase_Inspection/list/{id}/{dep_id}/{loc_id}','ThreePhaseInspectionController@list')->name('three_phase_Inspection/list');
Route::get('/getTreeView','ThreePhaseInspectionController@getTreeView')->name('getTreeView');
Route::post('/getSubInepection','ThreePhaseInspectionController@getSubInepection')->name('getSubInepection');
Route::get('/getInspectionDetails','ThreePhaseInspectionController@getInspectionDetails')->name('getInspectionDetails');


/*** Routes of Schedules ***/
Route::resource('schedules','SchedulesController')->except(['index']);
Route::get('/schedule/{enc_id}','SchedulesController@index')->name('schedule');
Route::get('/schedule/list/{id}/{loc_id}', 'SchedulesController@list')->name('schedule/list');

/*** Progress ***/
Route::get('/progress/{id?}', 'ProgressController@index')->name('progress');
Route::get('/filter_progress', 'ProgressController@filter_progress')->name('filter_progress');


/*** Routes of BQ Schedules ***/
Route::resource('BQSchedules','BQSchedulesController')->except(['index']);
Route::get('/BQSchedule/{id}/{dep_id}','BQSchedulesController@index')->name('BQSchedule');
Route::get('/BQSchedule/list/{id}/{dep_id}/{loc_id}','BQSchedulesController@list')->name('BQSchedule/list');
Route::get('/progress_notification/{id?}', 'ProgressController@progress_notification')->name('progress_notification');

/** Daily Reports route   */
Route::resource('daily_report','DailyReportController');
Route::post('daily_report/weather','DailyReportController@weather')->name('weather.index');
Route::post('daily_report/weather/store','DailyReportController@storeWeather')->name('daily_weather.store');
Route::post('daily_report/weather/update','DailyReportController@updateWeather')->name('daily_weather.update');

Route::post('daily_report/qcnarrative','DailyReportController@qcNarrative')->name('qcnarrative.index');
Route::post('daily_report/qcnarrative/store','DailyReportController@qcNarrativeStore')->name('qcnarrative.store');
Route::post('daily_report/qcnarrative/edit','DailyReportController@qcNarrativeEdit')->name('qcnarrative.edit');
Route::post('daily_report/qcnarrative/update','DailyReportController@qcNarrativeUpdate')->name('qcnarrative.update');

Route::post('daily_report/qctest','DailyReportController@qcTest')->name('qctest.index');
Route::post('daily_report/qctest/store', 'DailyReportController@qcTestStore')->name('qcTest.store');
Route::post('daily_report/qctest/edit', 'DailyReportController@qcTestEdit')->name('qcTest.edit');
Route::post('daily_report/qctest/update', 'DailyReportController@qcTestUpdate')->name('qcTest.update');
Route::post('daily_report/qctest/delete', 'DailyReportController@qcTestDelete')->name('qcTest.delete');

Route::post('daily_report/activities','DailyReportController@activities')->name('activities.index');
Route::post('daily_report/activities/store', 'DailyReportController@activitiesStore')->name('activities.store');
Route::post('daily_report/activities/edit', 'DailyReportController@activitiesEdit')->name('activities.edit');
Route::post('daily_report/activities/update', 'DailyReportController@activitiesUpdate')->name('activities.update');
Route::post('daily_report/activities/delete', 'DailyReportController@activitiesDelete')->name('activities.delete');

Route::post('daily_report/location','DailyReportController@location')->name('location.index');
Route::post('daily_report/location/store', 'DailyReportController@locationStore')->name('location.store');
Route::post('daily_report/location/update','DailyReportController@locationUpdate')->name('location.update');

Route::post('daily_report/equipment','DailyReportController@equipment')->name('equipment.index');
Route::post('aily_report/equipment/store', 'DailyReportController@equipmentStore')->name('equipment.store');
Route::post('aily_report/equipment/edit', 'DailyReportController@equipmentEdit')->name('equipment.edit');
Route::post('aily_report/equipment/update', 'DailyReportController@equipmentUpdate')->name('equipment.update');
Route::post('aily_report/equipment/delete', 'DailyReportController@equipmentDelete')->name('equipment.delete');

Route::post('daily_report/labor','DailyReportController@labor')->name('labor.index');
Route::post('daily_report/labor/store', 'DailyReportController@laborStore')->name('labor.store');
Route::post('daily_report/labor/edit', 'DailyReportController@laborEdit')->name('labor.edit');
Route::post('daily_report/labor/update', 'DailyReportController@laborUpdate')->name('labor.update');
Route::post('daily_report/labor/delete', 'DailyReportController@laborDelete')->name('labor.delete');

Route::post('daily_report/deficiencey_item','DailyReportController@deficiencey_item')->name('deficiencey_item.index');
Route::post('/bringUnit', 'DailyReportController@bringUnit')->name('bringUnit');
Route::post('/bringAmount', 'DailyReportController@bringAmount')->name('bringAmount');

/*** Accidents ***/
Route::post('daily_report/accidents','DailyReportController@accidents')->name('accidents.index');
Route::post('daily_report/accidents/store','DailyReportController@accidentsStore')->name('accidents.store');
Route::post('daily_report/accidents/edit','DailyReportController@accidentsEdit')->name('accidents.edit');
Route::post('daily_report/accidents/update','DailyReportController@accidentsUpdate')->name('accidents.update');


/*** Hazard Analysis ***/
Route::resource('hazard_analysis','HazardAnalysisController')->except(['index','show']);
Route::get('/hazard_analysis/{id}/{dep_id}','HazardAnalysisController@index')->name('hazard_analysis');
Route::get('/hazard_analysis/list/{id}/{dep_id}/{loc_id}','HazardAnalysisController@list')->name('hazard_analysis/list');
Route::get('/hazard_analysis/show/{pro_id}/{bq_id}/{loc_id}','HazardAnalysisController@show')->name('hazard_analysis/show');
Route::post('/hazard_analysis/store_sub','HazardAnalysisController@store_sub')->name('hazard_analysis/store_sub');
Route::post('/more_hazard_analysis','HazardAnalysisController@more_hazard_analysis')->name('more_hazard_analysis');
Route::post('/store_hazard_inspection','HazardAnalysisController@store_hazard_inspection')->name('store_hazard_inspection');
Route::post('/get_Hazard_Analysis_Data','HazardAnalysisController@get_Hazard_Analysis_Data')->name('get_Hazard_Analysis_Data');

/*** Charts ***/
Route::resource('charts','ChartsController')->except(['index']);
Route::get('charts/index/{id}/{dep_id}','ChartsController@index')->name('charts/index');
Route::get('charts/BQChart/{id}/{dep_id}/{loc_id}','ChartsController@BQChart')->name('charts/BQChart');

Route::get('/billQuantityChart/{id?}', 'ChartsController@billQuantityChart')->name('billQuantityChart');
/**
 * Contractore report route
 */
Route::get('report/contractor/index','ReportController@index')->name('report.index');
Route::post('report/contractor/show','ReportController@show')->name('contractor_report.show');
Route::post('report/contractor/search','DailyReportController@search')->name('contractor_report.search');
Route::post('project/location/option','ReportController@getPorjectLocation')->name('project_location.option');

/*** Bring search option route ***/
Route::post('search/option','HomeController@SearchOption')->name('search.option');