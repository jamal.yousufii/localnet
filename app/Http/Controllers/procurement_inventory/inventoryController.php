<?php 

namespace App\Http\Controllers\procurement_inventory;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\models\procurement_inventory\inventoryModel;
use Illuminate\Support\Collection;
use App\library\jdatetime;
use View;
use DB;
use Validator;
use Input;
use Redirect;
use Response;
use Datatable;
use Session;
use DateTime;
use DateInterval;
use File;
use Illuminate\Support\Str;
use URL;
use Carbon\Carbon;

class inventoryController extends Controller
{
		
//================================================= Autocomplete Functions =======================================================
	//get the data list based on table name and field name for autocomplete fields.
	public function getAutoCompleteListData($table="", $field="")
	{
        $q = strtolower(Input::get('term'));
		
		if (!$q) return;
		$options = DB::connection("procurement_inventory")->table($table)->select($field)->groupBy($field)->get();
		if (!$options) return;
		$json_array = array();
		foreach ($options as $key) 
		{
			if (strpos(strtolower($key->$field), $q) !== false) 
			{
				//echo "$key->company_name\n";
				array_push($json_array, $key->$field);
			}
		}
		echo json_encode($json_array);
	}

//================================================= Inventory Controller Functions ===================================
	
	//Load task/case form
	public function loadInventoryList()
	{
		$data['records'] = inventoryModel::getData("");
		//dd($data['records']);
		if(Input::get('ajax') == 1)
		{
			return View::make('procurement_inventory.inventory_list_ajax',$data);
		}
		else
		{
			//load view to show searchpa result
			return View::make('procurement_inventory.inventoryList', $data);
		}
		
	}
	//load the searched inventories based on product_type.
	public function loadSearchedInventories()
	{
		$department = Input::get('department');
		$person_name = Input::get('person_name');
		//dd($_POST);
		$data['records'] = inventoryModel::getData();
		$data['stock_in_total'] = inventoryModel::getStocksTotal($department,$person_name,'stock_in');
		$data['stock_out_total'] = inventoryModel::getStocksTotal($department,$person_name,'stock_out');
		if(Input::get('ajax') == 1)
		{
			return View::make('procurement_inventory.searchedInventoryList_ajax',$data);
		}
		else
		{
			return View::make('procurement_inventory.searchedInventoryList', $data);
		}
		
	}
	//get the stock in last value based on product type.
	public function getStockInByProductType()
	{
		$product_type = Input::get('product_type');
		$stock_in = inventoryModel::getStockInByProductType($product_type);
		echo $stock_in;
	}
	//Load Inventory form
	public function loadInventoryForm()
	{
		return View::make('procurement_inventory.inventory_form');
	}
	// Add Inventory form data.
	public function addInventoryFormData(Request $request)
	{
		//validate the input fields
	    $this->validate($request,[
	        "tag_number" 					=> "required|unique:procurement_inventory.inventory,tag_number",
	        "product_name" 					=> "required",
	        "person_name"					=> "required"
	        ]
	    );
	    //print_r($_POST);exit;
	    if(Input::get('product_type') == 0)
	    {
	    	//check if product_type doesn't exist in it's table then insert it and get it's id.
	    	$product_type = checkProcInventoryProductType(Input::get('other_product'));
	    	//echo $product_type;exit;
	    }else{
	    	$product_type = Input::get('product_type');
	    }
	    //check the date type if it's shamsi or miladi.
		if(isMiladiDate()){
			$date = Input::get('date');
			$m7_date = Input::get('m7_date');
		}else{
			$date = toGregorian(ymd_format(Input::get('date')));
			$m7_date = toGregorian(ymd_format(Input::get('m7_date')));
		}
	    //$balance = Input::get('stock_in') - Input::get('stock_out');
	    // get the form data.
	    $data = array(
	    		"tag_number"			=> Input::get('tag_number'),
	    		"serial_number"			=> Input::get('serial_number'),
	    		"product_name"			=> Input::get('product_name'),
	    		"department"			=> Input::get('department'),
	    		"product_type"			=> $product_type,
	    		"model"					=> Input::get('model'),
	    		"status"				=> Input::get('status'),
	    		"stock_in"				=> Input::get('stock_in'),
	    		"stock_out"				=> Input::get('stock_out'),
	    		"person_name"			=> Input::get('person_name'),
	    		"date"					=> $date,
	    		"masrafi"				=> Input::get('masrafi'),
	    		"item_register_page"	=> Input::get('item_register_page'),
	    		"cost"					=> Input::get('cost'),
	    		"total_cost"			=> Input::get('total_cost'),
	    		"m7"					=> Input::get('m7'),
	    		"m7_date"				=> $m7_date,
	    		"fes_5"					=> Input::get('fes_5'),
	    		"remarks"				=> Input::get('remarks'),
	    		"user_id"				=> Auth::user()->id,
	    		"created_at"			=> date('Y-m-d H:i:s')
	    	);
	    $object_id = inventoryModel::addInventory($data);
	    if($object_id){
	    	
	    	// getting all of the post data
			$file = Input::file('file');
			if(Input::hasFile('file'))
			{
			  // validating each file.
			  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			  $validator = Validator::make(
	
			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'file' => 'required|max:10000|image|mimes:jpeg,png,jpg,gif'
			        ]
			  	);
	
				if($validator->passes())
				{
				  	
				    // path is root/uploads
				    $destinationPath = 'documents/procurement_inventory_attachments';
				    $original_filename = $file->getClientOriginalName();
				    $temp = explode(".", $original_filename);
			    	$extension = end($temp);
				    			  			    
			    	$lastFileId = $object_id;
				    			  			    
				    $filename = Input::get('product_name')."_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
	
				    $upload_success = $file->move($destinationPath, $filename);
	
				    if($upload_success) 
				    {
					   
					   $data = array(
		   					'file_name'				=> $filename,
		   					'original_file_name'	=> $original_filename,
		   					'inventory_id'			=> $object_id,
		   					'user_id'				=> Auth::user()->id,
		   					'created_at'			=> date('Y-m-d H:i:s')
		   				);
	
						DB::connection('procurement_inventory')->table('uploads')->insert($data);
					} 
					else 
					{
						// send back to the page with the input data and errors
						return Redirect::route('procInventoryForm')->withInput($validator)->withErrors();
					}
			    
				} 
				else 
				{
				    // redirect back with errors.
				    return Redirect::route('procInventoryForm')->withInput($validator)->withErrors();
				}
				  
			}
	    	
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'procurement_inventory',
				'record_id'=>$object_id,
				'user' => Auth::user()->username,
				'action' => "Added",
				'created_at' => date('Y-m-d H:i:s')
				);
			inventoryModel::addLog($Log);
	    	return Redirect::route("procInventoryList")->with("success","معلومات موفقانه اضافه گردید.");
        }
        else
        {
            return Redirect::route("procInventoryList")->with("fail","مشکل وجود دارد، لطفآ همرا با مسئول سیستم به تماس شوید");
        }
	}
	//loading the inventory edit page.
	public function loadInventoryEditForm($id=0)
	{
		$data['inventory'] = inventoryModel::getSpecificInventoryDetails($id);
		//dd($data['inventory']);
		return View::make('procurement_inventory.edit_form', $data);	
	}
	// updating the changes in case table.
	public function postEditInventoryForm(Request $request,$id)
	{
		//validate the input fields
	    $this->validate($request,[
	    	// "tag_number" 					=> "required",
	        "product_name" 					=> "required",
	        "person_name"					=> "required"
	        ]
	    );
	    //print_r($_POST);exit;
	    if(Input::get('product_type') == 0)
	    {
	    	//check if product_type doesn't exist in it's table then insert it and get it's id.
	    	$product_type = checkProcInventoryProductType(Input::get('other_product'));
	    	//echo $product_type;exit;
	    }else{
	    	$product_type = Input::get('product_type');
	    }
	    //check the date type if it's shamsi or miladi.
		if(isMiladiDate()){
			$date = Input::get('date');
			$m7_date = Input::get('m7_date');
		}else{
			$date = toGregorian(ymd_format(Input::get('date')));
			$m7_date = toGregorian(ymd_format(Input::get('m7_date')));
		}
	    // get the form data.
	    $data = array(
	    		"tag_number"			=> Input::get('tag_number'),
	    		"serial_number"			=> Input::get('serial_number'),
	    		"product_name"			=> Input::get('product_name'),
	    		"department"			=> Input::get('department'),
	    		"product_type"			=> $product_type,
	    		"model"					=> Input::get('model'),
	    		"status"				=> Input::get('status'),
	    		"stock_in"				=> Input::get('stock_in'),
	    		"stock_out"				=> Input::get('stock_out'),
	    		"person_name"			=> Input::get('person_name'),
	    		"date"					=> $date,
	    		"masrafi"				=> Input::get('masrafi'),
	    		"item_register_page"	=> Input::get('item_register_page'),
	    		"cost"					=> Input::get('cost'),
	    		"total_cost"			=> Input::get('total_cost'),
	    		"m7"					=> Input::get('m7'),
	    		"m7_date"				=> $m7_date,
	    		"fes_5"					=> Input::get('fes_5'),
	    		"remarks"				=> Input::get('remarks'),
	    		"user_id"				=> Auth::user()->id,
	    		"created_at"			=> date('Y-m-d H:i:s')
	    	);
	    $updated = inventoryModel::updateInventory($id,$data);
	    if($updated){
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'procurement_inventory',
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Edited",
				'created_at' => date('Y-m-d H:i:s')
				);
			inventoryModel::addLog($Log);
	    	return Redirect::route("procInventoryList")->with("success","معلومات موفقانه اصلاح گردین.");
        }
        else
        {
            return Redirect::route("procInventoryList")->with("fail","مشکل وجود دارد، لطفآ همرا با مسئول سیستم به تماس شوید");
        }
		
	}
	public function inventoryDetails()
	{
		$proc_inventory_id = Input::get('id');
		$data['inventory'] = inventoryModel::getDetails($proc_inventory_id);
		//dd($data['inventory']);
		$data['file'] = inventoryModel::getFile($proc_inventory_id);
		return View::make('procurement_inventory.inventory_details',$data);
		
	}
	//change the status of the inventory based on it's id to 1 if it's deleted.
	public function deleteInventory()
	{
		$id = Input::get('record_id');
		$deleted = inventoryModel::getDeleteInventory($id);
		//dd($deleted);
		if($deleted)
	    {
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'procurement_inventory',
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			inventoryModel::addLog($Log);
        }
	}

}

?>