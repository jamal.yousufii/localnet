<?php 

namespace App\Http\Controllers\com;
use App\Http\Controllers\Controller;
//use View;
use Auth;
use Illuminate\Http\Request;
use DB;
use Datatable;
use App\models\com\Received_doc;
use App\models\com\Sended_doc;
use App\models\com\Com;
use App\models\com\dep_list;
use App\models\com\doc_type;
use Illuminate\Support\Collection;
use Input;
use Response;
use Validator;
use Illuminate\Support\Str;
use Excel;

class comController extends Controller
{

	/**
     	* Instantiate a new UserController instance.
     	*/
	public function __construct()
	{
		if(!Auth::check())
		{
			return \Redirect::route('getLogin');
		}		
	}	
	//get report list
	public function getReceived()
	{
		return view('com.received_list');	
		
	}
	//get data for datatable
	public function getData()
	{		
		//get data from model
		$docs = Received_doc::getData();
		//dd($entities);
		$collection = new Collection($docs);
		return \Datatable::collection($collection)
			    ->showColumns('id','dossier_name','code','number','summary','dep_name','date')
			    ->addColumn('operation',function($option){
			    	$options = '';
					if(canEdit('com_received_docs'))
					{
						$options .= '<a href="'.route('getUpdateReceived_doc',$option->id).'" title="تجدید نمودن"><i class="glyphicon glyphicon-edit"></i></a> &nbsp;';
					}
					if(canDelete('com_received_docs'))
					{
						$options .= '|&nbsp;<a href="'.route('getDeleteReceived_doc',$option->id).'" onclick="javascript:return confirm(\'آیا میخواهید این ریکارد را حذف نمایید؟\');" title="حذف نمودن"><i class="fa fa-trash-o"></i></a>';
					}
					if(canView('com_received_docs'))
					{
						$options .= '|&nbsp;<a href="'.route('getDetailsReceived_doc',$option->id).'" title="نمایش"><i class="glyphicon glyphicon-eye-open"></i></a>';
					}
					return $options;
			    })
			    //->orderColumns('id')
			    ->make();	
		
	}
	public function create_received_doc()
	{
		if(canAdd('com_received_docs'))
		{
			//get all document types
			$data['doc_types'] = com::getAll_stable('doc_types','en');
			//get all priority types
			$data['priority_types'] = com::getAll_stable('priority_types','en');
			
			return view('com.createReceived_doc',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function postCreateReceived_doc(Request $request)
	{
		if(canAdd('com_received_docs'))
		{
			//validate the input fields
		    $this->validate($request,[
		        "dossier" => "required",
		        "priority" => 'required',
		        "date" => 'required'		        
		        ]
		    );
			$date = '';
			if($request['date']!='')
			{
		    	$date = gregorian_format($request['date']);
			}
			$sender = '';
			$new_sender = TRUE;
			if($request['sender'] != '')
			{
				$deps = com::getAll_stable('dep_lists','en');
				if($deps)
				{
					foreach($deps AS $dep)
					{
						if($dep->name == $request['sender'])
						{
							$sender = $dep->id;
							$new_sender = FALSE;
							break;
						}
					}
				}
				if($new_sender)
				{
					$sender_id = new dep_list;
					$sender_id->name_en = $request['sender'];
					$sender_id->name_dr = $request['sender'];
					$sender_id->name_pa = $request['sender'];
					$sender_id->created_by = Auth::user()->id;
					$sender_id->save();
					$sender = $sender_id->id;
				}
			}
			$type = '';
			$new_type = TRUE;
			if($request['type'] != '')
			{
				$types = com::getAll_stable('doc_types','en');
				if($types)
				{
					foreach($types AS $t)
					{
						if($t->name == $request['type'])
						{
							$type = $t->id;
							$new_type = FALSE;
							break;
						}
					}
				}
				if($new_type)
				{
					$type_id = new doc_type;
					$type_id->name_en = $request['type'];
					$type_id->name_dr = $request['type'];
					$type_id->name_pa = $request['type'];
					$type_id->created_by = Auth::user()->id;
					$type_id->save();
					$type = $type_id->id;
				}
			}
		    //validation true      
	        $object = new Received_doc;
			$object->dossier_name = $request['dossier'];
	       $object->code = $request['code'];
	       $object->type_id = $type;
	       $object->number = $request['number'];
	       $object->date = $date;
	       $object->summary = $request['summary'];
	        //$object->dep_id = $request['department'];
	        $object->sender_id = $sender;
	       $object->priority_id = $request['priority'];
			$object->note = $request['note'];
			//$object->desc = $request['desc'];
			//$object->monthly_report = $request['report'];
	        $object->created_by = Auth::user()->id;
	
	        if($object->save())
	        {
	        	$inserted_id = $object->id;
				if(count($request['fname'])>0)
				{
					$names_data = array();
					for($i=0;$i<count($request['fname']);$i++)
					{
						if($request['fname'][$i] ==null && $request['position'][$i] == null && $request['phone'][$i] ==null && $request['email'][$i] == null)
						{
							continue;
						}
						else
						{
							//get item
							$items = array(
											"first_name" => $request['fname'][$i],
											"position" => $request['position'][$i],
											"email" => $request['email'][$i],
											"phone" => $request['phone'][$i],
											"received_docs_id" => $inserted_id
										);
							$names_data[] = $items;
						}
					}
					if(count($names_data)>0)
					{
						Com::insertBatch('received_docs_person_names',$names_data);
					}
				}
				//save the attachments
				$this->uploadDocs($inserted_id,'0');
				
	            return \Redirect::route("getReceived")->with("success","You successfuly created new document.");
	        }
	        else
	        {
	            return \Redirect::route("getReceived")->with("fail","An error occured plase try again.");
	        }
	    }
		else
		{
			return showWarning();
		}    		
	}
	public function getUpdateReceived_doc($id=0)
	{
		if(canEdit('com_received_docs'))
		{
			//get details
			$data['details'] = Received_doc::find($id);
			//get sender name
			if($data['details']->sender_id !=0)
			{
				$data['sender_name'] = dep_list::find($data['details']->sender_id)->name_en;
			}
			else
			{
				$data['sender_name'] = '';
			}
			//get type name
			if($data['details']->type_id !=0)
			{
				$data['type_name'] = doc_type::find($data['details']->type_id)->name_en;
			}
			else
			{
				$data['type_name'] = '';
			}
			//get all document types
			$data['doc_types'] = com::getAll_stable('doc_types','en');
			//get all priority types
			$data['priority_types'] = com::getAll_stable('priority_types','en');
			
			//get all attachments
			$data['attachments'] = com::getAll_attachments($id,'0');
			$data['id'] = $id;
			//get all saved names
			$data['names'] = Com::getAll_withid('received_docs_person_names','received_docs_id',$id);
			
			
			//load view for editing 
			return view('com.update_received_doc',$data);
		}
		else
		{
			return showWarning();
		}
		
	}
	public function postUpdateReceived_doc(Request $request,$id=0)
	{
		//validate the input fields
	    $this->validate($request,[
	        "dossier" => "required",
	        "priority" => 'required',
	        "date" => 'required'
	        ]
	    );
	    $date = NULL;
		if($request['date']!='')
		{
	    	$date = gregorian_format($request['date']);
	    }   
		$sender = '';
		$new_sender = TRUE;
		if($request['sender'] != '')
		{
			$deps = com::getAll_stable('dep_lists','en');
			if($deps)
			{
				foreach($deps AS $dep)
				{
					if($dep->name == $request['sender'])
					{
						$sender = $dep->id;
						$new_sender = FALSE;
						break;
					}
				}
			}
			if($new_sender)
			{
				$sender_id = new dep_list;
				$sender_id->name_en = $request['sender'];
				$sender_id->name_dr = $request['sender'];
				$sender_id->name_pa = $request['sender'];
				$sender_id->created_by = Auth::user()->id;
				$sender_id->save();
				$sender = $sender_id->id;
			}
		}
		$type = '';
		$new_type = TRUE;
		if($request['type'] != '')
		{
			$types = com::getAll_stable('doc_types','en');
			if($types)
			{
				foreach($types AS $t)
				{
					if($t->name == $request['type'])
					{
						$type = $t->id;
						$new_type = FALSE;
						break;
					}
				}
			}
			if($new_type)
			{
				$type_id = new doc_type;
				$type_id->name_en = $request['type'];
				$type_id->name_dr = $request['type'];
				$type_id->name_pa = $request['type'];
				$type_id->created_by = Auth::user()->id;
				$type_id->save();
				$type = $type_id->id;
			}
		}
        //create an object from class
        $object = Received_doc::find($id);
        $object->dossier_name = $request['dossier'];
        $object->code = $request['code'];
        $object->type_id = $type;
        $object->number = $request['number'];
        $object->date = $date;
        $object->summary = $request['summary'];
        //$object->dep_id = $request['department'];
        $object->sender_id = $sender;
        $object->priority_id = $request['priority'];
		$object->note = $request['note'];
		//$object->desc = $request['desc'];
		//$object->monthly_report = $request['report'];
        $object->created_by = Auth::user()->id;
        
        if($object->save())
        {
        	Com::delete_record('received_docs_person_names',array('received_docs_id'=>$id));
			//Com::delete_record('received_docs_receiver_add',array('received_docs_id'=>$id));
			$names_data = array();
			for($i=0;$i<count($request['fname']);$i++)
			{
				if($request['fname'][$i] ==null && $request['position'][$i] == null && $request['phone'][$i] ==null && $request['email'][$i] == null)
				{
					continue;
				}
				else
				{
					//get item
					$items = array(
									"first_name" => $request['fname'][$i],
									"position" => $request['position'][$i],
									"email" => $request['email'][$i],
									"phone" => $request['phone'][$i],
									"received_docs_id" => $id
								);
					$names_data[] = $items;
				}
			}
			if(count($names_data)>0)
			{
				Com::insertBatch('received_docs_person_names',$names_data);
			}
			//save the attachments
			$this->uploadDocs($id,'0');	
           return \Redirect::route("getReceived")->with("success","You successfuly created new document.");
        }
        else
        {
            return \Redirect::route("getReceived")->with("fail","An error occured plase try again.");
        }
		
	}
	//delete department
	public function getDeleteReceived_doc($id=0)
	{
		if(canDelete('com_received_docs'))
		{
			$dep = Received_doc::find($id);
			if($dep->delete())
	        {
	        	//get all saved names
				$names = Com::getAll_withid('received_docs_person_names','received_docs_id',$id);
				if(count($names))
				{
	        		Com::delete_record('received_docs_person_names',array('received_docs_id'=>$id));
				}
				//get all attachments
				$attachments = com::getAll_attachments($id,'0');
				if(count($attachments)>0)
				{
					foreach($attachments AS $attach)
					{
						$file= public_path(). "/documents/com_docs/".$attach->file_name;
						if(\File::delete($file))
						{
							//delete from database			
							com::delete_record('attachments',array('id'=>$attach->id));						
						}
					}
				}
	            return \Redirect::route("getReceived")->with("success","You successfuly deleted record.");
	        }
	        else
	        {
	            return \Redirect::route("getReceived")->with("fail","An error occured plase try again.");
	        }
		}
		else
		{
			return showWarning();
		}
	}
	//view details
	public function getDetailsReceived_doc($id=0)
	{
		if(canView('com_received_docs'))
		{
			//get details
			$data['details'] = Received_doc::find($id);
			//get all document types
			$data['doc_types'] = com::getAll_stable('doc_types','en');
			//get all priority types
			$data['priority_types'] = com::getAll_stable('priority_types','en');
			//get all department list
			$data['deps'] = com::getAll_stable('dep_lists','en');
			//get all attachments
			$data['attachments'] = com::getAll_attachments($id,'0');
			$data['id'] = $id;
			//get all saved names
			$data['names'] = Com::getAll_withid('received_docs_person_names','received_docs_id',$id);
			return view('com.received_details',$data);
		}
		else
		{
			return showWarning();
		}
	}
	
	//get report list
	public function getSended()
	{
		return view('com.sended_list');		
	}
	//get data for datatable for sended documents
	public function getSendData()
	{		
		//get data from model
		$docs = Sended_doc::getData();
		//dd($entities);
		$collection = new Collection($docs);
		return \Datatable::collection($collection)
			    ->showColumns('id','dossier_name','code','number','date','president_guide','desc','answer_to')
			    ->addColumn('operation',function($option){
			    	$options = '';
					if(canEdit('com_sended_docs'))
					{
						$options .= '<a href="'.route('getUpdateSended_doc',$option->id).'" title="تجدید نمودن"><i class="glyphicon glyphicon-edit"></i></a> &nbsp;';
					}
					if(canDelete('com_sended_docs'))
					{
						$options .= '|&nbsp;<a href="'.route('getDeleteSended_doc',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');" title="حذف نمودن"><i class="fa fa-trash-o"></i></a>';
					}
					if(canView('com_sended_docs'))
					{
						$options .= '&nbsp;|&nbsp; <a href="'.route('getDetailsSended_doc',$option->id).'" title="نمایش"><i class="glyphicon glyphicon-eye-open"></i></a>';
					}
					return $options;
			    })
			    //->orderColumns('id')
			    ->make();	
		
	}
	public function create_sended_doc()
	{
		if(canAdd('com_sended_docs'))
		{
			//get all document types
			$data['result_types'] = com::getAll_stable('result_types','en');
			//get all priority types
			$data['priority_types'] = com::getAll_stable('priority_types','en');
			
			return view('com.createSended_doc',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function postCreateSended_doc(Request $request)
	{
		//validate the input fields
	    $this->validate($request,[
	        "dossier" => "required",
	        "priority" => 'required',
	        "date" => 'required'
	        ]
	    );
		$doc_date = '';
		$dead_date = NULL;
		if($request['date']!='')
		{
	    	$doc_date = gregorian_format($request['date']);
	    }
	    if($request['deadline']!='')
		{   
			$dead_date = gregorian_format($request['deadline']);
		}   

	    //validation true      
        $object = new Sended_doc;
		$object->dossier_name = $request['dossier'];
        $object->code = $request['code'];
        $object->number = $request['number'];
        $object->date = $doc_date;
        $object->president_guide = $request['guide'];
        $object->priority_id = $request['priority'];
		$object->answer_to = $request['answer'];
		$object->desc = $request['desc'];
		$object->deadline_date = $dead_date;
		$object->result_id = $request['result'];
		if($request['result']==4)
		{
			$object->result_reason = $request['reason'];
		}
        $object->created_by = Auth::user()->id;

       if($object->save())
        {
        	$inserted_id = $object->id;
        	if(count($request['receivers'])>0)
        	{
	        	
				$rec_data = array();
				foreach($request['receivers'] AS $key=>$value)
				{
					//get item
					$items = array(
									"name" => $value,
									"sended_docs_id" => $inserted_id
								);
					$rec_data[] = $items;
					
				}
				if(count($rec_data)>0)
				{
					Com::insertBatch('sended_docs_receivers',$rec_data);
				}
			}
	        //save the attachments
			$this->uploadDocs($inserted_id,'1');
	       return \Redirect::route("getSended")->with("success","You successfuly created new document.");
        }
        else
        {
            return \Redirect::route("getSended")->with("fail","An error occured plase try again.");
        }		
	}
	public function getUpdateSended_doc($id=0)
	{
		if(canEdit('com_sended_docs'))
		{
			//get details
			$data['details'] = Sended_doc::find($id);
			//get all document types
			$data['result_types'] = com::getAll_stable('result_types','en');
			//get all priority types
			$data['priority_types'] = com::getAll_stable('priority_types','en');
			
			//get all attachments
			$data['attachments'] = com::getAll_attachments($id,'1');
			$data['id'] = $id;
			//get all receivers
			$data['receivers'] = Com::getAll_receivers($id);
			
			//load view for editing 
			return view('com.update_sended_doc',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function postUpdateSended_doc(Request $request,$id=0)
	{
		//validate the input fields
	    $this->validate($request,[
	        "dossier" => "required",
	        "priority" => 'required',
	        "date" => 'required'
	        ]
	    );
	    
	   	$doc_date = '';
		$dead_date = NULL;
		if($request['date']!='')
		{
	    	$doc_date = gregorian_format($request['date']);
	    }
	    if($request['deadline']!='')
		{   
			$dead_date = gregorian_format($request['deadline']);
		}     
        //create an object from class
        $object = Sended_doc::find($id);
        $object->dossier_name = $request['dossier'];
        $object->code = $request['code'];
        $object->number = $request['number'];
        $object->date = $doc_date;
        $object->president_guide = $request['guide'];
        $object->priority_id = $request['priority'];
		$object->answer_to = $request['answer'];
		$object->desc = $request['desc'];
		$object->deadline_date = $dead_date;
		$object->result_id = $request['result'];
		if($request['result']==4)
		{
			$object->result_reason = $request['reason'];
		}
        $object->created_by = Auth::user()->id;
        
        if($object->save())
        {
        	Com::delete_record('sended_docs_receivers',array('sended_docs_id'=>$id));
			if(count($request['receivers'])>0)
        	{
			
				$rec_data = array();
				foreach($request['receivers'] AS $key=>$value)
				{
					//get item
					$items = array(
									"name" => $value,
									"sended_docs_id" => $id
								);
					$rec_data[] = $items;
					
				}
				if(count($rec_data)>0)
				{
					Com::insertBatch('sended_docs_receivers',$rec_data);
				}
			}
			//save the attachments
			$this->uploadDocs($id,'1');
           return \Redirect::route("getSended")->with("success","You successfuly created new document.");
        }
        else
        {
            return \Redirect::route("getSended")->with("fail","An error occured plase try again.");
        }
		
	}
	//delete sended document
	public function getDeleteSended_doc($id=0)
	{
		if(canDelete('com_sended_docs'))
		{
			$dep = Sended_doc::find($id);
			if($dep->delete())
	        {
	        	Com::delete_record('sended_docs_receivers',array('sended_docs_id'=>$id));
				//get all attachments
				$attachments = com::getAll_attachments($id,'1');
				if(count($attachments)>0)
				{
					foreach($attachments AS $attach)
					{
						$file= public_path(). "/documents/com_docs/".$attach->file_name;
						if(\File::delete($file))
						{
							//delete from database			
							com::delete_record('attachments',array('id'=>$attach->id));						
						}
					}
				}
	            return \Redirect::route("getSended")->with("success","You successfuly deleted record.");
	        }
	        else
	        {
	            return \Redirect::route("getsended")->with("fail","An error occured plase try again.");
	        }
		}
		else
		{
			return showWarning();
		}
	}
	//view details
	public function getDetailsSended_doc($id=0)
	{
		if(canView('com_sended_docs'))
		{
			//get details
			$data['details'] = Sended_doc::find($id);
			//get all document types
			$data['result_types'] = com::getAll_stable('result_types','en');
			//get all priority types
			$data['priority_types'] = com::getAll_stable('priority_types','en');
			//get all department list
			$data['deps'] = getAllDepartments();
			//get all attachments
			$data['attachments'] = com::getAll_attachments($id,'1');
			$data['id'] = $id;
			//get all receivers
			$data['receivers'] = Com::getAll_receivers($id);
				
			return view('com.sended_details',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function getAutocomplete()
	{
		$autocompletiondata = Com::getAll_auto_receivers();
		if(isset($_GET['term'])) {
		    $result = array();
		    foreach($autocompletiondata as $value) {
		        if(strlen($_GET['term']) == 0 || strpos(strtolower($value->name), strtolower($_GET['term'])) !== false) {
		            $result[] = '{"id":'.$value->id.',"label":"'.$value->name.'","value":"'.$value->name.'"}';
		        }
		    }
		    
		    return "[".implode(',', $result)."]";
		}
	
	}
	public function getReceived_report()
	{
		if(canSearch('com_received_docs'))
		{
			//get all document types
			$data['doc_types'] = com::getAll_stable('doc_types','en');
			//get all priority types
			$data['priority_types'] = com::getAll_stable('priority_types','en');
			//get all department list
			$data['deps'] = com::getAll_stable('dep_lists','en');
			return view('com.received_report',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function postReportReceived_doc(Request $request)
	{
		$sender = '';
		if($request['sender'] != '')
		{
			$deps = com::getAll_stable('dep_lists','en');
			if($deps)
			{
				foreach($deps AS $dep)
				{
					if($dep->name == $request['sender'])
					{
						$sender = $dep->id;
						break;
					}
				}
			}
		}
		//get data from model
		$data['results'] = Received_doc::getData_search($request,$sender);	
		return view('com.received_search_result',$data);

	}
	public function getReceived_excel(request $request)
	{
		$sender = '';
		if($request['sender'] != '')
		{
			$deps = com::getAll_stable('dep_lists','en');
			if($deps)
			{
				foreach($deps AS $dep)
				{
					if($dep->name == $request['sender'])
					{
						$sender = $dep->id;
						break;
					}
				}
			}
		}
		//get data from model
		$results = Received_doc::getData_excel($request,$sender);
		
		Excel::load('excel_template/received.xlsx', function($file) use($results){
		//Excel::create('Filename', function($file) use($results){
			
			$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($results){
			//$sheet = $file -> getActiveSheetIndex(1);	
				$row = 7;
				foreach($results AS $item)
				{
					//get all receivers
					$receivers = Com::getAll_withid('received_docs_person_names','received_docs_id',$item->id);
					$first_name='';$position='';$phone='';$email='';
					if($receivers)
					{
						foreach($receivers AS $rec)
						{
							$first_name.=$rec->first_name.',';
							$position.=$rec->position.',';
							$phone.=$rec->phone.',';
							$email.=$rec->email.',';
						}
					}
					$sheet->setCellValue('A'.$row.'',$item->id);
					$sheet->setCellValue('B'.$row.'',$item->dossier_name);
					$sheet->setCellValue('C'.$row.'',$item->code);
					$sheet->setCellValue('D'.$row.'',$item->type_name);
					$sheet->setCellValue('E'.$row.'',$item->number);
					//$sheet->mergeCells('F'.$row.':H'.$row.'');
					$sheet->setCellValue('F'.$row.'',$item->date);
					$sheet->setCellValue('G'.$row.'',$item->summary);
					$sheet->setCellValue('H'.$row.'',$first_name);
					$sheet->setCellValue('I'.$row.'',$position);
					$sheet->setCellValue('J'.$row.'',$item->sender_name);
					//$sheet->mergeCells('M'.$row.':O'.$row.'');
					$sheet->setCellValue('K'.$row.'',$item->priority_name);
					$sheet->setCellValue('L'.$row.'',$phone);
					$sheet->setCellValue('M'.$row.'',$email);
					//$sheet->mergeCells('R'.$row.':T'.$row.'');
					$sheet->setCellValue('N'.$row.'',$item->note);
					//$sheet->setCellValue('S'.$row.'',$item->desc);
					$sheet->setCellValue('O'.$row.'',$item->monthly_report);
					$row++;
				}
				$border = $row-1;
				// Set border for range
				$sheet->setBorder('A7:P'.$border.'', 'thin');

    		});
		
		})->export('xls');
	}
	// public function getData_received_report(Request $request)
	// {	
		// //get data from model
		// $docs = Received_doc::getData_search($request);
		// //dd($entities);
		// $collection = new Collection($docs);
		// return \Datatable::collection($collection)
			    // ->showColumns('id','dossier_name','code','number','date')
			    // ->addColumn('operation',function($option){
			    	// $options = '';
					// if(canEdit('com_received_docs'))
					// {
						// $options .= '<a href="'.route('getUpdateReceived_doc',$option->id).'"><i class="fa fa-edit"></i> Edit</a> &nbsp;';
					// }
					// //if(canDelete('com_received_docs'))
					// //{
						// $options .= '|&nbsp;<a href="'.route('getDeleteReceived_doc',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="fa fa-trash-o"></i> Delete</a>';
					// //}
					// if(canView('com_received_docs'))
					// {
						// $options .= '&nbsp;|&nbsp; <a href="'.route('getDetailsReceived_doc',$option->id).'"><i class="fa fa-edit"></i> View</a>';
					// }
					// return $options;
			    // })
			    // //->orderColumns('id')
			    // ->make();	
// 		
	// }
	public function getSended_report()
	{
		if(canSearch('com_sended_docs'))
		{
			//get all document types
			$data['result_types'] = com::getAll_stable('result_types','en');
			//get all priority types
			$data['priority_types'] = com::getAll_stable('priority_types','en');
			$data['receivers']		= Com::getAll_auto_receivers();
			
			return view('com.sended_report',$data);
		}
		else
		{
			return showWarning();
		}
	}
	public function postReportSended_doc(Request $request)
	{
		//get data from model
		$data['results'] = Sended_doc::getData_search($request);
		
		return view('com.sended_search_result',$data);

	}
	public function getSended_excel(request $request)
	{
		//get data from model
		$results = Sended_doc::getData_excel($request);
		Excel::load('excel_template/sended.xlsx', function($file) use($results){
		//Excel::create('Filename', function($file) use($results){			
			$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($results){
			//$sheet = $file -> getActiveSheetIndex(1);	
				$row = 7;
				foreach($results AS $item)
				{
					//get all receivers
					$receivers = Com::getAll_withid('sended_docs_receivers','sended_docs_id',$item->id);
					$names='';
					if($receivers)
					{
						foreach($receivers AS $rec)
						{
							$names.=$rec->name.',';	
						}
					}
					$sheet->setCellValue('A'.$row.'',$item->id);
					$sheet->setCellValue('B'.$row.'',$item->dossier_name);
					//$sheet->setCellValue('U'.$row.'',$item->code);
					$sheet->setCellValue('D'.$row.'',$item->number);
					$sheet->setCellValue('E'.$row.'',$item->date);
					$sheet->setCellValue('F'.$row.'',$item->president_guide);
					$sheet->setCellValue('G'.$row.'',$names);
					$sheet->setCellValue('H'.$row.'',$item->priority_name);
					$sheet->setCellValue('I'.$row.'',$item->deadline_date);
					$sheet->setCellValue('J'.$row.'',$item->result_name);
					$sheet->setCellValue('L'.$row.'',$item->desc);
					$row++;
				}
				$border = $row-1;
				// Set border for range
				$sheet->setBorder('A7:L'.$border.'', 'thin');

    		});
		
		})->export('xls');
	}
	//upload document
	public function uploadDocs($doc_id=0,$type=null)
	{
		// if(canAdd('docscom_docscom') || canEdit('docscom_docscom') || canCheckOut('docscom_docscom'))
		// {

			// getting all of the post data
			$files = Input::file('files');
			$errors = "";
			$file_data = array();
			foreach($files as $file) 
			{
				if(Input::hasFile('files'))
				{
				  // validating each file.
				  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
				  $validator = \Validator::make(

				  		[
				            'file' => $file,
				            'extension'  => Str::lower($file->getClientOriginalExtension()),
				        ],
				        [
				            'file' => 'required|max:100000',
				            'extension'  => 'required|in:jpg,jpeg,png,doc,docx,zip,pdf,xlsx,xls,txt'
				        ]
				  	);

				  if($validator->passes())
				  {
				  	
				    // path is root/uploads
				    $destinationPath = 'documents/com_docs';
				    $name = $file->getClientOriginalName();

				    $temp = explode(".", $name);
				    $extension = end($temp);
				    //$lastFileId = getLastDocId();
				    
				    //$lastFileId++;
				    
				    $filename = $temp[0].'-'.$doc_id.'-'.time().'.'.$extension;

				    $upload_success = $file->move($destinationPath, $filename);

				    if($upload_success) 
				    {
					   
					   $data = array(
					   					'doc_id'=>$doc_id,
					   					'file_name'=>$filename,
					   					'doc_type' => $type
					   				);
					   if(count($data)>0)
						{
							com::insertRecord('attachments',$data);
						}
					   //return Response::json('success', 200);
					   
					} 
					else 
					{
					   $errors .= json('error', 400);
					}

			    
				  } 
				  else 
				  {
				    // redirect back with errors.
				    return Redirect::back()->withErrors($validator);
				  }
				}
			}
		
	}
	//download file from server
	public function downloadDoc($file_id=0)
	{	
		//get file name from database
		//$fileName = getDocscomFileName($file_id);
        //public path for file
        $file= public_path(). "/documents/com_docs/".$file_id;
        //download file
        return Response::download($file, $file_id);
	    
	}
	//remove file from folder
	public function deleteFile(Request $request)
	{
			$fileName = $request['name'];
			$id = $request['doc_id'];
			//get file name from database
			//$fileName = getDocscomFileName($file_id);
			$file= public_path(). "/documents/com_docs/".$fileName;
			if(\File::delete($file))
			{
				//delete from database
				
				com::delete_record('attachments',array('id'=>$id));
				return "<div class='alert alert-success'>File Deleted Successfully!</div>";
			}
			else
			{
				return "<div class='alert alert-danger'>Error!</div>";
			}
		
	}
	function change_date(request $request)
	{
		$date = explode('-', $request['date']);
		$m = $date[0];
		$d = $date[1];
		$y = $date[2];
		$shamsi = dateToShamsi($y,$m,$d);
		echo gregorian_format($shamsi);
		
	}
	function getSender_list(request $request)
	{
		$q = strtolower($request['term']);

		if (!$q) return;
		$options = com::getAll_stable('dep_lists','en');
		if (!$options) return;
		$json_array = array();
		foreach ($options as $key) 
		{
			if (strpos(strtolower($key->name), $q) !== false) 
			{
				//echo "$key->company_name\n";
				array_push($json_array, $key->name);
			}
		}
		echo json_encode($json_array);
	}
	function getType_list(request $request)
	{
		$q = strtolower($request['term']);

		if (!$q) return;
		$options = com::getAll_stable('doc_types','en');
		if (!$options) return;
		$json_array = array();
		foreach ($options as $key) 
		{
			if (strpos(strtolower($key->name), $q) !== false) 
			{
				//echo "$key->company_name\n";
				array_push($json_array, $key->name);
			}
		}
		echo json_encode($json_array);
	}
	
}
//end of comController
