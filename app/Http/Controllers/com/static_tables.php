<?php 

namespace App\Http\Controllers\com;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use DB;
use Datatable;
use App\models\com\static_table;
use App\models\com\Com;
use Illuminate\Support\Collection;
use Input;
use Response;
use Validator;
use Carbon\Carbon;

class static_tables extends Controller
{

	/**
     	* Instantiate a new UserController instance.
     	*/
	public function __construct()
	{
		if(!Auth::check())
		{
			return \Redirect::route('getLogin');
		}		
	}	
	//get report list
	public function getStaticTables()
	{
		$data['records'] = com::getAll_stable('static_tables','en');
		return view('com.static_tables_main',$data);	
		
	}
	public static function getTable_details(request $request)
	{
		$data['table'] = static_table::find($request['id']);
		//$data['id'] = $request['id'];
		return view('com.table_list',$data);
	}
	//get data for datatable
	public function getData($table)
	{	
		//get data from model
		$docs = static_table::getData($table);
		//dd($entities);
		$collection = new Collection($docs);
		return Datatable::collection($collection)
			    ->showColumns('id','name_en','name_dr','name_pa')
			    ->addColumn('operation',function($option){
			    	$options = '';
					if(canEdit('com_received_docs'))
					{
						$options .= '<a href="javascript:void()" title="تجدید نمودن" onclick="load_edit('.$option->id.')"><i class="glyphicon glyphicon-edit"></i></a> &nbsp;';
					}
					if(canDelete('com_received_docs'))
					{
						$options .= '|&nbsp;<a href="javascript:void()" onclick="javascript:if(confirm(\'آیا میخواهید این ریکارد را حذف نمایید؟\')){delete_table('.$option->id.')};" title="حذف نمودن"><i class="fa fa-trash-o"></i></a>';
					}
					
					return $options;
			    })
			    //->orderColumns('id')
			    ->make();	
		
	}
	public function postCreateStatic(request $request)
	{
		if(canAdd('com_received_docs'))
		{   
	        $data = array('name_en'=>$request['name_en'],
						  'name_pa' => $request['name_pa'],
						  'name_dr' => $request['name_dr'],
						  'created_by' => Auth::user()->id,
						  'created_at' => Carbon::now()
			);
	      Com::insertRecord($request['table_name'],$data); 
	      //return \Redirect::route("getStaticTables")->with("success","You successfuly created new document.");
			
		}
		else
		{
			return showWarning();
		}
	}
	public static function getUpdateStatic(request $request)
	{
		$data['id'] = $request['id'];
		$data['table'] = $request['table'];
		$data['table_id'] = $request['table_id'];
		
		$record = Com::getAll_withid($request['table'],'id',$request['id']);
		$data['record'] = head($record);
		
		return view('com.updateStatic',$data);
	}
	public function postUpdateStatic(request $request)
	{
		if(canAdd('com_received_docs'))
		{   
	        $data = array('name_en'=>$request['name_en'],
						  'name_pa' => $request['name_pa'],
						  'name_dr' => $request['name_dr']
			);
	      Com::update_record($request['table'],array('id'=>$request['id']),$data); 
	      //return \Redirect::route("getStaticTables")->with("success","You successfuly created new document.");
			
		}
		else
		{
			return showWarning();
		}
	}
	function delete_static(request $request)
	{
		$id = $request['id'];
		Com::delete_record($request['table'],array('id'=>$id));	
	}
}