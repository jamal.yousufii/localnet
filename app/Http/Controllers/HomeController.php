<?php namespace App\Http\Controllers;
use App\models\Application;
use App\models\hr\hrOperation;
use LaravelGettext;
use Redirect;
use URL;
use Auth;
use DB;
use Input;
use Session;

class HomeController extends Controller {


	// public function __construct()
	// {
	// 	$this->middleware('auth');		
	// }

	public function home()
	{
		$data['apps'] = Application::getAll();
		//$modules = Session::get('user_modules');dd($modules);
		//get my personal todos
		//$data['todos'] = DB::connection("workplan")->table('tasks')->where('user_id',Auth::user()->id)->get();
        $data['dir_leaves'] = 0;
        if(Auth::user()->position_id==2)
        {
            $sh_date = dateToShamsi(date('Y'),date('m'),date('d'));
            $year = explode('-', $sh_date);
            $year = $year[0];//current shamsi year
            $data['dir_leaves'] = hrOperation::getDir_pending_leaves_total($year);
        }
		$data['user'] = DB::connection("hr")->table('employees as e')->select('e.name_dr','e.last_name')
		->leftjoin('auth.users AS u','u.employee_id','=','e.id')->where('u.id',Auth::user()->id)->first();

        $data['notifications'] = hrOperation::getAllHRNotifications(true);
		return View('dashboard',$data);
		
	}
    public function home_new()
    {
        $data['apps'] = Application::getAll();
        //$modules = Session::get('user_modules');dd($modules);
        //get my personal todos
        //$data['todos'] = DB::connection("workplan")->table('tasks')->where('user_id',Auth::user()->id)->get();
        $data['dir_leaves'] = 0;
        if(Auth::user()->position_id==2)
        {//director
            $sh_date = dateToShamsi(date('Y'),date('m'),date('d'));
            $year = explode('-', $sh_date);
            $year = $year[0];//current shamsi year
            $data['dir_leaves'] = hrOperation::getDir_pending_leaves_total($year);
        }
        elseif(canAttHoliday())
        {//attendance responsible
            $sh_date = dateToShamsi(date('Y'),date('m'),date('d'));
            $year = explode('-', $sh_date);
            $year = $year[0];//current shamsi year
            $data['dir_leaves'] = hrOperation::getHR_pending_leaves_total($year);
        }
        $data['user'] = DB::connection("hr")->table('employees as e')->select('e.name_dr','e.last_name')
        ->leftjoin('auth.users AS u','u.employee_id','=','e.id')->where('u.id',Auth::user()->id)->first();

        $data['notifications'] = hrOperation::getAllHRNotifications(true);
        return View('dashboard',$data);
        
    }
	/**
    * Changes the current language and returns to previous page
    * @return Redirect
    */
    public function changeLang($locale=null){
        
        LaravelGettext::setLocale($locale);
        //save last changed locale
        $data = array(
        			"user_id" => Auth::user()->id,
        			"session_type" => 'lang',
        			"session_value" => $locale,
        			"last_update" => date('Y-m-d H:i:s')
        	);
        //first remove old change
        DB::table('user_session')
        ->where('user_id',Auth::user()->id)
        ->where('session_type','lang')
        ->delete();

        //insert new record
        DB::table('user_session')->insert($data);
         
        return Redirect::to(URL::previous());

    }

    /**
    * Changes the date setting
    * @return Redirect
    */
    public function changeDateSetting(){
        
        $data = array(
                    "user_id" => Auth::user()->id,
                    "session_type" => 'date',
                    "session_value" => Input::get("date"),
                    "last_update" => date('Y-m-d H:i:s')
            );

        DB::table('user_session')
        ->where('user_id',Auth::user()->id)
        ->where('session_type','date')
        ->delete();
        //insert new record
        DB::table('user_session')->insert($data);
        
        \Session::put('date_setting',Input::get("date"));
         
        return json_encode(array('cond'=>true));

    }

}