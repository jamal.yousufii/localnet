<?php 

namespace App\Http\Controllers\document_tracking;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\models\document_tracking\docsModel;
use Illuminate\Support\Collection;
use App\library\jdatetime;
use View;
use DB;
use Validator;
use Input;
use Redirect;
use Response;
use Datatable;
use Session;
use DateTime;
use DateInterval;
use File;
use Excel;
use Crypt;		
use Illuminate\Support\Str;
use URL;
use Carbon\Carbon;

class docsController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function __construct()
	{
		if(!Auth::check())
		{
			return Redirect::route('getLogin');
		}	
	}
//================================================= Document Tracking Controller Functions ============================
	
	// get the list of documents with datatables.
	public function getIncomingDocs()
	{
        $data['records'] = docsModel::getIncomingDocuments();
        $data['user_dept'] = getUserDepartmentTree(Auth::user()->id); //User Dept Tree  
		if(Input::get('ajax') == 1)
		{
			return View::make('document_tracking.incoming_documents.docs_list_ajax',$data);
		}
		else
		{
			//load view to show searchpa result
			return View::make('document_tracking.incoming_documents.docs_list',$data);
		}
	}

	public function getIncomingDocForm()
	{
		return view('document_tracking.incoming_documents.incoming_doc_form');
	}

	public function exportIncomingDocumentsToExcel()
	{
		//dd($_POST);
		$results = docsModel::getSearchIncomingDocuments('print');
		$curr_date = date('Y-m-d');
		Excel::load('excel_template/M&E Document Tracking Report.xlsx', function($file) use($results){
			//Excel::create('Filename', function($file) use($results){			
			$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($results){	
				$row = 3;
				$sheet->setFreeze('A3');
				
				foreach($results AS $item)
				{
					$sheet->getStyle('A3:O' . $sheet->getHighestRow())->getAlignment()->setWrapText(true); 
					if(getMEDocumentTypeName($item->doc_type) != "")
						$doc_type = getMEDocumentTypeName($item->doc_type);
					else 
						$doc_type = "";
					if($item->execution_type == 1) $execution_type = "عادی";
					elseif($item->execution_type == 2) $execution_type = "تفصیلی";
					else $execution_type = "";
					if($item->doc_status == 1) $doc_status = "اجراء";
					else $doc_status = $doc_status = "تحت اجراء";
					//$summary = preg_replace("/&nbsp;/",' ',$item->summary);

					$sheet->setHeight($row, 50);

					$sheet->setCellValue('A'.$row.'',$row-2);
					$sheet->setCellValue('B'.$row.'',$doc_type);
					$sheet->setCellValue('C'.$row.'',$execution_type);
					$sheet->setCellValue('D'.$row.'',$item->doc_number);
					$sheet->setCellValue('E'.$row.'',dmy_format(toJalali($item->doc_date)));
					$sheet->setCellValue('F'.$row.'',$item->ministries);
					$sheet->setCellValue('G'.$row.'',$item->sent_to);
					$sheet->setCellValue('H'.$row.'',$item->incoming_number);
					$sheet->setCellValue('I'.$row.'',dmy_format(toJalali($item->incoming_date)));
					$sheet->setCellValue('J'.$row.'',strip_tags($item->subject));
					$sheet->setCellValue('K'.$row.'',$item->number_of_papers);
					$sheet->setCellValue('L'.$row.'',dmy_format(toJalali($item->execution_date)));
					$sheet->setCellValue('M'.$row.'',$doc_status);
					if(getOperation($item->id) == 1)
						$sheet->setCellValue('P'.$row.'', 'حفظ شده');
					elseif(getOperation($item->id) == 2)
						$sheet->setCellValue('P'.$row.'', 'صادر شده');
					else
						$sheet->setCellValue('P'.$row.'', 'تحت اجراء');

					if(!empty(getAssignee($item->id)))
					{
	                	$assignee = getAssigneeName(getAssignee($item->id));	                	
	                	$sheet->setCellValue('N'.$row.'', $assignee);
	                }
	                else
					{
						$sheet->setCellValue('N'.$row.'', "");
					}
					$sheet->setCellValue('O'.$row.'',getAssigneeDescription($item->id));
					
					$row++;
				}

				$sheet->setBorder('A3:O'.($row-1).'', 'thin');
				
    		});
			
			})->export('xlsx');
	}

	public function addIncomingDoc()
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"doc_type"						=> "required",
			"doc_number"					=> "required",
			"doc_date"						=> "required",
			"sender"						=> "required",
			"sent_to"						=> "required",
			"incoming_number"				=> "required",
			"incoming_date"					=> "required",
			"subject"						=> "required",
			"number_of_papers"				=> "required"
		));

		if($validates->fails())
		{	
			return Redirect::route('loadIncomingDocForm')->withErrors($validates)->withInput();
		}
		else
		{
	    
		    //check the date type if it's shamsi or miladi.
			$doc_date = toGregorian(ymd_format(Input::get('doc_date')));
			$incoming_date = toGregorian(ymd_format(Input::get('incoming_date')));

		    // get the form data.
		    $data = array(
		    		"doc_type"					=> Input::get('doc_type'),
		    		"doc_number"				=> Input::get('doc_number'),
		    		"doc_date"					=> $doc_date,
		    		"sender"					=> Input::get('sender'),
		    		"sent_to"					=> Input::get('sent_to'),
		    		"incoming_number"			=> Input::get('incoming_number'),
		    		"incoming_date"				=> $incoming_date,
		    		"subject"					=> Input::get('subject'),
		    		"number_of_papers"			=> Input::get('number_of_papers'),
		    		"doc_status"				=> 0,
                    "user_id"					=> Auth::user()->id,
                    'department_id'             => getUserDepartment(Auth::user()->id)->department, 
		    		"created_at"				=> date('Y-m-d H:i:s')
		    	);
		    $insertedRecordID = docsModel::addRecord($data);
		    if($insertedRecordID){
		    
		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'incoming_docs',
					'record_id'=>$insertedRecordID,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				docsModel::addLog($Log);
		    	return Redirect::route('getIncomingDocsList')->with("success","سند وارده موفقانه اضافه گردید.");
	        }
	        else
	        {
	            return Redirect::route('getIncomingDocsList')->with("fail","مشکل وجود دارد لطفآ همرا با مسئول سیستم به تماس شوید .");
	        }
	    }
    }

	public function loadIncomingEditPage($doc_id)
	{
		$record_id = Crypt::decrypt($doc_id);
		$data['assignee'] = docsModel::getAssignee($record_id);
		$data['assignee_description'] = getAssigneeDescription($record_id);
		$data['record'] = docsModel::getDocDetails($record_id);
		//dd($data['record']);
		return view('document_tracking.incoming_documents.edit_incoming_doc', $data);
	}

	public function postUpdateIncomingDoc($record_id)
	{
		if(isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
		{
			if(Input::get("doc_status") == 0)
			{
				//validate the input fields
			    $validates = Validator::make(Input::all(), array(
					"doc_type"						=> "required",
					"doc_number"					=> "required",
					"doc_date"						=> "required",
					"sender"						=> "required",
					"sent_to"						=> "required",
					"incoming_number"				=> "required",
					"incoming_date"					=> "required",
					"subject"						=> "required",
					"number_of_papers"				=> "required"
				));
			}
			else
			{
				//validate the input fields
			    $validates = Validator::make(Input::all(), array(
					"doc_type"						=> "required",
					"doc_number"					=> "required",
					"doc_date"						=> "required",
					"sender"						=> "required",
					"sent_to"						=> "required",
					"incoming_date"					=> "required",
					"subject"						=> "required",
					"number_of_papers"				=> "required"
				));
			}
		}
		if(isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs'))
		{
			//validate the input fields
		    $validates = Validator::make(Input::all(), array(
				"execution_type"				=> "required",
				"execution_date"				=> "required",
				"assignee"						=> "required"
			));
		}
		if(isMEExpert('document_tracking_incoming_docs', 'm&e_expert_incoming_docs'))
		{
			//validate the input fields
		    $validates = Validator::make(Input::all(), array(
				"assignee_description"						=> "required"
			));
		}
		

		if($validates->fails())
		{	
			return Redirect::route('editIncomingDoc')->withErrors($validates)->withInput();
		}
		else
		{
			//dd($_POST);
		   //check the date type if it's shamsi or miladi.

		    //dd($assignees_data);
		    if(isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
		    {
				$doc_date = toGregorian(ymd_format(Input::get('doc_date')));
				$incoming_date = toGregorian(ymd_format(Input::get('incoming_date')));
				if(Input::get("doc_status") == 0)
				{
				    // get the form data.
				    $data = array(
			    		"doc_type"					=> Input::get('doc_type'),
			    		"doc_number"				=> Input::get('doc_number'),
			    		"doc_date"					=> $doc_date,
			    		"sender"					=> Input::get('sender'),
			    		"sent_to"					=> Input::get('sent_to'),
			    		"incoming_number"			=> Input::get('incoming_number'),
			    		"incoming_date"				=> $incoming_date,
			    		"subject"					=> Input::get('subject'),
			    		"number_of_papers"			=> Input::get('number_of_papers'),
			    		"user_id"					=> Auth::user()->id,
			    		"updated_at"				=> date('Y-m-d H:i:s')
				    );
				}
				else
				{
					// get the form data.
				    $data = array(
			    		"doc_type"					=> Input::get('doc_type'),
			    		"doc_number"				=> Input::get('doc_number'),
			    		"doc_date"					=> $doc_date,
			    		"sender"					=> Input::get('sender'),
			    		"sent_to"					=> Input::get('sent_to'),
			    		"incoming_date"				=> $incoming_date,
			    		"subject"					=> Input::get('subject'),
			    		"number_of_papers"			=> Input::get('number_of_papers'),
			    		"user_id"					=> Auth::user()->id,
			    		"updated_at"				=> date('Y-m-d H:i:s')
				    );
				}

		    	$updated = docsModel::updateIncomingDocument($data,$record_id);
				$assignees_updated = ""; $assignee_description_updated = "";
			}
		    if(isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs'))
		    {
		    	$execution_date = toGregorian(ymd_format(Input::get('execution_date')));
		    	// get the form data.
			    $data = array(
		    		"execution_type"			=> Input::get('execution_type'),
		    		"execution_date"			=> $execution_date,
		    		"user_id"					=> Auth::user()->id,
		    		"updated_at"				=> date('Y-m-d H:i:s')
			    );
			    
		    	$updated = docsModel::updateIncomingDocument($data,$record_id); 
		    	$assignees_updated = docsModel::updateIncomingDocumentAssignee($record_id);
		    	$assignee_description_updated = "";
		    }
		    if(isMEExpert('document_tracking_incoming_docs', 'm&e_expert_incoming_docs'))
		    {
		    	$updated= ""; $assignees_updated = "";
		    	$assignee_description = Input::get('assignee_description');
		   	 	$assignee_description_updated = docsModel::update_assignee_description($assignee_description, $record_id);
		   	}

		    if($updated || $assignees_updated || $assignee_description_updated){
		    	
		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'incoming_docs',
					'record_id'=>$record_id,
					'user' => Auth::user()->username,
					'action' => "Updated",
					'created_at' => date('Y-m-d H:i:s')
					);
				docsModel::addLog($Log);
		    	return Redirect::route('getIncomingDocsList')->with("success","سند وارده موفقانه تجدید نظر شد .");
	        }
	        else
	        {
	            return Redirect::route('getIncomingDocsList')->with("fail","مشکل وجود دارد لطفآ همرا با مسئول سیستم به تماس شوید .");
	        }
	    }
	}

	public function getSearchIncomingDocs()
	{
		//dd($_POST);
		$data['records'] = docsModel::getSearchIncomingDocuments("");
		if(Input::get('year') != "")
			$data['year'] = Input::get('year');
		else
			$data['year'] = "";

		return view('document_tracking.incoming_documents.search_result',$data);
	}

	//change the status of record based on it's id to 1 if it's deleted.
	public function getDeleteIncomingDocument()
	{
		$incoming_doc_id = Input::get('record_id');
		// check if based on incoming document id there is an issued document.
		$issued_doc_id = DB::connection('document_tracking')->table('issued_docs')->where('incoming_doc_id', $incoming_doc_id)->pluck('id');
		//dd($issued_doc_id);
		//$id = Crypt::decrypt($id);
		$deleted = docsModel::deleteIncomingDocInfo($incoming_doc_id,$issued_doc_id);
		//dd($deleted);
		if($deleted)
	    {
	    	if(!empty($issued_doc_id))
	    	{
		    	$removed = docsModel::removeDocFiles($issued_doc_id);
	    	}
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'incoming_docs',
				'record_id'=>$incoming_doc_id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			docsModel::addLog($Log);
    		echo "<div class='alert alert-success'> سند وارده همرا با سند صادره ان موفقانه جذف گردید </div>";
        }
	}

	// ================================================================= Document Issuing Management functions ===================

	public function loadIssueForm()
	{
		$data['pendingIncomingNumber'] = docsModel::getPendingIncomingNumbers();
		//dd($data['getPendingIncomingNumbers']);
		return view('document_tracking.issued_documents.issue_form', $data);
	}

	public function loadIssuedDocList()
	{
        $data['records'] = docsModel::getDocumentsForIssuing();
        $data['user_dept'] = getUserDepartmentTree(Auth::user()->id); //User Dept Tree  

		if(Input::get('ajax') == 1)
		{
			return view('document_tracking.issued_documents.doc_issue_list_ajax', $data);
		}
		else
		{
			//load view to show searchpa result
			return view('document_tracking.issued_documents.doc_issue_list', $data);
		}
	}

	public function addIssuedDoc(Request $request)
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			// "incoming_number"				=> "required",
			"issue_number"					=> "required",
			"issue_date"					=> "required",
			"third_copy"					=> "required",
			"sender"						=> "required",
			"sent_to"						=> "required",
			"subject"						=> "required",
			"reference_copy"				=> "required",
			"number_of_papers"				=> "required",
			"related_carton"				=> "required"
		));

		if($validates->fails())
		{	
			return Redirect::route('issueForm')->withErrors($validates)->withInput();
		}
		else
		{
	    	//dd($_POST);
		    //check the date type if it's shamsi or miladi.
			$issue_date = toGregorian(ymd_format(Input::get('issue_date')));
			$issue_type = Input::get('issue_type');
		    $incoming_ids = Input::get('incoming_ids');

		    $multi_rows = array();
		    // Loop until the number of incoming documnets number and store them in the array;
		    for($i=0;$i<count($incoming_ids);$i++)
		    {
		    	//get the incoming numbers based on the incoming ids.
		    	$incoming_number = getRecordFieldBasedOnID('incoming_docs','incoming_number',$incoming_ids[$i]);
		    	$multi_rows[] = array(

		    		"incoming_number"				=> $incoming_number,
		    		"issuing_number"				=> Input::get('issue_number'),
		    		"issuing_date"					=> $issue_date,
		    		"third_copy"					=> Input::get('third_copy'),
		    		"sender"						=> Input::get('sender'),
		    		"sent_to"						=> Input::get('sent_to'),
		    		"subject"						=> Input::get('subject'),
		    		"number_of_papers"				=> Input::get('number_of_papers'),
		    		"related_carton"				=> Input::get('related_carton'),
		    		"incoming_doc_id"				=> $incoming_ids[$i],
		    		"operations"					=> 2,
                    "user_id"						=> Auth::user()->id,
                    "department_id"                 => \getUserDepartment(Auth::user()->id)->department,
		    		"created_at"					=> date('Y-m-d H:i:s')
			    );
            }
		    //get the last record id of the issued docs table.
		    $last_record_id = getLastRecordId('issued_docs');
		    $inserted = DB::connection('document_tracking')->table('issued_docs')->insert($multi_rows);

		    //get the inserted records id which is greater than the last record id or newly inserted records.
	    	$inserted_ids = DB::connection('document_tracking')->table('issued_docs')->select('id')->where('id','>',$last_record_id)->get();
	    	$multi_ids = array();
	    	foreach ($inserted_ids as $ids) {
	    		$multi_ids[] = $ids->id; 
	    	}

		    if($inserted)
		    {
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 1;
				$file_data = array();

				if(Input::hasFile('files'))
				{				
					foreach($files as $file) 
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension())
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    $destinationPath = 'documents/document_tracking_attachments';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);
						    			  			    
					    	$lastFileId = $last_record_id+1;
							if($auto == 1)
						    $filename = "Issued_doc_attachment_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Issued_doc_attachment_".$lastFileId."_".$auto.".".$extension;
							
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success) 
						    {
						    	$upload_data = array();
						    	// now loop based on the number of records inserted and store the upload table required fields into the array.
						    	for($j = 0; $j < count($multi_ids); $j++)
						    	{
							    	$upload_data[] = array(
						   					'file_name'				=> $filename,
						   					'original_file_name'	=> $original_filename,
						   					'issued_doc_id'			=> $multi_ids[$j],
						   					'user_id'				=> Auth::user()->id,
						   					"created_at"			=> date('Y-m-d H:i:s')
						   				);
							   	}
						    	//call the model function to insert the data into upload table.
						    	docsModel::uploadFiles($upload_data);
							} 
							else 
							{
								// delete the inserted record if there were errors in uploading file.
								DB::connection('document_tracking')->table('issued_docs')->whereIn('id', $multi_ids)->delete();
							    // redirect back with errors.
					    		return Redirect::route('issueForm')->withErrors($validator);
							}
						}
						else
						{
							// delete the inserted record if there were errors in uploading file.
							DB::connection('document_tracking')->table('issued_docs')->whereIn('id', $multi_ids)->delete();
							// redirect back with errors.
					    	return Redirect::route('issueForm')->withErrors($validator);
						}
						$auto ++;
					}
				}
				$reference_copy = Input::get('reference_copy');
				$reference_data = array();
				//now insert the multiple references to the copy of references table.
				for($j = 0; $j < count($multi_ids); $j++)
		    	{
		    		for($k = 0; $k < count($reference_copy); $k++)
		    		{
			    		// get the the log data and insert it into the log table.
				    	$reference_data[] = array(
							'issued_doc_id' => $multi_ids[$j],
							'reference_id' => $reference_copy[$k]
						);
		    		}
			   	}	
			   	// now insert the reference_copy into the copy to references table.
			   	DB::connection('document_tracking')->table('copy_to_references')->insert($reference_data);
			   	$log_data = array();
				// now loop based on the number of records inserted and store the upload table required fields into the array.
		    	for($s = 0; $s < count($multi_ids); $s++)
		    	{
		    		// get the the log data and insert it into the log table.
			    	$log_data[] = array(
						'action_table' => 'issued_docs',
						'record_id'=>$multi_ids[$s],
						'user' => Auth::user()->username,
						'action' => "Added",
						'created_at' => date('Y-m-d H:i:s')
					);
			   	}				
				docsModel::addLog($log_data);
				// now update the document status of the documents that is issued in incoming table from 0 to 1 which means it is done now.
			   	DB::connection('document_tracking')->table('incoming_docs')->whereIn('id', $incoming_ids)->update(array('doc_status' => 1));
		    	return Redirect::route('issuedDocList')->with("success","سند موفقانه صادر شد.");
			}

	    }
	}

	public function loadIssuedDocEditPage($issued_doc_id)
	{
		$issued_doc_id = Crypt::decrypt($issued_doc_id);
		$data['record'] = docsModel::getIssudDocDetails($issued_doc_id);
		$data['references'] = docsModel::getIssuedDocReferences($issued_doc_id);
		return view('document_tracking.issued_documents.edit_issued_doc',$data);
	}

	public  function postUpdateIssuedDoc($issued_doc_id)
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			// "incoming_number"				=> "required",
			"issue_number"					=> "required",
			"issue_date"					=> "required",
			"third_copy"					=> "required",
			"sender"						=> "required",
			"sent_to"						=> "required",
			"subject"						=> "required",
			"reference_copy"				=> "required",
			"number_of_papers"				=> "required",
			"related_carton"				=> "required"
		));

		if($validates->fails())
		{	
			return Redirect::route('editIssuedDoc')->withErrors($validates)->withInput();
		}
		else
		{
			$issue_date = toGregorian(ymd_format(Input::get('issue_date')));
			// if(Input::get('incoming_number') != "")
			// {
		 //    	$incoming_number = Input::get('incoming_number');
			// }
			// else { $incoming_number = ""; }

			// if(Input::get('incoming_doc_id') != "")
			// {
		 //    	$incoming_doc_id = Input::get('incoming_doc_id');
			// }
			// else { $incoming_doc_id = ""; }

	    	$data = array(

	    		//"incoming_number"				=> $incoming_number,
	    		"issued_doc_type"				=> Input::get('issued_doc_type'),
	    		"issuing_number"				=> Input::get('issue_number'),
	    		"issuing_date"					=> $issue_date,
	    		"third_copy"					=> Input::get('third_copy'),
	    		"sender"						=> Input::get('sender'),
	    		"sent_to"						=> Input::get('sent_to'),
	    		"subject"						=> Input::get('subject'),
	    		"number_of_papers"				=> Input::get('number_of_papers'),
	    		"related_carton"				=> Input::get('related_carton'),
	    		// "incoming_doc_id"				=> $incoming_doc_id,
	    		"operations"					=> 2,
	    		"user_id"						=> Auth::user()->id,
	    		"updated_at"					=> date('Y-m-d H:i:s')
		    );

		    $updated = DB::connection('document_tracking')->table('issued_docs')->where('id', $issued_doc_id)->update($data);

		    if($updated)
		    {
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 1;
				$file_data = array();

				if(Input::hasFile('files'))
				{				
					foreach($files as $file) 
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension())
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    $destinationPath = 'documents/document_tracking_attachments';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);
						    			  			    
					    	$lastFileId = $issued_doc_id;
							if($auto == 1)
						    $filename = "Issued_doc_attachment_Editted".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Issued_doc_attachment_Editted".$lastFileId."_".$auto.".".$extension;
							
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success) 
						    {
						    	$upload_data = array(
					   					'file_name'				=> $filename,
					   					'original_file_name'	=> $original_filename,
					   					'issued_doc_id'			=> $issued_doc_id,
					   					'user_id'				=> Auth::user()->id,
					   					"created_at"			=> date('Y-m-d H:i:s')
					   				);
						    	//call the model function to insert the data into upload table.
						    	docsModel::uploadFiles($upload_data);
							} 
							else 
							{
							    // redirect back with errors.
					    		return Redirect::route('editIssuedDoc')->withErrors($validator);
							}
						}
						else
						{
							// redirect back with errors.
					    	return Redirect::route('editIssuedDoc')->withErrors($validator);
						}
						$auto ++;
					}
				}

				// if the issued document that does not have incoming document has assignee with it then remove it and insert the new one.
				docsModel::updateIssuedDocumentAssignee($issued_doc_id);

				$reference_copy = Input::get('reference_copy');
				$reference_data = array();
				//now insert the multiple references to the copy of references table.
	    		for($k = 0; $k < count($reference_copy); $k++)
	    		{
		    		// get the the log data and insert it into the log table.
			    	$reference_data[] = array(
						'issued_doc_id' => $issued_doc_id,
						'reference_id' => $reference_copy[$k]
					);
	    		}
	    		// delete the references based on issued doc id.
	    		$referees_deleted = DB::connection('document_tracking')->table('copy_to_references')->where('issued_doc_id', $issued_doc_id)->delete();
	    		if($referees_deleted)
	    		{
			   		// now insert the reference_copy into the copy to references table.
			   		DB::connection('document_tracking')->table('copy_to_references')->insert($reference_data);
	    		}
		    	// get the the log data and insert it into the log table.
		    	$log_data[] = array(
					'action_table' => 'issued_docs',
					'record_id'=>$issued_doc_id,
					'user' => Auth::user()->username,
					'action' => "Updated",
					'created_at' => date('Y-m-d H:i:s')
				);
				docsModel::addLog($log_data);
		    	return Redirect::route('issuedDocList')->with("success","سند صادره موفقانه تجدید نظر شد.");
	        }
	        else
	        {
	            return Redirect::route('issuedDocList')->with("fail","مشکل وجود دارد لطفآ همرا با مسئول سیستم به تماس شوید .");
	        }
	    }
	}

	public function getSearchIssuedDocs()
	{
		//dd($_POST);
		$data['records'] = docsModel::getSearchIssuedDocuments("");
		if(Input::get('year') != "")
			$data['year'] = Input::get('year');
		else
			$data['year'] = "";

		return view('document_tracking.issued_documents.search_result',$data);
	}

	public function getPendingIncomingsOnYearlyBases()
	{
		$year = Input::get('year');
		$data['YearlyPendingIncomingNumber'] = docsModel::getPendingIncomingNumbers("",$year);

		return view('document_tracking.issued_documents.pendingIncomingsOnYearlyBases',$data);
	}

	public function exportIssuedDocumentsToExcel($issue_type=0)
	{
		if($issue_type == 1)
			$results = docsModel::getSearchIssuedDocuments("print");
		elseif($issue_type == 2)
			$results = docsModel::getSearchIssuedDocumentsWithoutIncoming("print");
		else
            $results = "";
        
         
		$curr_date = date('Y-m-d');
		Excel::load('excel_template/M&E Document Tracking Report.xlsx', function($file) use($results){
		//Excel::create('Filename', function($file) use($results){			
		$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($results){	
			$row = 3;
			$sheet->setFreeze('A3');
        if($results)
        {	
			foreach($results AS $item)
			{
				$sheet->getStyle('A3:Z' . $sheet->getHighestRow())->getAlignment()->setWrapText(true); 
				if($item->operations == 1) $operations = "حفظ";
				elseif($item->operations == 2) $operations = "صادر";
				else $operations = "";
				$assignee = getAssigneeName(getAssignee($item->incoming_doc_id));
	            $issued_doc_assignee = getAssigneeName(getIssuedDocAssignee($item->id));
				$copy_to_references = getCopyToReferences($item->id);
				//$summary = preg_replace("/&nbsp;/",' ',$item->summary);

				$sheet->setHeight($row, 50);

				$sheet->setCellValue('A'.$row.'',$row-2);
				if($item->incoming_doc_id != 0)
				{
					if(getMEDocumentTypeName(getRecordFieldBasedOnID('incoming_docs','doc_type',$item->incoming_doc_id)) != "")
						$sheet->setCellValue('B'.$row.'', getMEDocumentTypeName(getRecordFieldBasedOnID('incoming_docs','doc_type',$item->incoming_doc_id)));
					else
						$sheet->setCellValue('B'.$row.'', "");
				}
				else
				{
					if(getMEDocumentTypeName($item->issued_doc_type) != "")
						$sheet->setCellValue('B'.$row.'', getMEDocumentTypeName($item->issued_doc_type));
					else
						$sheet->setCellValue('B'.$row.'', "");
				}
				$sheet->setCellValue('H'.$row.'',$item->incoming_number);

				if($item->incoming_doc_id != 0)
		            $sheet->setCellValue('N'.$row.'',$assignee);
			    else
			     	$sheet->setCellValue('N'.$row.'',$issued_doc_assignee);

				$sheet->setCellValue('P'.$row.'',$operations);
				$sheet->setCellValue('Q'.$row.'',$item->issuing_number);
				$sheet->setCellValue('R'.$row.'',dmy_format(toJalali($item->issuing_date)));
				$sheet->setCellValue('S'.$row.'',$item->third_copy);
				$sheet->setCellValue('T'.$row.'',getDepartmentName($item->sender,getLanguage()));
				$sheet->setCellValue('U'.$row.'',$item->ministries);
				$sheet->setCellValue('V'.$row.'',strip_tags($item->subject));
				if(!empty($copy_to_references))
				{
					$references = "";
					foreach ($copy_to_references as $referees) {
						$references .= getRecordFieldBasedOnID('ministries','name_dr',$referees->reference_id)." , ";
					}
					$sheet->setCellValue('W'.$row.'', $references);
				}
				else
				{
					$sheet->setCellValue('W'.$row.'', "");
				}
				$sheet->setCellValue('X'.$row.'',$item->number_of_papers);
				if(!empty(getDocumentFileName($item->id)))
					$sheet->setCellValue('Y'.$row.'',"ضمیمه دارد باید در سیستم چک شوید");
				else
					$sheet->setCellValue('Y'.$row.'',"ضمیمه ندارد");
				$sheet->setCellValue('Z'.$row.'',$item->related_carton);
				
				$row++;
            }
            $sheet->setBorder('A3:Z'.($row-1).'', 'thin');
          }

			
		});
		
		})->export('xlsx');

	}

	public function getLoadDocSavePage($doc_id=0)
	{
		$doc_id = Crypt::decrypt($doc_id);
		$data['incoming_number'] = getRecordFieldBasedOnID("incoming_docs","incoming_number",$doc_id);
		$data['incoming_doc_id'] = $doc_id;
		return view('document_tracking.issued_documents.save_doc_form', $data);
	}

	public  function addSavedDoc()
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(					
			"subject"						=> "required"
		));

		if($validates->fails())
		{	
			return Redirect::route('saveDoc')->withErrors($validates)->withInput();
		}
		else
		{
	    
		    // get the form data.
		    $data = array(

		    	"incoming_number"				=> Input::get('incoming_number'),
		    	"subject"						=> Input::get('subject'),
		    	"related_carton"				=> Input::get('related_carton'),
		    	"incoming_doc_id"				=> Input::get('incoming_doc_id'),
		    	"operations"					=> 1,
		    	"user_id"						=> Auth::user()->id,
		    	"created_at"					=> date('Y-m-d H:i:s')
		    );

		    $insertedRecordID = docsModel::insertDocumentDetails($data);
		    if($insertedRecordID){
		    
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 1;
				$file_data = array();

				if(Input::hasFile('files'))
				{				
					foreach($files as $file) 
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension())
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    $destinationPath = 'documents/document_tracking_attachments';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);
						    			  			    
					    	$lastFileId = $insertedRecordID;
							if($auto == 1)
						    $filename = "Saved_doc_attachment_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Saved_doc_attachment_".$lastFileId."_".$auto.".".$extension;
							
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success) 
						    {
							    $data = array(
						   					'file_name'				=> $filename,
						   					'original_file_name'	=> $original_filename,
						   					'issued_doc_id'			=> $insertedRecordID,
						   					'user_id'				=> Auth::user()->id,
						   					"created_at"			=> date('Y-m-d H:i:s')
						   				);
						    	//call the model function to insert the data into upload table.
						    	docsModel::uploadFiles($data);
							} 
							else 
							{
								// delete the inserted records if the file upload had problems.
			   					DB::connection('document_tracking')->table('issued_docs')->where('id', $insertedRecordID)->delete();
							    // redirect back with errors.
					    		return Redirect::route('issuedDocList')->withErrors($validator);
							}
						}
						else
						{
							// delete the inserted records if the file upload had problems.
			   				DB::connection('document_tracking')->table('issued_docs')->where('id', $insertedRecordID)->delete();
							// redirect back with errors.
					    	return Redirect::route('issuedDocList')->withErrors($validator);
						}
						$auto ++;
					}
				}

		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'issued_docs',
					'record_id'=>$insertedRecordID,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
					);
				docsModel::addLog($Log);
				$incoming_id = Input::get('incoming_doc_id');
				// now update the document status of the documents that is issued in incoming table from 0 to 1 which means it is done now.
			   	DB::connection('document_tracking')->table('incoming_docs')->where('id', $incoming_id)->update(array('doc_status' => 1));
		    	return Redirect::route('issuedDocList')->with("success","سند موفقانه حفظ گردید .");
	        }
	        else
	        {
	            return Redirect::route('issuedDocList')->with("fail","مشکل وجود دارد لطفآ با مسئول سیستم به تماس شوید.");
	        }
	    }
	}

	public function approveSavedDocument()
	{
		$saved_doc_id = Input::get('saved_doc_id');
		$incoming_doc_id = Input::get('incoming_doc_id');
		$approved = docsModel::approveSavedDocs($saved_doc_id, $incoming_doc_id);
		if($approved)
		echo "<span class='fa fa-check-square-o' style='color:green'> Approved</span>";
		else return 0;
	}

	public function loadSavedDocEditPage($id)
	{
		$id = Crypt::decrypt($id);
		$data['record'] = docsModel::getSavedDocDetails($id);
		return view('document_tracking.issued_documents.edit_saved_doc',$data);
	}

	public  function updateSavedDoc($saved_doc_id)
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(					
			"subject"						=> "required"
		));

		if($validates->fails())
		{	
			return Redirect::route('editSavedDoc')->withErrors($validates)->withInput();
		}
		else
		{
	    
		    // get the form data.
		    $data = array(

		    	"incoming_number"				=> Input::get('incoming_number'),
		    	"subject"						=> Input::get('subject'),
		    	"related_carton"				=> Input::get('related_carton'),
		    	"operations"					=> 1,
		    	"user_id"						=> Auth::user()->id,
		    	"updated_at"					=> date('Y-m-d H:i:s')
		    );

		    $updated = docsModel::updateDocumentDetails($saved_doc_id, $data);
		    if($updated){
		    
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 1;
				$file_data = array();

				if(Input::hasFile('files'))
				{				
					foreach($files as $file) 
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension())
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    $destinationPath = 'documents/document_tracking_attachments';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);
						    			  			    
					    	$lastFileId = $saved_doc_id;
							if($auto == 1)
						    $filename = "Saved_doc_attachment_Edited".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Saved_doc_attachment_Edited".$lastFileId."_".$auto.".".$extension;
							
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success) 
						    {
							    $data = array(
						   					'file_name'				=> $filename,
						   					'original_file_name'	=> $original_filename,
						   					'issued_doc_id'			=> $saved_doc_id,
						   					'user_id'				=> Auth::user()->id,
						   					"created_at"			=> date('Y-m-d H:i:s')
						   				);
						    	//call the model function to insert the data into upload table.
						    	docsModel::uploadFiles($data);
							} 
							else 
							{
							    // redirect back with errors.
					    		return Redirect::route('issuedDocList')->withErrors($validator);
							}
						}
						else
						{
							// redirect back with errors.
					    	return Redirect::route('issuedDocList')->withErrors($validator);
						}
						$auto ++;
					}
				}

		    	// get the the log data and insert it into the log table.
				$Log = array(
					'action_table' => 'issued_docs',
					'record_id'=>$saved_doc_id,
					'user' => Auth::user()->username,
					'action' => "Updated",
					'created_at' => date('Y-m-d H:i:s')
					);
				docsModel::addLog($Log);
		    	return Redirect::route('issuedDocList')->with("success","سند حفظ شده موفقانه تجدید نظر شد ");
	        }
	        else
	        {
	            return Redirect::route('issuedDocList')->with("fail","مشکل وجود دارد لطفآ همرا با مسئول سیستم به تماس شوید .");
	        }
	    }
	}

	//change the status of record based on it's id to 1 if it's deleted.
	public function getDeleteIssuedDocuments()
	{
		$id = Input::get('record_id');
		//$id = Crypt::decrypt($id);
		$deleted = docsModel::deleteIssuedDocInfo($id);
		//dd($deleted);
		if($deleted)
	    {
	    	$removed = docsModel::removeDocFiles($id);
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'issued_docs',
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			docsModel::addLog($Log);
    		echo "<div class='alert alert-success'> سند وارده موفقانه حذف گردید !</div>";
        }
	}

	public function downloadDocFile($doc_id)
	{
		$file_id = Crypt::decrypt($doc_id);
		//get file name from database
		$fileName = docsModel::getFileName($file_id);
		//dd($fileName);
        //public path for file
        $file= public_path(). "/documents/document_tracking_attachments/".$fileName;
        //download file
        return Response::download($file, $fileName);
	}

	public function removeDocumentFile()
	{
		$file_id = Input::get('file_id');
		$file_name = Input::get('file_name');
		$deleted = docsModel::removeFile($file_id);
		if($deleted)
		{
			File::delete(public_path()."/documents/document_tracking_attachments/$file_name");
			return 1;
		}
	}

	// ================================================================= Document Issuing without Incoming functions ===================	

	public function loadIssueFormWithoutIncoming()
	{
		//dd($data['getPendingIncomingNumbers']);
		return view('document_tracking.issued_docs_without_incoming.issue_without_incoming_form');
	}

	public function loadIssuedDocListWithoutIncoming()
	{
        $data['records'] = docsModel::getDocumentsForIssuingWithoutIncoming();
        $data['user_dept'] = getUserDepartmentTree(Auth::user()->id); //User Dept Tree  

		if(Input::get('ajax') == 1)
		{
			return view('document_tracking.issued_docs_without_incoming.doc_issue_without_incoming_list_ajax', $data);
		}
		else
		{
			//load view to show searchpa result
			return view('document_tracking.issued_docs_without_incoming.doc_issue_without_incoming_list', $data);
		}
	}

	public function addIssuedDocWithoutIncoming()
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"issue_number"					=> "required",
			"issue_date"					=> "required",
			"third_copy"					=> "required",
			"sender"						=> "required",
			"sent_to"						=> "required",
			"subject"						=> "required",
			"reference_copy"				=> "required",
			"number_of_papers"				=> "required",
			"related_carton"				=> "required"
		));

		if($validates->fails())
		{	
			return Redirect::route('issueFormWithoutIncoming')->withErrors($validates)->withInput();
		}
		else
		{
	    	//dd($_POST);
		    //check the date type if it's shamsi or miladi.
			$issue_date = toGregorian(ymd_format(Input::get('issue_date')));
			$issue_type = Input::get('issue_type');
		    $incoming_ids = Input::get('incoming_ids');

	    	$data = array(

	    		"incoming_number"				=> "",
	    		"issued_doc_type"				=> Input::get('issued_doc_type'),
	    		"issuing_number"				=> Input::get('issue_number'),
	    		"issuing_date"					=> $issue_date,
	    		"third_copy"					=> Input::get('third_copy'),
	    		"sender"						=> Input::get('sender'),
	    		"sent_to"						=> Input::get('sent_to'),
	    		"subject"						=> Input::get('subject'),
	    		"number_of_papers"				=> Input::get('number_of_papers'),
	    		"related_carton"				=> Input::get('related_carton'),
	    		"incoming_doc_id"				=> "",
	    		"operations"					=> 2,
                "user_id"						=> Auth::user()->id,
                'department_id'                 => getUserDepartment(Auth::user()->id)->department, 
	    		"created_at"					=> date('Y-m-d H:i:s')
		    );

		    $inserted_id = DB::connection('document_tracking')->table('issued_docs')->insertGetID($data);

		    if($inserted_id)
		    {
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 1;
				$file_data = array();

				if(Input::hasFile('files'))
				{				
					foreach($files as $file) 
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension())
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    $destinationPath = 'documents/document_tracking_attachments';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);
						    			  			    
					    	$lastFileId = $inserted_id;
							if($auto == 1)
						    $filename = "Issued_doc_without_incoming_attachment_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Issued_doc_without_incoming_attachment_".$lastFileId."_".$auto.".".$extension;
							
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success) 
						    {
						    	$upload_data = array(
					   					'file_name'				=> $filename,
					   					'original_file_name'	=> $original_filename,
					   					'issued_doc_id'			=> $inserted_id,
					   					'user_id'				=> Auth::user()->id,
					   					"created_at"			=> date('Y-m-d H:i:s')
					   				);
						    	//call the model function to insert the data into upload table.
						    	docsModel::uploadFiles($upload_data);
							} 
							else 
							{
								// delete the inserted record if there were errors in uploading file.
								DB::connection('document_tracking')->table('issued_docs')->where('id', $inserted_id)->delete();
							    // redirect back with errors.
					    		return Redirect::route('issueForm')->withErrors($validator);
							}
						}
						else
						{
							// delete the inserted record if there were errors in uploading file.
							DB::connection('document_tracking')->table('issued_docs')->where('id', $inserted_id)->delete();
							// redirect back with errors.
					    	return Redirect::route('issueForm')->withErrors($validator);
						}
						$auto ++;
					}
				}
				if(Input::get('assignee') != "")
				{
					// insert the assignee.
					$assignee = Input::get('assignee');
					DB::connection('document_tracking')->table('assignees')->insert(array('issued_doc_id' => $inserted_id, 'assignee_id' => $assignee));
				}
				$reference_copy = Input::get('reference_copy');
				$reference_data = array();
				//now insert the multiple references to the copy of references table.
	    		for($k = 0; $k < count($reference_copy); $k++)
	    		{
		    		// get the the log data and insert it into the log table.
			    	$reference_data[] = array(
						'issued_doc_id' => $inserted_id,
						'reference_id' => $reference_copy[$k]
					);
	    		}
			   	// now insert the reference_copy into the copy to references table.
			   	DB::connection('document_tracking')->table('copy_to_references')->insert($reference_data);
		    	// get the the log data and insert it into the log table.
		    	$log_data[] = array(
					'action_table' => 'issued_docs',
					'record_id'=>$inserted_id,
					'user' => Auth::user()->username,
					'action' => "Added",
					'created_at' => date('Y-m-d H:i:s')
				);
				docsModel::addLog($log_data);
		    	return Redirect::route('issuedDocListWithoutIncoming')->with("success","سند صادره موفقانه اضافه شد .");
	        }
	        else
	        {
	            return Redirect::route('issuedDocListWithoutIncoming')->with("fail","مشکل وجود دارد لطفآ همرا با مسئول سیستم به تماس شوید .");
	        }

	    }
	}

	public function loadIssuedDocWithoutIncomingEditPage($issued_doc_id)
	{
		$issued_doc_id = Crypt::decrypt($issued_doc_id);
		$data['record'] = docsModel::getIssudDocDetails($issued_doc_id);
		$data['references'] = docsModel::getIssuedDocReferences($issued_doc_id);
		return view('document_tracking.issued_docs_without_incoming.edit_issued_without_incoming_doc',$data);
	}

	public  function postUpdateIssuedDocWithoutIncoming($issued_doc_id)
	{
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"issue_number"					=> "required",
			"issue_date"					=> "required",
			"third_copy"					=> "required",
			"sender"						=> "required",
			"sent_to"						=> "required",
			"subject"						=> "required",
			"reference_copy"				=> "required",
			"number_of_papers"				=> "required",
			"related_carton"				=> "required"
		));

		if($validates->fails())
		{	
			return Redirect::route('editIssuedDoc')->withErrors($validates)->withInput();
		}
		else
		{
			$issue_date = toGregorian(ymd_format(Input::get('issue_date')));

	    	$data = array(

	    		"issued_doc_type"				=> Input::get('issued_doc_type'),
	    		"issuing_number"				=> Input::get('issue_number'),
	    		"issuing_date"					=> $issue_date,
	    		"third_copy"					=> Input::get('third_copy'),
	    		"sender"						=> Input::get('sender'),
	    		"sent_to"						=> Input::get('sent_to'),
	    		"subject"						=> Input::get('subject'),
	    		"number_of_papers"				=> Input::get('number_of_papers'),
	    		"related_carton"				=> Input::get('related_carton'),
	    		"operations"					=> 2,
	    		"user_id"						=> Auth::user()->id,
	    		"updated_at"					=> date('Y-m-d H:i:s')
		    );

		    $updated = DB::connection('document_tracking')->table('issued_docs')->where('id', $issued_doc_id)->update($data);

		    if($updated)
		    {
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 1;
				$file_data = array();

				if(Input::hasFile('files'))
				{				
					foreach($files as $file) 
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file,
					            'extension'  => Str::lower($file->getClientOriginalExtension())
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    $destinationPath = 'documents/document_tracking_attachments';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);
						    			  			    
					    	$lastFileId = $issued_doc_id;
							if($auto == 1)
						    $filename = "Issued_doc_without_incoming_attachment_Editted".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Issued_doc_without_incoming_attachment_Editted".$lastFileId."_".$auto.".".$extension;
							
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success) 
						    {
						    	$upload_data = array(
					   					'file_name'				=> $filename,
					   					'original_file_name'	=> $original_filename,
					   					'issued_doc_id'			=> $issued_doc_id,
					   					'user_id'				=> Auth::user()->id,
					   					"created_at"			=> date('Y-m-d H:i:s')
					   				);
						    	//call the model function to insert the data into upload table.
						    	docsModel::uploadFiles($upload_data);
							} 
							else 
							{
							    // redirect back with errors.
					    		return Redirect::route('editIssuedDocWithoutIncoming')->withErrors($validator);
							}
						}
						else
						{
							// redirect back with errors.
					    	return Redirect::route('editIssuedDocWithoutIncoming')->withErrors($validator);
						}
						$auto ++;
					}
				}

				// if the issued document that does not have incoming document has assignee with it then remove it and insert the new one.
				docsModel::updateIssuedDocumentAssignee($issued_doc_id);

				$reference_copy = Input::get('reference_copy');
				$reference_data = array();
				//now insert the multiple references to the copy of references table.
	    		for($k = 0; $k < count($reference_copy); $k++)
	    		{
		    		// get the the log data and insert it into the log table.
			    	$reference_data[] = array(
						'issued_doc_id' => $issued_doc_id,
						'reference_id' => $reference_copy[$k]
					);
	    		}
	    		// delete the references based on issued doc id.
	    		$referees_deleted = DB::connection('document_tracking')->table('copy_to_references')->where('issued_doc_id', $issued_doc_id)->delete();
	    		if($referees_deleted)
	    		{
			   		// now insert the reference_copy into the copy to references table.
			   		DB::connection('document_tracking')->table('copy_to_references')->insert($reference_data);
	    		}
		    	// get the the log data and insert it into the log table.
		    	$log_data[] = array(
					'action_table' => 'issued_docs',
					'record_id'=>$issued_doc_id,
					'user' => Auth::user()->username,
					'action' => "Updated",
					'created_at' => date('Y-m-d H:i:s')
				);
				docsModel::addLog($log_data);
		    	return Redirect::route('issuedDocListWithoutIncoming')->with("success","سند صادره موفقانه تجدید نظر شد.");
	        }
	        else
	        {
	            return Redirect::route('issuedDocListWithoutIncoming')->with("fail","مشکل وجود دارد لطفآ همرا با مسئول سیستم به تماس شوید .");
	        }
	    }
	}

	public function getSearchIssuedDocsWithoutIncoming()
	{
		//dd($_POST);
		$data['records'] = docsModel::getSearchIssuedDocumentsWithoutIncoming("");
		if(Input::get('year') != "")
			$data['year'] = Input::get('year');
		else
			$data['year'] = "";

		return view('document_tracking.issued_docs_without_incoming.search_result',$data);
	}

	// ================================================================= Document Advanced Search functions ===================

	public function loadDocAdvancedSearchPage()
	{
		return view('document_tracking.advanced_search.search_form');
	}

	public function searchIncomingIssuedDocuments()
	{
		//dd($_POST);
		$data['records'] = docsModel::getSearchIncomingIssuingDocuments("");
		return view('document_tracking.advanced_search.search_result',$data);
	}

	public function yearlyIncomingNumbers()
	{
		$year = Input::get('year');
		$data['yearlyIncomingNumbers'] = getAllIncomingNumber("",$year);
		return view('document_tracking.advanced_search.yearlyIncomingNumbers',$data);
	}

	public function exportIncomingIssuedDocumentsToExcel()
	{
		//dd($_POST);
		$results = docsModel::getSearchIncomingIssuingDocuments('print');
		//dd($searchInventories);
		$curr_date = date('Y-m-d');
		Excel::load('excel_template/M&E Document Tracking Report.xlsx', function($file) use($results){
			//Excel::create('Filename', function($file) use($results){			
			$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($results){	
				$row = 3;
				$sheet->setFreeze('A3');
				
				foreach($results AS $item)
				{
					$sheet->getStyle('A3:Z' . $sheet->getHighestRow())->getAlignment()->setWrapText(true); 
					if(getMEDocumentTypeName($item->doc_type) != "")
						$doc_type = getMEDocumentTypeName($item->doc_type);
					else $doc_type = "";
					if($item->execution_type == 1) $execution_type = "عادی";
					elseif($item->execution_type == 2) $execution_type = "تفصیلی";
					else $execution_type = "";
					if($item->doc_status == 1) $doc_status = "اجراء";
					else $doc_status = "تحت اجراء";
					if($item->operations == 1) $operations = "حفظ";
					elseif($item->operations == 2) $operations = "صادر";
					else $operations = "";
					$copy_to_references = getCopyToReferences($item->issued_doc_id);
					//$summary = preg_replace("/&nbsp;/",' ',$item->summary);

					$sheet->setHeight($row, 50);

					$sheet->setCellValue('A'.$row.'',$row-2);
					$sheet->setCellValue('B'.$row.'',$doc_type);
					$sheet->setCellValue('C'.$row.'',$execution_type);
					$sheet->setCellValue('D'.$row.'',$item->doc_number);
					$sheet->setCellValue('E'.$row.'',dmy_format(toJalali($item->doc_date)));
					$sheet->setCellValue('F'.$row.'',$item->incoming_sender);
					$sheet->setCellValue('G'.$row.'',$item->incoming_sent_to);
					$sheet->setCellValue('H'.$row.'',$item->incoming_number);
					$sheet->setCellValue('I'.$row.'',dmy_format(toJalali($item->incoming_date)));
					$sheet->setCellValue('J'.$row.'',strip_tags($item->incoming_subject));
					$sheet->setCellValue('K'.$row.'',$item->incoming_number_of_papers);
					$sheet->setCellValue('L'.$row.'',dmy_format(toJalali($item->execution_date)));
					$sheet->setCellValue('M'.$row.'',$doc_status);
					if(!empty(getAssignee($item->incoming_id)))
					{
	                	$assignee = getAssigneeName(getAssignee($item->incoming_id));	                	
	                	$sheet->setCellValue('N'.$row.'', $assignee);
	                }
	                else
					{
						$sheet->setCellValue('N'.$row.'', "");
					}
					$sheet->setCellValue('O'.$row.'',getAssigneeDescription($item->incoming_id));
					$sheet->setCellValue('P'.$row.'',$operations);
					$sheet->setCellValue('Q'.$row.'',$item->issuing_number);
					$sheet->setCellValue('R'.$row.'',dmy_format(toJalali($item->issuing_date)));
					$sheet->setCellValue('S'.$row.'',$item->third_copy);
					$sheet->setCellValue('T'.$row.'',$item->issuing_sender);
					$sheet->setCellValue('U'.$row.'',$item->issuing_sent_to);
					$sheet->setCellValue('V'.$row.'',strip_tags($item->issuing_subject));
					if(!empty($copy_to_references))
					{
						$references = "";
						foreach ($copy_to_references as $referees) {
							$references .= getRecordFieldBasedOnID('ministries','name_dr',$referees->reference_id)." , ";
						}
						$sheet->setCellValue('W'.$row.'', $references);
					}
					else
					{
						$sheet->setCellValue('W'.$row.'', "");
					}
					$sheet->setCellValue('X'.$row.'',$item->issuing_number_of_papers);
					if(!empty(getDocumentFileName($item->issued_doc_id)))
						$sheet->setCellValue('Y'.$row.'',"ضمیمه دارد باید در سیستم چک شود");
					else
						$sheet->setCellValue('Y'.$row.'',"ضمیمه ندارد");
					$sheet->setCellValue('Z'.$row.'',$item->related_carton);
					
					$row++;
				}

				$sheet->setBorder('A3:Z'.($row-1).'', 'thin');
				
    		});
			
			})->export('xlsx');
	}
	// ====================================================================== M&E Documents Trakcing Dashboard controller functions =====================================================

	public function loadMEDashboard()
	{
		$data['total_makatib'] = docsModel::getDocumentStats('','',1, "");
		$data['under_process_makatib'] = docsModel::getDocumentStats('','under_process', 1, "");
		$data['saved_makatib'] = docsModel::getDocumentStats('','saved', 1, "");
		$data['issued_makatib'] = docsModel::getDocumentStats('','issued', 1, "");
		//dd($data['under_process_makatib']);
		$data['total_peshnehadat'] = docsModel::getDocumentStats('','',2, "");
		$data['under_process_peshnehadat'] = docsModel::getDocumentStats('','under_process', 2, "");
		$data['saved_peshnehadat'] = docsModel::getDocumentStats('','saved', 2, "");
		$data['issued_peshnehadat'] = docsModel::getDocumentStats('','issued', 2, "");

		$data['total_hedayaat'] = docsModel::getDocumentStats('','',3, "");
		$data['under_process_hedayat'] = docsModel::getDocumentStats('','under_process', 3, "");
		$data['saved_hedayat'] = docsModel::getDocumentStats('','saved', 3, "");
		$data['issued_hedayat'] = docsModel::getDocumentStats('','issued', 3, "");

		$data['total_hedayat_report'] = docsModel::getDocumentStats('','',4, "");
		$data['under_process_hedayat_report'] = docsModel::getDocumentStats('','under_process', 4, "");
		$data['saved_hedayat_report'] = docsModel::getDocumentStats('','saved', 4, "");
		$data['issued_hedayat_report'] = docsModel::getDocumentStats('','issued', 4, "");

		$data['total_other_reports'] = docsModel::getDocumentStats('','',5, "");
		$data['under_process_other_reports'] = docsModel::getDocumentStats('','under_process', 5, "");
		$data['saved_other_reports'] = docsModel::getDocumentStats('','saved', 5, "");
		$data['issued_other_reports'] = docsModel::getDocumentStats('','issued', 5, "");

		// get total maktoob, peshnehat and hedayat.
		$data['total_incoming_documents'] = docsModel::getDocumentStats('total_incoming',"","");
		$data['maktoob'] = docsModel::getDocumentStats('',"",1, "");
		$data['peshnehad'] = docsModel::getDocumentStats('',"",2, "");
		$data['hedayat'] = docsModel::getDocumentStats('',"",3, "");
		$data['hedayat_report'] = docsModel::getDocumentStats('',"",4, "");
		$data['other_reports'] = docsModel::getDocumentStats('',"",5, "");

		// get total of documents based on execution type.
		$data['total_of_normal_docs'] = docsModel::getDocumentExecutionTypeStats(1, "");
		$data['total_of_descriptive_docs'] = docsModel::getDocumentExecutionTypeStats(2, "");

		// get total and issued document type based stats of documents which does not have incoming.
		$data['total_issued_docs_without_incoming'] = docsModel::getDocumentStatsWithoutIncoming("","");
		$data['issued_doc_maktoob_without_incoming'] = docsModel::getDocumentStatsWithoutIncoming(1, "");
		$data['issued_doc_peshnehad_without_incoming'] = docsModel::getDocumentStatsWithoutIncoming(2, "");
		$data['issued_doc_hedayat_without_incoming'] = docsModel::getDocumentStatsWithoutIncoming(3, "");
		$data['issued_doc_hedayat_report_without_incoming'] = docsModel::getDocumentStatsWithoutIncoming(4, "");
		$data['issued_doc_other_reports_without_incoming'] = docsModel::getDocumentStatsWithoutIncoming(5, "");

		return view('document_tracking.dashboard', $data);
	}

	public function getSearchDashboardData()
	{
		$year = Input::get('year');

		$data['total_makatib'] = docsModel::getDocumentStats('','',1,$year);
		$data['under_process_makatib'] = docsModel::getDocumentStats('','under_process', 1,$year);
		$data['saved_makatib'] = docsModel::getDocumentStats('','saved', 1,$year);
		$data['issued_makatib'] = docsModel::getDocumentStats('','issued', 1,$year);
		//dd($data['under_process_makatib']);
		$data['total_peshnehadat'] = docsModel::getDocumentStats('','',2,$year);
		$data['under_process_peshnehadat'] = docsModel::getDocumentStats('','under_process', 2,$year);
		$data['saved_peshnehadat'] = docsModel::getDocumentStats('','saved', 2,$year);
		$data['issued_peshnehadat'] = docsModel::getDocumentStats('','issued', 2,$year);

		$data['total_hedayaat'] = docsModel::getDocumentStats('','',3,$year);
		$data['under_process_hedayat'] = docsModel::getDocumentStats('','under_process', 3,$year);
		$data['saved_hedayat'] = docsModel::getDocumentStats('','saved', 3,$year);
		$data['issued_hedayat'] = docsModel::getDocumentStats('','issued', 3,$year);

		$data['total_hedayat_report'] = docsModel::getDocumentStats('','',4,$year);
		$data['under_process_hedayat_report'] = docsModel::getDocumentStats('','under_process', 4,$year);
		$data['saved_hedayat_report'] = docsModel::getDocumentStats('','saved', 4,$year);
		$data['issued_hedayat_report'] = docsModel::getDocumentStats('','issued', 4,$year);

		$data['total_other_reports'] = docsModel::getDocumentStats('','',5,$year);
		$data['under_process_other_reports'] = docsModel::getDocumentStats('','under_process', 5,$year);
		$data['saved_other_reports'] = docsModel::getDocumentStats('','saved', 5,$year);
		$data['issued_other_reports'] = docsModel::getDocumentStats('','issued', 5,$year);

		// get total maktoob, peshnehat and hedayat.
		$data['total_incoming_documents'] = docsModel::getDocumentStats('total_incoming',"",0,$year);
		$data['maktoob'] = docsModel::getDocumentStats('',"",1,$year);
		$data['peshnehad'] = docsModel::getDocumentStats('',"",2,$year);
		$data['hedayat'] = docsModel::getDocumentStats('',"",3,$year);
		$data['hedayat_report'] = docsModel::getDocumentStats('',"",4,$year);
		$data['other_reports'] = docsModel::getDocumentStats('',"",5,$year);

		// get total of documents based on execution type.
		$data['total_of_normal_docs'] = docsModel::getDocumentExecutionTypeStats(1,$year);
		$data['total_of_descriptive_docs'] = docsModel::getDocumentExecutionTypeStats(2,$year);

		// get total and issued document type based stats of documents which does not have incoming.
		$data['total_issued_docs_without_incoming'] = docsModel::getDocumentStatsWithoutIncoming("",$year);
		$data['issued_doc_maktoob_without_incoming'] = docsModel::getDocumentStatsWithoutIncoming(1,$year);
		$data['issued_doc_peshnehad_without_incoming'] = docsModel::getDocumentStatsWithoutIncoming(2,$year);
		$data['issued_doc_hedayat_without_incoming'] = docsModel::getDocumentStatsWithoutIncoming(3,$year);
		$data['issued_doc_hedayat_report_without_incoming'] = docsModel::getDocumentStatsWithoutIncoming(4,$year);
		$data['issued_doc_other_reports_without_incoming'] = docsModel::getDocumentStatsWithoutIncoming(5,$year);
		$data['year'] = $year;

		return view('document_tracking.dashboard_search_result', $data);
	}

}

?>