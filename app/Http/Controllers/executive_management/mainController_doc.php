<?php
namespace App\Http\Controllers\executive_management;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\executive_management\department;
use App\models\executive_management\document_model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Validator;
use Response;
use File;
use Str;

class mainController_doc extends Controller {

	public function loadRecordsList(){


		$pashnahad=document_model::get_document_pashnahad();
		$farman=document_model::get_document_farman();
		$istalam=document_model::get_document_istalam();
		$feceno=document_model::get_document_feceno();

		$taenat=document_model::get_document_tayenat();

		return view::make('executive_management.managment_list',array('pashnahad'=>$pashnahad,'farman'=>$farman,'istalam'=>$istalam,'feceno'=>$feceno,'taenat'=>$taenat));
	}

	public function get_hakom(request $request){

	    $data=document_model::get_document_hakom();
		if(Input::get('ajax') == 1)
		{
				return view::make('executive_management.paginate_hakom',$data);
		}
		else
		{
			//load view to show searchpa result
				return view::make('executive_management.get_hakom',$data);
		}
	}






	public function get_maktob(){

		$data=document_model::get_document_maktob();
		if(Input::get('ajax') == 1)
		{
				return view::make('executive_management.paginate_maktob',$data);
		}
		else
		{
				return view::make('executive_management.get_maktob',$data);

			}}
	public function get_copy_som(){

		$data=document_model::get_document_copy_som();
		if(Input::get('ajax') == 1)
		{
				return view::make('executive_management.paginate_copy_som',$data);
		}
		else
		{
				return view::make('executive_management.get_copy_som',$data);

			}}


	public function get_mateb_warada(){

		$data=document_model::get_makatab_warada();
		if(Input::get('ajax') == 1)
		{
				return view::make('executive_management.paginate_warda_maktob',$data);
		}
		else
		{
				return view::make('executive_management.get_makatab_warada',$data);

			}}

	public function get_pashnahad(){

	$data=document_model::get_document_pashnahad();


			if(Input::get('ajax') == 1)
		{
				return view::make('executive_management.paginate_peshnahad',$data);
		}
		else
		{


				return view::make('executive_management.get_pashnahad',$data);
			}
		}

	public function get_farman(){

		$data=document_model::get_document_farman();


			if(Input::get('ajax') == 1)
		{
				return view::make('executive_management.paginate_farman',$data);
		}
		else
		{


				return view::make('executive_management.get_farman',$data);

			}
		}

	public function get_istalam(){

		$data=document_model::get_document_istalam();


			if(Input::get('ajax') == 1)
		{
				return view::make('executive_management.paginate_istalam',$data);
		}
		else
		{


				return view::make('executive_management.get_istalam',$data);

			}
		}

	public function get_feceno(){

		$data=document_model::get_document_feceno();


			if(Input::get('ajax') == 1)
		{
				return view::make('executive_management.paginate_feceno',$data);
		}
		else
		{


				return view::make('executive_management.get_feceno',$data);

			}
		}

	public function get_maktob_hedayati(){

		$data=document_model::get_document_maktob_he();


			if(Input::get('ajax') == 1)
		{
				return view::make('executive_management.paginate_maktob_hadayati',$data);
		}
		else
		{
				return view::make('executive_management.get_maktob_hadayati',$data);

			}}

			//get get_waraqa_darkhasti data
	public function get_waraqa_darkhasti(){



		$data=document_model::get_document_waraqa_darkhasti();


			if(Input::get('ajax') == 1)
		{
				return view::make('executive_management.paginate_waraqa_darkhasti',$data);
		}
		else
		{
				return view::make('executive_management.get_waraqa_darkhasti',$data);

			}}

	public function get_pahshnahad_hedayati(){

		$data=document_model::get_document_pashnahad_he();
				if(Input::get('ajax') == 1)
					{
							return view::make('executive_management.paginate_pashnahad_hidayati',$data);
					}
					else
					{

				return view::make('executive_management.get_pashnahad_hadayati',$data);

			}}

	public function get_feceno_hadayati(){

		$data=document_model::get_document_feceno_he();
			if(Input::get('ajax') == 1)
				{
						return view::make('executive_management.paginate_feceno_hadayati',$data);
				}
				else
				{

				return view::make('executive_management.get_feceno_hadayati',$data);

			}}
	public function get_istalam_hadayati_data(){

		$data=document_model::get_document_istalam_he();
			if(Input::get('ajax') == 1)
				{
						return view::make('executive_management.paginate_istalam_hidayati',$data);
				}
				else
				{

				return view::make('executive_management.get_istalam_hidayati',$data);

			}}

	public function get_tayenat(){

		$data=document_model::get_document_tayenat();
				if(Input::get('ajax') == 1)
		{
				return view::make('executive_management.paginate_tayenat',$data);
		}
		else
		{


				return view::make('executive_management.get_tayenat',$data);

			}
			}
	public function get_all_search(){


				return view::make('executive_management.get_all_search');

			}
	public function get_yearly_search(){


				return view::make('executive_management.yearly_search');

			}
	public function loadpage_insert(){
		$department=department::get_department();

		return view::make('executive_management.insert_document',['department'=>$department]);
	}

	public function insert_doc(Request $request){
		//validate the input fields
	    $validates = Validator::make(Input::all(), array(
			"document_type"						=> "required",
			"number"					=> "required"
			));

		if($validates->fails())
		{
			return Redirect::route('insertForm_executive')->withErrors($validates)->withInput();
		}
		else
		{

		    //check the date type if it's shamsi or miladi.
			$date = toGregorian(gregorian_format(Input::get('date')));

		    // get the form data.
		    $data = array(
		    		"number_documents"	    => Input::get('number'),
		    		"date"					=> $date,
		    		"type_id"				=> Input::get('type_doc'),
		    		"department_id"			=> Input::get('department_id'),
		    		"description"			=> Input::get('description'),
		    		"document_type"			=> Input::get('document_type'),
		    		"name"					=> Input::get('name'),
		    		"father_name"			=> Input::get('father_name'),
		    		// "created_by"			=> Auth::user()->id,

		    	);
			$insertedRecordID=\DB::connection('executive_management')->table("executive_documents")->insert($data);
		    if($insertedRecordID){
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
 				$auto = 1;

				if(Input::hasFile('files'))
				{
					foreach($files as $file)
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    // path is root/uploads
						    $destinationPath = 'uploads_doc/';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);

						    $lastid = DB::connection('executive_management')->table('executive_documents')->orderBy('id', 'desc')->pluck('id');


					    	$lastFileId = $lastid;
							if($auto == 1)
						    $filename = "Attachment_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Attachment_".$lastFileId."_".$auto.".".$extension;

						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success)
						    {
						   // $lastid = DB::connection('executive_management')->table('executive_documents')->orderBy('id', 'desc')->pluck('id');
							    $data = array(
						   					'file_name'				=> $filename,
						   					'ex_doc_id'				=> $lastid,
						   				);
						    	//call the model function to insert the data into upload table.
						    $insertfile=\DB::connection('executive_management')->table("uploads")->insert($data);
							}
							else
							{
							    // redirect back with errors.
					    		return Redirect::route('insertForm_executive')->withErrors($validator);
							}
						}$auto ++;


					}
				}
		    	// get the the log data and insert it into the log table.

		    	return Redirect::route('insertForm_executive')->with("success","Information successfully Added.");
	        }
	        else
	        {
	            return Redirect::route('insertForm_executive')->with("fail","An error occured please try again or contact system developer.");
	        }}
	    }




	public function delete($id){

	$row=document_model::find($id);
	$row->status='1';

	$row->save();

		if($row)
        {
            return \Redirect::route("recordsList_executive")->with("success","You have successfuly deleted the record <span style='color:red;font_weight:bold;'></span>");
        }
        else
        {
            return \Redirect::route("recordsList_executive")->with("fail","An error occured please try again.");
        }

		}

		public function select_row_doc($id){
			$data=document_model::find($id);
			$department=department::get_department();
			$files=\DB::connection('executive_management')->table('uploads')->select('uploads.*','uploads.id as file_id')->where('ex_doc_id',$id)->get();

			return view::make('executive_management.document_update',array('data'=>$data,'department'=>$department,'files'=>$files));

		}
		public function update(Request $request, $id){


			$document= new document_model;
			$document=document_model::find($id);
			$validates = Validator::make(Input::all(), array(
			"document_type"						=> "required",
			"number"					=> "required"
			));

		if($validates->fails())
		{
			return Redirect::route('select_data_doc')->withErrors($validates)->withInput();
		}
		else
		{

		    //check the date type if it's shamsi or miladi.
			$date = toGregorian(gregorian_format(Input::get('date')));

		    // get the form data.
		    $data = array(
		    		"number_documents"	    => Input::get('number'),
		    		"date"					=> $date,
		    		"type_id"				=> Input::get('type_doc'),
		    		"department_id"			=> Input::get('department_id'),
		    		"description"			=> Input::get('description'),
		    		"document_type"			=> Input::get('document_type'),
		    		"name"					=> Input::get('name'),
		    		"father_name"			=> Input::get('father_name'),

		    	);
			$insertedRecordID=\DB::connection('executive_management')->table("executive_documents")->where('id',$id)->update($data);
		    if($insertedRecordID){
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 50;
				$file_data = array();

				if(Input::hasFile('files'))
				{
					foreach($files as $file)
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    // path is root/uploads
						    $destinationPath = 'uploads_doc/';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);

						    $lastid = DB::connection('executive_management')->table('executive_documents')->orderBy('id', 'desc')->pluck('id');
						    $file_id = DB::connection('executive_management')->table('uploads')->orderBy('id', 'desc')->pluck('id');
						   $file_id++;

					    	$lastFileId = $lastid;
							if($auto == 50)
						    $filename = "Attachment_".$lastFileId."_".$file_id.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Attachment_".$lastFileId."_".$file_id.".".$extension;

						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success)
						    {
						   $lastid = $id;
							    $data = array(
						   					'file_name'				=> $filename,
						   					'ex_doc_id'				=> $lastid,
						   				);
						    	//call the model function to insert the data into upload table.
						    $insertfile=\DB::connection('executive_management')->table("uploads")->insert($data);
							}
							else
							{
							    // redirect back with errors.
					    		return Redirect::route('select_data_doc')->withErrors($validator);
							}
						}$auto ++;


					}
				}
		    	// get the the log data and insert it into the log table.

		    	return Redirect::route('recordsList_executive')->with("success","Information successfully updeted.");
	        }
	        else
	        {
	            return Redirect::route('select_data_doc')->with("fail","An error occured please try again or contact system developer.");
	        }}
	    }


		public function show_file_toprint($id){



				$row=\DB::connection('executive_management')->table('uploads')->select('uploads.*')->where('ex_doc_id',$id)->get();


				return view::make('executive_management.show_file',['row'=>$row]);


		}

		public function delete_images(){


			$img_id=trim(Input::get('img_id'));

			$row=\DB::connection('executive_management')->table('uploads')->where('id',$img_id)->get();
			foreach ($row as $val ) {

				if( File::delete('uploads_doc/'.$val->file_name))

					 $row=\DB::connection('executive_management')->table('uploads')->where('id',$val->id)->delete();


		}

	}
		public function insertDepartment(Request $request){

			$validates = Validator::make(Input::all(), array(
			"name"						=> "required|unique:department",
			));
			if($validates->fails())
			{
				return Redirect::route('recordsList_executive')->withErrors($validates)->withInput();
			}else{
			$data=array(
				'name'=>Input::get('name'),
				'name_en'=>Input::get('name_en'),
				'parent'=>0,
				'unactive'=>0,
				'user_id'=>Auth::user()->id
			);

			$department=\DB::connection('executive_management')->table("department")->insert($data);
			return Redirect::route('recordsList_executive')->with("success","Information successfully Added.");

			}
	}

	
}



?>
