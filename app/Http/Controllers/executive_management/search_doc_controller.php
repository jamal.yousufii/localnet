<?php
namespace App\Http\Controllers\executive_management;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\executive_management\department;
use App\models\executive_management\document_model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Validator;
use Response;
use File;
use Excel;
class search_doc_controller extends Controller {

	public function search_live(){

			$search=trim(Input::get('field_value'));
			$cat_id=trim(Input::get('cat_id'));

			$date=explode('-', $search);
			if(count($date)==3){
			$date_val= toGregorian(gregorian_format(Input::get('field_value')));

			$rows=\DB::table('executive_management.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
			->whereRaw(" status= 0 AND document.document_type = ".$cat_id." AND (document.date like '%".$date_val."%'


				)")
			->orderby('document.id','desc')->get();
					$data['rows'] = $rows;
				return view::make('executive_management.search_live',$data);


			}


			$rows=\DB::table('executive_management.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
			->whereRaw(" status= 0 AND document.document_type = ".$cat_id." AND (document.number_documents like '%".$search."%'
				OR dept.name like '%".$search."%'
				OR document.name like '%".$search."%'
				OR document.father_name like '%".$search."%'
				OR document.description like '%".$search."%'
				)")
			->orderby('document.id','desc')->get();
					$data['rows'] = $rows;
				return view::make('executive_management.search_live',$data);



		}
		public function search_live_feceno(){

			$search=trim(Input::get('field_value'));
			$cat_id=trim(Input::get('cat_id'));

			$date=explode('-', $search);
			if(count($date)==3){
		$date_val= toGregorian(gregorian_format(Input::get('field_value')));

			$rows=\DB::table('executive_management.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
			->whereRaw(" status= 0 AND document.document_type = ".$cat_id." AND (document.date like '%".$date_val."%')")
			->orderby('document.id','desc')->get();
					$data['rows'] = $rows;
				return view::make('executive_management.search_fecen_live',$data);


			}


			$rows=\DB::table('executive_management.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
			->whereRaw(" status= 0 AND document.document_type = ".$cat_id." AND (document.number_documents like '%".$search."%'
				OR dept.name like '%".$search."%'
				OR document.name like '%".$search."%'
				OR document.father_name like '%".$search."%'
				OR document.description like '%".$search."%'
				)")
			->orderby('document.id','desc')->get();
					$data['rows'] = $rows;
				return view::make('executive_management.search_fecen_live',$data);


		}

		public function get_all_document(){

			$name=trim(Input::get('name'));
			$father_name=trim(Input::get('father_name'));
			$all_search=trim(Input::get('all_search'));
			$result=trim(Input::get('result'));

			if($all_search ==''){
				if($result !=""){


					$rows=\DB::table('executive_management.executive_documents as document')
					->select('document.*','dept.name as dept_name')
					->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
					->whereRaw(" status= 0  AND ( document.result = '".$result."'
						)")
					->orderby('document.id','desc');
						$data['records']=$rows->get();
						return view::make('executive_management.search_name_f_name',$data);


				}

					$rows=\DB::table('executive_management.executive_documents as document')
					->select('document.*','dept.name as dept_name')
					->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
					->whereRaw(" status= 0  AND (document.name like '%".$name."%'
						AND document.father_name like '%".$father_name."%'
						)")
					->orderby('document.id','desc');
						$data['records']=$rows->get();
						return view::make('executive_management.search_name_f_name',$data);


			}else{
				$date=explode('-', $all_search);
					if(count($date)==3){
				$date_val= toGregorian(gregorian_format(Input::get('all_search')));

				$rows=\DB::table('executive_management.executive_documents as document')
				->select('document.*','dept.name as dept_name')
				->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
				->whereRaw(" status= 0 AND

						 document.date ='".$date_val."'
						 OR document.result_date ='".$date_val."'

						")
				->orderby('document.id','desc')->get();
							$data['records'] = $rows;
					return view::make('executive_management.search_name_f_name',$data);



			}else{

				$rows=\DB::table('executive_management.executive_documents as document')
				->select('document.*','dept.name as dept_name')
				->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
				->whereRaw(" status= 0 AND (document.number_documents like '%".$all_search."%'
						OR dept.name like '%".$all_search."%'
						OR document.name like '%".$all_search."%'
						OR document.father_name like '%".$all_search."%'
						OR document.related_to like '%".$all_search."%'
						OR document.receiver like '%".$all_search."%'
						OR document.description like '%".$all_search."%'
						)")
				->orderby('document.id','desc')->get();
							$data['records'] = $rows;
					return view::make('executive_management.search_name_f_name',$data);

		}

			}

				}//hadayat document function
			public function search_live_hadayat(){

					$search=trim(Input::get('field_value'));
					$cat_id=trim(Input::get('cat_id'));
					$date=explode('-', $search);
					if(count($date)==3){
				$date_val= toGregorian(gregorian_format(Input::get('field_value')));

					$rows=\DB::table('executive_management.executive_documents as document')
					->select('document.*','dept.name as dept_name')
					->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
					->whereRaw(" status= 0 AND document.document_type = ".$cat_id." AND (document.date like '%".$date_val."%' OR document.result_date like '%".$date_val."%'


						)")
					->orderby('document.id','desc')->get();
							$data['rows'] = $rows;
						return view::make('executive_management.search_hadayat_doc',$data);


					}


					$rows=\DB::table('executive_management.executive_documents as document')
					->select('document.*','dept.name as dept_name')
					->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
					->whereRaw(" status= 0 AND document.document_type = ".$cat_id." AND (document.number_documents like '%".$search."%'
						OR dept.name like '%".$search."%'
						OR document.receiver like '%".$search."%'
						OR document.related_to like '%".$search."%'
						OR document.description like '%".$search."%'
						)")
					->orderby('document.id','desc')->get();
							$data['rows'] = $rows;
						return view::make('executive_management.search_hadayat_doc',$data);





		}

			public function search_warada_maktobs(){

						$search=trim(Input::get('field_value'));
					$cat_id=trim(Input::get('cat_id'));

					$date=explode('-', $search);
					if(count($date)==3){
					$date_val= toGregorian(gregorian_format(Input::get('field_value')));

					$rows=\DB::table('executive_management.executive_documents as document')
					->select('document.*','dept.name as dept_name')
					->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
					->whereRaw(" status= 0 AND document.document_type = ".$cat_id." AND (document.date like '%".$date_val."%'


						)")
					->orderby('document.id','desc')->get();
							$data['rows'] = $rows;
						return view::make('executive_management.search_warada_maktob',$data);


					}


					$rows=\DB::table('executive_management.executive_documents as document')
					->select('document.*','dept.name as dept_name')
					->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
					->whereRaw(" status= 0 AND document.document_type = ".$cat_id." AND (document.number_documents like '%".$search."%'
						OR dept.name like '%".$search."%'
						OR document.name like '%".$search."%'
						OR document.father_name like '%".$search."%'
						OR document.description like '%".$search."%'
						OR document.related_to like '%".$search."%'
						)")
					->orderby('document.id','desc')->get();
							$data['rows'] = $rows;
						return view::make('executive_management.search_warada_maktob',$data);

		}

		public function get_all_yearly_document(){

			$date_from=Input::get('from_year');
			$date_to=Input::get('to_year');
			$doc_type=trim(Input::get('doc_type'));

			$result=trim(Input::get('result'));

			if($date_from and $date_to !=""){
			$from_date = toGregorian(gregorian_format($date_from));
			$to_date = toGregorian(gregorian_format($date_to));

			$rows=\DB::table('executive_management.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id');

			$rows->where('status','0');
			$rows->whereBetween('date',[$from_date,$to_date]);
			if($result !="")
			 $rows->where('result',$result);
			if($doc_type !="")
			$rows->where('document_type',$doc_type);

			$data['records'] = $rows->get();;
					return view::make('executive_management.search_name_f_name',$data);
			}else{


			$rows=\DB::table('executive_management.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id');

			$rows->where('status','0');
			if($result !="")
			 $rows->where('result',$result);
			if($doc_type !="")
			$rows->where('document_type',$doc_type);

			$data['records'] = $rows->get();;
					return view::make('executive_management.search_name_f_name',$data);



				}
			}

			//waraq darkhasti search
		public function search_live_waraq_darkhasti(){

					$search=trim(Input::get('field_value'));
					$cat_id=trim(Input::get('cat_id'));
					$date=explode('-', $search);
					if(count($date)==3){
				$date_val= toGregorian(gregorian_format(Input::get('field_value')));

					$rows=\DB::table('executive_management.executive_documents as document')
					->select('document.*','dept.name as dept_name')
					->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
					->whereRaw(" status= 0 AND document.document_type = ".$cat_id." AND (document.date like '%".$date_val."%' OR document.result_date like '%".$date_val."%'


						)")
					->orderby('document.id','desc')->get();
							$data['rows'] = $rows;
						return view::make('executive_management.search_waraq_darkhasti',$data);


					}


					$rows=\DB::table('executive_management.executive_documents as document')
					->select('document.*','dept.name as dept_name')
					->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')
					->whereRaw(" status= 0 AND document.document_type = ".$cat_id." AND (document.name like '%".$search."%'
						OR dept.name like '%".$search."%'
						OR document.receiver like '%".$search."%'
						OR document.related_to like '%".$search."%'
						OR document.description like '%".$search."%'
						)")
					->orderby('document.id','desc')->get();
							$data['rows'] = $rows;
						return view::make('executive_management.search_waraq_darkhasti',$data);





		}
		public function export_excel(){


				$date_from=Input::get('from_year');
				$date_to=Input::get('to_year');
				$doc_type=trim(Input::get('doc_type'));
				$result=trim(Input::get('result'));


				if($date_from and $date_to !=""){
				$from_date = toGregorian(gregorian_format($date_from));
				$to_date = toGregorian(gregorian_format($date_to));

				$rows=\DB::table('executive_management.executive_documents as document')
				->select('document.*','dept.name as dept_name')
				->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id');

				$rows->where('status','0');
				$rows->whereBetween('date',[$from_date,$to_date]);
				if($result !="")
				 $rows->where('result',$result);
				if($doc_type !="")
				$rows->where('document_type',$doc_type);


				$data = $rows->get();
				$curr_date = date('Y-m-d');
				Excel::load('excel_template/executive_document_data.xlsx', function($file) use($data){
			//Excel::create('Filename', function($file) use($data){
			$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){
				$row = 3;
				$sheet->setFreeze('A3');

				foreach($data AS $item)
				{
					$sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true);
					//
					// $summary = preg_replace("/&nbsp;/",' ',$item->product_description);
					// $text = preg_replace("/&ndash;/",'-',$summary);
					if($item->document_type =='1'){
						$document_type="حکم";
					}elseif($item->document_type =='2'){
						$document_type="مکتوب";
					}elseif($item->document_type =='3'){
						$document_type="پشنهاد";
					}elseif($item->document_type =='4'){
						$document_type="فرمان";
					}elseif($item->document_type =='5'){
						$document_type="استعلام";
					}elseif($item->document_type =='6'){
						$document_type="ف س ۹";
					}elseif($item->document_type =='7'){
						$document_type="مکتوب هدایتی";
					}elseif($item->document_type =='8'){
						$document_type="پیشنهاد هدایتی";
					}elseif($item->document_type =='9'){
						$document_type="ف س ۹ هدایتی";
					}elseif($item->document_type =='10'){
						$document_type="تعيينات";
					}elseif($item->document_type =='11'){
						$document_type="مکتوب وارده";
					}elseif($item->document_type =='12'){
						$document_type="استعلام هدایتی";
					}elseif($item->document_type =='13'){
						$document_type="کاپی سوم";
					}elseif($item->document_type =='14'){
						$document_type="ورقه درخواستی";
					}elseif($item->document_type =='15'){
						$document_type="مصوبات";
					}
					//set value of sadira and warida
					if($item->type_id =='1'){
						$type_name='صادره';
					}elseif($item->type_id =='2'){
						$type_name='وارده';

					}elseif($item->type_id ==''){
						$type_name='';
					}

					//set value of ajra na ajra and hafzya
					if($item->result =='1'){
						$result='اجراه';
					}elseif($item->result =='2'){
						$result='نااجراه';
					}elseif($item->result =='3'){
						$result='حفظیه';
					}elseif($item->result ==''){
						$result='';
					}

					$sheet->setHeight($row, 20);
					$sheet->setCellValue('K'.$row.'',$row-2);
					$sheet->setCellValue('J'.$row.'',$item->number_documents);
					$sheet->setCellValue('I'.$row.'',$document_type);
					$sheet->setCellValue('H'.$row.'',checkEmptyDate($item->date));
					$sheet->setCellValue('G'.$row.'',$type_name);
					$sheet->setCellValue('F'.$row.'',$item->dept_name);
					$sheet->setCellValue('E'.$row.'',$item->description);
					$sheet->setCellValue('D'.$row.'',$item->related_to);
					$sheet->setCellValue('C'.$row.'',$item->receiver);
					$sheet->setCellValue('B'.$row.'',$result);
					$sheet->setCellValue('A'.$row.'',checkEmptyDate($item->result_date));



					//$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));

					$row++;
				}

				$sheet->setBorder('A3:P'.($row-1).'', 'thin');

    		});

			})->export('xlsx');


			}else{

				$rows=\DB::table('executive_management.executive_documents as document')
				->select('document.*','dept.name as dept_name')
				->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id');

				$rows->where('status','0');
				if($result !="")
				 $rows->where('result',$result);
				if($doc_type !="")
				$rows->where('document_type',$doc_type);

				$data = $rows->get();;
				$curr_date = date('Y-m-d');
				Excel::load('excel_template/executive_document_data.xlsx', function($file) use($data){
	  		//Excel::create('Filename', function($file) use($data){
		  	$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){
				$row = 3;
				$sheet->setFreeze('A3');

				foreach($data AS $item)
				{
					$sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true);
					//
					// $summary = preg_replace("/&nbsp;/",' ',$item->product_description);
					// $text = preg_replace("/&ndash;/",'-',$summary);
					if($item->document_type =='1'){
						$document_type="حکم";
					}elseif($item->document_type =='2'){
						$document_type="مکتوب";
					}elseif($item->document_type =='3'){
						$document_type="پشنهاد";
					}elseif($item->document_type =='4'){
						$document_type="فرمان";
					}elseif($item->document_type =='5'){
						$document_type="استعلام";
					}elseif($item->document_type =='6'){
						$document_type="ف س ۹";
					}elseif($item->document_type =='7'){
						$document_type="مکتوب هدایتی";
					}elseif($item->document_type =='8'){
						$document_type="پیشنهاد هدایتی";
					}elseif($item->document_type =='9'){
						$document_type="ف س ۹ هدایتی";
					}elseif($item->document_type =='10'){
						$document_type="تعيينات";
					}elseif($item->document_type =='11'){
						$document_type="مکتوب وارده";
					}elseif($item->document_type =='12'){
						$document_type="استعلام هدایتی";
					}elseif($item->document_type =='13'){
						$document_type="کاپی سوم";
					}elseif($item->document_type =='14'){
						$document_type="ورقه درخواستی";
					}elseif($item->document_type =='15'){
						$document_type="مصوبات";
					}
					//set value of sadira and warida
					if($item->type_id =='1'){
						$type_name='صادره';
					}elseif($item->type_id =='2'){
						$type_name='وارده';

					}elseif($item->type_id ==''){
						$type_name='';
					}

					//set value of ajra na ajra and hafzya
					if($item->result =='1'){
						$result='اجراه';
					}elseif($item->result =='2'){
						$result='نااجراه';
					}elseif($item->result =='3'){
						$result='حفظیه';
					}elseif($item->result ==''){
						$result='';
					}

					$sheet->setHeight($row, 20);
					$sheet->setCellValue('K'.$row.'',$row-2);
					$sheet->setCellValue('J'.$row.'',$item->number_documents);
					$sheet->setCellValue('I'.$row.'',$document_type);
					$sheet->setCellValue('H'.$row.'',checkEmptyDate($item->date));
					$sheet->setCellValue('G'.$row.'',$type_name);
					$sheet->setCellValue('F'.$row.'',$item->dept_name);
					$sheet->setCellValue('E'.$row.'',$item->description);
					$sheet->setCellValue('D'.$row.'',$item->related_to);
					$sheet->setCellValue('C'.$row.'',$item->receiver);
					$sheet->setCellValue('B'.$row.'',$result);
					$sheet->setCellValue('A'.$row.'',checkEmptyDate($item->result_date));



					//$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));

					$row++;
				}

				$sheet->setBorder('A3:P'.($row-1).'', 'thin');

				});

			})->export('xlsx');




								}

		}

	}



?>
