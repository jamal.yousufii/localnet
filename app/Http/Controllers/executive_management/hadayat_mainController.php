<?php 
namespace App\Http\Controllers\executive_management;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\executive_management\department;
use App\models\executive_management\document_model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Cookie;
use Validator;
use Response;
use File;
use Str;

class hadayat_mainController extends Controller {


	public function loadpage_insert_hadayat(){
		$department=department::get_department();

		return view::make('executive_management.insert_hadayat',['department'=>$department]);
	}

	public function insert_hadayat(){

		  $validates = Validator::make(Input::all(), array(
			"document_type"						=> "required",
			"number"				        	=> "required",
			"department_id"						=> "required"

			));

		if($validates->fails())
		{	
			return Redirect::route('insertForm_hadayat_executive')->withErrors($validates)->withInput();
		}
		else
		{
	    
		    //check the date type if it's shamsi or miladi.
			$date = toGregorian(gregorian_format(Input::get('date')));
			$result_date = toGregorian(gregorian_format(Input::get('result_date')));

		    // get the form data.
		    $data = array(
		    		"number_documents"	    => Input::get('number'),
		    		"date"					=> $date,
		    		"type_id"				=> Input::get('type_doc'),
		    		"department_id"			=> Input::get('department_id'),
		    		"description"			=> Input::get('description'),
		    		"document_type"			=> Input::get('document_type'),
		    		"related_to"			=> Input::get('related_to'),
		    		"receiver"		     	=> Input::get('receiver'),
		    		"result"		     	=> Input::get('result'),
		    		"result_date"		    => $result_date,
		    	
		    	);
			$insertedRecordID=\DB::connection('executive_management')->table("executive_documents")->insert($data);
		    if($insertedRecordID){
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 1;
				$file_data = array();

				if(Input::hasFile('files'))
				{				
					foreach($files as $file) 
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    // path is root/uploads
						    $destinationPath = 'uploads_doc/';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);
						    			  
						    $lastid = DB::connection('executive_management')->table('executive_documents')->orderBy('id', 'desc')->pluck('id');
		  
		   	 				  
					    	$lastFileId = $lastid;
							if($auto == 1)
						    $filename = "Attachment_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Attachment_".$lastFileId."_".$auto.".".$extension;
							
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success) 
						    {
						   $lastid = DB::connection('executive_management')->table('executive_documents')->orderBy('id', 'desc')->pluck('id');
							    $data = array(
						   					'file_name'				=> $filename,
						   					'ex_doc_id'				=> $lastid,
						   				);
						    	//call the model function to insert the data into upload table.
						    $insertfile=\DB::connection('executive_management')->table("uploads")->insert($data);
							} 
							else 
							{
							    // redirect back with errors.
					    		return Redirect::route('insertForm_hadayat_executive')->withErrors($validator);
							}
						}$auto ++;
						
						
					}
				}
		    	// get the the log data and insert it into the log table.
				
		    	return Redirect::route('insertForm_hadayat_executive')->with("success","Information successfully Added.");
	        }
	        else
	        {
	            return Redirect::route('insertForm_hadayat_executive')->with("fail","An error occured please try again or contact system developer.");
	        }}
	}


		public function delete_hadayat($id){

					$row=document_model::find($id);
					$row->status='1';

					$row->save();

						if($row)
				        {
				            return \Redirect::route("recordsList_executive")->with("success","You have successfuly deleted the record <span style='color:red;font_weight:bold;'></span>");
				        }
				        else
				        {
				            return \Redirect::route("recordsList_executive")->with("fail","An error occured please try again.");
				        }
						
						}

		public function select_row_doc_hadayat($id){

				$data=document_model::find($id);

				$department=department::get_department();

				$files=\DB::connection('executive_management')->table('uploads')->select('uploads.*','uploads.id as file_id')->where('ex_doc_id',$id)->get();
			
					return view::make('executive_management.update_hedayat_document',array('data'=>$data,'department'=>$department,'files'=>$files));

		}

		public function update_hadayat($id){



			$document= new document_model;
			$document=document_model::find($id);
			$validates = Validator::make(Input::all(), array(
			"document_type"						=> "required",
			"number"				        	=> "required",
			"department_id"						=> "required"
			));

		if($validates->fails())
		{	
			return Redirect::route('select_data_doc_hadayat')->withErrors($validates)->withInput();
		}
		else
		{
	    
		    //check the date type if it's shamsi or miladi.
			$date = toGregorian(gregorian_format(Input::get('date')));
			$result_date = toGregorian(gregorian_format(Input::get('result_date')));


		    // get the form data.
		    $data = array(
		    		"number_documents"	    => Input::get('number'),
		    		"date"					=> $date,
		    		"type_id"				=> Input::get('type_doc'),
		    		"department_id"			=> Input::get('department_id'),
		    		"description"			=> Input::get('description'),
		    		"document_type"			=> Input::get('document_type'),
		    		"related_to"			=> Input::get('related_to'),
		    		"receiver"		     	=> Input::get('receiver'),
		    		"result"		     	=> Input::get('result'),
		    		"result_date"		    => $result_date,
		    	);
			$insertedRecordID=\DB::connection('executive_management')->table("executive_documents")->where('id',$id)->update($data);
		    if($insertedRecordID){
		    	// getting all of the post data
				$files = Input::file('files');
				//print_r($files);exit;
				$errors = "";
				$auto = 50;
				$file_data = array();

				if(Input::hasFile('files'))
				{				
					foreach($files as $file) 
					{
					    // validating each file.
					    $validator = Validator::make(
					  		[
					            'file' => $file
					        ],
					        [
					            'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
					        ]
					  	);
					  	if($validator->passes())
					  	{
						    // path is root/uploads
						    $destinationPath = 'uploads_doc/';
						    $original_filename = $file->getClientOriginalName();
						    $temp = explode(".", $original_filename);
					    	$extension = end($temp);
						    			  
						    $lastid = DB::connection('executive_management')->table('executive_documents')->orderBy('id', 'desc')->pluck('id');
						    $file_id = DB::connection('executive_management')->table('uploads')->orderBy('id', 'desc')->pluck('id');
						   $file_id++;
		   	 				  
					    	$lastFileId = $lastid;
							if($auto == 50)
						    $filename = "Attachment_".$lastFileId."_".$file_id.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
							else
							$filename = "Attachment_".$lastFileId."_".$file_id.".".$extension;
							
						    $upload_success = $file->move($destinationPath, $filename);

						    if($upload_success) 
						    {
						   $lastid = $id;
							    $data = array(
						   					'file_name'				=> $filename,
						   					'ex_doc_id'				=> $lastid,
						   				);
						    	//call the model function to insert the data into upload table.
						    $insertfile=\DB::connection('executive_management')->table("uploads")->insert($data);
							} 
							else 
							{
							    // redirect back with errors.
					    		return Redirect::route('select_data_doc_hadayat')->withErrors($validator);
							}
						}$auto ++;
						
						
					}
				}
		    	// get the the log data and insert it into the log table.
				
		    	return Redirect::route('recordsList_executive')->with("success","Information successfully updeted.");
	        }
	        else
	        {
	            return Redirect::route('select_data_doc_hadayat')->with("fail","An error occured please try again or contact system developer.");
	        }}
	    }
	}


		






