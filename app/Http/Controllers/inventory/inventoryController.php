<?php 

namespace App\Http\Controllers\inventory;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\models\inventory\inventoryModel;
use Illuminate\Support\Collection;
use App\library\jdatetime;
use View;
use DB;
use Validator;
use Input;
use Redirect;
use Response;
use Datatable;
use Session;
use DateTime;
use DateInterval;
use File;
use Illuminate\Support\Str;
use URL;
use Carbon\Carbon;

class inventoryController extends Controller
{
		
//================================================= Autocomplete Functions =======================================================
	//get the data list based on table name and field name for autocomplete fields.
	public function getAutoCompleteListData($table="", $field="")
	{
        $q = strtolower(Input::get('term'));
		
		if (!$q) return;
		$options = DB::connection("inventory")->table($table)->select($field)->groupBy($field)->get();
		if (!$options) return;
		$json_array = array();
		foreach ($options as $key) 
		{
			if (strpos(strtolower($key->$field), $q) !== false) 
			{
				//echo "$key->company_name\n";
				array_push($json_array, $key->$field);
			}
		}
		echo json_encode($json_array);
	}

//================================================= Inventory Controller Functions ===================================
	
	//Load task/case form
	public function loadInventoryList()
	{
		$data['records'] = inventoryModel::getData("");
		//dd($data['records']);
		if(Input::get('ajax') == 1)
		{
			return View::make('inventory.inventory_list_ajax',$data);
		}
		else
		{
			//load view to show searchpa result
			return View::make('inventory.inventoryList', $data);
		}
		
	}
	//load the searched inventories based on product_type.
	public function loadSearchedInventories()
	{
		//dd($_POST);
		if(Input::get('product_type') != "")
		{
			//get the value of the product_type dropdown if it's been chosen.
			$product_type = Input::get('product_type');
			$data['records'] = inventoryModel::getData($product_type);
			$data['stock_in_total'] = inventoryModel::getStocksTotal($product_type, 'stock_in');
			$data['stock_out_total'] = inventoryModel::getStocksTotal($product_type, 'stock_out');
			//dd($data['stock_out_total']);
		}
		else{$data['records'] = "";}
		if(Input::get('ajax') == 1)
		{
			return View::make('inventory.searchedInventoryList_ajax',$data);
		}
		else
		{
			return View::make('inventory.searchedInventoryList', $data);
		}
		
	}
	//get the stock in last value based on product type.
	public function getStockInByProductType()
	{
		$product_type = Input::get('product_type');
		$stock_in = inventoryModel::getStockInByProductType($product_type);
		echo $stock_in;
	}
	//Load Inventory form
	public function loadInventoryForm()
	{
		return View::make('inventory.inventory_form');
	}
	// Add Inventory form data.
	public function addInventoryFormData(Request $request)
	{
		//validate the input fields
	    $this->validate($request,[
	        "product_name" 					=> "required",
	        "person_name"					=> "required"
	        ]
	    );
	    //print_r($_POST);exit;
	    if(Input::get('product_type') == 0)
	    {
	    	//check if product_type doesn't exist in it's table then insert it and get it's id.
	    	$product_type = checkProductType(Input::get('other_product'));
	    	//echo $product_type;exit;
	    }else{
	    	$product_type = Input::get('product_type');
	    }
	    //check the date type if it's shamsi or miladi.
		if(isMiladiDate()){
			$date = Input::get('date');
		}else{
			$date = toGregorian(ymd_format(Input::get('date')));
		}
	    //$balance = Input::get('stock_in') - Input::get('stock_out');
	    // get the form data.
	    $data = array(
	    		"product_name"			=> Input::get('product_name'),
	    		"product_type"			=> $product_type,
	    		"model"					=> Input::get('model'),
	    		"status"				=> Input::get('status'),
	    		"stock_in"				=> Input::get('stock_in'),
	    		"stock_out"				=> Input::get('stock_out'),
	    		"person_name"			=> Input::get('person_name'),
	    		"date"					=> $date,
	    		"remarks"				=> Input::get('remarks'),
	    		"user_id"				=> Auth::user()->id,
	    		"created_at"			=> date('Y-m-d H:i:s')
	    	);
	    $object_id = inventoryModel::addInventory($data);
	    if($object_id){
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'inventory',
				'record_id'=>$object_id,
				'user' => Auth::user()->username,
				'action' => "Added",
				'created_at' => date('Y-m-d H:i:s')
				);
			inventoryModel::addLog($Log);
	    	return Redirect::route("inventoryList")->with("success","Information successfully Added.");
        }
        else
        {
            return Redirect::route("inventoryList")->with("fail","An error occured please try again or contact system developer.");
        }
	}
	//loading the inventory edit page.
	public function loadInventoryEditForm($id=0)
	{
		$data['inventory'] = inventoryModel::getSpecificInventoryDetails($id);
		//dd($data['inventory']);
		return View::make('inventory.edit_form', $data);	
	}
	// updating the changes in case table.
	public function postEditInventoryForm(Request $request,$id)
	{
		//validate the input fields
	    $this->validate($request,[
	        "product_name" 					=> "required",
	        "person_name"					=> "required"
	        ]
	    );
	    //print_r($_POST);exit;
	    if(Input::get('product_type') == 0)
	    {
	    	//check if product_type doesn't exist in it's table then insert it and get it's id.
	    	$product_type = checkProductType(Input::get('other_product'));
	    	//echo $product_type;exit;
	    }else{
	    	$product_type = Input::get('product_type');
	    }
	    //check the date type if it's shamsi or miladi.
		if(isMiladiDate()){
			$date = Input::get('date');
		}else{
			$date = toGregorian(ymd_format(Input::get('date')));
		}
	    // get the form data.
	    $data = array(
	    		"product_name"			=> Input::get('product_name'),
	    		"product_type"			=> $product_type,
	    		"model"					=> Input::get('model'),
	    		"status"				=> Input::get('status'),
	    		"stock_in"				=> Input::get('stock_in'),
	    		"stock_out"				=> Input::get('stock_out'),
	    		"person_name"			=> Input::get('person_name'),
	    		"date"					=> $date,
	    		"remarks"				=> Input::get('remarks'),
	    		"user_id"				=> Auth::user()->id,
	    		"updated_at"			=> date('Y-m-d H:i:s')
	    	);
	    $updated = inventoryModel::updateInventory($id,$data);
	    if($updated){
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'inventory',
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Edited",
				'created_at' => date('Y-m-d H:i:s')
				);
			inventoryModel::addLog($Log);
	    	return Redirect::route("inventoryList")->with("success","Information successfully Edited.");
        }
        else
        {
            return Redirect::route("inventoryList")->with("fail","An error occured please try again or contact system developer.");
        }
		
	}
	//change the status of the inventory based on it's id to 1 if it's deleted.
	public function deleteInventory()
	{
		$id = Input::get('record_id');
		$deleted = inventoryModel::getDeleteInventory($id);
		//dd($deleted);
		if($deleted)
	    {
	    	// get the the log data and insert it into the log table.
			$Log = array(
				'action_table' => 'inventory',
				'record_id'=>$id,
				'user' => Auth::user()->username,
				'action' => "Deleted",
				'created_at'=>date('Y-m-d H:i:s')
				);
			inventoryModel::addLog($Log);
        }
	}

}

?>