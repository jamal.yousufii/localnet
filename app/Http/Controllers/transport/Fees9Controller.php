<?php 

namespace App\Http\Controllers\transport;

use App\Http\Controllers\Controller;
use App\models\transport\Fees9;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class Fees9Controller extends Controller
{
	
	//database connection
	public static $myDb = "transport";

	//Load vehicle list view
	public function getList()
	{

		return view("transport.fees9.list");
	}
	
	//get datatable json data
	public function getData()
	{

		//get data from model
		$apps = Fees9::getData();
		$collection = new Collection($apps);
		return \Datatable::collection($collection)
					->showColumns(
									'id',
									'number',
									'date',
									'requested_dep',
									'agency_name',
									'type'
									)
					->addColumn('operation',function($option)
						{
							$options = '';
							
							$options .= '<a href="'.route('getEditFees9',array($option->id,$option->category)).'"><i class="icon fa-edit fa-lg" aria-hidden="true"></i></a> &nbsp;';
						
							$options .= '|&nbsp;<a href="'.route('getDeleteFees9',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="icon fa-trash fa-lg" aria-hidden="true"></i></a> &nbsp;';
							
							$options .= '|&nbsp;<a href="'.route('getFees9PrintPreview',array($option->id,$option->category)).'" target="_blank"><i class="icon fa-print fa-lg" aria-hidden="true"></i></a>';
						
							return $options;
						}
					)
					
					->make();
	}
	
	/*
	getting form for inserting vehicle
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreate($category=1)
	{
		$data['type'] = $category;
		$data['items'] = DB::connection("transport")
							->table("filters_and_oils as t1")
							->select("t1.item_description","t1.amount","t1.token_amount","t1.id",DB::raw("sum(fi.amount) AS total_token"))
							->leftjoin("fees9_items AS fi","fi.item_id","=","t1.id")
							->where("category",$category)
							->groupBy("t1.id")
							->get();
		$data['drivers'] = DB::connection("transport")->table("drivers")->where("is_motamid",0)->get();
		$data['vehicles'] = DB::connection("transport")->table("vehicles")->get();
		//load view for inserting vehilce
		return View::make("transport.fees9.insert",$data);
		
	}
	
	public function insert()
	{
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "number" 	=> "required",
	        "date" 		=> "required"
	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateFees9")->withErrors($validates)->withInput();
	    }
	    else
	    {
	        //echo "<pre>";print_r($_POST);exit;
	        //create an object
	        $object = new Fees9();
	        $object->number = Input::get("number");
	        $object->requested_dep = Input::get("requested_dep");
	        $object->agency_name = Input::get("agency_name");
	        $object->header_description = Input::get("header_description");
	        $object->footer_description = Input::get("footer_description");
	        $object->type = Input::get("type");
	        $object->driver = Input::get("driver");
	        $object->vehicle_plate = Input::get("vehicle_plate");
	        //check date setting
			if(isMiladiDate())
			{
				$object->date 			= toJalali(Input::get("date"));
				
			}
			else
			{
				$object->date 			= ymd_format(Input::get("date"));
				
			}
	        
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	        	$fees9_id = $object->id;
	        	//insert fees9 items
	        	$itemRecords = array();
	        	
	        	$items = Input::get("items");
	        	$amounts = Input::get("amount");
	        	$units = Input::get("unit");
	        	$total_amount = Input::get("total_amount");
	        	$item_status = Input::get("item_status");
	        	
	        	for($i=0;$i<count($items); $i++){
	        		$record = array(
	        			
	        						"fees9_id" 		=> $fees9_id,
	        						"item_id" 		=> $items[$i],
	        						"amount" 		=> $amounts[$i],
	        						"unit" 			=> $units[$i]
	        						
	        						);
	        		if($item_status[$i]==1){
	        			$record['from_stack'] = 1;
	        			$record['from_market'] = 0;
	        			$record['contracted'] = 0;
	        		}else if($item_status[$i] == 2){
	        			$record['from_market'] = 1;
	        			$record['from_stack'] = 0;
	        			$record['contracted'] = 0;
	        		}else if($item_status[$i] == 3){
	        			$record['from_market'] = 0;
	        			$record['from_stack'] = 0;
	        			$record['contracted'] = 1;
	        		}
	        		
	        		//push recod to item records
	        		$itemRecords[] = $record;
	        		$t_amount = $total_amount[$i]+$amounts[$i];
	        		
	        		DB::connection("transport")->table("filters_and_oils")->where("id",$items[$i])->update(array("token_amount"=>$t_amount));
	        	}
	        	
	        	if(!empty($itemRecords)){
	        		
	        		Fees9::insertItems($itemRecords);
	        	}
	        	
	        	
	            return \Redirect::route("getFees9List")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getFees9List")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0,$category=1)
    {
    	
    	$data['items'] = DB::connection("transport")
							->table("filters_and_oils as t1")
							->select("t1.item_description","t1.amount","t1.token_amount","t1.id",DB::raw("sum(fi.amount) AS total_token"))
							->leftjoin("fees9_items AS fi","fi.item_id","=","t1.id")
							->where("category",$category)
							->groupBy("t1.id")
							->get();
    	//get data
    	$data['row'] = Fees9::getDetails($id);
    	$data['type'] = $category;
    	$data['drivers'] = DB::connection("transport")->table("drivers")->get();
		$data['vehicles'] = DB::connection("transport")->table("vehicles")->get();
    	//get fees9 items
    	$data['selected_items'] = DB::connection("transport")->table("fees9_items")->where("fees9_id",$id)->get();
    	
    	return View::make("transport.fees9.edit",$data);
    	
    }
    
    public function update($id=0)
    {
    	
    	//create an object
        $object = Fees9::find($id);
        $object->number = Input::get("number");
        $object->requested_dep = Input::get("requested_dep");
        $object->agency_name = Input::get("agency_name");
        $object->header_description = Input::get("header_description");
        $object->footer_description = Input::get("footer_description");
        $object->type = Input::get("type");
        $object->driver = Input::get("driver");
	    $object->vehicle_plate = Input::get("vehicle_plate");
        //check date setting
		if(isMiladiDate())
		{
			$object->date 			= toJalali(Input::get("date"));
			
		}
		else
		{
			$object->date 			= ymd_format(Input::get("date"));
			
		}
	        
        $object->updated_at = date("Y-m-d H:i:s");
	        
        if($object->save())
        {
        	$fees9_id = $id;
        	//insert fees9 items
        	$itemRecords = array();
        	
        	$items = Input::get("items");
        	$amounts = Input::get("amount");
        	$units = Input::get("unit");
        	$item_status = Input::get("item_status");
        	
        	for($i=0;$i<count($items); $i++){
        		$record = array(
        			
        						"fees9_id" => $fees9_id,
        						"item_id" => $items[$i],
        						"amount" => $amounts[$i],
        						"unit" => $units[$i]
        						
        						);
        		if($item_status[$i]==1){
        			$record['from_stack'] = 1;
        			$record['from_market'] = 0;
        			$record['contracted'] = 0;
        		}else if($item_status[$i] == 2){
        			$record['from_market'] = 1;
        			$record['from_stack'] = 0;
        			$record['contracted'] = 0;
        		}else if($item_status[$i] == 3){
        			$record['from_market'] = 0;
        			$record['from_stack'] = 0;
        			$record['contracted'] = 1;
        		}
        		//push recod to item records
        		$itemRecords[] = $record;
        		
        		DB::connection("transport")->table("filters_and_oils")->where("id",$items[$i])->update(array("token_amount"=>$amounts[$i]));
        	}
        	
        	if(!empty($itemRecords))
        	{
        		//remove the old items
        		DB::connection("transport")->table("fees9_items")->where("fees9_id",$fees9_id)->delete();
        		
        		Fees9::insertItems($itemRecords);
        	}
        	
            return \Redirect::route("getFees9List")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getFees9List")->with("fail","An error occured plase try again.");
        }
    	
    }
    public function getPrintPreview($id=0,$category=1)
    {
    	
    	$data['items'] = DB::connection("transport")->table("filters_and_oils")->where("category",$category)->get();
    	//get data
    	$data['row'] = Fees9::getDetails($id);
    	//get fees9 items
    	$data['selected_items'] = DB::connection("transport")->table("fees9_items")->where("fees9_id",$id)->get();
    	
    	return View::make("transport.fees9.print_preview",$data);
    	
    }
    
    public function getDelete($id=0)
    {
    	
    	//delete record
    	$deleted = Fees9::getDelete($id);
    	
    	if($deleted)
        {
            return \Redirect::route("getFees9List")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getFees9List")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function importExcel()
    {
    	
    	if(Input::hasFile('excel_file')){

			$path = Input::file('excel_file')->getRealPath();
			$year = Input::get("year");
			$category = Input::get("category");

			$data = Excel::load($path, function($reader) {

			})->get();

			if(!empty($data) && $data->count()){

				foreach ($data as $key => $value) {
					
					$insert[] = array(
										'item_description' => $value->item_description, 
										'properties' => $value->properties,
										'amount' => $value->amount,
										'token_amount' => 0,
										'unit' => $this->getUnitId($value->unit),
										'description' => $value->description,
										'year' => $year,
										'unit_price' => 0,
										'category' => $category
									);

				}
				
				if(!empty($insert)){

					DB::connection("transport")->table('filters_and_oils')->insert($insert);

				}

			}

		}

		return back();
    	
    	
        return \Redirect::route("getFilterOilList")->with("success","Excel file imported successfully.");
        
    	
    }
    
    public function getUnitId($value="")
    {
    	return DB::connection("transport")->table('mesure_units')->where('name', 'LIKE', "%$value%")->pluck("id");
    }
	
	
}

?>