<?php 

namespace App\Http\Controllers\transport;

use App\Http\Controllers\Controller;
use App\models\transport\DeputyVehicle;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class DeputyVehicleController extends Controller
{
	
	//database connection
	public static $myDb = "transport";

	//Load vehicle list view
	public function getList()
	{
		
		return view("transport.deputy.list");
	}
	
	//get datatable json data
	public function getData()
	{
		$data["vehicles"] = DeputyVehicle::getData();

		return view("transport.deputy.list_ajax",$data);
		
	}
	
	/*
	getting form for inserting vehicle
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreate()
	{
		
		//load view for inserting vehilce
		return View::make("transport.deputy.insert");
		
	}
	
	public function insertVehicle()
	{
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "vehicle_category" => "required",
	        "vehicle_type" => "required",
	        "plate" => "required"
	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateDeputyVehicle")->withErrors($validates)->withInput();
	    }
	    else
	    {
	        
	        //create an object from department class
	        $object = new DeputyVehicle();
	        $object->vehicle_category = Input::get("vehicle_category");
	        $object->vehicle_type = Input::get("vehicle_type");
	        $object->model = Input::get("model");
	        $object->plate = Input::get("plate");
	        $object->mileage = Input::get("mileage");
	        $object->color = Input::get("color");
	        $object->status = Input::get("status");
	        $object->description = Input::get("description");
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	        	$id = $object->id;
	        	$this->uploadFile(Input::get("plate"),$id);
	        	
	            return \Redirect::route("getDeputyVehicles")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getDeputyVehicles")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0)
    {
    	
    	//get data
    	$data['row'] = DeputyVehicle::getDetails($id);
    	
    	return View::make("transport.deputy.edit",$data);
    	
    }
    
    public function update($id=0)
    {
    	//create an object from department class
        $object = DeputyVehicle::find($id);
        $object->vehicle_category = Input::get("vehicle_category");
        $object->vehicle_type = Input::get("vehicle_type");
        $object->model = Input::get("model");
        $object->plate = Input::get("plate");
        $object->mileage = Input::get("mileage");
        $object->color = Input::get("color");
        $object->status = Input::get("status");
        $object->description = Input::get("description");

        if($object->save())
        {
        	$this->uploadFile(Input::get("plate"),$id);
            return \Redirect::route("getDeputyVehicles")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getDeputyVehicles")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function getDelete($id=0)
    {
    	
    	//delete attachment first
    	$this->deleteFile($id);
    	//delete record
    	$deleted = DeputyVehicle::getDelete($id);
    	
    	if($deleted)
        {
            return \Redirect::route("getDeputyVehicles")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getDeputyVehicles")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function uploadFile($plate=0,$record_id=0)
	{
		
		// getting all of the post data
		$files = Input::file('files');
		$errors = "";
		$file_data = array();
		
		foreach($files as $file) 
		{
			
			if(Input::hasFile('files'))
			{
			
			  // validating each file.
			  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			  $validator = \Validator::make(

			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'file' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png'
			        ]
			  	);

			  if($validator->passes())
			  {
			  	
			    // path is root/uploads
			    $destinationPath = 'documents/transport/deputy';
			    $filename = $file->getClientOriginalName();

			    $temp = explode(".", $filename);
			    $extension = end($temp);
			    
			    
			    $filename = 'car_photo_'.$plate.'_'.date('Y-m-d H:i:s').'.'.$extension;
				//first delete the old custom profile picture
				$this->deleteFile($record_id);
				
			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
				   
				   $data = array(
				   					'photo'=>$filename
				   				);
				   
				   if(count($data)>0)
					{
						DB::connection("transport")->table("deputy_vehicles")->where("id",$record_id)->update($data);
					}
				   
					return true;
				} 
				else 
				{
				   return "<div class='alert alert-danger'>Error with file</div>";
				}
					
				
			  } 
			  else 
			  {
			    // redirect back with errors.
			    return "<div class='alert alert-danger'>Validation Error</div>";
			  }
			}

		}
			
	}
	//remove file from folder
	public function deleteFile($id=0)
	{
		
			$photo = DB::connection("transport")->table("deputy_vehicles")->where("id",$id)->pluck("photo");
			
			$file= public_path(). "/documents/transport/deputy/".$photo;

			if(File::delete($file))
			{
				
				return true;
			}
			else
			{
				return false;
			}
		
	}
	
	
}

?>