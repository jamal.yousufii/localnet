<?php 

namespace App\Http\Controllers\transport;

use App\Http\Controllers\Controller;
use App\models\transport\Driver;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class DriverController extends Controller
{
	
	//database connection
	public static $myDb = "transport";

	//Load vehicle list view
	public function getList()
	{

		return view("transport.driver_list");
	}
	
	//get datatable json data
	public function getData()
	{

		//get data from model
		$apps = Driver::getData();
		$collection = new Collection($apps);
		return \Datatable::collection($collection)
					->showColumns(
									'id',
									'card_no',
									'first_name',
									'last_name',
									'father_name',
									'phone'								)
					->addColumn('operation',function($option)
						{
							$options = '';
							
							$options .= '<a href="'.route('getEditDriver',$option->id).'">Edit</a> &nbsp;';
						
							$options .= '|&nbsp;<a href="'.route('getDeleteDriver',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');">Delete</a>';
						
							return $options;
						}
					)
					
					->make();
	}
	
	/*
	getting form for inserting vehicle
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreateDriver()
	{
		
		//load view for inserting vehilce
		return View::make("transport.insert_driver");
		
	}
	
	public function insertDriver()
	{
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "card_no" => "required",
	        "first_name" => "required"
	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateDriver")->withErrors($validates)->withInput();
	    }
	    else
	    {
	        
	        //create an object
	        $object = new Driver();
	        $object->card_no = Input::get("card_no");
	        $object->first_name = Input::get("first_name");
	        $object->last_name = Input::get("last_name");
	        $object->father_name = Input::get("father_name");
	        $object->phone = Input::get("phone");
	        if(Input::get("is_motamid")){
	        	$object->is_motamid = Input::get("is_motamid");
	        }
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	            return \Redirect::route("getDriverList")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getDriverList")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0)
    {
    	
    	//get data
    	$data['row'] = Driver::getDetails($id);
    	
    	return View::make("transport.edit_driver",$data);
    	
    }
    
    public function update($id=0)
    {
    	//create an object
	        $object = Driver::find($id);
	        $object->card_no = Input::get("card_no");
	        $object->first_name = Input::get("first_name");
	        $object->last_name = Input::get("last_name");
	        $object->father_name = Input::get("father_name");
	        $object->phone = Input::get("phone");
	        if(Input::get("is_motamid")){
	        	$object->is_motamid = Input::get("is_motamid");
	        }else{
	        	$object->is_motamid = 0;
	        }
	        
        if($object->save())
        {
            return \Redirect::route("getDriverList")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getDriverList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function getDelete($id=0)
    {
    	
    	//delete record
    	$deleted = Driver::getDelete($id);
    	
    	if($deleted)
        {
            return \Redirect::route("getDriverList")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getDriverList")->with("fail","An error occured plase try again.");
        }
    	
    }
	
	
}

?>