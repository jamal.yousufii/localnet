<?php 

namespace App\Http\Controllers\transport;

use App\Http\Controllers\Controller;
use App\models\transport\FuelContracted;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class FuelContractedController extends Controller
{
	
	//database connection
	public static $myDb = "transport";

	//Load vehicle list view
	public function getList()
	{

		return view("transport.fuel_contracted.fuel_contracted_list");
	}
	
	//get datatable json data
	public function getData()
	{

		//get data from model
		$apps = FuelContracted::getData();
		$collection = new Collection($apps);
		return \Datatable::collection($collection)
					->showColumns(
									'id',
									'company',
									'gravity',
									'date',
									'serial_no',
									'vehicle_no',
									'driver_name'
									)
					->addColumn('operation',function($option)
						{
							$options = '';
							
							$options .= '<a href="'.route('getEditFuelContract',$option->id).'">Edit</a> &nbsp;';
						
							$options .= '|&nbsp;<a href="'.route('getDeleteFuelContract',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');">Delete</a>';
						
							return $options;
						}
					)
					
					->make();
	}
	
	/*
	getting form for inserting vehicle
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreateContract()
	{
		
		//load view for inserting vehilce
		return View::make("transport.fuel_contracted.insert_contract");
		
	}
	
	public function insertContract()
	{
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "company" 	=> "required",
	        "date" 		=> "required",
	        "fuel_type" 		=> "required",
	        "serial_no" => "required"
	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateFuelContract")->withErrors($validates)->withInput();
	    }
	    else
	    {
	        
	        //create an object
	        $object = new FuelContracted();
	        $object->company = Input::get("company");
	        $object->gravity = Input::get("gravity");
	        //check date setting
			if(isMiladiDate())
			{
				$object->date 			= toJalali(Input::get("date"));
				
			}
			else
			{
				$object->date 			= ymd_format(Input::get("date"));
				
			}
	        
	        $object->time = Input::get("time");
	        $object->serial_no = Input::get("serial_no");
	        $object->vehicle_no = Input::get("vehicle_no");
	        $object->driver_name = Input::get("driver_name");
	        $object->party_name = Input::get("party_name");
	        $object->description = Input::get("description");
	        $object->first_weight = Input::get("first_weight");
	        $object->second_weight = Input::get("second_weight");
	        $object->net_weight = Input::get("net_weight");
	        $object->fuel_type = Input::get("fuel_type");
	        
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	            return \Redirect::route("getFuelContractedList")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getFuelContractedList")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0)
    {
    	
    	//get data
    	$data['row'] = FuelContracted::getDetails($id);
    	
    	return View::make("transport.fuel_contracted.edit_contract",$data);
    	
    }
    
    public function update($id=0)
    {
    	
    	//create an object
        $object = FuelContracted::find($id);
        $object->company = Input::get("company");
        $object->gravity = Input::get("gravity");
        //check date setting
		if(isMiladiDate())
		{
			$object->date 			= toJalali(Input::get("date"));
			
		}
		else
		{
			$object->date 			= ymd_format(Input::get("date"));
			
		}
        
        $object->time = Input::get("time");
        $object->serial_no = Input::get("serial_no");
        $object->vehicle_no = Input::get("vehicle_no");
        $object->driver_name = Input::get("driver_name");
        $object->party_name = Input::get("party_name");
        $object->description = Input::get("description");
        $object->first_weight = Input::get("first_weight");
        $object->second_weight = Input::get("second_weight");
        $object->net_weight = Input::get("net_weight");
        $object->fuel_type = Input::get("fuel_type");
        $object->updated_at = date("Y-m-d H:i:s");
	        
        if($object->save())
        {
            return \Redirect::route("getFuelContractedList")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getFuelContractedList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function getDelete($id=0)
    {
    	
    	//delete record
    	$deleted = FuelContracted::getDelete($id);
    	
    	if($deleted)
        {
            return \Redirect::route("getFuelContractedList")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getFuelContractedList")->with("fail","An error occured plase try again.");
        }
    	
    }
	
	
}

?>