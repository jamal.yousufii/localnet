<?php 

namespace App\Http\Controllers\transport;

use App\Http\Controllers\Controller;
use App\models\transport\Repairing;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class RepairingController extends Controller
{
	
	//database connection
	public static $myDb = "transport";

	//Load vehicle list view
	public function getList()
	{

		return view("transport.vehicle_repairing.list");
	}
	
	//get datatable json data
	public function getData()
	{

		//get data from model
		$apps = Repairing::getData();
		$collection = new Collection($apps);
		return \Datatable::collection($collection)
					->showColumns(
									'id',
									'driver',
									'register_no',
									'palet_number',
									'date',
									'source'
									)
					->addColumn('operation',function($option)
						{
							$options = '';
							
							$options .= '<a href="'.route('getEditVehicleRepairing',$option->id).'">Edit</a> &nbsp;';
						
							$options .= '|&nbsp;<a href="'.route('getDeleteVehicleRepairing',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');">Delete</a>';
						
							return $options;
						}
					)
					
					->make();
	}
	
	/*
	getting form for inserting vehicle
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreate()
	{
		
		//get all drivers
		$data['drivers'] = DB::connection("transport")->table("drivers")->get();
		//load view for inserting vehilce
		return View::make("transport.vehicle_repairing.insert",$data);
		
	}
	
	public function insert()
	{
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "driver" 	=> "required",
	        "palet_number" 			=> "required",
	        "source" 			=> "required",
	        "register_no" 			=> "required"
	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateVehicleRepairing")->withErrors($validates)->withInput();
	    }
	    else
	    {
	        
	        //create an object
	        $object = new Repairing();
	        $object->register_no = Input::get("register_no");
	        //check date setting
			if(isMiladiDate())
			{
				$object->date 			= toJalali(Input::get("date"));
				
			}
			else
			{
				$object->date 			= ymd_format(Input::get("date"));
				
			}
			$object->palet_number = Input::get("palet_number");
			$object->technical_problem_reason = Input::get("technical_problem_reason");
			$object->technical_team_description = Input::get("technical_team_description");
	        $object->source = Input::get("source");
	        $object->driver = Input::get("driver");
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	            return \Redirect::route("getVehicleRepairingList")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getVehicleRepairingList")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0)
    {
    	
    	//get data
    	$data['row'] = Repairing::getDetails($id);
    	//get all drivers
		$data['drivers'] = DB::connection("transport")->table("drivers")->get();
    	
    	return View::make("transport.vehicle_repairing.edit",$data);
    	
    }
    
    public function update($id=0)
    {
    	
    	//create an object
        $object = Repairing::find($id);
        $object->register_no = Input::get("register_no");
        //check date setting
		if(isMiladiDate())
		{
			$object->date 			= toJalali(Input::get("date"));
			
		}
		else
		{
			$object->date 			= ymd_format(Input::get("date"));
			
		}
		$object->palet_number = Input::get("palet_number");
		$object->technical_problem_reason = Input::get("technical_problem_reason");
		$object->technical_team_description = Input::get("technical_team_description");
        $object->source = Input::get("source");
        $object->driver = Input::get("driver");
	        
        $object->updated_at = date("Y-m-d H:i:s");
	        
        if($object->save())
        {
            return \Redirect::route("getVehicleRepairingList")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getVehicleRepairingList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function getDelete($id=0)
    {
    	
    	//delete record
    	$deleted = Repairing::getDelete($id);
    	
    	if($deleted)
        {
            return \Redirect::route("getVehicleRepairingList")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getVehicleRepairingList")->with("fail","An error occured plase try again.");
        }
    	
    }
	
	
}

?>