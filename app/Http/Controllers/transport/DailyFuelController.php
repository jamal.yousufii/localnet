<?php 

namespace App\Http\Controllers\transport;

use App\Http\Controllers\Controller;
use App\models\transport\DailyFuel;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class DailyFuelController extends Controller
{
	
	//database connection
	public static $myDb = "transport";

	//Load vehicle list view
	public function getList()
	{

		return view("transport.daily_fuel.list");
	}
	
	//get datatable json data
	public function getData()
	{

		//get data from model
		$apps = DailyFuel::getData();
		$collection = new Collection($apps);
		return \Datatable::collection($collection)
					->showColumns(
									'id',
									'driver',
									'vehicle_type',
									'palet_number',
									'department',
									'amount',
									'date'
									)
					->addColumn('operation',function($option)
						{
							$options = '';
							
							$options .= '<a href="'.route('getEditDailyFuel',$option->id).'">Edit</a> &nbsp;';
						
							$options .= '|&nbsp;<a href="'.route('getDeleteDailyFuel',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');">Delete</a>';
						
							return $options;
						}
					)
					
					->make();
	}
	
	/*
	getting form for inserting vehicle
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreate()
	{
		
		//get all drivers
		$data['drivers'] = DB::connection("transport")->table("drivers")->get();
		//load view for inserting vehilce
		return View::make("transport.daily_fuel.insert",$data);
		
	}
	
	public function insert()
	{
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "driver" 	=> "required",
	        "vehicle_type" 		=> "required",
	        "palet_number" 			=> "required|Numeric",
	        "transaction_number" 				=> "required",
	        "department" 				=> "required",
	        "fuel_type" 		=> "required",
	        "amount" 		=> "required|Numeric"
	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateDailyFuel")->withErrors($validates)->withInput();
	    }
	    else
	    {
	        
	        //create an object
	        $object = new DailyFuel();
	        $object->driver = Input::get("driver");
	        $object->vehicle_type = Input::get("vehicle_type");
	        $object->palet_number = Input::get("palet_number");
	        $object->transaction_number = Input::get("transaction_number");
	        $object->department = Input::get("department");
	        $object->amount = Input::get("amount");
	        $object->description = Input::get("description");
	        $object->fuel_type = Input::get("fuel_type");
	       
	        //check date setting
			if(isMiladiDate())
			{
				$object->date 			= toJalali(Input::get("date"));
				
			}
			else
			{
				$object->date 			= ymd_format(Input::get("date"));
				
			}
	        
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	            return \Redirect::route("getDailyFuelList")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getDailyFuelList")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0)
    {
    	
    	//get data
    	$data['row'] = DailyFuel::getDetails($id);
    	//get all drivers
		$data['drivers'] = DB::connection("transport")->table("drivers")->get();
    	
    	return View::make("transport.daily_fuel.edit",$data);
    	
    }
    
    public function update($id=0)
    {
    	
    	//create an object
        $object = DailyFuel::find($id);
        $object->driver = Input::get("driver");
        $object->vehicle_type = Input::get("vehicle_type");
        $object->palet_number = Input::get("palet_number");
        $object->transaction_number = Input::get("transaction_number");
        $object->department = Input::get("department");
        $object->amount = Input::get("amount");
        $object->description = Input::get("description");
       	$object->fuel_type = Input::get("fuel_type");

        //check date setting
		if(isMiladiDate())
		{
			$object->date 			= toJalali(Input::get("date"));
			
		}
		else
		{
			$object->date 			= ymd_format(Input::get("date"));
			
		}
	        
        $object->updated_at = date("Y-m-d H:i:s");
	        
        if($object->save())
        {
            return \Redirect::route("getDailyFuelList")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getDailyFuelList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function getDelete($id=0)
    {
    	
    	//delete record
    	$deleted = DailyFuel::getDelete($id);
    	
    	if($deleted)
        {
            return \Redirect::route("getDailyFuelList")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getDailyFuelList")->with("fail","An error occured plase try again.");
        }
    	
    }
	
	
}

?>