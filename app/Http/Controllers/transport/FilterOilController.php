<?php 

namespace App\Http\Controllers\transport;

use App\Http\Controllers\Controller;
use App\models\transport\FilterOil;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class FilterOilController extends Controller
{
	
	//database connection
	public static $myDb = "transport";

	//Load vehicle list view
	public function getList()
	{
		$data["motamids"] = DB::connection("transport")->table("drivers")->where("is_motamid",1)->get();
		return view("transport.filter_oil.filter_oil_list",$data);
	}
	
	//get datatable json data
	public function getData()
	{

		//get data from model
		$apps = FilterOil::getData();
		$collection = new Collection($apps);
		return \Datatable::collection($collection)
					->showColumns(
									'id',
									'item_description',
									'properties',
									'amount',
									'unit',
									'motamid'									
									)
					->addColumn('operation',function($option)
						{
							$options = '';
							
							$options .= '<a href="'.route('getEditFilterOil',$option->id).'">Edit</a> &nbsp;';
						
							$options .= '|&nbsp;<a href="'.route('getDeleteFilterOil',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');">Delete</a>';
						
							return $options;
						}
					)
					
					->make();
	}
	
	/*
	getting form for inserting vehicle
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreateFilterOil()
	{
		$data["motamids"] = DB::connection("transport")->table("drivers")->where("is_motamid",1)->get();
		//load view for inserting vehilce
		return View::make("transport.filter_oil.insert_filter_oil",$data);
		
	}
	
	public function insertFilterOil()
	{
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "item_description" 	=> "required",
	        "properties" 		=> "required",
	        "amount" 			=> "required|Numeric",
	        "unit" 				=> "required"
	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateFilterOil")->withErrors($validates)->withInput();
	    }
	    else
	    {
	        
	        //create an object
	        $object = new FilterOil();
	        $object->item_description = Input::get("item_description");
	        $object->properties = Input::get("properties");
	        $object->amount = Input::get("amount");
	        $object->unit = Input::get("unit");
	        $object->motamid = Input::get("motamid");
	        $object->category = Input::get("category");
	        
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	            return \Redirect::route("getFilterOilList")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getFilterOilList")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0)
    {
    	
    	//get data
    	$data['row'] = FilterOil::getDetails($id);
    	$data["motamids"] = DB::connection("transport")->table("drivers")->where("is_motamid",1)->get();
    	
    	return View::make("transport.filter_oil.filter_oil_edit",$data);
    	
    }
    
    public function update($id=0)
    {
    	
    	//create an object
        $object = FilterOil::find($id);
        $object->item_description = Input::get("item_description");
        $object->properties = Input::get("properties");
        $object->amount = Input::get("amount");
        $object->unit = Input::get("unit");
        $object->motamid = Input::get("motamid");
	    $object->category = Input::get("category");
	    
        $object->updated_at = date("Y-m-d H:i:s");
	        
        if($object->save())
        {
            return \Redirect::route("getFilterOilList")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getFilterOilList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function getDelete($id=0)
    {
    	
    	//delete record
    	$deleted = FilterOil::getDelete($id);
    	
    	if($deleted)
        {
            return \Redirect::route("getFilterOilList")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getFilterOilList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function importExcel()
    {
    	
    	if(Input::hasFile('excel_file')){

			$path = Input::file('excel_file')->getRealPath();
			$year = Input::get("year");
			$category = Input::get("category");
			$motamid = Input::get("motamid");

			$data = Excel::load($path, function($reader) {

			})->get();

			if(!empty($data) && $data->count()){

				foreach ($data as $key => $value) {
					
					$insert[] = array(
										'item_description' => $value->item_description, 
										'properties' => $value->properties,
										'amount' => $value->amount,
										'unit' => $this->getUnitId($value->unit),
										'description' => $value->description,
										'year' => $year,
										'unit_price' => 0,
										'category' => $category,
										'motamid' => $motamid
									);

				}
				
				if(!empty($insert)){

					DB::connection("transport")->table('filters_and_oils')->insert($insert);

				}

			}

		}

		return back();
    	
    	
        return \Redirect::route("getFilterOilList")->with("success","Excel file imported successfully.");
        
    	
    }
    
    public function getUnitId($value="")
    {
    	return DB::connection("transport")->table('mesure_units')->where('name', 'LIKE', "%$value%")->pluck("id");
    }
	
	
}

?>