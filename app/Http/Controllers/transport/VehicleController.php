<?php 

namespace App\Http\Controllers\transport;

use App\Http\Controllers\Controller;
use App\models\transport\Vehicle;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class VehicleController extends Controller
{
	
	//database connection
	public static $myDb = "transport";

	//Load vehicle list view
	public function getList()
	{

		return view("transport.vehicle_list");
	}
	
	//get datatable json data
	public function getData()
	{

		//get data from model
		$apps = Vehicle::getData();
		$collection = new Collection($apps);
		return \Datatable::collection($collection)
					->showColumns(
									'id',
									'vehicle_type',
									'model',
									'palet_no',
									'engine_no',
									'shasi_no',
									'current_status'
								)
					->addColumn('operation',function($option)
						{
							$options = '';
							
							$options .= '<a href="'.route('getEditVehicle',$option->id).'">Edit</a> &nbsp;';
						
							$options .= '|&nbsp;<a href="'.route('getDeleteVehicle',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');">Delete</a>';
						
							return $options;
						}
					)
					
					->make();
	}
	
	/*
	getting form for inserting vehicle
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreateVehicle()
	{
		
		//load view for inserting vehilce
		return View::make("transport.insert_vehicle");
		
	}
	
	public function insertVehicle()
	{
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "vehicle_type" => "required",
	        "palet" => "required"
	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateVehicle")->withErrors($validates)->withInput();
	    }
	    else
	    {
	        //create an object from department class
	        $object = new Vehicle();
	        $object->type = Input::get("vehicle_type");
	        $object->model = Input::get("model");
	        $object->palet_no = Input::get("palet");
	        $object->engine_no = Input::get("engine");
	        $object->palet_no = Input::get("palet");
	        $object->shasi_no = Input::get("shasi");
	        $object->color = Input::get("color");
	        $object->licence_no = Input::get("license");
	        $object->current_status = Input::get("current_status");
	        $object->fuel_type = Input::get("fuel_type");
	        $object->spend_in_km = Input::get("spend_per_km");
	        $object->needs_repairing = Input::get("repairing_needs");
	        $object->amount_for_repair = Input::get("amount_for_repair");
	        $object->description = Input::get("description");
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	        	$id = $object->id;
	        	
	        	$this->uploadDocs($id);
	        	
	            return \Redirect::route("getVehicleList")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getVehicleList")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0)
    {
    	
    	//get data
    	$data['row'] = Vehicle::getDetails($id);
    	
    	//get employee attachments
    	$data["attachments"] = DB::connection('transport')->table('attachments')->where('record_id', $id)->where('table',"vehicles")->get();
    
    	
    	return View::make("transport.edit_vehicle",$data);
    	
    }
    
    public function update($id=0)
    {
    	//create an object from department class
        $object = Vehicle::find($id);
        $object->type = Input::get("vehicle_type");
        $object->model = Input::get("model");
        $object->palet_no = Input::get("palet");
        $object->engine_no = Input::get("engine");
        $object->palet_no = Input::get("palet");
        $object->shasi_no = Input::get("shasi");
        $object->color = Input::get("color");
        $object->licence_no = Input::get("license");
        $object->current_status = Input::get("current_status");
        $object->fuel_type = Input::get("fuel_type");
        $object->spend_in_km = Input::get("spend_per_km");
        $object->needs_repairing = Input::get("repairing_needs");
        $object->amount_for_repair = Input::get("amount_for_repair");
        $object->description = Input::get("description");
        $object->created_by = Auth::user()->id;

        if($object->save())
        {
        	$this->uploadDocs($id);
            return \Redirect::route("getVehicleList")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getVehicleList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function getDelete($id=0)
    {
    	
    	//delete record
    	$deleted = Vehicle::getDelete($id);
    	
    	if($deleted)
        {
            return \Redirect::route("getVehicleList")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getVehicleList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    //upload document
	public function uploadDocs($doc_id=0)
	{
		
		// getting all of the post data
		$files = Input::file('files');
		$errors = "";
		$file_data = array();

		foreach($files as $file) 
		{
			
			if(Input::hasFile('files'))
			{
			  // validating each file.
			  $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
			  $validator = Validator::make(

			  		[
			            'file' => $file,
			            'extension'  => Str::lower($file->getClientOriginalExtension()),
			        ],
			        [
			            'file' => 'required|max:100000',
			            'extension'  => 'required|in:jpg,jpeg,bmp,png,doc,docx,zip,rar,pdf,rtf,xlsx,xls,txt'
			        ]
			  	);

			  if($validator->passes())
			  {
			  	
			    // path is root/uploads
			    $destinationPath = 'documents/transport_attachments';
			    $filename = $file->getClientOriginalName();

			    //$temp = explode(".", $filename);
			    //$extension = end($temp);
			    
			    $lastFileId = DB::connection('transport')->table('attachments')->orderBy('id', 'desc')->pluck('id');
			  
			    $lastFileId++;
			    
			    $filename = $filename."_".$doc_id."_".date("Y-m-d H:i:s");//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;

			    $upload_success = $file->move($destinationPath, $filename);

			    if($upload_success) 
			    {
				   
				   $data = array(
				   					'record_id'=>$doc_id,
				   					'file_name'=>$filename,
				   					'created_at'=>date('Y-m-d H:i:s'),
				   					'table' => "vehicles",
				   					'user_id'	=> Auth::user()->id
				   				);
				   

				   if(count($data)>0)
					{
						DB::connection("transport")->table("attachments")->insert($data);
					}
					
					
				   //return Response::json('success', 200);
				   
				} 
				else 
				{
				   $errors .= json('error', 400);
				}

		    
			  } 
			  
			}

		}
		
		//return \Redirect::route("getEditServiceEmployee",$doc_id)->with("success","Attachments successfully uploaded.");
		
	}
	//download file from server
	public function downloadDoc($fileName='')
	{
		
        //public path for file
        $file= public_path(). "/documents/transport_attachments/".$fileName;
        //download file
        return Response::download($file, $fileName);
	    
	}

	//remove file from folder
	public function deleteFile($fileName="",$fileId=0,$doc_id=0)
	{
		
		
		//get file name from database
		$file= public_path(). "/documents/transport_attachments/".$fileName;
		if(File::delete($file))
		{
			//delete from database
			DB::connection('transport')
			->table('attachments')
			->where('id',$fileId)
			->delete();
			
			return \Redirect::route("getEditVehicle",$doc_id)->with("success","Attachments successfully deleted.");
		
		}
		else
		{
			return \Redirect::route("getEditVehicle",$doc_id)->with("fail","Some error!");
		}
		
	}
	
	
}

?>