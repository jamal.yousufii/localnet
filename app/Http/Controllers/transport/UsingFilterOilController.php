<?php 

namespace App\Http\Controllers\transport;

use App\Http\Controllers\Controller;
use App\models\transport\UsingFilterOil;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class UsingFilterOilController extends Controller
{
	
	//database connection
	public static $myDb = "transport";

	//Load vehicle list view
	public function getList()
	{

		return view("transport.used_filter.list");
	}
	
	//get datatable json data
	public function getData()
	{

		//get data from model
		$apps = UsingFilterOil::getData();
		$collection = new Collection($apps);
		return \Datatable::collection($collection)
					->showColumns(
									'id',
									'driver',
									'type',
									'palet_no',
									'form_no',
									'date'
									)
					->addColumn('operation',function($option)
						{
							$options = '';
							
							$options .= '<a href="'.route('getEditUsingFilterOil',$option->id).'">Edit</a> &nbsp;';
						
							$options .= '|&nbsp;<a href="'.route('getDeleteUsingFilterOil',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');">Delete</a>';
						
							return $options;
						}
					)
					
					->make();
	}
	
	/*
	getting form for inserting vehicle
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreate()
	{
		
		//get all drivers
		$data['drivers'] = DB::connection("transport")->table("drivers")->get();
		//get all filter and oil types
		$data['filter_types'] = DB::connection("transport")->table("filter_oil_types")->get();
		//load view for inserting vehilce
		return View::make("transport.used_filter.insert",$data);
		
	}
	
	public function insert()
	{
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "driver" 				=> "required",
	        "palet_number" 			=> "required",
	        "form_no" 				=> "required",
	        "date" 					=> "required"
	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateUsingFilterOil")->withErrors($validates)->withInput();
	    }
	    else
	    {
	        
	        //create an object
	        $object = new UsingFilterOil();
	        
	        //check date setting
			if(isMiladiDate())
			{
				$object->date 						= toJalali(Input::get("date"));
				$object->last_convert_date 			= toJalali(Input::get("last_convert_date"));
				$object->current_convert_date 		= toJalali(Input::get("current_convert_date"));
				
			}
			else
			{
				$object->date 						= ymd_format(Input::get("date"));
				$object->last_convert_date 			= ymd_format(Input::get("last_convert_date"));
				$object->current_convert_date 		= ymd_format(Input::get("current_convert_date"));
			}
			
			$object->driver = Input::get("driver");
			$object->palet_no = Input::get("palet_number");
			$object->last_convert_km = Input::get("last_convert_km");
			$object->current_convert_km = Input::get("current_convert_km");
	        $object->Parallel_km = Input::get("Parallel_km");
	        $object->form_no = Input::get("form_no");
	        $object->kambood_gaech = Input::get("kambood_gaech");
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	        	$last_id = $object->id;
	        	
	        	//get all filter and oil types for comparing
	        	$types = DB::connection("transport")->table("filter_oil_types")->get();
	        	
	        	$posted_types = Input::get("item_amount");
	        	$data_array = array();
	        	
	        	foreach($types AS $item)
	        	{
	        		if($posted_types[$item->id] != "")
	        		{
		        		$rows = array(
		        				"used_filter_id" => $last_id,
		        				"item" => $item->id,
		        				"amount" => $posted_types[$item->id]	        			
		        				);
		        				
		        		$data_array[] = $rows;
	        		}
	        	}
	        	
	        	DB::connection("transport")->table("used_filter_items")->insert($data_array);
	        	
	            return \Redirect::route("getUsedFilterOilList")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getUsedFilterOilList")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0)
    {
    	
    	//get data
    	$data['row'] = UsingFilterOil::getDetails($id);
    	//get all drivers
		$data['drivers'] = DB::connection("transport")->table("drivers")->get();
		//get all filter and oil types
		$data['filter_types'] = DB::connection("transport")->table("filter_oil_types")->get();
		
    	//get filter and oil related to this record
    	$selectedFilters = DB::connection("transport")->table("used_filter_items")->where("used_filter_id",$id)->get();
    	
    	$selectedItems = array();
    	foreach($selectedFilters AS $item)
    	{
    		$selectedItems[$item->item] = $item->amount;
    	}
    	
    	$data['selectedItems'] = $selectedItems;
    	
    	return View::make("transport.used_filter.edit",$data);
    	
    }
    
    public function update($id=0)
    {
    	
    	//create an object
        $object = UsingFilterOil::find($id);
        //check date setting
		if(isMiladiDate())
		{
			$object->date 						= toJalali(Input::get("date"));
			$object->last_convert_date 			= toJalali(Input::get("last_convert_date"));
			$object->current_convert_date 		= toJalali(Input::get("current_convert_date"));
			
		}
		else
		{
			$object->date 						= ymd_format(Input::get("date"));
			$object->last_convert_date 			= ymd_format(Input::get("last_convert_date"));
			$object->current_convert_date 		= ymd_format(Input::get("current_convert_date"));
		}
		
		$object->driver = Input::get("driver");
		$object->palet_no = Input::get("palet_number");
		$object->last_convert_km = Input::get("last_convert_km");
		$object->current_convert_km = Input::get("current_convert_km");
        $object->Parallel_km = Input::get("Parallel_km");
        $object->form_no = Input::get("form_no");
        $object->kambood_gaech = Input::get("kambood_gaech");
	        
        $object->updated_at = date("Y-m-d H:i:s");
	        
        if($object->save())
        {
        	//get all filter and oil types for comparing
        	$types = DB::connection("transport")->table("filter_oil_types")->get();
        	
        	$posted_types = Input::get("item_amount");
        	$data_array = array();
        	
        	foreach($types AS $item)
        	{
        		if($posted_types[$item->id] != "")
        		{
	        		$rows = array(
	        				"used_filter_id" => $id,
	        				"item" => $item->id,
	        				"amount" => $posted_types[$item->id]	        			
	        				);
	        				
	        		$data_array[] = $rows;
        		}
        	}
        	
        	//first remove the old items
        	DB::connection("transport")->table("used_filter_items")->where("used_filter_id",$id)->delete();
        	
        	DB::connection("transport")->table("used_filter_items")->insert($data_array);
        	
            return \Redirect::route("getUsedFilterOilList")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getUsedFilterOilList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function getDelete($id=0)
    {
    	//first remove the old items
        DB::connection("transport")->table("used_filter_items")->where("used_filter_id",$id)->delete();
        	
    	//delete record
    	$deleted = UsingFilterOil::getDelete($id);
    	
    	if($deleted)
        {
            return \Redirect::route("getUsedFilterOilList")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getUsedFilterOilList")->with("fail","An error occured plase try again.");
        }
    	
    }
	
	
}

?>