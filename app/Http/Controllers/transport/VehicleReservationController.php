<?php 

namespace App\Http\Controllers\transport;

use App\Http\Controllers\Controller;
use App\models\transport\VehicleReservation;
use Illuminate\Support\Collection;
use View;
use Auth;
use DB;
use Validator;
use Input;
use Redirect;
use Datatable;
use App\library\jdatetime;
use App\library\Ajax_pagination;
use Response;
use File;
use Illuminate\Support\Str;
use HTML;
use Excel;
use Request;


class VehicleReservationController extends Controller
{
	
	//database connection
	public static $myDb = "transport";

	//Load vehicle list view
	public function getList()
	{

		return view("transport.vehicle_reservation.list");
	}
	
	//get datatable json data
	public function getData()
	{

		//get data from model
		$apps = VehicleReservation::getData();
		$collection = new Collection($apps);
		return \Datatable::collection($collection)
					->showColumns(
									'id',
									'driver',
									'palet_number',
									'service_area',
									'reserved_type',
									'reserved_to_dep',
									'using_by',
									'status'
									)
					->addColumn('operation',function($option)
						{
							$options = '';
							
							$options .= '<a href="'.route('getEditVehicleReservation',$option->id).'">Edit</a> &nbsp;';
						
							$options .= '|&nbsp;<a href="'.route('getDeleteVehicleReservation',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');">Delete</a>';
						
							return $options;
						}
					)
					
					->make();
	}
	
	/*
	getting form for inserting vehicle
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreate()
	{
		
		//get all drivers
		$data['drivers'] = DB::connection("transport")->table("drivers")->get();
		
		//get all departments
		$data['departments'] = DB::table('department')->whereIn('parent',array(0,80))->where('unactive',0)->get();
		
		//get all departments
		$data['vehicles'] = DB::connection("transport")
							->table('vehicles AS v')
							->select("v.id AS v_id","vt.name AS v_type","v.palet_no","v.model")
							->leftJoin('vehicle_type AS vt', 'v.type', '=', 'vt.id')
							->get();
		
		//load view for inserting vehilce
		return View::make("transport.vehicle_reservation.insert",$data);
		
	}
	
	public function insert()
	{
		//validate the input fields
	    $validates = \Validator::make(Input::all(),array(
	        "driver" 				=> "required",
	        "vehicle" 			=> "required",
	        "service_area" 			=> "required",
	        "reserved_type" 		=> "required"	    ));
	    
	    //check the validation
	    if($validates->fails())
	    {                    
	        return \Redirect::route("getCreateVehicleReservation")->withErrors($validates)->withInput();
	    }
	    else
	    {
	        
	        //create an object
	        $object = new VehicleReservation();
	        
	        //check date setting
			if(isMiladiDate())
			{
				$object->date 			= toJalali(Input::get("date"));
				
			}
			else
			{
				$object->date 			= ymd_format(Input::get("date"));
				
			}
			$object->driver = Input::get("driver");
			$object->vehicle = Input::get("vehicle");
			$object->service_area = Input::get("service_area");
			$object->reserved_type = Input::get("reserved_type");
	        $object->reserved_to_dep = Input::get("reserved_to_dep");
	        $object->using_by = Input::get("using_by");
	        $object->is_hamala = Input::get("is_hamala");
	        $object->status = Input::get("status");
	        $object->line = Input::get("line");
	        $object->user_phone = Input::get("user_phone");
	        $object->created_by = Auth::user()->id;

	        if($object->save())
	        {
	            return \Redirect::route("getVehicleReservationList")->with("success","You successfuly created new record.");
	        }
	        else
	        {
	            return \Redirect::route("getVehicleReservationList")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	    
    public function getEdit($id=0)
    {
    	//get all departments
		$data['departments'] = DB::table('department')->whereIn('parent',array(0,80))->where('unactive',0)->get();
		
		//get all departments
		$data['vehicles'] = DB::connection("transport")
							->table('vehicles AS v')
							->select("v.id AS v_id","vt.name AS v_type","v.palet_no","v.model")
							->leftJoin('vehicle_type AS vt', 'v.type', '=', 'vt.id')
							->get();
    	//get data
    	$data['row'] = VehicleReservation::getDetails($id);
    	$parent_dep = $data['row']->service_area;
    	//get all drivers
		$data['drivers'] = DB::connection("transport")->table("drivers")->get();
		
		//get service area department
		$data['related_deps'] = DB::table('department')->where('parent',$parent_dep)->orWhere('id',$parent_dep)->where('unactive',0)->get();
    	
    	return View::make("transport.vehicle_reservation.edit",$data);
    	
    }
    
    public function update($id=0)
    {
    	
    	//create an object
        $object = VehicleReservation::find($id);
        //check date setting
		if(isMiladiDate())
		{
			$object->date 			= toJalali(Input::get("date"));
			
		}
		else
		{
			$object->date 			= ymd_format(Input::get("date"));
			
		}
		
		$object->driver = Input::get("driver");
		$object->vehicle = Input::get("vehicle");
		$object->service_area = Input::get("service_area");
		$object->reserved_type = Input::get("reserved_type");
        $object->reserved_to_dep = Input::get("reserved_to_dep");
        $object->using_by = Input::get("using_by");
        $object->is_hamala = Input::get("is_hamala");
        $object->status = Input::get("status");
        $object->line = Input::get("line");
        $object->user_phone = Input::get("user_phone");
	        
        $object->updated_at = date("Y-m-d H:i:s");
	        
        if($object->save())
        {
            return \Redirect::route("getVehicleReservationList")->with("success","You successfuly updated record.");
        }
        else
        {
            return \Redirect::route("getVehicleReservationList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function getDelete($id=0)
    {
    	
    	//delete record
    	$deleted = VehicleReservation::getDelete($id);
    	
    	if($deleted)
        {
            return \Redirect::route("getVehicleReservationList")->with("success","You successfuly deleted record.");
        }
        else
        {
            return \Redirect::route("getVehicleReservationList")->with("fail","An error occured plase try again.");
        }
    	
    }
    
    public function toExcel(request $request)
    {
    	//get data from model
		$results = VehicleReservation::getDataExcel();
		//echo "<pre>";var_dump($results);exit;
		Excel::load('reports/transport_vehicle_report.xlsx', function($file) use($results){
		//Excel::create('Filename', function($file) use($results){			
		$file->sheet($file->getActiveSheetIndex(1), function($sheet) use($results){
		//$sheet = $file->getActiveSheetIndex(0);	
			$row = 3;
			$sheet->setFreeze('A3');
			
			foreach($results AS $item)
			{
				$sheet->setHeight($row, 30);
				
				$sheet->setCellValue('A'.$row.'',$row-2);
				$sheet->setCellValue('B'.$row.'',$item->palet_number);
				$sheet->setCellValue('C'.$row.'',$item->vehicle_type);
				$sheet->setCellValue('D'.$row.'',$item->model);
				$sheet->setCellValue('E'.$row.'',$item->color);
				$sheet->setCellValue('F'.$row.'',$item->engine);
				$sheet->setCellValue('G'.$row.'',$item->shasi_no);
				$sheet->setCellValue('H'.$row.'',$item->license);
				$sheet->setCellValue('I'.$row.'',$item->driver);
				$sheet->setCellValue('J'.$row.'',$item->father_name);
				$sheet->setCellValue('K'.$row.'',$item->service_area);
				$sheet->setCellValue('L'.$row.'',$item->reserved_to_dep);
				$sheet->setCellValue('M'.$row.'',$item->using_by);
				$sheet->setCellValue('N'.$row.'',$item->driver_phone);
				$sheet->setCellValue('O'.$row.'',$item->user_phone);
				
				$row++;
			}

			$sheet->setBorder('A3:O'.($row-1).'', 'thin');
			
		});
		
		})->export('xlsx');	
    }
	
	
}

?>