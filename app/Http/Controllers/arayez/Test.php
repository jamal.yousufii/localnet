<?php 

namespace arayez;

use BaseController;
use View;
//use Arayez;
use Auth;
use DB;
use Datatables;
use Redirect;

class Test extends BaseController
{
	
	public function __construct(){
		if(!Auth::check())
		{
			return Redirect::to('user/login')->send();
		}
	}

	public function getTest()
	{
		
		$records = Arayez::getAll();
		//load view for list
		return View::make("arayez.sub.sub_test");
		
	}
	public function getView()
	{
		$result = DB::table('module')->select('module.id AS id','module.code','module.name','module.description','module.created_at','module.updated_at','module.user_id');
		return Datatables::of($result)->make();
	}

}
?>