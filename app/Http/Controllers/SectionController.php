<?php namespace App\Http\Controllers;
/*
@desc: Manipulates the module's entities
@Author: Gul Muhammad Akbari (gm.akbari27@gmail.com)
@Created At: 23 Feb 2015
@version: 1.0
*/

use Auth;
use View;
use Input;
use App\models\Section;
use App\models\Application;
use App\models\RoleX;
use Illuminate\Support\Collection;
class SectionController extends Controller
{

	/*
	Getting all records
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getAll($modId=0)
	{
		//check role
		if(canView('auth_section'))
		{
			//load view for list
			return View::make("auth.section.section_list")->with('module_id',$modId);
		}
		else
		{
			return showWarning();
		}
	}

	//get data for datatable
	public function getData($module_id=0)
	{
		//check role
		if(canView('auth_section'))
		{
			//get data from model
			$entities = Section::getData($module_id);
			//dd($entities);
			$collection = new Collection($entities);
			return \Datatable::collection($collection)
				    ->showColumns('id','code','table_name','section_name','module')
				    ->addColumn('operation',function($option){
				    	$options = '';
						if(canEdit('auth_section'))
						{
							$options .= '<a href="'.route('getUpdateSection',$option->id).'"><i class="fa fa-edit"></i> Edit</a> &nbsp;';
						}
						if(canDelete('auth_section'))
						{
							$options .= '|&nbsp;<a href="'.route('getDeleteSection',$option->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="fa fa-trash-o"></i> Delete</a>';
						}
						if(canView('auth_role'))
						{
						$options .= '&nbsp;|&nbsp; <a href="'.route('getAllRoles',$option->id).'"><i class="fa fa-edit"></i> Roles</a>';
						}
						return $options;
				    })
				    //->orderColumns('id')
				    ->make();	
		}
		else
		{
			return showWarning();
		}
	}
	
	/*
	getting form for inserting module's section
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function getCreateSection()
	{
		//check role
		if(canAdd('auth_section'))
		{
		
			//get all applications
			$apps = Application::getAll();
			//load view for users list
			return View::make("auth.section.section_create")->with('apps',$apps);
		}
		else
		{
			return showWarning();
		}
	}

	//get update module's section
	public function getUpdateSection($id=0)
	{
		//check role
		if(canEdit('auth_section'))
		{

			//get all applications
			$apps = Application::getAll();

			//get details
			$details = Section::getDetails($id);
			
			//load view for editing 
			return View::make('auth.section.section_edit')->with('details',$details)->with('apps',$apps);
		}
		else
		{
			return showWarning();
		}
	}

	/*
	Inserting filled form to module's entity
	@param: none
	@Accessiblity: public
	@return: Object
	*/
	public function postCreateSection()
	{
		//check role
		if(canAdd('auth_section'))
		{
			//validate the input fields
		    $validates = \Validator::make(Input::all(),array(
		        "code" => "required|unique:module",
		        "name_en" => "required",
		        "module"=>'required',
		        "table"	=> 'required',
		        "url_route" => 'required'
		    ));
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return \Redirect::route("getCreateSection")->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        //create an object from section class
		        $object = new Section();
		        $object->code = Input::get("code");
		        $object->name_en = Input::get("name_en");
		        $object->name_dr = Input::get("name_dr");
		        $object->name_pa = Input::get("name_pa");
		        $object->table_name = Input::get("table");
		        $object->module_id = Input::get("module");
		        $object->url_route = Input::get('url_route');
		        $object->fa_class_icon = Input::get('fa_class_icon');
		        $object->user_id = Auth::user()->id;

		        if($object->save())
		        {
		            return \Redirect::route("getAllSections")->with("success","You successfuly created new section.");
		        }
		        else
		        {
		            return \Redirect::route("getAllSections")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}
		
	}
	public function postUpdateSection($id=0)
	{
		//check role
		if(canEdit('auth_section'))
		{
			//validate the input fields
		    $validates = \Validator::make(Input::all(),array(
		        "code" => "required",
		        "name_en" => "required",
		        "module"=>'required',
		        "table"	=> 'required',
		        "url_route" => 'required'
		    ));
		    
		    //check the validation
		    if($validates->fails())
		    {                    
		        return \Redirect::route("getUpdateSection",$id)->withErrors($validates)->withInput();
		    }
		    else
		    {
		        
		        //create an object from class
		        $object = Section::find($id);
		        $object->code = Input::get("code");
		        $object->name_en = Input::get("name_en");
		        $object->name_dr = Input::get("name_dr");
		        $object->name_pa = Input::get("name_pa");
		        $object->table_name = Input::get("table");
		        $object->module_id = Input::get("module");
		        $object->url_route = Input::get('url_route');
		        $object->fa_class_icon = Input::get('fa_class_icon');
		        
		        if($object->save())
		        {
		            return \Redirect::route("getAllSections")->with("success","You successfuly updated record.");
		        }
		        else
		        {
		            return \Redirect::route("getAllSections")->with("fail","An error occured plase try again.");
		        }
		    }
		}
		else
		{
			return showWarning();
		}
		
	}
	//delete department
	public function getDeleteSection($id=0)
	{
		//check role
		if(!canDelete('auth_section'))
		{
			return showWarning();
		}
		else
		{


			$dep = Section::find($id);
			if($dep->delete())
	        {
	            return \Redirect::route("getAllSections")->with("success","You successfuly deleted record, ID: <span style='color:red;font_weight:bold;'>{{$id}}</span>");
	        }
	        else
	        {
	            return \Redirect::route("getAllSections")->with("fail","An error occured plase try again.");
	        }
	    }
	}
	/*
	Getting all records by id
	@param: section id
	@Accessiblity: public
	@return: Object
	*/
	public function getSectionRole($id=0)
	{
		//get role for entity
		$roles = RoleX::getAll($id);
		
		//load view for list
		return View::make("auth.role.role_list")->with('records',$roles);
	}

}


?>