<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Http\Requests\UsersRequest; 
use App\models\Sections; 
use App\models\Departments;
use App\models\Users;
use Response;
use DB;
use Validator;

class UsersController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      session(['current_module' => "system"]);
      session(['current_section' => "users"]);
      $data['records'] = Users::paginate(10);
      $data['lang'] = get_language();
      if(Input::get('ajax') == 1)
      {
        return view('egov/users/list_ajax',$data);
      }
      else
      {
        //load view to show searchpa result
        return view('egov/users/list',$data);
      }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $lang = get_language();
    // Get all Departments
    $data['departments'] = Departments::select('id','name_'.$lang.' as name')->get();
    // Get all Departments
    $data['sections'] = Sections::select('id','name_'.$lang.' as name')->whereIn('code',array('system','survey'))->get();
    return view('egov.users.add',$data); 
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */

  public function store(UsersRequest $request)
  {
    $record = new Users;
    $record->department_id  = $request->department_id;
    $record->email          = $request->email;
    $record->password       = Hash::make($request->password);
    $record->profile_pic    = "default.png";
    $record->created_by     = userid();
    $id = $record->save();
    if($id>0)
    {
      //INSERT USER SECTIONS
      $sections = $request->sections;
      $data = array();
      if($sections)
      {
        foreach($sections as $sec)
        {
          $sec_data = array(
            'user_id'     =>  $record->id,
            'section_id'  =>  $sec,
            'created_at'  =>  date('Y-m-d H:i:s'),
          );
          array_push($data,$sec_data);
        }
        if(COUNT($data)>0)
        {
          DB::table('user_section')->insert($data);
        }
      }

      Session()->flash('success', __("authentication.success_msg"));
    }
    else
    {
      Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($enc_id)
  {
    // Get language
    $lang = get_language();
    $data['lang'] = $lang;
    // Dycript the id
    $id = decrypt($enc_id);
    $data['enc_id'] = $enc_id;
    // Get Record
    $data['record'] = Users::find($id);
    $data['sections'] = Users::getUserSections($id,$lang);
    // Load View
    return view('egov.users.view',$data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($enc_id)
  {
    $lang = get_language();
    //Decrypt the id
    $data['enc_id'] = $enc_id;
    $id = decrypt($enc_id);
    $data['record']= Users::find($id);
    //Sections
    $data['userSections'] = array();
    $userSection = DB::table('user_section')->select('section_id')->where('user_id',$id)->get();
    if($userSection){
      foreach($userSection as $item)
      {
        $data['userSections'][] = $item->section_id;
      }
    }
    // Get all Departments
    $data['departments'] = Departments::select('id','name_'.$lang.' as name')->get();
    // Get all Departments
    $data['sections'] = Sections::select('id','name_'.$lang.' as name')->whereIn('code',array('system','survey'))->get();
    // Load view to display data
    return view('egov.users.edit',$data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request,$enc_id)
  {
    // Validate the request...
    $validates = $request->validate([
      'department_id'     => 'required',
      'email'             => 'required',
    ]);
    $id = decrypt($enc_id);
    $userid = Auth::user()->id;
    $record = Users::find($id);
    $record->department_id  = $request->department_id;
    $record->updated_by     = $userid;
    //Encrypt User's Password
    if($request->password)
    {
      $record->password = Hash::make($request->password);
    }
    $updated = $record->save();
    if($updated>0)
    {
      //Delete Befor Insert New Sections
      DB::table('user_section')->where('user_id',$id)->delete();
      //INSERT USER SECTIONS
      $sectins = $request->sectins;
      $data = array();
      if($sectins)
      {
        foreach($sectins as $sec)
        {
          $sec_data = array(
            'user_id'     =>  $record->id,
            'section_id'  =>  $sec,
            'created_at'  =>  date('Y-m-d H:i:s'),
          );
          array_push($data,$sec_data);
        }
        if(COUNT($data)>0)
        {
          DB::table('user_section')->insert($data);
        }
      }
      Session()->flash('success', __("authentication.success_edit_msg"));
    }
    else
    {
      Session()->flash('fail', __("authentication.failed_msg"));
    }
  }

  function filterUser()
  {
      $lang = get_language();
      $data['lang'] = $lang;
      if(Input::get('item'))
      {
          $item = Input::get('item');
          // Get data by keywords
          $data['records'] = Users::whereRaw("(users.email like '%".$item."%')")->paginate(10);
      }
      else
      {
          // Get all data
          $data['records'] = Users::paginate(10);
      }    
      // Load View to display records
      return view('egov.users.list_filter',$data); 
  }

  public function editPassword(Request $request)
  {
    //Validate the request...
    $validates = $request->validate([
        'password'          => 'required',
        'confirm_password'  => 'required',
    ]);
    $userid = Auth::user()->id;
    $record = Users::find($userid);
    $password = Hash::make($request->password);
    $record->password = $password;
    $updated = $record->save();
    if($updated>0)
    {
      return __("home.passord_success_edit_msg");
    }
    else
    {
      return __("home.passord_failed_edit_msg");
    }
  }

  public function uploadPic()
  {
    //Getting all of the post data
    $file = Input::file('profile_pic');
    $id = Input::post('userid');
    $errors = "";
    if(Input::hasFile('profile_pic'))
    {
      //validating each file.
      $rules = array('file' => 'required');
      $validator  = Validator::make(
        [
          'file'      => $file,
          'extension' => Str::lower($file->getClientOriginalExtension()),
        ],
        [
          'file'      => 'required|max:800000',
          'extension' => 'required|in:jpg,jpeg,png'
        ]
      );
      if($validator->passes())
      {
        //Path is root/uploads
        $destinationPath = 'attachments/users';
        $filename = $file->getClientOriginalName();
        $temp = explode(".", $filename);
        $extension = end($temp);
        $filename = "user_".$id.'.'.$extension;
        $upload_success = $file->move($destinationPath, $filename);
        if($upload_success)
        {
          $data = array('profile_pic' => $filename);
          Users::where('id',$id)->update($data);
          return '<div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show col-lg-12" role="alert">
                      <div class="m-alert__icon"><i class="la la-check-square"></i></div>
                      <div class="m-alert__text">'. __("authentication.pic_success_msg"). '</div>
                      <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
                  </div>';
        }
        else
        {
          return '<div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
                    <div class="m-alert__icon"><i class="la la-warning"></i></div>
                    <div class="m-alert__text">'. __("authentication.pic_failed_msg"). '</div>
                    <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
                  </div>';
        }
      }
      else
      {
        //Redirect back with errors.
        return Redirect::back()->withErrors($validator);
      }
    }
    else
    {
      return __("authentication.pic_failed_msg");
    }
  }
}
?>
