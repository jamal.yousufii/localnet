<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Auth;
use LaravelGettext;

abstract class NoLoginController extends BaseController {

	use DispatchesCommands, ValidatesRequests;

	
	public function __construct()
	{
		
	}
	
}


abstract class Controller extends NoLoginController {

	use DispatchesCommands, ValidatesRequests;

	
	public function __construct()
	{
		if(!Auth::check())
		{
			return \Redirect::to('user/login')->send();
		}

		//$this->middleware('auth');

		$domain=getCurrentAppCode();
		if($domain == 'manage')
		{
			$domain = 'auth';
		}
		//echo $domain;

		if (LaravelGettext::getDomain()!=$domain)
			LaravelGettext::setDomain($domain);

		parent::__construct();
	}
	
}
