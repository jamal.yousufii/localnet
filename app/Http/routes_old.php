<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Mail;
get("/email",function(){

	Mail::send('emails.test',array('name'=>'Gul Muhammad Akbari'),function($message){
		$message->to('gm.akbari27@gmail.com','Some Name')->subject('Email Subject');
	});
});

Route::filter('https', function() {
	Route::get('/testhttps', function(){

		//return View::make("layouts.testContent",array());
		return "HTTPS Passed";
	});
});

Route::get('/testForm', array('uses'=>'hr\hrController@testForm','as'=>'testForm'));
Route::post('/bringArtStyles', array('uses'=>'hr\hrController@bringArtStyles','as'=>'bringArtStyles'));

Route::get('/onlyContent', function(){

	return View::make("layouts.testContent",array());
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/msg_form',array('uses'=>'SendMsg@sendForm','as'=>'from_msg'));
Route::post('/send_msg',array('uses'=>'SendMsg@sendMsg','as'=>'send_msg'));

Route::group(
    array('prefix' => 'arayez'),
    function() {

        Route::get('test', 'arayez\Test@getTest');
        Route::get('getView', array('uses'=>'arayez\Test@getView','as'=>'arayezGetView'));

    }
);
Route::get('/changeLang/{lang}', array('uses'=>'HomeController@changeLang','as'=>'changeLang'));

Route::get('user/getView','UserController@getView');
Route::get('/', array('uses'=>'HomeController@home','as'=>'home'));

Route::group(array("before" => "guest"), function(){
	Route::get("/user/login",array("uses"=>"LoginController@getLogin","as"=>"getLogin"));

	Route::group(array('before' => 'csrf'), function(){
			Route::post("/user/login",array("uses"=>"LoginController@postLogin","as"=>"postLogin"));
	});
});

// --- Services route section -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'services'), function()
	    {

	    	Route::get('/getSerivceEmpList', array('uses'=>'services\ServicesEmpController@getList','as'=>'getServiceEmpList'));
	    	Route::get('/getSerivceEmpData', array('uses'=>'services\ServicesEmpController@getData','as'=>'getServiceEmpData'));
	    	Route::get('/getCreateEmployee', array('uses'=>'services\ServicesEmpController@getCreate','as'=>'getCreateServiceEmployee'));
			Route::get('/getEditEmployee/{id}', array('uses'=>'services\ServicesEmpController@getEdit','as'=>'getEditServiceEmployee'));
			Route::get('/getDownloadDocument/{fileName}', array('uses'=>'services\ServicesEmpController@downloadDoc','as'=>'getDownloadServiceEmpDoc'));
			Route::get('/getDeleteDocument/{fileName}/{fileId}/{doc_id}', array('uses'=>'services\ServicesEmpController@deleteFile','as'=>'getDeleteServiceEmpDoc'));
			Route::get('/getDeleteServiceEmp/{id}', array('uses'=>'services\ServicesEmpController@getDelete','as'=>'getDeleteServiceEmp'));
	    	Route::get('/getSerivceFees9List', array('uses'=>'services\ServicesFees9Controller@getList','as'=>'getServiceFees9List'));
	    	Route::get('/getSerivceFees9Data', array('uses'=>'services\ServicesFees9Controller@getData','as'=>'getServiceFees9Data'));
	    	Route::get('/getCreateFees9', array('uses'=>'services\ServicesFees9Controller@getCreate','as'=>'getCreateServiceFees9'));
			Route::get('/getEditFees9/{id}', array('uses'=>'services\ServicesFees9Controller@getEdit','as'=>'getEditServiceFees9'));
			Route::get('/getDeleteServiceFees9/{id}', array('uses'=>'services\ServicesFees9Controller@getDelete','as'=>'getDeleteServiceFees9'));
			Route::get('/getDeleteDocumentFees9/{fileName}/{fileId}/{doc_id}', array('uses'=>'services\ServicesFees9Controller@deleteFile','as'=>'getDeleteServiceFees9Doc'));
	    	Route::get('/getSerivceInvitationList', array('uses'=>'services\ServicesInvitationController@getList','as'=>'getServiceInvitationList'));
	    	Route::get('/getSerivceInvitationData', array('uses'=>'services\ServicesInvitationController@getData','as'=>'getServiceInvitationData'));
	    	Route::get('/getCreateInvitation', array('uses'=>'services\ServicesInvitationController@getCreate','as'=>'getCreateServiceInvitation'));
			Route::get('/getEditInvitation/{id}', array('uses'=>'services\ServicesInvitationController@getEdit','as'=>'getEditServiceInvitation'));
			Route::get('/getDeleteDocumentInvitation/{fileName}/{fileId}/{doc_id}', array('uses'=>'services\ServicesInvitationController@deleteFile','as'=>'getDeleteServiceInvitationDoc'));
	    	Route::get('/getSerivceSearchList', array('uses'=>'services\ServicesSearchController@getList','as'=>'getServiceSearchList'));
			Route::get('/getDeleteServiceInvitation/{id}', array('uses'=>'services\ServicesInvitationController@getDelete','as'=>'getDeleteServiceInvitation'));
	    	Route::get('/getSerivceReport', array('uses'=>'services\ServicesReportController@getList','as'=>'getServiceReportForm'));
			Route::get('/getSerivceItemList', array('uses'=>'services\ServicesItemController@getList','as'=>'getServiceItemList'));
			Route::get('/getSerivceItemData', array('uses'=>'services\ServicesItemController@getData','as'=>'getServiceItemData'));

			Route::group(array('before'=>'scrf'), function()
	    	{
	    		Route::post('/insertServiceEmpoyee', array('uses'=>'services\ServicesEmpController@insert','as'=>'insertServiceEmployee'));
	    		Route::post('/updateServiceEmployee/{id}', array('uses'=>'services\ServicesEmpController@update','as'=>'updateServiceEmployee'));
	    		Route::post('/saveEmployeeComment', array('uses'=>'services\ServicesEmpController@saveComment','as'=>'saveEmployeeComment'));
	    		Route::post('/removeEmployeeComment', array('uses'=>'services\ServicesEmpController@removeComment','as'=>'removeEmployeeComment'));
	    		Route::post('/uploadDocument/{record_id}', array('uses'=>'services\ServicesEmpController@uploadDocs','as'=>'uploadServicesEmpDocs'));
	    		Route::post('/insertServiceFees9', array('uses'=>'services\ServicesFees9Controller@insert','as'=>'insertServiceFees9'));
	    		Route::post('/updateServiceFees9/{id}', array('uses'=>'services\ServicesFees9Controller@update','as'=>'updateServiceFees9'));
	    		Route::post('/uploadDocumentFees9/{record_id}', array('uses'=>'services\ServicesFees9Controller@uploadDocs','as'=>'uploadServicesFees9Docs'));
	    		Route::post('/insertServiceInvitation', array('uses'=>'services\ServicesInvitationController@insert','as'=>'insertServiceInvitation'));
	    		Route::post('/updateServiceInvitation/{id}', array('uses'=>'services\ServicesInvitationController@update','as'=>'updateServiceInvitation'));
	    		Route::post('/uploadDocumentInvitation/{record_id}', array('uses'=>'services\ServicesInvitationController@uploadDocs','as'=>'uploadServicesInvitationDocs'));
	    		Route::post('/getSerivceSearchResult', array('uses'=>'services\ServicesSearchController@getSearchResult','as'=>'getServiceSearchResult'));
	    		Route::post('/bringRelatedType', array('uses'=>'services\ServicesReportController@getRelated','as'=>'getServiceReportRelatedFees9Types'));
	    		Route::post('/getServiceReportResult', array('uses'=>'services\ServicesReportController@getReportResult','as'=>'getServiceReportResult'));
				Route::post('/saveServiceItem', array('uses'=>'services\ServicesItemController@insert','as'=>'saveServiceItem'));

	    	});
	    });
});

// --- Transport route section -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'transport'), function()
	    {

	    	Route::get('/getVehicleList', array('uses'=>'transport\VehicleController@getList','as'=>'getVehicleList'));
	    	Route::get('/getVehicleData', array('uses'=>'transport\VehicleController@getData','as'=>'getVehicleData'));
	    	Route::get('/getVehicleDetails/{id}', array('uses'=>'transport\VehicleController@getEdit','as'=>'getEditVehicle'));
	    	Route::get('/getVehicleDelete/{id}', array('uses'=>'transport\VehicleController@getDelete','as'=>'getDeleteVehicle'));
	    	Route::get('/getDriverList', array('uses'=>'transport\DriverController@getList','as'=>'getDriverList'));
	    	Route::get('/getDriverData', array('uses'=>'transport\DriverController@getData','as'=>'getDriverData'));

	    	Route::get('/getDriverDetails/{id}', array('uses'=>'transport\DriverController@getEdit','as'=>'getEditDriver'));
	    	Route::get('/getDriverDelete/{id}', array('uses'=>'transport\DriverController@getDelete','as'=>'getDeleteDriver'));
			Route::get('/getFuelContractList', array('uses'=>'transport\FuelContractedController@getList','as'=>'getFuelContractedList'));
	    	Route::get('/getFuelContractData', array('uses'=>'transport\FuelContractedController@getData','as'=>'getFuelContractedData'));
	    	Route::get('/getFuelContractDetails/{id}', array('uses'=>'transport\FuelContractedController@getEdit','as'=>'getEditFuelContract'));
	    	Route::get('/getFuelContractDelete/{id}', array('uses'=>'transport\FuelContractedController@getDelete','as'=>'getDeleteFuelContract'));
	    	Route::get('/getFilterOilList', array('uses'=>'transport\FilterOilController@getList','as'=>'getFilterOilList'));
	    	Route::get('/getFilterOilData', array('uses'=>'transport\FilterOilController@getData','as'=>'getFilterOilData'));
	    	Route::get('/getFilterOilDetails/{id}', array('uses'=>'transport\FilterOilController@getEdit','as'=>'getEditFilterOil'));
	    	Route::get('/getFilterOilDelete/{id}', array('uses'=>'transport\FilterOilController@getDelete','as'=>'getDeleteFilterOil'));
	    	Route::get('/getDailyFuelList', array('uses'=>'transport\DailyFuelController@getList','as'=>'getDailyFuelList'));
	    	Route::get('/getDailyFuelData', array('uses'=>'transport\DailyFuelController@getData','as'=>'getDailyFuelData'));
	    	Route::get('/getDailyFuelDetails/{id}', array('uses'=>'transport\DailyFuelController@getEdit','as'=>'getEditDailyFuel'));
	    	Route::get('/getDailyFuelDelete/{id}', array('uses'=>'transport\DailyFuelController@getDelete','as'=>'getDeleteDailyFuel'));
			Route::get('/getVehicleRepairingList', array('uses'=>'transport\RepairingController@getList','as'=>'getVehicleRepairingList'));
	    	Route::get('/getVehicleRepairingData', array('uses'=>'transport\RepairingController@getData','as'=>'getVehicleRepairingData'));
	    	Route::get('/getVehicleReservationList', array('uses'=>'transport\VehicleReservationController@getList','as'=>'getVehicleReservationList'));
	    	Route::get('/getVehicleReservationData', array('uses'=>'transport\VehicleReservationController@getData','as'=>'getVehicleReservationData'));
	    	Route::get('/getUsedFilterOilList', array('uses'=>'transport\UsingFilterOilController@getList','as'=>'getUsedFilterOilList'));
	    	Route::get('/getUsedFilterOilData', array('uses'=>'transport\UsingFilterOilController@getData','as'=>'getUsedFilterOilData'));
	    	Route::get('/getFees9List', array('uses'=>'transport\Fees9Controller@getList','as'=>'getFees9List'));
	    	Route::get('/getFees9Data', array('uses'=>'transport\Fees9Controller@getData','as'=>'getFees9Data'));
	    	Route::get('/getFees9PrintPreview/{id}/{category}', array('uses'=>'transport\Fees9Controller@getPrintPreview','as'=>'getFees9PrintPreview'));

	    	Route::get('/getVehiclesList', array('uses'=>'transport\DeputyVehicleController@getList','as'=>'getDeputyVehicles'));
	    	Route::post('/getVehiclesData', array('uses'=>'transport\DeputyVehicleController@getData','as'=>'getDeputyVehiclesData'));
	    	Route::get('/getCreateVehicles', array('uses'=>'transport\DeputyVehicleController@getcreate','as'=>'getCreateDeputyVehicle'));
	    	Route::get('/getEditVehicles/{id}', array('uses'=>'transport\DeputyVehicleController@getEdit','as'=>'getEditDeputyVehicle'));
	    	Route::get('/getDeleteVehicles/{id}', array('uses'=>'transport\DeputyVehicleController@getDelete','as'=>'getDeleteDeputyVehicle'));


	    	Route::get('/getReportPanel', array('uses'=>'transport\ReportController@getPanel','as'=>'getTransportReportPanel'));
	    	Route::post('/getReportResult', array('uses'=>'transport\ReportController@getReportResult','as'=>'getTransportReportResult'));
	    	Route::post('/getFees9ItemDetails', array('uses'=>'transport\ReportController@getReportFees9ItemDetails','as'=>'getTransportFees9ItemDetails'));

	    	Route::get('/getDownloadVehicleDocument/{fileName}', array('uses'=>'transport\VehicleController@downloadDoc','as'=>'getDownloadTransportVehicleDoc'));
			Route::get('/getDeleteVehicleDocument/{fileName}/{fileId}/{doc_id}', array('uses'=>'transport\VehicleController@deleteFile','as'=>'getDeleteTransportVehicleDoc'));
			Route::get('/getReservedVehicleToExcel', array('uses'=>'transport\VehicleReservationController@toExcel','as'=>'getReservedVehicleToExcel'));

	    	Route::group(array('before'=>'scrf'), function()
	    	{
	    		Route::get('/getCreateVehicle', array('uses'=>'transport\VehicleController@getCreateVehicle','as'=>'getCreateVehicle'));
	    		Route::post('/insertVehicle', array('uses'=>'transport\VehicleController@insertVehicle','as'=>'insertVehicle'));
	    		Route::post('/updateVehicle/{id}', array('uses'=>'transport\VehicleController@update','as'=>'updateVehicle'));

	    		Route::get('/getCreateDriver', array('uses'=>'transport\DriverController@getCreateDriver','as'=>'getCreateDriver'));
	    		Route::post('/insertDriver', array('uses'=>'transport\DriverController@insertDriver','as'=>'insertDriver'));
				Route::post('/updateDriver/{id}', array('uses'=>'transport\DriverController@update','as'=>'updateDriver'));

				Route::get('/getCreatFuelContract', array('uses'=>'transport\FuelContractedController@getCreateContract','as'=>'getCreateFuelContract'));
				Route::post('/insertFuelContract', array('uses'=>'transport\FuelContractedController@insertContract','as'=>'insertFuelContract'));
	    		Route::post('/updateFuelContract/{id}', array('uses'=>'transport\FuelContractedController@update','as'=>'updateFuelContract'));

	    		Route::get('/getCreatFilterOil', array('uses'=>'transport\FilterOilController@getCreateFilterOil','as'=>'getCreateFilterOil'));
				Route::post('/insertFilterOil', array('uses'=>'transport\FilterOilController@insertFilterOil','as'=>'insertFilterOil'));
				Route::post('/updateFilterOil/{id}', array('uses'=>'transport\FilterOilController@update','as'=>'updateFilterOil'));

				Route::get('/getCreateDailyFuel', array('uses'=>'transport\DailyFuelController@getCreate','as'=>'getCreateDailyFuel'));
				Route::post('/insertDailyFuel', array('uses'=>'transport\DailyFuelController@insert','as'=>'insertDailyFuel'));
				Route::post('/updateDailyFuel/{id}', array('uses'=>'transport\DailyFuelController@update','as'=>'updateDailyFuel'));

				Route::get('/getCreateVehicleRepairing', array('uses'=>'transport\RepairingController@getCreate','as'=>'getCreateVehicleRepairing'));
				Route::post('/insertVehicleRepairing', array('uses'=>'transport\RepairingController@insert','as'=>'insertVehicleRepairing'));
				Route::get('/getVehicleRepairingDetails/{id}', array('uses'=>'transport\RepairingController@getEdit','as'=>'getEditVehicleRepairing'));
				Route::post('/updateVehicleRepairing/{id}', array('uses'=>'transport\RepairingController@update','as'=>'updateVehicleRepairing'));
	    		Route::get('/getVehicleRepairingDelete/{id}', array('uses'=>'transport\RepairingController@getDelete','as'=>'getDeleteVehicleRepairing'));

				Route::get('/getCreateVehicleReservation', array('uses'=>'transport\VehicleReservationController@getCreate','as'=>'getCreateVehicleReservation'));
	    		Route::post('/insertVehicleReservation', array('uses'=>'transport\VehicleReservationController@insert','as'=>'insertVehicleReservation'));
	    		Route::get('/getVehicleReservationDetails/{id}', array('uses'=>'transport\VehicleReservationController@getEdit','as'=>'getEditVehicleReservation'));
				Route::post('/updateVehicleReservation/{id}', array('uses'=>'transport\VehicleReservationController@update','as'=>'updateVehicleReservation'));
	    		Route::get('/getVehicleReservationDelete/{id}', array('uses'=>'transport\VehicleReservationController@getDelete','as'=>'getDeleteVehicleReservation'));

				Route::get('/getCreateUsingFilterOil', array('uses'=>'transport\UsingFilterOilController@getCreate','as'=>'getCreateUsingFilterOil'));
	    		Route::post('/insertUsingFilterOil', array('uses'=>'transport\UsingFilterOilController@insert','as'=>'insertUsingFilterOil'));
				Route::get('/getUsingFilterOilDetails/{id}', array('uses'=>'transport\UsingFilterOilController@getEdit','as'=>'getEditUsingFilterOil'));
				Route::post('/updateUsingFilterOil/{id}', array('uses'=>'transport\UsingFilterOilController@update','as'=>'updateUsingFilterOil'));
	    		Route::get('/getUsingFilterOilDelete/{id}', array('uses'=>'transport\UsingFilterOilController@getDelete','as'=>'getDeleteUsingFilterOil'));

				Route::post('/importContractExcel', array('uses'=>'transport\FilterOilController@importExcel','as'=>'importContractExcelFile'));

				Route::get('/getCreateFees9/{type}', array('uses'=>'transport\Fees9Controller@getCreate','as'=>'getCreateFees9'));
	    		Route::post('/insertFees9', array('uses'=>'transport\Fees9Controller@insert','as'=>'insertFees9'));
	    		Route::get('/getFees9Details/{id}/{category}', array('uses'=>'transport\Fees9Controller@getEdit','as'=>'getEditFees9'));
				Route::post('/updateFees9/{id}', array('uses'=>'transport\Fees9Controller@update','as'=>'updateFees9'));
	    		Route::get('/getFees9Delete/{id}', array('uses'=>'transport\Fees9Controller@getDelete','as'=>'getDeleteFees9'));

	    		Route::post('/getInsertVehicles', array('uses'=>'transport\DeputyVehicleController@insertVehicle','as'=>'insertDeputyVehicle'));
	    		Route::post('/getUpdateVehicles/{id}', array('uses'=>'transport\DeputyVehicleController@update','as'=>'updateDeputyVehicle'));

	    	});
		}
	);
});

// --- Documents and communications route section -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'docscom'), function()
	    {

	    	Route::get('/getForm', array('uses'=>'docscom\docscomController@getForm','as'=>'docsGetForm'));
	    	Route::get('/getAopForm', array('uses'=>'docscom\aopDocsController@getAopForms','as'=>'getAopDocsForm'));
	    	Route::get('/getDocAnswersForms', array('uses'=>'docscom\docAnswerController@getDocAnswersForms','as'=>'getDocAnswersForms'));
	    	Route::get('/getBarcodeForm', array('uses'=>'docscom\docscomController@getFormByBarcode','as'=>'getBarcodeForm'));
	    	Route::get('/getDocDetails/{id}/{table?}/{notification?}', array('uses'=>'docscom\docscomController@getDocDetails','as'=>'getDocDetails'));
	    	Route::get('/getCheckoutForm', array('uses'=>'docscom\docscomController@getCheckoutForm','as'=>'getCheckoutForm'));
	    	Route::get('/getDocscomDownload/{table}/{id}',array('uses'=>'docscom\docscomController@downloadDoc','as'=>'getDocscomDownload'));
	    	Route::get('/searchDocument',array('uses'=>'docscom\docscomController@searchForm','as'=>'getSearchForm'));
	    	Route::get('/getReportForm',array('uses'=>'docscom\docscomController@getReportForm','as'=>'getReportForm'));
	    	Route::group(array('before'=>'scrf'), function()
	    	{
	    		Route::post('/postForm', array('uses'=>'docscom\docscomController@postForm','as'=>'docsPostForm'));
	    		Route::post('/postUpdateForm/{table}/{id}', array('uses'=>'docscom\docscomController@postUpdateForm','as'=>'docsPostUpdateForm'));
	    		Route::post('/saveCheckOut', array('uses'=>'docscom\docscomController@saveCheckOut','as'=>'saveCheckOut'));
	    		Route::post('/findBarcodeDetails', array('uses'=>'docscom\docscomController@findBarcodeDetails','as'=>'findBarcodeDetails'));
	    		Route::post('/rejectDocument', array('uses'=>'docscom\docscomController@rejectDocument','as'=>'rejectDocument'));
			});

			//ajax post url routes
			Route::post('/validateForm', array('uses'=>'docscom\docscomController@validateForm','as'=>'docsValidateForm'));
			Route::post('/postDocComment', array('uses'=>'docscom\docscomController@postDocComment','as'=>'postDocComment'));
			Route::post('/approveDocument', array('uses'=>'docscom\docscomController@approveDocument','as'=>'approveDocument'));
			Route::post('/removeDocscomFile', array('uses'=>'docscom\docscomController@deleteFile','as'=>'removeDocscomFile'));
			Route::post('/getSearchResult', array('uses'=>'docscom\docscomController@getSearchResult','as'=>'getDocSearchResult'));
			Route::post('/bringRelatedSubDepartment', array('uses'=>'docscom\docscomController@bringRelatedSubDepartment','as'=>'bringRelatedSubDepartment'));
			Route::get('/getDocsListData','docscom\docscomController@getListData');
			Route::post('/getSearchResultData','docscom\docscomController@getSearchResultDataTable');
			Route::post('/getReportList','docscom\docscomController@getReportList');
			Route::post('/getReportData','docscom\docscomController@getReportData');
			Route::post('/changeDepartment','docscom\docscomController@changeDepartment');

			Route::get('/getAopDocsData','docscom\aopDocsController@getAopFormData');
			Route::get('/getDocAnswersData','docscom\docAnswerController@getDocAnswersData');

			Route::post('/searchResultToExcel',array('uses'=>'docscom\docscomController@searchResultToExcel','as'=>'searchResultToExcel'));

			Route::get('/getDocscomLog',array('uses'=>'docscom\docscomController@getDocLog','as'=>'getDocscomLog'));
			Route::post('/getReportLogList',array('uses'=>'docscom\docscomController@getReportLogList','as'=>'getReportDocscomLog'));

		}
	);
});
//change password
Route::group(array('before'=>'auth'),function(){
	// Route::group(array('before' => 'csrf'), function(){
		Route::post("/user/chagnePassword",array("uses"=>"UserController@changePassword","as"=>"changePassword"));
		Route::post("/user/chackOldPassword",array("uses"=>"UserController@checkOldPassword","as"=>"checkOldPassword"));
	// });
});

// --- Documents and communications route section -------------------//

// ============================== Scheduling Database Routes ============================
Route::group(array("before"=>"auth"),function()
{

	// ============================== Scheduling Database Routes ============================
	Route::group(
	    array('prefix' => 'sched'),
	    function() {

	    	Route::get('scheduling/SpecialDays', array("uses" => 'sched\SpecialDays@loadView', "as" => "SpecialDays"));
	    	Route::get('scheduling/SpecialDaysList', array("uses" => 'sched\SpecialDays@all_special_days', "as" => "SD_List"));
	    	Route::get('scheduling/form', array("uses" => 'sched\FormController@meetingForm', "as" => "MeetingForm"));
	    	Route::get('scheduling/dailyMeetings', array('uses'=>'sched\FormController@dailyMeetings','as'=>'getMeetings'));
	    	Route::get('scheduling/editMeeting/{id}/{hint}', array('uses'=>'sched\FormController@editMeetings','as'=>'EditMeetings'));
	    	// Route::get('/EditMeetings/{id}', array('uses'=>'scheduling\FormController@editMeetings','as'=>'EditMeetings'));
	    	Route::get('scheduling/downloadMeetingAttachment/{id}', array('uses'=>'sched\FormController@getDownloadMeetingAttachment','as'=>'getDownloadMeetingFile'));
	    	Route::get('scheduling/deleteMeeting/{id}', array('uses'=>'sched\FormController@deleteMeetings','as'=>'DeleteMeetings'));
	    	Route::get('scheduling/deleteRecurredMeeting/{id}', array('uses'=>'sched\FormController@deleteRecurredMeetings','as'=>'deleteRecurredMeeting'));
	    	Route::get('scheduling/loadEditView/{id}', array('uses'=>'sched\SpecialDays@loadSDEditView','as'=>'LoadSDView'));
		    Route::get('scheduling/deleteSpecialDay/{id}', array('uses'=>'sched\SpecialDays@deleteSpecialDay','as'=>'deleteSD'));
		    Route::get('scheduling/weeklyMeetingsSearch', array('uses'=>'sched\FormController@weeklyMeetings','as'=>'weeklyMeetingsSearch'));
			Route::get('scheduling/monthlyMeetings', array('uses'=>'sched\FormController@monthlyMeetings','as'=>'monthlyMeetings'));
			Route::get('/calendar', array('uses'=>'sched\FormController@showMeetingsInCalendar','as'=>'showMeetingsInCalendar'));
			// Route::get('/monthlyMeetingSearch', array('uses'=>'scheduling\FormController@searchMonthlyMeetings','as'=>'monthlyMeetingSearch'));
			// meeting log routes.
	    	Route::get('scheduling/getLogs', array('uses'=>'sched\FormController@getMeetingsLog','as'=>'getMeetingsLog'));
	    	Route::get('scheduling/searchMeetingsLog', array('uses'=>'sched\FormController@searchMeetingLog','as'=>'searchSchedLog'));
			// meeting log routes.
	    	Route::get('scheduling/delayedMeetings', array('uses'=>'sched\FormController@delayedMeetingsList','as'=>'getDelayedMeetings'));

	    	// search meetings
			Route::get('scheduling/loadSearch', array('uses'=>'sched\FormController@loadSearchPage','as'=>'getSearchMeetings'));

	    	Route::group(array('before' => 'csrf'), function(){
				Route::post('scheduling/searchMeetings', array('uses'=>'sched\FormController@searchMeetings','as'=>'postSearchMeetings'));
				Route::post('scheduling/exportExcel', array('uses'=>'sched\FormController@exportToExcel','as'=>'exportToExcel'));

				Route::post('scheduling/dailyMeetingsList', array('uses'=>'sched\FormController@dailyMeetingsPrevNext','as'=>'GetDailyMeetings'));
				Route::post('scheduling/addMeetingForm', array('uses'=>'sched\FormController@addMeetingForm','as'=>'addMeetingForm'));
				Route::post('scheduling/updateMeeting/{id}/{hint}', array('uses'=>'sched\FormController@updateMeetings','as'=>'UpdateMeetings'));
				Route::post('scheduling/updateSpecialDay/{id}', array('uses'=>'sched\SpecialDays@update_special_day','as'=>'UpdateSpecialDay'));
				Route::post('scheduling/weeklyMeetingsResult', array('uses'=>'sched\SpecialDays@searchResult','as'=>'searchResult'));
				Route::post('scheduling/getJalaliWeekDay', array('uses'=>'sched\DateConversion@getJalaliWeekDay','as'=>'getJalaliWeekDay'));
				Route::post('scheduling/getCurrentWeekDay', array('uses'=>'sched\DateConversion@getCurrentWeekDay','as'=>'getCurrentWeekDay'));
				Route::post('scheduling/convertDate', array('uses'=>'sched\DateConversion@convertToJalali','as'=>'convertToJalali'));
				Route::post('scheduling/getMonthName', array('uses'=>'sched\DateConversion@getJalaliMonthName','as'=>'getJalaliMonthName'));
				Route::post('scheduling/getMonthDifference', array('uses'=>'sched\DateConversion@getMonthDifference','as'=>'getMonthDifference'));
				Route::post('scheduling/weeklyMeetingsPrevNext', array('uses'=>'sched\FormController@weeklyMeetingsPrevNext','as'=>'weeklyMeetingsPrevNext'));
				Route::post('scheduling/monthlyMeetingsPrevNext', array('uses'=>'sched\FormController@monthlyMeetingsPrevNext','as'=>'monthlyMeetingsPrevNext'));
				Route::post('scheduling/addSpecialDays', array("uses" => 'sched\SpecialDays@add_special_days', "as" => "addSpecialDays"));
				Route::post('scheduling/checkSpecialDays', array("uses" => 'sched\SpecialDays@check_days', "as" => "checkSpecialDays"));
				Route::post("scheduling/removeSchedAttachment",array("uses"=>"sched\FormController@removeSchedulingFile","as"=>"getRemoveSchedFile"));
		});

	    }
	);
// ============================= End of Scheduling Routes =================================

	//auth route section
	Route::group(
	    array('prefix' => 'manage'),
	    function() {

	    	//change user profile picture type
	    	Route::group(array('before' => 'csrf'), function(){
	    		Route::post('changeUserPhotoType',array('uses'=>'UserController@changePhotoType','as'=>'changeUserPhotoType'));
	    	});

	    	//change user profile picture
	    	Route::group(array('before' => 'csrf'), function(){
	    		Route::post('updateUserProfilePicture',array('uses'=>'UserController@updateUserProfilePicture','as'=>'updateUserProfilePicture'));
	    	});

			//calendar event route
	    	Route::group(array('before' => 'csrf'), function(){
	    		Route::post('saveEvent',array('uses'=>'calendarController@saveCalendarEvent','as'=>'saveCalendarEvent'));
	    	});

			Route::get("/user/logout",array("uses"=>"UserController@getLogout","as"=>"getLogout"));
			//change user module route
			Route::get('user/changeModule/{moduel}',array('uses'=>'UserController@changeModule','as'=>'changeUserModule'));
			//show user warning
			Route::get('user/showWarning',array('uses'=>'UserController@showWarning','as'=>'showWarning'));

			//get emails
			Route::get('user/getEmails',array('uses'=>'UserController@getEmails','as'=>'getEmails'));
			//document routes
			Route::get('doc/getAll',array('uses'=>'DocumentController@getAll','as'=>'getAllDocuments'));
			Route::get('doc/getCreate/{id}',array('uses'=>'DocumentController@getCreateDoc','as'=>'getCreateDoc'));
			Route::get('doc/getUpdate/{id}',array('uses'=>'DocumentController@getUpdateDoc','as'=>'getUpdateDoc'));
			Route::get('doc/getCreateTree/{parent?}',array('uses'=>'DocumentController@getCreateTree','as'=>'getCreateTree'));
			Route::get('doc/getUpdateTree/{id}',array('uses'=>'DocumentController@getUpdateTree','as'=>'getUpdateTree'));
			Route::get('doc/getDownload/{id}',array('uses'=>'DocumentController@downloadDoc','as'=>'getDownload'));
			Route::get('doc/getDelete/{id}',array('uses'=>'DocumentController@deleteFile','as'=>'deleteDocFile'));

			Route::group(array('before' => 'csrf'), function(){
				Route::post('doc/postCreate',array('uses'=>'DocumentController@postCreateDoc','as'=>'postCreateDoc'));
				Route::post('doc/postUpdate/{id}',array('uses'=>'DocumentController@postUpdateDoc','as'=>'postUpdateDoc'));
				Route::post('doc/postCreateTree/{parent?}',array('uses'=>'DocumentController@postCreateTree','as'=>'postCreateTree'));
				Route::post('doc/postUpdateTree/{id}',array('uses'=>'DocumentController@postUpdateTree','as'=>'postUpdateTree'));

			});

			Route::group(array('before'=>'isAdmin'),function()
			{
				Route::get("/user/getAllUsers/{dep_id?}",array("uses"=>"UserController@getUsers","as"=>"getAllUsers"));

				Route::get('/user/getData',array('uses'=>'UserController@getData','as'=>'getUserData'));
				Route::get("/user/create",array("uses"=>"UserController@getCreate","as"=>"getCreate"));
				Route::get("/user/update/{id}",array("uses"=>"UserController@getUpdate","as"=>"getUpdate"));
				Route::get("/user/deleteUser/{id}",array("uses"=>"UserController@getDelete","as"=>"getDelete"));
				//request via ajax
				Route::post("/user/getDepApp",array("uses"=>"UserController@getDepApp","as"=>"getUserDepApp"));
				Route::post("/user/getAppSection",array("uses"=>"UserController@getAppSection","as"=>"getUserAppSecion"));
				Route::post("/user/getSectionRole",array("uses"=>"UserController@getSectionRole","as"=>"getUserSectionRole"));

				Route::group(array('before' => 'csrf'), function(){
					Route::post("/user/create",array("uses"=>"UserController@postCreate","as"=>"postCreate"));
					Route::post("/user/updateUser/{id}",array("uses"=>"UserController@postUpdate","as"=>"postUpdate"));
					Route::post("/user/getEmployeeDetails",array("uses"=>"UserController@getEmployeeDetails","as"=>"authGetEmployeeDetails"));

				});

				//department routes
				Route::get("/department/getAll",array("uses"=>"DepartmentController@getAllDepartment","as"=>"getAllDepartments"));
				Route::get("/department/getCreateDepartment",array("uses"=>"DepartmentController@getCreateDepartment","as"=>"getCreateDepartment"));
				Route::get("/department/getUpdateDepartment/{id}",array("uses"=>"DepartmentController@getUpdateDepartment","as"=>"getUpdateDepartment"));
				Route::get("/department/getDeleteDepartment/{id}",array("uses"=>"DepartmentController@getDeleteDepartment","as"=>"getDeleteDepartment"));
				Route::get("/department/getDepartmentApp/{id}",array("uses"=>"DepartmentController@getDepartmentApp","as"=>"getDepartmentApp"));
				Route::get("/department/getOrgChart",array("uses"=>"DepartmentController@departmentOrgChart","as"=>"getOrgChart"));
				Route::post("/home/dateSetting",array("uses"=>"HomeController@changeDateSetting","as"=>"changeDateSetting"));


				Route::group(array("before"=>"csrf"),function(){

						Route::post('/department/postCreateDepartment',array('uses'=>'DepartmentController@postCreateDepartment','as'=>'postCreateDepartment'));
						Route::post('/department/postUpdateDepartment/{id}',array('uses'=>'DepartmentController@postUpdateDepartment','as'=>'postUpdateDepartment'));
				});

				//application routes
				Route::get('/module/getAll',array('uses'=>'ApplicationController@getAll','as'=>'getAllApplications'));
				Route::get('/module/getData',array('uses'=>'ApplicationController@getData','as'=>'getAllAppData'));

				Route::get("/module/getCreateApp",array("uses"=>"ApplicationController@getCreateApp","as"=>"getCreateApp"));
				Route::get("/module/getUpdateApp/{id}",array("uses"=>"ApplicationController@getUpdateApp","as"=>"getUpdateApp"));
				Route::get("/module/getDeleteApp/{id}",array("uses"=>"ApplicationController@getDeleteApp","as"=>"getDeleteApp"));
				Route::get("/module/getAppSection/{id}",array("uses"=>"ApplicationController@getAppSection","as"=>"getAppSection"));


				Route::group(array("before"=>"csrf"),function(){

						Route::post('/mudule/postCreateApp',array('uses'=>'ApplicationController@postCreateApp','as'=>'postCreateApp'));
						Route::post('/module/postUpdateApp/{id}',array('uses'=>'ApplicationController@postUpdateApp','as'=>'postUpdateApp'));
				});


				//section routes
				Route::get('/section/getAll/{code?}',array('uses'=>'SectionController@getAll','as'=>'getAllSections'));
				Route::get('/section/getData/{code?}',array('uses'=>'SectionController@getData','as'=>'getSectionData'));
				Route::get("/section/getCreateSection",array("uses"=>"SectionController@getCreateSection","as"=>"getCreateSection"));
				Route::get("/section/getUpdateSection/{id}",array("uses"=>"SectionController@getUpdateSection","as"=>"getUpdateSection"));
				Route::get("/section/getDeleteSection/{id}",array("uses"=>"SectionController@getDeleteSection","as"=>"getDeleteSection"));
				Route::get("/section/getSectionRole/{id}",array("uses"=>"SectionController@getSectionRole","as"=>"getSectionRole"));


				Route::group(array("before"=>"csrf"),function(){

						Route::post('/section/postCreateSection',array('uses'=>'SectionController@postCreateSection','as'=>'postCreateSection'));
						Route::post('/section/postUpdateSection/{id}',array('uses'=>'SectionController@postUpdateSection','as'=>'postUpdateSection'));
				});

				//role routes
				Route::get('/role/getAll/{code?}',array('uses'=>'RoleController@getAll','as'=>'getAllRoles'));
				Route::get('/role/getData/{code?}',array('uses'=>'RoleController@getData','as'=>'getRoleData'));
				Route::get("/role/getCreateRole",array("uses"=>"RoleController@getCreateRole","as"=>"getCreateRole"));
				Route::get("/role/getUpdateRole/{id}",array("uses"=>"RoleController@getUpdateRole","as"=>"getUpdateRole"));
				Route::get("/role/getDeleteRole/{id}",array("uses"=>"RoleController@getDeleteRole","as"=>"getDeleteRole"));


				Route::group(array("before"=>"csrf"),function(){

						Route::post('/role/postCreateRole',array('uses'=>'RoleController@postCreateRole','as'=>'postCreateRole'));
						Route::post('/role/postUpdateRole/{id}',array('uses'=>'RoleController@postUpdateRole','as'=>'postUpdateRole'));
				});


				//localization routes
				Route::get('/localize/getAllApplication',array('uses'=>'LocalizationController@getAllApps','as'=>'getLocalizeApps'));
				Route::get('/localize/scanApplication/{appFolder}/{appId}',array('uses'=>'LocalizationController@scanApplicationFolder','as'=>'scanApplicationFolder'));
				Route::post('/localize/updateLocalizedFields',array('uses'=>'LocalizationController@updateLocalizedFields','as'=>'updateLocalizedFields'));

				Route::get('/localize/generateMoFile/{appId}/{appCode}',array('uses'=>'LocalizationController@generatingMoFile','as'=>'generatingMoFile'));
				Route::get('/localize/translateFields/{appId}/{appCode}/{current?}',array('uses'=>'LocalizationController@translateFields','as'=>'translateFields'));

				Route::post('/localize/saveAndNext',array('uses'=>'LocalizationController@saveAndNext','as'=>'saveAndNext'));
				Route::post('/localize/getFieldLimit',array('uses'=>'LocalizationController@getFieldLimit','as'=>'getFieldLimit'));
				Route::post('/localize/getPageForm',array('uses'=>'LocalizationController@getPageForm','as'=>'getPageForm'));
			});
		});

});
// ------------------- Task Management System Route -----------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(

		array('prefix' => 'task'), function(){

			//Route::get('home',array('uses'=>'workplan\HomeController@home','as'=>'getReportHome'));

			Route::get("/getReport",array("uses"=>"workplan\WorkplanController@getReport","as"=>"getReport"));
			Route::get("/getReportCreate",array("uses"=>"workplan\WorkplanController@getReportCreate","as"=>"getReportCreate"));
			Route::post("/postReportCreate",array("uses"=>"workplan\WorkplanController@postReportCreate","as"=>"postReportCreate"));
			Route::post("/postSubTaskCreate",array("uses"=>"workplan\WorkplanController@postSubTaskCreate","as"=>"postSubTaskCreate"));
			Route::post("/postProgress",array("uses"=>"workplan\WorkplanController@saveProgress","as"=>"postProgress"));
			Route::post("/postComment",array("uses"=>"workplan\WorkplanController@postComment","as"=>"postComment"));
			Route::post("/approveCompletedTask",array("uses"=>"workplan\WorkplanController@approveCompletedTask","as"=>"approveCompletedTask"));

			//added by ali
			Route::get("/getReportWeek/{week}/{mode}/{year}",array("uses"=>"workplan\WorkplanController@getReport","as"=>"getReportWeek"));
			Route::get("/getReportTooltip/{week}/{mode}/{year}/{week_no}",array("uses"=>"workplan\WorkplanController@getReport","as"=>"getReportTooltip"));
			Route::get("/getReportGroup/{num?}/{mode?}",array("uses"=>"workplan\WorkplanController@getReportGroup","as"=>"getReportGroup"));
			Route::get("/get_allTasks/{week}/{mode}/{year}",array("uses"=>"workplan\WorkplanController@getReport","as"=>"get_allTasks"));
			Route::get("/get_myTasks/{week}/{year}",array("uses"=>"workplan\WorkplanController@get_myTasks","as"=>"get_myTasks"));
			Route::get("/get_outTasks/{week}/{year}",array("uses"=>"workplan\WorkplanController@get_outTasks","as"=>"get_outTasks"));
			Route::post("/load_task_detail",array("uses"=>"workplan\WorkplanController@load_task_detail","as"=>"load_task_detail"));
			Route::post("/load_task_detail1",array("uses"=>"workplan\WorkplanController@load_task_detail1","as"=>"load_task_detail1"));
			Route::post("/load_task_assignee",array("uses"=>"workplan\WorkplanController@load_task_assignee","as"=>"load_task_assignee"));
			Route::post("/load_task_date",array("uses"=>"workplan\WorkplanController@load_task_date","as"=>"load_task_date"));
			Route::post('/rejectTask',array("uses"=>"workplan\WorkplanController@rejectTask","as"=>"rejectTask"));
			Route::get("/getWeeklyReport",array("uses"=>"workplan\WorkplanController@getWeeklyReport","as"=>"getWeeklyReport"));
			Route::post("/generateWeeklyReport",array("uses"=>"workplan\WorkplanController@generateWeeklyReport","as"=>"generateWeeklyReport"));
			//end of ali
		   	Route::get("/viewReport/{id}/{notification?}",array("uses"=>"workplan\WorkplanController@ViewReport","as"=>"ViewReport"));

		   	Route::get("/GetReportData/{id}",array("uses"=>"workplan\WorkplanController@GetReportData","as"=>"GetReportData"));

		   	Route::get("/GetSubtasksData/{id}",array("uses"=>"workplan\WorkplanController@GetSubtasksData","as"=>"GetSubtasksData"));

		   	Route::get("/editReport/{id}",array("uses"=>"workplan\WorkplanController@ViewReport","as"=>"EditReport"));

		   	Route::post("/updateReport/{id}",array("uses"=>"workplan\WorkplanController@UpdateReport","as"=>"UpdateReport"));

		   	Route::get("/deleteReport/{id}",array("uses"=>"workplan\WorkplanController@DeleteReport","as"=>"DeleteReport"));

		   	Route::get('/deleteSubtask/{id}/{parent}', array("uses"=>"workplan\WorkplanController@DeleteSubtask","as"=>"DeleteSubtask"));
		   	Route::post('/loadEditSubModal',array("uses"=>"workplan\WorkplanController@loadEditModal","as"=>"loadEditSubModal"));
			Route::post('/postEditSubModal',array("uses"=>"workplan\WorkplanController@postSubTaskUpdate","as"=>"postSubTaskUpdate"));
			Route::post('/loadModal',array("uses"=>"workplan\WorkplanController@loadModal","as"=>"loadModal"));
			Route::post('/loadModal_month',array("uses"=>"workplan\WorkplanController@loadModal_month","as"=>"loadModal_month"));

			Route::post('/loadApprovalModal',array("uses"=>"workplan\WorkplanController@loadApprovalModal","as"=>"loadApprovalModal"));
			Route::post('/postSaveApproval',array("uses"=>"workplan\WorkplanController@postSaveApproval","as"=>"postSaveApproval"));
			Route::post('/approveTask',array("uses"=>"workplan\WorkplanController@approveTask","as"=>"approveTask"));

			//task group routes
			Route::get('/getTaskGroups',array("uses"=>"workplan\TaskGroupController@getTaskGroups","as"=>"getTaskGroups"));
			Route::get('/getTaskGoupData','workplan\TaskGroupController@getTaskGroupData');
			Route::get('/getTaskGroupCreate',array("uses"=>"workplan\TaskGroupController@getTaskGroupCreate","as"=>"getTaskGroupCreate"));
			Route::get('/getDeleteTaskGroup/{id}',array("uses"=>"workplan\TaskGroupController@deleteTaskGroup","as"=>"getDeleteTaskGroup"));
			Route::get('/getTaskGroupUpdate/{id}',array("uses"=>"workplan\TaskGroupController@getTaskGroupUpdate","as"=>"getTaskGroupUpdate"));
			Route::post('/isTaskGroupInReport',array("uses"=>"workplan\TaskGroupController@isTaskGroupInReport","as"=>"isTaskGroupInReport"));

			//via ajax
			Route::post('/saveNewTaks',array("uses"=>"workplan\WorkplanController@saveNewTask","as"=>"saveNewTask"));
			Route::post('/loadDetailsViaAjax',array("uses"=>"workplan\WorkplanController@loadDetailsViaAjax","as"=>"loadDetailsViaAjax"));
		   	Route::post("/updateReportViaAjax",array("uses"=>"workplan\WorkplanController@updateReportViaAjax","as"=>"updateReportViaAjax"));
			Route::post("/updateAssigneeViaAjax",array("uses"=>"workplan\WorkplanController@updateAssigneeViaAjax","as"=>"updateAssigneeViaAjax"));
		   	Route::post("/changeDateViaAjax",array("uses"=>"workplan\WorkplanController@changeTaskDate","as"=>"changeTaskDate"));
			Route::post("/paginateTasks",array("uses"=>"workplan\WorkplanController@paginateTasks","as"=>"paginateTasks"));
		   	Route::post("/get_assignee",array("uses"=>"workplan\WorkplanController@getAssignee","as"=>"get_assignee"));

		   	Route::get("/weeklyReport_excel/{sdate}/{edate}/{dep}",array("uses"=>"workplan\WorkplanController@weeklyReport_excel","as"=>"weeklyReport_excel"));

			Route::get("/downloadAttachmentTasks/{id}",array("uses"=>"workplan\WorkplanController@downloadDoc","as"=>"downloadAttachmentTasks"));
			Route::post("/deleteAttachmentTask",array("uses"=>"workplan\WorkplanController@deleteFile","as"=>"deleteAttachmentTask"));

			Route::group(array('before'=>'csrf'),function(){
				Route::post('/postTaskGroupCreate',array("uses"=>"workplan\TaskGroupController@postTaskGroupCreate","as"=>"postTaskGroupCreate"));
				Route::post('/postTaskGroupUpdate/{id}',array("uses"=>"workplan\TaskGroupController@postTaskGroupUpdate","as"=>"postTaskGroupUpdate"));

			});
		}
	);
});
// -------------------IT Task Management System Route -----------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
		array('prefix' => 'it_task'), function(){
			Route::get("/getReport",array("uses"=>"workplan\it_task@getReport","as"=>"getReport_it"));
		  	Route::get("/getReportCreate",array("uses"=>"workplan\it_task@getReportCreate","as"=>"getReportCreate_it"));
			Route::post("/postReportCreate",array("uses"=>"workplan\it_task@postReportCreate","as"=>"postReportCreate_it"));
			Route::post("/postSubTaskCreate",array("uses"=>"workplan\it_task@postSubTaskCreate","as"=>"postSubTaskCreate_it"));
			Route::post("/postProgress",array("uses"=>"workplan\it_task@saveProgress","as"=>"postProgress_it"));
			Route::post("/postComment",array("uses"=>"workplan\it_task@postComment","as"=>"postComment_it"));
			Route::post("/approveCompletedTask",array("uses"=>"workplan\it_task@approveCompletedTask","as"=>"approveCompletedTask_it"));
			//added by ali
			Route::get("/getReportWeek/{week}/{mode}/{year}",array("uses"=>"workplan\it_task@getReport","as"=>"getReportWeek_it"));
			Route::get("/getReportTooltip/{week}/{mode}/{year}/{week_no}",array("uses"=>"workplan\it_task@getReport","as"=>"getReportTooltip_it"));
			Route::get("/getReportGroup/{num?}/{mode?}",array("uses"=>"workplan\it_task@getReportGroup","as"=>"getReportGroup_it"));
			Route::get("/get_allTasks/{week}/{mode}/{year}",array("uses"=>"workplan\it_task@getReport","as"=>"get_allTasks_it"));
			Route::get("/get_myTasks/{week}/{year}",array("uses"=>"workplan\it_task@get_myTasks","as"=>"get_myTasks_it"));
			Route::get("/get_outTasks/{week}/{year}",array("uses"=>"workplan\it_task@get_outTasks","as"=>"get_outTasks_it"));
			Route::post("/load_task_detail",array("uses"=>"workplan\it_task@load_task_detail","as"=>"load_task_detail_it"));
			Route::post("/load_task_detail1",array("uses"=>"workplan\it_task@load_task_detail1","as"=>"load_task_detail1_it"));
			Route::post("/load_task_assignee",array("uses"=>"workplan\it_task@load_task_assignee","as"=>"load_task_assignee_it"));
			Route::post("/load_task_date",array("uses"=>"workplan\it_task@load_task_date","as"=>"load_task_date_it"));
			Route::post('/rejectTask',array("uses"=>"workplan\it_task@rejectTask","as"=>"rejectTask_it"));
			Route::get("/getWeeklyReport",array("uses"=>"workplan\it_task@getWeeklyReport","as"=>"getWeeklyReport_it"));
			Route::post("/generateWeeklyReport",array("uses"=>"workplan\it_task@generateWeeklyReport","as"=>"generateWeeklyReport_it"));
			//end of ali
		   	Route::get("/viewReport/{id}/{notification?}",array("uses"=>"workplan\it_task@ViewReport","as"=>"ViewReport_it"));
		   	Route::get("/GetReportData/{id}",array("uses"=>"workplan\it_task@GetReportData","as"=>"GetReportData_it"));
		   	Route::get("/GetSubtasksData/{id}",array("uses"=>"workplan\it_task@GetSubtasksData","as"=>"GetSubtasksData_it"));
		   	Route::get("/editReport/{id}",array("uses"=>"workplan\it_task@ViewReport","as"=>"EditReport_it"));
		   	Route::post("/updateReport/{id}",array("uses"=>"workplan\it_task@UpdateReport","as"=>"UpdateReport_it"));
		   	Route::get("/deleteReport/{id}",array("uses"=>"workplan\it_task@DeleteReport","as"=>"DeleteReport_it"));
		   	Route::get('/deleteSubtask/{id}/{parent}', array("uses"=>"workplan\it_task@DeleteSubtask","as"=>"DeleteSubtask_it"));
		   	Route::post('/loadEditSubModal',array("uses"=>"workplan\it_task@loadEditModal","as"=>"loadEditSubModal_it"));
			Route::post('/postEditSubModal',array("uses"=>"workplan\it_task@postSubTaskUpdate","as"=>"postSubTaskUpdate_it"));
			Route::post('/loadModal',array("uses"=>"workplan\it_task@loadModal","as"=>"loadModal_it"));
			Route::post('/loadApprovalModal',array("uses"=>"workplan\it_task@loadApprovalModal","as"=>"loadApprovalModal_it"));
			Route::post('/postSaveApproval',array("uses"=>"workplan\it_task@postSaveApproval","as"=>"postSaveApproval_it"));
			Route::post('/approveTask',array("uses"=>"workplan\it_task@approveTask","as"=>"approveTask_it"));
			//task group routes
			Route::get('/getTaskGroups',array("uses"=>"workplan\it_task@getTaskGroups","as"=>"getTaskGroups_it"));
			Route::get('/getTaskGoupData',array("uses"=>"workplan\it_task@getTaskGroupData","as"=>"getTaskGroupData_it"));
			Route::get('/getTaskGroupCreate',array("uses"=>"workplan\it_task@getTaskGroupCreate","as"=>"getTaskGroupCreate_it"));
			Route::get('/getDeleteTaskGroup/{id}',array("uses"=>"workplan\it_task@deleteTaskGroup","as"=>"getDeleteTaskGroup_it"));
			Route::get('/getTaskGroupUpdate/{id}',array("uses"=>"workplan\it_task@getTaskGroupUpdate","as"=>"getTaskGroupUpdate_it"));
			Route::post('/isTaskGroupInReport',array("uses"=>"workplan\it_task@isTaskGroupInReport","as"=>"isTaskGroupInReport_it"));
			//via ajax
			Route::post('/saveNewTaks',array("uses"=>"workplan\it_task@saveNewTask","as"=>"saveNewTask_it"));
			Route::post('/loadDetailsViaAjax',array("uses"=>"workplan\it_task@loadDetailsViaAjax","as"=>"loadDetailsViaAjax_it"));
		   	Route::post("/updateReportViaAjax",array("uses"=>"workplan\it_task@updateReportViaAjax","as"=>"updateReportViaAjax_it"));
		   	Route::post("/saveReportViaAjax",array("uses"=>"workplan\it_task@saveReportViaAjax","as"=>"saveReportViaAjax_it"));
			Route::post("/updateAssigneeViaAjax",array("uses"=>"workplan\it_task@updateAssigneeViaAjax","as"=>"updateAssigneeViaAjax_it"));
		   	Route::post("/changeDateViaAjax",array("uses"=>"workplan\it_task@changeTaskDate","as"=>"changeTaskDate_it"));
			Route::post("/paginateTasks",array("uses"=>"workplan\it_task@paginateTasks","as"=>"paginateTasks_it"));
		   	Route::post("/get_assignee",array("uses"=>"workplan\it_task@getAssignee","as"=>"get_assignee_it"));
		   	Route::get("/weeklyReport_excel/{sdate}/{edate}",array("uses"=>"workplan\it_task@weeklyReport_excel","as"=>"weeklyReport_excel_it"));
			Route::get("/downloadAttachmentTasks/{id}",array("uses"=>"workplan\it_task@downloadDoc","as"=>"downloadAttachmentTasks_it"));
			Route::post("/deleteAttachmentTask",array("uses"=>"workplan\it_task@deleteFile","as"=>"deleteAttachmentTask_it"));
			Route::group(array('before'=>'csrf'),function(){
				Route::post('/postTaskGroupCreate',array("uses"=>"workplan\it_task@postTaskGroupCreate","as"=>"postTaskGroupCreate_it"));
				Route::post('/postTaskGroupUpdate/{id}',array("uses"=>"workplan\it_task@postTaskGroupUpdate","as"=>"postTaskGroupUpdate_it"));
			});
		}
	);
});

//------------------- Finance ----------------------------- //
Route::group(array("before"=>"auth"),function() {
    Route::group(

        array('prefix' => 'finance'), function () {

            Route::get('/new_info', 'finance\fcontroller@new_info');
            Route::get('/info_list', array('uses' => 'finance\fcontroller@listAll', 'as' => 'getFinanceList'));
            Route::get('details/{ide}', array('uses' => 'finance\fcontroller@details', 'as' => 'details'));

            Route::post('/new_info', array('uses' => 'finance\fcontroller@insert_info', 'as' => 'new_info'));

            // ajax routes ---------
            Route::get('/getData', array('uses' => 'finance\fcontroller@getAllData', 'as' => 'financeGetData'));
        }
    );
});
// ------------------- communication -----------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(

		array('prefix' => 'com'), function(){

			//Route::get('home',array('uses'=>'workplan\HomeController@home','as'=>'getReportHome'));

			Route::get("/getReceived",array("uses"=>"com\comController@getReceived","as"=>"getReceived"));
			Route::get("/CreateReceived_doc",array("uses"=>"com\comController@create_received_doc","as"=>"CreateReceived_doc"));
			Route::post("/postCreateReceived_doc",array("uses"=>"com\comController@postCreateReceived_doc","as"=>"postCreateReceived_doc"));
			Route::get("/getUpdateReceived_doc/{id}",array("uses"=>"com\comController@getUpdateReceived_doc","as"=>"getUpdateReceived_doc"));
			Route::post('/postUpdateReceived_doc/{id}',array('uses'=>'com\comController@postUpdateReceived_doc','as'=>'postUpdateReceived_doc'));
			Route::get("/getDeleteReceived_doc/{id}",array("uses"=>"com\comController@getDeleteReceived_doc","as"=>"getDeleteReceived_doc"));
			Route::get("/getDetailsReceived_doc/{id}",array("uses"=>"com\comController@getDetailsReceived_doc","as"=>"getDetailsReceived_doc"));

			Route::get("/getSended",array("uses"=>"com\comController@getSended","as"=>"getSended"));
			Route::get("/CreateSended_doc",array("uses"=>"com\comController@create_sended_doc","as"=>"CreateSended_doc"));
			Route::post("/postCreateSended_doc",array("uses"=>"com\comController@postCreateSended_doc","as"=>"postCreateSended_doc"));
			Route::get("/getUpdateSended_doc/{id}",array("uses"=>"com\comController@getUpdateSended_doc","as"=>"getUpdateSended_doc"));
			Route::post('/postUpdateSended_doc/{id}',array('uses'=>'com\comController@postUpdateSended_doc','as'=>'postUpdateSended_doc'));
			Route::get("/getDeleteSended_doc/{id}",array("uses"=>"com\comController@getDeleteSended_doc","as"=>"getDeleteSended_doc"));
			Route::get("/getDetailsSended_doc/{id}",array("uses"=>"com\comController@getDetailsSended_doc","as"=>"getDetailsSended_doc"));
			Route::get("/getAutocomplete}",array("uses"=>"com\comController@getAutocomplete","as"=>"getAutocomplete"));

			Route::post("/postReportReceived_doc",array("uses"=>"com\comController@postReportReceived_doc","as"=>"postReportReceived_doc"));
			Route::get("/getReceived_report",array("uses"=>"com\comController@getReceived_report","as"=>"getReceived_report"));
			Route::post('/getReceived_excel',array('uses'=>'com\comController@getReceived_excel','as'=>'getReceived_excel'));

			Route::get("/getSended_report",array("uses"=>"com\comController@getSended_report","as"=>"getSended_report"));
			Route::post("/postReportSended_doc",array("uses"=>"com\comController@postReportSended_doc","as"=>"postReportSended_doc"));
			Route::post('/getSended_excel',array('uses'=>'com\comController@getSended_excel','as'=>'getSended_excel'));

			Route::get("/downloadAttachment_received/{id}",array("uses"=>"com\comController@downloadDoc","as"=>"downloadAttachment_received"));
			Route::post("/deleteAttachment_received",array("uses"=>"com\comController@deleteFile","as"=>"deleteAttachment_received"));

			Route::get('/getStaticTables',array('uses'=>'com\static_tables@getStaticTables','as'=>'getStaticTables'));
			Route::post('/getTable_det',array('uses'=>'com\static_tables@getTable_details','as'=>'getTable_det'));
			Route::post('/postCreateStatic',array('uses'=>'com\static_tables@postCreateStatic','as'=>'postCreateStatic'));
			Route::post("/getUpdateStatic",array("uses"=>"com\static_tables@getUpdateStatic","as"=>"getUpdateStatic"));
			Route::post("/postUpdateStatic",array("uses"=>"com\static_tables@postUpdateStatic","as"=>"postUpdateStatic"));
			Route::post('/getDeleteStatic',array('uses'=>'com\static_tables@delete_static','as'=>'getDeleteStatic'));
			Route::post('/change_date',array('uses'=>'com\comController@change_date','as'=>'change_date'));
			Route::post('/get_senders_list',array('uses'=>'com\comController@getSender_list','as'=>'get_sender_list'));
			Route::post('/get_types_list',array('uses'=>'com\comController@getType_list','as'=>'get_type_list'));
		}
	);
	Route::get('/com/getData','com\comController@getData');
	Route::get('/com/getSendData','com\comController@getSendData');
	//Route::post('com/getSearchResultData_received','com\comController@getData_received_report');
	Route::get('/com/getData_statictable/{table}','com\static_tables@getData');
});



// --- HR route section -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'hr'), function()
	    {

	    	//insert images for attendance
	    	Route::post("/insertAttendanceImages",array("uses"=>"hr\hrController@insertImages","as"=>"insertAttendanceImages"));
	    	Route::get('/hrHomePage', array('uses'=>'hr\hrController@homePage','as'=>'hrHomePage'));
	    	Route::get('/getAllEmployees', array('uses'=>'hr\hrController@getAllEmployees','as'=>'getAllEmployees'));
	    	Route::get('/registerEmployee',array('uses'=>'hr\hrController@getRegisterForm','as'=>'getRegisterForm'));
	    	Route::get('/getEmployeeDetails/{id}',array('uses'=>'hr\hrController@getEmployeeDetails','as'=>'getEmployeeDetails'));
	    	Route::get('/getEmployeeCardDetails/{id}',array('uses'=>'hr\hrController@getEmployeeCardForm','as'=>'getEmployeeCardDetails'));
	    	Route::get('/getEmployeeDownload/{id}',array('uses'=>'hr\hrController@downloadDoc','as'=>'getEmployeeDownload'));
	    	Route::get('/getEmployeeIDs',array('uses'=>'hr\hrController@getEmployeeIDs','as'=>'getEmployeeIDs'));
	    	Route::post('/getEmployeeIDs_search',array('uses'=>'hr\hrController@getEmployeeIDs_search','as'=>'getEmployeeIDs_search'));

	    	Route::get('/cardTemplate/{id}',array('uses'=>'hr\hrController@cardTemplate','as'=>'getCardTemplate'));
	    	Route::get('/cardTemplatePrint/{id}',array('uses'=>'hr\hrController@cardTemplatePrint','as'=>'getCardTemplatePrint'));
	    	Route::get('/cardTemplateView/{id}',array('uses'=>'hr\hrController@cardTemplateView','as'=>'getCardTemplate_view'));
		Route::get('/newCardTemplate/{id}',array('uses'=>'hr\hrController@newCardTemplate','as'=>'newCardTemplate'));
	    	Route::get('/cardTemplateTemp/{id}',array('uses'=>'hr\hrController@cardTemplateTemp','as'=>'getCardTemplateTemp'));
	    	Route::get('/forPrint/{id}',array('uses'=>'hr\hrController@forPrint','as'=>'forPrint'));
	    	Route::get('/printAll',array('uses'=>'hr\hrController@getEmployeeForPrint','as'=>'printAll'));

	    	Route::group(array('before' => 'csrf'), function(){
	    		Route::post('/postEmployee',array('uses'=>'hr\hrController@postEmployee','as'=>'postEmployee'));
	    		Route::post('/postUpdateEmployee/{id}',array('uses'=>'hr\hrController@postUpdateEmployee','as'=>'postUpdateEmployee'));
	    		Route::post('/postEmployeeCard/{id}',array('uses'=>'hr\hrController@postEmployeeCard','as'=>'postEmployeeCard'));
	    		Route::post('/saveRFID',array('uses'=>'hr\hrController@saveRFID','as'=>'saveRFID'));
			Route::post('/postNewEmployee',array('uses'=>'hr\hrController@postNewEmployee','as'=>'postNewEmployee'));
	    	});
	    	// ajax routes ---------
            Route::get('/getEmployeeData', array('uses' => 'hr\hrController@getEmployeeData', 'as' => 'getEmployeeData'));
            Route::get('/getEmployeeCardData', array('uses' => 'hr\hrController@getEmployeeCardData', 'as' => 'getEmployeeCardData'));
            Route::post('/removeEmployeeFile', array('uses'=>'hr\hrController@deleteFile','as'=>'removeEmployeeFile'));
            Route::post('/postReady', array('uses'=>'hr\hrController@checkCardReady','as'=>'checkCardReady'));
            Route::post('/getProvinceDistrict', array('uses'=>'hr\hrController@getProvinceDistrict','as'=>'getProvinceDistrict'));
			Route::post('/getRelatedDepartment', array('uses'=>'hr\hrController@bringSubDepartment','as'=>'bringSubDepartment'));
			Route::get('/getEmployeeDataPrint', array('uses'=>'hr\hrController@getEmployeeDataPrint','as'=>'getEmployeeDataPrint'));
			Route::post('/searchDepartmentEmployees', array('uses'=>'hr\hrController@searchDepartmentEmployees','as'=>'searchDepartmentEmployees'));
			Route::get('/attendance_chart',array('uses'=>'hr\hrController@attendance_chart','as'=>'attendance_chart'));
			Route::post('/generate_att_chart',array('uses'=>'hr\hrController@generate_att_chart','as'=>'generate_att_chart'));
			//recruitment
			Route::get('/getRecruitment', array('uses'=>'hr\hrController@getRecruitment','as'=>'getRecruitment'));
			Route::post('/getRecruitment_search', array('uses'=>'hr\hrController@getRecruitment_search','as'=>'getRecruitment_search'));
			Route::get('/addNewEmployee',array('uses'=>'hr\hrController@addNewEmployee','as'=>'addNewEmployee'));
			Route::post("/fireDate",array("uses"=>"hr\hrController@fireDate","as"=>"fireDate"));
			Route::post("/retireDate",array("uses"=>"hr\hrController@retireDate","as"=>"retireDate"));
			Route::post("/tanqisDate",array("uses"=>"hr\hrController@tanqisDate","as"=>"tanqisDate"));
			Route::post("/employeeResign",array("uses"=>"hr\hrController@employeeResign","as"=>"employeeResign"));
			Route::post("/loadEmployees",array("uses"=>"hr\hrController@loadEmployees","as"=>"loadEmployees"));
			Route::get('/getFiredEmployeeData', array('uses' => 'hr\hrController@getFiredEmployeeData', 'as' => 'getFiredEmployeeData'));
			Route::get('/getResignEmployeeData', array('uses' => 'hr\hrController@getResignEmployeeData', 'as' => 'getResignEmployeeData'));
			Route::get('/getRetireEmployeeData', array('uses' => 'hr\hrController@getRetireEmployeeData', 'as' => 'getRetireEmployeeData'));
			Route::post("/load_change_employee",array("uses"=>"hr\hrController@load_change_employee","as"=>"load_change_employee"));
			Route::post("/changeEmployeeViaAjax",array("uses"=>"hr\hrController@changeEmployeeViaAjax","as"=>"changeEmployeeViaAjax"));
			Route::get('/getChangedEmployeeData', array('uses' => 'hr\hrController@getChangedEmployeeData', 'as' => 'getChangedEmployeeData'));
			Route::get('/getEmployeeUpdate/{id}',array('uses'=>'hr\hrController@getEmployeeUpdate','as'=>'getEmployeeUpdate'));
			Route::post("/load_change_detail",array("uses"=>"hr\hrController@load_change_detail","as"=>"load_change_detail"));
			Route::post("/load_service_employee",array("uses"=>"hr\hrController@load_service_employee","as"=>"load_service_employee"));
			Route::post("/serviceEmployeeViaAjax",array("uses"=>"hr\hrController@serviceEmployeeViaAjax","as"=>"serviceEmployeeViaAjax"));
			Route::get('/getServiceEmployeeData', array('uses' => 'hr\hrController@getServiceEmployeeData', 'as' => 'getServiceEmployeeData'));
			Route::get('/getEmployeeDocs/{id}',array('uses'=>'hr\hrController@getEmployeeDocs','as'=>'getEmployeeDocs'));
			Route::post('/postEmployeeDocs/{id}',array('uses'=>'hr\hrController@postEmployeeDocs','as'=>'postEmployeeDocs'));
			Route::get('/getEmployeeSuggestion/{id}',array('uses'=>'hr\hrController@getEmployeeSuggestion','as'=>'getEmployeeSuggestion'));
			Route::get('/getEmployeeHire/{id}',array('uses'=>'hr\hrController@getEmployeeHire','as'=>'getEmployeeHire'));
			Route::post('/postEmployeeSuggestion/{id}',array('uses'=>'hr\hrController@postEmployeeSuggestion','as'=>'postEmployeeSuggestion'));
			Route::post('/postEmployeeHire/{id}',array('uses'=>'hr\hrController@postEmployeeHire','as'=>'postEmployeeHire'));
			Route::post('/postEmployeeSalary/{id}',array('uses'=>'hr\hrController@postEmployeeSalary','as'=>'postEmployeeSalary'));
			Route::get('/getEmployeeLeaves/{id}/{year}/{month}',array('uses'=>'hr\hrController@getEmployeeLeaves','as'=>'getEmployeeLeaves'));
			Route::post('/getEditLeaveForm',array('uses'=>'hr\hrController@getEditLeaveForm','as'=>'getEditLeaveForm'));
			Route::post('/updateEmployee/{id}',array('uses'=>'hr\hrController@updateEmployee','as'=>'updateEmployee'));
			Route::post('/updateEmployeeContract/{id}',array('uses'=>'hr\hrController@updateEmployeeContract','as'=>'updateEmployeeContract'));
			Route::post('/bringExistBastViaAjax', array('uses'=>'hr\hrController@bringExistBastViaAjax','as'=>'bringExistBastViaAjax'));
			Route::get('/getExtraBastEmployeeData', array('uses' => 'hr\hrController@getExtraBastEmployeeData', 'as' => 'getExtraBastEmployeeData'));
			Route::get('/getTanqisEmployeeData', array('uses' => 'hr\hrController@getTanqisEmployeeData', 'as' => 'getTanqisEmployeeData'));
			Route::get('/getContractsEmployeeData', array('uses' => 'hr\hrController@getContractsEmployeeData', 'as' => 'getContractsEmployeeData'));
			Route::post('/getRecruitmentSearch', array('uses'=>'hr\hrController@getRecruitmentSearch','as'=>'getRecruitmentSearch'));
			Route::get('/getRecruitmentSearchData/{dep?}/{sub_dep?}/{bast?}/{type?}/{doc}/{typee?}/{gender?}/{edu?}/{year?}/{name?}', array('uses' => 'hr\hrController@getRecruitmentSearchData', 'as' => 'getRecruitmentSearchData'));
			//Route::post('/getRecruitmentSearchData', array('uses' => 'hr\hrController@getRecruitmentSearchData', 'as' => 'getRecruitmentSearchData'));
			Route::post('/saveEmployeeNewTashkil', array('uses'=>'hr\hrController@saveEmployeeNewTashkil','as'=>'saveEmployeeNewTashkil'));
			//documents
			Route::get('/getDocumentsUpdate/{id}',array('uses'=>'hr\hrController@getDocumentsUpdate','as'=>'getDocumentsUpdate'));
			Route::post('/getMoreExperience/{id}',array('uses'=>'hr\hrController@getMoreExperience','as'=>'getMoreExperience'));
			Route::post("/punishEmployeeUpdate",array("uses"=>"hr\hrController@punishEmployeeUpdate","as"=>"punishEmployeeUpdate"));
			Route::post("/load_promotion_employee",array("uses"=>"hr\hrController@load_promotion_employee","as"=>"load_promotion_employee"));
			Route::post("/promoteEmployeeViaAjax",array("uses"=>"hr\hrController@promoteEmployeeViaAjax","as"=>"promoteEmployeeViaAjax"));
			Route::post("/promoteEmployeeUpdate",array("uses"=>"hr\hrController@promoteEmployeeUpdate","as"=>"promoteEmployeeUpdate"));
			Route::post("/makafatEmployeeUpdate",array("uses"=>"hr\hrController@makafatEmployeeUpdate","as"=>"makafatEmployeeUpdate"));
			Route::post('/copyToAttendanceServer',array('uses'=>'hr\hrController@copyToAttendance','as'=>'copyToAttendanceServer'));
			Route::post('/copyToAttendance',array('uses'=>'hr\hrController@exportToCsv','as'=>'copyToAttendance'));
			Route::post("/load_promotion_update",array("uses"=>"hr\hrController@load_promotion_update","as"=>"load_promotion_update"));
			Route::post("/load_makafat_update",array("uses"=>"hr\hrController@load_makafat_update","as"=>"load_makafat_update"));
			Route::post("/load_punish_update",array("uses"=>"hr\hrController@load_punish_update","as"=>"load_punish_update"));
			//new
			Route::get('/getDetailsEmployee/{id}',array('uses'=>'hr\hrController@getDetailsEmployee','as'=>'getDetailsEmployee'));
			Route::get('/getContractEmployee/{id}',array('uses'=>'hr\hrController@getContractEmployee','as'=>'getContractEmployee'));
			Route::post("/loadEmployeeDetails",array("uses"=>"hr\hrController@loadEmployeeDetails","as"=>"loadEmployeeDetails"));
			Route::post('/postEmployeeExperiences/{id}',array('uses'=>'hr\hrController@postEmployeeExperiences','as'=>'postEmployeeExperiences'));
			Route::post('/postEmployeeTrainings/{id}',array('uses'=>'hr\hrController@postEmployeeTrainings','as'=>'postEmployeeTrainings'));
			Route::post('/postEmployeeSoldier/{id}',array('uses'=>'hr\hrController@postEmployeeSoldier','as'=>'postEmployeeSoldier'));
			Route::post("/evaluateEmployeeViaAjax",array("uses"=>"hr\hrController@evaluateEmployeeViaAjax","as"=>"evaluateEmployeeViaAjax"));
			Route::post("/punishEmployeeViaAjax",array("uses"=>"hr\hrController@punishEmployeeViaAjax","as"=>"punishEmployeeViaAjax"));
			Route::post("/makafatEmployeeViaAjax",array("uses"=>"hr\hrController@makafatEmployeeViaAjax","as"=>"makafatEmployeeViaAjax"));
			Route::get('/print_cv/{id}',array('uses'=>'hr\hrController@print_cv','as'=>'print_cv'));
			//communication
			Route::get('/getEmployeeCommunication', array('uses'=>'hr\hrController@getEmployeeCommunication','as'=>'getEmployeeCommunication'));
			Route::get('/getEmployeeComData', array('uses' => 'hr\hrController@getEmployeeComData', 'as' => 'getEmployeeComData'));
			Route::get('/getEmployeeComCardData', array('uses' => 'hr\hrController@getEmployeeComCardData', 'as' => 'getEmployeeComCardData'));
			Route::get('/getComEmplyeeMaktobs', array('uses' => 'hr\hrController@getComEmplyeeMaktobs', 'as' => 'getComEmplyeeMaktobs'));
			Route::post("/postComMaktob",array("uses"=>"hr\hrController@postComMaktob","as"=>"postComMaktob"));
			Route::get('/getComEmplyeeEstelams', array('uses' => 'hr\hrController@getComEmplyeeEstelams', 'as' => 'getComEmplyeeEstelams'));
			Route::post("/postComEstelam",array("uses"=>"hr\hrController@postComEstelam","as"=>"postComEstelam"));
			Route::get('/getComEmplyeeSecurity', array('uses' => 'hr\hrController@getComEmplyeeSecurity', 'as' => 'getComEmplyeeSecurity'));
			Route::post("/postComSecurity/{type}",array("uses"=>"hr\hrController@postComSecurity","as"=>"postComSecurity"));
			Route::post("/postComSecurityAccept",array("uses"=>"hr\hrController@postComSecurityAccept","as"=>"postComSecurityAccept"));
			Route::get('/getComEmplyeeProtection', array('uses' => 'hr\hrController@getComEmplyeeProtection', 'as' => 'getComEmplyeeProtection'));
			Route::get('/getComComplaints', array('uses' => 'hr\hrController@getComComplaints', 'as' => 'getComComplaints'));
			Route::get('/getComComitte', array('uses' => 'hr\hrController@getComComitte', 'as' => 'getComComitte'));
			Route::post("/postComComplain/{type}",array("uses"=>"hr\hrController@postComComplain","as"=>"postComComplain"));
			Route::post("/postComComitte",array("uses"=>"hr\hrController@postComComitte","as"=>"postComComitte"));
			Route::post("/postComComitteResult",array("uses"=>"hr\hrController@postComComitteResult","as"=>"postComComitteResult"));
			Route::post("/postComEmployeeCard",array("uses"=>"hr\hrController@postComEmployeeCard","as"=>"postComEmployeeCard"));
			//attendance
			Route::get('/getAttendance', array('uses'=>'hr\hrController@getAttendance','as'=>'getAttendance'));
			Route::post('/getRelatedEmployees', array('uses'=>'hr\hrController@getRelatedEmployees','as'=>'getRelatedEmployees'));
			Route::get('/getRelatedEmployees_get/{dep_id}/{sub}/{year}/{month}', array('uses'=>'hr\hrController@getRelatedEmployees_get','as'=>'getRelatedEmployees_get'));
			Route::get('/getEmployeeAttendance/{id}/{year}/{month}/{dep_id}/{sub_dep}/{in_att?}', array('uses'=>'hr\hrController@getEmployeeAttendance','as'=>'getEmployeeAttendance'));
			Route::get('/getEmployeeAttendanceCheck/{id}/{year}/{month}/{dep_id}/{sub_dep}/{in_att?}', array('uses'=>'hr\hrController@getEmployeeAttendanceCheck','as'=>'getEmployeeAttendanceCheck'));
			Route::get('/getDirEmployeeImages/{id}/{year}/{month}/{dep_id}/{sub_dep}', array('uses'=>'hr\hrController@getDirEmployee_images','as'=>'getDirEmployeeImages'));
			Route::post('/getDirEmployeeImages_ajax', array('uses'=>'hr\hrController@getDirEmployeeImages_ajax','as'=>'getDirEmployeeImages_ajax'));
			Route::get('/verifyNextEmployee/{rfid}/{year}/{month}/{type}/{dep}/{sub_dep}', array('uses'=>'hr\hrController@verifyNextEmployee','as'=>'verifyNextEmployee'));
			Route::get('/verifyNextEmployee_check/{rfid}/{year}/{month}/{type}/{dep}/{sub_dep}', array('uses'=>'hr\hrController@verifyNextEmployee_check','as'=>'verifyNextEmployee_check'));
			Route::get('/approveNextEmployee/{rfid}/{year}/{month}/{type}', array('uses'=>'hr\hrController@approveNextEmployee','as'=>'approveNextEmployee'));
			Route::post("/rejectImage",array("uses"=>"hr\hrController@rejectImage","as"=>"rejectImage"));
			Route::post("/acceptImage",array("uses"=>"hr\hrController@acceptImage","as"=>"acceptImage"));
			Route::post("/presentImage",array("uses"=>"hr\hrController@presentImage","as"=>"presentImage"));
			Route::get('/getAttendanceApproval/{id}/{year}/{month}', array('uses'=>'hr\hrController@getEmployeeAttendance','as'=>'getAttendanceApproval'));
			Route::post("/postLeaveDays",array("uses"=>"hr\hrController@postLeaveDays","as"=>"postLeaveDays"));
			Route::get('/getPayrollList/{dep_id}/{year}/{month}', array('uses'=>'hr\hrController@getPayrollList','as'=>'getPayrollList'));
			Route::get('/getAttCharts/{dep_id}/{year}/{month}', array('uses'=>'hr\hrController@getAttCharts','as'=>'getAttCharts'));
			Route::get('/printPayroll/{dep_id}/{year}/{month}', array('uses'=>'hr\hrController@printPayroll','as'=>'printPayroll'));
			Route::get('/printAttReport/{dep_id}/{year}/{month}', array('uses'=>'hr\hrController@printAttReport','as'=>'printAttReport'));
			Route::get('/printAttReport_aop/{dep_id}/{year}/{month}', array('uses'=>'hr\hrController@printAttReport_aop','as'=>'printAttReport_aop'));
			Route::get('/printAttReport_ajir/{dep_id}/{year}/{month}', array('uses'=>'hr\hrController@printAttReport_ajir','as'=>'printAttReport_ajir'));
			Route::get('/printExtraReport_ajir/{dep_id}/{year}/{month}', array('uses'=>'hr\hrController@printExtraReport_ajir','as'=>'printExtraReport_ajir'));
			Route::get('/printAbsentReport/{dep_id}/{year}/{month}', array('uses'=>'hr\hrController@printAbsentReport','as'=>'printAbsentReport'));
			Route::get('/printAbsentReport_ajir/{dep_id}/{year}/{month}', array('uses'=>'hr\hrController@printAbsentReport_ajir','as'=>'printAbsentReport_ajir'));
			Route::post('/printEmployees', array('uses'=>'hr\hrController@printEmployees','as'=>'printEmployees'));
			Route::post("/loadEmployeePayroll",array("uses"=>"hr\hrController@loadEmployeePayroll","as"=>"loadEmployeePayroll"));
			Route::post("/editPayroll",array("uses"=>"hr\hrController@editPayroll","as"=>"editPayroll"));
			Route::get('/leavesManager', array('uses'=>'hr\hrController@leavesManager','as'=>'leavesManager'));
			Route::get('/holidaysManager', array('uses'=>'hr\hrController@holidaysManager','as'=>'holidaysManager'));
			Route::post('/getLeavesEmployees', array('uses'=>'hr\hrController@getLeavesEmployees','as'=>'getLeavesEmployees'));
			Route::post("/postLeaves",array("uses"=>"hr\hrController@postLeaves","as"=>"postLeaves"));
			Route::post("/postLeaves_byUser",array("uses"=>"hr\hrController@postLeaves_byUser","as"=>"postLeaves_byUser"));
			Route::post("/updateLeaves",array("uses"=>"hr\hrController@updateLeaves","as"=>"updateLeaves"));
			Route::post("/postAttendanceTime",array("uses"=>"hr\hrController@postAttendanceTime","as"=>"postAttendanceTime"));
			Route::post("/postHolidays",array("uses"=>"hr\hrController@postHolidays","as"=>"postHolidays"));
			Route::post("/postEmergencyHolidays",array("uses"=>"hr\hrController@postEmergencyHolidays","as"=>"postEmergencyHolidays"));
			Route::post("/postEmployeeSalaryWait",array("uses"=>"hr\hrController@postEmployeeSalaryWait","as"=>"postEmployeeSalaryWait"));
			Route::get('/approveEmployeeAttendance/{id}/{year}/{month}', array('uses'=>'hr\hrController@approveEmployeeAttendance','as'=>'approveEmployeeAttendance'));
			Route::get('/getLeaveDoc/{id}',array('uses'=>'hr\hrController@downloadLeaveDoc','as'=>'getLeaveDoc'));
			Route::post('/removeLeaveFile', array('uses'=>'hr\hrController@deleteLeaveFile','as'=>'removeLeaveFile'));
			Route::get('/getEmployeeAttData/{dep}/{sub_dep}/{year}/{month}', array('uses' => 'hr\hrController@getEmployeeAttData', 'as' => 'getEmployeeAttData'));
			Route::get('/getTodayAttData/{dep}/{sub_dep}/{status?}/{type?}', array('uses' => 'hr\hrController@getTodayAttData', 'as' => 'getTodayAttData'));
			Route::get('/getLeavesEmployeesData/{dep}/{sub_dep}/{year}/{month}', array('uses' => 'hr\hrController@getLeavesEmployeesData', 'as' => 'getLeavesEmployeesData'));
			Route::post("/getEmployeeSalaryWait",array("uses"=>"hr\hrController@getEmployeeSalaryWait","as"=>"getEmployeeSalaryWait"));
			Route::post("/editPositionModal",array("uses"=>"hr\hrController@editPositionModal","as"=>"editPositionModal"));
			Route::post('/deleteEmployeeLeave', array('uses'=>'hr\hrController@deleteEmployeeLeave','as'=>'deleteEmployeeLeave'));
			Route::post('/postPayroll', array('uses'=>'hr\hrController@postPayroll','as'=>'postPayroll'));
			Route::post('/bringYearTime', array('uses'=>'hr\hrController@bringYearTime','as'=>'bringYearTime'));
			Route::post('/bringMonthHolidays', array('uses'=>'hr\hrController@bringMonthHolidays','as'=>'bringMonthHolidays'));
			Route::post('/bringMonthEmergencyHolidays', array('uses'=>'hr\hrController@bringMonthEmergencyHolidays','as'=>'bringMonthEmergencyHolidays'));
			Route::get('/printDailyAtt/{dep}/{sub}/{status?}/{type?}', array('uses' => 'hr\hrController@printDailyAtt', 'as' => 'printDailyAtt'));
			Route::get('/printDailyAttChart/{dep}', array('uses' => 'hr\hrController@printDailyAttChart', 'as' => 'printDailyAttChart'));
			Route::get('/putImagesToHr', array('uses'=>'hr\hrController@putImagesToHr','as'=>'putImagesToHr'));
			Route::get('/getAllPositions', array('uses'=>'hr\hrController@getAllPositions','as'=>'getAllPositions'));
			Route::get('/getAllPositionsData', array('uses'=>'hr\hrController@getAllPositionsData','as'=>'getAllPositionsData'));
			Route::post("/postEmployeePosition",array("uses"=>"hr\hrController@postEmployeePosition","as"=>"postEmployeePosition"));
			Route::get('/getAllInShifts', array('uses'=>'hr\hrController@getAllInShifts','as'=>'getAllInShifts'));
			Route::get('/getShiftEmployeesData/{dep}/{sub}/{status}/{gender}', array('uses'=>'hr\hrController@getShiftEmployeesData','as'=>'getShiftEmployeesData'));
			Route::post('/checkInsertImagesDate', array('uses'=>'hr\hrController@checkInsertImagesDate','as'=>'checkInsertImagesDate'));
			Route::post('/getAttendanceAjax', array('uses'=>'hr\hrController@getAttendanceAjax','as'=>'getAttendanceAjax'));
			Route::post('/getDirAttendance', array('uses'=>'hr\hrController@getDirAttendance','as'=>'getDirAttendance'));
			Route::post('/getMoreEmergencyDate', array('uses'=>'hr\hrController@getMoreEmergencyDate','as'=>'getMoreEmergencyDate'));
			Route::get('/getAllAttEmps', array('uses'=>'hr\hrController@getAllAttEmps','as'=>'getAllAttEmps'));
			Route::get('/getAtttEmployeesData/{dep}/{sub}/{gender}', array('uses'=>'hr\hrController@getAtttEmployeesData','as'=>'getAtttEmployeesData'));
			Route::get('/getAttEmployees/{dep}/{sub}/{gender}', array('uses' => 'hr\hrController@getAttEmployees', 'as' => 'getAttEmployees'));
			Route::get('/myAttendance/{id}/{year}/{month}', array('uses'=>'hr\hrController@getEmployeeAttendanceCheck_itself','as'=>'myAttendance'));
			Route::get('/printEmployeeLeaves/{id}/{year}/{month}', array('uses'=>'hr\hrController@getEmployeeLeaves_print','as'=>'printEmployeeLeaves'));
			Route::get('/printEmployeeLeaves_dep/{dep}/{subdep}/{year}/{month}', array('uses'=>'hr\hrController@getEmployeeLeavesDep_print','as'=>'printEmployeeLeaves_dep'));
			Route::get('/view_leaveForm', array('uses'=>'hr\hrController@view_leaveForm','as'=>'view_leaveForm'));
			Route::get('/downloadLeaveForm', array('uses'=>'hr\hrController@downloadLeaveForm','as'=>'downloadLeaveForm'));
			Route::get('/download_Form/{name}', array('uses'=>'hr\hrController@downloadForm','as'=>'download_Form'));
			Route::get('/leaveForm_add/{notification_id?}', array('uses'=>'hr\hrController@leaveFormAdd','as'=>'leaveForm_add'));
			//tashkil
			Route::get('/getTashkils/{year?}', array('uses'=>'hr\hrController@getTashkils','as'=>'getTashkils'));
			Route::get('/getTashkilData/{year}', array('uses' => 'hr\hrController@getTashkilData', 'as' => 'getTashkilData'));
			Route::post("/postNewBast",array("uses"=>"hr\hrController@postNewBast","as"=>"postNewBast"));
			Route::post("/bringTashkilDetViaAjax",array("uses"=>"hr\hrController@bringTashkilDetViaAjax","as"=>"bringTashkilDetViaAjax"));
			Route::post("/load_tashkil_det",array("uses"=>"hr\hrController@load_tashkil_det","as"=>"load_tashkil_det"));
			Route::post("/updateBast",array("uses"=>"hr\hrController@updateBast","as"=>"updateBast"));
			Route::post('/deleteTashkil', array('uses'=>'hr\hrController@deleteTashkil','as'=>'deleteTashkil'));
			Route::post('/deleteEmployeePromotion', array('uses'=>'hr\hrController@deleteEmployeePromotion','as'=>'deleteEmployeePromotion'));
			Route::post('/deleteEmployeeMakafat', array('uses'=>'hr\hrController@deleteEmployeeMakafat','as'=>'deleteEmployeeMakafat'));
			Route::post('/deleteEmployeePunish', array('uses'=>'hr\hrController@deleteEmployeePunish','as'=>'deleteEmployeePunish'));
			Route::get('/searchTashkil', array('uses'=>'hr\hrController@searchTashkil','as'=>'searchTashkil'));
			Route::post('/getTashkilSearch', array('uses'=>'hr\hrController@getTashkilSearch','as'=>'getTashkilSearch'));
			Route::get('/getTashkilSearchData/{dep?}/{sub_dep?}/{type?}/{bast?}/{title?}/{emp_type?}/{year}', array('uses' => 'hr\hrController@getTashkilSearchData', 'as' => 'getTashkilSearchData'));
			//capacity
			Route::get('/getCapacityBuilding', array('uses'=>'hr\hrController@getCapacityBuilding','as'=>'getCapacityBuilding'));
			Route::get('/getEmployeeCapacityData', array('uses' => 'hr\hrController@getEmployeeCapacityData', 'as' => 'getEmployeeCapacityData'));
			Route::post("/loadCapacityTrainings",array("uses"=>"hr\hrController@loadCapacityTrainings","as"=>"loadCapacityTrainings"));
			Route::get('/getCapacityTrainingsData', array('uses' => 'hr\hrController@getCapacityTrainingsData', 'as' => 'getCapacityTrainingsData'));
			Route::get('/getNewEmployeeCapacityData', array('uses' => 'hr\hrController@getNewEmployeeCapacityData', 'as' => 'getNewEmployeeCapacityData'));
			Route::post("/employee_oriented",array("uses"=>"hr\hrController@employee_oriented","as"=>"employee_oriented"));
			Route::post("/check_training_validity",array("uses"=>"hr\hrController@check_training_validity","as"=>"check_training_validity"));
			Route::post("/postNewTraining",array("uses"=>"hr\hrController@postNewTraining","as"=>"postNewTraining"));
			Route::post("/editTraining",array("uses"=>"hr\hrController@editTraining","as"=>"editTraining"));
			Route::get('/getCapacityTrainersData', array('uses' => 'hr\hrController@getCapacityTrainersData', 'as' => 'getCapacityTrainersData'));
			Route::post("/postNewTrainer",array("uses"=>"hr\hrController@postNewTrainer","as"=>"postNewTrainer"));
			Route::post("/load_employee_trainings",array("uses"=>"hr\hrController@load_employee_trainings","as"=>"load_employee_trainings"));
			Route::post("/load_training",array("uses"=>"hr\hrController@load_training","as"=>"load_training"));
			Route::post("/load_training_emps",array("uses"=>"hr\hrController@load_training_emps","as"=>"load_training_emps"));
			Route::get("/printTrainings",array("uses"=>"hr\hrController@printTrainings","as"=>"printTrainings"));
			Route::post('/bringRelatedBastViaAjax', array('uses'=>'hr\hrController@bringRelatedBastViaAjax','as'=>'bringRelatedBastViaAjax'));

			Route::post("/addTrainingToEmployee",array("uses"=>"hr\hrController@addTrainingToEmployee","as"=>"addTrainingToEmployee"));
			Route::get('/getTodayAttendances/{dep}/{sub}/{status?}/{type?}', array('uses' => 'hr\hrController@getTodayAttendances', 'as' => 'getTodayAttendances'));
			Route::post("/insertAttendanceImages_today",array("uses"=>"hr\hrController@insertImages_today","as"=>"insertAttendanceImages_today"));
			//test
			Route::get('/getAllAttendances/{year}/{month}/{dep}/{sub}', array('uses' => 'hr\hrController@getAllAttendances', 'as' => 'getAllAttendances'));
			Route::get('/get_cards', array('uses' => 'hr\hrController@insert_cards', 'as' => 'get_cards'));
			Route::post("/check_leave_validity",array("uses"=>"hr\hrController@check_leave_validity","as"=>"check_leave_validity"));
			Route::post("/check_leave_validity_date",array("uses"=>"hr\hrController@check_leave_validity_date","as"=>"check_leave_validity_date"));
			Route::post("/check_leave_validity_byUser",array("uses"=>"hr\hrController@check_leave_validity_byUser","as"=>"check_leave_validity_byUser"));
			Route::post("/process_leave_dir",array("uses"=>"hr\hrController@processLeaveDir","as"=>"process_leave_dir"));
			Route::post("/getSearchResult",array("uses"=>"hr\hrController@getSearchResult","as"=>"getSearchResult"));
			Route::post("/getDirSearchResult",array("uses"=>"hr\hrController@getDirSearchResult","as"=>"getDirSearchResult"));
			Route::post("/sendAbsent_emails",array("uses"=>"hr\hrController@sendAbsent_emails","as"=>"sendAbsent_emails"));
			Route::post("/sendAfternoon_emails",array("uses"=>"hr\hrController@sendAfternoon_emails","as"=>"sendAfternoon_emails"));
			Route::get('/getShiftEmployees/{dep}/{sub}/{status}/{gender}', array('uses' => 'hr\hrController@getShiftEmployees', 'as' => 'getShiftEmployees'));
	    	Route::get('/printShiftEmployees/{dep}/{sub}/{status?}/{gender?}', array('uses' => 'hr\hrController@printShiftEmployees', 'as' => 'printShiftEmployees'));
	    	Route::get('/notifyManager', array('uses' => 'hr\hrController@notifyManager', 'as' => 'notifyManager'));
	    	Route::post('/saveNewNotify', array('uses' => 'hr\hrController@saveNewNotify', 'as' => 'saveNewNotify'));
	    	Route::post('/editNotifyModal', array('uses' => 'hr\hrController@editNotifyModal', 'as' => 'editNotifyModal'));
	    	Route::post('/publishNotify', array('uses' => 'hr\hrController@publishNotify', 'as' => 'publishNotify'));
	    	Route::post('/postNotifyEdit', array('uses' => 'hr\hrController@postNotifyEdit', 'as' => 'postNotifyEdit'));
	    	//secretariat
	    	Route::get('/getAllSecretariatDocs', array('uses' => 'hr\hrController@printShiftEmployees', 'as' => 'getAllSecretariatDocs'));
	    	Route::post('/releaseCard', array('uses' => 'hr\hrController@releaseCard', 'as' => 'releaseCard'));
	    	Route::get('/test_email', array('uses' => 'hr\hrController@test_email', 'as' => 'test_email'));


	    }
	);
});
// --- OCS-HR route section -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'ocshr'), function()
	    {
	    	Route::get('/hrHomePage', array('uses'=>'ocshr\hrController@homePage','as'=>'hrHomePage-ocs'));
	    }
	);
});
// --- International Communication Database Travel Info route section -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'icom'), function()
	    {
	    	// Departure routes.
	    	Route::get('/getLoadDepList', array('uses'=>'icom\icomController@depList','as'=>'getLoadDepList'));
	    	Route::get('/getAllDeps', array('uses'=>'icom\icomController@getAllDepartures','as'=>'getAllDeps'));
	    	Route::get('/addDep', array('uses'=>'icom\icomController@addDepForm','as'=>'addDep'));
	    	Route::get('/depEdit/{id}', array('uses'=>'icom\icomController@editDeparture','as'=>'depEdit'));
	    	Route::get('/depDelete/{id}', array('uses'=>'icom\icomController@postDeleteDeparture','as'=>'postDepDelete'));
	    	Route::get('/getLoadFilterPage', array('uses'=>'icom\icomController@loadFilterPage','as'=>'getLoadFilterPage'));
	    	Route::get('/getLastWeekDeps', array('uses'=>'icom\icomController@getLastWeekDepartures','as'=>'getLastWeekDeps'));
	    	Route::get('/getFilteredDepsData', array('uses'=>'icom\icomController@getFilteredDeparturesData','as'=>'getFilteredDepsData'));
			// Return routes.
			Route::get('/getReturnList', array('uses'=>'icom\icomController@returnList','as'=>'returnList'));
	    	Route::get('/getAllReturns', array('uses'=>'icom\icomController@getReturns','as'=>'getAllReturns'));
	    	Route::get('/getLoadRetFilterPage', array('uses'=>'icom\icomController@loadReturnFilterPage','as'=>'getLoadRetFilterPage'));
	    	Route::get('/getLastWeekReturns', array('uses'=>'icom\icomController@getLastWeekReturns','as'=>'getLastWeekReturns'));
	    	Route::get('/getFilteredReturnsData', array('uses'=>'icom\icomController@getFilteredReturnsData','as'=>'getFilteredReturnsData'));
	    	Route::get('/getApprovedReturns', array('uses'=>'icom\icomController@loadApprovedReturnForm','as'=>'getApprovedReturns'));
	    	Route::get('/getApprovedReturnsData', array('uses'=>'icom\icomController@getApprovedReturnsData','as'=>'getApprovedReturnsData'));
	    	// Airport check up routes
	    	Route::get('/getLoadAirportPage', array('uses'=>'icom\icomController@loadAirportCheckupPage','as'=>'getLoadAirportPage'));
			Route::get('/getTravelsListForAirport', array('uses'=>'icom\icomController@getTravelsList','as'=>'getTravelsListForAirport'));
	    	Route::get('/getLoadCommentPage/{id}', array('uses'=>'icom\icomController@loadCommentPage','as'=>'getLoadCommentPage'));
	    	Route::get('/viewComment/{id}', array('uses'=>'icom\icomController@viewDepComment','as'=>'viewComment'));
	    	Route::get('/getIComDownload/{id}',array('uses'=>'icom\icomController@downloadIComFile','as'=>'getIComDownload'));
	    	// Syncing servers routes.
	    	Route::get('/syncServers', array('uses'=>'icom\syncController@syncServers','as'=>'getSyncServers'));

			Route::group(array('before' => 'csrf'), function(){
	    		Route::post('/postAddDep', array('uses'=>'icom\icomController@addDeparture','as'=>'postAddDepIcom'));
	    		Route::post('/postDepEdit/{id}', array('uses'=>'icom\icomController@postEditDeparture','as'=>'postDepEdit'));
	    		Route::post('/getFilteredDeps', array('uses'=>'icom\icomController@getFilteredDepartures','as'=>'getFilteredDeps'));
	    		Route::post('/getFilteredReturns', array('uses'=>'icom\icomController@getFilteredReturns','as'=>'getFilteredReturns'));
	    		Route::post('/getCreateComment/{id}', array('uses'=>'icom\icomController@addComments','as'=>'getCreateComment'));
	    		Route::post('/getApproveReturn', array('uses'=>'icom\icomController@approveReturn','as'=>'getApproveReturn'));

	    	});

		}
	);
});

// --- Procurement office database routes -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'procurement'), function()
	    {

	    	// Goods Purchase routes.
	    	Route::get('/procurementHomePage', array('uses'=>'procurement\procurementController@homePage','as'=>'procurementHomePage'));
			Route::get('/getLoadGoodsPurchaseList', array('uses'=>'procurement\procurementController@loadPurchaseList','as'=>'goodsPurchaseList'));
			Route::get('/getAllPurchases', array('uses'=>'procurement\procurementController@getAllPurchases','as'=>'getAllPurchases'));
			Route::get('/getLoadPurchaseForm', array('uses'=>'procurement\procurementController@loadPurchaseForm','as'=>'getLoadPurchaseForm'));
			Route::get('/getLoadPurchaseEditForm/{id}', array('uses'=>'procurement\procurementController@loadPurchaseEditForm','as'=>'getLoadPurchaseEditForm'));
	    	Route::get('/purchaseDelete/{id}', array('uses'=>'procurement\procurementController@postDeletePurchase','as'=>'postPurchaseDelete'));
			Route::get('/getLoadPurchaseDetails/{id}', array('uses'=>'procurement\procurementController@loadPurchaseDetails','as'=>'loadPurchaseDetails'));
			Route::get('/getSearchPurchases/{purchasing_team}', array('uses'=>'procurement\procurementController@getSearchedPurchases','as'=>'getSearchPurchases'));
			// Procurement Plan routes.
			Route::get('/planList', array('uses'=>'procurement\procurementController@loadProcurementPlanList','as'=>'procurementPlanList'));
			Route::get('/getAllProcurementPlan', array('uses'=>'procurement\procurementController@getAllProcurementPlan','as'=>'getAllProcurementPlan'));
			Route::get('/loadProcurementPlanForm', array('uses'=>'procurement\procurementController@loadProcurementPlanForm','as'=>'getLoadProcurementPlanForm'));
			Route::get('/procurementPlanDetails/{id}', array('uses'=>'procurement\procurementController@viewProcurementPlanDetails','as'=>'loadProcurementPlanDetails'));
			Route::get('/getLoadProcurementPlanEditForm/{id}', array('uses'=>'procurement\procurementController@loadProcurementPlanEditForm','as'=>'getLoadProcurementPlanEditForm'));
			Route::get('/procurementPlanDelete/{id}', array('uses'=>'procurement\procurementController@postDeleteProcurementPlan','as'=>'postProcurementPlanDelete'));
			// Procurement or supply routes.
			Route::get('/getLoadProcurementList', array('uses'=>'procurement\procurementController@loadProcurementList','as'=>'procurementList'));
			Route::get('/getAllProcurement', array('uses'=>'procurement\procurementController@getAllProcurement','as'=>'getAllProcurement'));
			Route::get('/getLoadProcurementForm/{id}', array('uses'=>'procurement\procurementController@loadProcurementForm','as'=>'getLoadProcurementForm'));
			Route::get('/getLoadProcurementEditForm/{id}', array('uses'=>'procurement\procurementController@loadProcurementEditForm','as'=>'getLoadProcurementEditForm'));
			Route::get('/procurementDelete/{id}', array('uses'=>'procurement\procurementController@postDeleteProcurement','as'=>'postProcurementDelete'));
			Route::get('/getLoadProcurementDetails/{id}', array('uses'=>'procurement\procurementController@loadProcurementDetails','as'=>'loadProcurementDetails'));
			// Contract routes.
	    	Route::get('/getLoadContractList', array('uses'=>'procurement\procurementController@loadContractList','as'=>'contractsList'));
			Route::get('/getContracts', array('uses'=>'procurement\procurementController@getAllContracts','as'=>'getContracts'));
			Route::get('/getLoadContractForm/{id}', array('uses'=>'procurement\procurementController@loadContractForm','as'=>'getLoadContractForm'));
			Route::get('/getLoadContractEditForm/{id}/{field?}/{main_id}', array('uses'=>'procurement\procurementController@loadContractEditForm','as'=>'getLoadContractEditForm'));
			Route::get('/getLoadContractDetails/{id}/{field?}', array('uses'=>'procurement\procurementController@loadContractDetails','as'=>'loadContractDetails'));
			Route::get('/getDeleteContract/{id}/{field?}', array('uses'=>'procurement\procurementController@postDeleteContract','as'=>'deleteContract'));
			Route::get('/getLoadArchivedContractList', array('uses'=>'procurement\procurementController@loadArchivedContractList','as'=>'archivedContractList'));
			Route::get('/getAllArchivedContracts', array('uses'=>'procurement\procurementController@getArchivedContractData','as'=>'archivedContractData'));
			Route::group(array('before' => 'csrf'), function(){
	    		Route::post('/postAddPurchase', array('uses'=>'procurement\procurementController@addPurchaseFormData','as'=>'postAddPurchase'));
	    		Route::post('/postAddProcurement', array('uses'=>'procurement\procurementController@addProcurementFormData','as'=>'postAddProcurement'));
	    		Route::post('/addProcurementPlan', array('uses'=>'procurement\procurementController@addProcurementPlanFormData','as'=>'postAddProcurementPlan'));
	    		Route::post('/postAddContract', array('uses'=>'procurement\procurementController@addContractFormData','as'=>'postAddContract'));
	    		Route::post('/postEditPurchase/{id}', array('uses'=>'procurement\procurementController@postEditPurchase','as'=>'postEditPurchase'));
	    		Route::post('/postEditProcurement/{id}', array('uses'=>'procurement\procurementController@postEditProcurement','as'=>'postEditProcurement'));
	    		Route::post('/postEditProcurementPlan/{id}', array('uses'=>'procurement\procurementController@postEditProcurementPlan','as'=>'postEditProcurementPlan'));
	    		Route::post('/postEditContract/{id}', array('uses'=>'procurement\procurementController@postEditContract','as'=>'postEditContract'));
				Route::post('/changeDate', array('uses'=>'procurement\procurementController@changeDate','as'=>'changeDate'));
				Route::post('/loadPurchaseSearchView', array('uses'=>'procurement\procurementController@loadSearchedPurchaseList','as'=>'loadPurchaseSearchView'));
	    		Route::post('/getContractArchived', array('uses'=>'procurement\procurementController@postArchiveContract','as'=>'archiveContract'));
	    	});

		}
	);
});

// --- Case management database routes -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'casemgmt'), function()
	    {

	    	// Case management routes.
			Route::get('/getLoadCaseForm', array('uses'=>'casemgmt\caseController@loadCaseForm','as'=>'caseForm'));
			Route::get('/getCaseList', array('uses'=>'casemgmt\caseController@loadCaseList','as'=>'caseList'));
			Route::get('/getCaseListData', array('uses'=>'casemgmt\caseController@getAllCases','as'=>'caseListData'));
			Route::get('/getLoadCaseEditForm/{id}', array('uses'=>'casemgmt\caseController@loadCaseEditForm','as'=>'caseEditForm'));
			Route::get('/viewCaseDetails/{id}/{notify?}', array('uses'=>'casemgmt\caseController@viewCaseDetails','as'=>'viewCase'));
			Route::get('/getDeleteCase/{id}', array('uses'=>'casemgmt\caseController@deleteCase','as'=>'getDeleteCase'));
			Route::get('/getCaseProgressList/{id}', array('uses'=>'casemgmt\caseController@getCaseProgressData','as'=>'progressList'));
			Route::get('/getCaseDownload/{id}/{field_name}',array('uses'=>'casemgmt\caseController@downloadCaseFile','as'=>'getCaseDownload'));
			// Case Notification routes.
			Route::get('/loadCaseMainPage', array('uses'=>'casemgmt\caseController@loadMainPage','as'=>'loadCaseMainPage'));
			Route::get('/logsListData', array('uses'=>'casemgmt\caseController@getLogsList','as'=>'logsListData'));
			// Case management records routes.
			Route::get('/getRequestDepManagementForm', array('uses'=>'casemgmt\caseController@loadDeptPersonMgmtForm','as'=>'manageRecords'));
			Route::get('/getAgencyManagementForm', array('uses'=>'casemgmt\caseController@loadAgencyManagementForm','as'=>'manageAgencies'));
			Route::get('/getCaseDepts', array('uses'=>'casemgmt\caseController@getCaseDepartments','as'=>'getCaseDepts'));
			Route::get('/getDeps', array('uses'=>'casemgmt\caseController@getDepartments','as'=>'getCaseDeps'));
			Route::get('/getPersons', array('uses'=>'casemgmt\caseController@getPersons','as'=>'getCasePersons'));
			Route::get('/getAgencyListData', array('uses'=>'casemgmt\caseController@getAgencyListData','as'=>'agencyListData'));
			Route::get('/caseEditRequestAndDep/{dept_id?}/{person_id?}', array('uses'=>'casemgmt\caseController@caseEditRequestAndDep','as'=>'caseEditRequestAndDep'));
			Route::get('/caseEditAgency/{id}', array('uses'=>'casemgmt\caseController@caseEditAgencyForm','as'=>'caseEditAgency'));
			Route::get('/getDeletePersonAndDept/{dept_id?}/{person_id?}', array('uses'=>'casemgmt\caseController@deletePersonAndDept','as'=>'getDeleteRequestDep'));
			Route::get('/getDeleteAgency/{id}', array('uses'=>'casemgmt\caseController@deleteAgency','as'=>'getDeleteAgency'));
			// Task Execution Order routes.
			Route::get('/getLoadExecOrderForm', array('uses'=>'casemgmt\caseController@loadExecOrderForm','as'=>'execOrderForm'));
			Route::get('/getLoadExecOrderList', array('uses'=>'casemgmt\caseController@loadCaseExecOrderList','as'=>'execOrderList'));
			Route::get('/getExecOrderData', array('uses'=>'casemgmt\caseController@getExecOrderData','as'=>'getExecOrderData'));
			Route::get('/editExecOrder/{id}', array('uses'=>'casemgmt\caseController@loadExecOrderEditForm','as'=>'editExecOrder'));
			Route::get('/deleteExecOrder/{id}', array('uses'=>'casemgmt\caseController@deleteExecOrder','as'=>'deleteExecOrder'));
			// Case Notification routes.
			Route::get('/loadNotification', array('uses'=>'casemgmt\caseController@loadCaseNotification','as'=>'caseNotification'));
			Route::get('/loadNotificationList', array('uses'=>'casemgmt\caseController@loadCaseNotificationList','as'=>'loadNotificationList'));
			Route::get('/notificationListData', array('uses'=>'casemgmt\caseController@notificationListData','as'=>'notificationListData'));
			Route::get('/getDeleteCaseNotification/{id}', array('uses'=>'casemgmt\caseController@deleteCaseNotification','as'=>'getDeleteCaseNotification'));
			//route which shows the details of the record sent from log table.
			Route::get('/notificationDetails/{action_table}/{record_id?}', array('uses'=>'casemgmt\caseController@viewNotificationDetails','as'=>'notificationDetails'));
			Route::get('/agencyDetails/{record_id}', array('uses'=>'casemgmt\caseController@viewLogAgencyDetails','as'=>'logAgencyDetails'));

			Route::group(array('before' => 'csrf'), function(){
	    		Route::post('/postAddCaseData', array('uses'=>'casemgmt\caseController@addCaseFormData','as'=>'postAddCaseData'));
	    		Route::post('/postEditCase/{id}', array('uses'=>'casemgmt\caseController@postEditCaseForm','as'=>'postEditCase'));
	    		Route::post('/postAddCaseDept', array('uses'=>'casemgmt\caseController@addCaseDeptPerson','as'=>'postAddCaseDept'));
	    		Route::post('/postAddCasePerson', array('uses'=>'casemgmt\caseController@addCasePerson','as'=>'postAddCasePerson'));
	    		Route::post('/postAddAgency', array('uses'=>'casemgmt\caseController@addAgencyData','as'=>'postAddAgency'));
	    		Route::post('/postEditRequestAndDep/{id}', array('uses'=>'casemgmt\caseController@postEditRequestAndDep','as'=>'postEditRequestAndDep'));
	    		Route::post('/postEditAgency/{id}', array('uses'=>'casemgmt\caseController@postEditCaseAgency','as'=>'postEditCaseAgency'));
	    		Route::post('/postUpdateTaskProgress/{id}', array('uses'=>'casemgmt\caseController@postUpdateTaskProgress','as'=>'postUpdateTaskProgress'));
				Route::post('/getAutocompleteList/{table}/{field}', array('uses'=>'casemgmt\caseController@getAutoCompleteListData','as'=>'getAutocompleteList'));
				Route::post('/addTaskDescRevisions', array('uses'=>'casemgmt\caseController@addTaskDesc','as'=>'addTaskDescRevisions'));
				Route::post('/getRelatedCaseDepartment', array('uses'=>'casemgmt\caseController@getRelatedCaseDepartment','as'=>'getRelatedCaseDepartment'));
				Route::post('/postAddExec', array('uses'=>'casemgmt\caseController@postAddExec','as'=>'postAddExec'));
				Route::post('/postCaseExecEdit/{id}', array('uses'=>'casemgmt\caseController@postEditExec','as'=>'postEditExec'));
	    		Route::post('/postAddCaseNotification', array('uses'=>'casemgmt\caseController@postAddCaseNotification','as'=>'postAddCaseNotification'));
				Route::post('/getApproveFinishedTask', array('uses'=>'casemgmt\caseController@getApproveFinishedTask','as'=>'getApproveFinishedTask'));
	    	});

		}
	);
});
//Case routes without authentication.
//Route::get('caseAgencies', array('uses'=>'casemgmt\caseAgencies@getCaseAgencies','as'=>'getCaseAgencies'));
Route::get('/getCaseAgencies',array('uses'=>'casemgmt\agenciesController@getCaseAgencies','as'=>'getCaseAgencies'));
Route::get('/execOrderList', array('uses'=>'casemgmt\executiveOrder@loadExecOrderView','as'=>'execOrderList'));
Route::get('/getExecOrderData', array('uses'=>'casemgmt\executiveOrder@execOrderData','as'=>'getExecOrderData'));
Route::get('/taskDetails/{id}', array('uses'=>'casemgmt\executiveOrder@taskDetailsForPresident','as'=>'caseDetails'));
Route::get('/getCaseProgressList/{id}', array('uses'=>'casemgmt\executiveOrder@getCaseProgressData','as'=>'progressList'));
Route::get('/getCaseDownload/{id}/{file_name}',array('uses'=>'casemgmt\executiveOrder@downloadCaseFile','as'=>'getCaseDownload'));
Route::get('/getCaseProgressDownload/{id}',array('uses'=>'casemgmt\executiveOrder@downloadCaseProgressFile','as'=>'getCaseProgressDownload'));
Route::post('/postAddCase', array('uses'=>'casemgmt\executiveOrder@addCaseSentData','as'=>'postAddCase'));
//case notification routes without authentication.
Route::post('/postUpdateCaseNotification', array('uses'=>'casemgmt\executiveOrder@postUpdateCaseNotification','as'=>'postUpdateCaseNotification'));
Route::get('/notificationList', array('uses'=>'casemgmt\executiveOrder@getNotificationList','as'=>'notificationList'));


// --- Inventory database routes -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'inventory'), function()
	    {
	    	// Inventory database routes.
			Route::get('/getLoadInventoryForm', array('uses'=>'inventory\inventoryController@loadInventoryForm','as'=>'inventoryForm'));
			Route::get('/getInventoryList', array('uses'=>'inventory\inventoryController@loadInventoryList','as'=>'inventoryList'));
			//Route::get('/getInventoryListData', array('uses'=>'inventory\inventoryController@getAllInventories','as'=>'inventoryListData'));
			Route::get('/getLoadInventoryEditForm/{id}', array('uses'=>'inventory\inventoryController@loadInventoryEditForm','as'=>'inventoryEditForm'));
			//Route::get('/getSearchedInventory/{product_type}', array('uses'=>'inventory\inventoryController@getSearchedInventoryData','as'=>'searchedInventoryData'));

			Route::group(array('before' => 'csrf'), function(){
	    		Route::post('/postAddInventoryData', array('uses'=>'inventory\inventoryController@addInventoryFormData','as'=>'postAddInventoryData'));
	    		Route::post('/getLoadSearchedInventories', array('uses'=>'inventory\inventoryController@loadSearchedInventories','as'=>'loadSearchedInventories'));
	    		Route::post('/getStockInByProductType', array('uses'=>'inventory\inventoryController@getStockInByProductType','as'=>'getStockInByProductType'));
				Route::post('/postEditInventoryData/{id}', array('uses'=>'inventory\inventoryController@postEditInventoryForm','as'=>'postEditInventoryData'));
				Route::post('/getDeleteInventory', array('uses'=>'inventory\inventoryController@deleteInventory','as'=>'getDeleteInventory'));
				Route::post('/getAutocompleteList/{table}/{field}', array('uses'=>'inventory\inventoryController@getAutoCompleteListData','as'=>'getInventoryAutocompleteList'));
			});

		}
	);
});

// --- Procurement Inventory database routes -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'procurement_inventory'), function()
	    {
	    	// Inventory database routes.
			Route::get('/getLoadProcInventoryForm', array('uses'=>'procurement_inventory\inventoryController@loadInventoryForm','as'=>'procInventoryForm'));
			Route::get('/getProcInventoryList', array('uses'=>'procurement_inventory\inventoryController@loadInventoryList','as'=>'procInventoryList'));
			//Route::get('/getInventoryListData', array('uses'=>'procurement_inventory\inventoryController@getAllInventories','as'=>'inventoryListData'));
			Route::get('/getLoadProcInventoryEditForm/{id}', array('uses'=>'procurement_inventory\inventoryController@loadInventoryEditForm','as'=>'procInventoryEditForm'));
			//Route::get('/getSearchedInventory/{product_type}', array('uses'=>'procurement_inventory\inventoryController@getSearchedInventoryData','as'=>'searchedInventoryData'));

			Route::group(array('before' => 'csrf'), function(){
	    		Route::post('/postAddProcInventoryData', array('uses'=>'procurement_inventory\inventoryController@addInventoryFormData','as'=>'postAddProcInventoryData'));
	    		Route::post('/searchedInventoryPage', array('uses'=>'procurement_inventory\inventoryController@loadSearchedInventories','as'=>'loadSearchedProcInventories'));
	    		Route::post('/getProcInvStockInByProductType', array('uses'=>'procurement_inventory\inventoryController@getStockInByProductType','as'=>'getProcInvStockInByProductType'));
				Route::post('/postEditProcInventoryData/{id}', array('uses'=>'procurement_inventory\inventoryController@postEditInventoryForm','as'=>'postEditProcInventoryData'));
				Route::post('/getDeleteProcInventory', array('uses'=>'procurement_inventory\inventoryController@deleteInventory','as'=>'getDeleteProcInventory'));
				Route::post('/getAutocompleteList/{table}/{field}', array('uses'=>'procurement_inventory\inventoryController@getAutoCompleteListData','as'=>'getProcInventoryAutocompleteList'));
				Route::post('/InventoryDetails', array('uses'=>'procurement_inventory\inventoryController@inventoryDetails','as'=>'getProcInventoryDetails'));
			});

		}
	);
});

// --- End of Inventory database routes -------------------- //
// --- Contacts route section -------------------- //

Route::get('/contacts/arg.net', array('uses'=>'contacts\contactsController@getAllContacts','as'=>'arg.net'));
Route::get('/contacts/ContsList', array('uses'=>'contacts\contactsController@getContactsData','as'=>'contactsList'));
Route::get('/contacts/loadEdit/{id}', array('uses'=>'contacts\contactsController@loadEditPage','as'=>'loadEdit'));
Route::post('/updateForm/{id}', array('uses'=>'contacts\contactsController@updateContacts','as'=>'postUpdateForm'));
Route::post('/bringContactsRelatedSubDepartment', array('uses'=>'contacts\contactsController@bringSubDepartments','as'=>'bringContactsRelatedSubDepartment'));
Route::post('/contacts/getContactsSearchResult', array('uses'=>'contacts\contactsController@getSearchResult','as'=>'getContactsSearchResult'));
Route::get('/contacts/ResultData', array('uses'=>'contacts\contactsController@getSearchResultData','as'=>'ResultData'));

// --- audit route section -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'audit'), function()
	    {
	    	//reports
			Route::get('/getAuditReports', array('uses'=>'audit\audit@getAuditReports','as'=>'getAuditReports'));
			Route::get('/getAduitRecommendations', array('uses'=>'audit\audit@getAduitRecommendations','as'=>'getAduitRecommendations'));
			Route::get('/getAuditManage', array('uses'=>'audit\audit@getAuditManage','as'=>'getAuditManage'));
			Route::get('/getProgress', array('uses'=>'audit\audit@getProgress','as'=>'getProgress'));
			Route::get('/getLibraries', array('uses'=>'audit\audit@getLibraries','as'=>'getLibraries'));
			Route::get('/addNewReport',array('uses'=>'audit\audit@addNewReport','as'=>'add_new_report'));
			Route::post('/postAuditReport',array('uses'=>'audit\audit@post_audit_report','as'=>'postAuditReport'));
			Route::get('/getAuditReportsData/{type}', array('uses' => 'audit\audit@getAuditReportsData', 'as' => 'getAuditReportsData'));
			Route::get('/editAuditReport/{id}',array('uses'=>'audit\audit@editAuditReport','as'=>'editAuditReport'));
			Route::get('/getReportDownload/{id}',array('uses'=>'audit\audit@downloadDoc','as'=>'getReportDownload'));
			Route::post('/removeReportFile', array('uses'=>'audit\audit@deleteFile','as'=>'removeReportFile'));
			Route::post('/postAuditReportEdit/{id}',array('uses'=>'audit\audit@postAuditReportEdit','as'=>'postAuditReportEdit'));
			Route::post('/removeReport',array('uses'=>'audit\audit@removeReport','as'=>'removeReport'));
			Route::post("/loadUnplanedReports",array("uses"=>"audit\audit@loadUnplanedReports","as"=>"loadUnplanedReports"));
			Route::post("/search_report",array("uses"=>"audit\audit@search_report","as"=>"search_report"));
			//tracking
			Route::get('/addNewRecommendation',array('uses'=>'audit\audit@addNewRecommendation','as'=>'addNewRecommendation'));
			Route::get('/getRecommandationsData', array('uses' => 'audit\audit@getRecommandationsData', 'as' => 'getRecommandationsData'));
			Route::post('/postRecommandation',array('uses'=>'audit\audit@postRecommandation','as'=>'postRecommandation'));
			Route::get('/editRecomandation/{id}',array('uses'=>'audit\audit@editRecomandation','as'=>'editRecomandation'));
			Route::post('/postRecommandationEdit/{id}',array('uses'=>'audit\audit@postRecommandationEdit','as'=>'postRecommandationEdit'));
			Route::get('/downloadRecommandationDoc/{id}',array('uses'=>'audit\audit@downloadRecommandationDoc','as'=>'downloadRecommandationDoc'));
			Route::post('/removeRecommandationFile', array('uses'=>'audit\audit@removeRecommandationFile','as'=>'removeRecommandationFile'));
			Route::post('/removeRecommandation',array('uses'=>'audit\audit@removeRecommandation','as'=>'removeRecommandation'));
			Route::post("/loadMoneyTracking",array("uses"=>"audit\audit@loadMoneyTracking","as"=>"loadMoneyTracking"));
			Route::get('/getMoneyData', array('uses' => 'audit\audit@getMoneyData', 'as' => 'getMoneyData'));
			Route::get('/addNewMoneyNote',array('uses'=>'audit\audit@addNewMoneyNote','as'=>'addNewMoneyNote'));
			Route::post('/postMoneyNote',array('uses'=>'audit\audit@postMoneyNote','as'=>'postMoneyNote'));
			Route::get('/editMoneyNote/{id}',array('uses'=>'audit\audit@editMoneyNote','as'=>'editMoneyNote'));
			Route::post('/postMoneyNoteEdit/{id}',array('uses'=>'audit\audit@postMoneyNoteEdit','as'=>'postMoneyNoteEdit'));
			Route::post('/removeMoneyNote',array('uses'=>'audit\audit@removeMoneyNote','as'=>'removeMoneyNote'));
			Route::get('/paybackMoneyNote/{id}',array('uses'=>'audit\audit@paybackMoneyNote','as'=>'paybackMoneyNote'));
			Route::post('/addMoneyPayment',array('uses'=>'audit\audit@addMoneyPayment','as'=>'addMoneyPayment'));
			Route::post('/load_payment_edit',array('uses'=>'audit\audit@load_payment_edit','as'=>'load_payment_edit'));
			Route::post('/editMoneyPayment/{id}',array('uses'=>'audit\audit@editMoneyPayment','as'=>'editMoneyPayment'));
			Route::post('/removePayment',array('uses'=>'audit\audit@removePayment','as'=>'removePayment'));
			Route::post('/ImplementedRecommandation',array('uses'=>'audit\audit@ImplementedRecommandation','as'=>'ImplementedRecommandation'));
			Route::post("/loadFindings",array("uses"=>"audit\audit@loadFindings","as"=>"loadFindings"));
			Route::get('/getFindingsData', array('uses' => 'audit\audit@getFindingsData', 'as' => 'getFindingsData'));
			Route::get('/addNewFinding',array('uses'=>'audit\audit@addNewFinding','as'=>'addNewFinding'));
			Route::post('/postFinding',array('uses'=>'audit\audit@postFinding','as'=>'postFinding'));
			Route::post('/postEditFinding/{id}',array('uses'=>'audit\audit@postEditFinding','as'=>'postEditFinding'));
			Route::get('/editFinding/{id}',array('uses'=>'audit\audit@editFinding','as'=>'editFinding'));
			Route::post("/getMoreFinding",array("uses"=>"audit\audit@getMoreFinding","as"=>"getMoreFinding"));
			Route::post("/getMoreSubFinding",array("uses"=>"audit\audit@getMoreSubFinding","as"=>"getMoreSubFinding"));
			Route::post('/removeFinding',array('uses'=>'audit\audit@removeFinding','as'=>'removeFinding'));
			Route::post("/getMoreRecommendation",array("uses"=>"audit\audit@getMoreRecommendation","as"=>"getMoreRecommendation"));
			Route::post("/loadRecommandations",array("uses"=>"audit\audit@loadRecommandations","as"=>"loadRecommandations"));
			Route::post("/loadRecommendationModal",array("uses"=>"audit\audit@loadRecommendationModal","as"=>"loadRecommendationModal"));
			Route::post("/loadRecommendationItemsModal",array("uses"=>"audit\audit@loadRecommendationItemsModal","as"=>"loadRecommendationItemsModal"));
			Route::post("/bringDepEmployees",array("uses"=>"audit\audit@bringDepEmployees","as"=>"bringDepEmployees"));
			Route::post("/getInvestigationDet",array("uses"=>"audit\audit@getInvestigationDet","as"=>"getInvestigationDet"));
			//other attachments
			Route::get('/getAllAttachments',array('uses'=>'audit\audit@getAllAttachments','as'=>'getAllAttachments'));
			Route::get('/getAllAttachmentsData',array('uses'=>'audit\audit@getAllAttachmentsData','as'=>'getAllAttachmentsData'));
			Route::get('/attach_new_file',array('uses'=>'audit\audit@attach_new_file','as'=>'attach_new_file'));
			Route::post('/postAttachment',array('uses'=>'audit\audit@post_new_file','as'=>'postAttachment'));

	    }
	);
});
// --- evaluation route section -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'evaluation'), function()
	    {
	    	Route::get('/evaluationHomePage/{year?}', array('uses'=>'evaluation\evaluation@homePage','as'=>'evaluationHomePage'));
	    	Route::get('/getAllReceivedDocs/{year?}',array('uses'=>'evaluation\evaluation@getAllReceivedDocs','as'=>'getAllReceivedDocs'));
	    	Route::post('/getAllReceivedDocs_post',array('uses'=>'evaluation\evaluation@getAllReceivedDocs_post','as'=>'getAllReceivedDocs_post'));
	    	Route::get('/getAllDocEmployees/{year?}',array('uses'=>'evaluation\evaluation@getAllDocEmployees','as'=>'getAllDocEmployees'));
	    	Route::post('/getAllDocEmployees_post',array('uses'=>'evaluation\evaluation@getAllDocEmployees_post','as'=>'getAllDocEmployees_post'));
	    	Route::get('/getAllDocs/{year?}/{type?}/{no?}', array('uses'=>'evaluation\evaluation@getAllDocs','as'=>'getAllDocs'));
	    	Route::get('/getAllEmployeesData/{year?}/{type?}/{item?}', array('uses'=>'evaluation\evaluation@getAllEmployeesData','as'=>'getAllEmployeesData'));
	    	Route::get('/addNewDoc',array('uses'=>'evaluation\evaluation@addNewDoc','as'=>'addNewDoc'));
	    	Route::post('/postDoc',array('uses'=>'evaluation\evaluation@postDoc','as'=>'postDoc'));
	    	Route::post('/postEditDoc',array('uses'=>'evaluation\evaluation@postEditDoc','as'=>'postEditDoc'));
	    	Route::post('/postDocViaAjax',array('uses'=>'evaluation\evaluation@postDocViaAjax','as'=>'postDocViaAjax'));
	    	Route::post('/postEditEmployee',array('uses'=>'evaluation\evaluation@postEditEmployee','as'=>'postEditEmployee'));
	    	Route::get('/editDoc/{id}',array('uses'=>'evaluation\evaluation@edit_doc','as'=>'edit_Doc'));
	    	Route::get('/depEmpEdit/{id}',array('uses'=>'evaluation\evaluation@depEmpEdit','as'=>'depEmpEdit'));
	    	Route::get('/docFullView/{id}/{emp_id}',array('uses'=>'evaluation\evaluation@docFullView','as'=>'docFullView'));
	    	Route::get('/viewDoc/{id}',array('uses'=>'evaluation\evaluation@viewDoc','as'=>'viewDoc'));
	    	Route::get('/loadRejectDoc/{id}',array('uses'=>'evaluation\evaluation@loadRejectDoc','as'=>'loadRejectDoc'));
	    	Route::post('/postRejectDoc',array('uses'=>'evaluation\evaluation@postRejectDoc','as'=>'postRejectDoc'));
	    	Route::post('/postRejectedDoc',array('uses'=>'evaluation\evaluation@postRejectedDoc','as'=>'postRejectedDoc'));
	    	Route::post('/unRejectDoc',array('uses'=>'evaluation\evaluation@unRejectDoc','as'=>'unRejectDoc'));
	    	Route::get('/approve_doc/{id}',array('uses'=>'evaluation\evaluation@approveDoc','as'=>'approve_doc'));
	    	Route::get('/postDocDelete/{id}',array('uses'=>'evaluation\evaluation@postDocDelete','as'=>'postDocDelete'));
	    	Route::get('/postEmployeeDelete/{id}',array('uses'=>'evaluation\evaluation@postEmployeeDelete','as'=>'postEmployeeDelete'));
	    	Route::post('/getMoreEmployees',array('uses'=>'evaluation\evaluation@getMoreEmployees','as'=>'getMoreEmployees'));
	    	Route::post('/bring_items',array('uses'=>'evaluation\evaluation@bring_items','as'=>'bring_items'));
			Route::post('/postApproveDoc',array('uses'=>'evaluation\evaluation@postApproveDoc','as'=>'postApproveDoc'));
			Route::post('/postApprovedDoc',array('uses'=>'evaluation\evaluation@postApprovedDoc','as'=>'postApprovedDoc'));
			Route::get('/getSearch',array('uses'=>'evaluation\evaluation@getSearch','as'=>'getSearch'));
			Route::post('/getEvaluationSearch', array('uses'=>'evaluation\evaluation@getEvaluationSearch','as'=>'getEvaluationSearch'));
			Route::post('/getEvaluationSearch_ajax', array('uses'=>'evaluation\evaluation@getEvaluationSearch_ajax','as'=>'getEvaluationSearch_ajax'));
	    	Route::get('/downloadDoc/{id}',array('uses'=>'evaluation\evaluation@downloadDoc','as'=>'downloadDoc'));
	    	Route::get('/downloadRejectedDoc/{id}',array('uses'=>'evaluation\evaluation@downloadRejectedDoc','as'=>'downloadRejectedDoc'));
	    	Route::get('/downloadDocument/{id}',array('uses'=>'evaluation\evaluation@downloadDocument','as'=>'downloadDocument'));
	    	Route::post('/removeFile', array('uses'=>'evaluation\evaluation@removeFile','as'=>'removeFile'));
	    	Route::post('/unApproveEmp',array('uses'=>'evaluation\evaluation@unApproveEmp','as'=>'unApproveEmp'));
	    	Route::post('/getEvaluationExcel',array('uses'=>'evaluation\evaluation@getEvaluationExcel','as'=>'getEvaluationExcel'));
	    	Route::post('/search_docs',array('uses'=>'evaluation\evaluation@search_docs','as'=>'search_docs'));
	    }
	);
});

// --- Construction and Maintenance database route section -------------------//
Route::group(array("before"=>"auth"),function()
{
	Route::group(
	    array('prefix' => 'const_maintenance'), function()
	    {
	    	Route::get('/getLoadEstateRegistryForm', array('uses'=>'const_maintenance\constMaintenanceController@loadEstateRegistryForm','as'=>'getEstateRegistryForm'));
	    	Route::get('/getListOfEstateRegistry', array('uses'=>'const_maintenance\constMaintenanceController@loadEstateRegistryList','as'=>'getEstateRegistryList'));
	    	Route::get('/loadEstateRegistryEditPage/{id}', array('uses'=>'const_maintenance\constMaintenanceController@loadEditPage','as'=>'getLoadEstateRegistryEditPage'));
	    	// Post routes
	    	Route::group(array('before' => 'csrf'), function(){
	    		Route::post('/postAddEstateRegistryData', array('uses'=>'const_maintenance\constMaintenanceController@addEstateRegistryFormData','as'=>'addEstateRegistry'));
	    		Route::post('/estateRegistryDetails', array('uses'=>'const_maintenance\constMaintenanceController@getDetails','as'=>'getEstateRegistryDetails'));
	    		Route::post('/getSearchEstateRegistry', array('uses'=>'const_maintenance\constMaintenanceController@searchRecords','as'=>'searchEstateRegistry'));
	    		Route::post('/editEstateRegistry/{id}', array('uses'=>'const_maintenance\constMaintenanceController@updateEstateRegistryRecord','as'=>'postEditEstateRegistry'));
	    		Route::post('/deleteEstateRegistry', array('uses'=>'const_maintenance\constMaintenanceController@deleteEstateRegistry','as'=>'postDeleteEstateRegistry'));
			});
	    }
	);
});

//=========================================== Routes of library management system ==========================================================

	Route::group(array("before" => "guest"), function(){
		Route::get("/user/login",array("uses"=>"LoginController@getLogin","as"=>"getLogin"));

		Route::get("/library/load_page",array("uses"=>"library\libraryController@loadlibraryList","as"=>"libraryList"));
    //pagination route
    Route::get("library/get_paginationData",array("uses"=>"library\libraryController@get_paginationData","as"=>"pagination_route"));
    //...........wasi..........
    //highSearch
    Route::get("/library/insert",array("uses"=>"library\libraryController@insert_rows","as"=>"insertLibraryData"));
    Route::get("/library/high_Search",array("uses"=>"library\libraryController@highSearch","as"=>"high_Search"));
    //add data
    Route::post("/library/insert_row",array("uses"=>"library\libraryController@create_row","as"=>"insert_row"));
    //delete rows
    Route::get("/library/deleterow/{id}",array("uses"=>"library\libraryController@delete_row","as"=>"DeleteData"));
    //select row for update
    Route::get("/library/selectrow/{id}",array("uses"=>"library\libraryController@select_id","as"=>"selectupdate"));
    //update
    Route::post("/library/createupdate/{id}",array("uses"=>"library\libraryController@update","as"=>"updaterow"));

    });
	    Route::group(array('before' => 'csrf'), function(){
	    Route::post("/library/searchpage",array("uses"=>"library\libraryController@searchfromtable","as"=>"searchLibraryData "));
	    Route::post("/library/searchSerial",array("uses"=>"library\libraryController@last_serial_num","as"=>"searchSerial"));
	});
	Route::group(array('before' => 'csrf'), function(){
    Route::post("/library/getLibraryData", array('uses' => 'library\libraryController@select_search',"as"=>"getLibraryData"));

    Route::post("/user/login",array("uses"=>"LoginController@postLogin","as"=>"postLogin"));
    Route::get("/user/logout",array("uses"=>"UserController@getLogout","as"=>"getLogout"));
    });
    //excel export and distribute book

    Route::post("/library/export_book",array('uses' =>'library\libraryController@exportBook' ,"as"=>"export_book" ));
    // distribute books
    Route::get("/library/distribute_book",array("uses"=>"library\DistributeController@distributeBook","as"=>"distribute_book"));
    //save customer
    Route::post("/library/insert_customer",array('uses' =>'library\DistributeController@insertCustomer' ,"as"=>"insert_customer" ));
    Route::get("/library/select_customer_row/{id?}",array('uses' =>'library\DistributeController@selectCustomerRow' ,"as"=>"select_customer_row" ));
    Route::post("/library/update_customer/{id?}",array('uses' =>'library\DistributeController@updateCustomer' ,"as"=>"update_customer" ));
    //distribute books
    Route::get("/library/distribute_form",array("uses"=>"library\DistributeController@distributeForm","as"=>"distribute_form"));
    Route::post("/library/distribute_insert",array('uses' =>'library\DistributeController@distributeInsert' ,"as"=>"distribute_insert" ));
    Route::get("/library/distribute_select_row/{id?}",array('uses' =>'library\DistributeController@distributeRow' ,"as"=>"distribute_select_row" ));
    Route::post("/library/distribute_update_row/{id?}",array('uses' =>'library\DistributeController@distributeRowUpdate' ,"as"=>"distribute_update_row" ));

    Route::post("/library/return_date",array('uses' =>'library\DistributeController@returnDate' ,"as"=>"return_date" ));
    Route::get("/library/get_book_type/{id?}",array('uses' =>'library\DistributeController@bookType' ,"as"=>"get_book_type" ));

//=========================================================== End of Library Management System ============================================================

//=========================================== Routes of Specification Database system ==========================================================
Route::group(array("before"=>"auth"),function()
{
    Route::get("/specification/specificationList",array("uses"=>"specification\mainController@loadRecordsList","as"=>"recordsList"));
    Route::get("/specification/insertForm",array("uses"=>"specification\mainController@loadpage","as"=>"insertForm"));

    Route::post("/specification/insert_fe",array("uses"=>"specification\mainController@insert_f_c_9","as"=>"insertData"));
    Route::get("/specification/deleterow/{id}",array("uses"=>"specification\mainController@delete","as"=>"deleterow"));
    Route::get("/specification/select_data/{id}",array("uses"=>"specification\mainController@select_row","as"=>"selectData"));
    Route::post("/specification/update_row/{id}",array("uses"=>"specification\mainController@update","as"=>"update_row"));
    Route::get("/specification/show_feceno/{id}",array("uses"=>"specification\mainController@show_feceno_file","as"=>"show_feceno"));

    //route of products
    Route::get("/specification/insert_product",array("uses"=>"specification\productController@insert_page","as"=>"insert_pro"));
    Route::post("/specification/insert_data_p",array("uses"=>"specification\productController@insert_row","as"=>"insert_data_p"));
    Route::get("/specification/delete_row/{id}",array("uses"=>"specification\productController@delete","as"=>"delete_row"));
    Route::get("/specification/select_row/{id}",array("uses"=>"specification\productController@selectRow","as"=>"select_row"));
    Route::post("/specification/updatedata/{id}",array("uses"=>"specification\productController@update","as"=>"update_product"));

    //all search route
    Route::get("/specification/search_page",array("uses"=>"specification\allSearchContorller@search_data","as"=>"search_page"));


     //status routs
    Route::get("/specification/status/{id}",array("uses"=>"specification\status_controller@show_form","as"=>"show_status"));
    Route::post("/specification/insert/{id}",array("uses"=>"specification\status_controller@update_status","as"=>"insert_status"));
    //update
    Route::get("/specification/status_update/{id}",array("uses"=>"specification\status_controller@reject_accept","as"=>"reject_accept"));
    Route::post("/specification/update_status_re/{id}",array("uses"=>"specification\status_controller@update_status_re","as"=>"update_status_re"));


    //print data route
    Route::post("/specification/print",array("uses"=>"specification\print_controller@select_data","as"=>"print_search"));
    Route::post("/specification/get_report", array('uses' => 'specification\allSearchContorller@speci_report',"as"=>"get_report"));
    //reports route
    Route::post("/specification/export_report",array("uses"=>"specification\allSearchContorller@export_report","as"=>"export_report"));

    //Equipment routs give product for employee
    Route::get("/specification/equipment_page",array("uses"=>"specification\main_equipment_controller@load_equipment","as"=>"load_equipment"));
    Route::get("/specification/load_insert",array("uses"=>"specification\main_equipment_controller@distribute_equipment","as"=>"load_insert"));
    Route::post("/specification/insertData_Eq",array("uses"=>"specification\main_equipment_controller@insert_rows","as"=>"insert_data"));
    Route::get("/specification/deleted_destribut_eq/{id}",array("uses"=>"specification\main_equipment_controller@delete_destribut","as"=>"delete_destribut"));
    Route::get("/specification/select_dest/{id}",array("uses"=>"specification\main_equipment_controller@select_row_destribut","as"=>"select_dest"));
    Route::post("/specification/update_destribut/{id}",array("uses"=>"specification\main_equipment_controller@updateDastribut","as"=>"update_destribut"));
    //insert equipment name and categories

     Route::get("/specification/load",array("uses"=>"specification\categories_equipmentController@load_equpment_name_page","as"=>"load_eq_name"));
     Route::get("/specification/loadpage_eq",array("uses"=>"specification\categories_equipmentController@load_inset_page","as"=>"insert_equipment"));
     Route::post("/specification/insert_eq_cat",array("uses"=>"specification\categories_equipmentController@insert_equipment","as"=>"insert_equ"));
     Route::get("/specification/deleted/{id}",array("uses"=>"specification\categories_equipmentController@deleterow","as"=>"delete_eq"));
     Route::get("/specification/selectrow/{id}",array("uses"=>"specification\categories_equipmentController@select_row_eq","as"=>"select_update"));
     Route::post("/specification/update_equipment/{id}",array("uses"=>"specification\categories_equipmentController@updateEquipment","as"=>"update_equipment"));
     Route::post("/specification/insert_range",array("uses"=>"specification\mainController@dataRange","as"=>"insert_range"));
     //all search of equipment

    Route::get("/specification/search_equipment",array("uses"=>"specification\allSearchEquipmentController@search_data","as"=>"search_form"));

     //export routs
     // feceno and product route
     Route::post("/specification/export_excel",array("uses"=>"specification\allSearchContorller@export_ex","as"=>"export_excel_spec"));
     Route::post("/specification/search_fe_re",array("uses"=>"specification\searchController@search_feceno_reject","as"=>"searchData_reject"));
     //equipment analyse exports route
     Route::post("/specification/export_data",array("uses"=>"specification\allSearchEquipmentController@export_eq","as"=>"export_data"));
     //reports route
    Route::post("/specification/export_report",array("uses"=>"specification\allSearchContorller@export_report","as"=>"export_report"));
    //export feceno to excel
    Route::post("/specification/feceno_excel",array("uses"=>"specification\allSearchContorller@export_feceno","as"=>"feceno_excel"));

     //categories routs

     Route::get("/specification/load",array("uses"=>"specification\categories_equipmentController@load_insert_categores","as"=>"load_cat"));
     Route::post("/specification/insertdata",array("uses"=>"specification\categories_equipmentController@insert_categories","as"=>"insert_cat"));
     Route::get("/specification/deletedCat/{id}",array("uses"=>"specification\categories_equipmentController@deletecat","as"=>"delete_cat"));
     Route::get("/specification/selectCat/{id}",array("uses"=>"specification\categories_equipmentController@updateCat","as"=>"update_cat"));
     Route::post("/specification/update/{id}",array("uses"=>"specification\categories_equipmentController@update_Cat","as"=>"updateCat"));
});
Route::group(array('before' => 'csrf'), function(){

	//live search routs
	Route::post("/specification/get_feceno", array('uses' => 'specification\allSearchContorller@feceno_search',"as"=>"getdata"));
	Route::post("/specification/searchProduct", array('uses' => 'specification\allSearchContorller@search_product',"as"=>"getdata2"));
	Route::post("/specification/search_approve",array("uses"=>"specification\searchController@search_approve","as"=>"searchapprove"));
	Route::post("/specification/se_reject",array("uses"=>"specification\searchController@search_reject","as"=>"searchreject"));
	Route::post("/specification/search_fe",array("uses"=>"specification\searchController@search_feceno","as"=>"searchData"));
	Route::post("/specification/search",array("uses"=>"specification\searchController@searchProduct","as"=>"search_product"));
        //equipment search controlller
        Route::post("/specification/search_des",array("uses"=>"specification\search_Equipment@search_destribut_equipment","as"=>"search_des_equipment"));
        Route::post("/specification/search_EQ",array("uses"=>"specification\search_Equipment@search_Equipment","as"=>"search_equipment"));
        Route::post("/specification/search_EQ_given",array("uses"=>"specification\search_Equipment@search_given_eq","as"=>"search_given_equipment"));
        Route::post("/specification/search_EQ_delete",array("uses"=>"specification\search_Equipment@search_delete","as"=>"search_deleted"));
        // all search of equipment
         Route::post("/specification/get_data", array('uses' => 'specification\allSearchEquipmentController@search_eq_data',"as"=>"get_eq_search_data"));
	// select data for print
          Route::post("/specification/get_data_print", array('uses' => 'specification\allSearchEquipmentController@search_eq_data_print',"as"=>"get_eq_search_data_pr"));
          Route::post("/specification/print_em_pro", array('uses' => 'specification\allSearchEquipmentController@print_empolyes_product',"as"=>"print_em_pro"));
					// department crud
					Route::post("/specification/insert_speci_department", array('uses' => 'specification\mainController@insertDepartment',"as"=>"insert_speci_department"));
});

//=========================================================== End of Specification Database System ============================================================

//=========================================== Routes of Executive Management Database system ==========================================================

	//Routs of HR managemnt system
    	Route::group(array("before" => "guest"), function(){
		Route::get("/executive_management/load_ex",array("uses"=>"executive_management\mainController_doc@loadRecordsList","as"=>"recordsList_executive"));
    		Route::get("/executive_management/insertForm_executive",array("uses"=>"executive_management\mainController_doc@loadpage_insert","as"=>"insertForm_executive"));
    		//ajax get data
    		Route::get("/executive_management/get_hakom",array("uses"=>"executive_management\mainController_doc@get_hakom","as"=>"get_hakom_data"));
    		Route::get("/executive_management/get_maktob",array("uses"=>"executive_management\mainController_doc@get_maktob","as"=>"get_maktob_data"));
     		Route::get("/executive_management/get_copy_som",array("uses"=>"executive_management\mainController_doc@get_copy_som","as"=>"get_copy_som_data"));
    		Route::get("/executive_management/get_mateb_warada",array("uses"=>"executive_management\mainController_doc@get_mateb_warada","as"=>"get_mateb_warada_data"));
    		Route::get("/executive_management/get_peshnahad",array("uses"=>"executive_management\mainController_doc@get_pashnahad","as"=>"get_pashnehad_data"));
    		Route::get("/executive_management/get_farman",array("uses"=>"executive_management\mainController_doc@get_farman","as"=>"get_farman_data"));
    		Route::get("/executive_management/get_istalam",array("uses"=>"executive_management\mainController_doc@get_istalam","as"=>"get_istalam_data"));
    		Route::get("/executive_management/get_feceno",array("uses"=>"executive_management\mainController_doc@get_feceno","as"=>"get_feceno_data"));
    		Route::get("/executive_management/get_maktob_he",array("uses"=>"executive_management\mainController_doc@get_maktob_hedayati","as"=>"get_maktob_hadayati_data"));
    		Route::get("/executive_management/get_pashnahad_he",array("uses"=>"executive_management\mainController_doc@get_pahshnahad_hedayati","as"=>"get_pashnahad_hadayati_data"));
    		Route::get("/executive_management/get_feceno_hadayati",array("uses"=>"executive_management\mainController_doc@get_feceno_hadayati","as"=>"get_feceno_hadayati_data"));
      		Route::get("/executive_management/get_istalam_hadayati",array("uses"=>"executive_management\mainController_doc@get_istalam_hadayati_data","as"=>"get_istalam_hadayati"));
      		Route::get("/executive_management/get_tayenat",array("uses"=>"executive_management\mainController_doc@get_tayenat","as"=>"get_tayenat_data"));
      		Route::get("/executive_management/search",array("uses"=>"executive_management\mainController_doc@get_all_search","as"=>"all_data_search"));
    		Route::post("/executive_management/insert_document",array("uses"=>"executive_management\mainController_doc@insert_doc","as"=>"insertData_doc"));
    		Route::get("/executive_management/deleterow_doc/{id}",array("uses"=>"executive_management\mainController_doc@delete","as"=>"deleterow_doc"));
    		Route::get("/executive_management/select_data_doc/{id}",array("uses"=>"executive_management\mainController_doc@select_row_doc","as"=>"selectData"));
    		Route::post("/executive_management/update_doc/{id}",array("uses"=>"executive_management\mainController_doc@update","as"=>"update_doc"));
    		Route::get("/executive_management/show_file/{id}",array("uses"=>"executive_management\mainController_doc@show_file_toprint","as"=>"show_file"));
		Route::get("/executive_management/get_waraq_darkhasti_data",array("uses"=>"executive_management\mainController_doc@get_waraqa_darkhasti","as"=>"get_waraq_darkhasti_data"));
		Route::get("/executive_management/search_yearly",array("uses"=>"executive_management\mainController_doc@get_yearly_search","as"=>"search_yearly"));
		Route::get('/executive_management/load_waraqa_darkhasti_insert',array("uses"=>"executive_management\waraq_darkhasti_controller@load_insert","as"=>"load_waraq_darkhasti"));
        	Route::post('/executive_management/insert_waraq_darkhasti', array("uses" =>"executive_management\waraq_darkhasti_controller@insert_waraq_darkhasti" ,"as"=>"insert_waraq_darkhasti" ));
       		//hadayat insert changes route
     		Route::get("/executive_management/insertForm_doc",array("uses"=>"executive_management\hadayat_mainController@loadpage_insert_hadayat","as"=>"insertForm_hadayat_executive"));
     		Route::post('/executive_management/insert_Data_hadayat', array("uses" =>"executive_management\hadayat_mainController@insert_hadayat" ,"as"=>"insert_hadayat_data" ));
     		Route::get("/executive_management/deleterow_doc_hadayat/{id}",array("uses"=>"executive_management\hadayat_mainController@delete_hadayat","as"=>"deleterow_doc_hadayat"));
     		Route::get("/executive_management/select_data_doc_hadayat/{id}",array("uses"=>"executive_management\hadayat_mainController@select_row_doc_hadayat","as"=>"select_data_doc_hadayat"));
     		Route::post("/executive_management/update_hadayat_doc/{id}",array("uses"=>"executive_management\hadayat_mainController@update_hadayat","as"=>"update_hadayat_doc"));
     		//makatab warada routes
     		Route::get('/executive_management/load_mkatob_wara_insert',array("uses"=>"executive_management\makatab_warda_controller@load_insert","as"=>"insert_makatab_warda"));
     		Route::post('/executive_management/insert_makata_warada', array("uses" =>"executive_management\makatab_warda_controller@insert_maktob_warada" ,"as"=>"insert_makatab_data" ));
     		Route::get("/executive_management/delete_warada_maktob/{id}",array("uses"=>"executive_management\makatab_warda_controller@delete_warda_doc","as"=>"delete_warada_maktob"));
     		Route::get("/executive_management/select_maktob_warda_row/{id}",array("uses"=>"executive_management\makatab_warda_controller@select_row_doc_maktob_wrda","as"=>"select_maktob_warda_row"));
     		Route::post("/executive_management/update_matob_warada/{id}",array("uses"=>"executive_management\makatab_warda_controller@update_warda_maktob","as"=>"update_matob_warada"));
     		Route::post("/executive_management/get_warada_maktob", array('uses' => 'executive_management\search_doc_controller@search_warada_maktobs',"as"=>"search_warada_maktob"));
		Route::post("/executive_management/get_yearly_doc", array('uses' => 'executive_management\search_doc_controller@get_all_yearly_document',"as"=>"search_all_yearly"));
		Route::post("/executive_management/get_waraqa_darkhasti", array('uses' => 'executive_management\search_doc_controller@search_live_waraq_darkhasti',"as"=>"search_live_waraqa_darkhasti"));
		Route::get("/executive_management/delete_waraq_darkhasti/{id}",array("uses"=>"executive_management\waraq_darkhasti_controller@delete_waraq_darkhasti_doc","as"=>"delete_waraq_darkhasti"));
		Route::get("/executive_management/select_waraq_darkhasti_row/{id}",array("uses"=>"executive_management\waraq_darkhasti_controller@select_row_doc_waraq_darkhasti","as"=>"select_waraq_darkhasti_row"));
        	Route::post("/executive_management/update_waraq_darkhasti/{id}",array("uses"=>"executive_management\waraq_darkhasti_controller@update_waraq_darkhasti","as"=>"update_waraq_darkhasti"));
					//export to excel
		      Route::post("/executive_management/export_excel_doc",array("uses"=>"executive_management\search_doc_controller@export_excel","as"=>"export_excel"));
					Route::post("/executive_management/insert_executive_department", array('uses' => 'executive_management\mainController_doc@insertDepartment',"as"=>"insert_executive_department"));
	});


	Route::group(array('before' => 'csrf'), function(){

		//live search routs
        	Route::post("/executive_management/get_document", array('uses' => 'executive_management\search_doc_controller@search_live',"as"=>"search_live"));
		Route::post("/executive_management/get_feceno", array('uses' => 'executive_management\search_doc_controller@search_live_feceno',"as"=>"search_live_feceno"));
		//hadayat search route
		Route::post("/executive_management/get_hadayat", array('uses' => 'executive_management\search_doc_controller@search_live_hadayat',"as"=>"search_live_hadayat"));
        	//hight search
        	Route::post("/executive_management/get_all_doc", array('uses' => 'executive_management\search_doc_controller@get_all_document',"as"=>"search_all"));
        	//search warada maktob
         	Route::post("/executive_management/get_warada_maktob", array('uses' => 'executive_management\search_doc_controller@search_warada_maktobs',"as"=>"search_warada_maktob"));
		//delete image
         	Route::post("/executive_management/delete_img", array('uses' => 'executive_management\mainController_doc@delete_images',"as"=>"delete_image"));

	});

//=========================================================== End of Executive Management Database System ============================================================

//=========================================== Routes of M&E Document Tracking Database system ==========================================================
	Route::group(array("before"=>"auth"),function()
	{
		// M&E Document Tracking database routes.
		Route::get('/document_tracking/addDocument/', array('uses'=>'document_tracking\docsController@getIncomingDocForm','as'=>'loadIncomingDocForm'));
		Route::get('/document_tracking/getIncomingDocuments/', array('uses'=>'document_tracking\docsController@getIncomingDocs','as'=>'getIncomingDocsList'));
		Route::get('/document_tracking/editIncomingDocument/{id}', array('uses'=>'document_tracking\docsController@loadIncomingEditPage','as'=>'editIncomingDoc'));
		Route::get('/document_tracking/downloadFile/{id}', array('uses'=>'document_tracking\docsController@downloadDocFile','as'=>'getDownloadDocFile'));
		// Issuing and saving documents routes
		Route::get('/document_tracking/issuingForm/', array('uses'=>'document_tracking\docsController@loadIssueForm','as'=>'issueForm'));
		Route::get('/document_tracking/issuingFormWithoutIncoming/', array('uses'=>'document_tracking\docsController@loadIssueFormWithoutIncoming','as'=>'issueFormWithoutIncoming'));
		Route::get('/document_tracking/issueDocument/', array('uses'=>'document_tracking\docsController@getLoadDocIssuePage','as'=>'issueDoc'));
		Route::get('/document_tracking/saveDocument/{id}', array('uses'=>'document_tracking\docsController@getLoadDocSavePage','as'=>'saveDoc'));
		Route::get('/document_tracking/issueDocumentsList/', array('uses'=>'document_tracking\docsController@loadIssuedDocList','as'=>'issuedDocList'));
		Route::get('/document_tracking/issueDocumentsListWithoutIncoming/', array('uses'=>'document_tracking\docsController@loadIssuedDocListWithoutIncoming','as'=>'issuedDocListWithoutIncoming'));
		Route::get('/document_tracking/editSavedDocument/{id}', array('uses'=>'document_tracking\docsController@loadSavedDocEditPage','as'=>'editSavedDoc'));
		Route::get('/document_tracking/editIssuedDocument/{id}', array('uses'=>'document_tracking\docsController@loadIssuedDocEditPage','as'=>'editIssuedDoc'));
		Route::get('/document_tracking/editIssuedDocumentWithoutIncoming/{id}', array('uses'=>'document_tracking\docsController@loadIssuedDocWithoutIncomingEditPage','as'=>'editIssuedDocWithoutIncoming'));
		Route::post('/document_tracking/editIssuedDocumentWithoutIncoming/{id}', array('uses'=>'document_tracking\docsController@postUpdateIssuedDocWithoutIncoming','as'=>'postEditIssuedDocWithoutIncoming'));
		// Routes of Document Advanced Search.
		Route::get('/document_tracking/advancedSearch/{id}', array('uses'=>'document_tracking\docsController@loadDocAdvancedSearchPage','as'=>'advancedSearchForm'));
		// Document Tracking Dashboard routes.
		Route::get('/document_tracking/documentsDashbaord/{id}', array('uses'=>'document_tracking\docsController@loadMEDashboard','as'=>'docsDashboardPage'));

	});
	Route::group(array('before' => 'csrf'), function(){
		Route::post('/document_tracking/addNewRecord', array('uses'=>'document_tracking\docsController@addIncomingDoc','as'=>'saveIncomingDoc'));
		Route::post('/document_tracking/addIssuedDocument', array('uses'=>'document_tracking\docsController@addIssuedDoc','as'=>'insertIssuedDoc'));
		Route::post('/document_tracking/addIssuedDocumentWithoutIncoming', array('uses'=>'document_tracking\docsController@addIssuedDocWithoutIncoming','as'=>'insertIssuedDocWithoutIncoming'));
		Route::post('/document_tracking/addSavedDocument', array('uses'=>'document_tracking\docsController@addSavedDoc','as'=>'insertSavedDoc'));
		Route::post('/document_tracking/deleteIssuedOrSavedDocument', array('uses'=>'document_tracking\docsController@getDeleteIssuedDocuments','as'=>'deleteIssuedSavedDocs'));
		Route::post('/document_tracking/deleteIncomingDocument', array('uses'=>'document_tracking\docsController@getDeleteIncomingDocument','as'=>'deleteIncomingDoc'));
		Route::post("/document_tracking/updateDocument/{id}",array("uses"=>"document_tracking\docsController@updateDoc","as"=>"postUpdateDoc"));
		Route::post("/document_tracking/removeDocument",array("uses"=>"document_tracking\docsController@removeDocumentFile","as"=>"getRemoveDocFile"));
		Route::post("/document_tracking/searchedIncomingDocuments",array("uses"=>"document_tracking\docsController@getSearchIncomingDocs","as"=>"searchIncomingDocs"));
		Route::post("/document_tracking/searchedIssuedDocuments",array("uses"=>"document_tracking\docsController@getSearchIssuedDocs","as"=>"searchIssuedDocs"));
		Route::post("/document_tracking/searchedIssuedDocumentsWithoutIncoming",array("uses"=>"document_tracking\docsController@getSearchIssuedDocsWithoutIncoming","as"=>"searchIssuedDocsWithoutIncoming"));
		Route::post("/document_tracking/exportIncomingDocsExcel",array("uses"=>"document_tracking\docsController@exportIncomingDocumentsToExcel","as"=>"exportIncomingDocsToExcel"));
		Route::post("/document_tracking/exportIssuedDocsExcel/{issue_type}",array("uses"=>"document_tracking\docsController@exportIssuedDocumentsToExcel","as"=>"exportIssuedDocsToExcel"));
		Route::post("/document_tracking/exportIncomingIssuedDocsExcel",array("uses"=>"document_tracking\docsController@exportIncomingIssuedDocumentsToExcel","as"=>"exportIncomingIssuedDocsToExcel"));
		Route::post("/document_tracking/approvedDocument",array("uses"=>"document_tracking\docsController@approveSavedDocument","as"=>"approveSavedDoc"));
		Route::post("/document_tracking/updateSavedDocument/{id}",array("uses"=>"document_tracking\docsController@updateSavedDoc","as"=>"updateSavedDoc"));
		Route::post("/document_tracking/updateIncomingDocument/{id}",array("uses"=>"document_tracking\docsController@postUpdateIncomingDoc","as"=>"updateIncomingDoc"));
		Route::post("/document_tracking/updateIssuedDocument/{id}",array("uses"=>"document_tracking\docsController@postUpdateIssuedDoc","as"=>"updateIssuedDoc"));
		Route::post("/document_tracking/searchedDocuments/{id}",array("uses"=>"document_tracking\docsController@searchIncomingIssuedDocuments","as"=>"postDocSearch"));
		Route::post('/document_tracking/docSearchedDashboard/{year}', array('uses'=>'document_tracking\docsController@getSearchDashboardData','as'=>'searchDashboardData'));
		Route::post('/document_tracking/pendingIncomingsYearly/{year}', array('uses'=>'document_tracking\docsController@getPendingIncomingsOnYearlyBases','as'=>'getPendingsBasedOnYear'));
		Route::post('/document_tracking/yearlyPendingIncomingNumbers/{year}', array('uses'=>'document_tracking\docsController@yearlyIncomingNumbers','as'=>'getYearlyIncomingNumbers'));


	});

//=========================================== End of M&E Document Tracking Database system =============================================================

//=========================================== Document Management Database Routes =============================================================

	Route::group(array('before' => 'csrf'), function(){
	//Document managemnt routs
		Route::get("/document_managment/load_ex",array("uses"=>"document_managment\mainController_doc@loadRecordsList","as"=>"document_recordsList"));
    Route::get("/document_managment/insertForm_document",array("uses"=>"document_managment\mainController_doc@loadpage_insert","as"=>"insertForm_document"));
    //live search maktob
    Route::post("/document_managment/get_maktob_search", array('uses' => 'document_managment\search_doc_controller@search_maktob_live',"as"=>"get_maktob_search_doc"));
    //get makatab warada
    Route::get("/document_managment/get_mateb_warada",array("uses"=>"document_managment\mainController_doc@get_mateb_warada","as"=>"get_maktob_warada_data_doc"));
     //get maktob sadar
    Route::get("/document_managment/get_maktob_sadara",array("uses"=>"document_managment\mainController_doc@get_maktob_sadara","as"=>"get_maktob_sadara_doc"));
		//update result to ajra or na ajra or hafzya
		Route::post("/document_managment/update_result/", array('uses' => 'document_managment\mainController_doc@change_result',"as"=>"update_result"));

    //ajax get data
    Route::get("/document_managment/get_hakom",array("uses"=>"document_managment\mainController_doc@get_hakom","as"=>"get_hakom_data_doc"));
    Route::get("/document_managment/get_maktob",array("uses"=>"document_managment\mainController_doc@get_maktob","as"=>"get_maktob_data_doc"));
    Route::get("/document_managment/get_copy_som",array("uses"=>"document_managment\mainController_doc@get_copy_som","as"=>"get_copy_som_data_doc"));
		//peshnahad routs
    Route::get("/document_managment/get_peshnahad",array("uses"=>"document_managment\mainController_doc@get_pashnahad","as"=>"get_pashnehad_data_doc"));
    Route::get("/document_managment/peshnahad_sadira",array("uses"=>"document_managment\mainController_doc@get_pashnahad_sadira","as"=>"peshnahad_sadira"));
    Route::get("/document_managment/pashnahad_warida",array("uses"=>"document_managment\mainController_doc@get_pashnahad_warida","as"=>"pashnahad_warida"));
          //new tab route hadyat

    Route::get("/document_managment/get_hadayat",array("uses"=>"document_managment\mainController_doc@get_hadayat","as"=>"get_hadayat_doc"));


    Route::get("/document_managment/get_farman",array("uses"=>"document_managment\mainController_doc@get_farman","as"=>"get_farman_data_doc"));
    Route::get("/document_managment/get_istalam",array("uses"=>"document_managment\mainController_doc@get_istalam","as"=>"get_istalam_data_doc"));
    Route::get("/document_managment/get_feceno",array("uses"=>"document_managment\mainController_doc@get_feceno","as"=>"get_feceno_data_doc"));
    //get all employees
    Route::get("/document_managment/all_employees",array("uses"=>"document_managment\EmployeesContrller@get_all_employees","as"=>"all_employees"));
    Route::post("/document_managment/search_employee",array("uses"=>"document_managment\EmployeesContrller@search","as"=>"search_employee"));

    //waraq darkhasti routs
    Route::get("/document_managment/get_waraq_darkhasti_doc",array("uses"=>"document_managment\mainController_doc@get_waraqa_darkhasti","as"=>"get_waraq_darkhasti_doc"));
    Route::get("/document_managment/get_pashnahad_he",array("uses"=>"document_managment\mainController_doc@get_pahshnahad_hedayati","as"=>"get_pashnahad_hadayati_doc"));
    Route::get("/document_managment/get_feceno_hadayati",array("uses"=>"document_managment\mainController_doc@get_feceno_hadayati","as"=>"get_feceno_hadayati_doc"));

    Route::get("/document_managment/get_istalam_hadayati_doc",array("uses"=>"document_managment\mainController_doc@get_istalam_hadayati_data","as"=>"get_istalam_hadayati_doc"));

    Route::get("/document_managment/get_tayenat",array("uses"=>"document_managment\mainController_doc@get_tayenat","as"=>"get_tayenat_doc"));
		//get mosawibat routes
    Route::get("/document_managment/get_mosawibat",array("uses"=>"document_managment\mainController_doc@get_mosawibat","as"=>"get_mosawibat_doc"));
    Route::get("/document_managment/search",array("uses"=>"document_managment\mainController_doc@get_all_search","as"=>"all_doc_search"));

    Route::get("/document_managment/search_yearly_doc",array("uses"=>"document_managment\mainController_doc@get_yearly_search","as"=>"search_yearly_doc"));

   	//insert hakom, istalam .... same document
    Route::post("/document_managment/insert_document",array("uses"=>"document_managment\mainController_doc@insert_doc","as"=>"insert_doc"));
    Route::get("/document_managment/delete_row_doc/{id}",array("uses"=>"document_managment\mainController_doc@delete","as"=>"delete_row_doc"));
    Route::get("/document_managment/select_data_doc/{id}",array("uses"=>"document_managment\mainController_doc@select_row_doc","as"=>"selectData_doc"));
    Route::post("/document_managment/updateDoC/{id}",array("uses"=>"document_managment\mainController_doc@update","as"=>"updateDoC"));
    Route::get("/document_managment/show_file_doc/{id}",array("uses"=>"document_managment\mainController_doc@show_file_toprint","as"=>"show_file_doc"));

	     //hadayat insert changes route
	  Route::get("/document_managment/insertForm_doc",array("uses"=>"document_managment\hadayat_mainController@loadpage_insert_hadayat","as"=>"insertForm_hadayat_doc"));
	  Route::post('/document_managment/insert_Data_hadayat', array("uses" =>"document_managment\hadayat_mainController@insert_hadayat" ,"as"=>"insert_hadayat_doc" ));
	  Route::get("/document_managment/select_doc_hadayat/{id}",array("uses"=>"document_managment\hadayat_mainController@select_row_doc_hadayat","as"=>"select_doc_hadayat"));
	  Route::post("/document_managment/update_hadayat_document/{id}",array("uses"=>"document_managment\hadayat_mainController@update_hadayat","as"=>"update_hadayat_document"));

	   //waraqa darkhasti routs
	   Route::get('/document_managment/load_waraqa_darkhasti_insert',array("uses"=>"document_managment\waraq_darkhasti_controller@load_insert","as"=>"load_waraq_darkhasti_doc"));
	   Route::post('/document_managment/insert_waraq_darkhasti_doc', array("uses" =>"document_managment\waraq_darkhasti_controller@insert_waraq_darkhasti" ,"as"=>"insert_waraq_darkhasti_doc" ));
	   Route::get("/document_managment/select_waraq_darkhasti_doc/{id}",array("uses"=>"document_managment\waraq_darkhasti_controller@select_row_doc_waraq_darkhasti","as"=>"select_waraq_darkhasti_doc"));
	   Route::post("/document_managment/update_waraq_darkhasti_doc/{id}",array("uses"=>"document_managment\waraq_darkhasti_controller@update_waraq_darkhasti","as"=>"update_waraq_darkhasti_doc"));
	//export to excel
	Route::post("/document_managment/export_excel_doc",array("uses"=>"document_managment\search_doc_controller@export_excel","as"=>"export_excel_doc"));
	 // export_employee
	Route::post("/document_managment/export_employee",array("uses"=>"document_managment\EmployeesContrller@export_employee","as"=>"export_employee"));
	Route::post("/document_managment/get_document", array('uses' => 'document_managment\search_doc_controller@search_live',"as"=>"search_live_doc"));
		 //peshnahad sadera warida search
     Route::post("/document_managment/get_sadera_warida", array('uses' => 'document_managment\search_doc_controller@search_sadera_warida',"as"=>"search_live_sadera_warida"));

		 //hadayat search route
		 Route::post("/document_managment/get_hadayat", array('uses' => 'document_managment\search_doc_controller@search_live_hadayat',"as"=>"search_live_hadayat_doc"));
     //waraqa darkhasti search
     Route::post("/document_managment/get_waraqa_darkhasti", array('uses' => 'document_managment\search_doc_controller@search_live_waraq_darkhasti',"as"=>"search_live_waraqa_darkhasti_doc"));
     //hight search
     Route::post("/document_managment/get_all_doc", array('uses' => 'document_managment\search_doc_controller@get_all_document',"as"=>"search_all_doc"));
     //hight search yealy
     Route::post("/document_managment/search_all_yearly_doc", array('uses' => 'document_managment\search_doc_controller@get_all_yearly_document',"as"=>"search_all_yearly_doc"));
     //search warada maktob
     // Route::post("/document_managment/get_warada_maktob", array('uses' => 'document_managment\search_doc_controller@search_warada_maktobs',"as"=>"search_warada_maktob_doc"));
		 //delete image
	   Route::post("/document_managment/delete_img", array('uses' => 'document_managment\mainController_doc@delete_images',"as"=>"delete_image_doc"));
		 // department crud
		  Route::post("/document_managment/insert_department", array('uses' => 'document_managment\mainController_doc@insertDepartment',"as"=>"insert_department"));
});
//=========================================== End of M&E Document Tracking Database system =============================================================

//============================================ Document Archive Management System =======================================================================================

Route::group(array("before"=>"auth"),function()
{
	// DAMS database routes.
	Route::get('/docs_archive/addDocument/', array('uses'=>'docs_archive\docsController@getLoadInsertPage','as'=>'loadDocInsertPage'));
	Route::get('/docs_archive/getDocuments/', array('uses'=>'docs_archive\docsController@getDocs','as'=>'getDocsList'));
	Route::get('/docs_archive/editDocument/{id}', array('uses'=>'docs_archive\docsController@loadEditDocPage','as'=>'editDoc'));
	Route::get('/docs_archive/downloadFile/{id}', array('uses'=>'docs_archive\docsController@downloadDocFile','as'=>'getDownloadDocFile'));
	// Routes of Document Type Management.
	Route::get('/docs_archive/loadDocTypeList/', array('uses'=>'docs_archive\docsController@loadDocTypeList','as'=>'loadDocTypeList'));
	Route::get('/docs_archive/loadSourcesList/', array('uses'=>'docs_archive\docsController@loadDocSourcesList','as'=>'loadSourcesList'));
	Route::get('/docs_archive/documentTypeList/', array('uses'=>'docs_archive\docsController@getDocTypeList','as'=>'docTypeList'));
	Route::get('/docs_archive/documentSourceList/', array('uses'=>'docs_archive\docsController@getSourcesList','as'=>'docSourceList'));
	Route::get('/docs_archive/editDocumentType/{id}', array('uses'=>'docs_archive\docsController@loadEditDocTypePage','as'=>'editDocType'));
	Route::get('/docs_archive/editDocumentSource/{id}', array('uses'=>'docs_archive\docsController@loadEditDocSourcePage','as'=>'editSources'));
	Route::group(array('before' => 'csrf'), function(){
		Route::post('/docs_archive/addNewRecord', array('uses'=>'docs_archive\docsController@addDoc','as'=>'saveNewDoc'));
		Route::post('/docs_archive/addNewDocType', array('uses'=>'docs_archive\docsController@addDocumentType','as'=>'saveNewDocType'));
		Route::post('/docs_archive/addNewDocSource', array('uses'=>'docs_archive\docsController@addDocSource','as'=>'saveNewDocSource'));
		Route::post('/docs_archive/removeRecord', array('uses'=>'docs_archive\docsController@getDeleteDoc','as'=>'deleteDoc'));
		Route::post('/docs_archive/removeDocumentType', array('uses'=>'docs_archive\docsController@getDeleteDocType','as'=>'deleteDocType'));
		Route::post('/docs_archive/removeDocumentSource', array('uses'=>'docs_archive\docsController@getDeleteDocSource','as'=>'deleteDocSource'));
		Route::post("/docs_archive/updateDocument/{id}",array("uses"=>"docs_archive\docsController@updateDoc","as"=>"postUpdateDoc"));
		Route::post("/docs_archive/updateDocType/{id}",array("uses"=>"docs_archive\docsController@updateDocumentType","as"=>"postUpdateDocType"));
		Route::post("/docs_archive/updateDocSource/{id}",array("uses"=>"docs_archive\docsController@updateDocumentSource","as"=>"postUpdateDocSource"));
		Route::post("/docs_archive/removeDocument",array("uses"=>"docs_archive\docsController@removeDocumentFile","as"=>"getRemoveDocFile"));
		Route::post("/docs_archive/searchedDocuments",array("uses"=>"docs_archive\docsController@getSearchedDocs","as"=>"searchDocs"));
		Route::post("/docs_archive/exportExcel",array("uses"=>"docs_archive\docsController@exportDocsToExcel","as"=>"exportToExcel"));
	});
});

//=========================================== Routes of Assets Management Database system ==========================================================
    Route::group(array("before"=>"auth"),function()
    {
        // Assets Settings Management database routes.
        Route::get('/assets_mgmt/assetsMgmtForm/', array('uses'=>'assets_mgmt\assetsMainController@getAssetsMgmtForm','as'=>'loadAssetsMgmtForm'));
        Route::get('/assets_mgmt/getAllAssets/', array('uses'=>'assets_mgmt\assetsMainController@assetsMgmtItemList','as'=>'getAssetsMgmtItemsList'));
        Route::get('/assets_mgmt/assetsNature/{item}', array('uses'=>'assets_mgmt\assetsMainController@getAssetNature','as'=>'getAssetsMgmtAssetNature'));
        Route::get('/assets_mgmt/getAssetsMainItems/{item}', array('uses'=>'assets_mgmt\assetsMainController@getAssetMainItems','as'=>'getAssetsMgmtMainItems'));
        Route::get('/assets_mgmt/getAssetsType/{item}', array('uses'=>'assets_mgmt\assetsMainController@getAssetType','as'=>'getAssetsMgmtAssetType'));
        Route::get('/assets_mgmt/getAssetsSubItems/{item}', array('uses'=>'assets_mgmt\assetsMainController@getAssetSubItems','as'=>'getAssetsMgmtSubItems'));
        Route::get('/assets_mgmt/getAssetsEndItems/{item}', array('uses'=>'assets_mgmt\assetsMainController@getAssetEndItems','as'=>'getAssetsMgmtEndItems'));
        Route::get('/assets_mgmt/getAssetsItemDetail/{item}', array('uses'=>'assets_mgmt\assetsMainController@getAssetItemDetail','as'=>'getAssetsMgmtItemDetail'));
        Route::get('/assets_mgmt/getAssetMgmtSanctions', array('uses'=>'assets_mgmt\assetsMainController@getAssetSanctions','as'=>'getAssetsMgmtSanctions'));
        Route::get('/assets_mgmt/getAssetMgmtVendors', array('uses'=>'assets_mgmt\assetsMainController@getAssetVendors','as'=>'getAssetsMgmtVendors'));
        Route::get('/assets_mgmt/getAssetMgmtLocations', array('uses'=>'assets_mgmt\assetsMainController@getAssetLocations','as'=>'getAssetsMgmtLocations'));
        Route::get('/assets_mgmt/getAssetMgmtCurrency', array('uses'=>'assets_mgmt\assetsMainController@getAssetCurrency','as'=>'getAssetsMgmtCurrency'));
        Route::get('/assets_mgmt/getAssetMgmtStoreKeeper', array('uses'=>'assets_mgmt\assetsMainController@getAssetStoreKeeper','as'=>'getAssetsMgmtStoreKeeper'));
        // Asset purchase routes.
        Route::get('/assets_mgmt/getAllPurchases/', array('uses'=>'assets_mgmt\assetsMainController@assetsMgmtPurchaseList','as'=>'getAssetsMgmtPurchaseList'));
        Route::get('/assets_mgmt/assetPurchaseEntryPage/', array('uses'=>'assets_mgmt\assetsMainController@loadAssetPurchaseEntryPage','as'=>'assetPurchaseEntryPage'));
        Route::get('/assets_mgmt/assetPurchaseDetails/{record_id?}', array('uses'=>'assets_mgmt\assetsMainController@assetPurchaseDetails','as'=>'viewAssetPurchaseDetails'));
        // Asset Purchase Inward routes.
        Route::get('/assets_mgmt/getAllPurchaseInwards/', array('uses'=>'assets_mgmt\assetsMainController@purchaseInwardList','as'=>'getAssetsMgmtPurchaseInwardList'));
        Route::get('/assets_mgmt/assetInwardDetails/{record_id?}', array('uses'=>'assets_mgmt\assetsMainController@assetInwardDetails','as'=>'viewAssetInwardDetails'));
        // Asset Identification Get Routes.
        Route::get('/assets_mgmt/getAssetIdentities/', array('uses'=>'assets_mgmt\assetsMainController@assetIdentityList','as'=>'getAssetIdentificationList'));
        Route::get('/assets_mgmt/assetIdentityDetails/{record_id?}', array('uses'=>'assets_mgmt\assetsMainController@assetIdentityDetails','as'=>'viewAssetIdentityDetails'));
        // Asset Allot Get Routes.
        Route::get('/assets_mgmt/getAssetAllots/', array('uses'=>'assets_mgmt\assetsMainController@assetAllotList','as'=>'getAssetAllotList'));
        Route::get('/assets_mgmt/assetAllotmentDetails/{record_id?}', array('uses'=>'assets_mgmt\assetsMainController@assetAllotDetails','as'=>'viewAssetAllotmentDetails'));
        // Allotment Return Get Routes.
        Route::get('/assets_mgmt/allotmentReturn/', array('uses'=>'assets_mgmt\assetsMainController@assetAllotmentReturnList','as'=>'getAssetAllotmentReturn'));
        Route::get('/assets_mgmt/assetAllotmentReturnForm/', array('uses'=>'assets_mgmt\assetsMainController@loadAssetAllotmentReturnPage','as'=>'getAssetAllotmentReturnForm'));
        Route::get('/assets_mgmt/assetAllotmentReturnDetails/{record_id?}', array('uses'=>'assets_mgmt\assetsMainController@allotmentReturnDetails','as'=>'viewAssetAllotmentReturnDetails'));
        // Allotment to StoreKeeper Get Routes.
        Route::get('/assets_mgmt/allotmentToStorekeeper/', array('uses'=>'assets_mgmt\assetsMainController@assetAllotmentToStorekeeperList','as'=>'getAssetAllotmentToStorekeeper'));
        Route::get('/assets_mgmt/assetAllotmentToStorekeeperForm/', array('uses'=>'assets_mgmt\assetsMainController@loadAssetAllotmentToStorekeeperPage','as'=>'getAssetAllotmentToStorekeeperForm'));
        Route::get('/assets_mgmt/assetAllotmentToStorekeeperDetails/{record_id?}', array('uses'=>'assets_mgmt\assetsMainController@allotmentToStorekeeperDetails','as'=>'viewAssetAllotmentToStorekeeperDetails'));
        // Allotment to Individual Get Routes.
        Route::get('/assets_mgmt/allotmentToIndividual/', array('uses'=>'assets_mgmt\assetsMainController@assetAllotmentToIndividualList','as'=>'getAssetAllotmentToIndividual'));
        Route::get('/assets_mgmt/assetAllotmentToIndividualForm/', array('uses'=>'assets_mgmt\assetsMainController@loadAssetAllotmentToIndividualPage','as'=>'getAssetAllotmentToIndividualForm'));
        Route::get('/assets_mgmt/assetAllotmentToIndividualDetails/{record_id?}', array('uses'=>'assets_mgmt\assetsMainController@allotmentToIndividualDetails','as'=>'viewAssetAllotmentToIndividualDetails'));

        // Notification Routes
        Route::get('/assets_mgmt/visitNotification/{table}/{record_id}/{notification_id}', array('uses'=>'assets_mgmt\assetsMainController@viewTheNotification','as'=>'viewNotification'));
    });
    Route::group(array('before' => 'csrf'), function(){
        Route::post('/assets_mgmt/addAssetItems/{item}', array('uses'=>'assets_mgmt\assetsMainController@addItems','as'=>'addAssetMgmtItems'));
        Route::post('/assets_mgmt/deleteAssetItems/', array('uses'=>'assets_mgmt\assetsMainController@deleteItem','as'=>'deleteAssetMgmtItem'));
        Route::post('/assets_mgmt/mainItemCode/', array('uses'=>'assets_mgmt\assetsMainController@mainItemCode','as'=>'getMainItemCode'));
        Route::post('/assets_mgmt/subItemCode/', array('uses'=>'assets_mgmt\assetsMainController@subItemCode','as'=>'getSubItemCode'));
        Route::post('/assets_mgmt/endItemCode/', array('uses'=>'assets_mgmt\assetsMainController@endItemCode','as'=>'getEndItemCode'));
        Route::post('/assets_mgmt/addAssetNature/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetNature','as'=>'addAssetMgmtAssetNature'));
        Route::post('/assets_mgmt/addAssetType/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetType','as'=>'addAssetMgmtAssetType'));
        Route::post('/assets_mgmt/addAssetSanction/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetSanction','as'=>'addAssetMgmtSanction'));
        Route::post('/assets_mgmt/addAssetVendor/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetVendor','as'=>'addAssetMgmtVendor'));
        Route::post('/assets_mgmt/deleteAssetNature/', array('uses'=>'assets_mgmt\assetsMainController@deleteAssetNature','as'=>'deleteAssetMgmtAssetNature'));
        Route::post('/assets_mgmt/deleteAssetType/', array('uses'=>'assets_mgmt\assetsMainController@deleteAssetType','as'=>'deleteAssetMgmtAssetType'));
        Route::post('/assets_mgmt/deleteSanction/', array('uses'=>'assets_mgmt\assetsMainController@deleteAssetSanction','as'=>'deleteAssetMgmtSanction'));
        Route::post('/assets_mgmt/deleteVendor/', array('uses'=>'assets_mgmt\assetsMainController@deleteAssetVendor','as'=>'deleteAssetMgmtVendor'));
        Route::post('/assets_mgmt/itemDetailInformation/', array('uses'=>'assets_mgmt\assetsMainController@getItemDetailInformation','as'=>'getItemDetailInfo'));
        Route::post('/assets_mgmt/addAssetLocation/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetLocation','as'=>'addAssetMgmtLocation'));
        Route::post('/assets_mgmt/deleteLocation/', array('uses'=>'assets_mgmt\assetsMainController@deleteAssetLocation','as'=>'deleteAssetMgmtLocation'));
        Route::post('/assets_mgmt/addAssetStoreKeeper/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetStoreKeeper','as'=>'addAssetMgmtStoreKeeper'));
        Route::post('/assets_mgmt/deleteStoreKeeper/', array('uses'=>'assets_mgmt\assetsMainController@deleteAssetStoreKeeper','as'=>'deleteAssetMgmtStoreKeeper'));
        Route::post('/assets_mgmt/addAssetCurrency/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetCurrency','as'=>'addAssetMgmtCurrency'));
        Route::post('/assets_mgmt/deleteCurrency/', array('uses'=>'assets_mgmt\assetsMainController@deleteAssetCurrency','as'=>'deleteAssetMgmtCurrency'));
        // Asset purchase post routes.
        Route::post('/assets_mgmt/addAssetPurchase/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetPurchase','as'=>'saveNewAssetPurchase'));
        Route::post('/assets_mgmt/purchaseEditForm/', array('uses'=>'assets_mgmt\assetsMainController@getLoadPurchaseEditForm','as'=>'assetPurchaseEditForm'));
        Route::post('/assets_mgmt/updateAssetPurchase/', array('uses'=>'assets_mgmt\assetsMainController@getUpdateAssetPurchase','as'=>'updateAssetMgmtPurchase'));
        Route::post('/assets_mgmt/deleteAssetPurchase/', array('uses'=>'assets_mgmt\assetsMainController@getDeleteAssetPurchase','as'=>'deleteAssetMgmtPurchase'));
        // Asset Purchase Inward Post Routes.
        Route::post('/assets_mgmt/purchaseInwardForm/', array('uses'=>'assets_mgmt\assetsMainController@loadPurchaseInwardForm','as'=>'getAssetsMgmtPurchaseInwardForm'));
        Route::post('/assets_mgmt/addAssetInward/', array('uses'=>'assets_mgmt\assetsMainController@addPurchaseInwardData','as'=>'addPurchaseInward'));
        Route::post('/assets_mgmt/itemAndPurchaseInformation/', array('uses'=>'assets_mgmt\assetsMainController@itemAndPurchaseInfo','as'=>'getItemAndPurchaseDetails'));
        Route::post('/assets_mgmt/deleteAssetInward/', array('uses'=>'assets_mgmt\assetsMainController@deletePurchaseInward','as'=>'deleteAssetMgmtInward'));
        Route::post('/assets_mgmt/assetInwardEditForm/', array('uses'=>'assets_mgmt\assetsMainController@loadInwardEditPage','as'=>'assetInwardEditForm'));
        Route::post('/assets_mgmt/updateAssetPurchase/', array('uses'=>'assets_mgmt\assetsMainController@updateAssetInward','as'=>'updateAssetMgmtInward'));
        // Asset Identification Post Routes.
        Route::post('/assets_mgmt/assetIdentityForm/', array('uses'=>'assets_mgmt\assetsMainController@loadAssetIdentityPage','as'=>'getAssetIdentityForm'));
        Route::post('/assets_mgmt/purchaseAndInwardInformation/', array('uses'=>'assets_mgmt\assetsMainController@purchaseAndInwardInfo','as'=>'getPurchaseAndInwardDetails'));
        Route::post('/assets_mgmt/addAssetIdentification/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetIdentification','as'=>'addAssetIdentity'));
        Route::post('/assets_mgmt/deleteAssetIdentification/', array('uses'=>'assets_mgmt\assetsMainController@deleteAssetIdentity','as'=>'deleteAssetIdentity'));
        Route::post('/assets_mgmt/assetIdentificationEditPage/', array('uses'=>'assets_mgmt\assetsMainController@loadAssetIdentityEditPage','as'=>'assetIdentityEditForm'));
        Route::post('/assets_mgmt/updateAssetIdentification/', array('uses'=>'assets_mgmt\assetsMainController@updateAssetIdentity','as'=>'updateAssetIdentity'));

        // Asset Allot Post Routes.
        Route::post('/assets_mgmt/assetAllotForm/', array('uses'=>'assets_mgmt\assetsMainController@loadAssetAllotPage','as'=>'getAssetAllotForm'));
        Route::post('/assets_mgmt/addAssetAllot/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetAllot','as'=>'addAssetAllot'));
        Route::post('/assets_mgmt/assetEmployeePosition/', array('uses'=>'assets_mgmt\assetsMainController@getAssetEmployeePosition','as'=>'getAssetEmpDesignation'));
        Route::post('/assets_mgmt/getDepartmentEmployees/', array('uses'=>'assets_mgmt\assetsMainController@getEmpBasedOnDept','as'=>'getAssetDeptEmployees'));
        Route::post('/assets_mgmt/deleteAssetAllot/', array('uses'=>'assets_mgmt\assetsMainController@deleteAssetAllot','as'=>'deleteAssetMgmtAllot'));
        Route::post('/assets_mgmt/assetAllotEditPage/', array('uses'=>'assets_mgmt\assetsMainController@loadAssetAllotEditPage','as'=>'assetAllotEditForm'));
        Route::post('/assets_mgmt/updateAssetAllot/', array('uses'=>'assets_mgmt\assetsMainController@updateAssetAllot','as'=>'updateAssetMgmtAllot'));
        Route::post('/assets_mgmt/authorizeOrRejectAllot/', array('uses'=>'assets_mgmt\assetsMainController@authorizeOrRejectAllotDetail','as'=>'AuthorizeOrRejectAllotment'));
        // Asset Allotment Return Post Routes
        Route::post('/assets_mgmt/addAssetAllotmentReturn/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetAllotmentReturn','as'=>'addAssetAllotmentReturn'));
        Route::post('/assets_mgmt/assetAllotmentReturnEditPage/', array('uses'=>'assets_mgmt\assetsMainController@loadAssetAllotmentReturnEditPage','as'=>'assetAllotmentReturnEditForm'));
        Route::post('/assets_mgmt/updateAssetAllotmentReturn/', array('uses'=>'assets_mgmt\assetsMainController@updateAssetAllotmentReturn','as'=>'updateAssetMgmtAllotmentReturn'));
        Route::post('/assets_mgmt/deleteAssetAllotmentReturn/', array('uses'=>'assets_mgmt\assetsMainController@deleteAssetAllotmentReturn','as'=>'deleteAssetMgmtAllotmentReturn'));
        // Asset Allotment to Storekeeper Post Routes
        Route::post('/assets_mgmt/addAssetAllotmentToStorekeeper/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetAllotmentToStorekeeper','as'=>'addAssetAllotmentToStorekeeper'));
        Route::post('/assets_mgmt/assetAllotmentToStorekeeperEditPage/', array('uses'=>'assets_mgmt\assetsMainController@loadAssetAllotmentToStorekeeperEditPage','as'=>'assetAllotmentToStorekeeperEditForm'));
        Route::post('/assets_mgmt/updateAssetAllotmentToStorekeeper/', array('uses'=>'assets_mgmt\assetsMainController@updateAssetAllotmentToStorekeeper','as'=>'updateAssetMgmtAllotmentToStorekeeper'));
        Route::post('/assets_mgmt/deleteAssetAllotmentToStorekeeper/', array('uses'=>'assets_mgmt\assetsMainController@deleteAssetAllotmentToStorekeeper','as'=>'deleteAssetMgmtAllotmentToStorekeeper'));
        // Asset Allotment to Individual Post Routes
        Route::post('/assets_mgmt/addAssetAllotmentToIndividual/', array('uses'=>'assets_mgmt\assetsMainController@insertAssetAllotmentToIndividual','as'=>'addAssetAllotmentToIndividual'));
        Route::post('/assets_mgmt/assetAllotmentToIndividualEditPage/', array('uses'=>'assets_mgmt\assetsMainController@loadAssetAllotmentToIndividualEditPage','as'=>'assetAllotmentToIndividualEditForm'));
        Route::post('/assets_mgmt/updateAssetAllotmentToIndividual/', array('uses'=>'assets_mgmt\assetsMainController@updateAssetAllotmentToIndividual','as'=>'updateAssetMgmtAllotmentToIndividual'));
        Route::post('/assets_mgmt/deleteAssetAllotmentToIndividual/', array('uses'=>'assets_mgmt\assetsMainController@deleteAssetAllotmentToIndividual','as'=>'deleteAssetMgmtAllotmentToIndividual'));

        // Authorize or Reject Records Routes.
        Route::post('/assets_mgmt/authorizeOrRejectRecords/', array('uses'=>'assets_mgmt\assetsMainController@authorizeOrRejectRecordDetail','as'=>'AuthorizeOrRejectRecords'));

    });

//=========================================== End of Assets Management Database system  =============================================================
//phone department route
//--------------------------Select Data -----------
  Route::get("/it_telephone/listOfPhonesAndSimCards",array("uses"=>"it_telephone\main_controller@loadreport","as"=>"phoneSimcardList"));
  Route::get("/it_telephone/sim_management",array("uses"=>"it_telephone\main_controller@sim_manag","as"=>"sim_management"));
  Route::get("/it_telephone/listOfSimCards",array("uses"=>"it_telephone\main_controller@loadRecordsList","as"=>"simcardsList"));
  //Import and export data to excel
  Route::post("/it_telephone/export_excel",array("uses"=>"it_telephone\main_controller@exportExcel","as"=>"export_to_excel"));
  //-----------------Download file
  Route::get("/it_telephone/downloadPhoneFile/{id}",array("uses"=>"it_telephone\main_controller@file_download","as"=>"downloadPhoneFile"));
  // Route::get()
  //----------------------------Insert Data-------------------------
  //-------------Insert Employee Information
  Route::get("/it_telephone/load_emp/{simcard_number}",array("uses"=>"it_telephone\main_controller@load_emp_form","as"=>"load_emp"));
  Route::post("/it_telephone/insert_emp",array("uses"=>"it_telephone\main_controller@insert_emp_info","as"=>"insert_emp"));
  //-------------Insert SIM Information
  Route::get("/it_telephone/insert_row",array("uses"=>"it_telephone\main_controller@load_insert_form","as"=>"insert_row"));
  Route::post("/it_telephone/insert_mob",array("uses"=>"it_telephone\main_controller@insert_mo_data","as"=>"insert_mobile"));
  //-------------Insert USB Dongle Information
  Route::get("/it_telephone/insert_dong",array("uses"=>"it_telephone\main_controller@load_dong_form","as"=>"lod_dong"));
  Route::post("/it_telephone/insert_dong_info",array("uses"=>"it_telephone\main_controller@insert_dong_data","as"=>"insert_dongle"));
  //-------------Insert 020 Information
  Route::get("/it_telephone/insert_020",array("uses"=>"it_telephone\main_controller@load_020_form","as"=>"lod_020"));
  Route::post("/it_telephone/insert_020_info",array("uses"=>"it_telephone\main_controller@insert_020_data","as"=>"insert_020"));
  //------------Insert Directorat Data
  Route::get("/it_telephone/lod_dir",array("uses"=>"it_telephone\main_controller@lod_dir_form","as"=>"lod_dir"));
  Route::post("/it_telephone/insert_dir_info",array("uses"=>"it_telephone\main_controller@insert_dir_data","as"=>"insert_dir"));
  //----------------------------Update Data---------------------------------
  //---------------Update Simcard info
  Route::get("/it_telephone/edit_equp/{id}",array("uses"=>"it_telephone\main_controller@load_editequp_form","as"=>"edit_equp"));
  Route::post("/it_telephone/edit_mob/{id}",array("uses"=>"it_telephone\main_controller@edit_mo_data","as"=>"edit_mobile"));
  //---------------Update Dongle info
  Route::get("/it_telephone/edit_dongle/{id}",array("uses"=>"it_telephone\main_controller@load_editdongle_form","as"=>"edit_dongle"));

  Route::post("/it_telephone/edit_usb/{id}",array("uses"=>"it_telephone\main_controller@edit_usb_data","as"=>"edit_usb"));

  //---------------Update 020 info
  Route::get("/it_telephone/edit_020/{id}",array("uses"=>"it_telephone\main_controller@load_edit020_form","as"=>"edit_020"));

  Route::post("/it_telephone/edit_020_info/{id}",array("uses"=>"it_telephone\main_controller@edit_020_data","as"=>"edit_020_info"));
  //------------------------Delete Record------------------------

  Route::get("/it_telephone/delete_equp/{id}",array("uses"=>"it_telephone\main_controller@delete_equp","as"=>"delete_equp"));
  Route::post("it_telephone/search_info",array('uses'=>'it_telephone\main_controller@search_engin',"as"=>"search_info"));
  Route::post("/it_telephone/search_sim", array('uses' => 'it_telephone\main_controller@search_sim_live',"as"=>"search_sim"));


  //=========================================== End of phone department Database system  =============================================================

 //  //============== Start of docscom_doc_tracking Database system ==================

 //  //--------------------------Select Document Data -----------

 //  Route::get("/docscom_doc_tracking/listOfdocument",array("uses"=>"docscom_doc_tracking\main_controller@loadRecordsList","as"=>"listOfdocument"));
 //  //--------------------------Select Deputy info
 //                //-------------Insert Document
 //  Route::get("/docscom_doc_tracking/load_doc",array("uses"=>"docscom_doc_tracking\main_controller@load_doc_form","as"=>"load_doc"));
 //  Route::post("/docscom_doc_tracking/insert_doc",array("uses"=>"docscom_doc_tracking\main_controller@insert_doc_info","as"=>"insert_doc"));
 //                 //--------------Insert Deputy
 //  Route::get("/docscom_doc_tracking/load_deputy",array("uses"=>"docscom_doc_tracking\main_controller@load_deputy_form","as"=>"load_deputy"));
 //  Route::post("/docscom_doc_tracking/insert_deputy",array("uses"=>"docscom_doc_tracking\main_controller@insert_deputy_info","as"=>"insert_deputy"));
	// 	         //--------------Insert Executing info
 //  Route::get("/docscom_doc_tracking/load_executing",array("uses"=>"docscom_doc_tracking\main_controller@load_executing_form","as"=>"load_executing"));
 //  Route::post("/docscom_doc_tracking/insert_executing",array("uses"=>"docscom_doc_tracking\main_controller@insert_executing_info","as"=>"insert_executing"));
	// 	         //--------------Insert Sender info
 //  Route::get("/docscom_doc_tracking/load_sender",array("uses"=>"docscom_doc_tracking\main_controller@load_sender_form","as"=>"load_sender"));
 //  Route::post("/docscom_doc_tracking/insert_sender",array("uses"=>"docscom_doc_tracking\main_controller@insert_sender_info","as"=>"insert_sender"));
 //           		//Import and export data to excel
 //  Route::post("/docscom_doc_tracking/export_excel",array("uses"=>"docscom_doc_tracking\main_controller@exportExcel","as"=>"export_to_excel"));
 //                //-----------------Download file
 //  Route::get("/docscom_doc_tracking/download_doc_File/{id}",array("uses"=>"docscom_doc_tracking\main_controller@file_download","as"=>"download_doc_File"));
 //  //----------------------------Update Data---------------------------------
	//         	//---------------Update Document info
 //  Route::get("/docscom_doc_tracking/edit_doc/{id}",array("uses"=>"docscom_doc_tracking\main_controller@load_editdoc_form","as"=>"edit_doc"));
 //  Route::post("/docscom_doc_tracking/edit_doc_info/{id}",array("uses"=>"docscom_doc_tracking\main_controller@insert_editdoc_data","as"=>"edit_doc_info"));
 //             	//----------------Delete Record------------------------
 //  Route::get("/docscom_doc_tracking/delete_doc/{id}",array("uses"=>"docscom_doc_tracking\main_controller@delete_doc","as"=>"delete_doc"));
 //    //-------------------------Searching----------------------------------
 //  Route::get("docscom_doc_tracking/search_mydoc",array('uses'=>'docscom_doc_tracking\main_controller@load_data_forsearch',"as"=>"search_mydoc"));
 //  Route::post("docscom_doc_tracking/search_info",array('uses'=>'docscom_doc_tracking\main_controller@search_engin',"as"=>"search_info"));
 //  Route::post("/docscom_doc_tracking/searchrow", array('uses' => 'docscom_doc_tracking\main_controller@search_mydoc_live',"as"=>"searchrow"));
 //   //============== End of docscom_doc_tracking Database system ==================
