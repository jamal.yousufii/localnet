<?php

namespace App\Exports;
use App\models\Billofquantities;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;

class ExcelExport implements FromCollection, WithHeadings, WithHeadingRow, WithCustomStartCell
{
    

    protected $bill,$location_id;

    public function __construct($bill,$location_id)
    {

    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $id         = $this->bill;
        $location_id= $this->location_id;
        return  Billofquantities::select('operation_type','unit_id','amount','price','total_price','percentage','remarks')->where('project_id',$id)->where('project_location_id',$location_id)->get();
    }

    public function headings(): array
    {
        return [
            'operation_type',
            'unit_id',
            'amount',
            'price',
            'total_price',
            'percentage',
            'remarks',
        ];
    }
    public function headingRow(): int
    {
        return 8;
    }
    public function startCell(): string
    {
        return 'A9';
    }
}
