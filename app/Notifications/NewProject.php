<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewProject extends Notification
{
    use Queueable;
    public $project_id;
    protected $message;
    protected $created_by;
    protected $redirect_url;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($project_id, $message, $created_by, $redirect_url)
    {
        $this->project_id   = $project_id;
        $this->message      = $message;
        $this->created_by   = $created_by;
        $this->redirect_url = $redirect_url;
        //$HOLLY_SHIT_I_HAVE_NO_IDEA_WHAT_TO_NAME_THIS_VARIABLE = "";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'project_id'    => $this->project_id,
            'message'       => $this->message,
            'created_by'    => $this->created_by,
            'redirect_url'  => $this->redirect_url,
        ];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
