<?php 
use App\library\Dateconverter;
use App\library\jdatetime;

	//this is the helper functions of the fixed asset management database
	//==============================================================================
	
	//get notifications for asset management menues either being authorized or rejected.
	function getAssetMgmtNotifications($getCount=false)
	{
		if(isManager() || isAdmin())
		{
			$object = DB::connection("assets_mgmt")->table("notification")->select("*")->where('is_viewed', 0)->get();
			//dd($object);
			if($getCount)
			{
				return count($object);
			}
			else
			{
				$li = "";
		
				foreach($object AS $item)
				{
					
					$url = URL::route("viewNotification",array($item->table_name,$item->table_id,$item->id));
		
					//$li .= '<li class="item">';
					$li .= '<a class="list-group-item" role="menuitem" href="'.$url.'"><div class="media"><div class="media-left padding-right-10">';
					$li .= '</div><div class="media-body">';
					
					$li .= '<h6 class="media-heading">Asset Management Notification</h6>';
					$li .=  '<span class="content-text">'.$item->subject.', Click here to see the details.</span>';
					
					$li .= '<time class="media-meta">&nbsp;<i class="fa fa-clock-o"></i>&nbsp;'.$item->created_at.'</time>';
					$li .= '</div></div></a>';
					//$li .= '</li>';
				}
		
				//return elements
				return $li;
			}
		}
	}

	// check table authorization.
	function isAuthorized($table="",$record_id=0)
	{
		$isAuthorized = DB::connection('assets_mgmt')->table($table)->select("id")->where("id",$record_id)->where("authorized",1)->get();
		return $isAuthorized;
	}
		
	function getFixedAssetMainItems()
	{
		$main_items = DB::connection('assets_mgmt')->table("main_item")->select("id","main_item_name")->where("deleted",0)->orderBy('id','asc')->groupBy('main_item_name')->get();
		if($main_items)
		{
			$options = "<option value=''>Select Item</option>";
			foreach ($main_items as $items) {
				if($items->main_item_name != "")
				{
					$options .= "<option value='".$items->id."'>".$items->main_item_name."</option>";	
				}
			}
			return $options;
		}
	}
	function getFixedAssetSubItems()
	{
		$sub_items = DB::connection('assets_mgmt')->table("sub_item")->select("id","sub_item_name")->where("deleted",0)->orderBy('id','asc')->groupBy('sub_item_name')->get();
		if($sub_items)
		{
			$options = "<option value=''>Select Item</option>";
			foreach ($sub_items as $items) {
				if($items->sub_item_name != "")
				{
					$options .= "<option value='".$items->id."'>".$items->sub_item_name."</option>";	
				}
			}
			return $options;
		}
	}
	
	function getFixedAssetEndItems()
	{
		$end_items = DB::connection('assets_mgmt')->table("end_item")->select("id","end_item_name")->where("deleted",0)->orderBy('id','asc')->groupBy('end_item_name')->get();
		if($end_items)
		{
			$options = "<option value=''>Select Item</option>";
			foreach ($end_items as $items) {
				if($items->end_item_name != "")
				{
					$options .= "<option value='".$items->id."'>".$items->end_item_name."</option>";	
				}
			}
			return $options;
		}
	}

	function getAssetNature($selected_item="")
	{
		$asset_nature = DB::connection('assets_mgmt')->table('asset_nature')->select('id','name')->where("deleted", 0)->groupBy('name')->get();
		if($asset_nature)
		{
			$options = "<option value=''>Select Asset Nature</option>";
			foreach ($asset_nature as $items) {
				if($items->name != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name."</option>";	
				}
			}
			return $options;
		}
	}

	function getAssetType($selected_item="")
	{
		$asset_type = DB::connection('assets_mgmt')->table('asset_type')->select('id','name')->where("deleted", 0)->groupBy('name')->get();
		if($asset_type)
		{
			$options = "<option value=''>Select Asset Type</option>";
			foreach ($asset_type as $items) {
				if($items->name != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name."</option>";	
				}
			}
			return $options;
		}
	}

	function getAuthDepartments($selected_item="")
	{
		$auth_deps = DB::connection('mysql')->table('department')->select('id','name')->where("unactive", 0)->groupBy('name')->get();
		if($auth_deps)
		{
			$options = "<option value=''>Select Department</option>";
			foreach ($auth_deps as $items) {
				if($items->name != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name."</option>";	
				}
			}
			return $options;
		}
	}
	// get the sub departments based on the parent id.
	function getAssetMgmtRelatedSubDepartment($dept_id,$selected_item="")
	{
		$auth_depts = DB::connection('mysql')->table('department')->select('id','name')->where('parent',$dept_id)->where("unactive", 0)->groupBy('name')->get();
		if($auth_depts)
		{
			$options = "<option value=''>اداره فرعی</option>";
			foreach ($auth_depts as $items) {
				if($items->name != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name."</option>";	
				}
			}
			return $options;
		}
	}
	
	// get the employees based on department id.
	function getAssetMgmtRelatedEmployees($dept_id,$selected_item="")
	{
		$related_emp = DB::connection('hr')->table('employees')->select('id','name_dr')->where('department',$dept_id)->groupBy('name_dr')->get();
		if($related_emp)
		{
			$options = "<option value=''>کارمندان این اداره</option>";
			foreach ($related_emp as $items) {
				if($items->name_dr != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name_dr."</option>";	
				}
			}
			return $options;
		}
	}

	function getHREmployeeList($selected_item="", $department_id="")
	{
		$hr_employees = DB::connection('hr')->table('employees')->select('id','name_dr')->where("mawqif_employee", 1)->get();
		if($hr_employees)
		{
			$options = "<option value=''>Select Department</option>";
			foreach ($hr_employees as $items) {
				if($items->name_dr != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name_dr."</option>";	
				}
			}
			return $options;
		}
	}

	function getDepartmentEmployeeList($department_id="")
	{
		$hr_employees = DB::connection('hr')->table('employees')->select('id','name_dr')->where("department", $department_id)->where("mawqif_employee", 1)->get();
		if($hr_employees)
		{
			$options = "<option value=''>Select Employee</option>";
			foreach ($hr_employees as $items) {
				if($items->name_dr != "")
				{
					$options .= "<option value='".$items->id."'>".$items->name_dr."</option>";	
				}
			}
			return $options;
		}
	}

	function getSanctions($selected_item="")
	{
		$sanctions = DB::connection('assets_mgmt')->table('sanction')->select('id','name')->where("deleted", 0)->groupBy('name')->get();
		if($sanctions)
		{
			$options = "<option value=''>Select Sanction</option>";
			foreach ($sanctions as $items) {
				if($items->name != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name."</option>";	
				}
			}
			return $options;
		}
	}

	function getExactLocations($selected_item="")
	{
		$exact_location = DB::connection('assets_mgmt')->table('location')->select('id','exact_location')->where("deleted", 0)->groupBy('exact_location')->get();
		if($exact_location)
		{
			$options = "<option value=''>موقعیت دقیق جنس</option>";
			foreach ($exact_location as $items) {
				if($items->exact_location != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->exact_location."</option>";	
				}
			}
			return $options;
		}
	}

	function getDetailItemsNotAllotted($selected_item="")
	{
		$not_alloted_detail_items = DB::connection('assets_mgmt')->table('item_detail')->select('id','item_detail_code')->where("allotted", 0)->where("deleted", 0)->groupBy('item_detail_code')->get();
		if($not_alloted_detail_items)
		{
			$options = "<option value=''>Select Item Detail</option>";
			foreach ($not_alloted_detail_items as $items) {
				if($items->item_detail_code != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->item_detail_code."</option>";	
				}
			}
			return $options;
		}
	}

	function getItemDetailCodeList($selected_item=0)
	{
		$item_detail_code = DB::connection('assets_mgmt')->table('item_detail')->select('id','item_detail_code')->where("deleted", 0)->groupBy('item_detail_code')->get();
		if($item_detail_code)
		{
			$options = "<option value=''>Select Item Detail</option>";
			foreach ($item_detail_code as $items) {
				if($items->item_detail_code != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->item_detail_code."</option>";	
				}
			}
			return $options;
		}
	}
	function getCurrencies($selected_item=0)
	{
		$currency = DB::connection('assets_mgmt')->table('currency')->select('id','name')->where("deleted", 0)->groupBy('name')->get();
		if($currency)
		{
			$options = "<option value=''>Select Currency / انتخاب واحد پولی</option>";
			foreach ($currency as $items) {
				if($items->name != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name."</option>";	
				}
			}
			return $options;
		}
	}
	function getStoreKeepers($selected_item=0)
	{
		$store_keeper = DB::connection('assets_mgmt')->table('store_keeper')->select('id','name')->where("deleted", 0)->groupBy('name')->get();
		if($store_keeper)
		{
			$options = "<option value=''>انتخاب معتمد</option>";
			foreach ($store_keeper as $items) {
				if($items->name != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name."</option>";	
				}
			}
			return $options;
		}
	}

	function getWarrantyList($selected_item="")
	{
		$options = "<option value=''>Select Warranty/Guarantee</option>";
		$options .= "<option ".("3 Months" == $selected_item ? 'selected':'')." value='3 Months'>3 Months</option>";	
		$options .= "<option ".("6 Months" == $selected_item ? 'selected':'')." value='6 Months'>6 Months</option>";
		$options .= "<option ".("1 Year" == $selected_item ? 'selected':'')." value='1 Year'>1 Year</option>";
		$options .= "<option ".("2 Years" == $selected_item ? 'selected':'')." value='2 Years'>2 Years</option>";
		$options .= "<option ".("3 Years" == $selected_item ? 'selected':'')." value='3 Years'>3 Years</option>";
		$options .= "<option ".("4 Years" == $selected_item ? 'selected':'')." value='4 Years'>4 Years</option>";
		$options .= "<option ".("5 Years" == $selected_item ? 'selected':'')." value='5 Years'>5 Years</option>";
		$options .= "<option ".("6 Years" == $selected_item ? 'selected':'')." value='6 Years'>6 Years</option>";
		$options .= "<option ".("7 Years" == $selected_item ? 'selected':'')." value='7 Years'>7 Years</option>";
		$options .= "<option ".("8 Years" == $selected_item ? 'selected':'')." value='8 Years'>8 Years</option>";
		$options .= "<option ".("9 Years" == $selected_item ? 'selected':'')." value='9 Years'>9 Years</option>";
		$options .= "<option ".("10 Years" == $selected_item ? 'selected':'')." value='10 Years'>10 Years</option>";
		$options .= "<option ".("No Warranty / وارنتی ندارد" == $selected_item ? 'selected':'')." value='No Warranty / وارنتی ندارد'>No Warranty / وارنتی ندارد</option>";
		return $options;
	}

	function getAssetEmployeeDesignation($emp_id=0)
	{
		$emp_position = DB::connection('hr')->table("tashkilat as tsh")->select("tsh.title");
		$emp_position->leftjoin("employees as emp","emp.tashkil_id","=","tsh.id")->where("emp.mawqif_employee",1)->where('emp.id', $emp_id);
		if($emp_position->count()>0){
			return $emp_position->first()->title;
		}else{
			return "";
		}
	}

	function isPurchaseInwarded($purchase_id)
	{
		$inward = DB::connection('assets_mgmt')->table("inward")->where('purchase_id', $purchase_id)->where("deleted",0)->pluck('id');
		if(!empty($inward)) return true; 
		else return false;
	}

	function isAssetIdentified($inward_id)
	{
		$identified = DB::connection('assets_mgmt')->table("asset_identification")->where('inward_id', $inward_id)->where("deleted",0)->pluck('id');
		if(!empty($identified)) return true; 
		else return false;
	}

	function getNameBasedOnId($table, $field, $id)
	{
		$name = DB::connection('assets_mgmt')->table($table)->where('id', $id)->pluck($field);
		if($name) return $name; 
		else return "";
	}

	function getLastRecord($table,$field)
	{
		$field_result = DB::connection('assets_mgmt')->table($table)->select($field)->orderBy('id','desc')->first();
		if($field_result) return $field_result->$field; 
		else return "";
	}

	//==================================================================================

	
?>