<?php
use App\library\Dateconverter;
use App\library\jdatetime;
use App\models\const_maintenance\const_maintenance_model;
use App\models\it_telephone\LogActivity;

	function getLangShortcut()
	{
		$lang = LaravelGettext::getLocaleLanguage();

		if($lang == 'ps')
		{
			return 'pa';
		}
		elseif($lang == 'fa')
		{
			return 'dr';
		}
		else
		{
			return $lang;
		}
	}

	//get check if date setting meladi or not
	function isShamsiDate()
	{


		$date_setting = Session::get('date_setting');

		if($date_setting == 'shamsi')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	//get check if date setting meladi or not
	function isMiladiDate()
	{

		$date_setting = Session::get('date_setting');

		if($date_setting == 'miladi')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	//get date picker input class
	function getDatePickerClass()
	{
		if(isMiladiDate())
		{
			return "datepicker";
		}
		else
		{
			return "datepicker_farsi";
		}
	}

	//---Webmail functions -------------------//
	function loginWebmail()
	{
		//include_once '/usr/share/afterlogic/libraries/afterlogic/api.php';
	    if (class_exists('CApi') && CApi::IsValid())
	    {
	    	$sEmail = 'g.akbari@aop.gov.af';
	    	$sPassword = 'aop@gov';
	    	$sFolder = 'INBOX';
	    	try
	    	{
	    		$oApiIntegratorManager = CApi::Manager('integrator');
	    		$oAccount = $oApiIntegratorManager->LoginToAccount($sEmail, $sPassword);
		    	if ($oAccount)
		    	{
		    		//$oApiMailManager = CApi::Manager('mail');
			    	//$aData = $oApiMailManager->getFolderInformation($oAccount, $sFolder);
			    	// echo '<b>'.$oAccount->Email.':</b><br />';
			    	// if (is_array($aData) && 4 === count($aData))
			    	// {
			    	// 	echo '<pre>';
			    	// 	echo 'Folder: '.$sFolder."\n";
			    	// 	echo 'Count: '.$aData[0]."\n";
			    	// 	echo 'Unread: '.$aData[1]."\n";
			    	// 	echo 'UidNext: '.$aData[2]."\n";
			    	// 	echo 'Hash: '.$aData[3]; echo '</pre>';
			    	// }

			    	// populating session data from the account
			    	$oApiIntegratorManager->SetAccountAsLoggedIn($oAccount);
			    	// redirecting to WebMail
			    	//CApi::Location('../');
		    	}
		    	else
		    	{
		    		echo $oApiIntegratorManager->GetLastErrorMessage();
		    	}
	    	} catch (Exception $oException) {
	    		echo $oException->getMessage();
	    	}
	    }
	    else
	    {
	    	echo 'WebMail API isn\'t available';
	    }
	}
	function logoutWebmail()
	{
		//include_once '/usr/share/afterlogic/libraries/afterlogic/api.php';
	    if (class_exists('CApi') && CApi::IsValid())
	    {
	    	$sEmail = 'g.akbari@aop.gov.af';
	    	$sPassword = 'aop@gov';
	    	$sFolder = 'INBOX';
	    	try
	    	{
	    		$oApiIntegratorManager = CApi::Manager('integrator');
	    		$oApiIntegratorManager->logoutAccount();

	    	} catch (Exception $oException) {
	    		echo $oException->getMessage();
	    	}
	    }
	    else
	    {
	    	echo 'WebMail API isn\'t available';
	    }
	}
	function getUnreadEmails()
	{
		//include_once '/usr/share/afterlogic/libraries/afterlogic/api.php';
	    if (class_exists('CApi') && CApi::IsValid())
	    {
	    	$sEmail = 'g.akbari@aop.gov.af';
	    	$sPassword = 'aop@gov';
	    	$sFolder = 'INBOX';
	    	try
	    	{
	    		$oApiIntegratorManager = CApi::Manager('integrator');
	    		$oAccount = $oApiIntegratorManager->LoginToAccount($sEmail, $sPassword);
		    	if ($oAccount)
		    	{
		    		$oApiMailManager = CApi::Manager('mail');
			    	$aData = $oApiMailManager->getFolderInformation($oAccount, $sFolder);

			    	if (is_array($aData) && 4 === count($aData))
			    	{
			    		//echo '<pre>';
			    		//echo 'Folder: '.$sFolder."\n";
			    		//echo 'Count: '.$aData[0]."\n";
			    		return $aData[1];
			    		//echo 'UidNext: '.$aData[2]."\n";
			    		//echo 'Hash: '.$aData[3]; echo '</pre>';
			    	}

			    	// populating session data from the account
			    	//$oApiIntegratorManager->SetAccountAsLoggedIn($oAccount);
			    	// redirecting to WebMail
			    	//CApi::Location('../');
		    	}
		    	else
		    	{
		    		//echo $oApiIntegratorManager->GetLastErrorMessage();
		    		return 0;
		    	}
	    	} catch (Exception $oException) {
	    		//echo $oException->getMessage();
	    		return 0;
	    	}
	    }
	    else
	    {
	    	echo 'WebMail API isn\'t available';
	    }
	}
	//---Webmail functions end ---------------//

	//get from current url application code
	function getCurrentAppCode()
	{
		$code = Request::segment(1);

		//get user modules form database
		$modules = DB::table('module AS t1')
					->select('t1.code')
					->leftjoin('user_module AS t2','t2.module_id','=','t1.id')
					->where('t2.user_id',Auth::user()->id)
					->orderBy('t1.id','DESC')
					->get();


		if($code == 'manage')
		{
			$code = "auth";
		}
		elseif($code == '')
		{
			foreach($modules AS $item)
			{
				$code = $item->code;
			}
		}

		return $code;
	}

	//get all my available datatabases
	function getMyApplications()
	{
		//get user modules form database
		$modules = DB::table('module AS t1')
					->select('t1.*')
					->leftjoin('user_module AS t2','t2.module_id','=','t1.id')
					->where('t2.user_id',Auth::user()->id)
					->orderBy('t1.id')
					->get();

		return $modules;

	}

	//get all sub departments of this department ----------
	function getAllSubDepartments($depId=0)
	{

		$object = DB::table('department')->where('parent',$depId)->where('unactive',0)->get();

		$results = '';
		if(count($object)>0)
		{
			foreach($object AS $item)
			{
				//get item
				$results .= $item->id.',';
				//check if has sub departments [recursive calling]
				$results .= getAllSubDepartments($item->id);
			}
			$results = substr($results, 0,-1);
		}
		return $results;
		//return $results;
	}
	function getAllSubDepartmentsWithParent($depId=0)
	{

		$object = DB::table('department')->where('parent',$depId)->where('unactive',0)->get();

		$results = '';
		if(count($object)>0)
		{
			foreach($object AS $item)
			{
				//get item
				$results .= $item->id.',';
				//check if has sub departments [recursive calling]
				$results .= getAllSubDepartments($item->id);
			}
			$results = substr($results, 0,-1);
		}
		$subDepIds		= explode(',',$results);
		$subDepIds[] 	= $depId;
		//$subDepIds 		= array_values(array_filter($subDepIds));

		return $subDepIds;
		//return $results;
	}

	//get related sub department
	function getRelatedSubDepartment($depId=0,$includeParent=false)
	{
		if($includeParent)
		{
			$object1 = DB::table('department')
						->where('id',$depId)
						->where('unactive',0)
						->get();

			$object2 = DB::table('department')
						->where('parent',$depId)
						->where('unactive',0)
						->get();

			$deps = array();

			foreach($object1 AS $item)
			{
				$deps[$item->id] = $item->name;
			}
			foreach($object2 AS $item)
			{
				$deps[$item->id] = $item->name;
			}

			return $deps;
		}

		if($depId==80)
		{//president office
			if(getUserDepType(Auth::user()->id)->dep_type==1)
			{
				$ids = array(427,428,429,430,431,432,81,536);
				$object = DB::table('department')
						->where('parent',$depId)
						->where('unactive',0)
						->whereIn('id',$ids)

						->get();
				return $object;
			}
			else
			{
				$ids = array(66,83,84,92,93,161,167,395);
				$object = DB::table('department')
						//->where('parent',$depId)
						->where('unactive',0)
						->whereIn('id',$ids)
						->whereRaw('parent = '.$depId.' OR (advisor = 1)')
						->get();
				return $object;
			}

		}
		elseif($depId==58)
        {//npa
			$ids = array(404,405,406,462,463,464,465,457,550,600);
			$object = DB::table('department')
					->where('parent',$depId)
					->where('unactive',0)
					->whereIn('id',$ids)
					->get();
			return $object;

		}
		elseif($depId==519 && getUserDepType(Auth::user()->id)->dep_type==2)
		{
			$ids = array(520,268,33);
			$object = DB::table('department')
					->where('parent',$depId)
					->where('unactive',0)
					->whereIn('id',$ids)
					->get();
			return $object;
		}
		elseif($depId==428 && getUserDepType(Auth::user()->id)->dep_type==2)
		{
			$ids = array(467,433);
			$object = DB::table('department')
					->where('parent',$depId)
					->where('unactive',0)
					->whereIn('id',$ids)
					->get();
			return $object;
		}

		elseif($depId==430 && getUserDepType(Auth::user()->id)->dep_type==2)
		{
			$ids = array(484);
			$object = DB::table('department')
					->where('parent',$depId)
					->where('unactive',0)
					->whereIn('id',$ids)
					->get();
			return $object;
        }
        elseif($depId==39 && getUserDepType(Auth::user()->id)->dep_type==1)
		{
            $ids = array(175);
			$object = DB::table('department')
					->where('parent',$depId)
					->where('unactive',0)
					->whereIn('id',$ids)
					->get();
			return $object;
        }
		else
		{
            if(hasRule('hr_attendance','filter_department')) 
            {
                $object = DB::table('department as d')
                            ->select('d.*')
                            ->join('user_dept as ud','d.id','=','ud.dept_id')
                            ->where('d.parent',$depId)
                            ->where('ud.user_id',Auth::user()->id)
                            ->where('ud.type','sub_dept')
                            ->where('unactive',0)
                            ->get();
            }
            else
            {
                $object = DB::table('department')
                            ->where('parent',$depId)
                            ->where('unactive',0)
                            ->get();
            }
			return $object;
		}


	}

	function getDepartmentName($depId=0,$lang=0)
	{
         $column_name = ($lang==0?'name':'name_'.$lang);    
		 return DB::table('department')->where('id',$depId)->pluck("$column_name");

	}
	function getPositionName($depId=0)
	{

		 return DB::table('position')->where('id',$depId)->pluck('title');

	}
	function getPositionNameHR($userId=0)
	{

		 return DB::connection('hr')->table('employees as t1')
		 ->leftjoin('auth.users AS u','u.employee_id','=','t1.id')
		 ->where('u.id',$userId)->pluck('t1.current_position_dr');

	}
	function getEmployeePhone($userId=0)
	{

		 return DB::connection('hr')->table('employees as t1')
		 ->leftjoin('auth.users AS u','u.employee_id','=','t1.id')
		 ->where('u.id',$userId)->pluck('t1.phone');

	}
	function getEmployeeId($userId=0)
	{

		 return DB::connection('hr')->table('employees as t1')
		 ->leftjoin('auth.users AS u','u.employee_id','=','t1.id')
		 ->where('u.id',$userId)->pluck('t1.id');

	}
	function getEmployeeSex($userId=0)
	{

		 return DB::connection('hr')->table('employees as t1')
		 ->leftjoin('auth.users AS u','u.employee_id','=','t1.id')
		 ->where('u.id',$userId)->pluck('t1.gender');

	}
	//get my department id
	function getMyDepartmentId()
	{
		return Auth::user()->department_id;
	}

	function getAllDepartments($depId=0)
	{

		$object = DB::table('department')->where('unactive',0)->get();
		return $object;
	}
	function getReallyAllDepartments()
	{

		$object = DB::connection('evaluation')->table('department')->get();
		return $object;
	}
	function getParentDepartment($id=0)
	{

		$object = DB::connection('evaluation')->table('department')->where('id',$id)->pluck('parent');
		return $object;
	}
	function getAllParentDepartments()
	{

		$object = DB::table('department')->where('parent',0)->where('unactive',0)->get();
		return $object;
	}
	//-----------------------------------------------------

	//---- get department trees ---------------------------//
	function getDepartmentTrees()
	{

		// if(isCaseAdmin())
		// {
		// 	$object = DB::table('department')->where('id',161)->where('unactive',0)->get();
		// }
		// else
		// {
			$object = DB::table('department')->where('parent',0)->where('unactive',0)->get();
		//}
		$ul = '';
		foreach($object AS $item)
		{

			$ul .= '<li data-options="state:\'closed\'">';
			$ul .= '<span>'.$item->name.' &nbsp;-&nbsp; '.$item->name_en.' &nbsp;&nbsp;&nbsp;&nbsp;';

			if(canEdit('auth_department'))
			{
				$ul .= '<a href="'.URL::route('getUpdateDepartment',$item->id).'"><i class="fa fa-edit"></i> Edit</a>';
			}
			if(canDelete('auth_department'))
			{
				$ul .= '&nbsp;|&nbsp;<a href="'.URL::route('getDeleteDepartment',$item->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="fa fa-trash-o"></i> Delete</a>';
			}
			if(canEdit('auth_department'))
			{
				$ul .= '&nbsp;|&nbsp;<a href="'.URL::route('getDepartmentApp',$item->id).'"><i class="fa fa-database"></i> Applications</a>';
			}
			$ul .= '</span>';
			//check if has sub departments [recursive calling]
			$ul .= getSubDepTree($item->id);

			$ul .= '</li>';
		}

		//return the result of query
		//echo "<pre>";print_r(htmlspecialchars($ul));
		return $ul;
	}

	function getSubDepTree($parent=0)
	{

		$object = DB::table('department')->where('parent',$parent)->where('unactive',0)->orderBy('parent')->get();

		$ul = '';
		if(count($object)>0)
		{

			$ul .= '<ul>';
			foreach($object AS $item)
			{
				$ul .= '<li>';
				$ul .= '<span>'.$item->name.'  &nbsp;-&nbsp;  '.$item->name_en.' &nbsp;&nbsp;&nbsp;&nbsp;';

				if(canEdit('auth_department'))
				{
					$ul .= '<a href="'.URL::route('getUpdateDepartment',$item->id).'"><i class="fa fa-edit"></i> Edit</a>';
				}
				if(canDelete('auth_department'))
				{
					$ul .= '&nbsp;|&nbsp;<a href="'.URL::route('getDeleteDepartment',$item->id).'" onclick="javascript:return confirm(\'Do you want to continue this operation?\');"><i class="fa fa-trash-o"></i> Delete</a>';
				}
				if(canView('auth_department'))
				{
					$ul .= '&nbsp;|&nbsp;<a href="'.URL::route('getDepartmentApp',$item->id).'"><i class="fa fa-database"></i> Applications</a>';
				}
				$ul .= '</span>';

				$ul .= getSubDepTree($item->id);

				$ul .= "</li>";

			}

			$ul .= "</ul>";
			//$ul .= "</li>";
		}

		return $ul;


	}

	//---- get department trees end ------------------------//

	//---- get department trees ---------------------------//
	function getOrganizationChart()
	{

		$object = DB::table('department')->where('parent',0)->where('unactive',0)->get();

		$ul = '';
		foreach($object AS $item)
		{

			$ul .= '<ul id="basic-chart-source" class="hide">';
			$ul .= '<li class="long-name">'.$item->name;

			//check if has sub departments [recursive calling]
			$ul .= getSubOrgChart($item->id);

			$ul .= '</li>';

			$ul .= '</ul>';
		}

		//return the result of query
		//echo "<pre>";print_r(htmlspecialchars($ul));
		return $ul;
	}

	function getSubOrgChart($parent=0)
	{

		$object = DB::table('department')->where('parent',$parent)->where('unactive',0)->orderBy('parent')->get();

		$ul = '';
		if(count($object)>0)
		{

			$ul .= '<ul>';
			foreach($object AS $item)
			{
				$ul .= '<li class="long-name">';
				$ul .= $item->name;

				$ul .= getSubOrgChart($item->id);

				$ul .= "</li>";

			}

			$ul .= "</ul>";
			//$ul .= "</li>";
		}

		return $ul;


	}

    // Explode date and convert it to miladi 
    function convertDate($date=0,$type='0',$order='default')
    {

        if($date!=0 && $type!='0')
        {
            $date = explode('-',$date); 
            if($type=='to_miladi')
            {
                return dateToMiladi($date[2],$date[1],$date[0]);                 
            }
            else
            { 
               if($order=='default') 
                 return dateToShamsi($date[2],$date[1],$date[0]); 
               else
                return dateToShamsi($date[0],$date[1],$date[2]); 
            }

        }
        else 
        {
            return false; 
        }
	} 
    
    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2020-01-28 10:59:16 
     * @Desc: Change date and time   
     */
    function convertDateAndTime($datetime,$type='to_miladi')
    {
        $datetime  = explode(" ",$datetime);
        $date = $datetime[0] ;
        $time = (isset($datetime[1])? $datetime[1]: '');
        if($date!=0 && $type!='0')
        {
            $date = explode('-',$date); 
            if($type=='to_miladi')
            {
                return dateToMiladi($date[2],$date[1],$date[0]).' '.$time;        
            }
            else
            {
                return dateToShamsi($date[0],$date[1],$date[2]).' '.$time; 
            }

        }
        else 
        {
            return false; 
        }

    }

	function convertToShamsi($y,$m,$d)
	{
		$converted = Dateconverter::GregorianToJalali($y,$m,$d);
		$month = $converted[1]; 
		$day   = $converted[2]; 
		if(strlen($converted[1])<2)
			$month = '0'.$converted[1]; 

		if(strlen($converted[2])<2)
			$day = '0'.$converted[2]; 

			return $converted[0].'-'.$month.'-'.$day;
	}

	//---- get department trees end ------------------------//


	//convert miladi date to shamsi (2015-01-01 to 1394-01-01)
	function dateToShamsi($y,$m,$d)
	{
		$converted = Dateconverter::GregorianToJalali($y,$m,$d);
		return $converted[0].'-'.sprintf("%02d",$converted[1]).'-'.sprintf("%02d",$converted[2]);
	}
	function dateToMiladi($y,$m,$d)
	{
		//$converted = Dateconverter::JalaliToGregorian($y,$m,$d);
		$converted = jdatetime::toGregorian($y,$m,$d);
		$month = $converted[1];
		$day = $converted[2];
		if($converted[1]<10)
		{
			$month = '0'.$converted[1];
		}
		if($converted[2]<10)
		{
			$day = '0'.$converted[2];
		}
		return $converted[0].'-'.$month.'-'.$day;
	}
	//get user profile picture
	function getProfilePicture($id=0,$byEmployee=false)
	{
		$custom_profile = DB::table('users')->where('id',$id)->pluck('photo');

		if($byEmployee == true)
		{
			$emp_id = DB::table('users')->where('id',$id)->pluck('employee_id');
		}
		else
		{
			$emp_id = Auth::user()->employee_id;
		}

		if(file_exists(public_path('documents/profile_pictures/'.$custom_profile)) && $custom_profile != "" && Auth::user()->use_custom_photo == 1)
		{
			$photo = $custom_profile;
		}
		else
		{
			$photo = DB::connection('hr')->table('employees')->where('id',$emp_id)->pluck('photo');
		}

		if($photo == '' || $emp_id == 0 || $custom_profile == "")
        {
            $photo = "default.jpeg";
        }
        elseif(!file_exists(public_path('documents/profile_pictures/'.$photo)))
        {
           $photo = "default.jpeg"; 
        }

        return $photo;
	}

	//get sub department by parent id
	function getSubDepartment($id=1)
	{
		return DB::table('department')->where('parent',$id)->where('unactive',0)->get();
	}
	//get all sub departments ids
	function getSubDepartmentIds($id=0,$includeParent=false)
	{
		$object = DB::table('department')->where('parent',$id)->where('unactive',0)->get();
		$ids = array();
		foreach($object AS $item)
		{
			$ids[] = $item->id;
		}

		if($includeParent)
		{
			$ids[] = getMyDepartmentId();
		}

		//return the ids
		return $ids;

	}

	function getSubTree($id=0)
	{
		return DB::table('document_trees')->where('parent',$id)->get();
	}

	function getModuleName($id=0)
	{
		return DB::table('module')->where('id',$id)->pluck('name');
	}

	function getFolderName($id=0)
	{
		return DB::table('document_trees')->where('id',$id)->pluck('name');
	}
	function getFileRecords($id=0)
	{
		return DB::table('document_records')->where('tree_id',$id)->get();
	}

	function getRecordFiles($id=0)
	{
		return DB::table('document_files')->where('document_id',$id)->get();
	}

	function getLastTableId($table='')
	{
		return DB::table('document_files')->orderBy('id', 'desc')->pluck('id');
	}

	function getModuleNameByCode($code='')
	{
		return DB::table('module')->where('code',$code)->pluck('name');
	}
	function getFileName($id=0)
	{
		return DB::table('document_files')->where('id',$id)->pluck('file_name');
	}
	function getSectionName($id='')
	{
		return DB::table('entity')->where('id',$id)->pluck('name_'.Session::get('lang'));
	}

	function getLang()
	{
		return getLangShortcut();
	}

	function setSessionRoles()
	{
		$RoleX = new \App\models\RoleX();
		//get user id from auth library
		$user_id = Auth::user()->id;
		//get user role (modules)
		$user_modules = $RoleX::getUserModules($user_id);

		//get user role (sections)
		$user_sections = $RoleX::getUserSections($user_id);

		//get user roles
		$user_roles = $RoleX::getUserRoles($user_id);

		//foreach user modules ---------------------------//
		$modules = array();
		$moduleNames = array();

		foreach($user_modules AS $user_module)
		{
			$modules[] = $user_module->module_code;
			$moduleNames[$user_module->module_code] = $user_module->module_name;
		}

		Session::put('user_modules',$modules);
		Session::put('module_names',$moduleNames);
		//------------------------------------------------//

		//foreach user sections --------------------------//
		$sections = array();

		foreach($user_sections AS $user_section)
		{
			$sections[] = $user_section->section_code;
		}

		Session::put('user_sections',$sections);
		//------------------------------------------------//

		//foreach user roles -----------------------------//
		$roles = array();

		foreach($user_roles AS $user_role)
		{
			$roles[$user_role->module_code][$user_role->section_code][$user_role->role_code]= $user_role->role_code;
		}

		Session::put('user_roles',$roles);
		//------------------------------------------------//

		//check the user module if not null
		if(count(Session::get('user_modules'))>0)
		{
			//set the default session module
			$modules = Session::get('user_modules');
			Session::put('default_module',$modules[0]);
			//means user has access on some modules
			return TRUE;
		}
		else
		{
			//user has not access on some modules
			return FALSE;
		}

	}
	function getDefaultModuleName()
	{
		$code = getCurrentAppCode();//getCurrentAppCode();
		if($code == 'manage')
		{
			$code = "auth";
		}

		$names = Session::get('module_names');
		if(count($code)>0 && count($names)>0)
		{
			return $names[$code];
		}
		else
		{
			return "No Application";
		}
	}

	function hasModule($module='')
	{
		if(in_array($module, Session::get('user_modules')))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function hasSection($section='')
	{
		if(in_array($section, Session::get('user_sections')))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

	}
	function canAdd($section='')
	{

		$default_module = getCurrentAppCode();//getCurrentAppCode();

		$roles = Session::get('user_roles');

		if(isset($roles[$default_module][$section]['add']))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function canEdit($section='')
	{
		$default_module = getCurrentAppCode();//getCurrentAppCode();

		$roles = Session::get('user_roles');

		if(isset($roles[$default_module][$section]['edit']))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function canDelete($section='')
	{

		$default_module = getCurrentAppCode();

		$roles = Session::get('user_roles');

		if(isset($roles[$default_module][$section]['delete']))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function canView($section='')
	{

		$default_module = getCurrentAppCode();

		$roles = Session::get('user_roles');

		if(isset($roles[$default_module][$section]['view']))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function canArchive($section='')
	{
		$default_module = getCurrentAppCode();

		$roles = Session::get('user_roles');

		if(isset($roles[$default_module][$section]['archive']))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function canAddDepartment($section='executive_management_list')
	{
		$default_module = getCurrentAppCode();

		$roles = Session::get('user_roles');

		if(isset($roles[$default_module][$section]['add_dep']))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function isAdmin()
	{

		if(Auth::user()->is_admin == '1')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function isManager()
	{

		if(Auth::user()->is_manager == '1')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	//get menus
	function putLeft()
	{
		//user left sections
		$User = new App\models\User;
		$sections = $User::getUserLeft(Auth::user()->id,getCurrentAppCode());

		$currentRoute = Route::currentRouteName();

		$lang = getLangShortcut();

		$li = "";
		foreach ($sections AS $item)
		{
			$name = "name_".$lang;

			if($currentRoute == $item->url_route)
			{
				$li .= "<li class='active'>";
			}
			else
			{
				$li .= "<li>";
			}

			if($item->url_route == '#')
			{
				$li .= "<a href='#'>
					<i class='".$item->fa_class_icon."'></i>
					<span class='hidden-xs'>".$item->$name."
					</span></a>";
			}
			else
			{
				$li .= "<a href='".URL::route($item->url_route)."'>
						<i class='".$item->fa_class_icon."'></i>
						<span class='hidden-xs'>".$item->$name."
						</span></a>";
			}
			$li .= "</li>";

		}

		echo $li;

	}

	//get menus
	function getSubMenus($appCode='')
	{
		//user left sections
		$User = new App\models\User;
		$sections = $User::getUserLeft(Auth::user()->id,$appCode);

		$currentRoute = Route::currentRouteName();

		$lang = getLangShortcut();

		$li = "";
		foreach ($sections AS $item)
		{
			$name = "name_".$lang;

			if($currentRoute == $item->url_route)
			{
				$li .= "<li class='active'>";
			}
			else
			{
				$li .= "<li>";
			}

			if($item->url_route == '#')
			{
				$li .= "<a href='#'>
					<i class='".$item->fa_class_icon."'></i>
					<span class='hidden-xs'>".$item->$name."
					</span></a>";
			}
			else
			{
				$li .= "<a href='".URL::route($item->url_route)."' onclick='loadloader();'>
						<i class='".$item->fa_class_icon."'></i>
						<span class='hidden-xs'>".$item->$name."
						</span></a>";
			}
			$li .= "</li>";

		}

		echo $li;

	}
	//set top menu for current application
	function setTopAsAppmenu($appCode='hr')
	{
		//user left sections
		$User = new App\models\User;
		$sections = $User::getUserLeft(Auth::user()->id,$appCode);

		$currentRoute = Route::currentRouteName();

		$lang = getLangShortcut();

		$li = "";
		foreach ($sections AS $item)
		{
			$name = "name_".$lang;

			if($item->url_route == '#')
			{
				$li .= '<li>
					            <a class="accordion-toggle" href="#">
					              <span class="sidebar-title">'.$item->$name.'</span>
					            </a>

		 		           </li>';
			}
			else
			{
				$li .= '<li>
				            <a class="accordion-toggle" href="'.URL::route($item->url_route).'" onclick="loadloader();">
				              <span class="sidebar-title">'.$item->$name.'</span>
				            </a>

	 		           </li>';

			}
		}

		echo $li;
	}

	//show warning to user
	function showWarning()
	{
		return Redirect::route('showWarning');
	}

	function getStaticTable($table="",$connection="auth",$selected = 0,$label=false)
	{

		$element = "<option value=''>---</option>";

		if($table != ""){
			if($label == true)
			{
				$name = DB::connection($connection)
						->table($table)
						->where('id',$selected)
						->pluck("name");
				return $name;
			}
			else
			{
				$object = DB::connection($connection)
							->table($table)
							->get();


				foreach($object AS $item)
				{
					$element .= "<option value='".$item->id."' ".($selected == $item->id?'selected':'').">".$item->name."</option>";
				}
			}
		}

		return $element;
	}

	function getJalaliMonthName($month)
	{
		return Dateconverter::monthname_shamsi($month);
	}
	// ================================================ Inventory database helper functions ====================================
	//get Product Types.
	function getProductTypes($selected_item=0)
	{
		$object = DB::connection('inventory')->table("product_type")->select('id','type');
		$items = "";
		//$items .= "<option value=''>Select</option>";
		foreach($object->get() AS $item)
		{
			$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->type.'</option>';
		}

		return $items;
	}
	//check the product type duplicate if it's not then insert it.
	function checkProductType($type="")
	{
		if($type != ""){
			$check = DB::connection('inventory')->table('product_type')->select('id')->where('type', $type)->exists();
			if(!$check)
			{
				$lastRecordId = DB::connection('inventory')->table('product_type')->insertGetId(array('type' => $type, 'created_at' => date('Y-m-d H:i:s')));
				return $lastRecordId;
			}
		}
	}
	//============================================ Procurement Inventory DB =========================================================

	//get the departments in which the inventory would belong to.
	function getDepartments($selected_item=0)
	{
		//query from auth static table
		$rows = DB::table('department')
			->select('id','name')->where('unactive',0)->get();

		$items = "";
		foreach($rows AS $item)
		{
			$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name.'</option>';
		}

		return $items;
	}
	function getPersonName()
	{
		//query from auth static table
		$rows = DB::connection('procurement_inventory')->table('inventory')->select('id','person_name')->groupBy('person_name')->get();

		$items = "";
		foreach($rows AS $item)
		{
			$items .= '<option value="'.$item->person_name.'">'.$item->person_name.'</option>';
		}

		return $items;
	}
	//get Product Types.
	function getProcInventoryProductTypes($selected_item=0)
	{
		$object = DB::connection('procurement_inventory')->table("product_type")->select('id','type');
		$items = "";
		//$items .= "<option value=''>Select</option>";
		foreach($object->get() AS $item)
		{
			$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->type.'</option>';
		}

		return $items;
	}
	//get product type name based on its id.
	function getProductTypeName($id)
	{
		$type_name = DB::connection('procurement_inventory')->table("product_type")->where('id',$id)->pluck('type');
		return $type_name;
	}
	//check procurement inventory's product type duplicate if it's not then insert it.
	function checkProcInventoryProductType($type="")
	{
		if($type != ""){
			$check = DB::connection('procurement_inventory')->table('product_type')->select('id')->where('type', $type)->exists();
			if(!$check)
			{
				$lastRecordId = DB::connection('procurement_inventory')->table('product_type')->insertGetId(array('type' => $type, 'created_at' => date('Y-m-d H:i:s')));
				return $lastRecordId;
			}
		}
	}
	// =================================================== Estate Registry Database Helper Functions =================================
	function getEstageRegistryLocations()
	{
		//query from auth static table
		$rows = DB::connection('const_maintenance')->table('locations')->select('location')->groupBy('location')->get();

		$items = "";
		foreach($rows AS $item)
		{
			if($item->location != "")
			$items .= '<option value="'.$item->location.'">'.$item->location.'</option>';
		}

		return $items;
	}
	//added by wasi
	//get latest from or to date
	function getLatestDate($date)
	{
		$date_range= \DB::connection('specification')->table('specification.data_range')->orderBy('id','desc')->first();
		if($date=='from') {
			return $date_range->from_date;
		}
		else if($date=='to'){
			return $date_range->to_date;
		}else if($date=='year'){
			return $date_range->year;
		}

	}
	//specification date range as mali year
	function getCurrentJalaliYearStartDateMaliYear($j_year="")
	{
		if($j_year == "")
		{
			// get current jalalai date's year
			$j_y = explode("-", toGregorian(date('Y-m-d')));
			$j_y = $j_y[0];
		}
		else
		{
			$j_y = $j_year;
		}

		$start_j_year = toGregorian($j_y."-10-01");

		return $start_j_year;
	}
	// get the end date of the jalali year based on parameter.
	function getCurrentJalaliYearEndDateMaliYear($j_year="")
	{
		if($j_year == "")
		{
			// get current jalalai date's year
			$j_y = explode("-", toGregorian(date('Y-m-d')));
			$j_y = $j_y[0];
		}
		else
		{
			$j_y = $j_year;
		}

		if(isItLeapYear($j_y))
		{
			$end_j_year = toGregorian($j_y."-9-30");
		}
		else
		{
			$end_j_year = toGregorian($j_y."-9-30");
		}
		return $end_j_year;
	}
	//executive management function helper
	function getElapsedDate($date)
	{

			$date=\Carbon\Carbon::parse($date);
	        $now=\Carbon\Carbon::now();
	        $diff = $date->diffInDays($now);
	        return $diff;

	}
	function getLog($table_name,$row_id,$operation){

	 $log= new logActivity();

	 $log->table=$table_name;
	 $log->record_id=$row_id;
	 $log->action=$operation;
	 $log->user_id=Auth::user()->id;
	 $log->user_name=Auth::user()->first_name;
	 $log->save();

 }

 /** 
  * @Author: Jamal  
  * @Date: 2019-11-11 09:59:51 
  * @Desc: Get User department   
  */ 

  function getUserDepartment($id=0)
  {
    $table = DB::connection('hr')
            ->table('employees as e')
            ->select('e.department','e.general_department')
            ->leftjoin('auth.users AS u','u.employee_id','=','e.id')
            ->where('u.id',$id)
            ->first();
    DB::disconnect('hr');
    return $table;
  }

  /** 
   * @Author: Jamal Yousufi 
   * @Date: 2019-11-12 11:22:29 
   * @Desc: Get User department Tree  
   */  

   function getUserDepartmentTree($user_id)
   {
        $department_id = getUserDepartment($user_id)->department; 
        $user_dept_tree = [];
        $i = 0;   
        while($department_id)
        {
            $department_parent = DB::table('department')->where('id',$department_id)->where('unactive',0)->first();
            $department_id     = $department_parent->parent; 

            $user_dept_tree[] = [
                'name_dr'    => $department_parent->name, 
                'name_pa'    => $department_parent->name_pa, 
                'name_en'    => $department_parent->name_en, 
            ];    
            $i++; 
        }
        return array_reverse($user_dept_tree);
   }


    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2019-11-13 16:50:42 
     * @Desc:  Get education degree of employee 
     */    
    function getEmployeeEducationDegree($employee_id)
    {
        $education =  DB::connection('hr')
                      ->table('employee_educations as edu')
                      ->leftjoin('auth.education_degree AS degree','degree.id','=','edu.education_id')
                      ->where('edu.employee_id',$employee_id)
                      ->orderBy('edu.id','desc')
                      ->first(); 
        return $education; 
    }

    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2019-11-14 10:29:42 
     * @Desc: remove duplicate object from array  
     */    

     function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 

        //convert objects to arrays, in_array() does not support objects
        if (is_object($array))
            $array = (array)$array;
        
        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 

    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2019-11-14 15:01:24 
     * @Desc: Get Language from session   
     */    
    function getLanguage()
    {
        return Session::get('lang'); 
    }

    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2019-11-18 10:30:30 
     * @Desc: Get Column Name  
     */    
    function getColumn($table='',$cond=array(),$column_name='',$conn='auth')
    {
        $result = DB::connection($conn)->table($table)->where($cond)->first();
        if(count($result)>0)
        {
            return $result->{$column_name}; 
        }
        else{
            return false; 
        }
        
    }

     /** 
     * @Author: Jamal Yousufi  
     * @Date: 2021-01-11 15:23:04 
     * @Desc: Get the user sub department   
     */
    function getUserSubDepartment()
	{	
      $user_id = Auth::user()->id; 
      $sub_department = DB::table('user_dept')->select('dept_id')->where('user_id',$user_id)->where('type','sub_dept')->get();
      return json_decode(json_encode($sub_department), true); 
    }

    /** 
     * @Author: Jamal Yousufi  
     * @Date: 2021-04-07 13:59:46 
     * @Desc:  
     */
    function encodeBasePath($path)
    {
        if($path!='' && !empty($path) && $path!='/img/default.jpeg')
        {
         $imageData = base64_encode(file_get_contents($path));
         $image_encoded_path = 'data:image/jpg;base64,'.$imageData;
         return $image_encoded_path;
        }
        else
        {
            return $path;
        }
    }
?>
