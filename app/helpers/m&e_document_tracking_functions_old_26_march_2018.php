<?php 
use App\library\Dateconverter;
use App\library\jdatetime;

	
	// check if the logged in user is the Director of M&E Directorate.
	function isMEDirector($section='', $role='')
	{
		// checking the user role nd entry previlliges.
	
		$default_module = getCurrentAppCode();
		$roles = Session::get('user_roles');

		if(isset($roles[$default_module][$section][$role]))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

	}
	// check if the logged in user is the Executive Manager of M&E Directorate.
	function isMEExecutiveManager($section='', $role='')
	{
		// checking the user role nd entry previlliges.
	
		$default_module = getCurrentAppCode();
		
		$roles = Session::get('user_roles');
		if(isset($roles[$default_module][$section][$role]))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	// check if the logged in user is the one of the experts in M&E Directorate.
	function isMEExpert($section='', $role='')
	{
		// checking the user role nd entry previlliges.
	
		$default_module = getCurrentAppCode();
		
		$roles = Session::get('user_roles');
		if(isset($roles[$default_module][$section][$role]))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function getLastRecordId($table)
	{
		$last_record_id = DB::connection('document_tracking')->table($table)->orderBy('id', 'DESC')->pluck('id');
		if(!empty($last_record_id)){return $last_record_id;}else{return 0;}	
	}
	// get the record name based on sent id as parameter.
	function getRecordFieldBasedOnID($table,$field,$id=0)
	{
		$record_name = DB::connection('document_tracking')->table($table)->where('id', $id)->pluck($field);
		if(!empty($record_name)){return $record_name;}else{return "";}	
	}
	// get the document operation type whether it is saved or issued based on incoming document id.
	function getOperation($incoming_doc_id)
	{
		$record = DB::connection('document_tracking')->table('issued_docs')->where('incoming_doc_id', $incoming_doc_id)->where('deleted', 0)->pluck('operations');
		if(!empty($record)){return $record;}else{return "";}	
	}
	// get the copy of references based on sent id as parameter.
	function getCopyToReferences($issued_doc_id=0)
	{
		$copy_to_references = DB::connection('document_tracking')->table('copy_to_references')->where('issued_doc_id', $issued_doc_id)->get();
		if(!empty($copy_to_references)){return $copy_to_references;}else{return "";}	
	}

	function getOrganizations($selected_item=0)
	{
		$sources = DB::connection('document_tracking')->table('ministries')->select('id','name_dr')->groupBy('name_dr')->get();
		if($sources)
		{
			$options = "";
			foreach ($sources as $items) {
				if($items->name_dr != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name_dr."</option>";	
				}
			}
			return $options;
		}
	}

	function getDocumentFileName($doc_id)
	{
		$file_name = DB::connection('document_tracking')->table('uploads')->where('issued_doc_id', $doc_id)->get();
		if($file_name){ return $file_name;} else{ return false;}
	}

	//get the list of pending incoming numbers of incoming documents.
	function getPendingIncomingNumbers($selected_item=0)
	{
		$pending_incoming_nums = DB::connection('document_tracking')->table('incoming_docs')->select('id','incoming_number')->where('doc_status', 0)->where('incoming_number','!=',0)->where('deleted',0)->get();
		if($pending_incoming_nums)
		{
			$options = "";
			foreach ($pending_incoming_nums as $items) {
				if($items->incoming_number != "")
				{
					$options .= "<option ".($items->incoming_number == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->incoming_number."</option>";	
				}
			}
			return $options;
		}
	}
	//get the list of all incoming numbers of incoming documents.
	function getAllIncomingNumber($selected_item=0)
	{
		$incoming_numbers = DB::connection('document_tracking')->table('incoming_docs')->select('id','incoming_number')->where('incoming_number','!=',0)->where('deleted',0)->get();
		if($incoming_numbers)
		{
			$options = "";
			foreach ($incoming_numbers as $items) {
				if($items->incoming_number != "")
				{
					$options .= "<option ".($items->incoming_number == $selected_item ? 'selected':'')." value='".$items->incoming_number."'>".$items->incoming_number."</option>";	
				}
			}
			return $options;
		}
	}
	//get the list of Experts in M&E directorate.
	function getMEExperts($selected_item=0)
	{
		$employees_id = array('1791','1811','1844','1793','1806','3000','3728','1809','3956','1871','3603','165','166','4335','3909','1797','1884','1895','1874','1878','1890','3656','170','180','4643','1897');
		$me_experts = DB::connection('hr')->table('employees')->select('id',DB::raw("concat(`name_dr`,' ',`last_name`) as emp_name"))->where('department',433)->whereIn('id', $employees_id)->get();
		if($me_experts)
		{
			$options = "";
			foreach ($me_experts as $items) {
				if($items->emp_name != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->emp_name."</option>";	
				}
			}
			return $options;
		}
	}

	// get document type.
	function getMEDocumentType($selected_item=0)
	{
		$doc_type = DB::connection('document_tracking')->table('document_type')->select('id','name')->orderBy('id','asc')->groupBy('name')->get();
		if($doc_type)
		{
			$options = "";
			foreach ($doc_type as $items) {
				if($items->name != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name."</option>";	
				}
			}
			return $options;
		}
	}
	// get document type name based on sent parameter.
	function getMEDocumentTypeName($document_type_id)
	{
		$doc_type_name = DB::connection('document_tracking')->table('document_type')->where('id', $document_type_id)->pluck('name');
		if($doc_type_name != ""){ return $doc_type_name;}
		else{ return "";}
	}
	// get the assignee based on incoming document id.
	function getAssignee($incoming_doc_id)
	{
		$assignee = DB::connection('document_tracking')->table('assignees')->where('incoming_doc_id', $incoming_doc_id)->pluck('assignee_id');
		if($assignee){ return $assignee;}
		else{ return "";}
	}
	// get the assignee based on issued document id.
	function getIssuedDocAssignee($issued_doc_id)
	{
		$assignee = DB::connection('document_tracking')->table('assignees')->where('issued_doc_id', $issued_doc_id)->pluck('assignee_id');
		if($assignee){ return $assignee;}
		else{ return "";}
	}
	// get the assignee description based on the employee id and incoming document id.
	function getAssigneeDescription($incoming_doc_id)
	{
		$assignee_description = DB::connection('document_tracking')->table('assignees')->where('incoming_doc_id', $incoming_doc_id)->pluck('assignee_description');
		if($assignee_description){ return $assignee_description;}
		else{ return "";}
	}
	// get the assignee description based on the employee id and incoming document id.
	function getAssigneeName($assignee_id)
	{
		$assignee_name = DB::connection('hr')->table('employees')->select(DB::raw("CONCAT(`name_dr`,' ',`last_name`) as assignee_name"))->where('id', $assignee_id)->first();
		if($assignee_name){ return $assignee_name->assignee_name;}
		else{ return "";}
	}
	// get reports based on sent parameters.
	function getMEStatsBasedOnParameters($table,$condition_field="",$condition_field_value="")
	{
		if($condition_field == "" || $condition_field_value == "")
			$record = DB::connection('document_tracking')->table($table)->where('deleted',0);
		elseif($condition_field_value == 0)
			$record = DB::connection('document_tracking')->table($table)->where($condition_field, $condition_field_value)->where('deleted',0);
		else
			$record = DB::connection('document_tracking')->table($table)->where($condition_field, $condition_field_value)->where('deleted',0);

		if(!isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs') && !isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$record->rightjoin('assignees as ass','ass.incoming_doc_id','=', "$table.id");
			$record->leftjoin('hr.employees as emp','emp.id','=','ass.assignee_id');
			$record->where('ass.assignee_id', Auth::user()->employee_id);
		}

		if($record->count() > 0){ return $record->count();}
		else{ return 0;}
	}
	function getMESavedDocumentStats()
	{
		$record = DB::connection('document_tracking')->table('issued_docs as issd')->where('issd.operations', 1)->where('issd.deleted',0);
		if(!isMEDirector('document_tracking_issued_docs', 'm&e_director_issued_docs') && !isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$record->rightjoin('assignees as ass','ass.incoming_doc_id','=','issd.incoming_doc_id');
			$record->leftjoin('hr.employees as emp','emp.id','=','ass.assignee_id');
			$record->where('ass.assignee_id', Auth::user()->employee_id);
		}
		if($record->count() > 0){ return $record->count();}
		else{ return 0;}
	}
	// get issued docs with or without incoming documents based on parameters.
	function getMEIssuedDocumentStats($issue_type)
	{
		if($issue_type == 1)
		{
			//issued documents with incoming document.
			$record = DB::connection('document_tracking')->table('issued_docs as issd')->where('issd.operations', 2)->where('issd.incoming_number','!=',0)->where('issd.incoming_doc_id','!=',0)->where('issd.deleted',0);
			if(!isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs') && !isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
			{
				// list the incoming documents based on specific M&E experts.
				$record->rightjoin('assignees as ass','ass.incoming_doc_id','=','issd.incoming_doc_id');
				$record->leftjoin('hr.employees as emp','emp.id','=','ass.assignee_id');
				$record->where('ass.assignee_id', Auth::user()->employee_id);
			}
		}
		else
		{
			//issued documents without incoming document.
			$record = DB::connection('document_tracking')->table('issued_docs as issd')->where('issd.operations', 2)->where('issd.incoming_number',0)->where('issd.incoming_doc_id',0)->where('issd.deleted',0);

			if(!isMEDirector('document_tracking_issued_docs', 'm&e_director_issued_docs') && !isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
			{
				// list the incoming documents based on specific M&E experts.
				$record->rightjoin('assignees as ass1','ass1.issued_doc_id','=','issd.id');
				$record->leftjoin('hr.employees as issd_emp','issd_emp.id','=','ass1.assignee_id');
				$record->where('ass1.assignee_id', Auth::user()->employee_id);
			}
		}

		if($record->count() > 0){ return $record->count();}
		else{ return 0;}
	}
?>
