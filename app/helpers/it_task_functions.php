<?php 
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
//---- get group task trees ---------------------------//
function getTaskTree_it($mode='all',$week=0,$year = 0)
{
	$table = DB::connection('helpdesk')->
				table('task_group AS tg');
				$table->select('tg.title','tg.id AS group_id','tg.user_id','tg.created_at');
				/*
				if(!isAdmin())
				{
					$table->whereRaw('(tg.access_level = 1 AND tg.dep_id = '.Auth::user()->department_id.') OR (tg.access_level = 1 AND tg.user_id = '.Auth::user()->id.') OR tg.user_id = '.Auth::user()->id.'');
				}
				*/
				$table->where('access_level',1);

	$object =	$table->get();
	
	$ul = '';
	$counter = 1;
	foreach($object AS $item)
	{
		/*
		//query for pagination count
		$t2 = DB::connection('helpdesk')
				->table('tasks AS t')
				->select(
					't.title',
					't.id AS task_id',
					't.user_id',
					't.start_date',
					't.end_date',
					't.created_at'
					)
				->where('task_group_id',$item->group_id)
				->where('parent_task',0)
				->where(DB::raw('YEAR(start_date)'),$year);
				
			if(!isAdmin())
			{
				if($mode == 'my')
				{	
					$t2->join('task_assigned_to AS ta','ta.task_id','=','t.id');
					$t2->where('ta.assigned_to',Auth::user()->id);
				}
				elseif($mode == 'out')
				{
					$t2->Where('t.user_id',Auth::user()->id);
				}
				else
				{
					$t2->join('task_assigned_to AS ta','ta.task_id','=','t.id');
					$t2->where('ta.assigned_to',Auth::user()->id);
					$t2->orWhere('t.user_id',Auth::user()->id);
				}
			}

		//$data['pages'] = round((count($t2->get()))/3);
		 */
		$data['item']=$item;
		$data['mode'] = $mode;
		$data['week'] = $week;
		$data['year'] = $year;
		$ul.= View('it_task.partial_views.grouptask',$data);

		$counter++;
	}
	//$ul.= View('task.partial_views.completed_group');
	//return the result of query
	//echo "<pre>";print_r(htmlspecialchars($ul));
	return $ul;
}
//get task group
function getTaskGroupTask_it($group_id=0,$page='0',$mode='all')
{
	$sdate = getFirstWeek_it(date('Y'),date('W'),'s');//get the start day of current week
	$edate = getFirstWeek_it(date('Y'),date('W'),'e');//get the end day of current week
	
	$table = DB::connection('helpdesk')
				->table('tasks AS t')
				->select(
					't.title',
					't.id AS task_id',
					't.user_id',
					't.start_date',
					't.end_date',
					't.created_at'
					)
				->where('task_group_id',$group_id)
				->where('parent_task',0)//level one
				->where('completed',0)
				->whereRaw('(((t.end_date < "'.date("Y-m-d").'") OR ((t.end_date >= "'.$sdate.'" AND t.end_date <= "'.$edate.'") OR (t.start_date >= "'.$sdate.'" AND t.start_date <= "'.$edate.'"))) OR (t.start_date <= "'.$sdate.'" AND t.end_date >= "'.$edate.'"))')
				//->where('t.end_date','<',date('Y-m-d'))
				->groupBy('t.id')
				->orderBy('t.end_date');
				//->take(3);
			
	if(!isAdmin())
	{
		if($mode == 'my')
		{
			$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
			//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
			$table->where(function($q){
				$q->whereRaw('(ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2)');		
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->whereRaw('(sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2)');
	           });
			});
		}
		elseif($mode == 'out')
		{
			$table->where(function($q){
				$q->Where('t.user_id',Auth::user()->id);
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    //->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->Where('tasks.user_id',Auth::user()->id);
	           });
			});
		}
		else
		{
			$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
			//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
			$table->where(function($q){
				$q->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');		
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->whereRaw('((sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2) or tasks.user_id = '.Auth::user()->id.')');
	           });
			});
			//$table->leftJoin('task_assigned_to AS ta',function($join){
				//$join->on('ta.task_id','=','t.id');
				//$join->on('ta.assigned_to','=',DB::raw(Auth::user()->id));
			//});
			//$table->whereRaw('(ta.assigned_to = '.Auth::user()->id.' or t.user_id = '.Auth::user()->id.')');
			//$table->where('ta.status','!=',2);
			//$table->orWhere('t.user_id',Auth::user()->id);
		}
	}
	$object = $table->get();
	
	$get_last_id = 0;

	$data['page'] = $page;
	$data['object'] = $object;
	$data['group_id'] = $group_id;
	$data['mode'] = $mode;
	
	return View('it_task.partial_views.tasks',$data);

}
//get all tasks of selected week
function getTaskGroupTask_week_it($group_id=0,$page='0',$mode='all',$week=0,$year=0)
{
	if(isShamsiDate())
	{
		$sdate = getFirstWeek_shamsi_it($year,$week,'s');
		$edate = getFirstWeek_shamsi_it($year,$week,'e');
	}
	else
	{
		$sdate = getFirstWeek_it($year,$week,'s');
		$edate = getFirstWeek_it($year,$week,'e');
	}
	//echo $sdate.'-'.$edate;exit;
	$table = DB::connection('helpdesk')
				->table('tasks AS t')
				->select(
					't.title',
					't.id AS task_id',
					't.user_id',
					't.start_date',
					't.end_date',
					't.created_at'
					)
				->where('task_group_id',$group_id)
				->where('parent_task',0)//level one
				->where('percentage','!=','100')
				->whereRaw('(((t.end_date >= "'.$sdate.'" AND t.end_date <= "'.$edate.'") OR (t.start_date >= "'.$sdate.'" AND t.start_date <= "'.$edate.'")) OR (t.start_date <= "'.$sdate.'" AND t.end_date >= "'.$edate.'"))')
				//->where('t.end_date','>=',$sdate)
				//->where('t.end_date','<=',$edate)
				//->where('t.end_date','>=',date('Y-m-d'))//due tasks not included
				->groupBy('t.id')
				->orderBy('t.end_date','desc');
				//->take(3);		
	if(!isAdmin())
	{
		if($mode == 'my')
		{
			$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
			//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
			$table->where(function($q){
				$q->whereRaw('(ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2)');		
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->whereRaw('(sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2)');
	           });
			});
		}
		elseif($mode == 'out')
		{
			$table->where(function($q){
				$q->Where('t.user_id',Auth::user()->id);
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    //->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->Where('tasks.user_id',Auth::user()->id);
	           });
			});
		}
		else
		{
			$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
			//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
			$table->where(function($q){
				$q->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');		
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->whereRaw('((sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2) or tasks.user_id = '.Auth::user()->id.')');
	           });
			});
		}
	}
	$object = $table->get();
	
	$get_last_id = 0;

	$data['page'] = $page;
	$data['object'] = $object;
	$data['group_id'] = $group_id;
	$data['mode'] = $mode;
	
	return View('it_task.partial_views.tasks',$data);
}
//get all completed tasks
function getTaskGroupTask_completed_it()
{
	$table = DB::connection('helpdesk')
				->table('tasks AS t')
				->select(
					't.title',
					't.id AS task_id',
					't.user_id',
					't.start_date',
					't.end_date',
					't.created_at'
					)
				//->where('task_group_id',$group_id)
				->where('parent_task',0)//level one
				->where('percentage','=','100')
				//->whereRaw('(((t.end_date >= "'.$sdate.'" AND t.end_date <= "'.$edate.'") OR (t.start_date >= "'.$sdate.'" AND t.start_date <= "'.$edate.'")) OR (t.start_date <= "'.$sdate.'" AND t.end_date >= "'.$edate.'"))')
				
				->groupBy('t.id')
				->orderBy('t.end_date','desc');
				//->take(3);		
				$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
				//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
				$table->where(function($q){
					$q->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');		
					$q->orWhereIN('t.id',function($query)
		           {
		                $query->select('tasks.parent_task')
							->from('tasks')
		                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
							->where('tasks.parent_task','!=',0)
		                    ->whereRaw('((sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2) or tasks.user_id = '.Auth::user()->id.')');
		           });
				});
	$object = $table->get();
	
	$get_last_id = 0;

	$data['page'] = 0;
	$data['object'] = $object;
	$data['group_id'] =0;
	$data['mode'] = 'all';
	
	return View('it_task.partial_views.completed_tasks',$data);
}
//get task group by id
function getTaskGroup_it($group_id=0,$page='0',$mode='all')
{
	$table = DB::connection('helpdesk')
				->table('tasks AS t')
				->select(
					't.title',
					't.id AS task_id',
					't.user_id',
					't.start_date',
					't.end_date',
					't.created_at'
					)
				->where('task_group_id',$group_id)
				->where('parent_task',0)//level one
				->where('percentage','!=','100')
				//->whereRaw('((t.end_date < "'.date("Y-m-d").'") OR ((t.end_date >= "'.$sdate.'" AND t.end_date <= "'.$edate.'") OR (t.start_date >= "'.$sdate.'" AND t.start_date <= "'.$edate.'")))')
				//->where('t.end_date','<',date('Y-m-d'))
				->groupBy('t.id')
				->orderBy('t.end_date')
				->take(Config::get('myConfig.record_per_page'));
				if($page != '0')
				{
					$table->skip($page*Config::get('myConfig.record_per_page'));
				}
			
	if(!isAdmin())
	{
		if($mode == 'my')
		{
			$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
			//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
			$table->where(function($q){
				$q->whereRaw('(ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2)');		
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->whereRaw('(sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2)');
	           });
			});
		}
		elseif($mode == 'out')
		{
			$table->where(function($q){
				$q->Where('t.user_id',Auth::user()->id);
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    //->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->Where('tasks.user_id',Auth::user()->id);
	           });
			});
		}
		else
		{
			$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
			//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
			$table->where(function($q){
				$q->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');		
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->whereRaw('((sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2) or tasks.user_id = '.Auth::user()->id.')');
	           });
			});
		}
	}
	$object = $table->get();
	//dd($object);
	$get_last_id = 0;

	$data['page'] = $page;
	$data['object'] = $object;
	$data['group_id'] = $group_id;
	$data['mode'] = $mode;
	
	return View('it_task.partial_views.tasks',$data);

}
//get task group by completed tasks
function getTaskGroup_completed_it($page='0')
{
	$table = DB::connection('helpdesk')
				->table('tasks AS t')
				->select(
					't.title',
					't.id AS task_id',
					't.user_id',
					't.start_date',
					't.end_date',
					't.created_at'
					)
				//->where('task_group_id',$group_id)
				->where('parent_task',0)//level one
				->where('completed',1)
				//->whereRaw('((t.end_date < "'.date("Y-m-d").'") OR ((t.end_date >= "'.$sdate.'" AND t.end_date <= "'.$edate.'") OR (t.start_date >= "'.$sdate.'" AND t.start_date <= "'.$edate.'")))')
				//->where('t.end_date','<',date('Y-m-d'))
				->groupBy('t.id')
				->orderBy('t.end_date')
				->take(Config::get('myConfig.record_per_page'));
				if($page != '0')
				{
					$table->skip($page*Config::get('myConfig.record_per_page'));
				}
				$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
				//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
				$table->where(function($q){
					$q->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');		
					$q->orWhereIN('t.id',function($query)
		           {
		                $query->select('tasks.parent_task')
							->from('tasks')
		                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
							->where('tasks.parent_task','!=',0)
		                    ->whereRaw('((sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2) or tasks.user_id = '.Auth::user()->id.')');
		           });
				});
	$object = $table->get();
	//dd($object);
	$get_last_id = 0;

	$data['page'] = $page;
	$data['object'] = $object;
	$data['group_id'] = 0;
	$data['mode'] = 'all';
	
	return View('it_task.partial_views.completed_tasks',$data);

}
function getSubTaskTree_it($parent=0,$mode='all')
{
	$table = DB::connection('helpdesk')
						->table('tasks AS t')
						->select(
								't.title',
								't.id AS task_id',
								't.user_id',
								't.start_date',
								't.end_date',
								't.created_at',
								't.parent_task'
								)
						->where('t.parent_task',$parent);
	//if(!isAdmin())
	//{
		if($mode == 'my')
		{
			$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
			//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
			$table->where(function($q){
				$q->whereRaw('(ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2)');		
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->whereRaw('(sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2)');
	           });
			});
		}
		elseif($mode == 'out')
		{
			$table->where(function($q){
				$q->Where('t.user_id',Auth::user()->id);
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    //->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->Where('tasks.user_id',Auth::user()->id);
	           });
			    $q->orWhereIN('t.parent_task',function($query)
	           {
	                $query->select('pt.id')
						->from('tasks AS pt')
	                    //->where('pt.id',$parent)
	                    ->where('pt.user_id',Auth::user()->id);
	           });
			});
		}
		else
		{
			$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
			//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
			$table->where(function($q){
				$q->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');		
				$q->orWhereIN('t.id',function($query)
	            {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->whereRaw('((sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2) or tasks.user_id = '.Auth::user()->id.')');
	            });
			    $q->orWhereIN('t.parent_task',function($query)
	            {
	                $query->select('pt.id')
						->from('tasks AS pt')
	                    //->where('pt.id',$parent)
	                    ->where('pt.user_id',Auth::user()->id);
	           });
			});
		}
	//}
	$table->groupBy('t.id');
	$object = $table->get();
	$ul = "";
	if(count($object)>0)
	{
        $data['object'] = $object;
        $data['mode'] = $mode;
		return View('it_task.partial_views.subtasks',$data);
	}
	//return the result of query
	
	return $ul;
}
function subTaskTree_count_it($parent=0,$mode='all')
{	
	$table = DB::connection('helpdesk')
						->table('tasks AS t')
						->select(
								't.title',
								't.id AS task_id',
								't.user_id',
								't.start_date',
								't.end_date',
								't.created_at'
								)
						->where('t.parent_task',$parent);
	if(!isAdmin())
	{
		if($mode == 'my')
		{
			$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
			//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
			$table->where(function($q){
				$q->whereRaw('(ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2)');		
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->whereRaw('(sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2)');
	           });
			});
		}
		elseif($mode == 'out')
		{
			$table->where(function($q){
				$q->Where('t.user_id',Auth::user()->id);
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    //->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->Where('tasks.user_id',Auth::user()->id);
	           });
			    $q->orWhereIN('t.parent_task',function($query)
	           {
	                $query->select('pt.id')
						->from('tasks AS pt')
	                    //->where('pt.id',$parent)
	                    ->where('pt.user_id',Auth::user()->id);
	           });
			});
		}
		else
		{
			$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
			//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
			$table->where(function($q){
				$q->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');		
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->whereRaw('((sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2) or tasks.user_id = '.Auth::user()->id.')');
	           });
			    $q->orWhereIN('t.parent_task',function($query)
	           {
	                $query->select('pt.id')
						->from('tasks AS pt')
	                    //->where('pt.id',$parent)
	                    ->where('pt.user_id',Auth::user()->id);
	           });
			});
		}
	}
	$object = $table->get();
	if(count($object)>0)
	{
        return TRUE;
	}
	else
	{
		return false;
	}
}
//check task group included in report
function isTaskGroupInReport_it($taskGroup=0)
{
	$in_report = DB::connection('helpdesk')->table('task_group')->where('id',$taskGroup)->pluck('in_report');
	//check if in report
	if($in_report == 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}
//get user Full Name
function getUserFullName_it($id=0)
{
	return DB::table('users')->where('id',$id)->pluck('first_name')." ".DB::table('users')->where('id',$id)->pluck('last_name');
}
//get user assigned ids for the task
function getTaskAssignees_it($task_id=0)
{
	return DB::connection('helpdesk')->table('task_assigned_to')->where('task_id',$task_id)->get();
}
//get sub tasks
function getSubTasks_it($task_id=0)
{
	return It_task_model::getSubTasks($task_id);
}
//check task status
function checkTaskApproval_it($task_id=0,$user=0)
{
	$status = DB::connection('helpdesk')
				->table('task_assigned_to')
				->where('task_id',$task_id)
				->where('assigned_to',$user)
				->pluck('status');
	//check task status
	if($status===2)
	{
		return "Rejected";
	}
	if($status === 1)
	{
		return "Approved";
	}
	else
	{
		return "";
	}
}
//get get sub task progress
function getTaskProgress_it($task_id=0)
{
	$objects = DB::connection('helpdesk')->table('task_progress')->where('task_id',$task_id)->get();
	$currentProgress = 0;
	foreach($objects AS $item)
	{
		if(($item->progress) > $currentProgress)
		{
			$currentProgress = $item->progress;
		}
	}
	return $currentProgress;
}
//get task progress summary details
function getTaskSummaryDetails_it($task_id=0)
{
	$objects = DB::connection('helpdesk')
				->table('task_progress')
				->where('task_id',$task_id)
				->orderBy('id','DESC')
				->get();
	
	//return the object
	return $objects;

}
//calculate progress according to sub tasks
function getSubTaskProgress_it($task_id=0)
{
	$objects = DB::connection('helpdesk')
				->table('tasks AS t1')
				->select('t1.percentage','t1.parent_task')
				//->leftJoin('helpdesk.task_progress as t2', 't2.task_id', '=', 't1.id')
				->where('t1.parent_task',$task_id)
				//->groupBy('t1.parent_task')
				->get();

	$currentProgress = 0;
	$counter = 0;
	foreach($objects AS $item)
	{
		$currentProgress += $item->percentage;
		$counter++;
	}
	if($counter != 0)
	{
		$final_progress = $currentProgress/$counter;
	}
	else
	{
		$final_progress = $currentProgress;
	}
	return $final_progress;
}
//get main task progress, with no sub task
function getMainTaskProgress_it($task_id=0)
{
	$objects = DB::connection('helpdesk')
				->table('tasks AS t1')
				->select('t1.percentage','t1.parent_task')
				//->leftJoin('helpdesk.task_progress as t2', 't2.task_id', '=', 't1.id')
				//->where('t1.parent_task',$task_id)
				->orWhere('t1.id',$task_id)
				//->groupBy('t2.task_id')
				->first();

	
	return $objects->percentage;
}
//get task comments
function getTaskComments_it($task_id=0)
{
	return DB::connection('helpdesk')
			->table('task_comments')
			->where('task_id',$task_id)
			->get();
}
function getTaskRejectedComments_it($task_id=0)
{
	return DB::connection('helpdesk')
			->table('task_comments')
			->where('task_id',$task_id)
			->where('type',1)
			->get();
}
//get task notifications
function getTaskNotifications_it($getCount = false)
{
	$object = DB::connection('helpdesk')
				->table('notifications AS t1')
				->select("t1.id AS notification_id","t1.to","t1.from","t1.notification_type","t1.task_id","t2.title","t1.created_at")
				->leftJoin("tasks AS t2","t2.id","=","t1.task_id")
				->where('to',Auth::user()->id)
				->where('t1.status',0)
				->get();
	//check if wanted count of notifications
	if($getCount)
	{
		return count($object);
	}
	else
	{
		$li = "";

		foreach($object AS $item)
		{
			$notification_type = "";
			$url = "";
			if($item->notification_type == 1)
			{
				$notification_type = '<span style="font-weight:normal;color:orange;">Assigned Task</span>';
				$url = URL::route('ViewReport_it',array($item->task_id,$item->notification_id));
			}
			elseif($item->notification_type == 2)
			{
				$notification_type = '<span style="font-weight:normal;color:red;">Rejected Task</span>';
				$url = URL::route('ViewReport_it',array($item->task_id,$item->notification_id));
			}
			elseif($item->notification_type == 3)
			{
				$notification_type = '<span style="font-weight:normal;color:green;">Approved Task</span>';
				$url = URL::route('ViewReport_it',array($item->task_id,$item->notification_id));
			}
			elseif($item->notification_type == 4)
			{
				$notification_type = '<span style="font-weight:normal;color:orange;">Commented On</span>';
				$url = URL::route('ViewReport_it',array($item->task_id,$item->notification_id));
			}
			elseif($item->notification_type == 5)
			{
				$notification_type = '<span style="font-weight:normal;color:green;">Completed Task</span>';
				$url = URL::route('ViewReport_it',array($item->task_id,$item->notification_id));
			}

	        //$li .= '<li class="item">';
	        $li .= '<a class="list-group-item" role="menuitem" href="'.$url.'"><div class="media"><div class="media-left padding-right-10">';
	       $image = getProfilePicture($item->from);
	       $li .= HTML::image('/img/'.$image,'',array('class' => 'notification-logo'));
	       $li .= '</div><div class="media-body">';
	        
		    $li .= '<h6 class="media-heading">
		                '.getUserFullName_it($item->from).' - '.$notification_type.'
		            </h6>';

	       $li .=  '<span class="content-text">'.$item->title.'</span>
	                </span>';
	       $li .= '<time class="media-meta"><i class="fa fa-clock-o"></i>'.$item->created_at.'</time>';
	       $li .= '</div></div></a>';
			//$li .= '</li>';
		}
		//return elements
		return $li;
	}
}

//get users who included in task
function getIncludedUsers_it($task_id=0)
{
	$getTaskCreator 	= DB::connection('helpdesk')->table('tasks')->where('id',$task_id)->pluck('user_id');
	$getTaskAssignees 	= DB::connection('helpdesk')->table('task_assigned_to')->where('task_id',$task_id)->get();
	$getWhoCommented 	= DB::connection('helpdesk')->table('task_comments')->where('task_id',$task_id)->get();
	
	$allUsers = array();
	if($getTaskCreator != Auth::user()->id)
	{
		$allUsers[] = $getTaskCreator;
	}
	
	foreach($getTaskAssignees AS $user)
	{
		if($user->assigned_to != Auth::user()->id)
		{
			$allUsers[] = $user->assigned_to;
		}
		if($user->assigned_by != Auth::user()->id)
		{
			$allUsers[] = $user->assigned_by;
		}
		
	}
	foreach($getWhoCommented AS $user)
	{
		if($user->user_id != Auth::user()->id)
		{
			$allUsers[] = $user->user_id;
		}
	}

	$allUsers = array_unique($allUsers);
	$allUsers = array_values($allUsers);

	return $allUsers;
}

function getTaskProgress4Complete_it($user_id=0,$getCount=false,$task_id=0)
{
	//initiate the list
	$li = "";
	
	//get assigned task notifications to me from database
	$table = DB::connection('helpdesk')
			->table('task_progress AS t1');

			$table->select(
					't2.title',
					't3.assigned_by',
					't2.id AS task_id',
					't2.created_at',
					't3.assigned_to'
					);

			$table->leftJoin('tasks AS t2','t2.id','=','t1.task_id');
			$table->leftJoin('task_assigned_to AS t3','t3.task_id','=','t1.task_id');
			//->where(DB::raw("t1.id = t2.task_id"))
			//$table->where('t3.assigned_by',$user_id);
			$table->where('t1.progress',100);
			$table->where('t1.status',0);
			if($task_id != 0)
			{
				$table->where('t1.task_id',$task_id);
			}
	$object = $table->get();
	
	foreach($object AS $item)
	{
        $li .= '<li class="item">';
        $li .= '<a href="'.URL::route('ViewReport_it',array($item->task_id)).'#task_completed">';
        $image = getProfilePicture($item->assigned_to);
        $li .= HTML::image('/img/'.$image,'',array('class' => 'notification-logo'));
        $li .= '<span class="content">';
        
	    $li .= '<span class="content-headline">
	                '.getUserFullName_it($item->assigned_to).' - <span style="font-weight:normal;color:green;">Completed Task</span>
	            </span>';

        $li .=  '<span class="content-text">'.$item->title.'</span>
                </span>';
        $li .= '<span class="time"><i class="fa fa-clock-o"></i>'.$item->created_at.'</span>';
        $li .= '</a>';
		$li .= '</li>';
	}
	
	if($getCount || $task_id != 0)
	{
		return count($object);
	}
	return $li;
}
//check sut task approvement
function isTaskCompleted_it($task_id=0)
{
	$table = DB::connection('helpdesk')
			->table('task_progress AS t1');
			
			//$table->where('t1.progress',100);
			$table->where('t1.status',1);//approved
			$table->where('t1.task_id',$task_id);
			
	$object = $table->get();
	if(count($object)>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
//check if main task with subtasks is 100% completed, it means it is approved
function isMainTaskApproved_it($task_id=0)
{
	$table = DB::connection('helpdesk')
			->table('tasks AS t1')
			->where('t1.parent_task',0)
			->where('t1.percentage',100)
			->where('t1.id',$task_id);
			
	$object = $table->get();
	
	if(count($object)>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
//check all sub tasks of main task is completed
function isMainTaskCompleted_withSubtasks_it($task_id=0)
{
	$tables = DB::connection('helpdesk')
		->table('tasks AS t1')
		
		->where('t1.percentage','!=',100)
		//->where('t1.status',0)//check unapporve
		->where('t1.parent_task',$task_id);
		
	$object = $tables->get();
	
	if(count($object)>0)
	{//at least one the sub tasks is nto completed yet
		return false;
	}
	else
	{
		return true;
	}
}
function isTaskPercentage_complete_it($task_id=0)
{
	$table = DB::connection('helpdesk')
			->table('tasks AS t1');
			
			$table->where('t1.percentage',100);
			//$table->where('t1.status',1);
			$table->where('t1.id',$task_id);
			
	$object = $table->get();
	if(count($object)>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function isTaskCreator_it($task_id=0)
{
	//get assigned task notifications to me from database
	$table = DB::connection('helpdesk')
			->table('tasks AS t1');
			$table->where('t1.user_id',Auth::user()->id);
			$table->where('t1.id',$task_id);
	$object = $table->get();

	if(count($object)>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function isGroupCreator_it($id=0)
{
	//get assigned task notifications to me from database
	$table = DB::connection('helpdesk')
			->table('task_group AS t1');
			$table->where('t1.user_id',Auth::user()->id);
			$table->where('t1.id',$id);
	$object = $table->get();

	if(count($object)>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function dmy_format_it($date)
{
	$gregorian_format = explode("-", $date);
	$jalali_format = "";
	
	if($date != '' && !empty($gregorian_format))
	{
	   $g_y = $gregorian_format[0];
	   $g_m = $gregorian_format[1];
	   $g_d = $gregorian_format[2];
	   $jalali_format = $g_d."-".$g_m."-".$g_y;
	}

   return $jalali_format;
}

// change the date format to Gregorian.
function ymd_format_it($date)
{
	$jalali_format = explode("-", $date);
	$gregorian_format = "";
	if($date != '' && !empty($jalali_format))
	{
		$j_y = $jalali_format[2];
		$j_m = $jalali_format[1];
		$j_d = $jalali_format[0];
		
		$gregorian_format = $j_y."-".$j_m."-".$j_d;
	}

	
	return $gregorian_format;
}
function getFirstWeek_it($year='',$week=0,$type='s')
{
	if($year=='')
	{
		$year = date('Y');
	}
	$yearstart = strtotime(''.$year.'-1-1');
	$firstsat = strtotime('next Saturday',$yearstart);
	$firstsatdate= date('Y-m-d', $firstsat);
	//echo 'First Week: '. date('Y-m-d',$yearstart) . ' - '.$firstsatdate;
	$week=($week-1)*7;
	if($type == 's')
	{
		return date('Y-m-d',strtotime('+'.($week-6).' days', $firstsat));
	}
	else
	{
		return date('Y-m-d',strtotime('+'.$week.' days', $firstsat));
	}
	//echo '<br>Second Week: ' . date('Y-m-d',strtotime('+'.($week-6).' days', $firstsat)) . ' - ' . date('Y-m-d',strtotime('+'.$week.' days', $firstsat));
}
function getFirstWeek_shamsi_it($year='',$week=0,$type='s')
{
	if($year=='')
	{
		$year = date('Y');
	}
	$shamsi_year = dateToShamsi($year,date('m'),date('d'));
	$shamsi_year = explode('-',$shamsi_year);
	$miladi_year = dateToMiladi($shamsi_year[0],01,01); 
	$yearstart = strtotime($miladi_year);
	$firstsat = strtotime('next Saturday',$yearstart);
	$firstsatdate= date('Y-m-d', $firstsat);
	//echo 'First Week: '. date('Y-m-d',$yearstart) . ' - '.$firstsatdate;
	$week=($week-1)*7;
	if($type == 's')
	{
		return date('Y-m-d',strtotime('+'.($week-6).' days', $firstsat));
	}
	else
	{
		return date('Y-m-d',strtotime('+'.$week.' days', $firstsat));
	}
	//echo '<br>Second Week: ' . date('Y-m-d',strtotime('+'.($week-6).' days', $firstsat)) . ' - ' . date('Y-m-d',strtotime('+'.$week.' days', $firstsat));
}
	//---- get department trees ---------------------------//
function getTaskTree_group_it($mode='all',$week=0,$id=0)
{
	$table = DB::connection('helpdesk')->
				table('task_group AS tg');
				$table->select('tg.title','tg.id AS group_id','tg.user_id','tg.created_at');
				$table->where('tg.id',$id);
				if(!isAdmin())
				{
					$table->join('group_shares_user AS gu','gu.task_group_id','=','tg.id');
					$table->where('gu.user_id',Auth::user()->id);
					$table->orWhere('tg.user_id',Auth::user()->id);
				}

	$object =	$table->get();
	//return View('docscom.tasks.grouptask')->with('object',$object);exit;
	$ul = '';
	$counter = 1;
	foreach($object AS $item)
	{
		//query for pagination count
		$t2 = DB::connection('helpdesk')
				->table('tasks AS t')
				->select(
					't.title',
					't.id AS task_id',
					't.user_id',
					't.start_date',
					't.end_date',
					't.created_at'
					)
				->where('task_group_id',$item->group_id)
				->where('parent_task',0);
				
			if(!isAdmin())
			{
				if($mode == 'my')
				{	
					$t2->join('task_assigned_to AS ta','ta.task_id','=','t.id');
					$t2->where('ta.assigned_to',Auth::user()->id);
				}
				elseif($mode == 'out')
				{
					$t2->Where('t.user_id',Auth::user()->id);
				}
				else
				{
					$t2->join('task_assigned_to AS ta','ta.task_id','=','t.id');
					$t2->where('ta.assigned_to',Auth::user()->id);
					$t2->orWhere('t.user_id',Auth::user()->id);
				}
			}
		$data['pages'] = round((count($t2->get()))/3);
		$data['item']=$item;
		$data['mode'] = $mode;
		$data['week'] = $week;
		$ul.= View('it_task.partial_views.grouptask',$data);

		$counter++;
	}
	return $ul;
}
//get menus
function putTaskLeft_it()
{
	//user left sections
	$User = new App\models\workplan\TaskGroup;
	//$sections = $User::getUserLeft(Auth::user()->id,Session::get('default_module'));
	$sections = $User::getAll();

	$lang = Session::get('lang');
	$li = "<li><a href='".URL::route('getTaskGroups_it')."'>
					<i class='fa fa-sitemap'></i>
					<span class='hidden-xs'>Group Tasks
					</span></a>
				</li>
				<li><a href='".URL::route('getReport_it')."'>
					<i class='fa fa-files-o'></i>
					<span class='hidden-xs'>All Tasks
					</span></a>
				</li>";
		foreach ($sections AS $item)
		{
			$name = "name_".$lang;

			$li .= "<li>";
			$li .= "<a href='".URL::route('getReportGroup_it',array($item->id,'all'))."'>
					<i class='fa_class_icon'></i>
					<span class='hidden-xs'>".$item->title."
					</span></a>";
			$li .= "</li>";

		}
	echo $li;
}
function weeks_in_month_it($year, $month)
{
	// Total number of days in the given month.
    $num_of_days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
 
    // Count the number of times it hits $start_day_of_week.
    $num_of_weeks = 0;
	//0 sunday, 1 monday ...
   for($i=1; $i<=$num_of_days; $i++)
    {
    	$timestamp = strtotime($year.'-'.$month.'-'.$i);
    	$day_of_week = date('D', $timestamp);
      	if($day_of_week=='Sat')
      	{
        	$num_of_weeks++;
        }
   }
 
    return $num_of_weeks;
}
function weeks_in_month_shamsi_it($year,$month)
{
	if($month<=6)
	{
		$num_of_days = 31;
	}
	elseif($month<=11)
	{
		$num_of_days = 30;
	}
	else
	{
		$num_of_days = 29;
	}
    // Count the number of times it hits $start_day_of_week.
    $num_of_weeks = 0;
	//0 sunday, 1 monday ...
   for($i=1; $i<=$num_of_days; $i++)
   {
   		$miladi_date = dateToMiladi($year,$month,$i);
   		$timestamp = strtotime($miladi_date);
    	$day_of_week = date('D', $timestamp);
      	if($day_of_week=='Sat')
        {
        	$num_of_weeks++;
        }
   }
 
    return $num_of_weeks;
}
function get_task_status_it($task_id)
{
	$User = new App\models\workplan\It_task_model;
	$status = $User::get_task_status($task_id);
	if($status)
	{
		return $status->status;
	}
	else
	{//task is not assigned to the user
		return -1;
	}
}
function checkGroup_task_it($id)
{
	$User = new App\models\workplan\It_taskGroup;
	$status = $User::checkGroup_task($id);
	if($status)
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
	
}
function has_task_it($year=0,$week=0,$mode='all')
{
	if(isShamsiDate())
	{
		$sdate = getFirstWeek_shamsi_it($year,$week,'s');
		$edate = getFirstWeek_shamsi_it($year,$week,'e');
	}
	else
	{
		$sdate = getFirstWeek_it($year,$week,'s');
		$edate = getFirstWeek_it($year,$week,'e');
	}
	
	$table = DB::connection('helpdesk')
				->table('tasks AS t')
				->select(
					't.title'
					)
				->where('parent_task',0)//level one
				->where('percentage','!=','100')
				->whereRaw('(((t.end_date >= "'.$sdate.'" AND t.end_date <= "'.$edate.'") OR (t.start_date >= "'.$sdate.'" AND t.start_date <= "'.$edate.'")) OR (t.start_date <= "'.$sdate.'" AND t.end_date >= "'.$edate.'"))')
				->groupBy('t.id');
		if($mode == 'my')
		{
			$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
			//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
			$table->where(function($q){
				$q->whereRaw('(ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2)');		
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->whereRaw('(sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2)');
	           });
			});
		}
		elseif($mode == 'out')
		{
			$table->where(function($q){
				$q->Where('t.user_id',Auth::user()->id);
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    //->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->Where('tasks.user_id',Auth::user()->id);
	           });
			});
		}
		else
		{
			$table->leftJoin('task_assigned_to AS ta','ta.task_id','=','t.id');
			//$table->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');
			$table->where(function($q){
				$q->whereRaw('((ta.assigned_to = '.Auth::user()->id.' AND ta.status != 2) or t.user_id = '.Auth::user()->id.')');		
				$q->orWhereIN('t.id',function($query)
	           {
	                $query->select('tasks.parent_task')
						->from('tasks')
	                    ->leftJoin('task_assigned_to AS sub','sub.task_id','=','tasks.id')
						->where('tasks.parent_task','!=',0)
	                    ->whereRaw('((sub.assigned_to = '.Auth::user()->id.' AND sub.status != 2) or tasks.user_id = '.Auth::user()->id.')');
	           });
			});
		}
	$object = $table->get();
	if(count($object)>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function is_assigned_before_it($userid,$task_id)
{
	$User = new App\models\workplan\It_task_model;
	$status = $User::is_assigned_before($userid,$task_id);
	if($status)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
function getTaskStatusByUser_it($userid,$task_id)
{
	$User = new App\models\workplan\It_task_model;
	$status = $User::geTaskStatusByUser($userid,$task_id);
	
	if($status)
	{
		return $status;
	}
	else
	{
		return FALSE;
	}
}
function has_subtask_it($task_id)
{
	$User = new App\models\workplan\It_task_model;
	$status = $User::has_subtask($task_id);
	
	if($status)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
//get user assigned ids for the task
function is_user_assigned_to_task_it($task_id=0)
{
	$result = DB::connection('helpdesk')->table('task_assigned_to')->where('task_id',$task_id)->where('assigned_to',Auth::user()->id)->get();
	if($result)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
function getSubTask_report_it($parent=0,$main_no=1,$sub='',$sdate,$edate)
{
	$table = DB::connection('helpdesk')
						->table('tasks AS t')
						->select(
								't.title',
								't.id',
								't.user_id',
								't.start_date',
								't.end_date',
								't.created_at',
								't.parent_task'
								)
						->where('t.parent_task',$parent);
	
	$object = $table->get();
	$i = 1;
	$tr = "";
	if($object)
	{
		if($sub == '')
		{//put tr header for sub task once
			$tr .= '<tr class="warning"><td colspan="4">'._("sub_task").'</td></tr>';
		}
		foreach($object AS $item)
		{
			$progress = getTaskAllProgress_it($item->id,$sdate,$edate);
			$tr .= '<tr><td>'.$main_no.'.'.$i.'</td><td>'.$item->title.'</td><td></td><td>';
			$task_progress='';
			if($progress){
  				foreach($progress AS $prog){
  					$task_progress .= $prog->progress_summary.',';
  				}
			}
			$tr .= $task_progress;
			$tr .= '</td></tr>';
			$level = $main_no.'.'.$i;
			$tr .= getSubTask_report_it($item->id,$level,'subs',$sdate,$edate);
			$i++;
		}
	}
	
	return $tr;
}
//get task progress summary details
function getTaskAllProgress_it($task_id=0)
{
	$objects = DB::connection('helpdesk')
				->table('task_progress')
				->where('task_id',$task_id)
				//->whereRaw('created_at >= "'.$sdate.'" AND created_at <= "'.$edate.'"')
				->orderBy('id','DESC')
				->get();
	
	//return the object
	return $objects;

}
function getCurrentShamsiWeek_it($year)
{
	$from = dateToMiladi($year,01,01);
	//$to = dateToMiladi($year,$month,15);//the end day of attendance month is 15 of current month\
	
	$begin = new DateTime($from);
	$end = new DateTime(date('Y-m-d'));//today's date
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	//create the all monthDay values for where: /2015/1216|/2015/1217
	$weeks = 0;
	foreach($period As $day)
	{
		$the_day = $day->format( "Y-m-d" );
		$day_code = date("w", strtotime($the_day));
		
		if($day_code == 6)//saturday
	        @$weeks++;
	}
	return $weeks;
	
}
?>