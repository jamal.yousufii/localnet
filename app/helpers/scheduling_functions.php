<?php 
use App\library\Dateconverter;
use App\library\jdatetime;

	//this is the helper functions of the president's meeting scheduling database.
	//==============================================================================
	# Transform hours like "1:45" into the total number of minutes, "105".
	function hoursToMinutes($hours)
	{
		if (strstr($hours, ':'))
		{
			# Split hours and minutes.
			$separatedData = explode(':', $hours);

			$minutesInHours    = $separatedData[0] * 60;
			$minutesInDecimals = $separatedData[1];

			$totalMinutes = $minutesInHours + $minutesInDecimals;
		}
		else
		{
			$totalMinutes = $hours * 60;
		}

		return $totalMinutes;
	}

	# Transform minutes like "105" into hours like "1:45".
	function minutesToHours($minutes)
	{
		$hours          = floor($minutes / 60);
		$decimalMinutes = $minutes - floor($minutes/60) * 60;

		# Put it together.
		$hoursMinutes = sprintf("%d:%02.0f", $hours, $decimalMinutes);
		return $hoursMinutes;
	}

	// change the date format from 1396-11-14 to Jalali e.g : 14-11-1396.
	function jalali_format($date)
	{
		$gregorian_format = explode("-", $date);

        $g_y = $gregorian_format[0];
        $g_m = $gregorian_format[1];
        $g_d = $gregorian_format[2];
		if($g_m<10)
		{
			$g_m = $g_m;
		}
		if($g_d<10)
		{
			$g_d = $g_d;
		}
        $jalali_format = $g_d."-".$g_m."-".$g_y;
        return $jalali_format;
	}

	// change the date format to Gregorian.
	function gregorian_format($date)
	{
		$jalali_format = explode("-", $date);

			$j_y = $jalali_format[2];
			$j_m = $jalali_format[1];
			$j_d = $jalali_format[0];

			$gregorian_format = $j_y."-".$j_m."-".$j_d;
			return $gregorian_format;
	}

	// convert the given Gregorian date to jalali and return it. 
	function convertToJalali($g_date)
	{
		$gregorian_date = explode("-", $g_date);
		
		$year = $gregorian_date[0];
		$month = $gregorian_date[1];
		$day = $gregorian_date[2];

		$jalali_date = jdatetime::toJalali($year, $month, $day);
		
		$j_year = $jalali_date[0];
		$j_month = $jalali_date[1];
		$j_day = $jalali_date[2];

		// add leading zero to month and day of the date.
        $j_m_pad = str_pad($j_month,2,0, STR_PAD_LEFT);
        $j_d_pad = str_pad($j_day,2,0, STR_PAD_LEFT);

		return $jalali_date = $j_year."-".$j_m_pad."-".$j_d_pad;
	}

	function convertToJalaliGetMonth($g_date)
	{
		$gregorian_date = explode("-", $g_date);
		
		$year = $gregorian_date[0];
		$month = $gregorian_date[1];
		$day = $gregorian_date[2];

		$jalali_date = jdatetime::toJalali($year, $month, $day);
		
		$j_year = $jalali_date[0];
		$j_month = $jalali_date[1];
		$j_day = $jalali_date[2];

		// add leading zero to month and day of the date.
        $j_m_pad = str_pad($j_month,2,0, STR_PAD_LEFT);
        $j_d_pad = str_pad($j_day,2,0, STR_PAD_LEFT);

		return $j_m_pad;
	}

	// convert the given Jalali date to Gregorian and return it. 
	function convertToGregorian($j_date)
	{
		$jalali_date = explode("-", $j_date);
		
		$year = $jalali_date[0];
		$month = $jalali_date[1];
		$day = $jalali_date[2];

		$gregorian_date = jdatetime::toGregorian($year, $month, $day);
		
		$g_year = $gregorian_date[0];
		$g_month = $gregorian_date[1];
		$g_day = $gregorian_date[2];

		// add leading zero to month and day of the date.
        $g_m_pad = str_pad($g_month,2,0, STR_PAD_LEFT);
        $g_d_pad = str_pad($g_day,2,0, STR_PAD_LEFT);

		return $gregorian_date = $g_year."-".$g_m_pad."-".$g_d_pad;
	}

	// get the jalali date desired format and how in the weekly and monthly table date section.
	function shamsi_date_format($j_date)
	{
        $jalali_date = explode("-", $j_date);
        
        //print_r($jalali_numbers);exit;
        $j_y = $jalali_date[0];
        $j_m = $jalali_date[1];
        $j_d = $jalali_date[2];
        
        $j_monthName = jdatetime::getMonthNames($j_m);
		
		return $j_d." ".$j_monthName." ".$j_y;

	}

	// get the date in desired format both jalali and gregorian.
	function shamsi_gregorian_date($date)
	{
		$g_date = convertToGregorian($date);
		// get the day name.
        $timestamp = strtotime($g_date);
        $day = date('D', $timestamp);
        
        $dayname = jdatetime::getDayNames($day);

		//$date = Input::get("date");
        $gregorian_date = explode("-", $g_date);
        //print_r($gregorian_date);exit;
        $g_y = $gregorian_date[0];
        $g_m = $gregorian_date[1];
        $g_d = $gregorian_date[2];
        //echo $g_d."-".$g_m."-".$g_y;
        // calling the function in Dateconverter library to change the gregorian date to jalali.
        $jalali_date = jdatetime::toJalali($g_y, $g_m, $g_d);
        $j_y = $jalali_date[0];
        $j_m = $jalali_date[1];
        $j_d = $jalali_date[2];
        // add leading zero to month and day of the date.
        $j_m_pad = str_pad($j_m,2,0, STR_PAD_LEFT);
        $j_d_pad = str_pad($j_d,2,0, STR_PAD_LEFT);

        $g_date_numbers = array($j_y, $j_m_pad, $j_d_pad);

        $jalali_numbers_date = jdatetime::convertNumbers($g_date_numbers);
        
        //print_r($jalali_numbers);exit;
        $j_y = $jalali_numbers_date[0];
        $j_m = $jalali_numbers_date[1];
        $j_d = $jalali_numbers_date[2];
       
        $timestamp = strtotime($g_date);
        $g_monthName = date('F', $timestamp);
        
        $j_monthName = jdatetime::getMonthNames($j_m);
		
		return $date = "<h4 align='center' dir='rtl' style='margin-top:20px' class='title'>".$dayname." ".$j_d." ".$j_monthName." ".$j_y." - ( <i dir='ltr'>".$g_d."-".$g_monthName."-".$g_y."</i> )</h4>";

	}

	// get the jalali dates and display the start and end days of a particular month and year both in jalali and gregorian.
	function shamsi_gregorian_months($start_date, $end_date)
	{

        $j_start_date = explode("-", $start_date);

        $g_y = $j_start_date[0];
        $g_m = $j_start_date[1];
        $g_d = $j_start_date[2];

        $j_end_date = explode("-", $end_date);

        $g_y_end = $j_end_date[0];
        $g_m_end = $j_end_date[1];
        $g_d_end = $j_end_date[2];

        // calling the function in jdatetime library to change the jalali date to gregorian.
        $g_start_date = jdatetime::toGregorian($g_y, $g_m, $g_d);

        $grg_y = $g_start_date[0];
        $grg_m = $g_start_date[1];
        $grg_d = $g_start_date[2];

        $gregorian_start_date = $grg_y."-".$grg_m."-".$grg_d;
        
        // get gregorian month name.
        $timestamp = strtotime($gregorian_start_date);
        $g_s_monthName = date('F', $timestamp);

        $g_end_date = jdatetime::toGregorian($g_y_end, $g_m_end, $g_d_end);

        $grg_y_end = $g_end_date[0];
        $grg_m_end = $g_end_date[1];
        $grg_d_end = $g_end_date[2];

        $gregorian_end_date = $grg_y_end."-".$grg_m_end."-".$grg_d_end;
        // get gregorian month name.
        $timestamp = strtotime($gregorian_end_date);
        $g_e_monthName = date('F', $timestamp);

        // calling the function in jdatetime library to change the gregorian numbers to jalali.
        $start_date_jalali_numbers = jdatetime::convertNumbers($start_date);
        $end_date_jalali_numbers = jdatetime::convertNumbers($end_date);

        $start_date_jalali_numbers = explode("-", $start_date_jalali_numbers);
        $end_date_jalali_numbers = explode("-", $end_date_jalali_numbers);
        //print_r($start_date_jalali_numbers);exit;
        $j_y = $start_date_jalali_numbers[0];
        $j_m = $start_date_jalali_numbers[1];
        $j_d = $start_date_jalali_numbers[2];

        $e_j_y = $end_date_jalali_numbers[0];
        $e_j_m = $end_date_jalali_numbers[1];
        $e_j_d = $end_date_jalali_numbers[2];
        //echo $j_d."-".$j_m."-".$j_y;

        $start_date_monthName = jdatetime::getMonthNames($j_m);
        $end_date_monthName = jdatetime::getMonthNames($e_j_m);

        return $date = "<h4 align='center' dir='rtl' style='margin-top:20px' class='title'>".$j_d." ".$start_date_monthName." - ".$e_j_d." ".$end_date_monthName." ".$j_y." مطابق <i dir='ltr'>".$grg_y_end." ".$g_e_monthName." ".$grg_d_end." - ".$g_s_monthName." ".$grg_d."</i></h4>";
	}

	// loop through each day meetings using day number.
	function day_meetings($day_num,$start_date,$end_date,$all=false)
	{

		$table = DB::connection("sched")
					->table('meeting_schedule')
					->select(array('meeting_start','meeting_end','meeting_with'));
		if($all==false)
		{
			$table->where("week_day", $day_num);
		}

		$meetings = $table->whereBetween('date', array($start_date, $end_date))
					->get();
		//print_r($meetings);
		if(count($meetings)>0 && $all == false)
		{
			$meeting_data = "";
			foreach($meetings as $row)
			{
				$start_time = date("g:i", STRTOTIME($row->meeting_start));
				$end_time = date("g:i", STRTOTIME($row->meeting_end));
				$start_time = explode(":", $start_time);
				$start_time = $start_time[0].":".$start_time[1];
				$end_time = explode(":", $end_time);
				$end_time = $end_time[0].":".$end_time[1];

				$meeting_data .= $start_time." - ".$end_time."<br />".substr($row->meeting_with, 0, 25)."<hr>";
				//echo "<td class='fixed'>".$row->meeting_with."</td>";
			}

			

			return $meeting_data;

		}
		else
		{
			// $out = '<table class="table table-bordered" dir="rtl" id="list_of_weeklyMeetings">';
   //          $out .= "<thead>";
   //            $out .= "<tr>";
   //              $out .= "<th>شنبه</th><th>یکشنبه</th><th>دوشنبه</th><th>سه شنبه</th><th>چهارشنبه</th><th>پنجشنبه</th><th>جمعه</th>";
   //            $out .= "</tr>";
   //          $out .= "</thead>";
   //          $out .= "<tbody>";
   //              $out .= "<td colspan='7'><div class='alert alert-danger span6' style='text-align:center'>جلسات برای این هفته در دیتابیس اضافه نگردیده است !</div></td>";
   //          $out .= "</tbody>";
   //      	$out .= "</table>";
			if($all == true && count($meetings) == 0)
			{
				return true;
			}
		}
		
	}

	function addDays($todate, $days)
	{
		$g_date = convertToGregorian($todate);
		$new_date = strtotime($g_date);
		$date = strtotime("+$days day", $new_date);
		$date = convertToJalali(date('Y-m-d', $date));
		$day_date = jdatetime::date($date,true,true);
        $day_date = shamsi_date_format($day_date);
        return $day_date;
	}

	function addGregorianDays($todate, $days)
	{
		$g_date = convertToGregorian($todate);
		$new_date = strtotime($g_date);
		$date = strtotime("+$days day", $new_date);
		$day_date = date('Y F d', $date);
        return $day_date;
	}
	
	function getDayName($day)
	{
		return jdatetime::getDayNames($day);
	}
	function getSEDate($start)
	{
		$date = jdatetime::date($start,true,true,'Asia/Kabul');
		return $date;
	}
	function getConvertNums($auto)
	{
		return jdatetime::convertNumbers($auto);
	}
	//check if the sent parameters does exist in the table.
	function checkRecordExistence($table,$field,$value)
	{
		if($value != "")
		{
			$existence  = DB::connection('sched')->table($table)->where($field,$value)->pluck($field);
			if($existence != "") return true; 
			else return false;
		}
	}
	//get id of table based on sent parameter.
	function getFieldValue($table,$returning_field,$condition_field,$value)
	{
		if($value != "")
		{
			$field_value  = DB::connection('sched')->table($table)->where($condition_field,$value)->pluck($returning_field);
			if($field_value != "") return $field_value; 
			else return "";
		}
	}

	function getSchedFileName($meeting_id)
	{
		$fileName = DB::connection('sched')->table("uploads")->where("meeting_id", $meeting_id)->get();
		if(!empty($fileName)){return $fileName;}
		else return false;
	}

	function addDaysToDate($date, $numOfDays)
	{
		$date = strtotime($date);
		$date = date('Y-m-d', strtotime("+$numOfDays day", $date));
		
		return $date;
	}

	function getLastMeetingID($table)
	{
		$last_record_id = DB::connection('sched')->table($table)->where("deleted",0)->orderBy('id', 'DESC')->pluck('id');
		if(!empty($last_record_id)){return $last_record_id;}
		else{return 0;}	
	}

	function getMeetingLastEndTimeOfToday($j_today_date)
	{
		// if there is a recurred meeting of the date, then don't apply this rule cuz there might be meetings that should be entried prior the recurred meeting.
		$last_end_time = DB::connection('sched')->table("meeting_schedule")->where("date", $j_today_date)->where('recurring', 0)->where('deleted',0)->orderBy('id', 'DESC')->pluck('meeting_end');
		if(!empty($last_end_time)){return $last_end_time;}
		else{return "";}	
	}

	function getMeetingFileName($meeting_id)
	{
		$file_name = DB::connection('sched')->table("uploads")->where("meeting_id", $meeting_id)->get();
		if(!empty($file_name)){return $file_name;}
		else{return "";}	
	}

	// check if the the record exists based on sent parameters.
	function checkRecurredMeetingExistence($field, $id)
	{
		$record = DB::connection('sched')->table("meeting_schedule")->where("id", $id)->where("deleted",0)->pluck($field);
		if(!empty($record)){return $record;}
		else{return "";}	
	}

	// check last recurred end date.
	function checkLastRecurreEndDate($id, $recure_type)
	{
		$record = DB::connection('sched')->table("meeting_schedule")->where("id", $id)->where("recure_type",$recure_type)->where("deleted",0)->pluck("end_recure");
		if(!empty($record)){return $record;}
		else{return "";}	
	}

	// check last recurred number of weeks.
	function checkLastRecurredNumOfWeeks($id, $recure_type)
	{
		$record = DB::connection('sched')->table("meeting_schedule")->where("id", $id)->where("recure_type",$recure_type)->where("deleted",0)->pluck("num_of_weeks");
		if(!empty($record)){return $record;}
		else{return "";}	
	}
	// check last recurred week days.
	function checkLastRecurredWeekDays($id)
	{
		$record = DB::connection('sched')->table("weekly_recurrence")->select("week_day")->where("meeting_id", $id)->get();
		if(!empty($record)){return $record;}
		else{return "";}	
	}
	// get the id of the meetings which is recurring weekly.
	function getWeeklyRecurringMeetingsID($start, $end, $end_recure, $recure_type)
	{
		$record = DB::connection('sched')->table('meeting_schedule')->select('id')->where('meeting_start', $start)->where('meeting_end', $end)
					->where('recurring', 1)->where('recure_type', $recure_type)->where('end_recure', $end_recure)->get();
		if(!empty($record)){return $record;}
		else{return "";}	
	}

	// get day number based on the day.
	function getDayNum($week_day)
	{
		switch ($week_day) {
            case 'شنبه':
                $week_day = 1;
                break;
            case 'یکشنبه':
                $week_day = 2;
                break;
            case 'دوشنبه':
                $week_day = 3;
                break;
            case 'سه شنبه':
                $week_day = 4;
                break;
            case 'چهارشنبه':
                $week_day = 5;
                break;
            case 'پنجشنبه':
                $week_day = 6;
                break;
            
            default:
                $week_day = 7;
                break;
        }

        return $week_day;
	} 
	//==================================================================================

	
?>