<?php 
function getFindingSubs($id=0)
{
	$table = DB::connection(self::$myDb)
			->table('sub_findings as m')
			->select('m.*')
			->leftjoin('findings AS f','f.id','=','m.finding_id')
			->where('f.report_id',$id)
			->get();
	return $table;
}
?>