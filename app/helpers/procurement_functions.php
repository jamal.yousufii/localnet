<?php 
use App\library\Dateconverter;
use App\library\jdatetime;

//convert miladi date to shamsi (2015-01-01 to 1394-01-01)
	function toJalali($g_date)
	{
		if($g_date == '0000-00-00' || $g_date == ''){
			return '';
		}
		$gregorian_date = explode("-", $g_date);
		
		$year = $gregorian_date[0];
		$month = $gregorian_date[1];
		$day = $gregorian_date[2];

		$jalali_date = jdatetime::toJalali($year, $month, $day);
		
		$j_year = $jalali_date[0];
		$j_month = $jalali_date[1];
		$j_day = $jalali_date[2];

		// add leading zero to month and day of the date.
        $j_m_pad = str_pad($j_month,2,0, STR_PAD_LEFT);
        $j_d_pad = str_pad($j_day,2,0, STR_PAD_LEFT);

		return $jalali_date = $j_year."-".$j_m_pad."-".$j_d_pad;
	}
//convert shamsi to meladi date.
	function toGregorian($j_date)
	{
		if($j_date == '0000-00-00' || $j_date == ''){
			return '';
		}
		$jalali_date = explode("-", $j_date);
		
		$year = $jalali_date[0];
		$month = $jalali_date[1];
		$day = $jalali_date[2];

		$gregorian_date = jdatetime::toGregorian($year, $month, $day);
		
		$g_year = $gregorian_date[0];
		$g_month = $gregorian_date[1];
		$g_day = $gregorian_date[2];

		// add leading zero to month and day of the date.
        $g_m_pad = str_pad($g_month,2,0, STR_PAD_LEFT);
        $g_d_pad = str_pad($g_day,2,0, STR_PAD_LEFT);

		return $gregorian_date = $g_year."-".$g_m_pad."-".$g_d_pad;
	}

// if the date field is empty then return empty
function checkEmptyDate($date)
{
	if($date == '0000-00-00' || $date == ""){
		return '';
	}
	else{
		
		$gregorian_format = explode("-", $date);

	   	$year = $gregorian_format[0];
	   	$month = $gregorian_format[1];
	   	$day = $gregorian_format[2];

	   	$jalali_date = jdatetime::toJalali($year, $month, $day);
		
		$j_year = $jalali_date[0];
		$j_month = $jalali_date[1];
		$j_day = $jalali_date[2];

		// add leading zero to month and day of the date.
        $j_m_pad = str_pad($j_month,2,0, STR_PAD_LEFT);
        $j_d_pad = str_pad($j_day,2,0, STR_PAD_LEFT);

		return $jalali_date = $j_d_pad."-".$j_m_pad."-".$j_year;
	}
}

//check gregorian date's emptyness.
function checkGregorianEmtpyDate($g_date)
{	
	if($g_date == '0000-00-00' || $g_date == ''){
		return '';
	}
	else{
		return $g_date;
	}
}

//get the procurement type.
function getProcType($selected_item=0)
{
	$lang = LaravelGettext::getLocaleLanguage();
	if($lang == 'fa'){
		//query from auth static table
		$rows = DB::connection('procurement')->table('procurement_type')->select('id','name_dr')->get();

		$items = "";
		foreach($rows AS $item)
		{
			$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name_dr.'</option>';
		}
	}
	else{
		//query from auth static table
		$rows = DB::connection('procurement')->table('procurement_type')->select('id','name_en')->get();

		$items = "";
		foreach($rows AS $item)
		{
			$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name_en.'</option>';
		}
	}

	return $items;
}

//get the procurement method.
function getProcMethod($selected_item=0)
{
	$lang = LaravelGettext::getLocaleLanguage();
	if($lang == 'fa'){
		//query from auth static table
		$rows = DB::connection('procurement')->table('procurement_method')->select('id','name_dr')->get();

		$items = "";
		foreach($rows AS $item)
		{
			$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name_dr.'</option>';
		}
	}
	else{
		//query from auth static table
		$rows = DB::connection('procurement')->table('procurement_method')->select('id','name_en')->get();

		$items = "";
		foreach($rows AS $item)
		{
			$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name_en.'</option>';
		}
	}

	return $items;
}
//get all the purchasing team of the purchases.
function getPurchasingTeam($selected_item='')
{
	$rows = DB::connection('procurement')->table('goods_purchase')->select('purchasing_team')->where('purchasing_team','!=','')
			->groupBy('purchasing_team')->get();
	$items = "<option value=''>Select Purchasing Team</option>";
	if($rows)
	{
		foreach($rows AS $item)
		{
			$items .= '<option '.($item->purchasing_team == $selected_item ? "selected":"").' value="'.$item->purchasing_team.'">'.$item->purchasing_team.'</option>';
		}	
	}

	return $items;
}

//get sub departments for consumers dropdown list in purchase form..
function getSubAllDeps($selected_item=0)
{
	// $lang = LaravelGettext::getLocaleLanguage();
	// if($lang == 'fa'){
	// 	//query from auth static table
	// 	$rows = DB::table('department')
	// 		->select('id','name')
	// 		->get();

	// 	$items = "";
	// 	foreach($rows AS $item)
	// 	{
	// 		$items .= '<option value="'.$item->id.'">'.$item->name.'</option>';
	// 	}
	// }
	// else{
	// 	//query from auth static table
	// 	$rows = DB::table('department')
	// 		->select('id','name_en')
	// 		->get();

	// 	$items = "";
	// 	foreach($rows AS $item)
	// 	{
	// 		$items .= '<option value="'.$item->id.'">'.$item->name_en.'</option>';
	// 	}
	// }
	
	//query from auth static table
	$rows = DB::table('department')
		->select('id','name')->where('unactive',0)->get();
	
	$items = "";
	foreach($rows AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name.'</option>';
	}

	return $items;
}
//check if the date difference is single or plural.
function checkDateDiff($datediff)
{
	if($datediff == '0000-00-00' || $datediff == ''){
		return '';
	}
	if($datediff == 1 || $datediff == 0){
        return $datediff = $datediff." Day";
    }
    else{
        return $datediff = $datediff." Days";
    }
}
// checking the user role nd entry previlliges.
function goodsCheck()
{
	$roles = Session::get('user_roles');
	if(isset($roles)){return TRUE;}
	else{return FALSE;}	
}
// checking the user role nd entry previlliges.
function purchaseCheck()
{
	$roles = Session::get('user_roles');
	if(isset($roles)){return TRUE;}
	else{return FALSE;}	
}
// checking the user role nd entry previlliges.
function contractCheck()
{
	$roles = Session::get('user_roles');
	if(isset($roles)){return TRUE;}
	else{return FALSE;}	
}
// checking the user role nd entry previlliges.
function goodsRoleCheck($section='')
{
	$default_module = getCurrentAppCode();
	$roles = Session::get('user_roles');
	if(isset($roles[$default_module][$section]['goods']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}	
}
// checking the user role nd entry previlliges.
function purchaseRoleCheck($section='')
{
	
	$default_module = getCurrentAppCode();
		
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['purchase']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
// checking the user role nd entry previlliges.
function contractGoodsRoleCheck($section='')
{
	
	$default_module = getCurrentAppCode();
		
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['contract_goods']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
// checking the user role nd entry previlliges.
function contractRoleCheck($section='')
{
	
	$default_module = getCurrentAppCode();
		
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['contract']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
// checking the user role if it is for the finance user in purchase section.
function financeGoodsRoleCheck($section='')
{
	
	$default_module = getCurrentAppCode();
		
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['finance_goods']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
// checking the user role if it is for the finance user in contract section.
function financeContractRoleCheck($section='')
{
	
	$default_module = getCurrentAppCode();
		
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['finance_contract']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
// get total amounts based on sent parameters.
function getTotalAmounts($table,$field)
{
	if($table == "contract" && $field == "total_contract_price")
	{
		$total_amount = DB::connection('procurement')->table($table)->select($field)->groupBy('procurement_id')->get();
		//dd($total_amount);
		$total = 0;
		foreach($total_amount as $key => $value)
		{
			$total += $value->$field;
		}
		if(!empty($total)){return $total;}else{return 0;}
	}
	elseif($field == 'date_of_sent_to_finance')
	{
		$total_amount = DB::connection('procurement')->table($table)->whereNotNull($field)->where($field, '!=', '0000-00-00')->pluck(DB::raw('count(id)'));
		if(!empty($total_amount)){return $total_amount;}else{return 0;}	
	}
	else
	{
		$total_amount = DB::connection('procurement')->table($table)->pluck(DB::raw('SUM('.$field.')'));
		if(!empty($total_amount)){return $total_amount;}else{return 0;}	
	}
}
// get total number of records based on send table name.
function getTotalNumberOfRecords($table, $archived=0)
{
	if($table == "contract" && $archived == 1 || $table == "procurement")
	{
		$total_records = DB::connection('procurement')->table($table)->select('id')->where('archived',$archived)->get();
		//dd(count($total_records));
		return count($total_records);
	}
	elseif($table == "contract")
	{
		$total_records = DB::connection('procurement')->table($table)->select('procurement_id')->where('archived',$archived)->groupBy('procurement_id')->get();
		//dd(count($total_records));
		return count($total_records);
	}
	else
	{
		$total_records = DB::connection('procurement')->table($table)->pluck(DB::raw('count(id) as total_records'));
		return $total_records;
	}
}

	
?>