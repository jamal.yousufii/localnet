<?php
use Illuminate\Http\Request;

function getResizedPhoto($name='')
{
	if ($name != '')
	{
		$img = Image::make(file_get_contents('documents/profile_pictures/'.$name ))->crop(1278, 1597,897,143);

		$img->encode('jpg');
		$type = 'jpg';
	}
	else
	{
		$img = Image::make(file_get_contents('documents/profile_pictures/default.jpeg' ))->crop(1278, 1597,897,143);
		$img->encode('jpeg');
		$type = 'jpeg';
	}

	$base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);
	return $base64;
}

function getEmployeeFirstExperience($id=0)
{
	return DB::connection('hr')
				->table('employee_experiences')
				->where('employee_id',$id)
				->orderBy('id')
				->first();
}
function getEmployeeFileName($id=0)
{
	return DB::connection('hr')
				->table('attachments')
				->where('id',$id)
				->pluck('file_name');
}

function getDepartmentWhereIn()
{
	if(getCurrentAppCode() == 'hr')
	{
		//$ids = array(23,25,27,39,44,48,52,55,58,66,80,178,167,161);

        //if(isOCS('hr_recruitment'))
       if(hasRule('hr_attendance','filter_general_dept')) 
       {
         $departments = DB::table('user_dept')->select('dept_id')->where('user_id',Auth::user()->id)->where('type','general_dept')->get(); 
         $ids = json_decode(json_encode($departments), true);
       }
       else
       { 
         if(getUserDepType(Auth::user()->id)->dep_type==1)
         {
            //$ids = array(80,427,428,429,430);
            // $ids = array(80,427,428,429,430,55,178,481,519,39);
            $ids = array(80,427,428,429,430,55,178,481,519,536,573,587);
         }
         else
         {
            $ids = array(25,39,58,66,80,161,420,408,404,405,406,468,469,493,23,526,548,541,533,553,601);
         }
       }

	}
	else
	{
		$ids = array(23,25,27,39,44,48,52,55,58,66,81,82,83,84,85,86,87,89,90,91,92,93);
	}

	$object = DB::table('department')->whereIn('id',$ids)->where('unactive',0)->get();
	return $object;
}
function getMainDepartments()
{
    // ddd(Session::get('user_roles'));
	if(getCurrentAppCode() == 'hr')
	{
       if(hasRule('hr_attendance','filter_general_dept')) 
       {
         $departments = DB::table('user_dept')->select('dept_id')->where('user_id',Auth::user()->id)->where('type','general_dept')->get(); 
         $ids = json_decode(json_encode($departments), true);
       }
       else
       {
            if(getUserDepType(Auth::user()->id)->dep_type==1)
            {
                //$ids = array(80,427,428,429,430);
                // $ids = array(80,427,428,429,430,55,178,481,519,39);
                $ids = array(80,427,428,429,430,55,178,481,519,536,573,587);
            }
            else
            {
                $ids = array(25,39,58,408,420,66,80,404,405,406,406,468,469,23,493,526,548,541,553,601);
                /*
                $object = DB::table('department')
                                    ->whereIn('id',function ($q) {
                                            $q->select('parent')->groupBy('parent')->get();
                                    })
                                    ->where('unactive',0)->get();
            */
            }
        }

	}
	else
	{
		$ids = array(23,25,27,39,44,48,52,55,58,66,81,82,83,84,85,86,87,89,90,91,92,93,526);
	}
	$object = DB::table('department')->whereIn('id',$ids)->where('unactive',0)->get();
	return $object;
}
function getDepartmentWhereNotIn()
{
	if(getCurrentAppCode() == 'hr')
	{
		//$ids = array(23,25,27,39,44,48,52,55,58,66,80,81,82,83,84,85,86,87,89,90,91,92,93);
		$ids = array(23,25,27,39,44,48,52,55,58,66,80,178,167,161);
	}
	else
	{
		$ids = array(23,25,27,39,44,48,52,55,58,66,81,82,83,84,85,86,87,89,90,91,92,93);
	}

	$object = DB::table('department')->whereNotIn('id',$ids)->where('unactive',0)->get();
	return $object;
}

/*
get static table data
@param table_name
@param lang
@param selected
return mixed
*/

function getStaticDropdown($table = '',$selected_item=0,$lang='dr',$order_by='')
{
	$rows = array();

	if($table != '')
	{
		if($order_by=='')
		{
			$rows = DB::table($table)
				->select('id','name_'.$lang.' AS name')
				->get();
		}
		else
		{
			$rows = DB::table($table)
				->select('id','name_'.$lang.' AS name')
				->orderBy($order_by)
				->get();
		}
	}

	$items = "";
	foreach($rows AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name.'</option>';
	}

	return $items;
}
function getStaticDropdownBoth($table = '',$selected_item=0,$lang='dr')
{
	$rows = array();

	if($table != '')
	{
		//query from auth static table
		$rows = DB::table($table)
				->select('id','name_en','name_dr')
				->get();
	}

	$items = "";
	foreach($rows AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name_dr.' - '.$item->name_en.'</option>';
	}

	return $items;
}
function getBastStaticList($table = '',$selected_item=0,$lang='dr')
{
	$rows = array();

	if($table != '')
	{
		//query from auth static table
		$rows = DB::table($table)
				->select('id','name_'.$lang.' AS name')
				->whereNotIn('id',array(13,14,15,16))
				->get();
	}

	$items = "";
	foreach($rows AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name.'</option>';
	}

	return $items;
}

function getProvinceDistricts($province=0,$selected_item=0,$lang='dr')
{

	//query from auth static table
	$rows = DB::table('districts')
			->select('id','name_'.$lang.' AS name')
			->where('province_id',$province)
			->orderBy('name_'.$lang,'ASC')
			->get();

	$items = "";
	foreach($rows AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name.'</option>';
	}

	return $items;
}
function getSubDepartmentDropdown($dep=0,$selected_item=0)
{

	//query from auth static table
	$rows = DB::table('department')
			->select('id','name')
			->where('parent',$dep)
			->get();

	$items = "";
	foreach($rows AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name.'</option>';
	}

	return $items;
}

function canUpdateCard($section='')
{

	$default_module = getCurrentAppCode();

	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['update_card']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
function canPrintCard($section='')
{

	$default_module = getCurrentAppCode();

	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['print_card']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
function isOCS($section='')
{
	$default_module = getCurrentAppCode();

	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['ocs']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
function getAllProvinces($lang = 'dr')
{

	return DB::table('provinces')
			->select('id','name_'.$lang.' AS name')
			->orderBy('name_'.$lang)
			->get();
}
function getProvinceDistrict($province=0,$lang = 'dr')
{

	return DB::table('districts')
			->select('id','name_'.$lang.' AS name')
			->where('province_id',$province)
			->orderBy('name_'.$lang)
			->get();
}
function getLastAttendanceId()
{

	$row=  DB::connection('hr')
			->table('attendance_temp')
			->select('*')
			->first();
	if($row)
	{
		return $row;
	}
	else
	{
		return 0;
	}
}
function getEmployeePresentDays($id=0,$year,$month)
{
	/*
	$att_time_in = getAttTime($year,$month,'in');
	$att_time_out = getAttTime($year,$month,'out');
	$att_time_in_thu = getAttTime_thu($year,$month,'in');
	$att_time_out_thu = getAttTime_thu($year,$month,'out');
	//check if user is deactivated from normal shift
	if(is_in_shift($id,$year,$month,null))
	{
		$att_time_in = getAttTime($year,$month,'in');
		$att_time_out = getAttTime($year,$month,'out');
	}
	else
	{
		$shift_time = is_in_shift($id,$year,$month);
		if($shift_time)
		{
			$shift_time_in = $shift_time->time_in;
			$shift_time_out= $shift_time->time_out;
			if($shift_time_in !=null)
			{
				$att_time_in_shift = explode(':',$shift_time_in);
				$shift_time_in = $att_time_in_shift[0].$att_time_in_shift[1].$att_time_in_shift[2];
				$att_time_in = $shift_time_in;
			}
			if($shift_time_out != null)
			{
				$att_time_out_shift = explode(':',$shift_time_out);
				$shift_time_out = $att_time_out_shift[0].$att_time_out_shift[1].$att_time_out_shift[2];
				$att_time_out = $shift_time_out;
			}
		}
	}
	*/
	$sday = att_month_days(0);
	$eday = att_month_days(1);
	if($month == 1)
	{
		$from = dateToMiladi($year-1,12,$sday);
	}
	else
	{
		$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
	}
	$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\

	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	//create the all monthDay values for where: /2015/1216|/2015/1217
	$days = array();
	$dayTotal=0;//count the days which the day is urgent,leave,holiday
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	$allHolidays =  DB::connection('hr')
				->table('month_holiday_dates')
				->select('date')
				->whereBetween('date', array($from, $to))
				->where('dep_type',$user_dep_type)
				->get();
	$allHolidays = json_decode(json_encode((array) $allHolidays), true);
	$new_array=array();
	foreach($allHolidays as $array)
	{
	    foreach($array as $val)
	    {
	        array_push($new_array, $val);
	    }
	}
	$emp_id = DB::connection('hr')->table('employees')->select('id')->where('RFID',$id)->first();
	$thuFri = is_in_ThuFri_shift($id);
	foreach($period As $day)
	{
		$the_day = $day->format( "Y-m-d" );
		$period_det = explode('-',$the_day);
		$period_day = $period_det[2];
		$period_month = $period_det[1];
		$period_year = $period_det[0];
		$day_code = date("w", strtotime($the_day));
		//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
		//$urgents = checkUrgentHoliday($the_day);
		$holiday = in_array($the_day, $new_array);


		$isInLeave = isEmployeeInLeave($emp_id->id,$the_day,$year,$month);
		if($isInLeave)
		{
			continue;
		}
		elseif($holiday || ($day_code == 5 && !$thuFri))
		{//holidays and fridays
			if(!checkHolidayAbsent($the_day,$id))
			{
				$dayTotal++;//calculate the day urgent,leave or fridays as present if day befor and after is present
			}
		}
		else
		{
			if($day_code == 4 || $day_code == 5)//dont include thu and fri
			{//thursdays
				if(is_in_thu_shift($id,$year,$month))
				{
					$dayTotal++;//calculate the thu days as present if he is in the shift
				}
				else
				{
					if($thuFri)
					{
						if(checkIfEmployeeIsPresent($id,$the_day))
						{
							$dayTotal++;
						}
						else
						{
							$thedate = explode('-', $the_day);
							$from_unix_time = mktime(0, 0, 0,$thedate[1],$thedate[2],$thedate[0]);
							$day_after = strtotime("tomorrow", $from_unix_time);
							$tomorrow = date('Y-m-d', $day_after);

							if(checkIfEmployeeIsPresent($id,$tomorrow))
							{
								$dayTotal++;
							}
						}
					}
					else
					{
						//$thu_days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
						$days[] = $period_year.'-'.$period_month.'-'.$period_day;
					}
				}

			}
			else
			{
				//$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
				$days[] = $period_year.'-'.$period_month.'-'.$period_day;
			}
		}
	}
	//dd($days);
	//remove the last pipe line
	//$days = substr($days, 0, -1);
	//remove the last pipe line
	//$thu_days = substr($thu_days, 0, -1);

	$rfid_lost = lost_rfid($emp_id->id);
	if($rfid_lost)
	{
		$rfids = array($id);
		foreach($rfid_lost as $new_rfid)
		{
			$rfids[] =$new_rfid->rfid;
		}
		if($days!=null)
		{
			$total =DB::connection('hr')
				->table('attendance_images')
				->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total'))//return the monthDay part of path
				->whereIn('RFID',$rfids)//check the rfid
				->where('status',0)//present
				//->whereRaw('CASE
				//			WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
				//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
				//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
				->whereIn('date',$days)
				->where('dep_type',$user_dep_type)
				->groupBy('groupByMe')
				->having('total', '>', 1)
				->get();
				//->toSql();dd($total->total);
		}
	}
	else
	{
		if($days!=null)
		{
			$total =DB::connection('hr')
				->table('attendance_images')
				->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total'))//return the monthDay part of path
				->where('RFID',$id)//check the rfid
				->where('status',0)//present
				//->whereRaw('CASE
				//			WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
				//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
				//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
				->whereIn('date',$days)
				->where('dep_type',$user_dep_type)
				->groupBy('groupByMe')
				->having('total', '>', 1)
				->get();
				//->toSql();dd($total);
		}
	}
	$result = 0 ;//dd($total);
	if(isset($total))
	{
		$result+=count($total);
	}
	$result=$result+$dayTotal;//dd($result);
	return $result;
}
function getEmployeePresentDays_tillToday($id=0,$year,$month)
{
	$sday = att_month_days(0);
	$eday = att_month_days(1);
	if($month == 1)
	{
		$from = dateToMiladi($year-1,12,$sday);
	}
	else
	{
		$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
	}
	$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
	if($to>date('Y-m-d'))
	{
		$to = date('Y-m-d');
	}
	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	//create the all monthDay values for where: /2015/1216|/2015/1217
	$days = array();
	$thu_days = array();
	$thuTotal=0;//count the thu for those who are in thu university shift as presented
	$dayTotal=0;//count the days which the day is urgent,leave,holiday
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	$allHolidays =  DB::connection('hr')
				->table('month_holiday_dates')
				->select('date')
				->whereBetween('date', array($from, $to))
				->where('dep_type',$user_dep_type)
				->get();
	$allHolidays = json_decode(json_encode((array) $allHolidays), true);
	$new_array=array();
	foreach($allHolidays as $array)
	{
	    foreach($array as $val)
	    {
	        array_push($new_array, $val);
	    }
	}
	$emp_id = DB::connection('hr')->table('employees')->select('id')->where('RFID',$id)->first();
	foreach($period As $day)
	{
		$the_day = $day->format( "Y-m-d" );
		$period_det = explode('-',$the_day);
		$period_day = $period_det[2];
		$period_month = $period_det[1];
		$period_year = $period_det[0];
		$day_code = date("w", strtotime($the_day));
		//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
		//$urgents = checkUrgentHoliday($the_day);
		$holiday = in_array($the_day, $new_array);


		$isInLeave = isEmployeeInLeave_today($emp_id->id,$the_day);
		if($isInLeave)
		{
			continue;
		}
		elseif($holiday || $day_code == 5)
		{
			if(!checkHolidayAbsent($the_day,$id))
			{
				$dayTotal++;//calculate the day urgent,leave or fridays as present
			}
		}
		else
		{
			if($day_code != 4)//dont include thu
			{

				//$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
				$days[] = $period_year.'-'.$period_month.'-'.$period_day;
			}
			else
			{//thursdays
				if(is_in_thu_shift($id,$year,$month))
				{
					$dayTotal++;//calculate the thu days as present if he is in the shift
				}
				else
				{
					//$thu_days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
					$days[] = $period_year.'-'.$period_month.'-'.$period_day;
				}
			}
		}
	}
	//dd($days);
	//remove the last pipe line
	//$days = substr($days, 0, -1);
	//remove the last pipe line
	//$thu_days = substr($thu_days, 0, -1);

	$rfid_lost = lost_rfid($emp_id->id);
	if($rfid_lost)
	{
		$rfids = array($id);
		foreach($rfid_lost as $new_rfid)
		{
			$rfids[] =$new_rfid->rfid;
		}
		if($days!=null)
		{
			$total =DB::connection('hr')
				->table('attendance_images')
				->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total'))//return the monthDay part of path
				->whereIn('RFID',$rfids)//check the rfid
				->where('status',0)//present
				//->whereRaw('CASE
				//			WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
				//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
				//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
				->whereIn('date',$days)
				->where('dep_type',$user_dep_type)
				->groupBy('groupByMe')
				->having('total', '>', 1)
				->get();
				//->toSql();dd($total->total);
		}
	}
	else
	{
		if($days!=null)
		{
			$total =DB::connection('hr')
				->table('attendance_images')
				->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total'))//return the monthDay part of path
				->where('RFID',$id)//check the rfid
				->where('status',0)//present
				//->whereRaw('CASE
				//			WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
				//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
				//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
				->whereIn('date',$days)
				->where('dep_type',$user_dep_type)
				->groupBy('groupByMe')
				->having('total', '>', 1)
				->get();
				//->toSql();dd($total);
		}
	}
	$result = 0 ;//dd($total);
	if(isset($total))
	{
		$result+=count($total);
	}
	$result=$result+$dayTotal;//dd($result);
	return $result;
}
//calculate the present days of employee including the leaves days
function getEmployeePresentDays_withLeaves($id=0,$year,$month)
{
	$sday = att_month_days(0);
	$eday = att_month_days(1);
	if($month == 1)
	{
		$from = dateToMiladi($year-1,12,$sday);
	}
	else
	{
		$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
	}
	$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
	if($to>date('Y-m-d'))
	{
		$to = date('Y-m-d');
	}
	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	//create the all monthDay values for where: /2015/1216|/2015/1217
	$days = array();
	$dayTotal=0;//count the days which the day is urgent,leave,holiday
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	$allHolidays =  DB::connection('hr')
				->table('month_holiday_dates')
				->select('date')
				->whereBetween('date', array($from, $to))
				->where('dep_type',$user_dep_type)
				->get();
	$allHolidays = json_decode(json_encode((array) $allHolidays), true);
	$new_array=array();
	foreach($allHolidays as $array)
	{
	    foreach($array as $val)
	    {
	        array_push($new_array, $val);
	    }
	}
	$emp_id = DB::connection('hr')->table('employees')->select('id')->where('RFID',$id)->first();
	$thuFri = is_in_ThuFri_shift($id);
	foreach($period As $day)
	{
		$the_day = $day->format( "Y-m-d" );
		$period_det = explode('-',$the_day);
		$period_day = $period_det[2];
		$period_month = $period_det[1];
		$period_year = $period_det[0];
		$day_code = date("w", strtotime($the_day));
		//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
		//$urgents = checkUrgentHoliday($the_day);
		$holiday = in_array($the_day, $new_array);

		$isInLeave = isEmployeeInLeave_today($emp_id->id,$the_day);
		if($isInLeave)
		{
			$dayTotal++;
		}
		elseif($holiday || ($day_code == 5 && !$thuFri))
		{//holidays and fridays
			if(!checkHolidayAbsent($the_day,$id))
			{
				$dayTotal++;//calculate the day urgent,leave or fridays as present if day befor and after is present
			}
		}
		else
		{
			if($day_code == 4 || $day_code == 5)//dont include thu and fri
			{//thursdays
				if(is_in_thu_shift($id,$year,$month))
				{
					$dayTotal++;//calculate the thu days as present if he is in the shift
				}
				else
				{
					if($thuFri)
					{
						if(checkIfEmployeeIsPresent($id,$the_day))
						{
							$dayTotal++;
						}
						else
						{
							$thedate = explode('-', $the_day);
							$from_unix_time = mktime(0, 0, 0,$thedate[1],$thedate[2],$thedate[0]);
							$day_after = strtotime("tomorrow", $from_unix_time);
							$tomorrow = date('Y-m-d', $day_after);

							if(checkIfEmployeeIsPresent($id,$tomorrow))
							{
								$dayTotal++;
							}
						}
					}
					else
					{
						//$thu_days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
						$days[] = $period_year.'-'.$period_month.'-'.$period_day;
					}
				}

			}
			else
			{
				//$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
				$days[] = $period_year.'-'.$period_month.'-'.$period_day;
			}
		}
	}
	//dd($days);
	//remove the last pipe line
	//$days = substr($days, 0, -1);
	//remove the last pipe line
	//$thu_days = substr($thu_days, 0, -1);

	$rfid_lost = lost_rfid($emp_id->id);
	if($rfid_lost)
	{
		$rfids = array($id);
		foreach($rfid_lost as $new_rfid)
		{
			$rfids[] =$new_rfid->rfid;
		}
		if($days!=null)
		{
			$total =DB::connection('hr')
				->table('attendance_images')
				->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total'))//return the monthDay part of path
				->whereIn('RFID',$rfids)//check the rfid
				->where('status',0)//present
				//->whereRaw('CASE
				//			WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
				//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
				//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
				->whereIn('date',$days)
				->where('dep_type',$user_dep_type)
				->groupBy('groupByMe')
				->having('total', '>', 1)
				->get();
				//->toSql();dd($total->total);
		}
	}
	else
	{
		if($days!=null)
		{
			$total =DB::connection('hr')
				->table('attendance_images')
				->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total'))//return the monthDay part of path
				->where('RFID',$id)//check the rfid
				->where('status',0)//present
				//->whereRaw('CASE
				//			WHEN time <= 120000 THEN time<='.$att_time_in.' ELSE time>='.$att_time_out.' END')
				//->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
				//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
				->whereIn('date',$days)
				->where('dep_type',$user_dep_type)
				->groupBy('groupByMe')
				->having('total', '>', 1)
				->get();
				//->toSql();dd($total);
		}
	}
	$result = 0 ;//dd($total);
	if(isset($total))
	{
		$result+=count($total);
	}
	$result=$result+$dayTotal;//dd($result);
	return $result;
}

function getEmployeePresentDays_noTime($id=0,$year,$month)
{
	$sday = att_month_days(0);
	$eday = att_month_days(1);
	if($month == 1)
	{
		$from = dateToMiladi($year-1,12,$sday);
	}
	else
	{
		$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
	}
	$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\

	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	//create the all monthDay values for where: /2015/1216|/2015/1217
	$days = array();
	//$thu_days = array();
	$thuTotal=0;//count the thu for those who are in thu university shift as presented
	$dayTotal=0;//count the days which the day is urgent,leave,holiday
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	$allHolidays =  DB::connection('hr')
				->table('month_holiday_dates')
				->select('date')
				->whereBetween('date', array($from, $to))
				->where('dep_type',$user_dep_type)
				->get();
	$allHolidays = json_decode(json_encode((array) $allHolidays), true);
	$new_array=array();
	foreach($allHolidays as $array)
	{
	    foreach($array as $val)
	    {
	        array_push($new_array, $val);
	    }
	}
	$emp_id = DB::connection('hr')->table('employees')->select('id')->where('RFID',$id)->first();
	foreach($period As $day)
	{
		$the_day = $day->format( "Y-m-d" );
		$period_det = explode('-',$the_day);
		$period_day = $period_det[2];
		$period_month = $period_det[1];
		$period_year = $period_det[0];
		$day_code = date("w", strtotime($the_day));
		//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
		//$urgents = checkUrgentHoliday($the_day);
		$holiday = in_array($the_day, $new_array);

		$isInLeave = isEmployeeInLeave($emp_id->id,$the_day,$year,$month);
		if($isInLeave)
		{
			continue;
		}
		elseif($holiday || $day_code == 5)
		{
			$dayTotal++;//calculate the day urgent,leave or fridays as present
		}
		else
		{
			if($day_code != 4)//dont include thu
			{
				//$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
				$days[] = $period_year.'-'.$period_month.'-'.$period_day;
			}
			else
			{//thursdays
				if(is_in_thu_shift($id,$year,$month))
				{
					$thuTotal++;//calculate the thu days as present if he is in the shift
				}
				else
				{
					//$thu_days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
					$days[] = $period_year.'-'.$period_month.'-'.$period_day;
				}
			}
		}
	}
	//dd($days);
	//remove the last pipe line
	//$days = substr($days, 0, -1);
	//remove the last pipe line
	//$thu_days = substr($thu_days, 0, -1);
	$emp_det = DB::connection('hr')
				->table('employees')->select('id')->where('RFID',$id)->first();
	$rfid_lost = lost_rfid($emp_det->id);
	if($rfid_lost)
	{
		$rfids = array($id);
		foreach($rfid_lost as $new_rfid)
		{
			$rfids[] =$new_rfid->rfid;
		}
		if($days!=null)
		{
			$total =DB::connection('hr')
				->table('attendance_images')
				->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total'))//return the monthDay part of path
				->whereIn('RFID',$rfids)//check the rfid
				->where('status',0)//present
				->whereIn('date',$days)
				->groupBy('groupByMe')
				->having('total', '>', 1)
				->get();
				//->toSql();dd($total->total);
		}
	}
	else
	{
		if($days!=null)
		{
			$total =DB::connection('hr')
				->table('attendance_images')
				->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total'))//return the monthDay part of path
				->where('RFID',$id)//check the rfid
				->where('status',0)//present
				->whereIn('date',$days)
				->groupBy('groupByMe')
				->having('total', '>', 1)
				->get();
				//->toSql();dd($total);
		}
	}
	$result = 0 ;
	if(isset($total))
	{
		$result+=count($total);
	}
	if(isset($thu_total))
	{
		$result+=count($thu_total);
	}
	$result=$result+$thuTotal+$dayTotal;
	return $result;
}
function getPresentedDays($rfid=0,$year=0,$month=0)
{
	$sday = att_month_days(0);
	$eday = att_month_days(1);
	if($month == 1)
	{
		$from = dateToMiladi($year-1,12,$sday);
	}
	else
	{
		$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
	}
	$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\

	$begin = new DateTime($from);
	$end = new DateTime($to);
	$presented=0;
	$emp_det = DB::connection('hr')
				->table('employees')->select('id')->where('RFID',$rfid)->first();
	$rfid_lost = lost_rfid($emp_det->id);
	if($rfid_lost)
	{
		$rfids = array($rfid);
		foreach($rfid_lost as $new_rfid)
		{
			$rfids[] =$new_rfid->rfid;
		}
		$total =DB::connection('hr')
				->table('attendance_presents')
				->select(DB::raw('count(id) As item,date'))
				->whereIn('RFID',$rfids)
				->where('date','>=',$begin)
				->where('date','<=',$end)
				->groupBy('date')
				//->having('item', '=', 1)
				->get();
		if(count($total)>0)
		{
			foreach($total AS $day)
			{
				if($day->item>1)
				{//if its 2, it means he is presented in this day
					$presented++;
				}
				else
				{
					$monthDay = explode('-',$day->date);
					$year = $monthDay[0];
					$monthDay = $monthDay[1].$monthDay[2];
					$days = '/'.$year.'/'.$monthDay;
					//$days = $year.'-'.$monthDay[1].'-'.$monthDay[2];

					$result =DB::connection('hr')
						->table('attendance_images')
						//->select(DB::raw('count(id) As total'))
						->whereIn('RFID',$rfids)
						->where('status',0)//present
						->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path

						->get();
					if(count($result)>0)
					{
						$presented++;
					}
				}

			}
		}
	}
	else
	{
		$total =DB::connection('hr')
				->table('attendance_presents')
				->select(DB::raw('count(id) As item,date'))
				->where('RFID',$rfid)
				->where('date','>=',$begin)
				->where('date','<=',$end)
				->groupBy('date')
				//->having('item', '=', 1)
				->get();
		if(count($total)>0)
		{
			foreach($total AS $day)
			{
				if($day->item>1)
				{//if its 2, it means he is presented in this day
					$presented++;
				}
				else
				{
					$monthDay = explode('-',$day->date);
					$year = $monthDay[0];
					$monthDay = $monthDay[1].$monthDay[2];
					$days = '/'.$year.'/'.$monthDay;


					$result =DB::connection('hr')
						->table('attendance_images')
						//->select(DB::raw('count(id) As total'))
						->where('RFID',$rfid)
						->where('status',0)//present
						->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path

						->get();
					if(count($result)>0)
					{
						$presented++;
					}
				}

			}
		}
	}
	return $presented;
}

function countHolidays($year,$month,$date_type=0)
{//count all fridays+official holidays+emergency holidays
	if($date_type==0)
	{
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month==1)
		{
			$from = dateToMiladi($year-1,12,$sday);
		}
		else
		{
			$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
		}
		$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
	}
	else
	{
		$sh_from = explode('-',$year);
		$sh_to = explode('-',$month);
		$from = dateToMiladi($sh_from[2],$sh_from[1],$sh_from[0]);
		$to = dateToMiladi($sh_to[2],$sh_to[1],$sh_to[0]);
	}

	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	//create the all monthDay values for where: /2015/1216|/2015/1217
	$fridays = 0;
	foreach($period As $day)
	{
		$the_day = $day->format( "Y-m-d" );
		$day_code = date("w", strtotime($the_day));

		//if($day_code == 5 || $day_code == 4)//friday and tuesday
		if($day_code == 5)//friday
	        @$fridays++;
	}
	$holidays = getMonthHolidays($year,$month,$date_type);
	if($holidays)
	{
		$fridays = $fridays+$holidays->days;
	}
	return $fridays;

}

function counOffdays($year,$month,$date_type=0)
{
  if($date_type==0)
  {
    if($month< 7)
    {
        $sday = 1;
        $eday = 31;
    }
    elseif($month<12)
    {
        $sday = 1;
        $eday = 30;
    }
    else
    {
        $sday = 1;
        $eday = 29;
    }
    $from = dateToMiladi($year,$month,$sday);
    $to = dateToMiladi($year,$month,$eday);
  }
  else
  {
      $from = $year; 
      $to   = $month; 
  }
  
	$begin = new DateTime($from);
	$end = new DateTime($to);
	$end->modify('+1 day');
	$interval = DateInterval::createFromDateString('1 day');
    $period = new DatePeriod($begin, $interval, $end);
    //create the all monthDay values for where: /2015/1216|/2015/1217
    $fridays = 0;
	foreach($period As $day)
	{
        $the_day = $day->format( "Y-m-d" );
        $day_code = date("w", strtotime($the_day));

		//if($day_code == 5 || $day_code == 4)//friday and tuesday
		if($day_code == 5)//friday
	        @$fridays++;
    }
	return $fridays; exit;

}
function countMonthDays($year,$month)
{
	$sday = att_month_days(0);
	$eday = att_month_days(1);
	if($month == 1)
	{
		$from = dateToMiladi($year-1,12,$sday);
	}
	else
	{
		$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
	}
	$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\

	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	//create the all monthDay values for where: /2015/1216|/2015/1217
	$days = 0;
	foreach($period As $day)
	{
	    $days++;
	}
	return $days;

}
function getEmployeeAbsentDays($present=0,$year,$month,$date_type=0)
{
	if($date_type==0)
	{
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month==1)
		{
			$from = dateToMiladi($year-1,12,$sday);
		}
		else
		{
			$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
		}
		$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
	}
	else
	{
		$sh_from = explode('-',$year);
		$sh_to = explode('-',$month);
		$from = dateToMiladi($sh_from[2],$sh_from[1],$sh_from[0]);
		$to = dateToMiladi($sh_to[2],$sh_to[1],$sh_to[0]);
	}
	$begin = new DateTime($from);
	$end = new DateTime($to);
	$diff = $end->diff($begin)->format("%a");

	return $diff-$present;
}
function getEmployeeAbsentDays_count($present=0,$year,$month)
{
	$sday = att_month_days(0);
	$eday = att_month_days(1);
	if($month == 1)
	{
		$from = dateToMiladi($year-1,12,$sday);
	}
	else
	{
		$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
	}
	$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month
	if($to>date('Y-m-d'))
	{
		$to = date('Y-m-d');
	}
	$begin = new DateTime($from);
	$end = new DateTime($to);
	$diff = $end->diff($begin)->format("%a");

	return $diff-$present;
}

function getEmployeeLeaveDays_tillToday($emp_id=0,$year,$month)
{
	$sday = att_month_days(0);
	$eday = att_month_days(1)-1;
	//the last day of month is set to 16 as dateperiod() function skips the last day.
	//here we need to foreach till day 15 so we -1
	if($month == 1)
	{
		$from = dateToMiladi($year-1,12,$sday);
	}
	else
	{
		$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
	}
	$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month
	if($to>date('Y-m-d'))
	{
		$to = date('Y-m-d');
	}
	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	//create the all monthDay values for where: /2015/1216|/2015/1217
	$days = 0;
	foreach($period As $day)
	{
		$the_day = $day->format( "Y-m-d" );
		if(isEmployeeInLeave_today($emp_id,$the_day))
		{
	    	$days++;
	    }
	}
	return $days;

}
function getEmployeeLeaveDays_tillToday_old($emp_id=0,$year,$month)
{
	$sday = att_month_days(0);
	//$eday = att_month_days(1)-1;
	//the last day of month is set to 16 as dateperiod() function skips the last day.
	//here we need to foreach till day 15 so we -1
	if($month == 1)
	{
		$from = dateToMiladi($year-1,12,$sday);
	}
	else
	{
		$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
	}
	$to = date('Y-m-d');

	$result= DB::connection('hr')
			->table('leaves')
			->where('date_to','>=',$from)
			->where('date_from','<=',$to)
			//->where('date_to','<=',$to)
			//->whereRaw('(date_to >= '.$from.' AND date_to <= '.$to.') OR (date_from >= '.$from.' AND date_from <= '.$to.')')
			->where('employee_id',$emp_id)
			//->where('type','!=',7)//do not include the other type
			//->sum('days_no');
			->get();//get all records belong to this month
	$days=0;
	if($result)
	{
		foreach($result AS $row)
		{
			$begin = new DateTime($from);
			$end = new DateTime($row->date_to);
			$diff = date_diff($begin,$end)->format("%a")+1;//calculate the different between start day of the month and the end date of record

			if($row->days_no>$diff)
			{//if the leave days of the reocrd is bigger than difference it means leave is started before start of month, then we need the difference
				$days=$days+$diff;
			}
			else
			{
				if($to>=$row->date_to)
				{//if end date of current month is bigger than end date of record, it means the leave period is finished inside the month, then we need the number of days
					$days=$days+$row->days_no;
				}
				else
				{//if end date of current month is smaller than end date ot record, it means the leave priod is finished outside the month, then we need the differnce between stard date of record and end date of month
					$begin = new DateTime($row->date_from);
					$end = new DateTime($to);
					$diff = date_diff($begin,$end)->format("%a");//include the end date itself in count

					$days=$days+$diff;
				}
			}
		}
	}
	return $days;
}
function getEmployeeLeaveDays($emp_id=0,$year,$month)
{
	$sday = att_month_days(0);
	$eday = att_month_days(1)-1;
	//the last day of month is set to 16 as dateperiod() function skips the last day.
	//here we need to foreach till day 15 so we -1
	if($month == 1)
	{
		$from = dateToMiladi($year-1,12,$sday);
	}
	else
	{
		$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
	}
	$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month

	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	//create the all monthDay values for where: /2015/1216|/2015/1217
	$days = 0;
	foreach($period As $day)
	{
		$the_day = $day->format( "Y-m-d" );
		if(isEmployeeInLeave_today($emp_id,$the_day))
		{
	    	$days++;
	    }
	}
	return $days;
}
function getEmployeeLeaveDays_old($emp_id=0,$year,$month)
{
	$sday = att_month_days(0);
	$eday = att_month_days(1)-1;
	//the last day of month is set to 16 as dateperiod() function skips the last day.
	//here we need to foreach till day 15 so we -1
	if($month == 1)
	{
		$from = dateToMiladi($year-1,12,$sday);
	}
	else
	{
		$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
	}
	$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month

	$result= DB::connection('hr')
			->table('leaves')
			->where('date_to','>=',$from)
			->where('date_from','<=',$to)
			//->where('date_to','<=',$to)
			//->whereRaw('(date_to >= '.$from.' AND date_to <= '.$to.') OR (date_from >= '.$from.' AND date_from <= '.$to.')')
			->where('employee_id',$emp_id)
			->where('type','!=',7)//do not include the other type
			//->sum('days_no');
			->get();//get all records belong to this month
	$days=0;
	if($result)
	{
		foreach($result AS $row)
		{
			$begin = new DateTime($from);
			$end = new DateTime($row->date_to);
			$diff = date_diff($begin,$end)->format("%a")+1;//calculate the different between start day of the month and the end date of record

			if($row->days_no>$diff)
			{//if the leave days of the reocrd is bigger than difference then we need the difference
				$days=$days+$diff;
			}
			else
			{
				if($to>=$row->date_to)
				{//if end date of current month is bigger than end date of record, it means the leave period is finished inside the month, then we need the number of days
					$days=$days+$row->days_no;
				}
				else
				{//if end date of current month is smaller than end date ot record, it means the leave priod is finished outside the month, then we need the differnce between stard date of record and end date of month
					$begin = new DateTime($row->date_from);
					$end = new DateTime($to);
					$diff = date_diff($begin,$end)->format("%a")+1;//include the end date itself in count

					$days=$days+$diff;
				}
			}
		}
	}
	return $days;
}
function calculateTax($salary=0)
{
	if($salary<= 5000)
   	{
   		$main_tax = 0;
   	}
   	elseif($salary<=12500)
   	{
   		$salary = $salary - 5000;
   		$main_tax = ($salary*2)/100;
   	}
   	elseif($salary<=100000)
   	{
   		$salary = $salary - 12500;
   		$main_tax = (($salary*10)/100)+150;
   	}
   	else
   	{
   		$salary = $salary - 100000;
   		$main_tax = (($salary*20)/100)+8900;
   	}
   	return $main_tax;
}
function getEmployeeLeaves($emp_id=0,$type=0,$year=0,$month=0,$date_type=0)
{
	if($date_type==0)
	{
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month!=0)
		{
			if($month == 1)
			{
				$from = dateToMiladi($year-1,12,$sday);
			}
			else
			{
				$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
			}
			$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
		}
		else
		{
			$from = dateToMiladi($year,1,1);
			if(isItLeapYear($year))
			{
				$to = dateToMiladi($year,12,30);
			}
			else
			{
				$to = dateToMiladi($year,12,29);
			}
		}
	}
	else
	{
		$sh_from = explode('-',$year);
		$sh_to = explode('-',$month);
		$from = dateToMiladi($sh_from[2],$sh_from[1],$sh_from[0]);
		$to = dateToMiladi($sh_to[2],$sh_to[1],$sh_to[0]);
	}
	$result= DB::connection('hr')
			->table('leaves')
			->where('date_to','>=',$from)
			->where('date_from','<=',$to)
			//->where('date_to','<=',$to)
			//->whereRaw('(date_to >= '.$from.' AND date_to <= '.$to.') OR (date_from >= '.$from.' AND date_from <= '.$to.')')
			->where('employee_id',$emp_id)
			->where('type',$type)
			//->sum('days_no');
			->get();//get all records belong to this month
	$days=0;
	if($result)
	{
		foreach($result AS $row)
		{
			$begin = new DateTime($from);
			$end = new DateTime($row->date_to);
			$diff = date_diff($begin,$end)->format("%a")+1;//calculate the different between start day of the month and the end date of record

			if($row->days_no>$diff)
			{//if the leave days of the reocrd is bigger than difference then we need the difference
				$days=$days+$diff;
			}
			else
			{
				if($to>=$row->date_to)
				{//if end date of current month is bigger than end date of record, it means the leave period is finished inside the month, then we need the number of days
					$days=$days+$row->days_no;
				}
				else
				{//if end date of current month is smaller than end date ot record, it means the leave priod is finished outside the month, then we need the differnce between stard date of record and end date of month
					$begin = new DateTime($row->date_from);
					$end = new DateTime($to);
					$diff = date_diff($begin,$end)->format("%a")+1;//include the end date itself in count

					$days=$days+$diff;
				}
			}
		}
	}
	return $days;
}
function getEmployeeLeaves_ajir($emp_id=0,$type=0,$year=0,$month=0,$date_type=0)
{

   if($date_type==0)
   {
        if($month< 7)
        {
            $sday = 1;
            $eday = 31;
        }
        elseif($month<12)
        {
            $sday = 1;
            $eday = 30;
        }
        else
        {
            $sday = 1;
            $eday = 29;
        }
        $from = dateToMiladi($year,$month,$sday);
        $to = dateToMiladi($year,$month,$eday);
    }
    else
    {
        $from = $year; 
        $to   = $month; 
    }

	$result= DB::connection('hr')
			->table('leaves')
			->where('date_to','>=',$from)
			->where('date_from','<=',$to)
			//->where('date_to','<=',$to)
			//->whereRaw('(date_to >= '.$from.' AND date_to <= '.$to.') OR (date_from >= '.$from.' AND date_from <= '.$to.')')
			->where('employee_id',$emp_id)
			->where('type',$type)
			//->sum('days_no');
			->get();//get all records belong to this month
	$days=0;
	if($result)
	{
		foreach($result AS $row)
		{
			$begin = new DateTime($from);
			$end = new DateTime($row->date_to);
			$diff = date_diff($begin,$end)->format("%a")+1;//calculate the different between start day of the month and the end date of record

			if($row->days_no>$diff)
			{//if the leave days of the reocrd is bigger than difference then we need the difference
				$days=$days+$diff;
			}
			else
			{
				if($to>=$row->date_to)
				{//if end date of current month is bigger than end date of record, it means the leave period is finished inside the month, then we need the number of days
					$days=$days+$row->days_no;
				}
				else
				{//if end date of current month is smaller than end date ot record, it means the leave priod is finished outside the month, then we need the differnce between stard date of record and end date of month
					$begin = new DateTime($row->date_from);
					$end = new DateTime($to);
					$diff = date_diff($begin,$end)->format("%a")+1;//include the end date itself in count

					$days=$days+$diff;
				}
			}
		}
	}
	return $days;
}
function getEmployeeLeaves_dates_ajir($emp_id=0,$type=0,$year=0,$month=0,$date_type)
{
   
   if($date_type==0)
   { 
    if($month< 7)
    {
        $sday = 1;
        $eday = 31;
    }
    elseif($month<12)
    {
        $sday = 1;
        $eday = 30;
    }
    else
    {
        $sday = 1;
        $eday = 29;
    }
    $from = dateToMiladi($year,$month,$sday);
    $to = dateToMiladi($year,$month,$eday);
   }
   else
   {
       $from = $year; 
       $to   = $month; 
   }
	return DB::connection('hr')
			->table('leaves')
			->where('employee_id',$emp_id)
			->where('type',$type)
			//->whereBetween('date_from',array($from,$to))
			//->whereBetween('date_to',array($from,$to))
			->whereRaw('((date_from >= "'.$from.'" AND date_from <= "'.$to.'") OR (date_to >= "'.$from.'" AND date_to <= "'.$to.'"))')
			->get();
}
function getEmployeeLeaves_dates($emp_id=0,$type=0,$year=0,$month=0,$date_type)
{
	if($date_type==0)
	{
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month!=0)
		{
			if($month == 1)
			{
				$from = dateToMiladi($year-1,12,$sday);
			}
			else
			{
				$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
			}
			$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
		}
		else
		{
			$from = dateToMiladi($year,1,1);
			if(isItLeapYear($year))
			{
				$to = dateToMiladi($year,12,30);
			}
			else
			{
				$to = dateToMiladi($year,12,29);
			}
		}
	}
	else
	{
		$sh_from = explode('-',$year);
		$sh_to = explode('-',$month);
		$from = dateToMiladi($sh_from[2],$sh_from[1],$sh_from[0]);
		$to = dateToMiladi($sh_to[2],$sh_to[1],$sh_to[0]);
	}

	return DB::connection('hr')
			->table('leaves')
			->where('employee_id',$emp_id)
			->where('type',$type)
			->whereRaw('((date_from >= "'.$from.'" AND date_from <= "'.$to.'") OR (date_to >= "'.$from.'" AND date_to <= "'.$to.'"))')
			->get();
}
function getEmployeeLeaves_total($emp_id=0,$year=0,$month=0)
{
	//$sday = att_month_days(0);
	//$eday = att_month_days(1);
	if($month!=0)
	{
		$from = dateToMiladi($year,$month,1);
		if($month<7)
		{//first 6 months is 31 days
			$to = dateToMiladi($year,$month,31);
		}
		elseif($month<12)
		{//other months is 30 days
			$to = dateToMiladi($year,$month,30);
		}
		else
		{//last month is either 30 or 29
			if(isItLeapYear($year))
			{//if it is leap year, then it is 30 days
				$to = dateToMiladi($year,$month,30);
			}
			else
			{//other wise the last month is 29 days
				$to = dateToMiladi($year,$month,29);
			}
		}
	}
	else
	{
		$from = dateToMiladi($year,1,1);
		if(isItLeapYear($year))
		{//if it is leap year, then it is 30 days
			$to = dateToMiladi($year,12,30);
		}
		else
		{//other wise the last month is 29 days
			$to = dateToMiladi($year,12,29);
		}

	}

	return DB::connection('hr')
			->table('leaves')
			->where('employee_id',$emp_id)
			//->where('type','!=',7)//except others
			->whereIn('type',array(1,2,3))
			->whereRaw('((date_from >= "'.$from.'" AND date_from <= "'.$to.'") OR (date_to >= "'.$from.'" AND date_to <= "'.$to.'"))')
			->sum('days_no');
}
function isEmployeeInLeave($emp_id=0,$date=null,$year=0,$month=0,$date_type=0)
{
	if($date_type==0)
	{
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month!=0)
		{
			if($month == 1)
			{
				$from = dateToMiladi($year-1,12,$sday);
			}
			else
			{
				$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
			}
			$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
		}
		else
		{
			$from = dateToMiladi($year,1,1);
			if(isItLeapYear($year))
			{
				$to = dateToMiladi($year,12,30);
			}
			else
			{
				$to = dateToMiladi($year,12,29);
			}
		}
	}
	else
	{
		$sh_from = explode('-',$year);
		$sh_to = explode('-',$month);
		$from = dateToMiladi($sh_from[2],$sh_from[1],$sh_from[0]);
		$to = dateToMiladi($sh_to[2],$sh_to[1],$sh_to[0]);
    }
	$data = DB::connection('hr')
			->table('leaves')
			->where('employee_id',$emp_id)
			//->where('type',$type)
			->whereRaw('((date_from >= "'.$from.'" AND date_from <= "'.$to.'") OR (date_to >= "'.$from.'" AND date_to <= "'.$to.'"))')
			//->whereRaw('(date_from <= "'.$date.'" AND date_to <= "'.$date.'")')
			->get();
	if($data)
	{
		foreach($data as $rec)
		{
			if($date >= $rec->date_from && $date <=$rec->date_to)
			{
				return $rec->type;
			}
		}
	}
	return false;

}
function isEmployeeInLeave_today($emp_id=0,$date='')
{
	$data = DB::connection('hr')
			->table('leaves')
			->where('employee_id',$emp_id)
			->where('hr_approved',1)
			//->where('type',$type)
			//->whereRaw('((date_from >= "'.$from.'" AND date_from <= "'.$to.'") OR (date_to >= "'.$from.'" AND date_to <= "'.$to.'"))')
			->whereRaw('(date_from <= "'.$date.'" AND date_to >= "'.$date.'")')
			->first();
	return $data;
}
function getAttendanceTime($year=0,$month=0)
{
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	return DB::connection('hr')
			->table('attendance_time')
			->where('year',$year)
			->where('month',$month)
			->where('thu',0)
			->where('dep_type',$user_dep_type)
			->first();
}
function getAttendanceTime_thu($year=0,$month=0)
{
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	return DB::connection('hr')
			->table('attendance_time')
			->where('year',$year)
			->where('month',$month)
			->where('thu',1)
			->where('dep_type',$user_dep_type)
			->first();
}
function getMonthHolidays($year=0,$month=0,$date_type=0)
{
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	if($date_type==0)
	{
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month==1)
		{
			$from = dateToMiladi($year-1,12,$sday);
		}
		else
		{
			$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
		}
		$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
	}
	else
	{
		$sh_from = explode('-',$year);
		$sh_to = explode('-',$month);
		$from = dateToMiladi($sh_from[2],$sh_from[1],$sh_from[0]);
		$to = dateToMiladi($sh_to[2],$sh_to[1],$sh_to[0]);
	}
	return DB::connection('hr')

			->table('month_holiday_dates')
			->select(DB::raw('count(id) As days'))
			->where('month_holidays_id','>',0)//dont enclude emergency holidays
			->where('date','>=',$from)
			->where('date','<',$to)//the eday is one day bigger than real day
			->where('dep_type',$user_dep_type)
			->first();
}
function getMonthHolidays_ajirs($year=0,$month=0,$date_type=0)
{
   $user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
    if($date_type==0)
    {
        if($month< 7)
        {
            $sday = 1;
            $eday = 31;
        }
        elseif($month<12)
        {
            $sday = 1;
            $eday = 30;
        }
        else
        {
            $sday = 1;
            $eday = 29;
        }
        $from = dateToMiladi($year,$month,$sday);
        $to = dateToMiladi($year,$month,$eday);
    }
    else
    {
        $from = $year; 
        $to   = $month; 
    }
    
	$days =  DB::connection('hr')

			->table('month_holiday_dates')
			->select(DB::raw('count(id) As days'))
			->where('month_holidays_id','>',0)//dont enclude emergency holidays
			->where('date','>=',$from)
			->where('date','<',$to)//the eday is one day bigger than real day
			->where('dep_type',$user_dep_type)
			->first();
	if($days)
	{
		return $days->days;
	}
	else
	{
		return 0;
	}
}
function getEmployeeFullName($id=0)
{
	return DB::connection('hr')->table('employees')->where('id',$id)->pluck('name_dr');
}
function getImageDetail($id,$day=1)
{
	$from = dateToMiladi(1394,10,$day);//the start day of attendance month is 15 of prevouce month
	$date = explode('-',$from);

	$days = '/'.$date[0].'/'.$date[1].$date[2];
	//remove the last pipe line

	$total =DB::connection('hr')
			->table('attendance_images')
			->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total,path'))
			->where('RFID',$id)//check the rfid
			//->where('status',0)
			->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
			->groupBy('groupByMe')
			//->having('total', '>=', 2)
			->get();


	return $total;
}
function getHRUserDeps()
{
	return DB::connection('hr')->table('hruser_deps')->select('deps')->where('user_id',Auth::user()->id)->first();
}
function getDailyAtt($dep=0,$sub=0,$type=0)
{
	$table= DB::connection('hr')
		->table('employees AS e')
		//->select(DB::raw('count(e.id) As total'))
		->join('attendance_images_today AS t1', 't1.RFID','=','e.RFID');
		if($type==1)
		{
			$table->where('e.employee_type','!=',2);
		}
		elseif($type==2)
		{
			$table->where('e.employee_type',2);
		}
		if($dep!=0)
		{
			if($sub!=0)
			{
				$table->where('e.department',$sub);
			}
			else
			{
				$table->where('e.general_department',$dep);
			}
		}
		$table->where('e.RFID','>',0);
		$table->where('e.position_dr','!=',4);//entezar mahsh
		$table->where('e.changed',0);//not changed to out
		$table->where('e.retired',0);//retire

		$table->where('e.fired',0);
		$table->where('e.resigned',0);
		$table->where('e.vacant','!=',1);//kambod
		$table->where('e.vacant','!=',4);//ezafa bast
		$table->where('e.tashkil_id','!=',0);//ezafa bast
	$number=$table->count();
	return $number;


}
function getDailyAbsent($dep=0,$sub=0,$type=1)
{
	$present = getDailyAtt($dep,$sub,$type);
	$table= DB::connection('hr')
		->table('employees AS e')
		->where('e.vacant','!=',1)
		->where('e.vacant','!=',4)
		->where('e.fired',0)
		->where('e.resigned',0)
		->where('e.tashkil_id','!=',0)
		->where('e.RFID','>',0)
		->where('e.position_dr','!=',4)//entezar mahsh
		->where('e.changed',0)//not changed to out
		->where('e.retired',0);//retire
		if($type==1)
		{
			$table->where('e.employee_type','!=',2);
		}
		elseif($type==2)
		{//ajirs
			$table->where('e.employee_type',2);
		}
		if($dep!=0)
		{
			if($sub!=0)
			{
				$table->where('e.department',$sub);
			}
			else
			{
				$table->where('e.general_department',$dep);
			}
		}

	$number=$table->count();
	$absent = $number-$present;
	return $absent;
}
function getAttTime($year=0,$month=0,$type='in')
{
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	$result = DB::connection('hr')->table('attendance_time')->where('year',$year)->where('month',$month)->where('thu',0)->where('dep_type',$user_dep_type)->first();
	if(!$result)
	{
		$result = DB::connection('hr')->table('attendance_time')->orderBy('id','desc')->where('thu',0)->where('dep_type',$user_dep_type)->first();
	}

	if($type == 'in')
	{
		$time = explode(':',$result->time_in);
		$time = $time[0].$time[1].$time[2];
		return $time;
	}
	else
	{
		$time = explode(':',$result->time_out);
		$time = $time[0].$time[1].$time[2];
		return $time;
	}
}
function getAttTime_thu($year=0,$month=0,$type='in')
{
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	$result = DB::connection('hr')->table('attendance_time')->where('year',$year)->where('month',$month)->where('thu',1)->where('dep_type',$user_dep_type)->first();
	if(!$result)
	{
		$result = DB::connection('hr')->table('attendance_time')->orderBy('id','desc')->where('thu',1)->where('dep_type',$user_dep_type)->first();
	}

	if($type == 'in')
	{
		$time = explode(':',$result->time_in);
		$time = $time[0].$time[1].$time[2];
		return $time;
	}
	else
	{
		$time = explode(':',$result->time_out);
		$time = $time[0].$time[1].$time[2];
		return $time;
	}
}
function getEmployeePayrollDetTotal($emp_id=0,$year,$month,$type='in')
{
	$total =0;
	$table= DB::connection('hr')
		->table('employee_payrolls')
		->where('employee_id',$emp_id)
		->where('year',$year)
		->where('month',$month)
		->first();
	if($table)
		if($type=='in')
		{
			$total = $table->makolat+$table->car_rent+$table->prr+$table->khatar+$table->other_benifit;
		}
		else
		{
			$total = $table->robh+$table->tazmin+$table->makolat_ksor+$table->bank+$table->returns+$table->other_ksor;
		}

	return $total;
}
function getEmployeeSalaryWait($emp_id=0,$year,$month)
{
	$table= DB::connection('hr')
		->table('employee_salary_waits')
		->where('employee_id',$emp_id)
		->where('year',$year)
		->where('month',$month)
		->first();

	return $table;
}
function canAttHoliday()
{
	$section = 'hr_attendance';
	$default_module = getCurrentAppCode();//getCurrentAppCode();

	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['att_holiday']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
function canCheckAtt()
{
	$section = 'hr_attendance';
	$default_module = 'hr';//getCurrentAppCode();

	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['check_att']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
function canViewChart()
{
	$section = 'hr_attendance';
	$default_module = getCurrentAppCode();//getCurrentAppCode();

	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['view_charts']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
function canAddHoliday()
{
	$section = 'hr_attendance';
	$default_module = getCurrentAppCode();//getCurrentAppCode();

	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['add_holiday']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
function canCheckImages()
{
	$section = 'hr_attendance';
	$default_module = getCurrentAppCode();//getCurrentAppCode();

    $roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['att_checkImg']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
function canAttTodayAtt()
{
	$section = 'hr_attendance';
	$default_module = getCurrentAppCode();//getCurrentAppCode();

	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['att_todayAtt']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
function getDepPresentDays($id=0,$year,$month,$day=1)
{
	$att_time_in = getAttTime($year,$month,'in');
	$att_time_out = getAttTime($year,$month,'out');

	$to = dateToMiladi($year,$month,$day);//the end day of attendance month is 15 of current month\
	$period_det = explode('-',$to);
	$period_day = $period_det[2];
	$period_month = $period_det[1];
	$period_year = $period_det[0];

	$days = '/'.$period_year.'/'.$period_month.$period_day;

	$total =DB::connection('hr')
			->table('attendance_images AS t1')
			->select(DB::raw('count(t1.id) As total'))
			->leftjoin('employees AS em','em.RFID','=','t1.RFID')
			->where('em.department',$id)//check the rfid
			->where('t1.status',0)
			->whereRaw('(SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)<='.$att_time_in.' || SUBSTRING_INDEX(SUBSTRING_INDEX(path,"_",1),"/",-1)>='.$att_time_out.')')
			->where('t1.path','like',"%$days%")//check the year/monthDay in image path
			->where('em.tashkil_id','!=',0)
			->groupBy('em.department')
			->having('total', '>=', 2)
			->first();
	if($total)
	{
		return $total->total;

	}
	else
	{
		return 0;
	}
}
function countMonthDays1($year,$month)
{
	if($month == 12)
	{
		$to = dateToMiladi($year+1,1,1);
	}
	else
	{
		$to = dateToMiladi($year,$month+1,1);//the start day of attendance month is 15 of prevouce month
	}
	$from = dateToMiladi($year,$month,1);

	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	//create the all monthDay values for where: /2015/1216|/2015/1217
	$days = 0;
	foreach($period As $day)
	{
	    $days++;
	}
	return $days;

}
function employeeKhedmat($emp_id=0)
{
	$table= DB::connection('hr')
		->table('employee_experiences')
		//->select('date_from','date_to')
		->where('employee_id',$emp_id)
		->where('type',1)
		->orderBy('id','desc')
		->get();

	return $table;
}
function employeeKhedmat_aop($emp_id=0)
{
	$table= DB::connection('hr')
		->table('employee_experiences')
		->select('date_from','date_to')
		->where('employee_id',$emp_id)
		->where('type',1)
		->where('internal',1)
		->get();

	return $table;
}
function is_in_shift($rfid=0,$year='1395',$month='6',$type=1)
{
	$table= DB::connection('hr')
		->table('att_shifts')
		->where('rfid',$rfid)
		->where('month',$year.$month)
		->where('status',$type)
		->first();
	if($table)
	{
		return $table;
	}
	else
	{//if sift is not added for given month, take the last month time
		$last_month = DB::connection('hr')->table('att_shifts')->where('rfid',$rfid)->where('month','<',$year.$month)->max('month');
		$table2= DB::connection('hr')
		->table('att_shifts')
		->where('rfid',$rfid)
		->where('month',$last_month)
		->where('status',$type)
		->first();
		if($table2)
		{
			return $table2;
		}
		else
		{
			return false;
		}
	}
}

function is_in_thu_shift($rfid=0,$year='1395',$month='6',$type=0,$date_type=0)
{
	if($date_type==0)
	{
		if(strlen($month)<2)
		{
			$month = '0'.$month;
		}

		$table= DB::connection('hr')
			->table('att_shifts')
			->where('rfid',$rfid)
			->where('month','>',$year.'00')
			->where('month','<=',$year.$month)
			->where('status',$type)//0:thu,3:sun
			->first();
	}
	else
	{
		$from = explode('-', $year);
		$from_year = $from[2];
		$from_month = $from[1];
		$to = explode('-', $month);
		$to_year = $to[2];
		$to_month = $to[1];

		$table= DB::connection('hr')
			->table('att_shifts')
			->where('rfid',$rfid)
			->where('month','<=',$to_year.$to_month)
			->where('month','>',$from_year.$from_month)
			->where('status',$type)//0:thu,3:sun
			->first();
	}
	if($table)
	{
		return $table;
	}
	else
	{
		return false;
	}
}

/** 
 * @Author: Jamal Yousufi  
 * @Date: 2019-11-27 15:26:17 
 * @Desc: Check if employee is in the University shift   
 */
function is_in_university_shift($rfid=0,$year='1395',$month='6',$type=0,$date_type=0)
{
	if($date_type=='compelete_month_date')
	{
	    $table = DB::connection('hr')
			->table('att_shifts')
            ->where('rfid',$rfid)
			->where('status',$type)//0:thu,3:sun
			->where('start_date','<=',$year)
			->orWhere('end_date','>=',$year)
            ->first();
            
		return $table; 	
	}
	elseif($date_type==0)
	{
	
          
		$table = DB::connection('hr')
			->table('att_shifts')
            ->where('rfid',$rfid)
            ->whereYear('start_date','=',$year)
            ->whereYear('end_date','=',$year)
			->whereMonth('start_date','<=',$month)
			->whereMonth('end_date','>=',$month-1)
			->where('status',$type)//0:thu,3:sun
            ->first();
            
		return $table; 
	}
	else
	{
		$from = explode('-', $year);
		$from_year = $from[2];
		$from_month = $from[1];
		$to = explode('-', $month);
		$to_year = $to[2];
		$to_month = $to[1];

		$table= DB::connection('hr')
			->table('att_shifts')
			->where('rfid',$rfid)
			->where('month','<=',$to_year.$to_month)
			->where('month','>',$from_year.$from_month)
			->where('status',$type)//0:thu,3:sun
			->first();
	}
	if($table)
	{
		return $table;
	}
	else
	{
		return false;
	}
}


function is_in_night_shift($rfid=0,$table='night_shifts')
{
	$table= DB::connection('hr')
		->table($table)
		->where('rfid',$rfid)
		->first();

	if($table)
	{
		return $table;
	}
	else
	{
		return false;
	}
}

function is_in_ThuFri_shift($rfid=0)
{
	$table= DB::connection('hr')
		->table('thuFri_shifts')
		->where('rfid',$rfid)
		->first();

	if($table)
	{
		return $table;
	}
	else
	{
		return false;
	}
}

function att_month_days($type=0)
{
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	if($user_dep_type==2)
	{
		if($type==0)
		{//start day
			return 11;
		}
		else
		{//end day
			return 11;
		}
	}
	else
	{
		if($type==0)
		{//start day
			return 11;
		}
		else
		{//end day
			return 11;
		}
	}
}
function lost_rfid($id=0)
{
	$table= DB::connection('hr')
		->table('emp_rfids')
		->select('rfid')
		->where('employee_id',$id)
		//->where('status',0)
		->get();
	DB::disconnect('hr');
	if($table)
	{
		return $table;
	}
	else
	{
		return false;
	}
}
function lost_rfidByRFID($rfid=0)
{
	$table= DB::connection('hr')
		->table('emp_rfids as r')
		->select('r.rfid')
		->leftjoin('employees AS em','em.id','=','r.employee_id')
		->where('em.RFID',$rfid)
		//->where('status',0)
		->get();
	DB::disconnect('hr');
	if($table)
	{
		return $table;
	}
	else
	{
		return false;
	}
}
function checkHoliday($the_day="")
{
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	$table= DB::connection('hr')
		->table('month_holiday_dates')
		->select('id','desc')
		->where('date',$the_day)
		->where('month_holidays_id','!=',0)
		->where('dep_type',$user_dep_type)
		->first();
	if($table)
	{
		return $table;
	}
	else
	{
		return false;
	}
}
function checkUrgentHoliday($the_day="")
{
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	$table= DB::connection('hr')
		->table('month_holiday_dates')
		->select('id','desc')
		->where('date',$the_day)
		->where('month_holidays_id',0)
		->where('dep_type',$user_dep_type)
		->first();
	if($table)
	{
		return $table;
	}
	else
	{
		return false;
	}
}
function isImageRejected($the_day="",$rfid=0)
{
	//$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	$table= DB::connection('hr')
		->table('attendance_images')
		->where('date',$the_day)
		->where('status',1)
		->where('RFID',$rfid)
		->first();
	if($table)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function is_manager_att($user_id=0)
{
	$table = DB::table('users as t1')
			->select('t1.position_id','em.general_department as gen_dep','em.department as sub_dep')
			->leftjoin('hr.employees AS em','em.id','=','t1.employee_id')
			->where('t1.id',$user_id)
			->first();
	if($table)
	{
		return $table;
	}
	else
	{
		return false;
	}
}
function isItLeapYear($year=1395)
{
	$a = 0; $b = 1309; $c = $year;
	for($i=1309;$i<=$c-4;$i+= 4)
    {
        // اضافه کردن یک دوره سال کبیسه
        $b += 4;
        // اضافه کردن یک دوره برای برسی دوره ۸ ساله
        $a += 1;
        //
        if ($a % 8 == 0) $b++;
    }
    if($c==$b)
    {
    	return true;
    }
    else
    {
    	return false;
    }
}
function getEmployeeEducation($emp_id=0)
{
	$table = DB::connection('hr')
			->table('auth.education_degree as e')
			->select('e.name_dr')
			->leftjoin('employee_educations AS f','f.education_id','=','e.id')
			->where('f.employee_id',$emp_id)
			->orderBy('f.id','desc')
			->first();
	return $table;
}
function getEmployeeDetails($emp_id=0)
{
	$table = DB::connection('hr')
			->table('employees as e')
			->leftjoin('auth.department AS d','d.id','=','e.department')
			->select('d.name as dep_name','e.first_date_appointment')

			->where('e.id',$emp_id)
			->first();
	return $table;
}
function getEmployeeChangesDetails($id=0,$emp_type=0)
{

	if($emp_type ==1)
	{
		$table = DB::connection('hr')
			->table('employee_changes as e')
			->leftjoin('auth.ministries AS d','d.id','=','e.ministry_id')
			->leftjoin('auth.military_rank AS r','r.id','=','e.military_bast')
			->select('d.name_dr as dep_name','r.name_dr as bast_name')
			->where('e.id',$id);
		return $table->first();
	}
	else
	{
		$table = DB::connection('hr')
			->table('employee_changes as e')
			->select('d.name as dep_name','r.name_dr as bast_name')
		  ->leftjoin('auth.department AS d','d.id','=','e.department')
		  ->leftjoin('auth.employee_rank AS r','r.id','=','e.emp_bast')
	    ->where('e.id',$id)
	    ->first();
		return $table;
	}

}
function getUserDepType($id=0)
{
	$table = DB::connection('hr')
			->table('employees as e')
			->select('e.dep_type')
			->leftjoin('auth.users AS u','u.employee_id','=','e.id')
			->where('u.id',$id)
			->first();
	DB::disconnect('hr');
	return $table;
}
function getUserDepTypeByRFID($rfid=0)
{
	$table = DB::connection('hr')
			->table('employees as e')
			//->select('e.dep_type')
			->where('e.RFID',$rfid)
			->pluck('e.dep_type');
	DB::disconnect('hr');
	return $table;
}
function getHRNotifications($getCount = false)
{
	$object = DB::connection('hr')
				->table('notifications AS t1')
				//->select("t1.id AS notification_id","t1.to","t1.from","t1.type","t2.title","t1.created_at")
				//->leftJoin("employees AS t2","t2.id","=","t1.employee_id")
				->where('to',Auth::user()->employee_id)
				->where('t1.status',0)
				->get();
	//check if wanted count of notifications
	if($getCount)
	{
		return count($object);
	}
	else
	{
		$li = "";

		foreach($object AS $item)
		{
			$notification_type = "";
			$url = URL::route('leaveForm_add',array($item->id));
			if($item->type == 1)
			{//dir leave rejected
				$notification_type = '<span style="font-weight:normal;color:red;">'.$item->content.'</span>';
			}
			elseif($item->type == 2)
			{//dir leave approved
				$notification_type = '<span style="font-weight:normal;color:green;">'.$item->content.'</span>';
			}
			elseif($item->type == 3)
			{//dir leave approved
				$notification_type = '<span style="font-weight:normal;color:green;">'.$item->content.'</span>';
			}
	        //$li .= '<li class="item">';
	        $li .= '<a class="list-group-item" role="menuitem" href="'.$url.'"><div class="media"><div class="media-left padding-right-10">';
	        if($item->from!=0)
	        {
	       		$image = getProfilePicture($item->from);
	       		$li .= HTML::image('/img/'.$image,'',array('class' => 'notification-logo'));
	       		$li .= '</div><div class="media-body">';

		    	$li .= '<h6 class="media-heading">
		                '.getUserFullName($item->from).' - '.$notification_type.'
		            	</h6>';
	       	}
	       	else
	       	{
	       		$li .= '<h6 class="media-heading">
		                '.$notification_type.'
		            	</h6>';
	       	}
	       	$li .= '<time class="media-meta"><i class="fa fa-clock-o"></i>'.$item->created_at.'</time>';
	       	$li .= '</div></div></a>';
			//$li .= '</li>';
		}

		//get other notification
		$li.= getDocRejectNotify();
		//return elements
		return $li;
	}
}
function checkHolidayAbsent($sdate,$rfid=0)
{
	$before = false;//by default the day before is absent
	$after = false;
	$thedate = explode('-', $sdate);
	$from_unix_time = mktime(0, 0, 0,$thedate[1],$thedate[2],$thedate[0]);//unix number of the date to find the day before
	$day_before = strtotime("yesterday", $from_unix_time);//the day before the date
	$yesterday = date('Y-m-d', $day_before);

	//check the day before
	$holiday = checkHoliday($yesterday);
	$urgents = checkUrgentHoliday($yesterday);
	$thu = false;
	$dayOfWeek = date('D',strtotime($yesterday));
	if($dayOfWeek=='Thu')
	{
		$thedate = explode('-', $yesterday);
		$sh_date = dateToShamsi($thedate[0],$thedate[1],$thedate[2]);
		$sh_date = explode('-', $sh_date);
		$thu = is_in_thu_shift($rfid,$sh_date[0],$sh_date[1]);
	}

	//$isDaybeforeOff=false;
	while($holiday || $urgents || $dayOfWeek=='Fri' || $thu)
	{//the day before is also off, so check the day before it till an official day is found
		$thedate = explode('-', $yesterday);
		$from_unix_time = mktime(0, 0, 0,$thedate[1],$thedate[2],$thedate[0]);
		$day_before = strtotime("yesterday", $from_unix_time);
		$yesterday = date('Y-m-d', $day_before);
		$holiday = checkHoliday($yesterday);
		$urgents = checkUrgentHoliday($yesterday);
		$thu = false;
		$dayOfWeek = date('D',strtotime($yesterday));
		if($dayOfWeek=='Thu')
		{
			$thedate = explode('-', $yesterday);
			$sh_date = dateToShamsi($thedate[0],$thedate[1],$thedate[2]);
			$sh_date = explode('-', $sh_date);
			$thu = is_in_thu_shift($rfid,$sh_date[0],$sh_date[1]);
		}
		//$isDaybeforeOff=true;
	}

	if(checkIfEmployeeIsPresent($rfid,$yesterday))
	{//if uesr is present the day before off day, then no issue to request leave
		$before = false;
	}
	else
	{//user is absent the day before off day
		$before = true;
	}
	//check the day after
	$thedate = explode('-', $sdate);
	$from_unix_time = mktime(0, 0, 0,$thedate[1],$thedate[2],$thedate[0]);
	$day_after = strtotime("tomorrow", $from_unix_time);
	$tomorrow = date('Y-m-d', $day_after);
	$holiday = checkHoliday($tomorrow);
	$urgents = checkUrgentHoliday($tomorrow);
	$thu = false;
	$dayOfWeek = date('D',strtotime($tomorrow));
	if($dayOfWeek=='Thu')
	{
		$thedate = explode('-', $tomorrow);
		$sh_date = dateToShamsi($thedate[0],$thedate[1],$thedate[2]);
		$sh_date = explode('-', $sh_date);
		$thu = is_in_thu_shift($rfid,$sh_date[0],$sh_date[1]);
	}
	while($holiday || $urgents || $dayOfWeek=='Fri' || $thu)
	{//the day before is also off, so check the day before it
		$thedate = explode('-', $tomorrow);
		$from_unix_time = mktime(0, 0, 0,$thedate[1],$thedate[2],$thedate[0]);
		$day_after = strtotime("tomorrow", $from_unix_time);
		$tomorrow = date('Y-m-d', $day_after);
		$holiday = checkHoliday($tomorrow);
		$urgents = checkUrgentHoliday($tomorrow);
		$dayOfWeek = date('D',strtotime($tomorrow));
		if($dayOfWeek=='Thu')
		{
			$thedate = explode('-', $tomorrow);
			$sh_date = dateToShamsi($thedate[0],$thedate[1],$thedate[2]);
			$sh_date = explode('-', $sh_date);
			$thu = is_in_thu_shift($rfid,$sh_date[0],$sh_date[1]);
		}
	}
	if(checkIfEmployeeIsPresent($rfid,$tomorrow))
	{//if uesr is present, then no issue to request leave
		$after = false;
	}
	else
	{//user is absent the day before off day
		$after = true;
	}
	$resutl = $after && $before;
	return $resutl;//if true, the employee is absent before and after holiday, the holidya shoud count as absent
}
function checkLeaveOffday($sdate,$edate,$rfid=0,$emp_id=0)
{
	$days = date_diff(date_create($sdate),date_create($edate));
	$requested_leaves = (int)$days->format("%a")+1;
	$thedate = explode('-', $sdate);
	$from_unix_time = mktime(0, 0, 0,$thedate[1],$thedate[2],$thedate[0]);//unix number of the date to find the day befor
	$day_before = strtotime("yesterday", $from_unix_time);//the day before the date
	$yesterday = date('Y-m-d', $day_before);

	if($requested_leaves==1)
	{//only one day leave request
		$holiday = checkHoliday($sdate);
		$urgents = checkUrgentHoliday($sdate);
		$dayOfWeek = date('D',strtotime($sdate));
		if($holiday || $urgents || $dayOfWeek=='Fri')
		{//the user requested leave in a off day
			return 'offDayLeave';
		}
		if(isImageRejected($sdate,$rfid))
		{//the image in this date is rejected, so he/she is not allowd to take leave
			return 'extraLeave';
		}
	}
		//check the day before
		$holiday = checkHoliday($yesterday);
		$urgents = checkUrgentHoliday($yesterday);
		$dayOfWeek = date('D',strtotime($yesterday));
		$isDaybeforeOff=false;
		while($holiday || $urgents || $dayOfWeek=='Fri')
		{//the day before is also off, so check the day before it till an official day is found
			$thedate = explode('-', $yesterday);
			$from_unix_time = mktime(0, 0, 0,$thedate[1],$thedate[2],$thedate[0]);
			$day_before = strtotime("yesterday", $from_unix_time);
			$yesterday = date('Y-m-d', $day_before);
			$holiday = checkHoliday($yesterday);
			$urgents = checkUrgentHoliday($yesterday);
			$dayOfWeek = date('D',strtotime($yesterday));
			$isDaybeforeOff=true;
		}

		if(checkIfEmployeeIsPresent($rfid,$yesterday))
		{//if uesr is present the day before off day, then no issue to request leave
			$result = 'true';
		}
		else
		{//user is absent the day before off day
			$isInLeave = isEmployeeInLeave_today($emp_id,$yesterday);
			if($isInLeave)
			{//if the day before the date, employee is in leave, he/she is not allowed to take normal leaves except extra sick leave by attendance
				if($isInLeave->type==7 || $isInLeave->type==8 || $isInLeave->type==9)
				{
					return 'true';
				}
				else
				{
					return 'extraLeave';
				}
			}
			else
			{
				$result = 'absent';
				return $result;
			}
		}
		//check the day after
		$thedate = explode('-', $edate);
		$from_unix_time = mktime(0, 0, 0,$thedate[1],$thedate[2],$thedate[0]);
		$day_after = strtotime("tomorrow", $from_unix_time);
		$tomorrow = date('Y-m-d', $day_after);
		if($tomorrow>=date('Y-m-d'))
		{
			return 'true';
		}
		$holiday = checkHoliday($tomorrow);
		$urgents = checkUrgentHoliday($tomorrow);
		$dayOfWeek = date('D',strtotime($tomorrow));
		//$isDayAfterOff=false;
		while($holiday || $urgents || $dayOfWeek=='Fri')
		{//the day before is also off, so check the day before it
			$thedate = explode('-', $tomorrow);
			$from_unix_time = mktime(0, 0, 0,$thedate[1],$thedate[2],$thedate[0]);
			$day_after = strtotime("tomorrow", $from_unix_time);
			$tomorrow = date('Y-m-d', $day_after);
			$holiday = checkHoliday($tomorrow);
			$urgents = checkUrgentHoliday($tomorrow);
			$dayOfWeek = date('D',strtotime($tomorrow));
			//$isDayAfterOff=true;
		}
		if(checkIfEmployeeIsPresent($rfid,$tomorrow))
		{//if uesr is present, then no issue to request leave
			$result = 'true';
		}
		else
		{//user is absent the day before off day
			$result = 'absent';
		}
		return $result;
}
function checkIfEmployeeIsPresent($rfid=0,$date)
{
	$rfid_lost = lost_rfidByRFID($rfid);
	if($rfid_lost)
	{
		$rfids = array($rfid);
		foreach($rfid_lost as $new_rfid)
		{
			$rfids[] =$new_rfid->rfid;
		}
		$total =DB::connection('hr')
			->table('attendance_images')
			->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total,SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",3),"/",-1) AS year'))//return the monthDay part of path
			->whereIn('RFID',$rfids)//check the rfid
			->where('status',0)//present
			->where('date',$date)
			->groupBy('groupByMe')
			->having('total', '>', 1)
			->get();
	}
	else
	{
		$total =DB::connection('hr')
			->table('attendance_images')
			->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total,SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",3),"/",-1) AS year'))//return the monthDay part of path
			->where('RFID',$rfid)//check the rfid
			->where('status',0)//present
			->where('date',$date)
			->groupBy('groupByMe')
			->having('total', '>', 1)
			->get();
	}
	if($total)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function att_img_source_date()
{
	$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
	if($user_dep_type==2)
	{//the returned value is not included in source
		return 139801;
	}
	else
	{
		return 139801;
	}
}

/** 
 * @Date: 2020-05-13 12:28:23 
 * @Desc: Attendance image source ip   
 */
function att_image_source_ip($clientIP='')
{
	
    // $image_source_ip = '';  
    // if(Session::has('image_server_ip'))
    // {
    //     $image_source_ip = Session::get('image_server_ip'); 
    // }
    // else
    // {
        $user_dept = getUserDepartment(Auth::user()->id);
        if($user_dept->department=='309' || $user_dept->general_department=='526' || $user_dept->department=='415')
        {
            $user_id = Auth::user()->id; 
            $user_id_array = array('502','533','506','524','532','530','529','217','522','527','531','525','528','841','883','884', '80','521','2160');
            if(in_array($user_id,$user_id_array)) //This employee can access the image server
            {
                $image_source_ip = 'http://mis_dv_server_img.aop.gov.af';
            }
            else
            {
                $image_source_ip = 'not_ping_able';
            }  
                
        }
        else
        {
            $image_source_ip = 'http://mis_dv_server_img.aop.gov.af';
        }
    //     //Put in the Sesion to not ping server in another request 
    //     Session::put('image_server_ip',$image_source_ip); 
    // }
    return $image_source_ip; 
}
//////////////////////////////////// audit
function getFindingSubs($id=0)
{
	$table = DB::connection('audit')
			->table('sub_findings')
			//->select('m.*')
			//->leftjoin('findings AS f','f.id','=','m.finding_id')
			->where('finding_id',$id)
			->get();
	return $table;
}
function findingHas_rec($id=0)
{
	$table = DB::connection('audit')
			->table('recommandations')
			//->select('m.*')
			//->leftjoin('findings AS f','f.id','=','m.finding_id')
			->where('report_title_id',$id)
			->get();
	return $table;
}
function getRecommandationDep($id=0,$type=0)
{
	if($type==0)
	{
		$table = DB::table('department')
				->select('name')
				//->leftjoin('findings AS f','f.id','=','m.finding_id')
				->where('id',$id)
				->first();
		return $table;
	}
	else
	{
		$table = DB::connection('audit')
				->table('ministries')
				->select('name_dr')
				//->leftjoin('findings AS f','f.id','=','m.finding_id')
				->where('id',$id)
				->first();
		return $table;
	}
}
//====================== evaluation
function getItems($id=0)
{
	$table = DB::connection('evaluation')
			->table('items')
			//->select('m.*')
			//->leftjoin('findings AS f','f.id','=','m.finding_id')
			->where('type_id',$id)
			->get();
	return $table;
}

 function getJanabiCard()
 {
    return $card = DB::table('position_short_title as ps')->orderby('order_card')->get(); 
 }

/** 
 * @Author: Jamal Yousufi  
 * @Date: 2019-10-06 10:59:45 
 * @Desc: Get data from tabase as array return diff array    
 */
 function diff_array($table,$column,$condition=false,$array=array())
 {
     $query = DB::table($table); 
     if($condition)
       $query->where($condition); 

     $result = $query->get(); 
     $note_in = ''; 
     $i=0; 
     if(count($result)>0 && count($array)>0)
     {
        foreach($result as $edu)
        {
           if(in_array($edu->id,$array))
           {
              
           }
           else
           {
              $note_in.=$edu->id.','; 
           }
        }

        return $note_in; 
     }
     else
     {
         return false; 
     }
     
 }

 /** 
  * @Author: Jamal Yousufi 
  * @Date: 2019-11-26 11:24:24 
  * @Desc: Check if the if the day is in shift  
  */ 
  function check_day_is_in_shfit($result,$date)
  {
	
    if($result)
	{   
       if($result->start_date <= $date && $date <= $result->end_date )
       {
           return true; 
       }
       else 
       {
           return false; 
       }
    }
    else 
     return false; 
  }


  /** 
   * @Author: Jamal Yousufi  
   * @Date: 2019-11-26 13:17:09 
   * @Desc: Caculdate date based on attendece department  
   */  
   function calculate_attendence_date($day,$month,$year,$type='start_date')
   {
     if($type=='start_date')
     {
        if($month==1)
        {
            $from = dateToMiladi($year-1,12,$day);
        }
        else
        {
            $from = dateToMiladi($year,$month-1,$day);//the start day of attendance month is 15 of prevouce month
        }
        return $from; 
      }
      else
      {
        $to = dateToMiladi($year,$month,$day);//the end day of attendance month is 15 of current month\
        if($to>date('Y-m-d'))
        {
            $to = date('Y-m-d');
        }

        return $to; 
      }
   }


    /** 
    * @Author: Jamall Yousufi  
    * @Date: 2019-12-31 16:43:05 
    * @Desc: Check the user role   
    */   
    function hasRule($section='',$role='')
    {
        if($section!='' && $role!='')
        {
            $default_module = getCurrentAppCode();//getCurrentAppCode();

            $roles = Session::get('user_roles');

            if(isset($roles[$default_module][$section][$role]))
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return false ;
        }
    }

    function is_in_quarin_date($date)
    {
        $table= DB::connection('hr')
            ->table('quarantine_shift_date')
            ->whereRaw('? between from_date and to_date', [$date])
            ->first();
        if($table)
        {
            return $table;
        }
        else
        {
            return false;
        }
    }

    function is_in_quarantine_shift($rfid,$type)
    {
        $table= DB::connection('hr')
            ->table('qaurantin_shift')
            ->where('rfid',$rfid)
            // ->where('shift',$type)
            ->get();
        if($table)
        {
            return $table;
        }
        else
        {
            return false;
        }
	}
	
	/** 
	 * @Author: Jamal Yousufi  
	 * @Date: 2020-11-18 10:09:41 
	 * @Desc: Get General Department based on access   
	 */
	function getUserGeneralDept() 
	{
	   
        $departments = DB::table('user_dept')->select('dept_id')->where('user_id',Auth::user()->id)->where('type','general_dept')->get(); 
		$ids = json_decode(json_encode($departments), true);
		if(count($ids)>0)
			return $ids; 
		else
		 	return 0; 
	   
	}

	/** 
	 * @Author: Jamal Yousufi  
	 * @Date: 2020-11-18 10:10:59 
	 * @Desc:  Get Sub department base on access 
	 */
	function getUserSubDept() 
	{
	   
		$departments = DB::table('user_dept')->select('dept_id')->where('user_id',Auth::user()->id)->where('type','sub_dept')->get(); 
		$ids = json_decode(json_encode($departments), true);
		if(count($ids) > 0)
			return $ids; 
		else 
			return 0; 
	   
	}

   function ddd($result)
   {
       echo "<pre>"; print_r($result); exit; 
   }

?>
