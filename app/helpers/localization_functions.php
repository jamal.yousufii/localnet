<?php 

//scan directory
function scanAppDirectory($appCode="")
{
	include_once '/var/www/libraries/gettext.php';
		
	//collect all application folder to an array
	$allFolders = array(
			"auth"       => array('application','auth','department','document','localization','role','section','user'),
			"docscom"    => "docscom",
			"finance"    => 'finance',
			"hr"         => 'hr',
			"task"       => 'workplan',
			"sched"      => 'scheduling'
			//"casemgmt"	 => 'casemgmt'
		);

	$selectedFolder = $appCode;//$allFolders[$appCode];

	$foundedFields = array();
	//create object from gettext scanner
	$gettext = new gettext();
    
    //check if multiple folder or not
    // if($appCode == 'auth')
    // {
    // 	for($i=0;$i<count($selectedFolder);$i++)
    // 	{
    // 		$lines = $gettext->scan_dir('/var/www/localnet/resources/views/'.$selectedFolder[$i].'/');
    		
    // 		if(count($lines)>0)
    // 		{
    // 			$foundedFields = array_merge($foundedFields,$lines);
    // 		}
    // 	}
    // }
    // else
    // {
    $foundedFields = $gettext->scan_dir('/var/www/html/localnet/resources/views/'.$appCode.'/');
    //}
    $fields = array_values($foundedFields);
    
    return $fields;
}

//calculate translated application
function getTranslatedProgress($appId=0)
{
    //get all fields from database lang_files table
    $fields = DB::table('lang_files')->where('module_id',$appId)->get();

    $progress = 0;

    $total_translated = 0;
    $totalFields = 0;
    if($fields)
    {
        foreach($fields AS $item)
        {
            
            $totalFields++;

            if($item->english != '')
            {
                $total_translated++;
            }
            if($item->dari != '')
            {
                $total_translated++;
            }
            if($item->pashto != '')
            {
                $total_translated++;
            }
        }
        //echo $total_translated." ->";
        $progress = round(($total_translated * 100)/($totalFields*3));
    }
    
    return $progress;

    //echo "total: "+($totalFields*3)." total_translated:".$total_translated;
}

//generate mo file
function generateMoFile($fields,$appCode)
{
//dd($appCode);
    //include_once '/var/www/libraries/php-mo.php';

    //array for all fields
    $labels = array();

    //loop the object for getting all translated labels
    foreach($fields AS $row)
    {
        if($row->english != '')
        {
            $labels['en'][$row->key] = $row->english;
            
        }
        if($row->dari != '')
        {
            $labels['da'][$row->key] = $row->dari;
        }
        if($row->pashto != '')
        {
            $labels['pa'][$row->key] = $row->pashto;
        }
    }

	//dd($labels);
    //generate new po file for english
    $fh = fopen("../resources/lang/i18n/en_US/LC_MESSAGES/".$appCode.".po", 'w');
    if(isset($labels['en']))
    {
	    foreach ($labels['en'] as $key => $value) {
	        $key = addslashes($key);
	        $value = addslashes($value);
	        fwrite($fh, "msgid \"$key\"\n");
	        fwrite($fh, "msgstr \"$value\"\n");
	    }
    }

    fclose($fh);
    //end of english po file

    //generate po file for Dari
    $fh = fopen("../resources/lang/i18n/fa_IR/LC_MESSAGES/".$appCode.".po", 'w');
    if(isset($labels['da']))
    {
	    foreach ($labels['da'] as $key => $value) {
	        $key = addslashes($key);
	        $value = addslashes($value);
	        fwrite($fh, "msgid \"$key\"\n");
	        fwrite($fh, "msgstr \"$value\"\n");
	    }
    }
    fclose($fh);
    //end of english po file

    //generate po file for Pashto
    $fh = fopen("../resources/lang/i18n/ps_AF/LC_MESSAGES/".$appCode.".po", 'w');
    if(isset($labels['pa']))
    {
	    foreach ($labels['pa'] as $key => $value) {
	        $key = addslashes($key);
	        $value = addslashes($value);
	        fwrite($fh, "msgid \"$key\"\n");
	        fwrite($fh, "msgstr \"$value\"\n");
	    }
	}
    fclose($fh);
    //end of english po file

    //shell commands for generating mo files
    $cmd_en = "msgfmt -o /var/www/html/localnet/resources/lang/i18n/en_US/LC_MESSAGES/".$appCode.".mo /var/www/html/localnet/resources/lang/i18n/en_US/LC_MESSAGES/".$appCode.".po";
    $cmd_da = "msgfmt -o /var/www/html/localnet/resources/lang/i18n/fa_IR/LC_MESSAGES/".$appCode.".mo /var/www/html/localnet/resources/lang/i18n/fa_IR/LC_MESSAGES/".$appCode.".po";
    $cmd_pa = "msgfmt -o /var/www/html/localnet/resources/lang/i18n/ps_AF/LC_MESSAGES/".$appCode.".mo /var/www/html/localnet/resources/lang/i18n/ps_AF/LC_MESSAGES/".$appCode.".po";
    
    //executing shell command for generating mo files
    shell_exec($cmd_en);
    shell_exec($cmd_da);
    shell_exec($cmd_pa);

    //remove po files after generating mo files
    unlink("/var/www/html/localnet/resources/lang/i18n/en_US/LC_MESSAGES/".$appCode.".po");
    unlink("/var/www/html/localnet/resources/lang/i18n/fa_IR/LC_MESSAGES/".$appCode.".po");
    unlink("/var/www/html/localnet/resources/lang/i18n/ps_AF/LC_MESSAGES/".$appCode.".po");

    
}
?>