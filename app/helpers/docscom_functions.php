<?php 

function getAllDocsComDepartments($depId=0)
{

	$object = DB::table('department')
				->where('id','<=',80)
				->where('unactive',0)
				->orWhereIn('id',array(89,175,176))
				->get();
	return $object;
}
function getDocsComDepartmentWhereIn()
{
	if(getCurrentAppCode() == 'hr')
	{
		$ids = array(23,25,27,39,44,48,52,55,58,66,80,81,82,83,84,85,86,87,90,91,92,93);
	}
	else
	{
		$ids = array(23,25,27,39,44,48,52,55,58,66);
	}
	
	$object = DB::table('department')->whereIn('id',$ids)->where('unactive',0)->get();
	return $object;
}

//get document durration
function getDocumentDurration($doc_id=0,$table_name="docs",$print=false)
{
	$result = DB::connection('docscom')
				->table('log')
				->where('doc_id',$doc_id)
				->where('table_name',$table_name)
				//->orderBy('check_in')
				->get();

	$days = "0";

	if($result)
	{
		$d1 = "";
		$d2 = "";

		$counter = 1;

		//foreach the records
		foreach($result AS $item)
		{
			//calculate the dates
			if($counter == 1)
			{
				$d1 = substr($item->check_in, 0,-9);
			}

			$d2 = substr($item->check_out, 0,-9);
			
			if($d2 == '0000-00-00')
			{
				$d2 = date('Y-m-d');
			}

			$counter++;
		}

		$date1 = new DateTime($d1);
		$date2 = new DateTime($d2);

		$days = $date2->diff($date1)->format("%a");
	}

	if($print)
	{
		return $days." Days";
	}

	return '<span class="label label-success">'.$days.' Days</span>';
	
	
}
/**
*get the document id and return the status of this doc
**/
function isDocumentApproved($doc_id=0,$table_name='docs')
{
	$result = DB::connection('docscom')
				->table($table_name)
				->where('id',$doc_id)
				->whereIn('status',array(1,4))
				->get();
	if(count($result)>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function isDocSaved($doc_id=0,$table_name='docs')
{
	$result = DB::connection('docscom')
				->table($table_name)
				->where('id',$doc_id)
				->where('status',3)
				->get();
	if(count($result)>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function isDocumentRejected($doc_id=0,$table_name='docs')
{
	$result = DB::connection('docscom')
				->table('comments')
				->where('doc_id',$doc_id)
				->where('table_name',$table_name)
				->where('type',2)
				->get();
	if(count($result)>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function amIRejected($doc_id=0,$table_name='docs')
{
	$result = DB::connection('docscom')
				->table('comments')
				->where('doc_id',$doc_id)
				->where('type',2)
				->where('table_name',$table_name)
				->where('user_id',Auth::user()->id)
				->get();
	if(count($result)>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function amIReturned($doc_id=0,$table_name='docs')
{
	$result = DB::connection('docscom')
				->table('comments')
				->where('doc_id',$doc_id)
				->where('type',1)
				->where('table_name',$table_name)
				->where('user_id',Auth::user()->id)
				->get();
	if(count($result)>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function amIApproved($doc_id=0,$table_name='docs')
{
	$result = DB::connection('docscom')
				->table('log')
				->where('doc_id',$doc_id)
				->where('status',3)
				->where('table_name',$table_name)
				->where('checked_in_by',Auth::user()->id)
				->where('checked_out_by',0)
				->get();
	
	if(count($result)>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function getLastDocId()
{
	return DB::connection('docscom')->table('attachments')->orderBy('id', 'desc')->pluck('id');
}

function getDocscomFileName($id=0,$table_name='docs')
{
	return DB::connection('docscom')
				->table('attachments')
				->where('id',$id)
				->where('table_name',$table_name)
				->pluck('file_name');
}

//get reject notification
function getDocRejectNotify($getCount=false,$table_name="docs")
{
	$object = DB::connection('docscom')
				->table('comments AS t1')
				->where('notify_to',Auth::user()->id)
				->where('t1.type',2)
				->where('t1.status',0)
				->get();
	if($getCount)
	{
		return count($object);
	}
	else
	{
		$li = "";

		foreach($object AS $item)
		{
			
			$notification_type = '<span style="font-weight:normal;color:red;">Rejected</span>';
			$url = URL::route('getDocDetails',array($table_name,$item->doc_id,$item->id."#comments_div"));

	        //$li .= '<li class="item">';
	        $li .= '<a class="list-group-item" role="menuitem" href="'.$url.'"><div class="media"><div class="media-left padding-right-10">';
	       $image = getProfilePicture($item->user_id);
	       $li .= HTML::image('/img/'.$image,'',array('class' => 'notification-logo'));
	       $li .= '</div><div class="media-body">';
	        
		    $li .= '<h6 class="media-heading">
		                '.getUserFullName($item->user_id).' - '.$notification_type.'
		            </h6>';

	       $li .=  '<span class="content-text">Document rejected, Click here to see the details.</span>';
	       $li .= '<time class="media-meta">&nbsp;<i class="fa fa-clock-o"></i>&nbsp;'.$item->created_at.'</time>';
	       $li .= '</div></div></a>';
			//$li .= '</li>';
		}

		//return elements
		return $li;
	}
}

// ----- docscom roles --------------------------------------//
function canComment($section='')
{
	$default_module = getCurrentAppCode();
	
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['comment']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
function canChangeDepartment($section='')
{
	$default_module = getCurrentAppCode();
	
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['change_department']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
function allComments($section='')
{
	$default_module = getCurrentAppCode();
	
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['all_comments']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
function canApprove($section='')
{
	$default_module = getCurrentAppCode();
	
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['approve']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
function canReject($section='')
{
	$default_module = getCurrentAppCode();
	
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['reject']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
function canCheckOut($section='')
{
	$default_module = getCurrentAppCode();
	
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['check_out']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
function canSearch($section='')
{
	$default_module = getCurrentAppCode();
	
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['search']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
function isConfidential($section='')
{
	$default_module = getCurrentAppCode();
	
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['confidential']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
function isNonConfidential($section='')
{
	$default_module = getCurrentAppCode();
	
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['non_confidential']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
function canFinalApprove($section='')
{
	$default_module = getCurrentAppCode();
	
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['final_approve']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
function canReturn($section='')
{
	$default_module = getCurrentAppCode();
	
	$roles = Session::get('user_roles');

	if(isset($roles[$default_module][$section]['return']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

}
//----------------------------------------------------------//


?>