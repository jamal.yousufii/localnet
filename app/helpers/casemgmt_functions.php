<?php 
use App\library\Dateconverter;
use App\library\jdatetime;

//get manager notifications.
function getCaseManagerNotification($getCount=false)
{
	if(isManager() || isAdmin())
	{
		$object = DB::connection("casemgmt")->table("notify_to_manager")->select("*")->where('is_viewed', 0)->groupBy('case_id')->get();
		//dd($object);
		if($getCount)
		{
			return count($object);
		}
		else
		{
			$li = "";
	
			foreach($object AS $item)
			{
				
				$url = URL::route("viewCase",array($item->case_id,"visited"));
	
		        //$li .= '<li class="item">';
		        $li .= '<a class="list-group-item" role="menuitem" href="'.$url.'"><div class="media"><div class="media-left padding-right-10">';
		       	$li .= '</div><div class="media-body">';
		       	
	       		if($item->table_name == "cases")
		       	{
		       		$li .= '<h6 class="media-heading">Case Notification</h6>';
		       		$li .=  '<span class="content-text">Task Created, Click here to see the details.</span>';
		       	}
		       	else
		       	{
		       		$li .= '<h6 class="media-heading">Progress Notification</h6>';
		       		$li .=  '<span class="content-text">Task progress finished, Click here to see the details.</span>';
		       	}
			    
		       	$li .= '<time class="media-meta">&nbsp;<i class="fa fa-clock-o"></i>&nbsp;'.$item->created_at.'</time>';
		       	$li .= '</div></div></a>';
				//$li .= '</li>';
			}
	
			//return elements
			return $li;
		}
	}
}
//get case management departments.
function getCaseDepartments($selected_item=0)
{
	$object = DB::table("department")->select('id','name','name_en')->whereIn('id',getAllSubDepartmentsWithParent(161));
	$items = "";
	$items .= "<option value=''>Select</option>";
	foreach($object->get() AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name.'</option>';
	}
	
	return $items;
}
//get case management persons.
function getCasePersons($selected_item=0)
{
	$object = DB::table("users")->select('id','first_name','username')->whereIn('department_id',getAllSubDepartmentsWithParent(161));
	$items = "";
	//$items .= "<option value=''>Select</option>";
	foreach($object->get() AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->username.'</option>';
	}
	
	return $items;
}
// get Task Departments
function getTaskDepartments($selected_item=0)
{

	$rows = DB::connection('casemgmt')->table('department')->select('id','name')->get();

	$items = "";
	$items .= "<option value=''>Select</option>";
	foreach($rows AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name.'</option>';
	}
	
	return $items;
}

// get Names
function getNames($selected_item=0)
{

	$rows = DB::connection('casemgmt')->table('request_department')->select('id','name')->get();

	$items = "";
	$items .= "<option value=''>Select</option>";
	foreach($rows AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name.'</option>';
	}
	
	return $items;
}

//get the categories
function getCategories($selected_item=0)
{

	$rows = DB::connection('casemgmt')->table('category')->select('id','name')->get();

	$items = "";
	$items .= "<option value=''>Select</option>";
	foreach($rows AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name.'</option>';
	}
	
	return $items;
}

//get the categories
function getTaskRequestLevel($selected_item=0)
{

	$rows = DB::connection('casemgmt')->table('task_request_level')->select('id','name')->get();

	$items = "";
	$items .= "<option value=''>Select</option>";
	foreach($rows AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name.'</option>';
	}
	
	return $items;
}

// get Agencies
function getCaseAgencies($selected_item=0, $lang='en')
{

	$items = "";
	if($lang=="dr" || $lang == "pa"){
		$rows = DB::connection('casemgmt')->table('agencies')->select('id','name_dr AS name')->get();
		$items .= "<option value=''>انتخاب کنید</option>";
	}
	else
	{
		$rows = DB::connection('casemgmt')->table('agencies')->select('id','name_en AS name')->get();
		$items .= "<option value=''>Select</option>";
	}
	foreach($rows AS $item)
	{
		$items .= '<option '.($item->id == $selected_item ? "selected":"").' value="'.$item->id.'">'.$item->name.'</option>';
	}
	
	return $items;
}
// get the agencies for executive order form.
function getExecOrderAgencies($selected_item=0, $lang='en')
{

	$items = "";
	if($lang=="dr" || $lang == "pa"){
		$rows = DB::connection('casemgmt')->table('agencies')->select('id','name_dr AS name')->get();
		$items .= "<option value=''>انتخاب اورگان</option>";
	}
	else
	{
		$rows = DB::connection('casemgmt')->table('agencies')->select('id','name_en AS name')->get();
		$items .= "<option value=''>Select Agency</option>";
	}
	foreach($rows AS $item)
	{
		$items .= '<option '.($item->name == $selected_item ? "selected":"").' value="'.$item->name.'">'.$item->name.'</option>';
	}
	
	return $items;
}
//check the agencies duplicate.
function checkAgencyDuplicate($agency_id=0)
{
	$check = "";
	if($agency_id != 0){
		$check .= DB::connection('casemgmt')->table('agencies')->select('name_en')->where('id', $agency_id)->exists();
	}
	return $check;
}
//check if the category already exists if not then insert it into category table.
function getCheckCategory($category_name)
{
	$check = DB::connection('casemgmt')->table('category')->select('id')->where('name', $category_name)->count();
	if($check == 0)
	{
		DB::connection('casemgmt')->table('category')->insert(array('name' => $category_name));
	}
}
// check if the particular item of the field of the table is exist.
function checkAutocompleteExistence($table, $field, $fieldValue)
{
	$object = DB::connection('casemgmt')->table($table)->where($field, $fieldValue)->exists();
	return $object;
}
//get last inserted case id.
function getCaseLastRecordId()
{
	return DB::connection('casemgmt')->table('cases')->select('id')->orderBy('id','desc')->limit(1);
}
function case_dmy_format($date)
{
	$gregorian_format = explode("/", $date);
	$jalali_format = "";
	
	if($date != '' && !empty($gregorian_format))
	{
	   $g_y = $gregorian_format[0];
	   $g_m = $gregorian_format[1];
	   $g_d = $gregorian_format[2];
	   $jalali_format = $g_d."-".$g_m."-".$g_y;
	}

   return $jalali_format;
}

// change the date format to Gregorian.
function case_ymd_format($date)
{
	$jalali_format = explode("/", $date);
	$gregorian_format = "";
	if($date != '' && !empty($jalali_format))
	{
		$j_y = $jalali_format[2];
		$j_m = $jalali_format[0];
		$j_d = $jalali_format[1];
		
		$gregorian_format = $j_y."-".$j_m."-".$j_d;
	}

	
	return $gregorian_format;
}

//added by gulmuhammad akbari
function isCaseAdmin()
{
	$default_module = getCurrentAppCode();//getCurrentAppCode();

	$roles = Session::get('user_roles');

	if(isset($roles[$default_module]['auth_user']['auth_case']))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

?>