<?php 
use App\library\Dateconverter;
use App\library\jdatetime;

	// get the Document Type dropdown.
	function getDocType($selected_item=0)
	{
		$doc_type = DB::connection('docs_archive')->table('document_type')->select('id','name')->groupBy('name')->get();
		if($doc_type)
		{
			$options = "<option value=''>نوع سند را انتخاب کنید</option>";
			foreach ($doc_type as $items) {
				$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name."</option>";
			}
			return $options;
		}
	}
	// get the Document Category dropdown.
	function getDocCategory($selected_item=0)
	{
		$doc_category = DB::connection('docs_archive')->table('document_category')->select('id','name')->groupBy('name')->get();
		if($doc_category)
		{
			$options = "<option value=''>Select Document Category</option>";
			foreach ($doc_category as $items) {
				$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name."</option>";
			}
			return $options;
		}
	}

	function getSender($selected_item="")
	{
		$sources = DB::connection('docs_archive')->table('sources')->select('id','sender')->groupBy('sender')->get();
		if($sources)
		{
			$options = "<option value=''>مرجع ارسال کننده را انتخاب کنید</option>";
			foreach ($sources as $items) {
				if($items->sender != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->sender."</option>";	
				}
			}
			return $options;
		}
	}

	function getExecutions($selected_item="")
	{
		$executions = DB::connection('docs_archive')->table('execution')->select('id','name')->groupBy('name')->get();
		if($executions)
		{
			$options = "<option value=''>نوع اجراات را انتخاب کنید</option>";
			foreach ($executions as $items) {
				if($items->name != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name."</option>";	
				}
			}
			return $options;
		}
	}

	function getRelatedDeputy($selected_item="")
	{
		$relatedDeputy = DB::connection('docs_archive')->table('sources')->select('id','internal_sender_related_deputy')->groupBy('internal_sender_related_deputy')->get();
		if($relatedDeputy)
		{
			$options = "<option value=''>معاونیت مربوطه را انتخاب کنید</option>";
			foreach ($relatedDeputy as $items) {
				if($items->internal_sender_related_deputy != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->internal_sender_related_deputy."</option>";	
				}
			}
			return $options;
		}
	}

	function getRelatedDirectorate($selected_item="")
	{
		$relatedDirectorate = DB::connection('docs_archive')->table('sources')->select('id','internal_sender_related_directorate')->groupBy('internal_sender_related_directorate')->get();
		if($relatedDirectorate)
		{
			$options = "<option value=''>ریاست مربوطه را انتخاب کنید</option>";
			foreach ($relatedDirectorate as $items) {
				if($items->internal_sender_related_directorate != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->internal_sender_related_directorate."</option>";	
				}
			}
			return $options;
		}
	}

	function getDocsArchiveFileName($doc_id)
	{
		$file_name = DB::connection('docs_archive')->table('uploads')->where('doc_id', $doc_id)->get();
		if($file_name){ return $file_name;} else{ return false;}
	}


?>
