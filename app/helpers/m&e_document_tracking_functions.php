<?php
use App\library\Dateconverter;
use App\library\jdatetime;


	// check if the logged in user is the Director of M&E Directorate.
	function isMEDirector($section='', $role='')
	{
		// checking the user role nd entry previlliges.

		$default_module = getCurrentAppCode();
		$roles = Session::get('user_roles');

		if(isset($roles[$default_module][$section][$role]))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

	}
	// check if the logged in user is the Executive Manager of M&E Directorate.
	function isMEExecutiveManager($section='', $role='')
	{
		// checking the user role nd entry previlliges.

		$default_module = getCurrentAppCode();

		$roles = Session::get('user_roles');
		if(isset($roles[$default_module][$section][$role]))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	// check if the logged in user is the one of the experts in M&E Directorate.
	function isMEExpert($section='', $role='')
	{
		// checking the user role nd entry previlliges.

		$default_module = getCurrentAppCode();

		$roles = Session::get('user_roles');
		if(isset($roles[$default_module][$section][$role]))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function getLastRecordId($table)
	{
		$last_record_id = DB::connection('document_tracking')->table($table)->orderBy('id', 'DESC')->pluck('id');
		if(!empty($last_record_id)){return $last_record_id;}else{return 0;}
	}
	// get the record name based on sent id as parameter.
	function getRecordFieldBasedOnID($table,$field,$id=0)
	{
		$record_name = DB::connection('document_tracking')->table($table)->where('id', $id)->pluck($field);
		if(!empty($record_name)){return $record_name;}else{return "";}
	}
	// get the document operation type whether it is saved or issued based on incoming document id.
	function getOperation($incoming_doc_id)
	{
		$record = DB::connection('document_tracking')->table('issued_docs')->where('incoming_doc_id', $incoming_doc_id)->where('deleted', 0)->pluck('operations');
		if(!empty($record)){return $record;}else{return "";}
	}
	// get the copy of references based on sent id as parameter.
	function getCopyToReferences($issued_doc_id=0)
	{
		$copy_to_references = DB::connection('document_tracking')->table('copy_to_references')->where('issued_doc_id', $issued_doc_id)->get();
		if(!empty($copy_to_references)){return $copy_to_references;}else{return "";}
	}

	function getOrganizations($selected_item=0)
	{
		$sources = DB::connection('document_tracking')->table('ministries')->select('id','name_dr')->groupBy('name_dr')->get();
		if($sources)
		{
			$options = "";
			foreach ($sources as $items) {
				if($items->name_dr != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name_dr."</option>";
				}
			}
			return $options;
		}
	}

	function getDocumentFileName($doc_id)
	{
		$file_name = DB::connection('document_tracking')->table('uploads')->where('issued_doc_id', $doc_id)->get();
		if($file_name){ return $file_name;} else{ return false;}
	}

	//get the list of pending incoming numbers of incoming documents.
	// function getPendingIncomingNumbers($selected_item=0)
	// {
	// 	$pending_incoming_nums = DB::connection('document_tracking')->table('incoming_docs')->select('id','incoming_number')->where('doc_status', 0)->where('incoming_number','!=',0)->where('deleted',0)->get();
	// 	if($pending_incoming_nums)
	// 	{
	// 		$options = "";
	// 		foreach ($pending_incoming_nums as $items) {
	// 			if($items->incoming_number != "")
	// 			{
	// 				$options .= "<option ".($items->incoming_number == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->incoming_number."</option>";
	// 			}
	// 		}
	// 		return $options;
	// 	}
	// }
	//get the list of all incoming numbers of incoming documents.
	function getAllIncomingNumber($selected_item=0,$year="")
	{
		$inc_nums = DB::connection('document_tracking')->table('incoming_docs')->select('id','incoming_number')->where('incoming_number','!=',0)->where('deleted',0);
		if($year != "")
		{
			$inc_nums->where('incoming_date', '>=', getCurrentJalaliYearStartDate($year))->where('incoming_date','<=', getCurrentJalaliYearEndDate($year));
		}
		else
		{
			$inc_nums->where('incoming_date', '>=', getCurrentJalaliYearStartDate())->where('incoming_date','<=', getCurrentJalaliYearEndDate());
		}

		$incoming_numbers= $inc_nums->get();
		if($incoming_numbers)
		{
			$options = "";
			foreach ($incoming_numbers as $items) {
				if($items->incoming_number != "")
				{
					$options .= "<option ".($items->incoming_number == $selected_item ? 'selected':'')." value='".$items->incoming_number."'>".$items->incoming_number."</option>";
				}
			}
			return $options;
		}
		else return $options = "";
	}
	//get the list of Experts in M&E directorate.
	function getMEExperts($selected_item=0,$department_id=0)
	{
        $employees_id = ''; 
        if($department_id==423)
        {
            $employees_id = array('1825','0473','4472','1797','6151','3909','3913','4889','6900','5641','6912','6869','5030','7040','6905','6865','7019','5968','6924','6925','6872','6147','6911','6881','6903','6926','7057','4021','7043','4801','1791','1874','2962','6901','1793','4707','7006','7024','4730','1897','3917','4728','5141','5781','4732','7045','7044','6179','6731','4734','5769','3483','1871','3656','5031','0170','1890','5744','1755','6868','5842','6616','3000','1857','4335','5754','6148');
        }
        elseif($department_id==504)
        {
            
            $employees_id = array('4369','4128','3005','4372','7366','4371','4370','4378','7367','4360','4359','4131','4363','4332','7372','4368','7373','4366','4374','7364','4362','7371','4373','4910','4376','6601','6600','4888','4285','4364','3180');
        }
		$me_experts = DB::connection('hr')->table('employees')->select('id',DB::raw("concat(`name_dr`,' ',`last_name`) as emp_name"))->whereIn('id', $employees_id)->get();
        
        if($me_experts)
		{
			$options = "";
			foreach ($me_experts as $items) {
				if($items->emp_name != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->emp_name."</option>";
				}
			}
			return $options;
		}
	}

	// get document type.
	function getMEDocumentType($selected_item=0)
	{
		$doc_type = DB::connection('document_tracking')->table('document_type')->select('id','name')->orderBy('id','asc')->groupBy('name')->get();
		if($doc_type)
		{
			$options = "";
			foreach ($doc_type as $items) {
				if($items->name != "")
				{
					$options .= "<option ".($items->id == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->name."</option>";
				}
			}
			return $options;
		}
	}
	// get document type name based on sent parameter.
	function getMEDocumentTypeName($document_type_id)
	{
		$doc_type_name = DB::connection('document_tracking')->table('document_type')->where('id', $document_type_id)->pluck('name');
		if($doc_type_name != ""){ return $doc_type_name;}
		else{ return "";}
	}
	// get the assignee based on incoming document id.
	function getAssignee($incoming_doc_id)
	{
		$assignee = DB::connection('document_tracking')->table('assignees')->where('incoming_doc_id', $incoming_doc_id)->pluck('assignee_id');
		if(!empty($assignee)){ return $assignee;}
		else{ return "";}
	}
	// get the assignee based on issued document id.
	function getIssuedDocAssignee($issued_doc_id)
	{
		$assignee = DB::connection('document_tracking')->table('assignees')->where('issued_doc_id', $issued_doc_id)->pluck('assignee_id');
		if($assignee){ return $assignee;}
		else{ return "";}
	}
	// get the assignee description based on the employee id and incoming document id.
	function getAssigneeDescription($incoming_doc_id)
	{
		$assignee_description = DB::connection('document_tracking')->table('assignees')->where('incoming_doc_id', $incoming_doc_id)->pluck('assignee_description');
		if(!empty($assignee_description)){ return $assignee_description;}
		else{ return "";}
	}
	// get the assignee description based on the employee id and incoming document id.
	function getAssigneeName($assignee_id)
	{
		$assignee_name = DB::connection('hr')->table('employees')->select(DB::raw("CONCAT(`name_dr`,' ',`last_name`) as assignee_name"))->where('id', $assignee_id)->first();
		if($assignee_name){ return $assignee_name->assignee_name;}
		else{ return "";}
	}
	// get the total of executed and under process documents.
	function getTotalOfDocsBasedOnParameters($doc_status="",$year="")
	{
		$record = DB::connection('document_tracking')->table("incoming_docs as ind");
		if($doc_status != "")
		{
			$record->leftjoin("issued_docs as issd","ind.id","=","issd.incoming_doc_id")->where("issd.incoming_doc_id",'!=',0)->where("issd.incoming_number",'!=',0)->where("issd.deleted",0);
		}

		if($year != "")
		{
			$record->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate($year))->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate($year));
		}
		else
		{
			$record->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate())->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate());
		}
		$record->where("ind.doc_status",$doc_status)->where("ind.deleted",0);

		if(!isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs') && !isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$record->rightjoin('assignees as ass','ass.incoming_doc_id','=', "ind.id");
			$record->leftjoin('hr.employees as emp','emp.id','=','ass.assignee_id');
			$record->where('ass.assignee_id', Auth::user()->employee_id);
        }
        // Filter record based on User Department 
        $record->where('ind.department_id',getUserDepartment(Auth::user()->id)->department); 
		$record->groupBy('ind.id');
		$total = count($record->get());
		if($total > 0){ return $total;}
		else{ return 0;}
	}

	function getTotalOfIncomingDocs($year="")
	{
		$record = DB::connection('document_tracking')->table("incoming_docs as incd");

		if($year != "")
		{
			$record->where('incoming_date','>=',getCurrentJalaliYearStartDate($year))->where('incoming_date','<=',getCurrentJalaliYearEndDate($year));
		}
		else
		{
			$record->where('incoming_date','>=',getCurrentJalaliYearStartDate())->where('incoming_date','<=',getCurrentJalaliYearEndDate());
		}
		$record->where('deleted',0);
		if(!isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs') && !isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$record->rightjoin('assignees as ass','ass.incoming_doc_id','=', "incd.id");
			$record->leftjoin('hr.employees as emp','emp.id','=','ass.assignee_id');
			$record->where('ass.assignee_id', Auth::user()->employee_id);
        }
        // Filter Record based on User Department 
        $record->where('incd.department_id',getUserDepartment(Auth::user()->id)->department); 

		if($record->count() > 0){ return $record->count();}
		else{ return 0;}
	}

	function getMESavedDocumentStats($year="")
	{
		$record = DB::connection('document_tracking')->table('issued_docs as issd')->leftjoin("incoming_docs as ind","ind.id","=","issd.incoming_doc_id");
		if($year != "")
		{
			$record->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate($year))->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate($year));
		}
		else
		{
			$record->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate())->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate());
		}
		if(!isMEDirector('document_tracking_issued_docs', 'm&e_director_issued_docs') && !isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$record->rightjoin('assignees as ass','ass.incoming_doc_id','=','issd.incoming_doc_id');
			$record->leftjoin('hr.employees as emp','emp.id','=','ass.assignee_id');
			$record->where('ass.assignee_id', Auth::user()->employee_id);
		}
		$record->where('issd.operations', 1)->where('issd.incoming_doc_id','!=',0)->where('issd.incoming_number','!=',0)->where('ind.deleted',0)->where('issd.deleted',0)->groupBy('ind.id');
        // Filter record based on user department ID 
        $record->where('issd.department_id',getUserDepartment(Auth::user()->id)->department);
        $total = count($record->get());
		if($total > 0){ return $total;}
		else{ return 0;}
	}
	// get issued docs with or without incoming documents based on parameters.
	function getMEIssuedDocumentStats($issue_type="",$year="")
	{
		if($issue_type == 1)
		{
			//issued documents with incoming document.
			$record = DB::connection('document_tracking')->table('issued_docs as issd')->leftjoin("incoming_docs as ind","ind.id","=","issd.incoming_doc_id");
			$record->where('issd.operations', 2)->where('issd.incoming_doc_id','!=',0)->where('issd.incoming_number','!=',0);
			if($year != "")
			{
				$record->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate($year))->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate($year));
			}
			else
			{
				$record->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate())->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate());
			}
			if(!isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs') && !isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
			{
				// list the incoming documents based on specific M&E experts.
				$record->rightjoin('assignees as ass','ass.incoming_doc_id','=','issd.incoming_doc_id');
				$record->leftjoin('hr.employees as emp','emp.id','=','ass.assignee_id');
				$record->where('ass.assignee_id', Auth::user()->employee_id);
			}
			$record->where('ind.deleted',0)->where('issd.deleted',0);
			$record->groupBy('ind.id');
		}
		else
		{
			//issued documents without incoming document.
			$record = DB::connection('document_tracking')->table('issued_docs as issd')->where('issd.operations', 2)->where('issd.incoming_number',0)->where('issd.incoming_doc_id',0);
			if($year != "")
			{
				$record->where('issd.issuing_date','>=',getCurrentJalaliYearStartDate($year))->where('issd.issuing_date','<=',getCurrentJalaliYearEndDate($year));
			}
			else
			{
				$record->where('issd.issuing_date','>=',getCurrentJalaliYearStartDate())->where('issd.issuing_date','<=',getCurrentJalaliYearEndDate());
			}
			$record->where('issd.deleted',0);

			if(!isMEDirector('document_tracking_issued_docs', 'm&e_director_issued_docs') && !isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
			{
				// list the incoming documents based on specific M&E experts.
				$record->rightjoin('assignees as ass1','ass1.issued_doc_id','=','issd.id');
				$record->leftjoin('hr.employees as issd_emp','issd_emp.id','=','ass1.assignee_id');
				$record->where('ass1.assignee_id', Auth::user()->employee_id);
			}
        }
        // Filter record based on user department 
        $record->where('issd.department_id',getUserDepartment(Auth::user()->id)->department); 
		//return $record->tosql();
		$total = count($record->get());
		if($total > 0){ return $total;}
		else{ return 0;}

	}

	// get the start date of the jalali year based on parameter.
	function getCurrentJalaliYearStartDate($j_year="")
	{
		if($j_year == "")
		{
			// get current jalalai date's year
			$j_y = explode("-", convertToJalali(date('Y-m-d')));
			$j_y = $j_y[0];
		}
		else
		{
			$j_y = $j_year;
		}

		$start_j_year = convertToGregorian($j_y."-01-01");

		return $start_j_year;
	}
	// get the end date of the jalali year based on parameter.
	function getCurrentJalaliYearEndDate($j_year="")
	{
		if($j_year == "")
		{
			// get current jalalai date's year
			$j_y = explode("-", convertToJalali(date('Y-m-d')));
			$j_y = $j_y[0];
		}
		else
		{
			$j_y = $j_year;
		}

		if(isItLeapYear($j_y))
		{
			$end_j_year = convertToGregorian($j_y."-12-30");
		}
		else
		{
			$end_j_year = convertToGregorian($j_y."-12-29");
		}
		return $end_j_year;
	}
	function getJalaliYears()
	{
		$max_year = 50;
		$options = "<option value=''>...</option>";
		$i = 1;
		$s_year = "1396";
		while($i <= $max_year)
		{
			$options .= "<option value='".$s_year."'>".$s_year."</option>";
			$s_year += 1;
			$i += 1;
		}
		return $options;
	}
?>
