<?php

namespace App\models\procurement;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Session;

class procurementModel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public static $myDb = "procurement";

	//================================================== Goods Purchase Model Functions =====================================
	
	// get All Purchases.If the purchasing team has been chosed from dropdown get the purchase details based on that.
	public static function getPurchaseData($purchasing_team)
	{
		if($purchasing_team != "")
		{
			$table = DB::connection(self::$myDb)
					->table('goods_purchase as p');
					$table->select(
							'p.*',
							'dep.name AS executive_department'
							);
			$table->leftjoin("auth.department AS dep","dep.id","=","p.end_user");
			$table->where('purchasing_team', $purchasing_team);
			$object = $table->orderBy('p.id','asc');
			return $object->get();
		}
		else
		{
			$table = DB::connection(self::$myDb)
					->table('goods_purchase as p');
					$table->select(
							'p.*',
							'dep.name AS executive_department'
							);
			$table->leftjoin("auth.department AS dep","dep.id","=","p.end_user");
			$object = $table->orderBy('p.id','desc');
			return $object->get();
		}
		
	}
	// insert the Purchase form details in its table.
	public static function addPurchaseDetails($formData)
	{
		$object = DB::connection(self::$myDb)->table('goods_purchase')->insert($formData);
		if($object)
		{	
			return true;
		}
		else
		{
			return false;
		}
	}
	// get Purchase based on id.
	public static function getSpecificPurchaseData($id)
	{

		$table = DB::connection(self::$myDb)
					->table('goods_purchase as p')->where('p.id', $id);
					$table->select('p.*','dep.name as executive_department', 
						DB::raw('CONCAT(DATEDIFF(`date_of_submission_to_the_finance_department`, `date_of_submission_to_stock`)) AS delay')
					);
		$table->leftjoin("auth.department AS dep","dep.id","=","p.end_user");
		$object = $table;
		return $object->get();
	}
	// update the Purchase form details;
	public static function editPurchaseDetails($id, $formData)
	{
		$object = DB::connection(self::$myDb)->table('goods_purchase')->where('id', $id)->update($formData);
		if($object)
		{	
			return true;
		}
		else
		{
			return false;
		}
	}
	//deleting the purchase data based on id.
	public static function deletePurchaseData($id)
	{	
		//get the purchase based on id and then insert the data into log table.
		$purchaseData = DB::connection(self::$myDb)->table('goods_purchase')->select('*')->where('id', $id)->get();
		foreach($purchaseData as $item)
		{
			$date_of_preparation = $item->date_of_preparation;
			$end_user = $item->end_user;
			$goods_detail = $item->goods_detail;
			$request_no = $item->request_no;
			$date_of_arrival = $item->date_of_arrival;
			$date_of_submission_to_the_purchasing_committee = $item->date_of_submission_to_the_purchasing_committee;
			//$estimation_cost = $item->estimation_cost;
			$purchasing_team = $item->purchasing_team;
			$lowest_price = $item->lowest_price;
			$winner_company = $item->winner_company;
			$m3_no = $item->m3_no;
			$date_of_m3 = $item->date_of_m3;
			$no_of_letter_sent_to_stock = $item->no_of_letter_sent_to_stock;
			$date_of_submission_to_stock = $item->date_of_submission_to_stock;
			$date_of_submission_to_the_finance_department = $item->date_of_submission_to_the_finance_department;
			$m3_no_sent_to_finance = $item->m3_no_sent_to_finance;
			$amount_sent_to_finance = $item->amount_sent_to_finance;
			$date_of_sent_to_finance = $item->date_of_sent_to_finance;
			$remarks = $item->remarks;

		}
    	// get the the log data and insert it into the log table.
		$purchaseLog = array(

			'date_of_preparation'								=> $date_of_preparation,
			'end_user'											=> $end_user,
			'goods_detail'										=> $goods_detail,
			'request_no'										=> $request_no,
			'date_of_arrival'									=> $date_of_arrival,
			'date_of_submission_to_the_purchasing_committee'	=> $date_of_submission_to_the_purchasing_committee,
			//'estimation_cost'									=> $estimation_cost,
			'purchasing_team'									=> $purchasing_team,
			'lowest_price'										=> $lowest_price,
			'winner_company'									=> $winner_company,
			'm3_no'												=> $m3_no,
			'date_of_m3'										=> $date_of_m3,
			'no_of_letter_sent_to_stock'						=> $no_of_letter_sent_to_stock,
			'date_of_submission_to_stock'						=> $date_of_submission_to_stock,
			'date_of_submission_to_the_finance_department'		=> $date_of_submission_to_the_finance_department,
			'm3_no_sent_to_finance'								=> $m3_no_sent_to_finance,
			'amount_sent_to_finance'							=> $amount_sent_to_finance,
			'date_of_sent_to_finance'							=> $date_of_sent_to_finance,
			'remarks'											=> $remarks,
			'user'												=> Auth::user()->username,
			'action'											=> 'Purchase Deleted'

		);
		$object = DB::connection(self::$myDb)->table('goods_purchase_log')->insert($purchaseLog);
		if($object){
			DB::connection('procurement')->table('goods_purchase')->where('id', $id)->delete();
			return true;
		}
		else{
			return false;
		}
	}
	//================================================== Procurement Plan Model Functions =====================================
	//get all the procurement plan data.
	public static function getProcurementPlanData()
	{
		$table = DB::connection(self::$myDb)
					->table('procurement_plan as pp');
					$table->select(
							'pp.id',
							'pp.sector',
							'pp.procurement_entity',
							'pp.budget_type',
							'pp.budget_code',
							'pp.year',
							// 'pp.name_of_project_in_approved_budget_plan',
							'pp.donor',
							'pp.procurement_description',
							'pp.type_of_contract',
							'pp.contract_number',
							'pm.name_en as procurement_method',
							'pp.guarantee_period_of_works'
							);
		$table->leftjoin("procurement_method AS pm","pm.id","=","pp.procurement_method");
		$object = $table->orderBy('pp.id','desc');
		return $object->get();
	}
	// insert the Procurement Plan form details in its table.
	public static function addProcurementPlanDetails($formData)
	{
		$object = DB::connection(self::$myDb)->table('procurement_plan')->insert($formData);
		if($object){return true;}else{return false;}
	}
	// get Procurement Plan based on id.
	public static function getSpecificProcurementPlanData($id)
	{
		$object = DB::connection(self::$myDb)->table('procurement_plan')->select('*')->where('id', $id)->first();
		return $object;
	}
	// update the Procurement Plan form details;
	public static function updateProcurementPlan($id, $formData)
	{
		$object = DB::connection(self::$myDb)->table('procurement_plan')->where('id', $id)->update($formData);
		if($object){return true;}else{return false;}
	}
	//deleting the procurement plan data based on id.
	public static function deleteProcurementPlanData($id)
	{	
		//get the Procurement plan based on id and then insert the data into log table.
		$procurementPlanData = DB::connection(self::$myDb)->table('procurement_plan')->select('*')->where('id', $id)->get();
		foreach($procurementPlanData as $item)
		{
			$sector = $item->sector;
			$procurement_entity = $item->procurement_entity;
			$budget_type = $item->budget_type;
			$budget_code = $item->budget_code;
			$year = $item->year;
			$name_of_project_in_arroved_budget_plan = $item->name_of_project_in_arroved_budget_plan;
			$donor = $item->donor;
			$procurement_description = $item->procurement_description;
			$type_of_contract = $item->type_of_contract;
			$procurement_preference_from_national_resources = $item->procurement_preference_from_national_resources;
			$contract_number = $item->contract_number;
			$date_of_budget_agreement_by_donor_or_mof = $item->date_of_budget_agreement_by_donor_or_mof;
			$procurement_method = $item->procurement_method;
			$estimated_cost_of_the_project_in_afg = $item->estimated_cost_of_the_project_in_afg;
			$estimated_date_of_procurement_initiation = $item->estimated_date_of_procurement_initiation;
			$estimated_date_of_project_announcement = $item->estimated_date_of_project_announcement;
			$estimated_date_of_bid_opening = $item->estimated_date_of_bid_opening;
			$date_of_bid_evaluation = $item->date_of_bid_evaluation;
			$date_of_completion_of_the_bid_evaluation = $item->date_of_completion_of_the_bid_evaluation;
			$date_of_the_evaluation_report_submission = $item->date_of_the_evaluation_report_submission;
			$estimated_date_of_the_public_award = $item->estimated_date_of_the_public_award;
			$estimation_date_of_contract_sign = $item->estimation_date_of_contract_sign;
			$guarantee_period_of_works = $item->guarantee_period_of_works;
			$estimation_date_of_contract_completion = $item->estimation_date_of_contract_completion;
			$remarks = $item->remarks;

		}
    	// get the the log data and insert it into the log table.
		$procurementLog = array(

			'sector'											=> $sector,
			'procurement_entity'								=> $procurement_entity,
			'budget_type'										=> $budget_type,
			'budget_code'										=> $budget_code,
			'year'												=> $year,
			'name_of_project_in_arroved_budget_plan'			=> $name_of_project_in_arroved_budget_plan,
			'donor'												=> $donor,
			'procurement_description'							=> $procurement_description,
			'type_of_contract'									=> $type_of_contract,
			'procurement_preference_from_national_resources'	=> $procurement_preference_from_national_resources,
			'contract_number'									=> $contract_number,
			'date_of_budget_agreement_by_donor_or_mof'			=> $date_of_budget_agreement_by_donor_or_mof,
			'procurement_method'								=> $procurement_method,
			'estimated_cost_of_the_project_in_afg'				=> $estimated_cost_of_the_project_in_afg,
			'estimated_date_of_procurement_initiation'			=> $estimated_date_of_procurement_initiation,
			'estimated_date_of_project_announcement'			=> $estimated_date_of_project_announcement,
			'estimated_date_of_bid_opening'						=> $estimated_date_of_bid_opening,
			'date_of_bid_evaluation'							=> $date_of_bid_evaluation,
			'date_of_completion_of_the_bid_evaluation'			=> $date_of_completion_of_the_bid_evaluation,
			'date_of_the_evaluation_report_submission'			=> $date_of_the_evaluation_report_submission,
			'estimated_date_of_the_public_award'				=> $estimated_date_of_the_public_award,
			'estimation_date_of_contract_sign'					=> $estimation_date_of_contract_sign,
			'guarantee_period_of_works'							=> $guarantee_period_of_works,
			'estimation_date_of_contract_completion'			=> $estimation_date_of_contract_completion,
			'remarks'											=> $remarks,
			'user'												=> Auth::user()->username,
			'action'											=> 'Procurement Plan Deleted'

		);
		$object = DB::connection(self::$myDb)->table('procurement_log')->insert($procurementLog);
		if($object){
			DB::connection(self::$myDb)->table('procurement_plan')->where('id', $id)->delete();
			return true;
		}
		else{
			return false;
		}
	}
	// get procurement details.
	public static function getProcurementPlanDetails($id)
	{
		$table = DB::connection(self::$myDb)
			->table('procurement_plan as pp')->where('pp.id', $id);
			$table->select(
						'pp.*',
						'pm.name_en as procurement_method'
						//DB::raw('CONCAT(DATEDIFF(`date_of_procurement_approval`, `project_submission_date_to_procurement_entity`)) AS submission_delay'),
						//DB::raw('CONCAT(DATEDIFF(`date_of_pre_qualification_meeting`, `announcement_date`)) AS announce_delay'),
						//DB::raw('CONCAT(DATEDIFF(`evaluation_completion_date`, `date_of_bid_opening`)) AS bid_opening_delay'),
						//DB::raw('CONCAT(DATEDIFF(`date_of_npc_approval`, `date_of_procurement_award`)) AS npc_approval_delay'),
						//DB::raw('CONCAT(DATEDIFF(`start_letter_date`, `date_of_notice_of_procurement_award`)) AS acceptance_letter_delay')
					);
		$table->leftjoin("procurement_method AS pm","pm.id","=","pp.procurement_method");
		$object = $table;
		return $object->get();
	}
	
	//================================================== Procurement Model Functions =====================================

	// get All Procurement
	public static function getProcurementData()
	{
		$table = DB::connection(self::$myDb)
					->table('procurement as p');
					$table->select(
							'p.id',
							'p.procurement_plan_id',
							'p.procurement_details',
							'dep.name as executive_department',
							'pm.name_en as procurement_method',
							'p.announcement_date',
							'p.date_of_pre_qualification_meeting',
							'p.date_of_bid_opening',
							'p.evaluation_completion_date',
							'p.date_of_procurement_award',
							'p.date_of_npc_approval',
							'p.date_of_notice_of_procurement_award',
							'p.start_letter_date',
							'p.winner_bidder',
							'p.archived'
							);
		$table->leftjoin("procurement_method AS pm","pm.id","=","p.procurement_method");
		$table->leftjoin("auth.department AS dep","dep.id","=","p.end_user");
		$object = $table->orderBy('p.id','desc');
		return $object->get();
	}

	// insert the Procurement form details in its table.
	public static function addProcurementDetails($formData)
	{
		$object = DB::connection(self::$myDb)->table('procurement')->insert($formData);
		if($object)
		{	
			return true;
		}
		else
		{
			return false;
		}
	}
	// get Procurement based on id.
	public static function getSpecificProcurementData($id)
	{
		$object = DB::connection(self::$myDb)->table('procurement')->select('*')->where('id', $id);
		return $object->get();
	}
	// update the Procurement form details;
	public static function editProcurementDetails($id, $formData)
	{
		$object = DB::connection(self::$myDb)->table('procurement')->where('id', $id)->update($formData);
		if($object)
		{	
			return true;
		}
		else
		{
			return false;
		}
	}
	//deleting the procurement data based on id.
	public static function deleteProcurementData($id)
	{	
		//get the Procurement based on id and then insert the data into log table.
		$procurementData = DB::connection(self::$myDb)->table('procurement')->select('*')->where('id', $id)->get();
		foreach($procurementData as $item)
		{
			$procurement_plan_id = $item->procurement_plan_id;
			$budget_code = $item->budget_code;
			$estimation_cost = $item->estimation_cost;
			$procurement_details = $item->procurement_details;
			$end_user = $item->end_user;
			$contract_number = $item->contract_number;
			$procurement_type = $item->procurement_type;
			$procurement_method = $item->procurement_method;
			$project_submission_date_to_procurement_entity = $item->project_submission_date_to_procurement_entity;
			$date_of_procurement_approval = $item->date_of_procurement_approval;
			$announcement_date = $item->announcement_date;
			$date_of_pre_qualification_meeting = $item->date_of_pre_qualification_meeting;
			$date_of_bid_opening = $item->date_of_bid_opening;
			$evaluation_completion_date = $item->evaluation_completion_date;
			$date_of_procurement_award = $item->date_of_procurement_award;
			$date_of_npc_approval = $item->date_of_npc_approval;
			$date_of_notice_of_procurement_award = $item->date_of_notice_of_procurement_award;
			$start_letter_date = $item->start_letter_date;
			$winner_bidder = $item->winner_bidder;
			$remarks = $item->remarks;

		}
    	// get the the log data and insert it into the log table.
		$procurementLog = array(
			'procurement_plan_id'								=> $procurement_plan_id,
			'budget_code'										=> $budget_code,
			'estimation_cost'									=> $estimation_cost,
			'procurement_details'								=> $procurement_details,
			'end_user'											=> $end_user,
			'contract_number'									=> $contract_number,
			'procurement_type'									=> $procurement_type,
			'procurement_method'								=> $procurement_method,
			'project_submission_date_to_procurement_entity'		=> $project_submission_date_to_procurement_entity,
			'date_of_procurement_approval'						=> $date_of_procurement_approval,
			'announcement_date'									=> $announcement_date,
			'date_of_pre_qualification_meeting'					=> $date_of_pre_qualification_meeting,
			'date_of_bid_opening'								=> $date_of_bid_opening,
			'evaluation_completion_date'						=> $evaluation_completion_date,
			'date_of_procurement_award'							=> $date_of_procurement_award,
			'date_of_npc_approval'								=> $date_of_npc_approval,
			'date_of_notice_of_procurement_award'				=> $date_of_notice_of_procurement_award,
			'start_letter_date'									=> $start_letter_date,
			'winner_bidder'										=> $winner_bidder,
			'remarks'											=> $remarks,
			'user'												=> Auth::user()->username,
			'action'											=> 'Procurement Deleted'

		);
		$object = DB::connection(self::$myDb)->table('procurement_log')->insert($procurementLog);
		if($object){
			DB::connection(self::$myDb)->table('procurement')->where('id', $id)->delete();
			return true;
		}
		else{
			return false;
		}
	}
	// get procurement details.
	public static function getProcurementDetails($id)
	{
		$table = DB::connection(self::$myDb)
					->table('procurement as p')->where('p.id', $id);
					$table->select(
									'*',
									'dep.name as executive_department',
									'pt.name_en as procurement_type',
									'pm.name_en as procurement_method',
									DB::raw('CONCAT(DATEDIFF(`date_of_procurement_approval`, `project_submission_date_to_procurement_entity`)) AS submission_delay'),
									DB::raw('CONCAT(DATEDIFF(`date_of_pre_qualification_meeting`, `announcement_date`)) AS announce_delay'),
									DB::raw('CONCAT(DATEDIFF(`evaluation_completion_date`, `date_of_bid_opening`)) AS bid_opening_delay'),
									DB::raw('CONCAT(DATEDIFF(`date_of_npc_approval`, `date_of_procurement_award`)) AS npc_approval_delay'),
									DB::raw('CONCAT(DATEDIFF(`start_letter_date`, `date_of_notice_of_procurement_award`)) AS acceptance_letter_delay')
								);

		$table->leftjoin("procurement_type AS pt","pt.id","=","p.procurement_type");
		$table->leftjoin("procurement_method AS pm","pm.id","=","p.procurement_method");
		$table->leftjoin("auth.department AS dep","dep.id","=","p.end_user");
		$object = $table;
		return $object->get();
	}
	

	//================================================== Contract Model Functions =====================================

	// get All Contracts.
	public static function getContractData()
	{

		$object = DB::connection(self::$myDb)
					->table('contract')
					->select('*')->where('archived','!=',1);
		// $object = $table->orderBy('id','desc');
		
		return $object->get();
	}
	// get the contracts total price and m3 total amount by id.
	public static function getContractById($id)
	{	
		$object = DB::connection(self::$myDb)->table('contract')->select('total_contract_price','total_m3_price')
					->where('procurement_id', $id);
		if(!empty($object)){
			$object = $object->orderBy('created_at', 'asc');
			return $object->first();
		}
		else{
			return false;
		}
	}
	// insert the contract form details in its table.
	public static function addContractDetails($formData)
	{
		$object = DB::connection(self::$myDb)->table('contract')->insert($formData);
		if($object)
		{	
			return true;
		}
		else
		{
			return false;
		}
	}
	// get Contract based on id and total_remained_contract_price.
	public static function getSpecificContractData($id, $field)
	{
		$table = DB::connection(self::$myDb)
					->table('contract')->where('procurement_id', $id)->where('total_remained_contract_price', $field);
					$table->select('*');
		$object = $table;
		return $object->get();
	}
	// update the Contract form details;
	public static function editContractDetails($id, $field, $formData)
	{
		$object = DB::connection(self::$myDb)->table('contract')
					->where('id', $id)->update($formData);
		if($object)
		{	
			return true;
		}
		else
		{
			return false;
		}
	}
	//deleting the Contract data based on id.
	public static function deleteContractData($id, $field)
	{	
		//get the Contract based on id and then insert the data into log table.
		$contractData = DB::connection(self::$myDb)->table('contract')->select('*')
							->where('procurement_id', $id)->where('total_remained_contract_price', $field)->get();
		foreach($contractData as $item)
		{
			$contract_name = $item->contract_name;
			$contract_no = $item->contract_no;
			$start_letter_date = $item->start_letter_date;
			$contract_completion_date = $item->contract_completion_date;
			$winner_bidder = $item->winner_bidder;
			$total_contract_price = $item->total_contract_price;
			$request = $item->request;
			$request_date = $item->request_date;
			$date_of_preparation_of_m3 = $item->date_of_preparation_of_m3;
			$m3_no = $item->m3_no;
			$total_m3_price = $item->total_m3_price;
			$date_of_m3 = $item->date_of_m3;
			$m3_no_to_stock = $item->m3_no_to_stock;
			$date_of_m3_to_stock = $item->date_of_m3_to_stock;
			$date_of_submission_letter_to_finance_department = $item->date_of_submission_letter_to_finance_department;
			$m3_number_sent_to_finance = $item->m3_number_sent_to_finance;
			$amount_sent_to_finance = $item->amount_sent_to_finance;
			$contract_modification_price = $item->contract_modification_price;
			$total_remained_contract_price = $item->total_remained_contract_price;
			$remained_contract_time = $item->remained_contract_time;
			$date_of_contract_termination = $item->date_of_contract_termination;
			$termination_reason = $item->termination_reason;
			$date_of_sent_to_finance = $item->date_of_sent_to_finance;
			$remarks = $item->remarks;

		}
    	// get the the log data and insert it into the log table.
		$contractLog = array(
			'procurement_id'									=> $id,
			'contract_name'										=> $contract_name,
			'contract_no'										=> $contract_no,
			'start_letter_date'									=> $start_letter_date,
			'contract_completion_date'							=> $contract_completion_date,
			'winner_bidder'										=> $winner_bidder,
			'total_contract_price'								=> $total_contract_price,
			'request'											=> $request,
			'request_date'										=> $request_date,
			'date_of_preparation_of_m3'							=> $date_of_preparation_of_m3,
			'm3_no'												=> $m3_no,
			'total_m3_price'									=> $total_m3_price,
			'date_of_m3'										=> $date_of_m3,
			'm3_no_to_stock'									=> $m3_no_to_stock,
			'date_of_m3_to_stock'								=> $date_of_m3_to_stock,
			'date_of_submission_letter_to_finance_department'	=> $date_of_submission_letter_to_finance_department,
			'm3_number_sent_to_finance'							=> $m3_number_sent_to_finance,
			'amount_sent_to_finance'							=> $amount_sent_to_finance,
			'contract_modification_price'						=> $contract_modification_price,
			'total_remained_contract_price'						=> $total_remained_contract_price,
			'remained_contract_time'							=> $remained_contract_time,
			'date_of_contract_termination'						=> $date_of_contract_termination,
			'termination_reason'								=> $termination_reason,
			'date_of_sent_to_finance'							=> $date_of_sent_to_finance,
			'remarks'											=> $remarks,
			'user'												=> Auth::user()->username,
			'action'											=> 'Contract Deleted'

		);
		$object = DB::connection(self::$myDb)->table('contract_log')->insert($contractLog);
		if($object){
			DB::connection(self::$myDb)->table('contract')
						->where('procurement_id', $id)->where('total_remained_contract_price', $field)->delete();
			return true;
		}
		else{
			return false;
		}
	}
	// get Contract based on id.
	public static function getSpecificContractDetails($id, $field)
	{
		$table = DB::connection(self::$myDb)
					->table('contract')->where('procurement_id', $id)->where('total_remained_contract_price', $field);
					$table->select(
							'*',
							DB::raw('CONCAT(DATEDIFF(`date_of_submission_letter_to_finance_department`, `date_of_m3_to_stock`)) AS delay'),
							DB::raw('CONCAT(DATEDIFF(`contract_completion_date`, NOW())) AS remained_contract_time')
						);
		$object = $table;
		return $object->get();
	}
	//================================================== Archived Contract Model Functions =====================================

	// get Archived Contracts.
	public static function getArchivedContracts()
	{

		$object = DB::connection(self::$myDb)
					->table('contract')
					->select(
							'id',
							'procurement_id',
							'contract_name',
							'start_letter_date',
							'contract_completion_date',
							'm3_no',
							'total_m3_price',
							'date_of_m3',
							'm3_no_to_stock',
							'date_of_submission_letter_to_finance_department',
							'm3_number_sent_to_finance',
							'amount_sent_to_finance',
							'date_of_m3_to_stock',
							'date_of_submission_letter_to_finance_department',
							'total_remained_contract_price'
							)->where('archived','!=',0);
		// $object = $table->orderBy('id','desc');
		
		return $object->get();
	}
	//deleting the Contract data based on id.
	public static function archiveContractData($procurement_id)
	{	
		//get the Contract based on id and then insert the data into log table.
		$procurement = DB::connection(self::$myDb)->table('procurement')->select('*')->where('id', $procurement_id)->first();
    	// get the the log data and insert it into the log table.
		$procurementLogData = array(
			'procurement_plan_id'								=> $procurement->procurement_plan_id,
			'budget_code'										=> $procurement->budget_code,
			'estimation_cost'									=> $procurement->estimation_cost,
			'procurement_details'								=> $procurement->procurement_details,
			'end_user'											=> $procurement->end_user,
			'contract_number'									=> $procurement->contract_number,
			'procurement_type'									=> $procurement->procurement_type,
			'procurement_method'								=> $procurement->procurement_method,
			'project_submission_date_to_procurement_entity'		=> $procurement->project_submission_date_to_procurement_entity,
			'date_of_procurement_approval'						=> $procurement->date_of_procurement_approval,
			'announcement_date'									=> $procurement->announcement_date,
			'date_of_pre_qualification_meeting'					=> $procurement->date_of_pre_qualification_meeting,
			'date_of_bid_opening'								=> $procurement->date_of_bid_opening,
			'evaluation_completion_date'						=> $procurement->evaluation_completion_date,
			'date_of_procurement_award'							=> $procurement->date_of_procurement_award,
			'date_of_npc_approval'								=> $procurement->date_of_npc_approval,
			'date_of_notice_of_procurement_award'				=> $procurement->date_of_notice_of_procurement_award,
			'start_letter_date'									=> $procurement->start_letter_date,
			'winner_bidder'										=> $procurement->winner_bidder,
			'remarks'											=> $procurement->remarks,
			'archived'											=> 1,
			'user'												=> Auth::user()->username,
			'action'											=> 'Contract Archived'

		);
		$object = DB::connection(self::$myDb)->table('procurement_log')->insert($procurementLogData);
		if($object){
			DB::connection(self::$myDb)->table('procurement')->where('id', $procurement_id)->update(array('archived' => 1));
			DB::connection(self::$myDb)->table('contract')->where('procurement_id', $procurement_id)->update(array('archived' => 1));
			return true;
		}
		else{
			return false;
		}
	}
	//====================================================== General Log insertion function ======================================================
	// add the log data based on the specified table name and data.
	public static function addLogData($table, $formData)
	{
		$object = DB::connection(self::$myDb)->table($table)->insert($formData);
	}

}
