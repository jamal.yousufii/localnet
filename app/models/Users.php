<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Users extends Model
{
  use SoftDeletes;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['department_id','name','father','email','position','password','confirm_password'];

  function department()
  {
    return $this->hasOne('App\models\Departments','id','department_id');
  }

  public static function getAllDepartments($lang="en")
  {
    return DB::table('departments')->select('id','name_'.$lang.' as name')->where('deleted_at',null)->get();
  }

  function contractor()
  {
    return $this->belongsTo('App\models\Settings\Contractor','contractor_id','id'); 
  }


  public static function getModulesByDepartment($id=0)
  {
    $lang = get_language();
    return DB::table('modules')->select('modules.id','modules.name_'.$lang.' as name')->leftJoin('module_deps as md','modules.id','=','md.module_id')->where('modules.deleted_at',null)->where('md.deleted_at',null)->where('md.department_id',$id)->get();
  }

  public static function getSectionsByModule($id=array())
  {
    $lang = get_language();
    $data = DB::table('sections')->select('sections.id','sections.name_'.$lang.' as name','md.name_'.$lang.' as application')->leftJoin('modules as md','module_id','=','md.id')->where('sections.deleted_at',null)->whereIn('md.id',$id)->orderBy('orders')->get();
    $selected_sec = array();
    if($data)
    {
      foreach($data as $item)
      {
        $selected_sec[$item->application][] = $item->id."-".$item->name;
      }
    }
    return $selected_sec;
  }
  
  public static function getContractorSectionsByModule($id=array())
  {
    $lang = get_language();
    $data = DB::table('sections')
            ->select('sections.id','sections.name_'.$lang.' as name','md.name_'.$lang.' as application')
            ->leftJoin('modules as md','module_id','=','md.id')
            ->whereRaw(('sections.code IN ("pmis_project","pmis_reports") OR sections.tab_code IN ("for_project","daily_report_menue","report_contractor","pmis_payments") '))
            ->where('sections.deleted_at',null)
            ->whereIn('md.id',$id)
            ->orderBy('orders')
            ->get();

    $selected_sec = array();
    if($data)
    {
      foreach($data as $item)
      {
        $selected_sec[$item->application][] = $item->id."-".$item->name;
      }
    }
    return $selected_sec;
  }

  public static function getRolesBySections($id=array(),$lang="en")
  {
    $lang = get_language();
    $data = DB::table('roles')->select('roles.id','roles.name_'.$lang.' as name','roles.code','s.name_'.$lang.' as section')->leftJoin('sections as s','section_id','=','s.id')->whereIn('section_id',$id)->where('roles.deleted_at',null)->get();
    $selected_role = array();
    if($data)
    {
      foreach($data as $item)
      {
        $selected_role[$item->section][] = $item->id."-".$item->name;
      }
    }
    return $selected_role;
  }

  public static function getUserModules($id=0,$lang="en")
  {
    return DB::table('module_user')->select('module_user.module_id','module_user.id','md.name_'.$lang.' as application')->leftJoin('modules as md','module_id','=','md.id')->where('md.deleted_at',null)->where('user_id',$id)->get();
  }

  public static function getUserSections($id=0,$lang="en",$flag=false)
  {
    return DB::table('user_section')->select('user_section.section_id','user_section.id','sec.name_'.$lang.' as sections')->leftJoin('sections as sec','section_id','=','sec.id')->where('user_id',$id)->where('sec.deleted_at',null)->get();
  }

  public static function getUserRoles($id=0,$lang="en",$flag=false)
  {
    $data = DB::table('user_role')->select('user_role.role_id','user_role.id','rol.name_'.$lang.' as roles','s.name_'.$lang.' as section')->leftJoin('roles as rol','role_id','=','rol.id')->leftJoin('sections as s','section_id','=','s.id')->where('user_id',$id)->where('rol.deleted_at',null)->get();
    if($flag){
        return $data;
    }
    else {
      $selected = array();
      if($data)
      {
        foreach($data as $item)
        {
          $selected[$item->section][] = $item->roles;
        }
      }
      return $selected;
    }
  }

  public static function getUserRoleCode($code=0,$lang="en")
  {
    return DB::table('users as u')->select('u.id')->join('user_role as ur','ur.user_id','=','u.id')->join('roles as rol','ur.role_id','=','rol.id')->where('rol.code',$code)->where('rol.deleted_at',null)->get();
  }
}
?>
