<?php

namespace App\models\sched;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class ManageSpecialDays extends Model
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'sched';
	protected $table = 'special_days';

	public static function insertFormData($formData)
	{
		$object = DB::connection("sched")
					->table('special_days')
					->insert($formData);

		return $object;
	}

	// check the special days.
	public static function checkSpecialDays($date)
	{
		$object = DB::connection("sched")
					->table('special_days')
					->select(array('date','title','description'))
					->where('date', $date)
					->get();

		return $object;
	}

	// get special days based on id.
	public static function getSpicificSD($id)
	{
		$object = DB::connection("sched")
			->table('special_days')
			->select(array('id','date','title','description','type'))
			->where('id', $id)
			->get();

		return $object;
	}

	public static function getAllSDList()
	{
		$object = DB::connection("sched")
			->table('special_days')
			->select(array('id','date','title','description','type'))
			->get();
		return $object;
	}

	public static function addLogData($logData)
	{
		DB::connection("sched")->table('log')->insert($logData);
	}
	
	public static function deleteSpecialDay($id)
	{
		$updated = DB::connection("sched")->table("special_days")->update(array('deleted' => 1));
		if($updated) return true;
		else return false;
	}


}
