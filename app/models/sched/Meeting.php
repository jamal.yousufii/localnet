<?php

namespace App\models\sched;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Input;

class Meeting extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public static $myDb = 'sched';
	protected $table = "schedule.meeting_schedule";

	public static function addMeetingData($formData)
	{
		$object = DB::connection(self::$myDb)->table('meeting_schedule')->insertGetID($formData);
		return $object;
	}

	public static function addRecurredMeetingData($formData)
	{
		$object = DB::connection(self::$myDb)->table('meeting_schedule')->insert($formData);
		if($object) 
			return true; 
		else 
			return false;
	}

	public static function updateMeeting($formData, $id)
	{
		$query = DB::connection(self::$myDb)->table('meeting_schedule')->where("id", $id)->update($formData);
		return $query;
	}

	// upload the files.
	public static function uploadFiles($data)
	{
		$uploaded = DB::connection('sched')->table('uploads')->insert($data);
		if($uploaded){return true;}else{return false;}
	}

	public static function getDailyMeetings($date)
	{

		$object = DB::connection(self::$myDb)
					->table('meeting_schedule as ms')
					->select(
						'ms.*',
						DB::raw('DATE_FORMAT(ms.meeting_start, "%H:%i") AS meeting_start'),
						DB::raw('IF(ms.meeting_end!="00:00:00", DATE_FORMAT(ms.meeting_end, "%H:%i"),"") AS meeting_end'),
						'l.name as location','s.name as sector','mt.name as meeting_type'
					)
					->leftjoin("locations as l","ms.location",'=',"l.id")
					->leftjoin("sectors as s",'ms.sector','=','s.id')
					->leftjoin("meeting_type as mt","ms.meeting_type","=","mt.id")
					->where("date", "=", $date)
					->where("delay", "=", 0)
					->where("deleted", "=", 0)
					//->orwhere('recurring', 1)
					//->where('recure_type', 1)
					//->orwhere('recure_type', 3)
					->orderBy('meeting_start')
					->get();
		
		return $object;
	}

	public static function getWeeklyMeetings($start_date, $end_date)
	{

		$object = DB::connection(self::$myDb)
					->table('meeting_schedule')
					->select('*')->where('date','>=',$start_date)->where('date','<=',$end_date)->where("deleted", 0)->orderBy('meeting_start')
					->get();
		
		return $object;
	}

	public static function getAllMeetings()
	{

		$object = DB::connection(self::$myDb)
					->table('meeting_schedule')
					->select(
						'id',
						DB::raw('DATE_FORMAT(meeting_start, "%h:%i %p") AS meeting_start'),
						DB::raw('DATE_FORMAT(meeting_end, "%h:%i %p") AS meeting_end'),
						DB::raw('abs(TIME_FORMAT(TIMEDIFF(meeting_start,meeting_end), "%i" )) AS duration'),
						'meeting_subject',
						'meeting_with',
						'location'
					)->where("deleted", "=", 0);

		return $object;
	}

	public static function getSpecificMeeting($id)
	{
		$object = DB::connection(self::$myDb)
					->table('meeting_schedule as ms')->select("ms.*")
					->leftjoin("locations as l","ms.location",'=',"l.id")
					->leftjoin("sectors as s",'ms.sector','=','s.id')
					->leftjoin("meeting_type as mt","ms.meeting_type","=","mt.id")
					->where("ms.id", $id)
					->where("ms.deleted", 0)
					->get();

		return $object;
	}

	// get searched meetings based on the send parameters.
	public static function postSearchMeetings($print)
	{
		if(Input::get('s_date') != "")
		$start_date = convertToGregorian(gregorian_format(Input::get('s_date')));
		else $start_date = "";
		if(Input::get('e_date') != "")
		$end_date = convertToGregorian(gregorian_format(Input::get('e_date')));
		else $end_date = "";

		$meeting_type = Input::get('meeting_type');
		$sector = Input::get('sector');

		$table = DB::connection(self::$myDb)->table('meeting_schedule as ms');
		$table->select(
						'ms.*',
						DB::raw('DATE_FORMAT(ms.meeting_start, "%H:%i") AS meeting_start'),
						DB::raw('IF(ms.meeting_end!="00:00:00", DATE_FORMAT(ms.meeting_end, "%H:%i"),"") AS meeting_end'),
						's.name as sector_name','mt.name as meeting_type_name'
					);
		$table->leftjoin("meeting_type AS mt","ms.meeting_type","=","mt.id");
		$table->leftjoin("sectors AS s","ms.sector","=","s.id");
		
		if($start_date != "")
		{
			$table->where('ms.date','>=',$start_date);
		}
		if($end_date != "")
		{
			$table->where('ms.date','<=',$end_date);
		}
		if($start_date != "" && $end_date != "")
		{
			$table->where('ms.date','>=',$start_date)->where('ms.date','<=',$end_date);
		}

		if($meeting_type != "")
		{
			$table->where('ms.meeting_type',$meeting_type);
		}

		if(count($sector) != 0)
		{
			$table->whereIn('ms.sector', $sector);
		}

		$table->where('ms.deleted',0)->orderBy('ms.meeting_start','asc');
		//dd($table->tosql());
		if($table->count()>0)
		{
			// if($print == "")
			// 	return $object = $table->paginate(100);
			// else
				return $object = $table->get();
		}
		else
			return "";
	}

	// get week days and return the result back to the controller.
	public static function getMeetingWeekDays()
	{
		//print_r($condition);exit;
		$object = DB::connection(self::$myDb)
					->table('week_days')
					->select("*")
					->get();
	
		return $object;

	}
	// get weekly recurring days based on meeting id from weekly recure table and return the result back to the controller.
	public static function getMeetingWeeklyRecurringDays($meeting_id)
	{
		//print_r($condition);exit;
		$object = DB::connection(self::$myDb)->table('weekly_recurrence')->select("week_day")->where("meeting_id", $meeting_id)->get();
	
		return $object;
	}

	// get Meeting Locations and return the result back to the controller.
	public static function getMeetingLocations()
	{
		//print_r($condition);exit;
		$object = DB::connection(self::$myDb)
					->table('locations')
					->select("*")
					->get();
	
		return $object;

	}
	
	// get Meeting Sectors and return the result back to the controller.
	public static function getMeetingSectors()
	{
		$object = DB::connection(self::$myDb)
					->table('sectors')
					->select("*")
					->get();
	
		return $object;

	}
	
	// get Meeting Type and return the result back to the controller.
	public static function getMeetingType()
	{
		$object = DB::connection(self::$myDb)
					->table('meeting_type')
					->select("*")
					->get();
	
		return $object;

	}
	
	public static function getDeleteMeeting($meeting_id)
	{
		$updated = DB::connection(self::$myDb)->table("meeting_schedule")->where("id", $meeting_id)->update(array('deleted' => 1));
		if($updated){
			$filesRemoved = DB::connection('sched')->table('uploads')->where('meeting_id', $meeting_id)->update(array('deleted' => 1));
			Meeting::addScheduleLog($meeting_id,"Deleted");
			return true; 	
		}
		else return false;
	}

	//delete the recurred meeting.
	public static function getDeleteReccuredMeeting($meeting_id)
	{
		Meeting::addScheduleLog($meeting_id,"Deleted");
		$deleted = DB::connection(self::$myDb)->table("meeting_schedule")->where("id", $meeting_id)->delete();
		if($deleted) 
		{
			return true; 
		}
		else return false;
	}

	// get all meetings log from its table and return to the controller.
	public static function getAllLogs()
	{
		$log = DB::table('schedule.meeting_log as ml')->select("ml.*","usr.first_name as fname","usr.last_name as lname","l.name as location","s.name as sector","mt.name as meeting_type");
		$log->leftjoin('auth.users as usr','usr.id','=','ml.user_id');
		$log->leftjoin("schedule.locations as l","ml.location",'=',"l.id");
		$log->leftjoin("schedule.sectors as s",'ml.sector','=','s.id');
		$log->leftjoin("schedule.meeting_type as mt","ml.meeting_type","=","mt.id");
		$log->orderBy('ml.id','desc');
		if($log->count()>0){
			return $log->paginate(10);
		}else{
			return "";
		}
	}

	// get all meetings for showing in JfullCalendar.
	public static function getMeetingsForCalendar()
	{

		$object = DB::connection(self::$myDb)->table("meeting_schedule")
						->select('meeting_start', 'meeting_end', 'meeting_subject', 'date')
						->where("deleted", 0)->get();

		return $object;

	}


	// get last updated record date and time.
	public static function getLastUpdatedRecordInfo()
	{
		$object = DB::connection(self::$myDb)
					->table('meeting_log')
					->select("updated_at")
					->orderBy('id', 'desc')
					->first();
	
		return $object;

	}

	//get file name based on sent parameters.
	public static function getFileName($file_id)
	{
		$object = DB::connection('sched')->table("uploads")->select('file_name', 'original_file_name')->where("id", $file_id)->first();
		if($object){return $object;}
		else return false;
	}

	public static function removeFile($file_id)
	{
		$deleted = DB::connection('sched')->table("uploads")->where("id", $file_id)->delete();
		if($deleted){return $deleted;}
		else{return false;}	
	}

	// Delete the old reccuring meetings based on sent parameters.
	public static function deleteOldRecurringMeetings($start, $end, $start_recurrence, $end_recure, $recure_type)
	{
		// get all the previously recurred meeting's id and add the log.
		$previousRecurredMeetingIDs = DB::connection(self::$myDb)->table('meeting_schedule')->select('id')->where('meeting_start', $start)->where('meeting_end', $end)
					->where('recurring', 1)->where('recure_type', $recure_type)->where('date', '>=', $start_recurrence)->where('end_recure', $end_recure)->get();
		foreach($previousRecurredMeetingIDs as $prmids)
		{
			// add every individual meeting's log into its table.
			Meeting::addScheduleLog($prmids->id,"Deleted");
		}
		// delete all the previously recurred meetings
		DB::connection(self::$myDb)->table('meeting_schedule')->where('meeting_start', $start)->where('meeting_end', $end)
					->where('recurring', 1)->where('recure_type', $recure_type)->where('date', '>=', $start_recurrence)->where('end_recure', $end_recure)->delete();
	}
	// Delete the non recurred meeting which is edited to be recurred
	public static function deleteExistingNonRecureMeeting($meeting_id)
	{
		DB::connection(self::$myDb)->table('meeting_schedule')->where('id', $meeting_id)->where('recurring', 0)->delete();
	}

	// add the meeting log.
	public static function addScheduleLog($record_id,$action="")
	{
		$meeting = DB::connection(self::$myDb)->table("meeting_schedule")->where("id", $record_id)->get();
		if(!empty($meeting))
		{
			$mData = "";
			foreach($meeting as $m)
			{
				$mData = array(
					"meeting_start" 		=> $m->meeting_start,
					'meeting_end' 			=> $m->meeting_end,
					'meeting_subject' 		=> $m->meeting_subject,
					'location' 				=> $m->location,
					'sector' 				=> $m->sector,
					'meeting_type' 			=> $m->meeting_type,
					'arg_media'				=> $m->arg_media,
					'date' 					=> $m->date,
					'recurring' 			=> $m->recurring,
					'recure_type' 			=> $m->recure_type,
					'num_of_weeks' 			=> $m->num_of_weeks,
					'end_recure' 			=> $m->end_recure,
					'delay' 				=> $m->delay,
					'deleted' 				=> $m->deleted,
					'meeting_id' 			=> $m->id,
					'user_id' 				=> Auth::user()->id,
					'action' 				=> $action,
					'updated_at' 			=> date('Y-m-d H:i:s')
				);
			}
			DB::connection("sched")->table('meeting_log')->insert($mData);
		}
	}

	// get all the delayed meetings.
	public static function getAllDelayedMeetings()
	{
		$delayedMeetings = DB::table('schedule.meeting_schedule as ms')->select("ms.*","l.name as location","s.name as sector","mt.name as meeting_type");
		$delayedMeetings->leftjoin("schedule.locations as l","ms.location",'=',"l.id");
		$delayedMeetings->leftjoin("schedule.sectors as s",'ms.sector','=','s.id');
		$delayedMeetings->leftjoin("schedule.meeting_type as mt","ms.meeting_type","=","mt.id");
		$delayedMeetings->where('delay',1)->orderBy('ms.id','desc');
		if($delayedMeetings->count()>0){
			return $delayedMeetings->paginate(10);
		}else{
			return "";
		}
	}

}
