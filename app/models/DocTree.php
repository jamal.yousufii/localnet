<?php namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class DocTree extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'document_trees';

	public static function getAllTrees()
	{
		  
		$userid = Auth::user()->id;

		$table = DB::table('document_trees');
		$table->where('user_id',$userid);
        $object = $table->get();

        return $object;
	}
	public static function getTreeDetails($id=0)
	{
		  
		$userid = Auth::user()->id;

		$table = DB::table('document_trees');
		$table->where('user_id',$userid);
		$table->where('id',$id);
        $object = $table->get();

        return $object;
	}
	public static function getOnlyParentTrees()
	{
		  
		$userid = Auth::user()->id;

		$table = DB::table('document_trees');
		$table->where('user_id',$userid);
		$table->where('parent',0);
        $object = $table->get();

        return $object;
	}
}
