<?php namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Session;
class RoleX extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'role';

	public static function getAll($entity_id=0)
	{
		    
		$table = DB::table('role');

		if($entity_id!=0)
		{
			$table->where('entity_id',$entity_id);
		}
		
		$lang = getLangShortcut();

		$table->leftJoin('entity','entity.id','=','role.entity_id');
		$table->leftJoin('module','module.id','=','entity.module_id');
		$table->select('role.*','entity.name_'.$lang.' AS section','module.name AS module');
        $object = $table->get();

        return $object;
	}

	public static function getData($entity_id=0)
	{
		    
		$table = DB::table('role AS r');
		
		$table->leftJoin('entity AS e','e.id','=','r.entity_id');
		$table->leftJoin('module AS m','m.id','=','e.module_id');
		if($entity_id!=0)
		{
			$table->where('entity_id',$entity_id);
		}
		$object = $table->select(
						'r.id',
						'r.code',
						'r.name',
						'e.name_'.getLangShortcut().' AS section',
						'm.name AS module'
						);
        
        return $object->get();
	}

	public static function getDetails($id=0)
	{
		$table = DB::table('role');
		$table->where('id',$id);
		$object = $table->get();
		return $object;
	}
	public static function getAllBySecId($id=0)
	{
		$entity_ids = explode(',', $id);

		$table = DB::table('role');
		$table->leftJoin('entity','entity.id','=','role.entity_id');
		
		$table->whereIn('entity.id',$entity_ids);
		$table->orderBy('entity.code');
		$table->select('role.*','entity.name_'.getLangShortcut().' AS section');
        $object = $table->get();

        return $object;
	}

	//get user module
	public static function getUserModules($userid=0)
	{
		$table = DB::table('user_module AS um');

		$table->select('m.name AS module_name','m.code AS module_code');
		$table->leftJoin('module AS m','m.id','=','um.module_id');
		$table->where('um.user_id',$userid);
		$object = $table->get();
		return $object;
	}
	//get user sections
	public static function getUserSections($userid=0)
	{
		$lang = getLangShortcut();
		
		$table = DB::table('user_entity AS ue');

		$table->select('e.name_'.$lang.' AS seciton','e.code AS section_code');
		$table->leftJoin('entity AS e','e.id','=','ue.entity_id');
		$table->where('ue.user_id',$userid);
		$object = $table->get();
		return $object;
	}

	//get user roles
	public static function getUserRoles($userid=0)
	{
		
		$table = DB::table('user_role AS ur');

		$table->select(
						'r.name AS role',
						'r.code AS role_code',
						'm.code AS module_code',
						'e.code AS section_code'
					  );

		$table->leftJoin('role AS r','r.id','=','ur.role_id');
		$table->leftJoin('entity AS e','e.id','=','r.entity_id');
		$table->leftJoin('module AS m','m.id','=','e.module_id');

		$table->where('ur.user_id',$userid);
		$object = $table->get();
		return $object;

	}

	

}
