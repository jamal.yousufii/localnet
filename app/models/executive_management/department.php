<?php namespace App\models\executive_management;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Input;
use get;


class department extends Model {		

		public static function get_department(){

			$rows=\DB::connection('executive_management')->table('department')->select('id','name')->get();
			return $rows;
		}
}
