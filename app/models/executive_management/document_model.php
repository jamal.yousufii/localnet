<?php namespace App\models\executive_management;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Input;
use get;


class document_model extends Model {

	protected $table ='executive_management.executive_documents';
	public $timestamps = false;

	public static function get_document_hakom(){

		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =1 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;

}
	public static function get_document_maktob(){

		
		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =2 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;

}	
public static function get_document_copy_som(){

		
		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =13 ")->orderby('document.id','desc')->paginate(18);
					$data['rows'] = $rows;
			return $data;

}
	public static function get_makatab_warada(){

		
		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =11 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;

}
	public static function get_document_pashnahad(){

		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =3 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;

}
	public static function get_document_farman(){

		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =4 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;
}
	public static function get_document_istalam(){

		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =5 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;

}
	public static function get_document_feceno(){

		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =6 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;

}
	public static function get_document_maktob_he(){

		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =7 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;

}
	public static function get_document_waraqa_darkhasti(){

		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =14 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;

}
	public static function get_document_pashnahad_he(){

		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =8 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;


}
	public static function get_document_feceno_he(){

		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =9 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;
		}
	public static function get_document_istalam_he(){

		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =12 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;


}
	public static function get_document_tayenat(){

		$rows=\DB::table('executive_management.executive_documents as document')->select('document.*','dept.name as dept_name')->leftjoin('executive_management.department AS dept','document.department_id','=','dept.id')->whereRaw(" status= 0 AND document.document_type =10 ")->orderby('document.id','desc')->paginate(15);
					$data['rows'] = $rows;
			return $data;

}
	public static function get_last_id(){

		$rows=\DB::table('executive_management.executive_documents as document')->select('id')->orderby('id','desc')->limit('1')->get();
		return $rows;

}

}
