<?php namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
class Section extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'entity';

	public static function getAll($id=0)
	{
		   
		$table = DB::table('entity');
		$table->leftJoin('module','module.id','=','entity.module_id');
		if($id!=0)
		{
			$table->where('module_id',$id);
		}
		$table->select('entity.*','module.name AS module');
        $object = $table->get();

        return $object;
	}
	public static function getUserSections($userid=0)
	{
		   
		$table = DB::table('user_entity');
		$table->where('user_id',$userid);
        $object = $table->get();

        return $object;
	}
	public static function getUserRoles($userid=0)
	{
		   
		$table = DB::table('user_role');
		$table->where('user_id',$userid);
        $object = $table->get();

        return $object;
	}
	public static function getData($module_id=0)
	{
		    
		$table = DB::table('entity AS e')
					->leftJoin('module AS m','m.id','=','e.module_id')
					->select(
								 'e.id',
								 'e.code',
								 'e.table_name',
								 'e.name_'.getLangShortcut().' AS section_name',
								 'm.name AS module'
								 );
        if($module_id!=0)
		{
			$table->where('module_id',$module_id);
		}

        return $table->get();
	}

	public static function getDetails($id=0)
	{
		$table = DB::table('entity');
		$table->where('id',$id);
		$object = $table->get();
		return $object;
	}
	public static function getAllByModId($module_id=0)
	{
		$module_ids = explode(',', $module_id);

		$table = DB::table('entity');
		$table->leftJoin('module','module.id','=','entity.module_id');
		
		$table->whereIn('module.id',$module_ids);
		$table->orderBy('module.code');
		$table->select('entity.*','module.name AS module');
        $object = $table->get();

        return $object;
	}

	

}
