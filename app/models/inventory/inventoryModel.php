<?php

namespace App\models\inventory;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Session;

class inventoryModel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public static $myDb = "inventory";

	//insert the Inventory form data into the table.
	public static function addInventory($form_data)
	{
		$object = DB::connection(self::$myDb)
					->table('inventory')
					->insertGetId($form_data);
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object;
	}
	//get stock in based on product type.
	public static function getStockInByProductType($product_type)
	{
		$object = DB::connection(self::$myDb)->table('inventory')->select('stock_in','stock_out')->where('product_type', $product_type)->where('deleted', 0)->orderBy('id','desc')->first();//->toSql();
		//dd($object);
		if($object != ""){
			$balance = $object->stock_in - $object->stock_out;
			return $balance;
		}else{return "";}
	}
	//get change the status of deleted field of cases table based on it's id.
	public static function getDeleteInventory($id)
	{
		//dd($id);
		$object = DB::connection(self::$myDb)->table('inventory')->where('id', $id)->update(array('deleted' => 1));
		if($object){return true;}else{return false;}
	}
	public static function addLog($data)
	{
		DB::connection(self::$myDb)->table("log")->insert($data);
	}

	// get the Departures.
	public static function getData($product_type)
	{
		if($product_type != "")
		{
			$table = DB::connection(self::$myDb)
					->table('inventory as i');
					$table->select('i.*','pt.type as product_type');
			$table->leftjoin("product_type AS pt","pt.id","=","i.product_type");
			$table->where('i.product_type',$product_type);
			$table->where('i.deleted',0);
			$table->orderBy('i.id','asc');
			$object = $table->paginate(15);
			return $object;
		}
		$table = DB::connection(self::$myDb)
					->table('inventory as i');
					$table->select('i.*','pt.type as product_type');
		$table->leftjoin("product_type AS pt","pt.id","=","i.product_type");
		$table->where('i.deleted',0);
		$table->orderBy('i.product_type','asc');
		//$table->groupBy('i.product_type');
		$object = $table->paginate(15);
		return $object;
	}
	//get the inventory details info based on id.
	public static function getSpecificInventoryDetails($id)
	{
		$table = DB::connection(self::$myDb)
					->table('inventory as i');
					$table->select('i.*');
		//$table->leftjoin("product_type AS pt","pt.id","=","i.product_type");
		$object = $table->where('i.id',$id);
		if($object){
			return $object->first();
		}
		else{
			return false;
		}
	}
	//update the inventory form data into the table.
	public static function updateInventory($id, $form_data)
	{
		$object = DB::connection(self::$myDb)
					->table('inventory')
					->where('id', $id)
					->update($form_data);
		return $object;
	}
	//get sum of stocks based on product type.
	public static function getStocksTotal($product_type, $field)
	{
		$object = DB::connection(self::$myDb)->table('inventory')->where('product_type',$product_type)->where('deleted',0)->sum($field);
		return $object;
	}

}
