<?php
namespace App\models\finance;
use Illuminate\Database\Eloquent\model;

use DB;

class financeNew extends Model {


    public static $dbConnect = "finance";

    protected $connection 	= 'finance';

    protected $table = 'finance';

//    public static function getAll()
//    {
//        $table = DB::connection(self::$dbConnect)->table('finance');
//        $table->where('id','>',2);
//        $table->where('id','<',5);
//        //$table->orWhere();
//        $object = $table->get();
//
//        //return the object
//        return $object;
//    }


    public static function listAll()
    {
        $table = DB::connection(self::$dbConnect)->table('finance');
        //$table->orderBy('id', 'desc')->paginate(2);
        $object = $table->get();
        return $object;
    }

    public static function details($ide=0)
    {
        $table = DB::connection(self::$dbConnect)->table('finance')->where('ide',$ide);
        $object = $table->get();
        return $object;
    }

}

