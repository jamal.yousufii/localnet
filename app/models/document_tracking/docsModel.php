<?php

namespace App\models\document_tracking;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Session;
use Input;

class docsModel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

	// upload the files.
	public static function uploadFiles($data)
	{
		$uploaded = DB::connection('document_tracking')->table('uploads')->insert($data);
		if($uploaded){return true;}else{return false;}
	}
	// get the documents based on the current shamsi year.
	public static function getIncomingDocuments()
	{

		$table = DB::connection('document_tracking')->table('incoming_docs as ind')->select('ind.*','m.name_dr as ministries');
		$table->leftjoin('ministries as m','m.id','=','ind.sender');
		if(!isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs') && !isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$table->rightjoin('assignees as ass','ass.incoming_doc_id','=','ind.id');
			$table->leftjoin('hr.employees as emp','emp.id','=','ass.assignee_id');
			$table->where('ass.assignee_id', Auth::user()->employee_id);
			$table->orderBy('ass.assignee_description','asc');
		}
		$table->where('ind.incoming_date', '>=', getCurrentJalaliYearStartDate());
		$table->where('ind.incoming_date','<=', getCurrentJalaliYearEndDate());
		$table->where('ind.deleted',0);
		$table->orderBy('ind.execution_date','asc');
        $table->orderBy('ind.incoming_number','asc');
        $table->where('ind.department_id',getUserDepartment(Auth::user()->id)->department); 
		if($table->count()>0){
			return $object = $table->paginate(10);
		}else{
			return "";
		}
	}

	//get the list of pending incoming numbers of incoming documents.
	public static function getPendingIncomingNumbers($selected_item=0,$year="")
	{
		$pending_incoming_nums = DB::connection('document_tracking')->table('incoming_docs')->select('id','incoming_number','execution_type','execution_date');

		if($year != "")
		{
			$pending_incoming_nums->where('incoming_date', '>=', getCurrentJalaliYearStartDate($year))->where('incoming_date','<=', getCurrentJalaliYearEndDate($year));
		}
		else
		{
			$pending_incoming_nums->where('incoming_date', '>=', getCurrentJalaliYearStartDate())->where('incoming_date','<=', getCurrentJalaliYearEndDate());
		}
		$pending_incoming_nums->where('doc_status', 0)->where('deleted',0);

		$records = $pending_incoming_nums->get();
		if(count($records) > 0)
		{
			$options = "";
			foreach ($records as $items) {
				if($items->incoming_number != 0 && $items->execution_type != 0 && $items->execution_date != "0000-00-00" && getAssignee($items->id) != "" && getAssigneeDescription($items->id) != "")
				{
					$options .= "<option ".($items->incoming_number == $selected_item ? 'selected':'')." value='".$items->id."'>".$items->incoming_number."</option>";	
				}
			}
			return $options;
		}
		else return $options = "";
	}

	// get search result based on sent field values.
	public static function getSearchIncomingDocuments($print="")
	{
		$doc_type = Input::get('doc_type');
		$doc_number = Input::get('doc_number');
		$doc_date = toGregorian(ymd_format(Input::get('doc_date')));
		$sender = Input::get('sender');
		$incoming_number = Input::get('incoming_number');
		$incoming_date = toGregorian(ymd_format(Input::get('incoming_date')));
		$subject = Input::get('subject');
		$number_of_papers = Input::get('number_of_papers');
		$assignee = Input::get('assignee');
		$doc_status = Input::get('doc_status');
		$operations = Input::get('operations');
		$selected_year = Input::get('year');

		if(Input::get('number_of_records_per_page') != "")
			$number_of_records_per_page = Input::get('number_of_records_per_page');
		else
			$number_of_records_per_page = 10;

		$table = DB::connection('document_tracking')->table('incoming_docs as incd')->select('incd.*','m.name_dr as ministries');
		$table->leftjoin('ministries as m','m.id','=','incd.sender');

		if(!isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs') && !isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$table->rightjoin('assignees as ass','ass.incoming_doc_id','=','incd.id');
			$table->leftjoin('hr.employees as emp','emp.id','=','ass.assignee_id');
			$table->where('ass.assignee_id', Auth::user()->employee_id);
			$table->orderBy('ass.assignee_description','asc');
		}

		if($doc_type != "")
		{
			$table->where('incd.doc_type','=',$doc_type);
		}
		if($doc_number != "")
		{
			$table->where('incd.doc_number','=',$doc_number);
		}
		if($doc_date != "")
		{
			$table->where('incd.doc_date','=',$doc_date);
		}
		if($sender != "")
		{
			$table->where('incd.sender',$sender);
		}
		if($incoming_number != "")
		{
			$table->where('incd.incoming_number',$incoming_number);
		}
		if($incoming_date != "")
		{
			$table->where('incd.incoming_date',$incoming_date);
		}
		if($subject != "")
		{
			$table->where('incd.subject','like','%'.$subject.'%');
		}
		if($number_of_papers != "")
		{
			$table->where('incd.number_of_papers',$number_of_papers);
		}
		if($assignee != "")
		{
			$table->leftjoin('assignees as ass','incd.id','=','ass.incoming_doc_id');
			$table->where('ass.assignee_id',$assignee);
		}
		if($doc_status != "")
		{
			$table->where('incd.doc_status',$doc_status);
		}
		if($operations != "")
		{
			$table->leftjoin('issued_docs as issd','incd.id','=','issd.incoming_doc_id');
			$table->where('issd.operations',$operations);
			$table->where('issd.deleted',0);
			$table->where('issd.incoming_number','!=',0);
			$table->where('issd.incoming_doc_id','!=',0);
			$table->groupBy('issd.incoming_doc_id');
		}

		if($selected_year != "")
		{	
			$table->where('incd.incoming_date','>=',getCurrentJalaliYearStartDate($selected_year));
			$table->where('incd.incoming_date','<=',getCurrentJalaliYearEndDate($selected_year));
		}
		else
		{
			$table->where('incd.incoming_date','>=',getCurrentJalaliYearStartDate());
			$table->where('incd.incoming_date','<=',getCurrentJalaliYearEndDate());
		}

		
        $table->where('incd.deleted',0);
        // Filter it based on user department ex: M&E or Followup 
        $table->where('incd.department_id',getUserDepartment(Auth::user()->id)->department); 
		$table->orderBy('incd.execution_date','asc');
		$table->orderBy('incd.incoming_number','asc');
		//dd($table->tosql());
		if($table->count()>0)
		{
			if($print == "")
				return $object = $table->paginate($number_of_records_per_page);
			else
				return $object = $table->get();
		}
		else
			return false;
	}

	//insert the document
	public static function addRecord($data)
	{
		//DB::enableQueryLog();
		$insertedID = DB::connection('document_tracking')->table('incoming_docs')->insertGetID($data);
		return $insertedID;
	}
	//update the document
	public static function updateIncomingDocument($data,$record_id)
	{
		$updated = DB::connection('document_tracking')->table('incoming_docs')->where('id',$record_id)->update($data);
		if($updated) return true;
		else return false;
	}
	//update incoming document assignees.
	public static function updateIncomingDocumentAssignee($incoming_doc_id)
	{
		// check if the information already exist. If not then insert the new one. If yes then delete the old ones.
		if(Input::get('assignee') != "")
		{
			// get the assignee_id based on incoming document id if exists.
			$existed_assignee_id = DB::connection('document_tracking')->table('assignees')->where('incoming_doc_id', $incoming_doc_id)->pluck('assignee_id');
			if($existed_assignee_id != "")
			{
				// if the assignee is a different person.
				if(Input::get('assignee') != $existed_assignee_id)
				{
					// delete the assignee based on incoming doc id if exists.
					$assignee_deleted = DB::connection('document_tracking')->table('assignees')->where('incoming_doc_id', $incoming_doc_id)->delete();
					if($assignee_deleted)
					{
						$inserted = DB::connection('document_tracking')->table('assignees')->insert(array('incoming_doc_id' => $incoming_doc_id, 'assignee_id' => Input::get('assignee')));
						if($inserted) return true;
						else return false;
					}
				}
			}
			else
			{
				$inserted = DB::connection('document_tracking')->table('assignees')->insert(array('incoming_doc_id' => $incoming_doc_id, 'assignee_id' => Input::get('assignee')));
				if($inserted) return true;
				else return false;
			}
		}
	}
	// update individual assignee's description in assignees table based on id.
	public static function update_assignee_description($assignee_description,$incoming_doc_id)
	{
		$updated = DB::connection('document_tracking')->table('assignees')->where('incoming_doc_id', $incoming_doc_id)->where('assignee_id', Auth::user()->employee_id)->update(array('assignee_description' => $assignee_description));
		if($updated){ return true;}
		else return false;
	}

	public static function getDocDetails($doc_id)
	{
		$doc_details = DB::connection('document_tracking')->table('incoming_docs')->select('*')->where('id', $doc_id)->first();
		if($doc_details != ""){ return $doc_details;}
		else{ return "";}
	}

	public static function deleteIncomingDocInfo($incoming_doc_id, $issued_doc_id)
	{
		$deleted = DB::connection('document_tracking')->table('incoming_docs')->where('id', $incoming_doc_id)->update(array('deleted' => 1));
		if($deleted)
		{
			if(!empty($issued_doc_id))
			{
				DB::connection('document_tracking')->table('issued_docs')->where('incoming_doc_id', $incoming_doc_id)->update(array('deleted' => 1));
			}
			return true;
		} 
		else return false;
	}

	// add department log details.
	public static function addLog($data)
	{
		$table = DB::connection('document_tracking')->table('log')->insert($data);
	}

	// ================================================== Document Issuing Model Functions ============================

	public static function getDocumentsForIssuing()
	{
		$table = DB::connection('document_tracking')->table('issued_docs as issd')->select('issd.*','m.name_dr as ministries');
		$table->leftjoin('incoming_docs as ind','ind.id','=','issd.incoming_doc_id');
		$table->leftjoin('ministries as m','m.id','=','issd.sent_to');

		if(!isMEDirector('document_tracking_issued_docs', 'm&e_director_issued_docs') && !isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$table->leftjoin('assignees as ass','ass.incoming_doc_id','=','issd.incoming_doc_id');
			$table->where('ass.assignee_id', Auth::user()->employee_id);
		}

		$table->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate());
		$table->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate());
		$table->where('issd.incoming_number','!=',0);
		$table->where('issd.incoming_doc_id','!=',0);
		$table->where('ind.deleted',0);
        $table->where('issd.deleted',0);
        $table->where('issd.department_id',getUserDepartment(Auth::user()->id)->department); 
		$table->groupBy('ind.id');
		$table->orderBy('issd.issuing_number','asc');
		if($table->count()>0){
			return $object = $table->paginate(10);
		}else{
			return "";
		}
	}

	public static function insertDocumentDetails($data)
	{
		$added = DB::connection('document_tracking')->table('issued_docs')->insertGetID($data);
		if($added) return $added;
		else return false;
	}

	public static function updateDocumentDetails($record_id,$data)
	{
		$updated = DB::connection('document_tracking')->table('issued_docs')->where('id', $record_id)->update($data);
		if($updated) return true;
		else return false;
	}
	//update issuing document assignees.
	public static function updateIssuedDocumentAssignee($issued_doc_id)
	{
		// if the issued document that does not have incoming document has assignee with it then remove it and insert the new one.
		if(Input::get('assignee') != "")
		{
			// get the assignee_id based on issued_doc_id if exists.
			$existed_assignee_id = DB::connection('document_tracking')->table('assignees')->where('issued_doc_id', $issued_doc_id)->pluck('assignee_id');
			if($existed_assignee_id != "")
			{
				// if the assignee is a different person.
				if(Input::get('assignee') != $existed_assignee_id)
				{
					// delete the assignee based on issued doc id if exists.
					$assignee_deleted = DB::connection('document_tracking')->table('assignees')->where('issued_doc_id', $issued_doc_id)->delete();
					if($assignee_deleted)
					{
						DB::connection('document_tracking')->table('assignees')->insert(array('issued_doc_id' => $issued_doc_id, 'assignee_id' => Input::get('assignee')));
					}
				}
			}
			else
			{
				DB::connection('document_tracking')->table('assignees')->insert(array('issued_doc_id' => $issued_doc_id, 'assignee_id' => Input::get('assignee')));
			}
		}
	}
	// get search result based on sent field values.
	public static function getSearchIssuedDocuments($print="")
	{
		$doc_type = Input::get('doc_type');
		$incoming_number = Input::get('incoming_number');
		$issuing_number = Input::get('issue_number');
		$issuing_date = toGregorian(ymd_format(Input::get('issue_date')));
		$third_copy = Input::get('third_copy');
		$sent_to = Input::get('sent_to');
		$subject = Input::get('subject');
		$number_of_papers = Input::get('number_of_papers');
		$related_carton = Input::get('related_carton');
		$operations = Input::get('operations');
		$assignee = Input::get('assignee');
		$selected_year = Input::get('year');

		if(Input::get('number_of_records_per_page') != "")
			$number_of_records_per_page = Input::get('number_of_records_per_page');
		else
			$number_of_records_per_page = 10;

		$table = DB::connection('document_tracking')->table('issued_docs as issd')->select('issd.*','m.name_dr as ministries');
		$table->leftjoin('incoming_docs as ind','ind.id','=','issd.incoming_doc_id');
		$table->leftjoin('ministries as m','m.id','=','issd.sent_to');

		if(!isMEDirector('document_tracking_issued_docs', 'm&e_director_issued_docs') && !isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$table->leftjoin('assignees as ass','ass.incoming_doc_id','=','issd.incoming_doc_id');
			$table->where('ass.assignee_id', Auth::user()->employee_id);
			$table->groupBy('issd.incoming_doc_id');
		}

		if($doc_type != "")
		{
			$table->where('ind.doc_type',$doc_type);
		}
		if($incoming_number != "")
		{
			$table->where('ind.incoming_number','=',$incoming_number);
		}
		if($issuing_number != "")
		{
			$table->where('issd.issuing_number','=',$issuing_number);
		}
		if($issuing_date != "")
		{
			$table->where('issd.issuing_date','=',$issuing_date);
		}
		if($third_copy != "")
		{
			$table->where('issd.third_copy','=',$third_copy);
		}
		if($sent_to != "")
		{
			$table->where('issd.sent_to',$sent_to);
		}
		if($subject != "")
		{
			$table->where('issd.subject','like','%'.$subject.'%');
		}
		if($number_of_papers != "")
		{
			$table->where('issd.number_of_papers',$number_of_papers);
		}
		if($related_carton != "")
		{
			$table->where('issd.related_carton',$related_carton);
		}
		if($assignee != "")
		{
			$table->leftjoin('assignees as ass','ass.incoming_doc_id','=','issd.incoming_doc_id');
			$table->where('ass.assignee_id',$assignee);
		}
		if($operations != "")
		{
			$table->where('issd.operations',$operations);
		}

		if($selected_year != "")
		{	
			$table->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate($selected_year));
			$table->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate($selected_year));
		}
		else
		{
			$table->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate());
			$table->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate());
		}
		$table->where('issd.incoming_number','!=',0);
		$table->where('issd.incoming_doc_id','!=',0);
		$table->where('issd.deleted',0);
        $table->where('ind.deleted',0);
        $table->where('issd.department_id',getUserDepartment(Auth::user()->id)->department); 
		$table->groupBy('ind.id');
		$table->orderBy('issd.issuing_number','asc');
		//dd($table->tosql());
		if($table->count()>0)
		{
			if($print == "")
				return $object = $table->paginate($number_of_records_per_page);
			else
				return $object = $table->get();
		}
		else
			return false;
	}

	public static function approveSavedDocs($saved_doc_id, $incoming_doc_id)
	{
		$approved = DB::connection('document_tracking')->table('issued_docs')->where('id', $saved_doc_id)->update(array('status' => 1));
		if($approved)
		{
			DB::connection('document_tracking')->table('incoming_docs')->where('id', $incoming_doc_id)->update(array('doc_status' => 1));
			return true;
		}
		else return false;
	}

	public static function getIssudDocDetails($issued_doc_id)
	{
		$object = DB::connection('document_tracking')->table("issued_docs")->where("id", $issued_doc_id)->first();
		if($object){return $object;}
		else return false;
	}

	public static function getIssuedDocReferences($issued_doc_id)
	{
		$references = DB::connection('document_tracking')->table("copy_to_references")->where("issued_doc_id", $issued_doc_id)->get();
		if($references){return $references;}
		else return false;
	}

	public static function getSavedDocDetails($saved_doc_id)
	{
		$object = DB::connection('document_tracking')->table("issued_docs")->where("id", $saved_doc_id)->first();
		if($object){return $object;}
		else return false;
	}

	public static function getAssignee($incoming_doc_id)
	{
		$assignee = DB::connection('document_tracking')->table("assignees")->where("incoming_doc_id", $incoming_doc_id)->pluck('assignee_id');
		if($assignee){return $assignee;}
		else return false;
	}

	// get delete the specific photo based on id.
	public static function getDeletePhoto($record_id)
	{
		$deleted = DB::connection('document_tracking')->table('photo_details')->where('id', $record_id)->update(array('deleted' => 1));
		if($deleted){ return true; } else{ return false; }
	}
	//get file name based on sent parameters.
	public static function getFileName($file_id)
	{
		$object = DB::connection('document_tracking')->table("uploads")->where("id", $file_id)->pluck('file_name');
		if($object){return $object;}
		else return false;
	}
	//get file name based on sent parameters.
	public static function getDocFilesName($issued_doc_id)
	{
		$object = DB::connection('document_tracking')->table("uploads")->where("issued_doc_id", $issued_doc_id)->get();
		if($object){return $object;}
		else return false;
	}
	public static function deleteIssuedDocInfo($issued_doc_id)
	{
		$deleted = DB::connection('document_tracking')->table('issued_docs')->where('id', $issued_doc_id)->update(array('deleted' => 1));
		if($deleted)
		{
			$incoming_doc_id = getRecordFieldBasedOnID('issued_docs','incoming_doc_id',$issued_doc_id);
			// update the status of incoming document and change it to under process if the issued documents successfully deleted.
			DB::connection('document_tracking')->table('incoming_docs')->where('id', $incoming_doc_id)->update(array('doc_status' => 0));
			return true;
		} 
		else return false;
	}
	public static function removeDocFiles($issued_doc_id)
	{
		$deleted = DB::connection('document_tracking')->table('uploads')->where('issued_doc_id', $issued_doc_id)->update(array('deleted' => 1));
		if($deleted) return true; else return false;
	}
	public static function removeFile($file_id)
	{
		$deleted = DB::connection('document_tracking')->table("uploads")->where("id", $file_id)->delete();
		if($deleted){return $deleted;}
		else{return false;}	
	}
	// ================================================== Document Issuing without incoming Model Functions ============================

	public static function getDocumentsForIssuingWithoutIncoming()
	{
		$table = DB::connection('document_tracking')->table('issued_docs as issd')->select('issd.*','m.name_dr as ministries');
		$table->leftjoin('ministries as m','m.id','=','issd.sent_to');

		if(!isMEDirector('document_tracking_issued_docs', 'm&e_director_issued_docs') && !isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$table->leftjoin('assignees as ass','ass.issued_doc_id','=','issd.id');
			$table->where('ass.assignee_id', Auth::user()->employee_id);
			$table->groupBy('issd.id');
		}

		$table->where('issd.issuing_date','>=',getCurrentJalaliYearStartDate());
		$table->where('issd.issuing_date','<=',getCurrentJalaliYearEndDate());
		$table->where('issd.incoming_number',0);
		$table->where('issd.incoming_doc_id',0);
        $table->where('issd.deleted',0);
        // Filter Record based on user Department 
        $table->where('issd.department_id',\getUserDepartment(Auth::user()->id)->department); 
		$table->orderBy('issd.issuing_number','asc');
		if($table->count()>0){
			return $object = $table->paginate(10);
		}else{
			return "";
		}
	}
	// get search result based on sent field values.
	public static function getSearchIssuedDocumentsWithoutIncoming($print="")
	{
		$doc_type = Input::get('doc_type');
		$issuing_number = Input::get('issue_number');
		$issuing_date = toGregorian(ymd_format(Input::get('issue_date')));
		$third_copy = Input::get('third_copy');
		$sent_to = Input::get('sent_to');
		$subject = Input::get('subject');
		$number_of_papers = Input::get('number_of_papers');
		$related_carton = Input::get('related_carton');
		$assignee = Input::get('assignee');
		$selected_year = Input::get('year');

		if(Input::get('number_of_records_per_page') != "")
			$number_of_records_per_page = Input::get('number_of_records_per_page');
		else
			$number_of_records_per_page = 10;

		$table = DB::connection('document_tracking')->table('issued_docs as issd')->select('issd.*','m.name_dr as ministries');
		$table->leftjoin('ministries as m','m.id','=','issd.sent_to');

		if(!isMEDirector('document_tracking_issued_docs', 'm&e_director_issued_docs') && !isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$table->rightjoin('assignees as ass','ass.issued_doc_id','=','issd.id');
			$table->where('ass.assignee_id', Auth::user()->employee_id);
			$table->groupBy('issd.id');
		}

		if($doc_type != "")
		{
			$table->where('issued_doc_type',$doc_type);
		}
		if($issuing_number != "")
		{
			$table->where('issd.issuing_number','=',$issuing_number);
		}
		if($issuing_date != "")
		{
			$table->where('issd.issuing_date','=',$issuing_date);
		}
		if($third_copy != "")
		{
			$table->where('issd.third_copy','=',$third_copy);
		}
		if($sent_to != "")
		{
			$table->where('issd.sent_to',$sent_to);
		}
		if($subject != "")
		{
			$table->where('issd.subject','like','%'.$subject.'%');
		}
		if($number_of_papers != "")
		{
			$table->where('issd.number_of_papers',$number_of_papers);
		}
		if($related_carton != "")
		{
			$table->where('issd.related_carton',$related_carton);
		}
		if($assignee != "")
		{
			$table->leftjoin('assignees as ass','ass.issued_doc_id','=','issd.id');
			$table->where('ass.assignee_id',$assignee);
		}

		if($selected_year != "")
		{
			$table->where('issd.issuing_date','>=',getCurrentJalaliYearStartDate($selected_year));
			$table->where('issd.issuing_date','<=',getCurrentJalaliYearEndDate($selected_year));
		}
		else
		{
			$table->where('issd.issuing_date','>=',getCurrentJalaliYearStartDate());
			$table->where('issd.issuing_date','<=',getCurrentJalaliYearEndDate());
		}

		$table->where('issd.incoming_number',0);
		$table->where('issd.incoming_doc_id',0);
        $table->where('issd.deleted',0);
        // Filter Record based on the Department 
        $table->where('issd.department_id',getUserDepartment(Auth::user()->id)->department); 
		$table->orderBy('issd.issuing_number','asc');
		//dd($table->tosql());
		if($table->count()>0)
		{
			if($print == "")
				return $object = $table->paginate($number_of_records_per_page);
			else
				return $object = $table->get();
		}
		else
			return "";
	}

	// ================================================== Incoming Issuing Documents Advanced Search Model Functions ============================

	public static function getSearchIncomingIssuingDocuments($print="")
	{
		
		$doc_type = Input::get('doc_type');
		$doc_number = Input::get('doc_number');
		$doc_date = toGregorian(ymd_format(Input::get('doc_date')));
		$incoming_number = Input::get('incoming_number');
		$incoming_date = toGregorian(ymd_format(Input::get('incoming_date')));
		$doc_status = Input::get('doc_status');
		$issuing_number = Input::get('issue_number');
		$issuing_date = toGregorian(ymd_format(Input::get('issue_date')));
		$third_copy = Input::get('third_copy');
		$sender = Input::get('sender');
		$sent_to = Input::get('sent_to');
		$subject = Input::get('subject');
		$reference_id = Input::get('reference_copy');
		$number_of_papers = Input::get('number_of_papers');
		$related_carton = Input::get('related_carton');
		$assignee = Input::get('assignee');
		$operations = Input::get('operations');
		$selected_year = Input::get('year');

		if(Input::get('number_of_records_per_page') != "")
			$number_of_records_per_page = Input::get('number_of_records_per_page');
		else
			$number_of_records_per_page = 10;

		$table = DB::connection('document_tracking')->table('issued_docs as issd')->select('ind.*','ind.id as incoming_id','mInd.name_dr as incoming_sender','ind.sent_to as incoming_sent_to','ind.subject as incoming_subject','ind.number_of_papers as incoming_number_of_papers','issd.*','issd.id as issued_doc_id','m.name_dr as issuing_sent_to','issd.subject as issuing_subject','issd.number_of_papers as issuing_number_of_papers','issd.sender as issuing_sender');
		$table->leftjoin('incoming_docs as ind','ind.id','=','issd.incoming_doc_id');
		$table->leftjoin('ministries as mInd','mInd.id','=','ind.sender');
		$table->leftjoin('ministries as m','m.id','=','issd.sent_to');

		if(!isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs') && !isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$table->rightjoin('assignees as ass','ass.incoming_doc_id','=','ind.id');
			$table->leftjoin('hr.employees as emp','emp.id','=','ass.assignee_id');
			$table->where('ass.assignee_id', Auth::user()->employee_id);
		}

		if($doc_type != "")
		{
			$table->where('ind.doc_type','=',$doc_type);
		}
		if($doc_number != "")
		{
			$table->where('ind.doc_number','=',$doc_number);
		}
		if($doc_date != "")
		{
			$table->where('ind.doc_date','=',$doc_date);
		}
		if($incoming_number != "")
		{
			$table->where('ind.incoming_number','=',$incoming_number);
		}
		if($incoming_date != "")
		{
			$table->where('ind.incoming_date','=',$incoming_date);
		}
		if($doc_status != "")
		{
			$table->where('ind.doc_status','=',$doc_status);
		}
		if($issuing_number != "")
		{
			$table->where('issd.issuing_number','=',$issuing_number);
		}
		if($issuing_date != "")
		{
			$table->where('issd.issuing_date','=',$issuing_date);
		}
		if($third_copy != "")
		{
			$table->where('issd.third_copy','=',$third_copy);
		}
		if($sender != "")
		{
			$table->where('issd.sender',$sender);
			$table->orWhere('ind.sender',$sender);
		}
		if($sent_to != "")
		{
			$table->where('issd.sent_to',$sent_to);
			$table->orWhere('ind.sent_to',$sent_to);
		}
		if($subject != "")
		{
			$table->where('issd.subject','like','%'.$subject.'%');
		}
		if($reference_id != "")
		{
			$table->leftjoin('copy_to_references as ctr','issd.id','=','ctr.issued_doc_id');
			$table->where('ctr.reference_id',$reference_id);
		}
		if($number_of_papers != "")
		{
			$table->where('issd.number_of_papers',$number_of_papers);
		}
		if($related_carton != "")
		{
			$table->where('issd.related_carton',$related_carton);
		}
		if($assignee != "")
		{
			$table->leftjoin('assignees as ass','ind.id','=','ass.incoming_doc_id');
			$table->where('ass.assignee_id',$assignee);
		}
		if($operations != "")
		{
			$table->where('issd.operations',$operations);
		}

		if($selected_year != "")
		{
			$table->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate($selected_year));
			$table->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate($selected_year));
		}
		else
		{
			$table->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate());
			$table->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate());
		}
		$table->where('issd.incoming_number','!=',0);
		$table->where('issd.incoming_doc_id','!=',0);
		$table->where('ind.deleted',0);
        $table->where('issd.deleted',0);
        // Filter record based on department 
        $table->where('ind.department_id',\getUserDepartment(Auth::user()->id)->department);
        // $table->where('issd.department_id',\getUserDepartment(Auth::user()->id)->department);
		$table->groupBy('ind.id');
		$table->orderBy('ind.incoming_number','asc');
		//dd($table->tosql());
		if($table->count()>0)
		{
			if($print == "")
				return $object = $table->paginate($number_of_records_per_page);
			else
				return $object = $table->get();
		}
		else
			return "";

	}

// ====================================================================== M&E Documents Trakcing Dashboard Model functions =====================================================

	// get Document stats based on document type and whether it is under process, saved or issued.
	public static function getDocumentStats($total="",$hint="",$doc_type=0,$year="")
	{
		$stats = DB::connection('document_tracking')->table('incoming_docs as ind');
		
		if($total == "")
		{
			if($year != "")
			{
				$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate($year))->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate($year));
			}
			else
			{
				$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate())->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate());
			}
			$stats->where('ind.doc_type', $doc_type);
		}
		
		if(!isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs') && !isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$stats->rightjoin('assignees as ass','ass.incoming_doc_id','=','ind.id');
			$stats->leftjoin('hr.employees as emp','emp.id','=','ass.assignee_id');
			$stats->where('ass.assignee_id', Auth::user()->employee_id);
		}

		if($hint == "under_process")
		{
			if($year != "")
			{
				$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate($year))->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate($year));
			}
			else
			{
				$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate())->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate());
			}
			$stats->where('ind.doc_status', 0);
			$stats->where('ind.deleted', 0);
		}
		elseif($hint == "issued")
		{
			$stats->leftjoin('issued_docs as issd','ind.id','=','issd.incoming_doc_id');
			$stats->where('issd.operations', 2);
			if($year != "")
			{
				$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate($year))->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate($year));
			}
			else
			{
				$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate())->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate());
			}
			$stats->where('issd.incoming_number','!=', 0);
			$stats->where('issd.incoming_doc_id','!=', 0);
			$stats->where('issd.deleted', 0);
			$stats->where('ind.deleted', 0);
			$stats->groupBy('issd.incoming_doc_id');
		}
		elseif($hint == "saved")
		{
			$stats->leftjoin('issued_docs as issd','ind.id','=','issd.incoming_doc_id');
			if($year != "")
			{
				// if the document is intended to be saved , then based on the created at compare the dates.
				if($hint == "saved")
					$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate($year))->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate($year));
				else
					$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate($year))->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate($year));
			}
			else
			{
				// if the document is intended to be saved , then based on the created at compare the dates.
				if($hint == "saved")
					$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate())->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate());
				else
					$stats->where('issd.issuing_date','>=',getCurrentJalaliYearStartDate())->where('issd.issuing_date','<=',getCurrentJalaliYearEndDate());
			}
			$stats->where('issd.incoming_number','!=', 0);
			$stats->where('issd.incoming_doc_id','!=', 0);
			$stats->where('issd.operations', 1);
			$stats->where('issd.deleted', 0);
			$stats->where('ind.deleted', 0);
			$stats->groupBy('issd.incoming_doc_id');
		}
		else
		{
			if($year != "")
			{
				$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate($year))->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate($year));
			}
			else
			{
				$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate())->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate());
			}
			$stats->where('ind.deleted', 0);
        }
        // Filter Record Based on User Department 
        $stats->where('ind.department_id',\getUserDepartment(Auth::user()->id)->department); 

		$total = count($stats->get());
		if($total > 0){ return $total;}
		else 
			return 0;
	}
	// get issued document stats which does not have incoming.
	public static function getDocumentStatsWithoutIncoming($issued_doc_type=0,$year="")
	{
		if($issued_doc_type == 0)
		{
			$stats = DB::connection('document_tracking')->table('issued_docs as issd')->where('issd.incoming_number', 0);
			if($year != "")
			{
				$stats->where('issd.incoming_doc_id', 0)->where('issd.issuing_date','>=',getCurrentJalaliYearStartDate($year))->where('issd.issuing_date','<=',getCurrentJalaliYearEndDate($year));
			}
			else
			{
				$stats->where('issd.incoming_doc_id', 0)->where('issd.issuing_date','>=',getCurrentJalaliYearStartDate())->where('issd.issuing_date','<=',getCurrentJalaliYearEndDate());
			}
		}
		else
		{
			$stats = DB::connection('document_tracking')->table('issued_docs as issd')->where('issd.issued_doc_type', $issued_doc_type)->where('issd.incoming_number', 0);
			if($year != "")
			{
				$stats->where('issd.incoming_doc_id', 0)->where('issd.issuing_date','>=',getCurrentJalaliYearStartDate($year))->where('issd.issuing_date','<=',getCurrentJalaliYearEndDate($year));
			}
			else
			{
				$stats->where('issd.incoming_doc_id', 0)->where('issd.issuing_date','>=',getCurrentJalaliYearStartDate())->where('issd.issuing_date','<=',getCurrentJalaliYearEndDate());
			}
		}
		$stats->where('issd.deleted', 0);
		if(!isMEDirector('document_tracking_issued_docs', 'm&e_director_issued_docs') && !isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$stats->rightjoin('assignees as ass','ass.issued_doc_id','=','issd.id');
			$stats->where('ass.assignee_id', Auth::user()->employee_id);
        }
        // Filter Record based on the department type 
        $stats->where('issd.department_id',\getUserDepartment(Auth::user()->id)->department); 

		$stats->get();

		if($stats->count() > 0)
			return $stats->count();
		else
			return 0;
	}

	// get the document stats based on execution type.
	public static function getDocumentExecutionTypeStats($execution_type,$year="")
	{
		$stats = DB::connection('document_tracking')->table('incoming_docs as ind')->where('ind.execution_type', $execution_type);
		if($year != "")
		{
			$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate($year))->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate($year));
		}
		else
		{
			$stats->where('ind.incoming_date','>=',getCurrentJalaliYearStartDate())->where('ind.incoming_date','<=',getCurrentJalaliYearEndDate());
		}

		if(!isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs') && !isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
		{
			// list the incoming documents based on specific M&E experts.
			$stats->rightjoin('assignees as ass','ass.incoming_doc_id','=','ind.id');
			$stats->leftjoin('hr.employees as emp','emp.id','=','ass.assignee_id');
			$stats->where('ass.assignee_id', Auth::user()->employee_id);
        }
        //Filter record based user department ID 
		$stats->where('ind.department_id',\getUserDepartment(Auth::user()->id)->department);
		$stats->where('ind.deleted', 0)->get();

		if($stats->count() > 0)
			return $stats->count();
		else
			return 0;
	}

}
