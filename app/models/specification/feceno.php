<?php namespace App\models\specification;
use Illuminate\specification\specificationenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Input;
use get;


class feceno extends Model {

	protected $table ='specification.feceno';
	public $timestamps = false;

	public static function get_feceno(){

		$rows=\DB::table('specification.feceno as fe')->select('fe.*','dept.name as dept_name')->leftjoin('specification.department AS dept','fe.department_id','=','dept.id')->orderby('fe.id','desc')->where('fe.fe_status','0')->whereBetween('fe.date',[getLatestDate('from'),getLatestDate('to')])->get();
		return $rows;

	}
	public static function get_feceno_reject(){

		$rows=\DB::table('specification.feceno as fe')->select('fe.*','dept.name as dept_name')->leftjoin('specification.department AS dept','fe.department_id','=','dept.id')->orderby('fe.id','desc')->where('fe.fe_status','1')->whereBetween('fe.date',[getLatestDate('from'),getLatestDate('to')])->get();

		return $rows;

	}
	public static function select_product(){

	$data=\DB::table('specification.products as prdt')->select('prdt.*','dept.name','f9.number')
	->leftjoin('specification.feceno as f9','prdt.feceno_id','=','f9.id')->leftjoin('specification.department as dept','prdt.department_id','=','dept.id')->orderby('prdt.id','desc')->where('prdt.status','0')->whereBetween('prdt.date',[getLatestDate('from'),getLatestDate('to')])->get();
	return $data;
}
public static function approve_products(){

	$data1=\DB::table('specification.products as prdt')->select('prdt.*','dept.name','f9.number')
	->leftjoin('specification.feceno as f9','prdt.feceno_id','=','f9.id')->leftjoin('specification.department as dept','prdt.department_id','=','dept.id')->orderby('prdt.id','desc')->where('prdt.status','1')->whereBetween('prdt.date',[getLatestDate('from'),getLatestDate('to')])->get();
	return $data1;

}
public static function reject_products(){

	$data1=\DB::table('specification.products as prdt')->select('prdt.*','dept.name','f9.number')
	->leftjoin('specification.feceno as f9','prdt.feceno_id','=','f9.id')->leftjoin('specification.department as dept','prdt.department_id','=','dept.id')->orderby('prdt.id','desc')->where('prdt.status','2')->whereBetween('prdt.date',[getLatestDate('from'),getLatestDate('to')])->get();
	return $data1;

}
}
