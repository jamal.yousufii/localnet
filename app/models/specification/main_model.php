<?php namespace App\models\specification;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Input;
use get;


class main_model extends Model {

		protected $table ='specification.equipments';
		
		//get id of equipment to insert record at employes equipment
		public static function get_equipment(){

			$rows=\DB::connection('specification')->table('equipments','categories')
			->select('equipments.*','categories.name')
			->leftjoin('categories','equipments.categories_id','=','categories.id')
			->whereRaw('used= 0 AND status=1')->orderby('id','desc')->get();
			//->where('used','=','0')->orwhere('status','=','0')->get();
			 return $rows;
		}
			

		//to update distribut product we mast select product
		public static function get_equipment_dest(){

			$rows=\DB::connection('specification')->table('equipments','categories')
			->select('equipments.*','categories.name')
			->leftjoin('categories','equipments.categories_id','=','categories.id')
			//->where('used','=','1')->orwhere('status','=','0')->get();
			->whereRaw('used= 1 or status=1')->orderby('id','desc')->get();
			 return $rows;
		}

		public static function get_employee(){

			$rows=\DB::connection('hr')->table('employees')->select('id',DB::raw("concat(`name_dr`,' - ',`father_name_dr`) as name"))->orderby('id','desc')->get();
			return $rows;
		}
		//product that are releted to person now
		public static function get_all_distribute_equipment(){
			$rows=\DB::table('hr.employees as emp')
			->select('emp.name_dr AS em_name','emp.father_name_dr AS last_name','dept.name AS dep_name','cat.*','equip.*','emp_equip.id AS em_eq_id')
			->leftjoin('specification.department as dept','dept.id','=','emp.department')
			->leftjoin('specification.employes_equipment as emp_equip','emp_equip.employes_id','=','emp.id')
			->leftjoin('specification.equipments as equip','emp_equip.equipment_id','=','equip.id')
			->leftjoin('specification.categories as cat','equip.categories_id','=','cat.id')
			->whereRaw('emp_equip.status= 0 AND emp_equip.delete_status=0')->orderby('emp_equip.id','desc')

			//->where('employes_equipment.status','=','0')

			
			->get(); 
			return $rows;
	
		}
		//product that are not releted to person
		public static function get_all_distribute_equipment_not_releted(){
			$rows=\DB::table('hr.employees as emp')
			->select('emp.name_dr AS em_name','emp.father_name_dr AS last_name','dept.name AS dep_name','cat.*','equip.*','emp_equip.id AS em_eq_id')
			->leftjoin('specification.department as dept','dept.id','=','emp.department')
			->leftjoin('specification.employes_equipment as emp_equip','emp_equip.employes_id','=','emp.id')
			->leftjoin('specification.equipments as equip','emp_equip.equipment_id','=','equip.id')
			->leftjoin('specification.categories as cat','equip.categories_id','=','cat.id')
			->whereRaw('emp_equip.status=1 AND emp_equip.delete_status=0')->orderby('emp_equip.id','desc')

			
			->get(); 
			return $rows;
	
		}
		//delete product equipment function
		public static function get_all_distribute_equipment_delete(){
			$rows=\DB::table('hr.employees as emp')
			->select('emp.name_dr AS em_name','emp.father_name_dr AS last_name','dept.name AS dep_name','cat.*','equip.*','emp_equip.id AS em_eq_id')
			->leftjoin('specification.department as dept','dept.id','=','emp.department')
			->leftjoin('specification.employes_equipment as emp_equip','emp_equip.employes_id','=','emp.id')
			->leftjoin('specification.equipments as equip','emp_equip.equipment_id','=','equip.id')
			->leftjoin('specification.categories as cat','equip.categories_id','=','cat.id')
			->whereRaw('emp_equip.status=1 AND emp_equip.delete_status=1')->orderby('emp_equip.id','desc')

			
			->get(); 
			return $rows;
	
		}
		public static function list_of_all_equipment(){

			$rows=\DB::connection('specification')->table('equipments','categories.name')->select('equipments.*','categories.name')->leftjoin('categories','equipments.categories_id','=','categories.id')->orderby('id','desc')->get();
			return $rows;
		}
			public static function list_of_all_categories(){

			$rows=\DB::connection('specification')->table('categories')->orderby('id','desc')->get();
			return $rows;
		}
}

