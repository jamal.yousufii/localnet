<?php namespace App\models\specification;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Input;
use get;


class department extends Model {		

		public static function get_department(){

			$rows=\DB::connection('specification')->table('department')->select('id','name')->get();
			return $rows;
		}
}
