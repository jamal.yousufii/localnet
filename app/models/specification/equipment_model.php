<?php namespace App\models\specification;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Input;
use get;


class equipment_model extends Model {

		protected $table ='specification.equipments';
		

		public static function get_categories(){

			$rows=\DB::connection('specification')->table('categories')->select('name','id')->orderby('id','desc')->get();
			return $rows;
		}
}
