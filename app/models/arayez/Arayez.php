<?php
namespace arayez;
use Eloquent;
use DB;
class Arayez extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'department';

	public static function getAll()
	{
		    
		$table = DB::table('department');
        //$table->leftJoin('users', 'department.id', '=', 'users.department_id');
      	//$table->groupBy('users.department_id');
        //$table->select('department.*', DB::raw('count(users.id) AS total_user'));
        //$table->orderBy('parent');
        //$table->where('parent',0);
        $object = $table->get();

        return $object;
	}
}
