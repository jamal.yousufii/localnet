<?php

namespace App\models\hr;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;
use DateTime;
use DateInterval;
use DatePeriod;
class HrOperation extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
    */
	protected $connection 	= 'hr';
	public static $myDb 	= "hr";
	protected $table 		= 'employees';

	public static function getData($lang = 'dr',$user_dep=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS fullname'),
							//DB::raw('CONCAT(t1.name_dr," ",t1.last_name) AS fullname'),
							//'t1.name_dr AS fullname',
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							DB::raw(
								'CASE t1.gender
								    WHEN "M" THEN "Male"
								    WHEN "F" THEN "Female"
								    ELSE " "
								  END AS gender'
								),
							'pro.name_'.$lang.' AS original_province',
							DB::raw('IF(t1.employee_type != 3,er.name_'.$lang.',mi.name_'.$lang.') AS rank'),
							DB::raw('IF(t1.employee_type != 3,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast'),
							//'er.name_'.$lang.' AS rank',
							//'er1.name_'.$lang.' AS bast',
							't1.current_position_dr',
							'dep.name AS department',
							't1.appointment_date_current_position AS emp_date',
							DB::raw('IF(t1.card_issued != "0000-00-00",t1.card_issued,"NOT ISSUED") AS card_issued')
							);
					$table->where('t1.fired',0);
					$table->where('t1.resigned',0);
					$table->where('t1.vacant','!=',1);//kambod
					$table->where('t1.vacant','!=',4);//ezafa bast
					$table->where('t1.position_dr','!=',4);//entezar mahsh
					$table->where('t1.changed',0);//not changed to out
					$table->where('t1.tashkil_id','!=',0);//tanqis tashkilati
					$table->where('t1.retired',0);//retire
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.military_rank AS mi','mi.id','=','t1.emp_rank');
		$table->leftjoin('auth.military_rank AS mi1','mi1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		if($user_dep != 0)
		{
			//$table->whereRaw('(t1.general_department IN ('.$user_dep.') OR t1.department IN ('.$user_dep.'))');
			$table->whereIn('t1.department',$user_dep);
		}
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getRecruitmentSearchData($user_dep=0,$dep_id=0,$sub_dep=0,$bast=0,$type=0,$doc=0,$typee=0,$gender=0,$edu=0,$year=0,$name='')
	{
		$lang = 'dr';
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS fullname'),
							//DB::raw('CONCAT(t1.name_dr," ",t1.last_name) AS fullname'),
							//'t1.name_dr AS fullname',
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							DB::raw(
								'CASE t1.gender
								    WHEN "M" THEN "Male"
								    WHEN "F" THEN "Female"
								    ELSE " "
								  END AS gender'
								),
							//'pro.name_'.$lang.' AS original_province',
							'er.name_'.$lang.' AS rank',
							'er1.name_'.$lang.' AS bast',
							't1.current_position_dr',
							'dep.name AS department',
							't1.phone',
							't1.appointment_date_current_position AS emp_date',
							't1.changed','t1.fired','t1.resigned','t1.retired','t1.position_dr','t1.tashkil_id'
							);
					if($gender==1)
					{
						$table->where('t1.gender','M');
					}
					elseif($gender==2)
					{
						$table->where('t1.gender','F');
					}
					if($typee == 1)
					{
						$table->where('t1.tashkil_id',0);
						$table->where('t1.fired',0);
						$table->where('t1.resigned',0);
						$table->where('t1.changed',0);
						$table->where('t1.retired',0);
						$table->where('t1.position_dr','!=',4);
					}
					elseif($typee == 2)
					{//changed to out
						$table->where('t1.changed',1);
					}
					elseif($typee == 3)
					{//salary wait
						$table->where('t1.position_dr',4);
					}
					elseif($typee == 4)
					{//salary wait
						$table->where('t1.tashkil_id','!=',0);
					}
					elseif($typee == 5)
					{//students
						$table->leftjoin('att_shifts AS s','s.employee_id','=','t1.id');
						$table->whereIN('s.status',[0,1,2]);
						$table->orderBy('s.id','desc');
						$table->groupBy('s.employee_id');
					}
					elseif($typee == 6)
					{//fired
						$table->where('t1.fired',1);
					}
					elseif($typee == 7)
					{//resign
						$table->where('t1.resigned',1);
					}
					elseif($typee == 8)
					{//retired
						$table->where('t1.retired',1);
					}
					if($dep_id != 0)
					{
						if($sub_dep != 0)
						{
							$table->where('t1.department',$sub_dep);
						}
						else
						{
							$table->where('t1.general_department',$dep_id);
						}
					}
					if($bast!=0)
					{
						$table->where('t.bast',$bast);
					}
					if($type!=0)
					{
						$table->where('t.employee_type',$type);
					}
					if($doc!=0)
					{
						$table->leftjoin('attachments AS att','att.doc_id','=','t1.id');
						$table->groupBy('t1.id');
						$table->havingRaw('count(att.id) < 5');
					}
					if($edu!=0)
					{
						$table->leftjoin('employee_educations AS edu','edu.employee_id','=','t1.id');
						$table->where('edu.education_id',$edu);
					}
					if($year!=0)
					{
						$table->where('t1.birth_year',$year);
					}
					if($name!='')
					{
						$table->where('t1.name_dr', 'like', '%'.$name.'%');
					}
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		if($type != 0 || $bast != 0)
		{
			$table->leftjoin('tashkilat AS t','t.id','=','t1.tashkil_id');
		}
		if($user_dep != 0)
		{
			//$table->whereRaw('(t1.general_department IN ('.$user_dep.') OR t1.department IN ('.$user_dep.'))');
			$table->whereIn('t1.department',$user_dep);
		}
		$object = $table->orderBy('t1.id');
		//echo $object->tosql();exit;
		return $object->get();
	}
	public static function getEmployeesForPrint($dep_id=0,$sub_dep=0,$bast=0,$type=0,$doc=0,$typee=0,$gender=0,$edu=0,$year=0,$name='')
	{
		$lang = 'dr';
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS fullname'),
							't1.father_name_dr AS father_name',
							'degree.name_'.$lang.' AS edu_degree',
							'edu.education_field',
							't1.birth_year',
							DB::raw('IF(t1.employee_type = 2,ar.name_'.$lang.',er.name_'.$lang.') AS rank'),
							DB::raw('IF(t1.employee_type = 2,ar1.name_'.$lang.',er1.name_'.$lang.') AS bast'),
							
							't1.current_position_dr',
							'dep.name AS department',
							'gdep.name AS gdepartment',
							't1.appointment_date_current_position AS emp_date',
							't1.employee_type',
							't1.mawqif_employee',
							't1.changed','t1.fired','t1.resigned','t1.retired','t1.position_dr','t1.tashkil_id'
							);
					if($gender==1)
					{
						$table->where('t1.gender','M');
					}
					elseif($gender==2)
					{
						$table->where('t1.gender','F');
					}
					if($typee == 1)
					{
						$table->where('t1.tashkil_id',0);
						$table->where('t1.fired',0);
						$table->where('t1.resigned',0);
						$table->where('t1.changed',0);
						$table->where('t1.retired',0);
						$table->where('t1.position_dr','!=',4);
					}
					elseif($typee == 2)
					{//changed to out
						$table->where('t1.changed',1);
					}
					elseif($typee == 3)
					{//salary wait
						$table->where('t1.position_dr',4);
					}
					elseif($typee == 4)
					{//salary wait
						$table->where('t1.tashkil_id','!=',0);
					}
					elseif($typee == 5)
					{//students
						$table->leftjoin('att_shifts AS s','s.employee_id','=','t1.id');
						$table->whereIN('s.status',[0,1,2]);
						$table->orderBy('s.id','desc');
						$table->groupBy('s.employee_id');
					}
					elseif($typee == 6)
					{//fired
						$table->where('t1.fired',1);
					}
					elseif($typee == 7)
					{//resign
						$table->where('t1.resigned',1);
					}
					elseif($typee == 8)
					{//retired
						$table->where('t1.retired',1);
					}
					
					if($dep_id != 0)
					{
						if($sub_dep != 0)
						{
							$table->where('t1.department',$sub_dep);
						}
						else
						{
							$table->where('t1.general_department',$sub_dep);
						}
					}
					if($bast!=0)
					{
						$table->where('t.bast',$bast);
					}
					if($type!=0)
					{
						$table->where('t.employee_type',$type);
					}
					if($doc!=0)
					{
						$table->leftjoin('attachments AS att','att.doc_id','=','t1.id');
						$table->groupBy('t1.id');
						$table->havingRaw('count(att.id) < 5');
					}
					if($edu!=0)
					{
						//$table->leftjoin('employee_educations AS edu','edu.employee_id','=','t1.id');
						$table->where('edu.education_id',$edu);
					}
					if($year!=0)
					{
						$table->where('t1.birth_year',$year);
					}
					if($name!='')
					{
						$table->where('t1.name_dr', 'like', '%'.$name.'%');
					}
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.employee_rank AS ar','ar.id','=','t1.ageer_rank');
		$table->leftjoin('auth.employee_rank AS ar1','ar1.id','=','t1.ageer_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		$table->leftjoin('auth.department AS gdep','gdep.id','=','t1.general_department');
		$table->leftjoin('employee_educations AS edu','edu.employee_id','=','t1.id');
		$table->leftjoin('auth.education_degree AS degree','degree.id','=','edu.education_id');
		if($type != 0 || $bast != 0)
		{
			$table->leftjoin('tashkilat AS t','t.id','=','t1.tashkil_id');
		}
		$object = $table->orderBy('t1.id');
		//echo $object->tosql();exit;
		return $object->get();
	}
	public static function getFiredData($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							//'t1.name_dr AS name',
							't1.number_tayenat',
							't1.fire_no',
							//'pro.name_'.$lang.' AS original_province',
							'er1.name_'.$lang.' AS bast',
							't1.current_position_dr',
							'dep.name AS department',
							't1.fire_date AS emp_date'
							);
					$table->where('t1.fired',1);
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getResignData($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							't1.resign_no',
							//'pro.name_'.$lang.' AS original_province',
							'er1.name_'.$lang.' AS bast',
							't1.current_position_dr',
							'dep.name AS department',
							't1.resign_date AS emp_date'
							);
					$table->where('t1.resigned',1);
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getRetireData($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							't1.retire_no',
							//'pro.name_'.$lang.' AS original_province',
							'er1.name_'.$lang.' AS bast',
							't1.current_position_dr',
							'dep.name AS department',
							't1.retire_date AS emp_date'
							);
					$table->where('t1.retired',1);
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getExtraBastData($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							DB::raw(
								'CASE t1.employee_type
								    WHEN 3 THEN mr.name_'.$lang.'
								    WHEN 2 THEN aj.name_'.$lang.'
								    ELSE er.name_'.$lang.'
								  END AS bast'
							),
							//'pro.name_'.$lang.' AS original_province',
							't1.current_position_dr',
							'dep.name AS department'
							);
					//$table->where('t1.vacant',4);
					//$table->where('t1.changed',0);
					$table->where('t1.mawqif_employee',2);
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_bast');
		$table->leftjoin('auth.ajir_ranks AS aj','aj.id','=','t1.ageer_bast');
		$table->leftjoin('auth.military_rank AS mr','mr.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getTanqistData($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							DB::raw(
								'CASE t1.employee_type
								    WHEN 3 THEN mr.name_'.$lang.'
								    WHEN 2 THEN aj.name_'.$lang.'
								    ELSE er.name_'.$lang.'
								  END AS bast'
							),
							//'pro.name_'.$lang.' AS original_province',
							't1.current_position_dr',
							'dep.name AS department'
							);
					$table->where('t1.tashkil_id',0);
					$table->where('t1.fired',0);
					$table->where('t1.resigned',0);
					$table->where('t1.changed',0);
					$table->where('t1.retired',0);
					$table->where('t1.mawqif_employee','!=',2);//not in salary wait
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_bast');
		$table->leftjoin('auth.ajir_ranks AS aj','aj.id','=','t1.ageer_bast');
		$table->leftjoin('auth.military_rank AS mr','mr.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getContractsData($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							't1.current_position_dr',
							'dep.name AS department'
							);
					$table->where('t1.employee_type',5);
					$table->orWhere('t1.employee_type',4);
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		//$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_bast');
		//$table->leftjoin('auth.ajir_ranks AS aj','aj.id','=','t1.ageer_bast');
		//$table->leftjoin('auth.military_rank AS mr','mr.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getChangedData($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1')->join('hr.employee_changes AS changed','changed.employee_id','=','t1.id');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.number_tayenat',
							't1.father_name_dr',
							'pro.name_'.$lang.' AS original_province',
							'er.name_'.$lang.' AS bast',
							'er1.name_'.$lang.' AS rank',
							't1.current_position_dr',
							//'dep.name AS department',
							't1.appointment_date_current_position AS emp_date'
							);
					$table->where('t1.changed',1);
					//$table->where('t1.resigned',0);
					
					//$table->where('changed.employee_id','=','t1.id');
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		//$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('hr.employee_changes AS changed','changed.employee_id','=','t1.id');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getServiceData($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1')->join('hr.employee_services AS s','s.employee_id','=','t1.id');
					$table->select(
							't1.id',
							't1.name_dr AS name',
							't1.number_tayenat',
							DB::raw(
								'CASE t1.gender
								    WHEN "M" THEN "Male"
								    WHEN "F" THEN "Female"
								    ELSE " "
								  END AS gender'
								),
							'pro.name_'.$lang.' AS original_province',
							'er.name_'.$lang.' AS bast',
							'er1.name_'.$lang.' AS rank',
							't1.current_position_dr',
							'dep.name AS department',
							't1.appointment_date_current_position AS emp_date',
							DB::raw('IF(t1.card_issued != "0000-00-00",t1.card_issued,"NOT ISSUED") AS card_issued')
							);
					$table->where('t1.fired',0);
					//$table->where('changed.employee_id','=','t1.id');
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('hr.employee_changes AS changed','changed.employee_id','=','t1.id');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getDataCard($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.employee_type',
							DB::raw('CONCAT(t1.name_dr," ",t1.last_name) AS name'),
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							DB::raw('IF(t1.gender != "M","Female","Male") AS gender'),
							'pro.name_'.$lang.' AS original_province',
							'er.name_'.$lang.' AS bast',
							'er1.name_'.$lang.' AS rank',
							't1.appointment_date_current_position AS emp_date',
							DB::raw('IF(t1.card_issued != "0000-00-00",t1.card_issued,"NOT ISSUED") AS card_issued')
							);
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->where(function ($query) {
                $query->where('t1.tashkil_id','!=',0)
                      ->orWhere('t1.employee_type',4)
                      ->orWhere('t1.employee_type',5);
            });
		//$table->where('t1.tashkil_id','!=',0);
		
		if(canUpdateCard('hr_cards'))
		{
			$table->whereIn('ready_to_print',array(1,0));
		}
		elseif(canPrintCard('hr_cards'))
		{
			$table->where('ready_to_print',1);
			//$table->where('RFID','');
			$table->where('RFID',NULL);
		}
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}

	public static function getEmployeeDetails($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employees AS t1')
				->select('t1.*','co.name_dr as e_location','t2.name_en AS b_group','t.tainat','t.dep_id','t.sub_dep_id as tashkil_sub_dep_id','t1.department as sub_dep_id',
						'p.name_dr as original_pro','cp.name_dr as current_pro','d.name AS dep_name','ed.name_dr as edu_degree','t1.emp_bast as old_bast','t_old.sub_dep_id as tashkil_sub_dep_id_old',
						DB::raw('IF(t1.employee_type != 3,er.name_dr,mi.name_dr) AS emprank'),
						DB::raw('IF(t1.employee_type != 3,er1.name_dr,mi1.name_dr) AS bast'))
				->leftjoin('auth.blood_group AS t2','t2.id','=','t1.blood_group')
				->leftjoin('tashkilat AS t','t.id','=','t1.tashkil_id')
				->leftjoin('tashkilat AS t_old','t_old.id','=','t1.tashkil_id_old')
				->leftjoin('employee_educations AS edu','edu.employee_id','=','t1.id')
				->leftjoin('auth.countries AS co','co.id','=','edu.edu_location')
				->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank')
				->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast')
				->leftjoin('auth.military_rank AS mi','mi.id','=','t1.emp_rank')
				->leftjoin('auth.military_rank AS mi1','mi1.id','=','t.bast')
				//->leftjoin('auth.employee_rank AS e','e.id','=','t.bast')
				->leftjoin('auth.provinces AS p','p.id','=','t1.original_province')
				->leftjoin('auth.provinces AS cp','cp.id','=','t1.current_province')
				->leftjoin('auth.department AS d','d.id','=','t1.department')
				->leftjoin('auth.education_degree AS ed','ed.id','=','edu.education_id')
				->where('t1.id',$id)
				->first();
	}
	public static function getDocuments($id=0)
	{
		return DB::connection(self::$myDb)
				->table('attachments')
				->where('doc_id',$id)
				->get();
	}

	public static function getDepartmentEmployees()
	{
		$object = DB::connection(self::$myDb)
				->table('employees AS t1')
				->select(
							't1.name_dr',
							't1.current_position_dr'
						);

			if(Input::get('general_department') != '' && Input::get('sub_dep')=='')
			{
				$object->where('t1.general_department',Input::get('general_department'));
			}
			elseif(Input::get('general_department') != '' && Input::get('sub_dep')!='')
			{
				$object->where('t1.department',Input::get('sub_dep'));
			}

			if(Input::get('show_vacant') != '1')
			{
				$object->whereNotIn('t1.vacant',array(1));
			}

			//return object
			return $object->get();
	}
	public static function insertRecord($table="",$data=array())
	{
		DB::connection(self::$myDb)->table($table)->insert($data);
	}
	public static function insertRecord_id($table="",$data=array())
	{
		return DB::connection(self::$myDb)->table($table)->insertGetId($data);
	}
	public static function delete_record($table='',$where)
	{
		DB::connection(self::$myDb)->table($table)->where($where)->delete();
	}
	public static function update_record($table='',$data,$where)
	{
		DB::connection(self::$myDb)->table($table)->where($where)->update($data);
	}
	public static function delete_holidays($code=0)
	{
		DB::connection(self::$myDb)->table('month_holiday_dates')->where('code',$code)->where('month_holidays_id','!=',0)->delete();
	}
	public static function delete_EmergencyHolidays($code=0)
	{
		DB::connection(self::$myDb)->table('month_holiday_dates')->where('code',$code)->where('emergency_id','!=',0)->delete();
	}
	public static function getEmployeeChanges($id=0,$lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employee_changes AS t1');
					$table->select(
							't1.id',
							't1.employee_type',
							//'dep.name as general_department',
							'dep1.name as department',
							'min.name_dr as ministry',
							't1.position_title',
							't1.position_dr',
							'er.name_'.$lang.' AS bast',
							'er1.name_'.$lang.' AS rank',							
							't1.changed_date',
							't1.change_type'
							
							);
					//$table->where('t1.fired',0);
					$table->where('t1.employee_id','=',$id);
		$table->leftjoin('auth.ministries AS min','min.id','=','t1.ministry_id');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_bast');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_rank');
		//$table->leftjoin('auth.department AS dep','dep.id','=','t1.general_department');
		$table->leftjoin('auth.department AS dep1','dep1.id','=','t1.department');
		//$table->leftjoin('hr.employee_changes AS changed','changed.employee_id','=','t1.id');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeLastChange($id=0,$lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employee_changes AS t1');
					$table->select(
							't1.id',
							't1.employee_type',
							//'dep.name as general_department',
							'dep1.name as department',
							'min.name_dr as ministry',
							't1.position_title',
							't1.position_dr',
							'er.name_'.$lang.' AS bast',
							'er1.name_'.$lang.' AS rank',							
							't1.changed_date',
							't1.change_type'
							
							);
					//$table->where('t1.fired',0);
					$table->where('t1.employee_id','=',$id);
		$table->leftjoin('auth.ministries AS min','min.id','=','t1.ministry_id');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_bast');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_rank');
		//$table->leftjoin('auth.department AS dep','dep.id','=','t1.general_department');
		$table->leftjoin('auth.department AS dep1','dep1.id','=','t1.department');
		//$table->leftjoin('hr.employee_changes AS changed','changed.employee_id','=','t1.id');
		
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->first();
	}
	public static function getEmployeeServices($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employee_services AS t1');
					$table->select(
							't1.id',
							't1.position_title',
							'dep.name AS dep_name',
							't1.service_date'
							
							);
					//$table->where('t1.fired',0);
					$table->where('t1.employee_id','=',$id);
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		//$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		//$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('hr.employee_changes AS changed','changed.employee_id','=','t1.id');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getDocumentsByType($id=0,$type='')
	{
		return DB::connection(self::$myDb)
				->table('attachments')
				->where('file_name','like',$type.'_'.$id.'.%')
				->first();
	}
	public static function getEmployeeExperiences($id=0,$type=1)
	{
		return DB::connection(self::$myDb)
				->table('employee_experiences')
				->where('employee_id',$id)
				->where('type',$type)
				->orderBy('date_from')
				->get();
	}
	public static function getEmployeeTrainings($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_trainings')
				->where('employee_id',$id)
				->get();
	}
	public static function getAllEmployeesForAttendanceServer()
	{
		$start_from = Input::get('id_count');
		
		return DB::connection(self::$myDb)
				->table('employees')
				->select('RFID','name_en','id')
				->where('RFID','!=','')
				->where('id','>',$start_from)
				->orderBy('id')
				->get();
	}
	public static function getAllEmployeesForAttendance()
	{
		$start_from = Input::get('id_count');
		
		return DB::connection(self::$myDb)
				->table('employees')
				->select('RFID','name_en','id')
				->where('RFID','!=','')
				->where('id','>',$start_from)
				->orderBy('id')
				->get();
	}
	public static function getLastEmployeeId()
	{
		return DB::connection(self::$myDb)
				->table('employees')
				->select('id')
				->where('RFID','!=','')
				//->where('id','>=',$start_from)
				->orderBy('id','desc')
				->first();
	}
	public static function getLastPromotion($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_promotions AS t1')
				->select('t1.employee_id as id','t2.employee_type','t1.emp_rank','t1.emp_bast','t1.military_rank','t1.military_bast','t1.promotion_date AS last_date_tarfee','t1.qadam')
				->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				->where('employee_id',$id)
				->orderBy('id','desc')
				->first();
	}
	public static function getEmployeePromotions($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_promotions AS t1')
				->select('t2.employee_type as current_type','t1.*','rank.name_dr AS rank_name','to.name_dr AS to_rank','ajir_rank.name_dr AS rank_ajir','ajir_to.name_dr AS to_ajir','mi.name_dr AS rank_military','mi_to.name_dr AS to_military')
				->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				->leftjoin('auth.employee_rank AS rank','rank.id','=','t1.emp_rank')
				->leftjoin('auth.employee_rank AS to','to.id','=','t1.emp_bast')
				->leftjoin('auth.ajir_ranks AS ajir_rank','ajir_rank.id','=','t1.ajir_rank')
				->leftjoin('auth.ajir_ranks AS ajir_to','ajir_to.id','=','t1.ajir_bast')
				->leftjoin('auth.military_rank AS mi','mi.id','=','t1.military_rank')
				->leftjoin('auth.military_rank AS mi_to','mi_to.id','=','t1.military_bast')
				->where('t1.employee_id',$id)
				->orderBy('promotion_date')
				->get();
	}
	public static function getEmployeeEvaluations($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_evaluations AS t1')
				->select('t1.*')
				//->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				->where('t1.employee_id',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function getEmployeePunishments($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_punishments AS t1')
				->select('t1.*')
				//->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				->where('t1.employee_id',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function getEmployeeMakafat($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_makafats AS t1')
				->select('t1.*')
				//->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				->where('t1.employee_id',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function getEmployeeLangs($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_languages AS t1')
				->select('t1.*')
				//->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				->where('t1.employee_id',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function getEmployeeEducations($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_educations AS t1')
				->select('t1.*','ed.name_dr as degree_name','co.name_dr as location_name')
				->leftjoin('auth.education_degree AS ed','ed.id','=','t1.education_id')
				->leftjoin('auth.countries AS co','co.id','=','t1.edu_location')
				->where('t1.employee_id',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function getEmployeeGarantee($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_garantee AS t1')
				->select('t1.*')
				->where('t1.employee_id',$id)
				->get();
	}
	//communication
	public static function getComData($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS fullname'),
							't1.name_dr AS name',
							't1.name_en',
							't1.father_name_dr AS father_name',
							't1.current_position_dr',
							'dep.name AS department',
							DB::raw('IF(t1.card_issued != "0000-00-00",t1.card_issued,"NOT ISSUED") AS card_issued')
							);
					$table->where('t1.fired',0);
					$table->where('t1.resigned',0);
					$table->where('t1.vacant','!=',1);//kambod
					$table->where('t1.vacant','!=',4);//ezafa bast
					$table->where('t1.changed',0);//not changed to out
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		//$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		//$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getComCardData($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employee_cards AS c');
					$table->select(
							't1.id',
							't1.name_dr AS name',
							't1.name_en',
							't1.current_position_dr',
							'dep.name AS department',
							'c.id AS card_id',
							'c.card_no',
							'c.card_color',
							'c.issue_date',
							'c.expiry_date'
							);
					$table->where('t1.fired',0);
					$table->where('t1.resigned',0);
					$table->where('t1.vacant','!=',1);//kambod
					$table->where('t1.vacant','!=',4);//ezafa bast
					$table->where('t1.changed',0);//not changed to out
		$table->leftjoin('employees AS t1','c.employee_id','=','t1.id');
		//$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		//$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		
		$object = $table->orderBy('c.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getComMaktobData($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('com_maktobs AS t1');
					$table->select(
							't1.id',
							't1.number',
							't1.date',
							'dep.name AS source'
							);
				
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		//$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		//$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.sub_dep');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getComEstelamData()
	{
		$table = DB::connection(self::$myDb)
					->table('com_estelams AS t1');
					$table->select(
							't1.id',
							't1.number',
							't1.date',
							'emp.name_dr AS name'
							);
				
		$table->leftjoin('employees AS emp','emp.id','=','t1.employee_id');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getComSecurityData($type=0)
	{
		$table = DB::connection(self::$myDb)
					->table('com_securities AS t1');
					$table->select(
							't1.id',
							't1.number',
							't1.date',
							't1.file_name'
							);
		$table->where('t1.type',$type);		
		//$table->leftjoin('employees AS emp','emp.id','=','t1.employee_id');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getComComplaintsData()
	{
		$table = DB::connection(self::$myDb)
					->table('com_complaints AS t1');
					$table->select(
							't1.id',
							't1.reg_date',
							't1.desc'
							);
		//$table->leftjoin('employees AS emp','emp.id','=','t1.employee_id');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getComComitteData()
	{
		$table = DB::connection(self::$myDb)
					->table('com_comitteis AS t1');
					$table->select(
							't1.id',
							't1.date',
							't1.number',
							'c.desc'
							);
		$table->leftjoin('com_complaints AS c','c.id','=','t1.complain_id');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getRelatedEmployees($id=0,$sub=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.name_dr AS name',
							't1.last_name',
							't1.father_name_dr AS father_name',
							'dep.name AS department',
							't1.number_tayenat',
							't1.emp_bast as emp_rank',
							't1.current_position_dr AS position',
							't1.general_department',
							't1.department as dep_id'
							//DB::raw('count(att.RFID) as user_count')
							);
					
					if($id!=0)
					{
						if($sub!=0)
						{
							$table->where('t1.department',$sub);
						}
						else
						{
							$table->where('t1.general_department',$id);
						}
					}
					
					$table->where('t1.fired',0);
					$table->where('t1.resigned',0);
					$table->where('t1.vacant','!=',1);//kambod
					$table->where('t1.vacant','!=',4);//ezafa bast
					$table->where('t1.position_dr','!=',4);//entezar mahsh
					$table->where('t1.changed',0);//not changed to out
					$table->where('t1.tashkil_id','!=',0);//tanqis tashkilati
					$table->where('t1.employee_type','!=',2);//dont include ajirs
					$table->where('t1.retired',0);//retire
					//$table->where('t1.RFID','>',0);
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('attendance_images AS att','att.RFID','=','t1.RFID');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getRelatedEmployeesAjax($id=0,$sub=0,$per_page=10)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.tashkil_id',
							't1.name_dr AS name',
							't1.last_name',
							't1.father_name_dr AS father_name',
							'dep.name AS department',
							't1.number_tayenat',
							't1.emp_bast as emp_rank',
							't1.current_position_dr AS position',
							't1.general_department',
							't1.department as dep_id',
							't1.in_attendance',
							't1.changed','t1.fired','t1.resigned','t1.retired','t1.position_dr'
							//DB::raw('count(att.RFID) as user_count')
							);
					
					if($id!=0)
					{
						if($sub!=0)
						{
							$table->where('t1.department',$sub);
						}
						else
						{
							$table->where('t1.general_department',$id);
						}
					}
					
					//$table->where('t1.fired',0);
					//$table->where('t1.resigned',0);
					//$table->where('t1.vacant','!=',1);//kambod
					//$table->where('t1.vacant','!=',4);//ezafa bast
					//$table->where('t1.position_dr','!=',4);//entezar mahsh
					//$table->where('t1.changed',0);//not changed to out
					//$table->where('t1.tashkil_id','!=',0);//tanqis tashkilati
					$table->where('t1.employee_type','!=',2);//dont include ajirs
					//$table->where('t1.retired',0);//retire
					//$table->where('t1.RFID','>',0);
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('attendance_images AS att','att.RFID','=','t1.RFID');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		$object = $table->paginate($per_page);
		return $object;
	}
	public static function getRelatedEmployeesAjax_search($item='',$id=0,$sub=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.tashkil_id',
							't1.name_dr AS name',
							't1.last_name',
							't1.father_name_dr AS father_name',
							'dep.name AS department',
							't1.number_tayenat',
							't1.emp_bast as emp_rank',
							't1.current_position_dr AS position',
							't1.general_department',
							't1.department as dep_id',
							't1.in_attendance',
							't1.changed','t1.fired','t1.resigned','t1.retired','t1.position_dr'
							);
					if($id!=0)
					{
						if($sub!=0)
						{
							$table->where('t1.department',$sub);
						}
						else
						{
							$table->where('t1.general_department',$id);
						}
					}
					if($item!='')
					{
						$table->where('t1.name_dr','like','%'.$item.'%');
					}
					
					//$table->where('t1.fired',0);
					//$table->where('t1.resigned',0);
					//$table->where('t1.vacant','!=',1);//kambod
					//$table->where('t1.vacant','!=',4);//ezafa bast
					//$table->where('t1.position_dr','!=',4);//entezar mahsh
					//$table->where('t1.changed',0);//not changed to out
					///$table->where('t1.tashkil_id','!=',0);//tanqis tashkilati
					$table->where('t1.employee_type','!=',2);//dont include ajirs
					//$table->where('t1.retired',0);//retire
					//$table->where('t1.RFID','>',0);
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('attendance_images AS att','att.RFID','=','t1.RFID');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		//$object = $table->paginate($per_page);
		return $object->get();
	}
	public static function getEmployees_leave($id=0,$sub=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.name_dr AS name',
							't1.last_name',
							't1.father_name_dr AS father_name',
							'dep.name AS department',
							't1.number_tayenat',
							't1.emp_bast as emp_rank',
							't1.current_position_dr AS position',
							't1.general_department',
							't1.department as dep_id',
							't1.changed',
							't1.fired',
							't1.resigned',
							't1.retired',
							't1.position_dr'
							//DB::raw('count(att.RFID) as user_count')
							);
					
					if($id!=0)
					{
						if($sub!=0)
						{
							$table->where('t1.department',$sub);
						}
						else
						{
							$table->where('t1.general_department',$id);
						}
					}
					
					//$table->where('t1.fired',0);
					//$table->where('t1.resigned',0);
					//$table->where('t1.vacant','!=',1);//kambod
					//$table->where('t1.vacant','!=',4);//ezafa bast
					//$table->where('t1.position_dr','!=',4);//entezar mahsh
					//$table->where('t1.changed',0);//not changed to out
					//$table->where('t1.tashkil_id','!=',0);//tanqis tashkilati
					//$table->where('t1.employee_type','!=',2);//dont include ajirs
					//$table->where('t1.retired',0);//retire
					//$table->where('t1.RFID','>',0);
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('attendance_images AS att','att.RFID','=','t1.RFID');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeImages($rfid=0,$year,$month)
	{
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month==1)
		{
			$from = dateToMiladi($year-1,12,$sday);
		}
		else
		{
			$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
		}
		$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
		
		$begin = new DateTime($from);
		$end = new DateTime($to);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);
		$days = array();
		foreach($period As $day)
		{
			$the_day = $day->format( "Y-m-d" );
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_year = $period_det[0];
			
			//$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
			$days[] = $period_year.'-'.$period_month.'-'.$period_day;
		}
	
		//remove the last pipe line
		//$days = substr($days, 0, -1);
		return  DB::connection('hr')
				->table('attendance_images')
				->select('id','path','status','late_status')
				->where('RFID',$rfid)//check the rfid
				//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
				->whereIn('date',$days)
				//->groupBy("RFID")
				->get();
		
	}
	public static function getEmployeeImages_lost($rfid=0,$year,$month)
	{
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month==1)
		{
			$from = dateToMiladi($year-1,12,$sday);
		}
		else
		{
			$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
		}
		$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
		
		$begin = new DateTime($from);
		$end = new DateTime($to);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);
		$days = array();
		foreach($period As $day)
		{
			$the_day = $day->format( "Y-m-d" );
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_year = $period_det[0];
			$days[] = $period_year.'-'.$period_month.'-'.$period_day;
			//$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
		}
		
		//remove the last pipe line
		//$days = substr($days, 0, -1);
		return  DB::connection('hr')
				->table('attendance_images')
				->select('id','path','status','late_status')
				->whereIn('RFID',$rfid)//check the rfid
				//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
				->whereIn('date',$days)
				//->groupBy("RFID")
				->get();
		
	}
	public static function absentEmployeeImages($rfid=0,$year,$month)
	{
		$from = dateToMiladi($year,$month-1,15);//the start day of attendance month is 15 of prevouce month
		$to = dateToMiladi($year,$month,15);//the end day of attendance month is 15 of current month\
		
		$begin = new DateTime($from);
		$end = new DateTime($to);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);
		$days = array();
		foreach($period As $day)
		{
			$the_day = $day->format( "Y-m-d" );
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_year = $period_det[0];
			$days[] = $period_year.'-'.$period_month.'-'.$period_day;
			//$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
		}
		
		//remove the last pipe line
		//$days = substr($days, 0, -1);
		return  DB::connection('hr')
				->table('attendance_images')
				->select('id','path','status')
				->where('RFID',$rfid)//check the rfid
				->where('status',1)
				//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
				->whereIn('date',$days)
				->get();
		
	}
	public static function getPhotoByRfid($rfid=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees');
					$table->select(
							'photo','name_dr','last_name','id','current_position_dr','att_out_date','att_in_date'
							);
		$table->where('RFID',$rfid);
	
		//echo $object->tosql();
		return $table->first();
	}
	public static function getPhotoByNoRfid($rfid=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees');
					$table->select(
							'photo','name_dr','last_name','id','current_position_dr','att_out_date','att_in_date',
							'fired','retired','resigned','changed'
							);
		$table->where('in_attendance',$rfid);
	
		//echo $object->tosql();
		return $table->first();
	}
	public static function getNextEmployeeRfid($rfid,$type)
	{
		$table = DB::connection(self::$myDb)
					->table('employees');
					$table->select(
							'id','department'
							);
		$table->where('RFID',$rfid);
		//$table->where('tashkil_id','!=',0);
		$id = $table->first();
		
		$result = DB::connection(self::$myDb)->table('employees')->select('rfid')
						->where('department',$id->department)
						->where('fired',0)
						->where('resigned',0)
						->where('vacant','!=',1)//kambod
						->where('vacant','!=',4)//ezafa bast
						->where('position_dr','!=',4)//entezar mahsh
						->where('changed',0)//not changed to out
						->where('tashkil_id','!=',0)//tanqis tashkilati
						->where('employee_type','!=',2)//dont include ajirs
						->where('retired',0)//retire
						->where('RFID','>',0);
						
		if($type=='next')
		{
			$result->where('id','>',$id->id)->orderBy('id','asc');
		}
		else
		{
			$result->where('id','<',$id->id)->orderBy('id','desc');
		}
		return $result->first();
	}
	
	//tashkil
	public static function getTashkilData($year=0)
	{
		$lang = 'dr';
		$table = DB::connection(self::$myDb)
					->table('tashkilat AS t1');
					$table->select(
							't1.id',
							DB::raw('(CASE WHEN t1.employee_type = 3 THEN mr.name_'.$lang.' ELSE er.name_'.$lang.' END) AS bast'),
							'dep.name AS department',
							'subdep.name AS sub_dep',
							't1.title',
							't1.tainat',
							't1.year','t1.status'
							);
		$table->where('t1.year',$year);
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.bast');
		$table->leftjoin('auth.military_rank AS mr','mr.id','=','t1.bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.dep_id');
		$table->leftjoin('auth.department AS subdep','subdep.id','=','t1.sub_dep_id');
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getTashkilDetail($id=0,$lang='dr')
	{
		$table = DB::connection(self::$myDb)
					->table('tashkilat AS t1');
					$table->select(
							't1.id',
							DB::raw('(CASE WHEN t1.employee_type = 3 THEN mr.name_'.$lang.' ELSE er.name_'.$lang.' END) AS bast_name'),
							't1.title',
							't1.employee_type',
							't1.bast',
							't1.dep_id',
							't1.sub_dep_id',
							't1.tainat',
							't1.job_desc',
							't1.year'
							);
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.bast');
		$table->leftjoin('auth.military_rank AS mr','mr.id','=','t1.bast');
		$object = $table->where('t1.id',$id);
		
		return $object->first();
	}
	public static function getCapacityTrainingsData()
	{
		$table = DB::connection(self::$myDb)
					->table('capacity_trainings AS t1');
					$table->select(
							't1.id',
							't1.title',
							't1.location',
							't1.start_date',
							't1.end_date',
							't1.seat_no',
							DB::raw('IF(t1.type != "0","خارجی","داخلی") AS type')
							);
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getCapacityTrainersData()
	{
		$table = DB::connection(self::$myDb)
					->table('capacity_trainers AS t1');
					$table->select(
							't1.*'
							);
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeCapacityTrainings($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('capacity_employee_trainings AS t1');
					$table->select(
							't1.id',
							'tr.title',
							'tr.location',
							'tr.start_date',
							'tr.end_date',
							DB::raw('IF(tr.type != "0","خارجی","داخلی") AS type')
							);
		$table->where('t1.employee_id',$id);
		$table->leftjoin('capacity_trainings AS tr','tr.id','=','t1.training_id');
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getTraining($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('capacity_trainings AS t1');
		$table->where('t1.id',$id);
		//$table->leftjoin('capacity_trainings AS tr','tr.id','=','t1.training_id');
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->first();
	}
	public static function getEmployeeSalary($emp_id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employee_salary')
					->where('employee_id',$emp_id);
		return $table->first();
	}
	public static function getEmployeePayroll($id=0,$lang='dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.name_dr AS name',
							't1.father_name_dr AS father_name',
							's.main_salary',
							's.extra_salary',
							's.prof_salary',
							's.kadri_salary',
							's.retirment',
							't1.current_position_dr'
							);
					$table->where('t1.fired',0);
					$table->where('t1.resigned',0);
					$table->where('t1.vacant','!=',1);//kambod
					$table->where('t1.vacant','!=',4);//ezafa bast
					$table->where('t1.position_dr','!=',4);//entezar mahsh
					$table->where('t1.changed',0);//not changed to out
					$table->where('t1.tashkil_id','!=',0);//tanqis tashkilati
					$table->where('t1.employee_type','!=',2);//dont include ajirs
					$table->where('t1.retired',0);//retire
					
					$table->where('t1.department',$id);
					
		$table->leftjoin('employee_salary AS s','s.employee_id','=','t1.id');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeForAttPrint($id=0,$lang='dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.employee_type',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							DB::raw('IF(t1.employee_type != 3,er.name_'.$lang.',mi.name_'.$lang.') AS rank'),
							DB::raw('IF(t1.employee_type != 3,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast'),
							't1.current_position_dr'
							);
					
					
					
					$table->where('position_dr','!=',4);//entezar mahsh
					$table->where('changed',0);//not changed to out
					//->where('employee_type','!=',2)//dont include ajirs
					$table->where('retired',0);//retire
					//->where('RFID','>',0);
					$table->where('t1.fired',0);
					$table->where('t1.resigned',0);
					$table->where('t1.vacant','!=',1);//kambod
					$table->where('t1.vacant','!=',4);//ezafa bast
					$table->where('t1.tashkil_id','!=',0);//new tashkil
					//$table->where('t1.changed',0);//not changed to out
					$table->where('t1.department',$id);
					
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.military_rank AS mi','mi.id','=','t1.emp_rank');
		$table->leftjoin('auth.military_rank AS mi1','mi1.id','=','t1.emp_bast');
		
		$object = $table->orderBy('t1.tashkil_id','asc');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getRelatedBasts($id=0,$type=0)
	{
		$year = date('Y')-621;
		$table = DB::connection(self::$myDb)
					->table('tashkilat AS t1');
					$table->select(
							't1.id',
							't1.title as name',
							't1.tainat'
							);
					
					if($type == 0)
					{
						$table->where('t1.sub_dep_id',$id);
						$table->where('t1.status',0);
					}
					else
					{
						$table->whereRaw('t1.sub_dep_id = '.$id.' AND (t1.status = 0 || t1.id = '.$type.')');
					}
					$table->where('t1.year',$year);
		$object = $table->orderBy('t1.id');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getExistBasts($id=0,$type=0)
	{
		$year = date('Y')-621;
		$table = DB::connection(self::$myDb)
					->table('tashkilat AS t1');
					$table->select(
							't1.id',
							't1.title as name',
							't1.tainat'
							);
					$table->where('t1.sub_dep_id',$id);
					$table->where('t1.status',1);
					//$table->where('t1.year',$year);
		$object = $table->orderBy('t1.id');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeLeaves($id=0,$year=0,$month=0)
	{
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month!=0)
		{
			if($month == 1)
			{
				$from = dateToMiladi($year-1,12,$sday);
			}
			else
			{
				$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
			}
			$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
		}
		else
		{
			$from = dateToMiladi($year,1,1);
			if(isItLeapYear($year))
			{
				$to = dateToMiladi($year,12,30);
			}
			else
			{
				$to = dateToMiladi($year,12,29);
			}
		}
	
		$table = DB::connection(self::$myDb)
					->table('leaves')
					->where('employee_id',$id)
					->whereRaw('((date_from >= "'.$from.'" AND date_from <= "'.$to.'") OR (date_to >= "'.$from.'" AND date_to <= "'.$to.'"))');
		$object = $table->orderBy('created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeLeaves_total($type=0,$id=0,$date=0)
	{
		$table = DB::connection(self::$myDb)
					//->select('SUM(days_no) as days')
					->table('leaves')
					->where('employee_id',$id)
					->where('type',$type);
		if($date !=0)
		{
			$sdate = dateToMiladi($date,01,01);
			if(isItLeapYear($date))
			{
				$edate = dateToMiladi($date,12,30);
			}
			else
			{
				$edate = dateToMiladi($date,12,29);
			}
			$table->where('date_from','>=',$sdate);
			$table->where('date_to','<=',$edate);
		}
		$object = $table->groupBy('employee_id');
		//echo $object->tosql();
		return $object->sum('days_no');
	}
	public static function getLeaveDetails($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('leaves')
					->where('id',$id);
		//echo $object->tosql();
		return $table->first();
	}
	//custom query
	public static function test($year,$month)
	{
		if($month == 1)
		{
			$from = dateToMiladi($year-1,12,15);
		}
		else
		{
			$from = dateToMiladi($year,$month-1,15);//the start day of attendance month is 15 of prevouce month
		}
		//$from = dateToMiladi($year,$month-1,15);//the start day of attendance month is 15 of prevouce month
		$to = dateToMiladi($year,$month,15);//the end day of attendance month is 15 of current month\
		
		$begin = new DateTime($from);
		$end = new DateTime($to);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);
		$days = '';
		foreach($period As $day)
		{
			$the_day = $day->format( "Y-m-d" );
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_year = $period_det[0];
			
			$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
		}
		
		//remove the last pipe line
		$days = substr($days, 0, -1);
		$imgs= DB::connection('hr')
				->table('attendance_images as t1')
				->select('t1.id','t1.path','t1.status','t1.RFID')
				//->where('RFID',$rfid)//check the rfid
				->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
			
				->get();
		$images = array();
		foreach($imgs AS $img)
// /2785872450008/2015/1212/124443_8426042.jpg		
		{
			if (array_key_exists($img->RFID.'_'.substr($img->path,20,4).'_1',$images)){
				$images[$img->RFID.'_'.substr($img->path,20,4).'_2']	=$img;
			}else {
				$images[$img->RFID.'_'.substr($img->path,20,4).'_1']	=$img;
			}
			//$images[]	= array('uid'=>$img->RFID.'_'.substr($img->path,20,4),'path'=>$img->path,'RFID'=>$img->RFID);
		}
		
		return $images;
	}


	// public static function test($year,$month)
	// {
	// 	$from = dateToMiladi($year,$month-1,15);//the start day of attendance month is 15 of prevouce month
	// 	$to = dateToMiladi($year,$month,15);//the end day of attendance month is 15 of current month\
		
	// 	$begin = new DateTime($from);
	// 	$end = new DateTime($to);
	// 	$interval = DateInterval::createFromDateString('1 day');
	// 	$period = new DatePeriod($begin, $interval, $end);
	// 	$days = '';
	// 	foreach($period As $day)
	// 	{
	// 		$the_day = $day->format( "Y-m-d" );
	// 		$period_det = explode('-',$the_day);
	// 		$period_day = $period_det[2];
	// 		$period_month = $period_det[1];
	// 		$period_year = $period_det[0];
			
	// 		$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
	// 	}
		
	// 	//remove the last pipe line
	// 	$days = substr($days, 0, -1);
	// 	return  DB::connection('hr')
	// 			->table('attendance_images as t1')
	// 			->select('t1.id','t1.path','t1.status','t1.RFID')
	// 			//->where('RFID',$rfid)//check the rfid
	// 			->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
			
	// 			->get();
		
	// }
	public static function getAllRFIDs($dep,$sub_dep)
	{
		$result= DB::connection('hr')
				->table('employees as t1')
				->select('t1.id','t1.name_dr AS name_dr','father_name_dr','t1.RFID','dep.name','t1.current_position_dr')
				->leftjoin('auth.department AS dep','dep.id','=','t1.department')
				->where('t1.RFID','!=','')
				->where('t1.fired',0)
				->where('t1.resigned',0)
				->where('t1.vacant','!=',1)//kambod
				->where('t1.vacant','!=',4)//ezafa bast
				->where('t1.tashkil_id','!=',0);//new tashkilat
				if($dep!=0)
				{
					if($sub_dep!=0)
					{
						$result->where('t1.department',$sub_dep);
					}
					else
					{
						$result->where('t1.general_department',$dep);
					}
				}
		return $result->get();
	}
	public static function getEmployeePromotionDetail($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_promotions AS t1')
				->select('t2.employee_type as current_type','t1.*')
				->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				
				->where('t1.id',$id)
				//->orderBy('id','desc')
				->first();
	}
	public static function getEmployeeMakafatDetail($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_makafats AS t1')
				->select('t1.*')
				//->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				
				->where('t1.id',$id)
				//->orderBy('id','desc')
				->first();
	}
	public static function getEmployeePunishDetail($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_punishments AS t1')
				->select('t1.*')
				//->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				
				->where('t1.id',$id)
				//->orderBy('id','desc')
				->first();
	}
	public static function getEmployeePresented($id=0)
	{
		return DB::connection(self::$myDb)
				->table('attendance_presents AS t1')
				->select('t1.*')
				->where('t1.RFID',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function getEmployeePresented_lost($id=0)
	{
		return DB::connection(self::$myDb)
				->table('attendance_presents AS t1')
				->select('t1.*')
				->whereIn('t1.RFID',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function insertAttImages($data=array())
	{
		// Start transaction
		DB::beginTransaction();
	
		$acct = DB::connection(self::$myDb)->table('attendance_images')->insert($data);
		
		if( !$acct )
		{
		    DB::rollback();
		    return false;
		} else {
		    // Else commit the queries
		    DB::commit();
		    return true;
		}
	}
	public static function getEmployeeSalaryWait($id=0,$year=0,$month=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_salary_waits AS t1')
				->select('t1.*')
				->where('t1.employee_id',$id)
				->where('t1.year',$year)
				->where('t1.month',$month)
				//->orderBy('id','desc')
				->first();
	}
	public static function check_tashkil($id=0,$sub=0,$tainat=0)
	{
		return DB::connection(self::$myDb)
				->table('tashkilat AS t1')
				->where('t1.dep_id',$id)
				->where('t1.sub_dep_id',$sub)
				->where('t1.tainat',$tainat)
				//->orderBy('id','desc')
				->first();
	}
	public static function check_employee_add($name='',$fname='',$gname='',$dep_id=0,$sub=0)
	{
		return DB::connection(self::$myDb)
				->table('employees AS t1')
				->where('t1.general_department',$dep_id)
				->where('t1.department',$sub)
				->where('t1.name_dr',$name)
				->where('t1.father_name_dr',$fname)
				->where('t1.grand_father_name',$gname)
				//->orderBy('id','desc')
				->first();
	}
	public static function getEmployeesTodayAtt($id=0,$sub=0,$status=0,$type=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.name_dr AS name',
							't1.father_name_dr AS father_name',
							'dep.name AS department',
							't1.number_tayenat',
							't1.current_position_dr',
							't1.emp_bast as emp_rank',
							DB::raw('IF(att.RFID != "0","حاضر","غیرحاضر") AS status')
							//DB::raw('count(att.RFID) as user_count')
							);
					if($status==1)
					{//presents
						$table->where('att.RFID','!=',0);
					}
					elseif($status==2)
					{//absents
						$table->where('att.RFID',null);
					}
					if($type==2)
					{//ajir
						$table->where('t1.employee_type',2);
					}
					elseif($type==1)
					{
						$table->where('t1.employee_type','!=',2);
					}
					$table->where('t1.RFID','>',0);
					$table->where('t1.position_dr','!=',4);//entezar mahsh
					$table->where('t1.changed',0);//not changed to out
					$table->where('t1.retired',0);//retire
					
					$table->where('t1.fired',0);
					$table->where('t1.resigned',0);
					$table->where('t1.vacant','!=',1);//kambod
					$table->where('t1.vacant','!=',4);//ezafa bast
					$table->where('t1.tashkil_id','!=',0);//ezafa bast
					//$table->where('t1.changed',0);//not changed to out
					if($id!=0)
					{
						if($sub!=0)
						{
							$table->where('t1.department',$sub);
						}
						else
						{
							$table->where('t1.general_department',$id);
						}
					}
					
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		$table->leftjoin('attendance_images_today AS att','att.RFID','=','t1.RFID');
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	//estehqaq mash
	public static function getEmployeePayroll_details($emp_id=0,$year,$month)
	{
		return DB::connection(self::$myDb)
				->table('employee_payrolls AS t1')
				->where('t1.employee_id',$emp_id)
				->where('t1.year',$year)
				->where('t1.month',$month)
				//->orderBy('id','desc')
				->first();
	}
	public static function insertLog($table='',$action=0,$desc='',$id=0)
	{//action: 1-update,2-delete
		DB::connection(self::$myDb)->table('hr_logs')->insert(array('user_id'=>Auth::user()->id,'action'=>$action,'table'=>$table,'desc'=>$desc,'table_id'=>$id,'created_at'=>date('Y-m-d H:i:s')));
	}
	public static function getTashkilSearch($dep_id,$sub_dep,$type,$bast,$title,$emp_type,$year)
	{
		$lang = 'dr';
		$table = DB::connection(self::$myDb)
					->table('tashkilat AS t1');
					$table->select(
							't1.id',
							DB::raw('(CASE WHEN t1.employee_type = 3 THEN mr.name_'.$lang.' ELSE er.name_'.$lang.' END) AS bast'),
							'dep.name AS department',
							'subdep.name AS sub_dep',
							't1.title',
							't1.tainat',
							't1.year','t1.status'
							);
		if($type!=2)
		{
			$table->where('status',$type);
		}
		if($sub_dep!=0)
		{
			$table->where('t1.sub_dep_id',$sub_dep);
		}
		else
		{
			if($dep_id != 0)
			{
				$table->where('t1.dep_id',$dep_id);
			}
		}
		if($bast!=0)
		{
			$table->where('t1.bast',$bast);
		}
		
		if($title != 0)
		{	
			//$title = str_replace('-', ' ', $title);
			$table->where('t1.title','like',"%$title%");
		}
		
		if($emp_type!=0)
		{
			$table->where('t1.employee_type',$emp_type);
		}
		if($year!=0)
		{
			$table->where('t1.year',$year);
		}
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.bast');
		$table->leftjoin('auth.military_rank AS mr','mr.id','=','t1.bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.dep_id');
		$table->leftjoin('auth.department AS subdep','subdep.id','=','t1.sub_dep_id');
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();exit;
		return $object->get();
	}
	public static function getEmployeeRank($type=0,$emp_id=0)
	{
		$lang = 'dr';
		$table = DB::connection(self::$myDb)
			->table('employees AS t1');
			if($type!=3)
			{
				$table->select(
				'er.name_'.$lang.' AS rank'
				);	
			}
			else
			{
				$table->select(
				'mi.name_'.$lang.' as rank'
				);
			}
			
			$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank')
			->leftjoin('auth.military_rank AS mi','mi.id','=','t1.emp_rank')
			->where('t1.id',$emp_id);

			//->orderBy('id','desc')
			return $table->first();
	}
	public static function getEmployeeBast($id=0)
	{
		if($id != 0 )
		{
			$lang = 'dr';
			$table = DB::connection(self::$myDb)
			->table('tashkilat AS t1')
			->select('t1.bast')
			->where('t1.id',$id);
			return $table->first()->bast;
		}
		else
		{
			return null;
		}
	}
	public static function getEmployeeTashkil($t_id=0)
	{
		$lang = 'dr';
		$table = DB::connection(self::$myDb)
			->table('tashkilat AS t1')
			->select(DB::raw('(CASE WHEN t1.employee_type = 3 THEN mr.name_'.$lang.' ELSE er.name_'.$lang.' END) AS bast'),
					'title')
			->leftjoin('auth.employee_rank AS er','er.id','=','t1.bast')
			->leftjoin('auth.military_rank AS mr','mr.id','=','t1.bast')
			->where('t1.id',$t_id);

			//->orderBy('id','desc')
			return $table->first();
	}
	public static function getDepName($dep_id=0)
	{
		return DB::connection(self::$myDb)
				->table('auth.department')
				->select('name')
				->where('id',$dep_id)
				->first();
	}
	public static function getDepEmployeeTotal($dep_id=0)
	{
		return DB::connection(self::$myDb)
				->table('tashkilat')
				->select(DB::raw('count(id) As total'))
				->where('sub_dep_id',$dep_id)
				->where('status',1)
				->where('employee_type','!=',2)
				->groupBy('sub_dep_id')
				->first()->total;
	}
	
	public static function getMonthHolidays($year,$month)
	{
		return DB::connection(self::$myDb)
				->table('month_holidays AS t1')
				->select('t1.days','t2.date','t2.desc')
				->leftjoin('month_holiday_dates AS t2','t2.month_holidays_id','=','t1.id')
				->where('t1.year',$year)
				->where('t1.month',$month)
				->get();
	}
	public static function getMonthHolidayDays($year,$month)
	{
		return DB::connection(self::$myDb)
				->table('month_holidays AS t1')
				->select('t1.days')
				//->leftjoin('month_holiday_dates AS t2','t2.month_holidays_id','=','t1.id')
				->where('t1.year',$year)
				->where('t1.month',$month)
				->first();
	}
	public static function getMonthEmergencyHolidays($year,$month)
	{
		return DB::connection(self::$myDb)
				->table('month_holiday_dates AS t2')
				->select('t1.days','t2.date','t2.desc')
				->leftjoin('emergency_days AS t1','t2.emergency_id','=','t1.id')
				->where('t1.year',$year)
				->where('t1.month',$month)
				->get();
	}
	public static function getMonthEmergencyDays($year,$month)
	{
		return DB::connection(self::$myDb)
				->table('emergency_days AS t1')
				->select('t1.days')
				//->leftjoin('month_holiday_dates AS t2','t2.month_holidays_id','=','t1.id')
				->where('t1.year',$year)
				->where('t1.month',$month)
				->first();
	}
	public static function checkInsertImagesDate($date)
	{
		$date = explode('-',$date);
		$the_date = $date[0].'/'.$date[1].$date[2];
		$result= DB::connection(self::$myDb)
				->table('attendance_images AS t1')
				->select('t1.id')
				->where('t1.path', 'like', '%'.$the_date.'%')
				->get();
		if($result)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public static function getDegreesChart()
	{
		$result= DB::connection(self::$myDb)
				->table('employees as t1')
				->select(DB::raw('count(t1.id) As total'),'e.education_id as education_degree')
				->leftjoin('employee_educations AS e','e.employee_id','=','t1.id')
				->where('t1.tashkil_id','!=',0)
				->groupBy('e.education_id')
				->get();	
		return $result;
	}
	public static function getDegrees()
	{
		$result= DB::connection(self::$myDb)
				->table('auth.education_degree')
				->select('id','name_dr')
				->get();	
		return $result;
	}
	public static function getGenders()
	{
		$result= DB::connection(self::$myDb)
				->table('employees')
				->select(DB::raw('count(id) As total'),'gender')
				->where('tashkil_id','!=',0)
				->groupBy('gender')
				->get();	
		return $result;
	}
	public static function getEmployeeTypes()
	{
		$result= DB::connection(self::$myDb)
				->table('employees')
				->select(DB::raw('count(id) As total'),'employee_type')
				->where('tashkil_id','!=',0)
				->groupBy('employee_type')
				->get();	
		return $result;
	}
	public static function getEmployeeDep($dep_id=0)
	{
		$result= DB::connection(self::$myDb)
				->table('employees')
				->select(DB::raw('count(id) As total'))
				->where('fired',0)
				->where('resigned',0)
				->where('vacant','!=',1)//kambod
				->where('vacant','!=',4)//ezafa bast
				->where('position_dr','!=',4)//entezar mahsh
				->where('changed',0)//not changed to out
				->where('tashkil_id','!=',0)//tanqis tashkilati
				->where('employee_type','!=',2)//dont include ajirs
				->where('retired',0)//retire
				->where('RFID','!=',0)
				->where('department',$dep_id)
				->groupBy('department')
				->first();	
		return $result;
	}
	public static function getEmployeeGen_Dep($dep_id=0)
	{
		$result= DB::connection(self::$myDb)
				->table('employees')
				->select(DB::raw('count(id) As total'))
				->where('fired',0)
				->where('resigned',0)
				->where('vacant','!=',1)//kambod
				->where('vacant','!=',4)//ezafa bast
				->where('position_dr','!=',4)//entezar mahsh
				->where('changed',0)//not changed to out
				->where('tashkil_id','!=',0)//tanqis tashkilati
				->where('employee_type','!=',2)//dont include ajirs
				->where('retired',0)//retire
				->where('RFID','!=',0)
				->where('general_department',$dep_id)
				->groupBy('general_department')
				->first();	
		return $result;
	}
	public static function get_dep_name($dep_id=0)
	{
		$result= DB::connection(self::$myDb)
				->table('auth.department')
				->select('name')
				->where('id',$dep_id)
				->first();	
		return $result;
	}
	public static function getAllHolidays($from='',$to='')
	{
		$result= DB::connection(self::$myDb)
				->table('month_holiday_dates')
				->select('date')
				->whereBetween('date', array($from, $to))
				->get();	
		return json_decode(json_encode((array) $result), true);
	}
	public static function get_todays_absent()
	{
		$result= DB::connection(self::$myDb)
				->table('employees AS e')
				->select('e.id','e.email','e.name_dr as name','e.last_name','e.department')
				//->where('e.department',78)
				->where('e.RFID','>',0)
				->where('e.tashkil_id','!=',0)
				->where('e.employee_type','!=',2)
				->whereNotIn('e.RFID', function($query){
				    $query->select('t1.RFID')
				    	  ->from('attendance_images_today as t1');
				})
				//->where('e.department',78)
				->get();	
		return $result;
	}
	public static function get_todays_absent_old($time='')
	{
		$result= DB::connection(self::$myDb)
				->table('attendance_images_today as t1')
				->select('e.email','e.name_dr as name','e.last_name','t1.time','e.department')
				->leftjoin('employees AS e','e.RFID','=','t1.RFID')
				->where('t1.time','>',$time)
				//->where('e.department',78)
				->get();	
		return $result;
	}
	public static function get_yesterdays_absent($the_date='')
	{
		$result= DB::connection(self::$myDb)
				->table('employees AS e')
				->select('e.id','e.email','e.name_dr as name','e.last_name','e.department')
				//->select('e.email','e.name_dr as name','e.last_name','e.department')
				//->where('e.department',78)
				->where('e.RFID','>',0)
				->where('e.tashkil_id','!=',0)
				->where('e.employee_type','!=',2)
				->whereNotIn('e.RFID', function($query) use ($the_date){
				    $query->select('t1.RFID')
				    	  ->from('attendance_images as t1')
				    	  ->where('date',$the_date)
				    	  ->where('time','>',120000);
				})
				//->where('e.department',78)
				->get();	
		return $result;
	}
	public static function getEmployees_inShift($dep=0,$sub=0,$status=0,$gender=0)
	{
		$table= DB::connection(self::$myDb)
				->table('employees as e')
				->select(DB::raw('IF(e.last_name != "",CONCAT(e.name_dr," ",e.last_name),e.name_dr) AS fullname'),
						'e.father_name_dr','e.department',
						'e.current_position_dr as position','d.name as dep_name',
						's.time_in','s.time_out','e.id')
				->leftjoin('att_shifts AS s','s.employee_id','=','e.id')
				->leftjoin('auth.department AS d','d.id','=','e.department');
				$table->where('e.in_shift','!=',0);
				if($dep!=0)
				{
					if($sub!=0)
					{
						$table->where('e.department',$sub);
					}
					else
					{
						$table->where('e.general_department',$dep);
					}
				}
				if($status!=0)
				{
					if($status==3)
					{//thus
						$table->where('s.status',0);
					}
					elseif($status==1)
					{//morning
						$table->where('s.time_in','>',0);
					}
					elseif($status==2)
					{//afternoon
						$table->where('s.time_out','>',0);
					}
				}
				else
				{
					$table->whereIn('s.status', [0,1,2]);
				}
				if($gender==1)
				{
					$table->where('e.gender','M');
				}
				elseif($gender==2)
				{
					$table->where('e.gender','F');
				}
				$table->orderBy('s.id','desc')
				->groupBy('s.employee_id');
		return $table->get();
	}
	public static function getEmployees_inAtt($dep=0,$sub=0,$gender=0)
	{
		$table= DB::connection(self::$myDb)
				->table('employees as e')
				->select(DB::raw('IF(e.last_name != "",CONCAT(e.name_dr," ",e.last_name),e.name_dr) AS fullname'),
						'e.father_name_dr','e.department',
						'e.current_position_dr as position','d.name as dep_name',
						'e.id')
				->leftjoin('auth.department AS d','d.id','=','e.department');
				if($dep!=0)
				{
					if($sub!=0)
					{
						$table->where('e.department',$sub);
					}
					else
					{
						$table->where('e.general_department',$dep);
					}
				}
				
				if($gender==1)
				{
					$table->where('e.gender','M');
				}
				elseif($gender==2)
				{
					$table->where('e.gender','F');
				}
				$table->where('e.RFID','>',0);
				$table->where('e.tashkil_id','>',0);
				$table->where('e.employee_type','!=',2);
				
		return $table->get();
	}
	public static function getEmployees_positions()
	{
		$result= DB::connection(self::$myDb)
				->table('employees as e')
				->select(DB::raw('IF(e.last_name != "",CONCAT(e.name_dr," ",e.last_name),e.name_dr) AS fullname'),
						'e.father_name_dr','e.department',
						'e.current_position_dr as position_dr','d.name as dep_name',
						'e.current_position_en as position_en',
						'e.id')
				//->leftjoin('att_shifts AS s','s.employee_id','=','e.id')
				->leftjoin('auth.department AS d','d.id','=','e.department')
				->where('e.tashkil_id','!=','')
				->orWhere('e.employee_type',4)
				->orWhere('e.employee_type',5)
				//->orderBy('s.id','desc')
				//->groupBy('s.employee_id')
				->get();	
		return $result;
	}
	//get employee details by id
	public static function getEmployeeById($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees');
					$table->select(
							'id','current_position_dr','current_position_en','RFID'
							);
		$table->where('id',$id);
	
		//echo $object->tosql();
		return $table->first();
	}
	public static function getEmployeeByUserId($userid=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees as e');
					$table->select(
							'e.id','e.RFID','e.department','e.general_department'
							);
		$table->leftjoin('auth.users AS u','u.employee_id','=','e.id');
		$table->where('u.id',$userid);
	
		//echo $object->tosql();
		return $table->first();
	}
	public static function getEmployeefiling($userid=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employee_filing');
		$table->where('employee_id',$userid);
	
		//echo $object->tosql();
		return $table->first();
	}
	public static function getEmployeeDet($empid=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees');
		$table->where('id',$empid);
	
		//echo $object->tosql();
		return $table->first();
	}
	public static function isTashkilInUse($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('tashkilat');
		$table->where('id',$id);
		$table->where('status',1);
	
		//echo $object->tosql();
		return $table->first();
	}
	/*
	public static function check_fname($name='',$fname='')
	{
		return DB::connection(self::$myDb)
				->table('employees')
				->select('id')
				->where('name_dr', 'like', '%'.$name.'%')
				->where('father_name_dr', 'like', '%'.$fname.'%')
				->first();
	}
	*/
}