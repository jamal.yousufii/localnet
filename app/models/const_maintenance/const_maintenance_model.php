<?php

namespace App\models\const_maintenance;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Session;
use Input;

class const_maintenance_model extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public static $myDb = "const_maintenance";

//================================================= Estate Registry Page Model Functions ======================================
	//get Records list.
	public static function getEstateRegistryData()
	{
		// get location details.
		$locations = DB::connection(self::$myDb)->table('locations')->select("*")->where('deleted', 0);
		if($locations->count()>0)
			return $object = $locations->paginate(10);
		else
			return "";
	}
	// get the Departures.
	public static function getSearchData()
	{
		$code = Input::get('code');
		$location = Input::get('location');
		$table = DB::connection(self::$myDb)->table('locations');
		if($code != "")
		{
			$table->where('code',$code);
		}
		if($location != "")
		{
			$table->where('location',$location);
		}
		if($code == "" || $location == "")
		{
			$table->where('deleted',0);
			if($table->count()>0)
			{
				return $object = $table->paginate(10);
			}
			else
				return "";
		}
		$table->where('deleted',0);
		if($table->count()>0)
		{
			return $object = $table->paginate(10);
		}
		else
			return "";
	}
	// insert the record to the send table as parameter.
	public static function addRecord($table,$data)
	{
		$insertedID = DB::connection(self::$myDb)->table($table)->insertGetId($data);
		if($insertedID){ return $insertedID;} else{ return false;}
	}
	// upload the files.
	public static function uploadFiles($data)
	{
		$uploaded = DB::connection(self::$myDb)->table('uploads')->insert($data);
		if($uploaded){return true;}else{return false;}
	}
	// get the uploaded file's name based on sent record_id.
	public static function getAttachments($record_id,$table,$field)
	{
		if($field == "")
		{
			$object = DB::connection(self::$myDb)->table($table)->select('id',$field)->where('location_id',$record_id)->where('deleted',0);
			if($object){return $object->get();}
			else{return false;}	
		}else{
			$object = DB::connection(self::$myDb)->table($table)->where('id', $record_id)->where('deleted',0)->pluck($field);
			//dd($object);
			if($object){return $object;}
			else{return false;}	
		}
	}
	public static function getDetails($id)
	{
		$object = DB::connection(self::$myDb)->table('locations')->where('id',$id)->where('deleted',0);
		return $object->first();
	}
	public static function getFiles($id)
	{
		$files = DB::connection(self::$myDb)->table('uploads')->where('location_id',$id)->where('deleted',0)->get();
		return $files;
	}
	//get Estate Registry details info based on id.
	public static function getSpecificRecordDetails($id)
	{
		$table = DB::connection(self::$myDb)->table('locations');
		$object = $table->where('id',$id)->where('deleted',0);
		if($object){
			return $object->first();
		}
		else{
			return false;
		}
	}
	//update Estate Registry form data into the table.
	public static function updateRecordData($id, $form_data)
	{
		$object = DB::connection(self::$myDb)->table('locations')->where('id', $id)->update($form_data);
		return $object;
	}
	public static function deleteEstateRegistryRecord($record_id)
	{
		$object = DB::connection(self::$myDb)->table('locations')->where('id', $record_id)->update(array('deleted' => 1));
		return $object;
	}
	public static function deleteAttachment($record_id)
	{
		$object = DB::connection(self::$myDb)->table('uploads')->where('location_id', $record_id)->update(array('deleted' => 1));
		return $object;
	}
	public static function addLog($data)
	{
		DB::connection(self::$myDb)->table("log")->insert($data);
	}
}
