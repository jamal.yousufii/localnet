<?php

namespace App\models\audit;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;
use DateTime;
use DateInterval;
use DatePeriod;
class AuditModel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'audit';
	public static $myDb 	= "audit";
	
	public static function insertRecord($table="",$data=array())
	{
		DB::connection(self::$myDb)->table($table)->insert($data);
	}
	public static function insertRecord_id($table="",$data=array())
	{
		return DB::connection(self::$myDb)->table($table)->insertGetId($data);
	}
	public static function update_record($table='',$data,$where)
	{
		DB::connection(self::$myDb)->table($table)->where($where)->update($data);
	}
	public static function delete_record($table='',$where)
	{
		DB::connection(self::$myDb)->table($table)->where($where)->delete();
	}
	public static function getAllReports($type=1)
	{
		$table = DB::connection(self::$myDb)
					->table('reports as r')
					->select('r.id','r.report_year','r.created_at','r.type','r.file_id','a.original_name as file_name',
						DB::raw(
								'CASE r.type
								    WHEN 1 THEN "Planed"
								    WHEN 2 THEN "Unplaned"
								    ELSE " "
								  END AS type_name'
								));
					$table->leftjoin('attachments AS a','a.id','=','r.file_id');
					$table->where('r.type',$type);
		
		$object = $table->orderBy('r.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getReports_search($type=0,$year=0,$attach='')
	{
		$table = DB::connection(self::$myDb)
					->table('reports as r')
					->select('r.id','r.report_year','r.created_at','r.type','r.file_id','a.original_name as file_name',
						DB::raw(
								'CASE r.type
								    WHEN 1 THEN "Planed"
								    WHEN 2 THEN "Unplaned"
								    ELSE " "
								  END AS type_name'
								));
					$table->leftjoin('attachments AS a','a.id','=','r.file_id');
					if($type!=0)
					{
						$table->where('r.type',$type);
					}
					if($year!=0)
					{
						$table->where('r.report_year',$year);
					}
					if($attach!='')
					{
						$table->where('a.original_name', 'like', '%'.$attach.'.%');
					}
		
		$object = $table->orderBy('r.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getReportDetails($id=0)
	{
		$table = DB::connection(self::$myDb)
				->table('reports as r')
				->select('r.*','a.original_name as file_name','a.file_name as filename')
				->leftjoin('attachments AS a','a.id','=','r.file_id')
				->where('r.id',$id)
				->first();
		return $table;
	}
	public static function getReportFileName($id=0)
	{
		$table = DB::connection(self::$myDb)
				->table('attachments')
				->where('id',$id)
				->pluck('file_name');
		return $table;
	}
	
	//<========================================== recommandations ========================================>
	
	public static function getAllRecommandations()
	{
		$table = DB::connection(self::$myDb)
					->table('recommandations')
					->select('recommandations.*','r.title as title','r.type','r.ministry','r.sub_dep',
							DB::raw(
								'CASE sent_authority
								    WHEN 1 THEN "Yes"
								    WHEN "0" THEN "No"
								    ELSE " "
								  END AS attorney'
							),
							DB::raw(
								'CASE implemented
								    WHEN 1 THEN "Yes"
								    WHEN "0" THEN "No"
								    ELSE " "
								  END AS implement'
							));
					//$table->where('type',$type);
		//$table->leftjoin('auth.department AS dep','dep.id','=','recommandations.responsible_dir');
		$table->leftjoin('finding_reports AS r','r.id','=','recommandations.report_title_id');
		$object = $table->orderBy('recommandations.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getRecommandationDetails($id=0)
	{
		$table = DB::connection(self::$myDb)
				->table('recommandations as r')
				->select('r.*','m.amount','m.date','m.id as money_id','a.original_name as file_name','a.file_name as filename')
				->leftjoin('attachments AS a','a.id','=','r.file_id')
				->leftjoin('money_notes AS m','m.recommandation_id','=','r.id')
				->where('r.id',$id)
				->first();
		return $table;
	}
	public static function getAllRecommendation_items($id=0)
	{
		$table = DB::connection(self::$myDb)
				->table('recommandation_items as r')
				->where('r.recommandation_id',$id)
				->get();
		return $table;
	}
	public static function getRecommandationFileName($id=0)
	{
		$table = DB::connection(self::$myDb)
				->table('recommandations')
				->where('id',$id)
				->pluck('file_name');
		return $table;
	}
	public static function getFindingDetails($id=0)
	{
		$table = DB::connection(self::$myDb)
				->table('finding_reports as m')
				//->select('m.*','f.title as finding')
				//->leftjoin('findings AS f','m.id','=','f.report_id')
				->where('m.id',$id)
				->first();
		return $table;
	}
	public static function getReportFindings($id=0)
	{
		$table = DB::connection(self::$myDb)
				->table('findings as m')
				//->select('f.title as finding')
				//->leftjoin('findings AS f','m.id','=','f.report_id')
				->where('m.report_id',$id)
				->get();
		return $table;
	}
	public static function getReportSubFindings($id=0)
	{
		$table = DB::connection(self::$myDb)
				->table('sub_findings as m')
				->select('m.*')
				->leftjoin('findings AS f','f.id','=','m.finding_id')
				->where('f.report_id',$id)
				->get();
		return $table;
	}
	public static function getAllMoneyNotes()
	{
		$table = DB::connection(self::$myDb)
					->table('money_notes as t1')
					->select('t1.*','gdep.name as general_dep','dep.name as sub_dep',DB::raw('sum(pay.amount) as payments'))
					->leftjoin('recommandations AS r','r.id','=','t1.recommandation_id')
					->leftjoin('auth.department AS gdep','gdep.id','=','r.general_dir')
					->leftjoin('auth.department AS dep','dep.id','=','r.responsible_dir')
					->leftjoin('money_paybacks AS pay','pay.money_id','=','t1.id')
					->groupBy('pay.money_id');
					//$table->where('type',$type);
		
		$object = $table->orderBy('r.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getMoneyNoteDetails($id=0)
	{
		$table = DB::connection(self::$myDb)
				->table('money_notes')
				->where('id',$id)
				->first();
		return $table;
	}
	public static function getMoneyNotePayments($id=0)
	{
		$table = DB::connection(self::$myDb)
				->table('money_paybacks')
				->where('money_id',$id)
				->get();
		return $table;
	}
	public static function getPaymentDetails($id=0)
	{
		$table = DB::connection(self::$myDb)
				->table('money_paybacks')
				->where('id',$id)
				->first();
		return $table;
	}
	public static function getAllFindings()
	{
		$table = DB::connection(self::$myDb)
					->table('finding_reports as t1')
					->select('t1.*');
					//->leftjoin('ministries AS m','m.id','=','t1.ministry');
					//->leftjoin('auth.department AS gdep','gdep.id','=','t1.general_dep')
					//->leftjoin('auth.department AS dep','dep.id','=','t1.sub_dep');
					//->leftjoin('money_paybacks AS pay','pay.money_id','=','t1.id')
					//->groupBy('pay.money_id');
					//$table->where('type',$type);
		
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getGeneralChart($implemented='')
	{
		$result= DB::connection(self::$myDb)
				->table('recommandations as r')
				->leftjoin('auth.department AS gdep','gdep.id','=','r.general_dir')
				->select(DB::raw('count(r.id) As total'),'gdep.name','r.general_dir');
				if($implemented!='')
				{
					$result->where('r.implemented',$implemented);
				}
				$result->orderBy('r.id')
				->groupBy('r.general_dir');
					
		return $result->get();;
	}
	public static function getSubChart($implemented='')
	{
		$result= DB::connection(self::$myDb)
				->table('recommandations as r')
				->leftjoin('auth.department AS gdep','gdep.id','=','r.responsible_dir')
				->select(DB::raw('count(r.id) As total'),'gdep.name','r.responsible_dir');
				if($implemented!='')
				{
					$result->where('r.implemented',$implemented);
				}
				$result->orderBy('r.id')
				->groupBy('r.responsible_dir');
					
		return $result->get();
	}
	public static function getInvestigationDet($id=0)
	{
		$result = DB::connection(self::$myDb)
				->table('finding_reports as f')
				->select('gdep.name as gen_dir','sdep.name as sub_dir',
				'm.name_dr as ministry_name',
				'f.*'
				)
				->leftjoin('auth.department AS gdep','gdep.id','=','f.general_dep')
				->leftjoin('auth.department AS sdep','sdep.id','=','f.sub_dep')
				->leftjoin('ministries AS m','m.id','=','f.ministry')
				->where('f.id',$id)
				->first();
		return $result;
	}
	public static function getAllMinistries()
	{
		$result = DB::connection(self::$myDb)
				->table('ministries')
				->get();
		return $result;
	}
	///////////////////
	public static function getAllAttachs()
	{
		$table = DB::connection(self::$myDb)
					->table('attachments as a')
					->leftjoin('categories AS c','c.id','=','a.category_id')
					->select('a.id','a.created_at','a.file_name','a.original_name','c.name_da as category');
		
		$object = $table->orderBy('created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getAllCategories()
	{
		$table = DB::connection(self::$myDb)
					->table('categories')
					->select('id','name_da as name');
		
		return $table->get();
	}
	public static function bringDepEmployees($id=0)
	{
		$table = DB::connection('hr')
					->table('employees')
					->select('id','name_dr as name','last_name')
					->where('department',$id);
		return $table->get();
	}
	public static function bringMoneyEmployees($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('money_note_employees as e')
					->select('e.employee_id')
					->leftjoin('money_notes AS c','c.id','=','e.money_id')
					->where('c.recommandation_id',$id);
		return $table->get();
	}
	public static function getMoneyNoteEmployees($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('money_note_employees as e')
					->select('e.employee_id','c.name_dr','c.last_name')
					->leftjoin('hr.employees AS c','c.id','=','e.employee_id')
					->where('e.money_id',$id);
		return $table->get();
	}
}