<?php namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class Document extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'document_records';

	public static function getAll()
	{
		  
		$userid = Auth::user()->id;

		$table = DB::table('document_records');
        $object = $table->get();

        return $object;
	}
	public static function getAllTrees()
	{
		  
		$userid = Auth::user()->id;

		$table = DB::table('document_trees');
		$table->where('user_id',$userid);
        $object = $table->get();

        return $object;
	}
	
	public static function getDetails($id=0)
	{
		$table = DB::table('document_records');
		$table->where('id',$id);
		$object = $table->get();
		return $object;
	}

	public static function getSharedDetails($id=0,$cond='')
	{
		if($cond=='u')
		{
			$table = DB::table('document_shares_user');
		}
		elseif ($cond=='f') 
		{
			$table = DB::table('document_files');
		}
		else
		{
			$table = DB::table('document_shares_department');
		}
		
		$table->where('document_id',$id);
		$object = $table->get();
		return $object;
	}

	public static function insertBatch($table='',$data=array())
	{
		DB::table($table)->insert($data);
	}

	public static function insertNew($table='',$data=array())
	{
		DB::table($table)->insert($data);
	}

	public static function removeShared($doc_id=0)
	{
		//delete shared with users
		DB::table('document_shares_user')->where('document_id',$doc_id)->delete();
		//delete shared with departments
		DB::table('document_shares_department')->where('document_id',$doc_id)->delete();
	}

	

}
