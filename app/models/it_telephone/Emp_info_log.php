<?php namespace App\models\it_telephone;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;


class Emp_info_log extends Model {


	protected $table = 'it_telephone.emp_info_log';

	public $timestamps = false;

	//protected $filedata=['khlas_mouzo','taqaror_anfaka_safarha','shomara_farman','shamara_hokam','date','jazyat_mouzo','malahaza'];

public static function insert_emp_info($data)
	{	
		$insert_data = DB::connection('it_telephone')->table('emp_info_log')->insert($data);
 		if($insert_data)
			return true;
		else
			return false;


		}
	
	public static function getFileName($record_id){

	$file_name=DB::connection('it_telephone')->table('emp_info_log')->where("id", $record_id)->pluck("file");
	return $file_name;
	
	

	}
}