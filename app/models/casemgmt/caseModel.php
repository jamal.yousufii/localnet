<?php

namespace App\models\casemgmt;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Session;

class caseModel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public static $myDb = "casemgmt";

//================================================= Records Management Model Functions =======================================

	//insert the department into the related tables.
	public static function addCaseDept($dept_data)
	{
		$object = DB::connection(self::$myDb)
					->table('department')
					->insert($dept_data);
		if($object){return true;}else{return false;}
	}
	//insert the person.
	public static function addCasePerson($person_data)
	{
		$object = DB::connection(self::$myDb)
					->table('person')
					->insert($person_data);
		if($object){return true;}else{return false;}
	}
	//update the department into the related tables.
	public static function editCaseDept($case_id,$dept_id, $dept_data)
	{
		$object = DB::connection(self::$myDb)->table('department');
					if($case_id != 0){$object->where('case_id', $case_id);}
					else{$object->where('id', $dept_id);}
					$object->update($dept_data);
		if($object){return true;}else{return false;}
	}
	//update the person.
	public static function editCasePerson($case_id,$person_id, $person_data)
	{
		$object = DB::connection(self::$myDb)->table('person');
					if($case_id != 0){$object->where('case_id', $case_id);}
					else{$object->where('id', $person_id);}
					$object->update($person_data);
		if($object){return true;}else{return false;}
	}
	//insert the agency form data.
	public static function addAgencyData($agency_data)
	{

		$object = DB::connection(self::$myDb)
					->table('agencies')
					->insert($agency_data);
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object;
	}
	// get the Departures.
	public static function getDeps()
	{
		$table = DB::connection(self::$myDb)
					->table('department');
					$table->select(
							'id',
							'name'
						);
		$object = $table->orderBy('id');
		return $object->get();
	}
	// get the Departures.
	public static function getPersons()
	{
		$table = DB::connection(self::$myDb)
					->table('person as p');
					$table->select(
							'p.id',
							'p.name',
							'dep.name as department'
						);
		$table->leftjoin('department as dep','dep.id','=','p.department');
		$object = $table->orderBy('id');
		return $object->get();
	}
	public static function getAgencyData()
	{

		$table = DB::connection(self::$myDb)
					->table('agencies');
					$table->select(
							'id',
							'name_en',
							'name_dr',
							'head_en',
							'head_dr',
							'contact1',
							'contact2',
							'contact3'
						);
		$object = $table->orderBy('id');
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object->get();
	}
	//get the department details based on id.
	public static function getDeptDetails($id)
	{
		$table = DB::connection(self::$myDb)
					->table('department');
					$table->select(
							'id',
							'name'
						)->where('id', $id);
		$object = $table->orderBy('id');
		return $object->get();
	}
	//get the person name details based on id.
	public static function getPersonDetails($id)
	{
		$table = DB::connection(self::$myDb)
					->table('person');
					$table->select(
							'id',
							'name',
							'department'
						)->where('id', $id);
		$object = $table->orderBy('id');
		return $object->get();
	}
	//update request and department data.
	public static function updateRequestAndDepData($id, $data)
	{
		$object = DB::connection(self::$myDb)
					->table('request_department')
					->where('id', $id)
					->update($data);
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object;
	}
	//get the agency details based on id.
	public static function getCaseAgencyDetails($id)
	{
		$table = DB::connection(self::$myDb)
					->table('agencies');
					$table->select("*")->where('id', $id);
		$object = $table->orderBy('id');
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object->get();
	}
	//update Case Agency data.
	public static function updateAgencyData($id, $data)
	{
		$object = DB::connection(self::$myDb)
					->table('agencies')
					->where('id', $id)
					->update($data);
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object;
	}
	//get related department based on user_id.
	public static function getRelatedDept($user_id)
	{
		$object = DB::table('auth.users')->where('id',$user_id)->pluck('department_id');	
		return $object;
		// $table = DB::connection(self::$myDb)
		// 			->table('auth.users as u');
		// 			$table->select(
		// 					'dept.name as department'
		// 					);
		// $table->leftjoin("auth.department AS dept","dept.id","=","u.department_id");
		// $table->where('u.id',$user_id);
		//return $table->first();
	}
//================================================= Main Page Model Functions ======================================
	//get the list of case management logs.
	public static function getLogList()
	{
		$object = DB::connection(self::$myDb)->table('log')->select("*")->orderBy('id','desc');
		return $object->get();
	}
	//add the case notify data to notify the manager.
	public static function addNotifyToManagerData($notify_data)
	{
		$object = DB::connection(self::$myDb)->table("notify_to_manager")->insert($notify_data);
		return true;
	}
	//get the notifications of the manager.
	public static function getNotification()
	{
		$object = DB::connection(self::$myDb)->table("notify_to_manager")->select("*")->where('is_viewed', 0)->groupBy('case_id');
		if($object){return $object->get();}else{return false;}
	}
	//visited notifications.
	public static function notificationVisited($id)
	{
		$object = DB::connection(self::$myDb)->table("notify_to_manager")->where('case_id', $id)->update(array('is_viewed' => 1));
	}
	//get approved finished tasks from notify to manger table based on case id.
	public static function getApprovedFinishedCases($id)
	{
		$object = DB::connection(self::$myDb)->table("notify_to_manager")->where('case_id', $id)->pluck("approved");
		return $object;
	}
	// approve the finished task based on case_id and then get the required information from cases table based on id then insert in to notification table.
	public static function getApprovedFinishedTask($case_id)
	{
		$updated = DB::connection(self::$myDb)->table('notify_to_manager')->where('case_id',$case_id)->update(array('approved' => 1));
		if($updated)
		{
			//get the title of the task based on case_id.
			$task_title = DB::connection(self::$myDb)->table('cases')->where('id',$case_id)->pluck('task_title');
			if($task_title == ""){$task_title = "Pending transcription";}
			$presidentNotificationData = array(
				'type' 			=> 'case',
				'subject' 		=> $task_title,
				'status' 		=> 3,
				'case_id' 		=> $case_id,
				"user_id"		=> Auth::user()->id,
		    	"created_at"	=> date('Y-m-d H:i:s')
			);
			$done = caseModel::addCaseNotification($presidentNotificationData);
			if($done){return true;}else{return false;}
		}
	}
	//get the notification details by table name and record_id sent from log table.
	public static function getSpecificNotificationLogDetails($table_name,$record_id)
	{
		$object = DB::connection(self::$myDb)->table($table_name)->select("*")->where('id', $record_id)->first();
		if($object){return $object;}else{return false;}
	}
	//get the Agency details by table name and record_id sent from log table.
	public static function getSpecificAgencyLogDetails($record_id)
	{
		$object = DB::connection(self::$myDb)->table("agencies")->select("*")->where('id', $record_id)->first();
		if($object){return $object;}else{return false;}
	}
//================================================= Case / Task Management Model Functions =======================================

	// get departments of the Case.
	public static function getCaseDepts()
	{
		$object = DB::connection(self::$myDb)->table("request_department")->select("id","department");
		if($object){
			return $object->get();
		}else{
			return false;
		}
	}

	//insert the Case form data into the table.
	public static function addCase($form_data)
	{
		$object = DB::connection(self::$myDb)
					->table('cases')
					->insertGetId($form_data);
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object;
	}
	//get change the status of deleted field of cases table based on it's id.
	public static function getDeleteCase($id)
	{
		//dd($id);
		$object = DB::connection(self::$myDb)
					->table('cases')->where('id', $id)
					->update(['deleted' => 1]);
		if($object){return true;}else{return false;}
	}
	//insert the task description data with the inserted case id.
	public static function addTaskDescription($data)
	{
		$object = DB::connection(self::$myDb)
					->table('task_description')
					->insert($data);
		return $object;
	}
	//get task description based on id.
	public static function getTaskDescription($case_id)
	{
		return DB::connection('casemgmt')->table('task_description')
					->select('task_description_en','task_description_dr_ps')->where('case_id', $case_id)->get();
	}
	public static function addCaseLog($data)
	{
		DB::connection(self::$myDb)->table("log")->insert($data);
	}

	// get the Departures.
	public static function getData()
	{

		// $table = DB::connection(self::$myDb)
		// 			->table('case as c');
		// 			$table->select(
		// 					'c.id',
		// 					'c.task_title',
		// 					'c.requesting_person',
		// 					'c.department',
		// 					'trl.name as task_request_level',
		// 					'c.start_date',
		// 					'c.deadline',
		// 					'tg.name as task_category',
		// 					'c.assigned_to_option',
		// 					'c.task_assigned_to',
		// 					'tp.status as status'
		// 					);
		// $table->leftjoin("category AS tg","tg.id","=","c.task_category");
		// $table->leftjoin("department AS rd","rd.id","=","c.department");
		// $table->leftjoin("task_progress AS tp","tp.case_id","=","c.id");
		// $table->leftjoin("task_request_level AS trl","trl.id","=","c.task_request_level");
		// $table->orderBy('tp.id','desc');
		// $object = $table->groupBy('c.id');
		
		// return $object->get();
		
		$sql = "SELECT
				    c.*, task_temp.status,cat.name AS task_category, trl.name as task_request_level, u.username as requesting_person,dept.name as department
				FROM
				    casemgmt.cases AS c
					
				    LEFT JOIN ( select * from (SELECT * from casemgmt.task_progress order by id desc) t group by case_id ) as task_temp
				
				    ON (c.id = task_temp.case_id)
					
					LEFT JOIN casemgmt.category AS cat ON cat.id = c.task_category
					LEFT JOIN auth.users AS u ON u.id = c.requesting_person
					LEFT JOIN auth.department AS dept ON dept.id = c.department
					LEFT JOIN casemgmt.task_request_level AS trl ON trl.id = c.task_request_level
				WHERE
					c.deleted = 0";
					
// SELECT
// 				    c.*, task_temp.status,cat.name AS task_category, trl.name as task_request_level
// 				FROM
// 				    casemgmt.cases AS c
				
// 				    LEFT JOIN ( SELECT p1.*
// 						FROM casemgmt.task_progress as p1
// 						LEFT JOIN casemgmt.task_progress AS p2
// 						     ON p1.case_id = p2.case_id AND p1.created_at < p2.created_at
// 						WHERE p2.case_id IS NULL ) as task_temp
				
// 				    ON (c.id = task_temp.case_id)
					
// 					LEFT JOIN casemgmt.category AS cat ON cat.id = c.task_category
// 					LEFT JOIN casemgmt.task_request_level AS trl ON trl.id = c.task_request_level					
					
		$object = DB::select($sql);
		return $object;
	}
	//get the task details based on id.
	public static function getSpecifiCaseDetails($id)
	{
		$table = DB::connection(self::$myDb)->table('cases as c');
		$table->select("c.*",'cat.name as task_category');
		$table->leftjoin("casemgmt.category AS cat","cat.id","=","c.task_category");
		$table->where('c.id', $id);
		$object = $table->orderBy('c.id');
		if($object){
			return $object->get();
		}
		else{
			return false;
		}
	}
	//update the Case form data into the table.
	public static function editCase($id, $form_data)
	{
		$object = DB::connection(self::$myDb)
					->table('cases')
					->where('id', $id)
					->update($form_data);
		return $object;
	}
	// check if the person name and department already exist by case id, if not then insert if yes then update.
	public static function postUpdateCaseDep($id, $person_name, $related_department)
	{
		//get the data based on id.
		$dept = DB::connection(self::$myDb)->table("department")->where('case_id', $id)->count();
		$person = DB::connection(self::$myDb)->table("person")->where('case_id', $id)->count();
		if($dept > 0 && $person > 0){
			return DB::connection(self::$myDb)->table("department")->where('case_id', $id)
			->update(array('name' => $related_department));
			//add log
			$caseLog = array('action_table' => "department",'action_by' => Auth::user()->username,'action_type' => "Added");
			addCaseLog($caseLog);
			return DB::connection(self::$myDb)->table("person")->where('case_id', $id)
			->update(array('name' => $person));
			//add log
			$caseLog = array('action_table' => "person",'action_by' => Auth::user()->username,'action_type' => "Added");
			addCaseLog($caseLog);
		}
		else{
			return DB::connection(self::$myDb)->table("department")
			->insert(array('name' => $related_department, 'case_id' => $id , 'user_id' => Auth::user()->id));
			//add log
			$caseLog = array('action_table' => "department",'action_by' => Auth::user()->username,'action_type' => "Edited");
			addCaseLog($caseLog);
			return DB::connection(self::$myDb)->table("person")
			->insert(array('name' => $person, 'case_id' => $id , 'user_id' => Auth::user()->id));
			//add log
			$caseLog = array('action_table' => "person",'action_by' => Auth::user()->username,'action_type' => "Edited");
			addCaseLog($caseLog);
		}
		
	}
	//get the task details.
	public static function getCaseDetails($id)
	{
		$table = DB::connection(self::$myDb)
					->table('cases as c');
					$table->select(
							'c.id',
							'c.task_title',
							'u.username as requesting_person',
							'dept.name as department',
							'trl.name as task_request_level',
							'c.start_date',
							'c.deadline',
							'c.deadline_option',
							'c.recurring_type',
							'c.alarm',
							'c.assigned_to_option',
							'c.task_assigned_to',
							'c.agency_manager',
							//'tg.name as task_category',
							'task_category',
							'c.task_description_en'
							)->where('c.id', $id);
		//$table->leftjoin("category AS tg","tg.id","=","c.task_category");
		//$table->leftjoin("department AS rd","rd.id","=","c.department");
		$table->leftjoin("auth.users AS u","u.id","=","c.requesting_person");
		$table->leftjoin("auth.department AS dept","dept.id","=","c.department");
		$table->leftjoin("task_request_level AS trl","trl.id","=","c.task_request_level");
		$object = $table->orderBy('c.id');
		if($object){
			return $object->get();
		}
		else{
			return false;
		}
	}
	//get the last inserted task progress based on case id.
	public static function getCaseProgress($id)
	{
		$object = DB::connection(self::$myDb)
					->table('task_progress')
					->select("*")->where('case_id', $id)->orderBy('id','desc')->first();
		return $object;
	}
	// add Task Progress in case table.
	public static function addProgress($data)
	{
		$object = DB::connection(self::$myDb)->table('task_progress')->insertGetId($data);
		if($object){ return $object;}else{return false;}
	}
	//get the progress details based on user_id;
	public static function getCaseProgressData($id)
	{
		$object = DB::connection(self::$myDb)
					->table('task_progress as tp')
					->select('tp.*','u.username as user','up.file_name as file_name')
					->where('tp.case_id', $id)
					->leftjoin("auth.users AS u","u.id","=","tp.user_id")
					->leftjoin("uploads AS up","up.progress_id","=",'tp.id');
		return $object->get();
	}
	// get the uploaded files of Case based on its id.
	public static function getCaseAttachments($case_id = 0)
	{
		// get the files based on case_id.
		$case_attachments = DB::connection(self::$myDb)
						->table('uploads')
						->where('case_id', $case_id)
						->get();
		return $case_attachments;
	}
	//add the case notification data.
	public static function addCaseNotification($data)
	{
		$object = DB::connection(self::$myDb)->table('notification')->insertGetId($data);
		if($object){ return $object;}else{return false;}
	}
// =================================================== Execution Order Model Functions ===========================================================
	//get execution order data for the list.
	public static function getExecOrderData()
	{
		$table = DB::connection(self::$myDb)
					->table('exec_order');
					$table->select(
							'id',
							'agency',
							'description',
							'priority',
							'deadline'
						);
		$object = $table->orderBy('id');
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object->get();
	}
	//add executive order data.
	public static function addExecOrderData($data)
	{
		$object = DB::connection(self::$myDb)
					->table('exec_order')
					->insert($data);
		return $object;
	}
	//get the executive order detailed information based on id.
	public static function getExecData($id)
	{
		$object = DB::connection(self::$myDb)
					->table('exec_order')
					->select("*", DB::raw('CONCAT(DATEDIFF(`deadline`, NOW())) AS dateDifference'))->where("id", $id);
		return $object->get();
	}
	// update the execution order information based on it's id.
	public static function updateExecOrderData($id, $execData)
	{
		$object = DB::connection(self::$myDb)
					->table('exec_order')
					->where('id', $id)
					->update($execData);
		return $object;
	}
	//get task progress attached file name based on progress_id.
	public static function getCaseProgressFileName($progress_id)
	{
		$object = DB::connection(self::$myDb)->table('uploads')->select('file_name')->where('progress_id', $progress_id)->first();
		if($object){return $object->file_name;}else{return false;}
	}
	//get task attached file name based on case id and field name.
	public static function getCaseFileName($file_id,$field_name)
	{
		$object = DB::connection(self::$myDb)->table('uploads')->select($field_name)->where('id', $file_id)->first();
		if($object){return $object->$field_name;}else{return false;}
	}
	//delete the duplication case notification based on case_id and type="case" prior inserting the new record.
	public static function deleteDuplicateNotification($case_id)
	{
		DB::connection(self::$myDb)->table('notification')->where('case_id', $case_id)->where('type','=','case')->delete();
	}
	//check if the status is not Not Started nd Feedback .
	public static function checkTaskStatus($id)
	{
		$object = DB::connection(self::$myDb)->table('task_progress')->select('status')->where('case_id', $id)
					->orderBy('id','desc')->first();  //->whereNotIn('status', [1,4])
		if($object){return $object;}else{return false;}
	}
	//update the notification based on id.
	public static function updateCaseNotification($id, $data)
	{
		$object = DB::connection(self::$myDb)->table('notification')->where('id', $id)->update($data);
		if($object){return true;}else{return false;}
	}
	//get the notification list.
	public static function getNotificationList()
	{
		$object = DB::connection(self::$myDb)->table('notification')->select("*")->where("viewed","=",0)->orderBy('id','desc');
		return $object->get();
	}

}
