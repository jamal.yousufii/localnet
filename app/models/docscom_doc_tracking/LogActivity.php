<?php namespace App\models\docscom_doc_tracking;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;

class LogActivity extends Model {

			protected $table = 'docscom_document_tracking.log_activities';

			public $timestamps = true;
    
}
