<?php

namespace App\models\icom;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Session;

class icomModel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public static $myDb = "icom";

	// get the Departures.
	public static function getData()
	{

		$table = DB::connection(self::$myDb)
					->table('departure');
					$table->select(
							'id',
							'name',
							'job',
							'office',
							'travel_date',
							'country',
							'return_date',
							'statement_number',
							'chart_number'
							);
		$object = $table->orderBy('id');
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object->get();
	}

	// get the Returns.
	public static function getReturns()
	{

		$table = DB::connection(self::$myDb)
					->table('return');
					$table->select(
							'id',
							'name',
							'job',
							'office',
							'return_date',
							'return_country',
							'passport_number',
							'airline_name',
							'email',
							'status'
							);
		$object = $table->orderBy('id');
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object->get();
	}

	//insert the departure info into the table.
	public static function insertDepartures($form_data)
	{

		$object = DB::connection(self::$myDb)
					->table('departure')
					->insert($form_data);
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object;
	}

	// insert the Departure data into the log table;
	public static function addDepartureLog($logData)
	{
		DB::connection(self::$myDb)
					->table('departure_log')
					->insert($logData);
	}

	// get the specific departure based on id.
	public static function getSpecificDep($id = 0)
	{
		$object = DB::connection(self::$myDb)
					->table('departure')
					->select("*")
					->where("id", $id)
					->get();

		return $object;
	}

	//insert the departure info into the table.
	public static function updateDepartures($form_data, $id)
	{

		$object = DB::connection(self::$myDb)
					->table('departure')
					->where('id','=',$id)
					->update($form_data);
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object;
	}

	// get last week's departures.
	public static function getLastWeekDeps($start_date, $end_date)
	{
		//echo $start_date." ".$end_date;exit;
		$object = DB::connection(self::$myDb)
					->table('departure as d')
					->select(
							'd.id',
							'd.name',
							'd.job',
							'd.office',
							'd.travel_date',
							'd.country',
							'd.return_date',
							'd.statement_number',
							'd.chart_number',
							'c.comment as comment'
							)
					->leftjoin("comments AS c","c.dep_id","=","d.id")
					->whereBetween('d.travel_date', array($start_date, $end_date))
					->orderBy('d.travel_date');
		//dd($object->toSql());
		return $object->get();
	}

	public static function getFilteredResultData()
	{

		$from = Session::get('start_date');
		$to = Session::get('end_date');

		$table = DB::connection(self::$myDb)
					->table('departure as d')
					->select(
							'd.id',
							'd.name',
							'd.job',
							'd.office',
							'd.travel_date',
							'd.country',
							'd.return_date',
							'd.statement_number',
							'd.chart_number',
							'c.comment as comment'
							)
					->leftjoin("comments AS c","c.dep_id","=","d.id");

		//--- search criteria ------------------------------------------//
		if($from != '' && $to == '')
		{
			$table->where('d.travel_date','>=',$from);
		}
		elseif($from == '' && $to != '') 
		{
			$table->where('d.travel_date','<=',$to);
		}
		elseif($from != '' && $to != '') 
		{
			$table->whereBetween('d.travel_date',array($from,$to));
		}

		$object = $table->orderBy('d.travel_date');
		//dd($object->toSql());
		return $object->get();

	}

	// add the deleted record into the Log table;
	public static function addDeletedDepsLog($id)
	{
		
		// get the data based on its id.
		$dep_data = DB::connection(self::$myDb)->table('departure')
						->select('*')
						->where('id', $id)->get();
		
		foreach ($dep_data as $item) {

			$name = $item->name;
			$job = $item->job;
			$office = $item->office;
			$travel_date = $item->travel_date;
			$country = $item->country;
			$return_date = $item->return_date;
			$statement_number = $item->statement_number;
			$chart_number = $item->chart_number;

		}

		// put the taken data into an array;
		$deletedLogData = array(

						"name" 					=> $name,
						"job" 					=> $job,
						"office" 				=> $office,
						"travel_date" 			=> $travel_date,
						"country"				=> $country,
						"return_date"			=> $return_date,
						"statement_number"		=> $statement_number,
						"chart_number"			=> $chart_number,
						"user"					=> Auth::user()->username,
						"action"				=> "Deleted"

			);
		
		DB::connection(self::$myDb)->table("departure_log")->insert($deletedLogData);

	}

	// get last week's returns.
	public static function getLastWeekReturns($start_date, $end_date)
	{
		//echo $start_date." ".$end_date;exit;
		$object = DB::connection(self::$myDb)
					->table('return')
					->select(
							'id',
							'name',
							'job',
							'office',
							'return_date',
							'return_country',
							'passport_number',
							'airline_name',
							'email'
							)
					->whereBetween('return_date', array($start_date, $end_date))
					->orderBy('return_date');
		//dd($object->toSql());
		//echo "<pre>";print_r($object);exit;
		return $object->get();
	}

	// get the returns based on the start and end date.
	public static function getReturnFilteredResultData()
	{

		$from = Session::get('start_date');
		$to = Session::get('end_date');

		$table = DB::connection(self::$myDb)
					->table('return');
					$table->select(
							'id',
							'name',
							'job',
							'office',
							'return_date',
							'return_country',
							'passport_number',
							'airline_name',
							'email'
							);

		//--- search criteria ------------------------------------------//
		if($from != '' && $to == '')
		{
			$table->where('return_date','>=',$from);
		}
		elseif($from == '' && $to != '') 
		{
			$table->where('return_date','<=',$to);
		}
		elseif($from != '' && $to != '') 
		{
			$table->whereBetween('return_date',array($from,$to));
		}

		$object = $table->orderBy('return_date');
		//dd($object->toSql());
		return $object->get();

	}

	// get the list of VIP travels based on today's date.
	public static function getTravelsData()
	{
		$today_date = date("Y-m-d");
		$object = DB::connection(self::$myDb)
					->table('departure as d')
					->select(
							'd.id',
							'd.name',
							'd.travel_date',
							'd.return_date',
							'd.statement_number',
							'd.chart_number',
							'c.comment as comment'
							)
					->leftjoin("comments AS c","c.dep_id","=","d.id")
					->where('travel_date', '=', $today_date)
					->orderBy('travel_date');
		//dd($object->toSql());
		return $object->get();
	}
	// get the comment of Departure based on its id.
	public static function getDepComment($id = 0)
	{
		// get the comment based on departure id.
		$dep_comment = DB::connection(self::$myDb)
						->table('comments')
						->where('dep_id', $id)
						->get();
		return $dep_comment;
	}

	// get the uploaded files of Departure based on its id.
	public static function getDepAttachments($id = 0)
	{
		// get the files based on departure id.
		$dep_attachment = DB::connection(self::$myDb)
						->table('attachments')
						->where('dep_id', $id)
						->get();
		return $dep_attachment;
	}

	//get the uploaded file name based on dep_id for download.
	public static function getIComFileName($id=0)
	{
		return DB::connection(self::$myDb)
				->table('attachments')
				->where('id',$id)
				->pluck('file_name');
	}

	// approve the return based on its id.
	public static function approveReturn($id = 0, $data)
	{
		$object = DB::connection(self::$myDb)
					->table('return')
					->where('id','=',$id)
					->update($data);
		if($object)
		{
			// get the return based on id.
			$approvedReturn = DB::connection(self::$myDb)
				->table('return')
				->where('id', $id)
				->get();

			foreach ($approvedReturn as $item) {
				$name = $item->name;
				$job = $item->job;
				$office = $item->office;
				$return_date = $item->return_date;
				$return_country = $item->return_country;
				$passport_number = $item->passport_number;
				$airline_name = $item->airline_name;
				$email = $item->email;
			}

			$return_logData = array(
                                    'name'                  => $name,
                                    'job'                   => $job,
                                    'office'                => $office,
                                    'return_date'           => $return_date, 
                                    'return_country'        => $return_country, 
                                    'passport_number'       => $passport_number, 
                                    'airline_name'          => $airline_name,
                                    'email'                 => $email,
                                    'user'					=> Auth::user()->username,
		    						'action'				=> 'Added'
        	);

        	DB::connection(self::$myDb)->table("return_log")->insert($return_logData);
        	return $object;
		}
	}

	// get approved returns
	public static function getApprovedReturns()
	{
		$table = DB::connection(self::$myDb)
					->table('return');
					$table->select(
							'id',
							'name',
							'job',
							'office',
							'return_date',
							'return_country',
							'passport_number',
							'airline_name',
							'email',
							'status'
							);
					$table->where('status','=','Approved');
		$object = $table->orderBy('id');
		
		return $object->get();
	}

}
