<?php

namespace App\models\procurement_inventory;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Session;
use Input;

class inventoryModel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public static $myDb = "procurement_inventory";

	//insert the Inventory form data into the table.
	public static function addInventory($form_data)
	{
		$object = DB::connection(self::$myDb)
					->table('inventory')
					->insertGetId($form_data);
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object;
	}
	//get stock in based on product type.
	public static function getStockInByProductType($product_type)
	{
		$object = DB::connection(self::$myDb)->table('inventory')->select('stock_in','stock_out')->where('product_type', $product_type)->where('deleted', 0)->orderBy('id','desc')->first();//->toSql();
		//dd($object);
		if($object != ""){
			$balance = $object->stock_in - $object->stock_out;
			return $balance;
		}else{return "";}
	}
	//get change the status of deleted field of cases table based on it's id.
	public static function getDeleteInventory($id)
	{
		//dd($id);
		$object = DB::connection(self::$myDb)->table('inventory')->where('id', $id)->update(array('deleted' => 1));
		if($object){return true;}else{return false;}
	}
	public static function addLog($data)
	{
		DB::connection(self::$myDb)->table("log")->insert($data);
	}

	// get the Departures.
	public static function getData()
	{
		$department = Input::get('department');
		$person_name = Input::get('person_name');
		$product_type = Input::get('product_type');
		$table = DB::connection(self::$myDb)->table('inventory as i');
		$table->select('i.*','dept.name as department','pt.type as product_type');
		$table->leftjoin("auth.department AS dept","dept.id","=","i.department");
		$table->leftjoin("product_type AS pt","pt.id","=","i.product_type");
		if($department != "")
		{
			$table->where('dept.id',$department);
		}
		if($person_name != "")
		{
			$table->where('i.person_name',$person_name);
		}
		if($product_type != "")
		{
			$table->where('i.product_type',$product_type);
		}

		$table->where('i.deleted',0);
		$table->orderBy('i.department','asc');
		if($table->count()>0)
		{
			return $object = $table->paginate(10);
		}
		else
			return "";
	}
	//get the procurement_inventory details info based on id.
	public static function getSpecificInventoryDetails($id)
	{
		$table = DB::connection(self::$myDb)
					->table('inventory as i');
					$table->select('i.*');
		//$table->leftjoin("product_type AS pt","pt.id","=","i.product_type");
		$object = $table->where('i.id',$id);
		if($object){
			return $object->first();
		}
		else{
			return false;
		}
	}
	//update the procurement_inventory form data into the table.
	public static function updateInventory($id, $form_data)
	{
		$object = DB::connection(self::$myDb)
					->table('inventory')
					->where('id', $id)
					->update($form_data);
		return $object;
	}
	//get sum of stocks based on product type.
	public static function getStocksTotal($department,$person_name,$field)
	{
		$object = DB::connection(self::$myDb)->table('inventory');
		if($department != "")
		{
		$object->where('department',$department);
		}
		if($person_name != "")
		{
			$object->where('person_name',$person_name);		
		}
		if($department != "" && $person_name != "")
		{
			$object->where('department',$department)->where('person_name',$person_name);
		}
		$object->where('deleted',0);
		return $object->sum($field);
	}
	public static function getDetails($id)
	{
		$object = DB::connection(self::$myDb)->table('inventory as i')->select('i.*','dept.name as department');
		$object->leftjoin("auth.department as dept","dept.id","=","i.department");
		$object->where('i.id',$id)->where('i.deleted',0);
		return $object->first();
	}
	public static function getFile($id)
	{
		$file = DB::connection(self::$myDb)->table('uploads')->where('inventory_id',$id)->where('deleted',0)->first();
		return $file;
	}

}
