<?php

namespace App\models\com;
use Illuminate\Database\Eloquent\Model;
use DB;

class Com extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'com';
	protected $table = 'received_docs';
	
	public static $myDb = 'com';

	public static function getAll_stable($table="",$lang='en')
	{
		return DB::connection(self::$myDb)->table($table)
                        ->select('id','name_'.$lang.' AS name')
                        ->get();
	}
	public static function getAll($table="")
	{
		return DB::connection(self::$myDb)->table($table)
                        ->select('*')
                        ->get();
	}
	public static function insertBatch($table='',$data=array())
	{
		DB::connection(self::$myDb)->table($table)->insert($data);
	}
	public static function delete_record($table='',$where)
	{
		DB::connection(self::$myDb)->table($table)->where($where)->delete();
	}
	public static function update_record($table='',$where,$data)
	{
		DB::connection(self::$myDb)->table($table)->where($where)->update($data);
	}
	public static function getAll_receivers($id)
	{
		return DB::connection(self::$myDb)->table('sended_docs_receivers')
                        ->select('*')
						->where('sended_docs_id',$id)
                        ->get();
	}
	public static function getAll_auto_receivers()
	{
		return DB::connection(self::$myDb)->table('sended_docs_receivers')
                        ->select('*')
						->groupBy('name')
                        ->get();
	}
	public static function getAll_withid($table,$field,$id)
	{
		return DB::connection(self::$myDb)->table($table)
                        ->select('*')
						->where($field,$id)
                        ->get();
	}
	//insert new record
	public static function insertRecord($table="",$data=array())
	{
		DB::connection(self::$myDb)->table($table)->insert($data);
	}
	public static function getAll_attachments($doc_id=0,$type=null)
	{
		return DB::connection(self::$myDb)->table('attachments')
                        ->select('*')
						->where('doc_id',$doc_id)
						->where('doc_type',$type)
                        ->get();
	}

}
