<?php

namespace App\models\com;
use Illuminate\Database\Eloquent\Model;
use DB;

class Sended_doc extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'com';
	//protected $table = 'received_docs';
	
	public static $myDb = 'com';

	public static function getData()
	{
		    
		$table = DB::connection(self::$myDb)->table('sended_docs')
					//->leftJoin('module AS m','m.id','=','e.module_id')
					->select(
								 'id',
								 'dossier_name',
								 'code',
								 'number',
								 'date',
								 'president_guide',
								 'desc',
								 'answer_to'
								 );

        return $table->get();
	}
	public static function getData_search($request=array())
	{
		$from = "";
		$to = "";
		$deadline="";
		if($request['fdate'] != '')
		{
			$from = gregorian_format($request['fdate']);
		}
		if($request['tdate'] != '')
		{
			$to = gregorian_format($request['tdate']);
		}
		if($request['deadline'] != '')
		{
			$deadline = gregorian_format($request['deadline']);
		}
		$table = DB::connection(self::$myDb)->table('sended_docs AS r')
					->select('r.*');
		$dossier = $request['dossier'];
		$reason = $request['reason'];
		$guide = $request['guide'];
		$desc = $request['desc'];
		$sender = $request['receivers'];
		
		if($dossier!='')
		{
			$table->where('r.dossier_name','LIKE',"%$dossier%");
		}
		if($sender!='')
		{
			$table->leftJoin('sended_docs_receivers AS m','m.sended_docs_id','=','r.id');
			$table->where('m.name','LIKE',"%$sender%");
		}
		if($request['code']!='')
		{
			$table->where('r.code',$request['code']);
		}
		if($request['priority']!='')
		{
			$table->where('r.priority_id',$request['priority']);
		}
		if($request['number']!='')
		{
			$table->where('r.number',$request['number']);
		}
		if($request['answer']!='')
		{
			$table->where('r.answer_to',$request['answer']);
		}
		if($request['result']!='')
		{
			$table->where('r.result_id',$request['result']);
		}
		if($request['summary']!='')
		{
			$table->where('r.result_reason','LIKE',"%$reason%");
		}
		if($request['guide']!='')
		{
			$table->where('r.president_guide','LIKE',"%$guide%");
		}
		if($request['desc']!='')
		{
			$table->where('r.desc','LIKE',"%$desc%");
		}
		if($deadline!='')
		{
			$table->where('r.deadline_date',$deadline);
		}
		
		if($request['fdate'] != '' && $request['tdate'] == '')
		{
			$table->where('r.date','>=',$from);
		}
		elseif($request['fdate'] == '' && $request['tdate'] != '') 
		{
			$table->where('r.date','<=',$to);
		}
		elseif($request['fdate'] != '' && $request['tdate'] != '') 
		{
			$table->whereBetween('r.date',array($from,$to));
		}
        return $table->get();
	}
	public static function getData_excel($request=array())
	{
		$from = "";
		$to = "";
		$deadline="";
		if($request['fdate'] != '')
		{
			$from = gregorian_format($request['fdate']);
		}
		if($request['tdate'] != '')
		{
			$to = gregorian_format($request['tdate']);
		}
		if($request['deadline'] != '')
		{
			$deadline = gregorian_format($request['deadline']);
		}
		$table = DB::connection(self::$myDb)->table('sended_docs AS r')
					->select('r.*','pt.name_en AS priority_name','rt.name_en AS result_name')
					->leftJoin('priority_types AS pt','pt.id','=','r.priority_id')
					->leftJoin('result_types AS rt','rt.id','=','r.result_id');
		$dossier = $request['dossier'];
		$reason = $request['reason'];
		$guide = $request['guide'];
		$desc = $request['desc'];
		$sender = $request['receivers'];
		
		if($dossier!='')
		{
			$table->where('r.dossier_name','LIKE',"%$dossier%");
		}
		if($sender!='')
		{
			$table->leftJoin('sended_docs_receivers AS m','m.sended_docs_id','=','r.id');
			$table->where('m.name','LIKE',"%$sender%");
		}
		if($request['code']!='')
		{
			$table->where('r.code',$request['code']);
		}
		if($request['priority']!='')
		{
			$table->where('r.priority_id',$request['priority']);
		}
		if($request['number']!='')
		{
			$table->where('r.number',$request['number']);
		}
		if($request['answer']!='')
		{
			$table->where('r.answer_to',$request['answer']);
		}
		if($request['result']!='')
		{
			$table->where('r.result_id',$request['result']);
		}
		if($request['summary']!='')
		{
			$table->where('r.result_reason','LIKE',"%$reason%");
		}
		if($request['guide']!='')
		{
			$table->where('r.president_guide','LIKE',"%$guide%");
		}
		if($request['desc']!='')
		{
			$table->where('r.desc','LIKE',"%$desc%");
		}
		if($deadline!='')
		{
			$table->where('r.deadline_date',$deadline);
		}
		
		if($request['fdate'] != '' && $request['tdate'] == '')
		{
			$table->where('r.date','>=',$from);
		}
		elseif($request['fdate'] == '' && $request['tdate'] != '') 
		{
			$table->where('r.date','<=',$to);
		}
		elseif($request['fdate'] != '' && $request['tdate'] != '') 
		{
			$table->whereBetween('r.date',array($from,$to));
		}
        return $table->get();
	}
	

}
