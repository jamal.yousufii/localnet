<?php

namespace App\models\com;
use Illuminate\Database\Eloquent\Model;
use DB;

class static_table extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'com';
	//protected $table = 'received_docs';
	
	public static $myDb = 'com';

	public static function getData($table)
	{		    
		$table = DB::connection(self::$myDb)->table($table)
					//->leftJoin('module AS m','m.id','=','e.module_id')
					->select(
								 'id',
								 'name_en',
								 'name_dr',
								 'name_pa'
								 );

        return $table->get();
	}
	

}
