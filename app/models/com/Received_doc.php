<?php

namespace App\models\com;
use Illuminate\Database\Eloquent\Model;
use DB;

class Received_doc extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'com';
	//protected $table = 'received_docs';
	
	public static $myDb = 'com';

	public static function getData()
	{
		    
		$table = DB::connection(self::$myDb)->table('received_docs AS r')
					->leftJoin('dep_lists AS d','d.id','=','r.sender_id')
					->select(
								 'r.id',
								 'r.dossier_name',
								 'r.code',
								 'r.summary',
								 'd.name_en AS dep_name',
								 'r.number',
								 'r.date'
								 );

        return $table->get();
	}
	public static function getData_search($request=array(),$sender='')
	{
		$from = "";
		$to = "";
		if($request['fdate'] != '')
		{
			$from = gregorian_format($request['fdate']);
		}
		if($request['tdate'] != '')
		{
			$to = gregorian_format($request['tdate']);
		}
		$table = DB::connection(self::$myDb)->table('received_docs AS r')
					->select('r.*')
					->leftJoin('received_docs_person_names AS m','r.id','=','m.received_docs_id');
		$dossier = $request['dossier'];
		$summary = $request['summary'];
		$note = $request['note'];
		$desc = $request['desc'];
		$report = $request['report'];
		
		$name = $request['fname'];
		$position = $request['position'];
		$email = $request['email'];
		$phone = $request['phone'];
		if($dossier!='')
		{
			$table->where('r.dossier_name','LIKE',"%$dossier%");
		}
		if($request['code']!='')
		{
			$table->where('r.code',$request['code']);
		}
		if($request['type']!='')
		{
			$table->where('r.type_id',$request['type']);
		}
		if($request['priority']!='')
		{
			$table->where('r.priority_id',$request['priority']);
		}
		if($request['number']!='')
		{
			$table->where('r.number',$request['number']);
		}
		if($request['sender']!='')
		{
			$table->where('r.sender_id',$sender);
		}
		if($request['summary']!='')
		{
			$table->where('r.summary','LIKE',"%$summary%");
		}
		if($request['note']!='')
		{
			$table->where('r.note','LIKE',"%$note%");
		}
		if($request['desc']!='')
		{
			$table->where('r.desc','LIKE',"%$desc%");
		}
		if($request['report']!='')
		{
			$table->where('r.monthly_report','LIKE',"%$report%");
		}
		if($request['fname']!='')
		{
			$table->where('m.first_name','LIKE',"%$name%");
		}
		if($request['position']!='')
		{
			$table->where('m.position','LIKE',"%$position%");
		}
		if($request['phone']!='')
		{
			$table->where('m.phone','LIKE',"%$phone%");
		}
		if($request['email']!='')
		{
			$table->where('m.email','LIKE',"%$email%");
		}
		
		if($request['fdate'] != '' && $request['tdate'] == '')
		{
			$table->where('r.date','>=',$from);
		}
		elseif($request['fdate'] == '' && $request['tdate'] != '') 
		{
			$table->where('r.date','<=',$to);
		}
		elseif($request['fdate'] != '' && $request['tdate'] != '') 
		{
			$table->whereBetween('r.date',array($from,$to));
		}
		$table->groupBy('r.id');
       return $table->get();
	}
	public static function getData_excel($request=array(),$sender='')
	{
		$from = "";
		$to = "";
		if($request['fdate'] != '')
		{
			$from = gregorian_format($request['fdate']);
		}
		if($request['tdate'] != '')
		{
			$to = gregorian_format($request['tdate']);
		}
		$table = DB::connection(self::$myDb)->table('received_docs AS r')
					//->leftJoin('module AS m','m.id','=','e.module_id')
					->select('r.id','r.dossier_name','r.code','r.number','r.date','dt.name_en AS type_name',
							'r.summary','dl.name_en AS sender_name','pt.name_en','pt.name_en AS priority_name','r.note','r.desc','r.monthly_report')
					->leftJoin('doc_types AS dt','dt.id','=','r.type_id')
					->leftJoin('dep_lists AS dl','dl.id','=','r.sender_id')
					->leftJoin('priority_types AS pt','pt.id','=','r.priority_id')
					->leftJoin('received_docs_person_names AS m','r.id','=','m.received_docs_id');
		$dossier = $request['dossier'];
		$summary = $request['summary'];
		$note = $request['note'];
		$desc = $request['desc'];
		$report = $request['report'];
		
		$name = $request['fname'];
		$position = $request['position'];
		$email = $request['email'];
		$phone = $request['phone'];
		if($dossier!='')
		{
			$table->where('r.dossier_name','LIKE',"%$dossier%");
		}
		if($request['code']!='')
		{
			$table->where('r.code',$request['code']);
		}
		if($request['type']!='')
		{
			$table->where('r.type_id',$request['type']);
		}
		if($request['priority']!='')
		{
			$table->where('r.priority_id',$request['priority']);
		}
		if($request['number']!='')
		{
			$table->where('r.number',$request['number']);
		}
		if($request['sender']!='')
		{
			$table->where('r.sender_id',$sender);
		}
		if($request['summary']!='')
		{
			$table->where('r.summary','LIKE',"%$summary%");
		}
		if($request['note']!='')
		{
			$table->where('r.note','LIKE',"%$note%");
		}
		if($request['desc']!='')
		{
			$table->where('r.desc','LIKE',"%$desc%");
		}
		if($request['report']!='')
		{
			$table->where('r.monthly_report','LIKE',"%$report%");
		}
		if($request['fname']!='')
		{
			$table->where('m.first_name','LIKE',"%$name%");
		}
		if($request['position']!='')
		{
			$table->where('m.position','LIKE',"%$position%");
		}
		if($request['phone']!='')
		{
			$table->where('m.phone','LIKE',"%$phone%");
		}
		if($request['email']!='')
		{
			$table->where('m.email','LIKE',"%$email%");
		}
		
		if($request['fdate'] != '' && $request['tdate'] == '')
		{
			$table->where('r.date','>=',$from);
		}
		elseif($request['fdate'] == '' && $request['tdate'] != '') 
		{
			$table->where('r.date','<=',$to);
		}
		elseif($request['fdate'] != '' && $request['tdate'] != '') 
		{
			$table->whereBetween('r.date',array($from,$to));
		}
		$table->groupBy('r.id');
        return $table->get();
	}
	
}
