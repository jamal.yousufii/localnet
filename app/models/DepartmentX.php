<?php namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Session;
class DepartmentX extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'department';

	public static function getAll()
	{
		    
		$table = DB::table('department');
        $object = $table->get();

        return $object;
	}
	public static function getAllParent()
	{
		    
		$table = DB::table('department');
        $table->where('parent',0);
        $object = $table->get();

        return $object;
	}

	public static function getDetails($id=0)
	{
		$table = DB::table('department');
		$table->where('id',$id);
		$object = $table->get();
		return $object;
	}

	public static function getDepartmentApp($id=0,$user_id=0)
	{
		$table = DB::table('module_department');
		$table->where('department_id',$id);
		//$table->leftJoin('department AS d','d.id','=','department_module.department_id');
		$table->leftJoin('module AS m','m.id','=','module_department.module_id');
		$table->select('m.name AS mname','m.code','m.description AS mdesc','m.id AS mid');
		$object = $table->get();
		return $object;
	}

	//get department employees
	public static function getDepartmentEmployees($dep_id=0)
	{
		//get my sub deparment id
		//$ids = getSubDepartmentIds($dep_id,true);

		$table = DB::connection("hr")->table('employees AS t1');
		$table->select("t1.id","t1.name_dr AS name","t1.father_name_dr AS father_name");
		$table->where('t1.department',$dep_id);
		//$table->orWhereIn('t1.department',$ids);

		return $table->get();
	}

	public static function getEmployeeDetails($id=0)
	{
		//get my sub deparment id
		//$ids = getSubDepartmentIds($dep_id,true);

		$table = DB::connection("hr")->table('employees AS t1');
        $table->select(
                    "t1.id AS userid",
                    "t1.name_dr AS name",
                    "t1.last_name AS last_name",
                    "t1.father_name_dr AS father_name",
                    "t1.name_en AS name_en",
                    "t1.department AS dep_no",
                    // "t1.RFID AS rfid",
                    "t1.position_dr AS position",
                    "t1.email",
                    "t1.photo"
                );
                    
		$table->where('t1.id',$id);
		//$table->orWhereIn('t1.department',$ids);

		return $table->get();
	}

	public static function deleteDepartmentRelation($id=0)
	{
		//delete department from module department
		DB::table('module_department')->where('department_id',$id);
		//delete department form user table
		DB::table('users')->where('department_id',$id);
		return true;
	}

	

}
