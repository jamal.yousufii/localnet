<?php namespace App\models;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use DB;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Auth;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password','remember_token');


	//get all users
	public static function getAll($department=0)
	{
		$table = DB::table('users');

        $table->leftJoin('department', 'department.id', '=', 'users.department_id');
        $table->leftJoin('position', 'position.id', '=', 'users.position_id');
        //$table->whereIn('user_id',array(1,2,3));
        $table->select('users.*', 'department.name AS dep_name','position.title AS position');
        //check if department is not zero
        if($department!=0)
        {
        	$table->where("department_id",$department);
        }
        
        $object = $table->get();

        return $object;
	}
	//get data for datatable
	public static function getData()
	{
		$object = DB::table('users AS u');

        $object->leftJoin('department AS d', 'd.id', '=', 'u.department_id');
        //->leftJoin('position AS p', 'p.id', '=', 'u.position_id')
        //$table->whereIn('user_id',array(1,2,3));
        $object->select(
        				'u.id',
        				'u.first_name AS full_name',
        				//DB::raw('CONCAT(u.first_name," ",u.last_name AS full_name'),
        				'u.father_name',
        				'u.username',
        				'u.email',
        				'u.position_id AS position',
        				'd.name AS dep_name'
        				);
        if(Auth::user()->is_manager == 1 && Auth::user()->is_admin == 0)
        {
        		//get all my sub departments
        		$subDepIds 		= getAllSubDepartmentsWithParent(Auth::user()->department_id);
        		$object->whereIn('u.department_id',$subDepIds);
        }
        
		$result = $object->get();
		
		return $result;

	}
	//get user details by id
	public static function getUserDetails($id)
	{
		$object = DB::table('users');
		$object->where('id',$id);
		if(Auth::user()->is_manager == 1 && Auth::user()->is_admin == 0)
        {
        		//get all my sub departments
        		$subDepIds 		= getAllSubDepartmentsWithParent(Auth::user()->department_id);
        		$object->whereIn('department_id',$subDepIds);
        }
		$object = $object->get();
		return $object;
	}
	//insert user modules
	public static function insertUserRelated($user_table='',$data=array())
	{
		DB::table($user_table)->insert($data);
	}
	//remove user related
	public static function removeUserRelated($user_table='',$id=0)
	{
		DB::table($user_table)->where('user_id',$id)->delete();
	}
	//get user related roles
	public static function getUserRelated($table_name='',$user_id=0)
	{
		return DB::table($table_name)->where('user_id',$user_id)->get();
	}
	//get user left menu
	public static function getUserLeft($userid=0,$module='')
	{
		$lang = getLangShortcut();
		
		$table = DB::table('user_entity AS ue');

		$table->select('e.name_'.$lang.' AS seciton_name','e.code AS section_code','e.*');
		$table->leftJoin('entity AS e','e.id','=','ue.entity_id');
		$table->leftJoin('module AS m','m.id','=','e.module_id');
		$table->where('m.code',$module);
		$table->where('ue.user_id',$userid);
		$table->where('e.status',1);
		$table->orderBy('e.item_order');
		$table->orderBy('e.id');
		$object = $table->get();
		return $object;
	}
	
	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

}
