<?php

namespace App\models\docs_archive;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Session;
use Input;

class docsModel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

	// upload the files.
	public static function uploadFiles($data)
	{
		$uploaded = DB::connection('docs_archive')->table('uploads')->insert($data);
		if($uploaded){return true;}else{return false;}
	}
	public static function getArchivedDocuments()
	{
		$table = DB::connection('docs_archive')->table('docs as d');
		$table->select('d.*','dt.name as document_type','intdt.name as internal_sender_related_doc_type','srs.sender as sender','exec.name as execution','srs_isrdep.internal_sender_related_deputy','srs_isrdir.internal_sender_related_directorate');
		$table->leftjoin('document_type as dt','dt.id','=','d.doc_type');
		$table->leftjoin('document_type as intdt','intdt.id','=','d.doc_type');
		$table->leftjoin('sources as srs','d.sender','=','srs.id');
		$table->leftjoin('execution as exec','d.execution','=','exec.id');
		$table->leftjoin('sources as srs_isrdep','d.internal_sender_related_deputy','=','srs_isrdep.id');
		$table->leftjoin('sources as srs_isrdir','d.internal_sender_related_directorate','=','srs_isrdir.id');
		$table->where('d.deleted',0);
		$table->orderBy('d.id','desc');
		if($table->count()>0){
			return $object = $table->paginate(20);
		}else{
			return "";
		}
	}

	// get search result based on send field values.
	public static function getSearchedData($print="")
	{
		$sender = Input::get('sender');
		$doc_number = Input::get('doc_number');
		$doc_date = toGregorian(ymd_format(Input::get('doc_date')));
		$doc_type = Input::get('doc_type');
		$internal_sender_related_deputy = Input::get('internal_sender_related_deputy');
		$internal_sender_related_directorate = Input::get('internal_sender_related_directorate');
		$internal_sender_related_doc_number = Input::get('internal_sender_related_doc_number');
		$internal_sender_related_doc_date = toGregorian(ymd_format(Input::get('internal_sender_related_doc_date')));
		$internal_sender_related_doc_type = Input::get('internal_sender_related_doc_type');
		$summary = Input::get('summary');
		$committee_comment = Input::get('committee_comment');
		$incoming_number = Input::get('incoming_number');
		$incoming_date = toGregorian(ymd_format(Input::get('incoming_date')));
		$execution = Input::get('execution');
		$hedayat_number = Input::get('hedayat_number');
		$hedayat_muqam_aali = Input::get('hedayat_muqam_aali');
		$execution_date = toGregorian(ymd_format(Input::get('execution_date')));
		$considerations = Input::get('considerations');

		$table = DB::connection('docs_archive')->table('docs as d');
		$table->select('d.*','dt.name as document_type','intdt.name as internal_sender_related_doc_type','srs.sender as sender','exec.name as execution','srs_isrdep.internal_sender_related_deputy','srs_isrdir.internal_sender_related_directorate');
		$table->leftjoin('document_type as dt','dt.id','=','d.doc_type');
		$table->leftjoin('document_type as intdt','intdt.id','=','d.doc_type');
		$table->leftjoin('sources as srs','d.sender','=','srs.id');
		$table->leftjoin('execution as exec','d.execution','=','exec.id');
		$table->leftjoin('sources as srs_isrdep','d.internal_sender_related_deputy','=','srs_isrdep.id');
		$table->leftjoin('sources as srs_isrdir','d.internal_sender_related_directorate','=','srs_isrdir.id');

		if($sender != "")
		{
			$table->where('d.sender','=',$sender);
		}
		if($doc_number != "")
		{
			$table->where('d.doc_number','=',$doc_number);
		}
		if($doc_date != "")
		{
			$table->where('d.doc_date','=',$doc_date);
		}
		if($doc_type != "")
		{
			$table->where('d.doc_type','=',$doc_type);
		}
		if($internal_sender_related_deputy != "")
		{
			$table->where('d.internal_sender_related_deputy',$internal_sender_related_deputy);
		}
		if($internal_sender_related_directorate != "")
		{
			$table->where('d.internal_sender_related_directorate',$internal_sender_related_directorate);
		}
		if($internal_sender_related_doc_number != "")
		{
			$table->where('d.internal_sender_related_doc_number',$internal_sender_related_doc_number);
		}
		if($internal_sender_related_doc_date != "")
		{
			$table->where('d.internal_sender_related_doc_date',$internal_sender_related_doc_date);
		}
		if($internal_sender_related_doc_type != "")
		{
			$table->where('d.internal_sender_related_doc_type',$internal_sender_related_doc_type);
		}
		if($summary != "")
		{
			$table->where('d.summary','like','%'.$summary.'%');
		}
		if($incoming_number != "")
		{
			$table->where('d.incoming_number',$incoming_number);
		}
		if($incoming_date != "")
		{
			$table->where('d.incoming_date',$incoming_date);
		}
		if($committee_comment != "")
		{
			$table->where('d.committee_comment','like','%'.$committee_comment.'%');
		}
		if($execution != "")
		{
			$table->where('d.execution',$execution);
		}
		if($hedayat_number != "")
		{
			$table->where('d.hedayat_number','=',$hedayat_number);
		}
		if($hedayat_muqam_aali != "")
		{
			$table->where('d.hedayat_muqam_aali','like','%'.$hedayat_muqam_aali.'%');
		}
		if($execution_date != "")
		{
			$table->where('d.execution_date',$execution_date);
		}
		if($considerations != "")
		{
			$table->where('d.considerations','like','%'.$considerations.'%');
		}
		$table->where('d.deleted',0);
		$table->orderBy('d.doc_date','desc');
		//dd($table->tosql());
		if($table->count()>0)
		{
			if($print == "")
				return $object = $table->paginate(20);
			else
				return $object = $table->get();
		}
		else
			return "";
	}

	//insert the document
	public static function addRecord($data)
	{
		//DB::enableQueryLog();
		$insertedID = DB::connection('docs_archive')->table('docs')->insertGetID($data);
		return $insertedID;
	}
	//update the document
	public static function updateRecord($data,$record_id)
	{
		$updated = DB::connection('docs_archive')->table('docs')->where('id',$record_id)->update($data);
		if($updated) return true;
		else return false;
	}

	public static function getDocDetails($doc_id)
	{
		$doc_details = DB::connection('docs_archive')->table('docs')->select('*')->where('id', $doc_id)->first();
		if($doc_details != ""){ return $doc_details;}
		else{ return "";}
	}
	// get delete the specific photo based on id.
	public static function getDeletePhoto($record_id)
	{
		$deleted = DB::connection('docs_archive')->table('photo_details')->where('id', $record_id)->update(array('deleted' => 1));
		if($deleted){ return true; } else{ return false; }
	}
	//get file name based on sent parameters.
	public static function getFileName($file_id)
	{
		$object = DB::connection('docs_archive')->table("uploads")->where("id", $file_id)->first();
		if($object){return $object;}
		else return false;
	}
	//get file name based on sent parameters.
	public static function getDocFilesName($doc_id)
	{
		$object = DB::connection('docs_archive')->table("uploads")->where("doc_id", $doc_id)->get();
		if($object){return $object;}
		else return false;
	}
	public static function removeFile($file_id)
	{
		$deleted = DB::connection('docs_archive')->table("uploads")->where("id", $file_id)->delete();
		if($deleted){return $deleted;}
		else{return false;}	
	}

	public static function deleteDocInfo($record_id)
	{
		$deleted = DB::connection('docs_archive')->table('docs')->where('id', $record_id)->update(array('deleted' => 1));
		if($deleted) return true; else return false;
	}
	public static function removeDocFiles($doc_id)
	{
		$deleted = DB::connection('docs_archive')->table('uploads')->where('doc_id', $doc_id)->update(array('deleted' => 1));
		if($deleted) return true; else return false;
	}
	// add department log details.
	public static function addLog($data)
	{
		$table = DB::connection('docs_archive')->table('log')->insert($data);
	}

	// ================================================== Document Type Management Model Functions ============================

	public static function getDocumentType()
	{
		$doc_type = DB::connection('docs_archive')->table('document_type')->get();
		if($doc_type){ return $doc_type; } else{ return false; }
	}
	public static function addDocTypeRecord($data)
	{
		$added = DB::connection('docs_archive')->table('document_type')->insert($data);
		if($added) return true;
		else return false;
	}
	public static function updateDocType($id, $data)
	{
		$updated = DB::connection('docs_archive')->table('document_type')->where('id', $id)->update($data);
		if($updated) return true;
		else return false;
	}
	public static function getDocType($id)
	{
		$object = DB::connection('docs_archive')->table("document_type")->where("id", $id)->first();
		if($object){return $object;}
		else return false;
	}
	public static function deleteDocType($id)
	{
		$deleted = DB::connection('docs_archive')->table('document_type')->where('id', $id)->delete();
		if($deleted) return true; else return false;
	}
	// ================================================== Document Category Management Model Functions ============================

	public static function getSources()
	{
		$doc_cat = DB::connection('docs_archive')->table('sources')->get();
		if($doc_cat){ return $doc_cat; } else{ return false; }
	}
	public static function addDocSource($data)
	{
		$added = DB::connection('docs_archive')->table('sources')->insert($data);
		if($added) return true;
		else return false;
	}
	public static function updateDocSource($id, $data)
	{
		$updated = DB::connection('docs_archive')->table('sources')->where('id', $id)->update($data);
		if($updated) return true;
		else return false;
	}
	public static function getDocSource($id)
	{
		$object = DB::connection('docs_archive')->table("sources")->where("id", $id)->first();
		if($object){return $object;}
		else return false;
	}
	public static function deleteDocSource($id)
	{
		$deleted = DB::connection('docs_archive')->table('sources')->where('id', $id)->delete();
		if($deleted) return true; else return false;
	}
}
