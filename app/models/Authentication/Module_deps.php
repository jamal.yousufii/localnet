<?php
namespace App\models\Authentication;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module_deps extends Model
{
    use SoftDeletes;

    function department()
    {
        return $this->belongsTo('App\models\Authentication\Departments','department_id','id');
    }
  
    function module()
    {
        return $this->belongsTo('App\models\Authentication\Modules','module_id','id');
    }

}

?>