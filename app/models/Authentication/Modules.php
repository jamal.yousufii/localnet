<?php
namespace App\models\Authentication;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modules extends Model
{
  use SoftDeletes;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['code','name_dr','name_pa','name_en','url','description'];

  function module_deps()
  {
    return $this->hasMany('App\models\Authentication\Module_deps','module_id','id');
  }
}

?>
