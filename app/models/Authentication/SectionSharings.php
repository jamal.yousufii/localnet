<?php
namespace App\models\Authentication;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class SectionSharings extends Model
{
  function allowedSections()
  {
    return $this->belongsTo('App\models\Authentication\Sections','sections','id');
  }

  function section_deps()
  {
    return $this->hasMany('App\models\Authentication\Section_deps','sections','id');
  }

  // Get the Department 
  public function departments()
  {
    return $this->belongsToMany('App\models\Authentication\Departments','section_deps','sections','department_id');
  }

}

?>
