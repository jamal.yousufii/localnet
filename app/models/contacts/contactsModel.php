<?php

namespace App\models\contacts;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Session;
use Input;

class contactsModel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'hr';
	public static $myDb 	= "hr";
	protected $table 		= 'employees';


	public static function getData($lang = 'dr')
	{

		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							//'dep.name AS department',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							//'t1.name_dr AS name',
							't1.current_position_dr',
							't1.phone',
							't1.ext1',
							't1.ext2',
							't1.ext3'
							);
		$table->where('tashkil_id','!=',0);
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$object = $table->groupBy('t1.department');
		$object = $table->orderBy('t1.id');
		//echo "<pre>";$object->tosql($object->get());exit;
		return $object->get();
	}

	// get the contacs based on id;
	public static function getContacts($id)
	{
		
		$object = DB::connection("hr")
			->table('employees')
			->select(array('id','phone','email','ext1','ext2','ext3'))
			->where('id', '=', $id)
			->get();
		//echo $object->tosql();exit;
		return $object;
	}

	public static function updateData($id, $data)
	{

		$object = DB::connection("hr")
			->table('employees')
			->where('id', '=', $id)
			->update($data);
		//echo $object->tosql();exit;
		return $object;
	}

	//get search result
	public static function getSearchResult()
	{

		//echo Input::get('general_dep')." ".Input::get('sub_dep');exit;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.name_dr AS name',
							't1.current_position_dr',
							't1.ext2',
							't1.ext1',
							't1.ext3',
							't1.phone'
							);

		$general_dep = Session::get('general_dep');
		$sub_dep = Session::get('sub_dep');

		if($general_dep != '' && $sub_dep == '')
		{
			$table->where('t1.general_department','=', $general_dep);
		}
		elseif($general_dep != '' && $sub_dep != '')
		{
			$table->where('t1.department','=', $sub_dep);
		}
		//--- search criteria ------------------------------------------//

		$object = $table->orderBy('t1.id');
		//$table->paginate(1);
		//$object = $table->get();
		//echo "<pre>";print_r(tosql($object));exit;
		return $object->get();
	}

}