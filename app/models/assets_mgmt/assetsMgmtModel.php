<?php

namespace App\models\assets_mgmt;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Session;
use Input;

class assetsMgmtModel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'purchase';
	protected $connection 	= 'assets_mgmt';
	public static $myDb 	= "assets_mgmt";

	// =========================================================================== Notification Model Functions ===========================================================
	
	public static function addNotification($notification_data)
	{
		$table = DB::connection('assets_mgmt')->table('notification')->insert($notification_data);
	}
	public static function updateNotification($notification_id)
	{
		$visited = DB::connection("assets_mgmt")->table("notification")->where('id', $notification_id)->update(array("is_viewed" => 1));
	}
	// =========================================================================== Authorize or Reject Record Model Functions ===========================================================
	
	public static function authorizeAssetRecord($table,$record_id)
	{
		$updated = DB::connection('assets_mgmt')->table($table)->where('id', $record_id)->update(array('authorized' => 1));
		if($updated){ return true; } else{ return false;}
	}
	public static function getRejectAssetRecord($table,$record_id, $reject_data)
	{
		$updated = DB::connection('assets_mgmt')->table($table)->where('id', $record_id)->update($reject_data);
		if($updated){ return true; } else{ return false;}
	}

    // =========================================================================== Assets Item Management Model Functions ===========================================================

	public static function getAssetsNature()
	{
		$table = DB::connection('assets_mgmt')->table('asset_nature');
		$table->where('deleted',0);
		$table->orderBy('id','desc');
		if($table->count()>0){
			return $object = $table->get();
		}else{
			return "";
		}
	}

	//add asset nature
	public static function addAssetNature($data)
	{
		//DB::enableQueryLog();
		$inserted = DB::connection('assets_mgmt')->table('asset_nature')->insertGetID($data);
		return $inserted;
	}

	public static function getMainItems()
	{
		$table = DB::connection('assets_mgmt')->table('main_item');
		$table->where('deleted',0);
		$table->orderBy('id','desc');
		if($table->count()>0){
			return $object = $table->get();
		}else{
			return "";
		}
	}

	public static function getAssetsType()
	{
		$table = DB::connection('assets_mgmt')->table('asset_type');
		$table->where('deleted',0);
		$table->orderBy('id','desc');
		if($table->count()>0){
			return $object = $table->get();
		}else{
			return "";
		}
	}

	//add asset type
	public static function addAssetType($data)
	{
		//DB::enableQueryLog();
		$inserted = DB::connection('assets_mgmt')->table('asset_type')->insertGetID($data);
		return $inserted;
	}

	public static function getMainItemCode($main_item_id)
	{
		$main_item_code = DB::connection('assets_mgmt')->table('main_item')->where('id',$main_item_id)->where('deleted',0)->pluck('code');
		if($main_item_code){
			return $main_item_code;
		}else{
			return "";
		}
	}

	public static function getSubItemCode($sub_item_id)
	{
		$sub_item_code = DB::connection('assets_mgmt')->table('sub_item')->where('id',$sub_item_id)->where('deleted',0)->pluck('code');
		if($sub_item_code){
			return $sub_item_code;
		}else{
			return "";
		}
	}

	public static function getEndItemCode($end_item_id)
	{
		$end_item_code = DB::connection('assets_mgmt')->table('end_item')->where('id',$end_item_id)->where('deleted',0)->pluck('code');
		if($end_item_code){
			return $end_item_code;
		}else{
			return "";
		}
	}

	public static function getSubItems()
	{
		$table = DB::connection('assets_mgmt')->table('sub_item');
		$table->where('deleted',0);
		$table->orderBy('id','desc');
		if($table->count()>0){
			return $object = $table->get();
		}else{
			return "";
		}
	}

	public static function getEndItems()
	{
		$table = DB::connection('assets_mgmt')->table('end_item');
		$table->where('deleted',0);
		$table->orderBy('id','desc');
		if($table->count()>0){
			return $object = $table->get();
		}else{
			return "";
		}
	}

	public static function getItemDetail()
	{
		$table = DB::connection('assets_mgmt')->table('item_detail');
		$table->where('deleted',0);
		$table->orderBy('id','desc');
		if($table->count()>0){
			return $object = $table->get();
		}else{
			return "";
		}
	}

	public static function getAssetSanctions()
	{
		$table = DB::table('assets_management.sanction as s')->select("s.*","dept.name as department");
		$table->leftjoin("auth.department as dept","dept.id","=","s.department_id");
		$table->where('s.deleted',0)->orderBy('s.id','desc');
		if($table->count()>0){
			return $object = $table->get();
		}else{
			return "";
		}
	}

	public static function addSanction($data)
	{
		//DB::enableQueryLog();
		$inserted = DB::connection('assets_mgmt')->table('sanction')->insertGetID($data);
		return $inserted;
	}

	public static function deleteSanction($table, $record_id)
	{
		$deleted = DB::connection('assets_mgmt')->table($table)->where('id', $record_id)->update(array('deleted' => 1));
		if($deleted) return true; else return false;
	}

	public static function getAssetVendors()
	{
		$table = DB::connection('assets_mgmt')->table('vendor')->where('deleted',0)->orderBy('id','desc');
		if($table->count()>0){
			return $object = $table->get();
		}else{
			return "";
		}
	}

	public static function addVendor($data)
	{
		//DB::enableQueryLog();
		$inserted = DB::connection('assets_mgmt')->table('vendor')->insertGetID($data);
		return $inserted;
	}

	public static function deleteVendor($table, $record_id)
	{
		$deleted = DB::connection('assets_mgmt')->table($table)->where('id', $record_id)->update(array('deleted' => 1));
		if($deleted) return true; else return false;
	}
	public static function addItems($table,$data)
	{
		//DB::enableQueryLog();
		$inserted = DB::connection('assets_mgmt')->table($table)->insertGetID($data);
		return $inserted;
	}
	public static function deleteItem($record_id,$table)
	{
		$deleted = DB::connection('assets_mgmt')->table($table)->where('id', $record_id)->update(array('deleted' => 1));
		if($deleted) return true; else return false;
	}
	// add department log details.
	public static function addLog($data)
	{
		$table = DB::connection('assets_mgmt')->table('log')->insert($data);
	}

	public static function getAssetLocations()
	{
		$table = DB::table('assets_management.location as l')->select("l.*","g_dept.name as general_dept","s_dept.name as sub_dept");
		$table->leftjoin("auth.department as g_dept","g_dept.id","=","l.general_dept");
		$table->leftjoin("auth.department as s_dept","s_dept.id","=","l.sub_dept");
		$table->where('l.deleted',0)->orderBy('l.id','desc');
		if($table->count()>0){return $object = $table->get();}
		else{return "";}
	}
	//insert the document
	public static function addLocation($data)
	{
		//DB::enableQueryLog();
		$inserted = DB::connection('assets_mgmt')->table("location")->insertGetID($data);
		return $inserted;
	}
	public static function deleteLocation($table,$location_id)
	{
		$deleted = DB::connection('assets_mgmt')->table($table)->where('id', $location_id)->update(array('deleted' => 1));
		if($deleted) return true; 
		else return false;
	}

	public static function getAssetCurrency()
	{
		$table = DB::connection('assets_mgmt')->table('currency')->select("*");
		$table->where('deleted',0)->orderBy('id','desc');
		if($table->count()>0){return $object = $table->get();}
		else{return "";}
	}
	//insert the document
	public static function addCurrency($data)
	{
		//DB::enableQueryLog();
		$inserted = DB::connection('assets_mgmt')->table("currency")->insertGetID($data);
		return $inserted;
	}
	public static function deleteCurrency($table,$record_id)
	{
		$deleted = DB::connection('assets_mgmt')->table($table)->where('id', $record_id)->update(array('deleted' => 1));
		if($deleted) return true; 
		else return false;
	}

	public static function getAssetStoreKeeper()
	{
		$table = DB::connection('assets_mgmt')->table('store_keeper')->select("*");
		$table->where('deleted',0)->orderBy('id','desc');
		if($table->count()>0){return $object = $table->get();}
		else{return "";}
	}
	//insert the document
	public static function addStoreKeeper($data)
	{
		//DB::enableQueryLog();
		$inserted = DB::connection('assets_mgmt')->table("store_keeper")->insertGetID($data);
		return $inserted;
	}
	public static function deleteStoreKeeper($table,$record_id)
	{
		$deleted = DB::connection('assets_mgmt')->table($table)->where('id', $record_id)->update(array('deleted' => 1));
		if($deleted) return true; 
		else return false;
	}

	// =================================================================== Purchase Model Functions =====================================

	public static function getAllPurchases()
	{
		$table = DB::connection('assets_mgmt')->table('purchase')->where('deleted',0)->orderBy('id','desc');
		if($table->count()>0){
			return $object = $table->paginate(10);
		}else{
			return "";
		}
	}
	public static function getItemDetailInformation($item_detail_id)
	{
		$table = DB::connection('assets_mgmt')->table('item_detail as itd');
		$table->select("itd.item_detail_name","mi.main_item_name as main_item","si.sub_item_name as sub_item","ei.end_item_name as end_item");
		$table->leftjoin("main_item as mi","mi.main_item_code","=","itd.main_item_code");
		$table->leftjoin("sub_item as si","si.sub_item_code","=","itd.sub_item_code");
		$table->leftjoin("end_item as ei","ei.end_item_code","=","itd.end_item_code");
		$table->where("itd.id",$item_detail_id)->where("mi.deleted",0)->where("si.deleted",0)->where("ei.deleted",0)->where('itd.deleted',0);
		if($table->count()>0){
			return $object = $table->get();
		}else{
			return "";
		}
	}
	public static function addPurchaseData($data)
	{
		//DB::enableQueryLog();
		$inserted = DB::connection('assets_mgmt')->table("purchase")->insertGetID($data);
		return $inserted;
	}
	public static function getPurchaseDetails($purchase_id)
	{
		$purchase_details = DB::connection('assets_mgmt')->table('purchase')->where('id', $purchase_id)->where('deleted',0)->first();
		if($purchase_details){ return $purchase_details; } else{ return "";}
	}
	public static function deletePurchase($purchase_id)
	{
		$deleted = DB::connection('assets_mgmt')->table("purchase")->where('id', $purchase_id)->update(array('deleted' => 1));
		if($deleted) return true; else return false;
	}
	// =================================================================== Purchase Inward Model Functions =====================================
	public static function addInward($data)
	{
		$inserted = DB::connection('assets_mgmt')->table("inward")->insertGetID($data);
		return $inserted;
	}
	public static function getAllPurchaseInwards()
	{
		$table = DB::connection('assets_mgmt')->table('inward as i');
		$table->select("i.*","p.item_detail_id");
		$table->leftjoin("purchase as p","i.purchase_id","=","p.id")->where("i.deleted",0)->where("p.deleted",0);
		if($table->count()>0){return $table->paginate(10);}else{return "";}
	}
	public static function deleteInward($inward_id)
	{
		$deleted = DB::connection('assets_mgmt')->table("inward")->where('id', $inward_id)->update(array('deleted' => 1));
		if($deleted) return true; else return false;
	}
	public static function getInwardDetails($inward_id)
	{
		$inward_details = DB::connection('assets_mgmt')->table('inward')->where('id', $inward_id)->where('deleted',0)->first();
		if($inward_details){ return $inward_details; } else{ return "";}
	}
	public static function updateInward($inward_id, $inward_data)
	{
		$updated = DB::connection('assets_mgmt')->table('inward')->where('id', $inward_id)->update($inward_data);
		if($updated){ return true; } else{ return false;}
	}
	// =================================================================== Asset Identification Model Functions =====================================
	public static function getAllAssetIdentities()
	{
		$table = DB::table('assets_management.asset_identification as ai')->select('ai.*','dept.name as item_location');
		$table->leftjoin("auth.department as dept","dept.id","=","ai.item_location");
		$table->where("ai.deleted",0)->orderBy('ai.id','desc');
		if($table->count()>0){return $table->paginate(10);}else{return "";}
	}
	public static function addAssetIdentityData($data)
	{
		$inserted = DB::connection('assets_mgmt')->table("asset_identification")->insertGetID($data);
		return $inserted;
	}
	public static function getAssetIdentityDetails($asset_identity_id)
	{
		$asset_identity_details = DB::connection('assets_mgmt')->table('asset_identification')->where('id', $asset_identity_id)->where('deleted',0)->first();
		if($asset_identity_details){ return $asset_identity_details; } else{ return "";}
	}
	public static function updateAssetIdentityData($asset_identity_id, $asset_identity_data)
	{
		$updated = DB::connection('assets_mgmt')->table('asset_identification')->where('id', $asset_identity_id)->update($asset_identity_data);
		if($updated){ return true; } else{ return false;}
	}
	public static function deleteAssetIdentity($asset_identity_id)
	{
		$deleted = DB::connection('assets_mgmt')->table("asset_identification")->where('id', $asset_identity_id)->update(array('deleted' => 1));
		if($deleted) return true; else return false;
	}
	// =================================================================== Asset Allotment Model Functions =====================================
	public static function getAllAssetAllots()
	{
		$table = DB::table('assets_management.allotment as al')->select("al.*","g_dept.name as general_dept","s_dept.name as sub_dept","emp.name_dr as allottee_name","s.name as sanction_by");
		$table->leftjoin("auth.department as g_dept","g_dept.id","=","al.general_dept");
		$table->leftjoin("auth.department as s_dept","s_dept.id","=","al.sub_dept");
		$table->leftjoin("hr.employees as emp","emp.id","=","al.allottee_name");
		$table->leftjoin("assets_management.sanction as s","s.id","=","al.sanction_by");
		$table->where("al.deleted",0)->orderBy('al.id','desc');
		if($table->count()>0){return $table->paginate(10);}else{return "";}
	}
	public static function addAssetAllotData($data)
	{
		$inserted = DB::connection('assets_mgmt')->table("allotment")->insertGetID($data);
		return $inserted;
	}
	public static function getAssetAllotDetails($asset_allot_id)
	{
		$asset_allot_details = DB::connection('assets_mgmt')->table('allotment')->where('id', $asset_allot_id)->where('deleted',0)->first();
		if($asset_allot_details){ return $asset_allot_details; } else{ return "";}
	}
	public static function updateAssetAllotData($asset_allot_id, $asset_allot_data)
	{
		$updated = DB::connection('assets_mgmt')->table('allotment')->where('id', $asset_allot_id)->update($asset_allot_data);
		if($updated){ return true; } else{ return false;}
	}
	public static function deleteAssetAllot($asset_allot_id)
	{
		$deleted = DB::connection('assets_mgmt')->table("allotment")->where('id', $asset_allot_id)->update(array('deleted' => 1));
		if($deleted) return true; else return false;
	}
	// =================================================================== Asset Allotment Return Model Functions =====================================
	public static function getAllAssetAllotmentReturn()
	{
		$table = DB::table('assets_management.allotment_return as alr')->select("alr.*","g_dept.name as general_dept","s_dept.name as sub_dept","emp.name_dr as from_person","strk.name as received_by","s.name as sanction_by");
		$table->leftjoin("auth.department as g_dept","g_dept.id","=","alr.general_dept");
		$table->leftjoin("auth.department as s_dept","s_dept.id","=","alr.sub_dept");
		$table->leftjoin("hr.employees as emp","emp.id","=","alr.from_person");
		$table->leftjoin("assets_management.store_keeper as strk","strk.id","=","alr.received_by");
		$table->leftjoin("assets_management.sanction as s","s.id","=","alr.sanction_by");
		$table->where("alr.deleted",0)->orderBy('alr.id','desc');
		if($table->count()>0){return $table->paginate(10);}
		else{return "";}
	}
	public static function addAssetAllotmentReturnData($data)
	{
		$inserted = DB::connection('assets_mgmt')->table("allotment_return")->insertGetID($data);
		return $inserted;
	}
	public static function getAssetAllotmentReturnDetails($allotment_return_id)
	{
		$allotment_return = DB::connection('assets_mgmt')->table('allotment_return')->where('id', $allotment_return_id)->where('deleted',0)->first();
		if($allotment_return){ return $allotment_return; } 
		else{ return "";}
	}
	public static function updateAllotmentReturnData($allotment_return_id, $allotment_return_data)
	{
		$updated = DB::connection('assets_mgmt')->table('allotment_return')->where('id', $allotment_return_id)->update($allotment_return_data);
		return $updated;
	}
	public static function deleteAssetAllotmentReturn($allotment_return_id)
	{
		$deleted = DB::connection('assets_mgmt')->table("allotment_return")->where('id', $allotment_return_id)->update(array('deleted' => 1));
		if($deleted) return true; 
		else return false;
	}
	// =================================================================== Asset Allotment to Storekeeper Model Functions =====================================
	public static function getAllAssetAllotmentToStoreKeeper()
	{
		$table = DB::table('assets_management.allotment_to_storekeeper as altsk')->select("altsk.*","s.name as sanction_by");
		$table->leftjoin("assets_management.sanction as s","s.id","=","altsk.sanction_by");
		$table->where("altsk.deleted",0)->orderBy('altsk.id','desc');
		if($table->count()>0){return $table->paginate(10);}
		else{return "";}
	}
	public static function addAssetAllotmentToStoreKeeperData($data)
	{
		$inserted = DB::connection('assets_mgmt')->table("allotment_to_storekeeper")->insertGetID($data);
		return $inserted;
	}
	public static function getAllotmentToStorekeeperDetails($allotment_to_storekeeper_id)
	{
		$allotment_to_storekeeper = DB::connection('assets_mgmt')->table('allotment_to_storekeeper')->where('id', $allotment_to_storekeeper_id)->where('deleted',0)->first();
		if($allotment_to_storekeeper){ return $allotment_to_storekeeper; } 
		else{ return "";}
	}
	public static function updateAllotmentToStoreKeeperData($allotment_to_storekeeper_id, $allotment_to_storekeeper_data)
	{
		$updated = DB::connection('assets_mgmt')->table('allotment_to_storekeeper')->where('id', $allotment_to_storekeeper_id)->update($allotment_to_storekeeper_data);
		return $updated;
	}
	public static function deleteAssetAllotmentToStoreKeeper($allotment_to_storekeeper_id)
	{
		$deleted = DB::connection('assets_mgmt')->table("allotment_to_storekeeper")->where('id', $allotment_to_storekeeper_id)->update(array('deleted' => 1));
		if($deleted) return true; 
		else return false;
	}
	// =================================================================== Asset Allotment to Individual Model Functions =====================================
	public static function getAllAssetAllotmentToIndividual()
	{
		$table = DB::table('assets_management.allotment_to_individual as altin')->select("altin.*","g_dept.name as general_dept","s_dept.name as sub_dept","emp.name_dr as allottee_name","s.name as sanction_by");
		$table->leftjoin("auth.department as g_dept","g_dept.id","=","altin.general_dept");
		$table->leftjoin("auth.department as s_dept","s_dept.id","=","altin.sub_dept");
		$table->leftjoin("hr.employees as emp","emp.id","=","altin.allottee_name");
		$table->leftjoin("assets_management.sanction as s","s.id","=","altin.sanction_by");
		$table->where("altin.deleted",0)->orderBy('altin.id','desc');
		if($table->count()>0){return $table->paginate(10);}
		else{return "";}
	}
	public static function addAssetAllotmentToIndividualData($data)
	{
		$inserted = DB::connection('assets_mgmt')->table("allotment_to_individual")->insertGetID($data);
		return $inserted;
	}
	public static function getAllotmentToIndividualDetails($allotment_to_individual_id)
	{
		$allotment_to_Individual = DB::connection('assets_mgmt')->table('allotment_to_individual')->where('id', $allotment_to_individual_id)->where('deleted',0)->first();
		if($allotment_to_Individual){ return $allotment_to_Individual; } 
		else{ return "";}
	}
	public static function updateAllotmentToIndividualData($allotment_to_individual_id, $allotment_to_individual_data)
	{
		$updated = DB::connection('assets_mgmt')->table('allotment_to_individual')->where('id', $allotment_to_individual_id)->update($allotment_to_individual_data);
		return $updated;
	}
	public static function deleteAssetAllotmentToIndividual($allotment_to_individual_id)
	{
		$deleted = DB::connection('assets_mgmt')->table("allotment_to_individual")->where('id', $allotment_to_individual_id)->update(array('deleted' => 1));
		if($deleted) return true; 
		else return false;
	}

}
