<?php

namespace App\models\docscom;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class Docscom extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'docscom';
	public static $myDb 	= "docscom";
	protected $table 		= 'docs';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('docs AS t1');
					$table->select(
							't1.id as doc_id',
							't1.barcode',
							't1.external_number',
							't1.internal_number',
							't1.internal_date',
							'dep.name AS executive_department',
							't1.organization',
							'u.first_name AS approved_by',
							//DB::raw('CONCAT(DATEDIFF(max(l.check_out), min(l.check_in) )," Days") AS days'),
							DB::raw('CONCAT(DATEDIFF(IF(t1.status=0,NOW(),max(l.check_out)), min(l.check_in) )," Days") AS days'),
							DB::raw(
								'(CASE 
							        WHEN t1.status = "0" THEN "تحت پروسیس"
							        WHEN t1.status = "1" THEN "تایید شده"
							        WHEN t1.status = "2" THEN "رد شده"
							        WHEN t1.status = "3" THEN "حفظ شده"
							        WHEN t1.status = "4" THEN "صادر شده"
							        ELSE 1
							    END) AS status'
								) 
							);
							
				$table->leftjoin("auth.department AS dep","dep.id","=","t1.executive_department");
				$table->leftjoin("auth.users AS u","u.id","=","t1.approved_by");
				$table->leftjoin("log AS l","l.doc_id","=","t1.id");
				$table->where('t1.deleted',0);
				
				$table->groupBy('t1.id');
				$table->take(25000);
				
				if(!allComments('docscom_docscom'))
				{
					//get my sub deparment id
					$ids = getSubDepartmentIds(getMyDepartmentId(),true);

					
					$table->whereIn('t1.executive_department',$ids);
					$table->orWhere('t1.user_id',Auth::user()->id);

					
					if(isConfidential('docscom_docscom') && !isNonConfidential('docscom_docscom'))
					{
						$table->where('t1.access_type',1);
					}
					elseif(isNonConfidential('docscom_docscom') && !isConfidential('docscom_docscom'))
					{
						$table->where('t1.access_type',2);
					}
					elseif(isConfidential('docscom_docscom') && isNonConfidential('docscom_docscom'))
					{
						$table->whereIn('t1.access_type',array(1,2));
					}
				}

		$object = $table->orderBy('t1.id','DESC');
		//echo $object->tosql();
		return $object->get();
	}

	//get search result
	public static function getSearchResult($params = array())
	{

		$from = "";
		$to = "";
		if(Input::get('start_date') != '')
		{
			if(isMiladiDate())
			{
				$from = Input::get('start_date');
			}
			else
			{
				$from = ymd_format(Input::get('start_date'));
				$from = convertToGregorian($from);
			}
		}
		if(Input::get('end_date') != '')
		{
			if(isMiladiDate())
			{
				$to = Input::get('end_date');
			}
			else
			{
				$to = ymd_format(Input::get('end_date'));
				$to = convertToGregorian($to);
			}
		}


        //$parties = Party::whereBetween('date', array($from, $to))->get();

		$table = DB::connection(self::$myDb)
					->table('docs AS t1');
					$table->select(
							't1.id',
							't1.barcode',
							't1.external_number',
							't1.internal_number',
							't1.internal_date',
							'dep.name AS executive_department',
							't1.organization',
							't1.doc_summary',
							'u.first_name AS approved_by',
							DB::raw('CONCAT(DATEDIFF(IF(t1.status=0,NOW(),max(l.check_out)), min(l.check_in) )," Days") AS days'),
							DB::raw(
								'(CASE 
							        WHEN t1.status = "0" THEN "تحت پروسیس"
							        WHEN t1.status = "1" THEN "تایید شده"
							        WHEN t1.status = "2" THEN "رد شده"
							        WHEN t1.status = "3" THEN "حفظ شده"
							        WHEN t1.status = "4" THEN "صادر شده"
							        ELSE 1
							    END) AS status'
								) 
							);
					$table->leftjoin("auth.department AS dep","dep.id","=","t1.executive_department");
					$table->leftjoin("auth.users AS u","u.id","=","t1.approved_by");
					$table->leftjoin("log AS l","l.doc_id","=","t1.id");
					$table->where('t1.deleted',0);
					$table->groupBy('t1.id');




		if(!allComments('docscom_docscom'))
		{	
			//get my sub deparment id
			$ids = getSubDepartmentIds(getMyDepartmentId(),true);
			
			$table->whereIn('t1.executive_department',$ids);
			//$table->orWhere('t1.user_id',Auth::user()->id);

			
			if(isConfidential('docscom_docscom') && !isNonConfidential('docscom_docscom'))
			{
				$table->where('t1.access_type',1);
			}
			elseif(isNonConfidential('docscom_docscom') && !isConfidential('docscom_docscom'))
			{
				$table->where('t1.access_type',2);
			}
			elseif(isConfidential('docscom_docscom') && isNonConfidential('docscom_docscom'))
			{
				$table->whereIn('t1.access_type',array(1,2));
			}
		}
		//--- search criteria ------------------------------------------//
		if(Input::get('start_date') != '' && Input::get('end_date') == '')
		{
			$table->where('t1.created_at','>=',$from);
		}
		elseif(Input::get('start_date') == '' && Input::get('end_date') != '') 
		{
			$table->where('t1.created_at','<=',$to);
		}
		elseif(Input::get('start_date') != '' && Input::get('end_date') != '') 
		{
			$table->whereBetween('t1.created_at',array($from,$to));
		}

		if(Input::get('condition')!='')
		{
			if(Input::get('condition') == 'na')
			{
				$table->where('t1.status','0');
			}
			elseif(Input::get('condition') == 'a')
			{
				$table->where('t1.status',1);
			}
			elseif(Input::get('condition') == 'r')
			{
				$table->where('t1.status',2);
			}
			elseif(Input::get('condition') == 's')
			{
				$table->where('t1.status',3);
			}
			elseif(Input::get('condition') == 'c')
			{
				$table->where('t1.status',4);
			}
			
		}

		if(Input::get('parent_dep')!='')
		{
			if(Input::get('sub_dep')!='')
			{
				
				$table->where('t1.executive_department',Input::get('sub_dep'));
			}
			else
			{
				$ids = array();

				if(getAllSubDepartments(Input::get('parent_dep')) != '')
				{
					//get sub department ids
					$ids = explode(',',getAllSubDepartments(Input::get('parent_dep')));
					
				}
				$ids[] = Input::get('parent_dep');
				
				//print_r($ids);exit;
				$table->whereIn('t1.executive_department',$ids);
			}


		}
		//$table->whereNotIn('t1.executive_department',array(0));

		if(Input::get('app') || Input::get('document'))
		{
			$doc_type = array();

			if(Input::get('app') == 1)
			{
				$doc_type[] = 1;
			}
			if(Input::get('document') == 2)
			{
				$doc_type[] = 2;
			}

			$table->whereIn('t1.doc_type',$doc_type);

		}
		//--- search criteria ------------------------------------------//
		
		$table->orderBy('t1.created_at','DESC');
		
		if(isset($params['toExcel']))
		{
			$object = $table->get();
		}
		else
		{
			$object = $table->paginate(15);
		}
		return $object;
		
	}
	//get report result
	public static function getReport($params = array())
	{

		$from = "";
		$to = "";
		if(Input::get('start_date') != '')
		{
			if(isMiladiDate())
			{
				$from = Input::get('start_date');
			}
			else
			{
				$from = ymd_format(Input::get('start_date'));
				$from = convertToGregorian($from);
			}
		}
		if(Input::get('end_date') != '')
		{
			if(isMiladiDate())
			{
				$to = Input::get('end_date');
			}
			else
			{
				$to = ymd_format(Input::get('end_date'));
				$to = convertToGregorian($to);
			}
		}

		$table = DB::connection(self::$myDb)
					->table('docs AS t1');
					$table->select(
							't1.id',
							't1.internal_number',
							'dep.name AS executive_department',
							't1.created_at AS days',
							't1.doc_type',
							't1.status'
							);
		$table->leftjoin("auth.department AS dep","dep.id","=","t1.executive_department");
		$table->where('t1.deleted',0);
		if(!allComments('docscom_docscom'))
		{	
			//get my sub deparment id
			$ids = getSubDepartmentIds(getMyDepartmentId(),true);
			$table->whereIn('t1.executive_department',$ids);
			//$table->orWhere('t1.user_id',Auth::user()->id);
		}
		
		if(Input::get('parent_dep')!='')
		{
			if(Input::get('sub_dep')!='')
			{
				$table->where('t1.executive_department',Input::get('sub_dep'));
			}
			else
			{
				$ids = array();

				if(getAllSubDepartments(Input::get('parent_dep')) != '')
				{
					//get sub department ids
					$ids = explode(',',getAllSubDepartments(Input::get('parent_dep')));
					
				}
				$ids[] = Input::get('parent_dep');
				
				//print_r($ids);exit;
				$table->whereIn('t1.executive_department',$ids);
			}
		}

		//--- search criteria ------------------------------------------//
		if(Input::get('start_date') != '' && Input::get('end_date') == '')
		{
			$table->where('t1.created_at','>=',$from);
		}
		elseif(Input::get('start_date') == '' && Input::get('end_date') != '') 
		{
			$table->where('t1.created_at','<=',$to);
		}
		elseif(Input::get('start_date') != '' && Input::get('end_date') != '') 
		{
			$table->whereBetween('t1.created_at',array($from,$to));
		}
		
		$object = $table->orderBy('t1.created_at','DESC');
		
		return $object->get();
	}
	public static function getReportLog($params = array())
	{

		$from = "";
		$to = "";
		if(Input::get('start_date') != '')
		{
			if(isMiladiDate())
			{
				$from = Input::get('start_date');
			}
			else
			{
				$from = ymd_format(Input::get('start_date'));
				$from = convertToGregorian($from);
			}
		}
		if(Input::get('end_date') != '')
		{
			if(isMiladiDate())
			{
				$to = Input::get('end_date');
			}
			else
			{
				$to = ymd_format(Input::get('end_date'));
				$to = convertToGregorian($to);
			}
		}


        //$parties = Party::whereBetween('date', array($from, $to))->get();

		$table = DB::connection(self::$myDb)
					->table('docs AS t1');
					$table->select(
							't1.id',
							't1.barcode',
							't1.external_number',
							't1.internal_number',
							't1.internal_date',
							't1.doc_type',
							'dep.name AS executive_department',
							't1.organization',
							'u.first_name AS approved_by',
							't1.status',
							DB::raw('CONCAT(DATEDIFF(IF(t1.status=0,NOW(),max(l.check_out)), min(l.check_in) )," Days") AS days'),
							DB::raw(
								'(CASE 
							        WHEN t1.status = "0" THEN "تحت پروسیس"
							        WHEN t1.status = "1" THEN "تایید شده"
							        WHEN t1.status = "2" THEN "رد شده"
							        WHEN t1.status = "3" THEN "حفظ شده"
							        WHEN t1.status = "4" THEN "صادر شده"
							        ELSE 1
							    END) AS status_label'
								) 
							);
					$table->leftjoin("auth.department AS dep","dep.id","=","t1.executive_department");
					$table->leftjoin("auth.users AS u","u.id","=","t1.approved_by");
					$table->leftjoin("log AS l","l.doc_id","=","t1.id");
					$table->leftjoin("auth.users AS u1","u1.id","=","l.checked_in_by");
					$table->leftjoin("auth.users AS u2","u2.id","=","l.checked_out_by");
					
					$table->where('l.checked_in_by','!=',0);
					$table->where('t1.deleted',0);
					$table->groupBy('l.doc_id');




		// if(!allComments('docscom_docscom'))
		// {	
		// 	//get my sub deparment id
		// 	//$ids = getSubDepartmentIds(getMyDepartmentId(),true);
			
		// 	//$table->whereIn('t1.executive_department',$ids);
		// 	//$table->orWhere('t1.user_id',Auth::user()->id);

			
		// 	if(isConfidential('docscom_docscom') && !isNonConfidential('docscom_docscom'))
		// 	{
		// 		$table->where('t1.access_type',1);
		// 	}
		// 	elseif(isNonConfidential('docscom_docscom') && !isConfidential('docscom_docscom'))
		// 	{
		// 		$table->where('t1.access_type',2);
		// 	}
		// 	elseif(isConfidential('docscom_docscom') && isNonConfidential('docscom_docscom'))
		// 	{
		// 		$table->whereIn('t1.access_type',array(1,2));
		// 	}
		// }
		//--- search criteria ------------------------------------------//
		if(Input::get('start_date') != '' && Input::get('end_date') == '')
		{
			$table->where('t1.created_at','>=',$from);
		}
		elseif(Input::get('start_date') == '' && Input::get('end_date') != '') 
		{
			$table->where('t1.created_at','<=',$to);
		}
		elseif(Input::get('start_date') != '' && Input::get('end_date') != '') 
		{
			$table->whereBetween('t1.created_at',array($from,$to));
		}

		if(Input::get('parent_dep')!='')
		{
			if(Input::get('sub_dep')!='')
			{
				
				//$table->where(DB::raw('u1.department_id = '.Input::get('sub_dep').' OR u2.department_id = '.Input::get('sub_dep')));
				$table->where('u1.department_id',Input::get('sub_dep'));
			}
			else
			{
				$ids = array();

				if(getAllSubDepartments(Input::get('parent_dep')) != '')
				{
					//get sub department ids
					$ids = explode(',',getAllSubDepartments(Input::get('parent_dep')));
					
				}
				$ids[] = Input::get('parent_dep');
				//$ids = implode(',', $ids);
				
				//$table->where(DB::raw('u1.department_id in ('.$ids.') OR u2.department_id in ('.$ids.')'));
				$table->whereIn('u1.department_id',$ids);
			}


		}
		
		//--- search criteria ------------------------------------------//

		$object = $table->orderBy('t1.created_at','DESC');
		//$table->paginate(1);
		//$object = $table->get();
		//echo $object->tosql();exit;
		return $object->get();
	}
	//search barcode form database
	public static function findBarcode($barcode='',$table='docs',$getCheckout=false)
	{
		$table = DB::connection(self::$myDb)->table($table);
		$table->where('barcode',$barcode);
		//$table->where('deleted',0);
		
		if(!$getCheckout)
		{
			if(!allComments('docscom_docscom'))
			{	
				
				//get my sub deparment id
				//$ids = getSubDepartmentIds(getMyDepartmentId(),true);

				//$table->whereIn('executive_department',$ids);
				//$table->orWhere('user_id',Auth::user()->id);

				//check roles
				if(isConfidential('docscom_docscom') && !isNonConfidential('docscom_docscom'))
				{
					$table->where('access_type',1);
				}
				elseif(isNonConfidential('docscom_docscom') && !isConfidential('docscom_docscom'))
				{
					$table->where('access_type',2);
				}
				elseif(isConfidential('docscom_docscom') && isNonConfidential('docscom_docscom'))
				{
					$table->whereIn('access_type',array(1,2));
				}
			}
		}

		$object = $table->get();

		//return record 
		return $object;
	}

	public static function findBarcodeInMyDep($barcode='',$table='docs',$getCheckout=false)
	{
		$table = DB::connection(self::$myDb)->table($table);
		$table->where('barcode',$barcode);
		
		if(!$getCheckout)
		{
			if(!allComments('docscom_docscom'))
			{	
				
				//get my sub deparment id
				$ids = getSubDepartmentIds(getMyDepartmentId(),true);

				$table->whereIn('executive_department',$ids);
				$table->orWhere('user_id',Auth::user()->id);

				//check roles
				if(isConfidential('docscom_docscom') && !isNonConfidential('docscom_docscom'))
				{
					$table->where('access_type',1);
				}
				elseif(isNonConfidential('docscom_docscom') && !isConfidential('docscom_docscom'))
				{
					$table->where('access_type',2);
				}
				elseif(isConfidential('docscom_docscom') && isNonConfidential('docscom_docscom'))
				{
					$table->whereIn('access_type',array(1,2));
				}
			}
		}
		$table->where('deleted',0);
		$object = $table->get();

		//return record 
		return $object;
	}

	//get doc details
	public static function getDocDetails($doc_id=0,$table_name='')
	{
		$table = DB::connection(self::$myDb)->table($table_name);
				$table->where('id',$doc_id);

				if(!allComments('docscom_docscom'))
				{	
					
					//get my sub deparment id
					//$ids = getSubDepartmentIds(getMyDepartmentId(),true);

					//$table->whereIn('executive_department',$ids);
					//$table->orWhere('user_id',Auth::user()->id);
					
					//check roles
					if(isConfidential('docscom_docscom') && !isNonConfidential('docscom_docscom'))
					{
						$table->where('access_type',1);
					}
					elseif(isNonConfidential('docscom_docscom') && !isConfidential('docscom_docscom'))
					{
						$table->where('access_type',2);
					}
					elseif(isConfidential('docscom_docscom') && isNonConfidential('docscom_docscom'))
					{
						$table->whereIn('access_type',array(1,2));
					}
				}
			$table->where('deleted',0);
			$object = $table->get();

		return $object;
	}

	//get doc comments
	public static function getDocComments($doc_id=0,$table_name='')
	{
		if(!allComments('docscom_docscom'))
		{
			$object = DB::connection(self::$myDb)
								->table('comments')
								->where('doc_id',$doc_id)
								->where('table_name',$table_name)
								->orderBy('id','DESC')
								->limit(1)
								->get();

		}
		else
		{
			$object = DB::connection(self::$myDb)
								->table('comments')
								->orderBy('id','DESC')
								->where('doc_id',$doc_id)
								->where('table_name',$table_name)
								->get();
		}
		return $object;
	}

	//get last record log
	public static function getLastLogRecord($doc_id,$table_name='')
	{
		$object = DB::connection(self::$myDb)
								->table('log')
								->where('doc_id',$doc_id)
								//->where('status',2)
								->where('table_name',$table_name)
								->orderBy('id','DESC')
								->limit(2)
								->get();
		
		$checked_in_by = 0;
		$counter = 1;
		foreach($object AS $item)
		{
			if($counter == 2 && count($object)>1)
			{
				$checked_in_by = $item->checked_in_by;
			}
			else
			{
				$checked_in_by = $item->checked_in_by;
			}

			$counter++;

		}
		
		return $checked_in_by;
	}
	//get doc attachments
	public static function getDocAttachs($doc_id=0,$table_name='')
	{
		$object = DB::connection(self::$myDb)
								->table('attachments')
								->where('doc_id',$doc_id)
								->where('table_name',$table_name)
								->get();
		return $object;
	}

	//get doc log
	public static function getDocLog($doc_id=0,$table_name='')
	{
		$object = DB::connection(self::$myDb)
								->table('log AS t1')
								->select(
											't1.*',
											't4.name AS department',
											DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS checked_in_by'),
											DB::raw('CONCAT(t3.first_name," ",t3.last_name) AS checked_out_by')

										)
								->leftJoin('auth.users AS t2','t2.id','=','t1.checked_in_by')
								->leftJoin('auth.users AS t3','t3.id','=','t1.checked_out_by')
								->leftJoin('auth.department AS t4','t4.id','=','t2.department_id')
								->where('doc_id',$doc_id)
								->where('t1.table_name',$table_name)
								->get();
		return $object;
	}

	//insert new record
	public static function insertRecord($table="",$data=array())
	{
		DB::connection(self::$myDb)->table($table)->insert($data);
	}

	//check the log, is check outed by this user or not
	public static function isCheckOuted($form_id=0)
	{
		$object = DB::connection(self::$myDb)->table('log')
								    ->where('doc_id',$form_id)
								    ->where('checked_in_by',Auth::user()->id)
								    ->where('status',1)
								    ->get();
		if(count($object)>0)
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public static function docChekedOut($form_id=0,$table_name='docs')
	{
		$object = DB::connection(self::$myDb)->table($table_name)
								    ->where('id',$form_id)
								    ->whereNotIn('exported_by',array(0))
								    ->get();
		if(count($object)>0)
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public static function getFormAnswers($doc_id = 0)
	{

		//get form number by form id
		$form_number = DB::connection(self::$myDb)
						->table('docs')
						->where('id',$doc_id)
						->pluck('external_number');

		$object = DB::connection(self::$myDb)
								->table('docs_answers AS t1')
								->select(
									't1.doc_summary',
									't2.name AS executive_department',
									't1.id'
									)
								->leftjoin('auth.department AS t2','t2.id','=','t1.executive_department')
								->where('form_number',$form_number)
								->get();
		return $object;

	}
	
}
