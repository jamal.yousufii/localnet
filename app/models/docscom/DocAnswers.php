<?php

namespace App\models\docscom;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class DocAnswers extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'docscom';
	public static $myDb 	= "docscom";
	protected $table 		= 'docs_answers';


	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('docs_answers AS t1');
					$table->select(
							't1.id AS doc_id',
							't1.barcode',
							't1.source_organization',
							'dep.name AS executive_department',
							DB::raw('IF(t1.is_answer = 1,CONCAT("جوابیه فورم"," - ",t1.form_number),"جوابیه نیست") AS is_answer'),
							DB::raw('IF(t1.status = 0,"تحت پروسیس است","تایید شده است") AS status') 
							);
					$table->leftjoin("auth.department AS dep",'dep.id','=','t1.executive_department');

				if(!allComments('docscom_docscom'))
				{	
					//get my sub deparment id
					$ids = getSubDepartmentIds(getMyDepartmentId(),true);

					
					$table->whereIn('t1.executive_department',$ids);
					$table->orWhere('t1.user_id',Auth::user()->id);

					
					// if(isConfidential('docscom_docscom') && !isNonConfidential('docscom_docscom'))
					// {
					// 	$table->where('access_type',1);
					// }
					// elseif(isNonConfidential('docscom_docscom') && !isConfidential('docscom_docscom'))
					// {
					// 	$table->where('access_type',2);
					// }
					// elseif(isConfidential('docscom_docscom') && isNonConfidential('docscom_docscom'))
					// {
					// 	$table->whereIn('access_type',array(1,2));
					// }
				}

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
}