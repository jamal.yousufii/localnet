<?php namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class Localization extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'lang_files';


	public static function getApps()
	{
		$apps = DB::table('module')->get();
		return $apps;
	}

	//get key fields from database
	public static function getLangFields($appId=0,$limit=false,$current=0)
	{
		if($limit)
			return DB::table('lang_files')->where('module_id',$appId)->skip($current)->take(20)->get();
		else
			return DB::table('lang_files')->where('module_id',$appId)->get();
	}

	//get fields limit
	public static function getFieldLimit($appId=0,$currentId = 0,$state='')
	{

		$table = DB::table('lang_files');
		$table->where('module_id',$appId);
		if($state == 'next')
		{
			$table->where("id",">",$currentId);
			$table->orderBy('id','ASC');
		}
		elseif($state == 'prev')
		{
			$table->where("id","<",$currentId)->max('id');
			$table->orderBy('id','DESC');
		}
		else
		{
			$table->where("id",$currentId);
		}
		//$table->paginate(1);
		$object = $table->paginate(1);

		return $object;

	}

	public static function getNextPagination($param=array())
	{

		$table = DB::table('lang_files');
		$table->where('module_id',$param['appId']);
		if($param['state'] == 'next')
		{
			$table->where("id",">",$param['current']);
			$table->orderBy('id','ASC');
		}
		elseif($param['state'] == 'prev')
		{
			$table->where("id","<",$param['current'])->max('id');
			$table->orderBy('id','DESC');
		}
		else
		{
			$table->where("id",$param['current']);
		}
		//$table->paginate(1);
		$object = $table->paginate(20);

		return $object;

	}

	public static function getAllFields($appId=0)
	{
		 return DB::table('lang_files')
		 ->where('module_id',$appId)
		 ->paginate(1);
	}

	//insert new rows to lang file
	public static function insertKeys($rows=array())
	{
		DB::table('lang_files')->insert($rows);
	}

	//remove old records
	public static function removeOldRecords($appId=0,$keys=array())
	{

		DB::table('lang_files')
		->where('module_id',$appId)
		->whereIn('key',$keys)
		->delete();
	}

	public static function updateKey($id=0,$data=array())
	{
		DB::table('lang_files')->where('id',$id)->update($data);
		return true;
	}

	
}