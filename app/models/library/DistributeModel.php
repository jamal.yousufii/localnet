<?php namespace App\models\library;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Input;
use get;


class DistributeModel extends Model {


	protected $table="distribute_book";
	public $timestamps = false;


	public static function getDistributedBook(){

		$row=\DB::connection('library')->table('distribute_book')->select('book.*','categories.*','customers.*','distribute_book.*')
		->leftJoin('book','distribute_book.book_id','=','book.id')
		->leftJoin('categories', 'book.book_type', '=', 'categories.id')
		->leftJoin('customers','distribute_book.customer_id','=','customers.id')
		->orderby('return_date','asc')
		->get();
		return $row;

	}


}
