<?php

namespace workplan;
use Eloquent;
use DB;

class Department extends Eloquent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'mysql2';
	protected $table = 'department';

	public static function getAll()
	{
		    
		$table = DB::connection("mysql")->table('department');
        //$table->leftJoin('users', 'department.id', '=', 'users.department_id');
      	//$table->groupBy('users.department_id');
        //$table->select('department.*', DB::raw('count(users.id) AS total_user'));
        $object = $table->get();

        return $object;
	}

	public static function getDetails($id=0)
	{
		$table = DB::connection('mysql2')->table('department');
		$table->where('id',$id);
		$object = $table->get();
		return $object;
	}

	

}
