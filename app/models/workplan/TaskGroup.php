<?php

namespace App\models\workplan;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class TaskGroup extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'workplan';
	protected $table = 'task_group';

	public static function getData()
	{
		if(isAdmin())
		{
			$object = DB::table('workplan.task_group AS t1')
				->select(
				't1.id',
				't1.title',
				't1.description',
				't1.user_id',
				DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS creator'))
				->leftJoin('auth.users as t2', 't2.id', '=', 't1.user_id');
		}
		else
		{
			$object = DB::table('workplan.task_group AS t1')
				->select(
				't1.id',
				't1.title',
				't1.description',
				't1.user_id',
				DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS creator'))
				->leftJoin('auth.users as t2', 't2.id', '=', 't1.user_id')
				//->leftJoin('workplan.group_shares_user AS gu','gu.task_group_id','=','t1.id')
				//->leftJoin('workplan.group_names AS gn','gn.task_group_id','=','t1.id')
				->whereRaw('(t1.access_level = 1 AND t1.dep_id = '.\Auth::user()->department_id.') OR (t1.access_level = 1 AND t1.user_id = '.\Auth::user()->id.') OR t1.user_id = '.\Auth::user()->id.'');
				//->where('t1.user_id',\Auth::user()->id)
				//->orWhere('t1.dep_id',\Auth::user()->department_id);
		}
		return $object->get();
	}

	public static function insertBatch($table='',$data=array())
	{
		DB::connection('workplan')->table($table)->insert($data);
	}

	//delete task group related
	public static function deleteRelated($taskGroupId = 0)
	{
		DB::connection('workplan')->table('group_shares_user')->where('task_group_id',$taskGroupId)->delete();
		DB::connection('workplan')->table('group_shares_department')->where('task_group_id',$taskGroupId)->delete();
		DB::connection('workplan')->table('tasks')->where('task_group_id',$taskGroupId)->delete();
	}

	//get task group details
	public static function getTaskGroupDetails($id=0)
	{
		return DB::connection('workplan')->table('task_group')->where('id',$id)->get();
	}
	//get task group shares
	public static function getTaskGroupShares($table='',$task_group = 0)
	{
		return DB::connection('workplan')->table($table)->where('task_group_id',$task_group)->get();
	}

	//remove the old task group related
	public static function removeTaskGroupRelated($table='',$taskGroupId=0)
	{
		DB::connection('workplan')->table($table)->where('task_group_id',$taskGroupId)->delete();
	}

	public static function getAll()
	{
		return DB::connection('workplan')->table('task_group')->get();
	}
	public static function getAll_myGroups()
	{
		return DB::connection('workplan')->table('task_group')
		->whereRaw('(access_level = 1 AND dep_id = '.\Auth::user()->department_id.') OR (access_level = 1 AND user_id = '.\Auth::user()->id.') OR user_id = '.\Auth::user()->id.' OR access_level =0')
		//->where('access_level',1)
		->get();
	}
	public static function checkGroup_task($id=0)
	{
		return DB::connection('workplan')->table('tasks')->where('task_group_id',$id)->first();
	}

}
