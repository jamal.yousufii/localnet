<?php

namespace App\models\workplan;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Report extends Model
{
	protected $connection = 'workplan';
	protected $table = 'tasks';
	
	public static function getReport()
	{   
		$object = DB::connection('workplan')
					->table('tasks AS t1')
					->select(
						't1.id',
						't1.title',
						't1.description',
						't1.indicator',
						't1.metric',
						't1.expected_outcome',
						't1.start_date',
						't1.end_date',
						DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS creator'))
					->leftJoin('auth.users as t2', 't2.id', '=', 't1.user_id')
					->where('t1.parent_task',0)
					->where('t1.user_id',Auth::user()->id)
					->get();
        return $object;
	}
	public static function getTaskDetails($id=0)
	{
		    
		$object = DB::connection('workplan')
					->table('tasks AS t1')
					->select(
						't1.id',
						't1.title',
						't1.description',
						't1.indicator',
						't1.metric',
						't1.expected_outcome',
						't1.start_date',
						't1.end_date',
						't1.notify_by_email',
						't1.notify_by_sms',
						't1.task_group_id',
						't1.parent_task',
						't1.created_at',
						't1.percentage',
						't1.user_id',
						't1.subordinates',
						't3.title AS task_group',
						't1.time_in',
						DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS creator'))
					->leftJoin('auth.users as t2', 't2.id', '=', 't1.user_id')
					->leftJoin('workplan.task_group as t3', 't3.id', '=', 't1.task_group_id')
					->where('t1.id',$id)
					->first();

		//$object = DB::raw("SELECT r.*, d.name AS department FROM mysql.report AS r LEFT JOIN mysql2.department AS d ON r.department_id = d.id");

        return $object;
	}

	//get task assignees
	public static function getTaskAssignee($task=0)
	{
		$object = DB::connection('workplan')
					->table('task_assigned_to AS t1')
					->select(
						't1.assigned_to','t1.email_notify',
						DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS assignee'))
					->leftJoin('auth.users as t2', 't2.id', '=', 't1.assigned_to')
					->where('t1.task_id',$task)
					->get();

		//$object = DB::raw("SELECT r.*, d.name AS department FROM mysql.report AS r LEFT JOIN mysql2.department AS d ON r.department_id = d.id");

        return $object;

	}

	//get sub tasks
	public static function getSubTasks($task=0)
	{
		$userid = Auth::user()->id;

					if(isTaskCreator($task))
					{
		$table = DB::connection('workplan')
					->table('tasks AS t1');
					$table->select(
						't1.id',
						't1.title',
						't1.description',
						't1.indicator',
						't1.metric',
						't1.expected_outcome',
						't1.start_date',
						't1.end_date',
						't1.notify_by_email',
						't1.notify_by_sms',
						't1.parent_task',
						't1.created_at',
						't1.user_id',
						DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS creator'));
					$table->leftJoin('auth.users as t2', 't2.id', '=', 't1.user_id');
					//$table->where('t1.parent_task',$task);

						$table->where('t1.parent_task',$task);
						$object = $table->get();
						
					} else {

		$object = DB::select("SELECT t1.id ,  t1.title ,  t1.description ,  t1.indicator ,  t1.metric ,  t1.expected_outcome ,  t1.start_date , t1.end_date ,  t1.notify_by_email ,  t1.notify_by_sms ,  t1.parent_task ,  t1.created_at ,  t1.user_id , CONCAT( t2.first_name,  \" \", t2.last_name ) AS creator
								FROM  workplan.tasks AS  t1 
								LEFT JOIN  auth.users AS  t2 ON  t2.id =  t1.user_id 
								LEFT JOIN  workplan.task_assigned_to AS  t3 ON  t3.task_id =  t1.id 
								WHERE (
								t1.parent_task =  ".$task."
								AND t1.user_id =  ".$userid."
								)
								AND (
								t3.assigned_to =  ".$userid."
								OR t1.user_id =  ".$userid."
								)
								GROUP BY  t1.id ");
					//print_r($table->toSql());exit;
					}
		return $object;

	}

	//get all users department vise
	public static function getDepUsers($managers=false)
	{
		//get sub departments id
		
		$results = getAllSubDepartments(Auth::user()->department_id);
		if($results == '')
		{
			$ids = array(Auth::user()->department_id);
		}
		else
		{
			$ids = explode(",", $results);
			$ids[] = Auth::user()->department_id;
		}
		/*
		if($managers)
		{
			//return DB::table('users')->whereIn('department_id',$ids)->where('is_manager',1)->get();
			return DB::table('users')->where('deputy_task',1)->where('position_id',2)->get();
		}
		*/
		return DB::table('users')->whereIn('department_id',$ids)->orWhere('is_manager',1)->get();
		//return DB::table('users')->where('deputy_task',1)->get();
	}	

	public static function insertBatch($table='',$data=array())
	{
		DB::connection('workplan')->table($table)->insert($data);
	}
	public static function updateRecord($table='',$data=array(),$where)
	{
		DB::connection('workplan')->table($table)->where('id',$where)->update($data);
	}
	public static function delete_record($table='',$where)
	{
		DB::connection('workplan')->table($table)->where($where)->delete();
	}
	//delete task and relative
	public static function deleteTask($task_id=0)
	{
		DB::connection('workplan')->table('task_assigned_to')->where('task_id', $task_id)->delete();
		DB::connection('workplan')->table('task_comments')->where('task_id', $task_id)->delete();
		DB::connection('workplan')->table('task_progress')->where('task_id', $task_id)->delete();
		DB::connection('workplan')->table('notifications')->where('task_id', $task_id)->delete();
		//DB::connection('workplan')->table('tasks')->where('parent_task', $task_id)->delete();
		DB::connection('workplan')->table('tasks')->where('id', $task_id)->delete();
		return true;
	}
	//insert new record
	public static function insertRecord($table="",$data=array())
	{
		DB::connection('workplan')->table($table)->insert($data);
	}
	//get task assigned to me
	public static function getTasksAssignedToMe()
	{

		$object = DB::connection('workplan')
					->table('tasks AS t1')
					->select(
						't1.id',
						't1.title',
						't1.description',
						't1.indicator',
						't1.metric',
						't1.expected_outcome',
						't1.start_date',
						't1.end_date',
						't3.status',
						't1.user_id',
						DB::raw('CONCAT(t2.first_name," ",t2.last_name) AS creator'))
					->leftJoin('auth.users as t2', 't2.id', '=', 't1.user_id')
					->leftJoin('workplan.task_assigned_to as t3', 't3.task_id', '=', 't1.id')
					//->leftJoin('workplan.notifications as t4', 't4.task_id', '=', 't1.id')
					//->whereNotIn('t4.notification_type',array(2))
					//->where('t4.notification_type',3)
					//->where('t1.parent_task',0)
					->where('t3.assigned_to',Auth::user()->id)
					->orderBy('t3.status','ASC')
					->get();
        return $object;

	}
	public static function get_task_status($task=0)
	{//get the status if task assigned to me, whether is approved or rejected
		$object = DB::connection('workplan')
					->table('task_assigned_to')
					->select(
						'status')
					->where('task_id',$task)
					->where('assigned_to',Auth::user()->id)
					->first();

		//$object = DB::raw("SELECT r.*, d.name AS department FROM mysql.report AS r LEFT JOIN mysql2.department AS d ON r.department_id = d.id");

        return $object;

	}
	public static function is_assigned_before($userid=0,$task=0)
	{
		$object = DB::connection('workplan')
					->table('task_assigned_to')
					->select(
						'status')
					->where('task_id',$task)
					->where('assigned_to',$userid)
					->where('status','!=',2)
					->first();

		//$object = DB::raw("SELECT r.*, d.name AS department FROM mysql.report AS r LEFT JOIN mysql2.department AS d ON r.department_id = d.id");

        return $object;

	}
	public static function geTaskStatusByUser($userid=0,$task=0)
	{
		$object = DB::connection('workplan')
					->table('task_assigned_to')
					->select(
						'status','id')
					->where('task_id',$task)
					->where('assigned_to',$userid)
					->first();

		//$object = DB::raw("SELECT r.*, d.name AS department FROM mysql.report AS r LEFT JOIN mysql2.department AS d ON r.department_id = d.id");

        return $object;

	}
	public static function has_subtask($task_id=0)
	{
		$object = DB::connection('workplan')
					->table('tasks')
					->select(
						'id')
					->where('parent_task',$task_id)
					->get();

		//$object = DB::raw("SELECT r.*, d.name AS department FROM mysql.report AS r LEFT JOIN mysql2.department AS d ON r.department_id = d.id");

        return $object;
	}
	public static function get_task_files($task_id=0)
	{
		$object = DB::connection('workplan')
					->table('attachments')
					->select(
						'*')
					->where('task_id',$task_id)
					->get();

		
        return $object;
	}
	public static function get_weekly_report($request)
	{
		$sdate = $request["start_date"];
		$edate = $request["end_date"];
		
		if($sdate != '')
		{
			$sdate = explode("-", $sdate);
			$sy = $sdate[2];
			$sm = $sdate[1];
			$sd = $sdate[0];
			$sdate = dateToMiladi($sy,$sm,$sd);		
		}
		if($edate != '')
		{
			$edate = explode("-", $edate);
			$ey = $edate[2];
			$em = $edate[1];
			$ed = $edate[0];
			$edate = dateToMiladi($ey,$em,$ed);		
		}
		$object = DB::connection('workplan')
					->table('tasks as t')
					->select(
						't.*')
					->leftJoin('task_group as g', 'g.id', '=', 't.task_group_id')
					->where('g.dep_id',$request['dep'])
					->where('t.parent_task',0)
					->whereRaw('(((t.end_date >= "'.$sdate.'" AND t.end_date <= "'.$edate.'") OR (t.start_date >= "'.$sdate.'" AND t.start_date <= "'.$edate.'")) OR (t.start_date <= "'.$sdate.'" AND t.end_date >= "'.$edate.'"))')
					->orderBy('t.user_id','desc')
					->get();
		return $object;
		
	}
	public static function get_weekly_reportSub($parent=0)
	{
		$table = DB::connection('workplan')
						->table('tasks AS t')
						->select(
								't.title',
								't.id',
								't.user_id',
								't.start_date',
								't.end_date',
								't.created_at',
								't.parent_task'
								)
						->where('t.parent_task',$parent);
	
		return $table->get();
		
	}
	public static function getLastProgress($task_id=0)
	{
		$table = DB::connection('workplan')
						->table('task_progress')
						
						->where('task_id',$task_id)
						->where('progress',100);
	
		return $table->first();
		
	}
}
