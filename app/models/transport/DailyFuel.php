<?php

namespace App\models\transport;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class DailyFuel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'transport';
	public static $myDb 	= "transport";
	protected $table 		= 'fuel_daily_spends';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('fuel_daily_spends AS t1');
					$table->select(
							't1.id',
							DB::raw('CONCAT(d.first_name," ",d.last_name," ولد ",d.father_name) AS driver'),
							'vt.name AS vehicle_type',
							't1.palet_number',
							't1.department',
							't1.amount',
							't1.date'
							);
		$table->leftjoin("drivers AS d","d.id","=","t1.driver");
		$table->leftjoin("vehicle_type AS vt","vt.id","=","t1.vehicle_type");
		
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("fuel_daily_spends")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("fuel_daily_spends")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}