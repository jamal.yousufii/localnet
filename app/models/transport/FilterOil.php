<?php

namespace App\models\transport;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class FilterOil extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'transport';
	public static $myDb 	= "transport";
	protected $table 		= 'filters_and_oils';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('filters_and_oils AS t1');
					$table->select(
							't1.id',
							't1.item_description',
							't1.properties',
							't1.amount',
							'u.name as unit',
							DB::raw('CONCAT(m.first_name," ",m.last_name) AS motamid')
							);
		$table->leftjoin("mesure_units AS u","u.id","=","t1.unit");
		$table->leftjoin("drivers AS m","m.id","=","t1.motamid");
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("filters_and_oils")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("filters_and_oils")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}