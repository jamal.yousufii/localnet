<?php

namespace App\models\transport;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class FuelContracted extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'transport';
	public static $myDb 	= "transport";
	protected $table 		= 'contracted_fuel_sources';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('contracted_fuel_sources AS t1');
					$table->select(
							't1.id',
							't1.company',
							't1.gravity',
							DB::raw('CONCAT(t1.date," ",t1.time) AS date'),
							't1.serial_no',
							't1.vehicle_no',
							't1.driver_name'
							);
				
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("contracted_fuel_sources")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("contracted_fuel_sources")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}