<?php

namespace App\models\transport;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class Report extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'transport';
	public static $myDb 	= "transport";
	protected $table 		= 'fees9';
	//$table = $table->paginate(15);

	public static function getFees9ReportResult()
	{
		
		$start_date = "";
		$end_date = "";
		if(Input::get('start_date') != '')
		{
			if(isMiladiDate())
			{
				$start_date = Input::get('start_date');
			}
			else
			{
				$start_date = ymd_format(Input::get('start_date'));
				$start_date = convertToGregorian($start_date);
			}
		}
		if(Input::get('end_date') != '')
		{
			if(isMiladiDate())
			{
				$end_date = Input::get('end_date');
			}
			else
			{
				$end_date = ymd_format(Input::get('end_date'));
				$end_date = convertToGregorian($end_date);
			}
		}
		

		$table = DB::connection(self::$myDb)
					->table('fees9_items AS t1');
					$table->select(
							't1.id',
							'i.id AS item_id',
							'i.item_description AS name',
							DB::raw('SUM(t1.amount) AS total'),
							'u.name AS unit',
							DB::raw('CONCAT(d.first_name," ",d.last_name) AS driver'),
							'v.palet_no AS plate',
							'dep.name AS department'
							);
				
		$table->leftjoin("filters_and_oils AS i","i.id","=","t1.item_id");
		$table->leftjoin("mesure_units AS u","u.id","=","t1.unit");
		$table->leftjoin("fees9 AS f","f.id","=","t1.fees9_id");
		$table->leftjoin("drivers AS d","d.id","=","f.driver");
		$table->leftjoin("vehicles AS v","v.id","=","f.vehicle_plate");
		$table->leftjoin("auth.department AS dep","dep.id","=","f.requested_dep");
		
		
		//--- search criteria ------------------------------------------//
		if(Input::get('start_date') != '' && Input::get('end_date') == '')
		{
			$table->where('f.created_at','>=',$start_date);
		}
		elseif(Input::get('start_date') == '' && Input::get('end_date') != '') 
		{
			$table->where('f.created_at','<=',$end_date);
		}
		elseif(Input::get('start_date') != '' && Input::get('end_date') != '') 
		{
			$table->whereBetween('f.created_at',array($start_date,$end_date));
		}
		
		if(Input::get("driver") != "")
		{
			$table->where("f.driver",Input::get("driver"));
		}
		if(Input::get("plate") != "")
		{
			$table->where("f.vehicle_plate",Input::get("plate"));
		}
		if(Input::get("department") != "")
		{
			$table->where("f.requested_dep",Input::get("department"));
		}
		if(Input::get("sub_department") != "")
		{
			$table->where("f.agency_name",Input::get("sub_department"));
		}
		if(Input::get("category") != "")
		{
			$table->where("f.type",Input::get("category"));
		}
		
		$table->groupBy("f.driver");
		$table->groupBy("t1.item_id");
		return $table->get();
		//echo $table->tosql();exit;
		//return $table->paginate(10);
	}
	public static function getItemDetails()
	{
		
		$start_date = "";
		$end_date = "";
		if(Input::get('start_date') != '')
		{
			if(isMiladiDate())
			{
				$start_date = Input::get('start_date');
			}
			else
			{
				$start_date = ymd_format(Input::get('start_date'));
				$start_date = convertToGregorian($start_date);
			}
		}
		if(Input::get('end_date') != '')
		{
			if(isMiladiDate())
			{
				$end_date = Input::get('end_date');
			}
			else
			{
				$end_date = ymd_format(Input::get('end_date'));
				$end_date = convertToGregorian($end_date);
			}
		}
		

		$table = DB::connection(self::$myDb)
					->table('fees9_items AS t1');
					$table->select(
							't1.id',
							'i.id AS item_id',
							'i.item_description AS name',
							'u.name AS unit',
							'f.created_at AS date',
							't1.amount',
							't1.fees9_id',
							'f.type AS fees9_type'
							);
				
		$table->leftjoin("filters_and_oils AS i","i.id","=","t1.item_id");
		$table->leftjoin("mesure_units AS u","u.id","=","t1.unit");
		$table->leftjoin("fees9 AS f","f.id","=","t1.fees9_id");
		
		
		//--- search criteria ------------------------------------------//
		if(Input::get('start_date') != '' && Input::get('end_date') == '')
		{
			$table->where('f.created_at','>=',$start_date);
		}
		elseif(Input::get('start_date') == '' && Input::get('end_date') != '') 
		{
			$table->where('f.created_at','<=',$end_date);
		}
		elseif(Input::get('start_date') != '' && Input::get('end_date') != '') 
		{
			$table->whereBetween('f.created_at',array($start_date,$end_date));
		}
		
		if(Input::get("driver") != "")
		{
			$table->where("f.driver",Input::get("driver"));
		}
		if(Input::get("plate") != "")
		{
			$table->where("f.vehicle_plate",Input::get("plate"));
		}
		
		$table->where("t1.item_id",Input::get("item_id"));
	
		return $table->get();
		//echo $table->tosql();exit;
		//return $table->paginate(10);
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("drivers")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("drivers")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}