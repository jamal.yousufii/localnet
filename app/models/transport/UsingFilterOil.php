<?php

namespace App\models\transport;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class UsingFilterOil extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'transport';
	public static $myDb 	= "transport";
	protected $table 		= 'used_filters_oils';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('used_filters_oils AS t1');
					$table->select(
							't1.id',
							DB::raw('CONCAT(d.first_name," ",d.last_name," ولد ",d.father_name) AS driver'),
							't1.palet_no',
							'vt.name AS type',
							't1.form_no',
							't1.date'
							);
		$table->leftjoin("drivers AS d","d.id","=","t1.driver");
		$table->leftjoin("vehicles AS v","v.palet_no","=","t1.palet_no");
		$table->leftjoin("vehicle_type AS vt","vt.id","=","v.type");
		
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("used_filters_oils")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("used_filters_oils")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}