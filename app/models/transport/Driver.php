<?php

namespace App\models\transport;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class Driver extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'transport';
	public static $myDb 	= "transport";
	protected $table 		= 'drivers';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('drivers AS t1');
					$table->select(
							't1.id',
							't1.card_no',
							't1.first_name',
							't1.last_name',
							't1.father_name',
							't1.phone'
							);
				
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("drivers")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("drivers")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}