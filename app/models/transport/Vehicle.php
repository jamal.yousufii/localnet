<?php

namespace App\models\transport;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class Vehicle extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'transport';
	public static $myDb 	= "transport";
	protected $table 		= 'vehicles';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('vehicles AS t1');
					$table->select(
							't1.id',
							't2.name AS vehicle_type',
							't1.model',
							't1.palet_no',
							't1.engine_no',
							't1.shasi_no',
							DB::raw("CASE t1.current_status 
							WHEN '1' THEN 'فعال'
							WHEN '2' THEN 'غیر فعال'
							END as current_status")
							);
					$table->leftjoin("vehicle_type AS t2",'t2.id','=','t1.type');

		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("vehicles")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("vehicles")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}