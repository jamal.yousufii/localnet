<?php

namespace App\models\transport;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class DeputyVehicle extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'transport';
	public static $myDb 	= "transport";
	protected $table 		= 'deputy_vehicles';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('deputy_vehicles AS t1');
					$table->select(
							't1.id',
							'vc.name AS vehicle_category',
							'vt.name AS vehicle_type',
							't1.model',
							't1.plate',
							't1.mileage',
							't1.status',
							't1.photo'
							);
					$table->leftjoin("vehicle_type AS vt",'vt.id','=','t1.vehicle_type');
					$table->leftjoin("vehicle_category AS vc",'vc.id','=','t1.vehicle_category');
		if(Input::get('search')==1){
			$table->where("t1.model","like","%".Input::get('keyword')."%");
			$table->orWhere("t1.plate","like","%".Input::get('keyword')."%");
			$table->orWhere("t1.mileage","like","%".Input::get('keyword')."%");
		}
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->paginate(10);
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("deputy_vehicles")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("deputy_vehicles")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}