<?php

namespace App\models\transport;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class Repairing extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'transport';
	public static $myDb 	= "transport";
	protected $table 		= 'request_for_repairing';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('request_for_repairing AS t1');
					$table->select(
							't1.id',
							DB::raw('CONCAT(d.first_name," ",d.last_name," ولد ",d.father_name) AS driver'),
							't1.register_no',
							't1.date',
							't1.palet_number',
							DB::raw("CASE t1.source 
							WHEN '1' THEN 'دیپو'
							WHEN '2' THEN 'قراردادی'
							ELSE 'تهیه از بازار(خوش خرید)'
							END as source")
							);
		$table->leftjoin("drivers AS d","d.id","=","t1.driver");
		
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("request_for_repairing")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("request_for_repairing")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}