<?php

namespace App\models\transport;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class VehicleReservation extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'transport';
	public static $myDb 	= "transport";
	protected $table 		= 'reserved_vehicles';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('reserved_vehicles AS t1');
					$table->select(
							't1.id',
							DB::raw('CONCAT(d.first_name," ",d.last_name," ولد ",d.father_name) AS driver'),
							'dep.name AS service_area',
							DB::raw("CASE t1.reserved_type 
							WHEN '1' THEN 'توظیف'
							WHEN '2' THEN 'خدمتی'
							ELSE 'نامعلوم'
							END as reserved_type"),
							'v.palet_no AS palet_number',
							'dep1.name AS reserved_to_dep',
							DB::raw("CASE t1.is_hamala 
							WHEN '1' THEN 'حمله'
							ELSE t1.using_by
							END as using_by"),
							DB::raw("CASE t1.status 
							WHEN '1' THEN 'توظیف شده'
							WHEN '0' THEN 'ریزرف شده یا پارک شده'
							ELSE 'NULL'
							END as status"));
		$table->leftjoin("drivers AS d","d.id","=","t1.driver");
		$table->leftjoin("vehicles AS v","v.id","=","t1.vehicle");
		$table->leftjoin("auth.department AS dep","dep.id","=","t1.service_area");
		$table->leftjoin("auth.department AS dep1","dep1.id","=","t1.reserved_to_dep");
		
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getDataExcel()
	{
		$table = DB::connection(self::$myDb)
					->table('reserved_vehicles AS t1');
					$table->select(
							't1.id',
							'v.palet_no AS palet_number',
							'vt.name AS vehicle_type',
							'v.model',
							'v.color',
							'v.engine_no AS engine',
							'v.shasi_no',
							'v.licence_no AS license',
							DB::raw('CONCAT(d.first_name," ",d.last_name) AS driver'),
							'd.father_name',
							'dep.name AS service_area',
							'dep1.name AS reserved_to_dep',
							DB::raw("CASE t1.is_hamala 
							WHEN '1' THEN 'حمله'
							ELSE t1.using_by
							END as using_by"),
							'd.phone AS driver_phone',
							't1.user_phone'
							);
		$table->leftjoin("drivers AS d","d.id","=","t1.driver");
		$table->leftjoin("vehicles AS v","v.id","=","t1.vehicle");
		$table->leftjoin("vehicle_type AS vt","vt.id","=","v.type");
		$table->leftjoin("auth.department AS dep","dep.id","=","t1.service_area");
		$table->leftjoin("auth.department AS dep1","dep1.id","=","t1.reserved_to_dep");
		
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("reserved_vehicles")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("reserved_vehicles")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}