<?php

namespace App\models\transport;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class Fees9 extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'transport';
	public static $myDb 	= "transport";
	protected $table 		= 'fees9';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('fees9 AS t1');
					$table->select(
							't1.id',
							't1.number',
							't1.date',
							'dep.name AS requested_dep',
							'dep1.name AS agency_name',
							't1.type as category',
							DB::raw("CASE t1.type 
							WHEN '1' THEN 'فلترباب/تیل'
							WHEN '2' THEN 'پورزه جات'
							WHEN '3' THEN 'تایر موتر'
							WHEN '4' THEN 'ضروریات ماشین'
							ELSE 'تیل'
							END as type")
							);
		$table->leftJoin("auth.department AS dep","dep.id","=","t1.requested_dep");
		$table->leftJoin("auth.department AS dep1","dep1.id","=","t1.agency_name");
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("fees9")->where("id",$id)->first();
	}
	public static function insertItems($data=array())
	{
		return DB::connection(self::$myDb)->table("fees9_items")->insert($data);
	}
	public static function getDelete($id=0)
	{
		DB::connection(self::$myDb)->table("fees9_items")->where('fees9_id',$id)->delete();
		
		if(DB::connection(self::$myDb)->table("fees9")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}