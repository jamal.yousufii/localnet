<?php namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
class DepartmentModule extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'module_department';


	public static function deleteModuleDepartment($id=0)
	{
		DB::table('module_department')->where('department_id',$id)->delete();
	}
	

}
