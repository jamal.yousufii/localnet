<?php namespace App\models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class Application extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'module';

	public static function getAll()
	{
		  
		$userid = Auth::user()->id;
		
		$table = DB::table('module AS m');
		if(!isAdmin())
		{
			$table->leftJoin('user_module AS um','um.module_id','=','m.id');
			$table->where('um.user_id',$userid);
		}

        $object = $table->get();

        return $object;
	}
	public static function getData()
	{
		    
		return DB::table('module')
        			->select('id','code','name','description')
					->get();
	}
	public static function getDetails($id=0)
	{
		$table = DB::table('module');
		$table->where('id',$id);
		$object = $table->get();
		return $object;
	}

	

}
