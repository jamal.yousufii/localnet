<?php

namespace App\models\evaluation;
use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Session;

class evaluationModel extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'evaluation';
	public static $myDb 	= "evaluation";

	// get the Departures.
	public static function getData($year=0,$doc_type=0,$doc_no=0)
	{

		$table = DB::connection(self::$myDb)
				->table('received_docs as d');
				$table->select('d.id','m.name_dr AS dep',
							DB::raw(
								'CASE d.doc_type
								    WHEN "1" THEN "مکتوب"
								    WHEN "2" THEN "پیشنهاد"
								    WHEN "3" THEN "هدایت"
								    ELSE " "
								  END AS doc_type'
								),
							'doc_no','doc_date','desc','year','file_name'
				);
				
				//$table->leftjoin('doc_employees AS e','e.doc_id','=','d.id');
				$table->leftjoin('ministries AS m','m.id','=','d.ministry');
				//$table->leftjoin('department AS dep','dep.id','=','d.sub_dep');
				$table->where('d.year',$year);
				if($doc_type!=0)
				{
					$table->where('d.doc_type',$doc_type);
				}
				if($doc_no!=0)
				{
					$table->where('d.doc_no',$doc_no);
				}
				$table->orderBy('d.id','desc');
		//echo "<pre>";$object->tosql($object->get());exit;
		return $table->get();
	}
	public static function getEmployeesData($year=0,$type=0,$item=0,$status=0)
	{
		$lang = 'dr';
		$table = DB::connection(self::$myDb)
				->table('doc_employees AS e');
				$table->select('e.id','e.status','e.name','f_name','i.name_dr as suggested_type_item','job_title','m.name_dr as dep','doc_no',
							DB::raw(
								'CASE e.suggested_type
								    WHEN "1" THEN "مدال"
								    WHEN "2" THEN "نشان"
								    WHEN "3" THEN "تحسین نامه"
								    WHEN "4" THEN "تقدیرنامه"
								    ELSE " "
								  END AS suggested_type'
								),
							DB::raw(
								'CASE e.status
								    WHEN "1" THEN "اجرا شده"
								    WHEN "2" THEN "اجرا نشده"
								    ELSE "تحت اجرا"
								  END AS status_title'
								),
							DB::raw(
								'CASE e.nationality
								    WHEN "1" THEN "داخلی"
								    WHEN "2" THEN "خارجی"
								    ELSE ""
								  END AS nationality'
								),
							DB::raw('IF(e.emp_type = 1,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast'),
							DB::raw('IF(e.emp_type = 1,er2.name_'.$lang.',mi2.name_'.$lang.') AS rank')
				);
				
				
				$table->leftjoin('received_docs as d','e.doc_id','=','d.id');
				$table->leftjoin('items AS i','i.id','=','e.suggested_type_item');
				$table->leftjoin('ministries AS m','m.id','=','e.ministry');
				//$table->leftjoin('department AS dep','dep.id','=','d.sub_dep');
				$table->leftjoin('auth.employee_rank AS er1','er1.id','=','e.emp_bast');
				$table->leftjoin('auth.military_rank AS mi1','mi1.id','=','e.military_bast');
				$table->leftjoin('auth.employee_rank AS er2','er2.id','=','e.emp_rank');
				$table->leftjoin('auth.military_rank AS mi2','mi2.id','=','e.military_rank');
				$table->where('d.year',$year);
				if($type!=0)
				{
					$table->where('e.suggested_type',$type);
				}
				if($item!=0)
				{
					$table->where('e.suggested_type_item',$item);
				}
				if($status!=0)
				{
					$table->where('e.status', $status);
				}
				$table->orderBy('e.id','desc');
		//echo "<pre>";$object->tosql($object->get());exit;
		return $table->get();
	}
	public static function getDocDetails($id=0)
	{
		$result = DB::connection(self::$myDb)
				->table('received_docs')
				->where('id',$id)
				->first();
		return $result;
	}
	public static function getEmpDetails($emp_id=0)
	{
		$lang = 'dr';
		$result = DB::connection(self::$myDb)
				->table('doc_employees as d')
				->select(
					'd.*',
					DB::raw('IF(d.emp_type = 1,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast')
					)
				->leftjoin('auth.employee_rank AS er1','er1.id','=','d.emp_bast')
				->leftjoin('auth.military_rank AS mi1','mi1.id','=','d.military_bast')
				->where('d.id',$emp_id)
				->first();
		return $result;
	}
	public static function getDocEmployees($id=0,$one_record=false)
	{

		$result = DB::connection(self::$myDb)
				->table('doc_employees')
				->where('doc_id',$id);
		if($one_record)
			return $result->first(); 
		else		
		  return $result->get();
	}
	public static function getAllMinistries()
	{
		$result = DB::connection(self::$myDb)
				->table('ministries')
				->get();
		return $result;
	}
	public static function getDocRejectReason($id=0)
	{
		$result = DB::connection(self::$myDb)
				->table('doc_employees')
				->where('id',$id)
				->first();
		return $result;
	}
	public static function getItems($type=0)
	{
		$result = DB::connection(self::$myDb)
				->table('items')
				->where('type_id',$type)
				->get();
		return $result;
	}
	public static function getEmpApptovedDet($id=0)
	{
		$result = DB::connection(self::$myDb)
				->table('approved_employees')
				->where('emp_id',$id)
				->first();
		return $result;
	}
	public static function getEmpRejectedDet($id=0)
	{
		$result = DB::connection(self::$myDb)
				->table('rejected_employees')
				->where('emp_id',$id)
				->first();
		return $result;
	}
	public static function getSentDeps_rejected($emp_id=0)
	{
		$result = DB::connection(self::$myDb)
				->table('rejected_send_to as r')
				->select('r.dep_id')
				->leftjoin('rejected_employees AS e','e.id','=','r.rejected_id')
				->where('e.emp_id',$emp_id)
				->get();
		return $result;
	}
	public static function getSentDeps_approved($emp_id=0)
	{
		$result = DB::connection(self::$myDb)
				->table('approved_send_to as r')
				->select('r.dep_id')
				->leftjoin('approved_employees AS e','e.id','=','r.approved_id')
				->where('e.emp_id',$emp_id)
				->get();
		return $result;
	}
	public static function insertRecord($table="",$data=array())
	{
		return DB::connection(self::$myDb)->table($table)->insert($data);
	}
	public static function insertRecord_id($table="",$data=array())
	{
		return DB::connection(self::$myDb)->table($table)->insertGetId($data);
	}
	public static function update_record($table='',$data,$where)
	{
		DB::connection(self::$myDb)->table($table)->where($where)->update($data);
	}
	public static function delete_record($table='',$where)
	{
		DB::connection(self::$myDb)->table($table)->where($where)->delete();
	}
	public static function delete_employee_det($doc_id=0)
	{
		//delete from approved_send_to
		DB::connection(self::$myDb)
			->table('approved_send_to')
			->leftjoin('approved_employees AS ae','ae.id','=','approved_send_to.approved_id')
			->leftjoin('doc_employees AS e','e.id','=','ae.emp_id')
			->where('e.doc_id',$doc_id)
			->delete();
		DB::connection(self::$myDb)
			->table('approved_employees')
			->leftjoin('doc_employees AS e','e.id','=','approved_employees.emp_id')
			->where('e.doc_id',$doc_id)
			->delete();
		//delete from rejected_send_to
		DB::connection(self::$myDb)
			->table('rejected_send_to')
			->leftjoin('rejected_employees AS re','re.id','=','rejected_send_to.rejected_id')
			->leftjoin('doc_employees AS e','e.id','=','re.emp_id')
			->where('e.doc_id',$doc_id)
			->delete();
		DB::connection(self::$myDb)
			->table('rejected_employees')
			->leftjoin('doc_employees AS e','e.id','=','rejected_employees.emp_id')
			->where('e.doc_id',$doc_id)
			->delete();
		return true;
	}
	public static function delete_employee_rejected_det($emp_id=0)
	{
		DB::connection(self::$myDb)
			->table('rejected_send_to')
			->leftjoin('rejected_employees AS re','re.id','=','rejected_send_to.rejected_id')
			//->leftjoin('doc_employees AS e','e.id','=','re.emp_id')
			->where('re.emp_id',$emp_id)
			->delete();
	}
	public static function getEvaluationSearchData($request,$total=false)
	{//dd($request);
		$lang = 'dr';
		$table = DB::connection(self::$myDb)
				->table('doc_employees AS e');
				$table->select('e.id','e.doc_id','e.status','e.name','f_name','i.name_dr as suggested_type_item','app_item.name_dr as approved_type_item','job_title','d.year','d.desc','app.title','d.doc_date','doc_no','app.hokm_no','app.hokm_date','app.farman_no','app.farman_date','app.issued_no','issued_date','app.subject','d.doc_no','d.id as recdoc_id','d.file_name as doc_file','app.file_name as app_file','m.name_dr AS dep','m1.name_dr AS elayhe','m2.name_dr AS emp_dir',
							
							DB::raw(
								'CASE app.title
								    WHEN "1" THEN "نهم حوت"
								    WHEN "2" THEN "هشت مارچ"
								    WHEN "3" THEN "بیست و هشت اسد"
								    WHEN "4" THEN "روز معلم"
								    ELSE " "
								  END AS title_name'
								),
							DB::raw(
								'CASE e.suggested_type
								    WHEN "1" THEN "مدال"
								    WHEN "2" THEN "نشان"
								    WHEN "3" THEN "تحسین نامه"
								    WHEN "4" THEN "تقدیرنامه"
								    ELSE " "
								  END AS suggested_type'
								),
							DB::raw(
								'CASE app.approved_type
								    WHEN "1" THEN "مدال"
								    WHEN "2" THEN "نشان"
								    WHEN "3" THEN "تحسین نامه"
								    WHEN "4" THEN "تقدیرنامه"
								    ELSE " "
								  END AS approved_type'
								),
							DB::raw(
								'CASE e.status
								    WHEN "1" THEN "اجرا شده"
								    WHEN "2" THEN "اجرا نشده"
								    ELSE "تحت اجرا"
								  END AS status_title'
								),
							DB::raw(
								'CASE e.nationality
								    WHEN "1" THEN "داخلی"
								    WHEN "2" THEN "خارجی"
								    ELSE ""
								  END AS nationality'
								),
							DB::raw(
								'CASE e.emp_type
								    WHEN "1" THEN "ملکی"
								    WHEN "2" THEN "نظامی"
								    ELSE ""
								  END AS emptype'
								),
							DB::raw(
								'CASE d.doc_type
								    WHEN "1" THEN "مکتوب"
								    WHEN "2" THEN "پیشنهاد"
								    WHEN "3" THEN "هدایت"
								    ELSE ""
								  END AS doc_type'
								),
							DB::raw(
								'CASE app.how
								    WHEN "1" THEN "رئیس جمهور اسلامی افغانستان"
								    WHEN "2" THEN "وزارت/اداره مربوط"
								    WHEN "0" THEN "تفویض نشده"
								    ELSE ""
								  END AS how'
								),
							DB::raw('IF(e.emp_type = 1,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast')
					);
				
				if($request['doc_type']!=0)
				{
					$table->where('d.doc_type',$request['doc_type']);
				}
				if($request['date_from'] != '')
				{
					$sdate = explode("-", $request['date_from']);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);	
					$table->where('d.doc_date','>=',$sdate);
				}
				if($request['date_to'] != '')
				{
					$edate = explode("-", $request['date_to']);
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);	
					$table->where('d.doc_date','<=',$edate);
				}
				if($request['doc_no']!='')
				{
					$table->where('d.doc_no',$request['doc_no']);
				}
				if($request['ministry'] != 0)
				{
					$table->where('d.ministry',$request['ministry']);
				}
				if($request['subject']!='')
				{
					$table->where('d.desc', 'like', '%'.$request["subject"].'%');
				}
				if($request['nationality']!=0)
				{
					$table->where('e.nationality',$request['nationality']);
				}
				if($request['name']!='')
				{
					$table->where('e.name', 'like', '%'.$request["name"].'%');
				}
				if($request['father_name']!='')
				{
					$table->where('e.f_name', 'like', '%'.$request["father_name"].'%');
				}
				if($request['employee_type']==1)
				{
					if($request['emp_bast']!=0)
					{
						$table->where('e.emp_bast',$request['emp_bast']);
					}
				}
				elseif($request['employee_type']==2)
				{
					if($request['military_bast']!=0)
					{
						$table->where('e.military_bast',$request['military_bast']);
					}
				}
				if($request['suggested_type'] != 0)
				{
					if($request['suggested_type_item'] != 0)
					{
						$table->where('e.suggested_type_item',$request['suggested_type_item']);
					}
					else
					{
						$table->where('e.suggested_type',$request['suggested_type']);
					}
				}
				if($request['job']!='')
				{
					$table->where('e.job_title', 'like', '%'.$request["job"].'%');
				}
				if($request['year']!='')
				{
					$table->where('d.year',$request['year']);
				}
					
				//$table->where('e.status',$request["status"]);
				if($request['hokm_date_from'] != '')
				{
					$sdate = explode("-", $request['hokm_date_from']);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);	
					$table->where('app.hokm_date','>=',$sdate);
				}
				if($request['hokm_date_to'] != '')
				{
					$edate = explode("-", $request['hokm_date_to']);
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);	
					$table->where('app.hokm_date','<=',$edate);
				}
				if($request['farman_date_from'] != '')
				{
					$sdate = explode("-", $request['farman_date_from']);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);	
					$table->where('app.farman_date','>=',$sdate);
				}
				if($request['farman_date_to'] != '')
				{
					$edate = explode("-", $request['farman_date_to']);
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);	
					$table->where('app.farman_date','<=',$edate);
				}
				if($request['hokm_no']!='')
				{
					$table->where('app.hokm_no', 'like', '%'.$request["hokm_no"].'%');
				}
				if($request['farman_no']!='')
				{
					$table->where('app.farman_no', 'like', '%'.$request["farman_no"].'%');
				}
				if($request['approved_type'] != 0)
				{
					if($request['approved_type_item'] != 0)
					{
						$table->where('app.approved_type_item',$request['approved_type_item']);
					}
					else
					{
						$table->where('app.approved_type',$request['approved_type']);
					}
				}
				if($request['title'] != 0)
				{
					$table->where('app.title',$request['title']);
				}
				if($request['how'] != '')
				{
					$table->where('app.how',$request['how']);
				}
				if($request['elay_ministry'] != 0)
				{
					$table->where('app.ministry',$request['elay_ministry']);
				}
				if($request['emp_ministry'] != 0)
				{
					$table->where('e.ministry',$request['emp_ministry']);
				}
					
				
				$table->leftjoin('received_docs as d','e.doc_id','=','d.id');
				$table->leftjoin('items AS i','i.id','=','e.suggested_type_item');
				$table->leftjoin('approved_employees as app','e.id','=','app.emp_id');
				$table->leftjoin('items AS app_item','app_item.id','=','app.approved_type_item');
				$table->leftjoin('ministries AS m','m.id','=','d.ministry');
				$table->leftjoin('ministries AS m1','m1.id','=','app.ministry');
				$table->leftjoin('ministries AS m2','m2.id','=','e.ministry');
				
				$table->leftjoin('auth.employee_rank AS er1','er1.id','=','e.emp_bast');
				$table->leftjoin('auth.military_rank AS mi1','mi1.id','=','e.military_bast');
		
		$object = $table->orderBy('e.id');
		//echo $object->tosql();exit;
		if($total)
		{
			return $object->get();
		}
		else
		{	
			return $object->simplePaginate(10);
		}
	}
	public static function getEvaluationSearchData_ajax($request)
	{//dd($request);
		$lang = 'dr';
		$table = DB::connection(self::$myDb)
				->table('doc_employees AS e');
				$table->select('e.id','e.doc_id','e.status','e.name','f_name','i.name_dr as suggested_type_item','app_item.name_dr as approved_type_item','job_title','d.year','d.desc','app.title','d.doc_date','doc_no','app.hokm_no','app.hokm_date','app.farman_no','app.farman_date','app.issued_no','issued_date','app.subject','d.doc_no','d.id as recdoc_id','appdep.name as sender_dep','appsent.name as sent_dep','d.file_name as doc_file','app.file_name as app_file','m2.name_dr AS emp_dir',
							DB::raw('IF(d.dep_type = 1,dep.name,m.name_dr) AS dep'),
							DB::raw(
								'CASE e.suggested_type
								    WHEN "1" THEN "مدال"
								    WHEN "2" THEN "نشان"
								    WHEN "3" THEN "تحسین نامه"
								    WHEN "4" THEN "تقدیرنامه"
								    ELSE " "
								  END AS suggested_type'
								),
							DB::raw(
								'CASE app.approved_type
								    WHEN "1" THEN "مدال"
								    WHEN "2" THEN "نشان"
								    WHEN "3" THEN "تحسین نامه"
								    WHEN "4" THEN "تقدیرنامه"
								    ELSE " "
								  END AS approved_type'
								),
							DB::raw(
								'CASE e.status
								    WHEN "1" THEN "اجرا شده"
								    WHEN "2" THEN "اجرا نشده"
								    ELSE "تحت اجرا"
								  END AS status_title'
								),
							DB::raw(
								'CASE e.nationality
								    WHEN "1" THEN "داخلی"
								    WHEN "2" THEN "خارجی"
								    ELSE ""
								  END AS nationality'
								),
							DB::raw(
								'CASE e.emp_type
								    WHEN "1" THEN "ملکی"
								    WHEN "2" THEN "نظامی"
								    ELSE ""
								  END AS emptype'
								),
							DB::raw(
								'CASE d.doc_type
								    WHEN "1" THEN "مکتوب"
								    WHEN "2" THEN "پیشنهاد"
								    WHEN "3" THEN "هدایت"
								    ELSE ""
								  END AS doc_type'
								),
							DB::raw(
								'CASE app.how
								    WHEN "1" THEN "رئیس جمهور اسلامی افغانستان"
								    WHEN "2" THEN "وزارت/اداره مربوط"
								    WHEN "0" THEN "تفویض نشده"
								    ELSE ""
								  END AS how'
								),
							DB::raw('IF(e.emp_type = 1,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast')
				);
				$table->leftjoin('approved_employees as app','e.id','=','app.emp_id');
				if($request['elay_type']==2)
				{
					$table->leftjoin('approved_send_to as sent_to','app.id','=','sent_to.approved_id');
				}
				if($request['type']==1)
				{
					if($request['general_department'] != 0)
					{
						$table->where('d.sub_dep',$request['general_department']);
					}
				}
				elseif($request['type']==2)
				{
					if($request['ministry'] != 0)
					{
						$table->where('d.ministry',$request['ministry']);
					}
				}
				
				if($request['year']!='')
				{
					$table->where('d.year',$request['year']);
				}
				if($request['employee_type']==1)
				{
					$table->where('e.emp_bast',$request['emp_bast']);
				}
				elseif($request['employee_type']==2)
				{
					$table->where('e.military_bast',$request['military_bast']);
				}
				if($request['name']!='')
				{
					$table->where('e.name', 'like', '%'.$request["name"].'%');
				}
				if($request['doc_type']!=0)
				{
					$table->where('d.doc_type',$request['doc_type']);
				}
				if($request['nationality']!=0)
				{
					$table->where('e.nationality',$request['nationality']);
				}
				if($request['suggested_type'] != 0)
				{
					if($request['suggested_type_item'] != 0)
					{
						$table->where('e.suggested_type_item',$request['suggested_type_item']);
					}
					else
					{
						$table->where('e.suggested_type',$request['suggested_type']);
					}
				}
				
				if($request['date_from'] != '')
				{
					$sdate = explode("-", $request['date_from']);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);	
					$table->where('d.doc_date','>=',$sdate);
				}
				if($request['date_to'] != '')
				{
					$edate = explode("-", $request['date_to']);
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);	
					$table->where('d.doc_date','<=',$edate);
				}
				
				if($request['doc_no']!='')
				{
					$table->where('d.doc_no',$request['doc_no']);
				}
				if($request['subject']!='')
				{
					$table->where('d.desc', 'like', '%'.$request["subject"].'%');
				}
				if($request['father_name']!='')
				{
					$table->where('e.f_name', 'like', '%'.$request["father_name"].'%');
				}
				if($request['job']!='')
				{
					$table->where('e.job_title', 'like', '%'.$request["job"].'%');
				}
				if($request['status']!='')
				{
					$table->where('e.status',$request["status"]);
					if($request['status']==1)
					{
						if($request['morsal_type']==1)
						{
							if($request['morsal_dep'] != 0)
							{
								$table->where('app.sender_sub_dep',$request['morsal_dep']);
							}
						}
						elseif($request['morsal_type']==2)
						{
							if($request['morsal_ministry'] != 0)
							{
								$table->where('app.sender_ministry',$request['morsal_ministry']);
							}
						}
						if($request['elay_type']==1)
						{
							if($request['elay_dep'] != 0)
							{
								$table->where('app.sent_to_sub_dep',$request['elay_dep']);
							}
						}
						elseif($request['elay_type']==2)
						{
							if($request['elay_ministry'] != 0)
							{
								$table->where('sent_to.dep_id',$request['elay_ministry']);
							}
						}
						if($request['hokm_no']!='')
						{
							$table->where('app.hokm_no', 'like', '%'.$request["hokm_no"].'%');
						}
						if($request['farman_no']!='')
						{
							$table->where('app.farman_no', 'like', '%'.$request["farman_no"].'%');
						}
						if($request['hokm_date_from'] != '')
						{
							$sdate = explode("-", $request['hokm_date_from']);
							$sy = $sdate[2];
							$sm = $sdate[1];
							$sd = $sdate[0];
							$sdate = dateToMiladi($sy,$sm,$sd);	
							$table->where('app.hokm_date','>=',$sdate);
						}
						if($request['hokm_date_to'] != '')
						{
							$edate = explode("-", $request['hokm_date_to']);
							$ey = $edate[2];
							$em = $edate[1];
							$ed = $edate[0];
							$edate = dateToMiladi($ey,$em,$ed);	
							$table->where('app.hokm_date','<=',$edate);
						}
						if($request['farman_date_from'] != '')
						{
							$sdate = explode("-", $request['farman_date_from']);
							$sy = $sdate[2];
							$sm = $sdate[1];
							$sd = $sdate[0];
							$sdate = dateToMiladi($sy,$sm,$sd);	
							$table->where('app.farman_date','>=',$sdate);
						}
						if($request['farman_date_to'] != '')
						{
							$edate = explode("-", $request['farman_date_to']);
							$ey = $edate[2];
							$em = $edate[1];
							$ed = $edate[0];
							$edate = dateToMiladi($ey,$em,$ed);	
							$table->where('app.farman_date','<=',$edate);
						}
						if($request['approved_type'] != 0)
						{
							if($request['approved_type_item'] != 0)
							{
								$table->where('app.approved_type_item',$request['approved_type_item']);
							}
							else
							{
								$table->where('app.approved_type',$request['approved_type']);
							}
						}
						if($request['title'] != 0)
						{
							$table->where('app.title',$request['title']);
						}
						if($request['how'] != '')
						{
							$table->where('app.how',$request['how']);
						}
					}
				}
				$table->leftjoin('received_docs as d','e.doc_id','=','d.id');
				$table->leftjoin('items AS i','i.id','=','e.suggested_type_item');
				$table->leftjoin('items AS app_item','app_item.id','=','app.approved_type_item');
				$table->leftjoin('ministries AS m','m.id','=','d.ministry');
				$table->leftjoin('ministries AS m1','m1.id','=','app.ministry');
				$table->leftjoin('ministries AS m2','m2.id','=','e.ministry');
				$table->leftjoin('department AS dep','dep.id','=','d.sub_dep');
				$table->leftjoin('department AS appdep','appdep.id','=','app.sender_sub_dep');
				$table->leftjoin('department AS appsent','appsent.id','=','app.sent_to_sub_dep');
				$table->leftjoin('auth.employee_rank AS er1','er1.id','=','e.emp_bast');
				$table->leftjoin('auth.military_rank AS mi1','mi1.id','=','e.military_bast');
		
		$object = $table->orderBy('e.id')->skip($request['pageno']*10)->take(10);
		//echo $object->tosql();exit;
		return $object->get();
	}
	public static function getDocsTotal($year=1390,$doc_type=0,$doc_no=0)
	{
		$table = DB::connection(self::$myDb)
			->table('received_docs');			
			if($doc_type!=0)
			{
				$table->where('doc_type',$doc_type);
			}
			if($doc_no!=0)
			{
				$table->where('doc_no',$doc_no);
			}
			$table->where('year',$year);
		//echo $object->tosql();
		$object = $table->get();
		return $object;
	}
	public static function getDocEmpsTotal($status=null,$year=1390,$doc_type=0,$doc_no=0)
	{
		$table = DB::connection(self::$myDb)
			->table('doc_employees as d')
			->leftjoin('received_docs AS e','e.id','=','d.doc_id');
			if($status!==null)
			{
				$table->where('d.status',$status);
			}
			if($doc_type!=0)
			{
				$table->where('e.doc_type',$doc_type);
			}
			if($doc_no!=0)
			{
				$table->where('e.doc_no',$doc_no);
			}
			$table->where('e.year',$year);
		//echo $object->tosql();
		$object = $table->get();
		return $object;
	}
	public static function medals_totol($type='',$year=1390,$medal_type=0,$item=0)
	{
		$table = DB::connection(self::$myDb)
			->table('approved_employees as d')
			->leftjoin('doc_employees AS m','m.id','=','d.emp_id')
			->leftjoin('received_docs AS e','e.id','=','m.doc_id');
			if($type!='')
			{
				$table->where('d.approved_type',$type);
			}
			if($medal_type!=0)
			{
				$table->where('d.approved_type',$medal_type);
			}
			if($item!=0)
			{
				$table->where('d.approved_type_item',$item);
			}
			$table->where('e.year',$year);
		//echo $object->tosql();
		$object = $table->get();
		return $object;
	}
	public static function getMedalTypes()
	{
		$result= DB::connection(self::$myDb)
				->table('approved_employees')
				->select(DB::raw('count(id) As total'),'approved_type')
				//->where('tashkil_id','!=',0)
				->groupBy('approved_type')
				->get();	
		return $result;
	}
	public static function getFileName($id=0,$table='')
	{
		$table = DB::connection(self::$myDb)
				->table($table)
				->where('id',$id)
				->pluck('file_name');
		return $table;
	}
	public static function getTypeChart($type=0,$year=0)
	{
		$result= DB::connection(self::$myDb)
				->table('approved_employees as t1')
				->select(DB::raw('count(t1.id) As total'),'t1.approved_type_item as type_id')
				->leftjoin('doc_employees AS e','e.id','=','t1.emp_id')
				->leftjoin('received_docs AS d','d.id','=','e.doc_id')
				->where('t1.approved_type',$type)
				->where('d.year',$year)
				->groupBy('t1.approved_type_item')
				->get();	
		return $result;
	}
	public static function getSubTypes($type=0)
	{
		$result= DB::connection(self::$myDb)
				->table('items')
				->select('id','name_dr')
				->where('type_id',$type)
				->get();	
		return $result;
	}
	public static function getEvaluationExcelData($request)
	{
		$lang = 'dr';
		$table = DB::connection(self::$myDb)
				->table('doc_employees AS e');
				$table->select('e.id','e.doc_id','e.status','e.name','f_name','i.name_dr as suggested_type_item','app_item.name_dr as approved_type_item','job_title','doc_no','d.doc_date','d.doc_date_string','d.desc','app.hokm_no','app.hokm_date','app.hokm_date_string','app.farman_no','app.farman_date','app_item.name_dr as approved_type_name','app.issued_no','issued_date','i.name_dr as suggested_type_name','appdep.name as sentdep','app.sent_to_dep_type as app_dep_type',
							DB::raw('IF(d.dep_type = 1,dep.name,m.name_dr) AS dep'),
							DB::raw(
								'CASE d.doc_type
								    WHEN "1" THEN "مکتوب"
								    WHEN "2" THEN "پیشنهاد"
								    WHEN "3" THEN "هدایت"
								    ELSE " "
								  END AS doc_type_name'
								),
							DB::raw(
								'CASE e.status
								    WHEN "1" THEN "اجرا شده"
								    WHEN "2" THEN "اجرا نشده"
								    ELSE "تحت اجرا"
								  END AS status_title'
								),
							DB::raw(
								'CASE e.nationality
								    WHEN "1" THEN "داخلی"
								    WHEN "2" THEN "خارجی"
								    ELSE ""
								  END AS nationality'
								),
							DB::raw(
								'CASE e.emp_type
								    WHEN "1" THEN "ملکی"
								    WHEN "2" THEN "نظامی"
								    ELSE ""
								  END AS emp_type_name'
								),
							DB::raw(
								'CASE app.title
								    WHEN "1" THEN "نهم حوت"
								    WHEN "2" THEN "هشت مارچ"
								    WHEN "3" THEN "بیست و هشت اسد"
								    WHEN "4" THEN "روز معلم"
								    ELSE " "
								  END AS title_name'
								),
							DB::raw(
								'CASE app.how
								    WHEN "1" THEN "رئیس جمهور"
								    WHEN "2" THEN "اداره مربوطه"
								    ELSE "تفویض نشده"
								  END AS how'
								),
							DB::raw('IF(e.emp_type = 1,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast')
				);
				$table->leftjoin('approved_employees as app','e.id','=','app.emp_id');
				if($request['type']==1)
				{
					if($request['general_department'] != 0)
					{
						if($request['sub_dep'] != 0)
						{
							$table->where('d.sub_dep',$request['sub_dep']);
						}
						else
						{
							$table->where('d.gen_dep',$request['general_department']);
						}
					}
				}
				else
				{
					if($request['ministry'] != 0)
					{
						$table->where('d.ministry',$request['ministry']);
					}
				}
				if($request['year']!='')
				{
					$table->where('d.year',$request['year']);
				}
				if($request['employee_type']==1)
				{
					$table->where('e.emp_bast',$request['emp_bast']);
				}
				elseif($request['employee_type']==2)
				{
					$table->where('e.military_bast',$request['military_bast']);
				}
				if($request['name']!='')
				{
					$table->where('e.name', 'like', '%'.$request["name"].'%');
				}
				if($request['doc_type']!=0)
				{
					$table->where('d.doc_type',$request['doc_type']);
				}
				if($request['nationality']!=0)
				{
					$table->where('e.nationality',$request['nationality']);
				}
				if($request['suggested_type'] != 0)
				{
					if($request['suggested_type_item'] != 0)
					{
						$table->where('e.suggested_type',$request['suggested_type_item']);
					}
					else
					{
						$table->where('e.suggested_type_item',$request['suggested_type']);
					}
				}
				
				if($request['date_from'] != '')
				{
					$sdate = explode("-", $request['date_from']);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);	
					$table->where('d.doc_date','>=',$sdate);
				}
				if($request['date_to'] != '')
				{
					$edate = explode("-", $request['date_to']);
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);	
					$table->where('d.doc_date','<=',$edate);
				}
				if($request['hokm_date_from'] != '')
				{
					$sdate = explode("-", $request['hokm_date_from']);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);	
					$table->where('app.hokm_date','>=',$sdate);
				}
				if($request['hokm_date_to'] != '')
				{
					$edate = explode("-", $request['hokm_date_to']);
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);	
					$table->where('app.hokm_date','<=',$edate);
				}
				if($request['farman_date_from'] != '')
				{
					$sdate = explode("-", $request['farman_date_from']);
					$sy = $sdate[2];
					$sm = $sdate[1];
					$sd = $sdate[0];
					$sdate = dateToMiladi($sy,$sm,$sd);	
					$table->where('app.farman_date','>=',$sdate);
				}
				if($request['farman_date_to'] != '')
				{
					$edate = explode("-", $request['farman_date_to']);
					$ey = $edate[2];
					$em = $edate[1];
					$ed = $edate[0];
					$edate = dateToMiladi($ey,$em,$ed);	
					$table->where('app.farman_date','<=',$edate);
				}
				if($request['doc_no']!='')
				{
					$table->where('d.doc_no',$request['doc_no']);
				}
				if($request['subject']!='')
				{
					$table->where('d.desc', 'like', '%'.$request["subject"].'%');
				}
				if($request['father_name']!='')
				{
					$table->where('e.f_name', 'like', '%'.$request["father_name"].'%');
				}
				if($request['job']!='')
				{
					$table->where('e.job_title', 'like', '%'.$request["job"].'%');
				}
				if($request['status']!='')
				{
					$table->where('e.status',$request["status"]);
					if($request['status']==1)
					{
						if($request['hokm_no']!='')
						{
							$table->where('app.hokm_no', 'like', '%'.$request["hokm_no"].'%');
						}
						if($request['farman_no']!='')
						{
							$table->where('app.farman_no', 'like', '%'.$request["farman_no"].'%');
						}
						if($request['approved_type'] != 0)
						{
							if($request['approved_type_item'] != 0)
							{
								$table->where('app.approved_type_item',$request['approved_type_item']);
							}
							else
							{
								$table->where('app.approved_type',$request['approved_type']);
							}
						}
						if($request['title'] != 0)
						{
							$table->where('app.title',$request['title']);
						}
						if($request['how'] != '')
						{
							$table->where('app.how',$request['how']);
						}
					}
				}
				$table->leftjoin('received_docs as d','e.doc_id','=','d.id');
				$table->leftjoin('items AS i','i.id','=','e.suggested_type_item');
				$table->leftjoin('items AS app_item','app_item.id','=','app.approved_type_item');
				$table->leftjoin('ministries AS m','m.id','=','d.ministry');
				$table->leftjoin('department AS dep','dep.id','=','d.sub_dep');
				$table->leftjoin('department AS appdep','appdep.id','=','app.sent_to_sub_dep');
				$table->leftjoin('auth.employee_rank AS er1','er1.id','=','e.emp_bast');
				$table->leftjoin('auth.military_rank AS mi1','mi1.id','=','e.military_bast');
		
		$object = $table->orderBy('e.id');
		//echo $object->tosql();exit;
		return $object->get();
	}
	public static function getDocsAjax($request,$per_page=10)
	{
		$table = DB::connection(self::$myDb)
			->table('received_docs as d');
			$table->select('d.id',
						DB::raw(
							'CASE d.doc_type
							    WHEN "1" THEN "مکتوب"
							    WHEN "2" THEN "پیشنهاد"
							    WHEN "3" THEN "هدایت"
							    ELSE " "
							  END AS doc_type'
							),
						DB::raw('IF(d.dep_type = 1,dep.name,m.name_dr) AS dep'),
						'doc_no','doc_date','desc','doc_date_string'
			);
			
			//$table->leftjoin('doc_employees AS e','e.doc_id','=','d.id');
			$table->leftjoin('ministries AS m','m.id','=','d.ministry');
			$table->leftjoin('department AS dep','dep.id','=','d.sub_dep');
			$table->where('d.year',$request['year']);
			if($request['doc_type']!='')
			{
				$table->where('d.doc_type',$request['doc_type']);
			}
			if($request['doc_no']!='')
			{
				$table->where('d.doc_no',$request['doc_no']);
			}
			$table->orderBy('d.created_at');
		//echo $object->tosql();
		$object = $table->paginate($per_page);
		return $object;
	}

	public static function getEmployeeDoc($emp_id)
	{
		$result = DB::connection(self::$myDb)
				->table('doc_employees')
				->where('id',$emp_id)
				->first();
		return $result;
	}
}
