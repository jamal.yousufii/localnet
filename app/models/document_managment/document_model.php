<?php namespace App\models\document_managment;
use App\http\Controllers\document_managment\Helpers;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Input;
use get;
use Auth;

class document_model extends Model {

	protected $table ='document_managment.executive_documents';
	// public $timestamps = false;

	public static function get_document_hakom(){

		$rows=\DB::table('document_managment.executive_documents as document')
		->select('document.*','dept.name as dept_name')
		->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')

		->leftjoin('auth.users AS users','users.id','=','document.created_by')
		->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
		->where('emp.department',getMyDepartmentId())
		->where('document.status',0)
		->where('document.document_type',1)
		->orderby('document.id','desc')
		->paginate(20);
		$data['rows'] = $rows;
		return $data;

}
	public static function get_document_maktob(){


		$rows=\DB::table('document_managment.executive_documents as document')
		->select('document.*','dept.name as dept_name')
		->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
		->leftjoin('auth.users AS users','users.id','=','document.created_by')
		->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
		->where('emp.department',getMyDepartmentId())
		->where('document.status',0)
		->where('document.document_type',2)
		->orderby('document.id','desc')
		->paginate(20);
					$data['rows'] = $rows;
			return $data;

}

	public static function get_makatab_warada(){


		$rows=\DB::table('document_managment.executive_documents as document')
		->select('document.*','dept.name as dept_name')
		->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
		->leftjoin('auth.users AS users','users.id','=','document.created_by')
		->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
		->where('emp.department',getMyDepartmentId())
		->where('document.status',0)
		->where('document.document_type',2)
		->where('document.type_id',2)
		->orderby('document.id','desc')
		->paginate(20);
					$data['rows'] = $rows;
			return $data;

}
	public static function get_maktob_sadara(){

		$rows=\DB::table('document_managment.executive_documents as document')
		->select('document.*','dept.name as dept_name')
		->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
		->leftjoin('auth.users AS users','users.id','=','document.created_by')
		->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
		->where('emp.department',getMyDepartmentId())
		->where('document.status',0)
		->where('document.document_type',2)
		->where('document.type_id',1)
		->orderby('document.id','desc')
		->paginate(20);
						$data['rows'] = $rows;
				return $data;

	}
	public static function get_document_copy_som(){


			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',13)
			->orderby('document.id','desc')
			->paginate(20);
						$data['rows'] = $rows;
				return $data;

	}

		public static function get_document_pashnahad(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',3)
			->orderby('document.id','desc')->paginate(20);
						$data['rows'] = $rows;
				return $data;

	}
		public static function get_pashnahad_sadira(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',3)
			->where('document.type_id',1)
			->orderby('document.id','desc')
			->paginate(20);
						$data['rows'] = $rows;
				return $data;

	}
		public static function get_pashnahad_warida(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',3)
			->where('document.type_id',2)			->orderby('document.id','desc')->paginate(20);
						$data['rows'] = $rows;
				return $data;

	}
		public static function get_document_farman(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',4)
			->orderby('document.id','desc')
			->paginate(20);
						$data['rows'] = $rows;
				return $data;
	}
		public static function get_document_istalam(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',5)
			->orderby('document.id','desc')
			->paginate(20);
						$data['rows'] = $rows;
				return $data;

	}
		public static function get_document_feceno(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',6)
			->orderby('document.id','desc')
			->paginate(20);
						$data['rows'] = $rows;
				return $data;

	}

		public static function get_document_waraqa_darkhasti(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',14)
			->orderby('document.id','desc')
			->paginate(20);
						$data['rows'] = $rows;
				return $data;

	}
		public static function get_document_pashnahad_he(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',8)
			->orderby('document.id','desc')
			->paginate(2);
						$data['rows'] = $rows;
				return $data;


	}
		public static function get_document_feceno_he(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',9)
			->orderby('document.id','desc')
			->paginate(20);
						$data['rows'] = $rows;
				return $data;
			}
		public static function get_document_istalam_he(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',12)
			->orderby('document.id','desc')
			->paginate(20);
						$data['rows'] = $rows;
				return $data;


	}
		public static function get_document_tayenat(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',10)
			->orderby('document.id','desc')
			->paginate(20);
						$data['rows'] = $rows;
				return $data;

	}
		public static function get_document_mosawibat(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',15)
			->orderby('document.id','desc')
			->paginate(20);
						$data['rows'] = $rows;
				return $data;

	}
			public static function get_document_hadayat(){

			$rows=\DB::table('document_managment.executive_documents as document')
			->select('document.*','dept.name as dept_name')
			->leftjoin('document_managment.department AS dept','document.department_id','=','dept.id')
			->leftjoin('auth.users AS users','users.id','=','document.created_by')
			->leftjoin('hr.employees AS emp','emp.id','=','users.employee_id')
			->where('emp.department',getMyDepartmentId())
			->where('document.status',0)
			->where('document.document_type',16)
			->orderby('document.id','desc')
			->paginate(20);
						$data['rows'] = $rows;
				return $data;

	}
		public static function get_last_id(){
			$rows=\DB::table('document_managment.executive_documents as document')
			->select('id')
			->orderby('id','desc')
			->limit('1')
			->get();
			return $rows;

	}

}
