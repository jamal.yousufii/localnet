<?php namespace App\models\document_managment;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
use Input;
use get;


class department extends Model {

	protected $table ='document_managment.department';

		public static function get_department(){

			$rows=\DB::connection('document_managment')->table('department')->select('id','name')->get();
			return $rows;
		}
}
