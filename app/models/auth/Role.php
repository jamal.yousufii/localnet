<?php

namespace App\models\auth;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
  /**
   * Get the request that owns the project.
   */
  public function section()
  {
      return $this->belongsTo('App\models\auth\Section');
  }
  public function users()
  {
      return $this->belongsToMany('App\User');
  }

}
