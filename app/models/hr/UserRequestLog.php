<?php

namespace App\models\hr;
use Illuminate\Database\Eloquent\Model;

class UserRequestLog extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
    */
	protected $table 		= 'user_request_log';

}

?>