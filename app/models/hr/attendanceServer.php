<?php

namespace App\models\hr;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class AttendanceServer extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'attendance';
	public static $myDb 	= "adms_db";
	protected $table 		= 'userinfo';
	
	public static function delete_record()
	{
		DB::connection('attendance')->table('userinfo')->truncate();
	}
}
?>