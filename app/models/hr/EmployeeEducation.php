<?php

namespace App\models\hr;
use Illuminate\Database\Eloquent\Model;

class EmployeeEducation extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
    */
	protected $connection 	= 'hr';
	public static $myDb 	= "hr";
	protected $table 		= 'employee_educations';


}
?>