<?php

namespace App\models\hr;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
    */
	protected $connection 	= 'hr';
	public static $myDb 	= "hr";
	protected $table 		= 'employees';

    public function subDepartment()
    {
        return $this->belongsTo('App\models\hr\Department','department');
    }

    public function mainDepartment()
    {
        return $this->belongsTo('App\models\hr\Department','general_department');
    }

}
?>