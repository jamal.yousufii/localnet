<?php

namespace App\models\hr;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;
use DateTime;
use DateInterval;
use DatePeriod;
use App\models\hr\EmployeeEducation; 
class HrOperation extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
    */
	protected $connection 	= 'hr';
	public static $myDb 	= "hr";
	protected $table 		= 'employees';

	public static function getData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS fullname'),
							//DB::raw('CONCAT(t1.name_dr," ",t1.last_name) AS fullname'),
							//'t1.name_dr AS fullname',
							't1.father_name_dr AS father_name',
							'ta.tainat as number_tayenat',
							DB::raw(
								'CASE t1.gender
								    WHEN "M" THEN "Male"
								    WHEN "F" THEN "Female"
								    ELSE " "
								  END AS gender'
								),
							'pro.name_'.$lang.' AS original_province',
							DB::raw('IF(t1.employee_type != 3,er.name_'.$lang.',mi.name_'.$lang.') AS rank'),
							DB::raw('IF(t1.employee_type != 3,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast'),
							//'er.name_'.$lang.' AS rank',
							//'er1.name_'.$lang.' AS bast',
							't1.current_position_dr',
							'dep.name AS department',
							't1.appointment_date_current_position AS emp_date',
							DB::raw('IF(t1.card_issued != "0000-00-00",t1.card_issued,"NOT ISSUED") AS card_issued')
							);
					$table->where('t1.mawqif_employee',1);
					// Filter employees based on general department only for those who general dept filter rule 
					if(hasRule('hr_attendance','filter_general_dept'))
					{
						$table->whereIn('t1.general_department',getUserGeneralDept()); 
					}
					// Filter employees based on sub department only for those who sub dept filter rule 
					if(hasRule('hr_attendance','filter_department'))
					{
						$table->whereIn('t1.department',getUserSubDept()); 
					}
					if(Input::get('id_number'))
					{
						$table->where('t1.id',Input::get('id_number'));
					}
					if(Input::get('full_name'))
					{
						$table->where('t1.name_dr','like','%'.Input::get("full_name").'%'); 
					}
					if(Input::get('father_name'))
					{
						$table->where('t1.father_name_dr','like','%'.Input::get("father_name").'%');
					}
					if(Input::get('last_name'))
					{
						$table->where('t1.last_name','like','%'.Input::get("last_name").'%'); 
					}
		$table->leftjoin('tashkilat AS ta','ta.id','=','t1.tashkil_id');
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','ta.bast');
		$table->leftjoin('auth.military_rank AS mi','mi.id','=','t1.emp_rank');
		$table->leftjoin('auth.military_rank AS mi1','mi1.id','=','ta.bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');


		if($user_dep_type!=0)
		{
		 	$table->where('t1.dep_type',$user_dep_type);
		}
        $table->orderBy('t1.name_dr');    
		if($table->count()>0){
			return $table->paginate(10);
		}else{
			return "";
        }
		//echo $object->tosql();
		//return $object->get();
	}
	public static function getTrainingEmployeeData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS fullname'),
							//DB::raw('CONCAT(t1.name_dr," ",t1.last_name) AS fullname'),
							//'t1.name_dr AS fullname',
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							DB::raw(
								'CASE t1.gender
								    WHEN "M" THEN "Male"
								    WHEN "F" THEN "Female"
								    ELSE " "
								  END AS gender'
								),
							'pro.name_'.$lang.' AS original_province',
							DB::raw('IF(t1.employee_type != 3,er.name_'.$lang.',mi.name_'.$lang.') AS rank'),
							DB::raw('IF(t1.employee_type != 3,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast'),
							//'er.name_'.$lang.' AS rank',
							//'er1.name_'.$lang.' AS bast',
							't1.current_position_dr',
							'dep.name AS department',
							't1.appointment_date_current_position AS emp_date',
							DB::raw('IF(t1.card_issued != "0000-00-00",t1.card_issued,"NOT ISSUED") AS card_issued')
							);
					$table->where('t1.mawqif_employee',1);
					if(Input::get('id_number'))
					{
						$table->where('t1.id',Input::get('id_number'));
					}
					if(Input::get('full_name'))
					{
						$table->where('t1.name_dr','like','%'.Input::get("full_name").'%');
					}
					if(Input::get('last_name'))
					{
						$table->where('t1.last_name','like','%'.Input::get("last_name").'%');
					}
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.military_rank AS mi','mi.id','=','t1.emp_rank');
		$table->leftjoin('auth.military_rank AS mi1','mi1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');

		if($user_dep_type!=0)
		{
			$table->where('t1.dep_type',$user_dep_type);
		}
		$table->orderBy('t1.name_dr');

		//echo $object->tosql();
		return $table->get();
	}
	public static function getTrainingNewEmployeeData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS fullname'),
							//DB::raw('CONCAT(t1.name_dr," ",t1.last_name) AS fullname'),
							//'t1.name_dr AS fullname',
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							DB::raw(
								'CASE t1.gender
								    WHEN "M" THEN "Male"
								    WHEN "F" THEN "Female"
								    ELSE " "
								  END AS gender'
								),
							'pro.name_'.$lang.' AS original_province',
							DB::raw('IF(t1.employee_type != 3,er.name_'.$lang.',mi.name_'.$lang.') AS rank'),
							DB::raw('IF(t1.employee_type != 3,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast'),
							//'er.name_'.$lang.' AS rank',
							//'er1.name_'.$lang.' AS bast',
							't1.current_position_dr',
							'dep.name AS department',
							't1.appointment_date_current_position AS emp_date',
							DB::raw('IF(t1.card_issued != "0000-00-00",t1.card_issued,"NOT ISSUED") AS card_issued')
							);
					$table->where('t1.mawqif_employee',1);
					if(Input::get('id_number'))
					{
						$table->where('t1.id',Input::get('id_number'));
					}
					if(Input::get('full_name'))
					{
						$table->where('t1.name_dr','like','%'.Input::get("full_name").'%');
					}
					if(Input::get('last_name'))
					{
						$table->where('t1.last_name','like','%'.Input::get("last_name").'%');
					}
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.military_rank AS mi','mi.id','=','t1.emp_rank');
		$table->leftjoin('auth.military_rank AS mi1','mi1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');

		if($user_dep_type!=0)
		{
			$table->where('t1.dep_type',$user_dep_type);
		}
		$table->where('t1.training_oriented',0);
		$table->orderBy('t1.name_dr');

		//echo $object->tosql();
		return $table->get();
	}

    public static function getRecruitmentSearchData()
	{
        $lang = 'dr';       
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
                            DB::raw('DISTINCT t1.id'),
                            'education_id',
							'edud.name_dr as education',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS fullname'),
							//DB::raw('CONCAT(t1.name_dr," ",t1.last_name) AS fullname'),
							//'t1.name_dr AS fullname',
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							DB::raw(
								'CASE t1.gender
								    WHEN "M" THEN "Male"
								    WHEN "F" THEN "Female"
								    ELSE " "
								  END AS gender'
                                ),
							//'pro.name_'.$lang.' AS original_province',
							'er.name_'.$lang.' AS rank',
							'er1.name_'.$lang.' AS bast',
							't1.current_position_dr',
							'dep.name AS department',
							't1.phone','t1.mawqif_employee',
							't1.appointment_date_current_position AS emp_date',
							't1.changed','t1.fired','t1.resigned','t1.retired','t1.position_dr','t1.tashkil_id'
							);
					if(Input::get('officetype')!=0)
					{
						$table->where('t1.dep_type',Input::get('officetype'));
					}
					if(Input::get('gender')==1)
					{
						$table->where('t1.gender','M');
					}
					elseif(Input::get('gender')==2)
					{
						$table->where('t1.gender','F');
					}
					if(Input::get('type') !=0)
					{
						$table->where('t1.mawqif_employee',Input::get('type'));
					}

					if(Input::get('general_department') != 0)
					{
						if(Input::get('sub_dep') != 0)
						{
							$table->where('t1.department',Input::get('sub_dep'));
                        }
                        elseif(Input::get('sub_dep')==0 && hasRule('hr_attendance','filter_department'))
                        {
                            $department_id = getUserSubDepartment();
                            $table->whereIn('t1.department',$department_id);
                        }
						else
						{
							$table->where('t1.general_department',Input::get('general_department'));
						}
					}
					$bast = 0;
					if(Input::get('employee_type')==2)
					{
						if(Input::get('ajir_bast')!=0)
						{
							$table->where('t.bast',Input::get('ajir_bast'));
							$bast = Input::get('ajir_bast');
						}
						if(Input::get('ajir_rank')!=0)
						{
							$table->where('t1.ageer_rank',Input::get('ajir_rank'));
						}
					}
					elseif(Input::get('employee_type')==3)
					{
						if(Input::get('military_bast')!=0)
						{
							$table->where('t.bast',Input::get('military_bast'));
							$bast = Input::get('military_bast');
						}
						if(Input::get('military_rank')!=0)
						{
							$table->where('t1.emp_rank',Input::get('military_rank'));
						}

					}
					else
					{
						if(Input::get('emp_bast')!=0)
						{
							$table->where('t.bast',Input::get('emp_bast'));
							$bast = Input::get('emp_bast');
						}
						if(Input::get('emp_rank')!=0)
						{
							$table->where('t1.emp_rank',Input::get('emp_rank'));
						}
					}
					if(Input::get('employee_type')!=0)
					{
						$table->where('t.employee_type',Input::get('employee_type'));
					}
					if(Input::get('docs'))
					{
						$table->leftjoin('attachments AS att','att.doc_id','=','t1.id');
						$table->groupBy('t1.id');
						$table->havingRaw('count(att.id) < 5');
					}
					$edus = Input::get('education_degree');
                    $edu_array=[];
					if(count($edus)>0)
					{
						for($i=0;$i<count($edus);$i++)
						{
                            $edu_array[] = $edus[$i]; 
                        } 
                        $table->whereIn('edu.education_id',$edu_array);
					}
					$years = Input::get('birth_year');
					$b_years=array();
					if(count($years)>0)
					{
						for($i=0;$i<count($years);$i++)
						{
							$b_years[]=$years[$i];
						}
						$table->whereIn('t1.birth_year',$b_years);
					}

					if(Input::get('name')!='')
					{
						$name = Input::get('name');
						$table->where('t1.name_dr', 'like', '%'.$name.'%');
                    } 
                    //Search phone Number 
                    if(Input::get('phone')!='')
                    {
                        $table->where('t1.phone','like','%'.Input::get('phone').'%');
                    } 
		$table->leftjoin('employee_educations AS edu',function($edujoin){
			$edujoin->on('edu.employee_id','=','t1.id')->where('edu.is_latest','=',1);
			// $edujoin->on('edu.employee_id','=','t1.id');
		});
        //$table->where('edu.is_latest',1); // Employee whos Degree is lates one  
		$table->leftjoin('auth.education_degree AS edud','edud.id','=','edu.education_id');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		if(Input::get('employee_type') != 0 || $bast != 0)
		{
			$table->leftjoin('tashkilat AS t','t.id','=','t1.tashkil_id');
		}
        $table->orderBy('edu.id','desc'); 
        // $table->where('t1.dep_type',1);
		if($table->count()>0){
			return $table->paginate(100);
		}else{
			return false;
		}
    }
    
	public static function getEmployeesForPrint($request_for='')
	{
		$lang = 'dr';
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
                    ->table('employees AS t1');
                    if($request_for=='tarfea') // if request is fro tarfea form then have these columns and tab
                    {
                        $table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS fullname'),
							't1.father_name_dr AS father_name','t1.name_en as en_fullname',
							'degree.name_'.$lang.' AS edu_degree',
                            'edu.education_field',
                            'edu.education_place AS university',
                            'edu.graduation_year',
							't1.birth_year',
							't1.phone',
							't1.email',
							DB::raw('IF(t1.employee_type = 2,ar.name_'.$lang.',er.name_'.$lang.') AS rank'),
							DB::raw('IF(t1.employee_type = 2,ar1.name_'.$lang.',er1.name_'.$lang.') AS bast'),

							'dep.name AS department',
							'gdep.name AS gdepartment',
							't1.last_date_tarfee',
                            't1.current_position_dr','t1.appointment_date_current_position AS emp_date','t1.first_date_appointment','t1.employee_type','t1.mawqif_employee','t1.changed','t1.fired','t1.resigned','t1.retired','t1.position_dr','t1.tashkil_id','t1.id_no','t.tainat'
						); 
                    }
                    else
                    {
                        $table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS fullname'),
							't1.father_name_dr AS father_name','t1.name_en as en_fullname',
							'degree.name_'.$lang.' AS edu_degree',
                            'edu.education_field',
                            'edu.education_place AS university',
							't1.birth_year',
							't1.phone',
							't1.email',
							DB::raw('IF(t1.employee_type = 2,ar.name_'.$lang.',er.name_'.$lang.') AS rank'),
							DB::raw('IF(t1.employee_type = 2,ar1.name_'.$lang.',er1.name_'.$lang.') AS bast'),

							't1.current_position_dr',
							'dep.name AS department',
							'gdep.name AS gdepartment',
							't1.appointment_date_current_position AS emp_date',
							't1.employee_type',
							't1.mawqif_employee',
                            't1.changed','t1.fired','t1.resigned','t1.retired','t1.position_dr','t1.tashkil_id','t1.id_no','t1.number_tayenat',
                            'pro.name_dr as origin_province_name',
                            'etni.name_dr as ethnicity'
							);
                    }
					
					$table->where('t1.dep_type',$user_dep_type);
					if(Input::get('gender')==1)
					{
						$table->where('t1.gender','M');
					}
					elseif(Input::get('gender')==2)
					{
						$table->where('t1.gender','F');
					}
					if(Input::get('type') !=0)
					{
						$table->where('t1.mawqif_employee',Input::get('type'));
					}

					if(Input::get('general_department') != 0)
					{
						if(Input::get('sub_dep') != 0)
						{
							$table->where('t1.department',Input::get('sub_dep'));
						}
						else
						{
							$table->where('t1.general_department',Input::get('general_department'));
						}
					}
					$bast = 0;
					if(Input::get('employee_type')==2)
					{
						if(Input::get('ajir_bast')!=0)
						{
							$table->where('t.bast',Input::get('ajir_bast'));
							$bast = Input::get('ajir_bast');
						}
					}
					elseif(Input::get('employee_type')==3)
					{
						if(Input::get('military_bast')!=0)
						{
							$table->where('t.bast',Input::get('military_bast'));
							$bast = Input::get('military_bast');
						}

					}
					else
					{
						if(Input::get('emp_bast')!=0)
						{
							$table->where('t.bast',Input::get('emp_bast'));
							$bast = Input::get('emp_bast');
						}
					}
					if(Input::get('employee_type')!=0)
					{
						$table->where('t.employee_type',Input::get('employee_type'));
					}
					if(Input::get('docs'))
					{
						$table->leftjoin('attachments AS att','att.doc_id','=','t1.id');
						$table->groupBy('t1.id');
						$table->havingRaw('count(att.id) < 5');
					}
					if(Input::get('education_degree')!=0)
					{
						$table->where('edu.education_id',Input::get('education_degree'));
					}
					$years = Input::get('birth_year');
					$b_years=array();
					if(count($years)>0)
					{
						for($i=0;$i<count($years);$i++)
						{
							$b_years[]=$years[$i];
						}
						$table->whereIn('t1.birth_year',$b_years);
					}
					if(Input::get('name')!='')
					{
						$name = Input::get('name');
						$table->where('t1.name_dr', 'like', '%'.$name.'%');
                    }
					if(Input::get('first_job_date')!='')
					{
                        $first_job_date = convertDate(Input::get('first_job_date'),'to_miladi');
                        $first_job_date = explode('-',$first_job_date);
						$table->where('t1.first_job_date', 'like', '%'.$first_job_date[0].'%');
                    }
                    if(Input::get('nationality')!="")
                    {
                        $table->where('t1.nationality',Input::get('nationality'));
                    }
       if($request_for=='tarfea') //Request is for print tarfea form from reqruitment
       {
            $table->leftjoin('tashkilat AS t','t.id','=','t1.tashkil_id');
            $table->leftjoin('auth.employee_rank AS er1','er1.id','=','t.bast');
            $table->leftjoin('auth.employee_rank AS ar1','ar1.id','=','t.bast');
       }
       else
       {             
            if(Input::get('employee_type') != 0 || $bast != 0)
            {
                $table->leftjoin('tashkilat AS t','t.id','=','t1.tashkil_id');
                $table->leftjoin('auth.employee_rank AS er1','er1.id','=','t.bast');
                $table->leftjoin('auth.employee_rank AS ar1','ar1.id','=','t.bast');
            }
            else
            {
                $table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
                $table->leftjoin('auth.employee_rank AS ar1','ar1.id','=','t1.ageer_bast');
            }
        }
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS ar','ar.id','=','t1.ageer_rank');
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		$table->leftjoin('auth.department AS gdep','gdep.id','=','t1.general_department');
		$table->leftjoin('employee_educations AS edu',function($edujoin){
			$edujoin->on('edu.employee_id','=','t1.id')->where('edu.is_latest','=',1);
		});
        $table->leftjoin('auth.education_degree AS degree','degree.id','=','edu.education_id');
        $table->leftjoin('auth.ethnicity as etni','t1.nationality','=','etni.id');
        //$table->where('edu.is_latest',1); 
		
		$object = $table->orderBy('t1.number_tayenat');
		$object = $table->orderBy('edu.id','desc');
		//echo $object->tosql();exit;
        return $object->get();
        
	}
	public static function getFiredData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							//'t1.name_dr AS name',
							't1.number_tayenat',
							't1.fire_no',
							//'pro.name_'.$lang.' AS original_province',
							'er1.name_'.$lang.' AS bast',
							't1.current_position_dr',
							'dep.name AS department',
							't1.fire_date AS emp_date'
							);
					$table->where('t1.fired',1);
					$table->where('t1.mawqif_employee',6);
					if($user_dep_type!=0)
					{
						$table->where('t1.dep_type',$user_dep_type);
					}
		// Filter employees based on general department only for those who general dept filter rule 
		if(hasRule('hr_attendance','filter_general_dept'))
		{
			$table->whereIn('t1.general_department',getUserGeneralDept()); 
		}
		// Filter employees based on sub department only for those who sub dept filter rule 
		if(hasRule('hr_attendance','filter_department'))
		{
			$table->whereIn('t1.department',getUserSubDept()); 
		}

		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getResignData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							't1.resign_no',
							//'pro.name_'.$lang.' AS original_province',
							'er1.name_'.$lang.' AS bast',
							't1.current_position_dr',
							'dep.name AS department',
							't1.resign_date AS emp_date'
							);
					$table->where('t1.resigned',1);
					$table->where('t1.mawqif_employee',9);
					if($user_dep_type!=0)
					{
						$table->where('t1.dep_type',$user_dep_type);
					}
		// Filter employees based on general department only for those who general dept filter rule 
		if(hasRule('hr_attendance','filter_general_dept'))
		{
			$table->whereIn('t1.general_department',getUserGeneralDept()); 
		}
		// Filter employees based on sub department only for those who sub dept filter rule 
		if(hasRule('hr_attendance','filter_department'))
		{
			$table->whereIn('t1.department',getUserSubDept()); 
		}
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getRetireData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							't1.retire_no',
							//'pro.name_'.$lang.' AS original_province',
							'er1.name_'.$lang.' AS bast',
							't1.current_position_dr',
							'dep.name AS department',
							't1.retire_date AS emp_date'
							);
					$table->where('t1.retired',1);
					$table->where('t1.mawqif_employee',5);
					if($user_dep_type!=0)
					{
						$table->where('t1.dep_type',$user_dep_type);
					}
		// Filter employees based on general department only for those who general dept filter rule 
		if(hasRule('hr_attendance','filter_general_dept'))
		{
			$table->whereIn('t1.general_department',getUserGeneralDept()); 
		}
		// Filter employees based on sub department only for those who sub dept filter rule 
		if(hasRule('hr_attendance','filter_department'))
		{
			$table->whereIn('t1.department',getUserSubDept()); 
		}
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getExtraBastData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							DB::raw(
								'CASE t1.employee_type
								    WHEN 3 THEN mr.name_'.$lang.'
								    WHEN 2 THEN aj.name_'.$lang.'
								    ELSE er.name_'.$lang.'
								  END AS bast'
							),
							//'pro.name_'.$lang.' AS original_province',
							't1.current_position_dr',
							'dep.name AS department'
							);

		$table->where('t1.mawqif_employee',2);
		if($user_dep_type!=0)
		{
			$table->where('t1.dep_type',$user_dep_type);
		}
		// Filter employees based on general department only for those who general dept filter rule 
		if(hasRule('hr_attendance','filter_general_dept'))
		{
			$table->whereIn('t1.general_department',getUserGeneralDept()); 
		}
		// Filter employees based on sub department only for those who sub dept filter rule 
		if(hasRule('hr_attendance','filter_department'))
		{
			$table->whereIn('t1.department',getUserSubDept()); 
		}
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_bast');
		$table->leftjoin('auth.ajir_ranks AS aj','aj.id','=','t1.ageer_bast');
		$table->leftjoin('auth.military_rank AS mr','mr.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getTanqistData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							DB::raw(
								'CASE t1.employee_type
								    WHEN 3 THEN mr.name_'.$lang.'
								    WHEN 2 THEN aj.name_'.$lang.'
								    ELSE er.name_'.$lang.'
								  END AS bast'
							),
							//'pro.name_'.$lang.' AS original_province',
							't1.current_position_dr',
							'dep.name AS department'
							);

					$table->where('t1.mawqif_employee',7);
					if($user_dep_type!=0)
					{
						$table->where('t1.dep_type',$user_dep_type);
					}
		// Filter employees based on general department only for those who general dept filter rule 
		if(hasRule('hr_attendance','filter_general_dept'))
		{
			$table->whereIn('t1.general_department',getUserGeneralDept()); 
		}
		// Filter employees based on sub department only for those who sub dept filter rule 
		if(hasRule('hr_attendance','filter_department'))
		{
			$table->whereIn('t1.department',getUserSubDept()); 
		}
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_bast');
		$table->leftjoin('auth.ajir_ranks AS aj','aj.id','=','t1.ageer_bast');
		$table->leftjoin('auth.military_rank AS mr','mr.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getContractsData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							't1.current_position_dr',
							'dep.name AS department'
							);
					$table->whereIn('t1.employee_type',[4,5]);
					//$table->orWhere('t1.employee_type',4);
					if($user_dep_type!=0)
					{
						$table->where('t1.dep_type',$user_dep_type);
					}
		// Filter employees based on general department only for those who general dept filter rule 
		if(hasRule('hr_attendance','filter_general_dept'))
		{
			$table->whereIn('t1.general_department',getUserGeneralDept()); 
		}
		// Filter employees based on sub department only for those who sub dept filter rule 
		if(hasRule('hr_attendance','filter_department'))
		{
			$table->whereIn('t1.department',getUserSubDept()); 
		}

		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		//$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_bast');
		//$table->leftjoin('auth.ajir_ranks AS aj','aj.id','=','t1.ageer_bast');
		//$table->leftjoin('auth.military_rank AS mr','mr.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getChangedData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
		->table('employees AS t1')->join('hr.employee_changes AS changed','changed.employee_id','=','t1.id');
		$table->select(
				't1.id',
				DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
				't1.number_tayenat',
				't1.father_name_dr',
				'pro.name_'.$lang.' AS original_province',
				'er.name_'.$lang.' AS bast',
				'er1.name_'.$lang.' AS rank',
				't1.current_position_dr',
				//'dep.name AS department',
				't1.appointment_date_current_position AS emp_date'
				);
		if($user_dep_type!=0)
		{
			$table->where('t1.dep_type',$user_dep_type);
		}
		// Filter employees based on general department only for those who general dept filter rule 
		if(hasRule('hr_attendance','filter_general_dept'))
		{
			$table->whereIn('t1.general_department',getUserGeneralDept()); 
		}
		// Filter employees based on sub department only for those who sub dept filter rule 
		if(hasRule('hr_attendance','filter_department'))
		{
			$table->whereIn('t1.department',getUserSubDept()); 
		}

		//$table->where('changed.employee_id','=','t1.id');
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		//$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('hr.employee_changes AS changed','changed.employee_id','=','t1.id');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getServiceData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1')
					// ->join('hr.employee_services AS s','s.employee_id','=','t1.id');
					->select(
							't1.id',
							't1.name_dr AS name',
							't1.number_tayenat',
							DB::raw(
								'CASE t1.gender
								    WHEN "M" THEN "Male"
								    WHEN "F" THEN "Female"
								    ELSE " "
								  END AS gender'
								),
							'pro.name_'.$lang.' AS original_province',
							'er.name_'.$lang.' AS bast',
							'er1.name_'.$lang.' AS rank',
							't1.current_position_dr',
							'dep.name AS department',
							't1.appointment_date_current_position AS emp_date',
							DB::raw('IF(t1.card_issued != "0000-00-00",t1.card_issued,"NOT ISSUED") AS card_issued')
							);
		if($user_dep_type!=0)
		{
			$table->where('t1.dep_type',$user_dep_type);
		}
		// Filter employees based on general department only for those who general dept filter rule 
		if(hasRule('hr_attendance','filter_general_dept'))
		{
			$table->whereIn('t1.general_department',getUserGeneralDept()); 
		}
		// Filter employees based on sub department only for those who sub dept filter rule 
		if(hasRule('hr_attendance','filter_department'))
		{
			$table->whereIn('t1.department',getUserSubDept()); 
		}

		//$table->where('changed.employee_id','=','t1.id');
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		$table->where('t1.employee_type',6); // Khedmati employee
		$table->where('t1.mawqif_employee',3); //Mawqif khedmati 
		//$table->leftjoin('hr.employee_changes AS changed','changed.employee_id','=','t1.id');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getDataCard($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.employee_type',
							DB::raw('CONCAT(t1.name_dr," ",t1.last_name) AS name'),
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							DB::raw('IF(t1.gender != "M","Female","Male") AS gender'),
							'pro.name_'.$lang.' AS original_province',
							'er.name_'.$lang.' AS bast',
							'er1.name_'.$lang.' AS rank',
							't1.appointment_date_current_position AS emp_date',
							DB::raw('IF(t1.card_issued != "0000-00-00",t1.card_issued,"NOT ISSUED") AS card_issued')
							);
					if($user_dep_type!=0)
					{
						$table->where('t1.dep_type',$user_dep_type);
					}
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->where(function ($query) {
                $query->where('t1.tashkil_id','>',0)
                      ->orWhere('t1.employee_type',4)
                      ->orWhere('t1.employee_type',5)
                      ->orWhere('t1.employee_type',6);
            });
		//$table->where('t1.mawqif_employee',1);

		if(canUpdateCard('hr_cards'))
		{
			$table->whereIn('ready_to_print',array(1,0));
		}
		elseif(canPrintCard('hr_cards'))
		{
			$table->where('ready_to_print',1);
			//$table->where('RFID','');
			$table->where('RFID',NULL);
		}
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getDataCard_search($dep_id=0,$sub_dep=0,$card_issued='default',$from_date=0,$to_date=0)
	{
		$lang = 'dr';
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.employee_type',
							DB::raw('CONCAT(t1.name_dr," ",t1.last_name) AS name'),
							't1.father_name_dr AS father_name',
							't1.number_tayenat',
							DB::raw('IF(t1.gender != "M","Female","Male") AS gender'),
							'pro.name_'.$lang.' AS original_province',
							'er.name_'.$lang.' AS bast',
							'er1.name_'.$lang.' AS rank',
							't1.appointment_date_current_position AS emp_date',
							DB::raw('IF(t1.card_issued != "0000-00-00",t1.card_issued,"NOT ISSUED") AS card_issued')
							);
					if($user_dep_type!=0)
					{
						$table->where('t1.dep_type',$user_dep_type);
					}
		$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->where(function ($query) {
                $query->where('t1.tashkil_id','!=',0)
                      ->orWhere('t1.employee_type',4)
                      ->orWhere('t1.employee_type',5)
                      ->orWhere('t1.employee_type',6);
            });
		if($sub_dep==0)
		{
			$table->where('t1.general_department',$dep_id);
		}
		else
		{
			$table->where('t1.department',$sub_dep);
		}
		if(canUpdateCard('hr_cards'))
		{
			$table->whereIn('ready_to_print',array(1,0));
		}
		elseif(canPrintCard('hr_cards'))
		{
			$table->where('ready_to_print',1);
			//$table->where('RFID','');
			$table->where('RFID',NULL);
        }
        if($card_issued!='default')
        {  
            if($card_issued=='with_card')
              $table->whereNotNull('t1.card_issued'); 
            else
               $table->whereNull('t1.card_issued'); 
        }
        if($from_date!=0 && $to_date!=0)
        {
            $table->whereBetween('t1.created_at',[convertDate($from_date,'to_miladi'),convertDate($to_date,'to_miladi')]); 
        }

		$object = $table->orderBy('t1.created_at');
		// echo"<pre/>"; print_r($object->tosql()); exit;   
		return $object->get();
	}
	public static function getEmployeeDetails($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employees AS t1')
				->select('t1.*','co.name_dr as e_location','t2.name_en AS b_group','t.year','t.tainat','t.dep_id','t.sub_dep_id as tashkil_sub_dep_id','t1.department as sub_dep_id',
						'p.name_dr as original_pro','cp.name_dr as current_pro','d.name AS dep_name','ed.name_dr as edu_degree','t1.emp_bast as old_bast','t_old.sub_dep_id as tashkil_sub_dep_id_old',
						DB::raw('IF(t.employee_type != 3,er1.name_dr,mi1.name_dr) AS bast'),
						DB::raw('IF(t1.employee_type != 3,er.name_dr,mi.name_dr) AS emprank')
						)
				->leftjoin('auth.blood_group AS t2','t2.id','=','t1.blood_group')
				->leftjoin('tashkilat AS t','t.id','=','t1.tashkil_id')
				->leftjoin('tashkilat AS t_old','t_old.id','=','t1.tashkil_id_old')
				->leftjoin('employee_educations AS edu','edu.employee_id','=','t1.id')
				->leftjoin('auth.countries AS co','co.id','=','edu.edu_location')
				->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank')
				->leftjoin('auth.employee_rank AS er1','er1.id','=','t.bast')
				->leftjoin('auth.military_rank AS mi','mi.id','=','t1.emp_rank')
				->leftjoin('auth.military_rank AS mi1','mi1.id','=','t.bast')
				//->leftjoin('auth.employee_rank AS e','e.id','=','t.bast')
				->leftjoin('auth.provinces AS p','p.id','=','t1.original_province')
				->leftjoin('auth.provinces AS cp','cp.id','=','t1.current_province')
				->leftjoin('auth.department AS d','d.id','=','t1.department')
				->leftjoin('auth.education_degree AS ed','ed.id','=','edu.education_id')
				->where('t1.id',$id)
				->first();
	}
	public static function getDocuments($id=0)
	{
		return DB::connection(self::$myDb)
				->table('attachments')
				->where('doc_id',$id)
				->get();
	}

	public static function getDepartmentEmployees()
	{
		$object = DB::connection(self::$myDb)
				->table('employees AS t1')
				->select(
							't1.name_dr',
							't1.current_position_dr'
						);

			if(Input::get('general_department') != '' && Input::get('sub_dep')=='')
			{
				$object->where('t1.general_department',Input::get('general_department'));
			}
			elseif(Input::get('general_department') != '' && Input::get('sub_dep')!='')
			{
				$object->where('t1.department',Input::get('sub_dep'));
			}

			if(Input::get('show_vacant') != '1')
			{
				$object->whereNotIn('t1.vacant',array(1));
			}

			//return object
			return $object->get();
	}
	public static function insertRecord($table="",$data=array())
	{
		DB::connection(self::$myDb)->table($table)->insert($data);
		DB::disconnect(self::$myDb);
	}
	public static function insertRecord_id($table="",$data=array())
	{
		return DB::connection(self::$myDb)->table($table)->insertGetId($data);
	}
	public static function delete_record($table='',$where)
	{
		DB::connection(self::$myDb)->table($table)->where($where)->delete();
		DB::disconnect(self::$myDb);
	}
	public static function update_record($table='',$data,$where)
	{
		DB::connection(self::$myDb)->table($table)->where($where)->update($data);
		DB::disconnect(self::$myDb);
	}
	public static function delete_holidays($code=0)
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		DB::connection(self::$myDb)->table('month_holiday_dates')->where('code',$code)->where('month_holidays_id','!=',0)->where('dep_type',$user_dep_type)->delete();
		DB::disconnect(self::$myDb);
	}
	public static function delete_EmergencyHolidays($code=0)
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		DB::connection(self::$myDb)->table('month_holiday_dates')->where('code',$code)->where('emergency_id','!=',0)->where('dep_type',$user_dep_type)->delete();
		DB::disconnect(self::$myDb);
	}
	public static function getEmployeeChanges($id=0,$lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employee_changes AS t1');
					$table->select(
							't1.id',
							't1.employee_type',
							//'dep.name as general_department',
							'dep1.name as department',
							'min.name_dr as ministry',
							't1.position_title',
							't1.position_dr',
							'er.name_'.$lang.' AS bast',
							'er1.name_'.$lang.' AS rank',
							't1.changed_date',
							't1.change_type'

							);
					//$table->where('t1.fired',0);
					$table->where('t1.employee_id','=',$id);
		$table->leftjoin('auth.ministries AS min','min.id','=','t1.ministry_id');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_bast');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_rank');
		//$table->leftjoin('auth.department AS dep','dep.id','=','t1.general_department');
		$table->leftjoin('auth.department AS dep1','dep1.id','=','t1.department');
		//$table->leftjoin('hr.employee_changes AS changed','changed.employee_id','=','t1.id');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeLastChange($id=0,$lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employee_changes AS t1');
					$table->select(
							't1.id',
							't1.employee_type',
							//'dep.name as general_department',
							'dep1.name as department',
							'min.name_dr as ministry',
							't1.position_title',
							't1.position_dr',
							'er.name_'.$lang.' AS bast',
							'er1.name_'.$lang.' AS rank',
							't1.changed_date',
							't1.change_type'

							);
					//$table->where('t1.fired',0);
					$table->where('t1.employee_id','=',$id);
		$table->leftjoin('auth.ministries AS min','min.id','=','t1.ministry_id');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_bast');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_rank');
		//$table->leftjoin('auth.department AS dep','dep.id','=','t1.general_department');
		$table->leftjoin('auth.department AS dep1','dep1.id','=','t1.department');
		//$table->leftjoin('hr.employee_changes AS changed','changed.employee_id','=','t1.id');

		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->first();
	}
	public static function getEmployeeServices($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employee_services AS t1');
					$table->select(
							't1.id',
							't1.position_title',
							'dep.name AS dep_name',
							't1.service_date'

							);
					//$table->where('t1.fired',0);
					$table->where('t1.employee_id','=',$id);
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		//$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		//$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('hr.employee_changes AS changed','changed.employee_id','=','t1.id');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getDocumentsByType($id=0,$type='')
	{
		return DB::connection(self::$myDb)
				->table('attachments')
				->where('file_name','like',$type.'_'.$id.'.%')
				->first();
	}
	public static function getEmployeeExperiences($id=0,$type=1)
	{
		return DB::connection(self::$myDb)
				->table('employee_experiences')
				->where('employee_id',$id)
				->where('type',$type)
				->orderBy('date_from')
				->get();
	}
	public static function getEmployeeExperience($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_experiences')
				->where('employee_id',$id)
				//->where('date_from',$date)
				->orderBy('id','desc')
				->first();
	}
	public static function getEmployeeTrainings($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_trainings')
				->where('employee_id',$id)
				->get();
	}
	public static function getEmployeeAOPTrainings($id=0)
	{
		return DB::connection(self::$myDb)
				->table('capacity_trainings as c')
				->leftjoin('capacity_employee_trainings AS t','c.id','=','t.training_id')
				->where('t.employee_id',$id)
				->get();
	}
	public static function getTrainingEmployees($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employees as c')
				->select('c.name_dr','c.last_name','d.name as dep_name')
				->leftjoin('capacity_employee_trainings AS t','c.id','=','t.employee_id')
				->leftjoin('auth.department AS d','d.id','=','c.department')
				->where('t.training_id',$id)
				->get();
	}
	public static function getAllTrainingsEmployees()
	{
		return DB::connection(self::$myDb)
				->table('capacity_employee_trainings AS t')
				->select('c.name_dr','c.last_name','d.name as dep_name','ct.*')
				->leftjoin('employees AS c','c.id','=','t.employee_id')
				->leftjoin('capacity_trainings AS ct','ct.id','=','t.training_id')
				->leftjoin('auth.department AS d','d.id','=','c.department')
				//->where('t.training_id',$id)
				->get();
	}
	public static function getAllEmployeesForAttendanceServer()
	{
		$start_from = Input::get('id_count');

		return DB::connection(self::$myDb)
				->table('employees')
				->select('RFID','name_en','id')
				->where('RFID','!=','')
				->where('id','>',$start_from)
				->orderBy('id')
				->get();
	}
	public static function getAllEmployeesForAttendance()
	{
		$start_from = Input::get('id_count');

		return DB::connection(self::$myDb)
				->table('employees')
				->select('RFID','name_en','id')
				->where('RFID','!=','')
				->where('id','>',$start_from)
				->orderBy('id')
				->get();
	}
	public static function getLastEmployeeId()
	{
		return DB::connection(self::$myDb)
				->table('employees')
				->select('id')
				->where('RFID','!=','')
				//->where('id','>=',$start_from)
				->orderBy('id','desc')
				->first();
	}
	public static function getLastPromotion($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_promotions AS t1')
				->select('t1.employee_id as id','t2.employee_type','t1.emp_rank','t1.emp_bast','t1.military_rank','t1.military_bast','t1.promotion_date AS last_date_tarfee','t1.qadam')
				->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				->where('employee_id',$id)
				->orderBy('t1.id','desc')
				->first();
	}
	public static function getEmployeePromotions($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_promotions AS t1')
				->select('t2.employee_type as current_type','t1.*','rank.name_dr AS rank_name','to.name_dr AS to_rank','ajir_rank.name_dr AS rank_ajir','ajir_to.name_dr AS to_ajir','mi.name_dr AS rank_military','mi_to.name_dr AS to_military')
				->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				->leftjoin('auth.employee_rank AS rank','rank.id','=','t1.emp_rank')
				->leftjoin('auth.employee_rank AS to','to.id','=','t1.emp_bast')
				->leftjoin('auth.ajir_ranks AS ajir_rank','ajir_rank.id','=','t1.ajir_rank')
				->leftjoin('auth.ajir_ranks AS ajir_to','ajir_to.id','=','t1.ajir_bast')
				->leftjoin('auth.military_rank AS mi','mi.id','=','t1.military_rank')
				->leftjoin('auth.military_rank AS mi_to','mi_to.id','=','t1.military_bast')
				->where('t1.employee_id',$id)
				->orderBy('promotion_date')
				->get();
	}
	public static function getEmployeeEvaluations($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_evaluations AS t1')
				->select('t1.*')
				//->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				->where('t1.employee_id',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function getEmployeePunishments($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_punishments AS t1')
				->select('t1.*')
				//->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				->where('t1.employee_id',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function getEmployeeMakafat($id=0,$latest_record=false)
	{
        if($latest_record==false)
        {
		  $employee_makfat = DB::connection(self::$myDb)
				->table('employee_makafats AS t1')
				->select('t1.*')
				//->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				->where('t1.employee_id',$id)
				//->orderBy('id','desc')
				->get();
        }
        else
        {
            $employee_makfat = DB::connection(self::$myDb)
				->table('employee_makafats AS t1')
				->select('t1.*')
				->where('t1.employee_id',$id)
				->orderBy('id','desc')
				->first();
        }

        return $employee_makfat;
	}
	public static function getEmployeeLangs($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_languages AS t1')
				->select('t1.*')
				//->leftjoin('employees AS t2','t2.id','=','t1.employee_id')
				->where('t1.employee_id',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function getEmployeeEducations($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_educations AS t1')
				->select('t1.*','ed.name_dr as degree_name','co.name_dr as location_name')
				->leftjoin('auth.education_degree AS ed','ed.id','=','t1.education_id')
				->leftjoin('auth.countries AS co','co.id','=','t1.edu_location')
				->where('t1.employee_id',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function getEmployeeGarantee($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_garantee AS t1')
				->select('t1.*')
				->where('t1.employee_id',$id)
				->get();
	}
	//communication
	public static function getComData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS fullname'),
							't1.name_dr AS name',
							't1.name_en',
							't1.father_name_dr AS father_name',
							't1.current_position_dr',
							'dep.name AS department',
							DB::raw('IF(t1.card_issued != "0000-00-00",t1.card_issued,"NOT ISSUED") AS card_issued')
							);
					$table->where('t1.mawqif_employee',1);
					$table->where('t1.dep_type',$user_dep_type);

		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		//$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		//$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getComCardData($lang = 'dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employee_cards AS c');
					$table->select(
							't1.id',
							't1.name_dr AS name',
							't1.name_en',
							't1.current_position_dr',
							'dep.name AS department',
							'c.id AS card_id',
							'c.card_no',
							'c.card_color',
							'c.issue_date',
							'c.expiry_date'
							);
					$table->where('t1.fired',0);
					$table->where('t1.resigned',0);
					$table->where('t1.vacant','!=',1);//kambod
					$table->where('t1.vacant','!=',4);//ezafa bast
					$table->where('t1.changed',0);//not changed to out
		$table->leftjoin('employees AS t1','c.employee_id','=','t1.id');
		//$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		//$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');

		$object = $table->orderBy('c.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getComMaktobData($lang = 'dr')
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('com_maktobs AS t1');
					$table->select(
							't1.id',
							't1.number',
							't1.date',
							'dep.name AS source'
							);
					$table->where('t1.dep_type',$user_dep_type);
		//$table->leftjoin('auth.provinces AS pro','pro.id','=','t1.original_province');
		//$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		//$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.sub_dep');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getComEstelamData()
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('com_estelams AS t1');
					$table->select(
							't1.id',
							't1.number',
							't1.date',
							'emp.name_dr AS name'
							);
					$table->where('emp.dep_type',$user_dep_type);
		$table->leftjoin('employees AS emp','emp.id','=','t1.employee_id');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getComSecurityData($type=0)
	{
		$table = DB::connection(self::$myDb)
					->table('com_securities AS t1');
					$table->select(
							't1.id',
							't1.number',
							't1.date',
							't1.file_name'
							);
		$table->where('t1.type',$type);
		//$table->leftjoin('employees AS emp','emp.id','=','t1.employee_id');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getComComplaintsData()
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('com_complaints AS t1');
					$table->select(
							't1.id',
							't1.reg_date',
							't1.desc'
							);
					$table->where('t1.dep_type',$user_dep_type);
		//$table->leftjoin('employees AS emp','emp.id','=','t1.employee_id');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getComComitteData()
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('com_comitteis AS t1');
					$table->select(
							't1.id',
							't1.date',
							't1.number',
							'c.desc'
							);
					$table->where('t1.dep_type',$user_dep_type);
		$table->leftjoin('com_complaints AS c','c.id','=','t1.complain_id');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getRelatedAjirs($id=0,$sub=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.name_dr AS name',
							't1.last_name',
							't1.father_name_dr AS father_name',
							'dep.name AS department',
							't1.number_tayenat',
							't1.emp_bast as emp_rank',
							't1.current_position_dr AS position',
							't1.general_department',
							't1.department as dep_id'
							//DB::raw('count(att.RFID) as user_count')
							);

					if($id!=0)
					{
						if($sub!=0)
						{
							$table->where('t1.department',$sub);
						}
						else
						{
							$table->where('t1.general_department',$id);
						}
					}
					$table->where('t1.tashkil_id','!=',0);//tanqis tashkilati
					$table->where('t1.employee_type',2);//dont include ajirs
					$table->where('t1.mawqif_employee',1);
					$table->where('t1.RFID','>',0);
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('attendance_images AS att','att.RFID','=','t1.RFID');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getRelatedEmployees($id=0,$sub=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.name_dr AS name',
							't1.last_name',
							't1.father_name_dr AS father_name',
							'dep.name AS department',
							't1.number_tayenat',
							't1.emp_bast as emp_rank',
							't1.current_position_dr AS position',
							't1.general_department',
							't1.department as dep_id'
							//DB::raw('count(att.RFID) as user_count')
							);

					if($id!=0)
					{
						if($sub!=0)
						{
							$table->where('t1.department',$sub);
						}
						else
						{
							$table->where('t1.general_department',$id);
						}
					}
					$table->where('t1.tashkil_id','!=',0);//tanqis tashkilati
					$table->where('t1.employee_type','!=',2);//dont include ajirs
					$table->where('t1.mawqif_employee',1);
					$table->where('t1.RFID','>',0);
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('attendance_images AS att','att.RFID','=','t1.RFID');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getRelatedEmployeesAjax($id=0,$sub=0,$per_page=10,$check=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.tashkil_id',
							't1.name_dr AS name',
							't1.last_name',
							't1.father_name_dr AS father_name',
							'dep.name AS department',
							't1.number_tayenat',
							't1.emp_bast as emp_rank',
							't1.current_position_dr AS position',
							't1.general_department',
							't1.department as dep_id',
							't1.in_attendance',
							't1.mawqif_employee'
							//DB::raw('count(att.RFID) as user_count')
							);

					if($id!=0)
					{
						if($sub!=0)
						{
							$table->where('t1.department',$sub);
						}
						else
						{
							$table->where('t1.general_department',$id);
						}
					}
					if($check==2)
					{
						$table->where('t1.employee_type',2);//ajirs
					}
					else
					{
						$table->where('t1.employee_type','!=',2);//dont include ajirs
					}
					//$table->where('t1.mawqif_employee',1);
					//$table->where('t1.RFID','>',0);
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('attendance_images AS att','att.RFID','=','t1.RFID');

		$object = $table->orderBy('t1.id','asc');
		//echo $object->tosql();
        $object = $table->paginate($per_page);
		return $object;
	}
	public static function getRelatedEmployeesAjax_search($item='',$id=0,$sub=0,$check=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.tashkil_id',
							't1.name_dr AS name',
							't1.last_name',
							't1.father_name_dr AS father_name',
							'dep.name AS department',
							't1.number_tayenat',
							't1.emp_bast as emp_rank',
							't1.current_position_dr AS position',
							't1.general_department',
							't1.department as dep_id',
							't1.in_attendance',
							't1.mawqif_employee'
							);
					if($id!=0)
					{
						if($sub!=0)
						{
							$table->where('t1.department',$sub);
						}
						else
						{
							$table->where('t1.general_department',$id);
						}
					}
					if($check==0)
					{
						$table->where('t1.employee_type','!=',2);//dont include ajirs
					}
					if($item!='')
					{
						$table->whereRaw("(t1.name_dr like '%".$item."%' OR t1.last_name like '%".$item."%' OR t1.father_name_dr like '%".$item."%')");

					}

					//$table->where('t1.retired',0);//retire
					//$table->where('t1.RFID','>',0);
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('attendance_images AS att','att.RFID','=','t1.RFID');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		//$object = $table->paginate($per_page);
		return $object->get();
	}
	public static function getEmployees_leave($id=0,$sub=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.name_dr AS name',
							't1.last_name',
							't1.father_name_dr AS father_name',
							//'dep.name AS department',
							't1.number_tayenat',
							't1.emp_bast as emp_rank',
							't1.current_position_dr AS position',
							't1.general_department',
							't1.department as dep_id',
							't1.mawqif_employee',
							't1.position_dr'
							//DB::raw('count(att.RFID) as user_count')
							);

					if($id!=0)
					{
						if($sub!=0)
						{
							$table->where('t1.department',$sub);
						}
						else
						{
							$table->where('t1.general_department',$id);
						}
					}

					//$table->where('t1.fired',0);
					//$table->where('t1.resigned',0);
					//$table->where('t1.vacant','!=',1);//kambod
					//$table->where('t1.vacant','!=',4);//ezafa bast
					//$table->where('t1.position_dr','!=',4);//entezar mahsh
					//$table->where('t1.changed',0);//not changed to out
					//$table->where('t1.tashkil_id','!=',0);//tanqis tashkilati
					//$table->where('t1.employee_type','!=',2);//dont include ajirs
					//$table->where('t1.retired',0);//retire
					//$table->where('t1.RFID','>',0);
		//$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('attendance_images AS att','att.RFID','=','t1.RFID');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeesWithRFID($id=0,$sub=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.name_dr AS name',
							't1.last_name',
							't1.father_name_dr AS father_name',
							'dep.name AS department',
							't1.number_tayenat',
							't1.emp_bast as emp_rank',
							't1.current_position_dr AS position',
							't1.general_department',
							't1.department as dep_id',
							't1.changed',
							't1.fired',
							't1.resigned',
							't1.retired',
							't1.position_dr'
							//DB::raw('count(att.RFID) as user_count')
							);

					if($id!=0)
					{
						if($sub!=0)
						{
							$table->where('t1.department',$sub);
						}
						else
						{
							$table->where('t1.general_department',$id);
						}
					}
					$table->where('t1.employee_type','!=',2);//dont include ajirs
					$table->where('t1.mawqif_employee',1);//active employees
					$table->where('t1.RFID','>',0);
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('attendance_images AS att','att.RFID','=','t1.RFID');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getAjirsWithRFID($id=0,$sub=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.name_dr AS name',
							't1.last_name',
							't1.father_name_dr AS father_name',
							'dep.name AS department',
							't1.number_tayenat',
							't1.emp_bast as emp_rank',
							't1.current_position_dr AS position',
							't1.general_department',
							't1.department as dep_id',
							't1.changed',
							't1.fired',
							't1.resigned',
							't1.retired',
							't1.position_dr'
							//DB::raw('count(att.RFID) as user_count')
							);

					if($id!=0)
					{
						if($sub!=0)
						{
							$table->where('t1.department',$sub);
						}
						else
						{
							$table->where('t1.general_department',$id);
						}
					}

					//$table->where('t1.fired',0);
					//$table->where('t1.resigned',0);
					//$table->where('t1.vacant','!=',1);//kambod
					//$table->where('t1.vacant','!=',4);//ezafa bast
					//$table->where('t1.position_dr','!=',4);//entezar mahsh
					//$table->where('t1.changed',0);//not changed to out
					//$table->where('t1.tashkil_id','!=',0);//tanqis tashkilati
					$table->where('t1.employee_type',2);//include ajirs
					//$table->where('t1.retired',0);//retire
					$table->where('t1.RFID','>',0);
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
		//$table->leftjoin('attendance_images AS att','att.RFID','=','t1.RFID');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeImages($rfid=0,$year,$month)
	{
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month==1)
		{
			$from = dateToMiladi($year-1,12,$sday);
		}
		else
		{
			$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
		}
		$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\

		//remove the last pipe line
		//$days = substr($days, 0, -1);
		$result=  DB::connection('hr')
				->table('attendance_images')
				->select('id','path','status','late_status')
				->where('RFID',$rfid)//check the rfid
				->where('date','>=',$from)
				->where('date','<=',$to)
				//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
				//->whereIn('date',$days)
				->orderBy("date")
				->orderBy("time")
				->get();
				DB::disconnect('hr');
		return $result;

	}
	public static function getEmployeeImages_lost($rfid=0,$year,$month)
	{
		$sday = att_month_days(0);
		$eday = att_month_days(1);
		if($month==1)
		{
			$from = dateToMiladi($year-1,12,$sday);
		}
		else
		{
			$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
		}
		$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\

		//remove the last pipe line
		//$days = substr($days, 0, -1);
		$object =  DB::connection('hr')
				->table('attendance_images')
				->select('id','path','status','late_status')
				->whereIn('RFID',$rfid)//check the rfid
				//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
				->where('date','>=',$from)
				->where('date','<=',$to)
				->orderBy("date")
				->orderBy("time")
				->get();
		DB::disconnect('hr');
		return $object;

	}
	public static function absentEmployeeImages($rfid=0,$year,$month)
	{
		$from = dateToMiladi($year,$month-1,15);//the start day of attendance month is 15 of prevouce month
		$to = dateToMiladi($year,$month,15);//the end day of attendance month is 15 of current month\

		$begin = new DateTime($from);
		$end = new DateTime($to);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);
		$days = array();
		foreach($period As $day)
		{
			$the_day = $day->format( "Y-m-d" );
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_year = $period_det[0];
			$days[] = $period_year.'-'.$period_month.'-'.$period_day;
			//$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
		}

		//remove the last pipe line
		//$days = substr($days, 0, -1);
		return  DB::connection('hr')
				->table('attendance_images')
				->select('id','path','status')
				->where('RFID',$rfid)//check the rfid
				->where('status',1)
				//->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path
				->whereIn('date',$days)
				->get();

	}
	public static function getPhotoByRfid($rfid=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees');
					$table->select(
							'photo','name_dr','last_name','id','current_position_dr','att_out_date','att_in_date','dep_type','employee_type'
							);
		$table->where('RFID',$rfid);
		$object = $table->first();
		DB::disconnect(self::$myDb);
		return $object;
	}
	public static function getPhotoByNoRfid($rfid=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees');
					$table->select(
							'photo','name_dr','last_name','id','current_position_dr','att_out_date','att_in_date',
							'fired','retired','resigned','changed','dep_type','employee_type'
							);
		$table->where('in_attendance',$rfid);

		//echo $object->tosql();
		return $table->first();
	}
	public static function getNextEmployeeRfid($rfid,$type)
	{
		$table = DB::connection(self::$myDb)
					->table('employees');
					$table->select(
							'id','department','employee_type',
							'rfid','att_in_date','photo','current_position_dr'
							);
		$table->where('RFID',$rfid);
		//$table->where('tashkil_id','!=',0);
		$id = $table->first();

		$result = DB::connection(self::$myDb)->table('employees')->select('id','rfid','att_in_date','photo','current_position_dr','employee_type')
						->where('department',$id->department)
						->where('mawqif_employee',1)
						->where('RFID','>',0);
		// if(getUserDepType(Auth::user()->id)->dep_type==1)
		// {//dont include ajirs for ocs
			if($id->employee_type==2)
			{
				$result->where('employee_type',2);
			}
			else
			{
				$result->where('employee_type','!=',2);
			}
		// }
		// else
		// {
		// 	$result->where('employee_type','!=',2);
		// }
		if($type=='next')
		{
			$result->where('id','>',$id->id)->orderBy('id','asc');
		}
		else
		{
			$result->where('id','<',$id->id)->orderBy('id','desc');
		}
		$return = $result->first();
		if($return)
		{
			return $return;
		}
		else
		{
			return $id;
		}
	}

	//tashkil
	public static function getTashkilData($year=0)
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;

		$lang = 'dr';
		$table = DB::connection(self::$myDb)
					->table('tashkilat AS t1');
					$table->select(
							't1.id',
							DB::raw('(CASE WHEN t1.employee_type = 3 THEN mr.name_'.$lang.' ELSE er.name_'.$lang.' END) AS bast'),
							'dep.name AS department',
							'subdep.name AS sub_dep',
							't1.title',
							't1.tainat',
							't1.year','t1.status'
							);
		$table->where('t1.year',$year);
		if($user_dep_type!=0)
		{
			$table->where('t1.dep_type',$user_dep_type);
		}
		// Filter employees based on general department only for those who general dept filter rule 
		if(hasRule('hr_attendance','filter_general_dept'))
		{
			$table->whereIn('t1.dep_id',getUserGeneralDept()); 
		}
		// Filter employees based on sub department only for those who sub dept filter rule 
		if(hasRule('hr_attendance','filter_department'))
		{
			$table->whereIn('t1.sub_dep_id',getUserSubDept()); 
		}
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.bast');
		$table->leftjoin('auth.military_rank AS mr','mr.id','=','t1.bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.dep_id');
		$table->leftjoin('auth.department AS subdep','subdep.id','=','t1.sub_dep_id');
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getTashkilDetail($id=0,$lang='dr')
	{
		$table = DB::connection(self::$myDb)
					->table('tashkilat AS t1');
					$table->select(
							't1.id',
							DB::raw('(CASE WHEN t1.employee_type = 3 THEN mr.name_'.$lang.' ELSE er.name_'.$lang.' END) AS bast_name'),
							't1.title',
							't1.employee_type',
							't1.bast',
							't1.dep_id',
							't1.sub_dep_id',
							't1.tainat',
							't1.job_desc',
							't1.year'
							);
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.bast');
		$table->leftjoin('auth.military_rank AS mr','mr.id','=','t1.bast');
		$object = $table->where('t1.id',$id);

		return $object->first();
	}
	public static function getCapacityTrainingsData()
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('capacity_trainings AS t1');
					$table->select(
							't1.id',
							't1.title',
							't1.location',
							't1.start_date',
							't1.end_date',
							't1.seat_no',
							DB::raw('IF(t1.type != "0","خارجی","داخلی") AS type')
							);
					$table->where('t1.dep_type',$user_dep_type);
		$object = $table->orderBy('t1.start_date','asc');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getCapacityTrainersData()
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('capacity_trainers AS t1');
					$table->select(
							't1.*'
							);
					$table->where('t1.dep_type',$user_dep_type);
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeCapacityTrainings($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('capacity_employee_trainings AS t1');
					$table->select(
							't1.id',
							'tr.title',
							'tr.location',
							'tr.start_date',
							'tr.end_date',
							DB::raw('IF(tr.type != "0","خارجی","داخلی") AS type')
							);
		$table->where('t1.employee_id',$id);
		$table->leftjoin('capacity_trainings AS tr','tr.id','=','t1.training_id');
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getTraining($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('capacity_trainings AS t1');
		$table->where('t1.id',$id);
		//$table->leftjoin('capacity_trainings AS tr','tr.id','=','t1.training_id');
		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->first();
	}
	public static function getEmployeeSalary($emp_id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employee_salary')
					->where('employee_id',$emp_id);
		return $table->first();
	}
	public static function getEmployeePayroll($id=0,$lang='dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.name_dr AS name',
							't1.father_name_dr AS father_name',
							's.main_salary',
							's.extra_salary',
							's.prof_salary',
							's.kadri_salary',
							's.retirment',
							't1.current_position_dr',
							't1.employee_type',
							't1.emp_rank',
							't1.emp_bast'
							);
					$table->where('t1.fired',0);
					$table->where('t1.resigned',0);
					$table->where('t1.vacant','!=',1);//kambod
					$table->where('t1.vacant','!=',4);//ezafa bast
					$table->where('t1.position_dr','!=',4);//entezar mahsh
					$table->where('t1.changed',0);//not changed to out
					$table->where('t1.tashkil_id','!=',0);//tanqis tashkilati
					$table->where('t1.employee_type','!=',2);//dont include ajirs
					$table->where('t1.retired',0);//retire

					$table->where('t1.department',$id);

		$table->leftjoin('employee_salary AS s','s.employee_id','=','t1.id');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getAjirPayroll($id=0,$lang='dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.name_dr AS name',
							't1.father_name_dr AS father_name',
							's.main_salary',
							's.extra_salary',
							's.prof_salary',
							's.kadri_salary',
							's.retirment',
							't1.current_position_dr',
							't1.employee_type',
							't1.emp_rank',
							't1.emp_bast'
							);
					$table->where('t1.fired',0);
					$table->where('t1.resigned',0);
					$table->where('t1.vacant','!=',1);//kambod
					$table->where('t1.vacant','!=',4);//ezafa bast
					$table->where('t1.position_dr','!=',4);//entezar mahsh
					$table->where('t1.changed',0);//not changed to out
					$table->where('t1.tashkil_id','!=',0);//tanqis tashkilati
					$table->where('t1.employee_type',2);//dont include employees
					$table->where('t1.retired',0);//retire

					$table->where('t1.department',$id);

		$table->leftjoin('employee_salary AS s','s.employee_id','=','t1.id');

		$object = $table->orderBy('t1.created_at');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeEstehqaq($id=0,$year=0,$month=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employee_payrolls AS t1');
					$table->select(
							't1.*'
							);

					$table->where('t1.employee_id',$id);
					$table->where('t1.year',$year);
					$table->where('t1.month','<=',$month);

		$object = $table->orderBy('t1.id','desc');
		//echo $object->tosql();
		return $object->first();
	}
	public static function getEmployeeForAttPrint($id=0,$lang='dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
		$table->select(
				't1.id',
				't1.RFID',
				't1.employee_type','t1.number_tayenat',
				DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
				't1.father_name_dr AS father_name',
				DB::raw('IF(t1.employee_type != 3,er.name_'.$lang.',mi.name_'.$lang.') AS rank'),
				DB::raw('IF(t1.employee_type != 3,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast'),
				't1.current_position_dr'
				);

		//$table->whereNotIn('t1.id',[572]);//active employees only
		$table->where('t1.mawqif_employee',1);//active employees only
		$table->where('t1.tashkil_id','!=',0);//new tashkil
		$table->where('t1.employee_type','!=',2);//dont include ajirs
		$table->where('t1.department',$id);
		$table->leftjoin('tashkilat AS ta','ta.id','=','t1.tashkil_id');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','ta.bast');
		$table->leftjoin('auth.military_rank AS mi','mi.id','=','t1.emp_rank');
		$table->leftjoin('auth.military_rank AS mi1','mi1.id','=','ta.bast');
		$object = $table->orderBy('ta.tainat');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getAjirForAttPrint($id=0,$lang='dr')
	{
		$table = DB::connection(self::$myDb)
					->table('employees AS t1');
					$table->select(
							't1.id',
							't1.RFID',
							't1.employee_type','t1.number_tayenat',
							DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
							't1.father_name_dr AS father_name',
							DB::raw('IF(t1.employee_type != 3,er.name_'.$lang.',mi.name_'.$lang.') AS rank'),
							DB::raw('IF(t1.employee_type != 3,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast'),
							't1.current_position_dr'
							);



					$table->where('t1.mawqif_employee',1);//active employees only
					$table->where('t1.tashkil_id','!=',0);//new tashkil
					$table->where('t1.employee_type',2);//dont include ajirs

					$table->where('t1.department',$id);
		$table->leftjoin('tashkilat AS ta','ta.id','=','t1.tashkil_id');
		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
		$table->leftjoin('auth.employee_rank AS er1','er1.id','=','ta.bast');
		$table->leftjoin('auth.military_rank AS mi','mi.id','=','t1.emp_rank');
		$table->leftjoin('auth.military_rank AS mi1','mi1.id','=','ta.bast');

		$object = $table->orderBy('t1.number_tayenat');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeForAttPrint_aop($id=0,$lang='dr')
	{
		$table = DB::connection(self::$myDb)
			->table('employees AS t1');
			$table->select(
					't1.id',
					't1.RFID',
					't1.employee_type','t1.number_tayenat',
					DB::raw('IF(t1.last_name != "",CONCAT(t1.name_dr," ",t1.last_name),t1.name_dr) AS name'),
					't1.father_name_dr AS father_name',
					DB::raw('IF(t1.employee_type != 3,er.name_'.$lang.',mi.name_'.$lang.') AS rank'),
					DB::raw('IF(t1.employee_type != 3,er1.name_'.$lang.',mi1.name_'.$lang.') AS bast'),
					't1.current_position_dr'
					);
			$table->where('t1.mawqif_employee',1);//active employees only
			$table->where('t1.tashkil_id','>',0);//new tashkil
			//$table->where('t1.changed',0);//not changed to out
			$table->where('t1.department',$id);
			$table->where('t1.employee_type','!=',2);//dont include ajirs
			$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank');
			$table->leftjoin('auth.employee_rank AS er1','er1.id','=','t1.emp_bast');
			$table->leftjoin('auth.military_rank AS mi','mi.id','=','t1.emp_rank');
			$table->leftjoin('auth.military_rank AS mi1','mi1.id','=','t1.emp_bast');

			$object = $table->orderBy('t1.number_tayenat');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getRelatedBasts($id=0,$type=0,$year=1396)
	{
        $user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('tashkilat AS t1');
					$table->select(
							't1.id',
							't1.title as name',
							't1.tainat'
							);

					if($type == 0)
					{
						$table->where('t1.sub_dep_id',$id);
						$table->where('t1.status',0);
					}
					else
					{
						$table->whereRaw('t1.sub_dep_id = '.$id.' AND (t1.status = 0 || t1.id = '.$type.')');
                    }
                  $table->where('t1.dep_type',$user_dep_type);   
		          $table->where('t1.year',$year);
		$object = $table->orderBy('t1.tainat');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getExistBasts($id=0,$type=0)
	{
		$sh_date = dateToShamsi(date('Y'),date('m'),date('d'));
		$year = explode('-', $sh_date);
		$year = $year[0];
		$table = DB::connection(self::$myDb)
					->table('tashkilat AS t1');
					$table->select(
							't1.id',
							't1.title as name',
							't1.tainat'
							);
					$table->where('t1.sub_dep_id',$id);
					$table->where('t1.status',1);
					$table->where('t1.year',$year);
		$object = $table->orderBy('t1.id');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeLeaves($id=0,$year=0,$month=0)
	{
		if($month!=0)
		{
			if($month<7)
			{
				$to = dateToMiladi($year,$month,31);
			}
			elseif($month<12)
			{
				$to = dateToMiladi($year,$month,30);
			}
			else
			{
				if(isItLeapYear($year))
				{
					$to = dateToMiladi($year,$month,30);
				}
				else
				{
					$to = dateToMiladi($year,$month,29);
				}
			}
			$from = dateToMiladi($year,$month,1);
		}
		else
		{
			$from = dateToMiladi($year,1,1);
			if(isItLeapYear($year))
			{
				$to = dateToMiladi($year,12,30);
			}
			else
			{
				$to = dateToMiladi($year,12,29);
			}
        }
		$table = DB::connection(self::$myDb)
					->table('leaves')
					->where('employee_id',$id)
					->whereRaw('((date_from >= "'.$from.'" AND date_from <= "'.$to.'") OR (date_to >= "'.$from.'" AND date_to <= "'.$to.'"))');
		$object = $table->orderBy('created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeLeaves_groupby($id=0,$year=1395)
	{
		$from = dateToMiladi($year,1,1);
		if(isItLeapYear($year))
		{
			$to = dateToMiladi($year,12,30);
		}
		else
		{
			$to = dateToMiladi($year,12,29);
		}
		$table = DB::connection(self::$myDb)
					->table('leaves')
					->select('type',
										DB::raw('SUM(days_no) AS total')
										)
					->where('employee_id',$id)
					->whereRaw('((date_from >= "'.$from.'" AND date_from <= "'.$to.'") OR (date_to >= "'.$from.'" AND date_to <= "'.$to.'"))');
		$object = $table->groupBy('type');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getDir_pendingLeaves($year=0)
	{
		$table = DB::connection(self::$myDb)
		//->select('l.id','e.name_dr','e.last_name')
		->table('leaves as l')
		->select(
				'l.*',
                'e.name_dr',
                'e.last_name',
                'd.name as dep_name'
				)
		->leftjoin('employees AS e','e.id','=','l.employee_id')
		->leftjoin('auth.department AS d','d.id','=','e.department')
		->where('e.department',Auth::user()->department_id)
		->where('l.dir_approved',0);
		$sdate = dateToMiladi($year,01,01);
		if(isItLeapYear($year))
		{
			$edate = dateToMiladi($year,12,30);
		}
		else
		{
			$edate = dateToMiladi($year,12,29);
		}
		$table->where('l.date_from','>=',$sdate);
		$table->where('l.date_to','<=',$edate);
		$object = $table->orderBy('l.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getOCS_pendingLeaves($year=0)
	{
		$table = DB::connection(self::$myDb)
		//->select('l.id','e.name_dr','e.last_name')
		->table('leaves as l')
		->select(
				'l.*',
				'e.name_dr',
				'e.last_name',
				'd.name as dep_name'
				)
		->leftjoin('employees AS e','e.id','=','l.employee_id')
		->leftjoin('auth.department AS d','d.id','=','e.department')
		->where('e.dep_type',1)
		->where('l.dir_approved',1)
		->where('l.hr_approved',0);

		$sdate = dateToMiladi($year,01,01);
		if(isItLeapYear($year))
		{
			$edate = dateToMiladi($year,12,30);
		}
		else
		{
			$edate = dateToMiladi($year,12,29);
		}
		$table->where('l.date_from','>=',$sdate);
		$table->where('l.date_to','<=',$edate);
		$object = $table->orderBy('l.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getAOP_pendingLeaves($year=0)
	{
		$table = DB::connection(self::$myDb)
		//->select('l.id','e.name_dr','e.last_name')
		->table('leaves as l')
		->select(
				'l.*',
				'e.name_dr',
				'e.last_name',
				'd.name as dep_name'
				)
		->leftjoin('employees AS e','e.id','=','l.employee_id')
		->leftjoin('auth.department AS d','d.id','=','e.department')
		->where('e.dep_type',2)
		->where('l.dir_approved',1)
		->where('l.hr_approved',0);

		$sdate = dateToMiladi($year,01,01);
		if(isItLeapYear($year))
		{
			$edate = dateToMiladi($year,12,30);
		}
		else
		{
			$edate = dateToMiladi($year,12,29);
		}
		$table->where('l.date_from','>=',$sdate);
		$table->where('l.date_to','<=',$edate);
		$object = $table->orderBy('l.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	public static function getEmployeeLeaves_total($type=0,$id=0,$date=0)
	{
		$table = DB::connection(self::$myDb)
					//->select('SUM(days_no) as days')
					->table('leaves')
					->where('employee_id',$id)
					->where('type',$type);
		if($date !=0)
		{
			$sdate = dateToMiladi($date,01,01);
			if(isItLeapYear($date))
			{
				$edate = dateToMiladi($date,12,30);
			}
			else
			{
				$edate = dateToMiladi($date,12,29);
			}
			$table->where('date_from','>=',$sdate);
			$table->where('date_to','<=',$edate);
		}
		$object = $table->groupBy('employee_id');
		//echo $object->tosql();
		return $object->sum('days_no');
	}
	public static function getDir_pending_leaves_total($year=0)
	{
		$table = DB::connection(self::$myDb)
		//->select('SUM(days_no) as days')
		->table('leaves as l')
        ->leftjoin('employees AS e','e.id','=','l.employee_id')
		->where('e.department',Auth::user()->department_id)
		->where('l.dir_approved',0);
		$sdate = dateToMiladi($year,01,01);
		if(isItLeapYear($year))
		{
			$edate = dateToMiladi($year,12,30);
		}
		else
		{
			$edate = dateToMiladi($year,12,29);
        }
		$table->where('l.date_from','>=',$sdate);
		$table->where('l.date_to','<=',$edate);
		$object = $table->groupBy('e.department');
		// ddd($object->tosql());
		return $object->count('l.id');
	}
	public static function getHR_pending_leaves_total($year=0)
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
		//->select('SUM(days_no) as days')
		->table('leaves as l')
		->leftjoin('employees AS e','e.id','=','l.employee_id')
		->where('e.dep_type',$user_dep_type)
		//->where('l.dir_approved',1)
		->where('l.hr_approved',0);

		$sdate = dateToMiladi($year,01,01);
		if(isItLeapYear($year))
		{
			$edate = dateToMiladi($year,12,30);
		}
		else
		{
			$edate = dateToMiladi($year,12,29);
		}
		$table->where('l.date_from','>=',$sdate);
		$table->where('l.date_to','<=',$edate);
		//$object = $table->groupBy('l.id');
		//echo $object->tosql();
		return $table->count('l.id');
	}
	public static function getLeaveDetails($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('leaves')
					->where('id',$id);
		//echo $object->tosql();
		return $table->first();
	}
	//custom query
	public static function test($year,$month)
	{
		if($month == 1)
		{
			$from = dateToMiladi($year-1,12,15);
		}
		else
		{
			$from = dateToMiladi($year,$month-1,15);//the start day of attendance month is 15 of prevouce month
		}
		//$from = dateToMiladi($year,$month-1,15);//the start day of attendance month is 15 of prevouce month
		$to = dateToMiladi($year,$month,15);//the end day of attendance month is 15 of current month\

		$begin = new DateTime($from);
		$end = new DateTime($to);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);
		$days = '';
		foreach($period As $day)
		{
			$the_day = $day->format( "Y-m-d" );
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_year = $period_det[0];

			$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
		}

		//remove the last pipe line
		$days = substr($days, 0, -1);
		$imgs= DB::connection('hr')
				->table('attendance_images as t1')
				->select('t1.id','t1.path','t1.status','t1.RFID')
				//->where('RFID',$rfid)//check the rfid
				->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path

				->get();
		$images = array();
		foreach($imgs AS $img)
// /2785872450008/2015/1212/124443_8426042.jpg
		{
			if (array_key_exists($img->RFID.'_'.substr($img->path,20,4).'_1',$images)){
				$images[$img->RFID.'_'.substr($img->path,20,4).'_2']	=$img;
			}else {
				$images[$img->RFID.'_'.substr($img->path,20,4).'_1']	=$img;
			}
			//$images[]	= array('uid'=>$img->RFID.'_'.substr($img->path,20,4),'path'=>$img->path,'RFID'=>$img->RFID);
		}

		return $images;
	}


	// public static function test($year,$month)
	// {
	// 	$from = dateToMiladi($year,$month-1,15);//the start day of attendance month is 15 of prevouce month
	// 	$to = dateToMiladi($year,$month,15);//the end day of attendance month is 15 of current month\

	// 	$begin = new DateTime($from);
	// 	$end = new DateTime($to);
	// 	$interval = DateInterval::createFromDateString('1 day');
	// 	$period = new DatePeriod($begin, $interval, $end);
	// 	$days = '';
	// 	foreach($period As $day)
	// 	{
	// 		$the_day = $day->format( "Y-m-d" );
	// 		$period_det = explode('-',$the_day);
	// 		$period_day = $period_det[2];
	// 		$period_month = $period_det[1];
	// 		$period_year = $period_det[0];

	// 		$days .= '/'.$period_year.'/'.$period_month.$period_day.'|';
	// 	}

	// 	//remove the last pipe line
	// 	$days = substr($days, 0, -1);
	// 	return  DB::connection('hr')
	// 			->table('attendance_images as t1')
	// 			->select('t1.id','t1.path','t1.status','t1.RFID')
	// 			//->where('RFID',$rfid)//check the rfid
	// 			->whereRaw('path REGEXP "'.$days.'"')//check the year/monthDay in image path

	// 			->get();

	// }
	public static function getAllRFIDs($dep,$sub_dep)
	{
		$result= DB::connection('hr')
				->table('employees as t1')
				->select('t1.id','t1.name_dr AS name_dr','father_name_dr','t1.RFID','dep.name','t1.current_position_dr')
				->leftjoin('auth.department AS dep','dep.id','=','t1.department')
				->where('t1.RFID','!=','')
				->where('t1.fired',0)
				->where('t1.resigned',0)
				->where('t1.vacant','!=',1)//kambod
				->where('t1.vacant','!=',4)//ezafa bast
				->where('t1.tashkil_id','!=',0);//new tashkilat
				if($dep!=0)
				{
					if($sub_dep!=0)
					{
						$result->where('t1.department',$sub_dep);
					}
					else
					{
						$result->where('t1.general_department',$dep);
					}
				}
		return $result->get();
	}
	public static function getEmployeePromotionDetail($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_promotions AS t1')
				->select('t2.employee_type as current_type','t1.*')
				->leftjoin('employees AS t2','t2.id','=','t1.employee_id')

				->where('t1.id',$id)
				//->orderBy('id','desc')
				->first();
	}
	public static function getEmployeeMakafatDetail($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_makafats AS t1')
				->select('t1.*')
				//->leftjoin('employees AS t2','t2.id','=','t1.employee_id')

				->where('t1.id',$id)
				//->orderBy('id','desc')
				->first();
	}
	public static function getEmployeePunishDetail($id=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_punishments AS t1')
				->select('t1.*')
				//->leftjoin('employees AS t2','t2.id','=','t1.employee_id')

				->where('t1.id',$id)
				//->orderBy('id','desc')
				->first();
	}
	public static function getEmployeePresented($id=0)
	{
		return DB::connection(self::$myDb)
				->table('attendance_presents AS t1')
				->select('t1.*')
				->where('t1.RFID',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function getEmployeePresented_lost($id=0)
	{
		return DB::connection(self::$myDb)
				->table('attendance_presents AS t1')
				->select('t1.*')
				->whereIn('t1.RFID',$id)
				//->orderBy('id','desc')
				->get();
	}
	public static function insertAttImages($data=array())
	{
		// Start transaction
		DB::beginTransaction();

		$acct = DB::connection(self::$myDb)->table('attendance_images')->insert($data);

		if(!$acct)
		{
		    DB::rollback();
		    return false;
		} else {
		    // Else commit the queries
		    DB::commit();
		    return true;
		}
	}
	public static function insertAttImages_temp($data=array())
	{
		// Start transaction
		DB::beginTransaction();

		$acct = DB::connection(self::$myDb)->table('attendance_images_temp')->insert($data);

		if( !$acct )
		{
		    DB::rollback();
		    return false;
		} else {
		    // Else commit the queries
		    DB::commit();
		    return true;
		}
	}
	public static function getEmployeeSalaryWait($id=0,$year=0,$month=0)
	{
		return DB::connection(self::$myDb)
				->table('employee_salary_waits AS t1')
				->select('t1.*')
				->where('t1.employee_id',$id)
				->where('t1.year',$year)
				->where('t1.month',$month)
				//->orderBy('id','desc')
				->first();
	}
	public static function check_tashkil($id=0,$sub=0,$tainat=0)
	{
		return DB::connection(self::$myDb)
				->table('tashkilat AS t1')
				->where('t1.dep_id',$id)
				->where('t1.sub_dep_id',$sub)
				->where('t1.tainat',$tainat)
				//->orderBy('id','desc')
				->first();
	}
	public static function check_employee_add($name='',$fname='',$gname='',$dep_id=0,$sub=0)
	{
		return DB::connection(self::$myDb)
				->table('employees AS t1')
				->where('t1.general_department',$dep_id)
				->where('t1.department',$sub)
				->where('t1.name_dr',$name)
				->where('t1.father_name_dr',$fname)
				->where('t1.grand_father_name',$gname)
				//->orderBy('id','desc')
				->first();
	}
	public static function checkTodayAtt($date='')
	{
		$result = DB::connection(self::$myDb)
				->table('attendance_images_today AS t1')
				->where('t1.path','like',"%$date%")
				//->where('t1.path',$date)
				->first();
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public static function getEmployeesTodayAtt($id=0,$sub=0,$status=0,$type=0,$from_date=0,$to_date=0)
	{
        if($status=='show_img')
        {
            $object = DB::connection(self::$myDb)
                     ->table('attendance_images_today as att')
                     ->select('*','st.name_dr as device_name',DB::raw('IF(att.RFID = "0","غیرحاضر","حاضر") AS status'))
                     ->leftjoin('static_data as st','st.code','=','att.device_id')
                     ->where('att.RFID',0)
                     ->whereBetween('att.date',[$from_date,$to_date])
                     ->orderBy('att.time','desc')
                     ->get(); 
            DB::disconnect(self::$myDb);
            return $object;

        }
        else
        {
            $table = DB::connection(self::$myDb)
                        ->table('employees AS t1');
                        $table->select(
                                't1.id',
                                't1.RFID',
                                't1.name_dr AS name',
                                't1.father_name_dr AS father_name',
                                'dep.name AS department',
                                't1.number_tayenat',
                                't1.current_position_dr',
                                't1.emp_bast as emp_rank',
                                'att.time',
                                'att.date',
                                'st.name_dr as device_name',
                                'att.device_id',
                                DB::raw('IF(att.RFID != "0","حاضر","غیرحاضر") AS status')
                                //DB::raw('count(att.RFID) as user_count')
                                );
                        if($status==2)
                        {//absents
                            $table->where('att.RFID',null);
                        }
                        if($type==2)
                        {//ajir
                            $table->where('t1.employee_type',2);
                        }
                        elseif($type==1)
                        {
                            $table->where('t1.employee_type','!=',2);
                        }
                        $table->where('t1.RFID','>',0);
                        $table->where('t1.mawqif_employee',1);
                        $table->where('t1.tashkil_id','!=',0);//ezafa bast
                        //$table->where('t1.changed',0);//not changed to out
                        if($id!=0)
                        {
                            if($sub!=0)
                            {
                                $table->where('t1.department',$sub);
                            }
                            else
                            {
                                $table->where('t1.general_department',$id);
                            }
                        }
                        if($status==1)
                        {
                            $table->whereBetween('att.date',[$from_date,$to_date]);
                        }

            $table->leftjoin('auth.department AS dep','dep.id','=','t1.department');
            $table->leftjoin('attendance_images_today AS att','att.RFID','=','t1.RFID')
            ->leftjoin('static_data as st','st.code','=','att.device_id');

            $object = $table->orderBy('t1.created_at')->get();

            DB::disconnect(self::$myDb);
            return $object;
        }
	}
	//estehqaq mash
	public static function getEmployeePayroll_details($emp_id=0,$year,$month)
	{
		return DB::connection(self::$myDb)
				->table('employee_payrolls AS t1')
				->where('t1.employee_id',$emp_id)
				->where('t1.year',$year)
				->where('t1.month',$month)
				//->orderBy('id','desc')
				->first();
	}
	public static function insertLog($table='',$action=0,$desc='',$id=0)
	{//action: 1-update,2-delete
		DB::connection(self::$myDb)->table('hr_logs')->insert(array('user_id'=>Auth::user()->id,'action'=>$action,'table'=>$table,'desc'=>$desc,'table_id'=>$id,'created_at'=>date('Y-m-d H:i:s')));
	}
	public static function getTashkilSearch($dep_id,$sub_dep,$type,$bast,$title,$emp_type,$year)
	{
		$lang = 'dr';
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('tashkilat AS t1');
					$table->select(
							't1.id',
							DB::raw('(CASE WHEN t1.employee_type = 3 THEN mr.name_'.$lang.' ELSE er.name_'.$lang.' END) AS bast'),
							'dep.name AS department',
							'subdep.name AS sub_dep',
							't1.title',
							't1.tainat',
							't1.year','t1.status'
							);
		if($type!=2)
		{
			$table->where('status',$type);
		}
		if($sub_dep!=0)
		{
			$table->where('t1.sub_dep_id',$sub_dep);
		}
		else
		{
			if($dep_id != 0)
			{
				$table->where('t1.dep_id',$dep_id);
			}
		}
		if($bast!=0)
		{
			$table->where('t1.bast',$bast);
		}

		if($title!='0')
		{
			//$title = str_replace('-', ' ', $title);
			$table->where('t1.title','like',"%$title%");
		}

		if($emp_type!=0)
		{
			$table->where('t1.employee_type',$emp_type);
		}
		if($year!=0)
		{
			$table->where('t1.year',$year);
		}
		if($user_dep_type!=0)
		{
			$table->where('t1.dep_type',$user_dep_type);
		}

		$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.bast');
		$table->leftjoin('auth.military_rank AS mr','mr.id','=','t1.bast');
		$table->leftjoin('auth.department AS dep','dep.id','=','t1.dep_id');
		$table->leftjoin('auth.department AS subdep','subdep.id','=','t1.sub_dep_id');
		$object = $table->orderBy('t1.created_at');
		// echo "<pre/>";  print_r($object->tosql());exit;
		return $object->get();
	}
	public static function getEmployeeRank($type=0,$emp_id=0)
	{
		$lang = 'dr';
		$table = DB::connection(self::$myDb)
			->table('employees AS t1');
			if($type!=3)
			{
				$table->select(
				'er.name_'.$lang.' AS rank'
				);
			}
			else
			{
				$table->select(
				'mi.name_'.$lang.' as rank'
				);
			}

			$table->leftjoin('auth.employee_rank AS er','er.id','=','t1.emp_rank')
			->leftjoin('auth.military_rank AS mi','mi.id','=','t1.emp_rank')
			->where('t1.id',$emp_id);

			//->orderBy('id','desc')
			return $table->first();
	}
	public static function getEmployeeBast($id=0)
	{
		if($id != 0 )
		{
			$lang = 'dr';
			$table = DB::connection(self::$myDb)
			->table('tashkilat AS t1')
			->select('t1.bast')
			->where('t1.id',$id);
			return $table->first()->bast;
		}
		else
		{
			return null;
		}
	}
	public static function getEmployeeTashkil($t_id=0)
	{
		$lang = 'dr';
		$table = DB::connection(self::$myDb)
			->table('tashkilat AS t1')
			->select(DB::raw('(CASE WHEN t1.employee_type = 3 THEN mr.name_'.$lang.' ELSE er.name_'.$lang.' END) AS bast'),
					'title')
			->leftjoin('auth.employee_rank AS er','er.id','=','t1.bast')
			->leftjoin('auth.military_rank AS mr','mr.id','=','t1.bast')
			->where('t1.id',$t_id);

			//->orderBy('id','desc')
			return $table->first();
	}
	public static function getDepName($dep_id=0)
	{
		return DB::connection(self::$myDb)
				->table('auth.department')
				->select('name')
				->where('id',$dep_id)
				->first();
	}
	public static function getDepEmployeeTotal($dep_id=0)
	{
		return DB::connection(self::$myDb)
				->table('tashkilat')
				->select(DB::raw('count(id) As total'))
				->where('sub_dep_id',$dep_id)
				->where('status',1)
				->where('employee_type','!=',2)
				->groupBy('sub_dep_id')
				->first()->total;
	}

	public static function getMonthHolidays($year,$month)
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		return DB::connection(self::$myDb)
				->table('month_holidays AS t1')
				->select('t1.days','t2.date','t2.desc')
				->leftjoin('month_holiday_dates AS t2','t2.month_holidays_id','=','t1.id')
				->where('t1.year',$year)
				->where('t1.month',$month)
				->where('t1.dep_type',$user_dep_type)
				->get();
	}
	public static function getMonthHolidayDays($year,$month)
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		return DB::connection(self::$myDb)
				->table('month_holidays AS t1')
				->select('t1.days')
				//->leftjoin('month_holiday_dates AS t2','t2.month_holidays_id','=','t1.id')
				->where('t1.year',$year)
				->where('t1.month',$month)
				->where('t1.dep_type',$user_dep_type)
				->first();
	}
	public static function getMonthEmergencyHolidays($year,$month)
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		return DB::connection(self::$myDb)
				->table('month_holiday_dates AS t2')
				->select('t1.days','t2.date','t2.desc')
				->leftjoin('emergency_days AS t1','t2.emergency_id','=','t1.id')
				->where('t1.year',$year)
				->where('t1.month',$month)
				->where('t1.dep_type',$user_dep_type)
				->get();
	}
	public static function getMonthEmergencyDays($year,$month)
	{
		return DB::connection(self::$myDb)
				->table('emergency_days AS t1')
				->select('t1.days')
				//->leftjoin('month_holiday_dates AS t2','t2.month_holidays_id','=','t1.id')
				->where('t1.year',$year)
				->where('t1.month',$month)
				->first();
	}
	public static function checkInsertImagesDate($date)
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$date = explode('-',$date);
		$the_date = $date[0].'/'.$date[1].$date[2];
		$result= DB::connection(self::$myDb)
				->table('attendance_images AS t1')
				->select('t1.id')
				->where('t1.dep_type',$user_dep_type)
				->where('t1.path', 'like', '%'.$the_date.'%')
                ->get();
		if($result)
		{
            if(Auth::user()->id=='113')
            {
                return false;
            }
            return false;
		}
		else
		{
			return true;
		}
    }
    /** 
     * @Author: Jamal Yousufi 
     * @Date: 2021-02-09 14:15:52 
     * @Desc: Check temporary image   
     */
	public static function checkTemporaryInsertImage($date)
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$date = explode('-',$date);
		$the_date = $date[0].'/'.$date[1].$date[2];
		$result= DB::connection(self::$myDb)
				->table('attendance_images AS t1')
				->select('t1.id')
				->where('t1.dep_type',$user_dep_type)
				->where('t1.date', $date)
                ->get();

		if($result)
		{
            return false;
		}
		else
		{
			return true;
		}
	}
	public static function getDegreesChart()
	{
		$result= DB::connection(self::$myDb)
				->table('employees as t1')
				->select(DB::raw('count(t1.id) As total'),'e.education_id as education_degree')
				->leftjoin('employee_educations AS e','e.employee_id','=','t1.id')
				->where('t1.tashkil_id','!=',0)
				->groupBy('e.education_id')
				->get();
		return $result;
	}
	public static function getDegrees()
	{
		$result= DB::connection(self::$myDb)
				->table('auth.education_degree')
				->select('id','name_dr')
				->get();
		return $result;
	}
	public static function getGenders()
	{
		$result= DB::connection(self::$myDb)
				->table('employees')
				->select(DB::raw('count(id) As total'),'gender')
				->where('tashkil_id','!=',0)
				->groupBy('gender')
				->get();
		return $result;
	}
	public static function getEmployeeTypes()
	{
		$result= DB::connection(self::$myDb)
				->table('employees')
				->select(DB::raw('count(id) As total'),'employee_type')
				->where('tashkil_id','!=',0)
				->groupBy('employee_type')
				->get();
		return $result;
	}
	public static function getEmployeeDep($dep_id=0)
	{
		$result= DB::connection(self::$myDb)
				->table('employees')
				->select(DB::raw('count(id) As total'))

				->where('employee_type','!=',2)//dont include ajirs
				->where('mawqif_employee',1)//retire
				->where('RFID','!=',0)
				->where('department',$dep_id)
				->groupBy('department')
				->first();
		return $result;
	}
	public static function getEmployeeGen_Dep($dep_id=0)
	{
		$result= DB::connection(self::$myDb)
				->table('employees')
				->select(DB::raw('count(id) As total'))
				->where('fired',0)
				->where('resigned',0)
				->where('vacant','!=',1)//kambod
				->where('vacant','!=',4)//ezafa bast
				->where('position_dr','!=',4)//entezar mahsh
				->where('changed',0)//not changed to out
				->where('tashkil_id','!=',0)//tanqis tashkilati
				->where('employee_type','!=',2)//dont include ajirs
				->where('retired',0)//retire
				->where('RFID','!=',0)
				->where('general_department',$dep_id)
				->groupBy('general_department')
				->first();
		return $result;
	}
	public static function get_dep_name($dep_id=0)
	{
		$result= DB::connection(self::$myDb)
				->table('auth.department')
				->select('name')
				->where('id',$dep_id)
				->first();
		return $result;
	}
	public static function getAllHolidays($from='',$to='')
	{
		$result= DB::connection(self::$myDb)
				->table('month_holiday_dates')
				->select('date')
				->whereBetween('date', array($from, $to))
				->get();
		return json_decode(json_encode((array) $result), true);
	}
	public static function get_todays_absent()
	{
		$result= DB::connection(self::$myDb)
				->table('employees AS e')
				->select('e.id','e.email','e.name_dr as name','e.last_name','e.department')
				//->where('e.department',78)
				->where('e.RFID','>',0)
				->where('e.tashkil_id','!=',0)
				->where('e.employee_type','!=',2)
				->whereNotIn('e.RFID', function($query){
				    $query->select('t1.RFID')
				    	  ->from('attendance_images_today as t1');
				})
				//->where('e.department',78)
				->get();
		return $result;
	}
	public static function get_todays_absent_old($time='')
	{
		$result= DB::connection(self::$myDb)
				->table('attendance_images_today as t1')
				->select('e.email','e.name_dr as name','e.last_name','t1.time','e.department')
				->leftjoin('employees AS e','e.RFID','=','t1.RFID')
				->where('t1.time','>',$time)
				//->where('e.department',78)
				->get();
		return $result;
	}
	public static function get_yesterdays_absent($the_date='',$user_dep_type=2)
	{
		//$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$data['the_date'] = $the_date;
        $data['dep_type'] = $user_dep_type;
              
		$result= DB::connection(self::$myDb)
				->table('employees AS e')
				->select('e.id','e.email','e.name_dr as name','e.last_name','e.department','e.RFID')
				//->select('e.email','e.name_dr as name','e.last_name','e.department')
				//->where('e.department',78)
				->where('e.RFID','>',0)
				->where('e.tashkil_id','>',0)
				->where('e.employee_type','!=',2)
				->where('e.dep_type',$user_dep_type)
				->whereNotIn('e.RFID', function($query) use ($data){
				    		$query->select('t1.RFID')
				    	  ->from('attendance_images as t1')
				    	  ->where('date',$data['the_date'])
				    	  ->where('dep_type',$data['dep_type'])
				    	  ->where('time','>',120000);
				})
				//->where('e.department',78)
				->get();
		return $result;
	}
	public static function getEmployees_inShift($dep=0,$sub=0,$status=0,$gender=0)
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table= DB::connection(self::$myDb)
				->table('employees as e')
				->select(DB::raw('IF(e.last_name != "",CONCAT(e.name_dr," ",e.last_name),e.name_dr) AS fullname'),
						'e.father_name_dr','e.department',
						'e.current_position_dr as position','d.name as dep_name',
						's.time_in','s.time_out','e.id')
				->leftjoin('att_shifts AS s','s.employee_id','=','e.id')
				->leftjoin('auth.department AS d','d.id','=','e.department');
				//$table->where('e.in_shift','!=',0);
				$table->where('e.dep_type',$user_dep_type);
				if($dep!=0)
				{
					if($sub!=0)
					{
						$table->where('e.department',$sub);
					}
					else
					{
						$table->where('e.general_department',$dep);
					}
				}
				if($status!=0)
				{
					if($status==3)
					{//thus
						$table->where('s.status',0);
					}
					elseif($status==1)
					{//morning
						$table->where('s.time_in','>',0);
					}
					elseif($status==2)
					{//afternoon
						$table->where('s.time_out','>',0);
					}
				}
				else
				{
					$table->whereIn('s.status', [0,1,2]);
				}
				if($gender==1)
				{
					$table->where('e.gender','M');
				}
				elseif($gender==2)
				{
					$table->where('e.gender','F');
				}
				$table->orderBy('s.id','desc')
				->groupBy('s.employee_id');
		return $table->get();
	}
	public static function getEmployees_inAtt($dep=0,$sub=0,$gender=0)
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table= DB::connection(self::$myDb)
				->table('employees as e')
				->select(DB::raw('IF(e.last_name != "",CONCAT(e.name_dr," ",e.last_name),e.name_dr) AS fullname'),
						'e.father_name_dr','e.department',
						'e.current_position_dr as position','d.name as dep_name',
						'e.id')
				->leftjoin('auth.department AS d','d.id','=','e.department');
				$table->where('e.dep_type',$user_dep_type);
				if($dep!=0)
				{
					if($sub!=0)
					{
						$table->where('e.department',$sub);
					}
					else
					{
						$table->where('e.general_department',$dep);
					}
				}

				if($gender==1)
				{
					$table->where('e.gender','M');
				}
				elseif($gender==2)
				{
					$table->where('e.gender','F');
				}
				$table->where('e.RFID','>',0);
				$table->where('e.tashkil_id','>',0);
				$table->where('e.employee_type','!=',2);

		return $table->get();
	}
	public static function getEmployees_positions()
	{
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$result= DB::connection(self::$myDb)
				->table('employees as e')
				->select(DB::raw('IF(e.last_name != "",CONCAT(e.name_dr," ",e.last_name),e.name_dr) AS fullname'),
						'e.father_name_dr','e.department',
						'e.current_position_dr as position_dr','d.name as dep_name',
						'e.current_position_en as position_en',
						'e.id')
				//->leftjoin('att_shifts AS s','s.employee_id','=','e.id')
				->leftjoin('auth.department AS d','d.id','=','e.department')
				->where('e.dep_type',$user_dep_type)
				->where('e.tashkil_id','>',0)
				//->WhereIn('e.employee_type',[4,5])
				//->orWhere('e.employee_type',5)
				//->orderBy('s.id','desc')
				//->groupBy('s.employee_id')
				->get();
		return $result;
	}
	//get employee details by id
	public static function getEmployeeById($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees');
					$table->select(
							'id','current_position_dr','current_position_en','RFID'
							);
		$table->where('id',$id);

		//echo $object->tosql();
		return $table->first();
	}
	public static function getEmployeeByUserId($userid=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees as e');
					$table->select(
							'e.id','e.RFID','e.department','e.general_department','e.email','e.current_position_dr','e.photo'
							);
		$table->leftjoin('auth.users AS u','u.employee_id','=','e.id');
		$table->where('u.id',$userid);

		$object = $table->first();
		DB::disconnect(self::$myDb);
		return $object;
	}
	public static function getEmployeefiling($userid=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employee_filing');
		$table->where('employee_id',$userid);

		//echo $object->tosql();
		return $table->first();
	}
	public static function getEmployeeDet($empid=0)
	{
		$table = DB::connection(self::$myDb)
					->table('employees');
		$table->where('id',$empid);

		//echo $object->tosql();
		return $table->first();
	}
	public static function isTashkilInUse($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('tashkilat');
		$table->where('id',$id);
		$table->where('status',1);

		//echo $object->tosql();
		return $table->first();
	}
	public static function getEmpDetByLeaveID($id=0)
	{
		$table = DB::connection(self::$myDb)
					->table('leaves');
		$table->where('id',$id);
		//echo $object->tosql();
		return $table->first();
	}
	public static function getAllHRNotifications($published=false)
	{//notification board
		$user_dep_type = getUserDepType(Auth::user()->id);
		if($user_dep_type)
		{
			$user_dep_type = $user_dep_type->dep_type;
		}
		else
		{
			return showWarning();
		}
		$table = DB::connection(self::$myDb)
					->table('hr_notification_board');
		$table->where('dep_type',$user_dep_type);
		if($published)
		{
			$table->where('status',0);
		}
		$table->orderBy('created_at','desc');
		//echo $object->tosql();
		return $table->get();
	}
	public static function getNotificationBoard($id=0)
	{//notification board
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('hr_notification_board');
		$table->where('dep_type',$user_dep_type);
		$table->where('id',$id);
		//echo $object->tosql();
		return $table->first();
	}
	public static function check_training_validity($id=0)
	{//notification board
		//$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$table = DB::connection(self::$myDb)
					->table('capacity_trainings');
		//$table->where('dep_type',$user_dep_type);
		$table->where('id',$id);
		//echo $object->tosql();
		$seat_no= $table->pluck('seat_no');

		$table = DB::connection(self::$myDb)
					->table('capacity_employee_trainings');
		//$table->where('dep_type',$user_dep_type);
		$table->where('training_id',$id);
		//echo $object->tosql();
		$emp_no= $table->count('id');

		if($seat_no>$emp_no)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static function getEmployeeAbsentDays($id=0,$year=0,$month=0,$emp_id=0,$new_array=array(),$date_type=0)
	{
		if($date_type==0)
		{
			$sday = att_month_days(0);
			$eday = att_month_days(1);
			if($month==1)
			{
				$from = dateToMiladi($year-1,12,$sday);
			}
			else
			{
				$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
			}
			$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
		}
		else
		{
			$sh_from = explode('-',$year);
			$sh_to = explode('-',$month);
			$from = dateToMiladi($sh_from[2],$sh_from[1],$sh_from[0]);
			$to = dateToMiladi($sh_to[2],$sh_to[1],$sh_to[0]);
		}

		if($to>date('Y-m-d'))
		{
			$to = date('Y-m-d');
		}
		$begin = new DateTime($from);
		$end = new DateTime($to);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);
		//create the all monthDay values for where: /2015/1216|/2015/1217
		$days = array();$apsentdays=array();

		$thuFri = is_in_ThuFri_shift($id);
		foreach($period As $day)
		{
			$the_day = $day->format( "Y-m-d" );
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_year = $period_det[0];
			$day_code = date("w", strtotime($the_day));
			//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
			//$urgents = checkUrgentHoliday($the_day);
			$holiday = in_array($the_day, $new_array);
            
			$isInLeave = isEmployeeInLeave($emp_id,$the_day,$year,$month,$date_type);

			if($isInLeave)
			{//if employee is in leave, no need to check the available pic
				continue;
			}
			elseif($holiday || ($day_code == 5 && !$thuFri))
			{//holidays and fridays
				if(!checkHolidayAbsent($the_day,$id))
				{//if it is holiday of friday, check the day before and after of holiday
					continue;
				}
			}
			else
			{
				if($day_code == 4)
				{//thursdays
					if(is_in_thu_shift($id,$year,$month,0,$date_type))
					{
						continue;
					}
					else
					{
						if($thuFri)
						{
							if(checkIfEmployeeIsPresent($id,$the_day))
							{//if employee is present at thu, no need to check the available pic
								continue;
							}
							else
							{//if employee is absent at thu, then check fri
								$thedate = explode('-', $the_day);
								$from_unix_time = mktime(0, 0, 0,$thedate[1],$thedate[2],$thedate[0]);
								$day_after = strtotime("tomorrow", $from_unix_time);
								$tomorrow = date('Y-m-d', $day_after);

								if(checkIfEmployeeIsPresent($id,$tomorrow))
								{
									continue;
								}
								else
								{//employee is absent in thu and fri, so no need to check the available pic
									//$days[] = $period_year.'-'.$period_month.'-'.$period_day;
									array_push($apsentdays, $period_year.$period_month.$period_day);
								}
							}
						}
						else
						{
							$days[] = $period_year.'-'.$period_month.'-'.$period_day;
							array_push($apsentdays, $period_year.$period_month.$period_day);
						}
					}

				}
				elseif($day_code == 5)
				{//fridays for thuFri shif, as thu is check with fri, do nothing for fri
					continue;
				}
				else
				{
					$days[] = $period_year.'-'.$period_month.'-'.$period_day;
					array_push($apsentdays, $period_year.$period_month.$period_day);
				}

			}
		}
		//dd($days);
		//remove the last pipe line
		//$days = substr($days, 0, -1);
		//remove the last pipe line
		//$thu_days = substr($thu_days, 0, -1);
		$total = '';
		$rfid_lost = lost_rfid($emp_id);
		if($rfid_lost)
		{
			$rfids = array($id);
			foreach($rfid_lost as $new_rfid)
			{
				$rfids[] =$new_rfid->rfid;
			}
			if($days!=null)
			{
				$total =DB::connection('hr')
					->table('attendance_images')
					->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total,SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",3),"/",-1) AS year'))//return the monthDay part of path
					->whereIn('RFID',$rfids)//check the rfid
					->where('status',0)//present
					->whereIn('date',$days)
					->groupBy('groupByMe')
					->having('total', '>', 1)
					->get();
					//->toSql();dd($total->total);
			}
		}
		else
		{
			if($days!=null)
			{
				$total =DB::connection('hr')
					->table('attendance_images')
					->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total,SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",3),"/",-1) AS year'))//return the monthDay part of path
					->where('RFID',$id)//check the rfid
					->where('status',0)//present
					->whereIn('date',$days)
					->groupBy('groupByMe')
					->having('total', '>', 1)
					->get();
					//->toSql();dd($total);
			}
		}
		$presented_days=array();
		if($total)
		{
			foreach($total as $pday)
			{
				array_push($presented_days, $pday->year.$pday->groupByMe);
			}
		}
		$absent_days = array_diff($apsentdays,$presented_days);

		return $absent_days;
	}

	public static function getAjirAbsentDays($id=0,$year=0,$month=0,$date_type=0)
	{
		if($date_type==0)
		{
			$sday = att_month_days(0);
			$eday = att_month_days(1);
			if($month==1)
			{
				$from = dateToMiladi($year-1,12,$sday);
			}
			else
			{
				$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
			}
			$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
		}
		else
		{
			$sh_from = explode('-',$year);
			$sh_to = explode('-',$month);
			$from = dateToMiladi($sh_from[2],$sh_from[1],$sh_from[0]);
			$to = dateToMiladi($sh_to[2],$sh_to[1],$sh_to[0]);
		}

		if($to>date('Y-m-d'))
		{
			$to = date('Y-m-d');
		}
		$begin = new DateTime($from);
		$end = new DateTime($to);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);
		//create the all monthDay values for where: /2015/1216|/2015/1217
		$days = array();$apsentdays=array();
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$allHolidays =  DB::connection('hr')
					->table('month_holiday_dates')
					->select('date')
					->whereBetween('date', array($from, $to))
					->where('dep_type',$user_dep_type)
					->get();
		$allHolidays = json_decode(json_encode((array) $allHolidays), true);
		$new_array=array();
		foreach($allHolidays as $array)
		{
		    foreach($array as $val)
		    {
		        array_push($new_array, $val);
		    }
		}
		$emp_id = DB::connection('hr')
						->table('employees')->select(
								'id'
								)->where('RFID',$id)->first();
		foreach($period As $day)
		{
			$the_day = $day->format( "Y-m-d" );
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_year = $period_det[0];
			$day_code = date("w", strtotime($the_day));
			//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
			//$urgents = checkUrgentHoliday($the_day);
			$holiday = in_array($the_day, $new_array);
			$isInLeave = isEmployeeInLeave($emp_id->id,$the_day,$year,$month,$date_type);
			if($isInLeave)
			{//if employee is in leave, no need to check the available pic
				continue;
			}
			elseif($holiday)
			{//holidays and fridays
				if(!checkHolidayAbsent($the_day,$id))
				{//if it is holiday of friday, check the day before and after of holiday
					continue;
				}
			}
			else
			{
				if($day_code == 5)
				{//fridays for thuFri shif, as thu is check with fri, do nothing for fri
					continue;
				}
				else
				{
					$days[] = $period_year.'-'.$period_month.'-'.$period_day;
					array_push($apsentdays, $period_year.$period_month.$period_day);
				}

			}
		}
		//dd($days);
		//remove the last pipe line
		//$days = substr($days, 0, -1);
		//remove the last pipe line
		//$thu_days = substr($thu_days, 0, -1);
		$total = '';
		$rfid_lost = lost_rfid($emp_id->id);
		if($rfid_lost)
		{
			$rfids = array($id);
			foreach($rfid_lost as $new_rfid)
			{
				$rfids[] =$new_rfid->rfid;
			}
			if($days!=null)
			{
				$total =DB::connection('hr')
					->table('attendance_images')
					->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total,SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",3),"/",-1) AS year'))//return the monthDay part of path
					->whereIn('RFID',$rfids)//check the rfid
					->where('status',0)//present
					->whereIn('date',$days)
					->groupBy('groupByMe')
					->having('total', '>', 1)
					->get();
					//->toSql();dd($total->total);
			}
		}
		else
		{
			if($days!=null)
			{
				$total =DB::connection('hr')
					->table('attendance_images')
					->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total,SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",3),"/",-1) AS year'))//return the monthDay part of path
					->where('RFID',$id)//check the rfid
					->where('status',0)//present
					->whereIn('date',$days)
					->groupBy('groupByMe')
					->having('total', '>', 1)
					->get();
					//->toSql();dd($total);
			}
		}
		$presented_days=array();
		if($total)
		{
			foreach($total as $pday)
			{
				array_push($presented_days, $pday->year.$pday->groupByMe);
			}
		}
		$absent_days = array_diff($apsentdays,$presented_days);

		return $absent_days;
	}
	public static function getAjirAbsentDaysExtra($id=0,$year=0,$month=0,$date_type=0)
	{
    
      if($date_type==0)
      {  
        if($month<7)
        {
            $sday = 1;
            $eday = 31;
        }
        elseif($month<12)
        {
            $sday = 1;
            $eday = 30;
        }
        else
        {
            $sday = 1;
            $eday = 29;
        }
        $from = dateToMiladi($year,$month,$sday);//the end day of attendance month is 15 of current month\
        $to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month\
       }
       else
       {
           $from = $year; 
           $to   = $month; 
       }
    
        
		$begin = new DateTime($from);
		$end = new DateTime($to);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($begin, $interval, $end);
		//create the all monthDay values for where: /2015/1216|/2015/1217
		$days = array();$apsentdays=array();
		$user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
		$allHolidays =  DB::connection('hr')
					->table('month_holiday_dates')
					->select('date')
					->whereBetween('date', array($from, $to))
					->where('dep_type',$user_dep_type)
					->get();
		$allHolidays = json_decode(json_encode((array) $allHolidays), true);
		$new_array=array();
		foreach($allHolidays as $array)
		{
		    foreach($array as $val)
		    {
		        array_push($new_array, $val);
		    }
		}
		$emp_id = DB::connection('hr')
						->table('employees')->select(
								'id'
								)->where('RFID',$id)->first();
		foreach($period As $day)
		{
			$the_day = $day->format( "Y-m-d" );
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_year = $period_det[0];
			$day_code = date("w", strtotime($the_day));
			//$holiday = checkHoliday($the_day);//we wont include the days which are holidays
			//$urgents = checkUrgentHoliday($the_day);
			$holiday = in_array($the_day, $new_array);
			$isInLeave = isEmployeeInLeave_today($emp_id->id,$the_day);
			if($isInLeave)
			{//if employee is in leave, no need to check the available pic
				continue;
			}
			elseif($holiday)
			{//holidays and fridays
				if(!checkHolidayAbsent($the_day,$id))
				{//if it is holiday of friday, check the day before and after of holiday
					continue;
				}
			}
			else
			{
				if($day_code == 5)
				{//fridays for thuFri shif, as thu is check with fri, do nothing for fri
					continue;
				}
				else
				{
					$days[] = $period_year.'-'.$period_month.'-'.$period_day;
					array_push($apsentdays, $period_year.$period_month.$period_day);
				}

			}
		}
		//dd($days);
		//remove the last pipe line
		//$days = substr($days, 0, -1);
		//remove the last pipe line
		//$thu_days = substr($thu_days, 0, -1);
		$total = '';
		$rfid_lost = lost_rfid($emp_id->id);
		if($rfid_lost)
		{
			$rfids = array($id);
			foreach($rfid_lost as $new_rfid)
			{
				$rfids[] =$new_rfid->rfid;
			}
			if($days!=null)
			{
				$total =DB::connection('hr')
					->table('attendance_images')
					->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total,SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",3),"/",-1) AS year'))//return the monthDay part of path
					->whereIn('RFID',$rfids)//check the rfid
					->where('status',0)//present
					->whereIn('date',$days)
					->groupBy('groupByMe')
					->having('total', '>', 0)
					->get();
					//->toSql();dd($total->total);
			}
		}
		else
		{
			if($days!=null)
			{
				$total =DB::connection('hr')
					->table('attendance_images')
					->select(DB::raw('SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",4),"/",-1) AS groupByMe,count(id) As total,SUBSTRING_INDEX(SUBSTRING_INDEX(path,"/",3),"/",-1) AS year'))//return the monthDay part of path
					->where('RFID',$id)//check the rfid
					->where('status',0)//present
					->whereIn('date',$days)
					->groupBy('groupByMe')
					->having('total', '>', 0)
					->get();
					//->toSql();dd($total);
			}
		}
		$presented_days=array();
		if($total)
		{
			foreach($total as $pday)
			{
				array_push($presented_days, $pday->year.$pday->groupByMe);
			}
		}
		$absent_days = array_diff($apsentdays,$presented_days);

		return $absent_days;
	}

	public static function checkAttImageInsertStatus()
	{
		return DB::connection(self::$myDb)
				->table('att_images_insert_log')
				//->select('id')
				//->where('dep_type',$type)
				->where('status',0)
				->first();
    }

    /**
     * @Author: Jamal Yousufi
     * @Date: 2019-06-26 11:18:05
     * @Desc:  check if employee is not absend sequentially DELETE it
     */
	public static function checkEmployeeSeqAbsent($emp_id,$the_date)
	{
        $date = date_create($the_date);
        $date = date_sub($date, date_interval_create_from_date_string('1 day'));
        $one_day_back = date_format($date, 'Y-m-d');
        $dayOfWeek = date('D',strtotime($one_day_back));
        if($dayOfWeek!='Fri')
        {
            $result= DB::connection(self::$myDb)
                ->table('employee_seq_absents')
                //->select('id')
                //->where('dep_type',$type)
                ->where('employee_id',$emp_id)
                ->where('date',$one_day_back)
                ->first();

                if(count($result)==0)
                {
                    DB::connection(self::$myDb)
                    ->table('employee_seq_absents')->where('employee_id',$emp_id)
                    ->where('sent_mail',0)->delete();
                }
        }
    }

    /**
     * @Author: Jamal Yousufi
     * @Date: 2019-06-26 11:23:38
     * @Desc:  Get employees whome are absent more than 5 days sequentially
     */
	public static function bringSeqAbsentEmps()
	{
		   $result= DB::connection(self::$myDb)
				->table('employee_seq_absents')
				->where('sent_mail',0)//Employee whome are not emailed before
				->groupBy('employee_id')
				->havingRaw('count(id) >= 5')
                ->get();

			return $result;
    }

    public static function getLastPromotionValue($id=0)
	{
		$data = DB::connection(self::$myDb)
				->table('employee_promotions')
				->select('qadam')
				->where('employee_id',$id)
				->orderBy('id','desc')
				->first();
        if($data){
            return $data->qadam;
        }else{
            return "";
        }
    }

    /**
     * @Author: Jamal Yousufi
     * @Date: 2019-06-26 16:36:39
     * @Desc: Update Email Status of employees whom are sent Email
     */
    public static function updateAbsentUserStatus($seq_absent_emps)
    {
        DB::connection(self::$myDb)
          ->table('employee_seq_absents')
          ->where('sent_mail',0)
          ->update(['sent_mail' => 1]);
    }


}
