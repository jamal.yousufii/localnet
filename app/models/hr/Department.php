<?php

namespace App\models\hr;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;
use DateTime;
use DateInterval;
use DatePeriod;
class Department extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
    */
    protected $connection 	= 'mysql';
	protected $table 		= 'department';


    public function employee()
    {
        return $this->haseMany('App\models\hr\Employee','id','department');
    }
}
