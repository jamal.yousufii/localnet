<?php

namespace App\models\services;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class ServiceItem extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'services';
	public static $myDb 	= "services";
	protected $table 		= 'items';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('items AS t1');
					$table->select(
							't1.id',
							't1.item_description',
							'mu.name AS unit',
							'ft.name AS type'
							);
		
		$table->leftjoin("mesure_units AS mu","mu.id","=","t1.unit");
		$table->leftjoin("fees9_types AS ft","ft.id","=","t1.fees9_type");

		$object = $table->orderBy('t1.id','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("items")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("items")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
}