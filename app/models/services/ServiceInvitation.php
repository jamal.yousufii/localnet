<?php

namespace App\models\services;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class ServiceInvitation extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'services';
	public static $myDb 	= "services";
	protected $table 		= 'invitations';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('invitations AS t1');
					$table->select(
							't1.id',
							't1.date',
							't1.time',
							DB::raw("CASE t1.type 
							WHEN '1' THEN 'فوق العاده'
							WHEN '2' THEN 'درجه اول'
							WHEN '3' THEN 'درجه دوم'
							WHEN '4' THEN 'درجه سوم'
							END as type"),
							't1.number_of_guest',
							't1.maktob_number',
							't2.name AS location',
							't1.source'							
							
							);
		
		$table->leftjoin("palaces AS t2","t2.id","=","t1.location");

		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("invitations")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("invitations")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
}