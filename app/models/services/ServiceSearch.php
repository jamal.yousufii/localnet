<?php

namespace App\models\services;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class ServiceSearch extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'services';
	public static $myDb 	= "services";
	protected $table 		= 'service_employees';
	//$table = $table->paginate(15);

	public static function getEmployeeSearchResult()
	{
		
		$from = "";
		$to = "";
		if(Input::get('start_date') != '')
		{
			if(isMiladiDate())
			{
				$from = Input::get('start_date');
			}
			else
			{
				$from = ymd_format(Input::get('start_date'));
				$from = convertToGregorian($from);
			}
		}
		if(Input::get('end_date') != '')
		{
			if(isMiladiDate())
			{
				$to = Input::get('end_date');
			}
			else
			{
				$to = ymd_format(Input::get('end_date'));
				$to = convertToGregorian($to);
			}
		}
		
		$table = DB::connection(self::$myDb)
					->table('service_employees AS t1');
					$table->select(
							't1.id',
							't1.name',
							't1.father_name',
							't1.position',
							't1.start_date',
							't1.end_date',
							't1.employee_id',
							't2.name AS palace'
							);
				
		$table->leftjoin("palaces AS t2","t2.id","=","t1.palace_id");
		
		if(Input::get('start_date') != '')
		{
			$table->where('t1.start_date','=',$from);
		}
		if(Input::get('end_date') != '')
		{
			$table->where('t1.end_date','=',$from);
		}
		if(Input::get('name') != '')
		{
			$table->where('t1.name','LIKE',"%".Input::get('name')."%");
		}
		if(Input::get('father_name') != '')
		{
			$table->where('t1.father_name','LIKE',"%".Input::get('father_name')."%");
		}
		if(Input::get('position') != '')
		{
			$table->where('t1.position','LIKE',"%".Input::get('position')."%");
		}
		if(Input::get('ID_card') != '')
		{
			$table->where('t1.employee_id','=',Input::get('ID_card'));
		}
		if(Input::get('palace_id') != '')
		{
			$table->where('t1.palace_id','=',Input::get('palace_id'));
		}
		if(Input::get('is_general_manager') != '')
		{
			$table->where('t1.is_general_manager','=',Input::get('is_general_manager'));
		}
		if(Input::get('is_palace_manager') != '')
		{
			$table->where('t1.is_palace_manager','=',Input::get('is_palace_manager'));
		}
		
		
		
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $table->paginate(10);
	}
	public static function getInvitationSearchResult()
	{
		
		$date = "";
		if(Input::get('date') != '')
		{
			if(isMiladiDate())
			{
				$date = Input::get('date');
			}
			else
			{
				$date = ymd_format(Input::get('date'));
				$date = convertToGregorian($date);
			}
		}
		
		
		$table = DB::connection(self::$myDb)
					->table('invitations AS t1');
					$table->select(
							't1.id',
							't1.date',
							't1.time',
							DB::raw("CASE t1.type 
							WHEN '1' THEN 'عادی'
							WHEN '2' THEN 'رسمی'
							END as type"),
							't1.number_of_guest',
							't1.maktob_number',
							't2.name AS location',
							't1.source'							
							
							);
		
		$table->leftjoin("palaces AS t2","t2.id","=","t1.location");
		
		if(Input::get('date') != '')
		{
			$table->where('t1.date','=',$date);
		}
		if(Input::get('time') != '')
		{
			$table->where('t1.time','=',Input::get("time"));
		}
		if(Input::get('type') != '')
		{
			$table->where('t1.type','=',Input::get("type"));
		}
		if(Input::get('number_of_guest') != '')
		{
			$table->where('t1.number_of_guest','=',Input::get("number_of_guest"));
		}
		if(Input::get('maktob_number') != '')
		{
			$table->where('t1.maktob_number','=',Input::get("maktob_number"));
		}
		if(Input::get('location') != '')
		{
			$table->where('t1.location','=',Input::get("location"));
		}
		if(Input::get('source') != '')
		{
			$table->where('t1.source','LIKE',"%".Input::get('source')."%");
		}
		
		
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $table->paginate(10);
	}
	
	public static function getFees9SearchResult()
	{
		
		$date = "";
		if(Input::get('date') != '')
		{
			if(isMiladiDate())
			{
				$date = Input::get('date');
			}
			else
			{
				$date = ymd_format(Input::get('date'));
				$date = convertToGregorian($date);
			}
		}
		
		$items = Input::get("fees9_item");
		
		
		$table = DB::connection(self::$myDb)
					->table('fees9 AS t1');
					$table->select(
							't1.id',
							't1.form_number',
							't1.date',
							't2.name as type',
							DB::raw('CONCAT(count(t3.fees9_id)," - قلم جنس" ) AS item_count'),
							't1.created_at'
							);
				
		$table->leftjoin("fees9_items AS t3","t3.fees9_id","=","t1.id");
		$table->leftjoin("fees9_types AS t2","t2.id","=","t1.type");
		
		
		if(Input::get('date') != '')
		{
			$table->where('t1.date','=',$date);
		}
		if(Input::get('form_number') != '')
		{
			$table->where('t1.form_number','=',Input::get("form_number"));
		}
		if(Input::get('type') != '')
		{
			$table->where('t1.type','=',Input::get("type"));
		}
		
		$table->where(function($query){
			$c1 = "0";
			$c2 = "0";
			$c3 = "0";
			$c4 = "0";
			
			if(Input::get('daily_specail') != '')
			{
				//$table->where('f.daily_specail','=',Input::get("daily_specail"));
				$c1 = 't1.daily_specail='.Input::get("daily_specail");
			}
			if(Input::get('daily_employee') != '')
			{
				//$table->where('f.daily_employee','=',Input::get("daily_employee"));
				$c2 = 't1.daily_employee='.Input::get("daily_employee");
			}
			if(Input::get('daily_employee_3') != '')
			{
				//$table->where('f.daily_employee_3','=',Input::get("daily_employee_3"));
				$c3 = 't1.daily_employee_3='.Input::get("daily_employee_3");
			}
			if(Input::get('other') != '')
			{
				//$table->where('f.other','=',Input::get("other"));
				$c4 = 't1.other='.Input::get("other");
			}
		
			return $query->where('daily_specail','=',$c1)
			->orWhere('daily_employee','=',$c2)
			->orWhere('daily_employee_3','=',$c3)
			->orWhere('other','=',$c4);
		});
		
		if(count($items) > 0)
		{
			
			$items_arr = array();
			for($i=0;$i<count($items);$i++){
				$items_arr[] = $items[$i];
			}
			
			$table->whereIn('t3.item_id',$items_arr);
		}
		
		$table->groupBy("t3.fees9_id");
		$table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $table->paginate(10);
	}
	public static function getFees9ReportResult()
	{
		
		$start_date = "";
		$end_date = "";
		if(Input::get('start_date') != '')
		{
			if(isMiladiDate())
			{
				$start_date = Input::get('start_date');
			}
			else
			{
				$start_date = ymd_format(Input::get('start_date'));
				$start_date = convertToGregorian($start_date);
			}
		}
		if(Input::get('end_date') != '')
		{
			if(isMiladiDate())
			{
				$end_date = Input::get('end_date');
			}
			else
			{
				$end_date = ymd_format(Input::get('end_date'));
				$end_date = convertToGregorian($end_date);
			}
		}
		
		$items = Input::get("fees9_item");
		
		$table = DB::connection(self::$myDb)
					->table('fees9_items AS t1');
					$table->select(
							't1.id',
							'i.item_description AS name',
							DB::raw('SUM(t1.amount) AS total'),
							'u.name AS unit'
							);
				
		$table->leftjoin("items AS i","i.id","=","t1.item_id");
		$table->leftjoin("mesure_units AS u","u.id","=","t1.unit");
		$table->leftjoin("fees9 AS f","f.id","=","t1.fees9_id");
		
		
		//--- search criteria ------------------------------------------//
		if(Input::get('start_date') != '' && Input::get('end_date') == '')
		{
			$table->where('f.created_at','>=',$start_date);
		}
		elseif(Input::get('start_date') == '' && Input::get('end_date') != '') 
		{
			$table->where('f.created_at','<=',$end_date);
		}
		elseif(Input::get('start_date') != '' && Input::get('end_date') != '') 
		{
			$table->whereBetween('f.created_at',array($start_date,$end_date));
		}
		
	
		$table->where(function($query){
		
			if(Input::get('daily_specail') != '')
			{
				$query->orWhere('daily_specail','=',1);
			}
			if(Input::get('daily_employee') != '')
			{
				$query->orWhere('daily_employee','=',1);
			}
			if(Input::get('daily_employee_3') != '')
			{
				$query->orWhere('daily_employee_3','=',1);
			}
			if(Input::get('other') != '')
			{
				$query->orWhere('other','=',1);
			}
		
			return $query;
		});
		
		if(count($items) > 0)
		{
			
			$items_arr = array();
			for($i=0;$i<count($items);$i++){
				$items_arr[] = $items[$i];
			}
			
			$table->whereIn('t1.item_id',$items_arr);
		}
		else if(Input::get("type") != "")
		{
			$table->where("type",Input::get("type"));
		}
		
		$table->groupBy("t1.item_id");
		return $table->get();
		//echo $table->tosql();exit;
		//return $table->paginate(10);
	}
	
}