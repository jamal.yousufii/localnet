<?php

namespace App\models\services;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class ServiceEmployees extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'services';
	public static $myDb 	= "services";
	protected $table 		= 'service_employees';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('service_employees AS t1');
					$table->select(
							't1.id',
							't1.name',
							't1.father_name',
							't1.position',
							't1.start_date',
							't1.end_date',
							't1.employee_id',
							't2.name AS palace'
							);
				
		$table->leftjoin("palaces AS t2","t2.id","=","t1.palace_id");
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("service_employees")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("service_employees")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static function getEmpComments($id=0)
	{
		return DB::connection(self::$myDb)->table("employee_comments")->where("emp_id",$id)->get();
	}
}