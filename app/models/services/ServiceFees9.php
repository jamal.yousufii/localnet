<?php

namespace App\models\services;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Input;

class ServiceFees9 extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection 	= 'services';
	public static $myDb 	= "services";
	protected $table 		= 'fees9';
	//$table = $table->paginate(15);

	public static function getData()
	{
		$table = DB::connection(self::$myDb)
					->table('fees9 AS t1');
					$table->select(
							't1.id',
							't1.form_number',
							't1.date',
							't2.name as type',
							DB::raw('CONCAT(count(t3.fees9_id)," - قلم جنس" ) AS item_count'),
							't1.created_at'
							);
				
		$table->leftjoin("fees9_items AS t3","t3.fees9_id","=","t1.id");
		$table->leftjoin("fees9_types AS t2","t2.id","=","t1.type");
		$table->groupBy("t3.fees9_id");
		$object = $table->orderBy('t1.created_at','desc');
		//echo $object->tosql();
		return $object->get();
	}
	
	public static function getDetails($id=0)
	{
		return DB::connection(self::$myDb)->table("fees9")->where("id",$id)->first();
	}
	public static function getDelete($id=0)
	{
		if(DB::connection(self::$myDb)->table("fees9")->where('id',$id)->delete())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static function getFees9Items($fees9Id=0)
	{
		return DB::connection(self::$myDb)->table("fees9_items")->where("fees9_id",$fees9Id)->get();
	}
}