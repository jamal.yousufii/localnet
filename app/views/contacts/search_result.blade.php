<head>
    
    <style type="text/css">

        .ltr
        {
            direction: ltr;
            text-align: center;
        }
        .center
        {
            text-align: center;
        }
       #list_previous a, #list_next a {padding:9px}


    </style>

</head>

    <?php
        Session::put('general_dep', $general_dep);
        Session::put('sub_dep', $sub_dep);
    ?>

<fieldset>
	<legend style="color:orange;">نتیجه جستجو!</legend>
	<div class="table-responsive" style="overflow-x:hidden">
		<table class="table" id="list" dir="rtl">
			<thead>
				<tr style="background: #0089c0;color:white;">
                    <th>#</th>
                    <!-- <th>دیپارتمنت</th> -->
                    <th>نام مکمل</th>
                    <th>وظیفه</th>
                    <th>سیسکو</th>
                    <th>زیمنس</th>
                    <th>فکس</th>
                    <th>موبایل</th>
                    @if(canEdit('cons_list'))
                        <th>عملیه</th>
                        @endif
                </tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>

	</div>

</fieldset>

<script type="text/javascript">

$(document).ready(function() {
    $('#list').dataTable(
        {

            //'sDom': 'Tlfr<"clearfix">tip',
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "print",
                        "bShowAll": true
                    }
                ]
            },
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('ResultData')!!}",
            //"aaSorting": [[ 1, "desc" ]],
            "aoColumns": [
                { 'sWidth': '20px', 'sClass': 'center'},
                { 'sWidth': '120px', 'sClass': 'center'},
                // { 'sWidth': '100px'},
                { 'sWidth': '160px', 'sClass': 'center'},
                { 'sWidth': '100px', 'sClass': 'ltr'},
                { 'sWidth': '100px', 'sClass': 'ltr'},
                { 'sWidth': '100px', 'sClass': 'ltr'},
                { 'sWidth': '100px', 'sClass': 'ltr'},
                { 'sWidth': '60px', 'sClass': 'center'}
            ],
            "language": {
                "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                "zeroRecords": "ریکارد موجود نیست",
                "info": "نمایش صفحه _PAGE_ از _PAGES_",
                "infoEmpty": "ریکارد موجود نیست",
                "search": "جستجو",
                "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
            }
        }
    );
});
</script>

