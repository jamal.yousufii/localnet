@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('edit_page')!!}</title>
    <style type="text/css">
        .ltr
        {
            direction: ltr;
            text-align: center;
        }
        a{text-decoration:none}
    	#list_previous a, #list_next a {padding:9px}
       	#list th{
       	color:white !important;
       	text-align:center !important;
       	}
       	#list td{
       	text-align:center !important;
       	}
       	.form-control
        {
            width: 50% !important;
        }
        .main-box
        {
            height: 54em !important;
        }
    </style>

@stop

@section('content')

<div class="row" style="opacity: 1;">  
<div class="container" dir="rtl">
    
        <div class="main-box" style="margin:0;padding:0">
        <div style="margin:0 auto;width:800px;text-align:center;">{!! HTML::image("img/header.png",'',array('style'=>'max-width:100%;padding:20px')) !!}</div>
            <!-- <div id="check_result" style="padding: 20px"></div> -->
            @foreach($contacts as $item)
            <div style="padding: 2em;">
                <h4 align="center" style="font-weight:bold">اضافه کردن شماره های تیلیفون</h4>
                <form role="form" method="post" action="{!! URL::route('postUpdateForm', $item->id) !!}">
               
                    <table class="table">
                        <tr>
                            <td>زیمنس :</td>
                            <td>
                                <input class="form-control" type="text" name="zemins" value="{!!$item->ext1!!}" dir="ltr" />
                            </td>
                        </tr>
                        <tr>
                             <td>سیسکو :</td>
                            <td>
                                <input class="form-control" type="text" name="cisco" value="{!!$item->ext2!!}" dir="ltr" />
                            </td>
                        </tr>
                        <tr>
                             <td>فکس :</td>
                            <td>
                                <input class="form-control" type="text" name="fax" value="{!!$item->ext3!!}" dir="ltr" />
                            </td>
                        </tr>
                        <!-- <tr>
                             <td>شماره موبایل :</td>
                            <td>
                                <input class="form-control" type="text" name="phone" value="{!!$item->phone!!}" dir="ltr" />
                            </td>
                        </tr>
                        <tr>
                             <td>ایمیل :</td>
                            <td>
                                <input class="form-control" type="email" name="email" value="{!!$item->email!!}" dir="ltr" />
                            </td>
                        </tr> -->
                    </table>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <hr style="border: 1px dashed #b6b6b6" />
                            <button class="btn btn-primary" type="submit">
                                <span>
                                    <i class="fa fa-check"></i>
                                </span>
                                &nbsp;تأید
                            </button>
                            <a href="{!!URL::route('arg.net')!!}" class="btn btn-danger" type="reset">
                                <i class="glyphicon glyphicon-remove"></i>
                                لغو کردن
                            </a>
                        </div>
                    </div>
                    @endforeach
                </form>
            </div>
        </div>
</div>

@stop

@section('footer-scripts')
<script type="text/javascript">
    $(document).ready(function() {

    });
</script>

@stop


