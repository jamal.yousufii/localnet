@extends('layouts.master')

@section('head')
    {!! HTML::style('/vendor/select2/select2.css') !!}

    <title>{!!_('vehicle_reservation_insert')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('vehicle_reservation_insert')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getVehicleReservationList')!!}" class="btn btn-success">Back</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('insertVehicleReservation')!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("driver")!!}</label>
                                <select name="driver" id="driver" class="form-control" style="width:100%">
                                    <option value="">---</option>
                                    @foreach($drivers AS $item)
                                    	<option {!! (old('driver')==$item->id?"selected":"")!!} value="{!!$item->id!!}">{!!$item->first_name." ".$item->last_name." (".$item->card_no.")"!!}</option>
                                    @endforeach
                                </select> 
                                <span style="color:red;">{!! $errors->first('driver') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("vehicle")!!}</label>
                                <select name="vehicle" id="vehicle" class="form-control" style="width:100%">
                                    <option value="">---</option>
                                    @foreach($vehicles AS $item)
                                    	<option {!! (old('vehicle')==$item->v_id?"selected":"")!!} value="{!!$item->v_id!!}">{!!$item->v_type." - نمبر پلت: ".$item->palet_no." مودل :" .$item->model!!}</option>
                                    @endforeach
                                </select> 
                                <span style="color:red;">{!! $errors->first('vehicle') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("service_area")!!}</label>
                                <select name="service_area" id="service_area" class="form-control" style="width:100%" onchange="bringRelatedSubDepartment('sub_dep_div',this.value)">
                                    <option value="">---</option>
                                    @foreach($departments AS $item)
                                    	<option {!! (old('service_area')==$item->id?"selected":"")!!} value="{!!$item->id!!}">{!!$item->name!!}</option>
                                    @endforeach
                                </select> 
                                <span style="color:red;">{!! $errors->first('service_area') !!}</span>
                            </div>
                  
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("reserved_date")!!}</label>
                                <input value="{!! old('date') !!}" type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}">
                                <span style="color:red;">{!! $errors->first('date') !!}</span> 
                            </div>
                            
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("reserved_to_dep")!!}</label>
                                <div id="sub_dep_div">
	                                <select name="reserved_to_dep" id="reserved_to_dep" class="form-control" style="width:100%">
	                                </select>
                                </div>
                                <span style="color:red;">{!! $errors->first('reserved_to_dep') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("reserved_type")!!}</label>
                                <select name="reserved_type" id="reserved_type" class="form-control" style="width:100%">
                                    <option value="">---</option>
                                    <option {!! (old('reserved_type')==1?"selected":"")!!} value="1">توظیف</option>
                                    <option {!! (old('reserved_type')==2?"selected":"")!!} value="2">خدمتی</option>

                                </select> 
                                <span style="color:red;">{!! $errors->first('reserved_type') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("status")!!}</label>
                                <select name="status" id="status" class="form-control" style="width:100%">
                                    <option {!! (old('status')==1?"selected":"")!!} value="1">توظیف شده</option>
                                    <option {!! (old('status')==0?"selected":"")!!} value="0">ریزرف شده یا پارک شده</option>

                                </select> 
                                <span style="color:red;">{!! $errors->first('status') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("using_by")!!}</label>
                                <input value="{!! old('using_by') !!}" type="text" name="using_by" id="using_by" class="form-control">
                                <span style="color:red;">{!! $errors->first('using_by') !!}</span>
                                
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("line")!!}</label>
                                <input type="text" name="line" id="line" class="form-control">
                                <span style="color:red;">{!! $errors->first('line') !!}</span>
                                
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("user_contact_number")!!}</label>
                                <input value="{!! old('user_phone') !!}" type="text" name="user_phone" id="user_phone" class="form-control">
                                <span style="color:red;">{!! $errors->first('user_phone') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label"> </label>
                                <div class="checkbox checkbox-nice" style="margin:20px;">
                                    <input onclick="getUsingBy()" id="is_hamala" name="is_hamala" type="checkbox" value="1">
                                    <label for="is_hamala">
                                        {!!_('is_hamala')!!}
                                    </label>
                                </div>
                            </div>
                            
                            
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('save')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
<script>
	$("#driver").select2();
	$("#vehicle").select2();
	$("#service_area").select2();
	$("#reserved_to_dep").select2();
	
	function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringRelatedSubDepartment")!!}',
                data: '&dep_id='+id+"&element_id=reserved_to_dep",
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                    $("#reserved_to_dep").select2();
                }
            }
        );
    }
    
    function getUsingBy(){
    
	    if($('#is_hamala').is(':checked')){
	    
	    	$("#using_by").val(" ");
    		$("#using_by").attr("disabled","disabled");
	    }
	    else
	    {
	    	$("#using_by").removeAttr("disabled");
	    }
    }
    // $("#is_hamala").on(":checked",function(){
    // });
</script>
@stop
