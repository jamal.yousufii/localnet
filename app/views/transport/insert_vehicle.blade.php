@extends('layouts.master')

@section('head')
    <title>{!!_('vehicle_insert')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('vehicle_insert')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getVehicleList')!!}" class="btn btn-success">{!!_('back')!!}</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('insertVehicle')!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-xs-3">
                            	<label class="control-label">نو وسایط</label>
                                <select name="vehicle_type" id="vehicle_type" class="form-control">
                                    {!!getStaticTable("vehicle_type","transport")!!}
                                </select>
                            </div>
                        
                            <div class="form-group col-xs-3">
                            	<label class="control-label">مودل</label>
                                <input type="text" name="model" id="model" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">نمبر پلیت</label>
                                <input type="text" name="palet" id="palet" class="form-control">
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">نمبر انجن</label>
                                <input type="text" name="engine" id="engine" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">نمبر شاسی</label>
                                <input type="text" name="shasi" id="shasi" class="form-control">
                                  
                            </div>
                             <div class="form-group col-xs-3">
                            	<label class="control-label">رنگ</label>
                                <input type="text" name="color" id="color" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">نمبر جواز سیر</label>
                                <input type="text" name="license" id="license" class="form-control">
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">وضعیت فعلی واسطه</label>
                                <select name="current_status" id="current_status" class="form-control">
                                   <option value="">---</option>
                                   <option value="1">فعال</option>
                                   <option value="2">غیر فعال</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">نوع مواد سوخت</label>
                                <select name="fuel_type" id="fuel_type" class="form-control">
                                    {!!getStaticTable("fuel_types","transport")!!}
                                </select>
                            </div>
                            
                            <div class="form-group col-xs-3">
                            	<label class="control-label">مصرف مواد سوخت در ۱۰۰ کیلومتر یا در مایل</label>
                                <input type="text" name="spend_per_km" id="spend_per_km" class="form-control">
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">ترمیمات مورد نیاز</label>
                                <input type="text" name="repairing_needs" id="repairing_needs" class="form-control">
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">مبلغ تخمینی برای ترمیم</label>
                                <input type="text" name="amount_for_repair" id="amount_for_repair" class="form-control">
                            </div>
                            <div class="form-group col-xs-12">
                            	<label class="control-label">ملاحظات</label>
                                <textarea name="description" id="description" class="form-control"></textarea>
                            	<div class="checkbox checkbox-nice" style="margin:20px;">
                                    <input id="lylam" name="lylam" type="checkbox" value="1">
                                    <label for="lylam">
                                        {!!_('lylam')!!}
                                    </label>
                                </div>
                            </div>
                            
                            <div class="form-group col-xs-12">
		                        <label class="control-label">{!!_('attachment')!!}</label>
		                        <div id='files_div'>
		                            <input style='width:1200px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
		                        </div>
		                        
		                    </div>
                           
                        </div>
                        <br>
                        <div class="form-group col-xs-1">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('save')!!}</button>
                        </div>
                        <div class="form-group col-xs-1" style="margin-left:10px;">
                            <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).on("change","#files",function(){
    var count = $('#files_div').find('input').length;
    count = count+1;
    $("<div id='div_"+count+"' style='display:inline;'><input style='width:1200px;splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:left;margin-top:-37px;' class=\"btn btn-default btn-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div'); 
	
	$("#upload_attachment").prop("disabled",false);
	
});

</script>

@stop
