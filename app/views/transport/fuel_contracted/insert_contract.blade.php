@extends('layouts.master')

@section('head')
    <title>{!!_('fuel_contract_insert')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('fuel_contract_insert')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getFuelContractedList')!!}" class="btn btn-success">Back</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('insertFuelContract')!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            
                            <div class="form-group col-xs-4">
                            	<label class="control-label">نام شرکت</label>
                                <input type="text" name="company" id="company" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">درجه ثقلت</label>
                                <input type="text" name="gravity" id="gravity" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">تاریخ</label>
                                <input type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}">
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">ساعت</label>
                                <input type="text" name="time" id="time" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">سریال نمبر</label>
                                <input type="text" name="serial_no" id="serial_no" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">نمبر عراده جات</label>
                                <input type="text" name="vehicle_no" id="vehicle_no" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">اسم دریور</label>
                                <input type="text" name="driver_name" id="driver_name" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">اسم پارتی ترانسپورت</label>
                                <input type="text" name="party_name" id="party_name" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">تشریحات مال بارشده</label>
                                <textarea name="description" id="description" class="form-control"></textarea>
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">وزن اول (کیلو گرام)</label>
                                <input type="text" name="first_weight" id="first_weight" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">وزن دوم (کیلو گرام)</label>
                                <input type="text" name="second_weight" id="second_weight" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">وزن خالص (کیلو گرام)</label>
                                <input type="text" name="net_weight" id="net_weight" class="form-control">
                                  
                            </div>
                            
                            <div class="form-group col-xs-4">
                            	<label class="control-label">{!!_("fuel_type")!!}</label>
                                <select name="fuel_type" id="fuel_type" class="form-control">
                                    {!!getStaticTable("fuel_types","transport",old('fuel_type'))!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('fuel_type') !!}</span>
                            </div>
                            
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('save')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop
