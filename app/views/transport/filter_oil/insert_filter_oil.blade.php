@extends('layouts.master')

@section('head')
    <title>{!!_('filter_oil_insert')!!}</title>
    {!! HTML::style('/vendor/select2/select2.css') !!}
@stop
<style>
	.select2{
		width: 100% !important;
	}
</style>
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('filter_oil_insert')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getFilterOilList')!!}" class="btn btn-success">{!!_('back')!!}</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('insertFilterOil')!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                                
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("item_category")!!}</label>
                                <select name="category" id="category" class="form-control">
									<option value="">{!!_('Select_Category')!!}</option>
									<option value="1">{!!_('Filter_&_Oil')!!}</option>
									<option value="2">{!!_('Porzajat')!!}</option>
									<option value="3">{!!_('Tires')!!}</option>
									<option value="4">{!!_('vehicle_needs')!!}</option>
									<option value="5">{!!_('oil')!!}</option>
								</select>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("item_description")!!}</label>
                                <textarea name="item_description" id="item_description" class="form-control">{!! old('item_description') !!}</textarea>
                                <span style="color:red;">{!! $errors->first('item_description') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("properties")!!}</label>
                                <input value="{!! old('properties') !!}" type="text" name="properties" id="properties" class="form-control">
                                <span style="color:red;">{!! $errors->first('properties') !!}</span>  
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("amount")!!}</label>
                                <input value="{!! old('amount') !!}" type="text" name="amount" id="amount" class="form-control">
                                <span style="color:red;">{!! $errors->first('amount') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("unit")!!}</label>
                                <select name="unit" id="unit" class="form-control">
                                    {!!getStaticTable("mesure_units","transport",old('unit'))!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('unit') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("motamid")!!}</label>
                                <select name="motamid" id="motamid" class="form-control">
                                <option value="">---</option>
                                @if($motamids)
                                	@foreach($motamids AS $item)
                                		<option value="{!!$item->id!!}">{!!$item->first_name." ".$item->last_name!!}</option>
                                	@endforeach
                                @endif
                                </select>
                                <span style="color:red;">{!! $errors->first('motamid') !!}</span>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('save')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
<script>
	$(document).ready(function(){
		
		$("#motamid").select2();
		$("#unit").select2();
		
	});
</script>
@stop
