@extends('layouts.master')

@section('head')

    <title>{!!_('filter_oil_list')!!}</title>
<style>

.import_form{
	padding: 20px;
	
	margin-bottom: 15px;

}
</style>
@stop
@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="main-box">
			<header class="main-box-header clearfix">
			    <h2>{!!_('filter_oil_list')!!}</h2>
			    @if(Session::has('success'))
			    <span class='alert alert-success' style="width:400px;">
			        <i class="fa fa-check-circle fa-fw fa-lg"></i>
			        {!!Session::get('success')!!}
			    </span>
			    @elseif(Session::has('fail'))
			    <span class='alert alert-danger' style="width:400px;">
			        <i class="fa fa-times-circle fa-fw fa-lg"></i>
			        {!!Session::get('fail')!!}
			    </span>
			    @endif
			    <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en")
			    	{
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getCreateFilterOil')!!}" class="btn btn-success">Add New</a></span>
				<div class="import_form">
					<label style="font-weight:bold;">{!!_('Import_Stack_Excel_File')!!}</label>
					<form method="post" action="{!!URL::route('importContractExcelFile')!!}" enctype="multipart/form-data">
						
						<input type="file" name="excel_file" class="form-control col-sm-2" />
						<select name="year" id="year" class="form-control col-sm-2">
						<option value="">{!!_('select_year')!!}</option>
						<?php 
							$year = date("Y");
							$year = $year-621;
							for($i=1;$i<=20;$i++)
							{
								
								echo "<option value='".$year."'>".$year."</option>";
								$year--;
							}
						?>
						</select>
						<select name="category" id="category" class="form-control col-sm-2">
							<option value="">{!!_('Select_Category')!!}</option>
							<option value="1">{!!_('Filter_&_Oil')!!}</option>
							<option value="2">{!!_('Porzajat')!!}</option>
							<option value="3">{!!_('Tires')!!}</option>
							<option value="4">{!!_('vehicle_needs')!!}</option>
							<option value="5">{!!_('oil')!!}</option>
						</select>
						<select name="motamid" id="motamid" class="form-control col-sm-2">
                        <option value="">{!!_('Select_motamid')!!}</option>
                        @if($motamids)
                        	@foreach($motamids AS $item)
                        		<option value="{!!$item->id!!}">{!!$item->first_name." ".$item->last_name!!}</option>
                        	@endforeach
                        @endif
                        </select>
						<input type="submit" value="{!!_('import')!!}" class="btn btn-warning col-sm-2" />
					</form>
				</div>
				<hr />
			</header>
			
			<div class="main-box-body clearfix">
				<div class="table-responsive">
				    <table class="table table-responsive" id='list'>
				        <thead>
				        <tr>
				            <th>#</th>
				            <th>{!!_('item_description')!!}</th>
				            <th>{!!_('properties')!!}</th>
				            <th>{!!_('amount')!!}</th>
				            <th>{!!_('unit')!!}</th>
				            <th>{!!_('motamid')!!}</th>
				            <th>{!!_('operation')!!}</th>
				        </tr>
				        </thead>
				        <tbody>
				        </tbody>
				    </table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {

                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                //"iDisplayLength": 2,
                "sAjaxSource": "{!!URL::route('getFilterOilData')!!}",
                "aaSorting": [[ 1, "desc" ]],
                
                "language": {
                    "lengthMenu": "{!!_('view')!!} _MENU_ {!!_('record_per_page')!!}",
                    "zeroRecords": "{!!_('record_not_found')!!}",
                    "info": "{!!_('page_view')!!} _PAGE_ {!!_('of')!!} _PAGES_",
                    "infoEmpty": "{!!_('record_not_found')!!}",
                    "search": "{!!_('search')!!}",
                    "infoFiltered": "(filtered {!!_('of')!!} _MAX_ {!!_('total_record')!!})"
                }
            }
        );

    });
</script>
@stop

