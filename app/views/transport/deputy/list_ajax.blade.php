<style>
	#list td{
		vertical-align: center;
		text-align: center;
	}
	#list th{
		text-align: center;
		font-weight: bold;
		background-color: #62a8ea;
		color: #fff !important;
	}
</style>

<table class="table table-bordered" id='list'>
    <thead>
    <tr>
        <th>#</th>
        <th>{!!_('vehicle_category')!!}</th>
        <th>{!!_('vehicle_model')!!}</th>
        <th>{!!_('vehicle_type')!!}</th>
        <th>{!!_('vehicle_mileage')!!}</th>
        <th>{!!_('vehicle_plate')!!}</th>
        <th>{!!_('is_assigned')!!}</th>
        <th>{!!_('vehicle_photo')!!}</th>
        <th colspan="2">{!!_('operation')!!}</th>
    </tr>
    </thead>
    <tbody>
    <?php $counter = $vehicles->firstItem(); ?>
    @if($vehicles && count($vehicles) > 0)
    
    	@foreach($vehicles AS $item)
    		<tr>
    		<td>{!!$counter!!}</td>
    		<td>{!!$item->vehicle_category!!}</td>
    		<td>{!!$item->model!!}</td>
    		<td>{!!$item->vehicle_type!!}</td>
    		<td>{!!$item->mileage!!}</td>
    		<td>{!!$item->plate!!}</td>
    		<td>{!!($item->status==1?"YES":"NO")!!}</td>
    		<td>
    		{!! HTML::image('/documents/transport/deputy/'.$item->photo, 'CAR PHOTO', array('class' => 'img-bordered img-bordered-orange','height'=>'90','width'=>'120')) !!}
    		</td>
    		<td>
                <a href="{!!URL::route('getEditDeputyVehicle',$item->id)!!}">
                    <i class="fa fa-folder-open fa-2x" aria-hidden="true"></i>
                </a>
            </td>
            <td>
                <a href="{!!URL::route('getDeleteDeputyVehicle',$item->id)!!}" onclick="javascript: return confirm('Do you want to continue?');">
                    <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                </a>
            </td>
            </tr>
            <?php $counter++; ?>
    	@endforeach
    @else
    <tr class="danger"><td colspan="9" style="font-weight: bold;"><i class="fa fa-warning fa-2x"></i> {!!_('record_not_found')!!}</td></tr>
    @endif
    </tbody>
</table>
<div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
	{!!$vehicles->render()!!}
</div>

<script>
$( document ).ready(function() {
		$('.pagination a').on('click', function(event) {
			event.preventDefault();
			if ($(this).attr('href') != '#') {
				//$('#ajaxContent').load($(this).attr('href'));
				
				$.ajax({
		                url: '{!!URL::route("getDeputyVehiclesData")!!}',
		                data: "&page="+$(this).text()+"&ajax=1",
		                type: 'post',
		                beforeSend: function(){
		
		                    //$("body").show().css({"opacity": "0.5"});
		                    $('#ajax_content').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
		                },
		                success: function(response)
		                {
		
		                    $('#ajax_content').html(response);
		                }
		            }
		        );
			
			}
		});
	});
</script>