@extends('layouts.master')

@section('head')
    <title>{!!_('vehicle_insert')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('vehicle_insert')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getDeputyVehicles')!!}" class="btn btn-success">Back</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('insertDeputyVehicle')!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('vehicle_category')!!}</label>
                                <select name="vehicle_category" id="vehicle_category" class="form-control">
                                    {!!getStaticTable("vehicle_category","transport")!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('vehicle_category') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('vehicle_type')!!} </label>
                                <select name="vehicle_type" id="vehicle_type" class="form-control">
                                    {!!getStaticTable("vehicle_type","transport")!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('vehicle_type') !!}</span>
                            </div>
                        
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('vehicle_model')!!}</label>
                                <input type="text" name="model" id="model" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('vehicle_mileage')!!}</label>
                                <input type="text" name="mileage" id="mileage" class="form-control">
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('vehicle_plate')!!}</label>
                                <input type="text" name="plate" id="plate" class="form-control">
                                <span style="color:red;">{!! $errors->first('plate') !!}</span>
                            </div>
                            
                             <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('color')!!}</label>
                                <input type="text" name="color" id="color" class="form-control">
                                  
                            </div>
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('vehicle_status')!!}</label>
                                <select name="status" id="status" class="form-control">
                                   <option value="">---</option>
                                   <option value="1">{!!_('assigned')!!}</option>
                                   <option value="2">{!!_('available')!!}</option>
                                </select>
                            </div>
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">ملاحظات</label>
                                <textarea name="description" id="description" class="form-control"></textarea>
                            	
                            </div>
                            <div class="form-group col-xs-6">
	                        	<label class="control-label">{!!_('car_photo')!!}</label>
	                            <input type="file" name="files[]" id="files" class="form-control">
	                        </div>
                           
                        </div>
                        
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('save')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop

