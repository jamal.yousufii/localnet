@extends('layouts.master')

@section('head')
    <title>{!!_('driver_edit')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('driver_edit')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getDriverList')!!}" class="btn btn-success">{!!_('back')!!}</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('updateDriver',$row->id)!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            
                            <div class="form-group col-xs-4">
                            	<label class="control-label">{!!_('card_no')!!}</label>
                                <input value="{!!$row->card_no!!}" type="text" name="card_no" id="card_no" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">{!!_('first_name')!!}</label>
                                <input value="{!!$row->first_name!!}" type="text" name="first_name" id="first_name" class="form-control">
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">{!!_('last_name')!!}</label>
                                <input value="{!!$row->last_name!!}" type="text" name="last_name" id="last_name" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">{!!_('father_name')!!}</label>
                                <input value="{!!$row->father_name!!}" type="text" name="father_name" id="father_name" class="form-control">
                                  
                            </div>
                             <div class="form-group col-xs-4">
                            	<label class="control-label">{!!_('phone')!!}</label>
                                <input value="{!!$row->phone!!}" type="text" name="phone" id="phone" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label>&nbsp;</label><br>
                                <label>{!!_('is_motamid')!!}: </lable><input type="checkbox" value="1" {!!($row->is_motamid==1?"checked":"")!!} name="is_motamid" id="is_motamid">
                                  
                            </div>
                            
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('save')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop
