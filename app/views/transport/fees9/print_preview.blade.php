{!! HTML::style('css/bootstrap.min.css') !!}
  {!! HTML::style('css/bootstrap-extend.min.css') !!}
  {!! HTML::style('css/site.min.css') !!}
  {!! HTML::style('css/new-template.css') !!}
  {!! HTML::style('css/noori.css') !!}

    <title>{!!_('fees9_form')!!}</title>
    
<style>

* {font-family: Noori !important; font-size: 24px !important;}
 
 .table_header td{
 	text-align: right;
 }
 .table_body th{
 	text-align: center;
 }
 
 li{
 	height: 30px;
 	list-style: none;
 }
 
</style>
<body dir="rtl">
    <table border="0" width="100%" class="table_header">
    	<tr>
        	<td width="33%">
        		<div class="header_right" style="float: right;">
        			۱. شماره:
        			 {!!$row->number!!}
        			  - شماره دیتابیس: {!!$row->id!!}
        			<br>
        			۲. تاریخ: 
        			@if(isMiladiDate())
                     <?php echo ($row->date != '0000-00-00') ? toGregorian($row->date):''; ?>
                	@else
                	 <?php echo ($row->date != '0000-00-00') ? dmy_format($row->date):''; ?>
                	@endif
        			<br>
        			<span>لطفا اجناس ذیل را فراهم نمایند</span>
        		</div>
        	</td>
        	<td width="33%" style="text-align: center;">
        		<div class="header_center">
        			<span style="font-weight: bold;">درخواست تحویلخانه</span>
        			<br>
        			<span>فرمایش روغنیات</span><br>
        			<span>ریاست تخنیک و ترانسپورت</span>
        		</div>
        	</td>
        	<td width="33%" style="text-align:right;">
        		<div class="header_left">
        			<span>فورمه ف س ۹</span>
        			<br>
        			۳. اسم شعبه درخواست دهنده:
        			 ({!!getDepartmentName($row->requested_dep)!!})
        			<br>
        			۴. اسم نماینده گی:
        			 ({!!getDepartmentName($row->agency_name)!!})
        		</div>
        	</td>
        </tr>
    </table>
    <br>
    <table width="100%" class="table_body" style="border: 2px solid;" border="1">
    	<tr>
    		<th width="5%">شماره اقلام</th>
    		<th width="5%">شماره ذخیره</th>
    		<th width="65%">تفصیلات جنس - خدمات</th>
    		<th width="5%">مقدار</th>
    		<th width="5%">واحد</th>
    		<th>به حساب معامله شود</th>
    	</tr>
    	<tr>
    		<th>۵</th>
    		<th>۶</th>
    		<th>۷</th>
    		<th>۸</th>
    		<th>۹</th>
    		<th>۱۰</th>
    	</tr>
    	
    	<tr>
    		<td></td>
    		<td style="text-align: center;">
    			<div style="width:100%;height:120px;"></div>
    			<?php $i=1; ?>
    			@foreach($selected_items AS $selected)
    				<li>{!!$i!!}</li>
    				<?php $i++; ?>
    			@endforeach
    			<div style="width:100%;height:120px;"></div>
    		</td>
    		
    		<!--Body Start-->
    		<td>
    			<div style="width:100%;height:120px;">
    				{!!$row->header_description!!}
    			</div>
    			<div id="body_items" style="padding:15px">
    				<?php $i=0; ?>
    				@foreach($selected_items AS $selected)
    				<?php $i++; ?>
        				<div id="body_i_{!!$i!!}">
            				@foreach($items AS $item)
            					@if($selected->item_id == $item->id)
      								<li>{!!$item->item_description!!}</li>
      							@endif
            				@endforeach
        				</div>
    				@endforeach
    				
    			</div>
    			<div style="width:100%;height:120px;">
					{!!$row->footer_description!!}
    			</div>
    		</td>
    		<!-- Body end -->
    		<td style="text-align: center;">
    			<div style="width:100%;height:120px;"></div>
    			<div id="body_amounts" style="padding:15px;display:inline;">
    				<?php $i=0; ?>
    				@foreach($selected_items AS $selected)
    				<?php $i++; ?>
        				<div id="body_a_{!!$i!!}" style="height: 36px">
        					{!!$selected->amount!!}
        				</div>
        			@endforeach
    			</div>
    			<div style="width:100%;height:120px;"></div>
    		</td>
    		<td style="text-align: center;">
    			<div style="width:100%;height:120px;"></div>
    			<div id="body_units" style="padding:15px;display:inline;">
    				<?php $i=0; ?>
    				@foreach($selected_items AS $selected)
    				<?php $i++; ?>
            			<div id="body_u_{!!$i!!}" style="height: 36px">
                				{!!getStaticTable("mesure_units","transport",$selected->unit,true)!!}
            			</div>
            		@endforeach
    			</div>
    			
    			<div style="width:100%;height:120px;"></div>
    		</td>
    		<td style="text-align: center;">
    			<div style="width:100%;height:120px;"></div>
    			<strong>ملاحظه شد!</strong><br>
    			اصولا اجرا شود.
    			<div style="width:100%;height:120px;"></div>
    		</td>
    		
    	</tr>
    </table>
    
    <table width="100%" style="border-bottom: 1px solid;padding: 10px">
    	<tr>
    		<td style="padding: 10px">۱۱. اقلامیکه در آن خط گرفته شده در تحویلخانه موجود نیست خریداری آن درخواست گردد
    		<br>
    		آمر تحویل خانه --------------------
    		<br>
    		تاریخ:
    		</td>
    		<td style="border-right: 1px solid;padding: 10px">
    			۱۲. نمبر تکت توزیع
    			<br>
    			تاریخ:
    			<br>
    			<br>
    			مدیریت عمومی محاسبه جنسی و تحویلخانه ها اصولا اجراات نمایید.
    		</td>
    	</tr>
    </table>


  </body>

