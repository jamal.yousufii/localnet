
<table class="table table-responsive table-bordered" id="list">
	<thead>
		<tr>
            <th>#</th>
            <th>{!!_('driver')!!}</th>
            <th>{!!_('palet_no')!!}</th>
            <th>{!!_('service_area')!!}</th>
            <th>{!!_('item_description')!!}</th>
            <th>{!!_('amount')!!}</th>
            <th>{!!_('unit')!!}</th>
        </tr>
	</thead>
	<tbody>

        <?php $counter = 1;//$rows->firstItem(); ?>
        @foreach($rows AS $item)
            <tr>
            <td>{!!$counter!!}</td>
            <td>{!!$item->driver!!}</td>
            <td>{!!$item->plate!!}</td>
            <td>{!!$item->department!!}</td>
            <td>{!!$item->name!!}</td>
            <td><a href="javascript:void();" onclick="getItemDetails({!!$item->item_id!!})" title="Click to see the full details"><span class="badge badge-success">{!!$item->total!!}</span></a></td>
            <td>{!!$item->unit!!}</td>
            </tr>
            <?php $counter++; ?>
        @endforeach
		
	</tbody>
</table>

<div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
	
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').DataTable({
            "pageLength": 20,
            dom: 'Bfrtip',
            buttons: ['csv','excel','pdf','print']
        });
    });
    
    function getItemDetails(item_id)
    {
    	$.ajax({
			
			url: "{!!URL::route('getTransportFees9ItemDetails')!!}",
			data: $("#by_driver_frm").serialize()+"&item_id="+item_id,
			dataType: "html",
			type: "post",
			beforeSend: function(){

                //$("body").show().css({"opacity": "0.5"});
                $(this).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
				$("#item_details_modal").modal("show");
                $('#item_details').html(response);
            }
		});
    }
</script>


