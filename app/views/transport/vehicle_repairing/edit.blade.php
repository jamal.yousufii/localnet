@extends('layouts.master')

@section('head')
    {!! HTML::style('/vendor/select2/select2.css') !!}

    <title>{!!_('vehicle_repairing_edit')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('vehicle_repairing_edit')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getVehicleRepairingList')!!}" class="btn btn-success">Back</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('updateVehicleRepairing',$row->id)!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("driver")!!}</label>
                                <select name="driver" id="driver" class="form-control" style="width:100%">
                                    <option value="">---</option>
                                    @foreach($drivers AS $item)
                                    	<option {!! ($row->driver==$item->id?"selected":"")!!} value="{!!$item->id!!}">{!!$item->first_name." ".$item->last_name." (".$item->card_no.")"!!}</option>
                                    @endforeach
                                </select> 
                                <span style="color:red;">{!! $errors->first('driver') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("register_no")!!}</label>
                                <input value="{!! $row->register_no !!}" type="text" name="register_no" id="register_no" class="form-control">
                                <span style="color:red;">{!! $errors->first('register_no') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("palet_number")!!}</label>
                                <input value="{!! $row->palet_number !!}" type="text" name="palet_number" id="palet_number" class="form-control">
                                <span style="color:red;">{!! $errors->first('palet_number') !!}</span>
                            </div>
                  
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("date")!!}</label>
                                @if(isMiladiDate())
                                <input value="<?php echo ($row->date != '0000-00-00') ? toGregorian($row->date):''; ?>" type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}">
                            	@else
                            	<input value="<?php echo ($row->date != '0000-00-00') ? dmy_format($row->date):''; ?>" type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}">
                            	@endif
                                <span style="color:red;">{!! $errors->first('date') !!}</span> 
                            </div>
                            <div class="form-group col-xs-12">
                            	<label class="control-label">{!!_("technical_problem_reason")!!}</label>
                                <textarea name="technical_problem_reason" id="technical_problem_reason" class="form-control">{!! $row->technical_problem_reason !!}</textarea>
                            </div>
                            <div class="form-group col-xs-12">
                            	<label class="control-label">{!!_("technical_team_description")!!}</label>
                                <textarea name="technical_team_description" id="technical_team_description" class="form-control">{!! $row->technical_team_description !!}</textarea>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("source")!!}</label>
                                <select name="source" id="source" class="form-control" style="width:100%">
                                    <option value="">---</option>
                                    <option {!! ($row->source==1?"selected":"")!!} value="1">دیپو</option>
                                    <option {!! ($row->source==2?"selected":"")!!} value="2">قراردادی</option>
                                    <option {!! ($row->source==3?"selected":"")!!} value="3">تهیه از بازار (خوش خرید)</option>
                                   
                                </select> 
                                <span style="color:red;">{!! $errors->first('source') !!}</span>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-refresh fa-lg"></i> {!!_('update')!!}</button>
                            </div>
                            
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
<script>
	$("#driver").select2();
</script>
@stop
