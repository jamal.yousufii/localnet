<?php

namespace App\Imports;
use App\models\Excel;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;

class ExcelImport implements ToCollection,ToModel,WithHeadingRow,WithValidation
{
    use Importable;
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row)
    {
        return new Excel([
            'operation_type' => $row['operation_type'], 
            'unit_id'        => $row['unit_id'], 
            'amount'         => $row['amount'], 
            'price'          => $row['price'], 
            'total_price'    => $row['total_price'], 
            'percentage'     => $row['percentage'], 
            'remarks'        => $row['remarks'],   
        ]);
    }

    public function headingRow(): int
    {
        return 8;
    }

    public function rules(): array
    {
        return [
              'operation_type' => 'required',
        ];
    }
}
