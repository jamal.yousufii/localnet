@extends('layouts.master')

@section('head')

    <title>{!!_('trans_vehicle_list')!!}</title>
@stop
@section('content')
<style>
	.search_bar input{
		border-radius: 22px;
		background: lightgoldenrodyellow;
		border: 1px solid #ddd;
		color: orange;
		font-weight: bold;
	}
</style>
<div class="row">
	<div class="col-lg-12">
		<div class="main-box">
			<header class="main-box-header clearfix">
			    <h2>{!!_('trans_vehicle_list')!!}</h2><br>
			    @if(Session::has('success'))
			    <span class='alert alert-success' style="width:400px;">
			        <i class="fa fa-check-circle fa-fw fa-lg"></i>
			        {!!Session::get('success')!!}
			    </span>
			    @elseif(Session::has('fail'))
			    <span class='alert alert-danger' style="width:400px;">
			        <i class="fa fa-times-circle fa-fw fa-lg"></i>
			        {!!Session::get('fail')!!}
			    </span>
			    @endif
			    <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en")
			    	{
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getCreateDeputyVehicle')!!}" class="btn btn-success"><i class="fa fa-plus-circle"></i> {!!_('add_new_vehicle')!!}</a></span>
				
				<div class="search_bar">
					<input type="text" name="search" placeholder="Search by: model,plate,mileage" id="search" class="form-control col-sm-4" />
					&nbsp;<button type="button" onclick="searchVehicle()" class="btn btn-floating btn-success btn-sm" aria-hidden="true"><i class="icon wb-search"></i></button>
				</div>
				
			</header>
			<br>
			<div class="main-box-body clearfix" id="ajax_content">
			</div>
		</div>
	</div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')
<script>
	$( document ).ready(function() {
		$.ajax({
		        url: '{!!URL::route("getDeputyVehiclesData")!!}',
		        data: "&search=0",
		        type: 'post',
		        beforeSend: function(){
		
		            //$("body").show().css({"opacity": "0.5"});
		            $('#ajax_content').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
		        },
		        success: function(response)
		        {
		
		            $('#ajax_content').html(response);
		        }
		    }
		);
});

function searchVehicle(){
	var keyword = document.getElementById("search").value;
	$.ajax({
		        url: '{!!URL::route("getDeputyVehiclesData")!!}',
		        data: "&search=1&keyword="+keyword,
		        type: 'post',
		        beforeSend: function(){
		
		            //$("body").show().css({"opacity": "0.5"});
		            $('#ajax_content').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
		        },
		        success: function(response)
		        {
		
		            $('#ajax_content').html(response);
		        }
		    }
		);
}
</script>
@stop

