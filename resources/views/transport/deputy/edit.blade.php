@extends('layouts.master')

@section('head')
    <title>{!!_('vehicle_edit')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('vehicle_edit')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getDeputyVehicles')!!}" class="btn btn-success">Back</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('updateDeputyVehicle',$row->id)!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('vehicle_category')!!}</label>
                                <select name="vehicle_category" id="vehicle_category" class="form-control">
                                    {!!getStaticTable("vehicle_category","transport",$row->vehicle_category)!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('vehicle_category') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('vehicle_type')!!} </label>
                                <select name="vehicle_type" id="vehicle_type" class="form-control">
                                    {!!getStaticTable("vehicle_type","transport",$row->vehicle_type)!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('vehicle_type') !!}</span>
                            </div>
                        
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('vehicle_model')!!}</label>
                                <input type="text" value="{!!$row->model!!}" name="model" id="model" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('vehicle_mileage')!!}</label>
                                <input type="text" value="{!!$row->mileage!!}" name="mileage" id="mileage" class="form-control">
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('vehicle_plate')!!}</label>
                                <input type="text" value="{!!$row->plate!!}" name="plate" id="plate" class="form-control">
                                <span style="color:red;">{!! $errors->first('plate') !!}</span>
                            </div>
                            
                             <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('color')!!}</label>
                                <input type="text" value="{!!$row->color!!}" name="color" id="color" class="form-control">
                                  
                            </div>
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_('vehicle_status')!!}</label>
                                <select name="status" id="status" class="form-control">
                                   <option value="">---</option>
                                   <option value="1" {!!($row->status == 1?"selected":"")!!}>{!!_('assigned')!!}</option>
                                   <option value="2" {!!($row->status == 2?"selected":"")!!}>{!!_('available')!!}</option>
                                </select>
                            </div>
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">ملاحظات</label>
                                <textarea name="description" id="description" class="form-control">{!!$row->description!!}</textarea>
                            	
                            </div>
                            <div class="form-group col-xs-6">
	                        	<label class="control-label">{!!_('update_car_photo')!!}</label>
	                            <input type="file" name="files[]" id="files" class="form-control">
	                        </div>
	                        
	                        <div class="form-group col-xs-6">
	                        	{!! HTML::image('/documents/transport/deputy/'.$row->photo, 'CAR PHOTO', array('class' => 'img-bordered img-bordered-orange'." ".$dir,'height'=>'250','width'=>'300')) !!}
	                        </div>
                           
                        </div>
                        
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-refresh fa-lg"></i> {!!_('update')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop

