@extends('layouts.master')

@section('head')
    {!! HTML::style('/vendor/select2/select2.css') !!}

    <title>{!!_('fees9_form')!!}</title>
    
<style>
 
 .table_header td{
 	text-align: center;
 }
 .table_body th{
 	text-align: center;
 }
 .select2{
 	width: 350px !important;
 }
 .select2-container{
 	display: inline-block !important;
 }
 .select2-container--default .select2-selection--single{
 	margin: 1px;
 }
</style>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('fees9_form')!!}</h1>
        <?php 
	    	$dir = getLangShortcut();
	    	if($dir == "en"){
	    		$dir = "pull-right";
	    	}
	    	else
	    	{
	    		$dir = "pull-left";
	    	}
	    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getFees9List')!!}" class="btn btn-success">Back</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('updateFees9',$row->id)!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                        <input type="hidden" name="type" value="{!!$type!!}" />
                        <br>
                            <table border="0" width="100%" class="table_header">
                            	<tr>
	                            	<td width="33%">
	                            		<div class="header_right">
	                            			۱. شماره:
	                            			<input type="text" value="{!!$row->number!!}" name="number" id="number" />
	                            			<br>
	                            			۲. تاریخ: 
	                            			@if(isMiladiDate())
			                                <input value="<?php echo ($row->date != '0000-00-00') ? toGregorian($row->date):''; ?>" type="text" name="date" id="date" class="{!!getDatePickerClass()!!}">
			                            	@else
			                            	<input value="<?php echo ($row->date != '0000-00-00') ? dmy_format($row->date):''; ?>" type="text" name="date" id="date" class="{!!getDatePickerClass()!!}">
			                            	@endif
	                            			<br>
	                            			<span style="text-align: right; float: right; margin-right: 85px;">لطفا اجناس ذیل را فراهم نمایند</span>
	                            		</div>
	                            	</td>
	                            	<td width="33%">
	                            		<div class="header_center">
	                            			<strong>درخواست تحویلخانه</strong>
	                            			<br>
	                            			<span>فرمایش روغنیات</span><br>
	                            			<span>ریاست تخنیک و ترانسپورت</span>
	                            		</div>
	                            	</td>
	                            	<td width="33%" style="text-align:right;">
	                            		<div class="header_left">
	                            			<span>فورمه ف س ۹</span>
	                            			<br>
	                            			۳. اسم شعبه درخواست دهنده:
	                            			<select name="requested_dep" id="requested_dep" onchange="bringRelatedSubDepartment('agency_name',this.value)">
	                            				<option value="">---</option>
	                            				@if(getAllDepartments())
	                            					@foreach(getAllDepartments() AS $item)
	                            						<option value="{!!$item->id!!}" {!!($item->id == $row->requested_dep?"selected":"")!!}>{!!$item->name!!}</option>
	                            					@endforeach
	                            				@endif
	                            			</select>
	                            			<br>
	                            			۴. اسم نماینده گی:
	                            			<select name="agency_name" id="agency_name">
	                            				<option value="">---</option>
	                            				@if(getAllDepartments())
	                            					@foreach(getAllDepartments() AS $item)
	                            						<option value="{!!$item->id!!}" {!!($item->id == $row->agency_name?"selected":"")!!}>{!!$item->name!!}</option>
	                            					@endforeach
	                            				@endif
	                            			</select>
	                            		</div>
	                            	</td>
	                            </tr>
                            </table>
                            <br>
                            <table border="1" width="100%" class="table_body">
                            	<tr>
                            		<th width="5%">شماره اقلام</th>
                            		<th width="5%">شماره ذخیره</th>
                            		<th width="50%">تفصیلات جنس - خدمات</th>
                            		<th width="15%">مقدار</th>
                            		<th width="15%">واحد</th>
                            		<th>به حساب معامله شود</th>
                            	</tr>
                            	<tr>
                            		<th>۵</th>
                            		<th>۶</th>
                            		<th>۷</th>
                            		<th>۸</th>
                            		<th>۹</th>
                            		<th>۱۰</th>
                            	</tr>
                            	
                            	<tr>
                            		<td></td>
                            		<td style="text-align: center;">
                            			<div style="width:100%;height:120px;"></div>
                            			<!--
                            			<select name="no">
                            				<option value="">Select</option>
                            				<?php 
                            					for($i=1;$i<=1000;$i++)
                            					{
                            						echo "<option value='".$i."'>".$i."</option>";
                            					}
                            				?>
                            			</select>
                            			-->
                            			<div style="width:100%;height:120px;"></div>
                            		</td>
                            		
                            		<!--Body Start-->
                            		<td>
                            			<div style="width:100%;height:120px;">
                            				<textarea name="header_description" placeholder="Enter some description ..." style="width:100%;height:120px;">{!!$row->header_description!!}</textarea>
                            			</div>
                            			<div id="body_items" style="padding:15px">
                            				<?php $i=0; ?>
                            				@foreach($selected_items AS $selected)
                            				<?php $i++; ?>
	                            				<div id="body_i_{!!$i!!}">
	                            					<label>از دیپو</label><input <?php  echo ($selected->from_stack==1?"checked":"");?> type="radio"  value="1" name="item_status[<?php echo $i-1; ?>]">  
	                            					<label>خریداری</label><input <?php  echo ($selected->from_market==1?"checked":"");?> type="radio"  value="2" name="item_status[<?php echo $i-1; ?>]">
			                            	        <label>قراردادی</label><input <?php  echo ($selected->contracted==1?"checked":"");?> type="radio"  value="3" name="item_status[<?php echo $i-1; ?>]">

			                            			<select name="items[]" style="height: 30px;">
			                            				@foreach($items AS $item)
			                            					@if($selected->item_id == $item->id)
			                      							<option value='{!!$item->id!!}' selected> {!!$item->item_description!!}</option>
			                      							@else
			                      							<option value='{!!$item->id!!}' > {!!$item->item_description!!}</option>
			                      							@endif
			                            				@endforeach
			                            			</select>
			                            			<a href="javascript:void()" class="btn btn-danger btn-xs" onclick="$(this).closest('div').remove();"> - </a>
	                            				</div>
	                            				@foreach($items AS $item)
		                            				<!--<input type="hidden" name="total_amount[]" value="{!!$item->total_token!!}" />-->
		                            			@endforeach
                            				@endforeach
                            				
                            			</div>
                            			<div style="width:100%;height:120px;">
                            				<textarea name="footer_description" placeholder="Enter some description ..." style="width:100%;height:120px;">{!!$row->footer_description!!}</textarea>
                            			</div>
                            		</td>
                            		<!-- Body end -->
                            		<td>
                            			<div style="width:100%;height:120px;"></div>
                            			<div id="body_amounts" style="padding:15px;display:inline;">
                            				<?php $i=0; ?>
                            				@foreach($selected_items AS $selected)
                            				<?php $i++; ?>
	                            				<div id="body_a_{!!$i!!}">
	                            					<input type="text" name="amount[]" value="{!!$selected->amount!!}" style="width: 145px;height: 36px" />
	                            					<a href="javascript:void()" class="btn btn-danger btn-xs" onclick="$(this).closest('div').remove();"> - </a>
	                            				</div>
	                            			@endforeach
                            			</div>
                            			<div style="width:100%;height:120px;"></div>
                            		</td>
                            		<td>
                            			<div style="width:100%;height:120px;"></div>
                            			<div id="body_units" style="padding:15px;display:inline;">
                            				<?php $i=0; ?>
                            				@foreach($selected_items AS $selected)
                            				<?php $i++; ?>
		                            			<div id="body_u_{!!$i!!}">
			                            			<select name="unit[]" style="height: 36px">
			                            				{!!getStaticTable("mesure_units","transport",$selected->unit)!!}
			                            			</select>
			                            			
			                            			<a href="javascript:void()" class="btn btn-danger btn-xs" onclick="$(this).closest('div').remove();"> - </a>
			                            			<a href="javascript:void()" onclick="repeatRow()" class="btn btn-success btn-xs"> + </a>
		                            			</div>
		                            		@endforeach
                            			</div>
                            			
                            			<div style="width:100%;height:120px;"></div>
                            		</td>
                            		<td style="text-align: center;">
                            			<div style="width:100%;height:120px;"></div>
                            			<strong>ملاحظه شد!</strong><br>
                            			اصولا اجرا شود.
                            			<div style="width:100%;height:120px;"></div>
                            		</td>
                            		
                            	</tr>
                            </table>
                            
                            <table width="100%" style="border-bottom: 1px solid;padding: 10px">
                            	<tr>
                            		<td style="padding: 10px">
                            		<label>در وجه</label>
                        			<select name="driver" style="height: 30px;" id="driver">
                        				<option value="">انتخاب دریور</option>
                        				@foreach($drivers AS $item)
                  							<option value='{!!$item->id!!}' {!!($row->driver==$item->id?"selected":"")!!}>{!!$item->first_name." ".$item->last_name." ID CARD: ".$item->card_no!!}</option>
                        				@endforeach
                        			</select>
                        			<label>پلیت نمبر</label>
                        			<select name="vehicle_plate" style="height: 30px;" id="vehicle_plate">
                        				<option value="">انتخاب پلیت</option>
                        				@foreach($vehicles AS $item)
                  							<option value='{!!$item->id!!}' {!!($row->vehicle_plate==$item->id?"selected":"")!!}>{!!$item->palet_no!!}</option>
                        				@endforeach
                        			</select>
                        			
                            			<br>
                            		۱۱. اقلامیکه در آن خط گرفته شده در تحویلخانه موجود نیست خریداری آن درخواست گردد
                            		<br>
                            		آمر تحویل خانه --------------------
                            		<br>
                            		تاریخ:
                            		</td>
                            		<td style="border-right: 1px solid;padding: 10px">
                            			۱۲. نمبر تکت توزیع
                            			<br>
                            			تاریخ:
                            			<br>
                            			<br>
                            			مدیریت عمومی محاسبه جنسی و تحویلخانه ها اصولا اجراات نمایید.
                            		</td>
                            	</tr>
                            </table>
                        </div>
                        <br>
                        <input class="btn btn-primary" type="submit" value="Update" />
                        
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
<script>
	$(document).ready(function(){
		$("#body_items select").select2();
		$("#driver").select2();
		$("#vehicle_plate").select2();
		$("#requested_dep").select2();
		$("#agency_name").select2();
	});
	function repeatRow(){
	
		var el3 = $("#body_u_1").html();
		var el2 = $("#body_a_1").html();
		var el1 = $("#body_i_1").html();
		
		var boxes = $("#body_items").find("select");
		boxes = boxes.length;
		var index = boxes;
		boxes = boxes+1;
		var options = $("#body_i_1 select").html(); 
		
		$("#body_units").append("<div>"+el3+"</div>");
		$("#body_amounts").append("<div>"+el2+"</div>");
		$("#body_items").append("<div id='body_i_"+boxes+"'><label>از دیپو</label><input type='radio'  value='1' name='item_status["+index+"]'>  <label>خریداری</label><input type='radio'  value='2' name='item_status["+index+"]'>  <label>قراردادی</label><input type='radio'  value='3' name='item_status["+index+"]'>  <select name='items[]'>"+options+"</select> <a href='javascript:void()' class='btn btn-danger btn-xs' onclick=\"$(this).closest('div').remove();\"> - </a></div>");
		
		$("#body_i_"+boxes+" select").select2();
		//$("#body_i_1 select").select2();
	}
	
	function removeRepeatRow(el)
	{
		alert($(el).position());
	}
	
	
	
	

	
</script>
@stop
