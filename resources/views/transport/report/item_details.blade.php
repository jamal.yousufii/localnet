<ul class="list-group list-group-bordered">
<?php $counter=1;$total=0;?>
@if($items)
	@foreach($items AS $item)
  		<li class="list-group-item">
  			{!!$counter!!}. {!!$item->name!!}
  			<span class="badge badge-success">{!!$item->amount!!}</span>
  			<span class="label label-warning">{!!$item->date!!}: </span>
  			&nbsp; | &nbsp;<a href="{!!URL::route('getEditFees9',array($item->fees9_id,$item->fees9_type))!!}" target="_blank">{!!_('fees9_details')!!}</a>
  		</li>
  		<?php $counter++;$total+=$item->amount;?>
  	@endforeach
  	<li class="list-group-item" style="border-color: #fff;"><span class="badge badge-success">{!!_('total').": ".$total!!}</span></li>
 @endif
</ul>