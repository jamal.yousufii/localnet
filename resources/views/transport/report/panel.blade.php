@extends('layouts.master')

@section('head')
{!! HTML::style('/vendor/select2/select2.css') !!}
{!! HTML::style('/css/datatable/buttons.dataTables.min.css') !!}
    <title>{!!_('report_panel')!!}</title>
@stop
@section('content')
<style>
	
	.select2{
		width: 100% !important;
	}
</style>

<div class="row">
<div class="col-md-12">
  <!-- Example Tabs In The Panel -->
  <div class="panel nav-tabs-horizontal">
    <div class="panel-heading">
      <h3 class="panel-title">{!!_('report_panel')!!}</h3>
    </div>
    <ul class="nav nav-tabs nav-tabs-line" data-plugin="nav-tabs" role="tablist">
      <li style="" class="active" role="presentation"><a aria-expanded="true" data-toggle="tab" href="#report_by_driver" aria-controls="report_by_driver" role="tab"><i class="fa fa-taxi" aria-hidden="true"></i> {!!_('report_by_driver_plate')!!}</a></li>
      <!--
      <li style="" class="" role="presentation"><a aria-expanded="false" data-toggle="tab" href="#report_by_department" aria-controls="report_by_department" role="tab"><i class="fa fa-sitemap" aria-hidden="true"></i> {!!_('report_by_department')!!}</a></li>
      -->
      
    <li style="transition-duration: 0.5s, 1s; transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1), cubic-bezier(0.4, 0, 0.2, 1); left: 100px; width: 140px;" class="nav-tabs-autoline"></li></ul>
    <div class="panel-body">
      <div class="tab-content">
      
        <div class="tab-pane active" id="report_by_driver" role="tabpanel">
         	<div id="by_driver_form" class="search_form">
         		<form action="" method="post" id="by_driver_frm">
         			<div class="col-sm-3 form-group">
	         			<select name="driver" id="driver" class="form-control">
	         				<option value="">{!!_('select_driver')!!}</option>
	         				@if($drivers)
	         					@foreach($drivers AS $item)
	         						<option value="{!!$item->id!!}">{!!$item->first_name." ".$item->last_name!!}</option>
	         					
	         					@endforeach
	         				@endif
	         			</select>
         			</div>
         			<div class="col-sm-3 form-group">
	         			<select name="plate" id="plate" class="form-control">
	         				<option value="">{!!_('select_plate')!!}</option>
	         				@if($plates)
	         					@foreach($plates AS $item)
	         						<option value="{!!$item->id!!}">{!!$item->palet_no!!}</option>
	         					
	         					@endforeach
	         				@endif
	         			</select>
	         		</div>
	         		<div class="col-sm-3 form-group">
         				<input type="text" name="start_date" id="start_date" placeholder="{!!_('start_date')!!}" class="form-control {!!getDatePickerClass()!!}" />
         			</div>
         			<div class="col-sm-3 form-group">
         				<input type="text" name="end_date" id="end_date" placeholder="{!!_('end_date')!!}" class="form-control {!!getDatePickerClass()!!}" />
         			</div>
         			
         			<div class="col-sm-3 form-group">
         				<select name="department" id="department" class="form-control" onchange="bringRelatedSubDepartment('sub_department',this.value)">
	         				<option value="">{!!_('select_department')!!}</option>
	         				@if(getAllDepartments())
            					@foreach(getAllDepartments() AS $item)
            						<option value="{!!$item->id!!}">{!!$item->name!!}</option>
            					@endforeach
            				@endif
	         			</select>
	         			
         			</div>
         			<div class="col-sm-3 form-group">
         				<select name="sub_department" id="sub_department" class="form-control">
	         				<option value="">{!!_('select_sub_department')!!}</option>
	         				@if(getAllDepartments())
            					@foreach(getAllDepartments() AS $item)
            						<option value="{!!$item->id!!}">{!!$item->name!!}</option>
            					@endforeach
            				@endif
	         			</select>
         			</div>
         			<div class="col-sm-3 form-group">
         			
	         			<select name="category" id="category" class="form-control">
							<option value="">{!!_('Select_Category')!!}</option>
							<option value="1">{!!_('Filter_&_Oil')!!}</option>
							<option value="2">{!!_('Porzajat')!!}</option>
							<option value="3">{!!_('Tires')!!}</option>
							<option value="4">{!!_('vehicle_needs')!!}</option>
							<option value="5">{!!_('oil')!!}</option>
						</select>
         			</div>
         			<div class="col-sm-3 form-group">
         				<a href="javascript:void()" class="btn btn-success" onclick="bringByDriver()"><i class="fa fa-search"></i> {!!_('search')!!}</a>
         			</div>
         		</form>
         	</div>
         	<?php 
		    	$dir = getLangShortcut();
		    	if($dir == "en")
		    	{
		    		$dir = "pull-right";
		    	}
		    	else
		    	{
		    		$dir = "pull-left";
		    	}
		    ?>
		    
         	<br><br><br>
         	<div id="by_driver_result" style="margin-top:45px;">
         	</div>
        </div>
        <!--
        <div class="tab-pane" id="report_by_department" role="tabpanel">
          
        </div>
        -->
      </div>
    </div>
  </div>
  <!-- End Example Tabs In The Panel -->
</div>
</div>

<div class="modal fade modal-primary" id="item_details_modal" aria-hidden="true" aria-labelledby="item_details_modal" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-center" style="width: 800px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title"><i class="fa fa-list fa-lg"></i> {!!_('item_details')!!}</h4>
      </div>
      <div id="item_details">
        
      </div>
    </div>
  </div>
</div>
<div class="md-overlay"></div><!-- the overlay element -->
@stop

@section("footer-scripts")
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
{!! HTML::script('/js/datatable/dataTables.tableTools.min.js')!!}
{!! HTML::script('/js/datatable/dataTables.buttons.min.js')!!}
{!! HTML::script('/js/datatable/jszip.min.js')!!}
{!! HTML::script('/js/datatable/buttons.html5.min.js')!!}
{!! HTML::script('/js/datatable/buttons.print.min.js')!!}
<script>
$(document).ready(function(){
		
	$("#driver").select2();
	$("#plate").select2();
	$("#department").select2();
	$("#sub_department").select2();
	
	

	
});

	function bringByDriver(){
		
		$.ajax({
			
			url: "{!!URL::route('getTransportReportResult')!!}",
			data: $("#by_driver_frm").serialize()+"&section=1",
			dataType: "html",
			type: "post",
			beforeSend: function(){

                //$("body").show().css({"opacity": "0.5"});
                $("#by_driver_result").html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {

				
                $('#by_driver_result').html(response);
                
            }
		});
	}
	function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringRelatedSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
</script>

@stop