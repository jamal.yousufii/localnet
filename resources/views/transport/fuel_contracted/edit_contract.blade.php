@extends('layouts.master')

@section('head')
    <title>{!!_('fuel_contract_edit')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('fuel_contract_edit')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getFuelContractedList')!!}" class="btn btn-success">Back</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('updateFuelContract',$row->id)!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            
                            <div class="form-group col-xs-4">
                            	<label class="control-label">نام شرکت</label>
                                <input value="{!!$row->company!!}" type="text" name="company" id="company" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">درجه ثقلت</label>
                                <input value="{!!$row->gravity!!}" type="text" name="gravity" id="gravity" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">تاریخ</label>
                            	@if(isMiladiDate())
                                <input value="<?php echo ($row->date != '0000-00-00') ? toGregorian($row->date):''; ?>" type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}">
                            	@else
                            	<input value="<?php echo ($row->date != '0000-00-00') ? dmy_format($row->date):''; ?>" type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}">
                            	@endif
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">ساعت</label>
                                <input value="{!!$row->time!!}" type="text" name="time" id="time" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">سریال نمبر</label>
                                <input value="{!!$row->serial_no!!}" type="text" name="serial_no" id="serial_no" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">نمبر عراده جات</label>
                                <input value="{!!$row->vehicle_no!!}" type="text" name="vehicle_no" id="vehicle_no" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">اسم دریور</label>
                                <input value="{!!$row->driver_name!!}" type="text" name="driver_name" id="driver_name" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">اسم پارتی ترانسپورت</label>
                                <input value="{!!$row->party_name!!}" type="text" name="party_name" id="party_name" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">تشریحات مال بارشده</label>
                                <textarea name="description" id="description" class="form-control">{!!$row->description!!}</textarea>
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">وزن اول (کیلو گرام)</label>
                                <input value="{!!$row->first_weight!!}" type="text" name="first_weight" id="first_weight" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">وزن دوم (کیلو گرام)</label>
                                <input value="{!!$row->second_weight!!}" type="text" name="second_weight" id="second_weight" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">وزن خالص (کیلو گرام)</label>
                                <input value="{!!$row->net_weight!!}" type="text" name="net_weight" id="net_weight" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-4">
                            	<label class="control-label">{!!_("fuel_type")!!}</label>
                                <select name="fuel_type" id="fuel_type" class="form-control">
                                    {!!getStaticTable("fuel_types","transport",$row->fuel_type)!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('fuel_type') !!}</span>
                            </div>
                            
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-refresh fa-lg"></i> {!!_('update')!!}</button>
                            </div>
               
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop
