@extends('layouts.master')

@section('head')
    {!! HTML::style('/vendor/select2/select2.css') !!}

    <title>{!!_('daily_fuel_insert')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('daily_fuel_insert')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getDailyFuelList')!!}" class="btn btn-success">Back</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('insertDailyFuel')!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("driver")!!}</label>
                                <select name="driver" id="driver" class="form-control" style="width:100%">
                                    <option value="">---</option>
                                    @foreach($drivers AS $item)
                                    	<option {!! (old('driver')==$item->id?"selected":"")!!} value="{!!$item->id!!}">{!!$item->first_name." ".$item->last_name." (".$item->card_no.")"!!}</option>
                                    @endforeach
                                </select> 
                                <span style="color:red;">{!! $errors->first('driver') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("vehicle_type")!!}</label>
                                <select name="vehicle_type" id="vehicle_type" class="form-control" style="width:100%">
                                    {!!getStaticTable("vehicle_type","transport",old('vehicle_type'))!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('vehicle_type') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("palet_number")!!}</label>
                                <input value="{!! old('palet_number') !!}" type="text" name="palet_number" id="palet_number" class="form-control">
                                <span style="color:red;">{!! $errors->first('palet_number') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("transaction_number")!!}</label>
                                <input value="{!! old('transaction_number') !!}" type="text" name="transaction_number" id="transaction_number" class="form-control">
                                <span style="color:red;">{!! $errors->first('transaction_number') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("department")!!}</label>
                                <input value="{!! old('department') !!}" type="text" name="department" id="department" class="form-control">
                                <span style="color:red;">{!! $errors->first('department') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("amount")!!}</label>
                                <input value="{!! old('amount') !!}" type="text" name="amount" id="amount" class="form-control">
                                <span style="color:red;">{!! $errors->first('amount') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("date")!!}</label>
                                <input value="{!! old('date') !!}" type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}">
                                <span style="color:red;">{!! $errors->first('date') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("fuel_type")!!}</label>
                                <select name="fuel_type" id="fuel_type" class="form-control">
                                    {!!getStaticTable("fuel_types","transport",old('fuel_type'))!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('fuel_type') !!}</span>
                            </div>
                            <div class="form-group col-xs-12">
                            	<label class="control-label">{!!_("description")!!}</label>
                                <textarea name="description" id="description" class="form-control">{!! old('item_description') !!}</textarea>
                                
                            </div>
                            
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('save')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
<script>
	$("#driver").select2();
	$("#vehicle_type").select2();
</script>
@stop
