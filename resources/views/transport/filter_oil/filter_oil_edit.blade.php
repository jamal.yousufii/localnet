@extends('layouts.master')

@section('head')
    <title>{!!_('filter_oil_edit')!!}</title>
    {!! HTML::style('/vendor/select2/select2.css') !!}
@stop
@section('content')
<style>
	.select2{
		width: 100% !important;
	}
</style>
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('filter_oil_edit')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getFilterOilList')!!}" class="btn btn-success">Back</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('updateFilterOil',$row->id)!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                                
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("item_category")!!}</label>
                                <select name="category" id="category" class="form-control">
									<option value="">{!!_('Select_Category')!!}</option>
									<option value="1" {!!($row->category == 1?"selected":"")!!}>{!!_('Filter_&_Oil')!!}</option>
									<option value="2" {!!($row->category == 2?"selected":"")!!}>{!!_('Porzajat')!!}</option>
									<option value="3" {!!($row->category == 3?"selected":"")!!}>{!!_('Tires')!!}</option>
									<option value="4" {!!($row->category == 4?"selected":"")!!}>{!!_('vehicle_needs')!!}</option>
									<option value="5" {!!($row->category == 5?"selected":"")!!}>{!!_('oil')!!}</option>
								</select>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("item_description")!!}</label>
                                <textarea name="item_description" id="item_description" class="form-control">{!! $row->item_description !!}</textarea>
                                <span style="color:red;">{!! $errors->first('item_description') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("properties")!!}</label>
                                <input value="{!! $row->properties !!}" type="text" name="properties" id="properties" class="form-control">
                                <span style="color:red;">{!! $errors->first('properties') !!}</span>  
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("amount")!!}</label>
                                <input value="{!! $row->amount !!}" type="text" name="amount" id="amount" class="form-control">
                                <span style="color:red;">{!! $errors->first('amount') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("unit")!!}</label>
                                <select name="unit" id="unit" class="form-control">
                                    {!!getStaticTable("mesure_units","transport",$row->unit)!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('unit') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("motamid")!!}</label>
                                <select name="motamid" id="motamid" class="form-control">
                                <option value="">---</option>
                                @if($motamids)
                                	@foreach($motamids AS $item)
                                		<option {!!($item->id == $row->motamid?"selected":"")!!} value="{!!$item->id!!}">{!!$item->first_name." ".$item->last_name!!}</option>
                                	@endforeach
                                @endif
                                </select>
                                <span style="color:red;">{!! $errors->first('motamid') !!}</span>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-refresh fa-lg"></i> {!!_('update')!!}</button>
                            </div>
                   
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
<script>
	$(document).ready(function(){
		
		$("#motamid").select2();
		$("#unit").select2();
		
	});
</script>
@stop
