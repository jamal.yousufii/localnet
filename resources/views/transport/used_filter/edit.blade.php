@extends('layouts.master')

@section('head')
    {!! HTML::style('/vendor/select2/select2.css') !!}

    <title>{!!_('used_filter_oil_edit')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('used_filter_oil_edit')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getUsedFilterOilList')!!}" class="btn btn-success">Back</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('updateUsingFilterOil',$row->id)!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("driver")!!}</label>
                                <select name="driver" id="driver" class="form-control" style="width:100%">
                                    <option value="">---</option>
                                    @foreach($drivers AS $item)
                                    	<option {!! ($row->driver == $item->id?"selected":"")!!} value="{!!$item->id!!}">{!!$item->first_name." ".$item->last_name." (".$item->card_no.")"!!}</option>
                                    @endforeach
                                </select> 
                                <span style="color:red;">{!! $errors->first('driver') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("palet_number")!!}</label>
                                <input value="{!! $row->palet_no !!}" type="text" name="palet_number" id="palet_number" class="form-control">
                                <span style="color:red;">{!! $errors->first('palet_number') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("form_no")!!}</label>
                                <input value="{!! $row->form_no !!}" type="text" name="form_no" id="form_no" class="form-control">
                                <span style="color:red;">{!! $errors->first('form_no') !!}</span>
                            </div>
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("reserved_date")!!}</label>
                                @if(isMiladiDate())
                                <input value="<?php echo ($row->date != '0000-00-00') ? toGregorian($row->date):''; ?>" type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}">
                            	@else
                            	<input value="<?php echo ($row->date != '0000-00-00') ? dmy_format($row->date):''; ?>" type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}">
                            	@endif
                                <span style="color:red;">{!! $errors->first('date') !!}</span> 
                            </div>
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("last_convert_date")!!}</label>
                                @if(isMiladiDate())
                                <input value="<?php echo ($row->last_convert_date != '0000-00-00') ? toGregorian($row->last_convert_date):''; ?>" type="text" name="last_convert_date" id="last_convert_date" class="form-control {!!getDatePickerClass()!!}">
                            	@else
                            	<input value="<?php echo ($row->last_convert_date != '0000-00-00') ? dmy_format($row->last_convert_date):''; ?>" type="text" name="last_convert_date" id="last_convert_date" class="form-control {!!getDatePickerClass()!!}">
                            	@endif
                                <span style="color:red;">{!! $errors->first('last_convert_date') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("last_convert_km")!!}</label>
                                <input value="{!!$row->last_convert_km !!}" onblur="getParalelKM()" type="text" name="last_convert_km" id="last_convert_km" class="form-control">
                                <span style="color:red;">{!! $errors->first('last_convert_km') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("current_convert_date")!!}</label>
                                @if(isMiladiDate())
                                <input value="<?php echo ($row->current_convert_date != '0000-00-00') ? toGregorian($row->current_convert_date):''; ?>" type="text" name="current_convert_date" id="current_convert_date" class="form-control {!!getDatePickerClass()!!}">
                            	@else
                            	<input value="<?php echo ($row->current_convert_date != '0000-00-00') ? dmy_format($row->current_convert_date):''; ?>" type="text" name="current_convert_date" id="current_convert_date" class="form-control {!!getDatePickerClass()!!}">
                            	@endif
                                <span style="color:red;">{!! $errors->first('current_convert_date') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("current_convert_km")!!}</label>
                                <input value="{!! $row->current_convert_km !!}" onblur="getParalelKM()" type="text" name="current_convert_km" id="current_convert_km" class="form-control">
                                <span style="color:red;">{!! $errors->first('current_convert_km') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("Parallel_km")!!}</label>
                                <input value="{!! $row->Parallel_km !!}" type="text" name="Parallel_km" id="Parallel_km" class="form-control">
                                <span style="color:red;">{!! $errors->first('Parallel_km') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("kambood_gaech")!!}</label>
                                <input value="{!! $row->kambood_gaech !!}" type="text" name="kambood_gaech" id="kambood_gaech" class="form-control">
                                <span style="color:red;">{!! $errors->first('kambood_gaech') !!}</span>
                            </div>
                            
                        </div>
                        
                        <fieldset><legend>روغنیات و فلترباب</legend>
                        <div class="row" style="background:#eee;border:1px solid #ddd">
                        	@foreach($filter_types AS $item)
	                        	<div class="form-group col-xs-3">
	                            	<label class="control-label">{!!$item->title!!}: <strong>{!!$item->mesure!!}</strong></label>
	                            	<?php
	                            		$exist = array_key_exists ( $item->id, $selectedItems );
	                            	?>
	                            	@if($exist)
	                                <input value = "{!!$selectedItems[$item->id]!!}" type="text" name="item_amount[{!!$item->id!!}]" class="form-control">
	                            	@else
	                            	<input type="text" name="item_amount[{!!$item->id!!}]" class="form-control">
	                            	@endif
	                            </div>
                        	@endforeach
                        </div>
                        </fieldset>
                        <br>
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-refresh fa-lg"></i> {!!_('update')!!}</button>
                            </div>
                           
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
<script>
	$("#driver").select2();
	function getParalelKM(){
		
		var last_km = $("#last_convert_km").val();
		var current_km = $("#current_convert_km").val();
		
		last_km = +last_km;
		current_km = +current_km;
		
		$("#Parallel_km").val(Number(current_km)-Number(last_km));
	}
</script>
@stop
