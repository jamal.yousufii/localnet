@extends('layouts.master')

@section('head')

    <title>{!!_('used_filter_oil_list')!!}</title>
@stop
@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="main-box">
			<header class="main-box-header clearfix">
			    <h2>{!!_('used_filter_oil_list')!!}</h2>
			    @if(Session::has('success'))
			    <span class='alert alert-success' style="width:400px;">
			        <i class="fa fa-check-circle fa-fw fa-lg"></i>
			        {!!Session::get('success')!!}
			    </span>
			    @elseif(Session::has('fail'))
			    <span class='alert alert-danger' style="width:400px;">
			        <i class="fa fa-times-circle fa-fw fa-lg"></i>
			        {!!Session::get('fail')!!}
			    </span>
			    @endif
			    <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en")
			    	{
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getCreateUsingFilterOil')!!}" class="btn btn-success">Add New</a></span>
			</header>
			
			<div class="main-box-body clearfix">
				<div class="table-responsive">
				    <table class="table table-responsive" id='list'>
				        <thead>
				        <tr>
				            <th>#</th>
				            <th>{!!_('driver')!!}</th>
				            <th>{!!_('vehicle_type')!!}</th>
				            <th>{!!_('palet_number')!!}</th>
				       		<th>{!!_('form_no')!!}</th>
							<th>{!!_('date')!!}</th>
				            <th>{!!_('operation')!!}</th>
				        </tr>
				        </thead>
				        <tbody>
				        </tbody>
				    </table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {

                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                //"iDisplayLength": 2,
                "sAjaxSource": "{!!URL::route('getUsedFilterOilData')!!}",
                "aaSorting": [[ 1, "desc" ]],
                
                "language": {
                    "lengthMenu": "{!!_('view')!!} _MENU_ {!!_('record_per_page')!!}",
                    "zeroRecords": "{!!_('record_not_found')!!}",
                    "info": "{!!_('page_view')!!} _PAGE_ {!!_('of')!!} _PAGES_",
                    "infoEmpty": "{!!_('record_not_found')!!}",
                    "search": "{!!_('search')!!}",
                    "infoFiltered": "(filtered {!!_('of')!!} _MAX_ {!!_('total_record')!!})"
                }
            }
        );

    });
</script>
@stop

