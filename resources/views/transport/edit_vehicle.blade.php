@extends('layouts.master')

@section('head')
    <title>{!!_('vehicle_edit')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('vehicle_edit')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getVehicleList')!!}" class="btn btn-success">{!!_('back')!!}</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('updateVehicle',$row->id)!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-xs-3">
                            	<label class="control-label">نو وسایط</label>
                                <select name="vehicle_type" id="vehicle_type" class="form-control">
                                    {!!getStaticTable("vehicle_type","transport",$row->type)!!}
                                </select>
                            </div>
                        
                            <div class="form-group col-xs-3">
                            	<label class="control-label">مودل</label>
                                <input value="{!!$row->model!!}" type="text" name="model" id="model" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">نمبر پلیت</label>
                                <input value="{!!$row->palet_no!!}" type="text" name="palet" id="palet" class="form-control">
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">نمبر انجن</label>
                                <input value="{!!$row->engine_no!!}" type="text" name="engine" id="engine" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">نمبر شاسی</label>
                                <input value="{!!$row->shasi_no!!}" type="text" name="shasi" id="shasi" class="form-control">
                                  
                            </div>
                             <div class="form-group col-xs-3">
                            	<label class="control-label">رنگ</label>
                                <input value="{!!$row->color!!}" type="text" name="color" id="color" class="form-control">
                                  
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">نمبر جواز سیر</label>
                                <input value="{!!$row->licence_no!!}" type="text" name="license" id="license" class="form-control">
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">وضعیت فعلی واسطه</label>
                                <select name="current_status" id="current_status" class="form-control">
                                   <option value="">---</option>
                                   <option value="1" {!!($row->current_status == 1?'selected':'')!!}>فعال</option>
                                   <option value="2" {!!($row->current_status == 2?'selected':'')!!}>غیر فعال</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">نوع مواد سوخت</label>
                                <select name="fuel_type" id="fuel_type" class="form-control">
                                    {!!getStaticTable("fuel_types","transport",$row->fuel_type)!!}
                                </select>
                            </div>
                            
                            <div class="form-group col-xs-3">
                            	<label class="control-label">مصرف مواد سوخت در ۱۰۰ کیلومتر یا در مایل</label>
                                <input value="{!!$row->spend_in_km!!}" type="text" name="spend_per_km" id="spend_per_km" class="form-control">
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">ترمیمات مورد نیاز</label>
                                <input value="{!!$row->needs_repairing!!}" type="text" name="repairing_needs" id="repairing_needs" class="form-control">
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">مبلغ تخمینی برای ترمیم</label>
                                <input value="{!!$row->amount_for_repair!!}" type="text" name="amount_for_repair" id="amount_for_repair" class="form-control">
                            </div>
                            <div class="form-group col-xs-12">
                            	<label class="control-label">ملاحظات</label>
                                <textarea name="description" id="description" class="form-control">{!!$row->description!!}</textarea>
                            	<div class="checkbox checkbox-nice" style="margin:20px;">
                                    <input id="lylam" name="lylam" type="checkbox" {!! ($row->lylam==1?"checked":"")!!} value="1">
                                    <label for="lylam">
                                        {!!_('lylam')!!}
                                    </label>
                                </div>
                            </div>
                            
                            <div class="form-group col-xs-12">
		                        <label class="control-label">{!!_('attachment')!!}</label>
		                        <div id='files_div'>
		                            <input style='width:1200px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
		                        </div>
		                        
		                    </div>
                            
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-refresh fa-lg"></i> {!!_('update')!!}</button>
                            </div>
                           
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
        <div class="col-lg-12" dir="ltr">
            <!-- Example Continuous Accordion -->
            <div class="examle-wrap">
              <div class="example">
                <div class="panel-group panel-group-continuous" id="exampleAccordionContinuous" aria-multiselectable="true" role="tablist">
                 
                  <div class="panel">
                    <div class="panel-heading" id="exampleHeadingContinuousTwo" role="tab" style="text-align: center;background-color: rgb(98, 168, 234);">
                      <a class="panel-title collapsed" style="color: white;font-weight: bold;" data-parent="#exampleAccordionContinuous" data-toggle="collapse" href="#exampleCollapseContinuousTwo" aria-controls="exampleCollapseContinuousTwo" aria-expanded="false">
                      <i class="icon fa-paperclip fa-lg" aria-hidden="true"></i> {!!_("vehicle_attachments")!!}
                      </a>
                    </div>
                    <div style="height: 0px;direction: rtl;" aria-expanded="false" class="panel-collapse collapse" id="exampleCollapseContinuousTwo" aria-labelledby="exampleHeadingContinuousTwo" role="tabpanel">
                      <div class="panel-body" style="border: 1px solid rgb(98, 168, 234);">
                        	<table class="attach_table table table-bordered">
                        		<tr>
                        			<td>#</td>
                        			<td>{!!_("file_name")!!}</td>
                        			<td colspan="2">{!!_("operation")!!}</td>
                        		</tr>
                        		@if($attachments)
                        			<?php $counter = 1; ?>
                        			@foreach($attachments AS $item)
		                        		<tr>
		                        			<td>{!!$counter!!}</td>
		                        			<td><a href="{!!URL::route('getDownloadTransportVehicleDoc',$item->file_name)!!}"><i class="icon fa-paperclip fa-lg" aria-hidden="true"></i> {!!$item->file_name!!}</a></td>
		                        			<td><a href="{!!URL::route('getDeleteTransportVehicleDoc',array($item->file_name,$item->id,$item->record_id))!!}" onclick="return confirm('آیا میخواهید ضمیمه را حذف نمایید؟');"><i class="icon fa-trash fa-lg" aria-hidden="true"></i> {!!_("remove")!!}</a></td>
		                        			<td><a href="{!!URL::route('getDownloadTransportVehicleDoc',$item->file_name)!!}"><i class="icon fa-download fa-lg" aria-hidden="true"></i> {!!_("download")!!}</a></td>
		                        		</tr>
		                        		<?php $counter++; ?>
		                        	@endforeach
		                        @endif
                        	</table>
                        
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
            <!-- End Example Continuous Accordion -->
          </div>
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).on("change","#files",function(){
    var count = $('#files_div').find('input').length;
    count = count+1;
    $("<div id='div_"+count+"' style='display:inline;'><input style='width:1200px;splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:left;margin-top:-37px;' class=\"btn btn-default btn-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div'); 
	
	$("#upload_attachment").prop("disabled",false);
	
});

</script>

@stop
