
<?php 
    
    foreach($comments AS $comment)
    {
?>
        <div class="conversation-item item-left clearfix">
            <div class="conversation-user">
                <?php $photo = getProfilePicture($comment->user_id); ?>
                {!! HTML::image('/img/'.$photo,'',array('class' => 'project-img-owner comment')) !!}
            </div>
            <div class="conversation-body">
                <div class="name">
                    {!!getUserFullName($comment->user_id)!!}
                </div>
                <div class="time hidden-xs">
                    {!!$comment->created_at!!}
                </div>
                <div class="text">
                    {!!nl2br($comment->comment)!!}
                </div>
            </div>
        </div>
<?php 
    }
?>
