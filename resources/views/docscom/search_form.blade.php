@extends('layouts.master')

@section('head')
{!! HTML::style('/css/template/libs/nifty-component.css') !!}

    <title>{!!_('search_document')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">{!!_('dashboard')!!}</a></li>
            <li class="active"><span>{!!_('search_document')!!}</span></li>
        </ol>
        
        <h1>{!!_('search_document')!!}</h1>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <header class="main-box-header clearfix">
                    {!!_('search_document')!!}
                </header>
                
                <div class="main-box-body clearfix">
                    <form id="search_frm" role="form" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group col-xs-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="start_date" type="text" class="form-control {!!getDatePickerClass()!!}" placeholder="تاریخ شروع">
                                </div>
                            </div>
                            <div class="form-group col-xs-4">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="end_date" type="text" class="form-control {!!getDatePickerClass()!!}" placeholder="تاریخ ختم">
                                </div>
                            </div>
                            <div class="form-group col-xs-4">
                                <select name="condition" id="condition" class="form-control">
                                    
                                    <option value="">{!!_('all')!!}</option>
                                    <option value="na">تحت پروسیس</option>
                                    <option value="a">تایید شده</option>
                                    <option value="r">رد شده</option>
                                    <option value="s">حفظ شده</option>
                                    <option value="c">صادر شده</option>
                                    <!-- <option value="nc">صادر ناشده</option> -->

                                </select>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="form-group col-xs-4">
                                <select name="parent_dep" id="parent_dep" class="form-control" onchange="bringRelatedSubDepartment('sub_dep_div',this.value)">
                                    <option value="">{!!_('all')!!}</option>
                                    @foreach($parentDeps AS $dep_item)
                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-xs-4" id="sub_dep_div">
                                <select name="sub_dep" id="sub_dap" class="form-control">
                                    <option value="">{!!_('all')!!}</option>
                                </select>
                            </div>
                            <div class="form-group col-xs-1">
                                <div class="checkbox checkbox-nice">
                                    <input id="app" name="app" type="checkbox" value="1">
                                    <label for="app">
                                        {!!_('application')!!}
                                    </label>
                                </div> 
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <div class="checkbox checkbox-nice">
                                    <input id="document" name="document" type="checkbox" value="2">
                                    <label for="document">
                                        {!!_('document_maktob')!!}
                                    </label>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <a href="#" class="btn btn-primary" onclick="getSearchResult('search_result')"><i class="fa fa-search fa-lg"></i> {!!_('search')!!}</a>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                    <br>
                    <!--Search result Start-->
                    <div id="search_result">
                    
						
                    </div>
                    <!--Search result End-->

                </div>
            </div>
        </div>
    </div>
@stop

@section('footer-scripts')
<script type="text/javascript">
    function getSearchResult(div)
    {
        $.ajax({
                url: '{!!URL::route("getDocSearchResult")!!}',
                data: $('#search_frm').serialize(),
                type: 'post',
                beforeSend: function(){

                    //$("body").show().css({"opacity": "0.5"});
                    $("#"+div).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {

                    $('#'+div).html(response);
                }
            }
        );

        return false;
        
    }
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringRelatedSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
</script>

@stop

