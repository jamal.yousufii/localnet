@extends('layouts.master')

@section('head')
{!! HTML::style('/css/template/libs/nifty-component.css') !!}
{!! HTML::style('/css/conversation.css') !!}
    <title>{!!_('document_details')!!}</title>

@stop


@section('content')
<div class="row" dir="ltr">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">{!!_('dashboard')!!}</a></li>
            <li class="active"><span>{!!_('documents')!!}</span></li>
        </ol>
        
        <h1>{!!_('document_details')!!}</h1>
        @if(isDocumentApproved($doc_id,$table_name))
            <div style="width:30%;display:inline;" class="alert alert-success pull-right">
                <i class="fa fa-check-circle fa-fw fa-lg"></i>
                {!!_('document_final_approved')!!}
            </div>
        @endif
        <table border='0' width="30%">
            <tr>
                <td>
                @if(amIApproved($doc_id,$table_name) && !isDocumentApproved($doc_id,$table_name) && !isDocumentRejected($doc_id,$table_name))
                    <div class="alert alert-warning" style="width:50%;display:inline;padding:10px;">
                        <i class="fa fa-check fa-lg"></i>
                        {!!_('document_approved')!!}
                    </div>
                @endif
                </td>
                <td>
                @if(amIReturned($doc_id,$table_name) && !isDocumentApproved($doc_id,$table_name) && !isDocumentRejected($doc_id,$table_name))
                    <div class="alert alert-warning" style="width:50%;display:inline;padding:10px;">
                        <i class="fa fa-times fa-lg"></i>
                        {!!_('document_rejected')!!}
                    </div>
                @endif
                </td>
            </tr>
        </table>
        <div id="approved_div" style='display:none;'>
            <div class="alert alert-success" style="width:30%;display:inline;">
                <i class="fa fa-check-circle fa-fw fa-lg"></i>
                {!!_('document_final_approved')!!}
            </div>
        </div>
    </div>
</div>

<div class="row" dir="ltr">
    <div class="col-lg-12">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <input type="hidden" id="doc_id" value="{!!$doc_id!!}">
            @if(!isDocumentRejected($doc_id,$table_name) && !isDocumentApproved($doc_id,$table_name))
                
                @if(canFinalApprove('docscom_docscom'))
                    <a href="javascript:void()" class="btn btn-warning" onclick="approveDocument('{!!$doc_id!!}')" id='approve_btn'>
                        <i class="fa fa-check fa-lg"></i> {!!_('final_approve')!!}
                    </a>
                @endif

                @if(!amIApproved($doc_id,$table_name))
                    @if(canApprove('docscom_docscom'))
                        <a href="javascript:void()" class="btn btn-warning" onclick="proceedDocument('{!!$doc_id!!}')" id='proceed_btn'>
                            <i class="fa fa-check fa-lg"></i> {!!_('approve')!!}
                        </a>
                    @endif
                @endif

                @if(!amIReturned($doc_id,$table_name))
                    @if(canReturn('docscom_docscom'))
                        <a href="javascript:void()" data-target="#form_return_modal" data-toggle="modal" class="btn btn-warning" id="return_btn">
                            <i class="fa fa-info fa-lg"></i> {!!_('request_for_info')!!}
                        </a>
                    @endif
                @endif
                
                @if(amIRejected($doc_id,$table_name))
                    <div class="alert alert-danger" style="width:50%;display:inline;">
                        <i class="fa fa-times fa-lg"></i>
                        {!!_('document_rejected')!!}
                    </div>
                @else
                    @if(canReject('docscom_docscom'))
                        <a href="javascript:void()" data-toggle="modal" data-target="#form_reject_modal" class="btn btn-danger" id="reject_btn">
                            <i class="fa fa-times fa-lg"></i> {!!_('reject')!!}
                        </a>
                    @endif
                @endif

            @endif

            @if(isDocSaved($doc_id,$table_name))
                    <div class="alert alert-danger" style="width:80%;display:inline;">
                        <a href="javascript:void();" title="Undo Save" class="btn btn-primary" onclick="javascript:saveDoc('{!!$doc_id!!}',true);" id='unsave_btn'>
                            <i class="fa fa-undo fa-lg"></i>
                        </a>
                        {!!_('document_saved')!!}
                    </div>
            @else

                @if(canApprove('docscom_docscom'))
                    <a href="javascript:void();" class="btn btn-primary" onclick="javascript:saveDoc('{!!$doc_id!!}',false);" id='save_btn'>
                        <i class="fa fa-save fa-lg"></i> {!!_('store')!!}
                    </a>
                @endif
            @endif

            @if(isDocumentRejected($doc_id,$table_name))
            <div style="width:30%;display:inline;" class="alert alert-danger">
                <i class="fa fa-times-circle fa-fw fa-lg"></i>
                {!!_('document_rejected')!!}
            </div>
            @endif

                @if(Session::has('success'))
                    <div class='alert alert-success'>
                        <i class="fa fa-check-circle fa-fw fa-lg"></i>
                        {!!Session::get('success')!!}
                    </div>
                @elseif(Session::has('fail'))
                    <div class='alert alert-danger'>
                        <i class="fa fa-times-circle fa-fw fa-lg"></i>
                        {!!Session::get('fail')!!}
                    </div>
                 @endif

                @if(canApprove('docscom_docscom'))
                <a href="javascript:void()" style="margin-right:10px;margin-left:10px;" class="btn btn-success pull-right" data-toggle="modal" data-target="#form_status_modal">
                    <i class="fa fa-refresh fa-lg"></i> {!!_('document_state')!!}
                </a>
                @endif

                <a href="{!!URL::to('/docscom/getForm')!!}" class="btn btn-success pull-right">
                    <i class="fa fa-chevron-left fa-lg"></i> {!!_('back')!!}
                </a>  

            </header>
            
            <div class="main-box-body clearfix form-horizontal">
            
            @foreach($records AS $item)
                
                @if(canEdit('docscom_docscom'))
                <form class="form-horizontal" role="form" method="post" action="{!!URL::route('docsPostUpdateForm',array($table_name,$item->id))!!}" enctype="multipart/form-data">
                @endif

                @if($table_name == "docs")
                <!-- original form start -->
                <fieldset>
                    <legend style="padding-bottom:8px;">{!!_('source_organization')!!}
                        
                        
                    </legend>

                    <div class="row">
                        <input type = "hidden" name="form_id" id="form_id" value="{!!$item->id!!}" >
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('organization_name')!!} :</label>
                            <div class="col-sm-4">
                                <input value="{!!$item->organization!!}" class="form-control" type="text" name="organization">
                            </div>
                            <label class="col-sm-2 control-label">{!!_('sadira_number')!!} :</label>
                            <div class="col-sm-4">
                                <input value="{!!$item->external_number!!}" class="form-control" type="text" name="external_number">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('sadira_date')!!} :</label>
                            <div class="col-sm-4">
                            	@if(isMiladiDate())
                                <input value="<?php echo ($item->external_date != '0000-00-00') ? toGregorian($item->external_date):''; ?>" class="form-control datepicker" type="text" name="external_date">
                                @else
                                <input value="<?php echo ($item->external_date != '0000-00-00') ? dmy_format($item->external_date):''; ?>" class="form-control datepicker_farsi" type="text" name="external_date">
                                @endif
                            </div>
                            
                        </div>
                    </div>
                    
                </fieldset>

                <fieldset><legend>{!!_('document_and_communication')!!}</legend>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('checkin_number')!!} :</label>
                            <div class="col-sm-4">
                                <input value="{!!$item->internal_number!!}" class="form-control" type="text" name="internal_number">
                            </div>
                            <label class="col-sm-2 control-label">{!!_('checkin_date')!!} :</label>
                            <div class="col-sm-4">
                                @if(isMiladiDate())
                                <input class="form-control datepicker" value="<?php echo ($item->internal_date != '0000-00-00') ? toGregorian($item->internal_date):''; ?>" type="text" name="internal_date">
                                @else
                                <input value="<?php echo ($item->internal_date != '0000-00-00') ? dmy_format($item->internal_date):''; ?>" class="form-control datepicker_farsi" type="text" name="internal_date">
                            	@endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('summary')!!} :</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="5" id="wysiwig_full" name="summary">{!!$item->doc_summary!!}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('executive_department')!!} :</label>
                            <div class="col-sm-10">
                                
                                <select class="form-control" name="executive_department" id="executive_department" onchange='changeDepartment(this.value,"{!!$item->id!!}")' <?php echo ((canChangeDepartment('docscom_docscom') || canEdit('docscom_docscom')) ? '':'disabled'); ?>>
                                        
                                        @foreach($departments AS $dep)
                                            <option <?php if($item->executive_department == $dep->id){echo 'selected';} ?> value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                                        @endforeach
                                    
                                </select>
                                <div id="changed_department_div"></div>
                            </div>
                        
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{!!_('document_type')!!}:</label>
                        <div class="col-sm-4">
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <div class="radio">
                                            <input <?php if($item->doc_type == 1){echo "checked";} ?> value='1' id='application' name="doc_type" type="radio">

                                        <label for="application">
                                            {!!_('application')!!}
                                        </label>
                                    </div>
                                    <div class="radio">
                                        
                                            <input <?php if($item->doc_type == 2){echo "checked";} ?> value='2' name="doc_type" id='document' type="radio">
                                            
                                        <label for="document">
                                            {!!_('document_maktob')!!}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <label class="col-sm-2 control-label">{!!_('confidentiality')!!} :</label>
                        <div class="col-sm-4">

                            <div class="row form-group">
                                
                                <div class="col-sm-10">
                                    @if($item->access_type==3 || $item->access_type==1)
                                    <div class="radio">
                                        <input <?php if($item->access_type == 3){echo "checked";} ?> value='3' type="radio" id="other" name="access">
                                        <label for="other">
                                            {!!_('other')!!}
                                        </label>
                                    </div>
                                    
                                    <div class="radio">
                                            <input <?php if($item->access_type == 1){echo "checked";} ?> value='1' id='confidential' name="access" type="radio">

                                        <label for="confidential">
                                            {!!_('confidential')!!}
                                        </label>
                                    </div>
                                    @elseif($item->access_type==2)
                                    <div class="radio">
                                        
                                            <input <?php if($item->access_type == 2){echo "checked";} ?> value='2' name="access" id='non_confidential' type="radio">
                                            
                                        <label for="non_confidential">
                                            {!!_('non_confidentail')!!}
                                        </label>
                                    </div>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </fieldset>

                @endif
                <!-- original form end -->

                @if($table_name == 'aop_docs')
                <!-- AOP form started -->
                <fieldset>
                    <legend style="padding-bottom:8px;">{!!_('aop_documents')!!}
                    </legend>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-4">
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <div class="radio">
                                                <input <?php if($item->doc_type == 1){echo "checked";} ?> value='1' id='application' name="doc_type" type="radio">

                                            <label for="application">
                                                {!!_('application')!!}
                                            </label>
                                        </div>
                                        <div class="radio">
                                            
                                                <input <?php if($item->doc_type == 2){echo "checked";} ?> value='2' name="doc_type" id='document' type="radio">
                                                
                                            <label for="document">
                                                {!!_('document_maktob')!!}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="col-sm-4">
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <div class="radio">
                                                <input <?php if($item->access_type == 1){echo "checked";} ?> value='1' id='confidential' name="access" type="radio">

                                            <label for="confidential">
                                                محرم
                                            </label>
                                        </div>
                                        <div class="radio">
                                            
                                                <input <?php if($item->access_type == 2){echo "checked";} ?> value='2' name="access" id='non_confidential' type="radio">
                                                
                                            <label for="non_confidential">
                                                غیر محرم
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                        </div>
                    </div>
                    
                    <div class="row">
                        <input type = "hidden" name="form_id" id="form_id" value="{!!$item->id!!}" >
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('number')!!}:</label>
                            <div class="col-sm-4">
                                <input value="{!!$item->number!!}" class="form-control" type="text" name="aop_number">
                            </div>
                            <label class="col-sm-2 control-label">{!!_('date')!!} :</label>
                            <div class="col-sm-4">
                            	@if(isMiladiDate())
                            	<input value="<?php echo ($item->date != '0000-00-00') ? toGregorian($item->date):''; ?>" class="form-control datepicker" type="text" name="aop_date">
                            	@else
                                <input value="{!!dmy_format($item->date)!!}" class="form-control datepicker_farsi" type="text" name="aop_date">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('applicant_department')!!} :</label>
                            <div class="col-sm-10">
                                
                                <select class="form-control" name="aop_app_department" id="aop_app_department">
                                    <option value=''>{!!_('select')!!}</option>             
                                    @foreach($departments AS $dep)
                                        <option <?php if($item->applicant_department == $dep->id){echo 'selected';} ?> value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('application_subject')!!}:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="6" id="wysiwig_full" name="aop_app_subject">{!!$item->applicantion_subject!!}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('executive_department')!!}:</label>
                            <div class="col-sm-10">
                                <!-- <select class="form-control" name="aop_executive_department" id="aop_executive_department"> -->
                                <select class="form-control" name="aop_executive_department" id="aop_executive_department" onchange='changeDepartment(this.value,"{!!$item->id!!}")' <?php echo ((canChangeDepartment('docscom_docscom') || canEdit('docscom_docscom')) ? '':'disabled'); ?>>
                                    <option value=''>{!!_('select')!!}</option>
                                    @foreach($departments AS $dep)
                                    <option <?php if($item->executive_department == $dep->id){echo 'selected';} ?> value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                                    @endforeach
                                </select>
                                <div id="changed_department_div"></div>

                            </div>
                        </div>
                    </div>
                </fieldset>
                <!-- AOP form ended -->
                @endif
                
                @if($table_name == 'docs_answers')
                <!-- Doc answers form started -->
                <fieldset>
                    <legend style="padding-bottom:8px;">{!!_('checkouted_document')!!}
                    </legend>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-4">
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <div class="radio">
                                                <input <?php if($item->doc_type == 1){echo "checked";} ?> value='1' id='application' name="doc_type" type="radio">

                                            <label for="application">
                                                {!!_('application')!!}
                                            </label>
                                        </div>
                                        <div class="radio">
                                            
                                                <input <?php if($item->doc_type == 2){echo "checked";} ?> value='2' name="doc_type" id='document' type="radio">
                                                
                                            <label for="document">
                                                {!!_('document_maktob')!!}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('source_organization')!!}:</label>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="doc_source" value="{!!$item->source_organization!!}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('summary')!!}:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="6" id="wysiwig_full" name="doc_summary">{!!$item->doc_summary!!}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('executive_department')!!} :</label>
                            <div class="col-sm-6">
                                <!-- <select class="form-control" name="doc_executive_department" id="doc_executive_department"> -->
                                <select class="form-control" name="doc_executive_department" id="doc_executive_department" onchange='changeDepartment(this.value,"{!!$item->id!!}")' <?php echo ((canChangeDepartment('docscom_docscom') || canEdit('docscom_docscom')) ? '':'disabled'); ?>>
                                    <option value=''>{!!_('select')!!}</option>
                                    @foreach($departments AS $dep)
                                    <option <?php if($item->executive_department == $dep->id){echo 'selected';} ?> value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                                    @endforeach
                                </select>
                                <div id="changed_department_div"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('checkouted_date')!!} :</label>
                            <div class="col-sm-6">
                            	@if(isMiladiDate())
                            	<input value="{!!toGregorian($item->date)!!}" class="form-control datepicker" type="text" name="doc_external_date">
                            	@else
                                <input value="{!!dmy_format($item->date)!!}" class="form-control datepicker_farsi" type="text" name="doc_external_date">
                            	@endif
                            </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('is_answer')!!}؟</label>
                            <div class="col-sm-4">
                                <div class="row form-group">
                                    <div class="col-sm-10">
                                        <div class="radio">
                                            <input <?php echo ($item->is_answer = 1 ? 'checked':""); ?> value='1' type="radio" id="doc_ans_yes" name="doc_answer">
                                            <label for="doc_ans_yes">
                                                {!!_('yes')!!}
                                            </label>
                                            <input value="{!!$item->form_number!!}" placeholder="شماره سند مربوطه ..." type="text" name="doc_yes" id="doc_yes" style="display: <?php echo ($item->is_answer = 1 ? 'inline':'none') ?>;" />
                                        </div>
                                        <div class="radio">
                                            <input <?php echo ($item->is_answer = 0 ? 'checked':""); ?> value='0' type="radio" id="doc_ans_no" name="doc_answer">
                                            <label for="doc_ans_no">
                                                {!!_('no')!!}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </fieldset>
                <!-- Doc answers form ended -->
                @endif

                <fieldset>
                    <legend>{!!_('attachments')!!}</legend>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="main-box clearfix">
                            <header class="main-box-header clearfix">
                                <h2>
                                    <i class="fa fa-upload fa-fw fa-lg" style='color:#03a9f4;'></i>
                                    {!!_('uploaded_attachments')!!}
                                </h2>
                            </header>
                            
                            <div class="main-box-body clearfix">
                                <input type="hidden" id="table_name" value="{!!$table_name!!}" />
                                <ul class="widget-todo">
                                    @if(count($attachments)>0)
                                        @foreach($attachments AS $attach)
                                        <li class="clearfix" id="li_{!!$attach->id!!}">
                                            <div class="name">
                                                <label for="todo-2">
                                                    <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
                                                    <strong>{!!$attach->file_name!!} @if($attach->sadira == 1) <font color="red">&nbsp; > ({!!_('checkouted')!!})</font>@endif</strong>
                                                </label>
                                            </div>
                                            <div class="actions">
                                                @if(canEdit('docscom_docscom'))
                                                <a href="{!!URL::route('getDocscomDownload',array($table_name,$attach->id))!!}" class="table-link success">
                                                    <i class="fa fa-cloud-download" style='color:#03a9f4;'></i>
                                                </a>
                                                
                                                <a href="javascript:void()" onclick="removeDocscomFile('{!!$attach->id!!}','li_{!!$attach->id!!}')" class="table-link danger">
                                                    <i class="fa fa-trash-o" style='color:red;'></i>
                                                </a>
                                                @endif
                                            </div>
                                        </li>
                                        @endforeach
                                    @else
                                        <li><span style='color:red;'>{!!_('no_attachment')!!}!</span></li>
                                    @endif
                                </ul>
                                    
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-1 control-label">{!!_('new_attachment')!!}</label>
                        <div class="col-sm-3" id='files_div' style='width:465px;'>
                            <input style='width:400px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
                        </div>
                        
                    </div>
                </fieldset>

                @if(canEdit('docscom_docscom'))
                <div class="form-group">
                    <label class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-sm-10">
                        <button class="btn btn-primary" type="submit">{!!_('edit_document')!!}</button>
                    </div>
                </div>
                @endif

                </form>

            @endforeach
            </div>
            @if($answers)
                <div class="main-box-body clearfix" dir="rtl">
                    <div class="panel-group accordion" id="accordion">
                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        {!!_('form_answers')!!}
                                    </a>
                                </h4>
                            </div>
                            <div style="height: 2px;" id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul id="search-results">
                                   @foreach($answers AS $item)
                                        <li>
                                            <h3 class="title">
                                                <a href="{!!URL::route('getDocDetails',array('docs_answers',$item->id))!!}">
                                                    {!!_('from_department')!!}: {!!$item->executive_department!!}
                                                </a>
                                            </h3>
                                            <div class="desc">
                                                <div style="display:inline-block;padding-left:32px;">
                                                    {!!nl2br($item->doc_summary)!!}
                                                </div>

                                                <a style="font-size:12px;" class="btn btn-warning" href="{!!URL::route('getDocDetails',array('docs_answers',$item->id))!!}">
                                                    <i class="fa fa-eye fa-lg"></i>
                                                     {!!_('more_details')!!}
                                                </a>

                                            </div>
                                        </li>
                                   @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

<div class="row" id="row_comments">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-box clearfix">
                    <header class="main-box-header clearfix">
                        <h2 class="pull-left value red">{!!_('comments')!!}</h2>
                    </header>
                    <div class="main-box-body clearfix">
                        <div class="conversation-wrapper">
                            <div class="conversation-content" id="comments_div">
                                <?php 
                                    
                                    foreach($comments AS $comment)
                                    {
                                ?>
                                        <div class="conversation-item item-left clearfix">
                                            <div class="conversation-user">
                                            	<a href="#" class="avatar">
                                                <?php $photo = getProfilePicture(0,true); ?>
                                                {!! HTML::image('/documents/profile_pictures/'.$photo,'',array('class' => 'img-responsive')) !!}
                                            	</a>
                                            </div>
                                            <div class="conversation-body">
                                                <div class="name">
                                                    {!!getUserFullName($comment->user_id)!!}
                                                </div>
                                                <div class="time hidden-xs">
                                                	<?php
	                                                    if(isMiladiDate())
	                                                    {
	                                                    	echo $comment->created_at;
	                                                    }
	                                                    else
	                                                    {
	                                                    	echo toJalali($comment->created_at).' '.substr($comment->created_at,11);
	                                                    }
                                    				?>

                                                </div>
                                                <div class="text">
                                                    {!!$comment->comment!!}@if($comment->old_type==2) <-> (<span style="font-weight:bold;color:red;">{!!_('reason_for_reject')!!}</span>)@endif
                                                </div>
                                            </div>
                                        </div>
                                <?php 
                                    }
                                ?>
                            </div>
                            <br><br>
                            @if(canComment('docscom_docscom'))
                            <div class="conversation-new-message">
                                <form class="comment_form" id="comment_form">
                                    <input type="hidden" name="doc_id" id="doc_id" value="{!!$doc_id!!}">
                                    <input type="hidden" name="table_name" value="{!!$table_name!!}">
                                    <div class="form-group">
                                        <textarea name="comment" id="comment" class="form-control" rows="2" placeholder="نظر جدید"></textarea>
                                    </div>
                                    
                                    <div class="clearfix">
                                        <button type="button" class="btn btn-success pull-right" onclick='postDocComment()'>{!!_('save_comment')!!}</button>
                                    </div>
                                </form>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-fade-in-scale-up" id="form_status_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog" style="width: 90%">
    	<div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        <span aria-hidden="true">×</span>
		    </button>
            <h4 class="modal-title">{!!_('document_process_state')!!}</h4>
        </div>
        <div class="modal-body">
            
            <div class="alert alert-success">
                <!-- <i class="fa fa-warning fa-fw fa-lg"></i> -->
                @foreach($records AS $item)
                <table width="100%" style="font-size:12px;">
                    <tr >
                        <th style="padding-bottom:8px;"><i class="fa fa-clock-o"></i> {!!_('checkouted_date')!!}:</th><td><?php if(isMiladiDate() && $item->exported_date!='0000-00-00 00:00:00'){ echo toGregorian($item->exported_date);}else{echo $item->exported_date;} ?></td>
                        <th style="padding-bottom:8px;"><i class="fa fa-check"></i> {!!_('checkouted_number')!!}:</th><td>{!!$item->exported_number!!}</td>
                        
                    </tr>
                    <tr>
                        <th><i class="fa fa-user"></i> {!!_('checkouted_by')!!}:</th><td>{!!getUserFullName($item->exported_by)!!}</td>
                        <th> <i class="fa fa-user"></i> {!!_('approved_by')!!}:</th><td>{!!getUserFullName($item->approved_by)!!}</td>
                    </tr>

                </table>
                @endforeach
                
            </div>

            <div id="pop_content2">
                <table class='table table-bordered' style="font-size:13px;">
                    <tr>
                        <th>#</th>
                        <th>{!!_('department')!!}</th>
                        <th>{!!_('checkin_date')!!}</th>
                        <th>{!!_('checkin_by')!!}</th>
                        <th>{!!_('issued_date')!!}</th>
                        <!-- <th>اجرا کننده</th> -->
                        <th>{!!_('status')!!}</th>
                    </tr>
                    <?php $counter = 1; ?>
                    @foreach($logs AS $log)
                        <tr>
                            <td>{!!$counter!!}</td>
                            <td>{!!$log->department!!}</td>
                            <td>
                            	
                            	<?php 
                            		if($log->check_in=='0000-00-00 00:00:00')
                            		{
                            			echo "--";
                            			
                            		}
                            		else
                            		{ 
                            			if(isMiladiDate())
                            			{
                            				echo $log->check_in;
                            			}
                            			else
                            			{
                            				echo toJalali($log->check_in).' '.substr($log->check_in,11);
                            			}
                            			
                            			
                            		} 
                            	?>
                            </td>
                            <td>{!!$log->checked_in_by!!}</td>
                            <td align="center">
                            	<?php 
                            		if($log->check_out=='0000-00-00 00:00:00')
                            		{
                            			echo "--";
                            			
                            		}
                            		else
                            		{ 
                            			if(isMiladiDate())
                            			{
                            				echo $log->check_out;
                            			}
                            			else
                            			{
                            				echo toJalali($log->check_out).' '.substr($log->check_out,11);;
                            			}
                            			
                            			
                            		} 
                            	?>
                            </td>
                            <!-- <td align="center">
                                <?php if($log->checked_out_by==''){echo "--";}else{ echo $log->checked_out_by;} ?>
                            </td> -->
                            <td class="text-center">
                                <?php 
                                    if($log->status == 1)
                                    {
                                        echo '<span title="Pending in this department..." class="label label-warning"><i class="fa fa-refresh fa-lg pending-icon"></i></span>';   
                                    }
                                    else
                                    {
                                        echo '<span title="Completed from this department" class="label label-success"><i class="fa fa-check fa-lg"></i></span>';
                                        
                                    }
                                ?>
                            </td>
                            
                        </tr>
                        <?php $counter++; ?>
                    @endforeach

                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button onclick='$("#close_icon").trigger("click");' type="button" class="btn btn-warning" data-dismiss="modal">
                <i class="fa fa-check-circle fa-lg"></i> {!!_('close')!!}</button>
            
        </div>
       </div>
    </div>
</div>

<div class="modal fade modal-fade-in-scale-up" id="form_reject_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog">
    	<div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			        <span aria-hidden="true">×</span>
			    </button>
                <h4 class="modal-title">{!!_('reason_for_reject')!!}</h4>
            </div>
            <div class="modal-body">
	            <form class="comment_form" method="post" action="{!!URL::route('rejectDocument')!!}" class="form-horizontal">
	                <div class="modal-body">
	                    <input type="hidden" name="table_name" value="{!!$table_name!!}">
	                    <input type="hidden" name="type2" id="type2" value="2">
	                    <input type="hidden" name="doc_id2" id="doc_id2" value="{!!$doc_id!!}">
	                    <input type="hidden" name="notify_to2" id="notify_to2" value="{!!$checked_in_by!!}">
	                    <div class="form-group">
	                        <textarea name="comment2" id="comment2" class="form-control" rows="4" placeholder="نظر"></textarea>
	                    </div>
	                </div>
	                <div class="modal-footer">
	                    <button type="submit" onclick='$("#close_icon").trigger("click");' type="button" class="btn btn-danger">
	                        <i class="fa fa-times fa-lg"></i> {!!_('reject')!!}</button>
	                </div>
	            </form>
          	</div>
        </div>
    </div>
</div>


<div class="modal fade modal-fade-in-scale-up" id="form_return_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog">
    	<div class="modal-content">
            <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			        <span aria-hidden="true">×</span>
			    </button>
                <h4 class="modal-title">{!!_('reason_for_return')!!}</h4>
            </div>
            <form class="comment_form" method="post" action="{!!URL::route('rejectDocument')!!}" class="form-horizontal">
            <div class="modal-body">
                <input type="hidden" name="table_name" value="{!!$table_name!!}">
                <input type="hidden" name="type1" id="type1" value="1">
                <input type="hidden" name="doc_id1" id="doc_id1" value="{!!$doc_id!!}">
                <input type="hidden" name="notify_to1" id="notify_to1" value="{!!$checked_in_by!!}">
                <div class="form-group">
                    <textarea name="comment1" id="comment1" class="form-control" rows="4" placeholder="نظر ..."></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" onclick='$("#close_icon").trigger("click");' type="button" class="btn btn-warning">
                    <i class="fa fa-times fa-lg"></i> {!!_('return')!!}</button>
            </div>
        </form>
    </div>
    </div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div>
<!-- the overlay element -->
@stop

@section('footer-scripts')
<script type="text/javascript">
// docscom js functions
var table_name = $('#table_name').val();

//change dapartment
function changeDepartment(dep,doc_id)
{

    var confirmed = confirm("اداره اجرا کننده تغییر یابد؟");
    if(confirmed)
    {
       
        $.ajax({
                url: '{!!URL::to("/docscom/changeDepartment")!!}',
                data: '&dep='+dep+'&doc_id='+doc_id+'&table_name='+table_name+'&from_dep={!!$item->executive_department!!}',
                type: 'post',
                dataType:'json',
                beforeSend: function()
                { 
                    $("#changed_department_div").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {

                    $('#changed_department_div').html(response.msg);
                }
            }
        );
    }

}

function approveDocument(doc_id)
{
    var confirmed = confirm("آیا میخواهید این سند را تایید نمایید؟");
    if(confirmed)
    {
       
        $.ajax({
                url: '{!!URL::route("approveDocument")!!}',
                data: '&doc_id='+doc_id+'&approve=1'+'&table_name='+table_name,
                type: 'post',
                dataType:'json',
                // beforeSend: function(){
                //     $("#approved_div").show();
                //     //$("#entry_details").html("LOADING...");
                //     $("#approved_div").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                // },
                success: function(response)
                {
                    if(response.cond == 'true')
                    {
                        $("#approve_btn").hide();
                        $("#reject_btn").hide();
                        $("#approved_div").show();
                    }

                }
            }
        );
    }
}


function saveDoc(doc_id,undo)
{
    if(undo)
    {
        var confirmed = confirm("آیا میخواهید سند را از حالت حفظ خارج نمایید؟");
        undo = 'undo';
    }
    else
    {
        var confirmed = confirm("آیا میخواهید سند را حفظ نمایید؟");
        undo = '';
    }

    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("approveDocument")!!}',
                data: '&doc_id='+doc_id+'&save=1'+'&table_name='+table_name+'&undo='+undo,
                type: 'post',
                dataType:'json',
                // beforeSend: function(){
                //     $("#approved_div").show();
                //     //$("#entry_details").html("LOADING...");
                //     $("#approved_div").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                // },
                success: function(response)
                {
                    window.location.reload();
                }
            }
        );
    }
}

function proceedDocument(doc_id)
{
    var confirmed = confirm("Do you want to process this document?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("approveDocument")!!}',
                data: '&doc_id='+doc_id+'&proceed=1'+'&table_name='+table_name,
                type: 'post',
                dataType:'json',
                // beforeSend: function(){
                //     $("#approved_div").show();
                //     //$("#entry_details").html("LOADING...");
                //     $("#approved_div").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                // },
                success: function(response)
                {   window.location.reload();
                    if(response.cond == 'true')
                    {
                        $("#proceed_btn").hide();
                        //$("#reject_btn").hide();
                        $("#approved_div").show();
                    }

                }
            }
        );
    }

}
function removeDocscomFile(doc_id,div)
{
    var confirmed = confirm("Do you want to remove this file?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeDocscomFile")!!}',
                data: '&doc_id='+doc_id+'&table_name='+table_name,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }

}

function postDocComment()
{   
    //var formId = $('#form_id').val();
    //$('#doc_id').val(formId);

    $.ajax({
        url     : "{!!URL::route('postDocComment')!!}",
        type    : "post",
        data    : $("#comment_form").serialize(),
        success : function(result){
            $('#comments_div').append(result);
            $('#comment').val('');
        },
        error  : function( xhr, err ){
            alert(err);     
        }
    });    
    return false;
}
</script>
{!! HTML::script('/js/template/modernizr.custom.js')!!}
{!! HTML::script('/js/template/classie.js')!!}
{!! HTML::script('/js/template/modalEffects.js')!!}
<script type="text/javascript">
$(document).on("change","#files",function(){
    var count = $('#files_div').find('input').length;
    count = count+1;
    $("<div id='div_"+count+"' style='display:inline;'><input style='width:400px;splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div'); 
});

$(document).ready(function(){

    //document.getElementById ("unsave_btn").addEventListener ("click", saveDoc(1), false);
});
</script>
<script type="text/javascript">



</script>
@stop

