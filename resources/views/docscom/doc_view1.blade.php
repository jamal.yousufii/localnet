@extends('layouts.master')

@section('head')
{{ HTML::style('/css/template/libs/nifty-component.css') }}
    <title>Documents by barcode</title>
@stop

<style type="text/css">
.project-img-owner {
border-radius: 50%;
background-clip: padding-box;
display: block;
float: left;
height: 40px;
padding: 3px;
overflow: hidden;
width: 40px;
}
img {
vertical-align: middle;
}
img {
border: 0;
}
.pending-icon{
    -webkit-animation: rotating 3s linear infinite;
}
</style>
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active"><span>Documents Details</span></li>
        </ol>
        
        <h1>Document<small>Details</small></h1>
    </div>
</div>

<!-- All modals added here for the demo. You would of course just have one, dynamically created -->
	<div class="md-modal md-effect-1" id="modal-1">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-2" id="modal-2">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-3" id="modal-3">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-4" id="modal-4">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-5" id="modal-5">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-6" id="modal-6">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-7" id="modal-7">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-8" id="modal-8">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-9" id="modal-9">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-10" id="modal-10">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-11" id="modal-11">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-13" id="modal-13">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-14" id="modal-14">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	<div class="md-modal md-effect-15" id="modal-15">
		<div class="md-content">
			<div class="modal-header">
				<button class="md-close close">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				<form role="form">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleTextarea">Textarea</label>
						<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
					</div>
					
					<div class="form-inline form-inline-box">
						<div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<select class="form-control">
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
	
	<!-- Standard Bootstrap Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body">
					<form role="form">
						<div class="form-group">
							<label for="exampleInputEmail1">Email address</label>
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
						</div>
						<div class="form-group">
							<label for="exampleTextarea">Textarea</label>
							<textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
						</div>
						
						<div class="form-inline form-inline-box">
							<div class="form-group">
								<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
							</div>
							<div class="form-group">
								<select class="form-control">
									<option>Active</option>
									<option>Inactive</option>
								</select>
							</div>
							<button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
						</div>
					</form>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
		
	<div class="row">
		<div class="col-lg-12">
			<div class="main-box clearfix">
				<header class="main-box-header clearfix">
					<h2>Nifty Modal with Effects</h2>
				</header>
				
				<div class="main-box-body clearfix">
					<p>
						Some inspiration for different modal window appearances. There are many possibilities for modal overlays to appear. 
						Here are some modern ways of showing them using CSS transitions and animations.
					</p>
					<a data-toggle="modal" href="#myModal" class="btn btn-primary mrg-b-lg">Standard Boostrap Modal</a>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-1">Fade in &amp; Scale</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-2">Slide in (right)</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-3">Slide in (bottom)</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-4">Newspaper</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-5">Fall</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-6">Side Fall</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-7">Sticky Up</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-8">3D Flip (horizontal)</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-9">3D Flip (vertical)</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-10">3D Sign</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-11">Super Scaled</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-13">3D Slit</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-14">3D Rotate Bottom</button>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-15">3D Rotate In Left</button>
				</div>
			</div>
		</div>
	</div>
			
	<div class="md-overlay"></div><!-- the overlay element -->

@stop

@section('footer-scripts')
{{ HTML::script('/js/template/modernizr.custom.js')}}
{{ HTML::script('/js/template/classie.js')}}
{{ HTML::script('/js/template/modalEffects.js')}}
<script type="text/javascript">
$(document).on("change","#files",function(){
    var count = $('#files_div').find('input').length;
    count = count+1;
    $("<div id='div_"+count+"' style='display:inline;'><input style='width:400px;splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div'); 
});
</script>
<script type="text/javascript">
function approveDocument(doc_id)
{
    var confirmed = confirm("Do you want to approve this document?");
    if(confirmed)
    {
        $.ajax({
                url: '{{URL::route("approveDocument")}}',
                data: '&doc_id='+doc_id,
                type: 'post',
                dataType:'json',
                // beforeSend: function(){
                //     $("#approved_div").show();
                //     //$("#entry_details").html("LOADING...");
                //     $("#approved_div").html('<span style="float:center;">{{HTML::image("/img/ajax-loader.gif")}}</span>');
                // },
                success: function(response)
                {
                    if(response.cond == 'true')
                    {
                        $("#approve_btn").hide();
                        $("#reject_btn").hide();
                        $("#approved_div").show();
                    }

                }
            }
        );
    }
}
function removeDocscomFile(doc_id,div)
{
    var confirmed = confirm("Do you want to remove this file?");
    if(confirmed)
    {
        $.ajax({
                url: '{{URL::route("removeDocscomFile")}}',
                data: '&doc_id='+doc_id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{{HTML::image("/img/ajax-loader.gif")}}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }

}

function postDocComment()
{   
    var formId = $('#form_id').val();
    $('#doc_id').val(formId);

    $.ajax({
        url     : "{{URL::route('postDocComment')}}",
        type    : "post",
        data    : $("form.comment_form").serialize(),
        success : function(result){
            $('#comments_div').append(result);
            $('#comment').val('');
        },
        error  : function( xhr, err ){
            alert(err);     
        }
    });    
    return false;
}
</script>
@stop