@foreach($record AS $item)
    @if(isDocumentApproved($item->id,'docs_answers'))
        <div style="padding-bottom:10px;" class="alert alert-success pull-right">
            <i class="fa fa-check-circle fa-fw fa-lg"></i>
            {!!_('approved_documents')!!}
        </div>
    @endif

<fieldset>
    <legend style="padding-bottom:8px;">{!!_('checkouted_documents')!!}
    </legend>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
                <div class="row form-group">
                    <div class="col-sm-12">
                        <div class="radio">
                                <input <?php if($item->doc_type == 1){echo "checked";} ?> value='1' id='application' name="doc_type" type="radio">

                            <label for="application">
                                {!!_('application')!!}
                            </label>
                        </div>
                        <div class="radio">
                            
                                <input <?php if($item->doc_type == 2){echo "checked";} ?> value='2' name="doc_type" id='document' type="radio">
                                
                            <label for="document">
                                {!!_('document_maktob')!!}
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('source_organization')!!}:</label>
            <div class="col-sm-6">
                <input class="form-control" type="text" name="doc_source" value="{!!$item->source_organization!!}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('summary')!!}:</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="6" id="wysiwig_full" name="doc_summary">{!!$item->doc_summary!!}</textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('executive_department')!!} :</label>
            <div class="col-sm-6">
                <select class="form-control" name="doc_executive_department" id="doc_executive_department">
                    <option value=''>{!!_('select')!!}</option>
                    @foreach($departments AS $dep)
                    <option <?php if($item->executive_department == $dep->id){echo 'selected';} ?> value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('checkout_date')!!} :</label>
            <div class="col-sm-6">
                <input value="{!!dmy_format($item->date)!!}" class="form-control datepicker_farsi" type="text" name="doc_external_date">
            </div>
            
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('is_answer')!!}؟</label>
            <div class="col-sm-4">
                <div class="row form-group">
                    <div class="col-sm-10">
                        <div class="radio">
                            <input <?php echo ($item->is_answer = 1 ? 'checked':""); ?> value='1' type="radio" id="doc_ans_yes" name="doc_answer">
                            <label for="doc_ans_yes">
                                {!!_('yes')!!}
                            </label>
                            <input value="{!!$item->form_number!!}" placeholder="شماره سند مربوطه ..." type="text" name="doc_yes" id="doc_yes" style="display: <?php echo ($item->is_answer = 1 ? 'inline':'none') ?>;" />
                        </div>
                        <div class="radio">
                            <input <?php echo ($item->is_answer = 0 ? 'checked':""); ?> value='0' type="radio" id="doc_ans_no" name="doc_answer">
                            <label for="doc_ans_no">
                                {!!_('no')!!}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="{!!URL::route('getDocDetails',array('docs_answers',$item->id))!!}" class="btn btn-warning pull-right">
            <i class="fa fa-eye fa-lg"></i> {!!_('more_details')!!}
    </a>
    
</fieldset>
@endforeach

@section('footer-scripts')
{!! HTML::script('/js/template/jquery.js') !!}
{!! HTML::script('/js/tasks/persian_datepicker.js')!!}

@stop

