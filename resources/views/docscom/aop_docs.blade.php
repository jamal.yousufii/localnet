@foreach($record AS $item)
    @if(isDocumentApproved($item->id,'aop_docs'))
        <div style="padding-bottom:10px;" class="alert alert-success pull-right">
            <i class="fa fa-check-circle fa-fw fa-lg"></i>
            {!!_('approved_documents')!!}
        </div>
    @endif

<fieldset>
    <legend style="padding-bottom:8px;">{!!_('aop_documents')!!}
    </legend>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
                <div class="row form-group">
                    <div class="col-sm-12">
                        <div class="radio">
                                <input <?php if($item->doc_type == 1){echo "checked";} ?> value='1' id='application' name="doc_type" type="radio">

                            <label for="application">
                                {!!_('application')!!}
                            </label>
                        </div>
                        <div class="radio">
                            
                                <input <?php if($item->doc_type == 2){echo "checked";} ?> value='2' name="doc_type" id='document' type="radio">
                                
                            <label for="document">
                                {!!_('document')!!}
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="col-sm-4">
                <div class="row form-group">
                    <div class="col-sm-12">
                        <div class="radio">
                                <input <?php if($item->access_type == 1){echo "checked";} ?> value='1' id='confidential' name="access" type="radio">

                            <label for="confidential">
                                محرم
                            </label>
                        </div>
                        <div class="radio">
                            
                                <input <?php if($item->access_type == 2){echo "checked";} ?> value='2' name="access" id='non_confidential' type="radio">
                                
                            <label for="non_confidential">
                                غیر محرم
                            </label>
                        </div>
                    </div>
                </div>
            </div> -->

        </div>
    </div>
    
    <div class="row">
        <input type = "hidden" name="form_id" id="form_id" value="{!!$item->id!!}" >
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('number')!!}:</label>
            <div class="col-sm-4">
                <input value="{!!$item->number!!}" class="form-control" type="text" name="aop_number">
            </div>
            <label class="col-sm-2 control-label">{!!_('date')!!} :</label>
            <div class="col-sm-4">
                <input value="{!!dmy_format($item->date)!!}" class="form-control" type="text" name="aop_date">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('applicant_department')!!} :</label>
            <div class="col-sm-10">
                
                <select class="form-control" name="aop_app_department" id="aop_app_department">
                    <option value=''>{!!_('select')!!}</option>             
                    @foreach($departments AS $dep)
                        <option <?php if($item->applicant_department == $dep->id){echo 'selected';} ?> value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                    @endforeach
                </select>
            </div>
        
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('application_subject')!!}:</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="6" id="wysiwig_full" name="aop_app_subject">{!!$item->applicantion_subject!!}</textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('executive_department')!!}:</label>
            <div class="col-sm-10">
                <select class="form-control" name="aop_executive_department" id="aop_executive_department">
                    <option value=''>{!!_('select')!!}</option>
                    @foreach($departments AS $dep)
                    <option <?php if($item->executive_department == $dep->id){echo 'selected';} ?> value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <a href="{!!URL::route('getDocDetails',array('aop_docs',$item->id))!!}" class="btn btn-warning pull-right">
            <i class="fa fa-eye fa-lg"></i> {!!_('more_details')!!}
        </a>
    
</fieldset>
@endforeach

@section('footer-scripts')
{!! HTML::script('/js/template/jquery.js') !!}
{!! HTML::script('/js/tasks/persian_datepicker.js')!!}


@stop

