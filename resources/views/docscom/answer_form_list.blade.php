@extends('layouts.master')

@section('head')

    <title>{!!_('doc_answer')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">{!!_('dashboard')!!}</a></li>
            <li class="active"><span>{!!_('documents')!!}</span></li>
        </ol>

        <h1>مکتوب صادره{!!_('document_sadira')!!}</h1>
    </div>
</div>

<div class="row">
<div class="col-lg-12">
<div class="main-box">
<header class="main-box-header clearfix">
    <h2>مکتوب صادره{!!_('document_sadira')!!}</h2>
    
</header>

<div class="main-box-body clearfix">

@if(canView('docscom_docscom'))

<div class="table-responsive">
    <table class="table table-responsive" id='list'>
        <thead>
        <tr>
            <th>#</th>
            <th>{!!_('barcode')!!}</th>
            <th>{!!_('document_sender')!!}</th>
            <th>{!!_('is_answer')!!}</th>
            <th>{!!_('executive_department')!!}</th>
            <th>{!!_('document_status')!!}</th>
            <th>{!!_('operation')!!}</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
    
@endif

</div>
</div>
</div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {

                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,

                //"iDisplayLength": 2,
                "sAjaxSource": "{!!URL::to('/docscom/getDocAnswersData')!!}",
                "aaSorting": [[ 1, "desc" ]],
                "aoColumns": [
                    { 'sWidth': '30px' },
                    { 'sWidth': '100px' },
                    { 'sWidth': '130px', 'sClass': 'center' },
                    { 'sWidth': '250px', 'sClass': 'center' },
                    { 'sWidth': '130px', 'sClass': 'center' },
                    { 'sWidth': '100px', 'sClass': 'center' },
                    { 'sWidth': '100px', 'sClass': 'center' }
                ],
                "language": {
                    "lengthMenu": "{!!_('view')!!} _MENU_ {!!_('record_per_page')!!}",
                    "zeroRecords": "{!!_('record_not_found')!!}",
                    "info": "{!!_('page_view')!!} _PAGE_ {!!_('of')!!} _PAGES_",
                    "infoEmpty": "{!!_('record_not_found')!!}",
                    "search": "{!!_('search')!!}",
                    "infoFiltered": "(filtered {!!_('of')!!} _MAX_ {!!_('total_record')!!})"
                }
            }
        );

    });
</script>
@stop

