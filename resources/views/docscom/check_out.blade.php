@extends('layouts.master')

@section('head')
{!! HTML::style('/css/template/libs/nifty-component.css') !!}
    <title>{!!_('checkout_document')!!}</title>
@stop
@section('content')
<style type="text/css">
.project-img-owner {
border-radius: 50%;
background-clip: padding-box;
display: block;
float: left;
height: 40px;
padding: 3px;
overflow: hidden;
width: 40px;
}
img {
vertical-align: middle;
}
img {
border: 0;
}
</style>

<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">{!!_('dashboard')!!}</a></li>
            <li class="active"><span>{!!_('checkout_document')!!}</span></li>
        </ol>
        
        <h1>{!!_('checkout_document')!!}</h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>{!!_('barcode')!!}</h2>
                
                @if(Session::has('success'))
                    <div class='alert alert-success'>
                        <i class="fa fa-check-circle fa-fw fa-lg"></i>
                        {!!Session::get('success')!!}
                    </div>
                @elseif(Session::has('fail'))
                    <div class='alert alert-danger'>
                        <i class="fa fa-times-circle fa-fw fa-lg"></i>
                        {!!Session::get('fail')!!}
                    </div>
                 @endif 
                 <a href="{!!URL::to('/docscom/getForm')!!}" class="btn btn-success pull-right">
                    <i class="fa fa-chevron-left fa-lg"></i> {!!_('back')!!}
                </a>     
            </header>
            
            <div class="main-box-body clearfix">
                <!-- <form class="form-horizontal" role="form" method="post" action="{!!URL::route('docsPostForm')!!}"> -->
                @if(canCheckOut('docscom_docscom'))
                <div class = "form-horizontal">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('barcode')!!} :</label>
                            
                            <div class="input-group col-sm-4">
                                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                <input onkeypress='return showForm(event)' type="text" class="form-control" id="barcode" name="barcode" placeholder="بارکد">
                                
                                <span style='color:red'>
                                @if($errors->has("barcode"))
                                    {!! $errors->first('barcode') !!}
                                @endif
                                </span>
                            
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            
                            <div class="col-sm-10">
                                <div style='display:none;width:50%' class="alert alert-danger" id='barcode_validate_div'>
                                    <i class="fa fa-times-circle fa-fw fa-lg"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <span id='barcode_validate'></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div id = "entry_details" style="display:none;">
                        <fieldset><legend>{!!_('sadira_form')!!}</legend>
                        <form class="form-horizontal" role="form" method="post" action="{!!URL::route('saveCheckOut')!!}" enctype="multipart/form-data">
                            <input type="hidden" name="doc_id" id="doc_id">
                            <input type="hidden" name="table_name" id="table_name">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{!!_('checkout_date')!!} :</label>
                                <div class="col-sm-4">
                                    <input class="form-control {!!getDatePickerClass()!!}" type="text" name="checkout_date">
                                </div>
                                <label class="col-sm-2 control-label">{!!_('checkout_number')!!} :</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="checkout_number">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{!!_('attachment')!!}</label>
                                <div class="col-sm-4" id='files_div'>
                                    <input type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
                                </div>
                                
                            </div>
                            {!! Form::token() !!}

                            <div class="form-group">
                                <label class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-sm-10">
            
                                    <button class="btn btn-primary" type="submit">{!!_('checkout_it')!!}</button>
                                
                                    <button onclick="$('#entry_details').slideUp();" class="btn btn-danger" type="button">{!!_('cancel')!!}</button>
                                </div>
                            </div>
                        </form>
                        </fieldset>
                    </div>

                </div>
                @endif
                
            </div>
        </div>
    </div>
</div>



@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).on("change","#files",function(){
    var count = $('#files_div').find('input').length;
    count = count+1;
    $("<div id='div_"+count+"' style='display:inline;'><input style='splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div'); 
});
</script>
<script type="text/javascript">
function showForm(e) 
{
    if (e.keyCode == 13) 
    {
        var barcodeValue = document.getElementById('barcode').value;
        if(barcodeValue == '')
        {
            $("#entry_details").slideUp();
            $('#barcode').focus();
            $('#barcode').css('border-color','red');
            return false;
        }
        else
        {
            
            //validate form in the database via ajax
            $.ajax({
                url: '{!!URL::route("findBarcodeDetails")!!}',
                data: '&barcode='+barcodeValue+"&checkout=1",
                type: 'post',
                beforeSend: function(){
                    //$("#entry_details").show();
                    //$("#entry_details").html("LOADING...");
                    //$("#entry_details").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                dataType: 'json',
                success: function(response){
                    
                    if(response.cond == 'false')
                    {
                        $("#entry_details").hide();
                        $('#barcode_validate_div').show();
                        $('#barcode_validate').html('بارکد موجود نیست!');
                        //alert('<div class="alert-danger">This barcode is not validate</div>');
                        return false;
                    }
                    else if(response.approved == 'false')
                    {

                        $("#entry_details").hide();
                        $('#barcode_validate_div').show();
                        $('#barcode_validate').html('سند تایید نگردیده!');
                        //alert('<div class="alert-danger">This barcode is not validate</div>');
                        return false;

                    }
                    else if(response.checkOuted=='true')
                    {
                        $("#entry_details").hide();
                        $('#barcode_validate_div').show();
                        $('#barcode_validate').html('سند قبلا صادر گردیده است!');
                        //alert('<div class="alert-danger">This barcode is not validate</div>');
                        return false;

                    }
                    else
                    {
                        
                        $('#barcode_validate_div').hide();
                        $("#entry_details").slideDown();
                        
                        $('#doc_id').val(response.doc_id);
                        $('#table_name').val(response.table);
                        
                        return false;
                    }
                }

            });
        }

        return false;
    }
}
</script>
<script type="text/javascript">
    $(function(){
        $('#barcode').focus();
    })
</script>
@stop

