@extends('layouts.master')

@section('head')
{!! HTML::style('/css/template/libs/nifty-component.css') !!}
    <title>Documents by barcode</title>
@stop

<style type="text/css">
.project-img-owner {
border-radius: 50%;
background-clip: padding-box;
display: block;
float: left;
height: 40px;
padding: 3px;
overflow: hidden;
width: 40px;
}
img {
vertical-align: middle;
}
img {
border: 0;
}
.pending-icon{
    -webkit-animation: rotating 3s linear infinite;
}
</style>
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active"><span>Documents Details</span></li>
        </ol>
        
        <h1>Document<small>Details</small></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <header class="main-box-header clearfix">
                @if(!isDocumentApproved($doc_id) && canApprove('docscom_docscom'))
                <a href="javascript:void()" class="btn btn-warning" onclick="approveDocument('{!!$doc_id!!}')" id='approve_btn'>
                    <i class="fa fa-check fa-lg"></i> Approve
                </a>
                <div id="approved_div" style='display:none;'>
                    <div class="alert alert-success" style="width:50%;display:inline;">
                        <i class="fa fa-check-circle fa-fw fa-lg"></i>
                        Document Approved
                    </div>
                </div>
                @endif

                @if(amIRejected($doc_id) && !isDocumentApproved($doc_id))
                
                <div class="alert alert-danger" style="width:50%;display:inline;">
                    <i class="fa fa-times fa-lg"></i>
                    Document Rejected
                </div> 
                
                @else
                    @if(canReject('docscom_docscom') && !isDocumentApproved($doc_id))
                    <a href="javascript:void()" data-modal="form_reject_modal" class="md-trigger btn btn-danger" id="reject_btn">
                        <i class="fa fa-times fa-lg"></i> Reject
                    </a>
                    @endif
                @endif

                @if(isDocumentApproved($doc_id))
                    <div style="width:50%;display:inline;" class="alert alert-success">
                        <i class="fa fa-check-circle fa-fw fa-lg"></i>
                        Document Approved
                    </div>
                @endif

                

                @if(Session::has('success'))
                    <div class='alert alert-success'>
                        <i class="fa fa-check-circle fa-fw fa-lg"></i>
                        {!!Session::get('success')!!}
                    </div>
                @elseif(Session::has('fail'))
                    <div class='alert alert-danger'>
                        <i class="fa fa-times-circle fa-fw fa-lg"></i>
                        {!!Session::get('fail')!!}
                    </div>
                 @endif   
                 
                @if(canApprove('docscom_docscom'))
                <a href="javascript:void()" style="margin-right:10px;margin-left:10px;" class="md-trigger btn btn-success pull-right" data-modal="form_status_modal">
                    <i class="fa fa-refresh fa-lg"></i> Check Doc Status
                </a>
                @endif

                <!-- <a href="#" style="margin-right:10px;margin-left:10px;" class="md-trigger btn btn-success pull-right">
                    <i class="fa fa-upload fa-lg"></i> Extract Document
                </a> -->
                
                <a href="{!!URL::to('/docscom/getForm')!!}" class="btn btn-success pull-right">
                    <i class="fa fa-chevron-left fa-lg"></i> Back
                </a>  

                <!-- <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
                <strong>Extracted Number: </strong> 1029102<br>
                <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
                <strong>Extracted Date: </strong> 1029102  -->

            </header>
            
            <div class="main-box-body clearfix form-horizontal">
            
            @foreach($records AS $item)
                @if(canEdit('docscom_docscom'))
                <form class="form-horizontal" role="form" method="post" action="{!!URL::route('docsPostUpdateForm',$item->id)!!}" enctype="multipart/form-data">
                @endif
                
                

                <fieldset>
                    <legend style="padding-bottom:8px;">Source Organization
                        
                        
                    </legend>

                    <div class="row">
                        <input type = "hidden" name="form_id" id="form_id" value="{!!$item->id!!}" >
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Organization Name :</label>
                            <div class="col-sm-4">
                                <input value="{!!$item->organization!!}" class="form-control" type="text" name="organization">
                            </div>
                            <label class="col-sm-2 control-label">External Number :</label>
                            <div class="col-sm-4">
                                <input value="{!!$item->external_number!!}" class="form-control" type="text" name="external_number">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">External Date :</label>
                            <div class="col-sm-4">
                                <input value="{!!dmy_format($item->external_date)!!}" class="form-control datepicker_farsi" type="text" name="external_date">
                            </div>
                            <label class="col-sm-2 control-label">Email :</label>
                            <div class="col-sm-4">
                                <input value="{!!$item->email!!}" class="form-control" type="text" name="email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Phone :</label>
                            <div class="col-sm-4">
                                <input value="{!!$item->phone!!}" class="form-control" type="text" name="phone">
                            </div>
                            
                        </div>
                    </div>
                </fieldset>

                <fieldset><legend>Document and communication</legend>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Internal Number :</label>
                            <div class="col-sm-4">
                                <input value="{!!$item->internal_number!!}" class="form-control" type="text" name="internal_number">
                            </div>
                            <label class="col-sm-2 control-label">Internal Date :</label>
                            <div class="col-sm-4">
                                <input value="{!!dmy_format($item->internal_date)!!}" class="form-control datepicker_farsi" type="text" name="internal_date">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Document Summary :</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="5" id="wysiwig_full" name="summary">{!!$item->doc_summary!!}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Executive Department :</label>
                            <div class="col-sm-10">
                                
                                <select class="form-control" name="executive_department" id="executive_department">
                                        
                                        @foreach($departments AS $dep)
                                            <option <?php if($item->executive_department == $dep->id){echo 'selected';} ?> value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                                        @endforeach
                                    
                                </select>
                            </div>
                        
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Document Type:</label>
                        <div class="col-sm-4">
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <div class="radio">
                                            <input <?php if($item->doc_type == 1){echo "checked";} ?> value='1' id='application' name="doc_type" type="radio">

                                        <label for="application">
                                            Application
                                        </label>
                                    </div>
                                    <div class="radio">
                                        
                                            <input <?php if($item->doc_type == 2){echo "checked";} ?> value='2' name="doc_type" id='document' type="radio">
                                            
                                        <label for="document">
                                            Document
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <label class="col-sm-2 control-label">Access Type :</label>
                        <div class="col-sm-4">

                            <div class="row form-group">
                                
                                <div class="col-sm-10">
                                    @if($item->access_type==3 || $item->access_type==1)
                                    <div class="radio">
                                        <input <?php if($item->access_type == 3){echo "checked";} ?> value='3' type="radio" id="other" name="access">
                                        <label for="other">
                                            Other
                                        </label>
                                    </div>
                                    
                                    <div class="radio">
                                            <input <?php if($item->access_type == 1){echo "checked";} ?> value='1' id='confidential' name="access" type="radio">

                                        <label for="confidential">
                                            Confidential
                                        </label>
                                    </div>
                                    @elseif($item->access_type==2)
                                    <div class="radio">
                                        
                                            <input <?php if($item->access_type == 2){echo "checked";} ?> value='2' name="access" id='non_confidential' type="radio">
                                            
                                        <label for="non_confidential">
                                            Non Confidential
                                        </label>
                                    </div>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                        
                        <!-- <div class="col-sm-4">
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <div class="radio">
                                            <input <?php if($item->access_type == 1){echo "checked";} ?> value='1' id='confidential' name="access" type="radio">

                                        <label for="confidential">
                                            Confidential
                                        </label>
                                    </div>
                                    <div class="radio">
                                        
                                            <input <?php if($item->access_type == 2){echo "checked";} ?> value='2' name="access" id='non_confidential' type="radio">
                                            
                                        <label for="non_confidential">
                                            Non Confidential
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>

                </fieldset>

                <fieldset>
                    <legend>Attachments</legend>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="main-box clearfix">
                            <header class="main-box-header clearfix">
                                <h2>
                                    <i class="fa fa-upload fa-fw fa-lg" style='color:#03a9f4;'></i>
                                    Uploaded Attachments
                                </h2>
                            </header>
                            
                            <div class="main-box-body clearfix">

                                <ul class="widget-todo">
                                    @if(count($attachments)>0)
                                        @foreach($attachments AS $attach)
                                        <li class="clearfix" id="li_{!!$attach->id!!}">
                                            <div class="name">
                                                <label for="todo-2">
                                                    <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
                                                    <strong>{!!$attach->file_name!!}</strong>
                                                </label>
                                            </div>
                                            <div class="actions">
                                                @if(canEdit('docscom_docscom'))
                                                <a href="{!!URL::route('getDocscomDownload',$attach->id)!!}" class="table-link success">
                                                    <i class="fa fa-cloud-download" style='color:#03a9f4;'></i>
                                                </a>
                                                
                                                <a href="javascript:void()" onclick="removeDocscomFile('{!!$attach->id!!}','li_{!!$attach->id!!}')" class="table-link danger">
                                                    <i class="fa fa-trash-o" style='color:red;'></i>
                                                </a>
                                                @endif
                                            </div>
                                        </li>
                                        @endforeach
                                    @else
                                        <li><span style='color:red;'>File not found!</span></li>
                                    @endif
                                </ul>
                                    
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">New Attachment</label>
                        <div class="col-sm-4" id='files_div' style='width:465px;'>
                            <input style='width:400px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
                        </div>
                        
                    </div>
                </fieldset> 
                @if(canEdit('docscom_docscom'))
                <div class="form-group">
                    <label class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-sm-10">
                        <button class="btn btn-primary" type="submit">Update Details</button>
                    </div>
                </div>
                </form>
                @endif
            @endforeach
            </div>
        </div>
    </div>
</div>

<div class="row" id="row_comments">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-box clearfix">
                    <header class="main-box-header clearfix">
                        <h2 class="pull-left value red">Comments</h2>
                    </header>
                    <div class="main-box-body clearfix">
                        <div class="conversation-wrapper">
                            <div class="conversation-content" id="comments_div">
                                <?php 
                                    
                                    foreach($comments AS $comment)
                                    {
                                ?>
                                        <div class="conversation-item item-left clearfix">
                                            <div class="conversation-user">
                                                <?php $photo = getProfilePicture($comment->user_id); ?>
                                                {!! HTML::image('/img/'.$photo,'',array('class' => 'project-img-owner comment')) !!}
                                            </div>
                                            <div class="conversation-body">
                                                <div class="name">
                                                    {!!getUserFullName($comment->user_id)!!}
                                                </div>
                                                <div class="time hidden-xs">
                                                    {!!$comment->created_at!!}

                                                </div>
                                                <div class="text">
                                                    {!!$comment->comment!!}@if($comment->type==1) <-> (<span style="font-weight:bold;color:red;">Rejected Comment</span>)@endif
                                                </div>
                                            </div>
                                        </div>
                                <?php 
                                    }
                                ?>
                            </div>
                            <br><br>
                            @if(canComment('docscom_docscom'))
                            <div class="conversation-new-message">
                                <form class="comment_form">
                                    <input type="hidden" name="doc_id" id="doc_id">
                                    <div class="form-group">
                                        <textarea name="comment" id="comment" class="form-control" rows="2" placeholder="Enter New Comment..."></textarea>
                                    </div>
                                    
                                    <div class="clearfix">
                                        <button type="button" class="btn btn-success pull-right" onclick='postDocComment()'>Post Comment</button>
                                    </div>
                                </form>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="md-modal md-effect-7" id="form_status_modal">
    <div class="md-content">
        <div class="modal-header">
            <button class="md-close close" id="close_icon">&times;</button>
            <h4 class="modal-title">Document Process Status</h4>
        </div>
        <div class="modal-body">
            
            <div class="alert alert-success">
                <!-- <i class="fa fa-warning fa-fw fa-lg"></i> -->
                @foreach($records AS $item)
                <table width="100%" style="font-size:12px;">
                    <tr >
                        <th style="padding-bottom:8px;"><i class="fa fa-clock-o"></i> Checked Out Date:</th><td>{!!$item->exported_date!!}</td>
                        <th style="padding-bottom:8px;"><i class="fa fa-check"></i> Checked Out Number:</th><td>{!!$item->exported_number!!}</td>
                        
                    </tr>
                    <tr>
                        <th><i class="fa fa-user"></i> Checked Out By:</th><td>{!!getUserFullName($item->exported_by)!!}</td>
                        <th> <i class="fa fa-user"></i> Approved By:</th><td>{!!getUserFullName($item->approved_by)!!}</td>
                    </tr>

                </table>
                @endforeach
                
            </div>

            <div id="pop_content2">
                <table class='table table-bordered'>
                    <tr>
                        <th>#</th>
                        <th>Department</th>
                        <th>Received Date</th>
                        <th>Received By</th>
                        <th>Issued Date</th>
                        <th>Issued By</th>
                        <th>Status</th>
                    </tr>
                    <?php $counter = 1; ?>
                    @foreach($logs AS $log)
                        <tr>
                            <td>{!!$counter!!}</td>
                            <td>{!!$log->department!!}</td>
                            <td>{!!$log->check_in!!}</td>
                            <td>{!!$log->checked_in_by!!}</td>
                            <td align="center"><?php if($log->check_out=='0000-00-00 00:00:00'){echo "--";}else{ echo $log->check_out;} ?></td>
                            <td align="center">
                                <?php if($log->checked_out_by==''){echo "--";}else{ echo $log->checked_out_by;} ?>
                            </td>
                            <td class="text-center">
                                <?php 
                                    if($log->status == 1)
                                    {
                                        echo '<span title="Pending in this department..." class="label label-warning"><i class="fa fa-refresh fa-lg pending-icon"></i></span>';   
                                    }
                                    else
                                    {
                                        echo '<span title="Completed from this department" class="label label-success"><i class="fa fa-check fa-lg"></i></span>';
                                        
                                    }
                                ?>
                            </td>
                            
                        </tr>
                        <?php $counter++; ?>
                    @endforeach

                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button onclick='$("#close_icon").trigger("click");' type="button" class="btn btn-warning">
                <i class="fa fa-check-circle fa-lg"></i> OK</button>
            
        </div>
    </div>
</div>
<div class="md-modal md-effect-7" id="form_reject_modal">
    <div class="md-content">
        
            <div class="modal-header">
                <button class="md-close close" id="close_icon">&times;</button>
                <h4 class="modal-title">Reason for reject</h4>
            </div>
            <form class="comment_form" method="post" action="{!!URL::route('rejectDocument')!!}" class="form-horizontal">
            <div class="modal-body">
                
                    <input type="hidden" name="doc_id" id="doc_id" value="{!!$doc_id!!}">
                    <input type="hidden" name="notify_to" id="notify_to" value="{!!$checked_in_by!!}">
                    <div class="form-group">
                        <textarea name="comment1" id="comment1" class="form-control" rows="4" placeholder="Enter Reason..."></textarea>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" onclick='$("#close_icon").trigger("click");' type="button" class="btn btn-danger">
                    <i class="fa fa-times fa-lg"></i> Reject</button>
                
            </div>
        </form>
    </div>
</div>

<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')
{!! HTML::script('/js/template/modernizr.custom.js')!!}
{!! HTML::script('/js/template/classie.js')!!}
{!! HTML::script('/js/template/modalEffects.js')!!}
<script type="text/javascript">
$(document).on("change","#files",function(){
    var count = $('#files_div').find('input').length;
    count = count+1;
    $("<div id='div_"+count+"' style='display:inline;'><input style='width:400px;splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div'); 
});
</script>
<script type="text/javascript">
function approveDocument(doc_id)
{
    var confirmed = confirm("Do you want to approve this document?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("approveDocument")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                dataType:'json',
                // beforeSend: function(){
                //     $("#approved_div").show();
                //     //$("#entry_details").html("LOADING...");
                //     $("#approved_div").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                // },
                success: function(response)
                {
                    if(response.cond == 'true')
                    {
                        $("#approve_btn").hide();
                        $("#reject_btn").hide();
                        $("#approved_div").show();
                    }

                }
            }
        );
    }
}
function removeDocscomFile(doc_id,div)
{
    var confirmed = confirm("Do you want to remove this file?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeDocscomFile")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }

}

function postDocComment()
{   
    var formId = $('#form_id').val();
    $('#doc_id').val(formId);

    $.ajax({
        url     : "{!!URL::route('postDocComment')!!}",
        type    : "post",
        data    : $("form.comment_form").serialize(),
        success : function(result){
            $('#comments_div').append(result);
            $('#comment').val('');
        },
        error  : function( xhr, err ){
            alert(err);     
        }
    });    
    return false;
}
</script>
@stop

