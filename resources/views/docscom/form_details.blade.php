@foreach($record AS $item)
    @if(isDocumentApproved($item->id))
        <div style="padding-bottom:10px;" class="alert alert-success pull-right">
            <i class="fa fa-check-circle fa-fw fa-lg"></i>
            {!!_('document_approved')!!}
        </div>
    @endif

<fieldset>
    <legend style="padding-bottom:8px;">{!!_('source_organization')!!}
        
        @if(!isDocumentApproved($item->id) && canApprove('docscom_docscom'))
        <!-- <a href="#" class="btn btn-warning pull-right" onclick="approveDocument('{!!$item->id!!}')" id='approve_btn'>
            <i class="fa fa-check fa-lg"></i> Approve
        </a> -->
        @endif
    </legend>

    <div class="row">
        <input type = "hidden" name="form_id" id="form_id" value="{!!$item->id!!}" >
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('organization_name')!!} :</label>
            <div class="col-sm-4">
                <input value="{!!$item->organization!!}" class="form-control" type="text" name="organization">
            </div>
            <label class="col-sm-2 control-label">{!!_('checkouted_number')!!} :</label>
            <div class="col-sm-4">
                <input value="{!!$item->external_number!!}" class="form-control" type="text" name="external_number">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('checkouted_date')!!} :</label>
            <div class="col-sm-4">
                <input value="{!!dmy_format($item->external_date)!!}" class="form-control datepicker_farsi" type="text" name="external_date">
            </div>
            <!-- <label class="col-sm-2 control-label">ایمیل :</label>
            <div class="col-sm-4">
                <input value="{!!$item->email!!}" class="form-control" type="text" name="email">
            </div> -->
        </div>
    </div>
    <!-- <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">نمبر موبایل :</label>
            <div class="col-sm-4">
                <input value="{!!$item->phone!!}" class="form-control" type="text" name="phone">
            </div>
            
        </div>
    </div> -->
</fieldset>

<fieldset><legend>{!!_('document_and_communication')!!}</legend>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('checkin_number')!!} :</label>
            <div class="col-sm-4">
                <input value="{!!$item->internal_number!!}" class="form-control" type="text" name="internal_number">
            </div>
            <label class="col-sm-2 control-label">{!!_('checkin_date')!!} :</label>
            <div class="col-sm-4">
                <input value="{!!dmy_format($item->internal_date)!!}" class="form-control datepicker_farsi" type="text" name="internal_date">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('summary')!!} :</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="5" id="wysiwig_full" name="summary">{!!$item->doc_summary!!}</textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <label class="col-sm-2 control-label">{!!_('executive_department')!!} :</label>
            <div class="col-sm-10">
                
                <select class="form-control" name="executive_department" id="executive_department">
                                        
                        @foreach($departments AS $dep)
                            <option <?php if($item->executive_department == $dep->id){echo 'selected';} ?> value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                        @endforeach
                    
                </select>
            </div>
        
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-sm-4">
            <div class="row form-group">
                <div class="col-sm-12">
                    <div class="radio">
                            <input <?php if($item->doc_type == 1){echo "checked";} ?> value='1' id='application' name="doc_type" type="radio">

                        <label for="application">
                            {!!_('application')!!}
                        </label>
                    </div>
                    <div class="radio">
                        
                            <input <?php if($item->doc_type == 2){echo "checked";} ?> value='2' name="doc_type" id='document' type="radio">
                            
                        <label for="document">
                            {!!_('document_maktob')!!}
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="row form-group">
                <div class="col-sm-12">
                    <div class="radio">
                            <input <?php if($item->access_type == 1){echo "checked";} ?> value='1' id='confidential' name="access" type="radio">

                        <label for="confidential">
                            {!!_('confidential')!!}
                        </label>
                    </div>
                    <div class="radio">
                        
                            <input <?php if($item->access_type == 2){echo "checked";} ?> value='2' name="access" id='non_confidential' type="radio">
                            
                        <label for="non_confidential">
                            {!!_('non_confidential')!!}
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="{!!URL::route('getDocDetails',array('docs',$item->id))!!}" class="btn btn-warning pull-right">
            <i class="fa fa-eye fa-lg"></i> {!!_('more_details')!!}
        </a>

</fieldset>
@endforeach

@section('footer-scripts')
{!! HTML::script('/js/template/jquery.js') !!}
{!! HTML::script('/js/tasks/persian_datepicker.js')!!}


@stop

