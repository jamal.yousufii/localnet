

<?php 
    
    $application = 0;
    $document = 0;
    $other = 0;
    foreach($records AS $item)
    {

        //application
        if($item->doc_type == 1)
        {
            
            $application++;

        }
        elseif($item->doc_type == 2)
        {
            
            $document++;
        }
        else
        {
            $other++;
        }

    }

    $label = "['پیشنهاد',".$application."],";
    $label .= "['مکتوب',".$document."],";
    $label .= "['دیگر',".$other."]";
?>

<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: '{!!_("log_report_in_groph")!!}'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    useHTML:true,
                    
                    format: '{point.name}: <b>{point.y}</b>'
                }
            }
        },
        series: [{
            type: 'pie',
            name: '{!!_("log_report")!!}',
            data: [{!!$label!!}]
        }]
    });
});

</script>




<fieldset>
    <legend style="color:orange;">{!!_('search_result')!!}! 
        <span class="pull-right"><input type="checkbox" id="show_chart" /> <label for="show_chart">Show Graph Mode</label></span>
    </legend>
    <div id="container" style="width: 1000px; height: 400px; margin: 0 auto;display:none;"></div>

    <div class="table-responsive" id="table_mode">
        <table class="table" id="list">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{!!_('barcode')!!}</th>
                    <th>{!!_('source_org_checkout_number')!!}</th>
                    <th>{!!_('checkin_number')!!}</th>
                    <th>{!!_('checkin_date')!!}</th>
                    <th>{!!_('executive_department')!!}</th>
                    <th>{!!_('source_organization')!!}</th>
                    <th>{!!_('operation')!!}</th>
                </tr>
            </thead>
            <tbody>

                <?php $counter = 1; ?>
                @foreach($records AS $item)
                    <tr>
                    <td>{!!$counter!!}</td>
                    <td>{!!$item->barcode!!}</td>
                    <td>{!!$item->external_number!!}</td>
                    <td>{!!$item->internal_number!!}</td>
                    <td>
                    	
                    	<?php 
                    		if(isMiladiDate())
                    		{
                    			$date = explode('-',$item->internal_date);
								
								echo dateToMiladi($date[0],$date[1],$date[2]);
                    		}
                    		else
                    		{
                    			echo $item->internal_date;
                    		}
                    	?>	
                    </td>
                    <td>{!!$item->executive_department!!}</td>
                    <td>{!!$item->organization!!}</td>
                    
                    <td>
                        <a href="{!!route('getDocDetails',array('docs',$item->id))!!}" class="table-link">
                            <span class="fa-stack">
                                <i class="fa fa-square fa-stack-2x"></i>
                                <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </td>
                    </tr>
                    <?php $counter++; ?>
                @endforeach
                
            </tbody>
        </table>

    </div>

</fieldset>

<script type="text/javascript">
//$(document).ready(function() {

//  var data = $('#search_fields').serializeArray().reduce(function(obj, item) {
//      obj[item.name] = item.value;
//      return obj;
//  }, {});

// var t= $('#list').dataTable( 
// {
//         // "ajax": {
//         //     "url": "{!!URL::to('/docscom/getSearchResultData')!!}",
//         //     "type": "POST",
//         //     "data": data
//         // }, 

//         'sDom': 'lf<"clearfix">tip',
//         "bProcessing": true,
//         "bServerSide": true,
//         "iDisplayLength": 2,
//         "sServerMethod": "POST",
//         "bJQueryUI": true,
//         "sAjaxSource": "{!!URL::to('/docscom/getSearchResultData')!!}",
//         //"aaSorting": [[ 1, "desc" ]],
//         // "aoColumns": [
//         //     { 'sWidth': '50px' },
//         //     { 'sWidth': '250px', 'sClass': 'center' },
//         //     { 'sWidth': '250px', 'sClass': 'center' },
//         //     { 'sWidth': '250px', 'sClass': 'center' },
//         //     { 'sWidth': '250px', 'sClass': 'center' },
//         //     { 'sWidth': '250px', 'sClass': 'center' },
//         //     { 'sWidth': '120px', 'sClass': 'center' },
//         //     { 'sWidth': '100px', 'sClass': 'center' }
//         // ],
//         "language": {
//             "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
//             "zeroRecords": "ریکارد موجود نیست",
//             "info": "نمایش صفحه _PAGE_ از _PAGES_",
//             "infoEmpty": "ریکارد موجود نیست",
//             "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
//         }
//     }
// );

// });
$(document).ready(function() {
    $('#show_chart').on('change',function(){
        if(this.checked == true)
        {
            $('#table_mode').slideUp();
            $('#container').slideDown();
        }
        else
        {
            $('#table_mode').slideDown();
            $('#container').slideUp();
        }
    });
    $('#list').dataTable({

            'sDom': 'lf<"clearfix">tip',
            "iDisplayLength": 10,
            "language": {
                    "lengthMenu": "{!!_('view')!!} _MENU_ {!!_('record_per_page')!!}",
                    "zeroRecords": "{!!_('record_not_found')!!}",
                    "info": "{!!_('page_view')!!} _PAGE_ {!!_('of')!!} _PAGES_",
                    "infoEmpty": "{!!_('record_not_found')!!}",
                    "search": "{!!_('search')!!}",
                    "infoFiltered": "(filtered {!!_('of')!!} _MAX_ {!!_('total_record')!!})"
                }
        });
} );
</script>

