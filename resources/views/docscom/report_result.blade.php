

<?php 
    
    

    $app_exported       = 0;
    $app_saved          = 0;
    $app_under_process  = 0;
    $app_approved  = 0;
    $app_total = 0;

    $doc_exported       = 0;
    $doc_saved          = 0;
    $doc_under_process  = 0;
    $doc_approved  = 0;
    $doc_total = 0;

    $other_exported       = 0;
    $other_saved          = 0;
    $other_under_process  = 0; 
    $other_approved  = 0;
    $other_total = 0;   

    foreach($records AS $item)
    {

        //application
        if($item->doc_type == 1)
        {
            
            if($item->status == 0)
            {
                $app_under_process++;
            }
            else if($item->status == 3)
            {
                $app_saved++;
            }
            else if($item->status == 4)
            {
                $app_exported++;
            }
            else if($item->status == 1)
            {
                $app_approved++;
            }

            $app_total++;

        }
        elseif($item->doc_type == 2)
        {
            if($item->status == 0)
            {
                $doc_under_process++;
            }
            else if($item->status == 3)
            {
                $doc_saved++;
            }
            else if($item->status == 4)
            {
                $doc_exported++;
            }
            else if($item->status == 1)
            {
                $doc_approved++;
            }

            $doc_total++;
        }
        else
        {
            if($item->status == 0)
            {
                $other_under_process++;
            }
            else if($item->status == 3)
            {
                $other_saved++;
            }
            else if($item->status == 4)
            {
                $other_exported++;
            }
            else if($item->status == 1)
            {
                $other_approved++;
            }

            $other_total++;
        }

    }

    $app = "[".$app_exported.",".$app_saved.",".$app_under_process.",".$app_approved."]";
    $doc = "[".$doc_exported.",".$doc_saved.",".$doc_under_process.",".$doc_approved."]";
    $other = "[".$other_exported.",".$other_saved.",".$other_under_process.",".$other_approved."]";
    
?>

<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '({!!$app_total+$doc_total+$other_total!!}) - چارت وضعیت پروسیس اسناد'
        },
        xAxis: {
            categories: [
                'صادر شده',
                'حفظ شده',
                'تحت پروسیس',
                'تایید شده'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'تعداد'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px;font-weight:bold;">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: &nbsp;</td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    //rotation: -90,
                    format: '{point.y}',
                    //color: '#FFFFFF',
                    //y:10,
                    // style: {
                    //     fontSize: '13px',
                    //     fontFamily: 'Verdana, sans-serif'
                    // }
                }
            }
        },
        series: [{
            name: '[{!!$doc_total!!}] مکتوب',
            data: {!!$doc!!}

        }, {
            name: '[{!!$app_total!!}] پیشنهاد',
            data: {!!$app!!}

        }, {
            name: '[{!!$other_total!!}] دیگر',
            data: {!!$other!!}

        }]
    });
});
</script>

<div id="container" style="min-width: 300px; height: 400px; margin: 0 auto"></div>