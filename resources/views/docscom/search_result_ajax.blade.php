
		<table class="table" id="list">
			<thead>
				<tr>
                    <th>#</th>
	                <th>{!!_('barcode')!!}</th>
                    <th>{!!_('source_org_checkout_number')!!}</th>
                    <th>{!!_('checkin_number')!!}</th>
                    <th>{!!_('checkin_date')!!}</th>
                    <th>{!!_('executive_department')!!}</th>
                    <th>{!!_('source_organization')!!}</th>
                    <th>{!!_('document_status')!!}</th>
                    <th>{!!_('approved_by')!!}</th>
                    <th>{!!_('duration')!!}</th>
                    <th>{!!_('operation')!!}</th>
	            </tr>
			</thead>
			<tbody>

                <?php $counter = $rows->firstItem(); ?>
                @foreach($rows AS $item)
                    <tr>
                    <td>{!!$counter!!}</td>
                    <td>{!!$item->barcode!!}</td>
                    <td>{!!$item->external_number!!}</td>
                    <td>{!!$item->internal_number!!}</td>
                    <td>
                    	<?php 
                    		if(isMiladiDate())
                    		{
                    			$date = explode('-',$item->internal_date);
								
								echo dateToMiladi($date[0],$date[1],$date[2]);
                    		}
                    		else
                    		{
                    			echo $item->internal_date;
                    		}
                    	?>
                    </td>
                    <td>{!!$item->executive_department!!}</td>
                    <td>{!!$item->organization!!}</td>
                    <td>{!!$item->status!!}</td>
                    <td>{!!$item->approved_by!!}</td>
                    <td>{!!$item->days!!}</td>
                    <td>
                        <a href="{!!route('getDocDetails',array('docs',$item->id))!!}" class="table-link">
                            <span class="fa-stack">
                                <i class="fa fa-square fa-stack-2x"></i>
                                <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </td>
                    </tr>
                    <?php $counter++; ?>
                @endforeach
				
			</tbody>
		</table>
		
		<div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
			{!!$rows->render()!!}
		</div>

<script>
	$( document ).ready(function() {
		$('.pagination a').on('click', function(event) {
			event.preventDefault();
			if ($(this).attr('href') != '#') {
				//$('#ajaxContent').load($(this).attr('href'));
				
				$.ajax({
		                url: '{!!URL::route("getSearchResult")!!}',
		                data: $('#search_fields').serialize()+"&page="+$(this).text()+"&ajax=1",
		                type: 'post',
		                beforeSend: function(){
		
		                    //$("body").show().css({"opacity": "0.5"});
		                    $('#ajaxContent').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
		                },
		                success: function(response)
		                {
		
		                    $('#ajaxContent').html(response);
		                }
		            }
		        );
			
			}
		});
	});
</script>

