

<fieldset>

	<legend style="color:orange;">{!!_('search_result')!!}! 
        <form method="post" id="search_fields" action="{!!URL::route('searchResultToExcel')!!}" role="form" enctype="multipart/form-data">
   
            <input value="{!!$start_date!!}" name="start_date" type="hidden" class="form-control datepicker_farsi" placeholder="تاریخ شروع">

            <input value="{!!$end_date!!}" name="end_date" type="hidden" class="form-control datepicker_farsi" placeholder="تاریخ ختم">
            
            <input value="{!!$condition!!}" type="hidden" name="condition" id="condition" class="form-control">
              

            <input value="{!!$parent_dep!!}" type="hidden" name="parent_dep" id="parent_dep" class="form-control">

            <input value="{!!$sub_dep!!}" type="hidden" name="sub_dep" id="sub_dap" class="form-control">
            @if($app == 1)
            <input value="{!!$app!!}" type="hidden" name="app" id="app" class="form-control">
            @endif
            @if($document == 2)
            <input value="{!!$document!!}" type="hidden" name="document" id="document" class="form-control">
            @endif
            <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-save"></i> چاپ اکسیل</button>
        </form>
        
    </legend>
	<br>
	<div class="table-responsive" id="ajaxContent">
		<table class="table" id="list">
			<thead>
				<tr>
                    <th>#</th>
	                <th>{!!_('barcode')!!}</th>
                    <th>{!!_('source_org_checkout_number')!!}</th>
                    <th>{!!_('checkin_number')!!}</th>
                    <th>{!!_('checkin_date')!!}</th>
                    <th>{!!_('executive_department')!!}</th>
                    <th>{!!_('source_organization')!!}</th>
                    <th>{!!_('document_status')!!}</th>
                    <th>{!!_('approved_by')!!}</th>
                    <th>{!!_('duration')!!}</th>
                    <th>{!!_('operation')!!}</th>
	            </tr>
			</thead>
			<tbody>

                <?php $counter = 1; ?>
                @foreach($rows AS $item)
                    <tr>
                    <td>{!!$counter!!}</td>
                    <td>{!!$item->barcode!!}</td>
                    <td>{!!$item->external_number!!}</td>
                    <td>{!!$item->internal_number!!}</td>
                    <td>
                    	<?php 
                    		if(isMiladiDate())
                    		{
                    			$date = explode('-',$item->internal_date);
								
								echo dateToMiladi($date[0],$date[1],$date[2]);
                    		}
                    		else
                    		{
                    			echo $item->internal_date;
                    		}
                    	?>
                    </td>
                    <td>{!!$item->executive_department!!}</td>
                    <td>{!!$item->organization!!}</td>
                    <td>{!!$item->status!!}</td>
                    <td>{!!$item->approved_by!!}</td>
                    <td>{!!$item->days!!}</td>
                    <td>
                        <a href="{!!route('getDocDetails',array('docs',$item->id))!!}" class="table-link">
                            <span class="fa-stack">
                                <i class="fa fa-square fa-stack-2x"></i>
                                <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </td>
                    </tr>
                    <?php $counter++; ?>
                @endforeach
				
			</tbody>
		</table>
		<div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
			{!!$rows->render()!!}
		</div>
	</div>

</fieldset>


<script>
	$( document ).ready(function() {
		$('.pagination a').on('click', function(event) {
			event.preventDefault();
			if ($(this).attr('href') != '#') {
				//$('#ajaxContent').load($(this).attr('href'));
				
				$.ajax({
		                url: '{!!URL::route("getSearchResult")!!}',
		                data: $('#search_fields').serialize()+"&page="+$(this).text()+"&ajax=1",
		                type: 'post',
		                beforeSend: function(){
		
		                    //$("body").show().css({"opacity": "0.5"});
		                    $('#ajaxContent').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
		                },
		                success: function(response)
		                {
		
		                    $('#ajaxContent').html(response);
		                }
		            }
		        );
			
			}
		});
	});
</script>


<script type="text/javascript">
// $(document).ready(function() {

// 	var data = $('#search_fields').serializeArray().reduce(function(obj, item) {
//     	obj[item.name] = item.value;
//     	return obj;
// 	}, {});

// var t= $('#list').dataTable( 
// {
//         "ajax": {
//             "url": "{!!URL::to('/docscom/getSearchResultData')!!}",
//             "type": "POST",
//             "data": data
//         }, 

//         'sDom': 'lf<"clearfix">tip',
//         "bProcessing": true,
//         "bServerSide": true,
//         "iDisplayLength": 10,
//         "sServerMethod": "POST",
//         "bJQueryUI": true,
//         "sAjaxSource": "{!!URL::to('/docscom/getSearchResultData')!!}",
//         //"aaSorting": [[ 1, "desc" ]],
//         // "aoColumns": [
//         //     { 'sWidth': '50px' },
//         //     { 'sWidth': '250px', 'sClass': 'center' },
//         //     { 'sWidth': '250px', 'sClass': 'center' },
//         //     { 'sWidth': '250px', 'sClass': 'center' },
//         //     { 'sWidth': '250px', 'sClass': 'center' },
//         //     { 'sWidth': '250px', 'sClass': 'center' },
//         //     { 'sWidth': '120px', 'sClass': 'center' },
//         //     { 'sWidth': '100px', 'sClass': 'center' }
//         // ],
//         "language": {
//                     "lengthMenu": "{!!_('view')!!} _MENU_ {!!_('record_per_page')!!}",
//                     "zeroRecords": "{!!_('record_not_found')!!}",
//                     "info": "{!!_('page_view')!!} _PAGE_ {!!_('of')!!} _PAGES_",
//                     "infoEmpty": "{!!_('record_not_found')!!}",
//                     "search": "{!!_('search')!!}",
//                     "infoFiltered": "(filtered {!!_('of')!!} _MAX_ {!!_('total_record')!!})"
//                 }
//     }
// );

// });

$(document).ready(function() {
    
    // $('#list').dataTable({

    //         'sDom': 'lf<"clearfix">tip',
    //         "iDisplayLength": 10,
    //         "language": {
    //                 "lengthMenu": "{!!_('view')!!} _MENU_ {!!_('record_per_page')!!}",
    //                 "zeroRecords": "{!!_('record_not_found')!!}",
    //                 "info": "{!!_('page_view')!!} _PAGE_ {!!_('of')!!} _PAGES_",
    //                 "infoEmpty": "{!!_('record_not_found')!!}",
    //                 "search": "{!!_('search')!!}",
    //                 "infoFiltered": "(filtered {!!_('of')!!} _MAX_ {!!_('total_record')!!})"
    //             }
    //     });
} );
</script>

