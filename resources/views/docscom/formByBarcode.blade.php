@extends('layouts.master')

@section('head')
{!! HTML::style('/css/template/libs/nifty-component.css') !!}
    <title>{!!_('document_by_barcode')!!}</title>
@stop
@section('content')
<style type="text/css">
.project-img-owner {
border-radius: 50%;
background-clip: padding-box;
display: block;
float: left;
height: 40px;
padding: 3px;
overflow: hidden;
width: 40px;
}
img {
vertical-align: middle;
}
img {
border: 0;
}
</style>



<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">{!!_('dashboard')!!}</a></li>
            <li class="active"><span>{!!_('document_by_barcode')!!}</span></li>
        </ol>
        
        <h1>{!!_('document_by_barcode')!!}</h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>{!!_('barcode')!!}</h2>
                
                @if(Session::has('success'))
                    <div class='alert alert-success'>
                        <i class="fa fa-check-circle fa-fw fa-lg"></i>
                        {!!Session::get('success')!!}
                    </div>
                @elseif(Session::has('fail'))
                    <div class='alert alert-danger'>
                        <i class="fa fa-times-circle fa-fw fa-lg"></i>
                        {!!Session::get('fail')!!}
                    </div>
                 @endif 
                 <a href="{!!URL::to('/docscom/getForm')!!}" class="btn btn-success pull-right">
                    <i class="fa fa-chevron-left fa-lg"></i> {!!_('back')!!}
                </a>     
            </header>
            
            <div class="main-box-body clearfix">
                <!-- <form class="form-horizontal" role="form" method="post" action="{!!URL::route('docsPostForm')!!}"> -->
                @if(canSearch('docscom_docscom'))
                <div class = "form-horizontal">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('barcode')!!} :</label>
                            
                            <div class="input-group col-sm-4">
                                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                <input onkeypress='return showForm(event)' type="text" class="form-control" id="barcode" name="barcode" placeholder="Scan Barcode Here...">
                                
                                <span style='color:red'>
                                @if($errors->has("barcode"))
                                    {!! $errors->first('barcode') !!}
                                @endif
                                </span>
                            
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            
                            <div class="col-sm-10">
                                <div style='display:none;width:50%' class="alert alert-danger" id='barcode_validate_div'>
                                    <i class="fa fa-times-circle fa-fw fa-lg"></i>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <span id='barcode_validate'></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="approved_div" class='pull-right' style='display:none;'>
                        <div class="alert alert-success pull-right">
                            <i class="fa fa-check-circle fa-fw fa-lg"></i>
                            {!!_('document_approved')!!}
                        </div>
                    </div>
                    <div id = "entry_details" style="display:none;">
                        
                    </div>

                </div>
                @endif
                
            </div>
        </div>
    </div>
</div>

<!-- <div class="row" id="row_comments" style="display:none;">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-box clearfix">
                    <header class="main-box-header clearfix">
                        <h2 class="pull-left value red">Comments</h2>
                    </header>
                    <div class="main-box-body clearfix">
                        <div class="conversation-wrapper">
                            <div class="conversation-content" id="comments_div">
                                
                            </div>
                            <br><br>
                            
                            <div class="conversation-new-message">
                                <form class="comment_form">
                                    <input type="hidden" name="doc_id" id="doc_id">
                                    <div class="form-group">
                                        <textarea name="comment" id="comment" class="form-control" rows="2" placeholder="Enter New Comment..."></textarea>
                                    </div>
                                    
                                    <div class="clearfix">
                                        <button type="button" class="btn btn-success pull-right" onclick='postDocComment()'>Post Comment</button>
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->


@stop
@section('footer-scripts')

<script type="text/javascript">
function approveDocument(doc_id)
{
    var confirmed = confirm("Do you want to approve this document?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("approveDocument")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                dataType:'json',
                // beforeSend: function(){
                //     $("#approved_div").show();
                //     //$("#entry_details").html("LOADING...");
                //     $("#approved_div").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                // },
                success: function(response)
                {
                    if(response.cond == 'true')
                    {
                        $("#approve_btn").hide();
                        $("#approved_div").show();
                    }

                }
            }
        );
    }
}

function postDocComment()
{   
    $.ajax({
        url     : "{!!URL::route('postDocComment')!!}",
        type    : "post",
        data    : $("form.comment_form").serialize(),
        success : function(result){
            $('#comments_div').append(result);
            $('#comment').val('');
        },
        error  : function( xhr, err ){
            alert(err);     
        }
    });    
    return false;
}

function showForm(e) 
{
    if (e.keyCode == 13) 
    {
        var barcodeValue = document.getElementById('barcode').value;
        if(barcodeValue == '')
        {
            $("#entry_details").slideUp();
            $("#row_comments").hide();
            $('#barcode').focus();
            $('#barcode').css('border-color','red');
            return false;
        }
        else
        {
            
            //validate form in the database via ajax
            $.ajax({
                url: '{!!URL::route("findBarcodeDetails")!!}',
                data: '&barcode='+barcodeValue,
                type: 'post',
                beforeSend: function(){
                    $("#entry_details").show();
                    //$("#entry_details").html("LOADING...");
                    $("#entry_details").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                dataType: 'json',
                success: function(response){
                    
                    if(response.cond == 'false')
                    {
                        $("#entry_details").hide();
                        $('#row_comments').hide();
                        $('#barcode_validate_div').show();
                        $('#barcode_validate').html('بارکد موجود نیست!');
                        //alert('<div class="alert-danger">This barcode is not validate</div>');
                        return false;
                    }
                    else
                    {
                        $('#row_comments').show();
                        $('#barcode_validate_div').hide();
                        $("#entry_details").slideDown();
                        $("#entry_details").html(response.details);
                        $("#comments_div").html(response.comments);
                        if(response.checkOuted == 'true')
                        {
                            $('#barcode_validate_div').show();
                            $('#barcode_validate').html('سند قبلا وارد گردیده!');
                        }
                        
                        var formId = $('#form_id').val();
                        $('#doc_id').val(formId);
                        
                        return false;
                    }
                }

            });
        }

        return false;
    }
}
</script>

<script type="text/javascript">
    
        
    $(function(){
        $('#barcode').focus();
    })
     
</script>
@stop

