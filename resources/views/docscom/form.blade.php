@extends('layouts.master')

@section('head')
{!! HTML::style('/css/template/libs/nifty-component.css') !!}
    <title>{!!_('document_entry')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">{!!_('left_dashboard')!!}</a></li>
            <li class="active"><span>{!!_('docscom')!!}</span></li>
        </ol>

        <h1>{!!_('document_details')!!}</h1>
    </div>
</div>

<div class="row">
<div class="col-lg-12">
<div class="main-box">
<header class="main-box-header clearfix">
    
    <?php //echo "<pre>"; print_r(Session::get('user_roles')); ?>

    @if(canCheckOut('docscom_docscom'))
    <a href="{!!URL::route('getCheckoutForm')!!}" style="margin-left:5px;margin-right:5px;" class="btn btn-warning pull-right">
        <i class="fa fa-upload fa-lg"></i> {!!_('checkout')!!}
    </a>
    @endif

    @if(canSearch('docscom_docscom'))
    <a href="{!!URL::route('getBarcodeForm')!!}" class="btn btn-warning pull-right">
        <i class="fa fa-download fa-lg"></i> {!!_('checkin')!!}
    </a>
    @endif

    @if(Session::has('success'))
    <div class='alert alert-success'>
        <i class="fa fa-check-circle fa-fw fa-lg"></i>
        {!!Session::get('success')!!}
    </div>
    @elseif(Session::has('fail'))
    <div class='alert alert-danger'>
        <i class="fa fa-times-circle fa-fw fa-lg"></i>
        {!!Session::get('fail')!!}
    </div>
    @endif

</header>

<div class="main-box-body clearfix">
@if(canAdd('docscom_docscom'))
<form class="form-horizontal" role="form" method="post" action="{!!URL::route('docsPostForm')!!}" enctype="multipart/form-data">
    <div class="row">
        <div class="form-group">
            <label class="col-sm-1 control-label">{!!_('barcode')!!} :</label>
            <div class="input-group col-sm-8">
                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                <input onkeypress='return showForm(event)' type="text" class="form-control barcode_field" id="barcode" name="barcode">
                <span style='color:red'>
                @if($errors->has("barcode"))
                    {!! $errors->first('barcode') !!}
                @endif
                </span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-1 control-label">&nbsp;</label>

            <div class="col-sm-10">
                <div style='display:none;' class="alert alert-danger" id='barcode_validate_div'>
                    <i class="fa fa-times-circle fa-fw fa-lg"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span id='barcode_validate'></span>
                </div>
            </div>
        </div>

    </div>

    <div id = "entry_details" style="display:none;">

        <fieldset style="border: 1px solid #eee;border-radius: 5px;margin-bottom: 20px;">
            <div class="row" style="text-align: center;">
                <div class="form-group">
                    <div class="col-sm-2">
                        <div class="radio">
                            <input value='1' id='application' name="doc_type" type="radio">
                            <label for="application">
                                {!!_('application')!!}
                            </label>
                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="radio">
                            <input value='2' id='document' name="doc_type" type="radio">
                            <label for="document">
                                {!!_('document_maktob')!!}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="app_related_div" style="text-align: center;display: none;">
                <div class="form-group">
                    <div class="col-sm-2">
                        <div class="radio">
                            <input value='2' id='aop' name="app_type" type="radio">
                            <label for="aop">
                                {!!_('aop')!!}
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="radio">
                            <input value='1' id='external_org' name="app_type" type="radio">
                            <label for="external_org">
                                {!!_('ministries')!!}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="doc_related_div" style="text-align: center;display: none;">
                <div class="form-group">
                    <div class="col-sm-2">
                        <div class="radio">
                            <input value='2' id='export' name="export_import" type="radio">
                            <label for="export">
                                {!!_('checkout')!!}
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="radio">
                            <input value='1' id='import' name="export_import" type="radio">
                            <label for="import">
                                {!!_('checkin')!!}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>

        <!-- original form start -->
        <div id="original_form" style="display: none;">
        <fieldset><legend>{!!_('source_organization')!!}</legend>
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('organization_name')!!} :</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" name="organization">
                    </div>
                    <label class="col-sm-2 control-label">{!!_('sadira_number')!!} :</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" name="external_number">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('sadira_date')!!} :</label>
                    <div class="col-sm-4">
                        <input class="form-control {!!getDatePickerClass()!!}" type="text" name="external_date">
                    </div>
                    <!-- <label class="col-sm-2 control-label">ایمیل :</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" name="email">
                    </div> -->
                </div>
            </div>
            <!-- <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label">نمبر موبایل :</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" name="phone">
                    </div>

                </div>
            </div> -->
        </fieldset>

        <fieldset><legend>{!!_('document_and_communication')!!}</legend>
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('checkin_number')!!} :</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" name="internal_number">
                    </div>
                    <label class="col-sm-2 control-label">{!!_('checkin_date')!!} :</label>
                    <div class="col-sm-4">
                        <input class="form-control {!!getDatePickerClass()!!}" type="text" name="internal_date">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('summary')!!} :</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="5" id="wysiwig_full" name="summary"></textarea>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('executive_department')!!} :</label>
                <div class="col-sm-10">
                    <select class="form-control" name="executive_department" id="executive_department">
                        <option value=''>{!!_('select')!!}</option>
                        @foreach($departments AS $dep)
                        <option value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                        @endforeach

                    </select>
                </div>
            </div>

            <div class="form-group">
                @if(isConfidential('docscom_docscom'))
                <label class="col-sm-2 control-label">{!!_('document_type')!!} :</label>
                <div class="col-sm-4">

                    <div class="row form-group">

                        <div class="col-sm-10">
                            <div class="checkbox-nice">
                                <input type="checkbox" id="access" name="access">
                                <label for="access">
                                    {!!_('other')!!}
                                </label>
                            </div>

                        </div>
                    </div>
                </div>
                @endif
                <!-- <div class="col-sm-4">
                    <div class="row form-group">
                        <div class="col-sm-12">
                            <div class="radio">
                                    <input value='1' id='confidential' name="access" type="radio">

                                <label for="confidential">
                                    Confidential
                                </label>
                            </div>
                            <div class="radio">

                                    <input value='2' name="access" id='non_confidential' type="radio">

                                <label for="non_confidential">
                                    Non Confidential
                                </label>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </fieldset>
        </div>
        <!-- original form end -->


        <!-- Annex A form start -->
        <div id="annex_a" style="display: none;">
            <fieldset>
                <legend>{!!_('checkout_documents')!!}</legend>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{!!_('source_organization')!!}:</label>
                        <div class="col-sm-6">
                            <input class="form-control" type="text" name="doc_source">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{!!_('summary')!!}:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="6" id="wysiwig_full" name="doc_summary"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{!!_('executive_department')!!} :</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="doc_executive_department" id="doc_executive_department">
                                <option value=''>{!!_('select')!!}</option>
                                @foreach($departments AS $dep)
                                <option value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{!!_('checkout_date')!!} :</label>
                        <div class="col-sm-6">
                            <input class="form-control {!!getDatePickerClass()!!}" type="text" name="doc_external_date">
                        </div>
                        <!-- <label class="col-sm-2 control-label">ایمیل :</label>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" name="email">
                        </div> -->
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{!!_('is_answer')!!}؟</label>
                        <div class="col-sm-4">
                            <div class="row form-group">
                                <div class="col-sm-10">
                                    <div class="radio">
                                        <input value='1' type="radio" id="doc_ans_yes" name="doc_answer">
                                        <label for="doc_ans_yes">
                                            {!!_('yes')!!}
                                        </label>
                                        <input placeholder="شماره سند مربوطه ..." type="text" name="doc_yes" id="doc_yes" style="display: none;" />
                                    </div>
                                    <div class="radio">
                                        <input value='0' type="radio" id="doc_ans_no" name="doc_answer">
                                        <label for="doc_ans_no">
                                            {!!_('no')!!}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <!-- Annex A form end -->


        <!-- Annex B form start -->
        <div id="annex_b" style="display: none;">
            <fieldset>
                <legend>{!!_('aop')!!}</legend>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{!!_('number')!!}:</label>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" name="aop_number">
                        </div>

                        <label class="col-sm-2 control-label">{!!_('date')!!}:</label>
                        <div class="col-sm-4">
                            <input class="form-control {!!getDatePickerClass()!!}" type="text" name="aop_date">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{!!_('applicant_department')!!}:</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="aop_app_department" id="aop_app_department">
                                <option value=''>{!!_('select')!!}</option>
                                @foreach($departments AS $dep)
                                <option value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{!!_('application_subject')!!}:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="6" id="wysiwig_full" name="aop_app_subject"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{!!_('executive_department')!!}:</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="aop_executive_department" id="aop_executive_department">
                                <option value=''>{!!_('select')!!}</option>
                                @foreach($departments AS $dep)
                                <option value="{!!$dep->id!!}">{!!$dep->name!!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <!-- Annex B form end -->

        <div class="form-group" style="display: none;" id="attachs_div">
            <label class="col-sm-2 control-label">{!!_('attachment')!!}</label>
            <div class="col-sm-4" id='files_div' style='width:465px;'>
                <input style='width:400px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
            </div>

        </div>
        {!! Form::token() !!}

        <div class="form-group">
            <label class="col-sm-2 control-label">&nbsp;</label>
            <div class="col-sm-10">

                <button class="btn btn-primary" type="submit">{!!_('save')!!}</button>

                <button onclick="$('#entry_details').slideUp();" class="btn btn-danger" type="button">{!!_('cancel')!!}</button>
            </div>
        </div>
    </div>

</form>
@endif
@if(canView('docscom_docscom'))
<button class="btn btn-primary" onclick="$('#list_view').toggle('slide');">{!!_('document_list')!!}</button>
<div id="list_view" style="display:none;">
    <fieldset><legend>{!!_('all_documents')!!}</legend>
        <div class="table-responsive">
            <table class="table table-hover dataTable table-striped width-full" id='list'>
                <thead>
                <tr>
                    
                    <th>{!!_('barcode')!!}</th>
                    <th>{!!_('source_org_checkout_number')!!}</th>
                    <th>{!!_('checkin_number')!!}</th>
                    <th>{!!_('checkin_date')!!}</th>
                    <th>{!!_('executive_department')!!}</th>
                    <th>{!!_('source_organization')!!}</th>
                    <th>{!!_('document_status')!!}</th>
                    <th>{!!_('approved_by')!!}</th>
                    <th>{!!_('duration')!!}</th>
                    <th>{!!_('operation')!!}</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </fieldset>
</div>
@endif

</div>
</div>
</div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">

$(document).ready(function() {
    $(".barcode_field").on("keypress", function(event) {

        // Disallow anything not matching the regex pattern (A to Z uppercase, a to z lowercase, digits 0 to 9 and white space)
        // For more on JavaScript Regular Expressions, look here: https://developer.mozilla.org/en-US/docs/JavaScript/Guide/Regular_Expressions
        var englishAlphabetDigitsAndWhiteSpace = /[0-9 a-z A-Z -]/g;
       
        // Retrieving the key from the char code passed in event.which
        // For more info on even.which, look here: http://stackoverflow.com/q/3050984/114029
        var key = String.fromCharCode(event.which);
        
        //alert(event.keyCode);
        
        // For the keyCodes, look here: http://stackoverflow.com/a/3781360/114029
        // keyCode == 8  is backspace
        // keyCode == 37 is left arrow
        // keyCode == 39 is right arrow
        // englishAlphabetDigitsAndWhiteSpace.test(key) does the matching, that is, test the key just typed against the regex pattern
        if (event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 37 || event.keyCode == 39 || englishAlphabetDigitsAndWhiteSpace.test(key)) {
            //$("#barcode_validate_div").hide();
            return true;
        }

        // If we got this far, just return false because a disallowed key was typed.
         //alert('Please change your keyboard language to english and try again!');
         $("#barcode_validate_div").show();
         $("#barcode_validate").text("Please change your keyboard language to english and try again!");

        return false;
    });

    $('.barcode_field').on("paste",function(e)
    {
        e.preventDefault();
    });
});
</script>

<script>
    $(function(){
        $('#application').on('change',function(){
            if($('#application').is(':checked'))
            {
                $('#app_related_div').slideDown();
                $('#doc_related_div').hide();
            }
        })

        $('#document').on('change',function(){
            if($('#document').is(':checked'))
            {
                $('#app_related_div').hide();
                $('#doc_related_div').slideDown();
            }
        });

        $('#doc_ans_yes').on('change',function(){
            if($('#doc_ans_yes').is(':checked'))
            {
                $('#doc_yes').show();
            }
        });

        $('#doc_ans_no').on('change',function(){
            if($('#doc_ans_no').is(':checked'))
            {
                $('#doc_yes').hide();
            }
        });

        $('#external_org, #import').on('change',function(){
            if($('#external_org, #import').is(':checked'))
            {
                $('#original_form').slideDown();
                $('#attachs_div').show();
                $('#annex_a').hide();
                $('#annex_b').hide();
            }
        });
        $('#aop').on('change',function(){
            if($('#aop').is(':checked'))
            {
                $('#original_form').hide();
                $('#annex_a').hide();
                $('#annex_b').slideDown();
                $('#attachs_div').show();
            }
        });

        $('#export').on('change',function(){
            if($('#export').is(':checked'))
            {
                $('#original_form').hide();
                $('#annex_a').slideDown();
                $('#attachs_div').show();
                $('#annex_b').hide();
            }
        });

    });
</script>

<script type="text/javascript">
    $(document).on("change","#files",function(){
        var count = $('#files_div').find('input').length;
        count = count+1;
        $("<div id='div_"+count+"' style='display:inline;'><input style='width:400px;splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div');
    });
    function showForm(e)
    {
        if (e.keyCode == 13)
        {
            var barcodeValue = document.getElementById('barcode').value;
            if(barcodeValue == '')
            {
                $("#entry_details").slideUp();
                $('#barcode').focus();
                $('#barcode').css('border-color','red');
                return false;
            }
            else
            {
                //validate form in the database via ajax
                $.ajax({
                    url: '{!!URL::route("docsValidateForm")!!}',
                    data: '&barcode='+barcodeValue,
                    type: 'post',
                    dataType: 'json',
                    success: function(response){

                        if(response.cond == 'true')
                        {
                            $('#barcode_validate_div').show();
                            $('#barcode_validate').html('بارکد مجاز نیست');
                            //alert('<div class="alert-danger">This barcode is not validate</div>');
                            return false;
                        }
                        else
                        {
                            $("#entry_details").slideDown();
                            return false;
                        }
                    }

                });


            }

            return false;
        }


    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "{!!URL::to('/docscom/getDocsListData')!!}",
                "language": {
                    "lengthMenu": "{!!_('view')!!} _MENU_ {!!_('record_per_page')!!}",
                    "zeroRecords": "{!!_('record_not_found')!!}",
                    "info": "{!!_('page_view')!!} _PAGE_ {!!_('of')!!} _PAGES_",
                    "infoEmpty": "{!!_('record_not_found')!!}",
                    "search": "{!!_('search')!!}",
                    "infoFiltered": "(filtered {!!_('of')!!} _MAX_ {!!_('total_record')!!})"
                }
            }
        );

    });
</script>
@stop


