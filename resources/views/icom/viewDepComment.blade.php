@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('departure_comment')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
        span{
            font-weight: bold;
        }
    </style>

@stop

@section('content')

@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('getLoadDepList') !!}">{!!_('departure_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('view_departure_comment')!!}</span>
                    </li>
                </ol>
                <h3>{!!_('comment_and_attachments_of_departure')!!}</h3>
			<hr />
            </div>
        
		</div>    
	</div>

    <div class="container">
    @if(count($deps)>0)
        @foreach($deps AS $item)
        	<?php
				
				if(isShamsiDate()){
					
					$travel_date = checkEmptyDate($item->travel_date);
					$return_date = checkEmptyDate($item->return_date);
				}
				else{
					$travel_date = checkGregorianEmtpyDate($item->travel_date);
					$return_date = checkGregorianEmtpyDate($item->return_date);
				}

			?>
            <legend><b>{!!_('departure_details')!!}</b></legend>
            <table class="table pull-right">
                <tr>
                    <td>{!!_('name')!!} :</td>
                    <td>
                        <span>{!!$item->name!!}</span>
                    </td>
                    <td>{!!_('occupation')!!} :</td>
                    <td>
                        <span>{!!$item->job!!}</span>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('office')!!} :</td>
                    <td>
                        <span>{!!$item->office!!}</span>
                    </td>
                    <td>{!!_('travel_date')!!} :</td>
                    <td>
                        <span>{!!$travel_date!!}</span>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('travel_country')!!} :</td>
                    <td>
                        <span>{!!$item->country!!}</span>
                    </td>
                    <td>{!!_('return_date')!!} :</td>
                    <td>
                        <span>{!!$return_date!!}</span>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('statement_number')!!} :</td>
                    <td>
                        <span>{!!$item->statement_number!!}</span>
                    </td>
                    <td>{!!_('chart_number')!!} :</td>
                    <td>
                        <span>{!!$item->chart_number!!}</span>
                    </td>
                </tr>

            </table>

            <fieldset>
                <legend><b>{!!_('comments_and_attachments')!!}</b></legend>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="main-box clearfix">
                        <header class="main-box-header clearfix">
                            <h3>
                                <i class="fa fa-comment fa-fw fa-lg" style='color:#03a9f4;'></i>
                                {!!_('comment')!!}
                            </h3>
                        </header>
                        
                        <div class="main-box-body clearfix">
                            <ul class="widget-todo" style="margin-top:10px;list-style:none">
                                @if(count($depComment)>0)
                                    @foreach($depComment AS $comment)
                                    <li class="clearfix">
                                        <div class="name">
                                            <label for="todo-2">
                                                <strong>{!!$comment->comment!!}</strong>
                                            </label>
                                        </div>
                                    </li>
                                    @endforeach
                                @else
                                    <li><span style='color:red;'>{!!_('no_comment_for_this_travel_!')!!} </span></li>
                                @endif
                            </ul>
                                
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="main-box clearfix">
                        <header class="main-box-header clearfix">
                            <h3>
                                <i class="fa fa-upload fa-fw fa-lg" style='color:#03a9f4;'></i>
                                {!!_('uploaded_attachments')!!}
                            </h3>
                        </header>
                        
                        <div class="main-box-body clearfix">
                            <ul class="widget-todo" style="margin-top:10px;list-style:none">
                                @if(count($depAttachments)>0)
                                    @foreach($depAttachments AS $attach)
                                    <li class="clearfix" id="li_{!!$attach->id!!}">
                                        <div class="name">
                                            <label for="todo-2">
                                                <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
                                                <strong>{!!$attach->file_name!!}</strong>
                                            </label>
			  &nbsp;&nbsp;
			  <a href="{!!URL::route('getIComDownload',array($attach->id))!!}" class="table-link success">
                                                <i class="fa fa-cloud-download" style='color:#03a9f4;' title="download file"></i>
                                            </a>
			  <a href="javascript:void()" onclick="removeIComFile('{!!$attach->id!!}','li_{!!$attach->id!!}')" class="table-link danger">
                                                <i class="fa fa-trash-o" style='color:red;'></i>
                                            </a>
                                        </div>                                    
		        </li>
                                    @endforeach
                                @else
                                    <li><span style='color:red;'>{!!_('no_uploaded_document_!')!!}</span></li>
                                @endif
                            </ul>
                                
                        </div>
                    </div>
                </div>

            </fieldset>

        @endforeach
    @endif
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">


</script>

@stop