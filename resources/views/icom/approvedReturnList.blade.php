@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('approved_returns')!!}</title>
    <style type="text/css">
        table thead tr th
        {
            text-align: center;
            border-color: #000; 
        }
        table tbody tr td
        {
            border-color: #000;
        }
        a#ToolTables_returnList_0{
            padding: 5px 10px !important;
            text-decoration: none;
            font-weight: bold;
            float: right;
        }
        a#ToolTables_returnList_0:hover{
            box-shadow: 0 0 5px #888;
            text-decoration: none;
        }
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop


@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success span6 noprint'>{!!Session::get('success')!!}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger span6 noprint'>{!!Session::get('fail')!!}</div>
    @endif
    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="pull-left">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{!! URL::route('home') !!}">{!!_('Dashboard')!!}</a>
                        </li>
                        <li class="active">
                            <span>{!!_('approved_returns')!!}</span>
                        </li>
                    </ol>
                    <h3>{!!_('approved_return_list')!!}</h3>
                </div>
            </div>
        </div>
        <div style="padding:15px">
            <table class="table table-bordered" id="returnList">
                <thead>
                  <tr>
                    <th>{!!_('no#')!!}</th>
                    <th>{!!_('name')!!}</th>
                    <th>{!!_('job')!!}</th>
                    <th>{!!_('office')!!}</th>
                    <th>{!!_('return_date')!!}</th>
                    <th>{!!_('country')!!}</th>
                    <th>{!!_('passport_num')!!}</th>
                    <th>{!!_('airline')!!}</th>
                    <th>{!!_('email')!!}</th>

                    <!-- <th>Operations</th> -->
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>

        </div>
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#returnList').dataTable(
        {

            "sDom": 'Tlfr<"clearfix">tip',
            //"sDom": 'lrf<"clear spacer">Ttip',
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "print",
                        "bShowAll": true
                    }
                ]
            },
            "bProcessing": false,
            "bServerSide": false,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getApprovedReturnsData')!!}"
        }
    );
    
	$("#returnList th").css({'font-size':'15px','font-weight':"normal"});
    $("#returnList td").css({'font-size':'14px'});
    $("#ToolTables_returnList_0").html("{!!_('print_view')!!}");
    $("#ToolTables_returnList_0").addClass('btn btn-success');
    $("#ToolTables_returnList_0").click(function(){
        $("#returnList th").removeClass('sorting');
        $("#returnList th").css({'font-size':'15px','font-weight':'bold'});
        $("#returnList td").css({'font-size':'14px'});
    });
});

$(document).keyup(function(e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
	    $("#returnList th").addClass('sorting');
	    $("#returnList th").css({'font-size':'15px','font-weight':'normal'});
	    $("#returnList td").css({'font-size':'14px'});
    }
});
    // Approve the return entry through ajax script.
    function approveReturn(tagID, rowID)
    {

        $.ajax({

            url: '{!!URL::route("getApproveReturn")!!}',
            type: 'POST',
            data: 'return_id='+rowID,
            beforeSend: function(){
                $("."+tagID+rowID).html('<span style="align:center;">processing ...</span>');
            },
            success: function(response)
            { 
                $('.'+tagID+rowID).html(response);
            }

        });

    }

</script>

@stop


