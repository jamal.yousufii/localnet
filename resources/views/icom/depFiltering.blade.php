@extends('layouts.master')

@section('head')
    @parent
    <title>{!!_('search_departures')!!}</title>
    <style type="text/css">
    
        a#ToolTables_depList_0{
        padding: 5px 10px !important;
        text-decoration: none;
        font-weight: bold;
        float: right;
	    }
	    a#ToolTables_depList_0:hover{
	        box-shadow: 0 0 5px #888;
	        text-decoration: none;
	    }
	    a.btn{
	    	text-decoration: none;
	    }
	    
    </style>
@stop

@section('content')


<div class="row" style="opacity: 1;">
    <div class="col-lg-12 noprint">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('home') !!}">{!!_('Dashboard')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('search_departures')!!}</span>
                    </li>
                </ol>
                <h3>{!!_('departures_filtering_by_date')!!}</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row noprint" id="search">
           
            <div class="col-xs-12 col-sm-12">
                <div class="box ui-draggable ui-droppable">

                    <div class="box-content">

                        <form id="search_form" role="form">

                            <table class="table" style="margin-top:10px">

                                <tr>
                                    <td>{!!_('start_date')!!} :</td>
                                    <td>
                                        <input class="{!!getDatePickerClass()!!} form-control" type="text" name="start_date" readonly="readonly">
                                    </td>
                                    <td>{!!_('end_date')!!} :</td>
                                    <td>
                                        <input class="{!!getDatePickerClass()!!} form-control" type="text" name="end_date" readonly="readonly">
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary" onclick="bringFilteredDepartures('search_result')"><i class="fa fa-search fa-lg"></i> {!!_('search')!!}</a>
                                    </td>
                                </tr>

                            </table>
                        </form>

                    </div>

                </div>
            </div>
        </div>
        <hr style="border: 1px dashed #b6b6b6" />
        <div id="search_result">
            <h4>{!!_('last_week_travel_information')!!}</h3><hr>
            <table class="table table-bordered table-responsive" id="depList">
                <thead>
                  <tr>
                    <th>{!!_('no#')!!}</th>
                    <th>{!!_('name')!!}</th>
                    <th>{!!_('job')!!}</th>
                    <th>{!!_('office')!!}</th>
                    <th>{!!_('date')!!}</th>
                    <th>{!!_('country')!!}</th>
                    <th>{!!_('return_date')!!}</th>
                    <th>{!!_('statement_num')!!}</th>
                    <th>{!!_('chart_num')!!}</th>
                    <th>{!!_('comment')!!}</th>

                    <!-- <th>Operations</th> -->
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
            
        </div>
    </div>
</div>

@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#depList').dataTable(
        {
            "sDom": 'Tlfr<"clearfix">tip',
            //"sDom": 'lrf<"clear spacer">Ttip',
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "print",
                        "bShowAll": true
                    }
                ]
            },
            "bProcessing": false,
            "bServerSide": false,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getLastWeekDeps')!!}"

        }
    );
    
    $("#ToolTables_depList_0").html("{!!_('print_view')!!}");
    $("#ToolTables_depList_0").addClass('btn btn-success');
    $("#ToolTables_depList_0").click(function(){
        $("#depList th").removeClass('sorting');
        $("#depList th").css({'font-size':'13px','font-weight':'bold'});
        $("#depList td").css({'font-size':'12px'});
    });

});

    function bringFilteredDepartures(div)
    {

        $.ajax({
                url: '{!!URL::route("getFilteredDeps")!!}',
                data: $('#search_form').serialize(),
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="align:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }

</script>

@stop

