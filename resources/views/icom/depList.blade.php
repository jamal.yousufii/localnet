@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('departrue_list')!!}</title>
    <style type="text/css">
        table thead tr th
        {
            text-align: center;
            border-color: #000; 
            text-align: center !important;
        }
        table tbody tr td
        {
            border-color: #000;
            text-align: center !important;
        }
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop


@section('content')

    @if(Session::has('success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('success')!!}
		</div>

    @elseif(Session::has('fail'))
    	<div class="alert alert-danger alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('fail')!!}
		</div>
    @endif
    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="pull-left">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{!! URL::route('home') !!}">{!!_('Dashboard')!!}</a>
                        </li>
                        <li class="active">
                            <span>{!!_('all_departures')!!}</span>
                        </li>
                    </ol>
                    <h3>{!!_('departure_list')!!}</h3>
                </div>
            </div>
        </div>
        <div style="padding:15px" class="table-responsive">
            <table class="table table-bordered table-responsive" id="depList">
                <thead>
                  <tr>
                    <th>{!!_('no#')!!}</th>
                    <th>{!!_('name')!!}</th>
                    <th>{!!_('job')!!}</th>
                    <th>{!!_('office')!!}</th>
                    <th>{!!_('date')!!}</th>
                    <th>{!!_('country')!!}</th>
                    <th>{!!_('return_date')!!}</th>
                    <th>{!!_('statement_num')!!}</th>
                    <th>{!!_('chart_num')!!}</th>

                    <th>{!!_('operations')!!}</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>

            <div style="padding-top:60px">
                <a href="{!!URL::route('addDep')!!}" class="btn btn-primary">{!!_('add_departure')!!}</a> 
            </div>
        </div>
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#depList').dataTable(
        {

            "sDom": 'lfr<"clearfix">tip',
            "bProcessing": true,
            "bServerSide": true,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getAllDeps')!!}"

        }
    );
});
</script>

@stop


