@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('airport_vip_checking')!!}</title>
    <style type="text/css">
        table thead tr th
        {
            text-align: center;
            border-color: #000; 
        }
        table tbody tr td
        {
            border-color: #000;
        }
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop


@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success span6 noprint'>{!!Session::get('success')!!}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger span6 noprint'>{!!Session::get('fail')!!}</div>
    @endif
    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="pull-left">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{!! URL::route('home') !!}">{!!_('Dashboard')!!}</a>
                        </li>
                        <li class="active">
                            <span>{!!_('travels_list')!!}</span>
                        </li>
                    </ol>
                    <h1>{!!_('all_today`s_travels_list')!!}</h1>
                </div>
            </div>
        </div>
        <div style="padding:15px" class="table-responsive">
            <h4 style="text-align:center;font-weight:bold">{!!_('vip_travel_list')!!} &nbsp;<span style="font-size: 12px">{!!_('today')!!} : {!!date('Y-m-d')!!}</span></h4>
            <table class="table table-bordered table-responsive" id="airportCheckList">
                <thead>
                  <tr>
                    <th>{!!_('no#')!!}</th>
                    <th>{!!_('name')!!}</th>
                    <th>{!!_('travel_date')!!}</th>
                    <th>{!!_('return_date')!!}</th>
                    <th>{!!_('statement_number')!!}</th>
                    <th>{!!_('chart_number')!!}</th>

                    <th>{!!_('operations')!!}</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>

            <!-- <div style="padding-top:20px">
                <a href="{!!URL::route('addDep')!!}" class="btn btn-primary">Add Return</a> 
            </div> -->
        </div>
        <!-- <button class="md-trigger btn btn-primary mrg-b-lg" data-modal="commentModal">3D Slit</button> -->
        <!-- ================================ Modal starts ========================================-->

        <div class="md-modal md-effect-13" id="commentModal">
            <div class="md-content">
                <div class="modal-header">
                    <button class="md-close close">&times;</button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label for="exampleTextarea">Textarea</label>
                            <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
                        </div>
                        
                        <div class="form-inline form-inline-box">
                            <div class="form-group">
                                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Active</option>
                                    <option>Inactive</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-link"><i class="fa fa-eye"></i> Preview</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>

        <!-- ================================ Modal ends ========================================-->
        <!-- the overlay element for modal shadow -->
        <div class="md-overlay"></div>
        
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#airportCheckList').dataTable(
        {

            // 'sDom': 'Tlfr<"clearfix">tip',
            // "oTableTools": {
            //     "aButtons": [
            //         {
            //             "sExtends": "print",
            //             "bShowAll": true
            //         }
            //     ]
            // },
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getTravelsListForAirport')!!}"

        }
    );
});
</script>

@stop


