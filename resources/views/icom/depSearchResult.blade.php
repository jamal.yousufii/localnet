<?php
    Session::put('start_date', $start_date);
    Session::put('end_date', $end_date);
?>

<style type="text/css">
    table thead tr th
    {
        text-align: center;
        border-color: #000; 
    }
    table tbody tr td
    {
        text-align: center;
        border-color: #000;
    }
    a#ToolTables_depFilterList_0{
	    padding: 5px 10px !important;
	    text-decoration: none;
	    font-weight: bold;
	    float: right;
    }
    a#ToolTables_depFilterList_0:hover{
        box-shadow: 0 0 5px #888;
        text-decoration: none;
    }
    a.btn{
    	text-decoration: none;
    }
    .fixed{width: 11%;}
    /*table.fixed { table-layout:fixed; }
    table.fixed td { overflow: hidden; }*/
</style>

<div class="table-responsive">
    <table class="table table-bordered table-responsive" id="depFilterList">
        <thead>
          <tr>
            <th>{!!_('no#')!!}</th>
            <th>{!!_('name')!!}</th>
            <th>{!!_('job')!!}</th>
            <th>{!!_('office')!!}</th>
            <th>{!!_('date')!!}</th>
            <th>{!!_('country')!!}</th>
            <th>{!!_('return_date')!!}</th>
            <th>{!!_('statement_num')!!}</th>
            <th>{!!_('chart_num')!!}</th>
            <th>{!!_('comment')!!}</th>

            <!-- <th>Operations</th> -->
            
          </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#depFilterList').dataTable(
        {

            "sDom": 'Tlfr<"clearfix">tip',
            //"sDom": 'lrf<"clear spacer">Ttip',
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "print",
                        "bShowAll": true
                    }
                ]
            },
            "bProcessing": false,
            "bServerSide": false,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getFilteredDepsData')!!}"

        }
    );
    
    $("#depFilterList th").css({'font-size':'15px','font-weight':"normal"});
    $("#depFilterList td").css({'font-size':'14px'});
    $("#ToolTables_depFilterList_0").html("{!!_('print_view')!!}");
    $("#ToolTables_depFilterList_0").addClass('btn btn-success');
    $("#ToolTables_depFilterList_0").click(function(){
        $("#depFilterList th").removeClass('sorting');
        $("#depFilterList th").css({'font-size':'15px','font-weight':'bold'});
        $("#depFilterList td").css({'font-size':'14px'});
    });
});

$(document).keyup(function(e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
	    $("#depFilterList th").addClass('sorting');
	    $("#depFilterList th").css({'font-size':'15px','font-weight':'normal'});
	    $("#depFilterList td").css({'font-size':'14px'});
    }
});
</script>


