@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('departure_form')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
    </style>

@stop

@section('content')

@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('getLoadDepList') !!}">{!!_('departures_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('add_departure')!!}</span>
                    </li>
                </ol>
                <h3>{!!_('departure_form')!!}</h3>
            </div>
        </div>
    </div>

    <div class="container">

        <form role="form" method="post" action="{!! URL::route('postAddDepIcom') !!}" class="form-horizontal">

            <table class="table pull-right">
                <tr>
                    <td>{!!_('name')!!} :</td>
                    <td>
                        <input class="form-control" type="text" name="name" value="{!!old('name')!!}" required />
                    </td>
                    <td>{!!_('occupation')!!} :</td>
                    <td>
                        <input class="form-control" type="text" name="job" value="{!!old('job')!!}">
                    </td>
                </tr>
                <tr>
                    @if($errors->has("name"))
                    <td class="hide_border"></td><td class="hide_border">
                        <span style="color: red">{!! $errors->first('name') !!}</span>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td>{!!_('office')!!} :</td>
                    <td>
                        <input type="text" class="form-control" name="office" value="{!!old('office')!!}" />
                    </td>
                    <td>{!!_('travel_date')!!} :</td>
                    <td>
                        <input type="text" class="{!!getDatePickerClass()!!} form-control" name="travel_date" value="{!!old('travel_date')!!}" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    @if($errors->has("travel_date"))
                    <td class="hide_border"></td><td class="hide_border"></td><td class="hide_border"></td>
                    <td class="hide_border">
                        <span style="color: red">{!! $errors->first('travel_date') !!}</span>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td>{!!_('travel_country')!!} :</td>
                    <td>
                        <input class="form-control" type="text" name="travel_country" value="{!!old('travel_country')!!}">
                    </td>
                    <td>{!!_('return_date')!!} :</td>
                    <td>
                        <input class="{!!getDatePickerClass()!!} form-control" type="text" name="return_date" value="{!!old('return_date')!!}" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    @if($errors->has("return_date"))
                    <td class="hide_border"></td><td class="hide_border"></td><td class="hide_border"></td>
                    <td class="hide_border">
                        <span style="color: red">{!! $errors->first('return_date') !!}</span>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td>{!!_('statement_number')!!} :</td>
                    <td>
                        <input type="number" class="form-control" name="statement_number" value="{!!old('statement_number')!!}" required/>
                    </td>
                    <td>{!!_('chart_number')!!} :</td>
                    <td>
                        <input type="number" class="form-control" name="chart_number" value="{!!old('chart_number')!!}" />
                    </td>
                </tr>
                <tr>
                    @if($errors->has("statement_number"))
                    <td class="hide_border"></td><td class="hide_border">
                        <span style="color: red">{!! $errors->first('statement_number') !!}</span>
                    </td>
                    @endif
                </tr>
    
            </table>
            <div class="form-group">
                <div class="col-sm-12">
                    <hr style="border: 1px dashed #b6b6b6" />
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-check"></i>
                        </span>
                        &nbsp;{!!_('submit')!!}
                    </button>
                    <a href="{!! URL::route('getLoadDepList') !!}" class="btn btn-danger">
                        <span>
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>
                        &nbsp;{!!_('cancel')!!}
                    </a>
                </div>
            </div>
        </form>
    
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     

    });


</script>

@stop