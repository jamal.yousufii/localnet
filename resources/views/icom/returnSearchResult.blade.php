<?php
    Session::put('start_date', $start_date);
    Session::put('end_date', $end_date);
?>
<style type="text/css">
    
    a#ToolTables_returnFilterList_0{
	    padding: 5px 10px !important;
	    text-decoration: none;
	    font-weight: bold;
	    float: right !important;
    }
    a#ToolTables_returnFilterList_0:hover{
        box-shadow: 0 0 5px #888;
        text-decoration: none;
    }
    a.btn{
    	text-decoration: none;
    }
    
</style>
<div class="table-responsive">
    <table class="table table-bordered table-responsive" id="returnFilterList">
        <thead>
          <tr>
            <th>{!!_('no#')!!}</th>
            <th>{!!_('name')!!}</th>
            <th>{!!_('job')!!}</th>
            <th>{!!_('office')!!}</th>
            <th>{!!_('return_date')!!}</th>
            <th>{!!_('country')!!}</th>
            <th>{!!_('passport_num')!!}</th>
            <th>{!!_('airline')!!}</th>
            <th>{!!_('email')!!}</th>

            <!-- <th>Operations</th> -->
            
          </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#returnFilterList').dataTable(
        {

            "sDom": 'Tlfr<"clearfix">tip',
            //"sDom": 'lrf<"clear spacer">Ttip',
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "print",
                        "bShowAll": true
                    }
                ]
            },
            "bProcessing": false,
            "bServerSide": false,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getFilteredReturnsData')!!}"
        }
    );
    $("#returnFilterList th").css({'font-size':'15px','font-weight':"normal"});
    $("#returnFilterList td").css({'font-size':'14px'});
    $("#ToolTables_returnFilterList_0").html("{!!_('print_view')!!}");
    $("#ToolTables_returnFilterList_0").addClass('btn btn-success');
    $("#ToolTables_returnFilterList_0").click(function(){
        $("#returnFilterList th").removeClass('sorting');
        $("#returnFilterList th").css({'font-size':'15px','font-weight':'bold'});
        $("#returnFilterList td").css({'font-size':'14px'});
    });
});

$(document).keyup(function(e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
	    $("#returnFilterList th").addClass('sorting');
	    $("#returnFilterList th").css({'font-size':'15px','font-weight':'normal'});
	    $("#returnFilterList td").css({'font-size':'14px'});
    }
});
</script>


