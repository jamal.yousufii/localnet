@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('departure_comment')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
    </style>

@stop

@section('content')

@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('getLoadAirportPage') !!}">{!!_('airport_check_up_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('comment_and_attachments')!!}</span>
                    </li>
                </ol>
                <h1>{!!_('add_comment_and_attachments')!!}</h1>
            </div>
        </div>
    </div>

    <div class="container">

        <form role="form" method="post" action="{!! URL::route('getCreateComment', array('id' => $id)) !!}" class="form-horizontal" enctype="multipart/form-data" >

            <table class="table pull-right">
                <tr>
                    <td>{!!_('comment')!!} :</td>
                    <td>
                        <textarea cols="20" rows="4" class="form-control" name="comment" value="{!!old('comment')!!}" style='width:600px;'></textarea>
                    </td>
                </tr>
                @if($errors->has("comment"))
                <tr>
                    <td class="hide_border"></td><td class="hide_border">
                        <span style="color: red" dir="ltr">{!! $errors->first('comment') !!}</span>
                    </td>
                </tr>
                @endif
                <tr>
                    <td>{!!_('attachment')!!} :</td>
                    <td>
                        <div class="input_fields_wrap">
                            <input style='width:400px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
                            <a class="add_field_button btn" id="add" style="background: green;color:white;" title="add"> + </a>
                        </div> 
                    </td>
                </tr>
                <tr>
                    @if($errors->has("files[]"))
                    <td class="hide_border"></td><td class="hide_border">
                        <span style="color: red" dir="ltr">{!! $errors->first('files[]') !!}</span>
                    </td>
                    @endif
                </tr>
    
            </table>
            <div class="form-group">
                <div class="col-sm-12">
                    <hr style="border: 1px dashed #b6b6b6" />
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-check"></i>
                        </span>
                        &nbsp;{!!_('submit')!!}
                    </button>
                    <a href="{!! URL::route('getLoadAirportPage') !!}" class="btn btn-danger">
                        <span>
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>
                        &nbsp;{!!_('cancel')!!}
                    </a>
                </div>
            </div>
        </form>
    
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    // $(document).on("change","#files",function(){
    //     var count = $('#files_div').find('input').length;
    //     count = count+1;
    //     $("<div id='div_"+count+"' style='display:inline;'><input style='width:400px;splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div');
    // });

    $(function(){     
        
        // repeat the input fields ===================================
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
       
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-append"><input style="width:400px;" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;" title="remove"> X </a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            $('#add').fadeIn("slow");
            $('#appendedInputButton').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x--;
        });

    });


</script>

@stop