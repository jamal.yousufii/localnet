@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('departure_comment')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
    </style>

@stop

@section('content')

@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('getLoadDepList') !!}">{!!_('departure_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('view_departure_comment')!!}</span>
                    </li>
                </ol>
                <h1>{!!_('comment_and_attachments_of_the_departure')!!}</h1>
            </div>
        </div>
    </div>

    <div class="container">
        @foreach($deps AS $item)

            <table class="table pull-right">
                <tr>
                    <td>{!!_('name')!!} :</td>
                    <td>
                        <span>{!!$item->name!!}</span>
                    </td>
                    <td>{!!_('occupation')!!} :</td>
                    <td>
                        <span>{!!$item->job!!}</span>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('office')!!} :</td>
                    <td>
                        <span>{!!$item->office!!}</span>
                    </td>
                    <td>{!!_('travel_date')!!} :</td>
                    <td>
                        <span>{!!$item->travel_date!!}</span>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('travel_country')!!} :</td>
                    <td>
                        <span>{!!$item->country!!}</span>
                    </td>
                    <td>{!!_('return_date')!!} :</td>
                    <td>
                        <span>{!!$item->return_date!!}</span>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('statement_number')!!} :</td>
                    <td>
                        <span>{!!$item->statement_number!!}</span>
                    </td>
                    <td>{!!_('chart_number')!!} :</td>
                    <td>
                        <span>{!!$item->chart_number!!}</span>
                    </td>
                </tr>

            </table>

            <hr style="border: 1px dashed #b6b6b6" />

            <table class="table">
                
            <tr>
                <td>{!!_('comment')!!} :</td><td>{!!$item->comment!!}</td>
            </tr>
            <tr>
                <td></td>
            </tr>

            </table>
        @endforeach
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     
        
        //datepicker
        $('#travel_date').datepicker({
          format: 'yyyy-mm-dd'
        });

        $('#return_date').datepicker({
          format: 'yyyy-mm-dd'
        });

    });


</script>

@stop