
@extends('layouts.master')
@section('content')
  <div class="page-head">
    <h3>فورمه ای اصلاح کردن مراجع اسناد</h3>
    <hr >
  </div>
<div class="container">
  <div class="cl-mcont" id="sdu_result">
    <form class="form-horizontal group-border-dashed" action="{!!URL::route('postUpdateDocSource',$record->id)!!}" method="post" style="border-radius: 0px;" enctype="multipart/form-data">
      	<div class="form-group">
          <label class="col-sm-4 control-label"> مرجع ارسال کننده</label>
          <div class="col-sm-6">
              <input type="text" name='sender' class="form-control" value="{!!$record->sender!!}" />
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-4 control-label">مرجع ارسال کننده داخلی  \ معاونیت مربوطه </label>
          <div class="col-sm-6">
              <input type="text" name='internal_sender_related_deputy' class="form-control" value="{!!$record->internal_sender_related_deputy!!}" />
          </div>
        </div>
        <div class="form-group">
        	<label class="col-sm-4 control-label">مرجع ارسال کننده داخلی  \ ریاست مربوطه </label>
        	<div class="col-sm-6">
            	<input type="text" name='internal_sender_related_directorate' class="form-control" value="{!!$record->internal_sender_related_directorate!!}" />
        	</div>
        </div>
  </div>
      	{!!Form::token();!!}
      	<hr />
      	<div class="form-group" style="margin-left: -18em">
	        <label class="col-sm-2 control-label"></label>
	        <div class="col-sm-4">
	          	<input type="submit" value="ثبت تغیرات" class="btn btn-success"/>
	          	<a href="javascript:history.back()" class="btn btn-warning"><i class="fa fa-arrow-left"></i> برگشت</a>
	        </div>
	    </div>
    </form>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

</script> 

@stop