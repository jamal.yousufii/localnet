
	<table class="table table-bordered table-responsive">
		<thead>
	      <tr>
	        <th>شماره#</th>
	        <th>مرجع ارسال کننده</th>
	        <th>شماره سند</th>
	        <th>تاریخ سند</th>
	        <th>نوع سند</th>
	        <th>معاونیت مربوطه</th>
	        <th>ریاست مربوطه</th>
	        <th>نمبر سند داخلی</th>
	        <th>تاریخ سند داخلی</th>
	        <th>نوعیت سند داخلی</th>
	        <th>خلص موضوع</th>
	        <th>نمبر وارده</th>
	        <th>تاریخ وارده</th>
	        <th>نظر کمیته</th>
	        <th>اجراآت</th>
	        <th>نمبر هدایت</th>
	        <th>هدایت مقام عالی</th>
	        <th>تاریخ اجراات</th>
	        <th>ملاحظات</th>
	        <th>ضمایم</th>

	        <th colspan="2" class="noprint">عملیات</th>
	        
	      </tr>
	    </thead>

	    <tbody>
	    	@if(!empty($records))
	    	<?php $counter = $records->firstItem(); ?>
	            @foreach($records AS $item)
	            	<?php
	            		$record_id = Crypt::encrypt($item->id); 
	            	?>
	                <tr class="remove_record{!!$item->id!!}">
		                <td>{!!$counter!!}</td>
		                <td>{!!$item->sender!!}</td>
		                <td>{!!$item->doc_number!!}</td>
		                <td>{!! dmy_format(toJalali($item->doc_date))!!}</td>
		                <td>{!!$item->document_type!!}</td>
		                <td>{!!$item->internal_sender_related_deputy!!}</td>
		                <td>{!!$item->internal_sender_related_directorate!!}</td>
		                <td>{!!$item->internal_sender_related_doc_number!!}</td>
		                <td>{!!dmy_format(toJalali($item->internal_sender_related_doc_date))!!}</td>
		                <td>{!!$item->internal_sender_related_doc_type!!}</td>
		                <td>{!!$item->summary!!}</td>
		                <td>{!!$item->incoming_number!!}</td>
		                <td>{!!dmy_format(toJalali($item->incoming_date))!!}</td>
		                <td>{!!$item->committee_comment!!}</td>
		                <td>{!!$item->execution!!}</td>
		                @if($item->execution == "هدایت مقام")
		                <td>{!!$item->hedayat_number!!}</td>
		                <td>{!!$item->hedayat_muqam_aali!!}</td>
		                @else
		                <td></td>
		                <td></td>
		                @endif
		                <td>{!!dmy_format(toJalali($item->execution_date))!!}</td>
		                <td>{!!$item->considerations!!}</td>
		                <td>
		                	<ul style="list-style:none">
					            @if(!empty(getDocsArchiveFileName($item->id)))
					                @foreach(getDocsArchiveFileName($item->id) AS $attach)
					                <li id="li_{!!$attach->id!!}">
					                <?php $file_id = Crypt::encrypt($attach->id); $original_file_name = wordwrap($attach->original_file_name, 50, "<br />"); ?>
					                    <div class="name">
							                <a href="aop_docs_archive_uploads/{!!$attach->file_name!!}" target="_blank"><strong>{!!$attach->original_file_name!!}</strong></a>
						                </div>                                   
					                </li>
					                @endforeach
					            @else
					                <li><span style='color:red;'>فایل ضمیمه آپلود نشده</span></li>
					            @endif
					        </ul>
					    </td>
		                @if(isAdmin())
		                <td align='center' class="noprint"><a href="{!!URL::route('editDoc',$record_id)!!}" title="Edit Record"><span class='fa fa-edit'></span></a></td>
		                <td align='center' class="noprint"><a onclick="delteDocument(this.id)" id="{!!$item->id!!}" title='Delete Record'><span class='fa fa-trash'></span></a></td>
		                @endif
	                </tr>
	                <?php $counter++; ?>
	            @endforeach
	        @else
	        <div style="padding: 10px" class="noprint">
	        	<span style="color:red">سند در سیستم موجود نیست !</span>
	        </div>
	    	@endif
	    </tbody>
	</table>
<script type="text/javascript">

	$(document).ready(function() {
    	$('td.text').each(function() {
	        var td = $(this);
	        var cs = td.text().length;
	        
	        if(cs>50)
	        {
	        	var shown = td.text().substring(0, 50);
	        	td.text(shown+'...');
	        }
	    });
	});

</script>