
@extends('layouts.master')
@section('content')
  <div class="page-head">
    <h3 align="center">فورمه ای اصلاح کردن سند اضافه شده</h3>
    <hr >
  </div>
  <div class="cl-mcont" id="sdu_result">
    <form class="form-horizontal group-border-dashed" action="{!!URL::route('postUpdateDoc',$record->id)!!}" method="post" style="border-radius: 0px;" enctype="multipart/form-data">
      	<div class="form-group">
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">مرجع ارسال کننده</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="sender" id="sender" style="width: 100%">
              {!!getSender($record->sender);!!}
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">شماره سند<span style="color:red"> * </span></label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="doc_number" value="{!!$record->doc_number!!}" required="required" />
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تاریخ سند</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="doc_date" value="{!!checkEmptyDate($record->doc_date)!!}" readonly="readonly" />
          </div>
        </div> 
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">نوع سند<span style="color:red"> * </span></label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="doc_type" id="doc_type" style="width: 100%" required="required">
              {!!getDocType($record->doc_type);!!}
            </select>
          </div>
        </div>  
      </div>
      <div class="form-group">
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">مرجع ارسال کننده داخلی / معاونیت مربوطه<span style="color:red"> * </span></label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="internal_sender_related_deputy" id="internal_sender_related_deputy" style="width: 100%" required="required">
              {!!getRelatedDeputy($record->internal_sender_related_deputy)!!}
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">مرجع ارسال کننده داخلی / ریاست مربوطه<span style="color:red"> * </span></label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="internal_sender_related_directorate" id="internal_sender_related_directorate" style="width: 100%" required="required">
              {!!getRelatedDirectorate($record->internal_sender_related_directorate)!!}
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">نمبر سند داخلی</label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="internal_sender_related_doc_number" value="{!!$record->internal_sender_related_doc_number!!}" />
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تاریخ سند داخلی</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="internal_sender_related_doc_date" value="{!!checkEmptyDate($record->internal_sender_related_doc_date)!!}" readonly="readonly" />
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">نوع سند داخلی<span style="color:red"> * </span></label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="internal_sender_related_doc_type" id="internal_sender_related_doc_type" style="width: 100%" required="required">
              {!!getDocType($record->internal_sender_related_doc_type);!!}
            </select>
          </div>
        </div> 
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">خلص موضوع</label>
          </div>
          <div class="col-sm-12">
            <textarea class="form-control" name="summary" value="{!!$record->summary!!}">{!!$record->summary!!}</textarea>
          </div>
        </div> 
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">شماره وارده<span style="color:red"> * </span></label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="incoming_number" value="{!!$record->incoming_number!!}" required="required" />
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تاریخ وارده</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="incoming_date" value="{!!checkEmptyDate($record->incoming_date)!!}" />
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">نظر کمیته</label>
          </div>
          <div class="col-sm-12">
            <textarea class="form-control" name="committee_comment" value="{!!$record->committee_comment!!}">{!!$record->committee_comment!!}</textarea>
          </div>
        </div> 
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">اجراآت</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="execution" id="execution" style="width: 100%">
              {!!getExecutions($record->execution);!!}
            </select>
          </div>
        </div>
        <div id="hedayat_div" style="display: none"> 
          <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">نمبر هدایت</label>
            </div>
            <div class="col-sm-12">
              <input type="text" class="form-control" name="hedayat_number" value="{!!$record->hedayat_number!!}" />
            </div>
          </div>
          <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">هدایت مقام عالی</label>
            </div>
            <div class="col-sm-12">
              <textarea class="form-control" name="hedayat_muqam_aali" value="{!!$record->hedayat_muqam_aali!!}">{!!$record->hedayat_muqam_aali!!}</textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تاریخ اجراات</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="execution_date" value="{!!checkEmptyDate($record->execution_date)!!}" readonly="readonly" />
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">ملاحظات</label>
          </div>
          <div class="col-sm-12">
            <textarea class="form-control" name="considerations" value="{!!$record->considerations!!}">{!!$record->considerations!!}</textarea>
          </div>
        </div> 
        <div class="form-group">
          <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12">ضمایم </label>
            </div>
    			  <div class="col-sm-12">
                <div class="input_fields_wrap">
                    <input type='file' id='files' style="width:87%;display:inline-block" name='files[]' class="form-control" multiple='multiple'>
                    <a class="add_field_button btn" id="add" style="background: green;color:white;display:inline-block;margin-top:-20px;" title="Add another file"> + </a>
                </div>
            </div>
          </div>

        </div>
        <hr style="border: 1px dashed;" />
      	<div class="form-group main-box-body clearfix">
            <ul class="widget-todo" style="margin-top:10px;list-style:none">
            	<h4>ضمایم آپلود شده</h4>
                @if(!empty(getDocsArchiveFileName($record->id)))
                    @foreach(getDocsArchiveFileName($record->id) AS $attach)
						<li class="clearfix" id="li_{!!$attach->id!!}">
							<?php $file_id = Crypt::encrypt($attach->id); ?>
	            <div class="name" style="margin-top:5px">
	              <label for="todo-2">
                  <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
	                  <a href="../aop_docs_archive_uploads/{!!$attach->file_name!!}" target="_blank"><strong>{!!$attach->original_file_name!!}</strong></a><br />
	                </label>&nbsp;&nbsp;&nbsp;
							  	<a href="{!!URL::route('getDownloadDocFile',array($file_id))!!}" class="table-link success">
				            <i class="fa fa-2x fa-cloud-download" style='color:#03a9f4;' title="download file"></i>
				          </a>
				          <button type="button" onclick="removeDocFile('{!!$attach->id!!}','{!!$attach->file_name!!}')" class="btn btn-danger" style="margin-top:-20px">
				            <i class="fa fa-trash" title="Remove file"></i>
				          </button>&nbsp;&nbsp;
	                <br />
              </div>                                    
						</li>
                    @endforeach
                @else
                    <li><span style='color:red;'>ضمیمه آپلود نشده</span></li>
                @endif
            </ul>
                
        </div>
      	{!!Form::token();!!}
      	<hr />
      	<div class="form-group" style="margin-left: -26em">
	        <label class="col-sm-2 control-label"></label>
	        <div class="col-sm-4">
	          	<input type="submit" value="ثبت تغیرات" class="btn btn-success"/>
	          	<a href="javascript:history.back()" class="btn btn-warning"><i class="fa fa-arrow-left"></i> برگشت</a>
	        </div>
	    </div>
    </form>
  </div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

  $("#sender").select2();
  $("#internal_sender_related_deputy").select2();
  $("#internal_sender_related_directorate").select2();

	function removeDocFile(file_id,file_name)
	{ 
		var result = confirm("Are you sure you want to delete?");
		if (result) 
		{
			$.ajax({

				url : "{!!URL::route('getRemoveDocFile')!!}",
				type: "post",
				data : "file_id="+file_id+"&file_name="+file_name+"&_token="+"<?=csrf_token();?>",
				success : function(response)
				{
					if(response != 0)
					{
						$("#li_"+file_id).css('background','Crimson');
	                    $("#li_"+file_id).slideUp('6000', function(){
	                    	$("#li_"+file_id).remove();
	                    });
					}
				}

			});
			return false;
		}
	}

  // get the selected index of execution dropdown.
  if($("#execution")[0].selectedIndex == 5)
  {
    $("#hedayat_div").fadeIn("slow");
  }
  else
  {
    $("#hedayat_div").hide();
  }

	$(function(){     
        
      $("#execution").change(function(){
        // get the selected index of execution dropdown.
        if($("#execution")[0].selectedIndex == 5)
        {
          $("#hedayat_div").fadeIn("slow");
        }
        else
        {
          $("#hedayat_div").hide();
        }
      });

        // repeat the input fields ===================================
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
       
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;display:inline-block;margin:-20px 0 0 5px" title="remove"> X </a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            $('#add').fadeIn("slow");
            $('#appendedInputButton').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x--;
        });

    });

</script> 

@stop