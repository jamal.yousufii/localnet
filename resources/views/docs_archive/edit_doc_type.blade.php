
@extends('layouts.master')
@section('content')
<div class="container">
  <div class="page-head">
    <h3>فورمه ای اصلاح کردن نوع سند</h3>
    <hr >
  </div>
  <div class="cl-mcont" id="sdu_result">
    <form class="form-horizontal group-border-dashed" action="{!!URL::route('postUpdateDocType',$record->id)!!}" method="post" style="border-radius: 0px;" enctype="multipart/form-data">
      	<div class="form-group">
          	<label class="col-sm-4 control-label"> نوع سند</label>
          	<div class="col-sm-6">
              	<input type="text" name='name' class="form-control" value="{!!$record->name!!}" required="required" />
          	</div>
              
      	</div>
      	{!!Form::token();!!}
      	<hr />
      	<div class="form-group" style="margin-left: -18em">
	        <label class="col-sm-2 control-label"></label>
	        <div class="col-sm-4">
	          	<input type="submit" value="ثبت تغیرات" class="btn btn-success"/>
	          	<a href="javascript:history.back()" class="btn btn-warning"><i class="fa fa-arrow-left"></i> برگشت</a>
	        </div>
	    </div>
    </form>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

</script> 

@stop