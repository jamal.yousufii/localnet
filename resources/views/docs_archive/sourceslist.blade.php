@extends('layouts.master')

@section('head')
    @parent
    @if(Auth::check())
    <title>لست مراجع اسناد</title>
    @endif
@stop


@section('content')

<div class="container" dir="rtl">

	@if (count($errors) > 0)
    <div class="alert alert-danger" style="margin: 10px 0 20px 0">
      <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    @if(Session::has('success'))
    <div class='alert alert-success'>{{Session::get('success')}}</div>

    @elseif(Session::has('fail'))
    <div class='alert alert-danger'>{{Session::get('fail')}}</div>
     @endif
    <div class="row" dir="rtl">
	    <h4 class="pull-right">لست مراجع اسناد</h4>
	    <a id="click" data-target="#exampleModalPrimary" data-toggle="modal" class="btn btn-success pull-left"><i class="icon fa-plus" aria-hidden="true"></i> اضافه کردن مرجع جدید سند</a>
    </div>
	<hr />
  <div id="result"></div>
	<div class="col-sm-12">
		<table class="table table-bordered table-responsive" id="doc_sources">
	      <thead>
	        <tr>
	          <th>شماره#</th>
	          <th>مراجع ارسال کننده</th>
            <th> مرجع ارسال کننده داخلی   \ معاونیت مربوطه </th>
	          <th> مرجع ارسال  کننده داخلی  \ ریاست مربوطه </th>
	          <th>تاریخ ایجاد</th>
	          <th>عملیات</th>
	        </tr>
	      </thead>
	      <tbody>
	      </tbody>
	    </table>
    </div>
</div>

<div class="modal fade modal-info" id="exampleModalPrimary" aria-hidden="true" aria-labelledby="exampleModalPrimary" role="dialog" tabindex="-1">
  <div class="modal-dialog" style="width: 1200px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title"><i class="fa fa-plus fa-lg"></i> جزئیات مراجع اسناد</h4>
      </div>
      <div id="form_part">
        <form class="form-horizontal" role="form" method="post" action="{!!URL::route('saveNewDocSource')!!}" enctype="multipart/form-data" id="modal_form">
          <div class="modal-body">
              <div class="form-group">
                  <label class="col-sm-4 control-label">مرجع ارسال کننده</label>
                  <div class="col-sm-6">
                      <input type="text" name='sender' class="form-control" value="{!!old('sender')!!}" />
                  </div> 
              </div>
              <div class="form-group">
                  <label class="col-sm-4 control-label">مرجع ارسال کننده داخلی  \ معاونیت مربوطه</label>
                  <div class="col-sm-6">
                      <input type="text" name='internal_sender_related_deputy' class="form-control" value="{!!old('internal_sender_related_deputy')!!}" />
                  </div> 
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">مرجع ارسال کننده داخلی  \ ریاست مربوطه</label>
                  <div class="col-sm-6">
                      <input type="text" name='internal_sender_related_directorate' class="form-control" value="{!!old('internal_sender_related_directorate')!!}" />
                  </div> 
              </div>
          </div>
          {!!Form::token();!!}
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">بسته کردن</button>
            <button type="submit" class="btn btn-info">اضافه کردن</button>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

	$(document).ready(function() {
	
		//get the contract type list for datatable
		$(document).ready(function() {
		    $('#doc_sources').dataTable(
		        {

		            "sDom": 'lfr<"clearfix">tip',
		            "bProcessing": false,
		            "bServerSide": true,
		            "bDeferRender": true,
		            "iDisplayLength": 10,
		            "aaSorting": [[ 0, "asc" ]],
		            "sAjaxSource": "{!!URL::route('docSourceList')!!}"

		        }
		    );
		});

	});

  function deleteDocSource(record_id)
  {
    if (confirm("Are you sure you want to delete? Previous documents with this Category will be effected as well")) 
    {
      $.ajax({
        url : '{!!URL::route("deleteDocSource")!!}',
        type : 'post',
        data : {'record_id':record_id,'_token':"{!!csrf_token()!!}"},
        success : function(response)
        {
          $("#result").html(response);
        }
      })
      window.setTimeout(function(){
          // Move to a new location or you can do something else
          window.location.href = "{!!URL::route('loadSourcesList')!!}";

      }, 1000);
    }
  }

</script> 
@stop