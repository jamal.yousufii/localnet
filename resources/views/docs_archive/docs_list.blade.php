@extends('layouts.master')

@section('head')
    @parent
    <title>لست اسناد آرشیف شده</title>
    <style type="text/css">
      a
      {
        cursor: pointer;
      }
    </style>
    {!! HTML::style('/css/font.css') !!}
    {!! HTML::style('/css/print.css', array('media' => 'print')) !!}
    {!! HTML::style('/vendor/select2/select2.min.css') !!}
    {!! HTML::script('/vendor/select2/select2.min.js')!!}
@stop


@section('content')


  @if (count($errors) > 0)
  <div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if(Session::has('success'))
  <div class='alert alert-success noprint'>{{Session::get('success')}}</div>

  @elseif(Session::has('fail'))
  <div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
   @endif
  <div class="row noprint" style="margin: 10px;">
    <h2 class="pull-right">لست اسناد</h2>
    <!-- <a id="click" data-target="#exampleModalPrimary" data-toggle="modal" class="btn btn-success pull-right"><i class="icon fa-plus" aria-hidden="true"></i> Add New Document</a> -->
    <a href="{!!URL::route('loadDocInsertPage')!!}" class="btn btn-success pull-left"><i class="icon fa-plus" aria-hidden="true"></i> اضافه کردن سند جدید</a>
  </div>
  <hr class="noprint" />
    <div id="deleted_result" class="noprint">
    </div>
    <form role="form" id="search_form" class="form-horizontal noprint" method="post" action="{!!URL::route('exportToExcel')!!}">
      <div class="form-group">
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">مرجع ارسال کننده</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="sender" id="sender" style="width: 100%">
              {!!getSender();!!}
            </select>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">شماره سند</label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="doc_number" placeholder="شماره سند" />
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تاریخ سند</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="doc_date" placeholder="تاریخ سند" readonly="readonly" />
          </div>
        </div> 
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">نوع سند</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="doc_type" id="doc_type" style="width: 100%">
              {!!getDocType();!!}
            </select>
          </div>
        </div>  
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">مرجع ارسال کننده داخلی / معاونیت مربوطه</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="internal_sender_related_deputy" id="internal_sender_related_deputy" style="width: 100%">
              {!!getRelatedDeputy();!!}
            </select>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">مرجع ارسال کننده داخلی / ریاست مربوطه</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="internal_sender_related_directorate" id="internal_sender_related_directorate" style="width: 100%">
              {!!getRelatedDirectorate();!!}
            </select>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">نمبر سند داخلی</label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="internal_sender_related_doc_number" placeholder="شماره سند" />
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تاریخ سند داخلی</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="internal_sender_related_doc_date" placeholder="تاریخ سند داخلی" readonly="readonly" />
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">نوع سند داخلی</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="internal_sender_related_doc_type" id="internal_sender_related_doc_type" style="width: 100%">
              {!!getDocType();!!}
            </select>
          </div>
        </div> 
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">خلص موضوع</label>
          </div>
          <div class="col-sm-12">
            <textarea class="form-control" name="summary" placeholder="خلص موضوع"></textarea>
          </div>
        </div> 
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">شماره وارده</label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="incoming_number" placeholder="شماره سند وارده" />
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تاریخ وارده</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="incoming_date" placeholder="تاریخ وارده" readonly="readonly" />
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">نظر کمیته</label>
          </div>
          <div class="col-sm-12">
            <textarea class="form-control" name="committee_comment" placeholder="نظر کمیته"></textarea>
          </div>
        </div> 
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">اجراآت</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="execution" id="execution" style="width: 100%">
              {!!getExecutions();!!}
            </select>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">نمبر هدایت</label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="hedayat_number" placeholder="نمبر هدایت" />
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">هدایت مقام عالی</label>
          </div>
          <div class="col-sm-12">
            <textarea class="form-control" name="hedayat_muqam_aali" placeholder="هدایت مقام عالی"></textarea>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تاریخ اجراات</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="execution_date" placeholder="تاریخ اجراات" readonly="readonly" />
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">ملاحظات</label>
          </div>
          <div class="col-sm-12">
            <textarea class="form-control" name="considerations" placeholder="ملاحظات"></textarea>
          </div>
        </div> 
      </div>
      {!!Form::token()!!}
      <div class="form-group">
        <hr />
        <div class="col-sm-12">
            <button class="btn btn-warning" id="search_docs" type="submit">
                <span>
                    <i class="fa fa-search"></i>
                </span>
                &nbsp;جستجو کردن
            </button>
            &nbsp;<input type="reset" value="پاک کردن" id="searchclear" class="btn btn-danger"/>
            &nbsp;
            <button type="submit" class="btn btn-success" id="print_to_excel" >چاپ به صفحه اکسل</button>
            &nbsp;
            <!-- <a href="#" onclick="window.print()" class="btn btn-primary noprint">چاپ صفحه</a> -->
        </div>
      </div>
    </form>
	<hr class="noprint" />
  <div id="search_result">
    {{--Bring the list table--}}
        @include('docs_archive.list_table')
    {{--list table end--}}
    <div class="dataTables_paginate paging_simple_numbers noprint" id="list_paginate">
      @if(!empty($records))
      {!!$records->render()!!}
      @endif
    </div>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

  $("#sender").select2();
  $("#internal_sender_related_deputy").select2();
  $("#internal_sender_related_directorate").select2();

  $(function(){
    $("#searchclear").click(function(){
      $("#sender").select2('val', '');
      $("#internal_sender_related_deputy").select2('val', '');
      $("#internal_sender_related_directorate").select2('val', '');
    });
  });

  $("#search_docs").click(function() {
    window.scrollTo(300, 2000);
});

  // $("#document_type").select2();

	$('.pagination a').on('click', function(event) {
    event.preventDefault();
    if ($(this).attr('href') != '#') {
      //$('#ajaxContent').load($(this).attr('href'));
      var dataString = $('#search_form').serialize();
      dataString += "&page="+$(this).text()+"&ajax="+1;
      $.ajax({
          url: '{!!URL::to("/home")!!}',
          data: dataString,
          type: 'get',
          beforeSend: function(){
              //$("body").show().css({"opacity": "0.5"});
              $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#search_result').html(response);
          }
      }
    );
    
    }
  });

  function delteDocument(record_id)
  {
      if (confirm("Are you sure you want to delete? All related attachments will be deleted as well")) 
      {
          $.ajax({
            url : '{!!URL::route("deleteDoc")!!}',
            type : 'post',
            data : {'record_id':record_id,'_token':"{!!csrf_token()!!}"},
            success : function(response)
            {
              $("#deleted_result").html(response);
              $(".remove_record"+record_id).css('background','Crimson');
              $(".remove_record"+record_id).slideUp('6000', function(){
              $(this).remove();
              //$("#deleted_result").html(mydata);
              });

            }
          })
        //   window.setTimeout(function(){
        //       // Move to a new location or you can do something else
        //     window.location.href = "{!!URL::to('/home')!!}";

        // }, 3000);
      }
  }

  //get the document list based on form data.
  $('#search_docs').click(function(){
      var datastring = $('#search_form').serialize();
      $.ajax({
          type: 'POST',
          url: '{!!URL::route("searchDocs")!!}',
          data: datastring,
          beforeSend: function(){
              $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#search_result').html(response);
          }
      });
      return false;
  });

	$(function(){     
        
      // repeat the input fields ===================================
      var max_fields      = 5; //maximum input boxes allowed
      var wrapper         = $(".input_fields_wrap"); //Fields wrapper
      var add_button      = $(".add_field_button"); //Add button ID
     
      var x = 1; //initlal text box count
      $(add_button).click(function(e){ //on add input button click
          e.preventDefault();
          if(x < max_fields){ //max input box allowed
              x++; //text box increment
              $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;display:inline-block;margin:-20px 0 0 5px" title="remove"> X </a></div>'); //add input box
          }
      });

      $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
          $('#add').fadeIn("slow");
          $('#appendedInputButton').fadeIn("slow");
          e.preventDefault(); $(this).parent('div').remove(); x--;
      });

  });

</script> 
@stop