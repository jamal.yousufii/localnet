@extends('layouts.master')
@section('content')
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>سیستم معلوماتی مدیریت عمومی تلیفون </h3></center>
    <ol class="breadcrumb">
     
      <li class="active"><h3>اضافه نمودن اطلاعات   ریاست  های جدید </h3></li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed" action="{!!URL::route('insert_dir')!!}" method="post" style="border-radius: 0px;">
      <input type="hidden" name="_token" value="{!!csrf_token()!!}">
      <div>
       <label>ریاست</label>   
      
      </div>     
      <input type="text" class="form-control" name="directorate" placeholder="لطفا ریاست های جدید را انجا بنوسید"><br><br>
    
     
     

               
          
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <input type="submit" value="ثبت " id="add_department" class="btn btn-success"/>
              <a href="{!!URL::route('phoneSimcardList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>

            </div>
          </div>
        </form>
  </div><hr><br>
     <div class="tab-pane active cont" id="tab1"> 
          <!-- DataTables for Contract Type -->
          <div class="">
           <div class="header">
            <br>
            <div class="row">
             <!-- <div class="pull-left">
               <a href="{!!URL::route('insert_phone')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه کردن معلومات جدید</a>
              </div>-->
              <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form" action="">
                 <div class="input-group custom-search-form">
                 
                 {!!Form::token();!!}
                  <!-- <span class="input-group-btn">
                      <button class="btn btn-default-sm" id="search_button"><i class="fa fa-search"></i> جستجو</button>
                    </span>-->
                  </div>
                </form>
              </div>
            </div>
           <!-- <h3 style="margin-top:20px;" align="center">مدیریت عمومی تلیفون ریاست تکنالوژی معلوماتی معاونیت مالی واداری</h3><hr />-->
            </div>
            <div class="content" id="datalist">
              <div>
               <h3>لست  ریاست های  درج شده </h3>
                   <table class="table table-bordered table-responsive"  >
                  <thead>
                    <tr>
                    <th>شماره#</th>
                    <th>ریاست </th>
                   <!--  <th>تغیر</th>
                    <th>حذف</th> -->
                    </thead>
                                         
                    </tr>
                       @if(!empty($all_data3))
                       
                        <?php $counter=1;
                      $count=0;
                       ?>
                       
                        
                      @foreach ($all_data3 as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->directorate}}</td>
                      <!-- <td><a href="edit_equp/{{$val-> id}}" class="fa fa-edit"></a></td>          
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td> -->                            
                      </tr> 
                   </tr> <?php $counter++;
                             $count++;    
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                 
                     </thead>
                  <tbody>
                  </tbody>
                 
                </table>
                
                <div class="test-center"> 
              
               </div>
              
              </div>
            </div>
          </div>
        </div>
    </div>
@stop

@section('footer-scripts') 
<script type="text/javascript">

</script> 
<script type="text/javascript">
//get the contract type list for datatable
$(document).ready(function() {
  
  $("#search_button").click(function(){
    var field_value = $('#search_field').val();
    if(field_value !="")
    $.ajax({
      type : "post",
      url : "{!!URL::route('search_sim')!!}",
      data : {"search_string": field_value, "_token": "<?=csrf_token();?>"},
      success : function(response)
      {
        $("#datalist").html(response);
      }
    });
      return false;
  });
  
});


</script> 
@stop