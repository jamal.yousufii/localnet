@extends('layouts.master')
@section('content')
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>سیستم معلوماتی مدیریت عمومی تلیفون </h3></center>
    <ol class="breadcrumb">
     
      <li class="active"><h3>اضافه نمودن اطلاعت</h3></li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed" action="{!!URL::route('edit_mobile',$edit_equp->id)!!}" enctype="multipart/form-data" method="post" style="border-radius: 0px;" ><!-- enctype="multipart/form-data" -->
          <input type="hidden" name="_token" value="{!!csrf_token()!!}">
          
                   
          <div class="col-lg-4 col-sm-4">
         <div class="form-group">
          <label>سریل نمبر جنس</label>
          <input type="text" class="form-control" name="equipment_serial" value="{{$edit_equp->equipment_serialno}}">
         <label>شماره سیم کارت   *</label>
          <input type="text" name="phonno" class="form-control" value="{{$edit_equp->simcard_number}}">
          
         <label>تعین کریدت و انترنت</label>
          <input type="text" name="MB_credit" class="form-control" value="{{$edit_equp->MB_credit}}" > 
          <label>تاریخ  توزیع  *</label>
          <input type="text" name="issue_date"  value ="{!!checkEmptyDate($edit_equp->issue_date)!!}" class="datepicker_farsi form-control">
          </div>
          </div>
          
          <div class="col-lg-4 col-sm-4">
         <div class="form-group">
          
          <label>قیمت کریدت و انترنت</label>
          <input type="text" name="price" class="form-control" value="{{$edit_equp->price}}" required>        
      
          <label>حالت سیم کارت  *</label>
          <select class="form-control" name="activation" required="">
          <option>--Select activation--</option>
           <option value="1" @if($edit_equp->activation==1) selected @endif> Active</option>
           <option value="2" @if($edit_equp->activation==2) selected @endif> Deactive </option>
          </select>
          </select> 

          <label>ذخیره /توزیع  *</label>
          <select class="form-control" name="storage" required="">
          <option>--Select activation--</option>
          <option value="1" @if($edit_equp->storage==1) selected @endif> Stock in</option>
          <option value="2" @if($edit_equp->storage==2) selected @endif> Stock out </option>
          </select> 
          <label>نوع سیم کارت   *</label>
          <select class="form-control" name="postpaid" required="">
          <option value="{{$edit_equp->postpaid}}">--Select type of SIM--</option>
            <option value="1" @if($edit_equp->postpaid==1) selected @endif> PostPaid </option>
           <option value="2" @if($edit_equp->postpaid==2) selected @endif> PrePaid </option>
          </select>

          <label>ملاحظات</label>
          <textarea class="form-control" name="remark" placeholder="برای معلومات اضافی" value="{{$edit_equp->remark}}">{{$edit_equp->remark}}</textarea>
          </div>
          </div>
          
          <div class="col-lg-4 col-sm-4">
          <div class="form-group">
          <!-- <label>اسم،تخلص،ولد</label>
          <select class="form-control" name="name" placeholder="اسم تخلص" required>
          <option>--Select Name--</option>
          @foreach($emp_info as $emp)
          <option value="{{$emp->id}}"<?php if ( $emp->id == $edit_equp->emp_id) echo "selected";?>>{{$emp->name}}</option>
          @endforeach
          </select> -->
         <label>کمپنی   *</label>
         <select class="form-control" name="company" required="">
         <option>--Select Company--</option>
           @foreach($company as $comp)
            <option value="{{$comp->id}}"<?php if ( $comp->id == $edit_equp->company_id) echo "selected";?>>{{$comp-> com_name}}</option>
            @endforeach
          </select>
          <label>جنس   *</label>
          <select class="form-control" name="equipment" required="">
          <option>--Select Equipment--</option>
            @foreach($equptype as $equp)
            <option value="{{$equp->id}}"<?php if ( $equp->id == $edit_equp->equipment_id) echo "selected";?>>{{$equp->equipment_name}}</option>
            @endforeach
          </select>
           <label>حدود سیمکارت   *</label>
          <select class="form-control" name="equipment_range" required="">
          <option >--Select Range of SIM Card--</option>
           <option value="1" @if($edit_equp->equipment_range==1) selected @endif> Limited</option>
           <option value="2" @if($edit_equp->equipment_range==2) selected @endif> Unlimited</option>
          </select>
          <label>کاپی  اسناد </label>
         <input type="file" class="form-control" name="files[]" multiple placeholder="" />
          
          <input type="hidden" class="form-control" name="id" value="{{$edit_equp->id}}">

         </div>
          </div>
            
          
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <input type="submit" value="ثبت" id="add_department" class="btn btn-success"/>
              <a href="{!!URL::route('sim_management')!!}">
               <input type="button" value=" برگشت به صفحه " id="add_department" class="btn btn-info"/></a>
              <a href="{!!URL::route('get_employee')!!}">
               <input type="button" value="برای تغیر نام " id="add_department" class="btn btn-info"/></a>
              
            </div>
          </div>
        </form>
  </div>
</div>
@stop

@section('footer-scripts') 
<script type="text/javascript">

</script> 

@stop