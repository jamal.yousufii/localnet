@extends('layouts.master')
@section('content')
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>سیستم معلوماتی مدیریت عمومی تلیفون </h3></center>
    <ol class="breadcrumb">
     
      <li class="active"><h3>اضافه نمودن اطلاعت</h3></li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed" action="{!!URL::route('insert_emp')!!}" method="post" enctype="multipart/form-data" style="border-radius: 0px;">
      <input type="hidden" name="_token" value="{!!csrf_token()!!}">
         
      
          
         

         <div class="col-lg-4 col-sm-4">
         <div class="form-group">
         <label>محل وظیفه</label>
         <input type="text" class="form-control" name="address" placeholder="" />
         <label>ایمیل ادرس</label>
         <input type="text" class="form-control" name="email_id" placeholder="email@web.com"  />
         <label>شماره دفتر</label>
         <input type="text" class="form-control" name="office_no" placeholder=""  />
         
         <!-- <label>کاپی جدول </label>
         <input type="file" class="form-control" name="jadwal" placeholder=""  />   
          <label>کاپی  اسناد اضافی </label>
         <input type="file" class="form-control" name="extra" placeholder=""  />   -->  
         </div>
         </div>

         <div class="col-lg-4 col-sm-4">
         <div class="form-group">
         <label>ریاست *</label>
         <select type="text" class="form-control" name="directorate" required >
         <option>-- برای انتخاب  ریاست --</option>
          @foreach($directorates as $directorate)
         <option value="{{$directorate->id}}">{{$directorate->directorate}}</option>
         @endforeach
         </select>
         <label>دیپارتمنت  </label>
         <input type="text" class="form-control" name="department" placeholder="" />
         <label>وظیفه  * </label>
         <input type="text" class="form-control" name="position" required />
         <label>تاریخ ثبت</label>
          <input type="text" name="issue_date" class="datepicker_farsi form-control" required>
         <label>ملاحظات  </label>
         <input type="text" class="form-control" name="remark" placeholder=""  />
         
         </div>
         </div>  

         <div class="col-lg-4 col-sm-4">
         <div class="form-group">
         <!--  <label>شماره سیم کارت</label> -->
       <!-- <input type="text" class="form-control" name="sim_id" placeholder="" value="{{$sim_id}}" readonly="" />-->
       <label>شماره  سیم کارت   * </label> 

       <input type="text" class="form-control" name="sim_no" placeholder="" value="{{$phone}}" readonly=""   />
      
        <label>شماره کارت   * </label>
         <input  type="text" class="form-control" name="cardno" placeholder="شماره کارت"  required=""  />
         
         <label>اسم   *</label>
         <input type="text" class="form-control" name="name" placeholder="Name" required />
         <label>تخلص   </label>
         <input type="text" class="form-control" name="lastname" placeholder="Last Name" />
        <label>جنسیت   *</label>
         <select class="form-control" name="gender" placeholder="مرد/زن" required>
          <option>-- برای انتخاب جنسیت --</option>
         <option value="1">مرد</option>
         <option value="2">زن</option></select>
         </div>
         </div>        
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4" style="float: right;">
              <input type="submit" value="ثبت " id="add_department" class="btn btn-success"/>
              
                <a href="{!!URL::route('insert_phone')!!}"><input type="button" value="صفحه سیم کارت" id="add_department" class="btn btn-primary"/></a> 
                <a href="{!!URL::route('sim_management')!!}"><input type="button" value=" برگشت به صفحه " id="add_department" class="btn btn-info"/></a>
            </div>
          </div>
        </form>
  </div><hr><br>
     <div class="tab-pane active cont" id="tab1"> 
          <!-- DataTables for Contract Type -->
          <div class="">
           <div class="header">
            <br>
            <div class="row">
             <!-- <div class="pull-left">
               <a href="{!!URL::route('insert_phone')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه کردن معلومات جدید</a>
              </div>-->
              <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form" action="">
                 <div class="input-group custom-search-form">
                 
                 {!!Form::token();!!}
                  <!-- <span class="input-group-btn">
                      <button class="btn btn-default-sm" id="search_button"><i class="fa fa-search"></i> جستجو</button>
                    </span>-->
                  </div>
                </form>
              </div>
            </div>
           <!-- <h3 style="margin-top:20px;" align="center">مدیریت عمومی تلیفون ریاست تکنالوژی معلوماتی معاونیت مالی واداری</h3><hr />-->
            </div>
            <div class="content" id="datalist">
              <div>
              
              
               <table class="table table-bordered table-responsive"  >
               
                 
                </table>
                
                
              </div>
            </div>
          </div>
        </div>
    </div>
@stop

@section('footer-scripts') 
<script type="text/javascript">

</script> 
<script type="text/javascript">
//get the contract type list for datatable
$(document).ready(function() {
  
  $("#search_button").click(function(){
    var field_value = $('#search_field').val();
    if(field_value !="")
    $.ajax({
      type : "post",
      url : "{!!URL::route('search_sim')!!}",
      data : {"search_string": field_value, "_token": "<?=csrf_token();?>"},
      success : function(response)
      {
        $("#datalist").html(response);
      }
    });
      return false;
  });
  
});


</script> 
@stop