
<head>
  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
@extends('layouts.master')
@section('content')
<div class="container" dir="rtl" >
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
      <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(2)").addClass("active");

      </script>
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
    <ul class="nav nav-tabs" id="ul_tabs">
        
     </ul><br><br>
      <div class="tab-content">
        <div class="tab-pane active cont" id="tab1"> 
          <!-- DataTables for Contract Type -->
        
           <div class="header">
            <br>
            <div class="row">
             <div class="col-md-6 pull-right" dir="ltr" style="text-align: right;">
                 <h4 >سیم کارت های ذخیره شده</h4>
                <form class="form-horizontal group-border-dashed" id="search_form1">
                 <div class="input-group custom-search-form">
                  <input type="text" name="simcard_number" id="simcard_number" class="form-control">
                 {!!Form::token();!!}
                   <span class="input-group-btn">
                      <input type="button" value="Search" class="btn btn-success" id="search_ing_submit" style="background-color: #008080" /></i> جستجو</button>
                      
                    </span></div>
                </form>
                
              </div>


              </div>
            </div>
            <h3 style="margin-top:20px;" align="pull-right"></h3><hr />
            </div>
           
            <div class="content" >
              <div>
               <table class="table table-bordered table-responsive" id="datalist" style="border:0px solid #E67E22;">
                  <thead>
                    <tr align="center">
                    <th bgcolor="#EDBB99">شماره#</th>
                    <th bgcolor="#EDBB99">اسم گیرنده </th>
                    <th bgcolor="#EDBB99">تخلیص</th>
                    <th bgcolor="#EDBB99">شماره سیم کارت</th>
                    <th bgcolor="#EDBB99">تغیر</th>
                    <!-- <th bgcolor="#EDBB99">حذف</th> -->
                    </tr>
                    </thead>
                                         
                    
                    <tbody id="search_content">
                    @if(!empty($employee_info))
                       
                        <?php $counter=1;
                     
                       ?>
                        
        
                    @foreach ($employee_info as $val)
                    <tr>
                      <td bgcolor="#EDBB99">{{$counter++}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->sim_id}}</td>
                      <td><a href="edit_employee/{{$val-> id}}" class="fa fa-edit"></a></td> 
                      <!-- <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                             -->
                    </tr> 
                 
                    @endforeach 
                    
                    @else
                    <tr>
                      <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                   </tr>
                    @endif
                 </tbody>
                </table>
                <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
                <div class="test-center"> 
             
               </div>
              
              </div>
            </div>
          </div>
        </div>
       
          
    </div>
  </div>
</div>
  <script type="text/javascript">
            //get the contract type list for datatable
            $(document).ready(function() {
              
              $("#search_ing_submit").click(function(){
                var field_value = $('#simcard_number').val();
                if(field_value !="")
                $.ajax({
                  type : "post",
                  url : "{!!URL::route('search_emp_info')!!}",
                  data : {"search_string2": field_value, "_token": "<?=csrf_token();?>"},
                  success : function(response)
                  {

                    $("#search_content").html(response);
                  }
                });
                  return false;
              });
              
            });


         </script> 


@stop
