@extends('layouts.master')
@section('content')
<style type="text/css">
  
img{
  width: 200px;
  height: 200px;
  align-content: center;
}

</style>
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h1>سیستم معلوماتی مدیریت عمومی تلیفون </h1></center>
    <ol class="breadcrumb">
     
      <li class="active"><h4>برای دونلود بالای عکس کلک نماید</h4></li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
     <div class="container">

       @foreach($files as $file)
                
        <a href="{{asset('phone_uploads/'.$file->file_name.'')}}" download><img src="{{asset('phone_uploads/'.$file->file_name.'')}}"></a>

       @endforeach

     </div>
  </div>
</div>
<br>
<a href="{!!URL::route('phoneSimcardList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
@stop

@section('footer-scripts') 
<script type="text/javascript">

</script> 

@stop