
<head>
  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
@extends('layouts.master')
@section('content')
<div class="container" dir="rtl" >
  <div class="page-head">
    <h3 style="text-align: center;">مدیریت عمومی تلیفون ها</h3>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
      <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(2)").addClass("active");

      </script>
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
   <!--   style="background-color: #22DDE6;" -->
      <ul class="nav nav-tabs" id="ul_tabs">
        
     <!--   <li  ><a href="#tab10" data-toggle="tab" >مکافات و مجازات</a></li>-->
        
        <li  ><a href="#tab1" data-toggle="tab">Salam</a></li>
        <li  ><a href="#tab2" data-toggle="tab">Afghan Telecom</a></li>
        <li  ><a href="#tab3" data-toggle="tab">AWCC</a></li>
        <li  ><a href="#tab4" data-toggle="tab">Wasel Telecom</a></li>
        <li  ><a href="#tab5" data-toggle="tab">Etesalat</a></li>      
        <li  ><a href="#tab6" data-toggle="tab">MTN</a></li>      
        <li  ><a href="#tab7" data-toggle="tab">Roshan</a></li>
        <!-- <li  ><a href="#tab8" data-toggle="tab">Storage</a></li> -->
        <li  ><a href="#tab9" data-toggle="tab">Activation</a></li>
        <li  ><a href="#tab10" data-toggle="tab">Search</a></li>
        <li  ><a href="#tab11" data-toggle="tab">USB Dongle</a></li>
        <li  ><a href="#tab12" data-toggle="tab">020 Phone</a></li>
        <li  ><a href="#tab13" data-toggle="tab">Siemens Phone</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active cont" id="tab1"> 
          <!-- DataTables for Contract Type -->
         
          <div class="">
           <div class="header">
            <br>
            <div class="row">
              <div class="pull-left">
              </div>
              <div class="col-md-6 pull-right" dir="ltr">
                <form class="form-horizontal group-border-dashed" id="search_form" action="get" name="radio">
                <!--   -->
               
                </form>


              </div>
            </div>
            <h3 style="margin-top:20px;" align="pull-right"></h3><hr />

             
            </div>
           
            <div class="content">
              <div>
               <!--  <center> <header id="header1" ><strong style="width: 100%; font-size: 50px;"> این سافتویر امتحانی میباشد </strong><small style=" font-size: 30px;">که توسط زاهدالله خټک ساخته شده </small ></header></center>     -->
             
               <table class="table table-bordered table-responsive" id="datalist2" style="border:0px solid #E67E22;">

              <thead>
                  
                                    
                  <thead>
                    <tr>
                    <th>شماره#</th>
                     <th>اسم گیرنده </th>
                    <th>تخلس</th>
                    <th>کمپنی مخابراتی</th>
                    <th>شماره سیم کارت</th>
                    <th>تعین کریدیت </th>
                    <th>حدود جنس</th>
                    <th>نوع سیم کارت</th>
                    <th>حالت سیم کارت</th>
                    <th>تاریخ  توزیع</th>
                    <th>نوع جنس</th>
                    <th>دونلود</th>
                    <th>تغیر</th>
                    <th>حذف</th>
                    </thead>
                                        
                    </tr>
                       @if(!empty($salam))
                       
                        <?php $counter=1;
                              $totale_salam=0;
                              $totale_salam_credit=0;
                              
                       ?>
                       
                        
                      @foreach ($salam as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->simcard_number}}</td>
                      <td>{{$val->MB_credit}}</td>
                      <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>
                      <td>@if($val->activation==1)Active @elseif($val->activation == 2)Deactive @endif</td>
                      <td>{!!checkEmptyDate($val->issue_date)!!}</td>
                      <td>@if($val->equipment_id==1)SIM Card @elseif($val->equipment_id==2) USB Dongle @elseif($val->equipment_id==3) 020 Phone @endif</td>
                     <td><a href="{!!URL::route('downloadPhoneFile',$val->id)!!}"><button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-download-alt"></i></button></a></td>
                     <td><a href="edit_equp/{{$val-> id}}" class="fa fa-edit"></a></td>          
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td> 
 
                      </tr> 
                   </tr> <?php $counter++;
                               $totale_salam++;
                               $totale_salam_credit += $val->MB_credit;
                             
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                <div>
                  <h5  style="color: green;"> مجموع سیمکارت های سلام   : {{$totale_salam or '0'}}</h5> 
                 <h5 style="color: green;"> مجموع  کریدیت  : {{$totale_salam_credit or '0'}}</h5>
                 </div> 
                     </thead>
                  <tbody>
                  </tbody>
                 <!--  -->
                  
                </table>

                  <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
                  
            
                <div class="test-center"> 
                  
               </div>
              
              </div>
            </div>
          </div>
        </div>
       
          <div class="tab-pane cont" id="tab2"> 
          <div class="col col-12">
      <div class="header"> <br> <br>
      <div class="row">
              <div class="pull-left">
              
              </div>
        <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form" action="">
                
                </form>
            
              </div>
               
      </div>
      <h3 style="margin-top:20px; text-align:right" align="pull-right"></h3>
              <hr />
              
            <div class="content">
             <table class="table table-bordered table-responsive" id="datalist2" style="border:0px solid #E67E22;">
              <thead>
                  
                                    
                  <thead>
                    <tr>
                    <th>شماره#</th>
                     <th>اسم گیرنده </th>
                    <th>تخلس</th>
                    <th>کمپنی مخابراتی</th>
                    <th>شماره سیم کارت</th>
                    <th>تعین کریدیت </th>
                    <th>حدود جنس</th>
                    <th>نوع سیم کارت</th>
                    <th>حالت سیم کارت</th>
                    <th>تاریخ  توزیع</th>
                     <th>نوع جنس</th>
                    
                    <th>دونلود</th>
                    <th>تغیر</th>
                    <th>حذف</th>
                    </thead>
                                         
                    </tr>
                    @if(!empty($a_telecom))
                       
                        <?php $counter=1;
                              $totalea_telecom=0;
                              $totalea_telecom_credit=0;
                       ?>
                        
        
                    @foreach ($a_telecom as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->simcard_number}}</td>
                      <td>{{$val->MB_credit}}</td>
                      <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>
                      <td>@if($val->activation==1)Active @elseif($val->activation == 2)Deactive @endif</td>
                      <td>{!!checkEmptyDate($val->issue_date)!!}</td>
                      <td>@if($val->equipment_id==1)SIM Card @elseif($val->equipment_id==2) USB Dongle @elseif($val->equipment_id==3) 020 Phone @endif</td>
                     <!--  <td>{{$val->file}}</td> -->
                     <td><a href="{!!URL::route('downloadPhoneFile',$val->id)!!}"><button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-download-alt"></i></button></a></td>
                      <td><a href="edit_equp/{{$val-> id}}" class="fa fa-edit"></a></td>          
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                            
                      </tr> 
                   </tr> <?php $counter++;
                               $totalea_telecom++;
                               $totalea_telecom_credit += $val->MB_credit;
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                <div> 
                <h5 style="color: green;"> مجموع سیمکارت های  افغان  تیلیکام   :  {{$totalea_telecom or '0'}}</h5> 
                 <h5 style="color: green;"> مجموع  کریدیت  : {{$totalea_telecom_credit or '0'}}</h5> 
                 </div>
                     </thead>
                  <tbody>
                  </tbody>
                
                   

                </table>
                <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a> 

                 
          </div>
        </div>
      </div>
    </div>

    <div class="tab-pane cont" id="tab3"> 
          <div class="col col-12">
      <div class="header"> <br> <br>
      <div class="row">
              <div class="pull-left">
              </div>
        <div class="col-md-6 pull-right" dir="ltr">
                <form class="form-horizontal group-border-dashed" id="search_form2" action="">
                
                </form>
            
              </div>
               
      </div>
     <h3 style="margin-top:20px; text-align:right" align="pull-right"></h3>
              <hr />
            <div class="content">
             <table class="table table-bordered table-responsive" id="datalist3" style="border:0px solid #E67E22;">
              <thead>
                  
                                    
                  <thead>
                    <tr>
                    <th>شماره#</th>
                     <th>اسم گیرنده </th>
                    <th>تخلس</th>
                    <th>کمپنی مخابراتی</th>
                    <th>شماره سیم کارت</th>
                    <th>تعین کریدیت </th>
                    <th>حدود جنس</th>
                    <th>نوع سیم کارت</th>
                    <th>حالت سیم کارت</th>
                    <th>تاریخ  توزیع</th>
                     <th>نوع جنس</th>
                    
                    <th>دونلود</th>
                    <th>تغیر</th>
                    <th>حذف</th>
                    </thead>
                                         
                    </tr>
                    @if(!empty($awcc))
                       
                        <?php $counter=1;
                              $totale_awcc=0;
                              $totale_awcc_credit=0;
                       ?>
                        
        
                    @foreach ($awcc as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->simcard_number}}</td>
                      <td>{{$val->MB_credit}}</td>
                      <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>
                      <td>@if($val->activation==1)Active @elseif($val->activation == 2)Deactive @endif</td>
                      <td>{!!checkEmptyDate($val->issue_date)!!}</td>
                      <td>@if($val->equipment_id==1)SIM Card @elseif($val->equipment_id==2) USB Dongle @elseif($val->equipment_id==3) 020 Phone @endif</td>
                     <!--  <td>{{$val->file}}</td> -->
                    <td><a href="{!!URL::route('downloadPhoneFile',$val->id)!!}"><button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-download-alt"></i></button></a></td>
                      <td><a href="edit_equp/{{$val-> id}}" class="fa fa-edit"></a></td>          
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                            
                      </tr> 
                   </tr> <?php $counter++;
                               $totale_awcc++; 
                               $totale_awcc_credit += $val->MB_credit;
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                <div>
                 <h5 style="color: green;"> مجموع سیمکارت های  افغان بیسیم  :  {{$totale_awcc or '0'}}</h5> 
                 <h5 style="color: green;"> مجموع  کریدیت  : {{$totale_awcc_credit or '0'}}</h5> 
                 </div>
                     </thead>
                  <tbody>
                  </tbody>
                

                </table>
                <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
          </div>
        </div>
      </div>
    </div>
          
      
   <div class="tab-pane cont" id="tab4"> 
          <div class="col col-12">
      <div class="header"> <br> <br>
      <div class="row">
              <div class="pull-left">
              </div>
        <div class="col-md-6 pull-right" dir="ltr">
                <form class="form-horizontal group-border-dashed" id="search_form3" action="">
             
                </form>
            
              </div>
               
      </div>
     <h3 style="margin-top:20px; text-align:right" align="pull-right"></h3>
              <hr />
            <div class="content">
             <table class="table table-bordered table-responsive" id="datalist4" style="border:0px solid #E67E22;">
              <thead>
                  
                                    
                  <thead>
                    <tr>
                    <th>شماره#</th>
                     <th>اسم گیرنده </th>
                    <th>تخلس</th>
                    <th>کمپنی مخابراتی</th>
                    <th>شماره سیم کارت</th>
                     <th>تعین کریدیت </th>
                    <th>حدود جنس</th>
                    <th>نوع سیم کارت</th>
                    <th>حالت سیم کارت</th>
                    <th>تاریخ  توزیع</th>
                     <th>نوع جنس</th>
                    
                    <th>دونلود</th>
                    <th>تغیر</th>
                    <th>حذف</th>
                    </thead>
                                         
                    </tr>
                    @if(!empty($w_telecom))
                       
                        <?php $counter=1;
                              $totale_w_telecom=0;
                              $totale_w_telecom_credit=0;
                       ?>
                        
        
                    @foreach ($w_telecom as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->simcard_number}}</td>
                      <td>{{$val->MB_credit}}</td>
                      <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>
                      <td>@if($val->activation==1)Active @elseif($val->activation == 2)Deactive @endif</td>
                      <td>{!!checkEmptyDate($val->issue_date)!!}</td>
                      <td>@if($val->equipment_id==1)SIM Card @elseif($val->equipment_id==2) USB Dongle @elseif($val->equipment_id==3) 020 Phone @endif</td>
                     <!--  <td>{{$val->file}}</td> -->
                     <td><a href="{!!URL::route('downloadPhoneFile',$val->id)!!}"><button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-download-alt"></i></button></a></td>
                      <td><a href="edit_equp/{{$val-> id}}" class="fa fa-edit"></a></td>          
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                            
                      </tr> 
                   </tr> <?php $counter++;
                               $totale_telecom++; 
                               $totale_w_telecom_credit += $val->MB_credit;
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                    <div>
                 <h5 style="color: green;"> مجموع سیمکارت های  وصل تیلیکام   : {{$totale_w_telecom or '0'}}</h5> 
                 <h5  style="color: green;"> مجموع  کریدیت  : {{$totale_w_telecom_credit or '0'}}</h5> 
                 </div>
                     </thead>
                  <tbody>
                  </tbody>
                

                </table>
                <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
          </div>
        </div>
      </div>
    </div>

    <div class="tab-pane cont" id="tab5"> 
          <div class="col col-12">
      <div class="header"> <br> <br>
      <div class="row">
              <div class="pull-left">

              </div>
         <div class="col-md-6 pull-right" dir="ltr">
                <form class="form-horizontal group-border-dashed" id="search_form4" action="">
                
                </form>
            
              </div>
               
      </div>
          <h3 style="margin-top:20px; text-align:right" align="pull-right"></h3>
              <hr />
            <div class="content">
             <table class="table table-bordered table-responsive" id="datalist5" style="border:0px solid #E67E22;">
              
               <thead>
                  
                                    
                  <thead>
                    <tr>
                    <th>شماره#</th>
                     <th>اسم گیرنده </th>
                    <th>تخلس</th>
                    <th>کمپنی مخابراتی</th>
                    <th>شماره سیم کارت</th>
                     <th>تعین کریدیت </th>
                    <th>حدود جنس</th>
                    <th>نوع سیم کارت</th>
                    <th>حالت سیم کارت</th>
                    <th>تاریخ  توزیع</th>
                     <th>نوع جنس</th>
                    
                    <th>دونلود</th>
                    <th>تغیر</th>
                    <th>حذف</th>
                    </thead>
                                         
                    </tr>
                    @if(!empty($etesalat))
                       
                        <?php $counter=1;
                              $totale_etesalat=0;
                              $totale_etesalat_credit=0;
                       ?>
                        
        
                    @foreach ($etesalat as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->simcard_number}}</td>
                      <td>{{$val->MB_credit}}</td>
                      <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>
                      <td>@if($val->activation==1)Active @elseif($val->activation == 2)Deactive @endif</td>
                      <td>{!!checkEmptyDate($val->issue_date)!!}</td>
                      <td>@if($val->equipment_id==1)SIM Card @elseif($val->equipment_id==2) USB Dongle @elseif($val->equipment_id==3) 020 Phone @endif</td>
                     <!--  <td>{{$val->file}}</td> -->
                     <td><a href="{!!URL::route('downloadPhoneFile',$val->id)!!}"><button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-download-alt"></i></button></a></td>
                      <td><a href="edit_equp/{{$val-> id}}" class="fa fa-edit"></a></td>          
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                            
                      </tr> 
                   </tr> <?php $counter++;
                               $totale_etesalat++; 
                               $totale_etesalat_credit += $val->MB_credit;
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                    <div>
                 <h5 style="color: green;"> مجموع سیمکارت های  اتصالات   :  {{$totale_etesalat or '0'}}</h5> 
                 <h5  style="color: green;"> مجموع  کریدیت  : {{$totale_etesalat_credit or '0'}}</h5>
                 </div> 
                     </thead>
                  <tbody>
                  </tbody>
                
                </table>
               
            <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane cont" id="tab6"> 
          <div class="col col-12">
      <div class="header"> <br> <br>
      <div class="row">
              <div class="pull-left">
              </div>
         <div class="col-md-6 pull-right" dir="ltr">
                <form class="form-horizontal group-border-dashed" id="search_form5" action="">
                 
                </form>
            
              </div>
               
      </div> 
      <h3 style="margin-top:20px;text-align:right;"></h3>
              <hr />
            <div class="content">
             <table class="table table-bordered table-responsive" id="datalist6" style="border:0px solid #E67E22;">
                <thead>
                  
                                    
                  <thead>
                    <tr>
                    <th>شماره#</th>
                     <th>اسم گیرنده </th>
                    <th>تخلس</th>
                    <th>کمپنی مخابراتی</th>
                    <th>شماره سیم کارت</th>
                     <th>تعین کریدیت </th>
                    <th>حدود جنس</th>
                    <th>نوع سیم کارت</th>
                    <th>حالت سیم کارت</th>
                    <th>تاریخ  توزیع</th>
                     <th>نوع جنس</th>
                    
                    <th>دونلود</th>
                    <th>تغیر</th>
                    <th>حذف</th>
                    </thead>
                                         
                    </tr>
                    @if(!empty($mtn))
                       
                        <?php $counter=1;
                              $totale_mtn=0;
                              $totale_mtn_credit=0;
                       ?>
                        
        
                    @foreach ($mtn as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->simcard_number}}</td>
                      <td>{{$val->MB_credit}}</td>
                      <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>
                      <td>@if($val->activation==1)Active @elseif($val->activation == 2)Deactive @endif</td>
                      <td>{!!checkEmptyDate($val->issue_date)!!}</td>
                      <td>@if($val->equipment_id==1)SIM Card @elseif($val->equipment_id==2) USB Dongle @elseif($val->equipment_id==3) 020 Phone @endif</td>
                      <!-- <td>{{$val->file_name}}</td> -->

                      <td><a href="{!!URL::route('downloadPhoneFile',$val->id)!!}"><button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-download-alt"></i></button></a></td>
                      <td><a href="edit_equp/{{$val-> id}}" class="fa fa-edit"></a></td>          
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                            
                      </tr> 
                   </tr> <?php $counter++;
                               $totale_mtn++; 
                               $totale_mtn_credit += $val->MB_credit;
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                <div class="col col-12"> <h5 style="color: green;"> مجموع سیمکارت های  ام تی ان   :  {{$totale_mtn or '0'}}</h5>
                 <h5  style="color: green;"> مجموع  کریدیت  : {{$totale_mtn_credit or '0'}}</h5></div>
                 
                     </thead>
                  <tbody>
                  </tbody>
                
                </table>
                <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
           
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane cont" id="tab7"> 
          <div class="col col-12">
      <div class="header"> <br> <br>
      <div class="row">
              <div class="pull-left">
              </div>
          <div class="col-md-6 pull-right" dir="ltr">
                <form class="form-horizontal group-border-dashed" id="search_form7" action="">
                 
                </form>
            
              </div>
               
      </div> 
      <h3 style="margin-top:20px;text-align:right;"></h3>
              <hr />
            <div class="content">
             <table class="table table-bordered table-responsive" id="datalist2" style="border:0px solid #E67E22;">
              <thead>
                  
                                    
                  <thead>
                    <tr>
                    <th>شماره#</th>
                     <th>اسم گیرنده </th>
                    <th>تخلس</th>
                    <th>کمپنی مخابراتی</th>
                    <th>شماره سیم کارت</th>
                     <th>تعین کریدیت </th>
                    <th>حدود جنس</th>
                    <th>نوع سیم کارت</th>
                    <th>حالت سیم کارت</th>
                    <th>تاریخ  توزیع</th>
                     <th>نوع جنس</th>
                    
                    <th>دونلود</th>
                    <th>تغیر</th>
                    <th>حذف</th>
                    </thead>
                                         
                    </tr>
                       @if(!empty($roshan))
                       
                        <?php $counter=1;
                              $totale_roshan=0;
                              $totale_roshan_credit=0;
                       ?>
                        
                      @foreach ($roshan as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->simcard_number}}</td>
                      <td>{{$val->MB_credit}}</td>
                      <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>
                      <td>@if($val->activation==1)Active @elseif($val->activation == 2)Deactive @endif</td>
                      <td>{!!checkEmptyDate($val->issue_date)!!}</td>
                      <td>@if($val->equipment_id==1)SIM Card @elseif($val->equipment_id==2) USB Dongle @elseif($val->equipment_id==3) 020 Phone @endif</td>
                     <!--  <td>{{$val->file}}</td> -->
                     <td><a href="{!!URL::route('downloadPhoneFile',$val->id)!!}"><button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-download-alt"></i></button></a></td>
                      <td><a href="edit_equp/{{$val-> id}}" class="fa fa-edit"></a></td>        
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                            
                      </tr> 
                   </tr> <?php $counter++;
                               $totale_roshan++; 
                               $totale_roshan_credit += $val->MB_credit;
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                    <div>
                  <h5 style="color: green;"> مجموع سیمکارت های  روشن   : {{$totale_roshan or '0'}}</h5> 
                  <h5  style="color: green;"> مجموع  کریدیت  : {{$totale_roshan_credit or '0'}}</h5> 
                  </div>
                     </thead>
                  <tbody>
                  <!--  -->
                  </tbody>
         
                </table>
                <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
           
          </div>
        </div>
      </div>
    </div>
    <!--  -->
     <div class="tab-pane cont" id="tab9"> 
          <div class="col col-12">
      <div class="header"> <br> <br>
      <div class="row">
              <div class="pull-left">
              </div>
          <div class="col-md-6 pull-right" dir="ltr">
                <form class="form-horizontal group-border-dashed" id="search_form7" action="">
                 
                </form>
            
              </div>
               
      </div> 
      <h3 style="margin-top:20px;text-align:right;"></h3>
              <hr />
            <div class="content">
             <table class="table table-bordered table-responsive" id="datalist9" style="border:0px solid #E67E22;">
              <thead>
                  
                                    
                  <thead>
                    <tr>
                    <th>شماره#</th>
                     <th>اسم گیرنده </th>
                    <th>تخلس</th>
                    <th>کمپنی مخابراتی</th>
                    <th>شماره سیم کارت</th>
                     <th>تعین کریدیت </th>
                    <th>حدود جنس</th>
                    <th>نوع سیم کارت</th>
                    <th>حالت سیم کارت</th>
                     <th>تاریخ  توزیع</th>
                    <th>تغیر</th>
                    <th>حذف</th>
                    </thead>
                                         
                    </tr>
                       @if(!empty($activation))
                       
                       <?php $counter=1;
                             $totale_DA=0;
                       ?>
                        
                      @foreach ($activation as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->simcard_number}}</td>
                      <td>{{$val->MB_credit}}</td>
                      <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>
                      <td>@if($val->activation==1)Active @elseif($val->activation == 2)Deactive @endif</td>
                      <td>{!!checkEmptyDate($val->issue_date)!!}</td>
                      <td>@if($val->equipment_id==1)SIM Card @elseif($val->equipment_id==2) USB Dongle @elseif($val->equipment_id==3) 020 Phone @endif</td>
                      <td><a href="edit_equp/{{$val-> id}}" class="fa fa-edit"></a></td>            
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                            
                      </tr> 
                   </tr> <?php $counter++;
                               $totale_DA++; 
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                 <h3 align='center' style="color: green;"> مجموع  سیمکارت های  غیر فعال  :{{$totale_DA or '0'}}</h3> 
                     </thead>
                  <tbody>
                          <tr>
               
              </tr>
                  </tbody>
         
                </table>
                <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
           
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane cont" id="tab10"> 
          <div class="col col-12">
      <div class="header"> <br> <br>
      <div class="row">
             
         <div class="cl-mcont" id="sdu_result">
           <form class="form-horizontal group-border-dashed" action="export_excel" method="post" id="search_inge" >
               <div class="form-group">
              <input type="hidden" name="_token" value="{!!csrf_token()!!}">

             <div class="col-sm-3">
              <label>اسم گیرنده</label>
               <input type="text" name="name" class="form-control">
               
              <label>شماره سیم کارت</label>
               <input type="text" name="simcard_number" class="form-control">
                <label>ذخیره / توزیع</label>
                 <select class="form-control" name="storage">
                  <option value="">-----</option>
                   <option value="1">Stock in</option>
                   <option value="2">Stock out</option>
                    </select>
                <label>نوع جنس </label>
                <select class="form-control" name="equipment_type">
                  <option value="">--- برای انتخاب  نوع جنس ---</option>
                  @foreach($equptype as $equip)
                 <option value="{{$equip->id}}">{{$equip->equipment_name}}</option>
                 @endforeach
                </select>
                </div>

              <div class="col-sm-3">

              <label>پوسټ پیډ/پری پیډ</label>
                 <select class="form-control" name="postpaid">
                  <option value="">--- برای انتخاب سیمکارت ---</option>
                  <option value="1">postpaid</option>
                  <option value="2">prepaid</option>
                
                 </select>
               </div>
              
              <div class="col-sm-3">
               <label>حدود سیم کارت</label>
                 <select class="form-control" name="equipment_range">
                  <option value="">--- برای انتخاب سیمکارت ---</option>
                  <option value="1">Limited</option>
                  <option value="2">Unlimited</option>
                  </select>
              </div>
               <div class="col-sm-3" >
                <label>شماره سیم کارت</label>
               <input type="text" name="simcard_number" class="form-control">
                <label>انتخاب کمپنی</label>
                <select class="form-control" name="company">
                  <option value="">--- برای انتخاب کمپنی ---</option>
                  @foreach($company as $comp)
                 <option value="{{$comp->id}}">{{$comp-> com_name}}</option>
                 @endforeach
                </select>
              </div>

              </div>
            </div>

                {!!Form::token()!!}
                  <div class="form-group" >
                    <input type="button" value="Search" class="btn btn-success" id="search_ing_submit" style="background-color: #008080" />
                    <a href="{!!URL::route('simcardsList')!!}">
                     <input type="button" value=" برگشت به صفحه  " id="add_department" class="btn btn-info"/>
                    </a>
                 <input type="submit" value="Eexel" id="add_department" class="btn btn-success"/>

                   </div>
                   
            </form>
           </div>
               
      </div>
            <div class="content" id="search_content">
     



          </div>
        </div>
      </div>
      <div class="tab-pane cont" id="tab11"> 
          <div class="col col-12">
      <div class="header"> <br> <br>
      <div class="row">
              <div class="pull-left">
              
              </div>
        <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form" action="">
                
                </form>
            
              </div>
               
      </div>
      <h3 style="margin-top:20px; text-align:right" align="pull-right"></h3>
              <hr />
            <div class="content">
             <table class="table table-bordered table-responsive" id="datalist2" style="border:0px solid #E67E22;">
              <thead>
                  
                                    
                  <thead>
                    <tr>
                    <th>شماره#</th>
                     <th>اسم گیرنده </th>
                    <th>تخلس</th>
                    <th>کمپنی مخابراتی</th>
                    <th>شماره سیم کارت</th>
                     <th>تعین کریدیت </th>
                    <th>حدود جنس</th>
                    <th>نوع سیم کارت</th>
                    <th>حالت سیم کارت</th>
                    <th>تاریخ  توزیع</th>
                    <th>نوع جنس</th>
                    <th>دونلود</th>
                    <th>تغیر</th>
                    <th>حذف</th>
                    </thead>
                                         
                    </tr>
                    @if(!empty($usbdongle))
                       
                        <?php $counter=1;
                              $totale_usbdongle=0;
                       ?>
                        
        
                    @foreach ($usbdongle as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->simcard_number}}</td>
                      <td>{{$val->MB_credit}}</td>
                      <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>
                      <td>@if($val->activation==1)Active @elseif($val->activation == 2)Deactive @endif</td>
                      <td>{{$val->issue_date}}</td>
                      <td>@if($val->equipment_id==1)SIM Card @elseif($val->equipment_id==2) USB Dongle @elseif($val->equipment_id==3) 020 Phone @endif</td>
                      <td><a href="{!!URL::route('downloadPhoneFile',$val->id)!!}"><button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-download-alt"></i></button></a></td>
                      <td><a href="edit_dongle/{{$val-> id}}" class="fa fa-edit"></a></td>          
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                            
                      </tr> 
                   </tr> <?php $counter++;
                                $totale_usbdongle++;
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                  <h3 align='center' style="color: green;"> مجموع  یوزبی دنگل ها  :{{$totale_usbdongle or '0'}}</h3> 
                     </thead>
                  <tbody>
                          <tr>
               
              </tr>
                  </tbody>
                

                </table>
                <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane cont" id="tab12"> 
          <div class="col col-12">
      <div class="header"> <br> <br>
      <div class="row">
              <div class="pull-left">
              
              </div>
        <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form" action="">
                
                </form>
            
              </div>
               
      </div>
      <h3 style="margin-top:20px; text-align:right" align="pull-right"></h3>
              <hr />
            <div class="content">
             <table class="table table-bordered table-responsive" id="datalist2" style="border:0px solid #E67E22;">
              <thead>
                  
                                    
                  <thead>
                    <tr>
                    <th>شماره#</th>
                     <th>اسم گیرنده </th>
                    <th>تخلس</th>
                    <th>کمپنی مخابراتی</th>
                    <th>شماره سیم کارت</th>
                     <th>تعین کریدیت </th>
                    <th>حدود جنس</th>
                    <th>نوع سیم کارت</th>
                    <th>حالت سیم کارت</th>
                    <th>تاریخ  توزیع</th>
                     <th>نوع جنس</th>
                    
                    <th>دونلود</th>
                    <th>تغیر</th>
                    <th>حذف</th>
                    </thead>
                                         
                    </tr>
                    @if(!empty($phone020))
                       
                        <?php $counter=1;
                              $totale_phone020=0;
                       ?>
                        
        
                    @foreach ($phone020 as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->simcard_number}}</td>
                      <td>{{$val->MB_credit}}</td>
                      <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>
                      <td>@if($val->activation==1)Active @elseif($val->activation == 2)Deactive @endif</td>
                      <td>{!!checkEmptyDate($val->issue_date)!!}</td>
                      <td>@if($val->equipment_id==1)SIM Card @elseif($val->equipment_id==2) USB Dongle @elseif($val->equipment_id==3) 020 Phone @elseif($val->equipment_id==4)Siemens Phone  @endif</td>
                     <!--  <td>{{$val->file}}</td> -->
                     <td><a href="{!!URL::route('downloadPhoneFile',$val->id)!!}"><button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-download-alt"></i></button></a></td>
                      <td><a href="edit_020/{{$val-> id}}" class="fa fa-edit"></a></td>          
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                            
                      </tr> 
                   </tr> <?php $counter++;
                               $totale_phone020++;     
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                  <h3 align='center' style="color: green;"> مجموع  تلیفون های  020 :    {{$totale_phone020 or '0'}}</h3> 
                     </thead>
                  <tbody>
                          <tr>
               
              </tr>
                  </tbody>
                

                </table>
                <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane cont" id="tab13"> 
          <div class="col col-12">
      <div class="header"> <br> <br>
      <div class="row">
              <div class="pull-left">
              
              </div>
        <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form" action="">
                
                </form>
            
              </div>
               
      </div>
      <h3 style="margin-top:20px; text-align:right" align="pull-right"></h3>
              <hr />
            <div class="content">
             <table class="table table-bordered table-responsive" id="datalist2" style="border:0px solid #E67E22;">
              <thead>
                  
                                    
                  <thead>
                    <tr>
                    <th>شماره#</th>
                    <th>اسم گیرنده </th>
                    <th>تخلس</th>
                    <th>کمپنی مخابراتی</th>
                    <th>شماره سیم کارت</th>
                    <th>تعین کریدیت </th>
                    <th>حدود جنس</th>
                    <th>نوع سیم کارت</th>
                    <th>حالت سیم کارت</th>
                    <th>تاریخ  توزیع</th>
                     <th>نوع جنس</th>
                    
                    <th>دونلود</th>
                    <th>تغیر</th>
                    <th>حذف</th>
                    </thead>
                                         
                    </tr>
                    @if(!empty($siemensPhone))
                       
                        <?php $counter=1;
                              $totale_siemensPhone=0; 
                       ?>
                        
        
                    @foreach ($siemensPhone as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->simcard_number}}</td>
                      <td>{{$val->MB_credit}}</td>
                      <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>
                      <td>@if($val->activation==1)Active @elseif($val->activation == 2)Deactive @endif</td>
                      <td>{!!checkEmptyDate($val->issue_date)!!}</td>
                      <td>@if($val->equipment_id==1)SIM Card @elseif($val->equipment_id==2) USB Dongle @elseif($val->equipment_id==3) 020 Phone @elseif($val->equipment_id==4)Siemens Phone  @endif</td>
                     <!--  <td>{{$val->file}}</td> -->
                     <td><a href="{!!URL::route('downloadPhoneFile',$val->id)!!}"><button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-download-alt"></i></button></a></td>



                      <td><a href="edit_020/{{$val-> id}}" class="fa fa-edit"></a></td>          
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                            
                      </tr> 
                   </tr> <?php $counter++;
                               $totale_siemensPhone++;   
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                  <h3 align='center' style="color: green;"> مجموع  تلیفون های زیمنس   : {{$totale_siemensPhone or '0'}}</h3> 
                     </thead>
                  <tbody>
                          <tr>
               
              </tr>
                  </tbody>
                

                </table>
                <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>

@stop

@section('footer-scripts') 
<!-- <script type="text/javascript">
//get the contract type list for datatable
$(document).ready(function() {
  
  $("#search_button").click(function(){

    var field_value = $('#search_field').val();
    if(field_value !="")
    $.ajax({
      type : "post",
      url : "{!!URL::route('search_info')!!}",
      data : {"search_string": field_value, "_token": "<?=csrf_token();?>"},
      success : function(response)
      {
        $("#datalist").html(response);
      }
    });
      return false;
  });
  
});


</script> 
 -->
  <script type="text/javascript">

  $(document).ready(function() {
    $('#search_ing_submit').click(function(){

      var dataString = $('#search_inge').serialize();
       $.ajax({
            type:'POST',
            url:"{!!URL::route('search_info')!!}",
            data: dataString,
            success:function(response){
               $('#search_content').html(response); 
               $('.datatable').dataTable({
                language: {
                    search: "جستجو:"
                }
            }
                );
                         }
           })

          return false;
         });
  });
    


// menu activation

</script>
 <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(1)").addClass("active");
  </script>
@stop
