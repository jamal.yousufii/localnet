@extends('layouts.master')
@section('content')
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>سیستم معلوماتی مدیریت عمومی تلیفون </h3></center>
    <ol class="breadcrumb">
     
      <li class="active"><h3>اضافه نمودن اطلاعات  سیمکارت ها </h3></li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed" action="{!!URL::route('insert_mobile')!!}"  enctype="multipart/form-data" method="post" style="border-radius: 0px;">
      <input type="hidden" name="_token" value="{!!csrf_token()!!}">
         
         
          <div class="col-lg-3 col-sm-3">
         <div class="form-group">
          <label>حدود سیمکارت</label>
          <select class="form-control" name="equipment_range" required="">
          <option>--Select Range of SIM Card--</option>
            <option value="1">Limited</option>
            <option value="2">Unlimited</option>
          </select>
          <label>سریل نمبر جنس</label>
          <input type="text" class="form-control" name="equipment_serialno" required>
          <label>شماره سیم کارت</label>
          <input type="text" name="simcard_number" class="form-control">
          <label>ملاحظات</label>
          <textarea class="form-control" name="remark" placeholder="برای معلومات اضافی"></textarea>
          
          
          </div>
          </div>
          <div class="col-lg-3 col-sm-3">
         <div class="form-group">
          <label>تعین کریدت و انترنت</label>
          <input type="text" name="MB_credit" class="form-control" required>  
          <label>تاریخ ثبت</label>
          <input type="text" name="rig_date" class="datepicker_farsi form-control" required>
         <label>قیمت کریدت و انترنت</label>
          <input type="text" name="price" class="form-control" required>  
          <label>نوع  سیم کارت</label>
          <select class="form-control" name="postpaid" required="">
          <option>--Select PostPaid or PrePaid--</option>
            <option value="1">PostPaid</option>
            <option value="2">PrePaid</option>
          </select>  
          </div>
          </div>
         <div class="col-lg-3 col-sm-3">
         <div class="form-group">       
                     
          <label>حالت سیم کارت</label>
          <select class="form-control" name="activation" required="">
          <option>--Select activation--</option>
            <option value="1">Active</option>
            <option value="2">Deactive</option>
          </select> 
          <label>ذخیره /توزیع</label>
          <select class="form-control" name="storage" required="">
          <option>--Select activation--</option>
            <option value="1">Stock in</option>
            <option value="2">Stock out</option>
          </select> 
          <label>تاریخ توزیع</label>
          <input type="text" name="issue_date" class="datepicker_farsi form-control" required>
          </div>
          </div>
          <div class="col-lg-3 col-sm-3">
         <div class="form-group">
         <label>کمپنی</label>
         <select class="form-control" name="company" required="">
         <option>--Select Company--</option>
           @foreach($company as $comp)
            <option value="{{$comp->id}}">{{$comp->com_name}}</option>
            @endforeach
          </select>
          <label>جنس</label>
          <select class="form-control" name="equipment" required="">
          <option>--Select Equipment--</option>
            @foreach($equptype as $equp)
            <option value="{{$equp->id}}">{{$equp->equipment_name}}</option>
            @endforeach
          </select>
          <label>کاپی  اسناد </label>
         <input type="file" class="form-control" name="files[]" multiple placeholder="" />
          </div>
          </div>

          {!!Form::token()!!}
          <div class="form-group">
            <div class="col-sm-3">
            <label class="col-sm-2 control-label"></label>
            <input type="submit" value="ثبت " id="add_department" class="btn btn-success"/>
              <a href="{!!URL::route('sim_management')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>

            </div>
          </div>
        </form>
  </div><hr><br>
     <div class="tab-pane active cont" id="tab1"> 
          <!-- DataTables for Contract Type -->
          <div class="">
           <div class="header">
            <br>
            <div class="row">
             <!-- <div class="pull-left">
               <a href="{!!URL::route('insert_phone')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه کردن معلومات جدید</a>
              </div>-->
              <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form" action="">
                 <div class="input-group custom-search-form">
                 
                 {!!Form::token();!!}
                  <!-- <span class="input-group-btn">
                      <button class="btn btn-default-sm" id="search_button"><i class="fa fa-search"></i> جستجو</button>
                    </span>-->
                  </div>
                </form>
              </div>
            </div>
           <!-- <h3 style="margin-top:20px;" align="center">مدیریت عمومی تلیفون ریاست تکنالوژی معلوماتی معاونیت مالی واداری</h3><hr />-->
            </div>
            <div class="content" id="datalist">
              <div>
               
                     </thead>
                  <tbody>
                  </tbody>
                 
                </table>
                
                <div class="test-center"> 
              <!-- {!! str_replace('/?', '?',$all_data->render()) !!}-->
               </div>
              
              </div>
            </div>
          </div>
        </div>
    </div>
@stop

@section('footer-scripts') 
<script type="text/javascript">

</script> 
<script type="text/javascript">
//get the contract type list for datatable
$(document).ready(function() {
  
  $("#search_button").click(function(){
    var field_value = $('#search_field').val();
    if(field_value !="")
    $.ajax({
      type : "post",
      url : "{!!URL::route('insert_mobile')!!}",
      data : {"search_string": field_value, "_token": "<?=csrf_token();?>"},
      success : function(response)
      {
        $("#datalist").html(response);
      }
    });
      return false;
  });
  
});


</script> 
@stop