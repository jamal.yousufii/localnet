
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('authentication.view_user') }}</h3>
          </div>
        </div>
        <div class="m-portlet__head-tools">
          <ul class="m-portlet__nav">
            <li class="m-portlet__nav-item">
                <a href="#" onclick="redirectFunction()" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                    <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                </a>
            </li>
          </ul>
        </div>
      </div>
      <!--begin::record-->
      @if($record)
        <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
          <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-5 col-md-5">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.user_dep') }} :</span></label><br>
                <span>{!!$record->department['name_'.$lang]!!}</span>
              </div>
              <div class="col-lg-5 col-md-5">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.email') }} :</span></label><br>
                <span>{!!$record->email!!}</span>
              </div>
              <div class="col-lg-2 col-md-2">
                <div style="text-align:center">
                  <img src="{!!asset('attachments/users/'.$record->profile_pic)!!}" class="img img-thumbnail" width="150" />
                </div>
              </div>
            </div>
            <div class="m-portlet__head text-title">
              <div class="m-portlet__head-caption">
                  {{ trans('authentication.user_roles') }}
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom" id="user_roles">
              <div class="col-lg-12 col-md-12">
                <label class="title-custom"><span class="m-widget12__text2">{{ trans('authentication.section') }} :</span></label><br>
                @if($sections)
                  @foreach($sections as $item)
                    {!! $item->sections !!}<br>
                  @endforeach
                @endif
              </div>
            </div>
          </div>
        </div>
      @endif
      <!--end::record-->
    </div>
