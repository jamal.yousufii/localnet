<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">{{ trans('authentication.edit_user') }}</h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      @if($record)
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
          <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-6">
                <label class="title-custom">{{ trans('authentication.user_dep') }} : <span style="color:red;">*</span></label>
                <div id="department_id" class="errorDiv">
                  <select class="form-control m-input m-input--air select-2" name="department_id" onchange="bringModules(this.value);">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($departments)
                      @foreach($departments as $item)
                        @if($item->id == $record->department_id)
                          <option value="{!!$item->id!!}" selected>{!!$item->name!!}</option>
                        @else
                          <option value="{!!$item->id!!}">{!!$item->name!!}</option>
                        @endif
                      @endforeach
                    @endif
                  </select>
                </div>
                <div class="department_id error-div" style="display:none;"></div>
              </div>
              <div class="col-lg-6">
                <label class="title-custom">{{ trans('authentication.email') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="email" value="{!!$record->email!!}" name="email" id="email">
                <div class="email error-div" style="display:none;"></div>
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-6">
                <label class="title-custom">{{ trans('authentication.new_password') }} :</label>
                <div class="m-input-icon m-input-icon--left">
  							  <input class="form-control m-input" type="password" value="" name="password" id="password" placeholder="{{ trans('authentication.password') }}">
  							  <span class="m-input-icon__icon m-input-icon__icon--left" onclick="ShowHide()"><span><i id="icon-pass" class="la la-eye"></i></span></span>
  						  </div>
              </div>
              <div class="col-lg-6">
                <label class="title-custom">{{ trans('authentication.conf_new_password') }} :</label>
                <div class="m-input-icon m-input-icon--left">
  							  <input class="form-control m-input" type="password" value="" name="confirm_password" id="confirm_password" onkeyup="ConfirmPassword()" placeholder="{{ trans('authentication.confirm_password') }}">
  							  <span class="m-input-icon__icon m-input-icon__icon--left" onclick="ShowHideConf()"><span><i id="icon-passConf" class="la la-eye"></i></span></span>
  						  </div>
                <span id="msg"><span>
              </div>
            </div>
            <div class="m-portlet__head text-title">
              <div class="m-portlet__head-caption">{{ trans('authentication.user_roles') }}</div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom" id="user_roles">
              <div class="col-lg-12" id="section_div">
                <label class="title-custom">{{ trans('authentication.section') }}:</label><br>
                <select class="form-control m-input m-input--air select-2" name="sections[]" multiple>
                  <option value="">{{ trans('global.select') }}</option>
                  @if($sections)
                    @foreach($sections as $item)
                      @if(in_array($item->id,$userSections))
                        <option value="{!!$item->id!!}" selected >{!!$item->name!!}</option>
                      @else
                        <option value="{!!$item->id!!}">{!!$item->name!!}</option>
                      @endif
                    @endforeach
                  @endif
                </select>
              </div>   
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-12">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions--solid">
                    <input type="hidden" name="enc_id" id="enc_id" value="{!!$enc_id!!}"/>
                    <button type="button" onclick="doEditRecord('{{route('users.update',$enc_id)}}','requestForm','PUT','response_div')" class="btn btn-primary">{{ trans('global.submit') }}</button>
                    <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @csrf
        </form>
      @endif
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
$('.select-2').select2();

function ShowHide()
{
  if($('#password').attr("type") == "text"){
    $('#password').attr('type', 'password');
    $('#icon-pass').addClass( "la-eye" );
    $('#icon-pass').removeClass( "la-eye-slash" );
  }else if($('#password').attr("type") == "password"){
    $('#password').attr('type', 'text');
    $('#icon-pass').removeClass( "la-eye" );
    $('#icon-pass').addClass( "la-eye-slash" );
  }
}

function ShowHideConf()
{
  if($('#confirm_password').attr("type") == "text"){
    $('#confirm_password').attr('type', 'password');
    $('#icon-passConf').addClass( "la-eye" );
    $('#icon-passConf').removeClass( "la-eye-slash" );
  }else if($('#confirm_password').attr("type") == "password"){
    $('#confirm_password').attr('type', 'text');
    $('#icon-passConf').removeClass( "la-eye" );
    $('#icon-passConf').addClass( "la-eye-slash" );
  }
}

function ConfirmPassword()
{
  var pass = $('#password').val();
  var conf = $('#confirm_password').val();
  if(conf == pass){
    $("#msg").css("color", "green");
    $('#msg').html("{{ trans('authentication.pass_match') }}");
  }else{
    $("#msg").css("color", "red");
    $('#msg').html("{{ trans('authentication.pass_not_match') }}");
  }
}

function bringModules(id)
{
  $.ajax({
      url: '{{ route("getModulesByDepartment") }}',
      data: {
          "_method" : 'POST',
          "id"      : id,
      },
      type: 'post',
      success: function(response)
      {
        $("#applications").html(response);
      }
  });
}

function bringSections()
{
  var modules = $("#applications").val();
  var userSections = $("#userSections").val();
  $.ajax({
      url: '{{ route("getSectionsByModule") }}',
      data: {
          "_method"      : 'POST',
          "modules"      : modules,
          "userSections" : userSections,
      },
      type: 'post',
      success: function(response)
      {
        $("#sectins").html(response);
      }
  });
}

function bringRoles()
{
  var sections = $("#sectins").val();
  var userRole = $("#userRoles").val();
  $.ajax({
      url: '{{ route("getRolesBySections") }}',
      data: {
          "_method"  : 'POST',
          "sections" : sections,
          "userRole" : userRole,
      },
      type: 'post',
      success: function(response)
      {
        $("#roles").html(response);
      }
  });
}
$(".select-2").select2();
</script>
