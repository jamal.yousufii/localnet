<table class="table table-striped- table-bordered table-hover table-checkable">
  <thead>
    <tr>
      <th width="8%">{{ trans('authentication.rec_id') }}</th>
      <th width="15%">{{ trans('authentication.email') }}</th>
      <th width="20%">{{ trans('authentication.user_dep') }}</th>
      <th width="6%">{{ trans('global.action') }}</th>
    </tr>
  </thead>
  <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
  @if($records)
    @foreach($records AS $rec)
      <?php $enc_id = encrypt($rec->id); ?>
      <tr>
        <td>{!!$rec->id!!}</td>
        <td>{!!$rec->email!!}</td>
        <td>{!!$rec->department['name_'.$lang]!!}</td>
        <td>
          <span class="dtr-data">
            <span class="dropdown">
              <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"><i class="la la-ellipsis-h"></i></a>
              <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-32px, 27px, 0px);">
                <a class="dropdown-item" href="javascript:void()" onclick="viewRecord('{{route('users.show',$enc_id)}}','','GET','response_div')"><i class="la la-file-text"></i>{{ trans('authentication.view') }}</a>
                <a class="dropdown-item" href="javascript:void()" onclick="editRecord('{{route('users.edit',$enc_id)}}','','GET','response_div')"><i class="la la-edit"></i>{{ trans('authentication.edit') }}</a>
              </div>
            </span>
          </span>
        </td>
      </tr>
    @endforeach
  @endif
  </tbody>
</table>
<!-- Pagination -->
@if(!empty($records))
  {!!$records->links('pagination')!!}
@endif
<script type="text/javascript">
    $(document).ready(function()
    {
        $('.pagination a').on('click', function(event) {
            event.preventDefault();
            if ($(this).attr('href') != '#') {
                document.cookie = "no="+$(this).text();
                var dataString = '';
                item = $('#keyWord').val();
                dataString += "&page="+$(this).attr('id')+"&ajax="+1+"&item="+item+"&type=search";
                        $.ajax({
                        url: '{{ route("filterUser") }}',
                        data: dataString,
                        type: 'get',
                        beforeSend: function(){
                            $('#searchresult').html('<span style="position:relative;left:30%;"><img alt="" src="{!!asset('img/loader.gif')!!}" /></span>');
                        },
                        success: function(response)
                        {
                            $('#searchresult').html(response);
                        }
                    }
                );
            }
        });
    });
</script>