<div class="m-portlet__head table-responsive">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">ثبت سروی کارمندان ادارات در سکتور تکنالوژی معلوماتی</h3>
        </div>
    </div>
</div>
<form  style="border:1px solid #ebedf2" class=" m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
    <div class="m-portlet__body">
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
                <label class="title-custom">اسم : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" name="name" id="name">
                <div class="name error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-6">
                <label class="title-custom">اسم پدر : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" name="father" id="father">
                <div class="father error-div" style="display:none;"></div>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
                <label class="title-custom">ولایت : <span style="color:red;">*</span></label>
                <select class="form-control m-input input-lg m-input--air select-2" name="province" id="province" style="width:100%;">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($provinces)
                        @foreach($provinces as $pro)
                            <option value="{!!$pro->id!!}">{!!$pro->name!!}</option>
                        @endforeach
                    @endif
                </select>
                <span class="m-form__help">ولایت کارمند را انتخاب نمایید</span>
            </div>
            <div class="col-lg-6">
                <label class="title-custom">درجه تحصیل : <span style="color:red;">*</span></label>
                <select class="form-control m-input input-lg m-input--air select-2" name="degree" id="degree" style="width:100%;">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($degree)
                        @foreach($degree as $item)
                            <option value="{!!$item->id!!}">{!!$item->name!!}</option>
                        @endforeach
                    @endif
                </select>
                <span class="m-form__help">درجه تحصیل را انتخاب نمایید</span>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
                <label class="title-custom">مکان تحصیل :</label>
                <input class="form-control m-input errorDiv" type="text" name="university" id="university">
                <span class="m-form__help">کشوری که کارمند در آن تحصیل نموده را بنویسید</span>
            </div>
             <div class="col-lg-6">
                <label class="title-custom">تخصص : </label>
                <input class="form-control m-input" type="text" name="request_text">
                <span class="m-form__help">تخصص کارمند در بخش های مربوطه</span>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
                <label class="title-custom">سرتیفیکیت :</label>
                <input class="form-control m-input errorDiv" type="text" name="certificate" id="certificate">
                <span class="m-form__help">سرتیفیکیت های معتبر مانند CCNA, CCNP, Oracle Administration ...</span>
            </div>
             <div class="col-lg-6">
                <label class="title-custom">وظیفه فعلی : </label>
                <input class="form-control m-input" type="text" name="request_text">
                <span class="m-form__help">وظیفه فعلی کارمند در اداره</span>
            </div>
        </div>    
       <div class="form-group m-form__group row m-form__group_custom">     
            
            <div class="col-lg-6">
                <label class="title-custom">تجربه کاری : </label>
                <select class="form-control m-input input-lg m-input--air select-2" name="request_text">
                    <option value="">انتخان گزینه</option>
                    <option value="1"> 1 </option>
                    <option value="2"> 2 </option>
                    <option value="3"> 3 </option>
                    <option value="4"> 4 </option>
                    <option value="5"> 5 </option>
                    <option value="6"> 6 </option>
                    <option value="7"> 7 </option>
                    <option value="8"> 8 </option>
                    <option value="9"> 9 </option>
                    <option value="10"> 10 </option>
                    <option value="11"> 11 </option>
                    <option value="12"> 12 </option>
                    <option value="13"> 13 </option>
                    <option value="14"> 14 </option>
                    <option value="15"> 15 </option>
                    <option value="16"> 16 </option>
                    <option value="17"> 17 </option>
                    <option value="18"> 18 </option>
                    <option value="19"> 19 </option>
                    <option value="20"> 20 </option>
                    <option value="21"> 21 </option>
                    <option value="22"> 22 </option>
                    <option value="23"> 23 </option>
                    <option value="24"> 24 </option>
                    <option value="25"> 25 </option>
                    <option value="26"> 26 </option>
                    <option value="27"> 27 </option>
                    <option value="28"> 28 </option>
                    <option value="29"> 29 </option>
                    <option value="30"> 30 </option>
                    <option value="31"> 31 </option>
                    <option value="32"> 32 </option>
                    <option value="33"> 33 </option>
                    <option value="34"> 34 </option>
                    <option value="35"> 35 </option>
                    <option value="36"> 36 </option>
                    <option value="37"> 37 </option>
                    <option value="38"> 38 </option>
                    <option value="39"> 39 </option>
                    <option value="40"> 40 </option>
                    <option value="41"> 41 </option>
                    <option value="42"> 42 </option>
                    <option value="43"> 43 </option>
                    <option value="44"> 44 </option>
                    <option value="45"> 45 </option>
                    <option value="46"> 46 </option>
                    <option value="47"> 47 </option>
                    <option value="48"> 48 </option>
                    <option value="49"> 49 </option>
                    <option value="50"> 50 </option>
                </select>
            </div>
            <div class="col-lg-6">
                <label class="title-custom">اداره :</label>
                <input class="form-control m-input errorDiv" type="text" name="orgnization" id="orgnization">
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <button type="button" onclick="storeRecord('{{route('hr.store')}}','requestForm','POST','response_div');" class="btn btn-primary">{{ trans('global.submit') }}</button>
                        <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
