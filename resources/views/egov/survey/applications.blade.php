<table class="table p-0" id="applications_{!!$number!!}">
    <tr>
        <td width="95%" colspan="2">
            <label class="title-custom">۱- نام اپلکشن  :</label>
            <select class="form-control m-input input-lg m-input--air select-2" name="" id="" style="width:100%;">
                <option value="">{{ trans('global.select') }}</option>
                @if($applications)
                    @foreach($applications as $item)
                        <option value="{!!$item->code!!}">{!!$item->name!!}</option>
                    @endforeach
                    <option value="other">دیگر موارید</option>
                @endif
            </select>
            <span class="m-form__help">در صورت نبودن نام اپلکش از گزینه دگر اپلکشن را اضافه نماید. </span>
        </td>
        <td width="5%">
        <label class="title-custom">&nbsp</label><br>
            <button type="button"id="applications_btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-sm" onclick="confirmRemoveMore('applications_{!!$number!!}',{!!$number!!},'applications_div_btn')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
        </td>
    </tr>
    <tr>
        <td width="50%">
            <label class="title-custom">۲ -اپلکشن توسط کدام اداره ایجاد و انکشاف داده شده ؟ </label>
            <br>
            <label class="m-checkbox">
                <input type="checkbox" name="" id=""><label class="title-custom">توسط اداره</label><span style="transform: rotate(90deg);"></span>
            </label>
            <br>
            <label class="m-checkbox pr-2">
                <input type="checkbox" name="" id=""><label class="title-custom">خریداری شده</label><span style="transform: rotate(90deg);"></span>
            </label><br>
        </td>
        <td width="50%" colspan="2">
            <label class="title-custom">۳- ایا اپلکشن فعال و مورد استفاده میباشد؟ </label>
            <br>
            <label class="m-checkbox">
                <input type="checkbox" name="" id=""><label class="title-custom">بلی</label><span style="transform: rotate(90deg);"></span>
            </label>
            <br>
            <label class="m-checkbox pr-2">
                <input type="checkbox" name="" id=""><label class="title-custom">نخیر</label><span style="transform: rotate(90deg);"></span>
            </label><br>
        </td>
    </tr> 
</table>
<script>
$(".select-2").select2({ width: '100%' });
</script>