<table class="table p-0" id="websites_{!!$number!!}">
    <tr>
        <td width="50%">
            <label class="title-custom">۱- آدرس ویب سایت :</label>
            <input class="form-control m-input" type="text" name="" id="" />
            <span class="m-form__help">آدرس ویب سایت ویبسایت اداره را بنویسید</span>
        </td>
        <td width="45%">
            <label class="title-custom">۲- ایجاد شده توسط :</label>
            <select class="form-control m-input input-lg m-input--air select-2" name="" id="" style="width:100%;">
                <option value="">{{ trans('global.select') }}</option>
                @if($websites)
                    @foreach($websites as $item)
                        <option value="{!!$item->code!!}">{!!$item->name!!}</option>
                    @endforeach
                @endif
            </select>
        </td>
        <td width="5%">
            <label class="title-custom">&nbsp</label><br>
            <button type="button"id="websites_btn_{!!$number!!}" class="btn btn-warning m-btn m-btn--air btn-sm" onclick="confirmRemoveMore('websites_{!!$number!!}',{!!$number!!},'websites_div_btn')"><i class="fa fa-minus" style="font-size:10px;"></i></button>
        </td>
    </tr>
</table>