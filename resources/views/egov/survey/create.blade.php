<div class="m-portlet__head table-responsive">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">ثبت سروی زیربناء تکنالوژی معلوماتی</h3>
        </div>
    </div>
</div>
<form  style="border:1px solid #ebedf2" class=" m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
    <div class="m-portlet__body">
        <div class="m-accordion m-accordion--default m-accordion--solid" id="m_accordion_3" role="tablist">

            <div class="m-accordion__item m-2 mb-3">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_1_head" data-toggle="collapse" href="#m_accordion_3_item_1_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۱. منبع خدمات انترنت اداره</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_1_body" role="tabpanel" aria-labelledby="m_accordion_3_item_1_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">۱- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> فایبر نوری </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۲- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> پاینت تو پاینت </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۳- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> وای مکس </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">۴- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> مایکروویف </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۵- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> وی ست </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">سایر منابع خدمات انترنت اداره  :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="3"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="3" ></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_2_head" data-toggle="collapse" href="#m_accordion_3_item_2_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۲. ظرفیت بندویت اداره</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_2_body" role="tabpanel" aria-labelledby="m_accordion_3_item_2_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">۱- ظرفیت بندویت :</label>
                                        <input class="form-control m-input" type="number" min='0' step=".1" name="" id="" />
                                        <span class="m-form__help">ظرفیت بندویت به MB</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">۲- تعداد استفاده کننده :</label>
                                        <input class="form-control m-input" type="number" min='0' name="" id="" />
                                        <span class="m-form__help">تعداد استفاده کننده گان از انترنت</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">سایر ظرفیت بندویت اداره :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_3_head" data-toggle="collapse" href="#m_accordion_3_item_3_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۳. سرور روم مرکزی اداره و وسایل آن</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_3_body" role="tabpanel" aria-labelledby="m_accordion_3_item_3_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">۱-  آیا سرور روم مرکزی در اداره وجود دارد؟ </label>
                                        <br>
                                        <label class="m-checkbox">
                                            <input type="checkbox"  name="server_yesno" id="yesno" /><label class="title-custom">بلی</label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox"  name="server_yesno" id="yesno" /><label class="title-custom">نخیر</label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        <br>
                                        <label class="title-custom">۲- آیا سرور روم مجهز و معیاری است؟ </label>
                                        <br>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="standard_yesno" id="yesno"><label class="title-custom">بلی</label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="standard_yesno" id="yesno"><label class="title-custom">نخیر</label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        <br>

                                        <label class="title-custom">۳-  آیا سیستم برق سرور روم معیاری است؟</label>
                                        <br>

                                        <label class="m-checkbox">
                                            <input type="checkbox" name="power_yesno" id="yesno"><label class="title-custom">بلی</label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="power_yesno" id="yesno"><label class="title-custom">نخیر</label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        <br>

                                        <label class="title-custom">۴-  آیا سیستم سرد کننده معیاری است؟</label>
                                        <br>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="cooling_yesno" id="yesno"><label class="title-custom">بلی</label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="cooling_yesno" id="yesno"><label class="title-custom">نخیر</label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        <br>

                                        <label class="title-custom">۵- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> Domain Controller </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۶- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> Domain Name  Services( DNS) </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۷- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> File Server </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۸- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> Windows Server Update Services(WSUS) </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۹- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> Windows Deployment services( WDS) </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        {{-- <br>
                                        <label class="title-custom">۱۰- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> Dynamic Host Configuration Protocol (  DHCP) </label><span style="transform: rotate(90deg);"></span>
                                        </label> --}}
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-5"><label class="title-custom">۱۰- نوعیت سرورهای موجود :</label></div>
                                            <div class="col-sm-5"><label class="title-custom">۱۱- تعداد سرورهای موجود :</label></div>
                                            <div class="col-sm-2">
                                                <button type="button" onclick="add_more('applications_div','{{route('more_applications')}}')" class="btn btn-dark m-btn m-btn--air btn-xs btn-sm " style="background: #1e183c;"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5"><input class="form-control m-input" type="text" name="" id="" /></div>
                                            <div class="col-sm-5"><input class="form-control m-input" type="number" min='0' name="" id="" /></div>
                                            <div class="col-sm-2"></div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-5"><label class="title-custom">۱۲- نوعیت روترهای موجود :</label></div>
                                            <div class="col-sm-5"><label class="title-custom">۱۵- تعداد روترهای موجود :</label></div>
                                            <div class="col-sm-2">
                                                <button type="button" onclick="add_more('applications_div','{{route('more_applications')}}')" class="btn btn-dark m-btn m-btn--air btn-xs btn-sm " style="background: #1e183c;"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5"><input class="form-control m-input" type="text" name="" id="" /></div>
                                            <div class="col-sm-5"><input class="form-control m-input" type="number" min='0' name="" id="" /></div>
                                            <div class="col-sm-2"></div>
                                        </div>

                                    </td>
                                    <td width="50%">

                                        <div class="row">
                                            <div class="col-sm-5"><label class="title-custom">۱۳- نوعیت فایروال های موجود :</label></div>
                                            <div class="col-sm-5"><label class="title-custom">۱۴- تعداد فایروال های موجود :</label></div>
                                            <div class="col-sm-2">
                                                <button type="button" onclick="add_more('applications_div','{{route('more_applications')}}')" class="btn btn-dark m-btn m-btn--air btn-xs btn-sm " style="background: #1e183c;"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5"><input class="form-control m-input" type="text" name="" id="" /></div>
                                            <div class="col-sm-5"><input class="form-control m-input" type="number" min='0' name="" id="" /></div>
                                            <div class="col-sm-2"></div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-5"><label class="title-custom">۱۶- نوعیت سویچ های موجود :</label></div>
                                            <div class="col-sm-5"><label class="title-custom">۱۷- تعداد سویچ های موجود :</label></div>
                                            <div class="col-sm-2">
                                                <button type="button" onclick="add_more('applications_div','{{route('more_applications')}}')" class="btn btn-dark m-btn m-btn--air btn-xs btn-sm " style="background: #1e183c;"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5"><input class="form-control m-input" type="text" name="" id="" /></div>
                                            <div class="col-sm-5"><input class="form-control m-input" type="number" min='0' name="" id="" /></div>
                                            <div class="col-sm-2"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5"><label class="title-custom">۱۸- نوعیت دستگاه های ذخیره سازی اطلاعات :</label></div>
                                            <div class="col-sm-5"><label class="title-custom">۱۹- تعداد دستگاه های ذخیره سازی اطلاعات :</label></div>
                                            <div class="col-sm-2">
                                                <button type="button" onclick="add_more('applications_div','{{route('more_applications')}}')" class="btn btn-dark m-btn m-btn--air btn-xs btn-sm " style="background: #1e183c;"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5"><input class="form-control m-input" type="text" name="" id="" /></div>
                                            <div class="col-sm-5"><input class="form-control m-input" type="number" min='0' name="" id="" /></div>
                                            <div class="col-sm-2"></div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-10"><label class="title-custom">۲۰- نوعیت سیستم مجازی سازی سرورها :</label></div>
                                            <div class="col-sm-2">
                                                <button type="button" onclick="add_more('applications_div','{{route('more_applications')}}')" class="btn btn-dark m-btn m-btn--air btn-xs btn-sm " style="background: #1e183c;"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-10"><input class="form-control m-input" type="text" name="" id="" /></div>
                                            <div class="col-sm-2"></div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-10"><label class="title-custom">۲۱- تعداد سیستم عامل لاینکس :</label></div>
                                            <div class="col-sm-2">
                                                <button type="button" onclick="add_more('applications_div','{{route('more_applications')}}')" class="btn btn-dark m-btn m-btn--air btn-xs btn-sm " style="background: #1e183c;"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-10"><input class="form-control m-input" type="text" name="" id="" /></div>
                                            <div class="col-sm-2"></div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-10"><label class="title-custom">۲۲- تعداد سیستم عامل ویندوز سرور :</label></div>
                                            <div class="col-sm-2">
                                                <button type="button" onclick="add_more('applications_div','{{route('more_applications')}}')" class="btn btn-dark m-btn m-btn--air btn-xs btn-sm " style="background: #1e183c;"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-10"><input class="form-control m-input" type="text" name="" id="" /></div>
                                            <div class="col-sm-2"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">سایر سرور روم مرکزی اداره و وسایل آن :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="3"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="3" ></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_4_head" data-toggle="collapse" href="#m_accordion_3_item_4_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۴. اتصال تعمیرات اداره با سرور روم مرکزی</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_4_body" role="tabpanel" aria-labelledby="m_accordion_3_item_4_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">۱- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> فایبر نوری </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۲- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> وایرلس </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">۳- </label>
                                        {{-- <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> VPN </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۴- </label>--}}
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> کیبل CAT 6 </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">سایر اتصال تعمیرات اداره با سرور روم مرکزی :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_5_head" data-toggle="collapse" href="#m_accordion_3_item_5_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۵. شبکه بندی داخل تعمیرات اداره</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_5_body" role="tabpanel" aria-labelledby="m_accordion_3_item_5_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">۱- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> Wired Network </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۲- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> Wireless Network </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">۳- وسایل موجود در شبکه تکنالوژی معلوماتی اداره از کمپنی های :</label>
                                        <select class="form-control m-input input-lg m-input--air select-2" name="" id="" style="width:100%;" multiple>
                                            <option value="">{{ trans('global.select') }}</option>
                                            @if($equipments)
                                                @foreach($equipments as $item)
                                                    <option value="{!!$item->code!!}">{!!$item->name!!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="m-form__help">لطفا کمپنی وسایل موجود را انتخاب نمایید</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">سایر شبکه بندی داخل تعمیرات اداره :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_6_head" data-toggle="collapse" href="#m_accordion_3_item_6_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۶. امنیت شبکه و سیستم های تکنالوژی معلوماتی اداره</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_6_body" role="tabpanel" aria-labelledby="m_accordion_3_item_6_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">۱- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> فایروال </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۲- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> SSL </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">۳- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> Antivirus </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۴- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> Security policy management system </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">سایر امنیت شبکه و سیستم های تکنالوژی معلوماتی اداره :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_7_head" data-toggle="collapse" href="#m_accordion_3_item_7_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۷. سیستم مکالماتی صوتی و تصویری اداره</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_7_body" role="tabpanel" aria-labelledby="m_accordion_3_item_7_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">۱- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> VoIP </label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="title-custom">۲- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> Digital </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">۳- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> PBX </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">سایر سیستم های مکالماتی صوتی و تصویری اداره :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_8_head" data-toggle="collapse" href="#m_accordion_3_item_8_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۸. سافت ویرها جهت انجام وظایف روزمره اداره</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_8_body" role="tabpanel" aria-labelledby="m_accordion_3_item_8_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">۱- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> پروگرام های مایکروسافت آفس </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        <br>
                                        <label class="title-custom">۲- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> پروگرام های Adobe </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">۳- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> پروگرام های لاینکس </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        <br>
                                        <label class="title-custom">۳- </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="" id=""><label class="title-custom"> پروگرام های مک </label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">سایر سافت ویرها جهت انجام وظایف روزمره اداره :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_9_head" data-toggle="collapse" href="#m_accordion_3_item_9_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۹. سیستم ایمیل های رسمی</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_9_body" role="tabpanel" aria-labelledby="m_accordion_3_item_9_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">۱-  آیا اداره شما سیستم ایمیل آدرس های رسمی دارد؟ </label>
                                        <br>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="email_yesno" id="yesno"><label class="title-custom">بلی</label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        <label class="m-checkbox">
                                            <input type="checkbox" name="email_yesno" id="yesno"><label class="title-custom">نخیر</label><span style="transform: rotate(90deg);"></span>
                                        </label>
                                        <br>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">۲- نوعیت سیستم ایمیل آدرس های رسمی اداره :</label>
                                        <select class="form-control m-input input-lg m-input--air select-2" name="" id="" style="width:100%;">
                                            <option value="">{{ trans('global.select') }}</option>
                                            @if($emails)
                                                @foreach($emails as $item)
                                                    <option value="{!!$item->code!!}">{!!$item->name!!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">سایر سیستم ایمیل های رسمی :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_10_head" data-toggle="collapse" href="#m_accordion_3_item_10_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۱۰ تعداد وسایل تکنالوژی معلوماتی، که جهت انجام وظایف روزمره استفاده میشود</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_10_body" role="tabpanel" aria-labelledby="m_accordion_3_item_10_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">۱- کمپیوتر دسکتاپ  :</label>
                                        <input class="form-control m-input" type="number" min='0' step=".1" name="" id="" />
                                        <span class="m-form__help">تعداد کمپیوتر دسکتاپ</span>
                                        <br>
                                        <label class="title-custom">۲- کمپیوتر لپتاپ :</label>
                                        <input class="form-control m-input" type="number" min='0' name="" id="" />
                                        <span class="m-form__help">تعداد کمپیوتر لپتاپ</span>
                                        <br>
                                        <label class="title-custom">۳- پرنتر ساده  :</label>
                                        <input class="form-control m-input" type="number" min='0' step=".1" name="" id="" />
                                        <span class="m-form__help">تعداد پرنتر ساده</span>
                                        <br>
                                        <label class="title-custom">۴- پرنتر رنگه :</label>
                                        <input class="form-control m-input" type="number" min='0' name="" id="" />
                                        <span class="m-form__help">تعداد پرنتر رنگه</span>
                                        <br>
                                        <label class="title-custom">۵- ماشین کاپی ساده  :</label>
                                        <input class="form-control m-input" type="number" min='0' step=".1" name="" id="" />
                                        <span class="m-form__help">تعداد ماشین کاپی ساده</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">۶- ماشین کاپی رنگه :</label>
                                        <input class="form-control m-input" type="number" min='0' name="" id="" />
                                        <span class="m-form__help">تعداد ماشین کاپی رنگه</span>
                                        <br>
                                        <label class="title-custom">۷- سکنر  :</label>
                                        <input class="form-control m-input" type="number" min='0' step=".1" name="" id="" />
                                        <span class="m-form__help">تعداد سکنر</span>
                                        <br>
                                        <label class="title-custom">۸- پروجکتور :</label>
                                        <input class="form-control m-input" type="number" min='0' name="" id="" />
                                        <span class="m-form__help">تعداد پروجکتور</span>
                                        <br>
                                        <label class="title-custom">۹- تیلفون های سرمیزی  :</label>
                                        <input class="form-control m-input" type="number" min='0' step=".1" name="" id="" />
                                        <span class="m-form__help">تعداد تیلفون های سرمیزی</span>
                                        <br>
                                        <label class="title-custom">۱۰- تلویزیون های هوشمند :</label>
                                        <input class="form-control m-input" type="number" min='0' name="" id="" />
                                        <span class="m-form__help">تعداد تلویزیون های هوشمند</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">معلومات اضافه مرتبط :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_11_head" data-toggle="collapse" href="#m_accordion_3_item_11_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۱۱. آپلیکیشن های موجود در اداره</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_11_body" role="tabpanel" aria-labelledby="m_accordion_3_item_11_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="100%" colspan="2">
                                        <table class="table p-0">
                                            <tr>
                                                <td width="95%" colspan="2">
                                                    <label class="title-custom">۱- نام اپلکشن  :</label>
                                                    <select class="form-control m-input input-lg m-input--air select-2" name="" id="" style="width:100%;">
                                                        <option value="">{{ trans('global.select') }}</option>
                                                        @if($applications)
                                                            @foreach($applications as $item)
                                                                <option value="{!!$item->code!!}">{!!$item->name!!}</option>
                                                            @endforeach
                                                            <option value="other">دیگر موارید</option>
                                                        @endif
                                                    </select>
                                                    <span class="m-form__help">در صورت نبودن نام اپلکش از گزینه دگر اپلکشن را اضافه نماید. </span>
                                                </td>
                                                <td width="5%">
                                                    <label class="title-custom">&nbsp</label><br>
                                                    <button type="button" onclick="add_more('applications_div','{{route('more_applications')}}')" class="btn btn-dark m-btn m-btn--air btn-xs btn-sm " style="background: #1e183c;"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="50%">
                                                    <label class="title-custom">۲ -اپلکشن توسط کدام اداره ایجاد و انکشاف داده شده ؟ </label>
                                                    <br>
                                                    <label class="m-checkbox">
                                                        <input type="checkbox" name="byoffice[]" id="yesno"><label class="title-custom">توسط اداره</label><span style="transform: rotate(90deg);"></span>
                                                    </label>
                                                    <br>
                                                    <label class="m-checkbox pr-2">
                                                        <input type="checkbox" name="byoffice[]" id="yesno"><label class="title-custom">خریداری شده</label><span style="transform: rotate(90deg);"></span>
                                                    </label><br>
                                                </td>
                                                <td width="50%" colspan="2">
                                                    <label class="title-custom">۳- ایا اپلکشن فعال و مورد استفاده میباشد؟ </label>
                                                    <br>
                                                    <label class="m-checkbox">
                                                        <input type="checkbox" name="active_yesno[]" id="yesno"><label class="title-custom">بلی</label><span style="transform: rotate(90deg);"></span>
                                                    </label>
                                                    <br>
                                                    <label class="m-checkbox pr-2">
                                                        <input type="checkbox" name="active_yesno[]" id="yesno"><label class="title-custom">نخیر</label><span style="transform: rotate(90deg);"></span>
                                                    </label><br>
                                                </td>
                                            </tr>
                                        </table>
                                        <span id="applications_div"></span><!-- Display more Hazard Analysis  -->
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">سایر آپلیکیشن های موجود در اداره :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_12_head" data-toggle="collapse" href="#m_accordion_3_item_12_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۱۲. ویب سایت های موجود در اداره</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_12_body" role="tabpanel" aria-labelledby="m_accordion_3_item_12_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="100%" colspan="2">
                                        <table class="table p-0">
                                            <tr>
                                                <td width="50%">
                                                    <label class="title-custom">۱- آدرس ویب سایت :</label>
                                                    <input class="form-control m-input" type="text" name="" id="" />
                                                    <span class="m-form__help">آدرس ویب سایت ویبسایت اداره را بنویسید</span>
                                                </td>
                                                <td width="45%">
                                                    <label class="title-custom">۲- ایجاد شده توسط :</label>
                                                    <select class="form-control m-input input-lg m-input--air select-2" name="" id="" style="width:100%;">
                                                        <option value="">{{ trans('global.select') }}</option>
                                                        @if($websites)
                                                            @foreach($websites as $item)
                                                                <option value="{!!$item->code!!}">{!!$item->name!!}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </td>
                                                <td width="5%">
                                                    <label class="title-custom">&nbsp</label><br>
                                                    <button type="button" onclick="add_more('websites_div','{{route('more_websites')}}')" class="btn btn-dark m-btn m-btn--air btn-xs btn-sm " style="background: #1e183c;"><i class="fa fa-plus" style="font-size:10px;"></i></button>
                                                </td>
                                            </tr>
                                        </table>
                                        <span id="websites_div"></span><!-- Display more Hazard Analysis  -->
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">سایر ویب سایت های موجود در اداره :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_13_head" data-toggle="collapse" href="#m_accordion_3_item_13_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۱۳. مؤثریت تکنالوژی معلوماتی در انجام وظایف روزمره اداره</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_13_body" role="tabpanel" aria-labelledby="m_accordion_3_item_13_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="100%" colspan="2">
                                        <label class="title-custom">۱- مؤثریت تکنالوژی معلوماتی در انجام وظایف روزمره اداره :</label>
                                        <select class="form-control m-input input-lg m-input--air select-2" name="" id="" style="width:100%;">
                                            <option value="">{{ trans('global.select') }}</option>
                                            @if($efficiency)
                                                @foreach($efficiency as $item)
                                                    <option value="{!!$item->code!!}">{!!$item->name!!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">سایر مؤثریت های تکنالوژی معلوماتی در انجام وظایف روزمره اداره :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_14_head" data-toggle="collapse" href="#m_accordion_3_item_14_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۱۴. Disaster Recovery Site</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_14_body" role="tabpanel" aria-labelledby="m_accordion_3_item_14_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">۱- آیا اداره شما Disaster Recovery Site دارد ؟ </label>
                                        <select class="form-control m-input input-lg m-input--air select-2" name="" id="" style="width:100%;">
                                            <option value="">{{ trans('global.select') }}</option>
                                            @if($yesNo)
                                                @foreach($yesNo as $item)
                                                    <option value="{!!$item->code!!}">{!!$item->name!!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </td>
                                    <td width="50%" >
                                        <label class="title-custom">نوعیت Disaster Recovery Site</label>
                                        <br>
                                        <label class="m-checkbox pr-2">
                                            <input type="checkbox" name="site" id="yesno"><label class="title-custom">OnSite</label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                        <label class="m-checkbox pr-2">
                                            <input type="checkbox" name="site" id="yesno"><label class="title-custom">OffSite</label><span style="transform: rotate(90deg);"></span>
                                        </label><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">Other Disaster Recovery Site :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-accordion__item mx-2">
                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_15_head" data-toggle="collapse" href="#m_accordion_3_item_15_body" aria-expanded="false">
                    <span class="m-accordion__item-icon"><i style="font-size:13pt;" class="fa flaticon-list-2"></i></span>
                    <span class="m-accordion__item-title">۱۵. Disaster Recovery Plan</span>
                    <span class="m-accordion__item-mode"></span>
                </div>
                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_15_body" role="tabpanel" aria-labelledby="m_accordion_3_item_15_head" data-parent="#m_accordion_3" style="">
                    <div class="m-accordion__item-content">
                        <div class="form-group m-form__group row m-form__group_custom">
                            <table class="table table-bordered p-0">
                                <tr>
                                    <td width="100%" colspan="2">
                                        <label class="title-custom">۱- آیا اداره شما Disaster Recovery Plan دارد ؟ </label>
                                        <select class="form-control m-input input-lg m-input--air select-2" name="" id="" style="width:100%;">
                                            <option value="">{{ trans('global.select') }}</option>
                                            @if($yesNo)
                                                @foreach($yesNo as $item)
                                                    <option value="{!!$item->code!!}">{!!$item->name!!}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%">
                                        <label class="title-custom">Other Disaster Recovery Plan :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن معلومات مرتبط به این بخش اینجا بنویسید.</span>
                                    </td>
                                    <td width="50%">
                                        <label class="title-custom">چالش ها در این بخش :</label>
                                        <textarea class="form-control tinymce m-input m-input--air" name="" id="" rows="2"></textarea>
                                        <span class="m-form__help">در صورت داشتن چالش ها در این بخش اینجا بنویسید.</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <button type="button" onclick="storeRecord('{{route('survey.store')}}','requestForm','POST','response_div');" class="btn btn-primary">{{ trans('global.submit') }}</button>
                        <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">



$(function() {
    $('input[id="yesno"]').on('click', function() {
      // in the handler, 'this' refers to the box clicked on
      var $box = $(this);
      if ($box.is(":checked")) {
        // the name of the box is retrieved using the .attr() method
        // as it is assumed and expected to be immutable
        var group = "input:checkbox[name='" + $box.attr("name") + "']";
        // the checked state of the group/box on the other hand will change
        // and the current value is retrieved using .prop() method
        $(group).prop("checked", false);
        $box.prop("checked", true);
      } else {
        $box.prop("checked", false);
      }
    });
  });
</script>
