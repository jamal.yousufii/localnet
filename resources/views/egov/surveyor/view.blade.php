@extends('master')
@section('head')
  <title>معرفی شخص ارایه کننده معلومات سروی</title>
@endsection
@section('content')
    <div class="m-portlet m-portlet--mobile">
      <div class="m-wizard m-wizard--5 m-wizard--success m-wizard--step-first" id="m_wizard">
            <div class="m-wizard__head m-portlet__padding-x">
                <div class="row">
                    <div class="col-xl-12 m-0">
                        <div class="m-wizard__nav">
                            <div class="m-wizard__steps">
                                <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                                    <div class="m-wizard__step-info">
                                        <a href="{{route('surveyor.index')}}" class="m-wizard__step-number">
                                            <span class="m-wizard__step-seq m-wizard__step-label">1.</span>
                                            <span class="m-wizard__step-label">
                                                معرفی شخص ارایه کننده معلومات سروی
                                            </span>
                                            <span class="m-wizard__step-icon"><i class="la la-check"></i></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
                                    <div class="m-wizard__step-info">
                                        <a href="{{route('hr.index')}}"  class="m-wizard__step-number">
                                            <span class="m-wizard__step-seq m-wizard__step-label">2.</span>
                                            <span class="m-wizard__step-label">
                                                کارمندان اداره در سکتور تکنالوژی معلوماتی 
                                            </span>
                                            <span class="m-wizard__step-icon"><i class="la la-check"></i></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
                                    <div class="m-wizard__step-info">
                                        <a href="{{route('survey.index')}}" class="m-wizard__step-number">
                                            <span class="m-wizard__step-seq m-wizard__step-label">3.</span>
                                            <span class="m-wizard__step-label">
                                                سروی زیربناء تکنالوژی معلوماتی
                                            </span>
                                            <span class="m-wizard__step-icon"><i class="la la-check"></i></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="response_content"> 
                <div class="m-portlet__head table-responsive">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">معرفی شخص ارایه کننده معلومات سروی</h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="javascript:void()"  onclick="addRecord('{{route('surveyor.create')}}','','GET','response_content')" class="btn m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air custome-button">
                                    <span><i class="la la-cart-plus"></i><span>علاوه نمودن</span></span>
                                </a>
                            </li>
                            <li class="m-portlet__nav-item">
                                <a href="{{ route('home') }}" class="btn btn-secondary m-btn--custom m-btn--icon btn-sm">
                                <span><i class="fa fa-reply-all"></i> <span>{{ trans('global.back') }}</span></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-wizard__form">
                    <div class="m-portlet__body">
                        <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="row">
                                        <div class="col-xl-12">
                                             @alert()
                                             @endalert
                                            <div class="m-form__section m-form__section--first">
                                                <div class="m-portlet__body table-responsive" id="searchresult"> 
                                                    <table class="table table-striped- table-bordered table-hover table-checkable">
                                                        <thead class="thead-light">
                                                            <tr class="font-title">
                                                                <th width="10%">{{ trans('global.number') }}</th>
                                                                <th width="10%">اسم</th>
                                                                <th width="10%">وظیفه</th>
                                                                <th width="10%">تحصیل و تخصص </th>
                                                                <th width="10%">نام اداره</th>
                                                                <th width="10%">نام ثبت کننده</th>
                                                                <th width="10%">{{ trans('global.action') }}</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="width: auto;overflow-x: auto;white-space: nowrap;">
                                                           @if ($surveyors)
                                                               @foreach($surveyors as $item)
                                                                  <tr>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>{{$item->name}}</td>
                                                                        <td>{{getColumn('mysql','statics',array('Id' => $item->job),'name_en')}}</td>
                                                                        <td>{{$item->education}}</td>
                                                                        <td>{{getColumn('mysql','departments',array('id' => $item->organization_id),'name_dr')}}</td>
                                                                        <td>{{$item->created_by}}</td>
                                                                        <td></td>
                                                                  </tr>
                                                               @endforeach
                                                           @endif
                                                        </tbody>
                                                    </table>
                                                    <!-- Pagination -->
                                                    @if(!empty($records))
                                                        {!!$records->links('pagination')!!}
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection






