<div class="m-portlet__head table-responsive">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">ثبت معرفی شخص ارایه کننده معلومات سروی</h3>
        </div>
    </div>
</div>
<form  style="border:1px solid #ebedf2" id="surveyor_form" class=" m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
    @csrf
    <div class="m-portlet__body">
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
                <label class="title-custom">اسم : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv required" type="text" name="name" id="name">
                <div class="name error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-6">
                <label class="title-custom">وظیفه : <span style="color:red;">*</span></label>
                <select class="form-control m-input errorDiv required"name="job" id="job">
                    <option value="">یگ گزینه را انتخاب نماید</option>
                    @if(count(getStaticData('job'))>0)
                        @foreach (getStaticData('job') as $item)
                            <option value="{{$item->code}}">{{$item->name_en}}</option>
                        @endforeach
                    @endif
                </select>   
                <div class="job error-div" style="display:none;"></div>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-6">
                <label class="title-custom">تحصیل و تخصص : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv required" type="text" name="education" id="education">
                <div class="education error-div" style="display:none;"></div>
            </div>
            <div class="col-lg-6">
                <label class="title-custom">نام اداره :</label>
                <select class="form-control m-input errorDiv required" type="text" name="organization_id" id="organization_id">
                    @foreach (getData('mysql','departments') as $item)
                        <option value="{{$item->id}}">{{$item->name_dr}}</option>
                    @endforeach
                </select>
                <div class="organization_id error-div" style="display:none;"></div>
                <span class="m-form__help">نام اداره شما</span>
            </div>
        </div>
        <div class="form-group m-form__group row m-form__group_custom">
            <div class="col-lg-12">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <button type="button" onclick="storeRecord('{{route('surveyor.store')}}','surveyor_form','POST','response_content')" class="btn btn-primary">{{ trans('global.submit') }}</button>
                        <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.cancel') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
