@extends('layouts.master')

@section('head')
    <title>{!!_('register_new_employee')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAuditReports')!!}">{!!_('Reports')!!}</a></li>
    <li class="active"><span>{!!_('Add_new_report')!!}</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postAuditReport')!!}" enctype="multipart/form-data">
			    <div class="panel-heading">
			      <h5 class="panel-title">{!!_('add_new_report_form')!!}</h5>
			    </div>
                <div class="container-fluid">
                	<div class="row">
                		
                		<div class="col-sm-6">
			            	<div class="col-sm-12">
			            		<label class="col-sm-2 ">{!!_('report_type')!!}</label>
                                <select name="report_type" class="form-control">
                                    <option value=''>{!!_('select_an_item')!!}</option>
                                    <option value='1' <?php echo (Input::old('report_type')=='1' ? 'selected':''); ?>>{!!_('planed')!!}</option>
                                    <option value='2' <?php echo (Input::old('report_type')=='2' ? 'selected':''); ?>>{!!_('unplaned')!!}</option>
                                    
                                </select>
			               	</div>
			               	
			            </div>
                	
			            <div class="col-sm-6">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">{!!_('year')!!}</label>
			                  	<?php 
                                     $current_year = date('Y')-621;
                                     
                                ?>
                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
                                <select name = "year" class="form-control">
                                    <option value=''>{!!_('select_an_item')!!}</option>
                                    <?php 
                                       
                                        for($i=$current_year-10;$i<=$current_year;$i++)
                                        {
                                            
                                            echo "<option>".$i."</option>";
                                        }
                                    ?>
                                </select>
			               	</div>
			            </div>
                	</div>
                </div>
	            </br>
				<div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">{!!_('report_attachment')!!}</label>
	                    		
	                            <input type='file'  name='attach' class="form-control">
	                    	</div>
	                   </div>
	                   <div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
		                        @if(canAdd('audit_reports'))
		                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
		                    </div>
                    	</div>
			      	</div>
			    </div>
			 	<div class="container_fluid">
			 		<div class="row">
			 			<div class="col-sm-12">
			 			@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
			 			</div>
			 		</div>
			 	</div>
                
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">
    
</script>

@stop

