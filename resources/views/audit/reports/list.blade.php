@extends('layouts.master')

@section('head')
	{!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
    <title>{!!_('auditAndInvestigation')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>{!!_('Reports')!!}</span></li>
        </ol>
    </div>
</div>
@if(canAdd('audit_reports'))
<header class="main-box-header clearfix">
    <h2>
    	<a href="{!!URL::route('add_new_report')!!}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle fa-lg"></i>{!!_('newReport')!!}</a>
    </h2>
</header>
@endif
<!-- Example Tabs -->
<div class="example-wrap">
	<div class="nav-tabs-horizontal">
	  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
	    <li @if($active=='all'){!!'class="active"'!!}@endif role="presentation"><a data-toggle="tab" href="#all" aria-controls="exampleTabsOne"
	      role="tab">{!!_('Planed')!!}</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_unplaned()" href="#unplaned" aria-controls="exampleTabsTwo"
	      role="tab">{!!_('UnPlaned')!!}</a></li>
	    
	    <li @if($active=='search'){!!'class="active"'!!}@endif role="presentation"><a data-toggle="tab" href="#search" aria-controls="exampleTabsFour"
	      role="tab">{!!_('advancedSearch')!!}</a></li>
	  </ul>
	  <div class="tab-content padding-top-20">
	    <div class="tab-pane @if($active=='all'){!!'active'!!}@endif" id="all" role="tabpanel">
	    	<div class="row">
			    <div class="col-lg-12">
			        <div class="main-box">
			            <div class="main-box-body clearfix">
			            <div class="table-responsive">
			                <table class="table table-responsive" id='list'>
			                    <thead>
			                    <tr>
			                        <th>#</th>
			                        <th>{!!_('file_name')!!}</th>
			                        <th>{!!_('report_type')!!}</th>
			                        <th>{!!_('year')!!}</th>
			                        <th>{!!_('created_at')!!}</th>
			                  
			                        <th>{!!_('actions')!!}</th>
			                    </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>
			            </div>
			        
			            </div>
			        </div>
			    </div>
			</div>
	    </div>
	    <div class="tab-pane" id="unplaned" role="tabpanel">
	 
	    </div>
	    
	    <div class="tab-pane @if($active=='search'){!!'active'!!}@endif" id="search" role="tabpanel">
	    
	    	<form class="form-horizontal" role="form" method="post"> 
		    	<div class="row">
		        	<div class="col-lg-12">
		        		<div class="container-fluid">
					      	<div class="row">
					      		
							    <div class="col-sm-2">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">{!!_('report_type')!!}</label>
		                                <select id="report_type" class="form-control">
		                                    <option value='0'>{!!_('all')!!}</option>
		                                    <option value='1' <?php echo (Input::old('report_type')=='1' ? 'selected':''); ?>>{!!_('planed')!!}</option>
		                                    <option value='2' <?php echo (Input::old('report_type')=='2' ? 'selected':''); ?>>{!!_('unplaned')!!}</option>
		                                    
		                                </select>
		                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		                			</div>
		                		</div>
		                		<div class="col-sm-2">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">{!!_('year')!!}</label>
					                  	<?php 
		                                     $current_year = date('Y')-621;
		                                     
		                                ?>
		                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
		                                <select id = "year" class="form-control">
		                                    <option value='0'>{!!_('all')!!}</option>
		                                    <?php 
		                                       
		                                        for($i=$current_year-10;$i<=$current_year;$i++)
		                                        {
		                                            
		                                            echo "<option>".$i."</option>";
		                                        }
		                                    ?>
		                                </select>
		                			</div>
		                		</div>
		                		<div class="col-sm-6">
					            	<div class="col-sm-12">
					            		<label class="col-sm-2 ">{!!_('file_name')!!}</label>
		                                <input type="text" id="attach" class="form-control"/>
					               	</div>
					               	
					            </div>
					            <div class="col-sm-2 pull-right">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">&nbsp;</label>
		                                <button class="btn btn-primary pull-right" type="button" onclick="do_search()">{!!_('search')!!}</button>
		                			</div>
		                			
		                		</div>
							</div>
						</div>
						
					</div>
				</div>
		    </form>
		    <hr>
		    <div id="search_result"></div>
	    </div>
	  </div>
	</div>
</div>
<!-- End Example Tabs -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {
				"order": [[ 1, 'asc' ]],
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/audit/getAuditReportsData',array(1))!!}"
            }
        );

    });
function removeReport(doc_id)
{
    var confirmed = confirm("Do you want to remove this record?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeReport")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                
                success: function(response)
                {
                    location.href="{!!URL::route('getAuditReports')!!}";
                }
            }
        );
    }

}  
function load_unplaned()
{
    var page = "{!!URL::route('loadUnplanedReports')!!}";
    
    $.ajax({
        url: page,
        type: 'post',
        //data: '&type='+div,
        //dataType:'HTML',
        success: function(r)
      	{
        	$('#unplaned').html(r);
        }
    });   
}
function do_search()
{
    var page = "{!!URL::route('search_report')!!}";
    var type = $('#report_type').val();
    var year = $('#year').val();
    var attach = $('#attach').val();
    $.ajax({
        url: page,
        type: 'post',
        data: '&type='+type+'&year='+year+'&attach='+attach,
        //dataType:'HTML',
        success: function(r)
      	{
        	$('#search_result').html(r);
        }
    });   
}
</script>
@stop

