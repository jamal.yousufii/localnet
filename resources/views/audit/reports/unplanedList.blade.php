<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='list1'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{!!_('file_name')!!}</th>
                        <th>{!!_('report_type')!!}</th>
                        <th>{!!_('year')!!}</th>
                        <th>{!!_('created_at')!!}</th>
                  
                        <th>{!!_('actions')!!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#list1').dataTable(
            {
				"order": [[ 1, 'asc' ]],
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/audit/getAuditReportsData',array(2))!!}"
            }
        );

    });
</script>