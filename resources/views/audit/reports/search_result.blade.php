<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='list1'>
                    <thead>
                    <tr>
                        
                        <th>{!!_('file_name')!!}</th>
                        <th>{!!_('report_type')!!}</th>
                        <th>{!!_('year')!!}</th>
                        <th>{!!_('created_at')!!}</th>
                  
                        <th colspan="3">{!!_('actions')!!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($records)
                    	@foreach($records as $rec)
                    		<tr>
                    		<td>{!!$rec->file_name!!}</td>
                    		<td>{!!$rec->type_name!!}</td>
                    		<td>{!!$rec->report_year!!}</td>
                    		<td>{!!$rec->created_at!!}</td>
                    		<td>
	                    		<a href="{!!route('editAuditReport',$rec->id)!!}" class="table-link" style="text-decoration:none;" title="Edit">
									<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
                    		</td>
                    		<td>
	                    		<a href="{!!route('getReportDownload',$rec->file_id)!!}" class="table-link" style="text-decoration:none;" title="Download">
									<i class="icon fa-download" aria-hidden="true" style="font-size: 16px;"></i>
								</a>
                    		</td>
                    		<td>
	                    		@if(canDelete('audit_reports'))
									<a href="javascript:void()" onclick="removeReport({!!$rec->id!!})" class="table-link" style="text-decoration:none;" title="Edit">
										<i class="icon fa-remove" aria-hidden="true" style="font-size: 16px;"></i>
									</a>
								@endif
                    		</td>
                    		
                    		</tr>
                    	@endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function removeReport(doc_id)
{
    var confirmed = confirm("Do you want to remove this record?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeReport")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                
                success: function(response)
                {
                    location.href="{!!URL::route('getAuditReports')!!}";
                }
            }
        );
    }
}  
</script>