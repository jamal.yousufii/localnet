@extends('layouts.master')

@section('head')
    <title>{!!_('attach_new_file')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAuditReports')!!}">{!!_('attachments')!!}</a></li>
    <li class="active"><span>{!!_('attach_new_file')!!}</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postAttachment')!!}" enctype="multipart/form-data">
			    <div class="panel-heading">
			      <h5 class="panel-title">{!!_('attach_new_file')!!}</h5>
			    </div>
                <div class="container-fluid">
                	<div class="row">
                		
                		<div class="col-sm-6">
			            	<div class="col-sm-12">
			            		<label class="col-sm-2 ">{!!_('category')!!}</label>
                                <select name="category" reguired="required" class="form-control">
                                    <option value=''>{!!_('select_an_item')!!}</option>
                                    @if($categories)
                                    	@foreach($categories AS $cat)
                                    	<option value="{!!$cat->id!!}">{!!$cat->name!!}</option>
                                    	@endforeach
                                    @endif
                                </select>
			               	</div>
			               	
			            </div>
                	
			            <div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">{!!_('attachment')!!}</label>
	                    		
	                            <input type='file' reguired="required" name='attach' class="form-control">
	                    	</div>
	                   </div>
                	</div>
                </div>
	            </br>
				<div class="container-fluid" >
			      	<div class="row">
	                   <div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
		                        @if(canAdd('audit_reports'))
		                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
		                    </div>
                    	</div>
			      	</div>
			    </div>
			 	<div class="container_fluid">
			 		<div class="row">
			 			<div class="col-sm-12">
			 			@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
			 			</div>
			 		</div>
			 	</div>
                
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

@stop

