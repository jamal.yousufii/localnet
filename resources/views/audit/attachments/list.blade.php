@extends('layouts.master')

@section('head')
	{!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
    <title>{!!_('other_attachments')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>{!!_('other_attachments')!!}</span></li>
        </ol>
    </div>
</div>
@if(canAdd('audit_reports'))
<header class="main-box-header clearfix">
    <h2>
    	<a href="{!!URL::route('attach_new_file')!!}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle fa-lg"></i>{!!_('attache_new_file')!!}</a>
    </h2>
</header>
@endif
<!-- Example Tabs -->
<div class="example-wrap">
	<div class="nav-tabs-horizontal">
	  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
	    <li @if($active=='all'){!!'class="active"'!!}@endif role="presentation"><a data-toggle="tab" href="#all" aria-controls="exampleTabsOne"
	      role="tab">{!!_('attachments')!!}</a></li>
	    <li @if($active=='search'){!!'class="active"'!!}@endif role="presentation"><a data-toggle="tab" href="#search" aria-controls="exampleTabsFour"
	      role="tab">{!!_('advancedSearch')!!}</a></li>
	  </ul>
	  <div class="tab-content padding-top-20">
	    <div class="tab-pane @if($active=='all'){!!'active'!!}@endif" id="all" role="tabpanel">
	    	<div class="row">
			    <div class="col-lg-12">
			        <div class="main-box">
			            <div class="main-box-body clearfix">
			            <div class="table-responsive">
			                <table class="table table-responsive" id='list'>
			                    <thead>
			                    <tr>
			                        <th>#</th>
			                        <th>{!!_('file_name')!!}</th>
			                        <th>{!!_('category')!!}</th>
			                        <th>{!!_('created_at')!!}</th>
			                  
			                        <th>{!!_('actions')!!}</th>
			                    </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>
			            </div>
			        
			            </div>
			        </div>
			    </div>
			</div>
	    </div>
	    
	    <div class="tab-pane @if($active=='search'){!!'active'!!}@endif" id="search" role="tabpanel">
	    <?php
	    if(isset($dep_id)){$dep = $dep_id;}else{$dep=0;};
	    if(isset($sub_dep_id)){$sub = $sub_dep_id;}else{$sub=0;};
	    if(isset($bast)){$bast_id = $bast;}else{$bast_id=0;};
	    if(isset($employee_type)){$emp_type = $employee_type;}else{$emp_type=0;};
	    if(isset($docs)){$doc = $docs;}else{$doc=0;};
	    if(isset($typee)){$type = $typee;}else{$type=0;};
	    if(isset($gender)){$gen = $gender;}else{$gen=0;};
	    ?>
	    	<form class="form-horizontal" role="form" method="post"> 
		    	<div class="row">
		        	<div class="col-lg-12">
		        		<div class="container-fluid">
					      	<div class="row">
					      		
							    <div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">ادارۀ عمومی</label>
		                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
		                                    <option value="0">انتخاب</option>
		                                    @foreach($parentDeps AS $dep_item)
		                                    	@if($dep_item->id == $dep)
		                                        <option selected value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
		                                        @else
		                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
		                                        @endif
		                                    @endforeach
		                                </select>
		                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		                			</div>
		                		</div>
		                		<div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">ادارۀ مربوط</label>
		                                <select class="form-control" name="sub_dep" id="sub_dep">
		                                    <option value='0'>انتخاب</option>
		                                    @if(isset($sub_deps))
		                                    	@foreach($sub_deps AS $sub_item)
		                                    		@if($sub_item->id == $sub)
			                                        <option selected value='{!!$sub_item->id!!}'>{!!$sub_item->name!!}</option>
			                                        @else
			                                        <option value='{!!$sub_item->id!!}'>{!!$sub_item->name!!}</option>
			                                        @endif
		                                    	@endforeach
		                                    @endif
		                                </select>
		                			</div>
		                		</div>
		                		
							    <div class="col-sm-3">
							    	<label class="col-sm-12 ">&nbsp;</label>
					    			<div class="col-sm-12 checkbox-custom checkbox-primary checkbox-inline">
				                        <input value='1' name="docs" type="checkbox" @if($doc == 1) checked @endif>
				                        <label for="subordinates">
				                           اسناد ناتکمیل
				                        </label> 
				                    </div>
				                </div>
							</div>
						</div>
						<div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-3">
					            	<div class="col-sm-12">
					            		<label class="col-sm-2 ">کارکنان</label>
		                                <select name="employee_type" class="form-control" onchange="showRank(this.value)">
		                                    <option value='0'>انتخاب</option>
		                                    <option value='1' <?php if($emp_type=='1'){echo 'selected';} ?>>مامور</option>
		                                    <option value='2' <?php if($emp_type=='2'){echo 'selected';} ?>>اجیر</option>
		                                    <option value='3' <?php if($emp_type=='3'){echo 'selected';} ?>>نظامی</option>
		                                    <option value='4' <?php if($emp_type=='4'){echo 'selected';} ?>>مامور بالمقطع</option>
		                                    <option value='5' <?php if($emp_type=='5'){echo 'selected';} ?>>اجیر بالمقطع</option>
		                                </select>
					               	</div>
					               	
					            </div>
					            <div id="bast_div">
						      		<div class="col-sm-3">
						      			<div class="col-sm-12">
						      				<label class="col-sm-12 ">بست</label>
			                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
			                                <select name = "emp_bast" class="form-control">
			                                    <option value='0'>انتخاب</option>
			                                    {!!getBastStaticList('employee_rank',$bast_id)!!}
			                                </select>
						      			</div>
						      		</div>
						      	</div>
						      	<div id="ajir_div" style="display:none;">
							      	<div class="col-sm-3">
						      			<div class="col-sm-12">
						      				<label class="col-sm-12 ">درجه اجیر</label>
			                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
			                                <select name = "ajir_bast" class="form-control">
			                                    <option value='0'>انتخاب</option>
			                                    {!!getBastStaticList('employee_rank')!!}
			                                </select>
						      			</div>
						      		</div>	
					      		</div>
					      		<div id="military_div" style="display:none;">
					      			<div class="col-sm-3">
						      			<div class="col-sm-12">
						      				<label class="col-sm-12 ">بست</label>
			                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
			                                <select name = "military_bast" class="form-control">
		                                        <option value='0'>انتخاب</option>
		                                        {!!getStaticDropdown('military_rank',Input::old('emp_bast'))!!}
		                                    </select>
						      			</div>
						      		</div>
					      		</div>
					      		<div class="col-sm-2">
					            	<div class="col-sm-12">
					            		<label class="col-sm-2 ">نوعیت</label>
		                                <select name="type" class="form-control">
		                                    <option value='0'>انتخاب</option>
		                                    <option value='1' <?php if($type=='1'){echo 'selected';} ?>>اضافه بست</option>
		                                    <option value='2' <?php if($type=='2'){echo 'selected';} ?>>تبدیلی</option>
		                                    <option value='3' <?php if($type=='3'){echo 'selected';} ?>>انتظار به معاش</option>
		                                </select>
					               	</div>
					               	
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					            		<label class="col-sm-2 ">جنسیت</label>
		                                <select name="gender" class="form-control">
		                                    <option value='0'>انتخاب</option>
		                                    <option value='1' <?php if($gen=='1'){echo 'selected';} ?>>مرد</option>
		                                    <option value='2' <?php if($gen=='2'){echo 'selected';} ?>>زن</option>
		                                    
		                                </select>
					               	</div>
					               	
					            </div>
								<div class="col-sm-2 pull-right">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">&nbsp;</label>
		                                <button class="btn btn-primary pull-right" type="submit"> جستجو</button>
		                			</div>
		                			
		                		</div>
		                	</div>
		                </div>
					</div>
				</div>
		    </form>
		    <hr>
		    <div class="row">
			    <div class="col-lg-12">
			        <div class="main-box">
			            <div class="main-box-body clearfix">
				            <div class="table-responsive">
				                <table class="table table-responsive" id='search_result'>
				                    <thead>
				                    <tr>
				                        <th>#</th>
				                        <th>نام کامل</th>
				                        <th>ولد</th>
				                        <th>وظیفه</th>
				                        <th>دیپارتمنت</th>
				                        <th>بست</th>
				                        <th>تاریخ تقرر</th>
				                        <th>عملیه</th>
				                    </tr>
				                    </thead>
				                    <tbody>
				                    </tbody>
				                </table>
				            </div>
				            @if($active=='search' AND canView('hr_documents'))
                			<div class="col-sm-6">
                				<label class="col-sm-12 ">&nbsp;</label>
                                <a href="{!!URL::route('printEmployees',array($dep,$sub,$bast_id,$emp_type,$doc,$type))!!}" class="btn btn-primary pull-right">
									پرنت اکسل
								</a>
                			</div>
                			@endif
			            </div>
			        </div>
			    </div>
			</div>
	    </div>
	  </div>
	</div>
</div>
<!-- End Example Tabs -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {
				"order": [[ 1, 'asc' ]],
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/audit/getAllAttachmentsData')!!}"
            }
        );

    });
    $(document).ready(function() {
        $('#search_result1').dataTable(
            {
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::route('getRecruitmentSearchData',array($dep,$sub,$bast_id,$emp_type,$doc,$type,$gen))!!}"
            }
        );

    });
function removeReport(doc_id)
{
    var confirmed = confirm("Do you want to remove this record?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeReport")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                
                success: function(response)
                {
                    location.href="{!!URL::route('getAuditReports')!!}";
                }
            }
        );
    }

}  
function load_unplaned()
{
    var page = "{!!URL::route('loadUnplanedReports')!!}";
    
    $.ajax({
        url: page,
        type: 'post',
        //data: '&type='+div,
        //dataType:'HTML',
        success: function(r)
      	{
        	$('#unplaned').html(r);
        }
    });   
}
</script>
@stop

