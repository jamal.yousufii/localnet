@extends('layouts.master')

@section('head')
	{!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
    <title>{!!_('auditAndInvestigation')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li><span>{!!_('recommendations_and_tracking')!!}</span></li>
            <li class="active"><span>{!!$record->amount!!}</span></li>
        </ol>
    </div>
</div>
@if(canAdd('audit_reports'))
<header class="main-box-header clearfix">
    <h2>
    	<a href="javascript:void();" class="btn btn-primary pull-right" onclick="show_add()"><i class="fa fa-plus-circle fa-lg"></i>{!!_('new_payment')!!}</a>
    </h2>
</header>
@endif
<div class="row" id="add_payment" style="display:none">
    <div class="col-lg-12">
        <form class="form-horizontal" role="form" method="post" action="{!!URL::route('addMoneyPayment')!!}">
		    <div class="panel-heading">
		      <h5 class="panel-title">{!!_('add_payment')!!}</h5>
		    </div>
            <div class="container-fluid">
            	<div class="row">
            		
            		<div class="col-sm-4">
		            	<div class="col-sm-12">
                    		<label class="col-sm-12 ">{!!_('amount')!!}</label>
                            <input class="form-control" type="text" name="amount" value="{!!Input::old('amount')!!}">
                    	</div>
		            </div>
            	
		            <div class="col-sm-4">
		            	<div class="col-sm-12">
                    		<label class="col-sm-12 ">{!!_('date')!!}</label>
                            <input class="form-control datepicker_farsi" readonly type="text" name="date" value="{!!Input::old('date')!!}">
                            
                    	</div>
		            </div>
		            <div class="col-sm-4">
            			<div class="col-sm-12">
            				<label class="col-sm-12 ">{!!_('employees')!!}</label>
                            <select name="employee" class="form-control">
                                <option value="">انتخاب</option>
                                @if($employees)
                                @foreach($employees AS $emp)
                                    <option value='{!!$emp->employee_id!!}'>{!!$emp->name_dr!!} {!!$emp->last_name!!}</option>
                                @endforeach
                                @endif
                            </select>
                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
            			</div>
            		</div>
		            <input type="hidden" name="id" id="money_id" value="{!!$id!!}"/>
            	</div>
            </div>
           	</br>
		    <div class="container-fluid" >
		      	<div class="row">
		      		<div class="col-sm-6">
                		<div class="col-sm-12">
                    		<label class="col-sm-2 ">&nbsp;</label>
	                        @if(canAdd('audit_tracking'))
	                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
	                    	@else
	                    		<p>You dont have permission</p>
	                    	@endif
	                        <button onclick="hide_add()" class="btn btn-danger" type="button">لغو</button>
	                    </div>
                	</div>
		      	</div>
		    </div>
            
            {!! Form::token() !!}
        </form>
    </div>
</div>
<table class="table table-responsive">
    <thead>
    <tr>
        <th>#</th>
        <th>{!!_('amount')!!}</th>
        <th>{!!_('date')!!}</th>
       
        <th>{!!_('actions')!!}</th>
    </tr>
    </thead>
    <tbsody>
    @if($records)
    	<?php $i=1;?>
    	@foreach($records AS $rec)
    	<tr>
    		<td>{!!$i!!}</td>
    		<td>{!!$rec->amount!!}</td>
    		<td>{!!$rec->payment_date!!}</td>
    		<td>
    			<a href="javascript:void()" onclick="load_editForm({!!$rec->id!!},{!!$rec->money_id!!})" class="table-link" style="text-decoration:none;" title="Edit">
					<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
				</a> | 
				<a href="javascript:void()" onclick="removePayment({!!$rec->id!!})" class="table-link" style="text-decoration:none;" title="Edit">
					<i class="icon fa-remove" aria-hidden="true" style="font-size: 16px;"></i>
				</a>
    		</td>
    	</tr>
    	<?php $i++;?>
    	@endforeach
    	
    @else
    	<tr><td colsapn="4">{!!_('no_record_available')!!}</td></tr>
    @endif
    </tbody>
</table>
<!-- End Example Tabs -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    
function removePayment(doc_id)
{
    var confirmed = confirm("Do you want to remove this record?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removePayment")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                
                success: function(response)
                {
                    location.href="{!!URL::route('paybackMoneyNote',array($id))!!}";
                }
            }
        );
    }

}  
function show_add()
{
	$('#add_payment').fadeIn();
}
function hide_add()
{
	$('#add_payment').fadeOut();
}
function load_editForm(id,money_id)
{
	$.ajax({
            url: '{!!URL::route("load_payment_edit")!!}',
            data: '&id='+id+'&money_id='+money_id,
            type: 'post',
            
            success: function(r)
            {
                $('#add_payment').fadeIn();
                $('#add_payment').html(r);
            }
        }
    );
}
</script>
@stop

