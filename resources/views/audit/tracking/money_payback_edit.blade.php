<div class="col-lg-12">
    <form class="form-horizontal" role="form" method="post" action="{!!URL::route('editMoneyPayment',array($record->id))!!}">
	    <div class="panel-heading">
	      <h5 class="panel-title">{!!_('add_payment')!!}</h5>
	    </div>
        <div class="container-fluid">
        	<div class="row">
        		
        		<div class="col-sm-4">
	            	<div class="col-sm-12">
                		<label class="col-sm-12 ">{!!_('amount')!!}</label>
                        <input class="form-control" type="text" name="amount" value="{!!$record->amount!!}">
                	</div>
	            </div>
        	
	            <div class="col-sm-4">
	            	<div class="col-sm-12">
                		<label class="col-sm-12 ">{!!_('date')!!}</label>
                		<?php $sdate = $record->payment_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                       	<input class="form-control datepicker_farsi" readonly type="text" name="date" value='<?php if($record->payment_date !=null){echo jalali_format($sdate);}?>'>
                        
                	</div>
	            </div>
	            <div class="col-sm-4">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">{!!_('employees')!!}</label>
                        <select name="employee" class="form-control">
                            <option value="">انتخاب</option>
                            @if($employees)
                            @foreach($employees AS $emp)
                            	@if($record->employee_id==$emp->employee_id)
                                <option value='{!!$emp->employee_id!!}' selected="selected">{!!$emp->name_dr!!} {!!$emp->last_name!!}</option>
                                @else
                                <option value='{!!$emp->employee_id!!}'>{!!$emp->name_dr!!} {!!$emp->last_name!!}</option>
                                @endif
                            @endforeach
                            @endif
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
        			</div>
        		</div>
	            <input type="hidden" name="id" id="money_id" value="{!!$record->money_id!!}"/>
        	</div>
        </div>
       	</br>
	    <div class="container-fluid" >
	      	<div class="row">
	      		<div class="col-sm-6">
            		<div class="col-sm-12">
                		<label class="col-sm-2 ">&nbsp;</label>
                        @if(canAdd('audit_tracking'))
                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
                    	@else
                    		<p>You dont have permission</p>
                    	@endif
                        <button onclick="hide_add()" class="btn btn-danger" type="button">لغو</button>
                    </div>
            	</div>
	      	</div>
	    </div>
        
        {!! Form::token() !!}
    </form>
</div>
<script>
$(".datepicker_farsi").persianDatepicker(); 
</script>