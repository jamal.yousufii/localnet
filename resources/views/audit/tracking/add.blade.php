@extends('layouts.master')

@section('head')
    <title>{!!_('register_new_employee')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAuditReports')!!}">{!!_('recommendations')!!}</a></li>
    <li class="active"><span>{!!_('add_new_recommendation')!!}</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postRecommandation')!!}" enctype="multipart/form-data">
			    <div class="panel-heading">
			      <h5 class="panel-title">{!!_('add_new_recommendation_form')!!}</h5>
			    </div>
                <div class="container-fluid">
               		<div class="row">
               			<div class="col-sm-6">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">{!!_('investigation_title')!!}
                				<span style="color:red">*</span>
                				</label>
                                <select name="report_title" required class="form-control" onchange="bringDetails(this);">
                                    <option value="">انتخاب</option>
                                    @foreach($reports AS $report)
                                        <option value='{!!$report->id!!}'>{!!$report->title!!}</option>
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div id="inve_det"></div>
                		
               		</div>
               	</div>
	            </br>
				<div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">{!!_('report_attachment')!!}</label>
	                    		
	                            <input type='file'  name='attach' class="form-control">
	                    	</div>
	                   </div>
			      		
			      		<div class="col-sm-3">
	                		<div class="col-sm-12">
	                		<p>{!!_('sent_to_attorney?')!!}</p>
	                    	
		                        <div class="radio-custom radio-primary">
				                  <input type="radio" value="0" name="authority" checked />
				                  <label for="inputRadiosUnchecked">{!!_('no')!!}</label>
				                </div>
				                <div class="radio-custom radio-primary">
				                  <input type="radio" value="1" name="authority" />
				                  <label for="inputRadiosChecked">{!!_('yes')!!}</label>
				                </div>
		                    </div>
                    	</div>
			            <div class="col-sm-3">
	                		<div class="col-sm-12">
	                		<p>{!!_('money_note?')!!}</p>
	                    	
		                        <div class="radio-custom radio-primary">
				                  <input type="radio" value="0" name="money" onclick="hideMoney()" checked />
				                  <label for="inputRadiosUnchecked">{!!_('no')!!}</label>
				                </div>
				                <div class="radio-custom radio-primary">
				                  <input type="radio" value="1" name="money" onclick="showMoney()"/>
				                  <label for="inputRadiosChecked">{!!_('yes')!!}</label>
				                </div>
		                    </div>
                    	</div>
			      	</div>
			    </div>
			    </br>
			    <div class="container-fluid" style="display:none" id="money_note_div">
                	<div class="row">
                		<div class="col-sm-4">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('amount')!!}</label>
                                <input class="form-control" type="text" name="amount" value="{!!Input::old('amount')!!}">
                        	</div>
			            </div>
                	
			            <div class="col-sm-4">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('date')!!}</label>
                                <input class="form-control datepicker_farsi" readonly type="text" name="date" value="{!!Input::old('date')!!}">
                                <span style="color:red">{!!$errors->first('date')!!}</span>
                        	</div>
			            </div>
			            <div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">{!!_('employees')!!}</label>
                				<select cols="50" rows="4" class="form-control" multiple="multiple" name="employees[]" id="employees">
                                	
                                </select>
                			</div>
                		</div>
                	</div>
                </div>
                <div class="col-sm-12">
                	<div class="col-sm-1" style="display:none;float:right" id="loading_div">
					    <label class="col-sm-12">&nbsp;</label>
					    <button class="btn btn-primary" style="float:right" type="button">LOADING</button>
				    </div>
	            	<div class="col-sm-11" id="add_finding_btn">
		            	<label class="col-sm-12">&nbsp;</label>
				        <button class="btn btn-primary" style="float:right" onclick="add_recom()" type="button"> + </button>
				    </div>
				    
	            </div>
			    <div class="container-fluid" id="recom_1">
                	<div class="row">
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('title')!!}
                        		<span style="color:red">*</span>
                        		</label>
                        		<textarea class="form-control" required name="title_1">{!!Input::old('title')!!}</textarea>
                                
                        	</div>
			            </div>
			            <div class="col-sm-2">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('employee_number')!!}</label>
                        		<input type="text" class="form-control" name="number_1">
                        	</div>
			            </div>
			            <div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">{!!_('recommandation_type')!!}</label>
                                <select name="type_1" required class="form-control">
                                        <option value="">انتخاب</option>
                                    	<option value='1'>{!!_('corruption')!!}</option>
                                    	<optgroup label="{!!_('violation')!!}">
											<option value="2">{!!_('advise')!!}</option>
										    <option value="3">{!!_('warning')!!}</option>
										    <option value="4">{!!_('salary_deduction')!!}</option>
										    <option value="5">{!!_('change')!!}</option>
										    <option value="6">{!!_('fire')!!}</option>
										</optgroup>
                                    	
                                    	<option value='7'>{!!_('reformation')!!}</option>
                                </select>
                			</div>
                		</div>
                		<div class="col-sm-2">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('deadline')!!}
                        		
                        		</label>
                                <input class="form-control" type="text" name="deadline_1" value="{!!Input::old('deadline')!!}" placeholder="yyyy-mm-dd">
                                <span style="color:red">{!!$errors->first('deadline')!!}</span>
                        	</div>
			            </div>
			            <div class="col-sm-2">
	                		<div class="col-sm-12">
	                		<p>{!!_('sent_to_attorney?')!!}</p>
	                    	
		                        <div class="radio-custom radio-primary">
				                  <input type="radio" value="0" name="authority_1" checked />
				                  <label for="inputRadiosUnchecked">{!!_('no')!!}</label>
				                </div>
				                <div class="radio-custom radio-primary">
				                  <input type="radio" value="1" name="authority_1" />
				                  <label for="inputRadiosChecked">{!!_('yes')!!}</label>
				                </div>
		                    </div>
                    	</div>
                		
                	</div>
                </div>
                <br/>
                <div id="other_recom"></div>
                <input type="hidden" id="total_recom" name="total_recom" value="1"/>
                
			    <div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
		                        @if(canAdd('audit_tracking'))
		                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
		                    </div>
                    	</div>
			      	</div>
			    </div>
			 	<div class="container_fluid">
			 		<div class="row">
			 			<div class="col-sm-12">
			 			@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
			 			</div>
			 		</div>
			 	</div>
                
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function getRelatedEmployees(e)
{
    $.ajax({
            url: '{!!URL::route("bringDepEmployees")!!}',
            data: '&dep_id='+e,
            type: 'post',
            beforeSend: function(){
                $("#employees").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#employees').html(response);
            }
        }
    );
}
function showMoney()
{
	$('#money_note_div').fadeIn();
}
function hideMoney()
{
	$('#money_note_div').fadeOut();
}
function add_recom()
{
	$('#add_finding_btn').hide();
	$('#loading_div').show();
	var current_total = $('#total_recom').val();
	//var total_findings = $('#total_findings').val();
	var total = parseInt(current_total)+parseInt(1);
	//var total_findings = parseInt(total_findings)+parseInt(1);
	$('#total_recom').val(total);
	//$('#total_findings').val(total_findings);
	$.ajax({
		url:'{{URL::route("getMoreRecommendation")}}',
		data: '&total='+total,
		type:'POST',
		success:function(r){
			$('#recom_1').prepend(r);
			$('#loading_div').hide();
			$('#add_finding_btn').show();
		}
	});
}
function remove_recom(no)
{
	$('#recom_'+no).remove();
}
function bringDetails(e)
{
	id = $(e).val();
	$.ajax({
		url:'{{URL::route("getInvestigationDet")}}',
		data: '&id='+id,
		type:'POST',
		success:function(r){
			$('#inve_det').html(r);
			getRelatedEmployees($('#sub_department').val());
		}
	});
}
</script>

@stop
