
<div class="container-fluid" id="recom_{!!$total!!}">
	<div class="row">
		<div class="col-sm-3">
        	<div class="col-sm-12">
        		<label class="col-sm-12 ">{!!_('title')!!}<span style="color:red">*</span></label>
        		<textarea class="form-control" name="title_{!!$total!!}">{!!Input::old('title')!!}</textarea>
                
        	</div>
        </div>
        <div class="col-sm-2">
        	<div class="col-sm-12">
        		<label class="col-sm-12 ">{!!_('employee_number')!!}</label>
        		<input type="text" class="form-control" name="number_{!!$total!!}">
        	</div>
        </div>
        <div class="col-sm-2">
			<div class="col-sm-12">
				<label class="col-sm-12 ">{!!_('recommandation_type')!!}</label>
                <select name="type_{!!$total!!}" class="form-control">
                    <option value="">انتخاب</option>
                	<option value='1'>{!!_('corruption')!!}</option>
                	<optgroup label="{!!_('violation')!!}">
						<option value="2">{!!_('advise')!!}</option>
					    <option value="3">{!!_('warning')!!}</option>
					    <option value="4">{!!_('salary_deduction')!!}</option>
					    <option value="5">{!!_('change')!!}</option>
					    <option value="6">{!!_('fire')!!}</option>
					</optgroup>
                	<option value='7'>{!!_('reformation')!!}</option>
                </select>
			</div>
		</div>
		<div class="col-sm-2">
        	<div class="col-sm-12">
        		<label class="col-sm-12 ">{!!_('deadline')!!}</label>
                <input class="form-control" type="text" name="deadline_{!!$total!!}" placeholder="yyyy-mm-dd">
                
        	</div>
        </div>
        <div class="col-sm-2">
    		<div class="col-sm-12">
    		<p>{!!_('sent_to_attorney?')!!}</p>
        	
                <div class="radio-custom radio-primary">
                  <input type="radio" value="0" name="authority_{!!$total!!}" checked />
                  <label for="inputRadiosUnchecked">{!!_('no')!!}</label>
                </div>
                <div class="radio-custom radio-primary">
                  <input type="radio" value="1" name="authority_{!!$total!!}" />
                  <label for="inputRadiosChecked">{!!_('yes')!!}</label>
                </div>
            </div>
    	</div>
		<div class="col-sm-1">
        	<div class="col-sm-12">
        		<label class="col-sm-12 ">&nbsp;</label>
        		<button class="btn btn-danger" onclick="remove_recom({!!$total!!})" type="button"> - </button>
        	</div>
        </div>
	</div>
</div>
