@if(canAdd('audit_reports'))
<header class="main-box-header clearfix">
    <h2>
    	<a href="{!!URL::route('addNewFinding')!!}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle fa-lg"></i>{!!_('new_finding')!!}</a>
    </h2>
</header>
@endif
<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='list2'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{!!_('report_title')!!}</th>
                        
                        
                        <th>{!!_('date')!!}</th>
                        <th>{!!_('directorate')!!}</th>
                        <th>{!!_('actions')!!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#list2').dataTable(
            {
				"order": [[ 1, 'asc' ]],
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/audit/getFindingsData')!!}"
            }
        );

    });
function removeMoney(doc_id)
{
    var confirmed = confirm("Do you want to remove this record?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeFinding")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                
                success: function(response)
                {
                    location.href="{!!URL::route('getAduitRecommendations')!!}";
                }
            }
        );
    }

}  
</script>