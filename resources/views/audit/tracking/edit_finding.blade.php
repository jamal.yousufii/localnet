@extends('layouts.master')

@section('head')
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif 
<style>
.sub{
    padding-left: 25px;
}
</style>
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAuditReports')!!}">{!!_('recommendations')!!}</a></li>
    <li class="active"><span>{!!_('edit_finding')!!}</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postEditFinding',array($row->id))!!}">
			    <div class="panel-heading">
			      <h5 class="panel-title">{!!_('edit')!!}</h5>
			    </div>
                <div class="container-fluid">
                	<div class="row">
                		
                		<div class="col-sm-6">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('investigation_title')!!}</label>
                                <input class="form-control" required type="text" name="title" value="{!!$row->title!!}">
                        	</div>
			            </div>
                	
			            <div class="col-sm-6">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('investigation_date')!!}
                        		<span style="color:red">*</span>
                        		</label>
                                <?php $sdate = $row->date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                                <input class="form-control datepicker_farsi" required readonly type="text" name="date" value='<?php if($row->date !=null){echo jalali_format($sdate);}?>'>
                                
                                <span style="color:red">{!!$errors->first('date')!!}</span>
                        	</div>
			            </div>
                	</div>
                </div>
               	<div class="container-fluid">
               		<div class="row">
               			<div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">نوعیت<span style="color:red">*</span></label>
                                <select name="type" class="form-control" required onchange="showServiceType(this.value)">
                                	<option value="0" @if($row->type==0) selected @endif>داخل اداره</optioin>
                                	<option value="1" @if($row->type==1) selected @endif>خارج از اداره</option>
                                </select>
                        	</div>
                        </div>
                        <div id="internal_div" @if($row->type==1) style="display:none" @endif>
	               			<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ادارۀ عمومی</label>
	                                <select name="general_department" id="general_department" required class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
	                                        <option value="">انتخاب</option>
	                                    @foreach($parentDeps AS $dep_item)
	                                    	@if($dep_item->id == $row->general_dep)
	                                        <option selected value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
	                                        @else
	                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>
	                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	                			</div>
	                		</div>
	                		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ادارۀ مربوط</label>
	                				<select class="form-control" name="sub_dep" id="sub_dep" required onchange="getRelatedBasts(this.value)">
	                                	<option value="">انتخاب</option>
	                                	@if($deps)
	                                    @foreach($deps AS $sub)
	                                    	@if($row->sub_dep == $sub->id)	
	                                        <option value='{!!$sub->id!!}' selected>{!!$sub->name!!}</option>
	                                        @else
	                                        <option value='{!!$sub->id!!}'>{!!$sub->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                    @endif
	                                </select>
	                                
	                			</div>
	                		</div>
	                	</div>
	                	<div id="external_div" @if($row->type==0) style="display:none" @endif>
                			<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">وزارت<span style="color:red">*</span></label>
	                                <select name="ministry" class="form-control">
	                                    <option value="">انتخاب</option>
	                                	@foreach($ministrires AS $ministry)
	                                    	@if($ministry->id == $row->ministry)
	                                        <option selected value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
	                                        @else
	                                        <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>
	                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	                			</div>
	                		</div>
                		</div>
               		</div>
               	</div>
	            <div class="col-sm-12">
	            	<div class="col-sm-11" id="add_finding_btn">
		            	<label class="col-sm-12">&nbsp;</label>
				        <button class="btn btn-primary" style="float:right" onclick="add_finding()" type="button"> + </button>
				    </div>
				    <div class="col-sm-1" style="display:none;float:right" id="loading_div">
					    <label class="col-sm-12">&nbsp;</label>
					    <button class="btn btn-primary" style="float:right" type="button">LOADING</button>
				    </div>
	            </div>
	            <?php $i=0; ?>
	            @foreach($findings as $find)
	            <?php $i++;$j=0;
	            $subs = getFindingSubs($find->id);
	            ?>
	            <input type="hidden" name="finding_id_{!!$i!!}" value="{!!$find->id!!}"/>
				<div class="container-fluid" id="findings_{!!$i!!}">
                	<div class="row">
                		<div class="col-sm-10">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('finding_item')!!}
                        		@if($i==1)
                        		<span style="color:red">*</span>
                        		@endif
                        		</label>
                                <input class="form-control" type="text" name="finding_{!!$i!!}" value="{!!$find->title!!}">
                        	</div>
			            </div>
			            @if($i!=1)
			            <div class="col-sm-2">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 ">&nbsp;</label>
			            		<button class="btn btn-danger" onclick="remove_finding({!!$i!!})" type="button"> - </button>
			            	</div>
			            </div>
			            @endif
                	</div>
                </div>
                @if($subs)
                	@foreach($subs AS $sub)
                	<?php $j++; ?>
                	<div id="subfindings_{!!$j!!}_{!!$i!!}">
	            	<div class="row sub">
	            		<div class="col-sm-9">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">{!!_('sub_item')!!}</label>
	                            <input class="form-control" type="text" name="subfinding_{!!$j!!}_{!!$i!!}" value="{!!$sub->title!!}">
	                    	</div>
			            </div>
			            @if($j==1)
			            <div class="col-sm-2">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 ">&nbsp;</label>
			            		<button class="btn btn-primary" onclick="add_subfinding({!!$i!!})" type="button"> + </button>
			            	</div>
			            </div>
			            @else
			            <div class="col-sm-2">
					    	<div class="col-sm-12">
					    		<label class="col-sm-12 ">&nbsp;</label>
					    		<button class="btn btn-danger" onclick="remove_subfinding({!!$j!!},{!!$i!!})" type="button"> - </button>
					    	</div>
					    </div>
			            @endif
	            	</div>
	            	</div>
                	@endforeach
                	<input type="hidden" id="sub_total_{!!$i!!}" name="total_sub_{!!$i!!}" value="{!!$j!!}"/>
                	<div id="other_subfindings_{!!$i!!}"></div>
                @else
                <div id="subfindings_1_1">
            	<div class="row sub">
            		<div class="col-sm-9">
		            	<div class="col-sm-12">
                    		<label class="col-sm-12 ">{!!_('sub_item')!!}</label>
                            <input class="form-control" type="text" name="subfinding_1_1" value="{!!Input::old('subfinding_1')!!}">
                    	</div>
		            </div>
		            <div class="col-sm-2">
		            	<div class="col-sm-12">
		            		<label class="col-sm-12 ">&nbsp;</label>
		            		<button class="btn btn-primary" onclick="add_subfinding(1)" type="button"> + </button>
		            	</div>
		            </div>
            	</div>
            	<input type="hidden" id="sub_total_1" name="total_sub_1" value="1"/>
            	</div>
            	<div id="other_subfindings_1"></div>
                @endif
                <br/>
               
                @endforeach
                <div id="other_findings"></div>
                <input type="hidden" id="total_files" name="total_findings" value="{!!$i!!}"/>
                
			    <div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
		                        @if(canAdd('audit_tracking'))
		                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
		                    </div>
                    	</div>
			      	</div>
			    </div>
			 	<div class="container_fluid">
			 		<div class="row">
			 			<div class="col-sm-12">
			 			@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
			 			</div>
			 		</div>
			 	</div>
                
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function add_finding()
{
	$('#add_finding_btn').hide();
	$('#loading_div').show();
	var current_total = $('#total_files').val();
	//var total_findings = $('#total_findings').val();
	var total = parseInt(current_total)+parseInt(1);
	//var total_findings = parseInt(total_findings)+parseInt(1);
	$('#total_files').val(total);
	//$('#total_findings').val(total_findings);
	$.ajax({
		url:'{{URL::route("getMoreFinding")}}',
		data: '&total='+total,
		type:'POST',
		success:function(r){
			$('#findings_1').prepend(r);
			$('#loading_div').hide();
			$('#add_finding_btn').show();
		}
	});
}
function remove_finding(no)
{
	$('#findings_'+no).remove();
}
function add_subfinding(finding_no)
{
	var current_total = $('#sub_total_'+finding_no).val();
	//var total_findings = $('#total_findings').val();
	var total = parseInt(current_total)+parseInt(1);
	//var total_findings = parseInt(total_findings)+parseInt(1);
	$('#sub_total_'+finding_no).val(total);
	//$('#total_findings').val(total_findings);
	$.ajax({
		url:'{{URL::route("getMoreSubFinding")}}',
		data: '&total='+total+'&finding='+finding_no,
		type:'POST',
		success:function(r){
			$('#other_subfindings_'+finding_no).append(r);
		}
	});
}
function remove_subfinding(no,finding_no)
{
	$('#subfindings_'+no+'_'+finding_no).remove();
}
function showServiceType(value)
{
    if(value == 1)
    {
        //$('#ageer_div').slideUp();
        $('#internal_div').slideUp();
        $('#external_div').slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#external_div').slideUp();
        $('#internal_div').slideDown();
    }

} 
</script>

@stop

