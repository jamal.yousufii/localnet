
<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='list1'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{!!_('amount')!!}</th>
                        <th>{!!_('general_directorate')!!}</th>
                        <th>{!!_('directorate')!!}</th>
                        <th>{!!_('received')!!}</th>
                        <th>{!!_('actions')!!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#list1').dataTable(
            {
				"order": [[ 1, 'asc' ]],
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/audit/getMoneyData')!!}"
            }
        );

    });
function removeMoney(doc_id)
{
    var confirmed = confirm("Do you want to remove this record?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeMoneyNote")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                
                success: function(response)
                {
                    location.href="{!!URL::route('getAduitRecommendations')!!}";
                }
            }
        );
    }

}  
</script>