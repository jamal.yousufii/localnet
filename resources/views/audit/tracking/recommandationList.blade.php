@if(canAdd('audit_reports'))
<header class="main-box-header clearfix">
    <h2>
    	<a href="{!!URL::route('addNewRecommendation')!!}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle fa-lg"></i>{!!_('new_recommendation')!!}</a>
    </h2>
</header>
@endif
<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='list3'>
                    <thead>
                    <tr>
                        <th>#</th>
                        
                        <th>{!!_('investigation_title')!!}</th>
                        <th>{!!_('deadline')!!}</th>
                        
                        <th>{!!_('sent_to_attorney?')!!}</th>
                        <th>{!!_('responsible_directorate')!!}</th>
                        <th>{!!_('actions')!!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-primary" id="exampleModalPrimary" aria-hidden="true" aria-labelledby="exampleModalPrimary" role="dialog" tabindex="-1">
  <div class="modal-dialog" style="width: 1200px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">{!!_('implementation')!!}</h4>
      </div>
      <div id="form_part">
        
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#list3').dataTable(
            {
				"order": [[ 1, 'asc' ]],
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/audit/getRecommandationsData')!!}"
            }
        );

    });
function add_recommendation_modal(doc_id)
{
	$.ajax({
        url: '{!!URL::route("loadRecommendationItemsModal")!!}',
        data: 'id='+doc_id,
        type: 'post',
        success: function(response)
        {
            $('#form_part').html(response);
        }
    }
  );
}

 
</script>