<form class="form-horizontal" role="form" method="post" action="{!!URL::route('ImplementedRecommandation')!!}">
	<div class="modal-body">
  		<input type="hidden" value="{!!$id!!}" name="doc_id">
  		<input type="hidden" value="{!!$type!!}" name="type">
      	<div class="form-group">
          	<label class="col-sm-2 control-label">{!!_('description')!!}</label>
          	<div class="col-sm-10">
            	<textarea class="form-control" name="desc">{!!$record->implementaion_desc!!}</textarea>
          	</div>
      	</div>
  	</div>

  	<div class="modal-footer">
	    <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">{!!_('close')!!}</button>
	    <button type="submit" class="btn btn-primary">{!!_('save')!!}</button>
  	</div>
</form>