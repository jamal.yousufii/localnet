@extends('layouts.master')

@section('head')
    <title>{!!_('register_new_employee')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAuditReports')!!}">{!!_('recommendations')!!}</a></li>
    <li class="active"><span>{!!_('edit_money_note')!!}</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postMoneyNoteEdit',array($row->id))!!}" enctype="multipart/form-data">
			    <div class="panel-heading">
			      <h5 class="panel-title">{!!_('edit_money_note_form')!!}</h5>
			    </div>
                <div class="container-fluid">
                	<div class="row">
                		
                		<div class="col-sm-6">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('amount')!!}</label>
                                <input class="form-control" type="text" name="amount" value="{!!$row->amount!!}">
                        	</div>
			            </div>
                	
			            <div class="col-sm-6">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('date')!!}</label>
                        		<?php $sdate = $row->date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                                <input class="form-control datepicker_farsi" readonly type="text" name="date" value='<?php if($row->date !=null){echo jalali_format($sdate);}?>'>
                                
                                <span style="color:red">{!!$errors->first('date')!!}</span>
                        	</div>
			            </div>
                	</div>
                </div>
               	<div class="container-fluid">
               		<div class="row">
               			<div class="col-sm-6">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی</label>
                                <select name="general_department" id="general_department" required class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                        <option value="">انتخاب</option>
                                    @foreach($parentDeps AS $dep_item)
                                    	@if($dep_item->id == $row->general_dir)
                                        <option selected value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @else
                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-6">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ مربوط</label>
                				<select class="form-control" name="sub_dep" id="sub_dep" required onchange="getRelatedBasts(this.value)">
                                	<option value="">انتخاب</option>
                                	@if($deps)
                                    @foreach($deps AS $sub)
                                    	@if($row->sub_dir == $sub->id)	
                                        <option value='{!!$sub->id!!}' selected>{!!$sub->name!!}</option>
                                        @else
                                        <option value='{!!$sub->id!!}'>{!!$sub->name!!}</option>
                                        @endif
                                    @endforeach
                                    @endif
                                </select>
                                
                			</div>
                		</div>
               		</div>
               	</div>
	            </br>
				
			    <div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
		                        @if(canAdd('audit_tracking'))
		                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
		                    </div>
                    	</div>
			      	</div>
			    </div>
			 	<div class="container_fluid">
			 		<div class="row">
			 			<div class="col-sm-12" id="errors">
			 			@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
			 			</div>
			 		</div>
			 	</div>
                
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}

</script>

@stop

