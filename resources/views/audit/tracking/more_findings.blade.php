<div class="container-fluid" id="findings_{!!$total!!}">
	<div class="row">
		<div class="col-sm-10">
        	<div class="col-sm-12">
        		<label class="col-sm-12 ">{!!_('finding_item')!!}</label>
                <input class="form-control" type="text" name="finding_{!!$total!!}">
        	</div>
        </div>
        <div class="col-sm-2">
        	<div class="col-sm-12">
        		<label class="col-sm-12 ">&nbsp;</label>
        		<button class="btn btn-danger" onclick="remove_finding({!!$total!!})" type="button"> - </button>
        	</div>
        </div>
	</div>
	<div id="subfindings_1_{!!$total!!}">
	<div class="row sub">
		<div class="col-sm-9">
	    	<div class="col-sm-12">
	    		<label class="col-sm-12 ">{!!_('sub_item')!!}</label>
	            <input class="form-control" type="text" name="subfinding_1_{!!$total!!}" value="{!!Input::old('subfinding_1')!!}">
	    	</div>
	    </div>
	    <div class="col-sm-2">
	    	<div class="col-sm-12">
	    		<label class="col-sm-12 ">&nbsp;</label>
	    		<button class="btn btn-primary" onclick="add_subfinding({!!$total!!})" type="button"> + </button>
	    	</div>
	    </div>
	</div>
	<input type="hidden" id="sub_total_{!!$total!!}" name="total_sub_{!!$total!!}" value="1"/>
	</div>
	<div id="other_subfindings_{!!$total!!}"></div>
</div>