@extends('layouts.master')

@section('head')
    <title>{!!_('register_new_employee')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAuditReports')!!}">{!!_('recommendations')!!}</a></li>
    <li class="active"><span>{!!_('add_new_money_note')!!}</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postMoneyNote')!!}">
			    <div class="panel-heading">
			      <h5 class="panel-title">{!!_('add_new_money_note_form')!!}</h5>
			    </div>
                <div class="container-fluid">
                	<div class="row">
                		
                		<div class="col-sm-6">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('amount')!!}</label>
                                <input class="form-control" type="text" name="amount" value="{!!Input::old('amount')!!}">
                        	</div>
			            </div>
                	
			            <div class="col-sm-6">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('date')!!}</label>
                                <input class="form-control datepicker_farsi" readonly type="text" name="date" value="{!!Input::old('date')!!}">
                                <span style="color:red">{!!$errors->first('date')!!}</span>
                        	</div>
			            </div>
                	</div>
                </div>
               	<div class="container-fluid">
               		<div class="row">
               			<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12">{!!_('general_department')!!}</label>
                                <select name="general_department" id="general_department" required class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                        <option value="">انتخاب</option>
                                    @foreach($parentDeps AS $dep_item)
                                    	
                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">{!!_('sub_department')!!}</label>
                				<select class="form-control" name="sub_dep" id="sub_dep" required onchange="getRelatedEmployees(this.value)">
                                	<option value="">انتخاب</option>
                                    
                                </select>
                                
                			</div>
                		</div>
                		<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">{!!_('employees')!!}</label>
                				<select cols="50" rows="4" class="form-control" multiple="multiple" name="employees[]" id="employees" required>
                                	
                                </select>
                                
                			</div>
                		</div>
               		</div>
               	</div>
	            </br>
				
			    <div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
		                        @if(canAdd('audit_tracking'))
		                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
		                    </div>
                    	</div>
			      	</div>
			    </div>
			 	<div class="container_fluid">
			 		<div class="row">
			 			<div class="col-sm-12">
			 			@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
			 			</div>
			 		</div>
			 	</div>
                
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function getRelatedEmployees(e)
{
    $.ajax({
            url: '{!!URL::route("bringDepEmployees")!!}',
            data: '&dep_id='+e,
            type: 'post',
            beforeSend: function(){
                $("#employees").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#employees').html(response);
            }
        }
    );
}
</script>

@stop

