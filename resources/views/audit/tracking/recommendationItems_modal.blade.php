<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='list3'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{!!_('title')!!}</th>
                        <th>{!!_('type')!!}</th>
                        <th>{!!_('deadline')!!}</th>
                        <th>{!!_('employee_number')!!}</th>
                        <th>{!!_('sent_to_attorney?')!!}</th>
                        <th>{!!_('implemented?')!!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($records)
                    	@foreach($records as $rec)
                    	<tr>
                    		<td>{!!$rec->id!!}</td>
                    		<td>{!!$rec->title!!}</td>
                    		<td>
                    			@if($rec->type==1){!!_('corruption')!!}
                    			@elseif($rec->type==7){!!_('reformation')!!}
                    			@else{!!_('violation')!!}
                    			@endif
                    		</td>
                    		<td>{!!$rec->deadline!!}</td>
                    		<td>{!!$rec->employee_number!!}</td>
                    		<td>
                    			@if($rec->authority==0){!!_('yes')!!}
                    			@else{!!_('no')!!}
                    			@endif
                    		</td>
                    		<td id="status_{!!$rec->id!!}">
                    		@if($rec->implemented==0)
								<a href="javascript:void()" onclick="show_desc({!!$rec->id!!})" class="table-link" style="text-decoration:none;" title="Not Implemented">
									<i class="icon fa-archive" aria-hidden="true"></i>
								</a>
							
							@else
								<a href="javascript:void()" onclick="implementedRec({!!$rec->id!!})" class="table-link" style="text-decoration:none;" title="Implemented">
									<i class="icon fa-check" aria-hidden="true"></i>
								</a>
							@endif
                    		</td>
                    	</tr>
                    	<tr id="desc_{!!$rec->id!!}" style="display:none">
                    		<td>
                    			{!!_('description')!!}:
                    		</td>
                    		<td colspan="4">
                    			<input class="col-sm-12 form-control" id="description_{!!$rec->id!!}" type="text"/>
                    		</td>
                    		<td colspan="2">
                    			<input type="button" class="btn btn-danger" onclick="hide_desc({!!$rec->id!!})" value="{!!_('cancel')!!}"/> | 
                    			<input type="button" class="btn btn-success" onclick="save_desc({!!$rec->id!!})" value="{!!_('save')!!}"/>
                    		</td>
                    	</tr>
                    	@endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<script>
function show_desc(id)
{
	$('#desc_'+id).show();
}
function hide_desc(id)
{
	$('#desc_'+id).hide();
}
function save_desc(id)
{
	var desc = $('#description_'+id).val();
	if(desc=='')
	{
		return false;
	}
	else
	{
		$.ajax({
            url: '{!!URL::route("ImplementedRecommandation")!!}',
            data: '&doc_id='+id+'&desc='+desc+'&type=0',
            type: 'post',
            beforeSend: function(){
                $("#desc_"+id).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
            	$("#desc_"+id).html('');
                $('#status_'+id).html(response);
            }
        });
	}
}
function implementedRec(doc_id)
{
	var confirmed = confirm("Do you want to change the status to NOT implemented?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("ImplementedRecommandation")!!}',
                data: '&doc_id='+doc_id+'&type=1',
                type: 'post',
                
                success: function(response)
                {
                    $('#status_'+doc_id).html(response);
                }
            }
        );
    }
} 
</script>