<div id="subfindings_{!!$total!!}_{!!$finding!!}">
<div class="row sub">
	<div class="col-sm-9">
    	<div class="col-sm-12">
    		<label class="col-sm-12 ">{!!_('sub_item')!!}</label>
            <input class="form-control" type="text" name="subfinding_{!!$total!!}_{!!$finding!!}">
    	</div>
    </div>
    <div class="col-sm-2">
    	<div class="col-sm-12">
    		<label class="col-sm-12 ">&nbsp;</label>
    		<button class="btn btn-danger" onclick="remove_subfinding({!!$total!!},{!!$finding!!})" type="button"> - </button>
    	</div>
    </div>
</div>
</div>