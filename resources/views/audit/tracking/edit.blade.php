@extends('layouts.master')

@section('head')
    <title>{!!_('register_new_employee')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAuditReports')!!}">{!!_('recommendations')!!}</a></li>
    <li class="active"><span>{!!_('edit_recommendation')!!}</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postRecommandationEdit',array($row->id))!!}" enctype="multipart/form-data">
			    <div class="panel-heading">
			      <h5 class="panel-title">{!!_('edit_recommendation_form')!!}</h5>
			    </div>
			    <div class="container-fluid">
               		<div class="row">
               			<div class="col-sm-6">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">{!!_('report_title')!!}</label>
                                <select name="report_title" required class="form-control" onchagne="bringDetails(this)">
                                    <option value="">انتخاب</option>
                                    @foreach($reports AS $report)
                                    	@if($report->id == $row->report_title_id)
                                        <option value='{!!$report->id!!}' selected>{!!$report->title!!}</option>
                                        @else
                                        <option value='{!!$report->id!!}'>{!!$report->title!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div id="inve_det">
                			<div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">{!!_("general_department")!!}</label>
				                    <input class="form-control" readonly type="text" value="{!!$investigation_det->gen_dir!!}">
				            	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">{!!_("sub_department")!!}</label>
				                    <input class="form-control" readonly type="text" value="{!!$investigation_det->sub_dir!!}">
				            	</div>
				            </div>
                		</div>
                		
               		</div>
               	</div>
	            </br>
				<div class="container-fluid" >
			      	<div class="row">
			      		@if($row->file_id!=0)
			      			<div class="col-sm-6" id="attached_file_div">
		                		<div class="col-sm-12">
		                    		<label class="col-sm-12 ">&nbsp;</label>
		                    		<a href="{!!URL::route('downloadRecommandationDoc',$row->file_id)!!}">{!!$row->file_name!!}</a>
		                    		@if(canDelete('audit_tracking'))
		                    		<a href="javascript:void()" onclick="removeReportFile('{!!$row->id!!}');" class="table-link danger">
                                        <i class="fa fa-trash-o" style='color:red;'></i>
                                    </a>	
                                    @endif
		                    	</div>
		                    </div>
			      		
	                   @endif
	                   <div class="col-sm-6" @if($row->file_id!=0) style="display:none" @endif id="attach_div">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">{!!_('report_attachment')!!}</label>
	                    		
	                            <input type='file'  name='attach' class="form-control">
	                    	</div>
	                   </div>
	                   <div class="col-sm-3">
	                		<div class="col-sm-12">
	                		<p>{!!_('sent_to_attorney?')!!}</p>
	                    	
		                        <div class="radio-custom radio-primary">
				                  <input type="radio" value="0" name="authority" @if($row->sent_authority==0) checked @endif />
				                  <label for="inputRadiosUnchecked">{!!_('no')!!}</label>
				                </div>
				                <div class="radio-custom radio-primary">
				                  <input type="radio" value="1" name="authority" @if($row->sent_authority==1) checked @endif/>
				                  <label for="inputRadiosChecked">{!!_('yes')!!}</label>
				                </div>
		                    </div>
                    	</div>
                    	<div class="col-sm-3">
	                		<div class="col-sm-12">
	                		<p>{!!_('money_note?')!!}</p>
	                    	
		                        <div class="radio-custom radio-primary">
				                  <input type="radio" value="0" onclick="hideMoney()" name="money" @if($row->money_note==0) checked @endif />
				                  <label for="inputRadiosUnchecked">{!!_('no')!!}</label>
				                </div>
				                <div class="radio-custom radio-primary">
				                  <input type="radio" value="1" onclick="showMoney()" name="money" @if($row->money_note==1) checked @endif/>
				                  <label for="inputRadiosChecked">{!!_('yes')!!}</label>
				                </div>
		                    </div>
                    	</div>
	                   
                	</div>
                </div>
                <br/>
                <div class="container-fluid" @if($row->money_note==0) style="display:none" @endif id="money_note_div">
                	<div class="row">
                		<div class="col-sm-4">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('amount')!!}</label>
                                <input class="form-control" type="text" name="amount" value="{!!$row->amount!!}">
                        	</div>
			            </div>
                	
			            <div class="col-sm-4">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('date')!!}</label>
                        		<?php $sdate = $row->date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                                <input class="form-control datepicker_farsi" readonly type="text" name="date" value='<?php if($row->date !=null){echo jalali_format($sdate);}?>'>
                                
                                <span style="color:red">{!!$errors->first('date')!!}</span>
                        	</div>
			            </div>
			            <div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">{!!_('employees')!!}</label>
                				<select cols="50" rows="4" multiple="multiple" class="form-control" name="employees[]" id="employees">
                                	<option value="">{!!_('select')!!}</option>
                                	@if($employees)
                                    @foreach($employees AS $employee)
                                    	
                                    	@if(array_key_exists($employee->id,$money_employees))	
                                        <option value='{!!$employee->id!!}' selected>{!!$employee->name!!} {!!$employee->last_name!!}</option>
                                        @else
                                        <option value='{!!$employee->id!!}'>{!!$employee->name!!} {!!$employee->last_name!!}</option>
                                        @endif
	                                    
                                    @endforeach
                                    @endif
                                </select>
                                
                			</div>
                		</div>
                		<input type="hidden" name="money_id" value="{!!$row->money_id!!}"/>
                	</div>
                </div>
                <div class="col-sm-12">
                	<div class="col-sm-1" style="display:none;float:right" id="loading_div">
					    <label class="col-sm-12">&nbsp;</label>
					    <button class="btn btn-primary" style="float:right" type="button">LOADING</button>
				    </div>
	            	<div class="col-sm-11" id="add_finding_btn">
		            	<label class="col-sm-12">&nbsp;</label>
				        <button class="btn btn-primary" style="float:right" onclick="add_recom()" type="button"> + </button>
				    </div>
				    
	            </div>
                <?php $i=0;?>
                @foreach($items as $item)
                <?php $i++;?>
                <div class="container-fluid" id="recom_{!!$i!!}">
                	<div class="row">
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('title')!!}</label>
                        		<textarea class="form-control" name="title_{!!$i!!}">{!!$item->title!!}</textarea>
                                
                        	</div>
			            </div>
			            <div class="col-sm-2">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('employee_number')!!}</label>
                        		<input type="text" class="form-control" name="number_1">
                        	</div>
			            </div>
                		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">{!!_('recommandation_type')!!}</label>
                                <select name="type_{!!$i!!}" class="form-control">
                                        <option value="">انتخاب</option>
                                    	<option value='1' @if($item->type==1) selected @endif>{!!_('corruption')!!}</option>
                                    	<optgroup label="{!!_('violation')!!}">
											<option value="2" @if($item->type==2) selected @endif>{!!_('advise')!!}</option>
										    <option value="3" @if($item->type==3) selected @endif>{!!_('warning')!!}</option>
										    <option value="4" @if($item->type==4) selected @endif>{!!_('salary_deduction')!!}</option>
										    <option value="5" @if($item->type==5) selected @endif>{!!_('change')!!}</option>
										    <option value="6" @if($item->type==6) selected @endif>{!!_('fire')!!}</option>
										</optgroup>
                                    	<option value='7' @if($item->type==7) selected @endif>{!!_('reformation')!!}</option>
                                </select>
                			</div>
                		</div>
                		<div class="col-sm-2">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">{!!_('deadline')!!}</label>
                        		<input class="form-control" type="text" name="deadline_{!!$i!!}" value='{!!$item->deadline!!}' placeholder="yyyy-mm-dd">
                                
                        	</div>
			            </div>
			            <div class="col-sm-2">
	                		<div class="col-sm-12">
	                		<p>{!!_('sent_to_attorney?')!!}</p>
		                        <div class="radio-custom radio-primary">
				                  <input type="radio" value="0" name="authority_{!!$i!!}" @if($item->authority==0) checked @endif />
				                  <label for="inputRadiosUnchecked">{!!_('no')!!}</label>
				                </div>
				                <div class="radio-custom radio-primary">
				                  <input type="radio" value="1" name="authority_{!!$i!!}" @if($item->authority==1) checked @endif/>
				                  <label for="inputRadiosChecked">{!!_('yes')!!}</label>
				                </div>
		                    </div>
                    	</div>
                    	@if($i!=1)
			            <div class="col-sm-1">
				        	<div class="col-sm-12">
				        		<label class="col-sm-12 ">&nbsp;</label>
				        		<button class="btn btn-danger" onclick="remove_recom({!!$i!!})" type="button"> - </button>
				        	</div>
				        </div>
				        @endif
                	</div>
                </div>
               	<br/>
                @endforeach
                <div id="other_recom"></div>
                <input type="hidden" id="total_recom" name="total_recom" value="{!!$i!!}"/>
                
			    <div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
		                        @if(canAdd('audit_tracking'))
		                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
		                    </div>
                    	</div>
			      	</div>
			    </div>
			 	<div class="container_fluid">
			 		<div class="row">
			 			<div class="col-sm-12" id="errors">
			 			@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
			 			</div>
			 		</div>
			 	</div>
                
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function getRelatedEmployees(e)
{
    $.ajax({
            url: '{!!URL::route("bringDepEmployees")!!}',
            data: '&dep_id='+e,
            type: 'post',
            beforeSend: function(){
                $("#employees").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#employees').html(response);
            }
        }
    );
}
function removeReportFile(doc_id)
{
    var confirmed = confirm("Do you want to remove this file?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeRecommandationFile")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                beforeSend: function(){
                    $("#errors").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#errors').html(response);
                    $('#attached_file_div').remove();
                    $('#attach_div').show();
                }
            }
        );
    }

}   
function showMoney()
{
	$('#money_note_div').fadeIn();
}
function hideMoney()
{
	$('#money_note_div').fadeOut();
}
function add_recom()
{
	$('#add_finding_btn').hide();
	$('#loading_div').show();
	var current_total = $('#total_recom').val();
	//var total_findings = $('#total_findings').val();
	var total = parseInt(current_total)+parseInt(1);
	//var total_findings = parseInt(total_findings)+parseInt(1);
	$('#total_recom').val(total);
	//$('#total_findings').val(total_findings);
	$.ajax({
		url:'{{URL::route("getMoreRecommendation")}}',
		data: '&total='+total,
		type:'POST',
		success:function(r){
			$('#recom_1').prepend(r);
			$('#loading_div').hide();
			$('#add_finding_btn').show();
		}
	});
}
function remove_recom(no)
{
	$('#recom_'+no).remove();
}
function bringDetails(e)
{
	id = $(e).val();
	$.ajax({
		url:'{{URL::route("getInvestigationDet")}}',
		data: '&id='+id,
		type:'POST',
		success:function(r){
			$('#inve_det').html(r);
			getRelatedEmployees($('#sub_department').val());
		}
	});
}
</script>

@stop

