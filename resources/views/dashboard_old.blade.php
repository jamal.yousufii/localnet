@extends('layouts.master')

@section('head')
	<title>Dashboard</title>
	@if(getLangShortcut()=='en')
	{!! HTML::style('/fullcalendar/assets/css/fullcalendar.css') !!}
	@else
	{!! HTML::style('/fullcalendar/assets/css/fullcalendar.css') !!}
	{!! HTML::style('/fullcalendar/assets/css/jfullcalendar.css') !!}
	@endif
	{!! HTML::style('/fullcalendar/assets/css/fullcalendar.print.css',array('media'=>'print')) !!}
	
	
	<style type="text/css">
		.main-box i{
			font-size: 2.6em !important;
			margin-left: -16px !important;
			color: #FFF !important;
			margin-top: -21px !important;
			margin-right: -1px !important;
		}
		.main-box .value{
			text-align: left;
			font-size: 21px;
			font-weight: bold;
		}
		.main-box a{
			color: white;
		}
		.main-box a:hover{
			color: lightgray;
			text-decoration: none;
		}
		
		.panel-collapse{
			border: 1px solid lightgray;
			
		}
		.panel {box-shadow:none;}
		.panel-title {padding:8px 11px !important;}
		.panel-actions {right:11px;}
		.panel-body {padding:15px;}
		.panel-actions .panel-action {padding:8px 5px;}
		.panel-title > .icon{margin:0 !important;}
		
		
		.maincalendar {position:relative; min-height:800px;}
		.innercalendar {position:absolute; left:0;width:100%}
		
		
	</style>
	<style>

	#trash{
		width: 40px;
		height: 40px;
		padding-bottom: 8px;
		float: left;
		
	}
		
	#wrap {
		width: 1100px;
		margin: 0 auto;
	}
		
	#external-events {
		float: left;
		width: 100%;
		padding: 0px 10px;
		border: 1px solid #CCC;
		background: none repeat scroll 0% 0% #EEE;
		text-align: left;
		height: 60px;
	}
		
	#external-events h4 {
		font-size: 16px;
		margin-top: 0;
		padding-top: 1em;
	}
		
	#external-events .fc-event {
		    margin: 10px 0px;
		    cursor: pointer;
		    width: 30%;
		    padding: 6px;
		    margin-left: 73px;

	}
		
	#external-events p {
		margin: 1.5em 0;
		font-size: 11px;
		color: #666;
	}
		
	#external-events p input {
		margin: 0;
		vertical-align: middle;
	}

	#calendar {
		float: right;
		width: 100%;
	}
	
	body .fc {
    font-size: 0.9em;
}


.add_todo input:focus{

	box-shadow: 0 0 5px rgba(81, 203, 238, 1);
	padding: 3px 0px 3px 3px;
	margin: 5px 1px 3px 0px;
	border: 1px solid rgba(81, 203, 238, 1);
	width: 185px !important;
	
}
.add_todo input{
			border:1px solid white;
			padding: 3px 0px 3px 3px;
			width: 187px !important;
		
}

.text_limit{
	white-space: nowrap; 
    width: 15em; 
    overflow: hidden;
    text-overflow: ellipsis; 
    display:inline-block;
}
.text_limit a{
	text-decoration: none;
	color: #76838F;
}
.li-item
{
	padding: 7px 15px !important;
	/*background-color:#F3F7F9;*/
	border: 1px solid white;
}
.li-item:hover
{
	padding: 7px 15px !important;
	background-color:#F3F7F9;
	border: 1px solid white;
}

.fc-header tr td{
	border:0px !important;
}
.fc-header{
	border:0px !important;
}
.fc-header .fc-button{
	padding:4px;
	font-size: 14px;
}

.fc-border-separate th,.fc-head th{
	font-weight:bold;
	background: #ddd;
}


</style>

@stop

@section('content')

<div class="row">
	
	
	<div class="col-sm-8 maincalendar">
	<!-- Example Panel Toolbar -->
      <div class="panel panel-bordered panel-dark" id="examplePanelToolbar">
        
        <div class="panel-collapse innercalendar" style="border:0px !important;">
        	
          
          <div class="panel-body" id="calendar">
          <div id='external-events'>
				<p>
					{!! HTML::image('/fullcalendar/assets/img/trashcan.png','',array('id'=>'trash')) !!}
				</p>
				<div class='fc-event'>New Event</div>
				
			</div>
          
          </div>
        </div>
      </div>
      <!-- End Example Panel Toolbar -->
    </div>
    
    
    <div class="col-sm-4">
	<!-- Example Panel Toolbar -->
      <div class="panel panel-bordered panel-dark">
        <div class="panel-heading">
          <div class="panel-actions">
       
            <a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-expanded="true"aria-hidden="true"></a>
            <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
            <a class="panel-action icon wb-close" data-toggle="panel-close" aria-hidden="true"></a>
          </div>
          <h3 class="panel-title"><i class="icon wb-list"></i> Personal Todos</h3>
          
        </div>
        <div class="panel-collapse">
          @if(count($todos)>8)
          <div class="panel-body" id="panel-body" style="height:300px; overflow-y: scroll;">
          @else
          <div class="panel-body" id="panel-body">
          @endif
          	<ul class="list-group calendar-list" id="todo_list">
          		@if($todos)
          			@foreach($todos AS $item)
		                <li class="list-group-item li-item" title="{!!$item->title!!}">
		                  <div class="text_limit">
			                  <i class="wb-check red-600 margin-right-10" aria-hidden="true"></i>
			                  <a href="javascript:void()" data-target="#task_detail" data-toggle="modal" onclick="load_task_detail('task_detail',{!!$item->id!!})">
			                  	{!!$item->title!!}
			                  </a>
		                  </div>
		                </li>
		                
                	@endforeach
                @endif
                
            </ul>
            
        	<ul class="list-group calendar-list add_todo">
            	<li class="list-group-item">
            	<span class="item-plus red-600">+</span>
            	<input style="width:199px;" type="text" id="todo_input" onkeypress="saveNewTask(event)" placeholder="New Todo Item...">
            	</li>
        	</ul>
            
        </div>
      </div>
      <br>
      <!-- End Example Panel Toolbar -->
      <div class="panel panel-bordered panel-dark">
        <div class="panel-heading">
          <div class="panel-actions">
       
            <a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-expanded="true"aria-hidden="true"></a>
            <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
            <a class="panel-action icon wb-close" data-toggle="panel-close" aria-hidden="true"></a>
          </div>
          <h3 class="panel-title" style="background: #57c7d4"><i class="icon wb-inbox"></i> Mailbox</h3>
          
        </div>
        <div class="panel-collapse">
          
          <div class="panel-body">
          	Your mailbox is empty!
          </div>
        </div>
      </div>
    </div>
    
  

</div>
<div class="modal fade modal-fade-in-scale-up" id="task_detail" aria-hidden="true" aria-labelledby="task_detail" role="dialog" tabindex="-1"></div>


@stop
@section('footer-scripts')

{!! HTML::script('/fullcalendar/assets/js/moment.min.js') !!}

{!! HTML::script('/fullcalendar/assets/js/jquery-ui.min.js') !!}
@if(getLangShortcut()=='en')
{!! HTML::script('/fullcalendar/assets/js/fullcalendar.js') !!}
@else
{!! HTML::script('/fullcalendar/assets/js/jfullcalendar.js') !!}
@endif
<script>

function load_task_detail(type,id)
{
	var page = "{!!URL::route('load_task_detail')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id+'&type='+type,
        success: function(r){
			$('#task_detail').html(r);
        }
    });
}

	//insert via ajax
    function saveNewTask(e)
    {   
    	
    	var task_group = 1;
    	var parent_task = '';
    	
        var page = "{!!URL::route('saveNewTask')!!}";

        var title = $('#todo_input').val();

        if (e.keyCode == 13 && title != '') 
        {
        	e.preventDefault();
        	
            $.ajax({
                url: page,
                type: 'post',
                data: '&title='+title+'&task_group='+task_group+'&parent='+parent_task+'&from_dash=1',
                dataType:'HTML',
                success: function(response)
            	{
                    $('#todo_list').append(response);
                    $('#todo_input').val('');
                }
            });
        }
       

    }
    
	
	$(document).ready(function() {

		var zone = "05:30";  //Change this to your timezone
		
	$.ajax({
		url: '{!!URL::route("saveCalendarEvent")!!}',
        type: 'POST', // Send post data
        data: 'type=fetch',
        //dataType:'json',
        async: false,
        success: function(s){
        	json_events = s;
        },
        error: function(jqXHR, textStatus, errorThrown){
        	
        }
	});


	var currentMousePos = {
	    x: -1,
	    y: -1
	};
		jQuery(document).on("mousemove", function (event) {
        currentMousePos.x = event.pageX;
        currentMousePos.y = event.pageY;
    });

		/* initialize the external events
		-----------------------------------------------------------------*/

		$('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});
			
			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});


		/* initialize the calendar
		-----------------------------------------------------------------*/

		$('#calendar').fullCalendar({
			events: JSON.parse(json_events),
			//events: [{"id":"14","title":"New Event","start":"2015-01-24T16:00:00+04:00","allDay":false}],
			utc: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			editable: true,
			droppable: true, 
			slotDuration: '00:30:00',
			eventReceive: function(event){
				var title = event.title;
				var start = event.start.format("YYYY-MM-DD[T]HH:MM:SS");
				$.ajax({
		    		url: '{!!URL::route("saveCalendarEvent")!!}',
		    		data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone,
		    		type: 'POST',
		    		dataType: 'json',
		    		success: function(response){
		    			event.id = response.eventid;
		    			$('#calendar').fullCalendar('updateEvent',event);
		    		},
		    		error: function(e){
		    			console.log(e.responseText);

		    		}
		    	});
				$('#calendar').fullCalendar('updateEvent',event);
				console.log(event);
			},
			dayRender: function(event, element) {
			    element.bind('dblclick', function(event,element) {	
						var title = prompt('Event Title:', event.title, { buttons: { Ok: true, Cancel: false} });
				          if (title){
				          	console.log(event);
				              event.title = title;
				              var start = event.start.format("YYYY-MM-DD[T]HH:MM:SS");
				              $.ajax({
						    		url: '{!!URL::route("saveCalendarEvent")!!}',
						    		data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone,
						    		type: 'POST',
						    		dataType: 'json',
						    		success: function(response){	
						    		event.id = response.eventid;
		    						$('#calendar').fullCalendar('updateEvent',event);
						    		},
						    		error: function(e){
						    			alert('Error processing your request: '+e.responseText);
						    		}
						    	});
				          }
					
			    });
			},
			eventDrop: function(event, delta, revertFunc) {
		        var title = event.title;
		        var start = event.start.format();
		        var end = (event.end == null) ? start : event.end.format();
		        $.ajax({
					url: '{!!URL::route("saveCalendarEvent")!!}',
					data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id,
					type: 'POST',
					dataType: 'json',
					success: function(response){
						if(response.status != 'success')		    				
						revertFunc();
					},
					error: function(e){		    			
						revertFunc();
						alert('Error processing your request: '+e.responseText);
					}
				});
		    },
		    eventClick: function(event, jsEvent, view) {
		    	console.log(event.id);
		          var title = prompt('Event Title:', event.title, { buttons: { Ok: true, Cancel: false} });
		          if (title){
		              event.title = title;
		              console.log('type=changetitle&title='+title+'&eventid='+event.id);
		              $.ajax({
				    		url: '{!!URL::route("saveCalendarEvent")!!}',
				    		data: 'type=changetitle&title='+title+'&eventid='+event.id,
				    		type: 'POST',
				    		dataType: 'json',
				    		success: function(response){	
				    			if(response.status == 'success')			    			
		              				$('#calendar').fullCalendar('updateEvent',event);
				    		},
				    		error: function(e){
				    			alert('Error processing your request: '+e.responseText);
				    		}
				    	});
		          }
			},
			eventResize: function(event, delta, revertFunc) {
				console.log(event);
				var title = event.title;
				var end = event.end.format();
				var start = event.start.format();
		        update(title,start,end,event.id);
		    },
			eventDragStop: function (event, jsEvent, ui, view) {
			    if (isElemOverDiv()) {
			    	var con = confirm('Are you sure to delete this event permanently?');
			    	if(con == true) {
						$.ajax({
				    		url: '{!!URL::route("saveCalendarEvent")!!}',
				    		data: 'type=remove&eventid='+event.id,
				    		type: 'POST',
				    		dataType: 'json',
				    		success: function(response){
				    			console.log(response);
				    			if(response.status == 'success'){
				    				$('#calendar').fullCalendar('removeEvents');
            						getFreshEvents();
            					}
				    		},
				    		error: function(e){	
				    			alert('Error processing your request: '+e.responseText);
				    		}
			    		});
					}   
				}
			}
		});

	function getFreshEvents(){
		$.ajax({
			url: '{!!URL::route("saveCalendarEvent")!!}',
	        type: 'POST', // Send post data
	        data: 'type=fetch',
	        async: false,
	        success: function(s){
	        	freshevents = s;
	        }
		});
		$('#calendar').fullCalendar('addEventSource', JSON.parse(freshevents));
	}


	function isElemOverDiv() {
        var trashEl = $('#trash');

        var ofs = trashEl.offset();

        var x1 = ofs.left;
        var x2 = ofs.left + trashEl.outerWidth(true);
        var y1 = ofs.top;
        var y2 = ofs.top + trashEl.outerHeight(true);

        if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&
            currentMousePos.y >= y1 && currentMousePos.y <= y2) {
            return true;
        }
        return false;
    }

	});
	
	
	
	

</script>
@stop

