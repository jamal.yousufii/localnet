@extends('layouts.print_master')
@section('content')
<style type="text/css">
@media print{
  .no-print ,no-print *{
    display: none !important;
  }
}
</style>

<div class="container" >

  <div class="page-head">
   <center><h3><img src="../img/header2.png" height="150px" width="600px" style=" height: 90%; margin-bottom: 20px;"></h3></center>
      
       <!--        <div style="  text-align: center; height: 80px;">
                <h6>معاونیت مالی واداری</h6>
                <h6>ریاست تکنالوژی معلوماتی‌</h6>
                <h6>آمریت خدمات تکنالوژی معلوماتی‌</h6>
                <h6>مدیریت عمومی مشخصات ومعاینه</h6>
                <h6>فورم مشخصات وسایل تکنالوژی معلوماتی‌  </h6>

              </div> -->
            </div>
                   
             @foreach($data as $value)
               <h5 style="margin-top:-30px; float: right;" dir="rtl">{!! $value->name!!}</h4>
                <span style="float: right;">شماره </span>
                <span dir="rtl" style="float: right;">  &nbsp; &nbsp;({!! $value->moshakhasat_id!!})</span>
                <span style="float: left;" dir="rtl">تاریخ   &nbsp; &nbsp; {!!checkEmptyDate($value->date)!!}   &nbsp; &nbsp; </span>
               @endforeach

              <div class="content">
              
               <table class="table table-bordered table-responsive" id="datalist" style=" border-top :3px  groove gray;  border-right :3px  groove gray;  border-left :3px  groove gray;" >
                  <thead>
                    <tr style=" text-align: left;">
                      <th >شماره</th>
                      <th style="width: 100px;">نام جنس</th>
                      <th>توضیحات جنس</th>
                      <th> تعداد </th>
                      <th >ضمانت</th>
                    </tr>
                 </thead> 
                  <?php $counter=1;?>
                   @foreach($rows as $val)
                  <tr style=" text-align: left;">
                   <td>{!! $counter !!}</td>
                   <td>{!! $val->product_name!!}</td>
                   <td style="width: 400px; ">
                    <div class="text-justify">
                    {!! $val->product_description!!}
                      <!--<?php $data//= str_replace("-", "<br/> <li>", $val->product_description.'</li>');
                   // echo $data;

                     ?>-->
                    </div>
                  </td>
                   <td> {!! $val->number_device!!} </br>{!! $val->unit !!}</td>
                   <td> {!! $val->guarantee!!}</td>
                 </tr>
                   <?php $counter++; ?>
                    @endforeach

                </table>
                <div id="print_footer" dir="rtl">
                <p style="margin-top:-20px;"   >چون بازار وسایل تکنالو‌ژی معلوماتی به سرعت در حال تغیر میباشد بنآدرصورت عدم پیدایش جنس مذکوربعد از یک ماه ریاست تکنالوژی معلوماتی مسٔولیت ندارد.ومعاینه جنس تهیه شده فوق مسٔولیت مدیریت عمومی مشخصات ومعاینه وسایل تکنالوژی بوده وبعد تصدیق این مدیریت قابل تسلیمی میباشد. </p>

                <div class="row">
                  <div class="col-md-4" style="float: right;">
                    <h5> ترتیب کننده &nbsp;&nbsp;:{{$request->created_by}}</h5>
                    <h5 > 
                      @foreach($data as $value)

                      <?php  
                        $year= explode("-", checkEmptyDate($value->date)); 
                      
                       echo ' &nbsp; &nbsp;/ '.' &nbsp; &nbsp;/'.$year[2];
                      ?>
                     @endforeach
                   </h5>
                    <h5>امضاء</h5>
                  </div>

                

                 <div class="col-md-5" style="margin-left:150px; float: left; " >
                    <h5>تصدیق کننده&nbsp;&nbsp;:{{$request->approved_by}}</h5>
                    <h5 > 
                      @foreach($data as $value)

                      <?php  
                        $year= explode("-", checkEmptyDate($value->date)); 
                      
                       echo ' &nbsp; &nbsp;/ '.' &nbsp; &nbsp;/'.$year[2];
                      ?>
                     @endforeach
                   </h5>
                    <h5>امضاء</h5>
                  </div>
                </div>


      
                    <!--     <?php // echo "Today is " . checkEmptyDate(date("Y-m-d")) . "<br>"; ?> -->
                    <div class="row">
                      <div class="col-md-12 text-center">
                        <a onclick="javascript:window.print();" id="print-btn" class="btn btn-success btn-large no-print" >Print
           <i class="fa fa-print"></i></a>
                         <a href="{!!URL::route('search_page')!!}" class="no-print"> <input type="button" value="برگرد
                            " id="add_department" class="btn btn-danger no-print"/></a>
                        </div> 
                   </div>
                 </div>
                       
              </div>
           </div>
    @stop

@section('footer-scripts') 

@stop