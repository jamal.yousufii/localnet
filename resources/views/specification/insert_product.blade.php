@extends('layouts.master')
@section('content')

  
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3> اضافه نمودن مشخصات وسایل</h3></center>
    <ol class="breadcrumb">
     
      <li class="active"> اضافه نمودن مشخصات وسایل \     </li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed" action="{!!URL:: route('insert_data_p')!!}"  method="post" style="border-radius: 0px;" name="myform" enctype= 'multipart/form-data'>

   
          <div class="form-group">
            <label class="col-sm-2 control-label"> </label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">نام جنس </span> <input type="text" name="product_name" class="form-control" placeholder="نام جنس " required>
            </div>
            <label class="col-sm-2 control-label"> </label>
            <div class="col-sm-4">
            <span style="font-weight: bold;"> تاریخ</span> <input type="text" name="date" id="date"  class="datepicker_farsi form-control" placeholder="تاریخ "  required>

            </div> 
          </div>
          <div class="form-group">
           <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">انتخاب ریاست </span> <select name="department_id"  id="department_id" class="form-control" style="width: 100%" required>
                <option value="" >- -  انتخاب ریاست  - -</option>
                @foreach($department as $value)
                <option value="{!!$value->id!!}">{!!$value->name!!}</option>
               @endforeach
              </select>
            </div> 



           <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4" >
               <span style="font-weight: bold;"> شماره مشخصات</span><input type="text" name="moshakhasat_id" id="mask_tin" class="form-control" placeholder="شماره مشخصات" required >

            </div>
          </div>
           <div class="form-group">
           <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4" >
              <span style="font-weight: bold;"> واحد</span> <input type="text" name="unit" class="form-control" placeholder="واحد"  >

            </div>
           <label class="col-sm-2 control-label"></label>
             <div class="col-sm-4">
               <span style="font-weight: bold;"> ضمانت</span><input type="text" name="guarantee" class="form-control" placeholder="ضمانت"  >
              
            </div>
          </div>
          
          <div class="form-group">
          <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4" >
               <span style="font-weight: bold;"> تعداد جنس </span><input type="number" name="number_device" class="form-control" placeholder="تعداد جنس" required >

            </div>
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;"> انتخاب ف س ۹</span><select name="feceno_id"  id="feceno_id" class="form-control" required style="width: 100%" required>
                <option value="" >- -  انتخاب ف سی ۹  - -</option>
                  @foreach($feceno as $val)
                <option value="{!! $val->id !!}">{!! $val->number !!} -- {!! $val->dept_name!!}</option>
                 @endforeach
              </select>
            </div> 
          </div>
            <div class="form-group">
          
           
              <label class="col-sm-2 control-label"> </label>
             <div class="col-sm-10">
             <span style="font-weight: bold;"> توضیحات جنس</span>
             <textarea rows="7" class="form-control ckeditor" id="messageArea" name="product_description" placeholder=" توضیحات جنس"> </textarea>
            
             </div>
          </div>
      
     
          
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
             <input type="submit"  class="btn btn-success" value="ثبت شود"  onclick="validate();" name="myButton" id="myButton">
              <a href="{!!URL::route('recordsList')!!}"> <input type="button" value="لیست اطلاعات 
                  " id="add_department" class="btn btn-danger"/></a>
            </div>
          </div>
        </form><br><br><br><br><br>
  </div></div>
</div>
@stop

@section('footer-scripts') 
<script type="text/javascript">
    $(document).ready(function(){
      });
   
</script>
<script type="text/javascript">
  function validate(){

var  myButton= document.getElementById('myButton');
var  myButton= document.getElementById('myButton');

                        setTimeout (function(){
                         
                          document.getElementById("myButton").disabled = true; 
                        },0);
                      setTimeout (function(){
                           document.getElementById("myButton").disabled = false; 
                        },4000);
}


</script>
  <script>
   $("#feceno_id").select2();
   $("#department_id").select2();



   $("#mask_tin").mask("99-9999");

</script>
@stop