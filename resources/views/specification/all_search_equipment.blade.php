@extends('layouts.master')
@section('content')
<style type="text/css">
#myBtn {
    display: none;
    position: fixed; 
    bottom: 10px;
    right: 30px; 
    z-index: 99; 
    border: none;
    outline: none; 
    cursor: pointer; 
    padding: 15px; 
 
}
table,thead,tr,th{
  text-align: center;
}
</style>
<a onclick="topFunction()" id="myBtn">{!! HTML::image('/img/t.png', 'Logo', array('class' => 'normal-logo logo-white', 'width' => '45px')) !!}</a>
<script type="text/javascript">
  window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera 
    document.documentElement.scrollTop = 0; // For IE and Firefox
}
</script>
<div class="container" dir="rtl">
  <div class="page-head">
  <h3>معاونیت مالی واداری  بررسی تجهیزات</h3>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
      <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(2)").addClass("active");
      </script>
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <ul class="nav nav-tabs" id="ul_tabs">
        <li class="active"><a href="#tab1" data-toggle="tab" style="font-weight: bold;">جستجوی معلومات در باره وسایل تکنالو‌‌ژی</a></li>
        <li  ><a href="#tab2" data-toggle="tab"  style=" font-weight: bold;">پرینت به اساس وسایل مربوط به کارمند</a></li>

 
      </ul>
      <div class="tab-content">
        <div class="tab-pane active cont" id="tab1"> 
          <!-- DataTables for Contract Type -->
          <div class="">
            <div class="header">
            <br>          
            <h3 style="margin-top:20px;" align="center">  جستجوی معلومات در باره وسایل تکنالو‌‌ژی</h3><hr />
            </div>
            <div class="content">

     
      <form class="form-horizontal group-border-dashed" action="{!! URL:: route('export_data')!!}" 
       id="form1"  method="post" style="border-radius: 0px;" name="myform" enctype= 'multipart/form-data'>

   
       <div class="form-group">
           <label class="col-sm-2 control-label">  </label>
            <div class="col-sm-4">
             <span style="font-weight: bold;">انتخاب نوع جنس </span><select name="categories_id"  id="categories" class="form-control" required style="width: 100%">
                <option value="0" >- -  انتخاب نوع جنس  - -</option>
                  @foreach($categories as $val)
                <option value="{!! $val->id !!}">{!! $val->name !!}</option>
                 @endforeach
              </select>
            </div> 
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">انتخاب ریاست</span><select name="department_id"  id="department_id" class="form-control" style="width: 100%">
                <option value="0" >- -  انتخاب ریاست  - -</option>
                @foreach($department as $value)
                <option value="{!!$value->id!!}">{!!$value->name!!}</option>
               @endforeach
              </select>
            </div> 
          </div>
          <div class="form-group">
          <label class="col-sm-2 control-label"></label>
             <div class="col-sm-4">
              <span style="font-weight: bold;">انتخاب کارمند</span><select name="employes_id"  id="employes_id" class="form-control" required style="width: 100%">
                <option value="0" >- - اسم  &nbsp;&nbsp; &nbsp;&nbsp;--نام پدر - -</option>
                  @foreach($employee as $val)
                <option value="{!! $val->id !!}" >{!! $val->name !!}</option>
                 @endforeach
              </select>
            </div> 
            
          </div>
          <br>
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <button value="Search Data" id="searchData" class="btn btn-success"> جستجوی اطلاعات </button>
              <a href="{!!URL::route('load_equipment')!!}"> <input type="button" value="لیست اطلاعات 
                  " id="add_department" class="btn btn-danger"/></a>
                 
                   <button type="submit" class="btn btn-primary" onClick="return confirm('Do want to export file ???')">
                     <span class=" fa-cloud-download"></span> Excel
                        </button>
            </div>
          </div>
        </form>
          <div>
               <table class="table table-bordered table-responsive" id="datalist" >
                  <thead>
                    <hr>
                       <tr>
                      <th>شماره #</th>
                      <th>نام </th>
                      <th>نام پدر</th>
                      <th>ریاست مربوطه</th>
                      <th>نوع جنس</th>
                      <th>مودل</th>
                      <th>شماره سریال</th>
                      <th>توضیحات</th>
                      <th colspan="2">عملیات</th>
                      </tr>                 
                  </thead>
                  <tbody id="search_result">
                  
                  </tbody>
                </table>
                
              </div>

            </div>
          </div>
        </div>
        <!-- secand tap of print  -->
        <div class="tab-pane cont" id="tab2"> 
          <!--Add New Department -->
          
          <div class="col col-12">

            <div class="header">

            <br>
                <a href="{!!URL::route('insert_pro')!!}" style=" float: left; font-weight: bold;" class="btn btn-success">+ اضافه نمودن جنس</a>
             <div class="col-md-6">
             </div><br><br>
               <h3 style="margin-top:20px;text-align:center">جستجوی معلومات در باره وسایل تکنالو‌‌ژی </h3><hr />
               
          <form class="form-horizontal group-border-dashed" action="{!! URL::route('print_em_pro')!!}" id="form2"  method="post" style="border-radius: 0px; " name="myform" enctype= 'multipart/form-data'>

            <div class="row">
            <center>
              <div class="col-md-12" >
                 <span style="font-weight: bold; margin-left: 277px;">انتخاب کارمند</span>
                 <select name="employes_id"  id="employes_id2" class="form-control"  style="width: 30%" required>
                    <option value="0" >- - اسم  &nbsp;&nbsp; &nbsp;&nbsp;--نام پدر - -</option>
                       @foreach($employee as $val)
                        <option value="{!! $val->id !!}" >{!! $val->name !!}</option>
                      @endforeach
                 </select>
                       <br>  
          {!!Form::token()!!}
          
              <button value="Search Data2" id="searchData2" class="btn btn-success"> جستجوی اطلاعات </button>
                <a href="{!!URL::route('load_equipment')!!}"> 
              <input type="button" value="لیست اطلاعات " id="add_department" class="btn btn-danger"/>
                </a>
              <button type="submit" class="btn btn-primary" >
                  <span class="fa-print" > </span> print 
             </button>

            </div></center>
          </div>
        </form>
           </div>
             <hr />
        <div class="content">
           <table class="table table-bordered table-responsive" id="datalist2" >
                  <thead>
                    <hr>
                       <tr>
                      <th>شماره #</th>
                      <th>نام </th>
                      <th>نام پدر</th>
                      <th>ریاست مربوطه</th>
                      <th>نوع جنس</th>
                      <th>مودل</th>
                      <th>شماره سریال</th>
                      <th>توضیحات</th>
                      <th colspan="2">عملیات</th>
                      </tr>                 
                  </thead>
                  <tbody id="search_result2">
                  
                  </tbody>
                </table>
            </div><div class="row">
            
          </div>
          </div>
        

        </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('footer-scripts') 
<script type="text/javascript">

  $(document).ready(function() {
    $('#searchData').click(function(){
      var dataString = $('#form1').serialize();
       $.ajax({
            type:'POST',
            url:'{!!URL::route("get_eq_search_data")!!}',
            data: dataString,
            success:function(response){
               $('#search_result').html(response);            }
           })

          return false;
         });
  });
    

</script> 

<script type="text/javascript">

  $(document).ready(function() {
    $('#searchData2').click(function(){
      var dataString = $('#form2').serialize();
       $.ajax({
            type:'POST',
            url:'{!!URL::route("get_eq_search_data_pr")!!}',
            data: dataString,
            success:function(response){
               $('#search_result2').html(response);            }
           })

          return false;
         });
  });
    

</script> 

 <script>
   $("#employes_id").select2();
   $("#employes_id2").select2();
   $("#department_id").select2();
    $("#categories").select2();
    $("#equipment").select2();


  </script>
<script type="text/javascript">
$(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
        $('#ul_tabs a[href="' + activeTab + '"]').tab('show');
    }
});
</script>
@stop