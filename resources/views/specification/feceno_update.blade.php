@extends('layouts.master')
@section('content')
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>اضافه نمودن ف س ۹</h3></center>
    <ol class="breadcrumb">
     
      <li class="active">اضافه نمودن ف س ۹  \     </li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed " action="{!!URL:: route('update_row',$data->id)!!}"  method="post" style="border-radius: 0px;" name="myform" enctype= 'multipart/form-data'>

   
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">شماره ف س ۹  </span> <input type="number" name="number" value="{!!$data->number!!}" class="form-control" placeholder="شماره ف س ۹ " >
            </div>
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
             <span style="font-weight: bold;">تاریخ  </span><input type="text" name="date" id="date"   value="{!!jalali_format(toJalali($data->date))!!}" class="datepicker_farsi form-control" placeholder="تاریخ "  >

            </div> 
          </div>
          <div class="form-group">
           <label class="col-sm-2 control-label">   </label>
            <div class="col-sm-4" >
               <span style="font-weight: bold;">فایل از ف س ۹ </span><input type="file" name="file" class="form-control"  placeholder=" فایل از ف س ۹  "  >

            </div>
           <label class="col-sm-2 control-label"></label>
             <div class="col-sm-4">
             <span style="font-weight: bold;"> توضیحات</span><textarea class="form-control"  name="description" placeholder=" توضیحات"> {!! $data->description !!}</textarea>
              
            </div>
          </div>
         <div class="form-group">
             <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">انتخاب ریاست </span> <select name="department_id"  id="department_id" class="form-control" style="width: 100%">
                <option value="" >- -  انتخاب ریاست  - -</option>
                @foreach($department as $value)
                <option value="{!!$value->id!!}"<?php if($data->department_id==$value->id) echo "selected"?>>{!!$value->name!!}</option>
               @endforeach
              </select>
            </div> 

             <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">حالت ف س ۹</span> 
              <select name="fe_status"  id="sta" class="form-control" style="width: 100%" required>
                <option value="" >- -  حالت ف س ۹  - -</option>
                <option value="0" <?php if($data->fe_status ==0) echo "selected";?>>قبول</option>
                <option value="1" <?php if($data->fe_status ==1) echo "selected";?>>رد</option>
              </select>
              
            </div> 

          </div>
          <br>
     
          
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
             <input type="submit"  class="btn btn-success" value="ثبت شود">
              <a href="{!!URL::route('recordsList')!!}"> <input type="button" value="لیست اطلاعات 
                  " id="add_department" class="btn btn-danger"/></a>
            </div>
          </div>
        </form><br><br><br><br><br>
  </div>
</div>
@stop

@section('footer-scripts') 
<script type="text/javascript">
       $("#department_id").select2();

    $(document).ready(function(){
   

        });
   
</script>

@stop