@extends('layouts.master')
@section('content')

  
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3> تغیرات آوردن در معلومات
</h3></center>
    <ol class="breadcrumb">
     
      <li class="active">تغیرات آوردن در معلومات
 \     </li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed" action="{!!URL::route('update_destribut',$employes_equipment->id)!!}"  method="post" style="border-radius: 0px;" name="myform">

        <!-- <div class="form-group">
            <label class="col-sm-2 control-label">انتخاب کارمند</label>
            <div class="col-sm-4">
             <select name="employes_id"  id="employes_id" class="form-control" required style="width: 100%">
                  @foreach($employee as $val)
                <option value="{!! $val->id !!}"  <?php //if($val->id == $employes_equipment->employes_id) echo "selected";  ?>> {!! $val->name !!}</option>
                 @endforeach
              </select>
            </div> 
            <label class="col-sm-2 control-label">انتخاب جنس </label>
            <div class="col-sm-4">
              <select name="equipment_id"  id="equipment_id" class="form-control" required style="width: 100%"  >
                <option value="" >- - انتخاب جنس  - -</option>
                  @foreach($equipment as $val)
                <option value="{!! $val->id !!}"   <?php //if($val->id == $employes_equipment->equipment_id) echo "selected";  ?>>  {!! $val->name !!}&nbsp;&nbsp;-{!! $val->model !!} &nbsp;&nbsp;- {!! $val->serial_number !!} </option>
                 @endforeach
              </select>
            </div> 
          </div>-->
               <div class="form-group">
             <label class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                 <span style="font-weight: bold;"> انتخاب کارمند</span>  <select name="employes_id" id="employes_id" class="form-control" required style="width: 100%; "  >
                  @foreach($employee as $val)
                <option value="{!! $val->id !!}"  <?php if($val->id == $employes_equipment->employes_id) echo "selected";  ?>> {!! $val->name !!}</option>
                 @endforeach
              </select>
            </div> 
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
            <span style="font-weight: bold;">قید نمودن یا آزادساختن جنس </span> <select name="status"  id="status" class="form-control" required style="width: 100%">
                <option value="" >- -  قید نمودن یا آزادساختن جنس  - -</option>
                
                <option value="1" <?php if($employes_equipment->status == '1') echo "selected";  ?>>آزادشودجنس</option>
                <option value="0"  <?php if($employes_equipment->status == '0') echo "selected";  ?>>قید شودجنس</option>

              </select>
            </div> 
          </div>
          <br>
     
          
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
             <input type="submit"  class="btn btn-success" value="ثبت شود"  onclick="validate();" name="myButton" id="myButton">
              <a href="{!!URL::route('load_equipment')!!}"> <input type="button" value="لیست اطلاعات 
                  " id="add_department" class="btn btn-danger"/></a>
            </div>
          </div>
        </form><br><br><br><br><br>
  </div></div>
</div>
@stop

@section('footer-scripts') 
<script type="text/javascript">
    $(document).ready(function(){
      });
   
</script>
<script type="text/javascript">
  function validate(){

var  myButton= document.getElementById('myButton');
var  myButton= document.getElementById('myButton');

                        setTimeout (function(){
                         
                          document.getElementById("myButton").disabled = true; 
                        },0);
                      setTimeout (function(){
                           document.getElementById("myButton").disabled = false; 
                        },4000);
}


</script>
  <script>
   $("#employes_id").select2();
   $("#equipment_id").select2();


</script>
@stop