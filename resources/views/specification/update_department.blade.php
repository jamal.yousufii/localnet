@extends('layouts.master')
@section('content')
<div class="container" dir="rtl">
  <div class="page-head">
    <h3>لیست ریاست های مربوطه</h3>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
      <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(2)").addClass("active");
      </script>
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <br><br><br>
       <form class="form-horizontal group-border-dashed" action="{!!URL::route('update_department',$row->id )!!}" id="form2"  method="post" style="border-radius: 0px;">

   
          <div class="form-group">
            <label class="col-sm-2 control-label">نام ریاست یا اداره  </label>
            <div class="col-sm-4">
              <input type="text" name="name" value="{!!$row->name !!}" class="form-control" >
            </div>
            <label class="col-sm-2 control-label">موقیعت ریاست یا اداره  </label>
            <div class="col-sm-4" >
              <input type="text" name="location" value="{!!$row-> location !!}" class="form-control" >  
          </div>
          
        
            </div>
          <br>
           {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <input type="submit" name="" class="btn btn-success" value="تغیر شود">
              <a href="{!!URL::route('department')!!}"> <input type="button" value="لیست  اطلاعات 
                  " id="add_department" class="btn btn-danger"/></a>
            </div>
          </div>
        </form><br><br><br><br><br>
  </div>
</div>
@stop

@section('footer-scripts') 
<script type="text/javascript">
    $(document).ready(function(){
   

        });
   
</script>

@stop