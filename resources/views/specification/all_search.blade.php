@extends('layouts.master')
@section('content')
<style type="text/css">
#myBtn {
    display: none;
    position: fixed; 
    bottom: 10px;
    right: 30px; 
    z-index: 99; 
    border: none;
    outline: none; 
    cursor: pointer; 
    padding: 15px; 
 
}
td:nth-child(2) {
    padding-right: 20px;
}​

</style>
  <style type="text/css">
            /* tab color */

          /* active tab color */
          #ul_tabs>li.active>a {
            color: #fff;
            background-color: #002233;
            border: 1px solid #888888;
          }

          /* hover tab color */
          #ul_tabs>li>a:hover {
            border-color: #000000;
            background-color: #004d4d;
          }
          </style>

<a onclick="topFunction()" id="myBtn">{!! HTML::image('/img/t.png', 'Logo', array('class' => 'normal-logo logo-white', 'width' => '45px')) !!}</a>
<script type="text/javascript">
  window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera 
    document.documentElement.scrollTop = 0; // For IE and Firefox
}
</script>
<div class="container" dir="rtl">
  <div class="page-head">
    <h3>مدیریت عمومی مشخصات ومعاینه
</h3>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
      <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(2)").addClass("active");
      </script>
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <ul class="nav nav-tabs" id="ul_tabs">
        <li class="active"><a href="#tab1" data-toggle="tab"  style=" font-weight: bold;">جستجوی ف سی ۹ </a></li>
        <li  ><a href="#tab2" data-toggle="tab"  style=" font-weight: bold;">جستجوی  مشخصات وسایل تکنالوژی معلوماتی</a></li>
        <li  ><a href="#tab4" data-toggle="tab"  style=" font-weight: bold;">گزارش مشخصات</a></li>
        <li><a href="#tab3" data-toggle="tab"  style=" font-weight: bold;">پرینت</a></li>
       
 
      </ul>
      <div class="tab-content">
        <div class="tab-pane active cont" id="tab1"> 
          <!-- DataTables for Contract Type -->
          <div class="">
            <div class="header">
            <br>
               <a href="{!!URL::route('insertForm')!!}" style=" float: left;font-weight: bold;" class="btn btn-success">+ اضافه کردن ف س ۹ جدید</a><br><br>
          
            <h3 style="margin-top:20px;" align="center">   جستجوی فی سی ۹</h3><hr />
            </div>
      <div class="content">
         <form class="form-horizontal group-border-dashed" id="form1" action="{{URL::route('feceno_excel')}}"  method="post" style="border-radius: 0px;" name="myform" enctype= 'multipart/form-data'>
          <div class="form-group">
            <label class="col-sm-2 control-label"> </label>
              <div class="col-sm-4">
                 <span style="font-weight: bold;"> شماره ف س ۹</span><input type="number" name="number" class="form-control" placeholder="شماره ف س ۹ "  >
             </div>
               <label class="col-sm-2 control-label"> </label>
            <div class="col-sm-4">
              <span style="font-weight: bold;"> تاریخ</span><input type="text" name="date" id="date"  class="datepicker_farsi form-control" placeholder="تاریخ "   >
            </div> 
          </div>
            <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4" >
              <span style="font-weight: bold;"> انتخاب ریاست</span>
              <select name="department_id" id="department_id" class="form-control" style="width: 100%;">
                <option value="" >- -  انتخاب ریاست  - -</option>
                @foreach($department as $value)
                <option value="{!!$value->id!!}">{!!$value->name!!}</option>
               @endforeach
              </select>
            </div>
           <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">حالت ف س ۹</span> 
              <select name="fe_status"  id="sta" class="form-control" style="width: 100%" >
                <option value="" >- -  حالت ف س ۹  - -</option>
                <option value="0">قبول</option>
                <option value="1">رد</option>
              </select>
              
            </div> 
            </div> 
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <button value="Search Data" id="searchData" class="btn btn-success" style="padding: 6px 8px;"> جستجوی اطلاعات </button>
              <a href="{!!URL::route('recordsList')!!}"> 
                <input type="button" value="لیست  اطلاعات " id="add_department" class="btn btn-danger" style="padding: 6px 8px;"/>
              </a>
               <button type="submit" class="btn btn-primary" onClick="return confirm('Do want to export file ???')" style="padding: 6px 8px;">
                     <span class=" fa-cloud-download" ></span> Excel
              </button>
              <button  type="reset" value=" " id="add_department" class="btn btn-info" style="padding: 6px 8px;"><i class="fa fa-eraser fa-lg"></i>&nbsp;پاک شود</button>
            </div>
          </div> 
        </form>
          <div>
               <table class="table table-bordered table-responsive" id="datalist" >
                  <thead>
                    <hr>
                    <tr>
                      <th>شماره #</th>
                      <th>شماره ف س ۹</th>
                      <th>تاریخ</th>
                      <th> فایل فی سی ۹</th>
                      <th> ریاست مربوطه </th>
                      <th>حالت ف س ۹</th>
                      <th>توضیحات</th>
                      <th colspan="2">عملیات</th>
                      </tr>                
                  </thead>
                  <tbody id="search_result">
                  </tbody>
                </table>
                
                
              </div>

            </div>
          </div>
        </div>
        <div class="tab-pane cont" id="tab2"> 
          <!--Add New Department -->
          
          <div class="col col-12">

            <div class="header">

            <br>
                <a href="{!!URL::route('insert_pro')!!}" style=" float: left; font-weight: bold;" class="btn btn-success">+ اضافه نمودن جنس</a>
             <div class="col-md-6">
             </div><br><br>
               <h3 style="margin-top:20px;text-align:center">جستجوی  مشخصات وسایل تکنالوژی معلوماتی </h3><hr />
               
          <form class="form-horizontal group-border-dashed" action="{!! URL::route('export_excel')!!}" id="form2"  method="post" style="border-radius: 0px;" name="myform" enctype= 'multipart/form-data'>

   
          <div class="form-group">
            <label class="col-sm-2 control-label"> </label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">نام جنس </span><input type="text" name="product_name" class="form-control" placeholder="نام جنس " >
            </div>
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
            <span style="font-weight: bold;"> تاریخ </span><input type="text" name="date" id="date"  class="datepicker_farsi form-control" placeholder="تاریخ "  >

            </div> 
          </div>
            <div class="form-group">
          
           <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4" >
              <span style="font-weight: bold;">شماره مشخصات </span><input type="text" name="moshakhasat_id" class="form-control" placeholder="شماره مشخصات"  >

            </div>
              <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4" >
              <span style="font-weight: bold;"> انتخاب ریاست</span><select name="department_id" id="department_id_report" class="form-control" style="width: 100%;">
                <option value=" " >- -  انتخاب ریاست  - -</option>
                @foreach($department as $value)
                <option value="{!!$value->id!!}">{!!$value->name!!}</option>
               @endforeach
              </select>
            </div> 
          </div>
        
            <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">

              <span style="font-weight: bold;"> انتخاب فی سی ۹</span><select name="feceno_id" id="feceno_id" class="form-control" style="width: 100%;">
                <option value=" " >- -  انتخاب ف سی ۹  - -</option>
                  @foreach($feceno as $val)
                <option value="{!! $val->id !!}">{!! $val->number !!}</option>
                 @endforeach
              </select>
            </div> 
          <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
             <span style="font-weight: bold;"> معاینه جنس </span> <select name="status" class="form-control select2me" >
                <option value="" >- -  معاینه   - -</option>
                <option value="1">قبول</option>
                 <option value="2">رد</option>
                 <option value="0">معلوم نیست</option>
               
              </select>
            </div> 
          </div>
          <br>
     
          
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <button value="Search Data2" id="searchData2" class="btn btn-success" style="padding: 6px 8px;"> جستجوی اطلاعات </button>
                <a href="{!!URL::route('recordsList')!!}"> 
                  <input type="button" value="لیست اطلاعات " id="add_department" class="btn btn-danger" style="padding: 6px 8px;"/>
                </a>
              <button type="submit" class="btn btn-primary" onClick="return confirm('Do want to export file ???')" style="padding: 6px 8px;">
                     <span class=" fa-cloud-download" ></span> Excel
              </button>
              <button  type="reset" value=" " id="add_department" class="btn btn-info" style="padding: 6px 8px;"><i class="fa fa-eraser fa-lg"></i>&nbsp;پاک شود</button>


            </div>
          </div>
        </form>
            </div>
              <hr />
        <div class="content">
            <table class="table table-bordered table-responsive" id="showdata">
                  <thead>
                       <tr>
                      <th>شماره #</th>
                      <th>شماره ف س  ۹</th>
                      <th>شماره مشخصات</th>
                      <th>فایل ف س ۹  </th>
                      <th>نام جنس</th>
                      <th> توضیحات جنس</th>
                      <th>تاریخ</th>
                      <th>تعداد جنس</th>
                      <th>واحد</th>
                      <th>ضمانت</th>
                       <th>معاینه</th>
                      <th>  ریاست مربوطه</th>
                      <!-- <th colspan="2">عملیات</th> -->
                      </tr> 
                   
            
                    <tbody id="search_result2">
                    
                      
                    </tbody>
                
                   
                  </thead>
                  
                </table>
             </div><div class="row">
            
          </div>
          </div>
        

        </div>
          <!-- third tab-->
          
        <div class="tab-pane cont" id="tab3"> 
          <div class="col col-12">
            <div class="header">
             <br>
               
             </div>
             <h3 align="center">پرینت به اساس شماره مشخصات</h3>
           <hr>
            </div>

           <form class="form-horizontal group-border-dashed"  action="{!!URL::route('print_search')!!}" id="form2"  method="post" style="border-radius: 0px;" name="myform" enctype= 'multipart/form-data'>
            <center>

            <table class="center" style="width: 400px">
              <tr>

                <td  colspan="2">
                  <select name=" moshakhasat_id" id="moshakhasat_id" class="form-control" style="width: 100%;" required>
                    <option value="" >- -  شماره مشخصات  - -</option>
                      @foreach($moshakhasat_id as $val)
                    <option value="{!! $val->moshakhasat_id !!}">{!! $val->moshakhasat_id !!}</option>
                     @endforeach
                 </select>
                </td>
                
              </tr>
            
                <tr> 
                <td> <br><input type=""  name="created_by" placeholder="ترتیب کننده  " class="form-control"></td>
                <td> <br><input type="" name="approved_by" placeholder="تصدیق کننده" class="form-control"></td>
              </tr>
              <tr>
                <td><br><br>     

                  <input type="submit" name="" value=" جستجوی اطلاعات " class="btn btn-success"> 

                </td>
                <td><br><br>

                    <a href="{!!URL::route('recordsList')!!}"> <input type="button" value="لیست اطلاعات " id="add_department" class="btn btn-danger"/></a>

                </td>
              </tr>

            </table>

            </center>

   
       
          {!!Form::token()!!}
        
        </form>

              <hr />
          
          </div>
           
        <div class="tab-pane cont" id="tab3"> 
          <div class="col col-12">
            <div class="header">
             <br>
               
             </div><br><br>
             <h3 align="center">پرینت به اساس شماره مشخصات</h3>
           <hr>
            </div>
           <form class="form-horizontal group-border-dashed"  action="{!!URL::route('print_search')!!}" id="form2"  method="post" style="border-radius: 0px;" name="myform" enctype= 'multipart/form-data'>

   
          <div class="form-group">
           
              <label class="col-sm-2 control-label"></label>
           
            <div class="col-sm-4">

              <span style="font-weight: bold;"> شماره مشخصات</span><select name=" moshakhasat_id" id="moshakhasat_id" class="form-control" style="width: 100%;" required>
                <option value="" >- -  شماره مشخصات  - -</option>
                  @foreach($moshakhasat_id as $val)
                <option value="{!! $val->moshakhasat_id !!}">{!! $val->moshakhasat_id !!}</option>
                 @endforeach
              </select>
            </div> 
          </div>
          <br>    
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
            <input type="submit" name="" value=" جستجوی اطلاعات " class="btn btn-success"> 
            <a href="{!!URL::route('recordsList')!!}"> <input type="button" value="لیست اطلاعات " id="add_department" class="btn btn-danger"/></a>
            </div>
          </div>
        </form><br><br>
              <hr />
            <div class="content">
           
                </table>

            </div>
          </div>
        <!-- fourth tab  -->
 <div class="tab-pane cont" id="tab4"> 
          <!--Add New Department -->
          
          <div class="col col-12">
<br>
            <div class="header">
            
               <h3 style="margin-top:20px;text-align:center">گزارش مشخصات </h3><hr />
               
          <form class="form-horizontal group-border-dashed" action="{!! URL::route('export_report')!!}" id="form4"  method="post" style="border-radius: 0px;" name="myform" enctype= 'multipart/form-data' >
            <table align="center">
              <tr>
                <td >
               <input type="text" name="from_date" id="from_date" class="datepicker_farsi form-control" placeholder="از تاریخ " >
                
                </td>

                <td>
               <input type="text" name="to_date" id="to_date"  class="datepicker_farsi form-control" placeholder="الی تاریخ "  >
                </td>
              </tr>
              <tr>

              <td><br>
                <span style="font-weight: bold;"></span> 
             <select name="status" class="form-control select2me" required>
                <option value=" " >- -  تمام اطلا عات  - -</option>
                <option value="1">قبول</option>
                 <option value="2">رد</option>
                 <option value="0"> نا معلوم</option> 
               
              </select>
                </td>

                <td style="float: right;"><br>                  
                  <span style="font-weight: bold;"> </span>  <input type="checkbox" id="check_data" class="check_data" name="all_data" value="1"> تمام اطلاعات<br>

                </td> 
              </tr>
                 <tr>

              <td><br>
                <span style="font-weight: bold;"></span> 

                <select name="department_id" id="department_id_re" class="form-control" style="width: 220px;">
                    <option value="" >- -  انتخاب ریاست  - -</option>
                    @foreach($department as $value)
                    <option value="{!!$value->id!!}">{!!$value->name!!}</option>
                   @endforeach
                </select>
                </td>

                <td style="float: right;">   <br>             
                  <span style="font-weight: bold;"> </span> 
                   <input type="text" name="product_name"  class=" form-control" placeholder="نام جنس" >

                </td> 
              </tr>
                 {!!Form::token()!!}

              <tr>
                <td><br>

                   <button value="Search Data2" id="search_spicefic_data" class="btn btn-success"> جستجوی اطلاعات </button>
                      
                   <button type="submit" class="btn btn-primary" onClick="return confirm('Do want to export file ???')">
                   <span class=" fa-cloud-download" ></span> Excel
                   </button>

                </td>
                <td style="float: right;"><br>

                  <button  type="reset" value=" " id="reset_botton" class="btn btn-info"><i class="fa fa-eraser fa-lg"></i>&nbsp;پاک شود</button>

                  <a href="{!!URL::route('recordsList')!!}"> 
                         <input type="button" value="لیست اطلاعات " id="add_department" class="btn btn-danger"/>
                     </a>
               
                    
                </td>

              </tr>
          
            </table>
             
        </form>
            </div>
              <hr />
        <div class="content">
            <table class="table table-bordered table-responsive" id="showdata">
                  <thead>
                       <tr>
                      <th>شماره #</th>
                      <th>شماره ف س  ۹</th>
                      <th>شماره مشخصات</th>
                      <th>فایل ف س ۹  </th>
                      <th>نام جنس</th>
                      <th> توضیحات جنس</th>
                      <th>تاریخ تقاضا</th>
                      <th>تعداد جنس</th>
                      <th>واحد</th>
                      <th>ضمانت</th>
                       <th>معاینه</th>
                      <th>  ریاست مربوطه</th>
                      <!-- <th colspan="2">عملیات</th> -->
                      </tr> 
                   
            
                    <tbody id="search_result4">
                    
                      
                    </tbody>
                
                   
                  </thead>
                  
                </table>
             </div><div class="row">
            
          </div>
          </div>
        

        </div>
       

        </div>  
       </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('footer-scripts') 


<script type="text/javascript">

  $(document).ready(function() {

    $('#searchData').click(function(){
      var dataString = $('#form1').serialize();
       $.ajax({
            type:'POST',
            url:'{!!URL::route("getdata")!!}',
            data: dataString,
            success:function(response){
               $('#search_result').html(response);            }
           })

          return false;
         });
  });
    

</script>
 <script type="text/javascript">

  $(document).ready(function() {

    $('#reset_botton').click(function(){

        $("#department_id_re").val('').trigger('change')

  });
   }); 

</script> 

<script type="text/javascript">

  $(document).ready(function() {
    $('#searchData2').click(function(){
      var dataString = $('#form2').serialize();
       $.ajax({
            type:'POST',
            url:'{!!URL::route("getdata2")!!} ',
            data: dataString,
            success:function(response){
               $('#search_result2').html(response);         
                  }
           })

          return false;
         });
  });
    

</script> 
<script type="text/javascript">

  $(document).ready(function() {
    $('#searchData3').click(function(){
      var dataString = $('#form3').serialize();
       $.ajax({
            type:'POST',
            url:' ',
            data: dataString,
            success:function(response){
               $('#search_result3').html(response);            }
           })

          return false;
         });
  });
    

</script> 
<!-- spicefic export excel search -->
<script type="text/javascript">

        $(document).ready(function() {

          $('#form4').change('input', function() {

          var field_value = $('#from_date').val();
          var field_value2 = $('#to_date').val();
       
         if(field_value || field_value2 )
            $('.check_data').prop('disabled', true);
         else
            $('.check_data').prop('disabled', false);
      });

    $('#search_spicefic_data').click(function(){
      var dataString = $('#form4').serialize();
       $.ajax({
            type:'POST',
            url:' {!!URL :: route("get_report")!!}',
            data: dataString,
              beforeSend: function(){
                  $('#search_result4').html('<center ><span>{!!HTML::image("/img/ajax-loader.gif")!!}</span></center>');
              },
            success:function(response){
               $('#search_result4').html(response);            }
           })

          return false;
         });
  });
    

</script> 

<script type="text/javascript">
$(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
        $('#ul_tabs a[href="' + activeTab + '"]').tab('show');
    }
});
</script>
 <script>
   $("#feceno_id").select2();
   $("#department_id").select2();
   $("#department_id_report").select2();
    $("#moshakhasat_id").select2();
    $("#department_id_re").select2();


  </script>

@stop