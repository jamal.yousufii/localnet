@extends('layouts.print_master')
@section('content')
<style type="text/css">
@media print{
  .no-print ,no-print *{
    display: none !important;
  }
}
</style>
<style type="text/css">
  
  table thead tr th {
    font-size:5px;
  }

</style>
<div class="container" dir="rtl">

  <div class="page-head">
   <center><h3><img src="../img/header.png" height="150px" width="600px" style=" margin-top:-30px;"></h3></center>
      
              <div style="  text-align: center; height: 80px;">
                <h4>معاونیت مالی واداری</h4>
                <h4>ریاست تکنالوژی معلوماتی‌</h4>
                <h4>لیست اجناس مربوط به کارمند</h4>

                  </div>
                   </div>
                     <br> 
            
                  <h5 style="margin-top:-30px;"></h4><br>
                  @foreach($data2 as $val)
              <table >
                  <tr>
                     <td style="float: right;">نام:</td>
                     <td style="float: left;"><span dir="rtl">   {!! $val-> em_name!!}</span></td>
                 </tr>
                 <tr>
                    <td style="float: right;">  نام پدر:</td>
                    <td style="float: left;">  <span dir="rtl">  {!!$val->last_name!!}</span></td>
                 </tr>
                 <tr>
                    <td style="float: right;">ریاست:</td>
                     <td style="float: left;"><span dir="rtl">  {!! $val->dep_name!!}</span></td>
                 </tr>
                @endforeach
             </table>
                   <span style="float: left;" dir="rtl">تاریخ   &nbsp; &nbsp;    &nbsp; &nbsp;<?php echo "" . checkEmptyDate(date("Y-m-d")) . "<br>"; ?> </span>

              <div class="content">
              
               <table class="table table-bordered table-responsive" id="datalist" style=" border-top :3px  groove gray;  border-right :3px  groove gray;  border-left :3px  groove gray;" >
                  <thead>
                   <tr>
                      <th>شماره #</th>
                      <th>نوع جنس</th>
                      <th>مودل</th>
                      <th>شماره سریال</th>
                      <th>توضیحات</th>
                      </tr>     
                 </thead> 
                   @if(!empty($data))
                  <?php $counter = 1; ?>
                 @foreach($data as $val)
                 <tr>
                   <td>{!! $counter !!}</td>
                  
                     <td>{!! $val->name !!}</td>
                     <td>{!! $val->model !!}</td>
                     <td>{!! $val->serial_number !!}</td>
                     <td>{!! $val->description !!}</td>
                     <!--<td><a href="deleted_destribut_eq/{{$val-> em_eq_id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>--> 
                     </tr>
                  <?php $counter++ ; ?>
                   @endforeach   
                    @else
                  <tr><td style="color:red ; text-align: center;" colspan="12"> موردی پیدا نشد !</td></tr>
                    @endif
                </table>
                <div id="print_footer">
                <p style="margin-top:-20px;"   > </p>

                <table style="width: 100%;margin-top: 100px;">
                 
                  <tr>
                 
                  <td>
                  امضاء:(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
                  </td>
                  </tr>
                
               </table>
                    <!--     <?php // echo "Today is " . checkEmptyDate(date("Y-m-d")) . "<br>"; ?> -->
                    <div class="row">
                      <div class="col-md-12 text-center">
                        <br>
                        <a onclick="javascript:window.print();" id="print-btn"  class="btn btn-success btn-large no-print" >Print
                        <i class="fa fa-print"></i></a>
                         <a href="{!!URL::route('search_form')!!}" class="no-print"> <input type="button" value="برگرد
                            " id="add_department" class="btn btn-danger no-print"/></a>
                        </div> 
                        </div>
                        </div>
                       
                        </div>
                        </div>
    @stop

@section('footer-scripts') 

@stop