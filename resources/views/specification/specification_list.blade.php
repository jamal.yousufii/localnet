@extends('layouts.master')
@section('content')
<style type="text/css">
   table,tr, td {
   text-align: start;
   ;
   }
   table,thead,tr,th{
   text-align: center;
   }
   tr:nth-child(even) {background-color: #f2f2f2}
   tr:hover {background-color: #ffffff}
   Try it Yourself »
</style>
<style type="text/css">
   #myBtn {
   display: none;
   position: fixed;
   bottom: 10px;
   right: 30px;
   z-index: 99;
   border: none;
   outline: none;
   cursor: pointer;
   padding: 15px;
   }
</style>
<style type="text/css">
   /* tab color */
   /* active tab color */
   #ul_tabs>li.active>a {
   color: #fff;
   background-color: #002233;
   border: 1px solid #888888;
   }
   /* hover tab color */
   #ul_tabs>li>a:hover {
   border-color: #000000;
   background-color: #004d4d;
   }
   .dataTables_length{
   float: left;
   }
   table.table-bordered tbody th, table.table-bordered tbody td {
   border:1px solid #e4eaec;
   }
   .col-sm-12 {
    overflow-x: scroll;
   }
   .table.table-bordered.dataTable {
    width: 100vw !important;
   }
</style>
<script type="text/javascript"></script>
<a onclick="topFunction()" id="myBtn">{!! HTML::image('/img/t.png', 'Logo', array('class' => 'normal-logo logo-white', 'width' => '45px')) !!}</a>
<script type="text/javascript">
   window.onscroll = function() {scrollFunction()};

   function scrollFunction() {
     if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
         document.getElementById("myBtn").style.display = "block";
     } else {
         document.getElementById("myBtn").style.display = "none";
     }
   }
   // When the user clicks on the button, scroll to the top of the document
   function topFunction() {
     document.body.scrollTop = 0; // For Chrome, Safari and Opera
     document.documentElement.scrollTop = 0; // For IE and Firefox
   }
</script>
<div class="container" dir="rtl">
   <div class="page-head">
      <h3>مدیریت عمومی مشخصات ومعاینه
      </h3>
   </div>
   <div class="cl-mcont" id="sdu_result">
      <div class="tab-container">
         @if (count($errors) > 0)
         <script type="text/javascript">
            $("ul#ul_tabs li:first-child").removeClass("active");
            $("ul#ul_tabs li:nth-child(2)").addClass("active");
         </script>
         <div class="alert alert-danger" style="margin: 10px 0 20px 0">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if(Session::has('success'))
         <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
         </div>
         @elseif(Session::has('fail'))
         <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
         </div>
         @endif
          <form class="form-horizontalsss" action="{!! URL::route('insert_range')!!}"  method="post" style="border-radius: 0px; margin-bottom: 20px;"  >

            <table align="center">
              <tr >
                <td >
              <div class="form-group" >
                 <label for="sel1"></label>
                 <select class="form-control" name="form_date" id="sel1" style="width:150px;">
                   <option value="1395" <?php if(getLatestDate('year') =='1395') echo "selected";?>>1395</option>
                   <option value="1396" <?php if(getLatestDate('year') =='1396') echo "selected";?>>1396</option>
                   <option value="1397" <?php if(getLatestDate('year') =='1397') echo "selected";?>>1397</option>
                   <option value="1398" <?php if(getLatestDate('year') =='1398') echo "selected";?>>1398</option>
                   <option value="1399" <?php if(getLatestDate('year') =='1399') echo "selected";?>>1399</option>
                   <option value="1400" <?php if(getLatestDate('year') =='1400') echo "selected";?>>1400</option>
                   <option value="1401" <?php if(getLatestDate('year') =='1401') echo "selected";?>>1401</option>
                   <option value="1402" <?php if(getLatestDate('year') =='1402') echo "selected";?>>1402</option>


                 </select>
               </div>

                </td>

                 {!!Form::token()!!}

                <td style="padding: 1px;">
                  <input  type="submit" value="جستجو" i class="btn btn-info"><i class="fa fa-insert fa-lg"></i>&nbsp;
                </td
              </tr>
            </table>
          </form>
            <div class="row" style="float: right;">
             @if (canAddDepartment('specs_records_list'))
                <button class="btn btn-info " type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="float:left;">
                  اضافه نمودن ریاست
                </button>
              @endif
            </div>
            <div class="row" style=" padding: 0px 0px 20px 0px; margin:0; ">

              <div class="collapse" id="collapseExample" style="margin-top: 50px;">
                <div class="well">
                  <div class="row">
                    <form action="{{route('insert_speci_department')}}" method="POST">

                      <div class="form-group">
                        <label for="department">ریاست دری</label>
                        <input type="text" name="name" placeholder="ریاست دری"  class="form-control" id="department" required>
                      </div>
                      <div class="form-group">
                        <label for="pwd">ریاست انگلیسی</label>
                        <input type="text" name="name_en" placeholder="ریاست انگلیسی" class="form-control" id="pwd">
                      </div>
                      {!!Form::token()!!}

                      <button type="submit" class="btn btn-primary">ثبت</button>
                    </form>
                  </div><!-- /.row -->
                </div>
              </div>
            </div>
         <ul class="nav nav-tabs" id="ul_tabs">
            <li class="active"><a href="#tab1" data-toggle="tab" style=" font-weight: bold;"> ف س ۹</a></li>
            <li ><a href="#tab5" data-toggle="tab" style=" font-weight: bold;">ف س ۹ رد</a></li>
            <li  ><a href="#tab2" data-toggle="tab"><b style="font-weight: bold;">لیست مشخصات وسایل تکنالوژی معلوماتی </b></a></li>
            <li  ><a href="#tab3" data-toggle="tab" style="font-weight: bold;">لیست مشخصات وسایل قبول شده </a></li>
            <li  ><a href="#tab4" data-toggle="tab" style="font-weight: bold;">لیست مشخصات وسایل رد شده </a></li>
            <li><a href="{!!URL::route('search_page')!!}" style="font-weight: bold;"> جستجوي پيشرفته </a></li>
         </ul>
         <div class="tab-content">
            <div class="tab-pane active cont" id="tab1">
               <!-- DataTables for Contract Type -->
               <div class="">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertForm')!!}" style="margin-left: 15px; font-weight: bold;" class="btn btn-success" >+ اضافه کردن ف س ۹ جدید</a>
                        </div>
                        <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form">
                              <div class="input-group custom-search-form">
                                 <input type="text" class="form-control" id="search_field" name="record" placeholder="جستجو" required />
                                 {!!Form::token();!!}
                                 <span class="input-group-btn">
                                 <button class="btn btn-default-sm" id="search_button"><i class="fa fa-search"></i> جستجو</button>
                                 </span>
                              </div>
                           </form>
                        </div>
                     </div>
                     <h3 style="margin-top:20px; color: #001a4d" align="center">لیست  ف س ۹ قبول شده</h3>
                     <hr />
                  </div>
                  <div class="content">
                     <div id="datalist">
                        <table class="table table-bordered table-responsive" id="feceno" >
                           <thead>
                              <tr>
                                 <th>شماره #</th>
                                 <th>شماره ف س ۹</th>
                                 <th>تاریخ</th>
                                 <th> فایل ف س ۹</th>
                                 <th style="width: 300px;">ریاست مربوطه</th>
                                 <th style="width: 300px;">توضیحات</th>
                                 <th>عملیات</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php $counter = 1; ?>
                              @foreach($feceno_data as $val)
                              <tr>
                                 <td>{!! $counter !!}</td>
                                 <td>{!! $val->number !!}</td>
                                 <td>{!!checkEmptyDate($val->date)!!}</td>
                                 <td>
                                    <a href="{!!URL::route('show_feceno',$val->id)!!}" target="_blank"  ><i class='fa-file-image-o fa-2x text-success'></i></a>
                                 </td>
                                 <td>{!!$val->dept_name !!}</td>
                                 <td>{!!$val->description !!}</td>
                                 <td><a href="select_data/{{$val-> id}}" class="fa fa-edit"></a></td>
                              </tr>
                              <?php $counter++ ; ?>
                              @endforeach
                           </tbody>
                        </table>
                        <!--    <div class="text-center">
                           {{-- {!! str_replace('/?', '?', $feceno_data->render()) !!} --}}


                           </div> -->
                     </div>
                  </div>
               </div>
            </div>
            <!-- tab 5 -->
            <div class="tab-pane  cont" id="tab5">
               <!-- DataTables for Contract Type -->
               <div class="">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <!-- <a href="{!!URL::route('insertForm')!!}" style="margin-left: 15px; font-weight: bold;" class="btn btn-success" >+ اضافه کردن ف س ۹ جدید</a> -->
                        </div>
                        <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form_r">
                              <div class="input-group custom-search-form">
                                 <input type="text" class="form-control" id="search_field_r" name="record" placeholder="جستجو" required />
                                 {!!Form::token();!!}
                                 <span class="input-group-btn">
                                 <button class="btn btn-default-sm" id="search_button_r"><i class="fa fa-search"></i> جستجو</button>
                                 </span>
                              </div>
                           </form>
                        </div>
                     </div>
                     <h3 style="margin-top:20px; color:#990000 " align="center">لیست  ف س ۹ ردشده</h3>
                     <hr />
                  </div>
                  <div class="content">
                     <div id="datalist_r">
                        <table class="table table-bordered table-responsive" id="rejecte_feceno" >
                           <thead>
                              <tr>
                                 <th>شماره #</th>
                                 <th>شماره ف س ۹</th>
                                 <th>تاریخ</th>
                                 <th> فایل ف س ۹</th>
                                 <th style="width: 300px;">ریاست مربوطه</th>
                                 <th style="width: 300px;">توضیحات</th>
                                 <th >عملیات</th>
                              </tr>
                           </thead>
                           <tbody>
                           <?php $counter = 1; ?>
                           @foreach($feceno_data_re as $val)
                           <tr>
                              <td>{!! $counter !!}</td>
                              <td>{!! $val->number !!}</td>
                              <td>{!!checkEmptyDate($val->date)!!}</td>
                              <!-- <td><a href='../uploads/{!! $val->image_fe !!}' target='_blank' ><i class='fa fa-download fa-2x text-success'></i></a></td> -->
                              <td><a href="{!!URL::route('show_feceno',$val->id)!!}" target="_blank"  ><i class='fa-file-image-o fa-2x text-success'></i></a> </td>

                              <td>{!!$val->dept_name !!}</td>
                              <td>{!!$val->description !!}</td>

                              <!-- <td><a href="deleterow/{{$val-> id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td> -->
                              <td><a href="select_data/{{$val-> id}}" class="fa fa-edit"></a></td>
                           </tr>
                           <?php $counter++ ; ?>
                           @endforeach
                         </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab2">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insert_pro')!!}" style="margin-left: 15px; font-weight: bold;" class="btn btn-success">+ اضافه نمودن جنس</a>
                        </div>
                        <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form1">
                              <div class="input-group custom-search-form">
                                 <input type="text" class="form-control" id="search_field1" name="record" placeholder="جستجو" required />
                                 {!!Form::token();!!}
                                 <span class="input-group-btn">
                                 <button class="btn btn-default-sm" id="search_button1" ><i class="fa fa-search"></i> جستجو</button>
                                 </span>
                              </div>
                   `        </form>
                        </div>
                     </div>
                     <h3 style="margin-top:20px;" align="center">لیست  مشخصات وسایل تکنالوژی معلوماتی</h3>
                     <hr />
                  </div>
                  <div id="datalist1">
                     <table class="table table-bordered table-responsive"  id="specification_list"  >
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>شماره ف س ۹</th>
                              <th>شماره مشخصات</th>
                              <th>فایل ف س ۹</th>
                              <th>نام جنس</th>
                              <th> توضیحات جنس</th>
                              <th>تاریخ</th>
                              <th>تعداد جنس</th>
                              <th>واحد</th>
                              <th>ضمانت</th>
                              <th>  ریاست مربوطه</th>
                              <th> معاینه</th>
                              <th style="width: 50px;">عملیات</th>
                           </tr>
                        </thead>
                        <tbody>
                        <?php $counter = 1; ?>
                        @foreach($product as $val)

                        <tr>
                           <td>{!! $counter !!}</td>
                           <td>{!!$val->number!!}</td>
                           <td>{!!$val->moshakhasat_id!!}</td>
                           <td>
                              <a href="{!!URL::route('show_feceno',$val->feceno_id)!!}" target="_blank"  ><i class='fa-file-image-o fa-2x text-success'></i>
                              </a>
                           </td>
                           <td>{!!$val->product_name!!}</td>
                           <td ><textarea style=" text-align: left;">{!! strip_tags($val->product_description )!!}</textarea></td>
                           <td>{!!checkEmptyDate($val->date)!!}</td>
                           <td>{!!$val->number_device!!}</td>
                           <td>{!!$val->unit!!}</td>
                           <td>{!!$val->guarantee!!}</td>
                           <td style="width: 200px">{!!$val->name!!}</td>
                           <td><a href="status/{{$val-> id}}" ><span class="label label-info">معاینه</span></a></td>
                           <td><a href="delete_row/{{$val-> id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>
                            -- <a href="select_row/{{$val-> id}}" class="fa fa-edit"></a></td>
                        </tr>
                        <?php $counter++ ; ?>
                        @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab3">
               <!--Add New  -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <!-- <a href="{!!URL::route('insert_pro')!!}" style="margin-left: 15px; font-weight: bold;" class="btn btn-success">+ اضافه نمودن جنس</a> -->
                        </div>
                        <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form3">
                              <div class="input-group custom-search-form">
                                 <input type="text" class="form-control" id="search_field3" name="record" placeholder="جستجو" required />
                                 {!!Form::token();!!}
                                 <span class="input-group-btn">
                                 <button class="btn btn-default-sm" id="search_button3" ><i class="fa fa-search"></i> جستجو</button>
                                 </span>
                              </div>
                           </form>
                        </div>
                     </div>
                     <h3 style="margin-top:20px;" align="center">لیست  مشخصات وسایل تکنالوژی معلوماتی قبول شده </h3>
                     <hr />
                  </div>
                  <div id="datalist3">
                     <table class="table table-bordered table-responsive"  id="approved_specification">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>شماره ف س ۹</th>
                              <th>شماره مشخصات</th>
                              <th>فایل ف س ۹</th>
                              <th>نام جنس</th>
                              <th> توضیحات جنس</th>
                              <th>تاریخ</th>
                              <th>تعداد جنس</th>
                              <th>واحد</th>
                              <th>ضمانت</th>
                              <th>معاینه</th>
                              <th> تاریخ معاینه</th>
                              <th>  ریاست مربوطه</th>
                              <!-- <th colspan="2">عملیات</th> -->
                           </tr>
                        </thead>

                        <?php $counter = 1; ?>
                        @foreach($ap_products as $val)
                        <tr>
                           <td>{!! $counter !!}</td>
                           <td>{!!$val->number!!}</td>
                           <td>{!!$val->moshakhasat_id!!}</td>
                           <td>
                              <a href="{!!URL::route('show_feceno',$val->feceno_id)!!}" target="_blank"  ><i class='fa-file-image-o fa-2x text-success'></i>
                              </a>
                           </td>
                           <td>{!!$val->product_name!!}</td>
                           <td ><textarea style=" text-align: left;">{!! strip_tags($val->product_description )!!}</textarea></td>
                           <td>{!!checkEmptyDate($val->date)!!}</td>
                           <td>{!!$val->number_device!!}</td>
                           <td>{!!$val->unit!!}</td>
                           <td>{!!$val->guarantee!!}</td>
                           <td><a href="#" ><?php if($val->status==1) echo "<span class='label label-success'>قبول</span>"; ?></a></td>
                           <td>{!!checkEmptyDate($val->status_date)!!}</td>
                           <td style="width: 200px">{!!$val->name!!}</td>
                           <!--
                              <td><a href="delete_row/{{$val-> id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                              <td><a href="select_row/{{$val-> id}}" class="fa fa-edit"></a></td> -->
                        </tr>
                        <?php $counter++ ; ?>
                        @endforeach
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab4">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <!-- <a href="{!!URL::route('insert_pro')!!}" style="margin-left: 15px ;font-weight: bold;" class="btn btn-success">+ اضافه نمودن جنس</a> -->
                        </div>
                        <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form4">
                              <div class="input-group custom-search-form">
                                 <input type="text" class="form-control" id="search_field4" name="record" placeholder="جستجو" required />
                                 {!!Form::token();!!}
                                 <span class="input-group-btn">
                                 <button class="btn btn-default-sm" id="search_button4" ><i class="fa fa-search"></i> جستجو</button>
                                 </span>
                              </div>
                           </form>
                        </div>
                     </div>
                     <h3 style="margin-top:20px; color:#990000 " align="center">لیست  مشخصات وسایل تکنالوژی معلوماتی رد شده</h3>
                     <hr />
                  </div>
                  <div id="datalist4"  >
                     <table class="table table-bordered table-responsive"  id="rejected_specification" >
                        <thead >
                           <tr >
                              <th>شماره #</th>
                              <th>شماره ف س ۹</th>
                              <th>شماره مشخصات</th>
                              <th>فایل ف س ۹</th>
                              <th>نام جنس</th>
                              <th> توضیحات جنس</th>
                              <th>تاریخ</th>
                              <th>تعداد جنس</th>
                              <th>واحد</th>
                              <th>ضمانت</th>
                              <th>معاینه</th>
                              <th>تاریخ معاینه </th>
                              <th>  ریاست مربوطه</th>
                              <th>  معلومات</th>
                              <th style="width: 50px;">عملیات</th>
                           </tr>
                        </thead>
                        <?php $counter = 1; ?>
                        @foreach($reject_products as $val)
                        <tr>
                           <td>{!! $counter !!}</td>
                           <td>{!!$val->number!!}</td>
                           <td>{!!$val->moshakhasat_id!!}</td>
                           <td>
                              <a href="{!!URL::route('show_feceno',$val->feceno_id)!!}" target="_blank"  ><i class='fa-file-image-o fa-2x text-success'></i>
                              </a>
                           </td>
                           <td>{!!$val->product_name!!}</td>
                           <td ><textarea style=" text-align: left;">{!! strip_tags($val->product_description )!!}</textarea></td>
                           <td>{!!checkEmptyDate($val->date)!!}</td>
                           <td>{!!$val->number_device!!}</td>
                           <td>{!!$val->unit!!}</td>
                           <td>{!!$val->guarantee!!}</td>
                           <td><a href="status_update/{{$val-> id}}" ><?php if($val->status==2) echo "<span class='label label-warning'>رد</span>"; ?></a></td>
                           <td>{!!checkEmptyDate($val->status_date)!!}</td>
                           <td style="width: 180px">{!!$val->name!!}</td>
                           <td>{!!$val->status_description!!}</td>
                           <td><a href="delete_row/{{$val-> id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a> -- <a href="select_row/{{$val-> id}}" class="fa fa-edit"></a></td>

                        </tr>
                        <?php $counter++ ; ?>
                        @endforeach
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@stop
@section('footer-scripts')
<script type="text/javascript">
   $(document).ready(function() {

   $("#search_button").click(function(){

     var field_value = $('#search_field').val();

    if(field_value !="")
     $.ajax({
       type : "post",
       url : "{!!URL::route('searchData')!!}",
       data : {"search_string": field_value, "_token": "<?=csrf_token();?>"},
       success : function(response)
       {
         $("#datalist").html(response);
       }
     });
       return false;
   });

   });
</script>
<script type="text/javascript">
   // r mean rejected feceno
   $(document).ready(function() {

   $("#search_button_r").click(function(){

     var field_value = $('#search_field_r').val();

    if(field_value !="")

     $.ajax({
       type : "post",
       url : "{!!URL::route('searchData_reject')!!}",
       data : {"search_string_r": field_value, "_token": "<?=csrf_token();?>"},
       success : function(response)
       {
         $("#datalist_r").html(response);
       }
     });
       return false;
   });

   });
</script>
<script type="text/javascript">
   $(document).ready(function() {

   $("#search_button1").click(function(){

     var field_value = $('#search_field1').val();

    if(field_value !="")

     $.ajax({
       type : "post",
       url : "{!!URL::route('search_product')!!}",
       data : {"search_string2": field_value, "_token": "<?=csrf_token();?>"},
       success : function(response)
       {
         $("#datalist1").html(response);
       }
     });
       return false;
   });

   });




</script>
<script type="text/javascript">
   $(document).ready(function() {

   $("#search_button3").click(function(){

     var field_value = $('#search_field3').val();

     if(field_value !="")

     $.ajax({
       type : "post",
       url : "{!!URL::route('searchapprove')!!}",
       data : {"search_string3": field_value, "_token": "<?=csrf_token();?>"},
       success : function(response)
       {
         $("#datalist3").html(response);
       }
     });
       return false;
   });

   });




</script>
<script type="text/javascript">
   $(document).ready(function() {

   $("#search_button4").click(function(){

     var field_value = $('#search_field4').val();

    if(field_value !="")

     $.ajax({
       type : "post",
       url : "{!!URL::route('searchreject')!!}",
       data : {"search_string4": field_value, "_token": "<?=csrf_token();?>"},
       success : function(response)
       {
         $("#datalist4").html(response);
       }
     });
       return false;
   });

   });




</script>
<script type="text/javascript">
   $(document).ready(function(){
       $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
           localStorage.setItem('activeTab', $(e.target).attr('href'));
       });
       var activeTab = localStorage.getItem('activeTab');
       if(activeTab){
           $('#ul_tabs a[href="' + activeTab + '"]').tab('show');
       }
   });
</script>
<script type="text/javascript" language="javascript">

   $(document).ready(function () {
      $("#feceno").DataTable();       //capital "D"
      $("#rejecte_feceno").DataTable();       //capital "D"
      $("#specification_list").DataTable();       //capital "D"
      $("#approved_specification").DataTable();       //capital "D"
      $("#rejected_specification").DataTable();       //capital "D"
    });
</script>
@stop
