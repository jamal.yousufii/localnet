@extends('layouts.master')
@section('content')
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>معاینه جنس</h3></center>
    <ol class="breadcrumb">
     
      <li class="active">معاینه جنس \     </li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed" action="{!!URL::route('update_status_re',$product->id)!!}"  method="post" style="border-radius: 0px;" name="myform" enctype= 'multipart/form-data'>

         <table align="center">
            <tr>
                <td >
                   <label class="col-sm-2 control-label"> </label>
                      <span style="font-weight: bold;"> معاینه جنس</span> 
                    <select name="status" class="form-control select2me" required>
                      <option value="" >- -  معاینه   - -</option>
                      <option value="1" <?php if($product->status==1) echo "selected"; ?> >قبول</option>
                      <option value="2" <?php if($product->status==2) echo "selected"; ?>>رد</option>
                      <option value="0" <?php if($product->status==3) echo "selected"; ?>>معلوم نیست</option>
                    </select>
                </td>
            </tr>
            <tr>
               
                <td> <br> <br>
                   <input type="submit"  class="btn btn-success" value="ثبت شود"  onclick="validate();" name="myButton" id="myButton">
                   <a href="{!!URL::route('recordsList')!!}"> 
                    <input type="button" value="لیست اطلاعات" id="add_department" class="btn btn-danger"/>
                  </a>
                </td>
            </tr>

            </table>   
          {!!Form::token()!!}
         
        </form><br><br><br><br><br>
  </div></div>
</div>
@stop

@section('footer-scripts') 
<script type="text/javascript">
    $(document).ready(function(){
   

        });
   
</script>
<script type="text/javascript">
  function validate(){

var  myButton= document.getElementById('myButton');
var  myButton= document.getElementById('myButton');

                        setTimeout (function(){
                         
                          document.getElementById("myButton").disabled = true; 
                        },0);
                      setTimeout (function(){
                           document.getElementById("myButton").disabled = false; 
                        },4000);
}


</script>
@stop