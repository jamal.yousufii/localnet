@extends('layouts.master')
@section('content')
<style type="text/css">
#myBtn {
    display: none;
    position: fixed; 
    bottom: 10px;
    right: 30px; 
    z-index: 99; 
    border: none;
    outline: none; 
    cursor: pointer; 
    padding: 15px; 
 
}


table,thead,tr,th{
  text-align: center;
}
</style>
<a onclick="topFunction()" id="myBtn">{!! HTML::image('/img/t.png', 'Logo', array('class' => 'normal-logo logo-white', 'width' => '45px')) !!}</a>
<script type="text/javascript">
  window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera 
    document.documentElement.scrollTop = 0; // For IE and Firefox
}
</script>
<div class="container" dir="rtl">
  <div class="page-head">
    <h3>معاونیت مالی واداری  بررسی تجهیزات</h3>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
      <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(2)").addClass("active");
      </script>
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <ul class="nav nav-tabs" id="ul_tabs">
        <li class="active"><a href="#tab1" data-toggle="tab"  style=" font-weight: bold;">لیست  وسایل توزیع شده </a></li>
        <li  ><a href="#tab3" data-toggle="tab"  style=" font-weight: bold;">معلومات وسایل گرفته شده از کار مندان</a></li>
        <li  ><a href="#tab2" data-toggle="tab" style=" font-weight: bold;">لیست وسایل تکنالوژی</a></li>
        <li  ><a href="#tab4" data-toggle="tab"  style=" font-weight: bold;">اضافه نمودن جنس جدید</a></li>
        <li><a href="#tab5" data-toggle="tab"  style=" font-weight: bold;">اطلا عات پاک شده</a></li>
        <li><a href="{!!URL::route('search_form')!!}"  style=" font-weight: bold;"> جستجوي پيشرفته </a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active cont" id="tab1"> 
          <!-- DataTables for Contract Type -->
          <div class="">
            <div class="header">
               <br>
            <div class="row">
              <div class="pull-left">                           
                <a href="{!!URL::route('load_insert')!!}" style=" margin-left: 15px; float: left; font-weight: bold;" class="btn btn-success" >توزیع اجناس+</a><br><br>
               </div>
           <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form">
                 <div class="input-group custom-search-form">
                  <input type="text" class="form-control" id="search_field" name="record" placeholder="جستجو" required />
                 {!!Form::token();!!}
                   <span class="input-group-btn">
                      <button class="btn btn-default-sm" id="search_button"><i class="fa fa-search"></i> جستجو</button>
                    </span></div>
                </form>
              </div>
            </div>
          
            <h3 style="margin-top:20px;" align="center">  لیست  وسایل توزیع شده</h3>
            </div>
            <div class="content">

     
     
          <div>
               <table class="table table-bordered table-responsive" id="datalist" >
                  <thead>
                    <hr>
                    <tr>
                      <th>شماره #</th>
                      <th>نام </th>
                      <th>نام پدر</th>
                      <th>ریاست مربوطه</th>
                      <th>نوع جنس</th>
                      <th>مودل</th>
                      <th>شماره سریال</th>
                      <th>توضیحات</th>
                      <th colspan="2">عملیات</th>
                      </tr>                
                  </thead>
                   @if(!empty($first_list))
                  <?php $counter = 1; ?>
                 @foreach($first_list as $val)
                  <tr>
                  <td>{!! $counter !!}</td>
                     <td>{!! $val->em_name !!}</td>
                     <td>{!! $val->last_name !!}</td>
                     <td>{!! $val->dep_name !!}</td>
                     <td>{!! $val->name !!}</td>
                     <td>{!! $val->model !!}</td>
                     <td>{!! $val->serial_number !!}</td>
                     <td>{!! $val->description !!}</td>
                     <!--<td><a href="deleted_destribut_eq/{{$val-> em_eq_id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>--> <td><a href="select_dest/{{$val-> em_eq_id}}" class="fa fa-edit"></a></td><td></td>
                     </tr>
                  <?php $counter++ ; ?>
                   @endforeach   
                    @else
                    <tr><td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td></tr>
                    @endif
                </table>
                
              </div>

            </div>
          </div>
        </div>
        <!--list of given product -->
          
     <div class="tab-pane cont" id="tab3"> 
       <div class="">
            <div class="header">
               <br>
            <div class="row">
              <div class="pull-left">                           
                <a href="{!!URL::route('load_insert')!!}" style=" margin-left: 15px; float: left; font-weight:bold;" class="btn btn-success">توزیع اجناس+</a><br><br>

              </div>

              <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form3">
                 <div class="input-group custom-search-form">
                  <input type="text" class="form-control" id="search_field3" name="record" placeholder="جستجو" required />
                 {!!Form::token();!!}
                   <span class="input-group-btn">
                      <button class="btn btn-default-sm" id="search_button3"><i class="fa fa-search"></i> جستجو</button>
                    </span></div>
                </form>
              </div>
            </div>
          
            <h3 style="margin-top:20px;" align="center"> معلومات وسایل گرفته شده از کار مندان</h3>
            </div>
        <div class="content">
              <table class="table table-bordered table-responsive" id="datalist3" >
                  <thead>
                    <hr>
                    <tr>
                      <th>شماره #</th>
                      <th>نام </th>
                      <th>نام پدر</th>
                      <th>ریاست مربوطه</th>
                      <th>نوع جنس</th>
                      <th>مودل</th>
                      <th>شماره سریال</th>
                      <th>توضیحات</th>
                      <th colspan="2">عملیات</th>
                      </tr>                
                  </thead>
                   @if(!empty($third))
                  <?php $counter = 1; ?>
                 @foreach($third as $val)
                  <tr>
                  <td>{!! $counter !!}</td>
                     <td>{!! $val->em_name !!}</td>
                     <td>{!! $val->last_name !!}</td>
                     <td>{!! $val->dep_name !!}</td>
                     <td>{!! $val->name !!}</td>
                     <td>{!! $val->model !!}</td>
                     <td>{!! $val->serial_number !!}</td>
                     <td>{!! $val->description !!}</td>
                    <td><a href="deleted_destribut_eq/{{$val-> em_eq_id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                    <!-- <td><a href="select_dest/{{$val-> em_eq_id}}" class="fa fa-edit"></a></td>--><td></td>
                     </tr>
                  <?php $counter++ ; ?>
                   @endforeach   
                    @else
                    <tr><td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td></tr>
                    @endif
                </table>
                
            </div>
                </div>
                  </div>
         <!--list of all equipments-->
        <div class="tab-pane cont" id="tab2">   
          <div class="col col-12">
            <div class="header">
            <br>
         <a href="{!!URL::route('insert_equipment')!!}" style=" float: left; font-weight: bold;" class="btn btn-success">+ اضافه نمودن جنس</a>  
            <div class="row">
              <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form2">
                 <div class="input-group custom-search-form">
                  <input type="text" class="form-control" id="search_field2" name="record" placeholder="جستجو" required />
                 {!!Form::token();!!}
                   <span class="input-group-btn">
                      <button class="btn btn-default-sm" id="search_button2"><i class="fa fa-search"></i> جستجو</button>
                    </span></div>
                </form>
              </div>
            </div>
             <div class="col-md-6">
             </div><br><br>
               <h3 style="margin-top:20px;text-align:center">لیست  وسایل تکنالوژی معلوماتی </h3><hr />
               
         
            </div>
            
        <div class="content">
              <table class="table table-bordered table-responsive" id="datalist2" >
                  <thead>
                    <tr>
                      <th>شماره #</th>
                      <th>نام جنس </th>
                      <th>مودل</th>
                      <th>شماره سریال</th>
                      <th>جنس موجود است</th>
                      <th>توضیحات</th>
                      <th colspan="2">عملیات</th>
                      </tr>                
                  </thead>
                   @if(!empty($secand_list))
                  <?php $counter = 1; ?>
                 @foreach($secand_list as $val)
                  <tr>
                  <td>{!! $counter !!}</td>
                     <td>{!! $val->name !!}</td>
                     <td>{!! $val->model !!}</td>
                     <td>{!! $val->serial_number !!}</td>
                     <td><?php if($val->status==1){ echo "<span class='label label-success'>بلی</span>";}
                     else echo "<span class='label label-warning'>نخیر</span>";  ?></td>
                     <td>{!! $val->description !!}</td><td></td>
                  <!--   <td><a href="deleted/{{$val-> id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>-->
                     <td><a href="selectrow/{{$val-> id}}" class="fa fa-edit"></a></td>
                  
                     </tr>
                  <?php $counter++ ; ?>
                   @endforeach   
                    @else
                    <tr><td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td></tr>
                    @endif
                </table>
                
            </div><div class="row">
            
          </div>
          </div>
        

        </div>
        <!--3th tab-->
          <!-- fourth tab-->
          <div class="tab-pane cont" id="tab4"> 
          <!--type of product -->          
          <div class="col col-12">
            <div class="header">
            <br>
                <a href="{!!URL::route('load_cat')!!}" style=" float: left; font-weight: bold;" class="btn btn-success">+ اضافه نمودن جنس</a>
             <div class="col-md-6">
             </div><br><br>
               <h3 style="margin-top:20px;text-align:center"> انواع مشخصات وسایل تکنالوژی معلوماتی </h3><hr />
            </div>
            
        <div class="content">
              <table class="table table-bordered table-responsive" id="datalist" >
                  <thead>
                    <tr>
                      <th>شماره #</th>
                      <th>نام </th>
                      <th>توضیحات</th>
                      <th colspan="2">عملیات</th>
                      </tr>                
                  </thead>
                   @if(!empty($fourth))
                  <?php $counter = 1; ?>
                 @foreach($fourth as $val)
                  <tr>
                  <td>{!! $counter !!}</td>
                     <td>{!! $val->name !!}</td>
                     <td>{!! $val->description !!}</td>
                     <td><a href="deletedCat/{{$val-> id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td> <td><a href="selectCat/{{$val-> id}}" class="fa fa-edit"></a></td>
                     </tr>
                  <?php $counter++ ; ?>
                   @endforeach   
                    @else
                    <tr><td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td></tr>
                    @endif
                </table>               
            </div>
          </div>      
        </div>
        <div class="tab-pane cont" id="tab5"> 
         <!--deleted record -->          
          <div class="">
            <div class="header">
               <br>
            <div class="row">
              <div class="pull-left">                           
                <a href="{!!URL::route('load_insert')!!}" style=" margin-left: 15px; float: left; font-weight: bold;" class="btn btn-success">توزیع اجناس+</a><br><br>
              </div>
              <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form4">
                 <div class="input-group custom-search-form">
                  <input type="text" class="form-control" id="search_field4" name="record" placeholder="جستجو" required />
                 {!!Form::token();!!}
                   <span class="input-group-btn">
                      <button class="btn btn-default-sm" id="search_button4"><i class="fa fa-search"></i> جستجو</button>
                    </span></div>
                </form>
              </div>
            </div>
            <h3 style="margin-top:20px;" align="center"> لیست اطلا عات پاک شده</h3>
            </div>
        <div class="content">
              <table class="table table-bordered table-responsive" id="datalist4" >
                  <thead>
                    <hr>
                    <tr>
                      <th>شماره #</th>
                      <th>نام </th>
                      <th>نام پدر</th>
                      <th>ریاست مربوطه</th>
                      <th>نوع جنس</th>
                      <th>مودل</th>
                      <th>شماره سریال</th>
                      <th>توضیحات</th>

                      <!--<th colspan="2">عملیات</th>-->

                    </tr>                
                  </thead>
                   @if(!empty($delet_list))
                  <?php $counter = 1; ?>
                 @foreach($delet_list as $val)
                  <tr>
                  <td>{!! $counter !!}</td>
                     <td>{!! $val->em_name !!}</td>
                     <td>{!! $val->last_name !!}</td>
                     <td>{!! $val->dep_name !!}</td>
                     <td>{!! $val->name !!}</td>
                     <td>{!! $val->model !!}</td>
                     <td>{!! $val->serial_number !!}</td>
                     <td  style="border: 1px solid lightgray;">{!! $val->description !!}</td>
                    <!-- <td><a href="deleted_destribut_eq/{{$val-> em_eq_id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>--><!-- <td><a href="select_dest/{{$val-> em_eq_id}}" class="fa fa-edit"></a></td>-->
                     </tr>
                  <?php $counter++ ; ?>
                   @endforeach   
                    @else
                    <tr><td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td></tr>
                    @endif
                </table>
                
            </div><div class="row">
            
          </div>
          </div>
        </div>
       
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('footer-scripts') 
<!-- destributed proudct search-->
<script type="text/javascript">
  
  $(document).ready(function() {
  
  $("#search_button").click(function(){

    var field_value = $('#search_field').val();
   
    $.ajax({
      type : "post",
      url : "{!!URL::route('search_des_equipment')!!}",
      data : {"search_string": field_value, "_token": "<?=csrf_token();?>"},
      success : function(response)
      {
        $("#datalist").html(response);
      }
    });
      return false;
  });
  
});
</script>
<!-- equipment  search-->

<script type="text/javascript">
  
  $(document).ready(function() {
  
  $("#search_button2").click(function(){

    var field_value = $('#search_field2').val();
   
    $.ajax({
      type : "post",
      url : "{!!URL::route('search_equipment')!!}",
      data : {"search_string2": field_value, "_token": "<?=csrf_token();?>"},
      success : function(response)
      {
        $("#datalist2").html(response);
      }
    });
      return false;
  });
  
});
</script>
<!-- gievn equipment search   -->

<script type="text/javascript">
  
  $(document).ready(function() {
  
  $("#search_button3").click(function(){

    var field_value = $('#search_field3').val();
   
    $.ajax({
      type : "post",
      url : "{!!URL::route('search_given_equipment')!!}",
      data : {"search_string3": field_value, "_token": "<?=csrf_token();?>"},
      success : function(response)
      {
        $("#datalist3").html(response);
      }
    });
      return false;
  });
  
});
</script>
<!-- deleted equipment search   -->

<script type="text/javascript">
  
  $(document).ready(function() {
  
  $("#search_button4").click(function(){

    var field_value = $('#search_field4').val();
   
    $.ajax({
      type : "post",
      url : "{!!URL::route('search_deleted')!!}",
      data : {"search_string4": field_value, "_token": "<?=csrf_token();?>"},
      success : function(response)
      {
        $("#datalist4").html(response);
      }
    });
      return false;
  });
  
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
        $('#ul_tabs a[href="' + activeTab + '"]').tab('show');
    }
});
</script>
 <script>
   $("#feceno_id").select2();
   $("#department_id").select2();
    $("#moshakhasat_id").select2();


  </script>

@stop