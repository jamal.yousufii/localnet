@extends('layouts.master')
@section('content')

  
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3> اضافه نمودن جنس جدید</h3></center>
    <ol class="breadcrumb">
     
      <li class="active">اضافه نمودن جنس جدید \     </li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed" action="{!!URL::route('updateCat',$categories->id)!!}"  method="post" style="border-radius: 0px;" name="myform">

  
        <div class="form-group">
           <label class="col-sm-2 control-label"> نوع جنس </label>
            <div class="col-sm-4" >
              <input type="text" name="name" value="{!! $categories->name !!}" class="form-control" placeholder=" مودل  " required >

            </div>
           <label class="col-sm-2 control-label">توضیحات</label>
             <div class="col-sm-4">
            <textarea class="form-control" name="description" placeholder=" توضیحات"> {!! $categories->description!!}</textarea>
              
            </div>
          </div>
          <br>
     
          
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
             <input type="submit"  class="btn btn-success" value="ثبت شود"  onclick="validate();" name="myButton" id="myButton">
              <a href="{!!URL::route('load_equipment')!!}"> <input type="button" value="لیست اطلاعات 
                  " id="add_department" class="btn btn-danger"/></a>
            </div>
          </div>
        </form><br><br><br><br><br>
  </div></div>
</div>
@stop

@section('footer-scripts') 
<script type="text/javascript">
    $(document).ready(function(){
      });
   
</script>
<script type="text/javascript">
  function validate(){

var  myButton= document.getElementById('myButton');
var  myButton= document.getElementById('myButton');

                        setTimeout (function(){
                         
                          document.getElementById("myButton").disabled = true; 
                        },0);
                      setTimeout (function(){
                           document.getElementById("myButton").disabled = false; 
                        },4000);
}


</script>
  <script>
   $("#categories").select2();


</script>
@stop