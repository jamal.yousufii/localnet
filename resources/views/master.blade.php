<!DOCTYPE html>
<html lang="en">
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		@yield('head')
		<!-- CSRF Token -->
    	<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<style>
			@font-face {
				font-family: "B Nazanin";
				src: url('{{asset('css/font/AdobeArabic-Regular.otf')}}')
				}

		</style>
		<!--end::Web font -->
		<?php
		$lang = get_language();
		if($lang=="en"){
			$dir = "";
		}else {
			$dir = ".rtl";
		}
		?>
	
		<!--begin:: Styles -->
		<link href="{{asset('assets/vendors/base/vendors.bundle'.$dir.'.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/demo/demo12/base/style.bundle'.$dir.'.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle'.$dir.'.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/vendors/custom/fullcalendar/persianDatepicker.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('assets/vendors/custom/jstree/dist/themes/default/style.min.css')}}" rel="stylesheet" type="text/css" />
		{{-- <link href="{!!asset('assets/font/font.css')!!}" rel="stylesheet" type="text/css" /> --}}
		{{-- Include Custome CSS based on Language  --}}
     	<link rel="stylesheet" href="{{asset('/css/custome_'.($lang=='en'?'en':'dr').'.css')}}">
     	<link rel="stylesheet" href="{{asset('/css/custome.css')}}">
		<!--end:: Styles -->
		<link rel="shortcut icon" href="{!!asset('img/index.png')!!}" />
	
	</head>
	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
	<script src="{!!asset('assets/vendors/custom/jquery.min.js')!!}" type="text/javascript"></script>
		{{-- @include('cms.layout.partials._loader_base') --}}
        @include('layouts.partials._loader_base')
        <!--Include Layout-->
        @include('layouts.layout')
		<!-- User Profile -->
        @include('layouts.profile')
        <!--begin:: JS FILES -->
		
		<script src="{!!asset('assets/vendors/custom/jquery.js')!!}" type="text/javascript"></script>
		<script src="{!!asset('assets/vendors/custom/jstree/dist/jstree.min.js')!!}" type="text/javascript"></script>
		<script src="{!!asset('assets/vendors/base/vendors.bundle.js')!!}" type="text/javascript"></script>
		<script src="{!!asset('assets/demo/demo12/base/scripts.bundle.js')!!}" type="text/javascript"></script>
		<script src="{!!asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')!!}" type="text/javascript"></script>
		<script src="{!!asset('assets/app/js/dashboard.js')!!}" type="text/javascript"></script>
		<script src="{!!asset('assets/vendors/custom/fullcalendar/persianDatepicker.js')!!}" type="text/javascript"></script>
		<script src="{{asset('assets/demo/demo12/custom/components/base/sweetalert2.js')}}"></script>
		<script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
		<script src="{{ asset('assets/vendors/custom/echarts/echarts.js') }}"></script>
		<script src="{{ asset('assets/vendors/custom/echarts/echarts.min.js') }}"></script>
		<script src="{{ asset('assets/vendors/custom/echarts/dataTool.min.js') }}"></script>
		
		
		<script>
			tinymce.init({
				selector:'textarea.tinymce',
			});
		</script>
		<!--end:: JS FILES -->
		@yield('js-code')
		<!-- Page Scripts -->
		@include('assets.js.custom_js')
	</body>
	<!-- end::Body -->
</html>