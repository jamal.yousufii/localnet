@extends('layouts.print_master')
@section('content')
<style type="text/css">
    @page{
        /*margin:-10px !important;*/
        margin-left: -50px;
        margin-right: -50px;
        margin-top: -30px;
        margin-bottom:-30px;
      }
    @media print {
        .no-print, no-print * {
            display: none !important;
        }
        #contentarea img{ 
          width:100%;
          /*margin: 0 !important;
          padding: 0 !important;*/
        }

    }
    
    /*@media print{
body{ background-color:#FFFFFF; background-image:none; color:#000000 }
#ad{ display:none;}
#leftbar{ display:none;}
#contentarea{ width:99%;}

}*/
</style>

<div class="container" dir="rtl" style=" padding-bottom: 20px; margin-top: -50px;">

    <div class="content" id="contentarea" style="text-align: center;   ">

        @if(!empty($row)) @foreach($row as $val)

        <br>
        <br>
        <a href="../../uploads_doc/{!! $val->file_name!!}" download style="color: green" class="no-print"><b>
               <i class='fa fa-download fa-4x text-success'></i></b></a> &nbsp; &nbsp; &nbsp;
        <img src="../../uploads_doc/{!! $val->file_name!!}" width="595px"> @endforeach @else
        <td style="color:red ; text-align: center;" colspan="12"> موردی پیدا نشد !</td>
        @endif

    </div>
    <div class="row">
      <div class="col-md-12 text-center">
        <br>
        <a onclick="javascript:window.print();" id="print-btn" class="btn btn-success btn-large no-print">Print <i class="fa fa-print"></i></a>
        <a class="no-print" " href="{!!URL::route( 'recordsList_executive')!!} ">
           <input type="button " value="لیست اطلاعات " id="add_department " class="btn btn-large  btn-danger no-print "/></a>
        </div> 
    </div>
  </div>

@stop
@section('footer-scripts') 

