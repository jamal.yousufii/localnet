            @if(!empty($records))
                   <?php $counter = 1; ?>
                 @foreach($records as $val)
               <tr>
                  <td>{!! $counter  !!}</td>
                  <td>{{$val->number_documents =='0' ? ' ':$val->number_documents}}</td>
                  <td><?php if($val->document_type=='1'){ echo "حکم";}
                  elseif($val->document_type=='2') {echo "مکتوب";}
                  elseif($val->document_type=='3') {echo "پشنهاد";}
                  elseif($val->document_type=='4') {echo "فرمان";}
                  elseif($val->document_type=='5') {echo "استعلام";}
                  elseif($val->document_type=='6') {echo "ف س ۹";}
                  elseif($val->document_type=='7') {echo "مکتوب هدایتی";}
                  elseif($val->document_type=='8') {echo "پیشنهاد هدایتی";}
                  elseif($val->document_type=='9') {echo "ف س ۹ هدایتی";}
                  elseif($val->document_type=='10') {echo "تعیینات";}
                  elseif($val->document_type=='11') {echo " مکتوب وارده";}
                  elseif($val->document_type=='12') {echo "استعلام هدایتی";}
                  elseif($val->document_type=='13') {echo "کاپی سوم";}
                  elseif($val->document_type=='14') {echo "ورقه درخواستی";}
                   ?></td>
                  <td>{!! $val-> name !!}</td>
                  <td>{!! $val-> father_name !!}</td>
                  <td>{!!checkEmptyDate($val->date)!!}</td>
                  <td><?php if($val->type_id==1){ echo "صادره";}else{echo "وارده";} ?> </td>
                  <td><a href="{!!URL::route('show_file',$val->id)!!}" target="_blank" ><i class='fa-file-image-o fa-2x text-success'></i></a> </td>
                  <td>{!! $val->dept_name !!}</td>
                  <td>{!! $val->description !!}</td>
                  <td>{!! $val->related_to !!}</td>
                  <td>{!! $val->receiver !!}</td>
                  <td ><?php if($val->result==1){ echo "<span class='label label-success'>اجراه</span>";}if($val->result==2){ echo "<span class='label label-danger'>نا اجراه</span>";}if($val->result==3){ echo "<span class='label label-primary'>حفظیه</span>";} ?></td>
                  <td style="{{$val->result==2 ? getElapsedDate($val->result_date) >= 3 ? 'color:red' :'' :'' }}">{!!checkEmptyDate($val->result_date)!!}</td>
                 <?php
                  if($val->document_type==14){
                    $route="select_waraq_darkhasti_row";
                  }elseif($val->document_type ==7 or $val->document_type ==8 or $val->document_type ==9 or $val->document_type ==11 or $val->document_type ==12){
                  $route="select_data_doc_hadayat";
                  }else{
                    $route="select_data_doc";
                   }
                  ?>
                  <td><a href="{{($val->document_type==14 ?'delete_waraq_darkhasti':'deleterow_doc')}}/{{$val->id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                  <td><a href="{{$route}}/{{$val->id}}" class="fa fa-edit" target="_blank"></a></td>
                </tr>
                  <?php $counter++ ; ?>
                   @endforeach
                    @else
                    <tr>
                       <td colspan="15" style="color: red;text-align: center;"> موردی پیدا نشد !</td>
                    </tr>
                    @endif
