@extends('layouts.master')
@section('content')

<style type="text/css">
 table,tr, td {
    text-align: start;
 ;
  }
table,thead,tr,th{
  text-align: center;

}
table,thead,tr,th{
  text-align: center;

}
tr:nth-child(even) {background-color: #f2f2f2}




</style>
<style type="text/css">
#myBtn {
    display: none;
    position: fixed;
    bottom: 10px;
    right: 30px;
    z-index: 99;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 15px;

}
.nav-tabs>li>a {
    padding:  9px;
    margin-top: 1px;
    }
ul#ul_tabs{
  padding: 0px;
}
/*.table {
    width: 100vw;
  }
.scroll-table{
      overflow-x: scroll;

}*/
</style>
<a onclick="topFunction()" id="myBtn">{!! HTML::image('/img/t.png', 'Logo', array('class' => 'normal-logo logo-white', 'width' => '45px')) !!}</a>
<script type="text/javascript">
  window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera
    document.documentElement.scrollTop = 0; // For IE and Firefox
}
</script>
<div class="container" dir="rtl">

  <div class="page-head">
    <h3> مدیریت اجرائیه
 </h3>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
      <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(2)").addClass("active");
      </script>
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <div class="row" style="float: right;">
         @if (canAddDepartment())
            <button class="btn btn-info " type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="float:left;">
              اضافه نمودن ریاست
            </button>
          @endif
        </div>
      <div class="row" style=" padding: 0 0px 15px 0px; margin:0;">
        <div class="collapse" id="collapseExample" style="margin-top: 50px;">
          <div class="well">
            <div class="row">
              <form action="{{route('insert_executive_department')}}" method="POST">

                <div class="form-group">
                  <label for="department">ریاست دری</label>
                  <input type="text" name="name" placeholder="ریاست دری"  class="form-control" id="department" required>
                </div>
                <div class="form-group">
                  <label for="pwd">ریاست انگلیسی</label>
                  <input type="text" name="name_en" placeholder="ریاست انگلیسی" class="form-control" id="pwd">
                </div>
                {!!Form::token()!!}

                <button type="submit" class="btn btn-primary">ثبت</button>
              </form>
            </div><!-- /.row -->
          </div>
        </div>
      </div>
      <ul class="nav nav-tabs" id="ul_tabs" style="border-top: 1px solid lightgray; border-radius: ; height: 46px;"  >
         <li class="tab"  id="get_hakom"> <a  href="#tab1" data-toggle="tab" style=" font-weight: bold;">حکم</a></li>
         <li class="tab" id="get_maktob" ><a href="#tab2" data-toggle="tab"><b style="font-weight: bold;">مکتوب</b></a></li>
         <li class="tab" id="get_copy3" ><a href="#tab3" data-toggle="tab"><b style="font-weight: bold;">کاپی سوم</b></a></li>
         <li class="tab" id="get_maktab_warada"  ><a href="#tab4" data-toggle="tab" style="font-weight: bold;">مکاتب وارده</a></li>
         <li class="tab" id="pashnahad" ><a href="#tab5" data-toggle="tab" style="font-weight: bold;">پشنهاد</a></li>
‍ ‍‍‍        <li class="tab" id="farman"><a href="#tab6" data-toggle="tab" style="font-weight: bold;">فرمان</a></li>
‍ ‍‍‍        <li class="tab"  id="istalam"><a href="#tab7" data-toggle="tab" style="font-weight: bold;">استعلام</a></li>
‍ ‍‍‍        <li class="tab" id="feceno"><a href="#tab8" data-toggle="tab" style="font-weight: bold;">ف س ۹</a></li>
‍ ‍‍‍        <li class="tab" id="istalam_hidayati" ><a href="#tab9" data-toggle="tab" style="font-weight: bold;">استعلام هدایتی </a></li>
‍ ‍‍‍        <li class="tab" id="maktob_hedayati"><a href="#tab10" data-toggle="tab" style="font-weight: bold;">مکتوب هدایتی </a></li>
‍ ‍‍‍        <li class="tab" id="pashnahad_hadayati"><a href="#tab11" data-toggle="tab" style="font-weight: bold;">پیشنهاد هدایتی</a></li>
‍ ‍‍‍        <li class="tab" id="feceno_hadayati"><a href="#tab12" data-toggle="tab" style="font-weight: bold;">ف س ۹ هدایتی</a></li>
‍ ‍‍‍        <li class="tab" id="tayenat"><a href="#tab13" data-toggle="tab" style="font-weight: bold;">تعيينات</a></li>
‍ ‍‍‍        <li class="tab" id="waraq_darkhasti"><a href="#tab14" data-toggle="tab" style="font-weight: bold;">ورقه درخواستی</a></li>
‍ ‍‍‍        <li>
          <div class="dropdown" style="padding: 5px;">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <i class="fa fa-search"></i>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="text-align: right; right: 0; margin-top: 5px;">
              <li id="all_data_search">
              <a style="font-weight: bold;">جستجو</a>
             </li>
          ‍ ‍‍‍    <li id="search_yearly">
              <a  style="font-weight: bold;">جستجو سالانه</a>
              </li>
            </ul>
          </div></li>
      </ul><br>

         <div class="tab-content" id="data"  >

        <!-- tabs contents -->

         </div>

          </div>
         </div>
        </div>
     
 @stop

@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $(".tab").click(function () {
        if(!$(this).hasClass('active'))
        {
            $(".tab.active").removeClass("active");
            $(this).addClass("active");
        }
    });
});
</script>

<!-- TAB ACTIVATION  -->
<script type="text/javascript">
$(document).ready(function() {
  if(window.location.hash == "")
  {
    $("#get_hakom").load( "li", function(evt) {
     $.ajax({
      type:'get',
      url:'{!!URL::route("get_hakom_data")!!}',
      success:function(data){
         $('#data').html(data);


    }
     })

    return false;
   });
  }
});

  $('#ul_tabs a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
  });

// store the currently selected tab in the hash value
$("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
  var id = $(e.target).attr("href").substr(1);
  window.location.hash = id;
  if(id == "tab1")
  {
     $("#get_hakom").load( "li", function(evt) {
     $.ajax({
      type:'get',
      url:'{!!URL::route("get_hakom_data")!!}',
      success:function(data){
         $('#data').html(data);


    }
     })

    return false;
   });
  }else if(id == "tab2"){
  $("#get_maktob").load( "li", function(evt) {
     $.ajax({
      type:'get',
      url:'{!!URL::route("get_maktob_data")!!}',
      success:function(data){
         $('#data').html(data);

    }
     })

    return false;
   });
  }else if(id == "tab3")
  {
    $("#get_copy3").load( "li", function(evt) {
     $.ajax({
      type:'get',
      url:'{!!URL::route("get_copy_som_data")!!}',
      success:function(data){
         $('#data').html(data);

    }
     })

    return false;
   });

}else if(id == "tab4"){
  $("#get_maktab_warada").load( "li", function(evt) {
     $.ajax({
      type:'get',
      url:'{!!URL::route("get_mateb_warada_data")!!}',
      success:function(data){
         $('#data').html(data);

    }
     })

    return false;
   });

  }else if(id == "tab5"){

  $("#pashnahad").load( "li", function(evt) {
     $.ajax({
        type:'get',
        url:'{!!URL::route("get_pashnehad_data")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });
  }else if(id == "tab6"){
  $("#farman").load( "li", function(evt) {
     $.ajax({
        type:'get',
        url:'{!!URL::route("get_farman_data")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });
  }else if(id == "tab7"){

    $("#istalam").load( "li", function(evt) {
       $.ajax({
        type:'get',
        url:'{!!URL::route("get_istalam_data")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });

  }else if(id == "tab8"){
    $("#feceno").load( "li", function(evt) {
       $.ajax({
          type:'get',
          url:'{!!URL::route("get_feceno_data")!!}',
          success:function(data){
             $('#data').html(data);

        }
         })

        return false;
       });
  }else if(id == "tab9"){
    $("#istalam_hidayati").load( "li", function(evt) {
       $.ajax({
          type:'get',
          url:'{!!URL::route("get_istalam_hadayati")!!}',
          success:function(data){
             $('#data').html(data);

        }
         })

        return false;
       });
  }else if(id == "tab10"){

  $("#maktob_hedayati").load( "li", function(evt) {
     $.ajax({
        type:'get',
        url:'{!!URL::route("get_maktob_hadayati_data")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });

  }else if(id == "tab11"){
  $("#pashnahad_hadayati").load( "li", function(evt) {
     $.ajax({
        type:'get',
        url:'{!!URL::route("get_pashnahad_hadayati_data")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });

}else if(id == "tab12"){
  $("#feceno_hadayati").load( "li", function(evt) {
     $.ajax({
        type:'get',
        url:'{!!URL::route("get_feceno_hadayati_data")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });

}else if(id == "tab13"){
  $("#tayenat").load( "li", function(evt) {
     $.ajax({
        type:'get',
        url:'{!!URL::route("get_tayenat_data")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });
}else if(id == "tab14"){
  $("#waraq_darkhasti").load( "li", function(evt) {
     $.ajax({
        type:'get',
        url:'{!!URL::route("get_waraq_darkhasti_data")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });
  }

});
 </script>
<script type="text/javascript">

   $(document).ready(function() {

  $("#get_hakom").click( "li", function(evt) {
     $.ajax({
            type:'get',
            url:'{!!URL::route("get_hakom_data")!!}',
            success:function(data){
               $('#data').html(data);

          }
           })

          return false;
         });
  });

</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#get_maktob").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_maktob_data")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#get_copy3").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_copy_som_data")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#get_maktab_warada").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_mateb_warada_data")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script>
 <script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#pashnahad").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_pashnehad_data")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#farman").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_farman_data")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#istalam").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_istalam_data")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#feceno").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_feceno_data")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#maktob_hedayati").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_maktob_hadayati_data")!!}',
            success:function(data){

               $('#data').html(data);
              }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#waraq_darkhasti").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_waraq_darkhasti_data")!!}',
            success:function(data){

               $('#data').html(data);
              }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#pashnahad_hadayati").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_pashnahad_hadayati_data")!!}',
            success:function(data){

               $('#data').html(data);
                  }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#feceno_hadayati").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_feceno_hadayati_data")!!}',
            success:function(data){

               $('#data').html(data);
                  }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#tayenat").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_tayenat_data")!!}',
            success:function(data){

               $('#data').html(data);
                   }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#istalam_hidayati").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_istalam_hadayati")!!}',
            success:function(data){

               $('#data').html(data);
                   }
           })
          return false;
         });
  });
</script>
<!-- search all script -->
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#all_data_search").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("all_data_search")!!}',

            success:function(data){

               $('#data').html(data);
                   }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#search_yearly").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("search_yearly")!!}',

            success:function(data){

               $('#data').html(data);

               $( "#fromdate" ).persianDatepicker();
               $( "#todate" ).persianDatepicker();

                   }
           })
          return false;
         });
  });
</script>







<script type="text/javascript">
$(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
        $('#ul_tabs a[href="' + activeTab + '"]').tab('show');
    }
});
</script>

@stop
