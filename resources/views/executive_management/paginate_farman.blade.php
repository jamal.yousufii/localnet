  <table class="table table-bordered table-responsive"  id="">
                  <thead>
                      <tr>
                      <th>شماره #</th>
                      <th>شماره فرمان</th>
                      <th> نام کارمند</th>
                      <th>نام پدر</th>
                      <th>تاریخ</th>
                      <th> نوع</th>
                      <th>فایل فرمان</th>
                      <th style="width: 200px;">ریاست مربوطه</th>
                      <th style="width: 200px;">توضیحات</th>
                      <th colspan="2">عملیات</th>
                      </tr>
                   </thead>
                   <tbody >
                       @if(!empty($rows))
                         <?php $counter = $rows->firstItem(); ?>
                       @foreach($rows as $val)
                        <tr>
                        <td>{!! $counter  !!}</td>
                        <td>{!! $val->number_documents !!}</td>
                        <td>{!! $val->name !!}</td>
                        <td>{!! $val->father_name !!}</td>
                        <td>{!!checkEmptyDate($val->date)!!}</td>
                        <td><?php if($val->type_id==1){ echo "صادره";}else{echo "وارده";} ?> </td>
                        <td><a href="{!!URL::route('show_file',$val->id)!!}" target="_blank"  ><i class='fa-file-image-o fa-2x text-success'></i></a> </td>
                        <td>{!! $val->dept_name !!}</td>
                        <td>{!! $val->description !!}</td>
                        <td><a href="deleterow_doc/{{$val->id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                        <td><a href="select_data_doc/{{$val->id}}" class="fa fa-edit"></a></td>
                         
                        </tr>
                        <?php $counter++ ; ?>
                         @endforeach   
                          @else
                          <tr><td align='center' colspan='15'>معلومات در سیستم اضافه نگردیده است</td></tr>
                          @endif
                      </tbody>

                </table>
                   <div class="text-center">
                  {!! $rows->render() !!}
                  
                </div>    

                 <script type="text/javascript">
                    $(document).ready(function() {

                    $('.pagination a').on('click', function(event) {
                      event.preventDefault();
                      if ($(this).attr('href') != '#') {
                        //$('#ajaxContent').load($(this).attr('href'));
                        var dataString = $('#search_form').serialize();
                        dataString += "&page="+$(this).text()+"&ajax="+1;
                        $.ajax({
                            url: '{!!URL::route("get_farman_data")!!}',
                            data: dataString,
                            type: 'get',
                            beforeSend: function(){
                                //$("body").show().css({"opacity": "0.5"});
                                $('#page_data').html('<span style="margin-right:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                            },
                            success: function(response)
                            {
                                $('#page_data').html(response);
                            }
                        }
                      );
                      
                      }
                    });
                      });

                      $(document).ready(function() {
                        $(".pagination a").on("click", function(){
                        $(".pagination").find(".active").removeClass("active");
                        $(this).parent().addClass("active");
                     });
                  });
                  </script>
