<div class="col col-12">
   <div class="header">
      <br>
      <div class="center">
         <form class="form-horizontal group-border-dashed" method="post" id="form">
            <div class="form-group " style="margin-left: 276px;">
               <div class="col-sm-8 ">
                  <span style="font-weight: bold;">جستجو عمومی</span> <input type="text" name="all_search"  class="form-control" placeholder=" جستجو عمومی" >
               </div>
            </div>
            <div class="form-group " style="margin-left: 276px;">
               <div class="col-sm-4 ">
                  <span style="font-weight: bold;">نام پدر</span> <input type="text" name="father_name" class="form-control" placeholder=" نام پدر" >
               </div>
               <div class="col-sm-4">
                  <span style="font-weight: bold;"> نام </span><input type="text" required name="name" id="date"  class="form-control" placeholder="نام "  >
               </div>
            </div>
            <div class="form-group " style="margin-left: 276px;">
               <div class="col-sm-4 ">
                  <span style="font-weight: bold;">نتیجه</span> 
                  <select name="result"  id="result" class="form-control" style="width: 100%" required >
                     <option value="" >- -  نتیجه   - -</option>
                     <option value="1" >اجراء</option>
                     <option value="2" >نااجراء </option>
                     <option value="3" >حفظیه </option>
                  </select>
               </div>
            </div>
            {!!Form::token()!!}
            <div class="form-group" style="margin-left: ;">
               <label class="col-sm-2 control-label"></label>
               <div class="col-sm-8">
                  <button value="Search Data" id="searchData" class="btn btn-success" style="float: right; margin-right: 100px;"> جستجوی اطلاعات </button>&nbsp;
                  <input type="reset" name="" class="btn btn-info"> 
               </div>
            </div>
            <hr>
         </form>
      </div>
      <h3 style="margin-top:20px;" align="center">لیست اطلاعات </h3>
      <hr />
   </div>
   <div>
      <div id="load"></div>
      <div id="datalist10">
         <table class="table table-bordered table-responsive"  id="search" style=" width: 100%;">
            <thead>
               <tr>
                  <th>شماره #</th>
                  <th>شماره اسناد</th>
                  <th>نوع اسناد</th>
                  <th> نام کارمند</th>
                  <th>نام پدر</th>
                  <th>تاریخ</th>
                  <th> نوع</th>
                  <th>فایل  </th>
                  <th style="width: 200px;">ریاست مربوطه(مرسل)</th>
                  <th>توضیحات</th>
                  <th>راجع شده</th>
                  <th>گیرنده</th>
                  <th>نتیجه</th>
                  <th>تاریخ نتیجه</th>
                  <th colspan="2">عملیات</th>
               </tr>
            </thead>
            <tbody id="search_result">
            </tbody>
         </table>
      </div>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function() {
     $('#searchData').click(function(){
       var dataString = $('#form').serialize();
       if(dataString !='')
        $.ajax({
             type:'POST',
             url:'{!!URL::route("search_all")!!}',
             data: dataString,
              beforeSend: function(){
                  $('#search_result').html('<center><span>{!!HTML::image("/img/ajax-loader.gif")!!}</span></center>');
              },
             success:function(response){
               // $('#datalist10').html('');
                $('#search_result').html(response);
             }
            })
   
           return false;
          });
   });
     
   
</script>