 <div class="col col-12">
          <div class="header">
             <br>
              <div class="row">
               <div class="pull-left">
               <a href="{!!URL::route('insertForm_hadayat_executive')!!}" style="margin-left: 15px; font-weight: bold;" class="btn btn-success" >+ اضافه نمودن اسناد</a>
                </div>
              <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form8">
                 <div class="input-group custom-search-form">
                  <input type="text" class="form-control" id="search_field8" name="record" placeholder="جستجو" required />
                    {!!Form::token();!!}
                     <span class="input-group-btn">
                      <button class="btn btn-default-sm" id="search_button8" cat_id="8"><i class="fa fa-search"></i> جستجو</button>
                  </span></div>
                </form>
              </div>
             </div>
            <h3 style="margin-top:20px;" align="center">لیست  پشنهاد هدایتی</h3><hr />
          </div>
            
        <div>
          <div id="datalist8" class="scroll-table">
            <div id="page_data">
            <table class="table table-bordered table-responsive"  id="">
                  <thead>
                     <tr>
                      <th>شماره #</th>
                      <th>شماره پیشنهاد هدایتی</th>
                      <th>تاریخ</th>
                      <th> نوع</th>
                      <th>فایل </th>
                      <th  style="width: 200px;">ریاست مربوطه</th>
                      <th>راجع شده</th>
                      <th>گیرنده</th>
                      <th>نتیجه</th>
                      <th>تاریخ نتیجه</th>
                      <th style="width: 200px;">توضیحات</th>
                      <th colspan="2">عملیات</th>
                      </tr>
                  </thead>

                 <tbody>
                      @if(!empty($rows))
                       <?php $counter = 1; ?>
                     @foreach($rows as $val)
                    <tr>
                      <td>{!! $counter  !!}</td>
                      <td>{!! $val-> number_documents !!}</td>
                      <td>{!!checkEmptyDate($val->date)!!}</td>
                      <td><?php if($val->type_id==1){ echo "صادره";}else{echo "وارده";} ?> </td>
                      <td><a href="{!!URL::route('show_file',$val->id)!!}" target="_blank" ><i class='fa-file-image-o fa-2x text-success'></i></a> </td>
                      <td>{!! $val->dept_name !!}</td>
                      <td>{!! $val->related_to !!}</td>
                      <td>{!! $val->receiver !!}</td>
                      <td ><?php if($val->result==1){ echo "<span class='label label-success'>اجراء</span>";}if($val->result==2){ echo "<span class='label label-danger'>نا اجراء</span>";}if($val->result==3){ echo "<span class='label label-primary'>حفظیه</span>";} ?></td>
                      <td style="{{$val->result==2 ? getElapsedDate($val->result_date) >= 3 ? 'color:red' :'' :'' }}">{!!checkEmptyDate($val->result_date)!!}</td>
                      <td>{!! $val->description !!}</td>
                       <td><a href="deleterow_doc_hadayat/{{$val->id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                      <td><a href="select_data_doc_hadayat/{{$val->id}}" class="fa fa-edit"></a></td>
                     
                       
                    </tr>
                      <?php $counter++ ; ?>
                       @endforeach   
                        @else
                        <tr><td align='center' colspan='15'>معلومات در سیستم اضافه نگردیده است</td></tr>
                        @endif
                  </tbody>
                </table>
               <div class="row" style="margin-right:0;">
                  <h5 style="font-weight:bold;">مجموعه اسناد:<span style="color:#00b1b3;"> {{$rows->total()}}</span></h5>
                </div>
                  <div class="text-center">
                    {!! $rows->render() !!}                                
                  </div>
                 </div>
                 </div>
                   </div>
                </div>
                  <script type="text/javascript">
                  //get the contract type list for datatable
                  $(document).ready(function() {
                    
                    $("#search_button8").click(function(){
                      var field_value = $('#search_field8').val();

                         var tab_id= $(this).attr('cat_id');
                   
                      $.ajax({
                        type : "post",
                        url : "{!!URL::route('search_live_hadayat')!!}",
                        data : {"cat_id":$(this).attr('cat_id'),"field_value":field_value,"_token": "<?=csrf_token();?>"},
                        success : function(response)
                        {
                          $("#datalist8").html(response);
                        }
                      });
                        return false;
                    });
                    
                  });


                  </script> 



                    <script type="text/javascript">
                      $(document).ready(function() {

                      $('.pagination a').on('click', function(event) {
                        event.preventDefault();
                        if ($(this).attr('href') != '#') {
                          //$('#ajaxContent').load($(this).attr('href'));
                          var dataString = $('#search_form').serialize();
                          dataString += "&page="+$(this).text()+"&ajax="+1;
                          $.ajax({
                              url: '{!!URL::route("get_pashnahad_hadayati_data")!!}',
                              data: dataString,
                              type: 'get',
                              beforeSend: function(){
                                  //$("body").show().css({"opacity": "0.5"});
                                  $('#page_data').html('<span style="margin-right:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                              },
                              success: function(response)
                              {
                                  $('#page_data').html(response);
                              }
                          }
                        );
                        
                        }
                      });
                        });

                        $(document).ready(function() {
                          $(".pagination a").on("click", function(){
                          $(".pagination").find(".active").removeClass("active");
                          $(this).parent().addClass("active");
                       });
                    });
                    </script>