
<table class="table table-bordered table-responsive" id="datalist">

<tr style="border-style: none;"><td colspan="15" style=" color: green;text-align: center;" ><b>نتیجه به دست آمده از جستجو</b></td></tr>

  
      <tr>
        <th>شماره #</th>
         <th>شماره اسناد</th>
        <th>نام کارمند </th>
         <th>نام پدر</th>
         <th>تاریخ</th>
         <th> نوع</th>
         <th>فایل </th>
         <th style="width: 200px;">ریاست مربوطه</th>
         <th style="border: 1px solid lightgray ;width: 200px;">توضیحات</th>
         <th colspan="2" style="border: 1px solid lightgray">عملیات</th>
         </tr>
    
                @if(!empty($rows))
                   <?php $counter =1; ?>
                 @foreach($rows as $val)
                  <tr>
                  <td>{!! $counter  !!}</td>
                  <td>{!! $val-> number_documents !!}</td>
                  <td>{!! $val-> name !!}</td>
                  <td>{!! $val-> father_name !!}</td>
                  <td>{!!checkEmptyDate($val->date)!!}</td>
                  <td><?php if($val->type_id==1){ echo "صادره";}else{echo "وارده";} ?> </td>
                  <td><a href="{!!URL::route('show_file',$val->id)!!}" target="_blank"   ><i class='fa-file-image-o fa-2x text-success'></i></a> </td>
                  <td>{!! $val->dept_name !!}</td>
                  <td>{!! $val->description !!}</td>
                  <td><a href="deleterow_doc/{{$val->id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                  <td style="border: 1px solid lightgray"><a href="select_data_doc/{{$val->id}}" class="fa fa-edit"></a></td>
             
                 
                   
                  </tr>
             <?php $counter++ ; ?> 
           @endforeach  
           
        @else
        <tr>
          <td colspan="10" style="color: red;text-align: center;"> موردی پیدا نشد !</td>
        </tr>
      @endif    
  

