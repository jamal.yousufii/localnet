                 @if(!empty($rows))
                 <?php $counter = $rows->firstItem(); ?>
                 @foreach($rows as $val)
                  <tr>
                  <td>{!! $counter  !!}</td>
                  <td>{!! $val-> number_documents !!}</td>
                  <td>{!!checkEmptyDate($val->date)!!}</td>
                  <td><?php if($val->type_id==1){ echo "صادره";}else{echo "وارده";} ?> </td>
                  <td><a href="{!!URL::route('show_file',$val->id)!!}"  target="_blank"><i class='fa-file-image-o fa-2x text-success'></i></a> </td>
                  <td>{!! $val->dept_name !!}</td>
                  <td>{!! $val->related_to !!}</td>
                  <td>{!! $val->receiver !!}</td>
                  <td><?php if($val->result==1){ echo "<span class='label label-success'>اجراء</span>";}if($val->result==2){ echo "<span class='label label-danger'>نا اجراء</span>";}if($val->result==3){ echo "<span class='label label-primary'>حفظیه</span>";} ?></td>
                  <td>{!!checkEmptyDate($val->result_date)!!}</td>
                  <td>{!! $val->description !!}</td>
                  <td><a href="deleterow_doc_hadayat/{{$val->id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                  <td><a href="select_data_doc_hadayat/{{$val->id}}" class="fa fa-edit"></a></td>                   
                  </tr>
                  <?php $counter++ ; ?>
                   @endforeach   
                    @else
                    <tr><td align='center' colspan='15'>معلومات در سیستم اضافه نگردیده است</td></tr>
                    @endif