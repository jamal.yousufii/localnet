<div class="col col-12">
    <div class="header">
        <br>
        <div class="center">
            <form class="form-horizontal group-border-dashed" action="{{URL::Route('export_excel')}}" method="post" id="form">
                <div class="form-group " style="margin-left: 276px;">
                    <div class="col-sm-4 ">
                        <span style="font-weight: bold;">الی تاریخ </span>
                        <input type="text" name="to_year" id="todate" class="datepicker_farsi form-control" placeholder="تاریخ الی">
                    </div>
                    <div class="col-sm-4">
                        <span style="font-weight: bold;">از تاریخ  </span>
                        <input type="text" name="from_year" id="fromdate" class="datepicker_farsi form-control" placeholder=" تاریخ از  ">
                    </div>
                </div>
                <div class="form-group " style="margin-left: 276px;">
                    <div class="col-sm-4 ">
                        <span style="font-weight: bold;">نتیجه</span>
                        <select name="result" id="result" class="form-control" style="width: 100%">
                            <option value="">- - نتیجه - -</option>
                            <option value="1">اجراء</option>
                            <option value="2">نااجراء </option>
                            <option value="3">حفظیه </option>
                        </select>
                    </div>
                    <div class="col-sm-4 ">
                        <span style="font-weight: bold;">نوع اسناد </span>
                        <select name="doc_type" id="doc_type" class="form-control" style="width: 100%">
                            <option value="">- - نوع اسناد - -</option>
                            <option value="1">حکم</option>
                            <option value="2">مکتوب</option>
                            <option value="3">پشنهاد</option>
                            <option value="4">فرمان</option>
                            <option value="5">استعلام</option>
                            <option value="6">ف س ۹</option>
                            <option value="7">مکتوب هدایتی</option>
                            <option value="8">پیشنهاد هدایتی</option>
                            <option value="9">ف س ۹ هدایتی</option>
                            <option value="10">تعيينات</option>
                            <option value="13">کاپی سوم</option>
                            <option value="7">مکتوب هدایتی</option>
                            <option value="8">پیشنهاد هدایتی</option>
                            <option value="9">ف س ۹ هدایتی</option>
                            <option value="12">استعلام هدایتی</option>
                            <option value="14">ورقه درخواستی</option>
                            <option value="11"> مکتوب وارده</option>
                        </select>
                    </div>
                </div>
                {!!Form::token()!!}
                <div class="form-group" style="margin-left: ;">
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-sm-8">
                        <button value="Search Data" id="searchData" class="btn btn-success" style="float: right; margin-right: 100px;"> جستجوی اطلاعات </button>&nbsp;
                        <input type="reset" name="" class="btn btn-info">
                        <button type="submit" name="" class="btn btn-primary" onClick="return confirm('Do want to export file ???')"><span class=" fa-cloud-download"></span>&nbsp;اکسل</button>
                    </div>
                </div>
                <hr>
            </form>
        </div>
        <h3 style="margin-top:20px;" align="center">لیست اطلاعات </h3>
        <hr />
    </div>
    <div>
        <table class="table table-bordered table-responsive" id="datalist10">
            <thead>
                <tr>
                    <th>شماره #</th>
                    <th>شماره اسناد</th>
                    <th>نوع اسناد</th>
                    <th> نام کارمند</th>
                    <th>نام پدر</th>
                    <th>تاریخ</th>
                    <th> نوع</th>
                    <th>فایل </th>
                    <th style="width: 200px;">ریاست مربوطه(مرسل)</th>
                    <th>توضیحات</th>
                    <th>راجع شده</th>
                    <th>گیرنده</th>
                    <th>نتیجه</th>
                    <th>تاریخ نتیجه</th>
                    <th colspan="2">عملیات</th>
                </tr>
            </thead>
            <tbody id="search_result">

            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#searchData').click(function() {
            var dataString = $('#form').serialize();
            $.ajax({
                type: 'POST',
                url: '{!!URL::route("search_all_yearly")!!}',
                data: dataString,
                beforeSend: function() {
                    $('#search_result').html('<center><span>{!!HTML::image("/img/ajax-loader.gif")!!}</span></center>');
                },
                success: function(response) {
                    $('#search_result').html(response);
                }
            })

            return false;
        });
    });
</script>
