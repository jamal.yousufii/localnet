@extends('layouts.master')
@section('content')
<style type="text/css">
  

</style>
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>اضافه نمودن اسناد</h3></center>
    <ol class="breadcrumb">
     
      <li class="active">اضافه نمودن اسناد  \     </li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed" action="{!!URL::route('insertData_doc')!!}"  method="post" style="border-radius: 0px;" name="myform" enctype= 'multipart/form-data' >

          <div class="form-group">
            <label class="col-sm-2 control-label" ></label>
            <div class="col-sm-4 ">
           <span style="font-weight: bold;">شماره اسناد</span> <input type="number" name="number" class="form-control" placeholder="شماره اسناد" required>
            </div>
            <label class="col-sm-2 control-label"> </label>
            <div class="col-sm-4">
            <span style="font-weight: bold;"> تاریخ</span><input type="text" name="date" id="date"  class="datepicker_farsi form-control" placeholder="تاریخ " required >

            </div> 
          </div>
           <div class="form-group">
              <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">نوع اسناد</span> <select name="document_type"   class="form-control" style="width: 100%" required>
                <option value="" >- -  نوع اسناد  - -</option>
                <option value="1">حکم</option>
                <option value="2">مکتوب</option>
                <option value="3">پشنهاد</option>
                <option value="4">فرمان</option>
                <option value="5">استعلام</option>
                <option value="6">ف س ۹</option>
                <option value="10">تعيينات</option>
                <option value="13">کاپی سوم</option>
              </select>
            </div> 
          <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">نوع  </span> <select name="type_doc"  id="type_doc" class="form-control" style="width: 100%" required >
                <option value="" >- -  نوع   - -</option>
                <option value="1" >صادره</option>
                <option value="2" >وارده </option>
       
              </select>
            </div> 
          </div>
           <div class="form-group">
             <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">انتخاب ریاست </span> 
              <select name="department_id"  id="department_select" class="form-control" style="width: 100%" required>
                <option value="" >- -  انتخاب ریاست  - -</option>
                @foreach($department as $val)
                <option value="{!!$val->id!!}">{!!$val->name!!}</option>
                @endforeach
              </select>
            </div> 
              <label class="col-sm-2 control-label"></label>
             <div class="col-sm-4">
            <span style="font-weight: bold;">توضیحات</span> <textarea class="form-control" name="description" placeholder=" توضیحات"> </textarea>
              
            </div>

          </div>
        <div class="form-group">
     
             <label class="col-sm-2 control-label" ></label>
            <div class="col-sm-4 ">
           <span style="font-weight: bold;">نام پدر</span> <input type="text" name="father_name" class="form-control" placeholder="نام پدر" >
            </div>
          <label class="col-sm-2 control-label" ></label>
            <div class="col-sm-4 ">
           <span style="font-weight: bold;">نام کارمند</span> <input type="text" name="name" class="form-control" placeholder="نام کارمند" >
            </div>
          </div>
       
          <div class="form-group">
          <label class="col-sm-2 control-label"> </label>
          <div class="col-sm-4">
              <span style="font-weight: bold;">فایل اسناد</span><div class="input_fields_wrap">
                  <input type='file' id='files' style="width:87%;display:inline-block" name='files[]' class="form-control" multiple='multiple' required>
                  <a class="add_field_button btn" id="add" style="background: green;color:white;display:inline-block;" title="Add another file"> + </a>
              </div>
          </div>

        </div>
          
          {!!Form::token()!!}
         <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
             <input type="submit"  class="btn btn-success" value="ثبت شود"  onclick="validate();" name="myButton" id="myButton">
              <a href="{!!URL::route('recordsList_executive')!!}"><input type="button" value="لیست اطلاعات 
                  " id="add_department" class="btn btn-danger"/></a>
            </div>
          </div>
        </form><br><br><br><br><br>
  </div></div>
</div>
@stop

@section('footer-scripts') 
<script type="text/javascript">
    $(document).ready(function(){
   

        });
   
</script>
<script type="text/javascript">
  function validate(){

var  myButton= document.getElementById('myButton');
var  myButton= document.getElementById('myButton');

                        setTimeout (function(){
                         
                          document.getElementById("myButton").disabled = true; 
                        },0);
                      setTimeout (function(){
                           document.getElementById("myButton").disabled = false; 
                        },4000);
}


</script>
<script type="text/javascript">

  $("#department_select").select2();
  $("#document_type").select2();

  $(function(){     
        
        // repeat the input fields ===================================
        var max_fields      = 50; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
       
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;display:inline-block;margin:0px 4px 5px 0px" title="remove"> X </a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            $('#add').fadeIn("slow");
            $('#appendedInputButton').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x--;
        });

    });

</script> 
@stop