       <table class="table table-bordered table-responsive"  id="datalist7">
        <tr style="border-style: none;"><td colspan="15" style=" color: green;text-align: center;" ><b>نتیجه به دست آمده از جستجو</b></td></tr>

                     <tr>
                      <th>شماره #</th>
                      <th>نام</th>
                      <th>تاریخ</th>
                      <th> نوع</th>
                      <th>فایل</th>
                      <th>ریاست مربوطه</th>
                      <th>راجع شده</th>
                      <th>گیرنده</th>
                      <th>نتیجه</th>
                      <th style="width: 200px;">تاریخ نتیجه</th>
                      <th style="width: 200px;">توضیحات</th>
                      <th colspan="2" style="border: 1px solid lightgray">عملیات</th>
                      </tr>                  
                    @if(!empty($rows))
                     <?php $counter = 1; ?>
                   @foreach($rows as $val)
                    <tr>
                    <td>{!! $counter  !!}</td>
                    <td>{!! $val-> name !!}</td>
                    <td>{!!checkEmptyDate($val->date)!!}</td>
                    <td><?php if($val->type_id==1){ echo "صادره";}else{echo "وارده";} ?> </td>
                    <td><a href="{!!URL::route('show_file',$val->id)!!}"  target="_blank"><i class='fa-file-image-o fa-2x text-success'></i></a> </td>
                    <td>{!! $val->dept_name !!}</td>
                    <td>{!! $val->related_to !!}</td>
                    <td>{!! $val->receiver !!}</td>
                 <td><?php if($val->result==1){ echo "<span class='label label-success'>اجراء</span>";}if($val->result==2){ echo "<span class='label label-danger'>نا اجراء</span>";}if($val->result==3){ echo "<span class='label label-primary'>حفظیه</span>";} ?></td>
                    <td style="{{$val->result==2 ? getElapsedDate($val->result_date) >= 3 ? 'color:red' :'' :'' }}">{!!checkEmptyDate($val->result_date)!!}</td>
                    <td>{!! $val->description !!}</td>
                     <td><a href="delete_waraq_darkhasti/{{$val->id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                    <td style="border: 1px solid lightgray"><a href="select_waraq_darkhasti_row/{{$val->id}}" class="fa fa-edit"></a></td>                                    
                    </tr>
                    <?php $counter++ ; ?>
                     @endforeach   
                      @else
                      <tr>        
                        <td colspan="20" style="color: red;text-align: center;"> موردی پیدا نشد !</td>
                      </tr>
                      @endif
                  </table>