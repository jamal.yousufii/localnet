@extends('layouts.master')

@section('head')
    {!! HTML::style('/vendor/select2/select2.css') !!}

    <title>{!!_('estate_registration_form')!!}</title>
    <style>
    	.form-control
    	{
    		margin-bottom: 10px !important;
    	}
    </style>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('estate_registration_form')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getEstateRegistryList')!!}" class="btn btn-success">{!!_('back')!!}</a></span>
    </div>
</div>

    <div class="container">
    	@if (count($errors) > 0)
		<div class="alert alert-danger" style="margin: 10px 0 20px 0">
	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
	      <ul>
	        @foreach ($errors->all() as $error)
	          <li>{{ $error }}</li>
	        @endforeach
	      </ul>
	    </div>
	    @endif
	    @if(Session::has('success'))
	    <div class="alert alert-success alert-dismissible" role="alert">
	      <button class="close" aria-label="Close" data-dismiss="alert" type="button">
	        <span aria-hidden="true">×</span>
	      </button>
	      {!!Session::get('success')!!}
	    </div>
	    @elseif(Session::has('fail'))
	    <div class="alert alert-danger alert-dismissible" role="alert">
	      <button class="close" aria-label="Close" data-dismiss="alert" type="button">
	        <span aria-hidden="true">×</span>
	      </button>
	      {!!Session::get('fail')!!}
	    </div>
	    @endif
    	<h4>{!!_('fill_the_below_form')!!}</h4><hr>
        <form action="{!!URL::route('addEstateRegistry')!!}" role="form" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group">
                	<label class="control-label col-sm-2">{!!_("code")!!} :</label>
                	<div class="col-sm-4">
                    	<input value="{!! old('code') !!}" type="text" name="code" class="form-control" required>
                    </div>
                    <label class="col-sm-2 control-label">{!!_('date')!!} :</label>
                    <div class="col-sm-4">
	                    <input type="text" class="{!!getDatePickerClass()!!} form-control" name='date' id="date" value="{!!old('date')!!}" readonly />
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="control-label col-sm-2">{!!_("location")!!} :</label>
                	<div class="col-sm-4">
                    	<textarea value="{!! old('location') !!}" name="location" class="form-control"></textarea>
                	</div>
                	<label class="control-label col-sm-2">{!!_("agriculture_verdancy")!!} :</label>
                	<div class="col-sm-4">
                    	<textarea value="{!! old('agriculture_verdancy') !!}" name="agriculture_verdancy" class="form-control"></textarea>
                	</div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-2 control-label">{!!_("general_information")!!} :</label>
                	<div class="col-sm-4">
                    	<textarea value="{!! old('general_information') !!}" name="general_information" class="form-control"></textarea>
                	</div>
                	<label class="col-sm-2 control-label">{!!_("attachments")!!} :</label>
                	<div class="col-sm-4">
	                    <div class="input_fields_wrap">
	                        <input type='file' id='files' style="width:87%;display:inline-block" name='files[]' class="form-control" multiple='multiple'>
	                        <a class="add_field_button btn" id="add" style="background: green;color:white;display:inline-block;margin-top:-20px;" title="Add another file"> + </a>
	                    </div>
	                </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="form-group col-xs-1">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('save')!!}</button>
                </div>
                <div class="form-group col-xs-1">
                    <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                </div>
            </div>
        </form>
    </div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
<script>
    
    $(function(){     
        
        // repeat the input fields ===================================
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
       
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;display:inline-block;margin:-20px 0 0 5px" title="remove"> X </a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            $('#add').fadeIn("slow");
            $('#appendedInputButton').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x--;
        });

    });
	
</script>
@stop
