	<?php 
	    if(isShamsiDate())
	    {
			$date = checkEmptyDate($record->date);
	    }
		else
		{
			$date = checkGregorianEmtpyDate($record->date);
		}
			
	?>
    <form class="form-horizontal" role="form" method="post">
      	<div class="modal-body">
      
          	<div class="form-group">
	              <label class="col-sm-2 control-label">{!!_('code')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="number" class="form-control" value="{!!$record->code!!}" disabled />
	              </div>
	              <label class="col-sm-2 control-label">{!!_('date')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="text" class="form-control" value="{!!$record->date!!}" disabled></textarea>
	              </div>
	        </div>
	        <div class="form-group">
	        	<label class="col-sm-2 control-label">{!!_('location')!!} : </label>
	           	<div class="col-sm-4">
	            	<textarea class="form-control" disabled>{!!$record->location!!}</textarea>
	            </div>
	              <label class="col-sm-2 control-label">{!!_('agriculture_verdancy')!!} : </label>
	              <div class="col-sm-4">
	                  <textarea class="form-control" disabled>{!!$record->agriculture_verdancy!!}</textarea>
	              </div>
	        </div>
	      	<div class="form-group">
	              <label class="col-sm-2 control-label">{!!_('general_information')!!} : </label>
	              <div class="col-sm-4">
	                  <textarea class="form-control" value="{!!$record->general_information!!}" disabled>{!!$record->general_information!!}</textarea>
	              </div>
	              <label class="col-sm-2 control-label">Uploaded Photo : </label>
	              <div class="col-sm-4">
	              @if(!empty($files))
	              	  @foreach($files as $filename)
	              	  <span style="padding-right: 10px">
	                  	<a href="../documents/const_maintenance_attachments/{!!$filename->file_name!!}" target="_blank">"<img src="../documents/const_maintenance_attachments/{!!$filename->file_name!!}"  width="160px" height="140" /></a>
	                  </span>
	                  @endforeach
	              @else
	              <p style="color:red">{!!_('no_attachment_uploaded')!!}</p>
	              @endif
	              </div>
          	</div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
      </div>
    </form>