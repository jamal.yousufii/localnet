@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('estate_registry_list')!!}</title>
    <style type="text/css">
        table thead tr th
        {
            text-align: center;
            border-color: #000; 
            text-align: center !important;
        }
        table tbody tr td
        {
            border-color: #000;
            text-align: center !important;
        }
        a{cursor:pointer;}
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop

@section('content')

    @if(Session::has('success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('success')!!}
		</div>

    @elseif(Session::has('fail'))
    	<div class="alert alert-danger alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('fail')!!}
		</div>
    @endif

    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="pull-left">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{!! URL::route('home') !!}">{!!_('Dashboard')!!}</a>
                        </li>
                        <li class="active">
                            <span>{!!_('all_estate_registry')!!}</span>
                        </li>
                    </ol>
                    <h1>{!!_('estate_registry_list')!!}</h1>
                </div>
            </div>
            <div class="form-group" style="margin-top:20px;margin-left: -13px">
            	<form id="search_form" class="form-horizontal">
	                <div class="col-sm-2">
	                    <input type="text" class="form-control" name="code" id="code" placeholder="{!!_('code')!!}" />
	                </div>
	                <div class="col-sm-4">
	                    <select class="form-control" name="location" id="location">
	                    	<option value=''>{!!_('select_location')!!}</option>
	                    	{!!getEstageRegistryLocations()!!}
				        </select>
	                </div>
	                <div class="col-sm-3">
	                    <input type="submit" value="{!!_('search')!!}" class="btn btn-warning"/>
	                    &nbsp;<input type="reset" value="{!!_('clear')!!}" class="btn btn-danger"/>
	                </div>
	        	</form>
	        	<?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){$dir = "pull-right";}else{$dir = "pull-left";}
			    ?>
                <span class="{!!$dir!!}"><a href="{!!URL::route('getEstateRegistryForm')!!}" class="btn btn-success">{!!_('add_new_estate_registry')!!}</a></span>
            </div>
        </div>
        <div style="padding:15px" class="table-responsive" id="search_result">
            <table class="table table-bordered table-responsive" id="inventoryList">
            	<thead>
		          <tr>
		            <th>{!!_('no#')!!}</th>
		            <th>{!!_('code')!!}</th>
		            <th>{!!_('location')!!}</th>
		            <th>{!!_('agriculture_verdancy')!!}</th>
		            <th>{!!_('general_information')!!}</th>
		            <th>{!!_('date')!!}</th>

		            <th colspan="3">{!!_('operations')!!}</th>
		            
		          </tr>
		        </thead>
		
		        <tbody>
		        	<?php 
		        		if(!empty($records))
		        		$counter = $records->firstItem(); 
		        	?>
		        	@if(!empty($records))
			            @foreach($records AS $item)
			                <tr class="remove_record{!!$item->id!!}">
				                <td>{!!$counter!!}</td>
				                <td>{!!$item->code!!}</td>
				                <td>{!!$item->location!!}</td>
				                <td>{!!$item->agriculture_verdancy!!}</td>
				                <td>{!!$item->general_information!!}</td>
				                @if(isShamsiDate())
									<td>{!!checkEmptyDate($item->date)!!}</td>
								@else
									<td>{!!checkGregorianEmtpyDate($item->date)!!}</td>
								@endif
				            	<td align='center'><a href="{!!route('getLoadEstateRegistryEditPage',$item->id)!!}" title='{!!_("edit_record")!!}'><span class='fa fa-edit'></span></a></td>
				                <td align='center'><a onclick="getDetails('{!!$item->id!!}')" title='{!!_("view_details")!!}'><span class='fa fa-eye'></span></a></td>
				                <td align='center'><a onclick="deleteRecord('{!!$item->id!!}')" title='{!!_("delete_record")!!}'><span class='fa fa-trash'></span></a></td>
			                </tr>
			                <?php $counter++; ?>
			            @endforeach
		            @else
			        <div style="padding: 10px">
			        	<span style="color:red">{!!_('no_records_found_in_the_system')!!}</span>
			        </div>
			    	@endif
		        </tbody>
            </table>
			<div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
				@if(!empty($records))
				{!!$records->render()!!}
				@endif
			</div>
        </div>
    </div>
    
    <div class="modal fade modal-info" id="exampleModalPrimary" aria-hidden="true" aria-labelledby="exampleModalPrimary" role="dialog" tabindex="-1">
	  <div class="modal-dialog" style="width: 1200px;">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">×</span>
	        </button>
	        <h4 class="modal-title">{!!_('details')!!}</h4>
	      </div>
	      <div id="form_part">
	
	      </div>
	    </div>
	  </div>
	</div>
    
@stop
@section('footer-scripts')

<script type="text/javascript">

$(document).ready(function() {
	
	$('.pagination a').on('click', function(event) {
		event.preventDefault();
		if ($(this).attr('href') != '#') {
			//$('#ajaxContent').load($(this).attr('href'));
			
			$.ajax({
	                url: '{!!URL::route("getEstateRegistryList")!!}',
	                data: {"page":$(this).text(),"ajax":1},
	                type: 'get',
	                beforeSend: function(){
	
	                    //$("body").show().css({"opacity": "0.5"});
	                    $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
	
	                    $('#search_result').html(response);
	                }
	            }
	        );
		
		}
	});
	
	//get the records list onchange of the dropdown.
	$("#search_form").submit(function(){
	
		var dataString = $('#search_form').serialize();
	    $.ajax({
	            url: "{!!URL::route('searchEstateRegistry')!!}",
	            data: dataString,
	            type: 'post',
	            beforeSend: function(){
	                $("#search_result").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	            },
	            success: function(response)
	            {
	                $("#search_result").html(response);
	            }
	        }
	    );
		return false;
	});
});

	function getDetails(id){
	  $.ajax({
	          url: '{!!URL::route("getEstateRegistryDetails")!!}',
	          data: {'id':id,'_token': "<?= csrf_token();?>"},
	          type: 'post',
	          beforeSend: function(){
	
	              //$("body").show().css({"opacity": "0.5"});
	              $('#form_part').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/images/ajax-loader.gif")!!}</span>');
	          },
	          success: function(response)
	          {
	
	              $('#form_part').html(response);
	              $('#exampleModalPrimary').modal("show");
	
	          }
	    });
	}
	
	// Delete Record ajax script;
    function deleteRecord(record_id) 
    {
        var ID = record_id;
        var dataString = "record_id="+ID;
        if(confirm("Are you sure you want to do this action ? THERE IS NO UNDO"))
        {
            $.ajax({
                 type: "POST",
                 url: "{!!URL::route('postDeleteEstateRegistry')!!}",
                 data: dataString,
                 cache: false,
                 success: function(mydata){
                    $(".remove_record"+ID).css('background','Crimson');
                    $(".remove_record"+ID).slideUp('6000', function(){
                    $(this).remove();
                    //$("#deleted_result").html(mydata);
                    });
                 }
            });
        }
        return false;
    }
</script>

@stop


