@extends('layouts.master')

@section('head')

    <title>{!!_('estate_registration_edit_form')!!}</title>
    <style>
    	.form-control
    	{
    		margin-bottom: 10px !important;
    	}
    </style>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('estate_registration_edit_form')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
    </div>
</div>

    <div class="container">
    	@if (count($errors) > 0)
		<div class="alert alert-danger" style="margin: 10px 0 20px 0">
	      <strong>Whoops!</strong> There were some problems with your input.<br><br>
	      <ul>
	        @foreach ($errors->all() as $error)
	          <li>{{ $error }}</li>
	        @endforeach
	      </ul>
	    </div>
	    @endif
	    @if(Session::has('success'))
	    <div class="alert alert-success alert-dismissible" role="alert">
	      <button class="close" aria-label="Close" data-dismiss="alert" type="button">
	        <span aria-hidden="true">×</span>
	      </button>
	      {!!Session::get('success')!!}
	    </div>
	    @elseif(Session::has('fail'))
	    <div class="alert alert-danger alert-dismissible" role="alert">
	      <button class="close" aria-label="Close" data-dismiss="alert" type="button">
	        <span aria-hidden="true">×</span>
	      </button>
	      {!!Session::get('fail')!!}
	    </div>
	    @endif
	    <?php 
		    if(isShamsiDate())
		    {
				$date = checkEmptyDate($record->date);
		    }
			else
			{
				$date = checkGregorianEmtpyDate($record->date);
			}
				
		?>
		<hr/>
        <form action="{!!URL::route('postEditEstateRegistry', $record->id)!!}" role="form" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group">
                	<label class="control-label col-sm-2">{!!_("code")!!} :</label>
                	<div class="col-sm-4">
                    	<input value="{!!$record->code!!}" type="text" name="code" class="form-control" required>
                    </div>
                    <label class="col-sm-2 control-label">{!!_('date')!!} :</label>
                    <div class="col-sm-4">
	                    <input type="text" class="{!!getDatePickerClass()!!} form-control" name='date' id="date" value="{!!$date!!}" readonly />
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="control-label col-sm-2">{!!_("location")!!} :</label>
                	<div class="col-sm-4">
                    	<textarea value="{!!$record->location!!}" name="location" class="form-control">{!!$record->location!!}</textarea>
                	</div>
                	<label class="control-label col-sm-2">{!!_("agriculture_verdancy")!!} :</label>
                	<div class="col-sm-4">
                    	<textarea name="agriculture_verdancy" class="form-control">{!!$record->agriculture_verdancy!!}</textarea>
                	</div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-2 control-label">{!!_("general_information")!!} :</label>
                	<div class="col-sm-4">
                    	<textarea value="{!!$record->general_information!!}" name="general_information" class="form-control">{!!$record->general_information!!}</textarea>
                	</div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="form-group col-xs-1">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-edit fa-lg"></i> {!!_('update')!!}</button>
                </div>
                <div class="form-group col-xs-1" style="margin-left:10px;">
                    <a href="{!!URL::route('getEstateRegistryList')!!}" class="btn btn-danger"><i class="fa fa-remove fa-lg"></i> {!!_('cancel')!!}</a>
                </div>
            </div>
        </form>
    </div>
@stop
@section('footer-scripts')
<script>
	
</script>
@stop
