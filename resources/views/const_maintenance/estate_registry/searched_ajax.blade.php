	<table class="table table-bordered table-responsive" id="inventoryList">
    	<thead>
          <tr>
            <th>{!!_('no#')!!}</th>
            <th>{!!_('code')!!}</th>
            <th>{!!_('location')!!}</th>
            <th>{!!_('agriculture_verdancy')!!}</th>
            <th>{!!_('general_information')!!}</th>
            <th>{!!_('date')!!}</th>

            <th colspan="3">{!!_('operations')!!}</th>
            
          </tr>
        </thead>

        <tbody>
        	<?php 
        		if(!empty($records))
        		$counter = $records->firstItem(); 
        	?>
        	@if(!empty($records))
	            @foreach($records AS $item)
	                <tr class="remove_record{!!$item->id!!}">
		                <td>{!!$counter!!}</td>
		                <td>{!!$item->code!!}</td>
		                <td>{!!$item->location!!}</td>
		                <td>{!!$item->agriculture_verdancy!!}</td>
		                <td>{!!$item->general_information!!}</td>
		                @if(isShamsiDate())
							<td>{!!checkEmptyDate($item->date)!!}</td>
						@else
							<td>{!!checkGregorianEmtpyDate($item->date)!!}</td>
						@endif
		            	<td align='center'><a href="{!!route('getLoadEstateRegistryEditPage',$item->id)!!}" title='{!!_("edit_record")!!}'><span class='fa fa-edit'></span></a></td>
		                <td align='center'><a onclick="getDetails('{!!$item->id!!}')" title='{!!_("view_details")!!}'><span class='fa fa-eye'></span></a></td>
		                <td align='center'><a onclick="deleteRecord('{!!$item->id!!}')" title='{!!_("delete_record")!!}'><span class='fa fa-trash'></span></a></td>
	                </tr>
	                <?php $counter++; ?>
	            @endforeach
            @else
	        <div style="padding: 10px">
	        	<span style="color:red">{!!_('no_records_found_in_the_system')!!}</span>
	        </div>
	    	@endif
        </tbody>
    </table>

	<div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
		@if(!empty($records))
		{!!$records->render()!!}
		@endif
	</div>
</div>
<script type="text/javascript">

$(document).ready(function() {
	
	$('.pagination a').on('click', function(event) {
		event.preventDefault();
		if ($(this).attr('href') != '#') {
			//$('#ajaxContent').load($(this).attr('href'));
			
			$.ajax({
	                url: '{!!URL::route("searchEstateRegistry")!!}',
	                data: {'code':$('#code').val(),'location':$('#location').val(),"page":$(this).text(),"ajax":1},
	                type: 'post',
	                beforeSend: function(){
	
	                    //$("body").show().css({"opacity": "0.5"});
	                    $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
	
	                    $('#search_result').html(response);
	                }
	            }
	        );
		
		}
	});
});

	// Delete Record ajax script;
    function deleteRecord(record_id) 
    {
        var ID = record_id;
        var dataString = "record_id="+ID;
        if(confirm("Are you sure you want to do this action ? THERE IS NO UNDO"))
        {
            $.ajax({
                 type: "POST",
                 url: "{!!URL::route('postDeleteEstateRegistry')!!}",
                 data: dataString,
                 cache: false,
                 success: function(mydata){
                    $(".remove_record"+ID).css('background','Crimson');
                    $(".remove_record"+ID).slideUp('6000', function(){
                    $(this).remove();
                    //$("#deleted_result").html(mydata);
                    });
                 }
            });
        }
        return false;
    }

</script>