@extends('layouts.master')
@section('head')
    <title>صادره وارده جدید</title>
@stop

@section('content')

<div class="finance">


@if(Session::has('success'))
{{Session::get('success')}}
@endif

@if(Session::has('failed'))
{{Session::get('failed')}}
@endif

<!-- {!! HTML::style('/css/font.css') !!} -->
<style>
    .finance  {font-family:Noori;direction:rtl;background:#fff;border:10px solid #fff;}
    .finance h1 {padding:10px;border-bottom:1px dashed #aaa;}
    .finance table {width:100%;background:#fff;}
    .new_info input[type=text], .new_info select, .new_info textarea {font-weight:bold;font-size:105%;border:1px solid #ccc;width:100%;padding:5px;margin:5px;}
    .new_info textarea {height:5em;transition:all 0.3s;}
    .new_info textarea:focus{height:10em;transition:all 0.3s;}
    .new_info table {width:100%;}
    .new_info td {padding:10px 30px;vertical-align:top}
    #config-tool {display:none !important;}
    input[type=submit] {font-weight: bold !important;}

</style>

<div class="finance new_info noorifont">

<h1>صادره/وارده جدید</h1>

<form id="new_info" action="" method="post">
    <table>

    <tr><td>
    <label>شماره مسلسل: </label><input required class="form-control" type="text" name="serial" /></td><td>
    <label>اسم مرجع مربوطه: </label><input required class="form-control" type="text" name="marji_marbota" /></td>
    </tr>

    <tr><td>
    <label>نمبرحکم: </label><input required class="form-control" type="text" name="hokum_no" /></td><td>
    <label>تاریخ حکم: </label><input required class="form-control datepicker_farsi" type="text" name="hokum_date" /></td>
    </tr>

    <tr><td>
    <label>نمبرمکتوب: </label><input required class="form-control" type="text" name="maktob_no" /></td><td>
    <label>تاریخ مکتوب: </label><input required class="form-control datepicker_farsi" type="text" name="maktob_date" /></td>
    </tr>

    <tr><td>
    <label>صادره/وارده: </label><select required name="sadira_warida"><option value="صادره">صادره</option><option value="وارده">وارده</option></select></td><td>
    <label>تاریخ صادره/وارده: </label><input required class="form-control datepicker_farsi" type="text" name="sadira_warida_date" /></td>
    </tr>

    <tr><td>
    <label>مرسل: </label><input required class="form-control" type="text" name="mursal" /></td><td>
    <label>مرسل الیه: </label><input required class="form-control" type="text" name="mursal_elaih" /></td>
    </tr>

    <tr><td>
    <label>نمبر رسیدات: </label><input required class="form-control" type="text" name="rasidat_no" /></td><td>
    <label>تاریخ رسیدات: </label><input required class="form-control datepicker_farsi" class="form-control" type="text" name="rasidat_date" /></td>
    </tr>

    <tr><td colspan="2">
    <label>ملاحظات: </label><textarea  class="form-control" required type="text" name="mulahizat" ></textarea></td><td>
    </tr>

    <tr><td>
    <input class="btn btn-success" type="submit" value="ثبت جدید" /></td>
    </tr>

    </table>

</form>
</div>
</div>

@stop