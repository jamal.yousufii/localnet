@extends('layouts.master')
@section('head')
    <title>جزئیات</title>
@stop

@section('content')

<div class="finance">


@if(Session::has('success'))
{{Session::get('success')}}
@endif

@if(Session::has('failed'))
{{Session::get('failed')}}
@endif

<!-- {!! HTML::style('/css/font.css') !!} -->
<style>
    .finance  {font-family:Noori;direction:rtl;background:#fff;border:10px solid #fff;}
    .finance h1 {padding:10px;border-bottom:1px dashed #aaa;}
    .finance table {font-weight: bold;font-size:110%;width:100%;background:#fff;}
    .finance label {padding:0 5px;font-weight: bold;}
    .new_info input[type=text], .new_info select, .new_info textarea {font-weight:bold;font-size:105%;border:1px solid #ccc;width:100%;padding:5px;margin:5px;}
    .new_info textarea {height:5em;transition:all 0.3s;}
    .new_info textarea:focus{height:10em;transition:all 0.3s;}
    .new_info table {width:100%;}
    .new_info td {padding:10px 30px;border:1px solid #ddd;vertical-align:top}
    #config-tool {display:none !important;}
    input[type=submit] {font-weight: bold !important;}
    .btn-back {margin:20px 47%;}

</style>

<style rel="stylesheet" media="print">
    .noprint {display:none;}
    #theme-wrapper {position:fixed;left:-2000px;top:0;width:0;height:0;}
    .finance {width:100%;display:block !important;position:fixed;right:0;top:0;}
    a[href]:after {content: none !important;}
</style>


<div class="finance new_info noorifont">

@foreach($record AS $item)
<h1>جزئیات شماره مسلسل:  {{$item->serial}} </h1>
@endforeach

<form id="new_info" action="" method="post">
    <table>

@foreach($record AS $item)
    <tr><td>
    <label>شماره مسلسل: </label> {{$item->serial}}</td><td>
    <label>اسم مرجع مربوطه: </label> {{$item->marji_marbota}}</td>
    <td><label>نمبرحکم: </label> {{$item->hokum_no}}</td>
    </tr>

    <tr><td>
    <label>تاریخ حکم: </label> {{$item->hokum_date}}</td><td>
    <label>نمبرمکتوب: </label> {{$item->maktob_no}}</td><td>
    <label>تاریخ مکتوب: </label> {{$item->maktob_date}}</td>
    </tr>

    <tr><td>
    <label>صادره/وارده: </label> {{$item->sadira_warida}}</td><td>
    <label>تاریخ صادره/وارده: </label> {{$item->sadira_warida_date}}</td><td>
    <label>مرسل: </label> {{$item->mursal}}</td>
    </tr>

    <tr><td>
    <label>مرسل الیه: </label> {{$item->mursal_elaih}}</td><td>
    <label>نمبر رسیدات: </label> {{$item->rasidat_no}}</td><td>
    <label>تاریخ رسیدات: </label> {{$item->rasidat_date}}</td>
    </tr>

    <tr><td colspan="3">
    <label>ملاحظات: </label> {{$item->mulahizat}}</textarea></td>
    </tr>

@endforeach


    </table>
    <a href="javascript:window.history.back();" class="btn noprint btn-warning btn-back" > برگشت </a>

</form>
</div>
</div>




@stop
