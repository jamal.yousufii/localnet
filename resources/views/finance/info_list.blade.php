@extends('layouts.master')
@section('head')
    <title>لست صادرات / واردات</title>
@stop

@section('content')

<div class="finance">
@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success  noprint'>{{Session::get('success')}}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger  noprint'>{{Session::get('fail')}}</div>
    @endif

@if(Session::has('success'))
{{Session::get('success')}}
@endif

@if(Session::has('failed'))
{{Session::get('failed')}}
@endif

<!-- {!! HTML::style('/css/font.css') !!} -->
<style>
    .finance  {font-family:Noori !important;background:#fff;border:10px solid #fff;}
    .finance h1 {padding:10px;border-bottom:1px dashed #aaa;}
    .finance table {width:100%;border:1px solid #ddd;background:#fff;}
    .finance td {padding:10px 30px;font-size:103% !important;vertical-align:top;}
    .finance th {font-weight: bold;text-align: right;background:#eee; }
    #config-tool {display:none !important;}
    .finance h1 {padding:5px 10px;}
</style>
<style rel="stylesheet" media="print">
    .noprint {display:none;}
    #theme-wrapper {position:fixed;left:-2000px;top:0;width:0;height:0;}
    .finance {width:100%;display:block !important;position:fixed;right:0;top:0;}
    a[href]:after {content: none !important;}
</style>

<div style="direction:rtl;" class="finance">


<h1>لست صادره/وارده
<a href="{!!URL::to('finance/new_info')!!}" class="btn btn-success pull-left"><i class="fa fa-plus-circle fa-lg"></i> اضافه کردن ریکارد جدید</a>
</h1>

<form action="" method="post">


    <table class="table table-datatable" id="info_list">
        <thead>
            <tr>
                <th>شماره مسلسل</th>
                <th>صادره/وارده</th>
                <th>اسم مرجع مربوطه</th>
                <th>تاریخ حکم</th>
                <th>مرسل</th>
                <th>مرسله الیه</th>
                <th>یشتر</th>
            </tr>
        </thead>

        <tbody>
        </tbody>

    </table>

</form>
</div>
</div>

@stop
@section('footer-scripts')
<script type="text/javascript">
$('#info_list').dataTable(
	{
		"bProcessing": true,
		"bServerSide": true,
		//"iDisplayLength": 2,
		"sAjaxSource": "{!!URL::route('financeGetData')!!}",
		"aaSorting": [[ 1, "desc" ]]
	}
);
</script>

@stop