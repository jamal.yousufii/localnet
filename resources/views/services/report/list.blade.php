
		<table class="table table-responsive table-bordered" id="list">
			<thead>
				<tr>
                    <th>#</th>
		            <th>{!!_('name')!!}</th>
		            <th>{!!_('amount')!!}</th>
		            <th>{!!_('unit')!!}</th>
	            </tr>
			</thead>
			<tbody>

                <?php $counter = 1;//$rows->firstItem(); ?>
                @foreach($rows AS $item)
                    <tr>
                    <td>{!!$counter!!}</td>
                    <td>{!!$item->name!!}</td>
                    <td>{!!$item->total!!}</td>
                    <td>{!!$item->unit!!}</td>
                    
                   
                    </tr>
                    <?php $counter++; ?>
                @endforeach
				
			</tbody>
		</table>
		
		<div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
			
		</div>

<script>
	// $( document ).ready(function() {
	// 	$('.pagination a').on('click', function(event) {
	// 		event.preventDefault();
	// 		if ($(this).attr('href') != '#') {
	// 			//$('#ajaxContent').load($(this).attr('href'));
				
	// 			$.ajax({
	// 	                url: '{!!URL::route("getServiceReportResult")!!}',
	// 	                data: $('#search_form').serialize()+"&page="+$(this).text()+"&ajax=1",
	// 	                type: 'post',
	// 	                beforeSend: function(){
		
	// 	                    //$("body").show().css({"opacity": "0.5"});
	// 	                    $('#search_result_area').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	// 	                },
	// 	                success: function(response)
	// 	                {
		
	// 	                    $('#search_result_area').html(response);
	// 	                }
	// 	            }
	// 	        );
			
	// 		}
	// 	});
	// });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').DataTable({
            "pageLength": 20,
            dom: 'Bfrtip',
            buttons: ['csv','excel','pdf','print']
        });
    });
</script>


