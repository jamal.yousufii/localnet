@extends('layouts.master')

@section('head')
	<title>{!!_('report_form')!!}</title>
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::style('/css/datatable/buttons.dataTables.min.css') !!}
	<style>
    <title>{!!_('report_panel')!!}</title>
    <style>
    	#search_area{
    		padding: 10px;
    		border: 1px solid #eee;
    		margin-top: 10px;
    	}
    </style>
@stop
@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="main-box">
			<header class="main-box-header clearfix">
			    <h2>{!!_('report_panel')!!}</h2>
			    @if(Session::has('success'))
			    <span class='alert alert-success' style="width:400px;">
			        <i class="fa fa-check-circle fa-fw fa-lg"></i>
			        {!!Session::get('success')!!}
			    </span>
			    @elseif(Session::has('fail'))
			    <span class='alert alert-danger' style="width:400px;">
			        <i class="fa fa-times-circle fa-fw fa-lg"></i>
			        {!!Session::get('fail')!!}
			    </span>
			    @endif
			    <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en")
			    	{
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}">
			    	<a href="javascript:void()" class="btn btn-direction btn-bottom btn-success btn-outline" id="toggle_btn"><i class="icon fa-search fa-lg" aria-hidden="true"></i> {!!_("advance_search_form")!!}</a>
			    </span>
		
			</header>
			<br>
			<div id="search_area" class="well">
				<form action="{!!URL::route('getServiceReportResult')!!}" id="search_form" role="form" method="post" enctype="multipart/form-data">
                    <div class="row">
                        	<div class="form-group col-xs-6">
                                <label class="control-label">{!!_("start_date")!!}</label>
                                <input type="text" name="start_date" id="start_date" class="form-control {!!getDatePickerClass()!!}">
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("end_date")!!}</label>
                                <input type="text" name="end_date" id="end_date" class="form-control {!!getDatePickerClass()!!}">
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("type")!!}</label>
                                <select name="type" id="type" class="form-control" onchange="bringRelated('fees9_item',this.value)">
                                    {!!getStaticTable("fees9_types","services",old('type'))!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('type') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("item_description")!!}</label>
                                <select name="fees9_item[]" id="fees9_item" class="form-control" style="width: 100%;" multiple="multiple">
                        			<option value="">{!!_('select_item_description')!!}</option>
                    				@foreach($items AS $item)
              							<option value='{!!$item->id!!}'>{!!$item->item_description!!}</option>
                    				@endforeach
                    			</select>
                            </div>
                           
                            <div class="form-group col-xs-6" style="padding-left: 210px;padding-right: 20px;">
                            
                            	<label class="control-label"> </label><br>
                                <input type="checkbox" value="1" name="daily_specail" id="	daily_specail" /> <label for="	daily_specail">{!!_("daily_specail")!!}</label> 
                                <br> 
                                <input type="checkbox" value="1" name="daily_employee" id="daily_employee" /> <label for="daily_employee">{!!_("daily_employee")!!}</label> 
                            	<br>
                            	<input type="checkbox" value="1" name="daily_employee_3" id="daily_employee_3" /> <label for="daily_employee_3">{!!_("daily_employee_3")!!}</label> 
                            	<br>
                            	<input type="checkbox" value="1" name="other" id="other" /> <label for="other">{!!_("other")!!}</label> 
                            </div>
                        
                    </div>
                    
                    <div class="row">
                        <div class="form-group col-xs-1">
                            <a href="javascript:void()" class="btn btn-primary" id="search_btn"><i class="fa fa-search fa-lg"></i> {!!_('search')!!}</a>
                        </div>
                        
                    </div>
                </form>
			</div>
			
			<div class="main-box-body clearfix" id="search_result_area">
				Search Result Area ...
			</div>
		</div>
	</div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
{!! HTML::script('/js/datatable/dataTables.tableTools.min.js')!!}
{!! HTML::script('/js/datatable/dataTables.buttons.min.js')!!}
{!! HTML::script('/js/datatable/jszip.min.js')!!}
{!! HTML::script('/js/datatable/buttons.html5.min.js')!!}
{!! HTML::script('/js/datatable/buttons.print.min.js')!!}
 
<script type="text/javascript">

$(document).ready(function(){
		$("#fees9_item").select2();
		
});
    
</script>
<script>
	$( document ).ready(function() {
		$('#search_btn').on('click', function(event) {
			event.preventDefault();
			$.ajax({
	                url: '{!!URL::route("getServiceReportResult")!!}',
	                data: $('#search_form').serialize(),
	                type: 'post',
	                beforeSend: function(){
	
	                    //$("body").show().css({"opacity": "0.5"});
	                    $('#search_result_area').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
						$("#search_area").slideUp();
	                    $('#search_result_area').html(response);
	                }
	            }
	        );
		});
		
		$("#toggle_btn").on("click", function(){
			$("#search_area").toggle("slow");
		});
		
		$("#monthly").on("change",function(){
			
			if($(this).val() != ""){
				$("#start_date").val("");
				$("#start_date").prop("disabled",true);
				$("#end_date").val("");
				$("#end_date").prop("disabled",true);
			}
			else{
				$("#start_date").prop("disabled",false);
				$("#end_date").prop("disabled",false);

			}
			
		});
	});
	
	function bringRelated(element,id)
    {
        $.ajax({
                url: '{!!URL::route("getServiceReportRelatedFees9Types")!!}',
                data: '&id='+id,
                type: 'post',
                dataType: 'html',
                beforeSend: function(){
                    $("#"+element).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+element).html(response);
                }
            }
        );
    }
    
</script>
@stop




