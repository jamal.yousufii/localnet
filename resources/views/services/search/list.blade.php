@extends('layouts.master')

@section('head')
	<title>{!!_('invitation_insert')!!}</title>
    {!! HTML::style('/vendor/clockpicker/clockpicker.min.css') !!}
    {!! HTML::style('/vendor/select2/select2.css') !!}

	<style>
    <title>{!!_('search_panel')!!}</title>
    <style>
    	#search_area{
    		padding: 10px;
    		border: 1px solid #eee;
    		margin-top: 10px;
    	}
    </style>
@stop
@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="main-box">
			<header class="main-box-header clearfix">
			    <h2>{!!_('search_panel')!!}</h2>
			    @if(Session::has('success'))
			    <span class='alert alert-success' style="width:400px;">
			        <i class="fa fa-check-circle fa-fw fa-lg"></i>
			        {!!Session::get('success')!!}
			    </span>
			    @elseif(Session::has('fail'))
			    <span class='alert alert-danger' style="width:400px;">
			        <i class="fa fa-times-circle fa-fw fa-lg"></i>
			        {!!Session::get('fail')!!}
			    </span>
			    @endif
			    <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en")
			    	{
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}">
			    	<a href="javascript:void()" class="btn btn-direction btn-bottom btn-success btn-outline" id="toggle_btn"><i class="icon fa-search fa-lg" aria-hidden="true"></i> {!!_("advance_search_form")!!}</a>
			    </span>
			</header>
			<br>
			<div id="search_area" class="well">
				<form action="{!!URL::route('getServiceSearchResult')!!}" id="search_form" role="form" method="post" enctype="multipart/form-data">
                    <div class="row">
                        
                        <div class="form-group col-xs-12">
                        	<label class="control-label">{!!_("search_category")!!}</label>
                            <select name="category_type" id="category_type" class="form-control">
                            	<option value=''>---</option>
                                <option value="1">کارمندان</option>
                                <option value="2">دعوتی ها</option>
                                <option value="3"> ف س ۹</option>
                            </select>
                        </div>
                        
                        
                        <div id="category_employee" style="display: none;">
                        	<div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("name")!!}</label>
                                <input value="{!! old('name') !!}" type="text" name="name" id="name" class="form-control">
                                <span style="color:red;">{!! $errors->first('name') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("father_name")!!}</label>
                                <input value="{!! old('father_name') !!}" type="text" name="father_name" id="father_name" class="form-control">
                                <span style="color:red;">{!! $errors->first('father_name') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("start_date")!!}</label>
                                <input type="text" value="{!! old('start_date') !!}" name="start_date" id="start_date" class="form-control {!!getDatePickerClass()!!}">
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("end_date")!!}</label>
                                <input type="text" value="{!! old('end_date') !!}" name="end_date" id="end_date" class="form-control {!!getDatePickerClass()!!}">
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("position")!!}</label>
                                <input value="{!! old('position') !!}" type="text" name="position" id="position" class="form-control">
                                <span style="color:red;">{!! $errors->first('position') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("ID_card")!!}</label>
                                <input value="{!! old('ID_card') !!}" type="text" name="ID_card" id="ID_card" class="form-control">
                                <span style="color:red;">{!! $errors->first('ID_card') !!}</span> 
                            </div>
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("palace_id")!!}</label>
                                <select name="palace_id" id="palace_id" class="form-control">
                                    {!!getStaticTable("palaces","services",old('palace_id'))!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('palace_id') !!}</span>
                            </div>
                            <div class="form-group col-xs-6" style="padding-left: 210px;padding-right: 20px;">
                            	<label class="control-label"> </label><br>
                                <input type="checkbox" value="1" name="is_general_manager" id="is_general_manager" /> &nbsp;&nbsp;<label for="is_general_manager">{!!_("is_general_manager")!!}</label> 
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" value="1" name="is_palace_manager" id="is_palace_manager" /> &nbsp;&nbsp;<label for="is_palace_manager">{!!_("is_palace_manager")!!}</label> 
                            </div>
                        </div>
                        
                        <div id="category_invitation" style="display: none;">
                        	<div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("type_of_invitation")!!}</label>
                                <select name="type" id="type" class="form-control">
                                	<option value=''>---</option>
                                    <option value="1">عادی</option>
                                    <option value="2">رسمی</option>
                                </select>
                                <span style="color:red;">{!! $errors->first('type') !!}</span>
                            </div>
                            
                            <div class="form-group col-xs-3">
                            	<label class="control-label">{!!_("date")!!}</label>
                                <input type="text" value="{!! old('date') !!}" name="date" id="date" class="form-control {!!getDatePickerClass()!!}">
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">{!!_("time")!!}</label>
                            	<div class="input-group clockpicker-wrap" data-plugin="clockpicker" data-autoclose="true">
			                    <input class="form-control" type="text" value="{!! old('time') !!}" name="time" id="time">
				                    <span class="input-group-addon">
				                      <span class="wb-time"></span>
				                    </span>
			                    </div>
                            </div>
                           
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("number_of_guest")!!}</label>
                                <input value="{!! old('number_of_guest') !!}" type="text" name="number_of_guest" id="number_of_guest" class="form-control">
                                <span style="color:red;">{!! $errors->first('number_of_guest') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("source")!!}</label>
                                <input value="{!! old('source') !!}" type="text" name="source" id="source" class="form-control">
                                <span style="color:red;">{!! $errors->first('source') !!}</span> 
                            </div>
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("location")!!}</label>
                                <select name="location" id="location" class="form-control">
                                    {!!getStaticTable("palaces","services",old('location'))!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('location') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("maktob_number")!!}</label>
                                <input value="{!! old('maktob_number') !!}" type="text" name="maktob_number" id="maktob_number" class="form-control">
                                <span style="color:red;">{!! $errors->first('maktob_number') !!}</span> 
                            </div>
                        </div>
                        
                        <div id="category_fees9" style="display: none;">
                        	<div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("form_number")!!}</label>
                                <input value="{!! old('form_number') !!}" type="text" name="form_number" id="form_number" class="form-control">
                                <span style="color:red;">{!! $errors->first('form_number') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("date")!!}</label>
                                <input type="text" value="{!! old('date') !!}" name="date" id="date" class="form-control {!!getDatePickerClass()!!}">
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("type")!!}</label>
                                <select name="type" id="type" class="form-control">
                                    {!!getStaticTable("fees9_types","services",old('type'))!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('type') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("item_description")!!}</label>
                                <select name="fees9_item[]" id="fees9_item" class="form-control" style="width: 100%;" multiple="multiple">
                        			<option value="">{!!_('select_item_description')!!}</option>
                    				@foreach($items AS $item)
              							<option value='{!!$item->id!!}'>{!!$item->item_description!!}</option>
                    				@endforeach
                    			</select>
                            </div>
                            
                            <div class="form-group col-xs-6" style="padding-left: 210px;padding-right: 20px;">
                            
                            	<label class="control-label"> </label><br>
                                <input type="checkbox" value="1" name="daily_specail" id="	daily_specail" /> <label for="	daily_specail">{!!_("daily_specail")!!}</label> 
                                <br> 
                                <input type="checkbox" value="1" name="daily_employee" id="daily_employee" /> <label for="daily_employee">{!!_("daily_employee")!!}</label> 
                            	<br>
                            	<input type="checkbox" value="1" name="daily_employee_3" id="daily_employee_3" /> <label for="daily_employee_3">{!!_("daily_employee_3")!!}</label> 
                            	<br>
                            	<input type="checkbox" value="1" name="other" id="other" /> <label for="other">{!!_("other")!!}</label> 
                            </div>
                        </div>
                    
                    </div>
                    
                    <div class="row">
                        <div class="form-group col-xs-1">
                            <a href="javascript:void()" class="btn btn-primary" id="search_btn"><i class="fa fa-search fa-lg"></i> {!!_('search')!!}</a>
                        </div>
                        
                    </div>
                </form>
			</div>
			
			<div class="main-box-body clearfix" id="search_result_area">
				Search Result Area ...
			</div>
		</div>
	</div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')
{!! HTML::script('/vendor/clockpicker/bootstrap-clockpicker.min.js') !!}
{!! HTML::script('/js/components/clockpicker.min.js') !!}
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
<script type="text/javascript">

$(document).ready(function(){
		$("#fees9_item").select2();
		$("#category_type").on("change",function(){
			if($(this).val() == 1){
				$("#category_invitation").slideUp("slow");
				$("#category_fees9").slideUp("slow");
				$("#category_employee").slideDown("slow");
			}else if($(this).val() == 2){
				$("#category_fees9").slideUp("slow");
				$("#category_employee").slideUp("slow");
				$("#category_invitation").slideDown("slow");
			}else if($(this).val() == 3){
				$("#category_employee").slideUp("slow");
				$("#category_invitation").slideUp("slow");
				$("#category_fees9").slideDown("slow");
			}else{
				$("#category_employee").slideUp("slow");
				$("#category_invitation").slideUp("slow");
				$("#category_fees9").slideUp("slow");
			}
		});
		
		$("#toggle_btn").on("click", function(){
			$("#search_area").toggle("slow");
		});
});
    
</script>
<script>
	$( document ).ready(function() {
		$('#search_btn').on('click', function(event) {
			event.preventDefault();
			$.ajax({
	                url: '{!!URL::route("getServiceSearchResult")!!}',
	                data: $('#search_form').serialize(),
	                type: 'post',
	                beforeSend: function(){
	
	                    //$("body").show().css({"opacity": "0.5"});
	                    $('#search_result_area').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
						$("#search_area").slideUp();
	                    $('#search_result_area').html(response);
	                }
	            }
	        );
		});
	});
</script>
@stop

