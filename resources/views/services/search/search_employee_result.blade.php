
		<table class="table table-responsive table-bordered" id="list">
			<thead>
				<tr>
                    <th>#</th>
		            <th>{!!_('name')!!}</th>
		            <th>{!!_('father_name')!!}</th>
		            <th>{!!_('position')!!}</th>
		            <th>{!!_('start_date')!!}</th>
		            <th>{!!_('end_date')!!}</th>
		            <th>{!!_('ID')!!}</th>
		            <th>{!!_('palace')!!}</th>
		            <th>{!!_('operation')!!}</th>
	            </tr>
			</thead>
			<tbody>

                <?php $counter = $rows->firstItem(); ?>
                @foreach($rows AS $item)
                    <tr>
                    <td>{!!$counter!!}</td>
                    <td>{!!$item->name!!}</td>
                    <td>{!!$item->father_name!!}</td>
                    <td>{!!$item->position!!}</td>
                    
                    <td>{!!$item->start_date!!}</td>
                    <td>{!!$item->end_date!!}</td>
                    <td>{!!$item->employee_id!!}</td>
                    <td>{!!$item->palace!!}</td>
                    <td>
                        <a href="{!!route('getEditServiceEmployee',$item->id)!!}" class="table-link" target="_blank">
                            <i class="icon wb-edit" aria-hidden="true"></i></a>
                        </a>
                    </td>
                    </tr>
                    <?php $counter++; ?>
                @endforeach
				
			</tbody>
		</table>
		
		<div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
			{!!$rows->render()!!}
		</div>

<script>
	$( document ).ready(function() {
		$('.pagination a').on('click', function(event) {
			event.preventDefault();
			if ($(this).attr('href') != '#') {
				//$('#ajaxContent').load($(this).attr('href'));
				
				$.ajax({
		                url: '{!!URL::route("getServiceSearchResult")!!}',
		                data: $('#search_form').serialize()+"&page="+$(this).text()+"&ajax=1",
		                type: 'post',
		                beforeSend: function(){
		
		                    //$("body").show().css({"opacity": "0.5"});
		                    $('#search_result_area').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
		                },
		                success: function(response)
		                {
		
		                    $('#search_result_area').html(response);
		                }
		            }
		        );
			
			}
		});
	});
</script>

