
		<table class="table table-responsive table-bordered" id="list">
			<thead>
				<tr>
                    <th>#</th>
		            <th>{!!_('date')!!}</th>
		            <th>{!!_('time')!!}</th>
		            <th>{!!_('type_of_invitation')!!}</th>
		            <th>{!!_('number_of_guest')!!}</th>
		            <th>{!!_('maktob_number')!!}</th>
		            <th>{!!_('location')!!}</th>
		            <th>{!!_('source')!!}</th>
		            <th>{!!_('operation')!!}</th>
	            </tr>
			</thead>
			<tbody>

                <?php $counter = $rows->firstItem(); ?>
                @foreach($rows AS $item)
                    <tr>
                    <td>{!!$counter!!}</td>
                    <td>{!!$item->date!!}</td>
                    <td>{!!$item->time!!}</td>
                    <td>{!!$item->type!!}</td>
                    <td>{!!$item->number_of_guest!!}</td>
                    <td>{!!$item->maktob_number!!}</td>
                    <td>{!!$item->location!!}</td>
                    <td>{!!$item->source!!}</td>
                    <td>
                        <a href="{!!route('getEditServiceInvitation',$item->id)!!}" class="table-link" target="_blank">
                            <i class="icon wb-edit" aria-hidden="true"></i></a>
                        </a>
                    </td>
                    </tr>
                    <?php $counter++; ?>
                @endforeach
				
			</tbody>
		</table>
		
		<div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
			{!!$rows->render()!!}
		</div>

<script>
	$( document ).ready(function() {
		$('.pagination a').on('click', function(event) {
			event.preventDefault();
			if ($(this).attr('href') != '#') {
				//$('#ajaxContent').load($(this).attr('href'));
				
				$.ajax({
		                url: '{!!URL::route("getServiceSearchResult")!!}',
		                data: $('#search_form').serialize()+"&page="+$(this).text()+"&ajax=1",
		                type: 'post',
		                beforeSend: function(){
		
		                    //$("body").show().css({"opacity": "0.5"});
		                    $('#search_result_area').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
		                },
		                success: function(response)
		                {
		
		                    $('#search_result_area').html(response);
		                }
		            }
		        );
			
			}
		});
	});
</script>

