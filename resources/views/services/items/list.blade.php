@extends('layouts.master')

@section('head')

    <title>{!!_('item_list')!!}</title>
@stop
@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="main-box">
			<header class="main-box-header clearfix">
			    <h2>{!!_('item_list')!!}</h2>
			    @if(Session::has('success'))
			    <span class='alert alert-success' style="width:400px;">
			        <i class="fa fa-check-circle fa-fw fa-lg"></i>
			        {!!Session::get('success')!!}
			    </span>
			    @elseif(Session::has('fail'))
			    <span class='alert alert-danger' style="width:400px;">
			        <i class="fa fa-times-circle fa-fw fa-lg"></i>
			        {!!Session::get('fail')!!}
			    </span>
			    @endif
			    <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en")
			    	{
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="javascript:void()" data-target="#details_modal" data-toggle="modal" class="btn btn-success"><i class="icon fa-plus-circle fa-lg" aria-hidden="true"></i> {!!_("add_new")!!}</a></span>
			</header>
			
			<div class="main-box-body clearfix">
				<div class="table-responsive">
				    <table class="table table-responsive table-bordered" id='list'>
				        <thead>
				        <tr>
				            <th>#</th>
				            <th>{!!_('item_description')!!}</th>
				            <th>{!!_('unit')!!}</th>
				            <th>{!!_('fees9_type')!!}</th>
				            <th>{!!_('operation')!!}</th>
				        </tr>
				        </thead>
				        <tbody>
				        </tbody>
				    </table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="details_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    	<div class="modal-dialog">
    		<div class="modal-content">
    
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        <span aria-hidden="true">×</span>
		    </button>
            <h4 class="modal-title"><i class="fa fa-item fa-lg"></i> {!!_('new_item')!!}</h4>
        </div>
        <div class="modal-body">
            <form role="form" class='form-horizontal' id="new_item_frm" name="new_item_frm" style="padding-right:10px;">
                
                <div class="form-group">
                    <label class="col-sm-4 control-label">{!!_('item_description')!!}</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" name="item_description" id="item_description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">{!!_('unit')!!}</label>
                    <div class="col-sm-8">
                        <select name="unit" id="unit" class="form-control">
                        	{!!getStaticTable('mesure_units','services')!!}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">{!!_('fees9_type')!!}</label>
                    <div class="col-sm-8">
                        <select name="fees9_type" id="fees9_type" class="form-control">
                        	{!!getStaticTable('fees9_types','services')!!}
                        </select>
                    </div>
                </div>
                
          </form>
        </div>
        <div class="modal-footer" id='create_footer'>
            <button id="btn_save" onclick='saveItem();' type="button" class="btn btn-primary modal-submit">{!!_('save')!!}</button>
        </div>
    </div>
    </div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {

                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                //"iDisplayLength": 2,
                "sAjaxSource": "{!!URL::route('getServiceItemData')!!}",
                "aaSorting": [[ 0, "desc" ]],
                
                "language": {
                    "lengthMenu": "{!!_('view')!!} _MENU_ {!!_('record_per_page')!!}",
                    "zeroRecords": "{!!_('record_not_found')!!}",
                    "info": "{!!_('page_view')!!} _PAGE_ {!!_('of')!!} _PAGES_",
                    "infoEmpty": "{!!_('record_not_found')!!}",
                    "search": "{!!_('search')!!}",
                    "infoFiltered": "(filtered {!!_('of')!!} _MAX_ {!!_('total_record')!!})"
                }
            }
        );

    });
    
    function saveItem()
    {

        var page = "{!!URL::route('saveServiceItem')!!}";
        
        $.ajax({
            url: page,
            type: 'post',
            data: $('#new_item_frm').serialize(),
            //dataType:'HTML',
            success: function(response)
            {
            	$("#details_modal").modal("hide");
                window.location.reload();
            },
            error:function(err)
            {
                alert(err);
            }
        });

        return false;
        
    }
</script>
@stop

