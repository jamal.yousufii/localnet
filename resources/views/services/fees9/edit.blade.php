@extends('layouts.master')
@section('head')

	{!! HTML::style('/vendor/select2/select2.css') !!}

    <title>{!!_('fees9_edit')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
    <span class='alert alert-success' style="width:400px;">
        <i class="fa fa-check-circle fa-fw fa-lg"></i>
        {!!Session::get('success')!!}
    </span>
    @elseif(Session::has('fail'))
    <span class='alert alert-danger' style="width:400px;">
        <i class="fa fa-times-circle fa-fw fa-lg"></i>
        {!!Session::get('fail')!!}
    </span>
    @endif
			    
    <div class="col-lg-12">

        <h1>{!!_('fees9_edit')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
		?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getServiceFees9List')!!}" class="btn btn-success">{!!_("back")!!}</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('updateServiceFees9',$row->id)!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            
                            <div class="form-group col-xs-12">
                            	<label class="control-label">{!!_("form_number")!!}</label>
                                <input value="{!! $row->form_number !!}" type="text" name="form_number" id="form_number" class="form-control" style="width: 500px;">
                                <span style="color:red;">{!! $errors->first('form_number') !!}</span>
                            </div>
                            <div class="form-group col-xs-12">
                            	<label class="control-label">{!!_("date")!!}</label>
                            	@if(isMiladiDate())
                                <input value="<?php echo ($row->date != '0000-00-00') ? toGregorian($row->date):''; ?>" type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}" style="width: 500px;">
                            	@else
                            	<input value="<?php echo ($row->date != '0000-00-00') ? dmy_format($row->date):''; ?>" type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}" style="width: 500px;">
                            	@endif
                            </div>
                            <div class="form-group col-xs-12">
                            	<label class="control-label">{!!_("type")!!}</label>
                                <select name="type" id="type" class="form-control" style="width: 500px;">
                                    {!!getStaticTable("fees9_types","services",$row->type)!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('type') !!}</span>
                            </div>
                            
                            <div class="form-group col-xs-12" id="items" style="border: 1px solid #eee;padding: 10px;">
                            
                            	<div id="item_hidden" class="my_row" style="display:none;">
                            		<div class="form-group col-xs-4">
	                            		<select name="items[]" style="width: 400px;" class="form-control">
	                            			<option value="">{!!_('select_item_description')!!}</option>
	                        				@foreach($items AS $item)
	                  							<option value='{!!$item->id!!}'>{!!$item->item_description!!}</option>
	                        				@endforeach
	                        			</select>
	                        		</div>
	                        		<div class="form-group col-xs-4">
	                        			<select name="unit[]" style="width: 400px;" class="form-control">
		                            		{!!getStaticTable("mesure_units","services",old('unit'))!!}
		                            	</select>
	                        		</div>
	                        		<div class="form-group col-xs-3">
	                        			<input type="text" name="amount[]" placeholder="Enter amount ..." class="form-control" />
	                        		</div>
	                        		<div class="form-group col-xs-1">
										<button type="button" onclick="repeatRow();" class="btn btn-floating btn-success btn-xs"><i class="icon wb-plus" aria-hidden="true"></i></button>
										<button type="button" onclick="$(this).parents().eq(1).remove();" class="btn btn-floating btn-danger btn-xs"><i class="icon wb-minus" aria-hidden="true"></i></button>
	                        		</div>
                            	</div>
                            
                            
                            <?php $i=0; ?>
            				@foreach($selected_items AS $selected)
            				<?php $i++; ?>
	                            <div id="item_1" class="my_row">
	                            		<div class="form-group col-xs-4">
		                            		<select name="items[]" style="width: 400px;" class="form-control">
		                            			<option value="">{!!_('select_item_description')!!}</option>
		                        				@foreach($items AS $item)
		                  							<option value='{!!$item->id!!}' {!!($selected->item_id == $item->id?"selected":"")!!}>{!!$item->item_description!!}</option>
		                        				@endforeach
		                        			</select>
		                        		</div>
		                        		<div class="form-group col-xs-4">
		                        			<select name="unit[]" style="width: 400px;" class="form-control">
			                            		{!!getStaticTable("mesure_units","services",$selected->unit)!!}
			                            	</select>
		                        		</div>
		                        		<div class="form-group col-xs-3">
		                        			<input type="text" name="amount[]" value="{!!$selected->amount!!}" placeholder="Enter amount ..." class="form-control" />
		                        		</div>
		                        		<div class="form-group col-xs-1">
											<button type="button" onclick="repeatRow();" class="btn btn-floating btn-success btn-xs"><i class="icon wb-plus" aria-hidden="true"></i></button>
											<button type="button" onclick="$(this).parents().eq(1).remove();" class="btn btn-floating btn-danger btn-xs"><i class="icon wb-minus" aria-hidden="true"></i></button>
		                        		</div>
	                            	</div>
	                            @endforeach
	                         </div>
	                        

                           
                            
                            <div class="form-group col-xs-6" style="padding-left: 210px;padding-right: 20px;">
                            
                            	<label class="control-label"> </label><br>
                                <input type="checkbox" {!!($row->daily_specail == 1?"checked":"")!!} value="1" name="daily_specail" id="	daily_specail" /> <label for="	daily_specail">{!!_("daily_specail")!!}</label> 
                                <br> 
                                <input type="checkbox" {!!($row->daily_employee == 1?"checked":"")!!} value="1" name="daily_employee" id="daily_employee" /> <label for="daily_employee">{!!_("daily_employee")!!}</label> 
                            	<br>
                            	<input type="checkbox" {!!($row->daily_employee_3 == 1?"checked":"")!!} value="1" name="daily_employee_3" id="daily_employee_3" /> <label for="daily_employee_3">{!!_("daily_employee_3")!!}</label> 
                            	<br>
                            	<input type="checkbox" {!!($row->other == 1?"checked":"")!!} value="1" name="other" id="other" /> <label for="other">{!!_("other")!!}</label> 
                            </div>
                            
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('update')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                </div>
                
                 <div class="panel">
                    <div class="panel-heading" id="exampleHeadingContinuousTwo" role="tab" style="text-align: center;background-color: rgb(98, 168, 234);">
                      <a class="panel-title collapsed" style="color: white;font-weight: bold;" data-parent="#exampleAccordionContinuous" data-toggle="collapse" href="#exampleCollapseContinuousTwo" aria-controls="exampleCollapseContinuousTwo" aria-expanded="false">
                      <i class="icon fa-paperclip fa-lg" aria-hidden="true"></i> {!!_("fees9_attachments")!!}
                      </a>
                    </div>
                    <div style="height: 0px;direction: rtl;" aria-expanded="false" class="panel-collapse collapse" id="exampleCollapseContinuousTwo" aria-labelledby="exampleHeadingContinuousTwo" role="tabpanel">
                      <div class="panel-body" style="border: 1px solid rgb(98, 168, 234);">
                        	<table class="attach_table table table-bordered">
                        		<tr>
                        			<td>#</td>
                        			<td>{!!_("file_name")!!}</td>
                        			<td colspan="2">{!!_("operation")!!}</td>
                        		</tr>
                        		@if($attachments)
                        			<?php $counter = 1; ?>
                        			@foreach($attachments AS $item)
		                        		<tr>
		                        			<td>{!!$counter!!}</td>
		                        			<td><a href="{!!URL::route('getDownloadServiceEmpDoc',$item->file_name)!!}"><i class="icon fa-paperclip fa-lg" aria-hidden="true"></i> {!!$item->file_name!!}</a></td>
		                        			<td><a href="{!!URL::route('getDeleteServiceFees9Doc',array($item->file_name,$item->id,$item->record_id))!!}" onclick="return confirm('آیا میخواهید ضمیمه را حذف نمایید؟');"><i class="icon fa-trash fa-lg" aria-hidden="true"></i> {!!_("remove")!!}</a></td>
		                        			<td><a href="{!!URL::route('getDownloadServiceEmpDoc',$item->file_name)!!}"><i class="icon fa-download fa-lg" aria-hidden="true"></i> {!!_("download")!!}</a></td>
		                        		</tr>
		                        		<?php $counter++; ?>
		                        	@endforeach
		                        @endif
                        	</table>
                        	<form class="form-horizontal" role="form" method="post" action="{!!URL::route('uploadServicesFees9Docs',$row->id)!!}" enctype="multipart/form-data">
	                        	<div class="form-group">
			                        <label class="col-sm-2 control-label">{!!_('upload_new_attachment')!!}</label>
			                        <div class="col-sm-4" id='files_div'>
			                            <input style='width:400px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
			                        </div>
			                        
			                    </div>
			                    <input class="col-sm-6 btn btn-success" id="upload_attachment" type="submit" value='{!!_("upload_attachment")!!}' />
		                    </form>
                        	
                      </div>
                    </div>
                  </div>
                  
            </div>
        </div>
    </div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
<script>
	$(document).ready(function(){
		$("#item_1 select").select2();
		$("#upload_attachment").prop("disabled",true);
	});
	function repeatRow(){
	
		var copied = $("#item_hidden").html();
		
		var boxes = $("#items").find(".my_row");
		boxes = boxes.length;
		boxes = boxes+1;
		
		$("#items").append("<div id='item_"+boxes+"' class='my_row'>"+copied+"</div>");

		$("#item_"+boxes+" select").select2();
		
	}
	
</script>

<script type="text/javascript">
$(document).on("change","#files",function(){
    var count = $('#files_div').find('input').length;
    count = count+1;
    $("<div id='div_"+count+"' style='display:inline;'><input style='width:400px;splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div'); 
	
	$("#upload_attachment").prop("disabled",false);
	
});

</script>

@stop

