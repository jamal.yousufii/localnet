@extends('layouts.master')
@section('head')

	{!! HTML::style('/vendor/select2/select2.css') !!}

    <title>{!!_('fees9_insert')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('fees9_insert')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
		?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getServiceFees9List')!!}" class="btn btn-success">{!!_("back")!!}</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('insertServiceFees9')!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            
                            <div class="form-group col-xs-12">
                            	<label class="control-label">{!!_("form_number")!!}</label>
                                <input value="{!! old('form_number') !!}" type="text" name="form_number" id="form_number" class="form-control" style="width: 500px;">
                                <span style="color:red;">{!! $errors->first('form_number') !!}</span>
                            </div>
                            <div class="form-group col-xs-12">
                            	<label class="control-label">{!!_("date")!!}</label>
                                <input type="text" value="{!! old('date') !!}" name="date" id="date" class="form-control {!!getDatePickerClass()!!}" style="width: 500px;">
                            </div>
                            <div class="form-group col-xs-12">
                            	<label class="control-label">{!!_("type")!!}</label>
                                <select name="type" id="type" class="form-control" style="width: 500px;">
                                    {!!getStaticTable("fees9_types","services",old('type'))!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('type') !!}</span>
                            </div>
                            
                            <div class="form-group col-xs-12" id="items" style="border: 1px solid #eee;padding: 10px;">
                            
                            	<div id="item_hidden" class="my_row" style="display:none;">
                            		<div class="form-group col-xs-4">
	                            		<select name="items[]" style="width: 400px;" class="form-control">
	                            			<option value="">{!!_('select_item_description')!!}</option>
	                        				@foreach($items AS $item)
	                  							<option value='{!!$item->id!!}'>{!!$item->item_description!!}</option>
	                        				@endforeach
	                        			</select>
	                        		</div>
	                        		<div class="form-group col-xs-4">
	                        			<select name="unit[]" style="width: 400px;" class="form-control">
		                            		{!!getStaticTable("mesure_units","services",old('unit'))!!}
		                            	</select>
	                        		</div>
	                        		<div class="form-group col-xs-3">
	                        			<input type="text" name="amount[]" placeholder="Enter amount ..." class="form-control" />
	                        		</div>
	                        		<div class="form-group col-xs-1">
										<button type="button" onclick="repeatRow();" class="btn btn-floating btn-success btn-xs"><i class="icon wb-plus" aria-hidden="true"></i></button>
										<button type="button" onclick="$(this).parents().eq(1).remove();" class="btn btn-floating btn-danger btn-xs"><i class="icon wb-minus" aria-hidden="true"></i></button>
	                        		</div>
                            	</div>
                            
                            
                            
                            <div id="item_1" class="my_row">
                            		<div class="form-group col-xs-4">
	                            		<select name="items[]" style="width: 400px;" class="form-control">
	                            			<option value="">{!!_('select_item_description')!!}</option>
	                        				@foreach($items AS $item)
	                  							<option value='{!!$item->id!!}'>{!!$item->item_description!!}</option>
	                        				@endforeach
	                        			</select>
	                        		</div>
	                        		<div class="form-group col-xs-4">
	                        			<select name="unit[]" style="width: 400px;" class="form-control">
		                            		{!!getStaticTable("mesure_units","services",old('unit'))!!}
		                            	</select>
	                        		</div>
	                        		<div class="form-group col-xs-3">
	                        			<input type="text" name="amount[]" placeholder="Enter amount ..." class="form-control" />
	                        		</div>
	                        		<div class="form-group col-xs-1">
										<button type="button" onclick="repeatRow();" class="btn btn-floating btn-success btn-xs"><i class="icon wb-plus" aria-hidden="true"></i></button>
										<button type="button" onclick="$(this).parents().eq(1).remove();" class="btn btn-floating btn-danger btn-xs"><i class="icon wb-minus" aria-hidden="true"></i></button>
	                        		</div>
                            	</div>
                            
                            </div>

                           
                            
                            <div class="form-group col-xs-6" style="padding-left: 210px;padding-right: 20px;">
                            
                            	<label class="control-label"> </label><br>
                                <input type="checkbox" value="1" name="daily_specail" id="	daily_specail" /> <label for="	daily_specail">{!!_("daily_specail")!!}</label> 
                                <br> 
                                <input type="checkbox" value="1" name="daily_employee" id="daily_employee" /> <label for="daily_employee">{!!_("daily_employee")!!}</label> 
                            	<br>
                            	<input type="checkbox" value="1" name="daily_employee_3" id="daily_employee_3" /> <label for="daily_employee_3">{!!_("daily_employee_3")!!}</label> 
                            	<br>
                            	<input type="checkbox" value="1" name="other" id="other" /> <label for="other">{!!_("other")!!}</label> 
                            </div>
                            
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('save')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
<script>
	$(document).ready(function(){
		$("#item_1 select").select2();
	});
	function repeatRow(){
	
		var copied = $("#item_hidden").html();
		
		var boxes = $("#items").find(".my_row");
		boxes = boxes.length;
		boxes = boxes+1;
		
		$("#items").append("<div id='item_"+boxes+"' class='my_row'>"+copied+"</div>");

		$("#item_"+boxes+" select").select2();
		
	}
	
</script>

@stop

