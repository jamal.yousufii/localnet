@extends('layouts.master')

@section('head')
    <title>{!!_('employee_edit')!!}</title>
    
    <style>
    	.attach_table a{
    		text-decoration: none;
    	}
    </style>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('employee_edit')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getServiceEmpList')!!}" class="btn btn-success">{!!_("back")!!}</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('updateServiceEmployee',$row->id)!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("name")!!}</label>
                                <input value="{!! $row->name !!}" type="text" name="name" id="name" class="form-control">
                                <span style="color:red;">{!! $errors->first('name') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("father_name")!!}</label>
                                <input value="{!! $row->father_name !!}" type="text" name="father_name" id="father_name" class="form-control">
                                <span style="color:red;">{!! $errors->first('father_name') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("start_date")!!}</label>
                            	@if(isMiladiDate())
                                <input value="<?php echo ($row->start_date != '0000-00-00') ? toGregorian($row->start_date):''; ?>" type="text" name="start_date" id="start_date" class="form-control {!!getDatePickerClass()!!}">
                            	@else
                            	<input value="<?php echo ($row->start_date != '0000-00-00') ? dmy_format($row->start_date):''; ?>" type="text" name="start_date" id="start_date" class="form-control {!!getDatePickerClass()!!}">
                            	@endif
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("end_date")!!}</label>
                            	@if(isMiladiDate())
                                <input value="<?php echo ($row->end_date != '0000-00-00') ? toGregorian($row->end_date):''; ?>" type="text" name="end_date" id="end_date" class="form-control {!!getDatePickerClass()!!}">
                            	@else
                            	<input value="<?php echo ($row->end_date != '0000-00-00') ? dmy_format($row->end_date):''; ?>" type="text" name="end_date" id="end_date" class="form-control {!!getDatePickerClass()!!}">
                            	@endif
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("position")!!}</label>
                                <input value="{!! $row->position !!}" type="text" name="position" id="position" class="form-control">
                                <span style="color:red;">{!! $errors->first('position') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("ID_card")!!}</label>
                                <input value="{!! $row->employee_id !!}" type="text" name="ID_card" id="ID_card" class="form-control">
                                <span style="color:red;">{!! $errors->first('ID_card') !!}</span> 
                            </div>
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("palace_id")!!}</label>
                                <select name="palace_id" id="palace_id" class="form-control">
                                    {!!getStaticTable("palaces","services",$row->palace_id)!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('palace_id') !!}</span>
                            </div>
                            <div class="form-group col-xs-6" style="padding-left: 210px;padding-right: 20px;">
                            	<label class="control-label"> </label><br>
                                <input type="checkbox" {!!($row->is_general_manager == 1?"checked":"")!!} value="1" name="is_general_manager" id="is_general_manager" /> &nbsp;&nbsp;<label for="is_general_manager">{!!_("is_general_manager")!!}</label> 
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" {!!($row->is_palace_manager == 1?"checked":"")!!} value="1" name="is_palace_manager" id="is_palace_manager" /> &nbsp;&nbsp;<label for="is_palace_manager">{!!_("is_palace_manager")!!}</label> 
                            </div>
                            <!--
                            <div class="form-group col-xs-12">
                            	<label class="control-label">{!!_("comment")!!}</label>
                                <textarea name="comment" id="comment" class="form-control">{!! $row->comment !!}</textarea>
                            </div>
                            -->
                            
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-refresh fa-lg"></i> {!!_('update')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
		<br><br>
        <div class="col-lg-12" dir="ltr">
            <!-- Example Continuous Accordion -->
            <div class="examle-wrap">
              <h4 class="example-title" style="text-align: right;">{!!_("employee_history")!!}</h4>
              <div class="example">
                <div class="panel-group panel-group-continuous" id="exampleAccordionContinuous" aria-multiselectable="true" role="tablist">
                  <div class="panel">
                    <div class="panel-heading" id="exampleHeadingContinuousOne" role="tab" style="text-align: center;background-color: rgb(98, 168, 234);">
                      <a class="panel-title collapsed" style="color: white;font-weight: bold;" data-parent="#exampleAccordionContinuous" data-toggle="collapse" href="#exampleCollapseContinuousOne" aria-controls="exampleCollapseContinuousOne" aria-expanded="false">
                      <i class="icon fa-comments fa-lg" aria-hidden="true"></i> {!!_("employee_comments")!!}
                      </a>
                    </div>
                    <div style="height: 0px;" aria-expanded="false" class="panel-collapse collapse" id="exampleCollapseContinuousOne" aria-labelledby="exampleHeadingContinuousOne" role="tabpanel">
                      <div class="panel-body" style="border: 1px solid rgb(98, 168, 234);">
	                        <div id="comments">
	                        	@if($comments)
	                        		@foreach($comments AS $item)
				                        <div class="content well" style="text-align: right;" id="comment_{!!$item->id!!}">
				                        	<a href="javascript:void()" style="float: left;color: red;" onclick="removeComment({!!$item->id!!});"><i class="icon fa-close fa-lg" aria-hidden="true"></i></a>
					                       	{!!$item->comment!!}
					                        <br>
					                        <small>Posted Date: {!!$item->date!!}</small>
				                      	</div>
				                      @endforeach
				                 @endif
		                      	
	                      	</div>
	                      	<div id="comment_box_div">
	                      		<input type="hidden" value="{!!$row->id!!}" name="emp_id" id="emp_id" />
	                      		<textarea name="comment_box" style="text-align: right;" id="comment_box" class="form-control" placeholder="New Comment ..."></textarea>
	                      		<a href="javascript:void();" id="save_comment" class="btn btn-warning" style="float: right;">{!!_("save_comment")!!}</a>
	                      	</div>
	                      	
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <div class="panel-heading" id="exampleHeadingContinuousTwo" role="tab" style="text-align: center;background-color: rgb(98, 168, 234);">
                      <a class="panel-title collapsed" style="color: white;font-weight: bold;" data-parent="#exampleAccordionContinuous" data-toggle="collapse" href="#exampleCollapseContinuousTwo" aria-controls="exampleCollapseContinuousTwo" aria-expanded="false">
                      <i class="icon fa-paperclip fa-lg" aria-hidden="true"></i> {!!_("employee_attachments")!!}
                      </a>
                    </div>
                    <div style="height: 0px;direction: rtl;" aria-expanded="false" class="panel-collapse collapse" id="exampleCollapseContinuousTwo" aria-labelledby="exampleHeadingContinuousTwo" role="tabpanel">
                      <div class="panel-body" style="border: 1px solid rgb(98, 168, 234);">
                        	<table class="attach_table table table-bordered">
                        		<tr>
                        			<td>#</td>
                        			<td>{!!_("file_name")!!}</td>
                        			<td colspan="2">{!!_("operation")!!}</td>
                        		</tr>
                        		@if($attachments)
                        			<?php $counter = 1; ?>
                        			@foreach($attachments AS $item)
		                        		<tr>
		                        			<td>{!!$counter!!}</td>
		                        			<td><a href="{!!URL::route('getDownloadServiceEmpDoc',$item->file_name)!!}"><i class="icon fa-paperclip fa-lg" aria-hidden="true"></i> {!!$item->file_name!!}</a></td>
		                        			<td><a href="{!!URL::route('getDeleteServiceEmpDoc',array($item->file_name,$item->id,$item->record_id))!!}" onclick="return confirm('آیا میخواهید ضمیمه را حذف نمایید؟');"><i class="icon fa-trash fa-lg" aria-hidden="true"></i> {!!_("remove")!!}</a></td>
		                        			<td><a href="{!!URL::route('getDownloadServiceEmpDoc',$item->file_name)!!}"><i class="icon fa-download fa-lg" aria-hidden="true"></i> {!!_("download")!!}</a></td>
		                        		</tr>
		                        		<?php $counter++; ?>
		                        	@endforeach
		                        @endif
                        	</table>
                        	<form class="form-horizontal" role="form" method="post" action="{!!URL::route('uploadServicesEmpDocs',$row->id)!!}" enctype="multipart/form-data">
	                        	<div class="form-group">
			                        <label class="col-sm-2 control-label">{!!_('upload_new_attachment')!!}</label>
			                        <div class="col-sm-4" id='files_div'>
			                            <input style='width:400px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
			                        </div>
			                        
			                    </div>
			                    <input class="col-sm-6 btn btn-success" id="upload_attachment" type="submit" value='{!!_("upload_attachment")!!}' />
		                    </form>
                        	
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
            <!-- End Example Continuous Accordion -->
          </div>
    </div>
@stop
@section('footer-scripts')
<script>
	
	$(document).ready(function(){
		$("#upload_attachment").prop("disabled",true);
		$("#save_comment").on("click",function(){
			if($("#comment_box").val() != ""){
				
				var emp_id = $("#emp_id").val();
				var comment = $("#comment_box").val();
				
				//post value via ajax to server
				$.ajax({
					type: "post",
					url: "{!!URL::route('saveEmployeeComment')!!}",
					data: "&emp_id="+emp_id+"&comment="+comment,
					dataType: "html",
					success: function(r){
						$("#comments").append(r);
						$("#comment_box").val("");
					},
					error: function(e)
					{
						alert(e);
					}
				});
			}else{
				//show validation error
				alert("Please write a comment...");
			}
		});
	});
	
	function removeComment(comment_id){
		if(confirm("آیا میخواهید متن را حذف نمایید؟")){
			$.ajax({
					type: "post",
					url: "{!!URL::route('removeEmployeeComment')!!}",
					data: "&comment="+comment_id,
					success: function(r){
						$("#comment_"+comment_id).html(r);
					},
					error: function(e)
					{
						alert(e);
					}
				});
		}
	}

</script>

<script type="text/javascript">
$(document).on("change","#files",function(){
    var count = $('#files_div').find('input').length;
    count = count+1;
    $("<div id='div_"+count+"' style='display:inline;'><input style='width:400px;splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div'); 
	
	$("#upload_attachment").prop("disabled",false);
	
});

</script>

@stop

