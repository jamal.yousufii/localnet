@extends('layouts.master')

@section('head')

    <title>{!!_('employee_list')!!}</title>
@stop
@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="main-box">
			<header class="main-box-header clearfix">
			    <h2>{!!_('employee_list')!!}</h2>
			    @if(Session::has('success'))
			    <span class='alert alert-success' style="width:400px;">
			        <i class="fa fa-check-circle fa-fw fa-lg"></i>
			        {!!Session::get('success')!!}
			    </span>
			    @elseif(Session::has('fail'))
			    <span class='alert alert-danger' style="width:400px;">
			        <i class="fa fa-times-circle fa-fw fa-lg"></i>
			        {!!Session::get('fail')!!}
			    </span>
			    @endif
			    <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en")
			    	{
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
			    ?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getCreateServiceEmployee')!!}" class="btn btn-success"><i class="icon fa-plus-circle fa-lg" aria-hidden="true"></i> {!!_("add_new")!!}</a></span>
			</header>
			
			<div class="main-box-body clearfix">
				<div class="table-responsive">
				    <table class="table table-responsive table-bordered" id='list'>
				        <thead>
				        <tr>
				            <th>#</th>
				            <th>{!!_('name')!!}</th>
				            <th>{!!_('father_name')!!}</th>
				            <th>{!!_('position')!!}</th>
				            <th>{!!_('start_date')!!}</th>
				            <th>{!!_('end_date')!!}</th>
				            <th>{!!_('ID')!!}</th>
				            <th>{!!_('palace')!!}</th>
				            <th>{!!_('operation')!!}</th>
				        </tr>
				        </thead>
				        <tbody>
				        </tbody>
				    </table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {

                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                //"iDisplayLength": 2,
                "sAjaxSource": "{!!URL::route('getServiceEmpData')!!}",
                "aaSorting": [[ 1, "desc" ]],
                
                "language": {
                    "lengthMenu": "{!!_('view')!!} _MENU_ {!!_('record_per_page')!!}",
                    "zeroRecords": "{!!_('record_not_found')!!}",
                    "info": "{!!_('page_view')!!} _PAGE_ {!!_('of')!!} _PAGES_",
                    "infoEmpty": "{!!_('record_not_found')!!}",
                    "search": "{!!_('search')!!}",
                    "infoFiltered": "(filtered {!!_('of')!!} _MAX_ {!!_('total_record')!!})"
                }
            }
        );

    });
</script>
@stop

