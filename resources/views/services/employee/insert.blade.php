@extends('layouts.master')
@section('head')
    <title>{!!_('employee_insert')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('employee_insert')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
		?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getServiceEmpList')!!}" class="btn btn-success">{!!_("back")!!}</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('insertServiceEmployee')!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("name")!!}</label>
                                <input value="{!! old('name') !!}" type="text" name="name" id="name" class="form-control">
                                <span style="color:red;">{!! $errors->first('name') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("father_name")!!}</label>
                                <input value="{!! old('father_name') !!}" type="text" name="father_name" id="father_name" class="form-control">
                                <span style="color:red;">{!! $errors->first('father_name') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("start_date")!!}</label>
                                <input type="text" value="{!! old('start_date') !!}" name="start_date" id="start_date" class="form-control {!!getDatePickerClass()!!}">
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("end_date")!!}</label>
                                <input type="text" value="{!! old('end_date') !!}" name="end_date" id="end_date" class="form-control {!!getDatePickerClass()!!}">
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("position")!!}</label>
                                <input value="{!! old('position') !!}" type="text" name="position" id="position" class="form-control">
                                <span style="color:red;">{!! $errors->first('position') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("ID_card")!!}</label>
                                <input value="{!! old('ID_card') !!}" type="text" name="ID_card" id="ID_card" class="form-control">
                                <span style="color:red;">{!! $errors->first('ID_card') !!}</span> 
                            </div>
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("palace_id")!!}</label>
                                <select name="palace_id" id="palace_id" class="form-control">
                                    {!!getStaticTable("palaces","services",old('palace_id'))!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('palace_id') !!}</span>
                            </div>
                            <div class="form-group col-xs-6" style="padding-left: 210px;padding-right: 20px;">
                            	<label class="control-label"> </label><br>
                                <input type="checkbox" value="1" name="is_general_manager" id="is_general_manager" /> &nbsp;&nbsp;<label for="is_general_manager">{!!_("is_general_manager")!!}</label> 
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" value="1" name="is_palace_manager" id="is_palace_manager" /> &nbsp;&nbsp;<label for="is_palace_manager">{!!_("is_palace_manager")!!}</label> 
                            </div>
                            
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('save')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

