@extends('layouts.master')
@section('head')
    <title>{!!_('invitation_edit')!!}</title>
    {!! HTML::style('/vendor/clockpicker/clockpicker.min.css') !!}
	<style>
	.clockpicker-popover{
		z-index:100000;
	}
	</style>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
    <span class='alert alert-success' style="width:400px;">
        <i class="fa fa-check-circle fa-fw fa-lg"></i>
        {!!Session::get('success')!!}
    </span>
    @elseif(Session::has('fail'))
    <span class='alert alert-danger' style="width:400px;">
        <i class="fa fa-times-circle fa-fw fa-lg"></i>
        {!!Session::get('fail')!!}
    </span>
    @endif
    <div class="col-lg-12">

        <h1>{!!_('invitation_edit')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
		?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getServiceInvitationList')!!}" class="btn btn-success">{!!_("back")!!}</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('updateServiceInvitation',$row->id)!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("type_of_invitation")!!}</label>
                                <select name="type" id="type" class="form-control">
                                	<option value=''>---</option>
                                    <option value="1" {!!($row->type==1?'selected':'')!!}>فوق العاده</option>
                                    <option value="2" {!!($row->type==2?'selected':'')!!}>درجه اول</option>
                                    <option value="3" {!!($row->type==3?'selected':'')!!}>درجه دوم</option>
                                    <option value="4" {!!($row->type==4?'selected':'')!!}>درجه سوم</option>
                                </select>
                                <span style="color:red;">{!! $errors->first('type') !!}</span>
                            </div>
                            
                            <div class="form-group col-xs-3">
                            	<label class="control-label">{!!_("date")!!}</label>
                            	@if(isMiladiDate())
                                <input value="<?php echo ($row->date != '0000-00-00') ? toGregorian($row->date):''; ?>" type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}" style="width: 500px;">
                            	@else
                            	<input value="<?php echo ($row->date != '0000-00-00') ? dmy_format($row->date):''; ?>" type="text" name="date" id="date" class="form-control {!!getDatePickerClass()!!}" style="width: 500px;">
                            	@endif
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">{!!_("time")!!}</label>
                            	<div class="input-group clockpicker-wrap" data-plugin="clockpicker" data-autoclose="true">
			                    <input class="form-control" type="text" value="{!! $row->time !!}" name="time" id="time">
				                    <span class="input-group-addon">
				                      <span class="wb-time"></span>
				                    </span>
			                    </div>
                            </div>
                           
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("number_of_guest")!!}</label>
                                <input value="{!! $row->number_of_guest !!}" type="text" name="number_of_guest" id="number_of_guest" class="form-control">
                                <span style="color:red;">{!! $errors->first('number_of_guest') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("source")!!}</label>
                                <input value="{!! $row->source !!}" type="text" name="source" id="source" class="form-control">
                                <span style="color:red;">{!! $errors->first('source') !!}</span> 
                            </div>
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("location")!!}</label>
                                <select name="location" id="location" class="form-control">
                                    {!!getStaticTable("palaces","services",$row->location)!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('location') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("maktob_number")!!}</label>
                                <input value="{!! $row->maktob_number !!}" type="text" name="maktob_number" id="maktob_number" class="form-control">
                                <span style="color:red;">{!! $errors->first('maktob_number') !!}</span> 
                            </div>
                            <div class="form-group col-xs-12">
                            	<label class="control-label">{!!_("description")!!}</label>
								<textarea class="form-control" id="description" name="description" rows="3">{!!$row->description!!}</textarea>                            
							</div>
							<div class="form-group col-xs-6">
                            
                                <input type="checkbox" {!!($row->lunch == 1?"checked":"")!!} value="1" name="lunch" id="lunch" /> <label for="lunch">{!!_("lunch")!!}</label> 
                                <br> 
                                <input type="checkbox" {!!($row->break_fast == 1?"checked":"")!!} value="1" name="break_fast" id="break_fast" /> <label for="break_fast">{!!_("break_fast")!!}</label> 
                            	<br>
                            	<input type="checkbox" {!!($row->after_noon == 1?"checked":"")!!} value="1" name="after_noon" id="after_noon" /> <label for="after_noon">{!!_("after_noon")!!}</label> 
                            	<br>
                            	<input type="checkbox" {!!($row->dinner == 1?"checked":"")!!} value="1" name="dinner" id="dinner" /> <label for="dinner">{!!_("dinner")!!}</label> 
                            </div>
                            
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('update')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                </div>
                
                <div class="panel">
                    <div class="panel-heading" id="exampleHeadingContinuousTwo" role="tab" style="text-align: center;background-color: rgb(98, 168, 234);">
                      <a class="panel-title collapsed" style="color: white;font-weight: bold;" data-parent="#exampleAccordionContinuous" data-toggle="collapse" href="#exampleCollapseContinuousTwo" aria-controls="exampleCollapseContinuousTwo" aria-expanded="false">
                      <i class="icon fa-paperclip fa-lg" aria-hidden="true"></i> {!!_("invitation_attachments")!!}
                      </a>
                    </div>
                    <div style="height: 0px;direction: rtl;" aria-expanded="false" class="panel-collapse collapse" id="exampleCollapseContinuousTwo" aria-labelledby="exampleHeadingContinuousTwo" role="tabpanel">
                      <div class="panel-body" style="border: 1px solid rgb(98, 168, 234);">
                        	<table class="attach_table table table-bordered">
                        		<tr>
                        			<td>#</td>
                        			<td>{!!_("file_name")!!}</td>
                        			<td colspan="2">{!!_("operation")!!}</td>
                        		</tr>
                        		@if($attachments)
                        			<?php $counter = 1; ?>
                        			@foreach($attachments AS $item)
		                        		<tr>
		                        			<td>{!!$counter!!}</td>
		                        			<td><a href="{!!URL::route('getDownloadServiceEmpDoc',$item->file_name)!!}"><i class="icon fa-paperclip fa-lg" aria-hidden="true"></i> {!!$item->file_name!!}</a></td>
		                        			<td><a href="{!!URL::route('getDeleteServiceInvitationDoc',array($item->file_name,$item->id,$item->record_id))!!}" onclick="return confirm('آیا میخواهید ضمیمه را حذف نمایید؟');"><i class="icon fa-trash fa-lg" aria-hidden="true"></i> {!!_("remove")!!}</a></td>
		                        			<td><a href="{!!URL::route('getDownloadServiceEmpDoc',$item->file_name)!!}"><i class="icon fa-download fa-lg" aria-hidden="true"></i> {!!_("download")!!}</a></td>
		                        		</tr>
		                        		<?php $counter++; ?>
		                        	@endforeach
		                        @endif
                        	</table>
                        	<form class="form-horizontal" role="form" method="post" action="{!!URL::route('uploadServicesInvitationDocs',$row->id)!!}" enctype="multipart/form-data">
	                        	<div class="form-group">
			                        <label class="col-sm-2 control-label">{!!_('upload_new_attachment')!!}</label>
			                        <div class="col-sm-4" id='files_div'>
			                            <input style='width:400px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
			                        </div>
			                        
			                    </div>
			                    <input class="col-sm-6 btn btn-success" id="upload_attachment" type="submit" value='{!!_("upload_attachment")!!}' />
		                    </form>
                        	
                      </div>
                    </div>
                  </div>
                  
            </div>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/vendor/clockpicker/bootstrap-clockpicker.min.js') !!}
{!! HTML::script('/js/components/clockpicker.min.js') !!}

<script type="text/javascript">
$(document).ready(function(){
		$("#upload_attachment").prop("disabled",true);
});
	
$(document).on("change","#files",function(){
    var count = $('#files_div').find('input').length;
    count = count+1;
    $("<div id='div_"+count+"' style='display:inline;'><input style='width:400px;splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div'); 
	
	$("#upload_attachment").prop("disabled",false);
	
});

</script>
@stop

