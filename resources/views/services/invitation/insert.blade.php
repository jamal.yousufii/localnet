@extends('layouts.master')
@section('head')
    <title>{!!_('invitation_insert')!!}</title>
    {!! HTML::style('/vendor/clockpicker/clockpicker.min.css') !!}
	<style>
	.clockpicker-popover{
		z-index:100000;
	}
	</style>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">

        <h1>{!!_('invitation_insert')!!}</h1>
        <?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){
			    		$dir = "pull-right";
			    	}
			    	else
			    	{
			    		$dir = "pull-left";
			    	}
		?>
			    <span class="{!!$dir!!}"><a href="{!!URL::route('getServiceInvitationList')!!}" class="btn btn-success">{!!_("back")!!}</a></span>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form action="{!!URL::route('insertServiceInvitation')!!}" role="form" method="post" enctype="multipart/form-data">
                        <div class="row">
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("type_of_invitation")!!}</label>
                                <select name="type" id="type" class="form-control">
                                	<option value=''>---</option>
                                    <option value="1">فوق العاده</option>
                                    <option value="2">درجه اول</option>
                                    <option value="3">درجه دوم</option>
                                    <option value="4">درجه سوم</option>
                                </select>
                                <span style="color:red;">{!! $errors->first('type') !!}</span>
                            </div>
                            
                            <div class="form-group col-xs-3">
                            	<label class="control-label">{!!_("date")!!}</label>
                                <input type="text" value="{!! old('date') !!}" name="date" id="date" class="form-control {!!getDatePickerClass()!!}">
                            </div>
                            <div class="form-group col-xs-3">
                            	<label class="control-label">{!!_("time")!!}</label>
                            	<div class="input-group clockpicker-wrap" data-plugin="clockpicker" data-autoclose="true">
			                    <input class="form-control" type="text" value="{!! old('time') !!}" name="time" id="time">
				                    <span class="input-group-addon">
				                      <span class="wb-time"></span>
				                    </span>
			                    </div>
                            </div>
                           
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("number_of_guest")!!}</label>
                                <input value="{!! old('number_of_guest') !!}" type="text" name="number_of_guest" id="number_of_guest" class="form-control">
                                <span style="color:red;">{!! $errors->first('number_of_guest') !!}</span> 
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("source")!!}</label>
                                <input value="{!! old('source') !!}" type="text" name="source" id="source" class="form-control">
                                <span style="color:red;">{!! $errors->first('source') !!}</span> 
                            </div>
                            
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("location")!!}</label>
                                <select name="location" id="location" class="form-control">
                                    {!!getStaticTable("palaces","services",old('location'))!!}
                                </select>
                                <span style="color:red;">{!! $errors->first('location') !!}</span>
                            </div>
                            <div class="form-group col-xs-6">
                            	<label class="control-label">{!!_("maktob_number")!!}</label>
                                <input value="{!! old('maktob_number') !!}" type="text" name="maktob_number" id="maktob_number" class="form-control">
                                <span style="color:red;">{!! $errors->first('maktob_number') !!}</span> 
                            </div>
                            <div class="form-group col-xs-12">
                            	<label class="control-label">{!!_("description")!!}</label>
								<textarea class="form-control" id="description" name="description" rows="3"></textarea>                            
							</div>
                            <div class="form-group col-xs-6">
                            
                                <input type="checkbox" value="1" name="lunch" id="lunch" /> <label for="lunch">{!!_("lunch")!!}</label> 
                                <br> 
                                <input type="checkbox" value="1" name="break_fast" id="break_fast" /> <label for="break_fast">{!!_("break_fast")!!}</label> 
                            	<br>
                            	<input type="checkbox" value="1" name="after_noon" id="after_noon" /> <label for="after_noon">{!!_("after_noon")!!}</label> 
                            	<br>
                            	<input type="checkbox" value="1" name="dinner" id="dinner" /> <label for="dinner">{!!_("dinner")!!}</label> 
                            </div>
                            
                            
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-xs-1">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save fa-lg"></i> {!!_('save')!!}</button>
                            </div>
                            <div class="form-group col-xs-1" style="margin-left:10px;">
                                <button type="reset" class="btn btn-danger"><i class="fa fa-eraser fa-lg"></i> {!!_('clear')!!}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/vendor/clockpicker/bootstrap-clockpicker.min.js') !!}
{!! HTML::script('/js/components/clockpicker.min.js') !!}
@stop

