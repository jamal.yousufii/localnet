@extends('layouts.master')

@section('head')
    @parent
    <title>Allotment to Individual List</title>
    <style type="text/css">
      a{cursor: pointer;}
      .modal-open .select2-dropdown {
        z-index: 10060;
      }
    </style>
    {!! HTML::style('/css/font.css') !!}
    {!! HTML::style('/css/print.css', array('media' => 'print')) !!}
    {!! HTML::style('/vendor/select2/select2.min.css') !!}
    {!! HTML::script('/vendor/select2/select2.min.js')!!}
@stop


@section('content')
  <div class="row noprint" style="margin: 10px;">
    <h3 align="center" class="pull-right">Allotment to Individual List / لست توزیع اجناس برای شخص</h3>
    <a href="{!!URL::route('getAssetAllotmentToIndividualForm')!!}" class="btn btn-success pull-left"><i class="icon fa-plus" aria-hidden="true"></i> Add New Allotment to Individual / اضافه کردن توزیع جنس جدید برای شخص</a>
  </div>
  <hr class="noprint" />
    <div id="deleted_result" class="noprint">
    </div>
  <div id="search_result">
    {{--Bring the list table--}}
        @include('assets_mgmt.allotment_to_individual.list_table')
    {{--list table end--}}
    <div class="dataTables_paginate paging_simple_numbers noprint" id="list_paginate">
      @if(!empty($records))
      {!!$records->render()!!}
      @endif
    </div>
  </div>
</div>

<div class="modal fade modal-info" id="exampleModalPrimary" aria-hidden="true" aria-labelledby="exampleModalPrimary" role="dialog">
  <div class="modal-dialog" style="width: 1200px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title"><i class="fa fa-edit fa-lg"></i></h4>
      </div>
      <div id="form_part">
        
      </div>
    </div>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

  $("#sender").select2();
  
  function itemDetails(item_detail_id)
  {
    $.ajax({
        url: '{!!URL::route("getItemDetailInfo")!!}',
        data: "item_detail_id="+item_detail_id,
        type: 'post',
        beforeSend: function(){
            $('#form_part').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            $('.modal-title').html("Item Details | جزئیات جنس");
            $('#form_part').html(response);
            $('#form_part').css("margin","20px");
            $('#exampleModalPrimary').modal("show");
        }
    })
  }

  function allotment_to_individual_details(allotment_to_individual_id)
  {
    $.ajax({
        url: '{!!URL::route("assetAllotmentToIndividualEditForm")!!}',
        data: "allotment_to_individual_id="+allotment_to_individual_id,
        type: 'post',
        beforeSend: function(){
          $('#form_part').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            $(".modal-title").html("Allotment To Individual Edit Form | فورمه تجدید جنس");
            $('#form_part').html(response);
            $('#exampleModalPrimary').modal("show");
            $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
            $(".datepicker_farsi").persianDatepicker();
        }
    })
  }

  function updateAllotmentToIndividualDetails()
  { 
    var dataString = $("#allotment_to_individual_edit_form").serialize();
    $.ajax({
        type: 'POST',
        url: "{!!URL::route('updateAssetMgmtAllotmentToIndividual')!!}",
        data: dataString,
        beforeSend: function(){
            $('#submit_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            //if there is error in form validation, then don't redirect, stay in this form.
            if(response == "error")
            {
                $('#submit_result').html("<div class='alert alert-danger'>لطفآ فارم را درست خانه پری نمائید</div>");
            }
            else
            {
              $('#submit_result').html(response);
              var delay = 1500; 
              setTimeout(function() { window.location = "{!!URL::route('getAssetAllotmentToIndividual')!!}"; }, delay);
            }
        }
    })
    return false;
  }

	$('.pagination a').on('click', function(event) {
    event.preventDefault();
    if ($(this).attr('href') != '#') {
      var dataString = "&page="+$(this).text()+"&ajax="+1;
      $.ajax({
          url: '{!!URL::route("getAssetAllotmentToIndividual")!!}',
          data: dataString,
          type: 'get',
          beforeSend: function(){
              $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#search_result').html(response);
          }
      }
    );
    
    }
  });

  function deleteAllotmentToIndividual(record_id)
  {
      if (confirm("آیا مطمئین هستید ؟")) 
      {
          $.ajax({
            url : '{!!URL::route("deleteAssetMgmtAllotmentToIndividual")!!}',
            type : 'post',
            data : {'record_id':record_id,'_token':"{!!csrf_token()!!}"},
            success : function(response)
            {
              $("#deleted_result").html(response);
              $(".remove_record"+record_id).css('background','Crimson');
              $(".remove_record"+record_id).slideUp('6000', function(){
                $(this).remove();
              });
              
            }
          })
      }
  }

</script> 
@stop