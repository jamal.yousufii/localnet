<div id="submit_result">
</div>
<form class="form-horizontal">
    
    <div class="modal-body">

        <h4 align="center">Item Detail Information | جزئیات جنس</h4>
        <table class="table table-bordered table-responsive">
            <thead>
                <tr>
                <th>Main Item</th>
                <th>Sub Item</th>
                <th>End Item</th>
                <th>Item Detail</th>	        
                </tr>
            </thead>
            <tbody>
                @if(!empty($item_detail_info))
                    @foreach($item_detail_info AS $item)
                        <tr>
                            <td>{!!$item->main_item!!}</td>
                            <td>{!!$item->sub_item!!}</td>
                            <td>{!!$item->end_item!!}</td>
                            <td>{!!$item->item_detail_name!!}</td>
                        </tr>
                    @endforeach
                @else
                <div style="padding: 10px" class="noprint">
                    <span style="color:red">No records exist in the system !</span>
                </div>
                @endif
            </tbody>
        </table>

        <h4 align="center">Purchase Details | جزئیات خریداری</h4><hr />
        <div class="form-group">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sanction By / تصویب شده توسط</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sanction_by" id="sanction_by" style="width: 100%" disabled>
                    {!!getSanctions($purchase_details->sanction_by);!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Peshnehad Number / نمبر پیشنهاد</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="peshnehad_number" value="{!!$purchase_details->peshnehad_number!!}" disabled />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Order Date / تاریخ سفارش</label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="order_date" value="{!!checkEmptyDate($purchase_details->order_date)!!}" disabled />
                </div>
            </div> 
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Remarks / ملاحظات</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="remarks" disabled>{!!$purchase_details->remarks!!}</textarea>
                </div>
            </div>  
        </div>
        <div class="form-group">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Number of Items / تعداد اجناس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="number_of_items" id="number_of_items" value="{!!$purchase_details->number_of_items!!}" disabled />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Unit Price / قیمت فی جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="unit_price" id="unit_price" value="{!!$purchase_details->unit_price!!}" disabled />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Total Cost / قیمت مجموعی</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="total_cost" id="total_cost" value="{!!$purchase_details->total_cost!!}" disabled />
                </div>
            </div>
        </div>
            {!!Form::token();!!}
            <hr />
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
            </div>
    </div>
</form>

<script>

    $("#sanction_by").select2();
    $("#item_detail_code").select2();
    $("#item_location").select2();

</script>
