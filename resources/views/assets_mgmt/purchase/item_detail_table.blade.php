
<h4 align="center">Item Detail Information | جزئیات جنس</h4>
<table class="table table-bordered table-responsive">
    <thead>
        <tr>
        <th>Main Item</th>
        <th>Sub Item</th>
        <th>End Item</th>
        <th>Item Detail</th>	        
        </tr>
    </thead>
    <tbody>
        @if(!empty($records))
            @foreach($records AS $item)
                <tr>
                    <td>{!!$item->main_item!!}</td>
                    <td>{!!$item->sub_item!!}</td>
                    <td>{!!$item->end_item!!}</td>
                    <td>{!!$item->item_detail_name!!}</td>
                </tr>
            @endforeach
        @else
        <div style="padding: 10px" class="noprint">
            <span style="color:red">No records exist in the system !</span>
        </div>
        @endif
    </tbody>
</table><br />