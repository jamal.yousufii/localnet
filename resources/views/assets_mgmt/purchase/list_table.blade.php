
	<table class="table table-bordered table-responsive">
		<thead>
	      <tr>
	        <th>No# / شماره</th>
			<td>Item Detail Code / کود جزئیات جنس</td>
	        <th>Sanction By / شخص منظور کننده</th>
	        <th>Peshnehad # / نمبر پیشنهاد</th>
	        <th>Order Date / تاریخ سفارش</th>
	        <th>Remarks / ملاحظات</th>
	        <th>Unit Price / قیمت فی واحد</th>
	        <th>Total Cost / قیمت مجموعی</th>
	        <th>Currency / واحد پولی</th>
	        <th>Reject Reason / دلیل رد کردن</th>

	        <th colspan="5" class="noprint" style="text-align:center">Actions / اجرای عملیات</th>
	        
	      </tr>
	    </thead>

	    <tbody>
	    	@if(!empty($records))
	    	<?php $counter = $records->firstItem(); ?>
	            @foreach($records AS $item)
	            	<?php
	            		$record_id = Crypt::encrypt($item->id); 
	            	?>
	                <tr class="remove_record{!!$item->id!!}">
		                <td>{!!$counter!!}</td>
						<td><a href="#" id="{!!$item->item_detail_id!!}" onclick="itemDetails(this.id)">{!!getNameBasedOnId("item_detail","item_detail_code",$item->item_detail_id)!!}</a></td>
		                <td>{!!getNameBasedOnId("sanction","name",$item->sanction_by)!!}</td>
		                <td>{!!$item->peshnehad_number!!}</td>
		                <td>{!! dmy_format(toJalali($item->order_date))!!}</td>
		                <td>{!!$item->remarks!!}</td>
		                <td>{!!$item->unit_price!!}</td>
		                <td>{!!$item->total_cost!!}</td>
		                <td>{!!getNameBasedOnId("currency","name",$item->currency)!!}</td>
						@if($item->authorized == 0)
						<td>{!!$item->reject_reason!!}</td>
						@else
						<td></td>
						@endif
		                
						<?php $record_id = Crypt::encrypt($item->id); ?>
							@if(!isPurchaseInwarded($item->id))
		                	<td align='center' class="noprint action"><a href="#" onclick="inward_form({!!$item->id!!})" title="Inward | رسید">Inward | رسید</a></td>
							@else
							<td align='center' class="noprint action"><span style='color:green;font-weight:bold'>Inwarded | رسید شده</span></td>
							@endif
							<td align='center' class="noprint"><a href="#" onclick="getPurchaseDetails({!!$item->id!!})" title="Edit / اصلاح کردن"><span class='fa fa-edit'></span></a></td>
							<td align='center' class="noprint"><a href="{!!URL::route('viewAssetPurchaseDetails',$record_id)!!}" title='Purchase Details / جزئیات خریداری'><span class='fa fa-eye'></span></a></td>
							<td align='center' class="noprint"><a onclick="deletePurchase(this.id)" id="{!!$item->id!!}" title='Delete Purchase / حذف خریداری'><span class='fa fa-trash'></span></a></td>
							@if($item->authorized == 1)
							<td align='center' class="noprint action"><span style="color:green;font-weight:bold">Authorized | تائیده شده</span></td>
							@elseif($item->reject_reason != "")
							<td align='center' class="noprint action"><span style="color:red;font-weight:bold">Rejected | رد شده</span></td>
							@else
							<td align='center' class="noprint action"><span style="color:orange;font-weight:bold">Pending | انتظار به تائید یا رد</span></td>
							@endif
		                
	                </tr>
	                <?php $counter++; ?>
	            @endforeach
	        @else
	        <div style="padding: 10px" class="noprint">
	        	<span style="color:red">No records exist in the system ! / معلومات در سیستم موجود نمیباشد</span>
	        </div>
	    	@endif
	    </tbody>
	</table>
<script type="text/javascript">

	$(document).ready(function() {
    	$('td.text').each(function() {
	        var td = $(this);
	        var cs = td.text().length;
	        
	        if(cs>50)
	        {
	        	var shown = td.text().substring(0, 50);
	        	td.text(shown+'...');
	        }
	    });
	});

</script>