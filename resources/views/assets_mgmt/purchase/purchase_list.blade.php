@extends('layouts.master')

@section('head')
    @parent
    <title>Purchase List</title>
    <style type="text/css">
      a{cursor: pointer;}
      .modal-open .select2-dropdown {
        z-index: 10060;
      }
      .action{font-size:12px !important;}
    </style>
    {!! HTML::style('/css/font.css') !!}
    {!! HTML::style('/css/print.css', array('media' => 'print')) !!}
    {!! HTML::style('/vendor/select2/select2.min.css') !!}
    {!! HTML::script('/vendor/select2/select2.min.js')!!}
@stop


@section('content')
  <div class="row noprint" style="margin: 10px;">
    <h2 class="pull-right">Purchase List /  لست خریداری ها</h2>
    <!-- <a id="click" data-target="#exampleModalPrimary" data-toggle="modal" class="btn btn-success pull-right"><i class="icon fa-plus" aria-hidden="true"></i> Add New Document</a> -->
    <a href="{!!URL::route('assetPurchaseEntryPage')!!}" class="btn btn-success pull-left"><i class="icon fa-plus" aria-hidden="true"></i> Add Purchase / اضافه کردن خریداری</a>
  </div>
  <hr class="noprint" />
    <div id="deleted_result" class="noprint">
    </div>
  <div id="search_result">
    {{--Bring the list table--}}
        @include('assets_mgmt.purchase.list_table')
    {{--list table end--}}
    <div class="dataTables_paginate paging_simple_numbers noprint" id="list_paginate">
      @if(!empty($records))
      {!!$records->render()!!}
      @endif
    </div>
  </div>
</div>

<div class="modal fade modal-info" id="exampleModalPrimary" aria-hidden="true" aria-labelledby="exampleModalPrimary" role="dialog">
  <div class="modal-dialog" style="width: 1200px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title"><i class="fa fa-edit fa-lg"></i></h4>
      </div>
      <div id="form_part">
        
      </div>
    </div>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

  $("#sender").select2();
  
  function itemDetails(item_detail_id)
  {
    $.ajax({
      url: '{!!URL::route("getItemDetailInfo")!!}',
      data: "item_detail_id="+item_detail_id,
      type: 'post',
      beforeSend: function(){
          $('#form_part').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
      },
      success: function(response)
      {
            $(".modal-title").html("Item Details | جزئیات جنس");
            $('#form_part').html(response);
            $('#form_part').css("margin","30px");
            $('#exampleModalPrimary').modal("show");
      }
    })
  }

  function getPurchaseDetails(id)
  {
    $.ajax({
      url: '{!!URL::route("assetPurchaseEditForm")!!}',
      data: "id="+id,
      type: 'post',
      beforeSend: function(){
          //$("body").show().css({"opacity": "0.5"});
          $('#form_part').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
      },
      success: function(response)
      {
        $('.modal-title').html("Purchase Edit Form / فورمه آوردن تغییرات در خریداری");
        $('#form_part').html(response);
        $('#exampleModalPrimary').modal("show");
        $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
        $(".datepicker_farsi").persianDatepicker();
      }
    })
  }

  function inward_form(purchase_id)
  {
    $.ajax({
      url: '{!!URL::route("getAssetsMgmtPurchaseInwardForm")!!}',
      data: "id="+purchase_id,
      type: 'post',
      beforeSend: function(){
          $('#form_part').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
      },
      success: function(response)
      {
          $('.modal-title').html("Inward Form / فورمه رسید اجناس");
          $('#form_part').html(response);
          $('#exampleModalPrimary').modal("show");
          $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
          $(".datepicker_farsi").persianDatepicker();
      }
    })
  }

  function add_inward()
  {
    var dataString = $("#inward_form").serialize();
    $.ajax({
      type: 'POST',
      url: "{!!URL::route('addPurchaseInward')!!}",
      data: dataString,
      beforeSend: function(){
          $('#submit_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
      },
      success: function(response)
      {
        $('#submit_result').html(response);
        var delay = 2000; 
        setTimeout(function() { window.location = "{!!URL::route('getAssetsMgmtPurchaseInwardList')!!}"; }, delay);
      }
    })
    return false;
  }

  function updatePurchaseDetails()
  { 
    var dataString = $("#purchase_edit_form").serialize();
    $.ajax({
      type: 'POST',
      url: "{!!URL::route('updateAssetMgmtPurchase')!!}",
      data: dataString,
      beforeSend: function(){
          $('#submit_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
      },
      success: function(response)
      {
        if(response == "error")
        {
          $('#submit_result').html("<div class='alert alert-danger'>لطفآ فارم را درست خانه پری نمائید</div>");
        }
        else
        {
          $('#submit_result').html(response);
          var delay = 2000; 
          setTimeout(function() { window.location = "{!!URL::route('getAssetsMgmtPurchaseList')!!}"; }, delay);
        }
      }
    })
    return false;
  }

  function view_purchase_details(asset_purchase_id)
  {
    $.ajax({
        url: '{!!URL::route("viewAssetPurchaseDetails")!!}',
        data: "asset_purchase_id="+asset_purchase_id,
        type: 'post',
        beforeSend: function(){
          $('#form_part').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            $(".modal-title").html("Purchased Item Details | جزئیات جنس خریداری شده");
            $('#form_part').html(response);
            $('#exampleModalPrimary').modal("show");
            $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
            $(".datepicker_farsi").persianDatepicker();
        }
    })
  }

	$('.pagination a').on('click', function(event) {
    event.preventDefault();
    if ($(this).attr('href') != '#') {
      var dataString = "&page="+$(this).text()+"&ajax="+1;
      $.ajax({
          url: '{!!URL::route("getAssetsMgmtPurchaseList")!!}',
          data: dataString,
          type: 'get',
          beforeSend: function(){
              $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#search_result').html(response);
          }
      }
    );
    
    }
  });

  function deletePurchase(record_id)
  {
      if (confirm("آیا مطمئین هستید ؟")) 
      {
          $.ajax({
            url : '{!!URL::route("deleteAssetMgmtPurchase")!!}',
            type : 'post',
            data : {'record_id':record_id,'_token':"{!!csrf_token()!!}"},
            success : function(response)
            {
              $("#deleted_result").html(response);
              $(".remove_record"+record_id).css('background','Crimson');
              $(".remove_record"+record_id).slideUp('6000', function(){
                $(this).remove();
              });
              
            }
          })
      }
  }

  //get the document list based on form data.
  $('#search_docs').click(function(){
      var datastring = $('#search_form').serialize();
      $.ajax({
          type: 'POST',
          url: '{!!URL::route("searchDocs")!!}',
          data: datastring,
          beforeSend: function(){
              $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#search_result').html(response);
          }
      });
      return false;
  });

</script> 
@stop