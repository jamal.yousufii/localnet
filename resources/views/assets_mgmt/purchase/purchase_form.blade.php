@extends('layouts.master')
@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
<strong>Whoops!</strong> There were some problems with your input.<br><br>
<ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
</div>
@endif
@if(Session::has('success'))
<div class='alert alert-success noprint'>{{Session::get('success')}}</div>

@elseif(Session::has('fail'))
<div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
@endif
<div class="page-head">
<h3 align="center">Purchase Entry Form / فورمه اضافه کردن خریداری</h3>
<h5>فیلد های که همرای <span style="color:red"> * </span> نشانی شده ضروری میباشند</h5>
<hr>
</div>
<div class="cl-mcont" id="sdu_result">
    
    <form class="form-horizontal group-border-dashed" action="{!!URL::route('saveNewAssetPurchase')!!}" method="post" style="border-radius: 0px;" enctype="multipart/form-data">
        
        <div style="maring:20px" class="row">
            <div class="form-group">
                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">Item Detail Code / کود جزئیات جنس<span style="color:red"> * </span></label>
                    </div>
                    <div class="col-sm-12">
                        <select class="form-control" name="item_detail_code" id="item_detail_code" style="width: 100%">
                        {!!getItemDetailCodeList();!!}
                        </select>
                    </div>
                </div>
                <div class="col-sm-7" style="margin-left:40px" id="item_detail_info">
                </div>
            </div>
        </div>
        <hr />

        <div class="form-group">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sanction By / تصویب شده توسط<span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sanction_by" id="sanction_by" style="width: 100%" required>
                    {!!getSanctions();!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Peshnehad Number / نمبر پیشنهاد<span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="peshnehad_number" required="required" />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Order Date / تاریخ سفارش<span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="order_date" readonly="readonly" />
                </div>
            </div> 
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Remarks / ملاحظات</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="remarks"></textarea>
                </div>
            </div>  
        </div>
        <div class="form-group">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Number of Items / تعداد اجناس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="number_of_items" id="number_of_items" />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Unit Price / قیمت فی جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="unit_price" id="unit_price" />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Total Cost / قیمت مجموعی</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="total_cost" id="total_cost" />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Currency / واحد پولی</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="currency" style="width: 100%" id="currency">
                        {!!getCurrencies()!!}
                    </select>
                </div>
            </div>
        </div>
    	{!!Form::token();!!}
    	<hr />
    	<div class="form-group">
        <div class="col-sm-4">
          	<input type="submit" value="تائید کردن" class="btn btn-success"/>
          	<a href="{!!URL::route('getAssetsMgmtPurchaseList')!!}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> برگشت به لست</a>
        </div>
	    </div>
    </form>
  </div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

    $("#sanction_by").select2();
    $("#item_detail_code").select2();
    $("#currency").select2();

    (function($) {
        $("#unit_price").on("blur", function(){    
            var unit_price = $("#unit_price").val();
            var number_of_items = $("#number_of_items").val();
            var total_cost = unit_price*number_of_items;
            $("#total_cost").val(total_cost);
        });
        
        $("#item_detail_code").on("change", function(){
            var dataString = $("#item_detail_code").val();
            $.ajax({
                type : "post",
                url : "{!!URL::route("getItemDetailInfo")!!}",
                data : "item_detail_id="+dataString,
                beforeSend: function(){
                    $('#item_detail_info').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#item_detail_info').html(response);
                }
            })
        });

    })(jQuery);

	

</script> 

@stop