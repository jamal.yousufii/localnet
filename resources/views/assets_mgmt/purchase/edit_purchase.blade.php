<div id="submit_result">
</div>
<form class="form-horizontal" id="purchase_edit_form">
    <div class="modal-body">
        <input type="hidden" name="purchase_id" value="{!!Crypt::encrypt($row->id)!!}" />

        <div class="form-group">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sanction By / تصویب شده توسط<span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sanction_by" id="sanction_by" style="width: 100%" required>
                    {!!getSanctions($row->sanction_by);!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Peshnehad Number / نمبر پیشنهاد<span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="peshnehad_number" value="{!!$row->peshnehad_number!!}" required="required" />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Order Date / تاریخ سفارش<span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="order_date" value="{!!checkEmptyDate($row->order_date)!!}" readonly="readonly" />
                </div>
            </div> 
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Remarks / ملاحظات</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="remarks">{!!$row->remarks!!}</textarea>
                </div>
            </div>  
        </div>
        <div class="form-group">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Number of Items / تعداد اجناس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="number_of_items" id="number_of_items" value="{!!$row->number_of_items!!}" />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Unit Price / قیمت فی جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="unit_price" id="unit_price" value="{!!$row->unit_price!!}" />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Total Cost / قیمت مجموعی</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="total_cost" id="total_cost" value="{!!$row->total_cost!!}" />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Currency / واحد پولی</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="currency" style="width: 100%" id="currency" disabled>
                        {!!getCurrencies($row->currency)!!}
                    </select>
                </div>
            </div>
        </div>
    	{!!Form::token();!!}
    	<hr />

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-info" onclick="updatePurchaseDetails()">Update</button>
    </div>
</form>

<script>

    $("#sanction_by").select2();
    $("#item_detail_code").select2();

    (function($) {

        $("#unit_price").on("blur", function(){    
            var unit_price = $("#unit_price").val();
            var number_of_items = $("#number_of_items").val();
            var total_cost = unit_price*number_of_items;
            $("#total_cost").val(total_cost);
        });

    })(jQuery);

</script>
