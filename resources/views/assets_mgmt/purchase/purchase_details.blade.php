@extends('layouts.master')

@section('head')
    @parent
    <title>Purchase Details</title>
    <style type="text/css">
      a{cursor: pointer;}
      .modal-open .select2-dropdown {
        z-index: 10060;
      }
    </style>
@stop


@section('content')
<h4 style="text-align:center">Purchase Details / جزئیات خریداری</h4>
<hr />
<div id="submit_result">
</div>
<form class="form-horizontal" style="margin:30px">

    <input type="hidden" class="form-control" name="record_id" id="record_id" value="{!!Crypt::encrypt($row->id)!!}" />

    <div class="form-group">
        <div class="col-sm-3">
            <div class="col-sm-12">
                <label class="col-sm-12 ">Sanction By / تصویب شده توسط<span style="color:red"> * </span></label>
            </div>
            <div class="col-sm-12">
                <select class="form-control" name="sanction_by" id="sanction_by" style="width: 100%" disabled>
                {!!getSanctions($row->sanction_by);!!}
                </select>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12">
                <label class="col-sm-12 ">Peshnehad Number / نمبر پیشنهاد<span style="color:red"> * </span></label>
            </div>
            <div class="col-sm-12">
                <input type="number" class="form-control" name="peshnehad_number" value="{!!$row->peshnehad_number!!}" disabled />
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12">
                <label class="col-sm-12 ">Order Date / تاریخ سفارش<span style="color:red"> * </span></label>
            </div>
            <div class="col-sm-12">
                <input type="text" class="form-control datepicker_farsi" name="order_date" value="{!!checkEmptyDate($row->order_date)!!}" disabled />
            </div>
        </div> 
        <div class="col-sm-3">
            <div class="col-sm-12">
                <label class="col-sm-12 ">Remarks / ملاحظات</label>
            </div>
            <div class="col-sm-12">
                <textarea class="form-control" name="remarks" disabled>{!!$row->remarks!!}</textarea>
            </div>
        </div>  
    </div>
    <div class="form-group">
        <div class="col-sm-3">
            <div class="col-sm-12">
                <label class="col-sm-12 ">Number of Items / تعداد اجناس</label>
            </div>
            <div class="col-sm-12">
                <input type="number" class="form-control" name="number_of_items" id="number_of_items" value="{!!$row->number_of_items!!}" disabled />
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12">
                <label class="col-sm-12 ">Unit Price / قیمت فی جنس</label>
            </div>
            <div class="col-sm-12">
                <input type="number" class="form-control" name="unit_price" id="unit_price" value="{!!$row->unit_price!!}" disabled />
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12">
                <label class="col-sm-12 ">Total Cost / قیمت مجموعی</label>
            </div>
            <div class="col-sm-12">
                <input type="number" class="form-control" name="total_cost" id="total_cost" value="{!!$row->total_cost!!}" disabled />
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12">
                <label class="col-sm-12 ">Currency / واحد پولی</label>
            </div>
            <div class="col-sm-12">
                <select class="form-control" name="currency" style="width: 100%" id="currency" disabled>
                    {!!getCurrencies($row->currency)!!}
                </select>
            </div>
        </div>
    </div>
    {!!Form::token();!!}
    <hr />

    </div>
    <div id="reject_form_div" style="display:none">
        <div class="form-group">
            <div class="col-sm-5">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Reason of rejecting the Purchase / دلیل رد کردن فارم خریداری جنس</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="reject_reason" id="reject_reason"></textarea>
                </div>
            </div>
        </div>
        <hr />
    </div>
    <div class="modal-footer">
        <a class="btn btn-danger" href="{!!URL::route('getAssetsMgmtPurchaseList')!!}"><i class="fa fa-remove"></i> Close</a>
        @if($row->authorized == 0)
        <button type="button" class="btn btn-success" id="authorize_btn" onclick="AuthorizeOrRejectPurchase()">Authorize | تائید کردن</button>
        @endif
        @if($row->reject_reason == "" && $row->authorized == 0)
        <button type="button" class="btn btn-danger" id="reject_btn" onclick="bringRejectForm()">Reject | رد کردن</button>
        <button type="button" class="btn btn-danger" id="reject_purchase" onclick="AuthorizeOrRejectPurchase()" style="display:none">Reject | رد کردن</button>
        @endif
    </div>
</form>

@stop

@section('footer-scripts') 
<script type="text/javascript">

    function bringRejectForm()
    {
        $("#reject_btn").hide();
        $('#reject_form_div').fadeIn('slow');
        $('#reject_purchase').fadeIn('slow');
        $('#authorize_btn').hide('fast');
    }

    function AuthorizeOrRejectPurchase()
    { 
        var record_id = $("#record_id").val();
        var reject_reason = $("#reject_reason").val();
        var table = "purchase";
        $.ajax({
            type: 'POST',
            url: "{!!URL::route('AuthorizeOrRejectRecords')!!}",
            data: "table="+table+"&record_id="+record_id+"&reject_reason="+reject_reason,
            beforeSend: function(){
                $('#submit_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#submit_result').html(response);
                var delay = 2500; 
                setTimeout(function() { window.location = "{!!URL::route('getAssetsMgmtPurchaseList')!!}"; }, delay);
            }
        })
        return false;
    }

</script>
@stop