@extends('layouts.master')
@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
<strong>Whoops!</strong> There were some problems with your input.<br><br>
<ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
</div>
@endif
@if(Session::has('success'))
<div class='alert alert-success noprint'>{{Session::get('success')}}</div>

@elseif(Session::has('fail'))
<div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
@endif

<form class="form-horizontal" id="asset_allotment_return_form">
    <div class="modal-body">
        <div class="container">
            <h4 align="center">Allotment Return Form | فورمه تسلیمی جنس</h4>
            <h6 align="center">Fields marked with an asterisk <span style="color:red">*</span> are required / فیلد های که همرای علامه ستاره سرخ رنگ نشانی شده ضروری میباشند</h6>
        </div>
        <hr />
        
        <div class="form-group">
            <div id="item_detail_info"></div>
        </div>

        <div class="form-group">
            
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12">General Department / ادارۀ عمومی <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select name="general_dept" id="general_dept" class="form-control" onchange="bringRelatedSubDepartment('sub_dept',this.value)" style='width:100%' required>
                        <option value="0">انتخاب</option>
                        @foreach($parentDeps AS $dep_item)
                            <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sub Department / ادارۀ مربوط <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sub_dept" id="sub_dept" style='width:100%' onchange="getEmployees()" required>
                        <option value='0'>انتخاب</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">From / از طرف<span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="from_person" id="from_person" style="width:100%" required>
                    </select>
                </div>
            </div>

        </div>
        
        <div class="form-group">

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allotment Number / نمبر توزیع جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="allotment_number" id="allotment_number" />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Detail Item Code / کود جزئیات جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="item_detail_id" id="item_detail_id" onchange="detailsOfSelectedItem(this.value)" style="width:100%" required>
                        {!!getItemDetailCodeList()!!}
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allotment Date / تاریخ توزیع جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="allotment_date" id="allotment_date" readonly required />
                </div>
            </div>

        </div>
        
        <div class="form-group">

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sanction By / منظور شده توسط <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sanction_by" id="sanction_by" style="width:100%" required>
                        {!!getStoreKeepers();!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Return Date / تاریخ تسلیمی جنس<span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="return_date" required readonly />
                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Received By / شخص تسلیم شونده<span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="received_by" id="received_by" style="width:100%" required>
                        {!!getStoreKeepers()!!}
                    </select>
                </div>
            </div>

        </div>

        <div class="form-group">

            <div class="col-sm-5">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Remakrs / ملاحظات</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="remarks"></textarea>
                </div>
            </div>

        </div>
            {!!Form::token();!!}
            <hr />
            <div id="submit_result">
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" href="{!!URL::route('getAssetAllotmentReturn')!!}">Close / بسته کردن</a>
                <button type="button" class="btn btn-info" onclick="add_asset_allotment_return()">Submit | تائید کردن</button>
            </div>
    </div>
</form>
@stop

@section('footer-scripts') 
<script type="text/javascript">

    $("#sanction_by").select2();
    $("#item_detail_code").select2();
    $("#general_dept").select2();
    $("#sub_dept").select2();
    $("#from_person").select2();
    $("#item_detail_id").select2();
    $("#received_by").select2();
    $("#sanction_by").select2();

    function detailsOfSelectedItem(item_detail_id)
    {
        $.ajax({
            url: '{!!URL::route("getItemDetailInfo")!!}',
            data: "item_detail_id="+item_detail_id,
            type: 'post',
            beforeSend: function(){
                $('#item_detail_info').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#item_detail_info').html(response);
                $('#item_detail_info').css("margin","20px");
                $('#exampleModalPrimary').modal("show");
            }
        })
    }

    // function getEmployeeDesignation()
    // {
    //     var emp_id = $("#allottee_name").val();
    //     $.ajax({
    //         url: '{!!URL::route("getAssetEmpDesignation")!!}',
    //         data: '&emp_id='+emp_id,
    //         type: 'post',
    //         success: function(response)
    //         {
    //             $('#allottee_designation').val(response);
    //         }
    //     });
    // }

    function add_asset_allotment_return()
    {
        var dataString = $("#asset_allotment_return_form").serialize();
        $.ajax({
        type: 'POST',
        url: "{!!URL::route('addAssetAllotmentReturn')!!}",
        data: dataString,
        beforeSend: function(){
            $('#submit_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            //if there is error in form validation, then don't redirect to the inward list, stay in this form.
            if(response == "error")
            {
            $('#submit_result').html("<div class='alert alert-danger'>لطفآ فورمه را درست خانه پری نمائید </div>");
            }
            else{

            $('#submit_result').html(response);
            var delay = 2000; 
            setTimeout(function() { window.location = "{!!URL::route('getAssetAllotmentReturn')!!}"; }, delay);
            }
        }
        })
        return false;
    }

    function getEmployees()
    {
        var department_id = $("#sub_dept").val();
        $.ajax({
            url: '{!!URL::route("getAssetDeptEmployees")!!}',
            data: '&department_id='+department_id,
            type: 'post',
            beforeSend: function(){
                $("#from_person").html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#from_person').html(response);
            }
        });
    }

    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }

</script>
@stop