
<form class="form-horizontal" id="allotment_return_edit_form">

    <div class="container">
        <h4 align="center">Allotment Return Edit Form | فورمه تجدید تسلیمی اجناس</h4>
        <h6 align="center">Fields marked with an asterisk <span style="color:red">*</span> are required / فیلد های که همرای علامه ستاره سرخ رنگ نشانی شده ضروری میباشند</h6>
        <hr />
        <div class="form-group" style="margin-bottom:-30px">
            <div id="item_detail_info"></div>
        </div>
    </div>
    <hr />

    <div class="modal-body">

        <div class="form-group">
            
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">General Department / ادارۀ عمومی <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select name="general_dept" id="general_dept" class="form-control" onchange="bringRelatedSubDepartment('sub_dept',this.value)" style='width:100%' required>
                        <option value="0">انتخاب</option>
                        @foreach($parentDeps AS $dep_item)
                            <option <?php if($row->general_dept == $dep_item->id) echo 'selected="selected"';?> value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sub Department / ادارۀ مربوط <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sub_dept" id="sub_dept" style='width:100%' onchange="getEmployees()" required>
                        {!!getAssetMgmtRelatedSubDepartment(getNameBasedOnId('allotment_return','general_dept',$row->id),$row->sub_dept);!!}
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">From / از طرف<span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="from_person" id="from_person" style="width:100%" required>
                    {!!getAssetMgmtRelatedEmployees($row->sub_dept,$row->from_person)!!}
                    </select>
                </div>
            </div>

        </div>
        
        <div class="form-group">

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allotment Number / نمبر توزیع جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="allotment_number" id="allotment_number" value="{!!$row->allotment_number!!}" />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Detail Item Code / کود جزئیات جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="item_detail_id" id="item_detail_id" onchange="detailsOfSelectedItem(this.value)" style="width:100%" required>
                        {!!getItemDetailCodeList($row->item_detail_id)!!}
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allotment Date / تاریخ توزیع جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="allotment_date" id="allotment_date" value="{!!checkEmptyDate($row->allotment_date)!!}" readonly required />
                </div>
            </div>

        </div>
        
        <div class="form-group">

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sanction By / منظور شده توسط <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sanction_by" id="sanction_by" style="width:100%" required>
                        {!!getStoreKeepers($row->sanction_by);!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Return Date / تاریخ تسلیمی جنس<span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="return_date" value="{!!checkEmptyDate($row->return_date)!!}" required readonly />
                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Received By / شخص تسلیم شونده<span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="received_by" id="received_by" style="width:100%" required>
                        {!!getStoreKeepers($row->received_by)!!}
                    </select>
                </div>
            </div>

        </div>

        <div class="form-group">

            <div class="col-sm-5">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Remakrs / ملاحظات</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="remarks">{!!$row->remarks!!}</textarea>
                </div>
            </div>

        </div>
        <input type="hidden" name="allotment_return_id" value="{!!$row->id!!}" />
    	{!!Form::token();!!}
    	<hr />
        <div id="submit_result">
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-info" onclick="updateAllotmentReturnDetails()">Update Allotment Return | تجدید تسلیمی جنس</button>
    </div>
</form>

<script>

    $("#sanction_by").select2();
    $("#item_detail_code").select2();
    $("#general_dept").select2();
    $("#sub_dept").select2();
    $("#from_person").select2();
    $("#item_detail_id").select2();
    $("#sanction_by").select2();
    $("#received_by").select2();

    // bring the item details based on item detail id on page load.
    var item_detail_id = "{!!$row->item_detail_id!!}";
    $.ajax({
        url: '{!!URL::route("getItemDetailInfo")!!}',
        data: "item_detail_id="+item_detail_id,
        type: 'post',
        beforeSend: function(){
            $('#item_detail_info').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            $('#item_detail_info').html(response);
            $('#exampleModalPrimary').modal("show");
        }
    })

    function getEmployeeDesignation()
    {
        var emp_id = $("#allottee_name").val();
        $.ajax({
            url: '{!!URL::route("getAssetEmpDesignation")!!}',
            data: '&emp_id='+emp_id,
            type: 'post',
            success: function(response)
            {
                $('#allottee_designation').val(response);
            }
        });
    }

    function detailsOfSelectedItem(item_detail_id)
    {
        $.ajax({
            url: '{!!URL::route("getItemDetailInfo")!!}',
            data: "item_detail_id="+item_detail_id,
            type: 'post',
            beforeSend: function(){
                $('#item_detail_info').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#item_detail_info').html(response);
                $('#exampleModalPrimary').modal("show");
            }
        })
    }

    function getEmployees()
    {
        var department_id = $("#sub_dept").val();
        $.ajax({
            url: '{!!URL::route("getAssetDeptEmployees")!!}',
            data: '&department_id='+department_id,
            type: 'post',
            beforeSend: function(){
                $("#allottee_name").html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#allottee_name').html(response);
            }
        });
    }

    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }

</script>
