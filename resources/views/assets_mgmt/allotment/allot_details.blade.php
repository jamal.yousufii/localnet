@extends('layouts.master')

@section('head')
    @parent
    <title>Asset Identification List</title>
    <style type="text/css">
      a{cursor: pointer;}
      .modal-open .select2-dropdown {
        z-index: 10060;
      }
    </style>
@stop


@section('content')
<div id="submit_result">
</div>
<form class="form-horizontal" id="allot_authorize_form">
    <div class="modal-body">
    <input type="hidden" name="record_id" id="record_id" value="{!!Crypt::encrypt($row->id)!!}" />
        <div class="form-group">
            <div id="item_detail_info"></div>
        </div>

        <div class="form-group">
            <h4 align="center">Allotted Item Details | جزئیات جنس توزیع شده</h4><hr />
            
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">General Department / ادارۀ عمومی</label>
                </div>
                <div class="col-sm-12">
                    <select name="general_dept" id="general_dept" class="form-control" onchange="bringRelatedSubDepartment('sub_dept',this.value)" style='width:100%' disabled>
                        <option value="0">انتخاب</option>
                        @foreach($parentDeps AS $dep_item)
                            <option <?php if($row->general_dept == $dep_item->id) echo 'selected="selected"';?> value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sub Department / ادارۀ مربوط</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sub_dept" id="sub_dept" style='width:100%' disabled onchange="getEmployees()">
                        {!!getAssetMgmtRelatedSubDepartment(getNameBasedOnId('allotment','general_dept',$row->id),$row->sub_dept);!!}
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allottee Name / اسم شخص</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="allottee_name" id="allottee_name" onchange="getEmployeeDesignation()" style="width:100%" disabled>
                    {!!getAssetMgmtRelatedEmployees($row->sub_dept,$row->allottee_name)!!}
                    </select>
                </div>
            </div>

        </div>
        
        <div class="form-group">

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allottee Designation / عنوان بست</label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="allottee_designation" id="allottee_designation" value="{!!$row->allottee_designation!!}" disabled/>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Detail Item Code / کود جزئیات جنس</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="item_detail_id" id="item_detail_id" onchange="detailsOfSelectedItem(this.value)" style="width:100%" disabled>
                        {!!getItemDetailCodeList($row->item_detail_id)!!}
                    </select>
                </div>
            </div>

            <div class="col-sm-5">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Remakrs / ملاحظات</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="remark" disabled>{!!$row->remark!!}</textarea>
                </div>
            </div>
        </div>
        
        <div class="form-group">

            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allotment Date / تاریخ توزیع جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="allotment_date" id="allotment_date" value="{!!checkEmptyDate($row->allotment_date)!!}" disabled  />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Store Keeper / معتمد</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="store_keeper" id="store_keeper" style="width:100%" disabled>
                        {!!getStoreKeepers($row->store_keeper);!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Fes5 / نمبر تکت توزیع یا فیس پنج</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="fes5" value="{!!$row->fes5!!}" disabled />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sanction By / منظور شده توسط</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sanction_by" id="sanction_by" style="width:100%" disabled>
                        {!!getSanctions($row->sanction_by)!!}
                    </select>
                </div>
            </div>
        </div>
    	{!!Form::token();!!}
    	<hr />

    </div>
    <div id="reject_form_div" style="display:none">
        <div class="form-group">
            <div class="col-sm-5">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Reason of rejecting the allotment / دلیل رد کردن فارم توزیع جنس</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="reject_reason" id="reject_reason"></textarea>
                </div>
            </div>
        </div>
        <hr />
    </div>

    <div class="modal-footer">
        <a class="btn btn-danger" href="{!!URL::route('getAssetAllotList')!!}"><i class="fa fa-remove"></i> Close</a>
        @if($row->authorized == 0)
        <button type="button" class="btn btn-success" id="authorize_btn" onclick="AuthorizeOrRejectAllotment()">Authorize | تائید کردن</button>
        @endif
        @if($row->reject_reason == "" && $row->authorized == 0)
        <button type="button" class="btn btn-danger" id="reject_btn" onclick="bringRejectForm()">Reject | رد کردن</button>
        <button type="button" class="btn btn-danger" id="reject_allotment" onclick="AuthorizeOrRejectAllotment()" style="display:none">Reject | رد کردن</button>
        @endif
    </div>
</form>

@stop

@section('footer-scripts') 
<script type="text/javascript">

    function bringRejectForm()
    {
        $("#reject_btn").hide();
        $('#reject_form_div').fadeIn('slow');
        $('#reject_allotment').fadeIn('slow');
        $('#authorize_btn').hide('fast');
    }

    function AuthorizeOrRejectAllotment()
    { 
        var record_id = $("#record_id").val();
        var reject_reason = $("#reject_reason").val();
        var table = "allotment";
        $.ajax({
            type: 'POST',
            url: "{!!URL::route('AuthorizeOrRejectRecords')!!}",
            data: "table="+table+"&record_id="+record_id+"&reject_reason="+reject_reason,
            beforeSend: function(){
                $('#submit_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#submit_result').html(response);
                var delay = 2500; 
                setTimeout(function() { window.location = "{!!URL::route('getAssetAllotList')!!}"; }, delay);
            }
        })
        return false;
    }

</script>
@stop