
<table class="table table-bordered table-responsive">
	<thead>
		<tr>
		<th>No#</th>
		<td>General Department / اداره عمومی</td>
		<th>Sub Department / اداره فرعی</th>
		<th>Allottee Name / اسم شخص</th>
		<th>Designation / عنوان بست</th>
		<th>Detail Item Code</th>
		<th>Remarks / ملاحظات</th>
		<th>Allotment Date / تاریخ توزیع جنس</th>
		<th>Sanction By  / منظور شده توسط</th>
		<th>Reject Reason / دلیل رد شدن</th>

		<th colspan="5" style="text-align:center" class="noprint">Actions | عملیات</th>
		
		</tr>
	</thead>

	<tbody>
		@if(!empty($records))
		<?php $counter = $records->firstItem(); ?>
			@foreach($records AS $item)
				<?php
					$record_id = Crypt::encrypt($item->id); 
				?>
				<tr class="remove_record{!!$item->id!!}">
					<td>{!!$counter!!}</td>
					<td>{!!$item->general_dept!!}</td>
					<td>{!!$item->sub_dept!!}</td>
					<td>{!!$item->allottee_name!!}</td>
					<td>{!!$item->allottee_designation!!}</td>
					<td><a href="#" id="{!!$item->item_detail_id!!}" onclick="itemDetails(this.id)">{!!getNameBasedOnId("item_detail","item_detail_code",$item->item_detail_id)!!}</a></td>
					<td>{!!$item->remark!!}</td>
					<td>{!!dmy_format(toJalali($item->allotment_date))!!}</td>
					<td>{!!$item->sanction_by!!}</td>
					@if($item->authorized == 0)
					<td>{!!$item->reject_reason!!}</td>
					@else
					<td></td>
					@endif

					<td align='center' class="noprint"><a href="#" onclick="allot_details({!!$item->id!!})" title="Edit Record"><span class='fa fa-edit'></span></a></td>
					<td align='center' class="noprint"><a href="{!!URL::route('viewAssetAllotmentDetails',$record_id)!!}" title="View Allot Details"><span class='fa fa-eye'></span></a></td>
					<td align='center' class="noprint"><a onclick="deleteAllot(this.id)" id="{!!$item->id!!}" title='Delete Record'><span class='fa fa-trash'></span></a></td>
					@if($item->authorized == 1)
					<td align='center' class="noprint"><span style="color:green;font-weight:bold">Authorized | تائیده شده</span></td>
					@elseif($item->reject_reason != "")
					<td align='center' class="noprint"><span style="color:red;font-weight:bold">Rejected | رد شده</span></td>
					@else
					<td align='center' class="noprint"><span style="color:orange;font-weight:bold">Pending | انتظار به تائید یا رد</span></td>
					@endif
				</tr>
				<?php $counter++; ?>
			@endforeach
		@else
		<div style="padding: 10px" class="noprint">
			<span style="color:red">No records exist in the system !</span>
		</div>
		@endif
	</tbody>
</table>
<script type="text/javascript">

	$(document).ready(function() {
    	$('td.text').each(function() {
	        var td = $(this);
	        var cs = td.text().length;
	        
	        if(cs>50)
	        {
	        	var shown = td.text().substring(0, 50);
	        	td.text(shown+'...');
	        }
	    });
	});

</script>