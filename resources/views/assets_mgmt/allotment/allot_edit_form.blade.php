
<form class="form-horizontal" id="allot_edit_form">

    <div class="container">
        <h4 align="center">Allotment Edit Form | فورمه تجدید توزیع اجناس</h4>
        <h6 align="center">Fields marked with an asterisk <span style="color:red">*</span> are required / فیلد های که همرای علامه ستاره سرخ رنگ نشانی شده ضروری میباشند</h6>
        <hr />
        <div class="form-group" style="margin-bottom:-30px">
            <div id="item_detail_info"></div>
        </div>
    </div>
    <hr />

    <div class="modal-body">
    <input type="hidden" name="asset_allot_id" value="{!!Crypt::encrypt($row->id)!!}" />

        <div class="form-group">
            
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">General Department / ادارۀ عمومی <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select name="general_dept" id="general_dept" class="form-control" onchange="bringRelatedSubDepartment('sub_dept',this.value)" style='width:100%' required>
                        <option value="0">انتخاب</option>
                        @foreach($parentDeps AS $dep_item)
                            <option <?php if($row->general_dept == $dep_item->id) echo 'selected="selected"';?> value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sub Department / ادارۀ مربوط <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sub_dept" id="sub_dept" style='width:100%' onchange="getEmployees()" required>
                        {!!getAssetMgmtRelatedSubDepartment(getNameBasedOnId('allotment','general_dept',$row->id),$row->sub_dept);!!}
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allottee Name / اسم شخص <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="allottee_name" id="allottee_name" onchange="getEmployeeDesignation()" style="width:100%" required>
                    {!!getAssetMgmtRelatedEmployees($row->sub_dept,$row->allottee_name)!!}
                    </select>
                </div>
            </div>

        </div>
        
        <div class="form-group">

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allottee Designation / عنوان بست</label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="allottee_designation" id="allottee_designation" value="{!!$row->allottee_designation!!}" readonly/>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Detail Item Code / کود جزئیات جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="item_detail_id" id="item_detail_id" onchange="detailsOfSelectedItem(this.value)" style="width:100%" required>
                        {!!getItemDetailCodeList($row->item_detail_id)!!}
                    </select>
                </div>
            </div>

            <div class="col-sm-5">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Remakrs / ملاحظات</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="remark">{!!$row->remark!!}</textarea>
                </div>
            </div>

        </div>

        <div class="form-group">

            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allotment Date / تاریخ توزیع جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="allotment_date" id="allotment_date" value="{!!checkEmptyDate($row->allotment_date)!!}" readonly required />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Store Keeper / معتمد <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="store_keeper" id="store_keeper" style="width:100%" required>
                        {!!getStoreKeepers($row->store_keeper);!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Fes5 / نمبر تکت توزیع یا فیس پنج <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="fes5" value="{!!$row->fes5!!}" required />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sanction By / منظور شده توسط</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sanction_by" id="sanction_by" style="width:100%" required>
                        {!!getSanctions($row->sanction_by)!!}
                    </select>
                </div>
            </div>
        </div>
    	{!!Form::token();!!}
    	<hr />
        <div id="submit_result">
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-info" onclick="updateAllotmentDetails()">Update Allotment | تجدید توزیع جنس</button>
    </div>
</form>

<script>

    $("#sanction_by").select2();
    $("#item_detail_code").select2();
    $("#general_dept").select2();
    $("#sub_dept").select2();
    $("#allottee_name").select2();
    $("#item_detail_id").select2();
    $("#sanction_by").select2();

    // bring the item details based on item detail id on page load.
    var item_detail_id = "{!!$row->item_detail_id!!}";
    $.ajax({
        url: '{!!URL::route("getItemDetailInfo")!!}',
        data: "item_detail_id="+item_detail_id,
        type: 'post',
        beforeSend: function(){
            $('#item_detail_info').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            $('#item_detail_info').html(response);
            $('#exampleModalPrimary').modal("show");
        }
    })

    function getEmployeeDesignation()
    {
        var emp_id = $("#allottee_name").val();
        $.ajax({
            url: '{!!URL::route("getAssetEmpDesignation")!!}',
            data: '&emp_id='+emp_id,
            type: 'post',
            success: function(response)
            {
                $('#allottee_designation').val(response);
            }
        });
    }

    function detailsOfSelectedItem(item_detail_id)
    {
        $.ajax({
            url: '{!!URL::route("getItemDetailInfo")!!}',
            data: "item_detail_id="+item_detail_id,
            type: 'post',
            beforeSend: function(){
                $('#item_detail_info').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#item_detail_info').html(response);
                $('#exampleModalPrimary').modal("show");
            }
        })
    }

    function getEmployees()
    {
        var department_id = $("#sub_dept").val();
        $.ajax({
            url: '{!!URL::route("getAssetDeptEmployees")!!}',
            data: '&department_id='+department_id,
            type: 'post',
            beforeSend: function(){
                $("#allottee_name").html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#allottee_name').html(response);
            }
        });
    }

    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }

</script>
