<div id="submit_result">
</div>
<h4 align="center">Inward Edit Form | فورمه تجدید رسید جنس</h4>
<h6 align="center">Fields marked with an asterisk <span style="color:red">*</span> are required / فیلد های که همرای علامه ستاره سرخ رنگ نشانی شده ضروری میباشند</h6>
<hr />
<form class="form-horizontal" id="inward_edit_form">
    <div class="modal-body">
        <input type="hidden" name="inward_id" value="{!!Crypt::encrypt($row->id)!!}" />

        <div class="form-group">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Inward Date / تاریخ رسید جنس <span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="inward_date" id="inward_date" value="{!!checkEmptyDate($row->inward_date)!!}" readonly required />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">M7 Number / نمبر میم هفت <span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="m7_number" value="{!!$row->m7_number!!}" required />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Store Keeper / معتمد <span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="store_keeper" style="width:100%" required>
                        {!!getStoreKeepers($row->store_keeper)!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Warranty/Guarantee /تصمین یا گرانتی <span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="warranty" required>
                        {!!getWarrantyList($row->warranty)!!}
                    </select>
                </div>
            </div>
        </div>
    	{!!Form::token();!!}
    	<hr />

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-info" onclick="updateInwardDetails()">Update Inward | تجدید رسید جنس</button>
    </div>
</form>

<script>

    

</script>
