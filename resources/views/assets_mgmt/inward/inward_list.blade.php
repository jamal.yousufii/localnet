@extends('layouts.master')

@section('head')
    @parent
    <title>Inward List</title>
    <style type="text/css">
      a{cursor: pointer;}
      .modal-open .select2-dropdown {
        z-index: 10060;
      }
    </style>
    {!! HTML::style('/css/font.css') !!}
    {!! HTML::style('/css/print.css', array('media' => 'print')) !!}
    {!! HTML::style('/vendor/select2/select2.min.css') !!}
    {!! HTML::script('/vendor/select2/select2.min.js')!!}
@stop


@section('content')
  <div class="row noprint" style="margin: 10px;">
    <h3 align="center">Inward List / لست رسید اجناس</h3>
  </div>
  <hr class="noprint" />
    <div id="deleted_result" class="noprint">
    </div>
  <div id="search_result">
    {{--Bring the list table--}}
        @include('assets_mgmt.inward.list_table')
    {{--list table end--}}
    <div class="dataTables_paginate paging_simple_numbers noprint" id="list_paginate">
      @if(!empty($records))
      {!!$records->render()!!}
      @endif
    </div>
  </div>
</div>

<div class="modal fade modal-info" id="exampleModalPrimary" aria-hidden="true" aria-labelledby="exampleModalPrimary" role="dialog">
  <div class="modal-dialog" style="width: 1200px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title"><i class="fa fa-edit fa-lg"></i> Inward Edit Form / فورمه تجدید کردن رسید اجناس</h4>
      </div>
      <div id="form_part">
        
      </div>
    </div>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

  $("#sender").select2();
  
  function itemDetails(purchase_id)
  {
    $.ajax({
        url: '{!!URL::route("getItemAndPurchaseDetails")!!}',
        data: "purchase_id="+purchase_id,
        type: 'post',
        beforeSend: function(){
          $('#form_part').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            $(".modal-title").html("Item and Purchase Details | جزئیات جنس و خریداری");
            $('#form_part').html(response);
            $('#exampleModalPrimary').modal("show");
        }
    })
  }

  function get_asset_identity_form(purchase_id,inward_id)
  {
    $.ajax({
      url: '{!!URL::route("getAssetIdentityForm")!!}',
      data: {'purchase_id':purchase_id,'inward_id':inward_id},
      type: 'post',
      beforeSend: function(){
        $('#form_part').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
      },
      success: function(response)
      {
        $(".modal-title").html("Asset Identification Form with Purchase and Inward Details | فورمه ای اضافه کردن بخش تشخیص اجناس همراه با جزئیات خریداری و رسید آن");
        $('#form_part').html(response);
        $('#exampleModalPrimary').modal("show");
        $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
        $(".datepicker_farsi").persianDatepicker();
      }
    })
  }

  function add_asset_identity()
  {
    var dataString = $("#asset_identity_form").serialize();
    $.ajax({
      type: 'POST',
      url: "{!!URL::route('addAssetIdentity')!!}",
      data: dataString,
      beforeSend: function(){
          $('#submit_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
      },
      success: function(response)
      {
        //if there is error in form validation, then don't redirect to the inward list, stay in this form.
        if(response == "validation_error")
        {
          $('#submit_result').html("<div class='alert alert-danger'>Please fill the required fields</div>");
        }
        else
        {
          $('#submit_result').html(response);
          var delay = 2000; 
          setTimeout(function() { window.location = "{!!URL::route('getAssetIdentificationList')!!}"; }, delay);
        }
      }
    })
    return false;
  }

  function view_inward_details(asset_inward_id)
  {
    $.ajax({
        url: '{!!URL::route("viewAssetInwardDetails")!!}',
        data: "asset_inward_id="+asset_inward_id,
        type: 'post',
        beforeSend: function(){
          $('#form_part').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            $(".modal-title").html("Inwarded Item Details | جزئیات جنس رسید شده");
            $('#form_part').html(response);
            $('#exampleModalPrimary').modal("show");
            $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
            $(".datepicker_farsi").persianDatepicker();
        }
    })
  }

  function inward_details(inward_id)
  {
    $.ajax({
        url: '{!!URL::route("assetInwardEditForm")!!}',
        data: "inward_id="+inward_id,
        type: 'post',
        beforeSend: function(){
          $('#form_part').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            $(".modal-title").html("Inward Edit Form | فارم تجدید رسید جنس");
            $('#form_part').html(response);
            $('#exampleModalPrimary').modal("show");
            $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
            $(".datepicker_farsi").persianDatepicker();
        }
    })
  }

  function updateInwardDetails()
  { 
    var dataString = $("#inward_edit_form").serialize();
    $.ajax({
        type: 'POST',
        url: "{!!URL::route('updateAssetMgmtInward')!!}",
        data: dataString,
        beforeSend: function(){
            $('#submit_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            //if there is error in form validation, then don't redirect to the inward list, stay in this form.
            if(response == "error")
            {
                $('#submit_result').html("<div class='alert alert-danger'>لطفآ فارم را درست خانه پری نمائید</div>");
            }
            else
            {
                $('#submit_result').html(response);
                var delay = 2000; 
                setTimeout(function() { window.location = "{!!URL::route('getAssetsMgmtPurchaseInwardList')!!}"; }, delay);
            }
        }
    })
    return false;
  }

	$('.pagination a').on('click', function(event) {
    event.preventDefault();
    if ($(this).attr('href') != '#') {
      var dataString = "&page="+$(this).text()+"&ajax="+1;
      $.ajax({
          url: '{!!URL::route("getAssetsMgmtPurchaseInwardList")!!}',
          data: dataString,
          type: 'get',
          beforeSend: function(){
              $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#search_result').html(response);
          }
      }
    );
    
    }
  });

  function deleteInward(record_id)
  {
      if (confirm("آیا مطمئین هستید ؟")) 
      {
          $.ajax({
            url : '{!!URL::route("deleteAssetMgmtInward")!!}',
            type : 'post',
            data : {'record_id':record_id,'_token':"{!!csrf_token()!!}"},
            success : function(response)
            {
              $("#deleted_result").html(response);
              $(".remove_record"+record_id).css('background','Crimson');
              $(".remove_record"+record_id).slideUp('6000', function(){
                $(this).remove();
              });
              
            }
          })
      }
  }

</script> 
@stop