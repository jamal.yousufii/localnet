
<table class="table table-bordered table-responsive">
	<thead>
		<tr>
		<th>No# / شماره</th>
		<td>Item Detail Code | کود جزئیات جنس</td>
		<th>Inward Date | تاریخ رسید اجناس</th>
		<th>M7 Number | نمبر میم هفت</th>
		<th>Store Keeper | معتمد</th>
		<th>Warranty/Guarantee | گرانتی یا وارنتی</th>
		<th>Reject Reason / دلیل رد شدن</th>

		<th colspan="5" class="noprint" style="text-align:center">Actions / اجرای عملیات</th>
		
		</tr>
	</thead>

	<tbody>
		@if(!empty($records))
		<?php $counter = $records->firstItem(); ?>
			@foreach($records AS $item)
				<?php
					$record_id = Crypt::encrypt($item->id); 
				?>
				<tr class="remove_record{!!$item->id!!}">
					<td>{!!$counter!!}</td>
					<td><a href="#" id="{!!$item->purchase_id!!}" onclick="itemDetails(this.id)">{!!getNameBasedOnId("item_detail","item_detail_code",$item->item_detail_id)!!}</a></td>
					<td>{!!dmy_format(toJalali($item->inward_date))!!}</td>
					<td>{!!$item->m7_number!!}</td>
					<td>{!!getNameBasedOnId("store_keeper","name",$item->store_keeper)!!}</td>
					<td>{!!$item->warranty!!}</td>
					@if($item->authorized == 0)
					<td>{!!$item->reject_reason!!}</td>
					@else
					<td></td>
					@endif
					
					<?php $record_id = Crypt::encrypt($item->id); ?>
						@if(!isAssetIdentified($item->id))
						<td align='center' class="noprint"><a href="#" onclick="get_asset_identity_form({!!$item->purchase_id!!},{!!$item->id!!})" title="Asset Identification | تشخیص جنس">Asset Identification | تشخیص جنس</a></td>
						@else
						<td align='center' class="noprint"><span style='color:green;font-weight:bold'>Asset Identified | جنس مشخص گردید</span></td>
						@endif
						<td align='center' class="noprint"><a href="#" onclick="inward_details({!!$item->id!!})" title="Edit Inward / اصلاح کردن"><span class='fa fa-edit'></span></a></td>
						<td align='center' class="noprint"><a href="{!!URL::route('viewAssetInwardDetails',$record_id)!!}" title='Inward Details / جزئیات رسید جنس'><span class='fa fa-eye'></span></a></td>
						<td align='center' class="noprint"><a onclick="deleteInward(this.id)" id="{!!$item->id!!}" title='Delete Inward / حذف کردن'><span class='fa fa-trash'></span></a></td>
						@if($item->authorized == 1)
						<td align='center' class="noprint"><span style="color:green;font-weight:bold">Authorized | تائیده شده</span></td>
						@elseif($item->reject_reason != "")
						<td align='center' class="noprint"><span style="color:red;font-weight:bold">Rejected | رد شده</span></td>
						@else
						<td align='center' class="noprint"><span style="color:orange;font-weight:bold">Pending | انتظار به تائید یا رد</span></td>
						@endif
					
				</tr>
				<?php $counter++; ?>
			@endforeach
		@else
		<div style="padding: 10px" class="noprint">
			<span style="color:red">No records exist in the system ! / معلومات در سیستم پیدا نشد</span>
		</div>
		@endif
	</tbody>
</table>
<script type="text/javascript">

	$(document).ready(function() {
    	$('td.text').each(function() {
	        var td = $(this);
	        var cs = td.text().length;
	        
	        if(cs>50)
	        {
	        	var shown = td.text().substring(0, 50);
	        	td.text(shown+'...');
	        }
	    });
	});

</script>