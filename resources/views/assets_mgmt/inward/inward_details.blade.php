@extends('layouts.master')

@section('head')
    @parent
    <title>Asset Identification List</title>
    <style type="text/css">
      a{cursor: pointer;}
      .modal-open .select2-dropdown {
        z-index: 10060;
      }
    </style>
@stop


@section('content')
<div id="submit_result">
</div>
<h4 align="center">Inward Details / جزئیات رسید جنس</h4>
<hr />
<form class="form-horizontal" id="inward_edit_form">
    <div class="modal-body">
        <input type="hidden" name="record_id" id="record_id" value="{!!Crypt::encrypt($row->id)!!}" />

        <div class="form-group">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Inward Date / تاریخ رسید جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="inward_date" id="inward_date" value="{!!checkEmptyDate($row->inward_date)!!}" disabled  />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">M7 Number / نمبر میم هفت</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="m7_number" value="{!!$row->m7_number!!}" disabled />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Store Keeper / معتمد</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="store_keeper" style="width:100%" disabled>
                        {!!getStoreKeepers($row->store_keeper)!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Warranty/Guarantee /تصمین یا گرانتی</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="warranty" disabled>
                        {!!getWarrantyList($row->warranty)!!}
                    </select>
                </div>
            </div>
        </div>
    	{!!Form::token();!!}
    	<hr />

    </div>
    <div id="reject_form_div" style="display:none">
        <div class="form-group">
            <div class="col-sm-5">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Reason of rejecting the Inward / دلیل رد کردن فارم رسید جنس</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="reject_reason" id="reject_reason"></textarea>
                </div>
            </div>
        </div>
        <hr />
    </div>
    <div class="modal-footer">
        <a class="btn btn-danger" href="{!!URL::route('getAssetsMgmtPurchaseInwardList')!!}"><i class="fa fa-remove"></i> Close</a>
        @if($row->authorized == 0)
        <button type="button" class="btn btn-success" id="authorize_btn" onclick="AuthorizeOrRejectInward()">Authorize | تائید کردن</button>
        @endif
        @if($row->reject_reason == "" && $row->authorized == 0)
        <button type="button" class="btn btn-danger" id="reject_btn" onclick="bringRejectForm()">Reject | رد کردن</button>
        <button type="button" class="btn btn-danger" id="reject_inward" onclick="AuthorizeOrRejectInward()" style="display:none">Reject | رد کردن</button>
        @endif
    </div>
</form>

@stop

@section('footer-scripts') 
<script type="text/javascript">

    function bringRejectForm()
    {
        $("#reject_btn").hide();
        $('#reject_form_div').fadeIn('slow');
        $('#reject_inward').fadeIn('slow');
        $('#authorize_btn').hide('fast');
    }

    function AuthorizeOrRejectInward()
    { 
        var record_id = $("#record_id").val();
        var reject_reason = $("#reject_reason").val();
        var table = "inward";
        $.ajax({
            type: 'POST',
            url: "{!!URL::route('AuthorizeOrRejectRecords')!!}",
            data: "table="+table+"&record_id="+record_id+"&reject_reason="+reject_reason,
            beforeSend: function(){
                $('#submit_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#submit_result').html(response);
                var delay = 2500; 
                setTimeout(function() { window.location = "{!!URL::route('getAssetsMgmtPurchaseInwardList')!!}"; }, delay);
            }
        })
        return false;
    }

</script>
@stop