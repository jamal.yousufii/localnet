<div id="submit_result">
</div>
<form class="form-horizontal" id="inward_form">
<input type="hidden" name="purchase_id" value="{!!Crypt::encrypt($row->id)!!}" />
    <div class="modal-body">
        <h4 align="center">Purchase Details | جزئیات خریداری</h4>
        <hr />
        <div class="form-group">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sanction By / تصویب شده توسط</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sanction_by" id="sanction_by" style="width: 100%" disabled>
                    {!!getSanctions($row->sanction_by);!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Peshnehad Number / نمبر پیشنهاد</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="peshnehad_number" value="{!!$row->peshnehad_number!!}" disabled />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Order Date / تاریخ سفارش</label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="order_date" value="{!!checkEmptyDate($row->order_date)!!}" disabled />
                </div>
            </div> 
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Remarks / ملاحظات</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="remarks" disabled>{!!$row->remarks!!}</textarea>
                </div>
            </div>  
        </div>
        <div class="form-group">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Number of Items / تعداد اجناس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="number_of_items" id="number_of_items" value="{!!$row->number_of_items!!}" disabled />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Unit Price / قیمت فی جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="unit_price" id="unit_price" value="{!!$row->unit_price!!}" disabled />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Total Cost / قیمت مجموعی</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="total_cost" id="total_cost" value="{!!$row->total_cost!!}" disabled />
                </div>
            </div>
        </div>
        <div class="form-group">
            <h4 align="center">Inward form | فورمه رسید جنس</h4>
            <h6 align="center">Fields marked with an asterisk <span style="color:red">*</span> are required / فیلد های که همرای علامه ستاره سرخ رنگ نشانی شده ضروری میباشند</h6>
            <hr />
            
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Inward Date / تاریخ رسید جنس <span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="inward_date" id="inward_date" readonly required  />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">M7 Number / نمبر میم هفت <span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="m7_number" required />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Store Keeper / معتمد <span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="store_keeper" style="width:100%" required>
                        {!!getStoreKeepers()!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Warranty/Guarantee /تضمین یا گرانتی <span style="color:red"> * </span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="warranty" required>
                        {!!getWarrantyList()!!}
                    </select>
                </div>
            </div>
        </div>
            {!!Form::token();!!}
            <hr />
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info" onclick="add_inward()">Add Inward | تائید کردن فورمه رسید</button>
            </div>
    </div>
</form>

<script>

    $("#sanction_by").select2();
    $("#item_detail_code").select2();

    (function($) {

        $("#unit_price").on("blur", function(){    
            var unit_price = $("#unit_price").val();
            var number_of_items = $("#number_of_items").val();
            var total_cost = unit_price*number_of_items;
            $("#total_cost").val(total_cost);
        });

    })(jQuery);

</script>
