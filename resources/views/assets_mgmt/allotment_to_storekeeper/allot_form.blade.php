@extends('layouts.master')
@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
<strong>Whoops!</strong> There were some problems with your input.<br><br>
<ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
</div>
@endif
@if(Session::has('success'))
<div class='alert alert-success noprint'>{{Session::get('success')}}</div>

@elseif(Session::has('fail'))
<div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
@endif

<form class="form-horizontal" id="asset_allotment_to_storekeeper_form">
    <div class="modal-body">
        <div class="container">
            <h4 align="center">Allotment to Storekeeper Form | فورمه توزیع جنس به معتمد</h4>
            <h6 align="center">Fields marked with an asterisk <span style="color:red">*</span> are required / فیلد های که همرای علامه ستاره سرخ رنگ نشانی شده ضروری میباشند</h6>
        </div>
        <hr />
        
        <div class="form-group">
            <div id="item_detail_info"></div>
        </div>

        <div class="form-group">

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allotment Number / نمبر توزیع جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="allotment_number" id="allotment_number" />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Storekeeper / معتمد<span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="storekeeper" id="storekeeper" style="width:100%" required>
                        {!!getStoreKeepers()!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Detail Item Code / کود جزئیات جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="item_detail_id" id="item_detail_id" onchange="detailsOfSelectedItem(this.value)" style="width:100%" required>
                        {!!getDetailItemsNotAllotted()!!}
                    </select>
                </div>
            </div>

        </div>
        
        <div class="form-group">

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sanction By / منظور شده توسط <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sanction_by" id="sanction_by" style="width:100%" required>
                        {!!getSanctions();!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allotment Return Date / تاریخ توزیع جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="allotment_return_date" id="allotment_return_date" readonly required />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Remakrs / ملاحظات</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="remarks"></textarea>
                </div>
            </div>

        </div>
        {!!Form::token();!!}
        <hr />
        <div id="submit_result">
        </div>
        <div class="modal-footer">
            <a class="btn btn-danger" href="{!!URL::route('getAssetAllotmentToStorekeeper')!!}">Close / بسته کردن</a>
            <button type="button" class="btn btn-info" onclick="add_asset_allotment_to_storekeeper()">Submit | تائید کردن</button>
        </div>
    </div>
</form>
@stop

@section('footer-scripts') 
<script type="text/javascript">

    $("#sanction_by").select2();
    $("#item_detail_code").select2();
    $("#storekeeper").select2();
    $("#item_detail_id").select2();

    function detailsOfSelectedItem(item_detail_id)
    {
        $.ajax({
            url: '{!!URL::route("getItemDetailInfo")!!}',
            data: "item_detail_id="+item_detail_id,
            type: 'post',
            beforeSend: function(){
                $('#item_detail_info').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#item_detail_info').html(response);
                $('#item_detail_info').css("margin","20px");
                $('#exampleModalPrimary').modal("show");
            }
        })
    }

    function add_asset_allotment_to_storekeeper()
    {
        var dataString = $("#asset_allotment_to_storekeeper_form").serialize();
        $.ajax({
        type: 'POST',
        url: "{!!URL::route('addAssetAllotmentToStorekeeper')!!}",
        data: dataString,
        beforeSend: function(){
            $('#submit_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            //if there is error in form validation, then don't redirect to the inward list, stay in this form.
            if(response == "error")
            {
            $('#submit_result').html("<div class='alert alert-danger'>لطفآ فورمه را درست خانه پری نمائید </div>");
            }
            else{

            $('#submit_result').html(response);
            var delay = 2000; 
            setTimeout(function() { window.location = "{!!URL::route('getAssetAllotmentToStorekeeper')!!}"; }, delay);
            }
        }
        })
        return false;
    }

</script>
@stop