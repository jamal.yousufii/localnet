
<form class="form-horizontal" id="allotment_to_storekeeper_edit_form">

    <div class="container">
        <h4 align="center">Edit Form | فورمه تجدید</h4>
        <h6 align="center">Fields marked with an asterisk <span style="color:red">*</span> are required / فیلد های که همرای علامه ستاره سرخ رنگ نشانی شده ضروری میباشند</h6>
        <hr />
        <div class="form-group" style="margin-bottom:-30px">
            <div id="item_detail_info"></div>
        </div>
    </div>
    <hr />

    <div class="modal-body">

        <div class="form-group">
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allotment Number / نمبر توزیع جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="allotment_number" id="allotment_number" value="{!!$row->allotment_number!!}" />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Storekeeper / معتمد<span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="storekeeper" id="storekeeper" style="width:100%" required>
                        {!!getStoreKeepers($row->storekeeper)!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Detail Item Code / کود جزئیات جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="item_detail_id" id="item_detail_id" onchange="detailsOfSelectedItem(this.value)" style="width:100%" required>
                        {!!getItemDetailCodeList($row->item_detail_id)!!}
                    </select>
                </div>
            </div>

        </div>
        
        <div class="form-group">

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sanction By / منظور شده توسط <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sanction_by" id="sanction_by" style="width:100%" required>
                        {!!getSanctions($row->sanction_by);!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allotment Return Date / تاریخ توزیع جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="allotment_return_date" id="allotment_return_date" value="{!!checkEmptyDate($row->allotment_return_date)!!}" readonly required />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Remakrs / ملاحظات</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="remarks">{!!$row->remarks!!}</textarea>
                </div>
            </div>

        </div>
        <input type="hidden" name="allotment_to_storekeeper_id" value="{!!$row->id!!}" />
    	{!!Form::token();!!}
    	<hr />
        <div id="submit_result">
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close / بسته کردن فورمه</button>
        <button type="button" class="btn btn-info" onclick="updateAllotmentToStoreKeeperDetails()">Update | تجدید کردن</button>
    </div>
</form>

<script>

    $("#sanction_by").select2();
    $("#item_detail_code").select2();
    $("#general_dept").select2();
    $("#sub_dept").select2();
    $("#from_person").select2();
    $("#item_detail_id").select2();
    $("#sanction_by").select2();
    $("#received_by").select2();

    // bring the item details based on item detail id on page load.
    var item_detail_id = "{!!$row->item_detail_id!!}";
    $.ajax({
        url: '{!!URL::route("getItemDetailInfo")!!}',
        data: "item_detail_id="+item_detail_id,
        type: 'post',
        beforeSend: function(){
            $('#item_detail_info').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        },
        success: function(response)
        {
            $('#item_detail_info').html(response);
            $('#exampleModalPrimary').modal("show");
        }
    })

    function detailsOfSelectedItem(item_detail_id)
    {
        $.ajax({
            url: '{!!URL::route("getItemDetailInfo")!!}',
            data: "item_detail_id="+item_detail_id,
            type: 'post',
            beforeSend: function(){
                $('#item_detail_info').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#item_detail_info').html(response);
                $('#exampleModalPrimary').modal("show");
            }
        })
    }

</script>
