@extends('layouts.master')

@section('head')
    @parent
    <title>Allotment Return Details</title>
    <style type="text/css">
      a{cursor: pointer;}
      .modal-open .select2-dropdown {
        z-index: 10060;
      }
    </style>
@stop


@section('content')
<div id="submit_result">
</div>
<form class="form-horizontal" id="allotment_to_storekeeper_form">
    <div class="modal-body">

        <div class="form-group">
            <h4 align="center">Allotment to Storekeeper Details | جزئیات توزیع جنس به معتمد</h4><hr />
            
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allotment Number / نمبر توزیع جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="allotment_number" id="allotment_number" value="{!!$row->allotment_number!!}" disabled />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Storekeeper / معتمد<span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="storekeeper" id="storekeeper" style="width:100%" disabled>
                        {!!getStoreKeepers($row->storekeeper)!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Detail Item Code / کود جزئیات جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="item_detail_id" id="item_detail_id" onchange="detailsOfSelectedItem(this.value)" style="width:100%" disabled>
                        {!!getItemDetailCodeList($row->item_detail_id)!!}
                    </select>
                </div>
            </div>

        </div>
        
        <div class="form-group">

            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sanction By / منظور شده توسط <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sanction_by" id="sanction_by" style="width:100%" disabled>
                        {!!getSanctions($row->sanction_by);!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Allotment Return Date / تاریخ توزیع جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="allotment_return_date" id="allotment_return_date" value="{!!checkEmptyDate($row->allotment_return_date)!!}" readonly disabled />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Remakrs / ملاحظات</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="remarks" disabled>{!!$row->remarks!!}</textarea>
                </div>
            </div>

        </div>
    	{!!Form::token();!!}
    	<hr />

    </div>
    <div id="reject_form_div" style="display:none">
        <div class="form-group">
            <div class="col-sm-5">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Reason of rejecting the return / دلیل رد کردن فارم</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="reject_reason" id="reject_reason"></textarea>
                </div>
            </div>
        </div>
        <hr />
    </div>

    <div class="modal-footer">
        <a class="btn btn-danger" href="{!!URL::route('getAssetAllotmentToStorekeeper')!!}"><i class="fa fa-remove"></i> Close</a>
        @if($row->authorized == 0)
        <button type="button" class="btn btn-success" id="authorize_btn" onclick="AuthorizeOrRejectAllotment()">Authorize | تائید کردن</button>
        @endif
        @if($row->reject_reason == "" && $row->authorized == 0)
        <button type="button" class="btn btn-danger" id="reject_btn" onclick="bringRejectForm()">Reject | رد کردن</button>
        <button type="button" class="btn btn-danger" id="reject_allotment" onclick="AuthorizeOrRejectAllotment()" style="display:none">Reject | رد کردن</button>
        @endif
    </div>
</form>

@stop

@section('footer-scripts') 
<script type="text/javascript">

    function bringRejectForm()
    {
        $("#reject_btn").hide();
        $('#reject_form_div').fadeIn('slow');
        $('#reject_allotment').fadeIn('slow');
        $('#authorize_btn').hide('fast');
    }

    function AuthorizeOrRejectAllotment()
    { 
        var record_id = $("#record_id").val();
        var reject_reason = $("#reject_reason").val();
        var table = "allotment";
        $.ajax({
            type: 'POST',
            url: "{!!URL::route('AuthorizeOrRejectRecords')!!}",
            data: "table="+table+"&record_id="+record_id+"&reject_reason="+reject_reason,
            beforeSend: function(){
                $('#submit_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#submit_result').html(response);
                var delay = 2500; 
                setTimeout(function() { window.location = "{!!URL::route('getAssetAllotList')!!}"; }, delay);
            }
        })
        return false;
    }

</script>
@stop