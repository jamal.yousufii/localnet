@extends('layouts.master')
@section('content')

<title>Asset Settings</title>
<style type="text/css">
 table,tr, td {
    text-align: start;
 ;
  }
table,thead,tr,th{
  text-align: center;

}
table,thead,tr,th{
  text-align: center;

}
tr:nth-child(even) {background-color: #f2f2f2}




</style>
<style type="text/css">
#myBtn {
    display: none;
    position: fixed; 
    bottom: 10px;
    right: 30px; 
    z-index: 99; 
    border: none;
    outline: none; 
    cursor: pointer; 
    padding: 15px; 
 
}

</style>
<a onclick="topFunction()" id="myBtn">{!! HTML::image('/img/t.png', 'Logo', array('class' => 'normal-logo logo-white', 'width' => '45px')) !!}</a>
<script type="text/javascript">
  window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera 
    document.documentElement.scrollTop = 0; // For IE and Firefox
}
</script>
<div style="margin:20px">

  <div class="page-head">
    <h3 style="margin-bottom: 20px"> Initial necessary information settings for upcoming forms / بخش تنظیمات معلومات ضروری اولیه برای فارم های بعدی</h3>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
      <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(2)").addClass("active");
      </script>
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input. / در خانه پری فورمه ای تان مشکل وجود دارد لطفآ چک کنید و دوباره کوشش نمائید<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <ul class="nav nav-tabs" id="ul_tabs" style="border-top: 1px solid lightgray; border-radius: ; height: 46px;"  >
        <li class="tab"  id="get_asset_nature"> <a  href="#tab1" data-toggle="tab" style=" font-weight: bold;">Asset Nature / نوعیت مصرفی</a></li>
        <li class="tab"  id="get_main_items"> <a  href="#tab2" data-toggle="tab" style=" font-weight: bold;">Main Items / جنس عمومی</a></li>
        <li class="tab" id="get_asset_type" ><a href="#tab3" data-toggle="tab"><b style="font-weight: bold;">Asset Type / نوعیت جنس (منقول و غیر منقول)</b></a></li>
        <li class="tab" id="get_sub_items" ><a href="#tab4" data-toggle="tab"><b style="font-weight: bold;">Sub Items / جنس فرعی</b></a></li>
        <li class="tab" id="get_end_items" ><a href="#tab5" data-toggle="tab"><b style="font-weight: bold;">End Items / جنس</b></a></li>
        <li class="tab" id="get_item_detail" ><a href="#tab6" data-toggle="tab"><b style="font-weight: bold;">Item Detail / جزئیات جنس</b></a></li>
        <li class="tab" id="get_sanctions" ><a href="#tab7" data-toggle="tab"><b style="font-weight: bold;">Sanction Person / شخص منظور کننده</b></a></li>
        <li class="tab" id="get_vendors" ><a href="#tab8" data-toggle="tab"><b style="font-weight: bold;">Vendor / کمپنی تولیدی جنس</b></a></li>
        <li class="tab" id="get_locations" ><a href="#tab9" data-toggle="tab"><b style="font-weight: bold;">Location / موقعیت دقیق جنس</b></a></li>
        <li class="tab" id="get_currencies" ><a href="#tab10" data-toggle="tab"><b style="font-weight: bold;">Currency / واحد پولی</b></a></li>
        <li class="tab" id="get_store_keepers" ><a href="#tab11" data-toggle="tab"><b style="font-weight: bold;">Store Keeper / معتمد</b></a></li>
      </ul><br><hr>

         <div class="tab-content" id="data"  >

        <!-- tabs contents -->
        
         </div> 
      
          </div>
         </div>
        </div>
     
 @stop

@section('footer-scripts') 

<script type="text/javascript">
$(document).ready(function() {
  if(window.location.hash == "")
  {
    $("#get_asset_nature").addClass("active");
    $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtAssetNature")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
  }
});
</script>

<script type="text/javascript">

  // $(window).load(function(){
  //     $('#get_asset_nature').trigger('click');
  // });

  $('#ul_tabs a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
  });
  
  // store the currently selected tab in the hash value
  $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
    
    var id = $(e.target).attr("href").substr(1);
    window.location.hash = id;
    
    if(id == "tab1")
    {
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtAssetNature")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    }
    else if(id=="tab2")
    {
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtMainItems")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    }
    else if(id=="tab3")
    {
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtAssetType")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    }
    else if(id=="tab4")
    {
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtSubItems")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    }
    else if(id=="tab5")
    {
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtEndItems")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    }
    else if(id=="tab6")
    {
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtItemDetail")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    }
    else if(id=="tab7")
    {
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtSanctions")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    }
    else if(id=="tab8")
    {
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtVendors")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    }
    else if(id=="tab9")
    {
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtLocations")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    }
    else if(id=="tab10")
    {
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtCurrency")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    }
    else if(id=="tab11")
    {
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtStoreKeeper")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    }

  });

  // on load of the page: switch to the currently selected tab
  var hash = window.location.hash;
  $('#ul_tabs a[href="' + hash + '"]').tab('show');

  $(document).ready(function() {
    
    $("#get_asset_nature").click( "li", function(evt) { 
       $.ajax({
          type:'get',
          url:'{!!URL::route("getAssetsMgmtAssetNature")!!}',
          success:function(data){
             $('#data').html(data);
          }
        })
        return false;
    });

    $("#get_main_items").click( "li", function(evt) { 
        $.ajax({
          type:'get',
          url:'{!!URL::route("getAssetsMgmtMainItems")!!}',
          success:function(data){
             $('#data').html(data);
          }
        })
        return false;
    });

    $("#get_asset_type").click( "li", function(evt) { 
       $.ajax({
          type:'get',
          url:'{!!URL::route("getAssetsMgmtAssetType")!!}',
          success:function(data){
             $('#data').html(data);
          }
        })

        return false;
    });

    $("#get_sub_items").click( "li", function(evt) {  

       $.ajax({
            type:'get',
            url:'{!!URL::route("getAssetsMgmtSubItems")!!}',
            success:function(data){
               $('#data').html(data);
              }
           })
          return false;
    });

    $("#get_end_items").click( "li", function(evt) {  

      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtEndItems")!!}',
        success:function(data){
           $('#data').html(data);              
         }
       })
      return false;
    });

    $("#get_item_detail").click( "li", function(evt) {  

      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtItemDetail")!!}',
        success:function(data){
           $('#data').html(data);              
         }
       })
      return false;
    });

    $("#get_sanctions").click( "li", function(evt) { 
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtSanctions")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
    });
    
    $("#get_vendors").click( "li", function(evt) { 
      return false;
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtVendors")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    });

    $("#get_locations").click( "li", function(evt) { 
      return false;
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtLocations")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    });

    $("#get_currencies").click( "li", function(evt) { 
      return false;
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtCurrency")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    });

    $("#get_store_keepers").click( "li", function(evt) { 
      return false;
      $.ajax({
        type:'get',
        url:'{!!URL::route("getAssetsMgmtStoreKeeper")!!}',
        success:function(data){
            $('#data').html(data);
        }
      })
      return false;
    });

  });

</script> 

@stop