<style type="text/css">
  table tr td, table tr th {
    text-align: center;
  }
  table tr th { font-weight: bold; font-size: 18px; }
  a:hover{cursor: pointer;}
</style>
<div class="col col-12">
<div class="header">
   <br>
    <div class="row">
     <div class="pull-left">
      <a href="#" id="add_new_vendor" class="btn btn-success" >Add Vendor / اضافه کردن کمپنی تولیدی</a>                
     </div>
    <div class="col-md-6 pull-right">
     <form class="form-horizontal group-border-dashed" id="search_form1">
       <div class="input-group custom-search-form">
        <input type="text" class="form-control" id="search_field1" name="record" placeholder="Search / جستجو" required />
          {!!Form::token();!!}
           <span class="input-group-btn">
            <button class="btn btn-default-sm" id="search_button1" cat_id="1"><i class="fa fa-search"></i> Search / جستجو</button>
        </span></div>
      </form>
    </div>
   </div>
  <hr />
  <div id="result"></div>
  <div id="vendor_div" style="display: none">
    <form class="form-horizontal group-border-dashed" id="vendor_form">
      <div class="form-group">
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">Vendor Code / کود</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control" name="vendor_code" placeholder="Vendor Code" />
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">Vendor Name / نام</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control" name="vendor_name" placeholder="Vendor Name" required />
          </div>
        </div>
        {!!Form::token();!!}
        <div class="col-sm-3" style="margin-top: 25px">
            <button type="submit" class="btn btn-primary" id="add_vendor">Submit / تائيد</button>
            <a href="#" class="btn btn-danger" id="cancel_vendor">Cancel / رد</a>
        </div>
      </div>
      <hr />
    </form>
  </div>
  <h3 style="margin-top:20px;" align="center">Vendors List / لست کمپنی های تولیدی</h3><hr />
</div>
<div id="search_result">          
  
  <table class="table table-bordered table-responsive">
    <thead>
          <tr>
            <th># / شماره</th>
            <th>Vendor Code / کود</th>
            <th>Vendor Name / نام کمپنی تولیدی</th>
            <th colspan="2">Actions / اجرای عملیات</th>
          
          </tr>
      </thead>

      <tbody>
        @if(!empty($records))
        <?php $counter = 1; ?>
              @foreach($records AS $item)
                <?php
                  $record_id = Crypt::encrypt($item->id); 
                ?>
                  <tr class="remove_record{!!$item->id!!}">
                    <td>{!!$counter!!}</td>
                    <td>{!!$item->code!!}</td>
                    <td>{!!$item->name!!}</td>
                    
                    
                    <td align='center' class="noprint"><a href="#" title="Edit / اصلاح کردن"><span class='fa fa-edit'></span></a></td>
                    <td align='center' class="noprint"><a onclick="deleteVendor(this.id)" id="{!!$item->id!!}" title='Delete Vendor / حذف کردن'><span class='fa fa-trash'></span></a></td>
                    
                  </tr>
                  <?php $counter++; ?>
              @endforeach
          @else
          <div style="padding: 10px" class="noprint">
            <span style="color:red">No Records Found / معلومات پیدا نشد</span>
          </div>
        @endif
      </tbody>
  </table>
</div>

<script type="text/javascript">

  $(document).ready(function() {
    
    $("#add_new_vendor").click(function(){
      $("#vendor_div").fadeIn("fast");
    });
    $("#cancel_vendor").click(function(){
      $("#vendor_div").hide("fast");
    });

    $("#vendor_form").submit(function(){

      var dataString = $('#vendor_form').serialize();
   
      $.ajax({
        type : "post",
        url : "{!!URL::route('addAssetMgmtVendor')!!}",
        data : dataString,
        success : function(response)
        {
          if(response == "error")
          {
            $("#result").html("<div class='alert alert-danger'>لطفآ فورمه را درست خانه پری نمائید و کود نباید که تکراری باشد.</div>");
          }
          else{
            $("#result").html(response);
            setTimeout(function() { getVendors(); }, 1500);
          }
        }
      });
        return false;
    });

    $("#search_button1").click(function(){
      var field_value = $('#search_field1').val();

         var tab_id= $(this).attr('cat_id');

   
      $.ajax({
        type : "post",
        url : "{!!URL::route('search_live')!!}",
        data : {"cat_id":$(this).attr('cat_id'),"field_value":field_value,"_token": "<?=csrf_token();?>"},
        success : function(response)
        {
          $("#datalist2").html(response);
        }
      });
        return false;
    });
    
  });

  function getVendors()
  {
    $.ajax({
      type:'get',
      url:'{!!URL::route("getAssetsMgmtVendors")!!}',
      success:function(data){
         $('#data').html(data);
      }
    });
  }

  function deleteVendor(record_id)
  {
    if (confirm("Are you sure you want to delete this Vendor? THERE IS NO UNDO")) 
    {
        $.ajax({
          url : '{!!URL::route("deleteAssetMgmtVendor")!!}',
          type : 'post',
          data : {'record_id':record_id,'table':'vendor','_token':"{!!csrf_token()!!}"},
          success : function(response)
          {
            $("#result").html(response);
            $(".remove_record"+record_id).css('background','Crimson');
            $(".remove_record"+record_id).slideUp('6000', function(){
            $(this).remove();
            setTimeout(function() { getVendors(); }, 1500);
            });

          }
        });
    }
  }


  </script>