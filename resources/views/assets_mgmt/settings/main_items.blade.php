<style type="text/css">
  table tr td, table tr th {
    text-align: center;
  }
  table tr th { font-weight: bold; font-size: 18px; }
  a:hover{cursor: pointer;}
</style>
<div class="col col-12">
<div class="header">
   <br>
    <div class="row">
     <div class="pull-left">
      <a href="#" id="add_new_item" class="btn btn-success" >Add Main Item / اضافه کردن جنس عمومی</a>                
     </div>
    <div class="col-md-6 pull-right">
     <form class="form-horizontal group-border-dashed" id="search_form1">
       <div class="input-group custom-search-form">
        <input type="text" class="form-control" id="search_field1" name="record" placeholder="Search / جستجو" required />
          {!!Form::token();!!}
           <span class="input-group-btn">
            <button class="btn btn-default-sm" id="search_button1" cat_id="1"><i class="fa fa-search"></i> Search / جستجو</button>
        </span></div>
      </form>
    </div>
   </div>
  <hr />
  <div id="result"></div>
  <div id="main_item_div" style="display: none">
    <form class="form-horizontal group-border-dashed" id="main_item_form">
      <div class="form-group">
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">Main Item Code / کود جنس عمومی</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control" name="main_item_code" placeholder="Main Item Code" />
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">Main Item Name / نام جنس عمومی</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control" name="main_item_name" placeholder="Main Item Name" required />
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">Asset Nature / نوع مصرفی</label>
          </div>
          <div class="col-sm-12">
            <select name="asset_nature" class="form-control" id="asset_nature" required>
              {!!getAssetNature()!!}
            </select>
          </div>
        </div>
        {!!Form::token();!!}
        <div class="col-sm-3" style="margin-top: 25px">
            <button type="submit" class="btn btn-primary" id="add_item">Submit / تائید</button>
            <a href="#" class="btn btn-danger" id="cancel_item">Cancel / رد</a>
        </div>
      </div>
      <hr />
    </form>
  </div>
  <h3 style="margin-top:20px;" align="center">Main Items List / لست اجناس عمومی</h3><hr />
</div>
<div id="search_result">          
  
  <table class="table table-bordered table-responsive">
    <thead>
          <tr>
            <th># / شماره</th>
            <th>Main Item Code / کود جنس عمومی</th>
            <th>Main Item Name / نام جنس عمومی</th>
            <th>Asset Nature / نوع مصرفی</th>
            <th colspan="2">Actions / اجرای عملیات</th>
          
          </tr>
      </thead>

      <tbody>
        @if(!empty($records))
        <?php $counter = 1; ?>
              @foreach($records AS $item)
                <?php
                  $record_id = Crypt::encrypt($item->id); 
                ?>
                  <tr class="remove_record{!!$item->id!!}">
                    <td>{!!$counter!!}</td>
                    <td>{!!$item->main_item_code!!}</td>
                    <td>{!!$item->main_item_name!!}</td>
                    <td>{!!getNameBasedOnId('asset_nature', 'name', $item->asset_nature)!!}</td>
                    
                    
                    <td align='center' class="noprint"><a href="#" title="Edit Item / اصلاح جنس"><span class='fa fa-edit'></span></a></td>
                    <td align='center' class="noprint"><a onclick="deleteItem(this.id)" id="{!!$item->id!!}" title='Delete Item / حذف جنس'><span class='fa fa-trash'></span></a></td>
                    
                  </tr>
                  <?php $counter++; ?>
              @endforeach
          @else
          <div style="padding: 10px" class="noprint">
            <span style="color:red">No Records Found / معلومات پیدا نشد</span>
          </div>
        @endif
      </tbody>
  </table>
</div>

<script type="text/javascript">

  $(document).ready(function() {
    
    $("#add_new_item").click(function(){
      $("#main_item_div").fadeIn("fast");
    });
    $("#cancel_item").click(function(){
      $("#main_item_div").hide("fast");
    });

    $("#main_item_form").submit(function(){

      var dataString = $('#main_item_form').serialize();
   
      $.ajax({
        type : "post",
        url : "{!!URL::route('addAssetMgmtItems','main_item')!!}",
        data : dataString,
        success : function(response)
        {
          if(response == "error"){
            $("#result").html("<div class='alert alert-danger'>لطفآ فورمه را درست خانه پری نمائید و کود نباید که تکراری باشد.</div>");
          }
          else{
            $("#result").html(response);
            setTimeout(function() { getMainItems(); }, 1500);
          }
        }
      });
        return false;
    });

    $("#search_button1").click(function(){
      var field_value = $('#search_field1').val();

         var tab_id= $(this).attr('cat_id');

   
      $.ajax({
        type : "post",
        url : "{!!URL::route('search_live')!!}",
        data : {"cat_id":$(this).attr('cat_id'),"field_value":field_value,"_token": "<?=csrf_token();?>"},
        success : function(response)
        {
          $("#datalist2").html(response);
        }
      });
        return false;
    });
    
  });

  function getMainItems()
  {
    $.ajax({
      type:'get',
      url:'{!!URL::route("getAssetsMgmtMainItems")!!}',
      success:function(data){
         $('#data').html(data);
      }
    });
  }

  function deleteItem(record_id)
  {
    if (confirm("Are you sure you want to delete the Main Item? THERE IS NO UNDO")) 
    {
        $.ajax({
          url : '{!!URL::route("deleteAssetMgmtItem")!!}',
          type : 'post',
          data : {'record_id':record_id,'table':'main_item','_token':"{!!csrf_token()!!}"},
          success : function(response)
          {
            $("#result").html(response);
            $(".remove_record"+record_id).css('background','Crimson');
            $(".remove_record"+record_id).slideUp('6000', function(){
            $(this).remove();
            setTimeout(function() { getMainItems(); }, 1500);
            });

          }
        });
    }
  }


  </script>


  <script type="text/javascript">
  $(document).ready(function() {

  $('.pagination a').on('click', function(event) {
    event.preventDefault();
    if ($(this).attr('href') != '#') {
      //$('#ajaxContent').load($(this).attr('href'));
      var dataString = $('#search_form').serialize();
      dataString += "&page="+$(this).text()+"&ajax="+1;
      $.ajax({
          url: '{!!URL::route("get_copy_som_data")!!}',
          data: dataString,
          type: 'get',
          beforeSend: function(){
              //$("body").show().css({"opacity": "0.5"});
              $('#page_data').html('<span style="margin-right:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#page_data').html(response);
          }
      }
    );
    
    }
  });
    });

    $(document).ready(function() {
      $(".pagination a").on("click", function(){
      $(".pagination").find(".active").removeClass("active");
      $(this).parent().addClass("active");
   });
  });
  </script>