<style type="text/css">
  table tr td, table tr th {
    text-align: center;
  }
  table tr th { font-weight: bold; font-size: 18px; }
  a:hover{cursor: pointer;}
</style>
<div class="col col-12">
<div class="header">
   <br>
    <div class="row">
     <div class="pull-left">
      <a href="#" id="add_new_location" class="btn btn-success" >Add Location / اضافه کردن موقعیت</a>                
     </div>
    <div class="col-md-6 pull-right">
     <form class="form-horizontal group-border-dashed" id="search_form1">
       <div class="input-group custom-search-form">
        <input type="text" class="form-control" id="search_field1" name="record" placeholder="Search / جستجو" required />
          {!!Form::token();!!}
           <span class="input-group-btn">
            <button class="btn btn-default-sm" id="search_button1" cat_id="1"><i class="fa fa-search"></i> Search / جستجو</button>
        </span></div>
      </form>
    </div>
   </div>
  <hr />
  <div id="result"></div>
  <div id="location_div" style="display: none">
    <form class="form-horizontal group-border-dashed" id="location_form">
      <div class="form-group">
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">Location Code / کود موقعیت</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control" name="code" placeholder="Location Code" required />
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">General Department / ادارۀ عمومی</label>
          </div>
          <div class="col-sm-12">
            <select name="general_dept" id="general_dept" class="form-control" onchange="bringRelatedSubDepartment('sub_dept',this.value)" style='width:100%' required>
                <option value="0">انتخاب</option>
                @foreach($parentDeps AS $dep_item)
                    <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                @endforeach
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">Sub Department / ادارۀ مربوط</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="sub_dept" id="sub_dept" style='width:100%'>
                <option value='0'>انتخاب</option>
            </select>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">Exact Location / موقعیت دقیق</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control" name="exact_location" placeholder="Write Exact Location" required />
          </div>
        </div>
        {!!Form::token();!!}
        <div class="col-sm-3" style="margin-top: 25px">
            <button type="submit" class="btn btn-primary" id="add_location">Submit / تائید</button>
            <a href="#" class="btn btn-danger" id="cancel_location">Cancel / رد</a>
        </div>
      </div>
      <hr />
    </form>
  </div>
  <h3 style="margin-top:20px;" align="center">Locations List / لست موقعیت ها</h3><hr />
</div>
<div id="search_result">          
  
  <table class="table table-bordered table-responsive">
    <thead>
          <tr>
            <th># / شماره</th>
            <th>Location Code / کود موقعیت</th>
            <th>General Department / اداره عمومی</th>
            <th>Sub Department / اداره فرعی</th>
            <th>Exact Location / موقعیت دقیق</th>
            <th colspan="2">Actions / اجرای عملیات</th>
          
          </tr>
      </thead>

      <tbody>
        @if(!empty($records))
        <?php $counter = 1; ?>
              @foreach($records AS $item)
                <?php
                  $record_id = Crypt::encrypt($item->id); 
                ?>
                  <tr class="remove_record{!!$item->id!!}">
                    <td>{!!$counter!!}</td>
                    <td>{!!$item->code!!}</td>
                    <td>{!!$item->general_dept!!}</td>
                    <td>{!!$item->sub_dept!!}</td>
                    <th>{!!$item->exact_location!!}</th>
                    
                    
                    <td align='center' class="noprint"><a href="#" title="Edit Location / اصلاح کردن"><span class='fa fa-edit'></span></a></td>
                    <td align='center' class="noprint"><a onclick="deleteLocation(this.id)" id="{!!$item->id!!}" title='Delete Location / حذف کردن'><span class='fa fa-trash'></span></a></td>
                    
                  </tr>
                  <?php $counter++; ?>
              @endforeach
          @else
          <div style="padding: 10px" class="noprint">
            <span style="color:red">No Records Found / معلومات پیدا نشد</span>
          </div>
        @endif
      </tbody>
  </table>

</div>

<script type="text/javascript">

  $("#general_dept").select2();
  $("#sub_dept").select2();

  $(document).ready(function() {
    
    $("#add_new_location").click(function(){
      $("#location_div").fadeIn("fast");
    });
    $("#cancel_location").click(function(){
      $("#location_div").hide("fast");
    });

    $("#location_form").submit(function(){

      var dataString = $('#location_form').serialize();
   
      $.ajax({
        type : "post",
        url : "{!!URL::route('addAssetMgmtLocation')!!}",
        data : dataString,
        success : function(response)
        {
          if(response == "error")
          {
            $("#result").html("<div class='alert alert-danger'>لطفآ فورمه را درست خانه پری نمائید و کود نباید که تکراری باشد.</div>");
          }
          else{
            $("#result").html(response);
            setTimeout(function() { getLocations(); }, 1500);
          }
        }
      });
        return false;
    });

    $("#search_button1").click(function(){
      var field_value = $('#search_field1').val();

         var tab_id= $(this).attr('cat_id');

   
      $.ajax({
        type : "post",
        url : "{!!URL::route('search_live')!!}",
        data : {"cat_id":$(this).attr('cat_id'),"field_value":field_value,"_token": "<?=csrf_token();?>"},
        success : function(response)
        {
          $("#datalist2").html(response);
        }
      });
        return false;
    });
    
  });

  function getLocations()
  {
    $.ajax({
      type:'get',
      url:'{!!URL::route("getAssetsMgmtLocations")!!}',
      success:function(data){
         $('#data').html(data);
      }
    });
  }

function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}

  function deleteLocation(record_id)
  {
    if (confirm("آیا مطمئین هستید ؟ موقعیت تمام خریداری ها و رسید اجناس نیز حذف خواهد شد.")) 
    {
        $.ajax({
          url : '{!!URL::route("deleteAssetMgmtLocation")!!}',
          type : 'post',
          data : {'record_id':record_id,'table':'location','_token':"{!!csrf_token()!!}"},
          success : function(response)
          {
            $("#result").html(response);
            $(".remove_record"+record_id).css('background','Crimson');
            $(".remove_record"+record_id).slideUp('6000', function(){
            $(this).remove();
            setTimeout(function() { getLocations(); }, 1500);
            });

          }
        });
    }
  }


  </script>