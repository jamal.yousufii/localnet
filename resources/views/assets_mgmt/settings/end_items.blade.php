
<style type="text/css">
  table tr td, table tr th {
    text-align: center;
  }
  table tr th { font-weight: bold; font-size: 18px; }
  a:hover{cursor: pointer;}
</style>

<div class="col col-12">
<div class="header">
   <br>
    <div class="row">
     <div class="pull-left">
      <a href="#" id="add_new_item" class="btn btn-success" >Add End Item / اضافه کردن جنس</a>              
     </div>
    <div class="col-md-6 pull-right">
     <form class="form-horizontal group-border-dashed" id="search_form1">
       <div class="input-group custom-search-form">
        <input type="text" class="form-control" id="search_field1" name="record" placeholder="Search / جستجو" required />
          {!!Form::token();!!}
           <span class="input-group-btn">
            <button class="btn btn-default-sm" id="search_button1" cat_id="3"><i class="fa fa-search"></i> Search / جستجو</button>
        </span></div>
      </form>
    </div>
   </div>
   <hr />
  <div id="result"></div>
  <div id="end_item_div" style="display: none">
    <form class="form-horizontal group-border-dashed" id="end_item_form">
      <div class="form-group">
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">Main Items / لست اجناس عمومی</label>
          </div>
          <div class="col-sm-12">
            <select name="main_item_name" id="main_items" class="form-control" style="width: 100%" required>
              {!!getFixedAssetMainItems()!!}
            </select>
          </div>
        </div>
        <input type="hidden" class="form-control" name="main_item_code" id="main_item_code" readonly="" />
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">Sub Items / لست اجناس فرعی</label>
          </div>
          <div class="col-sm-12">
            <select name="sub_item_name" id="sub_items" class="form-control" style="width: 100%" required>
              {!!getFixedAssetSubItems()!!}
            </select>
          </div>
        </div>
        <input type="hidden" class="form-control" name="sub_item_code" id="sub_item_code" readonly="" />
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">End Item Code / کود جنس</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control" name="end_item_code" placeholder="End Item Code" required />
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">End Item Name / نام جنس</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control" name="end_item_name" placeholder="End Item Name" required />
          </div>
        </div>
      </div>
      <div class="form-group">
        {!!Form::token();!!}
        <div class="col-sm-3" style="margin-top: 25px">
            <button type="submit" class="btn btn-primary" id="add_item">Submit / تائید</button>
            <a href="#" class="btn btn-danger" id="cancel_item">Cancel / رد</a>
        </div>
      </div>
      <hr />
    </form>
  </div>
  <h3 style="margin-top:20px;" align="center">End Items List / لست اجناس</h3><hr />
</div>
<div id="search_result">          
  
<table class="table table-bordered table-responsive">
    <thead>
          <tr>
            <th># / شماره</th>
            <th>Main Item Code / کود جنس عمومی</th>
            <th>Sub Item Code / کود جنس فرعی</th>
            <th>End Item Code / کود جنس</th>
            <th>End Item Name / نام جنس</th>
            <th colspan="2">Actions / اجرای عملیات</th>
          
          </tr>
      </thead>

      <tbody>
        @if(!empty($records))
        <?php $counter = 1; ?>
              @foreach($records AS $item)
                <?php
                  $record_id = Crypt::encrypt($item->id); 
                ?>
                  <tr class="remove_record{!!$item->id!!}">
                    <td>{!!$counter!!}</td>
                    <td>{!!$item->main_item_code!!}</td>
                    <td>{!!$item->sub_item_code!!}</td>
                    <td>{!!$item->end_item_code!!}</td>
                    <td>{!!$item->end_item_name!!}</td>
                    
                    
                    <td align='center' class="noprint"><a href="#" title="Edit Item / اصلاح کردن"><span class='fa fa-edit'></span></a></td>
                    <td align='center' class="noprint"><a onclick="deleteItem(this.id)" id="{!!$item->id!!}" title='Delete Item / حذف کردن'><span class='fa fa-trash'></span></a></td>
                    
                  </tr>
                  <?php $counter++; ?>
              @endforeach
          @else
          <div style="padding: 10px" class="noprint">
            <span style="color:red">No Records Found / معلومات پیدا نشد</span>
          </div>
        @endif
      </tbody>
  </table>

</div>

  <script type="text/javascript">

  $(document).ready(function() {
    
    $('#main_items').select2();
    $('#sub_items').select2();

    $("#add_new_item").click(function(){
      $("#end_item_div").fadeIn("fast");
    });
    $("#cancel_item").click(function(){
      $("#end_item_div").hide("fast");
    });

    $("#end_item_form").submit(function(){

      var dataString = $('#end_item_form').serialize();
   
      $.ajax({
        type : "post",
        url : "{!!URL::route('addAssetMgmtItems','end_item')!!}",
        data : dataString,
        success : function(response)
        {
          if(response == "error"){
            $("#result").html("<div class='alert alert-danger'>لطفآ فورمه را درست خانه پری نمائید و کود نباید که تکراری باشد.</div>");
          }
          else{
            $("#result").html(response);
            setTimeout(function() { getEndItems(); }, 1500);
          }
        }
      });
        return false;
    });

    // on change of the main items select box bring the code of related main item.
    $("#main_items").change(function(){
      var selected_item = $(this).val();
      $.ajax({
        type : "post",
        url : "{!!URL::route('getMainItemCode')!!}",
        data : "main_item_id="+selected_item,
        success : function(response)
        {
          $("#main_item_code").val(response);
          //$("#main_item_code_div").fadeIn("fast");
        }
      });
        return false;
    });

    // on change of the sub items select box bring the code of related main item.
    $("#sub_items").change(function(){
      var selected_item = $(this).val();
      $.ajax({
        type : "post",
        url : "{!!URL::route('getSubItemCode')!!}",
        data : "sub_item_id="+selected_item,
        success : function(response)
        {
          $("#sub_item_code").val(response);
          //$("#sub_item_code_div").fadeIn("fast");
        }
      });
        return false;
    });

    $("#search_button1").click(function(){
      var field_value = $('#search_field1').val();

         var tab_id= $(this).attr('cat_id');

   
      $.ajax({
        type : "post",
        url : "{!!URL::route('search_live')!!}",
        data : {"cat_id":$(this).attr('cat_id'),"field_value":field_value,"_token": "<?=csrf_token();?>"},
        success : function(response)
        {
          $("#datalist2").html(response);
        }
      });
        return false;
    });
    
  });

  function getEndItems()
  {
    $.ajax({
      type:'get',
      url:'{!!URL::route("getAssetsMgmtEndItems")!!}',
      success:function(data){
         $('#data').html(data);
      }
    });
  }

  function deleteItem(record_id)
  {
    if (confirm("Are you sure you want to delete the End Item? THERE IS NO UNDO")) 
    {
        $.ajax({
          url : '{!!URL::route("deleteAssetMgmtItem")!!}',
          type : 'post',
          data : {'record_id':record_id,'table':'end_item','_token':"{!!csrf_token()!!}"},
          success : function(response)
          {
            $("#result").html(response);
            $(".remove_record"+record_id).css('background','Crimson');
            $(".remove_record"+record_id).slideUp('6000', function(){
            $(this).remove();
            setTimeout(function() { getEndItems(); }, 1500);
            });

          }
        });
    }
  }

  </script>

  <script type="text/javascript">
  $(document).ready(function() {

  $('.pagination a').on('click', function(event) {
    event.preventDefault();
    if ($(this).attr('href') != '#') {
      //$('#ajaxContent').load($(this).attr('href'));
      var dataString = $('#search_form').serialize();
      dataString += "&page="+$(this).text()+"&ajax="+1;
      $.ajax({
          url: '{!!URL::route("get_copy_som_data")!!}',
          data: dataString,
          type: 'get',
          beforeSend: function(){
              //$("body").show().css({"opacity": "0.5"});
              $('#page_data').html('<span style="margin-right:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#page_data').html(response);
          }
      }
    );
    
    }
  });
    });

    $(document).ready(function() {
      $(".pagination a").on("click", function(){
      $(".pagination").find(".active").removeClass("active");
      $(this).parent().addClass("active");
   });
  });
  </script>