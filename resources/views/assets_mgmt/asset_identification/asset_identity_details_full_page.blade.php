@extends('layouts.master')

@section('head')
    @parent
    <title>Asset Identification List</title>
    <style type="text/css">
      a{cursor: pointer;}
      .modal-open .select2-dropdown {
        z-index: 10060;
      }
    </style>
@stop


@section('content')
<form class="form-horizontal" style="margin:10px">

    <input type="hidden" class="form-control" name="record_id" id="record_id" value="{!!Crypt::encrypt($row->id)!!}" />

    <div class="form-group">
        <h4>Asset Identification Details | جزئیات تشخیص جنس</h4>
        <hr />
        <div class="col-sm-3">
            <div class="col-sm-12">
                <label class="col-sm-12 ">Item Location / موقعیت جنس</label>
            </div>
            <div class="col-sm-12">
                <select class="form-control" name="item_location" id="item_location" style='width:100%' disabled>
                    {!!getAuthDepartments($row->item_location)!!}
                </select>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12">
                <label class="col-sm-12 ">Asset Number /  نمبر یا کود جنس</label>
            </div>
            <div class="col-sm-12">
                <input type="text" class="form-control" name="asset_number" id="asset_number" disabled value="{!!$row->asset_number!!}" />
            </div>
        </div>
    </div>
    {!!Form::token();!!}
    <hr />

    </div>
    <div id="reject_form_div" style="display:none">
        <div class="form-group">
            <div class="col-sm-5">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Reason of rejecting the Purchase / دلیل رد کردن فارم خریداری جنس</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="reject_reason" id="reject_reason"></textarea>
                </div>
            </div>
        </div>
        <hr />
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
        @if($row->authorized == 0)
        <button type="button" class="btn btn-success" id="authorize_btn" onclick="AuthorizeOrRejectAssetIdentity()">Authorize | تائید کردن</button>
        @endif
        @if($row->reject_reason == "" && $row->authorized == 0)
        <button type="button" class="btn btn-danger" id="reject_btn" onclick="bringRejectForm()">Reject | رد کردن</button>
        <button type="button" class="btn btn-danger" id="reject_asset_identity" onclick="AuthorizeOrRejectAssetIdentity()" style="display:none">Reject | رد کردن</button>
        @endif
    </div>
</form>

@stop

@section('footer-scripts') 
<script type="text/javascript">

    function bringRejectForm()
    {
        $("#reject_btn").hide();
        $('#reject_form_div').fadeIn('slow');
        $('#reject_asset_identity').fadeIn('slow');
        $('#authorize_btn').hide('fast');
    }

    function AuthorizeOrRejectAssetIdentity()
    { 
        var record_id = $("#record_id").val();
        var reject_reason = $("#reject_reason").val();
        var table = "asset_identification";
        $.ajax({
            type: 'POST',
            url: "{!!URL::route('AuthorizeOrRejectRecords')!!}",
            data: "table="+table+"&record_id="+record_id+"&reject_reason="+reject_reason,
            beforeSend: function(){
                $('#submit_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#submit_result').html(response);
                var delay = 2500; 
                setTimeout(function() { window.location = "{!!URL::route('getAssetIdentificationList')!!}"; }, delay);
            }
        })
        return false;
    }

</script>
@stop