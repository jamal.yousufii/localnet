
<form class="form-horizontal" id="asset_identity_form">
<input type="hidden" name="inward_id" value="{!!Crypt::encrypt($inward_details->id)!!}" />
    <div class="modal-body">
        <h4 align="center">Purchase Details | جزئیات خریداری</h4><hr />
        <div class="form-group">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Sanction By / تصویب شده توسط</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="sanction_by" id="sanction_by" style="width: 100%" disabled>
                    {!!getSanctions($purchase_details->sanction_by);!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Peshnehad Number / نمبر پیشنهاد</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="peshnehad_number" value="{!!$purchase_details->peshnehad_number!!}" disabled />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Order Date / تاریخ سفارش</label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="order_date" value="{!!checkEmptyDate($purchase_details->order_date)!!}" disabled />
                </div>
            </div> 
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Remarks / ملاحظات</label>
                </div>
                <div class="col-sm-12">
                    <textarea class="form-control" name="remarks" disabled>{!!$purchase_details->remarks!!}</textarea>
                </div>
            </div>  
        </div>
        <div class="form-group">
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Number of Items / تعداد اجناس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="number_of_items" id="number_of_items" value="{!!$purchase_details->number_of_items!!}" disabled />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Unit Price / قیمت فی جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="unit_price" id="unit_price" value="{!!$purchase_details->unit_price!!}" disabled />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Total Cost / قیمت مجموعی</label>
                </div>
                <div class="col-sm-12">
                    <input type="number" class="form-control" name="total_cost" id="total_cost" value="{!!$purchase_details->total_cost!!}" disabled />
                </div>
            </div>
        </div>
        <div class="form-group">
            <h4 align="center">Inward Details | جزئیات رسید جنس</h4><hr />
            
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Inward Date / تاریخ رسید جنس</label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control datepicker_farsi" name="inward_date" id="inward_date" value="{!!checkEmptyDate($inward_details->inward_date)!!}" disabled  />
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Warranty / Guarantee /  تضمین یا گرانتی</label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="warranty" disabled>
                        {!!getWarrantyList($inward_details->warranty)!!}
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
        <hr />
            <h4 align="center">Asset Identification Form | فورمه تشخیص جنس</h4>
            <h6 align="center">Fields marked with an asterisk <span style="color:red">*</span> are required / فیلد های که همرای علامه ستاره سرخ رنگ نشانی شده ضروری میباشند</h6>
            <hr />
            
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Exact Location / موقعیت دقیق جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="item_location" id="item_location" style='width:100%' required>
                        {!!getExactLocations()!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Asset Number /  نمبر یا کود جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="asset_number" id="asset_number" value="{!!$item_detail_code!!}" required readonly />
                </div>
            </div>
        </div>
            {!!Form::token();!!}
            <div id="submit_result">
            </div>
            <hr />
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info" onclick="add_asset_identity()">Add Asset Identity | تائید کردن فورمه تشخیص جنس</button>
            </div>
    </div>
</form>

<script>

    $("#sanction_by").select2();
    $("#item_detail_code").select2();
    $("#item_location").select2();

</script>
