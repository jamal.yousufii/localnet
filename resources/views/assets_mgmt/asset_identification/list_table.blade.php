
<table class="table table-bordered table-responsive">
	<thead>
		<tr>
		<th>No# / شماره</th>
		<td>Item Detail Code / کود جزئیات جنس</td>
		<th>Item Location / موقعیت دقیق جنس</th>
		<th>Asset Number / نمبر یا کود عمومی جنس</th>
		<th>Reject Reason / دلیل رد کردن</th>

		<th colspan="4" class="noprint" style="text-align:center">Actions / اجرای عملیات</th>
		
		</tr>
	</thead>

	<tbody>
		@if(!empty($records))
		<?php $counter = $records->firstItem(); ?>
			@foreach($records AS $item)
				<?php
					$record_id = Crypt::encrypt($item->id); 
					$purchase_id = getNameBasedOnId("inward","purchase_id",$item->inward_id);
					$item_detail_id = getNameBasedOnId("purchase","item_detail_id",$purchase_id);
				?>
				<tr class="remove_record{!!$item->id!!}">
					<td>{!!$counter!!}</td>
					<td><a href="#" onclick="getPurchaseAndInwardDetails({!!$purchase_id!!},{!!$item->inward_id!!})">{!!getNameBasedOnId("item_detail","item_detail_code",$item_detail_id)!!}</a></td>
					<td>{!!$item->item_location!!}</td>
					<td>{!!$item->asset_number!!}</td>
					@if($item->authorized == 0)
					<td>{!!$item->reject_reason!!}</td>
					@else
					<td></td>
					@endif
					
					<?php $record_id = Crypt::encrypt($item->id);?>
					<td align='center' class="noprint"><a href="#" onclick="get_asset_identity_details({!!$item->id!!})" title="Edit / اصلاح کردن"><span class='fa fa-edit'></span></a></td>
					<td align='center' class="noprint"><a href="{!!URL::route('viewAssetIdentityDetails', array($record_id))!!}" title='Details / جزئیات بیشتر'><span class='fa fa-eye'></span></a></td>
					<td align='center' class="noprint"><a onclick="deleteAssetIdentity(this.id)" id="{!!$item->id!!}" title='Delete / حذف کردن'><span class='fa fa-trash'></span></a></td>
					@if($item->authorized == 1)
							<td align='center' class="noprint action"><span style="color:green;font-weight:bold">Authorized | تائیده شده</span></td>
							@elseif($item->reject_reason != "")
							<td align='center' class="noprint action"><span style="color:red;font-weight:bold">Rejected | رد شده</span></td>
							@else
							<td align='center' class="noprint action"><span style="color:orange;font-weight:bold">Pending | انتظار به تائید یا رد</span></td>
							@endif
					
				</tr>
				<?php $counter++; ?>
			@endforeach
		@else
		<div style="padding: 10px" class="noprint">
			<span style="color:red">No records exist in the system ! / معلومات در سیستم پیدا نشد</span>
		</div>
		@endif
	</tbody>
</table>
<script type="text/javascript">

	$(document).ready(function() {
    	$('td.text').each(function() {
	        var td = $(this);
	        var cs = td.text().length;
	        
	        if(cs>50)
	        {
	        	var shown = td.text().substring(0, 50);
	        	td.text(shown+'...');
	        }
	    });
	});

</script>