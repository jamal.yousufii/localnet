<div id="submit_result">
</div>
<form class="form-horizontal" id="asset_identity_edit_form">
    <div class="modal-body">
        <input type="hidden" name="asset_identity_id" value="{!!Crypt::encrypt($row->id)!!}" />

        <div class="form-group">
            <h4 align="center">Asset Identification Edit Form | فورمه تجدید تشخیص جنس</h4>
            <h6 align="center">Fields marked with an asterisk <span style="color:red">*</span> are required / فیلد های که همرای علامه ستاره سرخ رنگ نشانی شده ضروری میباشند</h6>
            <hr />
            
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Item Location / موقعیت جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <select class="form-control" name="item_location" id="item_location" style='width:100%' required>
                        {!!getAuthDepartments($row->item_location)!!}
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <label class="col-sm-12 ">Asset Number /  نمبر یا کود جنس <span style="color:red">*</span></label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="asset_number" id="asset_number" value="{!!$row->asset_number!!}" readonly required />
                </div>
            </div>
        </div>
    	{!!Form::token();!!}
    	<hr />

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-info" onclick="updateAssetIdentityDetails()">Update Asset Identity | تجدید تشخیص جنس</button>
    </div>
</form>

<script>

    

</script>
