@extends('layouts.master')

@section('head')
	@parent
	<title>اضافه کردن جلسه</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
    </style>
@stop

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
        <strong>Whoops !</strong>There were some problems with your input, please check it and try again. <br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
    </div>
@endif
@if(Session::has('success'))
    <div class='alert alert-success noprint'>{{Session::get('success')}}</div>
@elseif(Session::has('fail'))
    <div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
@endif
@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('getMeetings') !!}">جلسات روزانه</a>
                    </li>
                    <li class="active">
                        <span>اضافه کردن جلسه</span>
                    </li>
                </ol>
            </div>
        </div>
    </div>

    <?php
        /* 
            * get the today's date and change it to jalali and then change its format to jalali in order to send 
            * the value through ajax and check the special day on page load.
        */
        $date = date("Y-m-d");
        $date = jalali_format(convertToJalali($date));
    ?>
    <input type="hidden" value="{!!$date!!}" id="today_date" />
    <div style="margin: 40px;direction: rtl !important;">
        <!-- <div id="check_result" style="padding: 20px"></div> -->
        <h4 align="center">فورمه اضافه کردن جلسه</h4>
		<hr />
        <form role="form" method="post" action="{!! URL::route('addMeetingForm') !!}" class="form-horizontal" enctype="multipart/form-data">
        
			<div class="form-group">
                <div class="col-sm-2">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">آغاز جلسه</label>
                    </div>
                    <div class="col-sm-12">
                        <input class="form-control" type="text" id="timepicker2" name="meeting_start" placeholder="آغاز جلسه" value="{!!old('meeting_start')!!}" required />
                    </div>
                </div>
	            <div class="col-sm-2">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">ختم جلسه</label>
                    </div>
                    <div class="col-sm-12">
                        <input class="form-control" type="text" id="timepicker1" name="meeting_end" placeholder="ختم جلسه" value="{!!old('meeting_end')!!}" required>
                    </div>
                </div>
	            <div class="col-sm-5">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">موضوع و اشتراک کننده گان</label>
                    </div>
                    <div class="col-sm-12">
                        <textarea class="form-control" name="meeting_subject" id="meeting_subject" placeholder="موضوع و اشتراک کننده گان">{!!old('meeting_subject')!!}</textarea>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">تاریخ جلسه</label>
                    </div>
                    <div class="col-sm-12">
                        <input class="datepicker_farsi form-control" type="text" name="date" id="current_date" value="{!!old('date')!!}" readonly="readonly">
                    </div>
                </div>
	        </div>
	        
	        <div class="form-group">
	            
	            <div class="col-sm-4">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">محل</label>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_fields_wrap_location" style="margin-top:10px">
                            <select name="location" class="form-control" id="loc_list" style="width:87%;display:inline-block">
                                <option value=""> از لست انتخاب کنید </option>
                                @foreach($locations AS $item)
                                    @if($item->name == old('location'))
                                        <option value='{!!$item->id!!}' selected="selected">{!!$item->name!!}</option>  
                                    @else
                                        <option value='{!!$item->id!!}'>{!!$item->name!!}</option>
                                    @endif
                                @endforeach
                            </select>
                            <a class="add_field_button_location btn" id="add_location" style="background: green;color:white;" title="add"> + </a>
                        </div> 
                    </div>
                </div>
	            <div class="col-sm-4">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">سکتور</label>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_fields_wrap_sector" style="margin-top:10px">
                            <select name="sector" class="form-control" id="sector_list" style="width:87%;display:inline-block">
                                <option value="">از لست انتخاب کنید</option>
                                @foreach($sectors AS $item)
                                    @if($item->name == old('sector'))
                                        <option value='{!!$item->id!!}' selected="selected">{!!$item->name!!}</option>  
                                    @else
                                        <option value='{!!$item->id!!}'>{!!$item->name!!}</option>
                                    @endif
                                @endforeach
                            </select>
                            <a class="add_field_button_sector btn" id="add_sector" style="background: green;color:white;" title="add"> + </a>
                        </div> 
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">نوع جلسه</label>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_fields_wrap_meeting_type" style="margin-top:10px">
                            <select name="meeting_type" class="form-control" id="meeting_type_list" style="width:87%;display:inline-block">
                                <option value="">از لست انتخاب کنید</option>
                                @foreach($meeting_type AS $item)
                                    @if($item->name == old('meeting_type'))
                                        <option value='{!!$item->id!!}' selected="selected">{!!$item->name!!}</option>  
                                    @else
                                        <option value='{!!$item->id!!}'>{!!$item->name!!}</option>
                                    @endif
                                @endforeach
                            </select>
                            <a class="add_field_button_meeting_type btn" id="add_meeting_type" style="background: green;color:white;" title="add"> + </a>
                        </div>
                    </div>
                </div>
	      	</div>
	      	<div class="form-group">

                <div class="col-sm-5">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">ضمایم</label>
                    </div>
                    <div class="col-sm-12">
                        <div class="input_fields_wrap">
                            <input type='file' id='files' style="width:87%;display:inline-block" name='files[]' class="form-control" multiple='multiple'>
                            <a class="add_field_button btn" id="add" style="background: green;color:white;" title="{!!_('add_another_file') !!}"> + </a>
                        </div>
                    </div>
                </div>
	            <div class="col-sm-2">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">مطبوعات داخل ارگ</label>
                    </div>
                    <div class="col-sm-12">
                        <input type="checkbox" class="form-control" name="arg_media" />
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="col-sm-12">
                        <label class="col-sm-12">به تعویق انداختن جلسه</label>
                    </div>
                    <div class="col-sm-12">
                    <input type="checkbox" class="form-control" name="delay" />
                    </div>
                </div>

	            <div class="col-sm-2">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">تکرار جلسه</label>
                    </div>
                    <div class="col-sm-12">
                    <input type="checkbox" class="form-control" name="recure" id="check" />
                    </div>
                </div>
            </div>
            <div class="form-group">

                <div style="display:none;margin-top: 15px" id="repeat" class="col-sm-12">
                    <div class="col-sm-3" style="float: right">
                        <input type="radio" name="recure_type" id="day" value="1"> تکرار روزانه <br />
                        <input type="radio" name="recure_type" id="week" value="2"> تکرار هفته وار <br />
                        <input type="radio" name="recure_type" id="week_num" value="3"> تکرار بعد از هر
                    </div>
                </div>
                <div style="display: none;margin-top: 15px;float: right" id="daily_recure_end_date" class="col-sm-3">
                    <input type="text" name="daily_recure_end_date" id="daily_end_date" placeholder="تاریخ ختم تکرار" class="datepicker_farsi form-control" readonly="readonly" />
                </div>
                <div style="display:none;margin-top: 15px" id="day_selection" class="col-sm-12">
                    
                    <div class="col-sm-6" style="float:right">
                        <select name="weekly_recure_day[]" class="form-control" id="weekly_recure_day" style="width:100%" multiple="multiple">

                            @foreach($week_days AS $item)
                            
                                <option value='{!!$item->g_weekday_num!!}'>{!!$item->name!!}</option>
                                
                            @endforeach

                        </select>
                    </div>
                    <div class="col-sm-3" style="float:right">
                        <input type="text" name="weekly_recure_end_date" id="weekly_end_date" placeholder="تاریخ ختم تکرار" class="datepicker_farsi form-control" readonly="readonly" />
                    </div>
                </div>

                <div style="display: none;margin-top: 15px" id="after_weeks" class="col-sm-12">
                    <div class="col-sm-2" style="float:right">
                        <input type="text" name="after_weeks_recure" id="after_weeks_input" placeholder="تعداد هفته" class="form-control" />
                    </div>
                    <div class="col-sm-3" style="float:right"> 
                        هفته تکرار شود الی تاریخ : 
                    </div>
                    <div class="col-sm-3" style="float:right;margin-right: -150px">
                        <input type="text" name="after_weeks_end_date" id="after_weeks_end_date" placeholder="تاریخ ختم" class="datepicker_farsi form-control" readonly="readonly" />
                    </div>
                </div>

	        </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <hr style="border: 1px dashed #b6b6b6" />
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-check"></i>
                        </span>
                        &nbsp; ثبت کردن
                    </button>
                    <a href="{!!URL::route('getMeetings')!!}" class="btn btn-danger">
                        <span>
                            <i class="fa fa-remove"></i>
                        </span>
                        &nbsp; رد کردن
                    </a>
                </div>
            </div>
        </form>
        <!-- ======================================= Notification Modal ====================================== -->
        <!-- <div class="md-modal md-effect-11" id="modal-11">
            <div class="md-content">
                <div class="modal-header">
                    <button class="md-close close">&times;</button>
                    <h4 class="modal-title">روز خاص سال</h4>
                </div>
                <div class="modal-body">
                    <div id="check_result"></div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div> -->

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" align="center">روز خاص سال</h4>
                    </div>
                    <div class="modal-body">
                        <div id="check_result"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <!-- ======================================= End of Notification Modal ====================================== -->
        <!-- <button id="modal_btn" class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-11">Super modal</button> -->
        <a data-toggle="modal" href="#myModal" class="btn btn-primary mrg-b-lg" id="modal_btn" style="display:none"></a>
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $("#weekly_recure_day").select2();

    $('#timepicker1').timepicker({
        showPeriod: true,
        onHourShow: OnHourShowCallback,
        onMinuteShow: OnMinuteShowCallback
    });
    function OnHourShowCallback(hour) {
        if ((hour > 20) || (hour < 6)) {
            return false; // not valid
        }
        return true; // valid
    }
    function OnMinuteShowCallback(hour, minute) {
        if ((hour == 20) && (minute >= 30)) { return false; } // not valid
        if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        return true;  // valid
    }

    $('#timepicker2').timepicker({
        showPeriod: true,
        onHourShow: OnHourShowCallback,
        onMinuteShow: OnMinuteShowCallback
    });
    function OnHourShowCallback(hour) {
        if ((hour > 20) || (hour < 6)) {
            return false; // not valid
        }
        return true; // valid
    }
    function OnMinuteShowCallback(hour, minute) {
        if ((hour == 20) && (minute >= 30)) { return false; } // not valid
        if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        return true;  // valid
    }

    //send the current date to the controller and covnvert it to jalali date and replace the value.
    $.ajax({

        type: 'post',
        url: '{!!URL::route("convertToJalali")!!}',
        data: 'date='+"{!!date('Y-m-d')!!}",
        success: function(response){
            $("#current_date").val(response);
        
        }
    });

    // now send the replaced date to distinguish the day of the week in jalali.
    $.ajax({

        type: 'post',
        url: '{!!URL::route("getJalaliWeekDay")!!}',
        data: 'date='+"{!!date('Y-m-d')!!}",
        success: function(response){
            //alert(response);
            $("#week_day").val(response);
        
        }
    });

    // onchange of the date the weekday is being changed accordingly.
    $("#current_date").change(function(){
        var value = $("#current_date").val();

        // now send the replaced date to distinguish the day of the week in jalali.
        $.ajax({

            type: 'post',
            url: '{!!URL::route("getCurrentWeekDay")!!}',
            data: 'date='+value,
            success: function(response){
                //alert(response);
                $("#week_day").val(response);
            
            }
        });

        // send the value through ajax script to validate if the particular date is not included in special days.
        $.ajax({

            type: 'post',
            url: '{!!URL::route("checkSpecialDays")!!}',
            data: 'date='+value,
            success: function(response){
                //alert(response);
                if(response != '')
                {
                    $('#modal_btn').click();
                    $("#check_result").html(response);
                }
            }
        });

    }); 

    var today_date = $("#today_date").val();
    // send the value on page load through ajax script to validate if the particular date is not included in special days.
    $.ajax({

        type: 'post',
        url: '{!!URL::route("checkSpecialDays")!!}',
        data: 'date='+today_date,
        success: function(response){
            //alert(response);
            if(response != '')
            {
                $('#modal_btn').click();
                $("#check_result").html(response);
            }
        
        }
    });

    $(function(){ 
  
        // repeat the attachment field ===================================
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
         
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;margin-left:2px" title="remove"> X </a></div>'); //add input box
            }
        });
  
        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            $('#add').fadeIn("slow");
            $('#appendedInputButton').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x--;
        });
  
    });

    $(function(){     

		// location dropdown and manual field
        // repeat the input fields ===================================
        var max_location_fields      = 2; //maximum input boxes allowed
        var wrapper_location        = $(".input_fields_wrap_location"); //Fields wrapper
        var add_button_location      = $(".add_field_button_location"); //Add button ID
       
        var x1 = 1; //initlal text box count
        $(add_button_location).click(function(e){ //on add input button click
            e.preventDefault();
            if(x1 < max_location_fields){ //max input box allowed
                x1++; //text box increment
                $(wrapper_location).append('<div class="input-append"><input type="text" style="width:87%;display:inline-block" id="appendedInputButtonLocation" class="form-control" name="manual_loc" placeholder="محل جلسه" value="{!!old("manual_loc")!!}" /><a class="remove_field btn" id="remove_location" style="background: red;color:white;margin-left:2px" title="remove"> X </a></div>'); //add input box
            }
        });
       
        $(wrapper_location).on("click","#remove_location", function(e){ //user click on remove text
            $('#add_location').fadeIn("slow");
            $('#loc_list').fadeIn("slow");
            $("#remove_location").parent('div').remove(); x1--;
        });

        $('#add_location').click(function(){
            $('#add_location').hide();
            $('#loc_list').hide();
            $('#loc_list').val('');
        });
        
    	// Sector dropdown and manual field
        // repeat the input fields ===================================
        var max_fields_sector      = 2; //maximum input boxes allowed
        var wrapper_sector         = $(".input_fields_wrap_sector"); //Fields wrapper
        var add_button_sector      = $(".add_field_button_sector"); //Add button ID
       
        var x2 = 1; //initlal text box count
        $(add_button_sector).click(function(e){ //on add input button click
            e.preventDefault();
            if(x2 < max_fields_sector){ //max input box allowed
                x2++; //text box increment
                $(wrapper_sector).append('<div class="input-append"><input type="text" style="width:87%;display:inline-block" id="appendedInputButtonSector" class="form-control" name="manual_sector" placeholder="سکتور را بنویسید" value="{!!old("manual_sector")!!}" /><a class="remove_field_sector btn" id="remove_sector" style="background: red;color:white;margin-left:2px" title="remove"> X </a></div>'); //add input box
            }
        });
       
        $(wrapper_sector).on("click","#remove_sector", function(e){ //user click on remove text
            $('#add_sector').fadeIn("slow");
            $('#sector_list').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x2--;
        });
        $('#add_sector').click(function(){
            $('#add_sector').hide();
            $('#sector_list').hide();
            $('#sector_list').val('');
        });
        
        // Meeting Type dropdown and manual field
        // repeat the input fields ===================================
        var max_fields_meeting_type      = 2; //maximum input boxes allowed
        var wrapper_meeting_type         = $(".input_fields_wrap_meeting_type"); //Fields wrapper
        var add_button_meeting_type      = $(".add_field_button_meeting_type"); //Add button ID
       
        var x3 = 1; //initlal text box count
        $(add_button_meeting_type).click(function(e){ //on add input button click
            e.preventDefault();
            if(x3 < max_fields_meeting_type){ //max input box allowed
                x3++; //text box increment
                $(wrapper_meeting_type).append('<div class="input-append"><input type="text" style="width:87%;display:inline-block" id="appendedInputButtonMeeting_type" class="form-control" name="manual_meeting_type" placeholder="نوع جلسه" value="{!!old("manual_meeting_type")!!}" /><a class="remove_field_meeting_type btn" id="remove_meeting_type" style="background: red;color:white;margin-left:2px" title="remove"> X </a></div>'); //add input box
            }
        });
       
        $(wrapper_meeting_type).on("click","#remove_meeting_type", function(e){ //user click on remove text
            $('#add_meeting_type').fadeIn("slow");
            $('#meeting_type_list').fadeIn("slow");
            $(this).parent('div').remove(); x3--;
        });
        $('#add_meeting_type').click(function(){
            $('#add_meeting_type').hide();
            $('#meeting_type_list').hide();
            $('#meeting_type_list').val('');
        });
    });

    $(function(){

        $('#check').change(function(){
            if (this.checked) {
                $('#repeat').fadeIn('slow');
            }
            else
            {
                $('#week').prop('checked', false);
                $('#day').prop('checked', false);
                $('#week_num').prop('checked', false);
                $('#repeat').hide();
                $("#day_selection").hide("slow");
                $("#week_day").fadeIn("slow");
                $("#current_date").prop('disabled', false);
                $("#daily_end_date").val("");
                $('#weekly_end_date').val("");
                $('#weekly_recure_day').val("");
                $('#after_weeks_input').val("");
                $('#after_weeks_end_date').val("");
                $("#daily_recure_end_date").hide();
                $('#weekly_end_date').prop("required", false);
                $('#weekly_recure_day').prop("required", false);
                $('#daily_end_date').prop("required", false);
                $('#after_weeks_input').prop('required',false);
                $('#after_weeks_end_date').prop('required',false);
            }
            $('input[id=day]').change(function(){

                $('#daily_recure_end_date').fadeIn('slow');
                $("#weekly_recure_day").val("");
                $("#day_selection").hide();
                $("#after_weeks_input").val("");
                $('#after_weeks_end_date').val("");
                $("#after_weeks").hide();
                $("#current_date").removeClass("datepicker_farsi");
                if (this.checked) {
                    $('#daily_end_date').prop('required',true);
                    $('#weekly_end_date').prop("required", false);
                    $('#weekly_recure_day').prop("required", false);
                    $('#after_weeks_input').prop('required',false);
                    $('#after_weeks_end_date').prop('required',false);
                }

            });

            $('input[id=week]').change(function(){
            
                $("#day_selection").fadeIn("slow");
                $("#current_date").removeClass("datepicker_farsi");
                $('#daily_end_date').val("");
                $('#daily_recure_end_date').hide();
                $('#after_weeks_input').val("");
                $('#after_weeks_end_date').val("");
                $('#after_weeks').hide();
                if (this.checked) {
                    $('#weekly_end_date').prop('required',true);
                    $('#weekly_recure_day').prop('required',true);
                    $('#daily_end_date').prop("required", false);
                    $('#after_weeks_input').prop('required',false);
                    $('#after_weeks_end_date').prop('required',false);

                }

            });

            $('input[id=week_num]').change(function(){
            
                $("#after_weeks").fadeIn("slow");
                $('#daily_end_date').val("");
                $('#daily_recure_end_date').hide();
                $('#weekly_recure_day').val("");
                $('#day_selection').hide();
                if (this.checked) {
                    $('#after_weeks_input').prop('required',true);
                    $('#after_weeks_end_date').prop('required',true);
                    $('#daily_end_date').prop("required", false);
                    $('#weekly_end_date').prop("required", false);
                    $('#weekly_recure_day').prop("required", false);
                }

            });


        });

        

    });


</script>

@stop