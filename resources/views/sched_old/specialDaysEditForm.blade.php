@extends('layouts.master')

@section('head')
	@parent
	<title>Edit Special Days</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
        .form-control{
            width: 60% !important;
        }
    </style>
@stop

@section('content')

<div class="row" style="opacity: 1;">
    <div class="col-lg-12 noprint">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!!URL::route('getMeetings')!!}">Daily Meetings</a>
                    </li>
                    <li class="active">
                        <span>Special Days manipulation</span>
                    </li>
                </ol>
                <h1>Edit this special day</h1>
            </div>
        </div>
    </div>
    <div class="container" dir="rtl">
        <h4 class="page-header" align="center">اصلاح کردن فارم روز های خاص سال</h4>

        @foreach($record AS $data)

            <form role="form" method="post" action="{!! URL::route('UpdateSpecialDay', $data->id) !!}" class="form-horizontal">

                <table class="table">
                    <tr>
                        <td>عنوان :</td>
                        <td>
                            <input type="text" class="form-control" name="title" value="{!!$data->title!!}" />              
                        </td>
                    </tr>
                    <tr>
                        <td class="hide_border"></td>
                        @if($errors->has("title"))
                        <td class="hide_border"> 
                            <span style="color: red" dir="ltr">{!! $errors->first('title') !!}</span>
                        </td>
                        @endif
                    </tr>
                    <tr>
                        <td>شرح بیشتر :</td>
                        <td>
                            <textarea cols="20" rows="3" value="{!!$data->description!!}" class="form-control" name="description">{!!$data->description!!}</textarea>                    
                        </td>
                    </tr>
                    <tr>
                        @if($errors->has("description"))
                        <td class="hide_border"></td>
                        <td class="hide_border">
                            <span style="color: red" dir="ltr">{!! $errors->first('description') !!}</span>
                        </td>
                        @endif
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            @if($data->type == '1')
                            <input type="radio" name="type" checked="checked" value="1"> ملی &nbsp;&nbsp;
                            @else
                            <input type="radio" name="type" value="1"> ملی &nbsp;&nbsp;
                            @endif
                            @if($data->type == '2')
                            <input type="radio" checked="checked" name="type" value="2"> بین المللی  
                            @else
                            <input type="radio" name="type" value="2"> بین المللی
                            @endif
                        </td>
                    </tr>
                    <tr>
                        @if($errors->has("type"))
                        <td class="hide_border"></td><td class="hide_border">
                            <span style="color: red" dir="ltr">{!! $errors->first('type') !!}</span>
                        </td>
                        @endif
                    </tr>
                    <tr>
                        <td>تاریخ :</td>
                        <td>
                            <?php $date = jalali_format($data->date);?>
                            <input class="datepicker_farsi form-control" type="text" name="date" value="{!!$date!!}" readonly="readonly">                 
                        </td>
                    </tr>
                    <tr>
                        @if($errors->has("date"))
                        <td class="hide_border"></td>
                        <td class="hide_border">
                            <span style="color: red" dir="ltr">{!! $errors->first('date') !!}</span>
                        </td>
                        @endif
                    </tr>

                </table>

                <div class="form-group">

                    <div class="col-sm-12">
                        <hr style="border: 1px dashed #b6b6b6" />
                        <button class="btn btn-primary" type="submit">
                            <span>
                                <i class="fa fa-check"></i>
                            </span>
                            &nbsp;ذخیره تغییرات
                        </button>
                    </div>

                </div>
            </form>

        @endforeach
        
    </div>

</div>


<script type="text/javascript">

</script>


@stop