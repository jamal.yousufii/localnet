<?php use App\library\jdatetime; ?>
<style>
	
	table { direction: rtl; }

	table#list_of_meetings th
    {
        text-align: center !important;
        border-color: #000; 
        font-weight: bold;
        color: #000;
        border: 1px solid;
    }
    table#list_of_meetings tr td
    {
        border-color: #000;
        border: 1px solid;
        color: #000;
        font-size: 15px !important;
    }
	table#meeting_types th
    {
        text-align: center !important;
        border-color: #000; 
        color: #000;
        font-weight: bold;
        border: 1px solid;
    }
    table#meeting_types tr td
    {
        border-color: #000;
        color: #000;
        border: 1px solid;
        font-size: 15px !important;
    }

	.head
	{
		font-weight: bold;
		font-size: 13px !important;
		text-align: center;
	}

</style>

<h3 style="text-align:center;color: orange;font-weight: bold;margin: 20px 0 -50px 0" class="noprint">نتیجه جستجو</h3>
<a href="#" onclick="window.print()" class="btn btn-success noprint" style="margin-bottom:10px">چاپ کردن راپور</a> 
<table class="table" id="list_of_meetings">
	<th>شماره</th>
	<th>آغاز جلسه</th>
	<th>ختم جلسه</th>
	<th>مدت</th>
	<th>تاریخ</th>
	<th>نوع جلسه</th>
	<th>سکتور</th>
	<th>موضوع و اشتراک کننده گان</th>

	@if(!empty($records))
		<?php 
			$total_time_spent = ""; $auto = 1; 
			$one_by_one_total = 0;
			$group_meeting_total = 0;
			$meetings_total = 0;
			$speeches_total = 0;
			$programs_total = 0;
			$travels_total = 0;
		?>
		@foreach($records AS $items)
		<?php 
			$start = date("g:i A", strtotime($items->meeting_start));
		    $end = date("g:i A", strtotime($items->meeting_end));
		    $s_time = jdatetime::date($start,true,true,'Asia/Kabul');
		    $e_time = jdatetime::date($end,true,true,'Asia/Kabul');
		    $start_time = strtotime($start);
			$end_time = strtotime($end);
		?>
		<tr>
			<td width="1%" align="center">{!!$auto!!}</td>
			<td width="8%">{!!$s_time!!}</td>
            <td width="8%">{!!$e_time!!}</td>
            <td width="5%">{!!round(abs($start_time - $end_time) / 60,2);!!} دقیقه</td>
            <td width="6%">{!!convertToJalali($items->date)!!}</td>
            <td width="10%">{!!$items->meeting_type_name!!}</td>
            <td width="10%">{!!$items->sector_name!!}</td>
            <td width="30%">{!!$items->meeting_subject!!}</td>
            <?php $total_time_spent += round(abs($start_time - $end_time) / 60,2) ?>
		</tr>
		<?php 
			$auto++; 
			if($items->meeting_type == 1)
			$one_by_one_total++;
			if($items->meeting_type == 2)
			$group_meeting_total++;
			if($items->meeting_type == 3)
			$meetings_total++;
			if($items->meeting_type == 4)
			$speeches_total++;
			if($items->meeting_type == 5)
			$programs_total++;
			if($items->meeting_type == 6)
			$travels_total++;
		?>
		@endforeach
	<tr>
		<td colspan="10" style="color:green;font-weight:bold" align="center"> مجموع زمان سپری شده : {!!floor($total_time_spent/ 60)!!}  ساعت {!!($total_time_spent % 60)!!} دقیقه </td>
	</tr>
</table>
<table class="table" id="meeting_types">
	<tr>
		<th colspan="10" style="font-size:16px;">مجموع جلسات به اساس سکتور آن</th>
	</tr>
	<tr class="head">
		<td>مجموع جلسات یک به یک</td>
		<td>مجموع جلسات گروپی</td>
		<td>مجموع جلسات</td>
		<td>مجموع مکالمات</td>
		<td>مجموع برنامه ها</td>
		<td>مجموع سفر ها</td>
	</tr>
	<tr class="head">
		<td>{!! $one_by_one_total; !!}</td>
		<td>{!! $group_meeting_total; !!}</td>
		<td>{!! $meetings_total; !!}</td>
		<td>{!! $speeches_total; !!}</td>
		<td>{!! $programs_total; !!}</td>
		<td>{!! $travels_total; !!}</td>
	</tr>
	@else
	<tr>
		<td colspan="10" style="color:red" align="center">موردی پیدا نشد</td>
	</tr>
	@endif

</table>