@extends('layouts.master')

@section('head')
	<title>Meeting form</title>

    <style type="text/css">
        table thead tr th
        {
            text-align: center;
            border-color: #000; 
        }
        table tbody tr td
        {
            border-color: #000;
        }
        .fixed{width: 11% !important;}

         /*a[title]:hover:after {
            content: attr(title);
            display:block;
            position:absolute;
            top:-2em;
            left:0px;
            padding: 0.5em 1em;
            border:1px solid #996633;
            background-color:#FFFF66;
            color:#000;
            z-index:99;
            text-decoration: none;
            width:500px;
          }*/

        /*table.fixed { table-layout:inherit !important; }
        table.fixed td { overflow: hidden; width: 100px !important; }*/
    </style>
    {{ HTML::style('/css/font.css') }}
    {{ HTML::style('/css/schedule_db/jquery.define.css') }}
    
@stop

@section('content')

<div class="row" style="opacity: 1;">
    <div class="col-lg-12 noprint">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ URL::route('home') }}">Dashboard</a>
                    </li>
                    <li class="active">
                        <span>Weekly Meetings</span>
                    </li>
                </ol>
                <h1>Current, previous and next week's Meetings</h1>
            </div>
        </div>
    </div>

    <?php $last_updated_record = $last_update->updated_at; ?>
    <div class="last_update_record" style="display:none"><?= $last_updated_record; ?> &nbsp&nbsp: اصلاح شده</div>
    <!-- <button class="btn btn-info noprint" onclick="prev_next_days('today')">امروز</button> -->
    <div style="text-align:center;" class="noprint">
        <button class="btn btn-info" onclick="prev_next_weeks('next')"><i class="fa fa-angle-double-left"></i> بعدی </button>  <button class="btn btn-info" onclick="prev_next_weeks('prev')"> قبلی <i class="fa fa-angle-double-right"></i></button>
    </div>
    <?php 

        $g_start_date = gregorian_format($start_date);
        $g_end_date = gregorian_format($end_date); 
        // now convert it to gregorian to get previous and next months through ajax.
        echo $g_start_date = convertToGregorian($g_start_date);
        echo $g_end_date = convertToGregorian($g_end_date);

    ?>
    <input type="hidden" id="start_date" value="{{$g_start_date}}" />
    <input type="hidden" id="end_date" value="{{$g_end_date}}" />

    <!-- <span style="display: none;margin:0 auto;" class="title"><i id="date_duration_print"></i></span> -->
    
    <div id="weekly_meetings">

        <!-- get one week start and end date both in jalali and gregorian using ajax. -->
        <i id="date_duration"></i>
        
        <table class="table table-bordered" dir="rtl" id="list_of_weeklyMeetings">
            <thead>
              <tr>
                <!-- <th>شماره</th> -->
                <!-- <th>تاریخ</th> -->
                <!-- <th>آغاز جلسه</th>
                <th>ختم جلسه</th> -->
                <th>شنبه</th>
                <th>یکشنبه</th>
                <th>دوشنبه</th>
                <th>سه شنبه</th>
                <th>چهارشنبه</th>
                <th>پنجشنبه</th>
                <th>جمعه</th>
                <!-- <th colspan="2" class="noprint">عملیات</th> -->
                
              </tr>
            </thead>
            <tbody>
            <!-- change the format of the start and end date sent from controller -->
            <?php $meeting_start = gregorian_format($start_date); ?>
            <?php $meeting_end = gregorian_format($end_date); ?>
                @if(day_meetings(1,$meeting_start,$meeting_end,true))
                    <td colspan="7"><div class="alert alert-danger" align="center">جلسات برای این هفته در دیتابیس اضافه نگردیده است !</div></td>
                @else
                    <td class="fixed">{{day_meetings(1,$meeting_start,$meeting_end)}}</td>
                    <td class="fixed">{{day_meetings(2,$meeting_start,$meeting_end)}}</td>
                    <td class="fixed">{{day_meetings(3,$meeting_start,$meeting_end)}}</td>
                    <td class="fixed">{{day_meetings(4,$meeting_start,$meeting_end)}}</td>
                    <td class="fixed">{{day_meetings(5,$meeting_start,$meeting_end)}}</td>
                    <td class="fixed">{{day_meetings(6,$meeting_start,$meeting_end)}}</td>
                    <td class="fixed">{{day_meetings(7,$meeting_start,$meeting_end)}}</td>
                @endif
        <button class="btn btn-default" title="" data-original-title="" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." data-placement="top" 
        data-toggle="popover" data-container="body" type="button" aria-describedby="popover426001"> Popover on top </button>
            </tbody>
        </table>
       
    </div>
    
    <a href="{{URL::route('MeetingForm')}}" class="btn btn-primary noprint">اضافه کردن جلسه جدید</a>
    <a href="#" class="btn btn-success noprint" onclick="window.print()">چاپ جلسات</a> 

</div>

@stop

@section('footer-scripts')
{{ HTML::script('/js/template/jfullcalendar.min.js') }}
{{ HTML::script('/js/schedule_db/jquery.define.js') }}

<script type="text/javascript">
    
    // send the weekHint and bring the meetings based on the dayHint.    
    function prev_next_weeks(weekHint)
    {
        var s_date = $("#start_date").val();
        var e_date = $("#end_date").val();
        
        $.ajax({

            type: 'post',
            url: '{{URL::route("weeklyMeetingsPrevNext")}}',
            data: 'week='+weekHint+"&s_date="+s_date+"&e_date="+e_date,
            success: function(response){
                $("#weekly_meetings").html(response);
                $("#start_date").val($("#s_date").val());
                $("#end_date").val($("#e_date").val());
            }
        });


    }

    var start_date = "<?=$start_date;?>";
    var end_date = "<?=$end_date;?>";

    // now send the replaced date to distinguish the day of the week in jalali.
    $.ajax({

        type: 'post',
        url: '{{URL::route("getMonthDifference")}}',
        data: {start_date: start_date, end_date: end_date},
        success: function(response){
            $("#date_duration").html(response);
            $("#date_duration_print").html(response);
        
        }
    });
    
    $('#timepicker').timepicker({
        showPeriod: true,
        onHourShow: OnHourShowCallback,
        onMinuteShow: OnMinuteShowCallback
    });
    function OnHourShowCallback(hour) {
        if ((hour > 20) || (hour < 6)) {
            return false; // not valid
        }
        return true; // valid
    }
    function OnMinuteShowCallback(hour, minute) {
        if ((hour == 20) && (minute >= 30)) { return false; } // not valid
        if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        return true;  // valid
    }

    $('#timepicker2').timepicker({
        showPeriod: true,
        onHourShow: OnHourShowCallback,
        onMinuteShow: OnMinuteShowCallback
    });
    function OnHourShowCallback(hour) {
        if ((hour > 20) || (hour < 6)) {
            return false; // not valid
        }
        return true; // valid
    }
    function OnMinuteShowCallback(hour, minute) {
        if ((hour == 20) && (minute >= 30)) { return false; } // not valid
        if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        return true;  // valid
    }

    $(function(){

        $('[data-toggle="tooltip"]').tooltip();
        $('.btn').popover();

    //     jQuery.balloon.init();
    //     addclose     : false,
    //     addoverlay   : false,
    //     target       : "self",
    //     highlight    : true,
    //     justonce     : false,
    //     ease         : [0, .96, 0, 1.02],
    //     animTime     : 250,
    //     bgcolor      : "#333333",
    //     bordercolor  : "#ffffff",
    //     textcolor    : "#ffffff",
    //     oncursor     : false,
    //     forceposition: "auto", // or: up, down, left, right
    //     timer        : 0, // close the balloon after x millis (0 = never)
    //     balloon      : "This is an mb.balloon"

    });

</script>

@stop