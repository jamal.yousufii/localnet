
    {{--Bring the list table--}}
        @include('sched.meeting_log.table')
    {{--list table end--}}
<div class="dataTables_paginate paging_simple_numbers noprint" id="list_paginate">
	@if(!empty($records))
	{!!$records->render()!!}
	@endif
</div>
<script type="text/javascript">

$(document).ready(function() {
	
	$('.pagination a').on('click', function(event) {
		event.preventDefault();
		if ($(this).attr('href') != '#') {
			var dataString = "&page="+$(this).text()+"&ajax="+1;
			$.ajax({
	                url: '{!!URL::route("getMeetingsLog")!!}',
	                data: dataString,
	                type: 'get',
	                beforeSend: function(){
	                    //$("body").show().css({"opacity": "0.5"});
	                    $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
	                    $('#search_result').html(response);
	                }
	            }
	        );
		
		}
	});

});

</script>