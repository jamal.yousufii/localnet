@extends('layouts.master')

@section('head')
	<title>جلسات هفته وار</title>

    <style type="text/css">
        table { direction: rtl; }

        table#list_of_weeklyMeetings th
        {
            text-align: center !important;
            border-color: #000; 
            font-weight: bold;
            color: #000;
            border: 1px solid;
        }
        table#list_of_weeklyMeetings tr td
        {
            border-color: #000;
            border: 1px solid;
            color: #000;
            font-size: 15px !important;
        }
        .fixed{width: 11% !important;vertical-align: top !important;}
        .meeting_item
        {
            border-top:1px dashed  #aaa;
            overflow: hidden;
            height: 50px;
        }

        body.full_titles .meeting_item
        {
          height: auto !important;
          overflow: inherit !important;
          margin-bottom: 10px;
          margin-top: 10px;
        }

        .meeting_item:first-child {border:none;}
    </style>
    {!! HTML::style('/css/font.css') !!}
    {!! HTML::style('/css/print.css', array('media' => 'print')) !!}
    
@stop

@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success span6 noprint'>{!! Session::get('success') !!}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger span6 noprint'>{!! Session::get('fail') !!}</div>
    @endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12 noprint">
        <div id="content-header" class="clearfix">
            <div class="col-sm-5">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('home') !!}">صفحه اصلی</a>
                    </li>
                    <li class="active">
                        <span>جلسات هفته وار</span>
                    </li>
                </ol>
            </div>
            <div class="col-sm-6" style="margin-left: -50px">
            	<h3>لست جلسات هفته وار</h3>
            </div>
        </div>
    </div>

    <!-- <button class="btn btn-info noprint" onclick="prev_next_days('today')">امروز</button> -->
    <div style="text-align:center;" class="noprint">
        <button class="btn btn-info" onclick="prev_next_weeks('next')"><i class="fa fa-angle-double-left"></i> بعدی </button>  <button class="btn btn-info" onclick="prev_next_weeks('prev')"> قبلی <i class="fa fa-angle-double-right"></i></button>
        <span class="noprint" style="margin-left: 20px"><input type="checkbox" name="print" id="full" /> نمایش مکمل جزئیات جلسه </span>
    </div>
    <?php 
        use App\library\jdatetime;
        // start_date is in this format of jalali date e.g : 14-11-1396 .
        $g_start_date = gregorian_format($start_date);
        $g_end_date = gregorian_format($end_date); 
        // now convert it to gregorian to get previous and next months through ajax.
        $g_start_date = convertToGregorian($g_start_date);
        $g_end_date = convertToGregorian($g_end_date);

        // change the format of the start date to be shown with the week days
        $day_date = jdatetime::date(gregorian_format($start_date),true,true);
        $day_date = shamsi_date_format($day_date);

    ?>
    <input type="hidden" id="start_date" value="{!!$g_start_date!!}" />
    <input type="hidden" id="end_date" value="{!!$g_end_date!!}" />

    <!-- <span style="display: none;margin:0 auto;" class="title"><i id="date_duration_print"></i></span> -->
    

    <!-- get one week start and end date both in jalali and gregorian using ajax. -->
    <div id="weekly_meetings">
        <i id="date_duration"></i>
        <div id="tableContent">
        @if(count(array_filter($weeklyMeetings)) != '')
        <table class="table" id="list_of_weeklyMeetings">
            <thead>
              <tr>
                <!-- <th>شماره</th> -->
                <!-- <th>تاریخ</th> -->
                <!-- <th>آغاز جلسه</th>
                <th>ختم جلسه</th> -->
                <th>شنبه<br /><span dir="rtl">{!!addDays(gregorian_format($start_date),'0')!!}</span> <br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'0')!!}</span></th>
                <th>یکشنبه<br><span dir="rtl">{!!addDays(gregorian_format($start_date),'1')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'1')!!}</span></th>
                <th>دوشنبه<br><span dir="rtl">{!!addDays(gregorian_format($start_date),'2')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'2')!!}</span></th>
                <th>سه شنبه<br><span dir="rtl">{!!addDays(gregorian_format($start_date),'3')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'3')!!}</span></th>
                <th>چهارشنبه<br><span dir="rtl">{!!addDays(gregorian_format($start_date),'4')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'4')!!}</span></th>
                <th>پنجشنبه<br><span dir="rtl">{!!addDays(gregorian_format($start_date),'5')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'5')!!}</span></th>
                <th>جمعه<br><span dir="rtl">{!!addDays(gregorian_format($start_date),'6')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'6')!!}</span></th>
                <!-- <th colspan="2" class="noprint">عملیات</th> -->
                
              </tr>
            </thead>
            <tbody>
                <tr>
                @foreach ($weeklyMeetings as $meeting) 
                    <td class="fixed">
                    @foreach ($meeting as $row) 
                        <div class="meeting_item">{!!$row!!}</div>
                    @endforeach
                    </td>
                @endforeach                
                </tr>
        
            </tbody>
        </table>
        @else
                
        <table class="table" id="list_of_weeklyMeetings">
            <thead>
              <tr>
                <!-- <th>شماره</th> -->
                <!-- <th>تاریخ</th>
                <th>آغاز جلسه</th>
                <th>ختم جلسه</th> -->
                <th>شنبه<br><span dir="rtl">{!!$j_day_date!!}</span><br /><span dir="ltr">{!!$g_day_date!!}</span></th>
                <th>یکشنبه<br><span dir="rtl">{!!addDays(gregorian_format($start_date),'1')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'1')!!}</span></th>
                <th>دوشنبه<br><span dir="rtl">{!!addDays(gregorian_format($start_date),'2')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'2')!!}</span></th>
                <th>سه شنبه<br><span dir="rtl">{!!addDays(gregorian_format($start_date),'3')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'3')!!}</span></th>
                <th>چهارشنبه<br><span dir="rtl">{!!addDays(gregorian_format($start_date),'4')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'4')!!}</span></th>
                <th>پنجشنبه<br><span dir="rtl">{!!addDays(gregorian_format($start_date),'5')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'5')!!}</span></th>
                <th>جمعه<br><span dir="rtl">{!!addDays(gregorian_format($start_date),'6')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($start_date),'6')!!}</span></th>
                
              </tr>
            </thead>
            <tbody>
                <tr><td colspan='7'><div class='alert alert-danger span6' style='text-align:center;font-weight: bold;font-size: 16px;'>جلسات برای این هفته در دیتابیس اضافه نشده </div></td></tr>
            </tbody>
        </table>
        @endif
        </div>
    <!-- <a href="{!!URL::route('MeetingForm')!!}" class="btn btn-primary noprint">اضافه کردن جلسه جدید</a> -->
    </div>
    <a href="#" class="btn btn-success noprint" onclick="window.print()">چاپ جلسات</a>

</div>

@stop

@section('footer-scripts')
{!! HTML::script('/js/template/jfullcalendar.min.js') !!}

<script type="text/javascript">
    
    // send the weekHint and bring the meetings based on the dayHint.    
    function prev_next_weeks(weekHint)
    {
        var s_date = $("#start_date").val();
        var e_date = $("#end_date").val();
        
        $.ajax({

            type: 'post',
            url: '{!!URL::route("weeklyMeetingsPrevNext")!!}',
            data: 'week='+weekHint+"&s_date="+s_date+"&e_date="+e_date,
            success: function(response){
                $("#tableContent").html(response);
                $("#start_date").val($("#s_date").val());
                $("#end_date").val($("#e_date").val());
            }
        });


    }

    var start_date = "<?=$start_date;?>";
    var end_date = "<?=$end_date;?>";
    // now send the replaced date to distinguish the day of the week in jalali.
    $.ajax({

        type: 'post',
        url: '{!!URL::route("getMonthDifference")!!}',
        data: {start_date: start_date, end_date: end_date},
        success: function(response){
            $("#date_duration").html(response);
            $("#date_duration_print").html(response);
        
        }
    });
    

    $('input:checkbox[name="print"]').change(function(){
        if ($(this).is(':checked')) {
            $('body').addClass('full_titles');
        }
        else
        {
            $('body').removeClass('full_titles');
        }
    });

    // $('full').change(function()
    // {
    //     $('body').addClass('full_titles');
    // });
    // $('short').change(function()
    // {
    //     $('body').removeClass('full_titles');
    // });

    $(function(){

        //$('[data-toggle="tooltip"]').tooltip();
        $('.full_text').popover();
        

    //     jQuery.balloon.init();
    //     addclose     : false,
    //     addoverlay   : false,
    //     target       : "self",
    //     highlight    : true,
    //     justonce     : false,
    //     ease         : [0, .96, 0, 1.02],
    //     animTime     : 250,
    //     bgcolor      : "#333333",
    //     bordercolor  : "#ffffff",
    //     textcolor    : "#ffffff",
    //     oncursor     : false,
    //     forceposition: "auto", // or: up, down, left, right
    //     timer        : 0, // close the balloon after x millis (0 = never)
    //     balloon      : "This is an mb.balloon"

    });

</script>

@stop