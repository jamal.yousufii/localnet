@extends('layouts.master')

@section('head')
	<title>Meeting form</title>
    <style type="text/css">
        table thead tr th
        {
            text-align: center;
            border-color: #000; 
        }
        table tbody tr td
        {
            border-color: #000;
        }
        .fixed{width: 11% !important;}
    </style>
    {{ HTML::style('/css/font.css') }}
@stop

@section('content')

<div class="row" style="opacity: 1;">
    <div class="col-lg-12 noprint">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ URL::route('home') }}">Dashboard</a>
                    </li>
                    <li class="active">
                        <span>Monthly Meetings</span>
                    </li>
                </ol>
                <h1>Current, previous and next month's Meetings</h1>
            </div>
        </div>
    </div>
   

   <!--  <div class="container noprint" dir="rtl">
        <h4 class="page-header" align="center">جستجو جلسات به اساس تاریخ</h4>

        <form role="form" method="post" action="{{URL::route('searchResult')}}" class="form-horizontal">

            <div class="form-group">

                <div class="col-sm-4">
                    <input class="datepicker_farsi form-control" type="text" name="to" placeholder="End date" readonly="readonly">
                    @if($errors->has("to"))
                        <span style="color: red" dir="ltr">{{ $errors->first('to') }}</span>
                    @endif
                </div>
                <label class="col-sm-2 control-label">تاریخ ختم :</label>
                
                <div class="col-sm-4">
                    <input class="datepicker_farsi form-control" type="text" name="from" readonly="readonly" placeholder="Start date">
                    @if($errors->has("from"))
                        <span style="color: red" dir="ltr">{{ $errors->first('from') }}</span>
                    @endif
                </div>
                <label class="col-sm-2 control-label">تاریخ آغاز :</label>
                
                <div class="col-sm-12">
                    <hr style="border: 1px dashed #b6b6b6" />
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-search"></i>
                        </span>
                        &nbsp;جستجو
                    </button>
                </div>

            </div>
        </form>

    </div> -->

    <!-- <button class="btn btn-info noprint" onclick="prev_next_days('today')">امروز</button> -->
    <div style="text-align:center;" class="noprint">
        <button class="btn btn-info" onclick="prev_next_months('next')"><i class="fa fa-angle-double-left"></i> بعدی </button>  <button class="btn btn-info" onclick="prev_next_months('prev')"> قبلی <i class="fa fa-angle-double-right"></i></button>
    </div>
    <!-- <span style="display: none;margin:0 auto;" class="title"><i id="date_duration_print"></i></span> -->
    <input type="hidden" id="c_date" value="{{date('Y-m-d')}}" />
    <div id="monthly_meetings">

        <i id="date_duration"></i>
        <?php //print_r($monthlyMeetings);exit;?>
        @if(!$monthlyMeetings == '')

        <table class="table table-bordered table-striped" dir="rtl" id="list_of_weeklyMeetings">
            <thead>
              <tr>
                <!-- <th>شماره</th> -->
                <th>تاریخ</th>
                <!-- <th>آغاز جلسه</th>
                <th>ختم جلسه</th> -->
                <th>شنبه</th>
                <th>یکشنبه</th>
                <th>دوشنبه</th>
                <th>سه شنبه</th>
                <th>چهارشنبه</th>
                <th>پنجشنبه</th>
                <th>جمعه</th>
                <th colspan="2" class="noprint">عملیات</th>
                
              </tr>
            </thead>
            <tbody>

                <?php 
                    $counter=0;
                    //print_r($monthlyMeetings);exit;
                ?>
                @foreach($monthlyMeetings AS $item)
                
                    <?php 
                        
                        $counter++;

                        if(count($monthlyMeetings[$counter])==0)
                        {
                            continue;
                        }

                    $meetings = $monthlyMeetings[$counter];

                    for($i=0;$i<count($meetings);$i++)
                    {
                        
                        $row = $meetings[$i];
                        $row = explode("_@_", $row);
                    ?>
                        <tr>
                            
                            <!-- <td>{{$counter}}</td> -->
                            <td class='fixed'>{{$row[1]}}</td>
                            <!-- <td class='fixed'>{{$row[2]}}</td>
                            <td class='fixed'>{{$row[3]}}</td> -->
                            @if($counter == 1)
                                <td class='fixed'>{{$row[0]}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @elseif($counter == 2)
                                <td></td>
                                <td class='fixed'>{{$row[0]}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @elseif($counter == 3)
                                <td></td>
                                <td></td>
                                <td class='fixed'>{{$row[0]}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @elseif($counter == 4)
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class='fixed'>{{$row[0]}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @elseif($counter == 5)
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class='fixed'>{{$row[0]}}</td>
                                <td></td>
                                <td></td>
                            @elseif($counter == 6)
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class='fixed'>{{$row[0]}}</td>
                                <td></td>
                            @elseif($counter == 7)
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class='fixed'>{{$row[0]}}</td>
                            @endif

                            {{$row[4]}}
                            {{$row[5]}}

                        </tr>
                    <?php 
                        } 
                    ?>
                    
                @endforeach

            </tbody>
        </table>
        @else

        <table class="table table-bordered table-striped" dir="rtl" id="list_of_weeklyMeetings">
            <thead>
              <tr>
                <!-- <th>شماره</th> -->
                <th>تاریخ</th>
                <th>آغاز جلسه</th>
                <th>ختم جلسه</th>
                <th>شنبه</th>
                <th>یکشنبه</th>
                <th>دوشنبه</th>
                <th>سه شنبه</th>
                <th>چهارشنبه</th>
                <th>پنجشنبه</th>
                <th>جمعه</th>
                
              </tr>
            </thead>
            <tbody>  
                <td colspan='10'><div class='alert alert-danger span6' style='text-align:center'>جلسات برای این ماه در دیتابیس اضافه نگردیده است !</div></td>
            </tbody> 
        </table>     

        @endif
    </div>
    
    <a href="{{URL::route('MeetingForm')}}" class="btn btn-primary noprint">اضافه کردن جلسه جدید</a>
    <a href="#" class="btn btn-success noprint" onclick="window.print()">چاپ جلسات</a> 

</div>

@stop

@section('footer-scripts')
{{ HTML::script('/js/template/jfullcalendar.min.js') }}

<script type="text/javascript">

    
    // send the weekHint and bring the meetings based on the dayHint.    
    function prev_next_months(monthHint)
    {
        
        var current_date = $("#c_date").val();
        $.ajax({

            type: 'post',
            url: '{{URL::route("monthlyMeetingsPrevNext")}}',
            data: 'month='+monthHint+"&date="+current_date,
            success: function(response){
                $("#monthly_meetings").html(response);
                $("#c_date").val($("#date").val());
            }
        });


    }


    var start_date = "<?=$start_date;?>";
    var end_date = "<?=$end_date;?>";
    // now send the replaced date to distinguish the day of the week in jalali.
    $.ajax({

        type: 'post',
        url: '{{URL::route("getMonthDifference")}}',
        data: {start_date: start_date, end_date: end_date},
        success: function(response){
            $("#date_duration").html(response);
            $("#date_duration_print").html(response);
        
        }
    });
    
    $('#timepicker').timepicker({
        showPeriod: true,
        onHourShow: OnHourShowCallback,
        onMinuteShow: OnMinuteShowCallback
    });
    function OnHourShowCallback(hour) {
        if ((hour > 20) || (hour < 6)) {
            return false; // not valid
        }
        return true; // valid
    }
    function OnMinuteShowCallback(hour, minute) {
        if ((hour == 20) && (minute >= 30)) { return false; } // not valid
        if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        return true;  // valid
    }

    $('#timepicker2').timepicker({
        showPeriod: true,
        onHourShow: OnHourShowCallback,
        onMinuteShow: OnMinuteShowCallback
    });
    function OnHourShowCallback(hour) {
        if ((hour > 20) || (hour < 6)) {
            return false; // not valid
        }
        return true; // valid
    }
    function OnMinuteShowCallback(hour, minute) {
        if ((hour == 20) && (minute >= 30)) { return false; } // not valid
        if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        return true;  // valid
    }

</script>

@stop