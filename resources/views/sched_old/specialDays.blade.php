@extends('layouts.master')

@section('head')
	@parent
	<title>Managing special days</title>
    <style type="text/css">
        .col-sm-4{
            float: right;
        }
        .control-label
        {
            float: right;
        }
        label{
            padding-right: 10px;
        }

        td.hide_border{
            border-top: none !important;
        }
        .form-control{
            width: 60% !important;
        }

    </style>
@stop

@section('content')

@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('getMeetings') !!}">Managing special days</a>
                    </li>
                    <li class="active">
                        <span>Add Days</span>
                    </li>
                </ol>
                <h1>Add special days of the year</h1>
            </div>
        </div>
    </div>


    <div class="container" dir="rtl">
        @if(Session::has('success'))
            <div class='alert alert-success span6 noprint' dir="rtl" align="center">{!!Session::get('success')!!}</div>

        @elseif(Session::has('fail'))
            <div class='alert alert-danger span6 noprint' dir="rtl" align="center">{!!Session::get('fail')!!}</div>
        @endif
        <h4 class="page-header" align="center">اضافه کردن روز های خاص در یک سال</h4>

        <form role="form" method="post" action="{!! URL::route('addSpecialDays') !!}" class="form-horizontal">

            <table class="table">
                <tr>
                    <td>عنوان :</td>
                    <td>
                        <input type="text" class="form-control" name="title" placeholder="Title ..." value="{!!old('title')!!}" required/>              
                    </td>
                </tr>
                <tr>
                    <td class="hide_border"></td>
                    @if($errors->has("title"))
                    <td class="hide_border"> 
                        <span style="color: red" dir="ltr">{!! $errors->first('title') !!}</span>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td>شرح بیشتر :</td>
                    <td>
                        <textarea cols="20" rows="3" placeholder="Description ..." class="form-control" name="description">{!!old('description')!!}</textarea>                    
                    </td>
                </tr>
                <tr>
                    @if($errors->has("description"))
                    <td class="hide_border"></td>
                    <td class="hide_border">
                        <span style="color: red" dir="ltr">{!! $errors->first('description') !!}</span>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="radio" name="type" value="1"> ملی &nbsp;&nbsp;
                        <input type="radio" name="type" value="2"> بین المللی  
                    </td>
                </tr>
                <tr>
                    @if($errors->has("type"))
                    <td class="hide_border"></td><td class="hide_border">
                        <span style="color: red" dir="ltr">{!! $errors->first('type') !!}</span>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td>تاریخ :</td>
                    <td>
                        <input class="datepicker_farsi form-control" type="text" id="current_date" name="date" readonly="readonly" value="{!!old('date')!!}">                 
                    </td>
                </tr>
                <tr>
                    @if($errors->has("date"))
                    <td class="hide_border"></td>
                    <td class="hide_border">
                        <span style="color: red" dir="ltr">{!! $errors->first('date') !!}</span>
                    </td>
                    @endif
                </tr>

            </table>

            <div class="form-group">
                <div class="col-sm-12">
                    <hr style="border: 1px dashed #b6b6b6" />
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-check"></i>
                        </span>
                        &nbsp;تأید
                    </button>
                </div>

                <div class="col-sm-12">
                    <a style='margin-bottom:10px' class='pull-left span2 btn btn-success' href="{!!URL::route('SD_List')!!}">لست روز های خاص</a>
                </div>

            </div>
        </form>

    </div>

</div>

<script type="text/javascript">
    
    // $('#timepicker').timepicker({
    //     showPeriodLabels: false,
    // });

    // $('#timepicker2').timepicker({
    //     showPeriodLabels: false,
    // });

    //send the current date to the controller and covnvert it to jalali date and replace the value.
    $.ajax({

        type: 'post',
        url: '{!!URL::route("convertToJalali")!!}',
        data: 'date='+"{!!date('Y-m-d')!!}",
        success: function(response){
            $("#current_date").val(response);
        
        }
    });

    // now send the replaced date to distinguish the day of the week in jalali.
    $.ajax({

        type: 'post',
        url: '{!!URL::route("getJalaliWeekDay")!!}',
        data: 'date='+"{!!date('Y-m-d')!!}",
        success: function(response){
            //alert(response);
            $("#week_day").val(response);
        
        }
    });

    // onchange of the date the weekday is being changed accordingly.
    $("#current_date").change(function(){
        var value = $("#current_date").val();

        // now send the replaced date to distinguish the day of the week in jalali.
        $.ajax({

            type: 'post',
            url: '{!!URL::route("getCurrentWeekDay")!!}',
            data: 'date='+value,
            success: function(response){
                //alert(response);
                $("#week_day").val(response);
            
            }
        });

    }); 

    $(function()
    {     


    });


</script>

@stop