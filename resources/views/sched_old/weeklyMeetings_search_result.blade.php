@extends('layouts.master')

@section('head')
    @parent
    <title>Weekly Meetings</title>
@stop

@section('content')


<div class="row" style="opacity: 1;">
    <div class="col-lg-12 noprint">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ URL::route('weeklyMeetingsSearch') }}">Search Meetings</a>
                    </li>
                    <li class="active">
                        <span>Search Meetings result</span>
                    </li>
                </ol>
                <h1>Meetings search result</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row noprint" id="search" style="display: none">
           
            <div class="col-xs-12 col-sm-12">
                <div class="box ui-draggable ui-droppable">

                    <div class="box-content" dir="rtl">
                        <h4 class="page-header" align="center">جستجو جلسات به اساس تاریخ</h4>

                        <form role="form" method="post" action="{{URL::route('searchResult')}}" class="form-horizontal">

                            <div class="form-group">

                                <div class="col-sm-4">
                                    <input class="datepicker_farsi form-control" value="{{$end_date}}" type="text" name="to" id="to" placeholder="End date" readonly="readonly">
                                    @if($errors->has("to"))
                                        <span style="color: red" dir="ltr">{{ $errors->first('to') }}</span>
                                    @endif
                                </div>
                                <label class="col-sm-2 control-label">تاریخ ختم :</label>
                                
                                <div class="col-sm-4">
                                    <input class="datepicker_farsi form-control" type="text" value="{{$start_date}}" name="from" id="from" readonly="readonly" placeholder="Start date">
                                    @if($errors->has("from"))
                                        <span style="color: red" dir="ltr">{{ $errors->first('from') }}</span>
                                    @endif
                                </div>
                                <label class="col-sm-2 control-label">تاریخ آغاز :</label>
                                
                                <div class="col-sm-12">
                                    <hr style="border: 1px dashed #b6b6b6" />
                                    <button class="btn btn-primary" type="submit">
                                        <span>
                                            <i class="fa fa-search"></i>
                                        </span>
                                        &nbsp;جستجو
                                    </button>
                                </div>

                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>

    
<!--         <center>
            <img style="display: none;margin:0 auto;" class="title" width="110px" height="120px" src="<?= asset('img/logo.jpg')?>">
        </center>
        <span style="display: none;margin:0 auto;" class="title">جمهوری اسلامی افغانستان</span>
        <span style="display: none;margin:0 auto;" class="title">ریاست تشریفات</span>
        <span style="display: none;margin:0 auto;" class="title">جلسات هفته وار جلالتمآب رئیس صاحب جمهور</i></span> -->
        <span style="display: none;margin:0 auto;" class="title"><i id="date_duration"></i></span>

        <h3 style="text-align:center;padding-bottom:10px" class="noprint">جلسات از <?=$start_date?> الی <?=$end_date?></i></h3>
        <table class="table table-bordered table-striped" dir="rtl" id="list_of_weeklyMeetings">
            <thead>
              <tr>
                <!-- <th>شماره</th> -->
                <th>تاریخ</th>
                <th>آغاز جلسه</th>
                <th>ختم جلسه</th>
                <th>شنبه</th>
                <th>یکشنبه</th>
                <th>دوشنبه</th>
                <th>سه شنبه</th>
                <th>چهار شنبه</th>
                <th>پنج شنبه</th>
                <th>جمعه</th>
                
              </tr>
            </thead>
            <tbody>

                <?php 
                    $counter=0;
                    //print_r($weeklyMeetings);
                ?>
                @foreach($weeklyMeetings AS $item)
                
                    <?php 
                        $counter++;
                        if(count($weeklyMeetings[$counter])==0)
                        {
                            continue;
                        }
                    


                    $meetings = $weeklyMeetings[$counter];

                    for($i=0;$i<count($meetings);$i++)
                    {
                        
                        $row = $meetings[$i];
                        $row = explode("_@_", $row);
                    ?>
                        <tr>
                            
                            <!-- <td>{{$counter}}</td> -->
                            <td>{{$row[1]}}</td>
                            <td>{{$row[2]}}</td>
                            <td>{{$row[3]}}</td>
                            @if($counter == 1)
                                <td>{{$row[0]}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @elseif($counter == 2)
                                <td></td>
                                <td>{{$row[0]}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @elseif($counter == 3)
                                <td></td>
                                <td></td>
                                <td>{{$row[0]}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @elseif($counter == 4)
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{$row[0]}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            @elseif($counter == 5)
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{$row[0]}}</td>
                            @elseif($counter == 6)
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{$row[0]}}</td>
                            @elseif($counter == 7)
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{$row[0]}}</td>
                            @endif
                        </tr>
                    <?php 
                        } 
                    ?>
                    
                @endforeach

                @if(count($weeklyMeetings) == 0)
                    <tr>
                        <td colspan="10" align="center" style="color: red">موردی پیدا نشد</td>
                    </tr>
                @endif
            </tbody>
        </table>
        
            <div class="btn-group" role="group" style='padding:10px;padding-top:20px;'>
              <a href="#" class="btn btn-success noprint" onclick="window.print()">چاپ جلسات هفته وار</a> 
            </div>

            <div class="btn-group pull-right" role="group" style='padding:10px;padding-top:20px;'>
              <a href="#" class="btn btn-primary noprint" id="search_again">جستجوی دوباره</a> 
            </div>
        </div>
    </div>

<script type="text/javascript">

    $(function(){

        $("#search_again").click(function(){
            $("#search").fadeIn('slow');
            $("#search_again").hide();
        });

    });

    var start_date = "<?=$start_date;?>";
    var end_date = "<?=$end_date;?>";
    // now send the replaced date to distinguish the day of the week in jalali.
    $.ajax({

        type: 'post',
        url: '{{URL::route("getMonthDifference")}}',
        data: {start_date: start_date, end_date: end_date},
        success: function(response){
            $("#date_duration").html(response);
        
        }
    });

</script>

@stop


