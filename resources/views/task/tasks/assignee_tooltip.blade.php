
@foreach($assignees AS $a_item)
	<?php
	    $photo = getProfilePicture_task($a_item->assigned_to);
	    if (file_exists('/var/www/html/localnet/public/documents/profile_pictures/small_'.$photo))
        {
            $photo = 'small_'.$photo;
        }
        else
        {
        	$photo = 'small_default.jpeg';
        }
	    $tooltip = getUserFullName($a_item->assigned_to);
	?>
	<div>
		{!!HTML::image('/documents/profile_pictures/'.$photo, '', array('class' => 'project-img-owner'));!!}
		<span style="padding-top: 6px;display: inline-block">
		&nbsp;<?=$tooltip?>
		</span>
	</div>
	<br/>
@endforeach
