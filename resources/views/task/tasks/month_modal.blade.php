<?php
$shamsi_months = array('1'=>'حمل',
                     '2'=>'ثور',
                     '3'=>'جوزا',
                     '4'=>'سرطان',
                     '5'=>'اسد',
                     '6'=>'سنبله',
                     '7'=>'میزان',
                     '8'=>'عقرب',
                     '9'=>'قوس',
                     '10'=>'جدی',
                     '11'=>'دلو',
                     '12'=>'حوت'); 
$the_date = dateToMiladi($year,$month,1);
$the_date = date("N", strtotime($the_date))+2;//1:monday+2=3=> our monday from satarday
?>
<table border="1px solid;" width="100%" style="text-align: center;font-weight: bold;">
<tr>
    <td colspan="7">{!!$shamsi_months[$month]!!} {!!$year!!}</td>
</tr>
<tr>
    <td>شنبه</td>
    <td>یک شنبه</td>
    <td>دو شنبه</td>
    <td>سه شنبه</td>
    <td>چهار شنبه</td>
    <td>پنج شنبه</td>
    <td>جمعه</td>
</tr>
<?php 
if($month<=6)
{
    $month_day = 31;
}
elseif($month<12)
{
    $month_day = 30;
}
else
{
    if(isItLeapYear($year))
    {
        $month_day = 30;
    }
    else
    {
        $month_day = 29;
    }
}
$day_counter=0;?>
@for($i=1;$i<=42;$i++)
@if($i>=$the_date)
<?php $day_counter++;?>
@endif
@if($i == 1 || $i%7==1)
<tr>
@endif
@if($day_counter>0 && $day_counter<=$month_day)
    <?php
    $the_day = dateToMiladi($year,$month,$day_counter);
    $miladi_year = explode('-', $the_day);
    $the_day_no = date("z", strtotime($the_day))-78;//day of the year - 83 = day no of shamsi year
    $the_week_no = $the_day_no/7;
    $the_week_no = round($the_week_no)+1;
    ?>
    <td @if(day_has_task($the_day)) style="background-color: #bcd8f1;cursor: pointer;" onclick="load_day_tasks('{!!$the_week_no!!}','{!!$miladi_year[0]!!}')" @endif>
        {!!$day_counter!!}
    </td>
@else
    <td>
        
    </td>
@endif
@if($i%7==0)
</tr>
@endif

@endfor
</table>

<script type="text/javascript">
    function load_day_tasks(week,year)
    {
        window.location = "/task/getReportWeek/"+week+"/all/"+year;
    }
</script>