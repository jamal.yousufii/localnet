@extends('layouts.master')

@section('head')
    {{ HTML::style('/css/farsi_date/pwt-datepicker.css') }}
    <title>Create New Task</title>
@stop

@section("content")

<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active"><span>Task Create</span></li>
        </ol>
        
        <h1>Task<small>Create</small></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>Create Task</h2>
            </header>
            
            <div class="main-box-body clearfix">
                <form role="form" method="post" action="{{ URL::route('postReportCreate') }}" class="form-horizontal">
                    
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Title :</label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" name="title" data-placement="bottom">
                            </div>
                            
                            <label class="col-sm-2 control-label">Task Group :</label>
                            <div class="col-sm-4">
                                <?php 
                                    $page = URL::route('isTaskGroupInReport');
                                ?>
                                <select class="form-control" name='task_group' id="task_group" onchange="isTaskGroupInReport('{{$page}}',this.value)">
                                    <option>Select</option>
                                    @foreach($taskGroup AS $item)
                                        <option value='{{$item->id}}'>{{$item->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            
                            <label class="col-sm-2 control-label">Start date :</label>
                            <div class="col-sm-4">
                                <input class="form-control datepicker_farsi" type="text" name="start_date" data-placement="bottom" readonly="readonly">
                            </div>
                        
                            <label class="col-sm-2 control-label">End date :</label>
                            <div class="col-sm-4">
                                <input class="form-control datepicker_farsi" type="text" name="end_date" data-placement="bottom" readonly="readonly">
                            </div>
                            
                        </div>
                        

                        <!--include in report-->
                        <div id="include_in_report" style="display:none;">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Indicator :</label>
                                <div class="col-sm-2">
                                    <input class="form-control" type="text" name="indicator" data-placement="bottom">
                                </div>
                                <label class="col-sm-2 control-label">Metric :</label>
                                <div class="col-sm-2">
                                    <input class="form-control" type="text" name="metric" data-placement="bottom">
                                </div>

                                <label class="col-sm-2 control-label" style="padding-left: 0;font-size: 90%;">Quantity of deliverables :</label>
                                <div class="col-sm-2">
                                    <input class="form-control" type="text" name="quantity_of_deliverables" data-placement="bottom">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="form-styles">Expected outcome :</label>
                                <div class="col-sm-10">
                                        <textarea class="form-control" rows="2" id="wysiwig_full" name="expected_outcome"></textarea>
                                </div>
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-styles">Description :</label>
                            <div class="col-sm-10">
                                    <textarea class="form-control" rows="2" name="description"></textarea>
                            </div>
                        </div>
                        

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Assign To :</label>
                            <div class="col-sm-10">
                                <div id="users_div" style="display:none;">
                                    <select name='assign_with' id='assign_with' style="width:500px;">
                                        <option value=''>Select</option>
                                        <?php 
                                            foreach($users AS $uitem)
                                            {
                                                echo "<option value='".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div id="managers_div" style="display:inline">
                                    <select name='assign_with' id='assign_with1' style="width:500px;">
                                        <option value=''>Select</option>
                                        <?php 
                                            foreach($managers AS $uitem)
                                            {
                                                echo "<option value='".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                &nbsp;
                                <div class="checkbox-nice checkbox-inline">
                                    <input type="checkbox" id="only" onclick="showManagers()">
                                    <label for="only">
                                        Show All Subordinates
                                    </label>
                                </div>

                            </div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="checkbox-nice checkbox-inline">
                                <input value='1' id='by_email' name="by_email" type="checkbox">
                                <label for="by_email">
                                    Notify By Email
                                </label>
                            </div>
                            <div class="checkbox-nice checkbox-inline">
                                <input value='1' name="by_sms" id='by_sms' type="checkbox">
                                <label for="by_sms">
                                    Notify By SMS
                                </label> 
                            </div>
                            
                        </div>


                        <fieldset>
                            <legend>Sub Tasks</legend>
                                <div id="sub_task_frm">
                                    <div class="row" id="all_rows" style="padding:10px;">
                                        
                                        <div id="row_1">
                                            <div class="col-xs-5">
                                                <table width="100%">
                                                    <tr>
                                                        <td id="hide_fields"><a href="#" style="display:inline;"><i class="fa fa-plus-circle fa-lg"></i></a></td>
                                                        <td><input type="text" class="form-control" name="sub_title[]" placeholder="Title"></td>
                                                    </tr>
                                                </table>
                                            </div> 
                                            <div class="col-xs-2">
                                                <div class="input-group">
                                                    <input type="text" class="form-control datepicker_farsi" name="sub_start[]" placeholder="Start Date">
                                                </div>
                                            </div>            
                                            <div class="col-xs-2">
                                                <div class="input-group">
                                                    
                                                    <input type="text" class="form-control datepicker_farsi" name="sub_end[]" placeholder="End Date">
                                                </div>
                                            </div> 
                                            <div class="col-xs-1">
                                                <div class="input-group">
                                                    
                                                    <span class="input-group"><i class="fa fa-plus-circle fa-lg"></i></span>
                                                    <!-- <select class="form-control" name="sub_assign[]" style="width:200px;">
                                                        <option value="">Select</option>
                                                        <?php 
                                                            //foreach($users AS $uitem)
                                                            {
                                                                //echo "<option value='".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
                                                            }
                                                        ?>
                                                    </select> -->
                                                
                                                </div>
                                            </div>
                                             
                                        </div>
                                        <div class="col-xs-2">
                                            <div class="input-group">
                                                
                                                <a href="javascript:void()" class="btn btn-primary" onclick="repeatRow()"><i class="fa fa-plus-circle fa-lg"></i></a>
                                                
                                            </div>
                                        </div>

                                    </div>
                                </div>
                        
                        </fieldset>
                    {{ Form::token() }}
                    <fieldset>
                        <legend>&nbsp;</legend>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-sm-10">
    
                            <button class="btn btn-primary" type="submit">Save</button>
                        
                            <button onclick="history.back()" class="btn btn-danger" type="button">Cancel</button>
                        </div>
                    </div>
                    </fieldset>
                </form>
                
            </div>
        </div>

    </div>
</div>
@stop
@section('footer-scripts')
{{ HTML::script('/js/template/select2.min.js')}}
{{ HTML::script('/js/template/hogan.js')}}
{{ HTML::script('/js/template/typeahead.min.js')}}

<script type="text/javascript">
    function showDatePicker(e)
    {

        $(".datepicker_farsi").persianDatepicker();       
    }
</script>

<script type="text/javascript">
      $("#assign_with").select2();
      $("#assign_with1").select2();

        $(document).ready(function() {
        // Create Wysiwig editor for textare
        TinyMCEStart('#wysiwig_simple', null);
        //TinyMCEStart('#wysiwig_full', 'extreme');
        });

    //repeat row
    function repeatRow()
    {

        
        allRows = $('#sub_task_frm').find('.row').length;
        allRows = parseInt(allRows)+1;
        var row = $('#row_1').html();
        var removeEl = '<div class="col-xs-2"><div class="input-group"><a href="javascript:void()" class="btn btn-danger" onclick="removeRow(\'row_'+allRows+'\')"><i class="fa fa-minus-circle fa-lg"></i></a></div></div>';
        row = "<div id='row_"+allRows+"' class='row' style='padding:10px;'>"+row+removeEl+"</div>";
        $('#sub_task_frm').append(row);
        //$('#sub_task_frm').append($('<input type="text" class="form-control datepicker_farsi" name="sub_ssstart[]" placeholder="Start Date">'));
        $('.datepicker_farsi').persianDatepicker();
    }
    //remove row
    function removeRow(row)
    {
        $('#'+row).remove();
    }

    //is task group included in report or not
    function isTaskGroupInReport(page,selectedTaskGroup)
    {
        $.ajax({
            url:page,
            type:'post',
            dataType:'json',
            data: '&task_group='+selectedTaskGroup,
            success: function(response){

                if(response.condition == 'true')
                {
                    $('#include_in_report').slideDown();
                }
                else
                {
                    $('#include_in_report').slideUp();
                }
            }
        });
    }
    //show only managers
    function showManagers()
    {
        var ch = document.getElementById('only');
        if(ch.checked)
        {
            document.getElementById('managers_div').style.display = "none";
            document.getElementById('users_div').style.display = "inline";
            
        }
        else
        {
            document.getElementById('managers_div').style.display = "inline";
            document.getElementById('users_div').style.display = "none";
            
        }
    }
</script>
@stop
