@foreach($task_details AS $task_item)
<form id="form_edit">
    <input type="hidden" name="task_id" value="{!!$task_item->id!!}">
    <!--  -->
    <table border='0' width="100%">
        <tr>
            <td>
                <input value='{!!$task_item->title!!}' placeholder="Title..." type="text" name="title">
            </td>
        </tr>
        <tr> 
            
            <td>
                <?php 
                    $page = route('isTaskGroupInReport');
                ?>
                <select name='task_group' id="task_group" onchange="isTaskGroupInReport1('{!!$page!!}',this.value)">
                    <option>Task Group...</option>
                    @foreach($taskGroup AS $item)
                        <option <?php if($item->id == $task_item->task_group_id){echo "selected";} ?> value='{!!$item->id!!}'>{!!$item->title!!}</option>
                    @endforeach
                </select>
            </td>
        </tr>
        <tr>
            
            <td>
            	<?php $sdate = $task_item->start_date; if($sdate!='0000-00-00'){$s_date = explode('-', $task_item->start_date);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                <input value='<?php if($task_item->start_date!="0000-00-00"){echo jalali_format($sdate);}?>' placeholder='Start Date...' class="datepicker_farsi" type="text" name="start_date" data-placement="bottom" readonly="readonly">
                
            </td>
        </tr>
        <tr>
            
            <td>
            	<?php $edate = $task_item->end_date; if($edate!='0000-00-00'){$e_date = explode('-', $task_item->end_date);$edate = dateToShamsi($e_date[0], $e_date[1], $e_date[2]);}?>
                <input value='<?php if($task_item->end_date!="0000-00-00"){echo jalali_format($edate);}?>' placeholder='End Date...' class="datepicker_farsi" type="text" name="end_date" data-placement="bottom" readonly="readonly">
            </td>
        </tr>
        <tr>
            <td>
                <textarea placeholder='Descriptions...' rows="2" name="description">{!!$task_item->description!!}</textarea>
            </td>
        </tr>
        <tr>
            <table id="include_in_report1" width="100%" style="display:<?php if(isTaskGroupInReport($task_item->task_group_id)){echo '';}else{echo "none";} ?>;">
                <tr>
                    <td><input value='{!!$task_item->indicator!!}' placeholder="Indicator..." type="text" name="indicator"></td>
                </tr>
                <tr>
                    <td><input value='{!!$task_item->metric!!}' placeholder="Matric..." type="text" name="metric" data-placement="bottom"></td>
                </tr>
                <tr>
                    <td><input value='{!!$task_item->quantity_of_deliverables!!}' placeholder="Quantity of deliverables..." type="text" name="quantity_of_deliverables" data-placement="bottom"></td>
                </tr>
                <tr>
                    <td><textarea placeholder="Expected outcom..." rows="2" id="wysiwig_full" name="expected_outcome">{!!$task_item->expected_outcome!!}</textarea></td>
                </tr>
            </table>
            
        </tr>
        <tr>
            <td>
                <div id="users_div1" style="display:none;">
                    <select name='assign_with1[]' id='assign_with2' style="width:100%;" multiple="multiple">
                        
                        <?php 
                            foreach($users AS $uitem)
                            {
                                $selected = "";
                                if(in_array($uitem->id, $assignees)){$selected='selected';}
                                echo "<option ".$selected." value='".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
                            }
                        ?>
                    </select>
                </div>
                <div id="managers_div1" style="display:inline">
                    <select name='assign_with[]' id='assign_with3' style="width:100%;" multiple="multiple">
                        <?php 
                            foreach($managers AS $uitem)
                            {
                                $selected = "";
                                if(in_array($uitem->id, $assignees)){$selected='selected';}
                                echo "<option ".$selected." value='".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
                            }
                        ?>
                    </select>
                </div>
                
                <input type="checkbox" name="subordinates" id="only1" onclick="showManagers1()">
                <label for="only1">
                    Subordinates
                </label>
                
            </td>
        </tr>
        <tr>
            <td colspan='2'>
                <div style="margin-top:10px;" class="form-group">
                    <div class="checkbox-nice checkbox-inline">
                        <input <?php if($task_item->notify_by_email==1){echo "checked";} ?> value='1' id='by_email1' name="by_email" type="checkbox">
                        <label for="by_email1">
                            Notify By Email
                        </label>
                    </div>
                    <div class="checkbox-nice checkbox-inline">
                        <input <?php if($task_item->notify_by_sms==1){echo "checked";} ?> value='1' name="by_sms" id='by_sms1' type="checkbox">
                        
                        <label for="by_sms1">
                            Notify By SMS
                        </label> 
                    </div>
                    
                </div>
            </td>
        </tr>
    </table>

    <!-- Sub task start -->
    <fieldset style="border-color:red !important;">
        <legend>Sub Task</legend>
        <div class="list-task">
            <ul style="margin:0 0 0 0px;">
                
                <div id="task_id_{!!$task_item->id!!}">
                    @foreach($sub_tasks AS $item)
                    <?php 
                        if($item->end_date != '0000-00-00')
                        {
                            //convert shamsi to meladi
                            $end_date = explode("-", $item->end_date);
                            $end_date = dateToMiladi($end_date[0],$end_date[1],$end_date[2]);
                        }
                        else
                        {
                            $end_date = date('Y-m-d');
                        }
                        
                        $from=date_create($end_date);
                        $to=date_create(date('Y-m-d'));
                        
                        $diff=date_diff($to,$from);
                        
                        $deadline = $diff->format('%R%a');
                        $d=$deadline;

                        $label_status = "";
                        $days;

                        switch (true) {
                            case ($d < 0):
                                $days='Due';
                                break;
                            case ($d == 0):
                                $days='Today';
                                break;
                            case ($d == 1):
                                $days='Tomorrow';
                                break;
                            case ($d > 1 and $d < 7):
                                $days=abs($d).' Days';
                                break;
                            case ($d > 6 and $d < 15):
                                $days='Next Week';
                                break;
                            case ($d > 14 and $d < 22):
                                $days='Two Weeks';
                                break;
                            case ($d > 21 and $d < 29):
                                $days='Three Weeks';
                                break;
                            case ($d > 28 and $d < 56):
                                $days='Next Month';
                                break;
                            case ($d > 55 and $d < 336):
                                $nm=round($d/30);
                                $days=$nm.' Months';
                                break;
                            case ($d > 335):
                                $ny=round($d/365);
                                $days=$ny.' Year(s)';
                                break;
                            default:
                                $days='select date';
                                break;
                        }

                        if($d>0)
                        {
                            $label_status = "label-success";
                        }
                        else if($d == 0)
                        {
                            $label_status = "label-warning";
                        }
                        else
                        {
                            $label_status = "label-danger";
                        }

                        $s_date = '';
                        $e_date = '';
                        if($item->start_date !='0000-00-00')
                        {
                            $s_date = dmy_format($item->start_date);
                        }
                        if($item->end_date !='0000-00-00')
                        {
                            $e_date = dmy_format($item->end_date);
                        }

                    ?>
                    <li>
                        <div class="checkbox-nice checkbox-inline">
                            <input type="checkbox" readonly>
                            <label>&nbsp;</label>
                        </div>
                        <a href="#" onclick="loadTaskDetails('edit_form','{!!$item->id!!}')"><div style="width:150px;" class="task_title">{!!$item->title!!}</div></a>
                        <span id="label_{!!$item->id!!}">
                            <a href="javascript:void()" onclick="$('#task_id_modal').val('{!!$item->id!!}');$('#start_date_modal').val('{!!$s_date!!}');$('#end_date_modal').val('{!!$e_date!!}');" class="md-trigger" data-modal="change_date_modal">
                                <span class="label {!!$label_status!!}"> {!!$days!!}</span>
                            </a>
                        </span>
                        
                        <div class="pull-right">
                            <!--
                            <div class="calendar-icon" style="float:left;margin:7px 5px 0px 0px;">
                                <a href="javascript:void()" onclick="$(\'#task_id_modal\').val(\''.$item->id.'\');$(\'#start_date_modal\').val(\''.$s_date.'\');$(\'#end_date_modal\').val(\''.$e_date.'\');" class="md-trigger" data-modal="change_date_modal"><i class="fa fa-calendar fa-lg"></i></a>
                            </div>
                            -->
                            <div class="assignees" style="display:inline;">
                            <?php
                            //get task progress
                            $progress = getMainTaskProgress($item->id);
                            //get task assignees
                            $assignees = getTaskAssignees($item->id);
                            foreach($assignees AS $a_item)
                            {
                                $photo = getProfilePicture($a_item->assigned_to);
                                $tooltip = getUserFullName($a_item->assigned_to);

                                echo HTML::image('/img/'.$photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip'));
                            }

                            ?>
                            </div>
                            <div class="progress sub_progress" title="{!!$progress!!}% Completed" style="display:inline-block;">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{!!$progress!!}" aria-valuemin="0" aria-valuemax="100" style="width: {!!$progress!!}%;">
                                    <span class="sr-only">{!!$progress!!}% Complete</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </div>
                
                <div class="add-item">
                    <span class="item-plus">+</span>
                    <input onkeypress="saveNewTask(event,'task_id_{!!$task_item->id!!}','{!!$task_item->task_group_id!!}','{!!$task_item->id!!}','task_title_{!!$task_item->id!!}')" style="width:300px;border:0px;" placeholder="Add New Sub Task..." type="text" name="title_sub" id="task_title_{!!$task_item->id!!}" style="border:none;">
                </div>
                
            </ul>
        </div>
    </fieldset>
    <!-- Sub task edn -->
    
    <!--  -->
    {!! Form::token() !!}
    <br>
    <div class="form-group pull-right">
        <label class="col-sm-2 control-label">&nbsp;</label>
        <div class="col-sm-10" style="width:100%">
            <a href="{!!URL::route('DeleteReport', $task_item->id)!!}" class="btn btn-danger" type="button" onclick="javascript:return confirm('Are you sure you want to DELETE this report and its subtasks ? THERE IS NO UNDO')"><i class="fa fa-trash-o fa-lg"></i> Delete</a>
            <button class="btn btn-primary" type="button" onclick="updateTaskDetails()"><i class="fa fa-refresh fa-lg"></i> Update</button>
            <button class="btn btn-primary" type="button" onclick="location.href='viewReport/{!!$task_item->id!!}'"><i class="fa fa-refresh fa-lg"></i> Details</button>
        </div>
    </div>
    
</form>


@endforeach

{!! HTML::script('/js/template/modernizr.custom.js')!!}
{!! HTML::script('/js/template/classie.js')!!}
{!! HTML::script('/js/template/modalEffects.js')!!}

<script type="text/javascript">

//show only managers
function showManagers1()
{
    var ch = document.getElementById('only1');
    if(ch.checked)
    {
        document.getElementById('managers_div1').style.display = "none";
        document.getElementById('users_div1').style.display = "inline";
        
    }
    else
    {
        document.getElementById('managers_div1').style.display = "inline";
        document.getElementById('users_div1').style.display = "none";
        
    }
}
//update task details
function updateTaskDetails()
{
    var page = "{!!URL::route('updateReportViaAjax')!!}";
    $.ajax({
            url: page,
            type: 'post',
            data: $('#form_edit').serialize(),
            beforeSend: function(){

                    $("#form_edit").hide();
                    $("#form_new").show();
                    $("#form_new").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            dataType:'json',
            success: function(response)
            {
                if(response.cond == 'true')
                {
                    $("#form_new").hide();
                    $("#form_edit").slideDown();
                    $('#success_alert').slideDown();
                }
                else
                {
                    alert('Error!');
                }

            },
            error:function(err)
            {
                alert(err);
            }
    });
    return false;
    

}
//is task group included in report or not
function isTaskGroupInReport1(page,selectedTaskGroup)
{
    $.ajax({
        url:page,
        type:'post',
        dataType:'json',
        data: '&task_group='+selectedTaskGroup,
        success: function(response){

            if(response.condition == 'true')
            {
                $('#include_in_report1').slideDown();
            }
            else
            {
                $('#include_in_report1').slideUp();
            }
        }
    });
}
$("#assign_with2").select2();
$("#assign_with3").select2();
$(".datepicker_farsi").persianDatepicker(); 
</script>
