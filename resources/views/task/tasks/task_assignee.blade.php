
<style>
	.select2-dropdown{
		z-index: 100000;
	}
</style>
<script>
	$("#assign_with").select2();
	$("#assign_with1").select2();
	//show only managers
	function showManagers1()
	{
	    if($('#subordinates').is(':checked'))
	    {
	        document.getElementById('managers_div1').style.display = "inline";
	        document.getElementById('users_div1').style.display = "none";
	    }
	    else
	   {
	   		document.getElementById('managers_div1').style.display = "none";
	        document.getElementById('users_div1').style.display = "inline";     
	    }
	}
	function updateTaskDetails()
	{
	    var page = "{!!URL::route('updateAssigneeViaAjax')!!}";
	    $.ajax({
	            url: page,
	            type: 'post',
	            data: $('#form_edit').serialize(),
	            dataType:'json',
	            success: function(response)
	           {
	                if(response.cond == 'true')
	                {
	                    window.location.reload();
	                }
	                else
	              {
	                    alert('Sorry, something is wrong. try agian later');
	                }
	
	            },
	            error:function(err)
	           {
	                alert(err);
	            }
	    });
	    return false;	
	}
</script>
<div class="modal-dialog" style="width: 70%">
	<div class="modal-content">
		<div class="modal-header">
			@if($type=='task_detail')
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{!!_('assign_task')!!}</h4>
			@else
				<button type="button" class="btn btn-danger" onclick="load_task_detail('task_detail',{!!$details->parent_task!!})">{!!_('back')!!}</button>
			@endif
			
		</div>
		<form id="form_edit">
    	<input type="hidden" name="task_id" value="{!!$details->id!!}">
    	<input type="hidden" name="by_email" value="">
		<div class="modal-body">
			<div class="row">				
				<div class="main-box-body clearfix">						
					<div class="input-group form-group col-md-12 ">
	               		<div class="col-md-8 select2-primary no-padding">
	                		<div class="input-group">
								<span class="input-group-addon">{!!_('assigned_to')!!}</span>						
								<div id="managers_div1" <?php if($details->subordinates === 0){echo 'style="display:block"';}else{echo 'style="display:none;"';}?>>
		                    		<select name='assign_with[]' id='assign_with' style="width:100%;" multiple="multiple" data-plugin="select2">
				                        <?php 
				                            foreach($managers AS $uitem)
				                            {
				                                $selected = "";
				                                if(in_array($uitem->id, $assignees)){$selected='selected';}
				                                echo "<option ".$selected." value='".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
				                      }
				                        ?>
				                    </select>					        
		                		</div>
				                <div id="users_div1" <?php if($details->subordinates !== 0){echo 'style="display:block"';}else{echo 'style="display:none;"';}?>>
				                    <select name='assign_with1[]' id='assign_with1' style="width:100%;" multiple="multiple" data-plugin="select2">
				                        
				                        <?php 
				                            foreach($users AS $uitem)
				                            {
				                                $selected = "";
				                                if(in_array($uitem->id, $assignees)){$selected='selected';}
				                                echo "<option ".$selected." value='".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
				                            }
				                        ?>
				                    </select>
				                </div> 
		               		</div>
		                </div>          
			            <div class="col-md-3 col-md-offset-1 checkbox-custom checkbox-primary checkbox-inline">    	
                            <input onclick="showManagers1()" value='0' name="subordinates" id='subordinates' type="checkbox" @if($details->subordinates ===0) checked @endif>
                            <label for="subordinates">
                                {!!_('direct_subordinates')!!}
                            </label> 
                                                                   	                                                                         
						</div>
		            </div>
		             	
					<div class="form-group pull-right" id="all_buttons">
				        <label>&nbsp;</label>
				        <div class="col-sm-12">
				            <button class="btn btn-primary" type="button" onclick="updateTaskDetails()"><i class="fa fa-refresh fa-lg"></i> {!!_('update')!!}</button>
				        </div>
				    </div>
				</div>
					
			</div>
		</div>
		</form>
	</div>
</div>
