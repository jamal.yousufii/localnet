@extends('layouts.master')
@section('head')
 	
    
    {!! HTML::style('/css/tooltipster.css') !!}
    {!! HTML::style('/css/template/libs/nifty-component.css') !!}
	{!! HTML::style('/css/template/libs/jquery.nouislider.css') !!}

	
    {!! HTML::script('/js/jquery.tooltipster.min.js')!!}
    {!! HTML::style('/css/tasks.css') !!}
    {!! HTML::script('/js/tasks.js') !!}
    {!! HTML::style('/vendor/clockpicker/clockpicker.min.css') !!}
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
    {!! HTML::script('/js/template/jquery.slimscroll.min.js')!!}
    {!! HTML::script('/js/template/jquery.nouislider.js')!!}
    {!! HTML::script('/vendor/clockpicker/bootstrap-clockpicker.min.js') !!}
	{!! HTML::script('/js/components/clockpicker.min.js') !!}
    <style>
	.clockpicker-popover{
		z-index:100000;
	}
	</style>
    
	<?php
	//tooltipster
	if($week_no!=0){?>
	<script>
		$(document).ready(function(){
			
			$('#week_<?=$week_no?>').css("background-color", "rgb(0,0,0)");
            $('#week_<?=$week_no?>').tooltipster({
            	content: $('<strong>The task is added in this week</strong>')
            });   
		   // then immediately show the tooltip
		   $('#week_<?=$week_no?>').tooltipster('show');		   
		   // as soon as a key is pressed on the keyboard, hide the tooltip.
		   $(window).click(function() {
		      $('#week_<?=$week_no?>').tooltipster('hide');
		   });
		});
		
	</script>
	<?php } ?>
	<script>
	
		function load_task_detail(type,id)
	    {
	    	var page = "{!!URL::route('load_task_detail')!!}";
	    	$.ajax({
	            url:page,
	            type:'post',
	            data: '&id='+id+'&type='+type,
	            success: function(r){
					$('#task_detail').html(r);
	            }
	        });
	    }
	    function load_task_detail1(type,id)
	    {
	    	var page = "{!!URL::route('load_task_detail1')!!}";
	    	$.ajax({
	            url:page,
	            type:'post',
	            data: '&id='+id+'&type='+type,
	            success: function(r){
					$('#task_detail').html(r);
	            }
	        });
	    }
	    function loadModal(id)
	    {   
	    	$('#pop_content1').html('Loading...');
	        $.ajax({
	            url     : "{!!URL::route('loadModal')!!}",
	            type    : "post",
	            data    : "&id="+id+"&main="+0,
	            success : function(result){
	                $('#pop_content1').html(result);
	            }
	        });
	    }
	    function loadModal_month(month,year)
	    {   
	    	$('#pop_content2').html('Loading...');
	        $.ajax({
	            url     : "{!!URL::route('loadModal_month')!!}",
	            type    : "post",
	            data    : "&id="+month+'&year='+year,
	            success : function(result){
	                $('#pop_content2').html(result);
	            }
	        });
	    }
	    //change start and end date
	    function changeDate()
	   {
	        var page = "{!!URL::route('changeTaskDate')!!}";
	        //get task id
	        var task_id = $('#task_id_modal').val();
	
	        $.ajax({
	            url: page,
	            type: 'post',
	            data: $('#change_modal_frm').serialize(),
	            dataType:'HTML',
	            success: function(response)
	          	{
	          		if($('#type').val()=='sub_task_detail')
	          		{
	          			var parent_id = $('#parent_id').val();
	          			load_task_detail('task_detail',parent_id);
	          		}
	          		else
	          		{
	                	$("#label_updated_"+task_id).html(response);
	                	$('.close').click();
	                }
	            }
	        });
	
	        
	    }
	    //insert via ajax
	    function saveNewTask(e,div,task_group,parent_task,id)
	    {   
	    	
	        var page = "{!!URL::route('saveNewTask')!!}";
	
	        var title = $('#'+id).val();
	
	        if (e.keyCode == 13 && title != '') 
	        {
	        	e.preventDefault();
	        	
	            $.ajax({
	                url: page,
	                type: 'post',
	                data: '&title='+title+'&task_group='+task_group+'&parent='+parent_task,
	                dataType:'HTML',
	                success: function(response)
	            	{
	                    $('#'+div).append(response);
	                    $('#'+id).val('');
	                }
	            });
	        }
	       
	
	    }
	    function load_task_date(div,id)
	   {
	    	var page = "{!!URL::route('load_task_date')!!}";
	    	$.ajax({
	            url:page,
	            type:'post',
	            data: '&id='+id+'&type='+div,
	            success: function(r){
					$('#'+div).html(r);
	            }
	        });
	    }
	    function load_task_date_sub(type,id)
	   {
	    	var page = "{!!URL::route('load_task_date')!!}";
	    	$.ajax({
	            url:page,
	            type:'post',
	            data: '&id='+id+'&type='+type,
	            success: function(r){
					$('#task_detail').html(r);
	            }
	        });
	    }
	    function load_task_assignee_sub(id)
	   {
	    	var page = "{!!URL::route('load_task_assignee')!!}";
	    	$.ajax({
	            url:page,
	            type:'post',
	            data: '&id='+id+'&type=sub_task_detail',
	            success: function(r){
					$('#task_detail').html(r);
	            }
	        });
	    }
	    
	</script>
    <title>All Tasks</title>
@stop

@section("content")
<div class="row">
@if(Session::has('success'))
	<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif
<div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix col-md-12">
            	<!-- year scroll -->
            	@if(isMiladiDate())
            	<div class="icon-box pull-left">
	           		<?php if($year == date('Y')){$w = 1;}else{$w = date('W');} ?>
					<a href="{!!URL::route('get_allTasks',array($w,$mode,$year-1))!!}" onclick="change_year('left')" class="btn pull-left">
						<i class="icon ml-chevron_left"></i>
					</a>
					<h4 class="year pull-left" id="year_value">{!!$year!!}</h4>
					<a href="{!!URL::route('get_allTasks',array($w,$mode,$year+1))!!}" onclick="change_year('right')" class="btn pull-right">
						<i class="icon ml-chevron_right"></i>
					</a>
				</div>
				
                <div class="btn-group pull-right col-md-8">
					<label class="btn btn-primary btn-sm  <?php if($mode == 'all'){echo 'active';}?>">
						<span onclick="location.href='{!!URL::route('get_allTasks',array($week,'all',$year))!!}'"> {!!_('all_tasks')!!}</span>
					</label>
					<label class="btn btn-primary btn-sm <?php if($mode == 'my'){echo 'active';}?>">
						<span onclick="location.href='{!!URL::route('get_myTasks',array($week,$year))!!}'"> {!!_('my_tasks')!!}</span>
					</label>
					<label class="btn btn-primary btn-sm <?php if($mode == 'out'){echo 'active';}?>">
						<span onclick="location.href='{!!URL::route('get_outTasks',array($week,$year))!!}'"> {!!_('outgoing_tasks')!!}</span>
					</label>
				</div>           		
                <table  id="tbl_weeks">
                	<thead><tr><th colspan="{!!date('W')!!}">&nbsp;</th></tr></thead>
                	
                	<tr id="tr_weeks">
                		<?php $i=1?>
                		@for($m=1;$m<=12;$m++)
                			@for($d=1;$d<=weeks_in_month($year,$m);$d++)
                			<td onclick="location.href='{!!URL::route('getReportWeek',array($i,$mode,$year))!!}'" id="week_{!!$i!!}" @if($i == $week)class="active"@elseif(has_task($year,$i,$mode) && $i != $week)class="has_task"@endif>{!!$d!!}</td>
                			<?php $i++?>
                			@endfor
                		@endfor
                	</tr>
                	<tr id="tr_months">
                		@for($m=1;$m<=12;$m++)
                		<td colspan="{!!weeks_in_month($year,$m)!!}" onclick="loadModal_month({!!$m!!},{!!$year!!})" class="progress md-trigger" data-target="#month_modal" data-toggle="modal" style="cursor: pointer;">{!!date('M', mktime(0, 0, 0, $m,1,$year))!!}</td>

                		@endfor
                	</tr>
                </table>
            	@else
            	<div class="icon-box pull-left">
	           		<?php 
	           		$shamsi_year = dateToShamsi($year,date('m'),date('d'));
	           		$shamsi_year = explode('-',$shamsi_year);
	           		if($year != date('Y'))
	           		{//selected week for other year than currect year is first week
	           			$w = 1;
	           		}
	           		else
	           		{
	           			if($week == date('W'))
	           			{//get current week selected
	           				$w = getCurrentShamsiWeek($shamsi_year[0]);
	           			}
	           			else{$w = $week;}
	           		}
	           		?>
					<a href="{!!URL::route('get_allTasks',array($week,$mode,$year-1))!!}" onclick="change_year('left')" class="btn pull-left">
						<i class="icon ml-chevron_left"></i>
					</a>
					<h4 class="year pull-left" id="year_value">{!!$shamsi_year[0]!!}</h4>
					<a href="{!!URL::route('get_allTasks',array($week,$mode,$year+1))!!}" onclick="change_year('right')" class="btn pull-right">
						<i class="icon ml-chevron_right"></i>
					</a>
				</div>
				<div class="btn-group pull-right col-md-8">
					<label class="btn btn-primary btn-sm  <?php if($mode == 'all'){echo 'active';}?>">
						<span onclick="location.href='{!!URL::route('get_allTasks',array($week,'all',$year))!!}'">All Tasks</span>
					</label>
					<label class="btn btn-primary btn-sm <?php if($mode == 'my'){echo 'active';}?>">
						<span onclick="location.href='{!!URL::route('get_myTasks',array($week,$year))!!}'">Assigned to me</span>
					</label>
					<label class="btn btn-primary btn-sm <?php if($mode == 'out'){echo 'active';}?>">
						<span onclick="location.href='{!!URL::route('get_outTasks',array($week,$year))!!}'">Assigned by me</span>
					</label>
				</div>           		
                <table  id="tbl_weeks" style="direction:rtl">
                	<thead><tr><th colspan="{!!date('W')!!}">&nbsp;</th></tr></thead>
                	
                	<tr id="tr_weeks">
                		<?php $i=1?>
                		@for($m=1;$m<=12;$m++)
                			@for($d=1;$d<=weeks_in_month_shamsi($shamsi_year[0],$m);$d++)
	                			<td onclick="location.href='{!!URL::route('getReportWeek',array($i,$mode,$year))!!}'" id="week_{!!$i!!}" @if($i == $w)class="active"@elseif(has_task($year,$i,$mode) && $i != $week)class="has_task"@endif>{!!$d!!}
	                			</td>
	                			<?php $i++?>
                			@endfor
                		@endfor
                	</tr>
                	<tr id="tr_months">
                		<?php 
                		$shamsi_months = array('1'=>'حمل',
                								 '2'=>'ثور',
                								 '3'=>'جوزا',
                								 '4'=>'سرطان',
                								 '5'=>'اسد',
                								 '6'=>'سنبله',
                								 '7'=>'میزان',
                								 '8'=>'عقرب',
                								 '9'=>'قوس',
                								 '10'=>'جدی',
                								 '11'=>'دلو',
                								 '12'=>'حوت'); 
                		?>
                		@for($m=1;$m<=12;$m++)
                		<td colspan="{!!weeks_in_month_shamsi($shamsi_year[0],$m)!!}" onclick="loadModal_month({!!$m!!},{!!$shamsi_year[0]!!})" class="progress md-trigger" data-target="#month_modal" data-toggle="modal" style="cursor: pointer;">{!!$shamsi_months[$m]!!}</td>
                		@endfor
                	</tr>
                </table>
	           	
                @endif
            </header>              
            <div class="main-box-body clearfix">         
                <!-- Task list start -->
                <div class="col-md-12 list-task" id="main_task_view">                    
                    <div id="task_items">
                        <ul>
                            {!!getTaskTree($mode,$week,$year)!!}
                        </ul>
                    </div>
                </div>                 
            </div>
        </div>    
    </div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="task_detail" aria-hidden="true" aria-labelledby="task_detail" role="dialog" tabindex="-1"></div>
<!-- Change date modal start -->
<div class="modal fade modal-fade-in-scale-up" id="change_date_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">×</span>
	      </button>
	      <h4 class="modal-title">{!!_('change_date')!!}</h4>
	    </div>
	    <div class="modal-body">
	      <form role="form" class='form-horizontal' id="change_modal_frm" name="change_modal_frm" style="padding-right:10px;">
	            <input type="hidden" name="task_id" id="task_id_modal" value="0">              
	            <div class="form-group">
	                <label class="col-sm-2 control-label">{!!_('start_date')!!} :</label>
	                <div class="col-sm-4">
	                    <input class="form-control datepicker_farsi" type="text" name="start_date" id="start_date_modal" data-placement="bottom">
	                </div>
	                <label class="col-sm-2 control-label">{!!_('end_date')!!} :</label>
	                <div class="col-sm-4">
	                    <input class="form-control datepicker_farsi" type="text" name="end_date" id="end_date_modal" data-placement="bottom">
	                </div>
	            </div>              
	      </form>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default margin-0" data-dismiss="modal">{!!_('close')!!}</button>
	      <button onclick='changeDate();' type="button" class="btn btn-primary modal-submit">{!!_('update')!!}</button>
	    </div>
	  </div>
	</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="progress_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog">
	    <div class="modal-content">
	        <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        <span aria-hidden="true">×</span>
		      </button>
		      <h4 class="modal-title">{!!_('update_task_progress')!!}</h4>
		    </div>
	        <div class="modal-body" id="pop_content1">
	        </div>
	        <div class="modal-footer" id='create_footer'>
	            <button onclick='postProgress()' id="progress_button_pop" disabled type="button" class="btn btn-primary modal-submit">{!!_('update')!!}</button>
	        </div>
	    </div>
	</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="month_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog">
	    <div class="modal-content">
	        <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        <span aria-hidden="true">×</span>
		      </button>
		      
		    </div>
	        <div class="modal-body" id="pop_content2">
	        </div>
	        
	    </div>
	</div>
</div>
<div class="md-overlay"></div><!-- the overlay element -->
@stop


