@extends('layouts.master')

@section('head')
{{ HTML::style('/css/template/libs/nifty-component.css') }}
{{ HTML::style('/css/template/libs/morris.css') }}
{{ HTML::style('/css/template/libs/jquery.nouislider.css') }}
<style type="text/css">

.project-img-owner {
border-radius: 50%;
background-clip: padding-box;
display: block;
float: left;
height: 28px;
padding: 3px;
overflow: hidden;
width: 28px;
}
img {
vertical-align: middle;
}
img {
border: 0;
}

.todo-table {
	wi/dth:420px;
}

.todo-li {
	vertical-align: middle;
}
.todo-icon {
	width:38px;
	margin-bottom: -4px;
	display: block;
}
.todo-warning{
	width:38px;
	display: inline-block;
	margin-bottom: 9px;
	color: red;
}
.todo-approval {
	width:38px;
	display: inline-block;
}
.todo-title {
	width:100%;
	max-width: 250px;
	overflow: hidden;
	white-space: nowrap;
}
.todo-photos {
	display:flex;
}

.todo-title div {
	height:20px;
	width: 100%;
	overflow: hidden;
    text-overflow:ellipsis;
    white-space: nowrap;
}
.todo-li a{
	color: #000;
	text-decoration: none;
}
.red a{
	color: red;
	text-decoration: none;
	font-weight: bold;
}

.todo-photos .label {
height: 16px;
	margin-top: 6px;
	margin-left:6px;
font-family: 'Open Sans', sans-serif;
font-size: 10.71875px;
font-weight: 600;
line-height: 10.71875px;
}
.charts input{
	font-size: 13px !important;
    height: 14px !important;
    font-weight: normal !important;
}
body.rtl .charts input{
    font-size: 10px !important;
    height: 12px !important;
    position: absolute;
    right: 40px
}

</style>
<title>Task list</title>
@stop

@section('content')
<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<ol class="breadcrumb pull-left">
			<li><a href="#">Dashboard</a></li>
			<li><a href="{{URL::route('getReport')}}">Tasks</a></li>
		</ol>
		<h1>Task<small>List</small></h1>
	</div>
</div>
@if(Session::has('success'))
	<div class='alert alert-success span6'>{{Session::get('success')}}</div>

@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{{Session::get('fail')}}</div>
@endif

<div class="row">		

	
	<div class="col-lg-6">
		<div class="main-box clearfix">
			<header class="main-box-header clearfix">
				<h2 class="pull-left">My Tasks</h2>
				
			</header>
			<div class="main-box-body clearfix">
				<ul class="widget-todo">
					@foreach($assigned_to_me AS $item)
										
						<li class="clearfix todo-li">
						<a href="{{URL::route('ViewReport', $item->id)}}">
							<table class="todo-table">
								
								<tr>
									@if($item->status==0 && Auth::user()->id != $item->user_id)
									<td class="todo-approval">
										<a href="#" onclick="approveTask({{$item->id}})" style='color:green;'  title="Approve Taks"><i class="fa fa-check-circle fa-lg"></i></a><br>
										<a href="#" style='color:#e84e40;' data-modal="approval_modal" class='md-trigger' onclick='loadApprovalModal("{{$item->id}}")' title="Reject Task"><i class="fa fa-times-circle fa-lg"></i></a>
									</td>
									@else
									<td class="todo-icon">
										
										<?php $page = URL::route('loadModal'); ?>
                                        <!-- <a href="javascript:void();" data-modal="progress_modal" class='md-trigger' onclick='loadModal("{{$item->id}}","{{$page}}");'> -->
										<div class='charts'>
		                                    <input class="knob" data-readOnly=true data-width="32" data-height='32' data-min="0" data-fgColor="#03a9f4"
		                                    value="{{getMainTaskProgress($item->id)}}">
		                                </div>
			                            <!-- </a> -->
									</td>
									@endif
									<td class="todo-title">
										<div>
											{{$item->title}}
										</div>
									</td>
									<td class="todo-photos">
										<?php 
                                            //get task assignees
                                            $assignees = getTaskAssignees($item->id);
                                            foreach($assignees AS $a_item)
                                            {
                                                $photo = getProfilePicture($a_item->assigned_to);
                                                $tooltip = getUserFullName($a_item->assigned_to);
                                        ?>
                                                {{ HTML::image('/img/'.$photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip')) }}
                                        <?php   
                                            }
                                        ?>

                                        <?php 
                                        
                                        $y = date("Y");
                                        $m = date("m");
                                        $d = date("d");
                                        $todayDate = dateToShamsi($y,$m,$d);

                                        $d1 = date_create($todayDate);
                                        $d2 = date_create($item->end_date);
                                        
                                        $deedline=date_diff($d1,$d2);
                                        //$deedline->format("%R%a days");
                                        $deedline = $deedline->format("%R%a");
                                        $label_status = "";
                                        if($deedline>=3)
                                        {
                                            $label_status = "label-success";
                                        }
                                        else if($deedline < 3 && $deedline >= 1)
                                        {
                                            $label_status = "label-warning";
                                        }
                                        else
                                        {
                                            $label_status = "label-danger";
                                        }

                                    ?>
                                    <div class="label  {{$label_status}}">{{$item->end_date}}</div>
									</td>
								</tr>
							</table>
						</a>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="main-box clearfix">
			<header class="main-box-header clearfix">
				<h2 class="pull-left">Outgoing Tasks</h2>
				<div class="pull-right">
					<a href="{{URL::route('getReportCreate')}}" class="btn btn-primary">
						<i class="fa fa-plus-circle fa-lg"></i>
						New task
					</a>
				</div>
			</header>
			<div class="main-box-body clearfix">
				<ul class="widget-todo">
					@foreach($reports AS $item)
						@if(count(getTaskComments($item->id))>0)
						<li class="clearfix todo-li">
						@else
						<li class="clearfix todo-li">
						@endif
						<a href="{{URL::route('ViewReport', $item->id)}}">
							<table class="todo-table">
								<tr>
									@if(count(getTaskRejectedComments($item->id))>0)
									<td class="todo-warning">
										<i class="fa fa-warning fa-lg" title="Task Rejected"></i>
									</td>
									@else
									<td class="todo-icon">
										<div class='charts'>
		                                    <input class="knob" data-readOnly=true data-width="32" data-height='32' data-min="0" data-fgColor="#03a9f4"
		                                    value="{{getMainTaskProgress($item->id)}}">
		                                </div>
									</td>
									@endif
									<td class="todo-title">

										<div>
											{{$item->title}}
										</div>
									</td>
									<td class="todo-photos">
										<?php 
                                            //get task assignees
                                            $assignees = getTaskAssignees($item->id);
                                            foreach($assignees AS $a_item)
                                            {
                                                $photo = getProfilePicture($a_item->assigned_to);
                                                $tooltip = getUserFullName($a_item->assigned_to);
                                        ?>
                                                {{ HTML::image('/img/'.$photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip')) }}
                                        <?php   
                                            }
                                        ?>

                                        <?php 
                                        
                                        $y = date("Y");
                                        $m = date("m");
                                        $d = date("d");
                                        $todayDate = dateToShamsi($y,$m,$d);

                                        $d1 = date_create($todayDate);
                                        $d2 = date_create($item->end_date);
                                        
                                        $deedline=date_diff($d1,$d2);
                                        //$deedline->format("%R%a days");
                                        $deedline = $deedline->format("%R%a");
                                        $label_status = "";
                                        if($deedline>=3)
                                        {
                                            $label_status = "label-success";
                                        }
                                        else if($deedline < 3 && $deedline >= 1)
                                        {
                                            $label_status = "label-warning";
                                        }
                                        else
                                        {
                                            $label_status = "label-danger";
                                        }

                                    ?>
                                    <div class="label  {{$label_status}}">{{$item->end_date}}</div>
									</td>
								</tr>
							</table>
						</a>
						</li>
					@endforeach
				</ul>
					
			</div>
		</div>
	</div>
	
</div>

<div class="md-modal md-effect-7" id="approval_modal">
    <div class="md-content">
        <div class="modal-header">
            <button class="md-close close">&times;</button>
            <h4 class="modal-title">Task Approval: <font color='red'>Reject</font></h4>
        </div>
        <div class="modal-body" id="content1">
        </div>
        <div class="modal-footer" id='create_footer'>
            <button onclick='postRejection()' type="button" class="btn btn-danger modal-submit">Reject</button>
        </div>
    </div>
</div>
<div class="md-modal md-effect-7" id="progress_modal">
    <div class="md-content">
        <div class="modal-header">
            <button class="md-close close">&times;</button>
            <h4 class="modal-title">Change Task Progress</h4>
        </div>
        <div class="modal-body" id="pop_content1">
        </div>
        <div class="modal-footer" id='create_footer'>
            <button onclick='postProgress()' type="button" class="btn btn-primary modal-submit">Save changes</button>
        </div>
    </div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->

@stop
@section('footer-scripts')
{{ HTML::script('/js/template/modernizr.custom.js')}}
{{ HTML::script('/js/template/classie.js')}}
{{ HTML::script('/js/template/modalEffects.js')}}

{{ HTML::script('/js/template/jquery.knob.js')}}
{{ HTML::script('/js/template/raphael-min.js')}}
{{ HTML::script('/js/template/tmorris.js')}}

{{ HTML::script('/js/template/jquery.nouislider.js')}}

<script type="text/javascript">
function approveTask(id)
{   
	var confirmed = confirm("Want to approve?");
	if(confirmed)
	{
	    $.ajax({
	        url     : "{{URL::route('approveTask')}}",
	        type    : "post",
	        data    : "&task_id="+id,
	        success : function(result){
	            //$('#sub_task_div').html(result);
	            location.reload();
	        },
	        error  : function( xhr, err ){
	            alert(err);     
	        }
	    }); 
	}   
}

function loadModal(id,page)
    {   
        $.ajax({
            url     : page,
            type    : "post",
            data    : "&id="+id+"&main="+1,
            success : function(result){
                $('#pop_content1').html(result);
            },
            error : function( xhr, err ){
                alert(err);     
            }
        });
    }


/* KNOB CHART LIBRARY */
		
		$(".knob").knob({
			change : function (value) {
				//console.log("change : " + value);
			},
			release : function (value) {
				//console.log(this.$.attr('value'));
				console.log("release : " + value);
			},
			cancel : function () {
				console.log("cancel : ", this);
			},
			draw : function () {
				// "tron" case
				if(this.$.data('skin') == 'tron') {

					var a = this.angle(this.cv)  // Angle
						, sa = this.startAngle		  // Previous start angle
						, sat = this.startAngle		 // Start angle
						, ea							// Previous end angle
						, eat = sat + a				 // End angle
						, r = 1;

					this.g.lineWidth = this.lineWidth;

					this.o.cursor
						&& (sat = eat - 0.3)
						&& (eat = eat + 0.3);

					if (this.o.displayPrevious) {
						ea = this.startAngle + this.angle(this.v);
						this.o.cursor
							&& (sa = ea - 0.3)
							&& (ea = ea + 0.3);
						this.g.beginPath();
						this.g.strokeStyle = this.pColor;
						this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
						this.g.stroke();
					}

					this.g.beginPath();
					this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
					this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
					this.g.stroke();

					this.g.lineWidth = 2;
					this.g.beginPath();
					this.g.strokeStyle = this.o.fgColor;
					this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
					this.g.stroke();

					return false;
				}
			}
		});
function loadApprovalModal(id)
{   
    $.ajax({
        url     : "{{URL::route('loadApprovalModal')}}",
        type    : "post",
        data    : "&id="+id,
        success : function(result){
            $('#content1').html(result);
        },
        error : function( xhr, err ){
            alert(err);     
        }
    });
}
function postRejection()
{   
    $.ajax({
        url     : "{{URL::route('postSaveApproval')}}",
        type    : "post",
        data    : $("form.form-horizontal").serialize(),
        success : function(result){
            //$('#sub_task_div').html(result);
            location.reload();
        },
        error  : function( xhr, err ){
            alert(err);     
        }
    });    
    return false;
}



</script>
@stop


