
<style>
	#ui-datepicker-div{
		z-index: 100000 !important;
	}
	.select2-dropdown{
		z-index: 100000;
	}
	.no-padding {
		padding:0px;
	}
	.panel-body{
		padding-top:0px;
	}
	.input-group-addon{
		width:100px;
	}
	
	
</style>

<?php if(!isTaskGroupInReport($details->task_group_id)){ ?>
<script>
	$('.hider').hide();
	
</script>
<?php } ?>
{!! HTML::style('/css/conversation.css') !!}

@if($details->user_id != Auth::user()->id && !isAdmin())
	<script type="text/javascript">
		$("#all_inputs :input").prop("disabled", true);
		$("#all_buttons :input").prop("disabled", false);
	</script>
@endif
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">
			@if($type=='task_detail')
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			@else
				<button type="button" class="btn btn-danger" onclick="load_task_detail('task_detail',{!!$details->parent_task!!})">Back</button>
			@endif
		</div>
		<form id="form_edit" method="post" action="{!!URL::route('updateReportViaAjax')!!}" enctype="multipart/form-data">
	    	<input type="hidden" name="task_id" value="{!!Crypt::encrypt($details->id)!!}">
	    	<input type="hidden" id="current_week" value="{!!Request::segment(3)!!}" />
			<div class="modal-body">
				
				<div class="panel-body container-fluid" style="margin-bottom:-30px">
					<div class="row row-lg">
						<div id="all_inputs">
							<div class="form-group col-md-8">
								<div class="form-group input-group col-md-12">
									<span class="input-group-addon">عنوان</span>
				                    <input type="text" required="required" class="form-control" placeholder="عنوان" name="title" value="{!!$details->title!!}">     
				                </div>		
				                
								<div class="form-group input-group col-md-12">
									<span class="input-group-addon">تشریحات</span>
									<textarea class="form-control" required="required" placeholder='تشریحات' rows="4" cols="40" name="description">{!!$details->description!!}</textarea>
								</div>
								
								
				                <div class="input-group form-group col-md-12 ">
				               		<div class="col-md-10 select2-primary no-padding">
				                		<div class="input-group">
											<span class="input-group-addon">Assign to</span>						
											
							                <div id="users_div1">
							                    <select name='assign_with1[]' required="required" id='assign_with2' style="width:100%;" multiple="multiple" data-plugin="select2">
							                        
							                        <?php 
							                            foreach($users AS $uitem)
							                            {
							                                $selected = "";
							                                if(in_array($uitem->id, $assignees)){$selected='selected';}
							                                echo "<option ".$selected." value='".$uitem->id."'>".$uitem->username."</option>";
							                            }
							                        ?>
							                    </select>
							                </div> 
					               		</div>
					                </div>
					           		<div class="col-md-2 form-group">
								
										<div class="form-group col-md-6">
											<div class="input-group col-md-12">
												<div class="checkbox-custom checkbox-primary checkbox-inline">
				                                    <input value='1' id='by_email' name="by_email" type="checkbox" @if($details->notify_by_email == 1) checked @endif>
				                                    <label for="by_email">
				                                        Email
				                                    </label>
				                                </div>
			                                </div>
										</div>
									
																			
									</div>
						            
				             	</div>  
				             	<div class="col-md-12 input-group form-group">
				             		@if(count($files)>0)
										@foreach($files AS $file)
										<div id="div_file_{!!$file->id!!}">
											<a href="{!!route('downloadAttachmentTasks',$file->file_name)!!}">{!!$file->file_name!!}</a>&nbsp;@if($details->user_id == Auth::user()->id || isAdmin())<span style="color: red;cursor: pointer;" title="{!!_('delete_the_file')!!}" onclick="if(confirm('Are you sure?')){delete_attachment('{!!$file->id!!}','{!!$file->file_name!!}')}">X</span>@endif
										</div>
										@endforeach								
									@endif										
				             	</div>						             	
							</div>
							<div class="col-md-4">
								<div class="form-group input-group col-md-12">
									<?php 
                                    $page = route('isTaskGroupInReport_it');
                                    ?>
                                    <select class="form-control" name='task_group' id="task_group" onchange="isTaskGroupInReport1('{!!$page!!}',this.value)">
                                        
                                        @foreach($taskGroup AS $item)
                                        	@if($item->id == $details->task_group_id)
                                            <option value='{!!$item->id!!}' selected="selected">{!!$item->title!!}</option>
                                            @else
                                            <option value='{!!$item->id!!}'>{!!$item->title!!}</option>
                                            @endif
                                        @endforeach
                                    </select>
								</div>
							</div>
							<div class="col-md-4 input-group form-group">
								
								<span class="input-group-addon">تاریخ شروع</span>
							</div>
							<div class="col-md-4 input-group form-group">
								<div class="col-md-6 ">
									@if(isMiladiDate())
	                                <input value="<?php echo ($details->start_date != '0000-00-00') ? $details->start_date:''; ?>" type="text" name="start_date" data-placement="bottom" readonly="readonly" class="form-control datepicker">
	                            	@else
									<?php $sdate = $details->start_date; if($sdate!='0000-00-00'){$s_date = explode('-', $details->start_date);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                					<input value='<?php if($details->start_date!="0000-00-00"){echo jalali_format($sdate);}?>' placeholder="{!!_('start_date')!!}" class="form-control" type="text" name="start_date" data-placement="bottom" readonly="readonly">
									@endif
								</div>
								<div class="col-md-6">
									<?php
									if($details->time_in==null)
									{
									$time_in = explode(' ', $details->created_at);
									$time_in = explode(':', $time_in[1]); 
									$time_in = $time_in[0].':'.$time_in[1];
									}
									else
									{
										$time_in = $details->time_in;
									}
									?>
									<div class="input-group">
										<span class="input-group-addon" style="width: 35%">
										  <span class="wb-time"></span>
										</span>
										<input type="text" name="time_in" id="time_in" value="{!!$time_in!!}" class="form-control" readonly="readonly" data-plugin="clockpicker" data-autoclose="true">
									</div>
						                
						        </div>
								
								
							</div>
							<div class="col-md-4 input-group form-group">
								
							
									<span class="input-group-addon">تاریخ ختم</span>
							</div>
							<div class="col-md-4 input-group form-group">
								<div class="col-md-6 ">
									<?php $edate = $details->end_date; if($edate!='0000-00-00'){$e_date = explode('-', $details->end_date);$edate = dateToShamsi($e_date[0], $e_date[1], $e_date[2]);}?>
									@if(isMiladiDate())
	                                <input value="<?php echo ($details->end_date != '0000-00-00') ? $details->end_date:''; ?>" type="text" name="end_date" data-placement="bottom" readonly="readonly" class="form-control datepicker">
	                            	@else
									<input value='<?php if($details->end_date!="0000-00-00"){echo jalali_format($edate);}?>' placeholder="{!!_('end_date')!!}" class="form-control datepicker_farsi" type="text" name="end_date" id="end_date" data-placement="bottom" readonly="readonly">
            						@endif
            					</div>
            					<div class="col-md-6 ">
            					@if($last_progress)
            						<?php $time_out = explode(' ', $last_progress->created_at); $time_out = $time_out[1]; ?>
            					@else
            						<?php $time_out = 'تحت کار';?>            					
            					@endif
            						<div class="input-group">
						                    <span class="input-group-addon" style="width: 35%">
						                      <span class="wb-time"></span>
						                    </span>
						                    <input readonly="readonly" type="text" name="time_out" value="{!!$time_out!!}" class="form-control" data-plugin="clockpicker" data-autoclose="true">
						            </div>
            					</div>
            						<input type="hidden" id="current_enddate" value="<?=jalali_format($edate)?>"/>
            						<input type="hidden" id="enddate" value="<?=$details->end_date?>" />
								
							</div>
							
							
							<div class="form-group col-md-8">
								<div id="all_files">
									<div id="files_1">
										<div class="col-md-11">
											<input class="form-control" type="file" name="files[]">
											<input type="hidden" id="total_files" value="1"/>
										</div>
										<div class="col-md-1">
											<i class="icon wb-plus-circle" aria-hidden="true" style="font-size: 25px;cursor: pointer;" onclick="add_file()"></i>
										</div>	
									</div>									
								</div>	
							</div>	
							
							
							<div class="col-md-4 form-group" id="all_buttons">
						        <div class="col-sm-12">
						        	@if(($details->user_id == Auth::user()->id || isAdmin()) && (checkTaskApprovalForAssigner($details->id) == 0) || $details->description=='')
							            <button class="btn btn-primary btn-xs" type="submit"><i class="fa fa-refresh fa-lg"></i> ثبت تغییرات</button>
							            <a href="{!!URL::route('DeleteReport', $details->id)!!}" class="btn btn-danger btn-xs" type="button" onclick="javascript:return confirm('Are you sure you want to DELETE this report and its subtasks ? THERE IS NO UNDO')"><i class="fa fa-trash-o fa-lg"></i> {!!_('delete')!!}</a>
							            
						            @endif
									@if(get_task_status($details->id) == 0 && is_user_assigned_to_task($details->id))													           
							            <button class="btn btn-primary btn-xs" type="button" onclick="if(confirm('Once you approve the task, you cant UNDO it. Are you sure?')){task_approve('{!!$details->id!!}')}"><i class="fa fa-check fa-lg"></i> @if(get_task_status_it($details->id) == 1) {!!_('approved')!!} @else {!!_('approve')!!} @endif</button>
							            <button class="btn btn-danger btn-xs" type="button" onclick="$('#all_buttons').hide();$('#reject_reason').show()"><i class="fa fa-times fa-lg"></i> {!!_('reject')!!}</button>						          	
						           @endif				           
						        </div>				    
							</div>
							
						</div>
					
					    <div id="reject_reason" style="display: none">
					    	<div class="form-group col-md-8">
								<label>{!!_('reason_to_reject')!!}</label>
								<div class="input-group col-md-12">
									<textarea class="form-control" placeholder="{!!_('reason_to_reject')!!}" rows="5" cols="40" id="reason"></textarea>										
								</div>
							</div>
							<div class="form-group col-md-4">
								<label>&nbsp;</label>
								<div class="col-md-12">
									<button class="btn btn-danger" type="button" onclick="task_reject('{!!$details->id!!}');"><i class="fa fa-times fa-lg"></i> {!!_('reject')!!}</button>
									<button class="btn btn-primary" type="button" onclick="$('#reject_reason').hide();$('#all_buttons').show()"><i class="fa fa-times fa-lg"></i> {!!_('cancel')!!}</button>
								</div>
							</div>								
					    </div>
					</div>
				</div>	
			</div>
		{!! Form::token() !!}
		</form>
		@if($details->description!='' && checkTaskApprovalForAssigner($details->id) == 1)
		<div class="form-group col-md-12" >                        
            <!-- Sub task start -->
		    <fieldset>
		        <div class="list-task" style="border:1px solid;margin-left:20px;margin-right:40px;">
		            <ul style="margin:0 0 0 0px;">					                
		                <div id="task_id_{!!$details->id!!}">
		                    @foreach($sub_tasks AS $item)
		                    <?php 
		                        if($item->end_date != '0000-00-00')
		                        {
		                            $end_date = $item->end_date;	
		                            //convert shamsi to meladi
		                            //$end_date = explode("-", $item->end_date);
		                         	//$end_date = dateToMiladi($end_date[0],$end_date[1],$end_date[2]);
		                     	}
		                        else
		                        {
		                            $end_date = date('Y-m-d');
		                   	}
		                        
		                        $from=date_create($end_date);
	                        	$to=date_create(date('Y-m-d'));
	                        
	                        	$diff=date_diff($to,$from);
	                        
	                        	$deadline = $diff->format('%R%a');
	                        	$d=$deadline;
	
	                        	$label_status = "";
	                        	$days;
		
		                        switch (true) {
		                            case ($d < 0):
		                                $days='Due';
		                                break;
		                            case ($d == 0):
		                                $days='Today';
		                                break;
		                            case ($d == 1):
		                                $days='Tomorrow';
		                                break;
		                            case ($d > 1 and $d < 7):
		                                $days=abs($d).' Days';
		                                break;
		                            case ($d > 6 and $d < 15):
		                                $days='Next Week';
		                                break;
		                            case ($d > 14 and $d < 22):
		                                $days='Two Weeks';
		                                break;
		                            case ($d > 21 and $d < 29):
		                                $days='Three Weeks';
		                                break;
		                            case ($d > 28 and $d < 56):
		                                $days='Next Month';
		                                break;
		                            case ($d > 55 and $d < 336):
		                                $nm=round($d/30);
		                                $days=$nm.' Months';
		                                break;
		                            case ($d > 335):
		                                $ny=round($d/365);
		                                $days=$ny.' Year(s)';
		                                break;
		                            default:
		                                $days='select date';
		                                break;
		                        }
		
		                        if($d>0)
		                        {
		                            $label_status = "label-success";
		                        }
		                        else if($d == 0)
		                        {
		                            $label_status = "label-warning";
		                        }
		                        else
		                        {
		                            $label_status = "label-danger";
		                        }
		
		                        $s_date = $item->start_date;
								$e_date = $item->end_date;
								if($s_date != '0000-00-00')
								{
									$sdate = explode("-", $s_date);
									$sy = $sdate[0];
									$sm = $sdate[1];
									$sd = $sdate[2];
									$s_date = dateToShamsi($sy,$sm,$sd);		
								}
								if($e_date != '0000-00-00')
								{
									$edate = explode("-", $e_date);
									$ey = $edate[0];
									$em = $edate[1];
									$ed = $edate[2];
									$e_date = dateToShamsi($ey,$em,$ed);		
								}
							//get task progress
		                    if(has_subtask($item->id))
							{
								$progress = getSubTaskProgress($item->id);
							}
							else
							{
								$progress = getMainTaskProgress($item->id);
							}
		                    ?>
		                    <li>
		                        @if($progress == 100)
								<div class="checkbox-custom checkbox-inline">
									<input type="checkbox" checked disabled>
									<label>&nbsp;</label>
								</div>
								@else
								<div class="checkbox-custom task-check checkbox-inline">
									<input type="checkbox" disabled="">
									<label>&nbsp;</label>
								</div>
								@endif
								<?php $status = get_task_status($item->id); ?>
		                        <a href="javascript:void()" onclick="load_task_detail('sub_task_detail',{!!$item->id!!})">
									<div class="task_title big_title">{!!$item->title!!}</div>
								</a>
								@if($item->user_id!=Auth::user()->id && $status != -1)
								<div class="task_title big_title"> 
									@if($status==0)<a href="{!!URL::route('ViewReport',array($item->id))!!}" style="color: #B9B6B6;font-style: italic;font-size: 0.9em">- {!!_('pending')!!} </a>@endif
								</div> 
								@endif
								<?php 
						       	$onclick_date = 'title="'._("You_dont_have_access_to_change_the_date").'" style="cursor:default"';
						       	if(Auth::user()->id == $item->user_id || isAdmin())
								{
									$onclick_date = "onclick=\"load_task_date_sub('sub_task_detail',".$item->id.")\"";
								}
								?>
								<span id="label_{!!$item->id!!}" style="margin-right: 170px;direction:rtl;float: right;position:relative;">
									<a href="javascript:void()" <?=$onclick_date?>>
										<span id="label_updated_{!!$item->id!!}" class="label {!!$label_status!!} date_middle"> {!!$days!!}</span>
									</a>
								</span>     
		                        <div class="pull-right">
		                            <div class="assignees" style="display:inline;">
		                            <?php 
							       	$onclick = 'href="javascript:void()" title="'._("You_dont_have_access_to_change_assignee").'" style="cursor:default"';
							       	if(Auth::user()->id == $item->user_id || isAdmin())
									{
										$onclick = 'href="javascript:void()" onclick="load_task_assignee_sub('.$item->id.')"';
									}
									$assignees = getTaskAssignees($item->id);
									?>
									@if(count($assignees)>0)
										@if(count($assignees)<=3)
											@foreach($assignees AS $a_item)
											<?php
											    $photo = getProfilePicture($a_item->assigned_to);
											    $tooltip = getUserFullName($a_item->assigned_to);
											?>
											<a data-toggle="modal" <?=$onclick?>>
												{!!HTML::image('/img/'.$photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip'));!!}
											</a>
											@endforeach
										@else
											<span id="more_assignee_<?=$item->id?>" style="cursor: pointer" onmouseover="show_tooltipster(<?=$item->id?>,'<?=URL::route("get_assignee_it")?>')">
												{!!HTML::image('/img/more.png', '', array('class' => 'project-img-owner'));!!}
											</span>
											<?php $count = 1; ?>
											@foreach($assignees AS $a_item)
												@if($count <= 2)
													<?php
													    $photo = getProfilePicture($a_item->assigned_to);
													    $tooltip = getUserFullName($a_item->assigned_to);
													?>
													<a data-toggle="modal" <?=$onclick?>>
														{!!HTML::image('/img/'.$photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip'));!!}
													</a>
													<?php $count++;?>
												@endif
											@endforeach
										@endif
			                            
			                        @else
										<a data-toggle="modal" <?=$onclick?>>
											{!!HTML::image('/img/default.jpeg', '', array('class' => 'project-img-owner','data-original-title'=>'New Assignee','data-toggle'=>'tooltip'));!!}
										</a>
									@endif
		                            </div>
		                            <?php
		                            $onclick_progress = 'title="'._("Since_this_task_has_some_sub_tasks,_you_cant_update_progress_directly(").''.$progress.'% '._("completed)").'" class="progress"';
							       	if(!has_subtask($item->id))
									{
										$onclick_progress = 'onclick="loadModal('.$item->id.')" title="'.$progress.'% '._('completed').'" class="progress md-trigger" data-target="#progress_modal" data-toggle="modal" ';
									}
									?>
								    <div <?=$onclick_progress?> style="display:inline-block;">
								        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{!!$progress!!}" aria-valuemin="0" aria-valuemax="100" style="width: {!!$progress!!}%;">
								            <span class="sr-only">{!!$progress!!}% {!!_('complete')!!}</span>
								        </div>
								    </div>
		                            
		                        </div>
		                        {!!getSubTaskTree($item->id)!!}
		                    </li>
		                    @endforeach
		                </div>	
		                @if(!getTaskSummaryDetails($details->id))
		                <div>
		                    <span class="item-plus">+</span>
		                    <input onkeypress="saveNewTask(event,'task_id_{!!$details->id!!}','{!!$details->task_group_id!!}','{!!$details->id!!}','task_title_{!!$details->id!!}');" style="width:600px;border:0px;" placeholder="ایجاد فعالیت فرعی" type="text" name="title_sub" id="task_title_{!!$details->id!!}" />
		                </div>	
		                @endif
		            </ul>
		        </div>
		    </fieldset>
        </div>
        
        <div class="row" id="other_det">
		    <div class="form-group col-md-6" style="border-right: 1px solid #eee;margin-top: -10px">						        
		    	<div class="col-lg-12">	                
                    <header class="main-box-header clearfix">
                        <h2 class="pull-left value red">نظریات</h2>
                    </header>
                    <div class="main-box-body clearfix">
                        <div class="conversation-wrapper">
                        	<?php 
                        	$comments = getTaskComments($details->id); ?>
                            <div class="conversation-content" id="comments_div" @if(count($comments)>3)style="max-height: 220px; overflow-y: scroll;"@endif>
                            	@foreach($comments AS $comment)
                                    <div class="conversation-item item-left clearfix">
                                        <div class="conversation-user">
                                        	<span style="display:inline-block;cursor: pointer;">
												<div class="assignees" style="display:inline;">
													<?php
												    $AssignedBy_photo = getProfilePicture_task($comment->user_id);
												    $tooltip = getUserFullName($comment->user_id);
												    if($AssignedBy_photo)
												    {
													    if (!file_exists('/var/www/html/localnet/public/documents/profile_pictures/small_'.$AssignedBy_photo))
									                    {
										                    $img = Image::make('/var/www/html/localnet/public/documents/profile_pictures/'.$AssignedBy_photo);
									          						// crop image
									          						$img->crop(1278, 1597,897,80)->fit(100,100)->save('/var/www/html/localnet/public/documents/profile_pictures/small_'.$AssignedBy_photo);
									          						//HTML::image('/documents/profile_pictures/'.$photo, Auth::user()->username)
									                    }

													    
														?>
														<a href="javascript:void()" title="{!!$tooltip!!}">
														{!!HTML::image('/documents/profile_pictures/small_'.$AssignedBy_photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip'));!!}
														</a>
													<?php }
									                else
									                { ?>
									                	<a href="javascript:void()" title="{!!$tooltip!!}">
															{!!HTML::image('/img/default.jpeg', '', array('class' => 'project-img-owner'));!!}
														</a>
									                <?php }
									                ?>
												</div>
											</span>
                                        	
                                        </div>
                                        <div class="conversation-body" onclick="showCommentDate(<?=$comment->id?>,'comment')" style="cursor:pointer" title="حزییات بیشتر">			                                			                              
                                            <div class="text">
                                                {!!$comment->comment!!}
                                            </div>
                                        </div>
                                        <div class="time hidden-xs comment_date" style="display: none;margin-left: 60px" id="comment_<?=$comment->id?>">
                                            {!!$comment->created_at!!}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            
                            <div class="conversation-new-message">
                                <form class="comment_form">
                                    <input type="hidden" id="comment_task_id" value="{!!$details->id!!}">
                                    <div class="form-group">
                                        <textarea name="comment" id="comment" class="form-control" rows="2" placeholder="نظر خود را اینجا تایپ نمایید"></textarea>
                                    </div>
                                    
                                    <div class="clearfix">
                                        <button type="button" class="btn btn-success pull-right" onclick='postComment()'>ثبت نظر</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>					                
		        </div>					        
		    </div>			
		    <?php
            $completed = true;$approved = false;
            if(has_subtask($details->id))
            {
            	if(!isMainTaskCompleted_withSubtasks($details->id))
            	{
            		$completed = false;
            	}
            	if(isMainTaskApproved($details->id))
            	{
            		$approved = true;
            	}
            }
            else
            {
            	if(!isTaskPercentage_complete($details->id))
            	{
            		$completed = false;
            	}
            	if(isTaskCompleted($details->id))
            	{
            		$approved = true;
            	}
            }
            ?>				
		    <div class="col-lg-6" style="border-left: 1px solid #eee;margin-top: -10px">
		    	<div class="col-lg-12">					                
                    <header class="main-box-header clearfix">
                        <h2 class="pull-left value red">اجرآت</h2>
                        <span class="pull-right" id="task_completed_alert">
                        	@if(!$approved && $completed && ($details->user_id == Auth::user()->id || isAdmin()))
                                <button type="button" class='btn btn-warning mrg-b-lg' onclick="approveCompletedTask('{!!$details->id!!}','{!!$details->parent_task!!}');"><i class="fa fa-check-circle fa-lg"></i> {!!_('approve_as_completed')!!}</button>
                            @endif
                            
                            
                            @if($approved)
                            	<div class="alert alert-success">
                                    <i class="fa fa-check-circle fa-fw fa-lg"></i>
                                    <strong>{!!_('well_done!')!!}</strong> {!!_('task_approved')!!}
                                </div>
                            @elseif($completed)
                            	<div class="alert alert-success">
                                    <i class="fa fa-check-circle fa-fw fa-lg"></i>
                                    <strong>{!!_('task_completed!')!!}</strong> {!!_('pending_for_approval')!!}
                                </div>
                            @endif
                            
                        </span>
                    </header>
                    <div class="main-box-body clearfix">
                        <div class="conversation-wrapper">
                        	<?php
                        	$summary = getTaskSummaryDetails($details->id);?>
                            <div class="conversation-content" id="progress_summary_div" @if(count($summary)>2)style="max-height: 220px; overflow-y: scroll;"@endif>				                           
                            @foreach($summary AS $summary_item)				                           
                                <div class="conversation-item item-left clearfix">
                                    <div class="conversation-user">
                                    	<?php
									    $AssignedBy_photo = getProfilePicture_task($summary_item->user_id);
									    $tooltip = getUserFullName($summary_item->user_id);
									    if($AssignedBy_photo)
									    {
										    if (!file_exists('/var/www/html/localnet/public/documents/profile_pictures/small_'.$AssignedBy_photo))
						                    {
							                    $img = Image::make('/var/www/html/localnet/public/documents/profile_pictures/'.$AssignedBy_photo);
						          						// crop image
						          						$img->crop(1278, 1597,897,80)->fit(100,100)->save('/var/www/html/localnet/public/documents/profile_pictures/small_'.$AssignedBy_photo);
						          						//HTML::image('/documents/profile_pictures/'.$photo, Auth::user()->username)
						                    }

										    
											?>
											<a href="javascript:void()" title="{!!$tooltip!!}">
											{!!HTML::image('/documents/profile_pictures/small_'.$AssignedBy_photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip'));!!}
											</a>
										<?php }
						                else
						                { ?>
						                	<a href="javascript:void()" title="{!!$tooltip!!}">
												{!!HTML::image('/img/default.jpeg', '', array('class' => 'project-img-owner'));!!}
											</a>
						                <?php }
						                ?>
                                    </div>
                                    <div class="conversation-body" onclick="showCommentDate(<?=$summary_item->id?>,'progress')" style='cursor:pointer'>
                                        <div class="text">
                                            {!!$summary_item->progress_summary!!}
                                            
                                            <div class="progress" title="{!!$summary_item->progress!!}% Completed">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{!!$summary_item->progress!!}" aria-valuemin="0" aria-valuemax="100" style="width: {!!$summary_item->progress!!}%;">
                                                    <span class="sr-only">{!!$summary_item->progress!!}% {!!_('complete')!!}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="time hidden-xs progress_date" style="display: none;margin-left: 60px" id="progress_<?=$summary_item->id?>">
                                        {!!$summary_item->created_at!!}
                                    </div>
                                </div>
                            @endforeach
                            </div>
                            
                            @if(!has_subtask($details->id))
                            	@if(!$completed)
		                            <div class="conversation-new-message">
		                                <form id="progress_chagne_modal">
		                                    <input type="hidden" name="task_id" value="{!!$details->id!!}">
		                                    <div class="form-group">
		                                        <textarea name="progress_summary" id="progress_summary" class="form-control" rows="2" placeholder="تشریحات را اینجا تایپ نمایید"></textarea>
		                                    </div>
		                                    <div class="form-group"	
			                                    <h5><center><span id="progress"></span></center></h5>
											    <div class="slider-basic1" onchange="check_percentage()"></div>
											    <input type="hidden" name="percentage" id="percentage">	
										    </div>			                               
		                                    <div class="clearfix">
		                                        <button type="button" id="progress_button" class="btn btn-warning pull-right" disabled onclick='postProgress()'>ثبت</button>
		                                    </div>
		                                </form>
		                            </div>
		                        @else
		                        	<div class="conversation-new-message">
		                                <div class="clearfix">				                                    	
		                                    {!!_('task_completed')!!}!				                                       
		                                </div>
		                            </div>
		                        @endif
		                    @else
		                    	<div class="alert alert-success">
                                    <i class="fa fa-check-circle fa-fw fa-lg"></i>
                                    <strong>این وظیفه دارای وظیفه فرعی میباشد</strong>
                                </div>
                            @endif
                        </div>
                    </div>						             
		        </div>						      
		    </div>
		</div>
		@endif     
	</div>
</div>

<script type="text/javascript">
function show_tooltipster(id,page)
{
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id,
        success: function(r){
			$('#more_assignee_'+id).tooltipster({
				animation: 'grow',
				//autoClose:false,
				hideOnClick: true,
				trigger: 'click',
				interactive:true,
				onlyOne: true,
				position: 'left',
	        	content: $(r)
	        });
	        // then immediately show the tooltip
			$('#more_assignee_'+id).tooltipster('show');	
			$('#more_assignee_'+id).mouseout(function() {
				setTimeout(function(){
					if($('.tooltipster-content').is(':hover'))
					{
						tooltipster_content(id);						
					}
					else
					{
						$('#more_assignee_'+id).tooltipster('hide');
					}
				},500);   
		   });
        }
    });      
}
function tooltipster_content(id)
{
	$('.tooltipster-content').mouseleave(function() {
		setTimeout(function(){
			$('#more_assignee_'+id).tooltipster('hide');
		},500);  
	});
}
function postComment()
{  
	var id = $('#comment_task_id').val();
	var comment = $('#comment').val();
	if(comment=='')
	{//if no comment inserted
		alert('لطفا نظر خود را تایپ نمایید');return;
	}
	$.ajax({
	    url     : "{!!URL::route('postComment')!!}",
	    type    : "post",
	    data    : '&task_id='+id+'&comment='+comment,
	    success : function(result){
	        $('#comments_div').append(result);
	        $('#comment').val('');
	        }
	    });    
	    return false;
}
//show only managers
function showManagers1()
{
    if($('#subordinates').is(':checked'))
    {
        document.getElementById('managers_div1').style.display = "inline";
        document.getElementById('users_div1').style.display = "none";
    }
    else
   {
   		document.getElementById('managers_div1').style.display = "none";
        document.getElementById('users_div1').style.display = "inline";     
    }
}
//update task details
function updateTaskDetails()
{
	var current_enddate = $('#current_enddate').val();
    var end_date = $('#end_date').val();
    
    var page = "{!!URL::route('updateReportViaAjax')!!}";
    $.ajax({
            url: page,
            type: 'post',
            data: $('#form_edit').serialize(),
            //beforeSend: function(){

                    //$("#form_edit").hide();
                    //$("#form_new").show();
                    //$("#form_new").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            //},
            dataType:'json',
            success: function(response)
          {
                if(response.cond == 'true')
                {
                    //$("#form_new").hide();
                    //$("#form_edit").slideDown();
                    //$('#success_alert').slideDown();
                    if(current_enddate == end_date)
                    {
                    	window.location.reload();
                    }
                    else
                 	{
                 		//load the tooltipster funtion and reload the tasks via ajax
					    //var url = window.location.pathname+"/"+response.week_no;
                  		window.location.reload();
                  	}
                }
                else
            	{
                    alert('Error!');
                }

            }
    });
    return false;
    

}
//is task group included in report or not
function isTaskGroupInReport1(page,selectedTaskGroup)
{
    $.ajax({
        url:page,
        type:'post',
        dataType:'json',
        data: '&task_group='+selectedTaskGroup,
        success: function(response){

            if(response.condition == 'true')
            {
                $('.hider').fadeIn();
            }
            else
         {
                $('.hider').fadeOut();
            }
        }
    });
}
//approve completed task
function approveCompletedTask(id,parent)
{
    var r = confirm("Do you want to continue?");

    if(r == true)
    {
        $.ajax({
            url     : "{!!URL::route('approveCompletedTask')!!}",
            type    : "post",
            data    : "&task_id="+id+'&parent_task='+parent,
            dataType: 'json',
            success : function(result){
                
                if(result.cond === 'true')
                {
                    $('#trigger_modal').trigger('click');
                    $('#pop_content2').html(result.data);
                }
                else
                {
                    $("#task_completed_alert").html(result.data);
                }
            }
        });
    }
    else
  	{
        return false;
    }
}
function task_approve(task_id)
{
	var page = "{!!URL::route('approveTask')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&task_id='+task_id,
        success: function(response){
        	//$('#status_buttons').hide();
			window.location.reload();
        }
    });
}    
function task_reject(task_id)
{
	var comment = $('#reason').val();
	var end_date= $('#end_date').val();//new end date
	var enddate= $('#enddate').val();//current end_date
	
	if(comment == ''){alert('Type your reason to reject!');return;}
	
	var page = "{!!URL::route('rejectTask')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&task_id='+task_id+'&comment='+comment+'&end_date='+end_date+'&enddate='+enddate,
        success: function(response){
        	location.href="{!!URL::route('getReport')!!}";
        }
    });
}
$("#assign_with2").select2();
$("#assign_with3").select2();
$(".datepicker_farsi").persianDatepicker(); 
$( ".datepicker" ).datepicker();
$( ".timepicker" ).clockpicker();

</script>
<script type="text/javascript">
      //min/max slider
      var currentProgress = '{!!$details->percentage!!}';

    $('.slider-basic1').noUiSlider({
        range: [0,100],
        start: [0],
        handles: 1,
        connect: 'lower',
        slide: function(){
            var val = Math.round($(this).val());
            $('#progress').text(
                     val+"%"
                );
            $(this).next('input').val(val);
            
        },
        set: function() {
            var val = Math.round($(this).val());
            $('#progress').text(
                     val+"%"
                );
            $(this).next('input').val(val);
        }
    });
    $('.slider-basic1').val(currentProgress, true);

    function postProgress()
	{   
		if($('#progress_summary').val() == '')
		{
			alert('لطفا اجرآت خود را تایپ نمایید');return;
		}
	    $.ajax({
	        url     : "{!!URL::route('postProgress')!!}",
	        type    : "post",
	        data    : $("#progress_chagne_modal").serialize(),
	        success : function(result){
	            $('#progress_summary_div').prepend(result);
	        	$('#progress_summary').val('');
	        	//var currentProgress = $('#percentage').val();
	        } 
	    });    
	    return false;
	}
	function check_percentage()
	{
		var percentage = $('#percentage').val();
		$('#progress_button').attr('disabled','true');
		if(percentage > currentProgress || percentage == 100)
		{
			$('#progress_button').removeAttr('disabled');
		}
	}
	function showCommentDate(id,type)
	{
		$('.'+type+'_date').hide();
		$('#'+type+'_'+id).show();
	}
	function delete_attachment(id,name)
	{
		var counter = 1;
		$.ajax({
			url:'{{URL::route("deleteAttachmentTask")}}',
			data: '&doc_id='+id+'&name='+name,
			type:'POST',
			success:function(r){
				$('#div_file_'+id).hide();
			}
		});
	}
	function add_file()
	{
		var current_total = $('#total_files').val();
		var total = parseInt(current_total)+parseInt(1);
		$('#total_files').val(total);
		var new_div = '<div id="files_'+total+'"><div class="col-md-11"><input class="form-control" type="file" name="files[]"></div><div class="col-md-1"><i class="icon wb-minus-circle" aria-hidden="true" style="font-size: 25px;cursor: pointer;" onclick="remove_file('+total+')"></i></div></div>';
		$('#all_files').append(new_div);	
	}
	function remove_file(no)
	{
		$('#files_'+no).remove();
	}

</script>
