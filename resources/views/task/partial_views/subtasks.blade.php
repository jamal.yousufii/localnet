<style>
#subtask_li li{
    background: none;
	border: none;
}

.sub_task_li {
	    border:4px solid white !important;
}
.sub_task_li:hover{
	background: #f8f8f8 !important;
	border:4px solid #f8f8f8 !important;
}
.collapse_button{
	display: inline-block;
	cursor: pointer;
	position:relative;
	background:white;
	border: 2px solid #eee;
	margin-left: 4px;
	font-size: 17px;
	width:22px;
	text-align:center;
}
</style>
<div id="subtask_li">
	<ul>	
	@foreach($object as $item)
	<?php
	if($item->end_date != '0000-00-00')
	{
		$end_date = $item->end_date;
		//convert shamsi to meladi
		//$end_date = explode("-", $item->end_date);
		//$end_date = dateToMiladi($end_date[0],$end_date[1],$end_date[2]);
	}
	else
	{
		$end_date = date('Y-m-d');
	}
	
	$from=date_create($end_date);
	$to=date_create(date('Y-m-d'));
	
	$diff=date_diff($to,$from);
	
	$deadline = $diff->format('%R%a');
	$d=$deadline;

    $label_status = "";
    $days;

    switch (true) {
    	case ($d < 0):
    		$days='Due';
    		break;
    	case ($d == 0):
    		$days='Today';
    		break;
    	case ($d == 1):
    		$days='Tomorrow';
    		break;
    	case ($d > 1 and $d < 7):
    		$days=abs($d).' Days';
    		break;
    	case ($d > 6 and $d < 15):
    		$days='Next Week';
    		break;
    	case ($d > 14 and $d < 22):
    		$days='Two Weeks';
    		break;
    	case ($d > 21 and $d < 29):
    		$days='Three Weeks';
    		break;
    	case ($d > 28 and $d < 56):
    		$days='Next Month';
    		break;
    	case ($d > 55 and $d < 336):
    		$nm=round($d/30);
    		$days=$nm.' Months';
    		break;
    	case ($d > 335):
    		$ny=round($d/365);
    		$days=$ny.' Year(s)';
    		break;
    	default:
    		$days='select date';
    		break;
    }

    if($d>0)
    {
        $label_status = "label-success";
    }
    else if($d == 0)
    {
        $label_status = "label-warning";
    }
    else
    {
        $label_status = "label-danger";
    }

	if(has_subtask($item->task_id))
	{
		$progress = getSubTaskProgress($item->task_id);
	}
	else
	{
		$progress = getMainTaskProgress($item->task_id);
	}

	$s_date = $item->start_date;
	$e_date = $item->end_date;
	if($s_date != '0000-00-00')
	{
		$sdate = explode("-", $s_date);
		$sy = $sdate[0];
		$sm = $sdate[1];
		$sd = $sdate[2];
		$s_date = dateToShamsi($sy,$sm,$sd);		
	}
	if($e_date != '0000-00-00')
	{
		$edate = explode("-", $e_date);
		$ey = $edate[0];
		$em = $edate[1];
		$ed = $edate[2];
		$e_date = dateToShamsi($ey,$em,$ed);		
	}
	$assignees = getTaskAssignees($item->task_id);
	
	?>
		<li class="list_main_task sub_task_li" id="list_{!!$item->task_id!!}">
			<span style="display:inline-block;cursor: pointer;">
				<div class="assignees" style="display:inline;">
				<?php
				    $AssignedBy_photo = getProfilePicture_task($item->user_id);
				    $tooltip = getUserFullName($item->user_id);
				    if($AssignedBy_photo)
				    {
					    if (!file_exists('/var/www/html/localnet/public/documents/profile_pictures/small_'.$AssignedBy_photo))
	                    {
		                    $img = Image::make('/var/www/html/localnet/public/documents/profile_pictures/'.$AssignedBy_photo);
	          						// crop image
	          						$img->crop(1278, 1597,897,80)->fit(100,100)->save('/var/www/html/localnet/public/documents/profile_pictures/small_'.$AssignedBy_photo);
	          						//HTML::image('/documents/profile_pictures/'.$photo, Auth::user()->username)
	                    }

					    
						?>
						<a href="javascript:void()" title="Assgin By: {!!$tooltip!!}">
						{!!HTML::image('/documents/profile_pictures/small_'.$AssignedBy_photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip'));!!}
						</a>
					<?php }
	                else
	                { ?>
	                	<a href="javascript:void()" title="Assgin By: {!!$tooltip!!}">
							{!!HTML::image('/img/default.jpeg', '', array('class' => 'project-img-owner'));!!}
						</a>
	                <?php }
	                ?>
				</div>
			</span>
			<span style="display:inline-block;width:70%;cursor: pointer;" data-target="#task_detail" data-toggle="modal" onclick="load_task_detail('task_detail',{!!$item->task_id!!})">
				
			
				<?php $status = get_task_status($item->task_id); ?>
				<div class="task_title big_title" style="margin-top:0.3em;">{!!$item->title!!}</div>
				
				@if($item->user_id!=Auth::user()->id && $status != -1)
				<div class="task_title big_title" id="pending_div"> 
					@if($status==0)<a href="{!!URL::route('ViewReport',array($item->task_id))!!}" style="color: #B9B6B6;font-style: italic;font-size: 0.9em">- {!!_('pending')!!} </a>@endif
				</div> 
				@endif
				@if(subTaskTree_count($item->task_id,$mode))
				<div id="tasks_collapse_<?=$item->task_id?>" style="display: inline">
				<div class="collapse_button" onclick="collapse_subtasks('show',<?=$item->task_id?>)">
					<b>+</b>		
				</div>
				</div>
				@endif
			</span>
			
			<span id="label_{!!$item->task_id!!}" style="margin-right: 170px;direction:rtl;float: right;position:relative;">
				<span class="label {!!$label_status!!} date_middle"> {!!$days!!}</span>
			</span>
			<div class="pull-right">
	            <div class="assignees" style="display:inline;">
	            
		            @if(count($assignees)>0)
		            	@if(count($assignees)<=3)
			            	@foreach($assignees AS $a_item)
							<?php
							    $photo = getProfilePicture_task($a_item->assigned_to);
							    if (!file_exists('/var/www/html/localnet/public/documents/profile_pictures/small_'.$photo))
			                    {
				                    $img = Image::make('/var/www/html/localnet/public/documents/profile_pictures/'.$photo);
			          						// crop image
			          						$img->crop(1278, 1597,897,80)->fit(100,100)->save('/var/www/html/localnet/public/documents/profile_pictures/small_'.$photo);
			          						//HTML::image('/documents/profile_pictures/'.$photo, Auth::user()->username)
			                    }
							    $tooltip = getUserFullName($a_item->assigned_to);
							?>
							<a data-toggle="modal" title="{!!$tooltip!!}">
								{!!HTML::image('/documents/profile_pictures/small_'.$photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip'));!!}
							</a>
							@endforeach
						@else
							<span id="more_assignee_<?=$item->task_id?>" style="cursor: pointer" onclick="show_tooltipster(<?=$item->task_id?>,'<?=URL::route("get_assignee")?>')">
								{!!HTML::image('/img/more.png', '', array('class' => 'project-img-owner'));!!}
							</span>
							<?php $count = 1;?>
							@foreach($assignees AS $a_item)
								@if($count <=2)
									<?php
									    $photo = getProfilePicture_task($a_item->assigned_to);
									    if (!file_exists('/var/www/html/localnet/public/documents/profile_pictures/small_'.$photo))
					                    {
						                    $img = Image::make('/var/www/html/localnet/public/documents/profile_pictures/'.$photo);
					          						// crop image
					          						$img->crop(1278, 1597,897,80)->fit(100,100)->save('/var/www/html/localnet/public/documents/profile_pictures/small_'.$photo);
					          						//HTML::image('/documents/profile_pictures/'.$photo, Auth::user()->username)
					                    }
									    $tooltip = getUserFullName($a_item->assigned_to);
									?>
									<a href="javascript:void()" title="{!!$tooltip!!}">
										{!!HTML::image('/documents/profile_pictures/small_'.$photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip'));!!}
									</a>
									
								@endif
								<?php $count++;?>
							@endforeach
							
						@endif
					@else
						<a href="javascript:void()" title="No Assignee">
							{!!HTML::image('/img/default.jpeg', '', array('class' => 'project-img-owner','data-original-title'=>'New Assignee','data-toggle'=>'tooltip'));!!}
						</a>
					@endif
				</div>
	            <?php 
		       	$onclick_progress = 'title="'._("You_dont_have_access_to_update_progress").'" class="progress"';
		       	if(Auth::user()->id == $item->user_id || isAdmin() || is_user_assigned_to_task($item->task_id))
				{							
			       	$onclick_progress = 'title="'._("Since_this_task_has_some_sub_tasks,_you_cant_update_progress_directly").'('.$progress.'% '._("completed)").'" class="progress"';
			       	if(!has_subtask($item->task_id))
					{
						$onclick_progress = 'onclick="loadModal('.$item->task_id.')" title="'.$progress.'% '._('completed').'" class="progress md-trigger" data-target="#progress_modal" data-toggle="modal" ';
					}
				}
				?>
			    <div <?=$onclick_progress?> style="display:inline-block;">
			        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{!!$progress!!}" aria-valuemin="0" aria-valuemax="100" style="width: {!!$progress!!}%;">
			            <span class="sr-only">{!!$progress!!}% {!!_('complete')!!}</span>
			        </div>
			    </div>
	        </div>
		</li>
		
		<div id='level3_tasks_<?=$item->task_id?>' style="display: none">
		{!!getSubTaskTree($item->task_id)!!}
		</div>
	@endforeach
	</ul>
</div>
<script>
	function collapse_subtasks(mode,id)
	{
		if(mode == 'show')
		{
			$('#level3_tasks_'+id).slideDown();
			$('#tasks_collapse_'+id).html('<div class="collapse_button" onclick="collapse_subtasks(\'hide\','+id+')"><b>-</b></div>');			
		}
		else
		{
			$('#level3_tasks_'+id).slideUp();
			$('#tasks_collapse_'+id).html('<div class="collapse_button" onclick="collapse_subtasks(\'show\','+id+')"><b>+</b></div>');			
		}
	}

</script>
