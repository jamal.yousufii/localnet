@extends('layouts.master')

@section('head')
    <title>{!!_('create_task_group')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">Dashboard</a></li>
            <li class="active"><span>{!!_('create_task_group')!!}</span></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            
            <div class="main-box-body clearfix">
                <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postTaskGroupCreate')!!}">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('title')!!} :</label>
                            <div class="col-sm-10">
                                <input class="form-control" required="required" type="text" name="title">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('description')!!} :</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" required="required" rows="5" id="wysiwig_full" name="description"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <div class="checkbox-custom">
                                        <input value='1' type="checkbox" id="in_report" name="in_report">
                                        <label for="in_report">
                                            {!!_('include_in_reports')!!}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php //if(isManager()){ ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <div class="radio">
                                            <input value='1' id='public' name="access" checked="" type="radio">             
                                        <label for="public">
                                            {!!_('department')!!}
                                        </label>
                                    </div>
                                    <div class="radio">                                          
                                            <input value='2' name="access" id='restrict' type="radio">                                               
                                        <label for="restrict">
                                            {!!_('personal')!!}
                                        </label>
                                    </div>
                                </div>
                            </div>                          
                        </div>
                    </div>
                    <?php //}?>
                    <!--
                    <div class="form-group" id='share_with_div' style='display:none'>
                        <label class="col-sm-2 control-label">Assign To</label>
                        <div class="col-sm-10">
                        <select name='share_with[]' id='share_with' multiple="multiple" style="width:840px;">
                                <?php 
                                    foreach($users AS $uitem)
                                    {
                                        echo "<option value='u_".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
                                    }

                                    foreach($deps AS $ditem)
                                    {
                                        echo "<option value='d_".$ditem->id."'>".$ditem->name."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    -->
                    {!! Form::token() !!}

                    <div class="form-group">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-sm-10">
    
                            <button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
                        
                            <button onclick="history.back()" class="btn btn-danger" type="button">{!!_('cancel')!!}</button>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
</div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/template/select2.min.js')!!}
{!! HTML::script('/js/template/hogan.js')!!}
{!! HTML::script('/js/template/typeahead.min.js')!!}
<!--
    <script type="text/javascript">
        $("#restrict").click(function(){
            $("#share_with_div").slideDown('slow');
        });
    $("#public").click(function(){
            $("#share_with_div").slideUp('slow');
        }); 

      $('#share_with').select2({
            placeholder: 'Select',
            allowClear: true
        });

        $(document).ready(function() {
        // Create Wysiwig editor for textare
        TinyMCEStart('#wysiwig_simple', null);
        //TinyMCEStart('#wysiwig_full', 'extreme');
        });

    </script>
-->
@stop
