@extends('layouts.master')
@section('head')
 	
    {!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
    {!! HTML::style('/css/tooltipster.css') !!}
    {!! HTML::style('/css/template/libs/nifty-component.css') !!}
	{!! HTML::style('/css/template/libs/jquery.nouislider.css') !!}

	
    {!! HTML::script('/js/jquery.tooltipster.min.js')!!}
    {!! HTML::style('/css/tasks.css') !!}
    {!! HTML::script('/js/tasks.js') !!}
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
    {!! HTML::script('/js/template/jquery.slimscroll.min.js')!!}
    {!! HTML::script('/js/template/jquery.nouislider.js')!!}
    <script>
	
		function load_task_detail(type,id)
	    {
	    	var page = "{!!URL::route('load_task_detail')!!}";
	    	$.ajax({
	            url:page,
	            type:'post',
	            data: '&id='+id+'&type='+type,
	            success: function(r){
					$('#task_detail').html(r);
	            }
	        });
	    }
	    function load_task_detail1(type,id)
	    {
	    	var page = "{!!URL::route('load_task_detail1')!!}";
	    	$.ajax({
	            url:page,
	            type:'post',
	            data: '&id='+id+'&type='+type,
	            success: function(r){
					$('#task_detail').html(r);
	            }
	        });
	    }
	    function loadModal(id)
	    {   
	    	$('#pop_content1').html('Loading...');
	        $.ajax({
	            url     : "{!!URL::route('loadModal')!!}",
	            type    : "post",
	            data    : "&id="+id+"&main="+0,
	            success : function(result){
	                $('#pop_content1').html(result);
	            }
	        });
	    }
	    //change start and end date
	    function changeDate()
	   {
	        var page = "{!!URL::route('changeTaskDate')!!}";
	        //get task id
	        var task_id = $('#task_id_modal').val();
	
	        $.ajax({
	            url: page,
	            type: 'post',
	            data: $('#change_modal_frm').serialize(),
	            dataType:'HTML',
	            success: function(response)
	          	{
	          		if($('#type').val()=='sub_task_detail')
	          		{
	          			var parent_id = $('#parent_id').val();
	          			load_task_detail('task_detail',parent_id);
	          		}
	          		else
	          		{
	                	$("#label_updated_"+task_id).html(response);
	                	$('.close').click();
	                }
	            }
	        });
	
	        
	    }
	    //insert via ajax
	    function saveNewTask(e,div,task_group,parent_task,id)
	    {   
	    	
	        var page = "{!!URL::route('saveNewTask')!!}";
	
	        var title = $('#'+id).val();
	
	        if (e.keyCode == 13 && title != '') 
	        {
	        	e.preventDefault();
	        	
	            $.ajax({
	                url: page,
	                type: 'post',
	                data: '&title='+title+'&task_group='+task_group+'&parent='+parent_task,
	                dataType:'HTML',
	                success: function(response)
	            	{
	                    $('#'+div).append(response);
	                    $('#'+id).val('');
	                }
	            });
	        }
	       
	
	    }
	    function load_task_date(div,id)
	   {
	    	var page = "{!!URL::route('load_task_date')!!}";
	    	$.ajax({
	            url:page,
	            type:'post',
	            data: '&id='+id+'&type='+div,
	            success: function(r){
					$('#'+div).html(r);
	            }
	        });
	    }
	    function load_task_date_sub(type,id)
	   {
	    	var page = "{!!URL::route('load_task_date')!!}";
	    	$.ajax({
	            url:page,
	            type:'post',
	            data: '&id='+id+'&type='+type,
	            success: function(r){
					$('#task_detail').html(r);
	            }
	        });
	    }
	    function load_task_assignee_sub(id)
	   {
	    	var page = "{!!URL::route('load_task_assignee')!!}";
	    	$.ajax({
	            url:page,
	            type:'post',
	            data: '&id='+id+'&type=sub_task_detail',
	            success: function(r){
					$('#task_detail').html(r);
	            }
	        });
	    }
	    
	</script>
	
    <title>Group Tasks</title>
@stop

<style>
/*image circle style*/
.project-img-owner {
border-radius: 50%;
background-clip: padding-box;
display: block;
float: left;
height: 28px;
padding: 3px;
overflow: hidden;
width: 28px;
}
img {
vertical-align: middle;
}
img {
border: 0;
}
/*-------------------*/

.task_title{
    width: 230px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      display: inline-block;
}

.list-task {}
.list-task h3 {border-bottom:none;  !important;font-size: 17px;margin:5px 0px;}
/*.list-task div~h3 {border-top:1px solid #eee;border-bottom:0;margin:5px 0px;}
*/
.list-task a {text-decoration: none}
.list-task ul {margin:30px 0 0 0;padding: 0;}
.list-task ul li {list-style: none;margin:8px 0px 0px 5px;}
.list-task ul li a {color:#444;margin: 0 0px;}
.list-task ul li ul {margin:8px 0px 0px 24px;}
.list-task .label{font-size: 9px;}
.list-task .progress {margin-bottom: 1px;margin:10px 5px;height: 8px;width: 60px !important;}
.list-task .sub_progress {margin-bottom: 1px;margin:10px 5px;height: 8px;width: 35px !important;}
/*.list-task ul li ul li  {}*/
span.li-date {color:#999;font-size:90%;padding:0 10px;}
.list-task input[type=checkbox] {display:inline-block;margin:6px 10px;font-size:14px;}
div.add-item{display:inline-block;margin:0 7px;font-size:12px;padding:0 13px;color:#888;}
div.add-item input{padding:4px;}
span.item-plus {display:inline-block;padding:5px 0px;font-size:150%;}

.task-details {border-left:1px solid #ddd;margin-top:-5px;}
.task-details input[type=text], .task-details select, .task-details textarea {font-size:14px;background:#fff;padding:3px;width:100%;border:0;}
.task-details td {display:block;border-bottom: 1px dashed #ddd; margin:8px 15px !important;}
.task-details textarea {color:grey;border-radius:5px;overflow: auto;min-height:2em;transition:all 0.3s;}
.task-details textarea:focus {border:0;box-shadow:0 0 5px #999;height:8em !important;transition:all 0.3s}
.task-details h3 {margin:0 0px 30px 15px;padding:10px 0px;text-align:center;border-bottom:0px solid #eee;}
.task-details label{font-size:14px;}
.task-check {
    margin-top:-8px;
}

.list-task li:hover{
    background: #eee;
}

.paging-btn{
    margin-top: 5px;
    margin-right: 10px;
    margin-left: 10px;
    
}

.task-group-list + .task-group-list {border-top:1px solid #eee;}

.paging-btn button{
    height: 27px;
    font-size: 10px;
    border-radius: 0px;
}
#tbl_weeks {
	width:100%;	
}
#tbl_weeks th{
	height:10px !important;
	font-size:1pt;
}
#tr_weeks td{
	width:18px;
	padding:2px;
	background:#eee;
	border: 1px solid #fff;
	font-size: 9pt;
}
#tr_weeks a{
	text-decoration:none;
	color:gray;
}
#tr_weeks td:hover a, #tr_weeks td.active a{
		color:white !important;
}
#tr_weeks td:hover {
	background:#002A80;
}

td.active {
	background:#003399 !important;
}
#tr_months td{
	width:18px;
	padding:2px;
	background:#eee;
	border: 1px solid #fff;
	font-size: 9pt;
	text-align:center;
}

.year {
	display:block;
	padding:0 8px;
	font-size:12pt;
}
</style>
@section("content")
<div class="row">
	@if(Session::has('success'))
	<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>

@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif
    <div class="col-lg-12">
            <div class="main-box clearfix">
                <header class="main-box-header clearfix col-md-12">
                	
                    <div class="btn-group pull-right col-md-8">
						<label class="btn btn-primary btn-sm  <?php if($mode == 'all'){echo 'active';}?>">
							<span onclick="location.href='{!!URL::route('getReportGroup',array($id,'all'))!!}'"> {!!_('all_tasks')!!}</span>
						</label>
						<label class="btn btn-primary btn-sm <?php if($mode == 'my'){echo 'active';}?>">
							<span onclick="location.href='{!!URL::route('getReportGroup',array($id,'my'))!!}'"> {!!_('my_tasks')!!}</span>
						</label>
						<label class="btn btn-primary btn-sm <?php if($mode == 'out'){echo 'active';}?>">
							<span onclick="location.href='{!!URL::route('getReportGroup',array($id,'out'))!!}'"> {!!_('outgoing_tasks')!!}</span>
						</label>
					</div>           		
                    
                </header>              
                <div class="main-box-body clearfix">         
                    <!-- Task list start -->
                    <div class="col-md-12 list-task" id="main_task_view"> 
                    	<input type=hidden id="total-pages-{!!$id!!}" value="{!!$pages!!}"> 
                    	<h3>
                    		<a href="javascript:void()" style="cursor: default">@if($id != 0) {!!$details->title!!} @else {!!_('completed_tasks')!!} @endif</a>
							<span style="border-bottom:0px;" class="paging-btn" id="buttons_{!!$id!!}">
								<button class="btn btn-primary" disabled onclick="paginateTasks('{!!$id!!}','prev');"><i class="fa  fa-chevron-left"></i> </button>
					            <button class="btn btn-primary" @if($pages <= 1) disabled @endif onclick="paginateTasks('{!!$id!!}','next');"> <i class="fa  fa-chevron-right"></i></button>
							</span>
						</h3>                  
                        <div>
                            <ul id="group_{!!$id!!}">
                            	@if($id != 0)
                                {!!getTaskGroup($id,0,$mode)!!}
                                @else
                                {!!getTaskGroup_completed(0)!!}
                                @endif
                            </ul>
                        </div>
                    </div>                 
                </div>
            </div>    
    </div>
</div>
<!-- Change date modal start -->
<div class="modal fade modal-fade-in-scale-up" id="change_date_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">×</span>
	      </button>
	      <h4 class="modal-title">{!!_('change_date')!!}</h4>
	    </div>
	    <div class="modal-body">
	      <form role="form" class='form-horizontal' id="change_modal_frm" name="change_modal_frm" style="padding-right:10px;">
	            <input type="hidden" name="task_id" id="task_id_modal" value="0">              
	            <div class="form-group">
	                <label class="col-sm-2 control-label">{!!_('start_date')!!} :</label>
	                <div class="col-sm-4">
	                    <input class="form-control datepicker_farsi" type="text" name="start_date" id="start_date_modal" data-placement="bottom">
	                </div>
	                <label class="col-sm-2 control-label">{!!_('end_date')!!} :</label>
	                <div class="col-sm-4">
	                    <input class="form-control datepicker_farsi" type="text" name="end_date" id="end_date_modal" data-placement="bottom">
	                </div>
	            </div>              
	      </form>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default margin-0" data-dismiss="modal">{!!_('close')!!}</button>
	      <button onclick='changeDate();' type="button" class="btn btn-primary modal-submit">{!!_('update')!!}</button>
	    </div>
	  </div>
	</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="progress_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog">
	    <div class="modal-content">
	        <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        <span aria-hidden="true">×</span>
		      </button>
		      <h4 class="modal-title">{!!_('update_task_progress')!!}</h4>
		    </div>
	        <div class="modal-body" id="pop_content1">
	        </div>
	        <div class="modal-footer" id='create_footer'>
	            <button onclick='postProgress()' id="progress_button_pop" disabled type="button" class="btn btn-primary modal-submit">{!!_('update')!!}</button>
	        </div>
	    </div>
	</div>
</div>
<div class="md-overlay"></div><!-- the overlay element -->

@stop

<script>
	function paginateTasks(task_group,next_prev)
    {
        var page = 0;
        var total_pages=$('#total-pages-'+task_group).val();
        if(next_prev == 'prev')
        {
            page=parseInt($('#group_'+task_group+' li:first').attr('class'));
            if (page>0) page=page-1;
        }
        if(next_prev == 'next')
        {
            page = $('#group_'+task_group+' li:first').attr('class');
            page=parseInt(page)+1;
        }
            console.log(page);

        var url = "{!!URL::route('paginateTasks')!!}";

        $.ajax({
            url: url,
            type: 'post',
            data: "page="+page+"&task_group="+task_group,
            dataType:'HTML',
            success: function(response)
           {
                $("#group_"+task_group).html(response);
                if (page==0){
                    $('#buttons_'+task_group+' button:first').prop('disabled', true);
                    $('#buttons_'+task_group+' button:last').prop('disabled', false);
                } else if (total_pages-page==1) {
                    $('#buttons_'+task_group+' button:first').prop('disabled', false);
                    $('#buttons_'+task_group+' button:last').prop('disabled', true);
                } else {
                    $('#buttons_'+task_group+' button').prop('disabled', false);
                }
            }
        });

        return false;
        
    }
</script>