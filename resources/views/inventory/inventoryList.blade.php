@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('inventory_list')!!}</title>
    <style type="text/css">
        table thead tr th
        {
            text-align: center;
            border-color: #000; 
            text-align: center !important;
        }
        table tbody tr td
        {
            border-color: #000;
            text-align: center !important;
        }
        a{cursor:pointer;}
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop

@section('content')

    @if(Session::has('success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('success')!!}
		</div>

    @elseif(Session::has('fail'))
    	<div class="alert alert-danger alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('fail')!!}
		</div>
    @endif

    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="pull-left">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{!! URL::route('home') !!}">{!!_('Dashboard')!!}</a>
                        </li>
                        <li class="active">
                            <span>{!!_('all_inventories')!!}</span>
                        </li>
                    </ol>
                    <h1>{!!_('inventory_list')!!}</h1>
                </div>
            </div>
            <div class="form-group" style="margin-top:20px">
            	<form id="search_form" class="form-horizontal">
	            	<label class="col-sm-2 control-label">{!!_('select_product_type')!!} </label>
	                <div class="col-sm-3">
	                    <select class="form-control" name="product_type" id="product_type">
	                    	<option value=''>---</option>
				        	{!!getProductTypes()!!}
				        </select>
	                </div>
	                <div class="col-sm-3">
	                    <input type="submit" value="{!!_('search')!!}" class="btn btn-warning"/>
	                    &nbsp;<input type="reset" value="{!!_('clear')!!}" class="btn btn-danger"/>
	                </div>
	        	</form>
	        	<?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){$dir = "pull-right";}else{$dir = "pull-left";}
			    ?>
                <span class="{!!$dir!!}"><a href="{!!URL::route('inventoryForm')!!}" class="btn btn-success">{!!_('add_new_inventory')!!}</a></span>
            </div>
        </div>
        <div style="padding:15px" class="table-responsive" id="search_result">
            <table class="table table-bordered table-responsive" id="inventoryList">
            	<thead>
		          <tr>
		            <th>{!!_('no#')!!}</th>
		            <th>{!!_('product_name')!!}</th>
		            <th>{!!_('product_type')!!}</th>
		            <th>{!!_('model')!!}</th>
		            <th>{!!_('status')!!}</th>
		            <th>{!!_('stock_in')!!}</th>
		            <th>{!!_('stock_out')!!}</th>
		            <th>{!!_('person_name')!!}</th>
		            <th>{!!_('date')!!}</th>
		            <th>{!!_('remakrs')!!}</th>
		
		            <th colspan="2">{!!_('operations')!!}</th>
		            
		          </tr>
		        </thead>
		
		        <tbody>
		        	<?php $counter = 1; ?>
		        	@if(!empty($records))
			            @foreach($records AS $item)
			                <tr class="remove_record{!!$item->id!!}">
				                <td>{!!$counter!!}</td>
				                <td>{!!$item->product_name!!}</td>
				                <td>{!!$item->product_type!!}</td>
				                <td>{!!$item->model!!}</td>
				                @if($item->status == 1)
				                <td>{!!_('new')!!}</td>
				                @elseif($item->status == 2)
				                <td>{!!_('used')!!}</td>
				                @else
				                <td>{!!_('damaged')!!}</td>
				                @endif
				                <td>{!!$item->stock_in!!}</td>
				                <td>{!!$item->stock_out!!}</td>
				                <td>{!!$item->person_name!!}</td>
				                @if(isMiladiDate())
				                <td>{!!$item->date;!!}</td>
								@else
								<td>{!!toJalali($item->date);!!}</td>
								@endif
				                <td>{!!$item->remarks!!}</td>
				                <td align='center'><a href="{!!route('inventoryEditForm',$item->id)!!}" title='{!!_("edit_record")!!}'><span class='fa fa-edit'></span></a></td>
				                <td align='center'><a onclick="deleteRecord('{!!$item->id!!}')" title='{!!_("delete_record")!!}'><span class='fa fa-trash'></span></a></td>
			                </tr>
			                <?php $counter++; ?>
			            @endforeach
		            @else
			        <div style="padding: 10px">
			        	<span style="color:red">{!!_('no_records_found_in_the_system')!!}</span>
			        </div>
			    	@endif
		        </tbody>
            </table>
			<div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
				@if(!empty($records))
				{!!$records->render()!!}
				@endif
			</div>
        </div>
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">

$(document).ready(function() {
	
	$('.pagination a').on('click', function(event) {
		event.preventDefault();
		if ($(this).attr('href') != '#') {
			//$('#ajaxContent').load($(this).attr('href'));
			
			$.ajax({
	                url: '{!!URL::route("inventoryList")!!}',
	                data: {"page":$(this).text(),"ajax":1},
	                type: 'get',
	                beforeSend: function(){
	
	                    //$("body").show().css({"opacity": "0.5"});
	                    $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
	
	                    $('#search_result').html(response);
	                }
	            }
	        );
		
		}
	});
	
	//get the inventory list based on product type onchange of the dropdown.
	$("#search_form").submit(function(){
	
		var product_type = $('#product_type').val();
	    $.ajax({
	            url: "{!!URL::route('loadSearchedInventories')!!}",
	            data: '&product_type='+product_type,
	            type: 'post',
	            beforeSend: function(){
	                $("#search_result").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	            },
	            success: function(response)
	            {
	                $("#search_result").html(response);
	            }
	        }
	    );
		return false;
	});
});
	// Delete Record ajax script;
    function deleteRecord(record_id) 
    {
        var ID = record_id;
        var dataString = "record_id="+ID;
        if(confirm("Are you sure you want to do this action ? THERE IS NO UNDO"))
        {
            $.ajax({
                 type: "POST",
                 url: "{!!URL::route('getDeleteInventory')!!}",
                 data: dataString,
                 cache: false,
                 success: function(mydata){
                    $(".remove_record"+ID).css('background','Crimson');
                    $(".remove_record"+ID).slideUp('6000', function(){
                    $(this).remove();
                    //$("#deleted_result").html(mydata);
                    });
                 }
            });
        }
        return false;
    }
</script>

@stop


