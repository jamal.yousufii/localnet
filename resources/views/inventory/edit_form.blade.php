@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('inventory_edit_form')!!}</title>
    <style type="text/css">
        
    </style>
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
    {!! HTML::style('/css/autocomplete/jquery-ui.css') !!}

@stop

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
        </div>

    @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
        </div>
    @endif
    <?php 
	    if(isShamsiDate())
			$date = checkEmptyDate($inventory->date);
		else
			$date = checkGregorianEmtpyDate($inventory->date);
	?>
<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <h3 id="form_title">{!!_('inventory_edit_form')!!}</h3>
            </div>
        </div>
        <hr style="border: 1px dashed #b6b6b6" />
    </div>

    <div class="container">
        <form role="form" method="post" action="{!! URL::route('postEditInventoryData', $inventory->id) !!}" class="form-horizontal">
			
            <div id="first_page">
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('product_name')!!} </label>
                    <div class="col-sm-2">
                        <input class="form-control" type="text" name="product_name" id="product_name" value="{!!$inventory->product_name!!}" required />
                    </div>
                    <div id="product_type_div">
	                    <label class="col-sm-2 control-label">{!!_('product_type')!!} </label>
	                    <div class="col-sm-2">
		                    <select class="form-control" name='product_type' id="product_type" value="{!!old('product_type')!!}">
		                        <option value=''>---</option>
		                        {!!getProductTypes($inventory->product_type)!!}
		                    </select>
	                    </div>
	                </div>
	                <div id="other_product_div" style="display:none">
	                    <label class="col-sm-2 control-label">{!!_('product_type')!!} </label>
	                    <div class="col-sm-2">
		                    <input type="text" class="form-control" name='other_product' id="other_product" value="{!!old('product_type')!!}" />
	                    </div>
	                </div>
                    <label class="col-sm-2 control-label">{!!_('model')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='model' id="model" value="{!!$inventory->model!!}" />
                    </div>
                </div>
    			<div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('status')!!} </label>
                    <div class="col-sm-2">
	                    <select class="form-control" name='status' id="status" value="{!!old('status')!!}">
	                    	<option value="">{!!_('select')!!}</option>
	                    	<option value="1" <?php if($inventory->status == 1){ echo "selected='selected'";}?>>{!!_('new')!!}</option>
	                    	<option value="2" <?php if($inventory->status == 2){ echo "selected='selected'";}?>>{!!_('used')!!}</option>
	                    	<option value="3" <?php if($inventory->status == 3){ echo "selected='selected'";}?>>{!!_('damaged')!!}</option>
	                    </select>
                    </div>
                    <label class="col-sm-2 control-label">{!!_('stock_in')!!} </label>
                    <div class="col-sm-2">
	                    <input type="number" class="form-control" name='stock_in' id="stock_in" value="{!!$inventory->stock_in!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('stock_out')!!} </label>
                    <div class="col-sm-2">
	                    <input type="number" class="form-control" name='stock_out' id="stock_out" value="{!!$inventory->stock_out!!}" />
                    </div>
                </div>
                <div class="form-group">
                	<label class="col-sm-2 control-label">{!!_('person_name')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='person_name' id="person_name" value="{!!$inventory->person_name!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('date')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="{!!getDatePickerClass()!!} form-control" name='date' id="date" value="{!!$date!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('remarks')!!} </label>
                    <div class="col-sm-2">
	                    <textarea cols="30" rows="3" class="form-control" name='remarks' id="remarks">{!!$inventory->remarks!!}</textarea>
                    </div>
                </div>
                {!! Form::token() !!}
                <div class="form-group">
                    <div class="col-sm-12" style="margin-top:30px">
	                    <button class="btn btn-success" type="submit">
	                        <span>
	                            <i class="fa fa-save"></i>
	                        </span>
	                        &nbsp;{!!_('save_changes')!!}
	                    </button>
	                    <a href="javascript:history.back()" class="btn btn-danger">
	                        <span>
	                            <i class="fa fa-remove"></i>
	                        </span>
	                        &nbsp;{!!_('cancel')!!}
	                    </a>
	                </div>
                </div>
                <hr style="border: 1px dashed #b6b6b6" />
            </div>
        </form>
    
    </div>

</div>

@stop
@section('footer-scripts')
{!! HTML::script('/js/autocomplete/jquery-ui.js')!!}

<script type="text/javascript">

	$("#product_type").append("<option value='0'>Other</option>");
	$("#product_type").change(function(){
		if($("#product_type").val() == "0")
		{
			$("#product_type_div").hide();
			//$("#product_type").prop("disabled",true);
			$("#other_product_div").fadeIn('slow');
		}
		else
		{
			$("#other_prduct_div").hide();
			//$("#other_product").prop("disabled",true);
			$("#prduct_type_div").fadeIn('slow');
		}
	});
	function getStockIn(){
		$.ajax({ 
				url:"{!!URL::route('getStockInByProductType')!!}",
				data: { product_type: $("#product_type").val()},
				type: "POST",
				success: function(data){
					if(data != 0 || data != "")
					{
						$("#stock_in").val(data);
						$("#stock_in").prop('readonly',true);
					}else{
						$("#stock_in").val("");
						$("#stock_in").prop('readonly',false);
					}
				}
		});
	}
	$( "#product_name" ).autocomplete({
	
		source: function(request, response) {
			
			$.ajax({ 
				url:"{!!URL::route('getAutocompleteList', array('table' => 'inventory', 'field' => 'product_name'))!!}",
				data: { term: $("#product_name").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);				
				}
		});
	},
	minLength: 1
	});

    $(function(){


    });

</script>


@stop