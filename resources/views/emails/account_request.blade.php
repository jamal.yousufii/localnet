<html>
    <div style="direction: rtl;">
        <p style="font-size:20px; font-weight:bold">جناب محترم/محترمه {!!$name!!} {!!$last_name!!}!</p>
        <p style="font-size:15px; font-weight:bold">برای ایجاد اکونت لطفاً بالای لینک ذیل کلیک نمائید.</p>
        <br>
    <a href="{{URL::to('/')}}/account/register/{{Crypt::encrypt($email)}}" style="font-size:15px; font-weight:bold"> ایجاد اکونت جدید در سیستم HRMIS </a>
    </div>
</html>