<html>
<div style="direction: rtl;">
	<p>جناب محترم {!!$name!!} {!!$last_name!!}!</p>
	<br/>
	<p>امروز به تاریخ 
	({!!$the_date!!})
	در سیستم حاضری الکترونیکی غیر حاضر میباشید.
	</p>
	<br/>
	<p>
		موضوع به شما گسیل یافت.
	</p>
	<br/>
	<p>
		با احترام
	</p>
	<br/>
	<p>
	آمریت حاضری - ریاست منابع بشری
	</p>
</div>
<p>
Note: It is an auto generated email from HRMIS system.
</p>
</html>