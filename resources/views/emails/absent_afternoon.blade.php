<html>
<div style="direction: rtl;">
	<p>جناب محترم {!!$name!!} {!!$last_name!!}!</p>
	<br/>
	<p>شما به تاریخ
	({!!$the_date!!})
	 از طرف بعدازظهر در سیستم حاضری ندادید.
	لذا غیرحاضر محسوب شدید.
	</p>
	<br/>
	<p>
		موضوع به شما گسیل یافت.
	</p>
	<br/>
	<p>
		با احترام
	</p>
	<br/>
	<p>
	آمریت حاضری - ریاست منابع بشری
	</p>
</div>
<p>
Note: THIS WAS SENT FROM AN AUTOMATED SYSTEM. PLEASE DO NOT REPLY TO THIS MESSAGE.
</p>
</html>
