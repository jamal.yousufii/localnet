<html>
    <div style="direction: rtl;">
        <p style="font-size:20px; font-weight:bold">جناب محترم/محترمه {!!$name!!} {!!$last_name!!}!</p>
        <p style="font-size:15px; font-weight:bold">پسورد شما موفقانه تبدیل گردید.</p>
        <br>
        <p style="font-size:15px; font-weight:bold">شما میتوانید با استفاده از یوزر و پسورد ذیل به اکونت خویش دسترسی پیدا کنید.</p>
        <br>
        <p style="font-size:15px; font-weight:bold">یوزر : {{$email}}</p>
        <p style="font-size:15px; font-weight:bold">پسورد : {{$password}}</p>
    </div>
</html>