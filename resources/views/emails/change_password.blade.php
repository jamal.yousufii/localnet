<html>
    <div style="direction: rtl;">
        <p style="font-size:20px; font-weight:bold">جناب محترم/محترمه {!!$name!!} {!!$last_name!!}!</p>
        <p style="font-size:15px; font-weight:bold">برای تغیر پسورد لطفاً بالای لینک ذیل کلیک نمائید.</p>
        <br>
        <a href="{{URL::to('/')}}/user/forgot/reset_password/{{Crypt::encrypt($email)}}" style="font-size:15px; font-weight:bold"> تغیر پسورد سیستم HRMIS </a>
    </div>
</html>