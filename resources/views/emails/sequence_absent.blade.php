<html>
<div style="direction: rtl;">
	<p>جناب محترم !</p>
    <br/>
    کارمندان ذیل از طرف بعد از ظهر در سیستم بیشتر از پنج روز بطور مسلسل حاضری نداده اند
	</p>
	<br/>
	<ul>
	@foreach($emps as $emp)
	<li>
	{!!$emp->name!!} {!!$emp->last_name!!}
	</li>
  @endforeach
	</ul>
	<br/>
	<p>
		موضوع به شما گسیل یافت.
	</p>
	<br/>
	<p>
		با احترام
	</p>
	<br/>
	<p>
		آمریت حاضری - ریاست منابع بشری
	</p>
</div>
<p>
Note: THIS WAS SENT FROM AN AUTOMATED SYSTEM. PLEASE DO NOT REPLY TO THIS MESSAGE.
</p>
</html>
