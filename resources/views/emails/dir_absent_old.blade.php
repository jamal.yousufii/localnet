<html>
<div style="direction: rtl;">
	<p>جناب محترم !</p>
	<br/>
	<p>امروز به تاریخ ({!!date('Y-m-d')!!}) 
	کارمندان ذیل بعد از وقت تعیین شده کارتهای خویش را در ماشین حاضری پیش نمودند
	فلهذا غیرحاضر محسوب میشوید.
	</p>
	<br/>
	<ul>
	@for($i=0;$i<count($emps)-1;$i++)
	<li>
	{!!$emps[$i]['name']!!} {!!$emps[$i]['last_name']!!} ,
	به ساعت
	: {!!$emps[$i]['time']!!}
	</li>
	@endfor
	</ul>
	<br/>
	<p>
		موضوع به شما گسیل یافت.
	</p>
	<br/>
	<p>
		با احترام
	</p>
	<br/>
	<p>
	مدیریت حاضری - ریاست منابع بشری
	</p>
</div>
<p>
Note: It is an auto generated email from HRMIS system.
</p>
</html>
