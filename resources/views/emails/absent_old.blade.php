<html>
<div style="direction: rtl;">
	<p>جناب محترم {!!$name!!} {!!$last_name!!}!</p>
	<br/>
	<p>امروز به تاریخ ({!!date('Y-m-d')!!}) شما در ماشین حاضری به ساعت {!!$time!!} کارت خویش را پیش نمودید
	فلهذا غیرحاضر محسوب میشوید.
	</p>
	<br/>
	<p>
		موضوع به شما گسیل یافت.
	</p>
	<br/>
	<p>
		با احترام
	</p>
	<br/>
	<p>
	مدیریت حاضری - ریاست منابع بشری
	</p>
</div>
<p>
Note: It is an auto generated email from HRMIS system.
</p>
</html>