<html>
<div style="direction: rtl;">
	<p>جناب محترم !</p>
	<br/>
	<p> به تاریخ
	({!!$the_date!!})
	کارمندان ذیل از طرف بعدازظهر در سیستم حاضری نداده اند.
	لذا غیرحاضر محسوب شده اند.
	</p>
	<br/>
	<ul>
	@for($i=0;$i<count($emps)-1;$i++)
	<li>
	{!!$emps[$i]['name']!!} {!!$emps[$i]['last_name']!!}
	</li>
	@endfor
	</ul>
	<br/>
	<p>
		موضوع به شما گسیل یافت.
	</p>
	<br/>
	<p>
		با احترام
	</p>
	<br/>
	<p>
		آمریت حاضری - ریاست منابع بشری
	</p>
</div>
<p>
Note: THIS WAS SENT FROM AN AUTOMATED SYSTEM. PLEASE DO NOT REPLY TO THIS MESSAGE.
</p>
</html>
