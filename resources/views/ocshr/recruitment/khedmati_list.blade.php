
<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='service_list'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>نام کامل</th>
                        <th>شماره تعینات</th>
                        <th>جنسیت</th>
                        <th>سکونت اصلی</th>
                        
                        <th>رتبه</th>
                        <th>بست</th>
                        <th>تاریخ تقرر</th>
                        <th>علمیه</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->

<script type="text/javascript">
    $(document).ready(function() {
        $('#service_list').dataTable(
            {

                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getServiceEmployeeData')!!}"
            }
        );

    });
</script>

