@extends('layouts.master')

@section('head')
{!! HTML::style('/css/tasks/persian_datepicker.css') !!}
{!! HTML::script('/js/tasks/persian_datepicker.js')!!}
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><a href="{!!URL::route('getRecruitment')!!}">کارمندان</a></li>
            <li class="active"><span>{!!$row->name_dr!!} {!!$row->last_name!!}</span></li>
        </ol>
    </div>
</div>

<!-- Example Tabs -->
<div class="example-wrap">
	<div class="nav-tabs-horizontal">
	  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
	  @if(canView('hr_recruitment'))
	    <li class="active" role="presentation"><a data-toggle="tab" onclick="$('#details').show();$('#all').hide();" href="#details" aria-controls="exampleTabsOne"
	      role="tab">جزییات کارمند</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('changed',{!!$id!!})" href="#all" aria-controls="exampleTabsTwo"
	      role="tab">تبدیلی ها</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('khedmati',{!!$id!!})" href="#all" aria-controls="exampleTabsThree"
	      role="tab">خدمتی ها</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('docs',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">اسناد ضمیمه</a></li>
	    <!--<li role="presentation"><a data-toggle="tab" onclick="load_employee_det('suggestion',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">پیشنهاد مقرری</a></li>-->
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('hire',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab"> حکم مقرری</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('resign',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">استعفا</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('fire',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">منفکی</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('retire',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">تقاعد</a></li>
	  @endif
	  @if(canView('hr_documents'))
	  	<li role="presentation"><a data-toggle="tab" onclick="load_employee_det('salary',{!!$id!!})" href="#all" aria-controls="exampleTabsTwo"
	      role="tab">معاش</a></li>
	  	<li role="presentation"><a data-toggle="tab" onclick="load_employee_det('experience',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">تجارب کاری</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('training',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">آموزش ها</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('soldier',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">عسکری</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('promotion',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">ترفیع</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('evaluation',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">ارزیابی</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('punishment',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">مجازات</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('makafat',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">مکافات</a></li>
	    @if(canAdd('hr_documents'))
	    <li role="presentation"><a data-toggle="tab" onclick="load_employee_det('cv',{!!$id!!})" href="#all" aria-controls="exampleTabsFour"
	      role="tab">خلص سوانح</a></li>
	    @endif
	  @endif
	  </ul>
	  <div class="tab-content padding-top-20">
	    <div class="tab-pane active" id="details" role="tabpanel">
	    	{!!$employeeUpdate!!}
	    </div>
	    <div class="tab-pane" id="all" role="tabpanel">
	    
	    </div>
	  </div>
	</div>
</div>
<!-- End Example Tabs -->


@stop
@section('footer-scripts')
<script>

	function load_employee_det(div,emp_id)
	{
		$('#details').hide();$('#all').show();
	    var page = "{!!URL::route('loadEmployeeDetails')!!}";
	    
	    $.ajax({
	        url: page,
	        type: 'post',
	        data: '&type='+div+'&id='+emp_id,
	        //dataType:'HTML',
	        success: function(r)
	      	{
	        	$('#all').html(r);
	        }
	    });   
	}
</script>
@stop

