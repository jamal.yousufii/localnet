@extends('layouts.master')

@section('head')
	{!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>کارمندان</span></li>
        </ol>
    </div>
</div>
@if(canAdd('hr_recruitment'))
<header class="main-box-header clearfix">
    <h2>
    	<a href="{!!URL::route('addNewEmployee')!!}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle fa-lg"></i> کارمند جدید</a>
    </h2>
</header>
@endif
<!-- Example Tabs -->
<div class="example-wrap">
	<div class="nav-tabs-horizontal">
	  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
	    <li @if($active=='all'){!!'class="active"'!!}@endif role="presentation"><a data-toggle="tab" href="#all" aria-controls="exampleTabsOne"
	      role="tab">تمام کارمندان</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('changed')" href="#changed" aria-controls="exampleTabsTwo"
	      role="tab">تبدیلی ها</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('khedmati')" href="#khedmati" aria-controls="exampleTabsThree"
	      role="tab">خدمتی ها</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('fired')" href="#fired" aria-controls="exampleTabsFour"
	      role="tab">منفکی ها</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('resign')" href="#resign" aria-controls="exampleTabsFour"
	      role="tab">استعفا</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('retire')" href="#retire" aria-controls="exampleTabsFour"
	      role="tab">متقاعدین</a></li>
	    
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('extra_bast')" href="#extra_bast" aria-controls="exampleTabsFour"
	      role="tab">انتظار به معاش</a></li>
	    
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('tanqis')" href="#tanqis" aria-controls="exampleTabsFour"
	      role="tab">اضافه بست</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('contracts')" href="#contracts" aria-controls="exampleTabsFour"
	      role="tab">قراردادی ها</a></li>
	    
	    <li @if($active=='search'){!!'class="active"'!!}@endif role="presentation"><a data-toggle="tab" href="#search" aria-controls="exampleTabsFour"
	      role="tab">جستجوی پیشرفته </a></li>
	  </ul>
	  <div class="tab-content padding-top-20">
	    <div class="tab-pane @if($active=='all'){!!'active'!!}@endif" id="all" role="tabpanel">
	    	<div class="row">
			    <div class="col-lg-12">
			        <div class="main-box">
			            <div class="main-box-body clearfix">
			            <div class="table-responsive">
			                <table class="table table-responsive" id='list'>
			                    <thead>
			                    <tr>
			                        <th>#</th>
			                        <th>نام کامل</th>
			                        <th>ولد</th>
			                        <th>شماره تعینات</th>
			                        <th>دیپارتمنت</th>
			                        <th>سکونت اصلی</th>
			                        
			                        
			                        <th>بست</th>
			                        <th>تاریخ تقرر</th>
			                        <th>عملیه</th>
			                    </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>
			            </div>
			        
			            </div>
			        </div>
			    </div>
			</div>
	    </div>
	    <div class="tab-pane" id="changed" role="tabpanel">
	    	لیست تبدیلی ها
	    </div>
	    <div class="tab-pane" id="khedmati" role="tabpanel">
	    	لیست خدمتی ها
	    </div>
	    <div class="tab-pane" id="fired" role="tabpanel">
	    	لیست منفکی ها
	    </div>
	    <div class="tab-pane" id="resign" role="tabpanel">
	    	لیست استعفا
	    </div>
	    <div class="tab-pane" id="retire" role="tabpanel">
	    	لیست تقاعدی ها
	    </div>
	    <div class="tab-pane" id="extra_bast" role="tabpanel">
	    	انتظار به معاش
	    </div>
	    <div class="tab-pane" id="tanqis" role="tabpanel">
	    	اضافه بست
	    </div>
	    <div class="tab-pane" id="contracts" role="tabpanel">
	    	
	    </div>
	    <div class="tab-pane @if($active=='search'){!!'active'!!}@endif" id="search" role="tabpanel">
	    <?php
	    if(isset($dep_id)){$dep = $dep_id;}else{$dep=0;};
	    if(isset($sub_dep_id)){$sub = $sub_dep_id;}else{$sub=0;};
	    if(isset($bast)){$bast_id = $bast;}else{$bast_id=0;};
	    if(isset($employee_type)){$emp_type = $employee_type;}else{$emp_type=0;};
	    if(isset($docs)){$doc = $docs;}else{$doc=0;};
	    if(isset($typee)){$type = $typee;}else{$type=0;};
	    if(isset($gender)){$gen = $gender;}else{$gen=0;};
	    if(isset($education)){$edu = $education;}else{$edu=0;};
	    if(isset($birth_year)){$year = $birth_year;}else{$year=0;};
	    if(isset($full_name)){$name = $full_name;}else{$name='';};
	    ?>
	    	<form class="form-horizontal" role="form" method="post" action="{!!URL::route('getRecruitmentSearch')!!}"> 
		    	<div class="row">
		        	<div class="col-lg-12">
		        		<div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">نام مکمل</label>
		                                <input type="text" name="name" id="fullname" class="form-control" value="{!!$name!!}"/>
		                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		                			</div>
		                		</div>
							    <div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">ادارۀ عمومی</label>
		                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
		                                    <option value="0">انتخاب</option>
		                                    @foreach($parentDeps AS $dep_item)
		                                    	@if($dep_item->id == $dep)
		                                        <option selected value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
		                                        @else
		                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
		                                        @endif
		                                    @endforeach
		                                </select>
		                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		                			</div>
		                		</div>
		                		<div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">ادارۀ مربوط</label>
		                                <select class="form-control" name="sub_dep" id="sub_dep">
		                                    <option value='0'>انتخاب</option>
		                                    @if(isset($sub_deps))
		                                    	@foreach($sub_deps AS $sub_item)
		                                    		@if($sub_item->id == $sub)
			                                        <option selected value='{!!$sub_item->id!!}'>{!!$sub_item->name!!}</option>
			                                        @else
			                                        <option value='{!!$sub_item->id!!}'>{!!$sub_item->name!!}</option>
			                                        @endif
		                                    	@endforeach
		                                    @endif
		                                </select>
		                			</div>
		                		</div>
		                		<div class="col-sm-3">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">سویه تحصیلی</label>
					            		<select name = "education_degree" id="education_degree" class="form-control">
		                                    <option value='0'>انتخاب</option>
		                                    {!!getStaticDropdown('education_degree',$edu)!!}
		                                </select>
					               	</div>
					            </div>
					            
							</div>
						</div>
						<div class="container-fluid">
					      	<div class="row">
					      		
					      		<div class="col-sm-3">
					            	<div class="col-sm-12">
					            		<label class="col-sm-2 ">کارکنان</label>
		                                <select name="employee_type" id="employee_type" class="form-control" onchange="showRank(this.value)">
		                                    <option value='0'>انتخاب</option>
		                                    <option value='1' <?php if($emp_type=='1'){echo 'selected';} ?>>مامور</option>
		                                    <option value='2' <?php if($emp_type=='2'){echo 'selected';} ?>>اجیر</option>
		                                    <option value='3' <?php if($emp_type=='3'){echo 'selected';} ?>>نظامی</option>
		                                    <option value='4' <?php if($emp_type=='4'){echo 'selected';} ?>>مامور بالمقطع</option>
		                                    <option value='5' <?php if($emp_type=='5'){echo 'selected';} ?>>اجیر بالمقطع</option>
		                                </select>
					               	</div>
					               	
					            </div>
					            <div id="bast_div">
						      		<div class="col-sm-3">
						      			<div class="col-sm-12">
						      				<label class="col-sm-12 ">بست</label>
			                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
			                                <select name = "emp_bast" id = "emp_bast" class="form-control">
			                                    <option value='0'>انتخاب</option>
			                                    {!!getBastStaticList('employee_rank',$bast_id)!!}
			                                </select>
						      			</div>
						      		</div>
						      	</div>
						      	<div id="ajir_div" style="display:none;">
							      	<div class="col-sm-3">
						      			<div class="col-sm-12">
						      				<label class="col-sm-12 ">درجه اجیر</label>
			                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
			                                <select name = "ajir_bast" id = "ajir_bast" class="form-control">
			                                    <option value='0'>انتخاب</option>
			                                    {!!getBastStaticList('employee_rank')!!}
			                                </select>
						      			</div>
						      		</div>	
					      		</div>
					      		<div id="military_div" style="display:none;">
					      			<div class="col-sm-3">
						      			<div class="col-sm-12">
						      				<label class="col-sm-12 ">بست</label>
			                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
			                                <select name = "military_bast" name = "military_bast" class="form-control">
		                                        <option value='0'>انتخاب</option>
		                                        {!!getStaticDropdown('military_rank',Input::old('emp_bast'))!!}
		                                    </select>
						      			</div>
						      		</div>
					      		</div>
					      		<div class="col-sm-3">
					            	<div class="col-sm-12">
					            		<label class="col-sm-2 ">نوعیت</label>
		                                <select name="type" id="type" class="form-control">
		                                    <option value='0'>انتخاب</option>
		                                    <option value='1' <?php if($type=='1'){echo 'selected';} ?>>اضافه بست</option>
		                                    <option value='2' <?php if($type=='2'){echo 'selected';} ?>>تبدیلی</option>
		                                    <option value='3' <?php if($type=='3'){echo 'selected';} ?>>انتظار به معاش</option>
		                                    <option value='4' <?php if($type=='4'){echo 'selected';} ?>>برحال</option>
		                                    <option value='5' <?php if($type=='5'){echo 'selected';} ?>>محصل</option>
		                                    <option value='6' <?php if($type=='6'){echo 'selected';} ?>>منفک</option>
		                                    <option value='7' <?php if($type=='7'){echo 'selected';} ?>>استعفا</option>
		                                    <option value='8' <?php if($type=='8'){echo 'selected';} ?>>تقاعد</option>
		                                </select>
					               	</div>
					               	
					            </div>
					            <div class="col-sm-2">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">سال تولد</lable>
		                                <?php 
		                                     $current_year = date('Y')-621;
		                                     
		                                ?>
		                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
		                                <select name = "birth_year" id = "birth_year" class="form-control">
		                                    <option value='0'>انتخاب</option>
		                                    <?php 
		                                       
		                                        for($i=$current_year-84;$i<=$current_year-10;$i++)
		                                        {
		                                            if($year==$i)
		                                            {
		                                            	echo "<option selected>".$i."</option>";
		                                            }
		                                            else
		                                            {
		                                            	echo "<option>".$i."</option>";
		                                            }
		                                        }
		                                    ?>
		                                </select>
					               	</div>
					            </div>
							   
		                	</div>
		                </div>
		                <div class="container-fluid">
					      	<div class="row">
		                		<div class="col-sm-2">
					            	<div class="col-sm-12">
					            		<label class="col-sm-2 ">جنسیت</label>
		                                <select name="gender" id="gender" class="form-control">
		                                    <option value='0'>انتخاب</option>
		                                    <option value='1' <?php if($gen=='1'){echo 'selected';} ?>>مرد</option>
		                                    <option value='2' <?php if($gen=='2'){echo 'selected';} ?>>زن</option>
		                                    
		                                </select>
					               	</div>
					               	
					            </div>
					             <div class="col-sm-2">
							    	<label class="col-sm-12 ">&nbsp;</label>
					    			<div class="col-sm-12 checkbox-custom checkbox-primary checkbox-inline">
				                        <input value='1' name="docs" id="docs" type="checkbox" @if($doc == 1) checked @endif>
				                        <label for="subordinates">
				                           اسناد ناتکمیل
				                        </label> 
				                    </div>
				                </div>
								<div class="col-sm-2 pull-right">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">&nbsp;</label>
		                                <button class="btn btn-primary pull-right" type="submit"> جستجو</button>
		                			</div>
		                			
		                		</div>
		                	</div>
		                </div>
					</div>
				</div>
		    </form>
		    <hr>
		    <div class="row">
			    <div class="col-lg-12">
			        <div class="main-box">
			            <div class="main-box-body clearfix">
				            @if(isset($table))
				                {!!$table->render()!!}
				  				{!!$table->script()!!}
				  			@endif
				            @if($active=='search' AND canView('hr_documents'))
                			<div class="col-sm-6">
                				<label class="col-sm-12 ">&nbsp;</label>
                                <a href="{!!URL::route('printEmployees',array($dep,$sub,$bast_id,$emp_type,$doc,$type,$gen,$edu,$year,$name))!!}" class="btn btn-primary pull-right">
									پرنت اکسل
								</a>
                			</div>
                			@endif
			            </div>
			        </div>
			    </div>
			</div>
	    </div>
	  </div>
	</div>
</div>
<!-- End Example Tabs -->


<!-- fire modal start -->
<div class="modal fade modal-fade-in-scale-up" id="change_date_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">×</span>
	      </button>
	      <h4 class="modal-title">تاریخ منفکی</h4>
	    </div>
	    <div class="modal-body">
	      <form role="form" class='form-horizontal' id="change_modal_frm" name="change_modal_frm" style="padding-right:10px;">
	            <input type="hidden" name="employee_id" id="employee_id" value="0">              
	            <div class="form-group">
	                <label class="col-sm-2 control-label">تاریخ :</label>
	                <div class="col-sm-4">
	                    <input class="form-control datepicker_farsi" type="text" name="date" data-placement="bottom">
	                </div>
	                
	            </div>              
	      </form>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default margin-0" data-dismiss="modal">{!!_('close')!!}</button>
	      @if(canAdd('hr_recruitment'))
	      <button onclick='fireDate();' type="button" class="btn btn-primary modal-submit">منفک</button>
	      @else
	      <p>You dont have permission</p>
	      @endif
	    </div>
	  </div>
	</div>
</div>
<!-- Modal End -->
<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>

<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {
				"order": [[ 1, 'asc' ]],
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getEmployeeData')!!}"
            }
        );

    });
    $(document).ready(function() {
    
        $('#search_result').dataTable(
        {
            'sDom': 'lf<"clearfix">tip',
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            /*
            "ajax": {
	            "url": "{!!URL::route('getRecruitmentSearchData')!!}",
	            "type": "POST",
	            "data": {
			        "dep": $('#general_department').val(),
			        "sub_dep": $('#sub_dep').val(),
			        "emp_bast": $('#emp_bast').val(),
			        "ajir_bast": $('#ajir_bast').val(),
			        "military_bast": $('#military_bast').val(),
			        "type": $('#employee_type').val(),
			        "doc": $('#docs').val(),
			        "typee": $('#type').val(),
			        "gender": $('#gender').val(),
			        "edu": $('#education_degree').val(),
			        "year": $('#birth_year').val(),
			        "name": $('#fullname').val()
				 }
            }
            */
            "sAjaxSource": "{!!URL::route('getRecruitmentSearchData',array($dep,$sub,$bast_id,$emp_type,$doc,$type,$gen,$edu,$year))!!}"
        }
        );

    });
function fireDate()
{
    var page = "{!!URL::route('fireDate')!!}";
    
    $.ajax({
        url: page,
        type: 'post',
        data: $('#change_modal_frm').serialize(),
        dataType:'HTML',
        success: function(response)
      	{
        	$('.close').click();
        }
    });   
}
function load_employees(div)
{
    var page = "{!!URL::route('loadEmployees')!!}";
    
    $.ajax({
        url: page,
        type: 'post',
        data: '&type='+div,
        //dataType:'HTML',
        success: function(r)
      	{
        	$('#'+div).html(r);
        }
    });   
}
function load_change_employee(id)
{
	var page = "{!!URL::route('load_change_employee')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id,
        success: function(r){
			$('#change_employee').html(r);
        }
    });
}
function load_promotion_employee(id)
{
	var page = "{!!URL::route('load_promotion_employee')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id,
        success: function(r){
			$('#change_employee').html(r);
        }
    });
}
function load_service_employee(id)
{
	var page = "{!!URL::route('load_service_employee')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id,
        success: function(r){
			$('#change_employee').html(r);
        }
    });
}

function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function showRank(value)
{
    if(value == 3)
    {//military
        $('#ajir_div').slideUp();
        $('#bast_div').slideUp();
        $('#military_div').slideDown();

    }
    else if(value == 2)
    {//ajir
    	$('#ajir_div').slideDown();
        $('#military_div').slideUp();
        $('#bast_div').slideUp();
    }
    else
    {
        $('#ajir_div').slideUp();
        $('#military_div').slideUp();
        $('#bast_div').slideDown();
    }

}
</script>
@stop

