
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postEmployeeDocs',$row->id)!!}" enctype="multipart/form-data">
			 
				@if($attachments)
				<div class="panel-heading">
			      <h5 class="panel-title">فایل های ضمیمه شده</h5>
			    </div>
			    <div class="container-fluid">
	                <div class="row">	         
    					@foreach($attachments AS $doc)	    						
		                	<div class="col-sm-3" id="doc_{!!$doc->id!!}">
		                		<div class="col-sm-12">
		                    		<label class="col-sm-12 ">&nbsp;</label>
		                    		<a href="{!!URL::route('getEmployeeDownload',$doc->id)!!}">{!!$doc->file_name!!}</a>
		                    		@if(canDelete('hr_recruitment'))
		                    		<a href="javascript:void()" onclick="removeEmployeeFile('{!!$doc->id!!}');" class="table-link danger">
                                        <i class="fa fa-trash-o" style='color:red;'></i>
                                    </a>	
                                    @endif
		                    	</div>
		                   </div>
		             	@endforeach			    		
	                </div>
	            </div>
	            @endif
	            <div class="panel-heading">
			      <h5 class="panel-title">ضمیمه نمودن فورم ها</h5>
			    </div>
			    <div class="container-fluid">
	                <div class="row">
	                	<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">فورم تحصیلی</label>
	                    		
	                            <input type='file' id='files'  name='education' class="form-control">
	                    	</div>
	                   </div>
	                   <div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">فورم جنایی</label>
	                            <input type='file' id='files'  name='crime' class="form-control">
	                    	</div>
	                   </div>
	                </div>
	           </div>
	           <div class="container-fluid">
	                <div class="row">
	                	<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">فورم صحی</label>
	                            <input type='file' id='files'  name='health' class="form-control">
	                    	</div>
	                   </div>
	                   <div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">جواز کار</label>
	                            <input type='file' id='files'  name='job' class="form-control">
	                    	</div>
	                   </div>
	                </div>
	           </div>
	           <div class="container-fluid">
	                <div class="row">
	                	<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">تذکره</label>
	                            <input type='file' id='files'  name='tazkira' class="form-control">
	                    	</div>
	                   </div>
	                   <div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">متفرقه</label>
	                            <input type='file' id='files'  name='other' class="form-control">
	                    	</div>
	                   </div>
	                </div>
	           </div>
	           <div class="container-fluid">
	                <div class="row">
	                	<div class="col-sm-4">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">الماری</label>
	                            <select name="rack" class="form-control">
                                    <option value=''>{!!_('select')!!}</option>
                                    @for($i=1;$i<21;$i++)
                                    	@if($filing)
                                    		@if($filing->rack_no==$i)
                                    			<option selected>{!!$i!!}</option>
                                    		@else
                                    			<option>{!!$i!!}</option>
                                    		@endif
                                    	@else
                                    		<option>{!!$i!!}</option>
                                    	@endif
                                    @endfor
                                </select>
	                    	</div>
	                   </div>
	                   <div class="col-sm-4">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">طبقه</label>
	                            <select name="floor" class="form-control">
                                    <option value=''>{!!_('select')!!}</option>
                                    @for($i=1;$i<6;$i++)
                                    	@if($filing)
                                    		@if($filing->floor_no==$i)
                                    			<option selected>{!!$i!!}</option>
                                    		@else
                                    			<option>{!!$i!!}</option>
                                    		@endif
                                    	@else
                                    		<option>{!!$i!!}</option>
                                    	@endif
                                    @endfor
                                </select>
	                    	</div>
	                   </div>
	                   <div class="col-sm-4">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">دوسیه</label>
	                            <input type='text' @if($filing) value="{!!$filing->file_no!!}" @endif  name='file_no' class="form-control">
	                    	</div>
	                   </div>
	                </div>
	           </div>
	           </br>
                <div class="container-fluid">
	                <div class="row">
	                	<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
	                    		@if(canAdd('hr_recruitment'))
		                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        
		                    </div>
                    	</div>
                	</div>
                </div>
                {!! Form::token() !!}
            </form>
        </div>
    </div>

<script type="text/javascript">
function removeEmployeeFile(doc_id)
{
    var confirmed = confirm("Do you want to remove this file?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeEmployeeFile")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                beforeSend: function(){
                    $("#doc_"+doc_id).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#doc_'+doc_id).html(response);
                }
            }
        );
    }

}
</script>

