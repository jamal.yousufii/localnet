<div class="row">
	<div class="col-lg-12">
		@if(canAdd('hr_recruitment'))
		<header class="main-box-header clearfix">
		    <h2>
		    	<a href="javascript:void()" onclick="load_change_employee({!!$id!!})" class="btn btn-primary pull-right" data-target="#change_employee" data-toggle="modal">
					<i class="fa fa-plus-circle fa-lg"></i>
				</a>
		        
		    </h2>
		</header>
		@endif
		<div class="modal-body">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>عنوان بست</th>
                        <th>کارکنان</th>
                        <th>رتبه</th>
                        <th>بست</th>
                        <th>اداره مربوطه</th>
                        
                        <th>تاریخ</th>
                      </tr>
                    </thead>
                    <tbody>
                    @if($changes)
                    <?php $i = 1; ?>
                   		@foreach($changes AS $row)
                   		<tr>
                    	<td>{!!$i!!}</td>
                    	<td>{!!$row->position_title!!}</td>
                    	<td>
                    	@if($row->employee_type==1)
                    	مامور
                    	@elseif($row->employee_type==2)
                    	اجیر
                    	@else
                    	نظامی
                    	@endif
                    	</td>
                    	<td>{!!$row->rank!!}</td>
                    	<td>{!!$row->bast!!}</td>
                    	@if($row->change_type==0)
                    	<td>{!!$row->department!!}</td>
                    	@else
                    	<td>{!!$row->ministry!!}</td>
                    	@endif
                    	<?php
                    	$sdate = $row->changed_date; 
                    	if($sdate!='0000-00-00' && $sdate !=null)
                    	{
                    		$s_date = explode('-', $sdate);
                    		$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);
                    		$sdate = jalali_format($sdate);
                    	}
                    	?>
                    	<td>{!!$sdate!!}</td>
                    	</tr>
                    	<?php $i++; ?>
                    	@endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>

<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>

<div class="md-overlay"></div><!-- the overlay element -->
<script>
	function load_change_employee(id)
	{
		var page = "{!!URL::route('load_change_employee')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&id='+id,
	        success: function(r){
				$('#change_employee').html(r);
	        }
	    });
	}
</script>