
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="@if(canAdd('hr_recruitment')) {!!URL::route('changeEmployeeViaAjax')!!} @endif">
			    <input type="hidden" name="employee_id" value="{!!Crypt::encrypt($details->id)!!}">	 
			    <div class="panel-heading">
			      <h5 class="panel-title">معلومات درباره بست مورد نظر</h5>
			    </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">عنوان بست مورد نظر<span style="color:red">*</span></label>
	                                <input class="form-control" type="text" name="position_title" required value="{!!$details->position_title!!}">
	                                
	                			</div>
	                		</div>
	                		<div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-2 ">کارکنان<span style="color:red">*</span></label>
	                                <select name="employee_type" class="form-control" required onchange="showRank(this.value)">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1' <?php echo ($details->employee_type=='1' ? 'selected':''); ?>>مامور</option>
	                                    <option value='2' <?php echo ($details->employee_type=='2' ? 'selected':''); ?>>اجیر</option>
	                                    <option value='3' <?php echo ($details->employee_type=='3' ? 'selected':''); ?>>نظامی</option>
	                                    <option value='4' <?php echo ($details->employee_type=='4' ? 'selected':''); ?>>مامور بالمقطع</option>
	                                    <option value='5' <?php echo ($details->employee_type=='5' ? 'selected':''); ?>>اجیر بالمقطع</option>
	                                </select>
				               	</div>
				               	
				            </div>
	                		<div id="bast_div1">
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">رتبه</label>
		                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
		                                <select name = "emp_rank" id="emp_rank" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('employee_rank',$details->emp_rank)!!}
		                                </select>
		                                
					      			</div>
					      		</div>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">بست</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "emp_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getBastStaticList('employee_rank',$details->emp_bast)!!}
		                                </select>
					      			</div>
					      		</div>
				      		</div>
				      		<div id="military_div1" style="display:none;">
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">رتبه</label>
		                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
		                                <select name = "military_rank" id="military_rank" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('military_rank',$details->emp_rank)!!}
		                                </select>
		                                
					      			</div>
					      		</div>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">بست</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "military_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('military_rank',$details->emp_bast)!!}
		                                </select>
		                            
					      			</div>
					      		</div>
				      		</div>
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">نوعیت<span style="color:red">*</span></label>
	                                <select name="type" class="form-control" required onchange="showServiceType(this.value)">
	                                	<option value="0">داخل اداره</optioin>
	                                	<option value="1">خارج از اداره</option>
	                                </select>
	                        	</div>
	                        </div>
	                        <div id="internal_div">
		                		<div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">ادارۀ عمومی</label>
		                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('subdep',this.value)">
		                                        <option value="">انتخاب</option>
		                                    @foreach($parentDeps AS $dep_item)
		                                    	@if($dep_item->id == $details->general_department)
		                                        <option value='{!!$dep_item->id!!}' selected="selected">{!!$dep_item->name!!}</option>
		                                        @else
		                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
		                                        @endif
		                                    @endforeach
		                                </select>
		                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		                			</div>
		                		</div>
		                		<div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">ادارۀ مربوط</label>
		                                <select class="form-control" name="sub_dep" id="subdep">
		                                    <option value=''>انتخاب</option>
		                                </select>
		                			</div>
		                		</div>
				      		</div>
				      		<div id="external_div" style="display:none">
	                			<div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">وزارت<span style="color:red">*</span></label>
		                                <select name="ministry" class="form-control">
		                                    <option value="">انتخاب</option>
		                                	{!!getStaticDropdown('ministries')!!}
		                                </select>
		                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		                			</div>
		                		</div>
	                		</div>
				            <div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">موقف بست</label>
	                				<select name = "position_dr" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <option value='1' <?php echo ($details->position_dr=='1' ? 'selected':''); ?>>اصلی</option>
		                                    <option value='2' <?php echo ($details->position_dr=='2' ? 'selected':''); ?>>خدمتی</option>
		                                    <option value='3' <?php echo ($details->position_dr=='3' ? 'selected':''); ?>>تحصیلی</option>
		                                    <option value='4' <?php echo ($details->position_dr=='4' ? 'selected':''); ?>>انتظار معاش</option>
		                                    <option value='5' <?php echo ($details->position_dr=='5' ? 'selected':''); ?>>عسکری</option>
		                                </select>
	                                
	                			</div>
	                		</div>	
	                			                  	                				            				         
				      	</div>
				    </div>
				    <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">تاریخ تبدیلی<span style="color:red">*</span></label>
	                                <input class="form-control datepicker_farsi" required readonly type="text" name="change_date">
	                        	</div>
	                        </div>
	                		<div class="col-sm-3">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">تاریخ حکم</label>
	                                <input class="form-control datepicker_farsi" readonly type="text" name="hokm_date">
	                        	</div>
	                        </div>
	                        <div class="col-sm-3">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">نمبر حکم</label>
	                                <input class="form-control" type="text" name="hokm_no">
	                        	</div>
	                        </div>
	                					            				           
				      	</div>
				    </div>
				    <div class="container-fluid">
	                	<div class="row">
				    		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_recruitment'))
	                					<button class="btn btn-primary" type="submit"><i class="fa fa-refresh fa-lg"></i> تبدیل</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>
	                	</div>
	                </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
<script type="text/javascript">
    $(".datepicker_farsi").persianDatepicker(); 
    function showRank(value)
    {
        if(value == 3)
        {
            //$('#ageer_div').slideUp();
            $('#bast_div1').slideUp();
            $('#military_div1').slideDown();

        }
        else
        {
            //$('#ageer_div').slideUp();
            $('#military_div1').slideUp();
            $('#bast_div1').slideDown();
        }

    }              
    function getProvinceDistrict(page,ele,value)
    {

        $.ajax({
            url: page,
            type: 'post',
            data: 'province='+value,
            dataType: 'html',
            success:function(respones){
                $('#'+ele).html(respones);
            }
        });

    }
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function showServiceType(value)
    {
        if(value == 1)
        {
            //$('#ageer_div').slideUp();
            $('#internal_div').slideUp();
            $('#external_div').slideDown();

        }
        else
        {
            //$('#ageer_div').slideUp();
            $('#external_div').slideUp();
            $('#internal_div').slideDown();
        }

    } 
</script>