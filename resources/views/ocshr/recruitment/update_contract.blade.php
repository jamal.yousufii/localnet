
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('updateEmployeeContract',$id)!!}" enctype="multipart/form-data">
			    <div class="panel-heading">
			      <h5 class="panel-title">معلومات درباره بست مورد نظر</h5>
			    </div>
		    
		    	
                <div class="container-fluid">
                	<div class="row">
                	
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-2 ">کارکنان</label>
                                <select name="employee_type" class="form-control" onchange="showRank(this.value)">
                                    <option value=''>انتخاب</option>
                                    <option value='1' <?php echo ($row->employee_type=='1' ? 'selected':''); ?>>مامور</option>
                                    <option value='2' <?php echo ($row->employee_type=='2' ? 'selected':''); ?>>اجیر</option>
                                    <option value='3' <?php echo ($row->employee_type=='3' ? 'selected':''); ?>>نظامی</option>
                                    <option value='4' <?php echo ($row->employee_type=='4' ? 'selected':''); ?>>مامور بالمقطع</option>
                                    <option value='5' <?php echo ($row->employee_type=='5' ? 'selected':''); ?>>اجیر بالمقطع</option>
                                </select>
			               	</div>
			               	
			            </div>
			      		<div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 ">نوع تقرر</label>
                                <select name = "free_compitition" class="form-control">
                                    <option value=''>انتخاب</option>
                                    @if($row->free_compitition=='جدیدالتقرر')
                                    <option selected value='جدیدالتقرر'>
                                    	جدیدالتقرر
                                    </option>
                                    <option value='سابق'>
                                    	سابق
                                    </option>
                                    <option value='تبدیلی'>
                                    	تبدیلی
                                    </option>
                                    @elseif($row->free_compitition=='سابق')
                                    <option value='جدیدالتقرر'>
                                    	جدیدالتقرر
                                    </option>
                                    <option selected value='سابق'>
                                    	سابق
                                    </option>
                                    <option value='تبدیلی'>
                                    	تبدیلی
                                    </option>
                                    @elseif($row->free_compitition=='تبدیلی')
                                    <option value='جدیدالتقرر'>
                                    	جدیدالتقرر
                                    </option>
                                    <option value='سابق'>
                                    	سابق
                                    </option>
                                    <option selected value='تبدیلی'>
                                    	تبدیلی
                                    </option>
                                    @else
                                    <option value='جدیدالتقرر'>
                                    	جدیدالتقرر
                                    </option>
                                    <option value='سابق'>
                                    	سابق
                                    </option>
                                    <option value='تبدیلی'>
                                    	تبدیلی
                                    </option>
                                    @endif
                                </select>
                                <span style="color:red">{!!$errors->first('free_compitition')!!}</span>
			            	</div>
			            </div>
                	</div>
                </div>
	                
				<div class="container-fluid" >
			      	<div class="row">
			      		<!--
			      		<div class="col-sm-3">
			      			<div class="col-sm-12">
			      				<label class="col-sm-12 ">رتبه علمی</label>
                                <input class="form-control" type="text" name="education_rank" value="{!!Input::old('education_rank')!!}">
                               
			      			</div>
			      		</div>
			      		-->
			      		<div class="col-sm-3">
			      			<div class="col-sm-12">
			      				<label class="col-sm-12 ">موقف کارکن</label>
                                <select name="mawqif_employee" class="form-control">
                                    <option value=''>انتخاب</option>
                                    <option value='1' <?php echo ($row->mawqif_employee=='1' ? 'selected':''); ?>>برحال</option>
                                    <option value='2' <?php echo ($row->mawqif_employee=='2' ? 'selected':''); ?>>انتظار بامعاش</option>
                                    <option value='3' <?php echo ($row->mawqif_employee=='3' ? 'selected':''); ?>>بالمقطع</option>
                                    <option value='4' <?php echo ($row->mawqif_employee=='4' ? 'selected':''); ?>>درجریان تحصیل</option>
                                    <option value='5' <?php echo ($row->mawqif_employee=='5' ? 'selected':''); ?>>وغیره</option>
                                </select>
                               
			      			</div>
			      		</div>
			      		<div class="col-sm-3">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ درخواست</label>
                        		<?php $sdate = $row->first_date_appointment;?>
                                <input class="form-control datepicker_farsi" readonly type="text" required="required" name="first_appointment_date" value="<?php if($row->first_date_appointment !=""){echo $sdate;}?>">
                                <span style="color:red">{!!$errors->first('first_appointment_date')!!}</span>
                        	</div>
                        </div>
                        <div class="col-sm-6">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">وظیفه فعلی</label>
                                <input class="form-control" type="text" name="current_position_dr" id="current_position_dr" value="{!!$row->current_position_dr!!}">
                                <span style="color:red">{!!$errors->first('current_position_dr')!!}</span>
                			</div>
                		</div>
                        <!--
                        <div class="col-sm-3">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ اخرین ترفع</label>
                                <input class="form-control datepicker_farsi" type="text" name="last_appointment_date" value="{!!Input::old('last_appointment_date')!!}">
                        	</div>
                        </div>
                        -->
			      	</div>
			    </div>
			
			<div id="contract" >
		    	<div class="container-fluid">
                	<div class="row">
                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی</label>
                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                    <option value="">انتخاب</option>
                                    @foreach($parentDeps AS $dep_item)
                                    	@if($dep_id == $dep_item->id)	
                                        <option value='{!!$dep_item->id!!}' selected>{!!$dep_item->name!!}</option>
                                        @else
                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ مربوط</label>
                				<select class="form-control" name="sub_dep" id="sub_dep" onchange="getRelatedBasts(this.value)">
                                	<option value="">انتخاب</option>
                                	@if($deps)
                                    @foreach($deps AS $sub)
                                    	@if($sub_dep_id == $sub->id)	
                                        <option value='{!!$sub->id!!}' selected>{!!$sub->name!!}</option>
                                        @else
                                        <option value='{!!$sub->id!!}'>{!!$sub->name!!}</option>
                                        @endif
                                    @endforeach
                                    @endif
                                </select>
                                
                			</div>
                		</div>
                		<div class="col-sm-3">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ ختم قرارداد</label>
                        		<?php $edate = $row->contract_expire_date;?>
                                <input class="form-control datepicker_farsi" readonly type="text" name="last_date" value="<?php if($row->contract_expire_date !=""){echo $edate;}?>">
                        		<span style="color:red">{!!$errors->first('last_date')!!}</span>
                        	</div>
                        </div>
                		
                	</div>
                </div>
			</div>  
			<div class="panel-heading">
				      <h5 class="panel-title">معلومات عمومی در مورد کاندید</h5>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">نام</label>
				            		<input class="form-control" type="text" name="name_dr" value="{!!$row->name_dr!!}">
	                                <span style="color:red">{!!$errors->first('name_dr')!!}</span>
				              		
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">تخلص</lable>
				                  	<input type="text" class="form-control" name="last_name" value="{!!$row->last_name!!}" />
	                                
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">نام پدر</lable>
				                  	<input class="form-control" type="text" name="father_name_dr" value="{!!$row->father_name_dr!!}">
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">پدر کلان</lable>
				            		<input type="text" class="form-control" name="grand_father_name" value="{!!$row->grand_father_name!!}"/>
				               	</div>
				            </div>
				      	</div>
				    </div>
					<div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">نمبر تذکره</label>
				            		<input class="form-control" type="text" name="id_no" value="{!!$row->id_no!!}">
	                                <span style="color:red">{!!$errors->first('id_no')!!}</span>
				              		
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">جلد</lable>
				                  	<input type="text" class="form-control" name="id_jild" value="{!!$row->id_jild!!}"/>	                                
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">صفحه</lable>
				                  	<input class="form-control" type="text" name="id_page" value="{!!$row->id_page!!}">
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">ثبت</lable>
				            		<input type="text" class="form-control" name="id_sabt" value="{!!$row->id_sabt!!}"/>
				               	</div>
				            </div>
				      	</div>
				    </div>  
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">قوم</label>
				            		<select name = "nationality" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getStaticDropdown('ethnicity',$row->nationality)!!}
	                                </select>
				              		
				               	</div>
				            </div>
				      		
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">جنسیت</label>
	                                
	                                <select name="gender" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='M' <?php echo ($row->gender=='M' ? 'selected':''); ?>>مرد</option>
	                                    <option value='F' <?php echo ($row->gender=='F' ? 'selected':''); ?>>زن</option>
	                                </select>
	                                
				            	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">گروپ خون</label>
	                                <select name = "blood_group" id="blood_group" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getStaticDropdown('blood_group',$row->blood_group)!!}
	                                </select>
				            	</div>
				            </div>
				            <div class="col-sm-3">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">سال تولد</lable>
	                                <?php 
	                                     $current_year = date('Y')-621;
	                                     
	                                ?>
	                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
	                                <select name = "birth_year" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <?php 
	                                       
	                                        for($i=$current_year-84;$i<=$current_year-10;$i++)
	                                        {
	                                        	if($i == $row->birth_year)
												{	                                            
	                                            	echo "<option value='".$i."' selected='selected'>".$i."</option>";
												}
												else
												{
													echo "<option value='".$i."'>".$i."</option>";
												}
	                                   }
	                                    ?>
	                                </select>
				               	</div>
				            </div>
				      	</div>
				    </div> 
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">&nbsp;</label>
				      				<h5>سکونت اصلی</h5>
				      			</div>
				      		</div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ولایت</label>
				                  	<?php $page=URL::route('getProvinceDistrict'); ?>
	                                <select class="form-control" name="original_province" id="original_province" onchange="getProvinceDistrict('{!!$page!!}','districts',this.value);">
	                                    <option value=''>انتخاب</option>
	                                    @foreach($provinces AS $pro)
	                                    	@if($pro->id == $row->original_province)
	                                        	<option value='{!!$pro->id!!}' selected="selected">{!!$pro->name!!}</option>
	                                        @else
	                                        	<option value='{!!$pro->id!!}'>{!!$pro->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ولسوالی</lable>
				              		<select name = "district" id="districts" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    @foreach($org_districts AS $org_dist)
	                                    	@if($org_dist->id == $row->district)
	                                        	<option value='{!!$org_dist->id!!}' selected="selected">{!!$org_dist->name!!}</option>
	                                        @else
	                                        	<option value='{!!$org_dist->id!!}'>{!!$org_dist->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>
				                  	
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ناحیه</lable>
				                  	<input type="text" class="form-control" name="village" value="{!!$row->village!!}" />
				               	</div>
				            </div>
				      	</div>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				      		<div class="col-sm-12">
				      				<label class="col-sm-12 ">&nbsp;</label>
				      				<h5>سکونت فعلی</h5>
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ولایت</label>
				                  	<?php $page=URL::route('getProvinceDistrict'); ?>
	                                <select class="form-control" name="current_province" id="current_province" onchange="getProvinceDistrict('{!!$page!!}','current_districts',this.value);">
	                                    <option value=''>انتخاب</option>
	                                    @foreach($provinces AS $pro)
	                                        @if($pro->id == $row->current_province)
	                                        	<option value='{!!$pro->id!!}' selected="selected">{!!$pro->name!!}</option>
	                                        @else
	                                        	<option value='{!!$pro->id!!}'>{!!$pro->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ولسوالی</lable>
				              		<select name = "current_district" id="current_districts" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    @foreach($cur_districts AS $cur_dist)
	                                    	@if($cur_dist->id == $row->current_district)
	                                        	<option value='{!!$cur_dist->id!!}' selected="selected">{!!$cur_dist->name!!}</option>
	                                        @else
	                                        	<option value='{!!$cur_dist->id!!}'>{!!$cur_dist->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>
				                  
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ناحیه</lable>
				                  	<input type="text" class="form-control" name="current_village" value="{!!$row->current_village!!}" />
				               	</div>
				            </div>
				            
				      	</div>
				    </div>
				    <?php $edu_count =0;?>
				    @if($educations)
				    	@foreach($educations as $edu)
				    	<?php $edu_count++;?>
						    <div class="container-fluid">
						      	<div class="row">
						      		<div class="col-sm-2">
						              	<div class="col-sm-12">
						              		<label class="col-sm-12 ">سویه تحصیلی</label>
						            		<select name = "education_degree_{!!$edu_count!!}" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    {!!getStaticDropdown('education_degree',$edu->education_id)!!}
			                                </select>
						               	</div>
						            </div>
						      		
						            <div class="col-sm-2">
						            	<div class="col-sm-12">
						            		<label class="col-sm-12 ">رشته تحصیلی</label>
			                                <input class="form-control" type="text" name="education_field_{!!$edu_count!!}" value="{!!$edu->education_field!!}">
						            	</div>
						            </div>
						            <div class="col-sm-3">
						            	<div class="col-sm-12">
						              		<label class="col-sm-12 ">موسسه تحصیلی</lable>
						                  	<input type="text" class="form-control" name="education_place_{!!$edu_count!!}" value="{!!$edu->education_place!!}"/>
						               	</div>
						            </div>
						            <div class="col-sm-2">
						            	<div class="col-sm-12">
						            		<label class="col-sm-12 ">محل تحصیل</label>
			                                <select name = "edu_location_{!!$edu_count!!}" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    {!!getStaticDropdown('countries',$edu->edu_location)!!}
			                                </select>
						            	</div>
						            </div>
						            <div class="col-sm-2">
						            	<div class="col-sm-12">
						              		<label class="col-sm-12 ">سال فراغت</label>
						                  	<?php 
			                                     $current_year = date('Y')-621;
			                                     
			                                ?>
			                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
			                                <select name = "graduation_year_{!!$edu_count!!}" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    <?php 
			                                       
			                                        for($i=$current_year-84;$i<=$current_year;$i++)
			                                        {	                                            
			                                            if($i == $edu->graduation_year)
														{	                                            
			                                            	echo "<option value='".$i."' selected='selected'>".$i."</option>";
														}
														else
														{
															echo "<option value='".$i."'>".$i."</option>";
														}
			                                   }
			                                    ?>
			                                </select>
						               	</div>
						            </div>				            				            
						      	</div>
						    </div>
						@endforeach
						@if($edu_count<2)
							<div class="container-fluid">
						      	<div class="row">
						      		<div class="col-sm-2">
						              	<div class="col-sm-12">
						              		<label class="col-sm-12 ">سویه تحصیلی</label>
						            		<select name = "education_degree_2" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    {!!getStaticDropdown('education_degree',0)!!}
			                                </select>
						               	</div>
						            </div>
						      		
						            <div class="col-sm-2">
						            	<div class="col-sm-12">
						            		<label class="col-sm-12 ">رشته تحصیلی</label>
			                                <input class="form-control" type="text" name="education_field_2" value="">
						            	</div>
						            </div>
						            <div class="col-sm-3">
						            	<div class="col-sm-12">
						              		<label class="col-sm-12 ">موسسه تحصیلی</lable>
						                  	<input type="text" class="form-control" name="education_place_2" value=""/>
						               	</div>
						            </div>
						            <div class="col-sm-2">
						            	<div class="col-sm-12">
						            		<label class="col-sm-12 ">محل تحصیل</label>
			                                <select name = "edu_location_2" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    {!!getStaticDropdown('countries',0)!!}
			                                </select>
						            	</div>
						            </div>
						            <div class="col-sm-2">
						            	<div class="col-sm-12">
						              		<label class="col-sm-12 ">سال فراغت</label>
						                  	<?php 
			                                     $current_year = date('Y')-621;
			                                     
			                                ?>
			                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
			                                <select name = "graduation_year_2" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    <?php 
			                                       
		                                        for($i=$current_year-84;$i<=$current_year;$i++)
		                                        {	                                            
		                                            echo "<option value='".$i."'>".$i."</option>";
			                                   	}
			                                    ?>
			                                </select>
						               	</div>
						            </div>				            				            
						      	</div>
						    </div>
						@endif
				    @else
				    	<div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-2">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">سویه تحصیلی</label>
					            		<select name = "education_degree_1" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('education_degree',0)!!}
		                                </select>
					               	</div>
					            </div>
					      		
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					            		<label class="col-sm-12 ">رشته تحصیلی</label>
		                                <input class="form-control" type="text" name="education_field_1" value="">
					            	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">موسسه تحصیلی</lable>
					                  	<input type="text" class="form-control" name="education_place_1" value=""/>
					               	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					            		<label class="col-sm-12 ">محل تحصیل</label>
		                                <select name = "edu_location_1" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('countries',0)!!}
		                                </select>
					            	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">سال فراغت</label>
					                  	<?php 
		                                     $current_year = date('Y')-621;
		                                     
		                                ?>
		                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
		                                <select name = "graduation_year_1" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <?php 
		                                       
	                                        for($i=$current_year-84;$i<=$current_year;$i++)
	                                        {	                                            
	                                            echo "<option value='".$i."'>".$i."</option>";
		                                   	}
		                                    ?>
		                                </select>
					               	</div>
					            </div>				            				            
					      	</div>
					    </div>
					    <div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-2">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">سویه تحصیلی</label>
					            		<select name = "education_degree_2" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('education_degree',0)!!}
		                                </select>
					               	</div>
					            </div>
					      		
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					            		<label class="col-sm-12 ">رشته تحصیلی</label>
		                                <input class="form-control" type="text" name="education_field_2" value="">
					            	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">موسسه تحصیلی</lable>
					                  	<input type="text" class="form-control" name="education_place_2" value=""/>
					               	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					            		<label class="col-sm-12 ">محل تحصیل</label>
		                                <select name = "edu_location_2" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('countries',0)!!}
		                                </select>
					            	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">سال فراغت</label>
					                  	<?php 
		                                     $current_year = date('Y')-621;
		                                     
		                                ?>
		                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
		                                <select name = "graduation_year_2" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <?php 
		                                       
	                                        for($i=$current_year-84;$i<=$current_year;$i++)
	                                        {	                                            
	                                            echo "<option value='".$i."'>".$i."</option>";
		                                   	}
		                                    ?>
		                                </select>
					               	</div>
					            </div>				            				            
					      	</div>
					    </div>
				    @endif
				    
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-6">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">تیلفون</label>
				            		<input class="form-control" type="text" name="phone" value="{!!$row->phone!!}">
				               	</div>
				            </div>
				      		
				            <div class="col-sm-6">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">آدرس الکترونیکی</label>
				                  	<input type="text" name="email" class="form-control" value="{!!$row->email!!}">
				               	</div>
				            </div>
				            
				            
				      	</div>
				    </div>	    
	            
				<br/>
                <div class="container-fluid">
	                <div class="row">
	                	<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
	                    		@if(canEdit('hr_recruitment'))
		                        <button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    	<p>You dont have permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
		                    </div>
                    	</div>
                	</div>
                </div>
                {!! Form::token() !!}
            </form>
        </div>
    </div>

<script type="text/javascript">
$(".datepicker_farsi").persianDatepicker();
    $(document).on("change","#files",function(){
        var count = $('#files_div').find('input').length;
        count = count+1;
        $("<div id='div_"+count+"' style='display:inline;'><input style='splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div');
    });
	function getRelatedBasts(value)
    {
    	var type = '{!!$row->tashkil_id!!}';
    	$.ajax({
            url: '{!!URL::route("bringRelatedBastViaAjax")!!}',
            type: 'post',
            data: 'id='+value+'&type='+type,
            dataType: 'html',
            success:function(respones){
                $('#tashkil').html(respones);
            }
        });
    }
	function bringTashkilDet(value)
    {
        $.ajax({
            url: '{!!URL::route("bringTashkilDetViaAjax")!!}',
            type: 'post',
            data: '&id='+value,
            dataType: 'json',
            success:function(respones){
                $('#tashkil_bast').val(respones.bast);
                $('#number_tayenat').val(respones.number);
               //$('#tashkil_sub_dep').val(respones.sub_dep);
            }
        });
    }  
    function changeDiv(value)
    {
        if(value==1)
        {
            $('#vacant_div').slideDown()
            $('#exist_div').slideUp()
        }
        else
        {
            $('#vacant_div').slideUp()
            $('#exist_div').slideDown()
        }
    }
    function showRank(value)
    {
        if(value == 3)
        {//military
            $('#ajir_div').slideUp();
            $('#bast_div').slideUp();
            $('#military_div').slideDown();

        }
        else if(value == 2)
        {//ajir
        	$('#ajir_div').slideDown();
            $('#military_div').slideUp();
            $('#bast_div').slideUp();
        }
        else
        {
            $('#ajir_div').slideUp();
            $('#military_div').slideUp();
            $('#bast_div').slideDown();
        }

    }                
    function getProvinceDistrict(page,ele,value)
    {

        $.ajax({
            url: page,
            type: 'post',
            data: 'province='+value,
            dataType: 'html',
            success:function(respones){
                $('#'+ele).html(respones);
            }
        });

    }
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function saveEmployeeNewTashkil(id)
    {
    	var no 		= $('#number_tayenat').val();
    	var dep 	= $('#general_department').val();
    	var sub_dep = $('#sub_dep').val();
    	var tashkil = $('#tashkil').val();
    	var position= $('#current_position_dr').val();
        $.ajax({
                url: '{!!URL::route("saveEmployeeNewTashkil")!!}',
                data: '&no='+no+'&dep='+dep+'&sub_dep='+sub_dep+'&tashkil='+tashkil+'&id='+id+'&position='+position,
                type: 'post',
             
                success: function(r)
                {
                    alert('updated');return;
                }
            }
        );
    }
</script>

