@extends('layouts.master')

@section('head')

{!! HTML::style('/css/tasks/persian_datepicker.css') !!}
{!! HTML::script('/js/tasks/persian_datepicker.js')!!}
{!! HTML::style('/vendor/select2/select2.css') !!}
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>کارمندان</span></li>
        </ol>
    </div>
</div>

<!-- Example Tabs -->
<div class="example-wrap">
	<div class="nav-tabs-horizontal">
	  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
	    <li class="active" role="presentation"><a data-toggle="tab" href="#all" aria-controls="exampleTabsOne"
	      role="tab">تمام کارمندان</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('maktob')" href="#other" aria-controls="exampleTabsOne"
	      role="tab">مکتوب اخذ کارت</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('estelam')" href="#other" aria-controls="exampleTabsTwo"
	      role="tab">استعلام ها</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('security')" href="#other" aria-controls="exampleTabsThree"
	      role="tab">مکتوب شورای امنیت</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('protection')" href="#other" aria-controls="exampleTabsThree"
	      role="tab">مکتوب محافظت</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('cards')" href="#other" aria-controls="exampleTabsThree"
	      role="tab">کارت ها</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('complain')" href="#other" aria-controls="exampleTabsThree"
	      role="tab">شکایات</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('comitte')" href="#other" aria-controls="exampleTabsThree"
	      role="tab">کمیته حل شکایات</a></li>
	   
	  </ul>
	  <div class="tab-content padding-top-20">
	    <div class="tab-pane active" id="all" role="tabpanel">
	    	<div class="row">
			    <div class="col-lg-12">
			        <div class="main-box">
			            <div class="main-box-body clearfix">
			            <div class="table-responsive">
			                <table class="table table-responsive" id='list'>
			                    <thead>
			                    <tr>
			                        <th>#</th>
			                        <th>نام کامل</th>
			                        <th>ولد</th>
			                        <th>وظیفه</th>
			                        <th>ریاست</th>
			                        
			                        <th>عملیه</th>
			                    </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>
			            </div>
			        
			            </div>
			        </div>
			    </div>
			</div>
	    </div>
	    <div class="tab-pane" id="other" role="tabpanel">
	    	لیست
	    </div>
	    
	  </div>
	</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="estelam_modal" aria-hidden="true" aria-labelledby="estelam_modal" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postComEstelam')!!}">
                <div class="container-fluid">
                	<div class="row">
                		<div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">نام شخص</label>
                                <input class="form-control" type="text" id="employee" value="" readonly="readonly">
                                <input type="hidden" id="employee_id" name="employee_id" value=""/>
                        	</div>
                        </div>
			      		 <div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">شماره استعلام</label>
                                <input class="form-control" type="text" name="number">
                        	</div>
                        </div>
			      		<div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ</label>
                                <input class="form-control datepicker_farsi" type="text" name="date">
                        	</div>
                        </div>
                	</div>
                </div>
	            
				    <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_documents'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>			            				           
				      	</div>
				    </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="card_modal" aria-hidden="true" aria-labelledby="estelam_modal" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postComEmployeeCard')!!}">
                <div class="container-fluid">
                	<div class="row">
                		<div class="col-sm-3">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">شماره کارت</label>
                                <input class="form-control" type="text" name="card_no">
                                <input type="hidden" id="employee_card" name="employee_card" value=""/>
                        	</div>
                        </div>
			      		 <div class="col-sm-3">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">رنگ کارت</label>
                                <input class="form-control" type="text" name="card_color">
                        	</div>
                        </div>
			      		<div class="col-sm-3">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ توزیع</label>
                                <input class="form-control datepicker_farsi" type="text" name="issue_date">
                        	</div>
                        </div>
                        <div class="col-sm-3">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ انقضا</label>
                                <input class="form-control datepicker_farsi" type="text" name="expiry_date">
                        	</div>
                        </div>
                	</div>
                </div>
	            <div class="container-fluid">
                	<div class="row">
                		<div class="col-sm-6">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">فایل</label>
                                <input class="form-control" type="file" name="scan">
                        	</div>
                        </div>
                        <div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
                				@if(canAdd('hr_communication'))
                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
                				@else
                					<p>You dont have permission</p>
                				@endif
                			</div>
                		</div>	
                	</div>
                </div>
				    
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="md-overlay"></div>
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {
				"order": [[ 1, 'asc' ]],
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getEmployeeComData')!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });

function load_employees(div)
{
    var page = "{!!URL::route('loadEmployees')!!}";
    
    $.ajax({
        url: page,
        type: 'post',
        data: '&type='+div,
        //dataType:'HTML',
        success: function(r)
      	{
        	$('#other').html(r);
        }
    });   
}

</script>
@stop

