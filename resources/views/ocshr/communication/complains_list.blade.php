@section('head')
{!! HTML::style('/css/tasks/persian_datepicker.css') !!}
{!! HTML::script('/js/tasks/persian_datepicker.js')!!}
@stop
<div class="row">
    <div class="col-lg-12">
    	<header class="main-box-header clearfix">
		    <h2>
		    	<a href="javascript:void()" class="btn btn-primary pull-right" data-target="#security_modal" data-toggle="modal">
					<i class="fa fa-plus-circle fa-lg"></i>
				</a>
		    </h2>
		</header>
        <div class="main-box">
            
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='maktobs'>
                    <thead>
                    <tr>
                        
                        <th>شماره مسلسل</th>
                        <th>تاریخ ثبت شکایت</th>
                        <th>مورد شکایت</th>
                        
                        <th>عملیه</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-fade-in-scale-up" id="security_modal" aria-hidden="true" aria-labelledby="security_modal" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postComComplain')!!}">
                <div class="container-fluid">
                	<div class="row">
			      		 
			      		<div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 "> تاریخ ثبت شکایت</label>
                                <input class="form-control datepicker_farsi" type="text" name="date">
                        	</div>
                        </div>
                        <div class="col-sm-8">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">مورد شکایت</label>
                                <textarea row="3" class="form-control" name="desc"></textarea>
                        	</div>
                        </div>
                    </div>
                </div>
                
	                
				    <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_documents'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>			            				           
				      	</div>
				    </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="edit_complain_modal" aria-hidden="true" aria-labelledby="security_modal" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postComComitte')!!}">
                <div class="container-fluid">
                	<div class="row">
			      		<input type="hidden" id="complain_id" name="complain_id" value=""/>
			      		<div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 "> تاریخ ارجاع موضوع به کمیته حل شکایات</label>
                                <input class="form-control datepicker_farsi" type="text" name="date">
                        	</div>
                        </div>
                        <div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 "> شماره</label>
                                <input class="form-control" type="text" name="number">
                        	</div>
                        </div>
                        <div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
                				@if(canAdd('hr_documents'))
                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
                				@else
                					<p>You dont have permission</p>
                				@endif
                			</div>
                		</div>	
                    </div>
                </div>
                
	                
				    
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="md-overlay"></div>
<script type="text/javascript">
$(".datepicker_farsi").persianDatepicker(); 
    $(document).ready(function() {
        $('#maktobs').dataTable(
            {
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getComComplaints')!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
</script>

