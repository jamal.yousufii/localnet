@section('head')
{!! HTML::style('/css/tasks/persian_datepicker.css') !!}
{!! HTML::script('/js/tasks/persian_datepicker.js')!!}
@stop
<div class="row">
    <div class="col-lg-12">
    	
        <div class="main-box">
            
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='comitte'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>شماره</th>
                        <th>تاریخ  ارجاع به کمیته</th>
                        <th>مورد شکایت</th>
                        
                        <th>عملیه</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>


<div class="modal fade modal-fade-in-scale-up" id="comitte_modal" aria-hidden="true" aria-labelledby="security_modal" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postComComitteResult')!!}">
                <div class="container-fluid">
                	<div class="row">
			      		<input type="hidden" id="comitte_id" name="comitte_id" value=""/>
			      		<div class="col-sm-8">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">فیصله کمیته</lable>
                                <textarea row="3" class="form-control" name="result"></textarea>
                        	</div>
                        </div>
                        
                        <div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
                				@if(canAdd('hr_communication'))
                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
                				@else
                					<p>You dont have permission</p>
                				@endif
                			</div>
                		</div>	
                    </div>
                </div>
                
	                
				    
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="md-overlay"></div>
<script type="text/javascript">
$(".datepicker_farsi").persianDatepicker(); 
    $(document).ready(function() {
        $('#comitte').dataTable(
            {
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getComComitte')!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
</script>

