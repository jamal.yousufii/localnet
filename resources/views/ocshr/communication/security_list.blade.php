<style>
	.select2-dropdown{
		z-index: 100000;
	}
</style>
@section('head')

{!! HTML::style('/css/tasks/persian_datepicker.css') !!}
{!! HTML::script('/js/tasks/persian_datepicker.js')!!}

@stop
<div class="row">
    <div class="col-lg-12">
    	<header class="main-box-header clearfix">
		    <h2>
		    	<a href="javascript:void()" class="btn btn-primary pull-right" data-target="#security_modal" data-toggle="modal">
					<i class="fa fa-plus-circle fa-lg"></i>
				</a>
		    </h2>
		</header>
        <div class="main-box">
            
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='securities'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>شماره</th>
                        <th>تاریخ</th>
                        <th>فایل</th>
                        
                        <th>عملیه</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-fade-in-scale-up" id="security_modal" aria-hidden="true" aria-labelledby="security_modal" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postComSecurity','0')!!}" enctype="multipart/form-data">
                <div class="container-fluid">
                	<div class="row">
			      		 <div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">شماره</label>
                                <input class="form-control" type="text" name="number">
                        	</div>
                        </div>
			      		<div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ</label>
                                <input class="form-control datepicker_farsi" type="text" name="date">
                        	</div>
                        </div>
                        <div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">فایل</label>
                                <input class="form-control" type="file" name="scan">
                        	</div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                	<div class="row">
                        <div class="col-sm-8">
                        	<div class="col-sm-12">
                        	<label class="col-sm-12 ">کارمندان</label>
	                        <select class="form-control" style="width:100%" name='employees[]' id='assign_with2' multiple="multiple" data-plugin="select2">
	                        <?php 
	                            foreach($users AS $uitem)
	                            {
	                                $selected = "";
	                                
	                                echo "<option ".$selected." value='".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
	                            }
	                        ?>
		                    </select>
		                    </div>
	                    </div>
                        <!--
                       <div class="col-sm-3">
			      			<div class="col-sm-12">
			      				<label class="col-sm-12 ">مرجع</label>
                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment1('subdep_security',this.value)">
                                	<option value="">انتخاب</option>
                                    @foreach($parentDeps AS $dep_item)
                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                    @endforeach
                                </select>
			      			</div>
			      		</div>
			      		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ مربوط</label>
                                <select class="form-control" name="sub_dep" id="subdep_security">
                                    <option value=''>انتخاب</option>
                                </select>
                			</div>
                		</div>
                		-->
                	</div>
                </div>
	                
				    <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_documents'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>			            				           
				      	</div>
				    </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="accept_modal" aria-hidden="true" aria-labelledby="security_modal" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postComSecurityAccept')!!}" enctype="multipart/form-data">
                <div class="container-fluid">
                	<div class="row">
			      		 <div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">شماره</label>
                                <input class="form-control" type="text" name="number">
                        	</div>
                        </div>
			      		<div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ</label>
                                <input class="form-control datepicker_farsi" type="text" name="date">
                        	</div>
                        </div>
                        <div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">فایل</label>
                                <input class="form-control" type="file" name="scan">
                        	</div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="security_id" name="security_id" value="" />
	                
				    <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_documents'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>			            				           
				      	</div>
				    </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="md-overlay"></div>
<script type="text/javascript">
$("#assign_with2").select2();
$(".datepicker_farsi").persianDatepicker(); 
    $(document).ready(function() {
        $('#securities').dataTable(
            {
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getComEmplyeeSecurity')!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });
 function bringRelatedSubDepartment1(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }   
</script>

