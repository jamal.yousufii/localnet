@extends('layouts.master')

@section('head')

    <title>All Employees</title>
@stop
@section('content')
<div class="row no-print">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">لوحه معلومات</a></li>
            <li class="active"><span>کارمندان</span></li>
        </ol>

        <h1>جستجوی کارمندان نظر به اداره</h1>
    </div>
</div>

<div class="row no-print">
    <div class="col-lg-12">
        <div class="main-box">
            <header class="main-box-header clearfix no-print">
            </header>
            <div class="main-box-body clearfix">
                <form id="search_frm" role="form" enctype="multipart/form-data">
                    <div class="col-sm-4">
                        <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                            <option value="">اداره عمومی</option>
                            @foreach($parentDeps AS $dep_item)
                                <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <select class="form-control" name="sub_dep" id="sub_dep">
                            <option value=''>اداره مربوطه</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <div class="checkbox-nice">
                            <input value='1' type="checkbox" id="checkbox-1" name="show_vacant">
                            <label for="checkbox-1">
                                Show Vacant
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a href="#" class="btn btn-primary" onclick="getDepartmentEmployees('result_div')"><i class="fa fa-search fa-lg"></i> جستجو</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row" style="display:none;" id="main_result">
    <div class="col-lg-12">
        <div class="main-box">
            <header class="main-box-header clearfix no-print">
                <button onclick="window.print()" class="btn btn-primary no-print pull-right">Print Preview</button>
            </header>
            <div class="main-box-body clearfix">
                
                <div class="table-responsive" id="result_div">
                
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        
        // $('#list').dataTable(
        //     {

        //         'sDom': 'T<"clear">lfrtip',
        //         //'sDom': 'lf<"clearfix">tip',
        //         "bProcessing": true,
        //         "bServerSide": true,
        //         "iDisplayLength": 1000,
        //         "scrollX": false,
        //         "oTableTools": {
        //             "aButtons": [
        //                 {
        //                     "sExtends": "print",
        //                     "bShowAll": false,
        //                     "mColumns": [0,2, 3, 4]
        //                 }
        //             ]
        //         },
        //         "sAjaxSource": "{!!URL::to('/hr/getEmployeeDataPrint')!!}",
        //         //"aaSorting": [[ 1, "desc" ]],
        //         "aoColumns": [
                    
        //             { 'sWidth': '80px', 'sClass': 'center' },
        //             { 'sWidth': '50px', 'sClass': 'center' },
        //             { 'sWidth': '50px', 'sClass': 'center' },
        //             { 'sWidth': '50px', 'sClass': 'center' },
        //             { 'sWidth': '50px', 'sClass': 'center' }
        //         ],
        //         "language": {
        //             "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
        //             "zeroRecords": "ریکارد موجود نیست",
        //             "info": "نمایش صفحه _PAGE_ از _PAGES_",
        //             "infoEmpty": "ریکارد موجود نیست",
        //             "search": "جستجو",
        //             "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
        //         }
        //     }
        // );
    
        $('#list_filter').addClass('no-print');
        $('#list_length').addClass('no-print');
        $('#list_info').addClass('no-print');
        $('#list_paginate').addClass('no-print');
        $('.sorting_1').addClass('no-print');

    });

function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function getDepartmentEmployees(div)
{
    $.ajax({
            url: '{!!URL::route("searchDepartmentEmployees")!!}',
            data: $('#search_frm').serialize(),
            type: 'post',
            beforeSend: function(){
                $('#main_result').show();
                //$("body").show().css({"opacity": "0.5"});
                $("#"+div).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#main_result').show();
                $('#'+div).html(response);
            }
        }
    );

    return false;
    
}
</script>
@stop

