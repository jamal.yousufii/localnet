@extends('layouts.master')

@section('head')
    <title>{!!_('all_employees')!!}</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>کارمندان</span></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>کارمندان
                    <a href="{!!URL::route('getRegisterForm')!!}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle fa-lg"></i> کارمند جدید</a>
                    @if(Auth::user()->id==114 || Auth::user()->id==113)
                    <div style="display: inline;margin: 1px;">
			        <a href="{!!URL::route('copyToAttendanceServer')!!}" class="btn btn-primary pull-right">Copy Employees to attendance server</a>
			        </div>
			        <form role="form" class='form-horizontal' method="post" action="{!!URL::route('copyToAttendance')!!}">
			            <div class="form-group">
			                <div class="col-sm-2">
			                    <input class="form-control" type="text" name="id_count">
			                    <button type="submit" class="btn btn-primary modal-submit">Copy Employees to CSV</button>
			                </div>
			            </div>
			      	</form>
			        
			        @endif
                </h2>
            </header>

            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='list'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>نام کامل</th>
                        <th>ولد</th>
                        <th>شماره تعینات</th>
                        <th>جنسیت</th>
                        <th>سکونت اصلی</th>
                        <th>بست</th>
                        <th>تاریخ تقرر</th>
                        <th>عملیه</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {

                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getEmployeeData')!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });
</script>
@stop

