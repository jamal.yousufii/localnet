@extends('layouts.master')

@section('head')
    <title>All Employees</title>
    {!! HTML::style('/css/template/libs/nifty-component.css') !!}
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">لوحه معلومات</a></li>
            <li><span>کارمندان</span></li>
            <li class="active"><span>کارت هویت کارمندان</span></li>
        </ol>

        
        
    </div> 
</div>

<div class="row">
@if(Session::has('success'))
	<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif
    <div class="col-lg-12">
        <div class="main-box">
            
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='list'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>نام کامل</th>
                        <th>ولد</th>
                        <th>شماره تعینات</th>
                        <th>جنسیت</th>
                        <th>سکونت اصلی</th>
                        <th>رتبه</th>
                        <th>تاریخ تقرر</th>
                        <th>عملیه</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="rfid_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Enter RFID</h4>
            </div>
            <div class="modal-body">
                <form class="comment_form" method="post" action="{!!URL::route('saveRFID')!!}" class="form-horizontal">
                
                    <input type="hidden" name="record_id" id = "record_id">
                    <div class="form-group">
                        <input type="text" class="form-control" name="RFID" placehloder="Enter card RFID...">
                    </div>
                
                    <button type="submit" type="button" class="btn btn-primary">
                        <i class="fa fa-save fa-lg"></i> Save</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {
                "fnDrawCallback" : function() {
                    $(document).trigger('doc:updated');
                },
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getEmployeeCardData')!!}",
                "aaSorting": [[ 1, "desc" ]],
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });

function sendBack(id)
{

    if(confirm("Send Back For Update Card?"))
    {
        $.ajax({
            url:'{!!URL::route("checkCardReady")!!}',
            type:'post',
            data:'ready=0'+'&id='+id,
            //dataType:'json',
            success:function(r)
            {
                location.reload();
            }
        });
    }
    else
    {
        return false;
    }
   
}

</script>
@stop

