
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postEmployeeExperiences',$row->id)!!}" enctype="multipart/form-data">
			    <?php if($row->employee_type==2){$rank = 'درجه';}else{$rank='رتبه';} ?>
					<div class="panel-heading">
				      <h5 class="panel-title">معلومات در مورد تجارب کاری</h5>
				    </div>
				    @if($experiences)
				    <?php $i = 0;$experience_number = 0; ?>
				    @foreach($experiences AS $exp)
				    <?php $i++;?>
				    <div id="files_{!!$i!!}">
					    <div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-3">
		                        	<div class="col-sm-12">
		                        		<label class="col-sm-12 ">ارگان / موسسه</label>
		                        		<input class="form-control" type="text" name="experience_company_{!!$i!!}" value="{!!$exp->organization!!}" required="required">
		                        	</div>
		                        </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">عنوان بست</label>
					                  	<input type="text" class="form-control" name="experience_position_{!!$i!!}" value="{!!$exp->position!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">بست</lable>
					                  	<input type="text" class="form-control" name="experience_bast_{!!$i!!}" value="{!!$exp->bast!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">{!!$rank!!}</lable>
					                  	<input type="text" class="form-control" name="experience_rank_{!!$i!!}" value="{!!$exp->rank!!}" />
					               	</div>
					            </div>
					      	</div>
					    </div>
					    
					    <div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-3">
		                        	<div class="col-sm-12">
		                        		<label class="col-sm-12 ">از</label>
		                        		<?php $sdate = $exp->date_from; if($sdate!=''){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
		                                <input class="form-control datepicker_farsi" type="text" name="date_from_{!!$i!!}" value='<?php if($exp->date_from!=""){echo jalali_format($sdate);}?>'>
		                        	</div>
		                        </div>
		                        <div class="col-sm-3">
		                        	<div class="col-sm-12">
		                        		<label class="col-sm-12 ">تا</label>
		                        		<?php $edate = $exp->date_to; if($edate!=''){$e_date = explode('-', $edate);$edate = dateToShamsi($e_date[0], $e_date[1], $e_date[2]);}?>
		                                <input class="form-control datepicker_farsi" type="text" name="date_to_{!!$i!!}" value='<?php if($exp->date_to!=""){echo jalali_format($edate);}?>'>
		                        	</div>
		                        </div>
		                        <div class="col-sm-5">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">دلایل ترک وظیفه</lable>
					                  	<input type="text" class="form-control" name="experience_leave_{!!$i!!}" value="{!!$exp->leave_reason!!}" />
					               	</div>
					            </div>
					            <div class="col-md-1">
					            	<div class="col-sm-12">
					              		<label class="col-sm-1 ">&nbsp;</lable>
					              		@if($i==1)
					              			@if(canAdd('hr_documents'))
												<i class="icon wb-plus-circle" aria-hidden="true" style="font-size: 25px;cursor: pointer;" onclick="add_file('file')"></i>
											@endif
										@else
											@if(canDelete('hr_documents'))
												<i class="icon wb-minus-circle" aria-hidden="true" style="font-size: 25px;cursor: pointer;" onclick="remove_file({!!$i!!},'file')"></i>
											@endif
										@endif
									</div>
								</div>
					      	</div>
					    </div>
					</div>
					<?php $experience_number++; ?>
				    @endforeach
				    <input type="hidden" id="total_files" value="{!!$i!!}"/>
				    <input type="hidden" id="experiences_number" name="experiences_number" value="{!!$experience_number!!}"/>
				    
				    @else
				    <div id="files_1">
					    <div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-3">
		                        	<div class="col-sm-12">
		                        		<label class="col-sm-12 ">ارگان / موسسه</label>
		                        		<input class="form-control" type="text" name="experience_company_1" value="" required="required">
		                        	</div>
		                        </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">عنوان بست</label>
					                  	<input type="text" class="form-control" name="experience_position_1" />
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">بست</lable>
					                  	<input type="text" class="form-control" name="experience_bast_1" />
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">{!!$rank!!}</lable>
					                  	<input type="text" class="form-control" name="experience_rank_1" />
					               	</div>
					            </div>
					      	</div>
					    </div>
					    
					    <div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-3">
		                        	<div class="col-sm-12">
		                        		<label class="col-sm-12 ">از</label>
		                        		
		                                <input class="form-control datepicker_farsi" type="text" name="date_from_1" value="">
		                        	</div>
		                        </div>
		                        <div class="col-sm-3">
		                        	<div class="col-sm-12">
		                        		<label class="col-sm-12 ">تا</label>
		                        		
		                                <input class="form-control datepicker_farsi" type="text" name="date_to_1" value="">
		                        	</div>
		                        </div>
		                        <div class="col-sm-5">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">دلایل ترک وظیفه</lable>
					                  	<input type="text" class="form-control" name="experience_leave_1" />
					               	</div>
					            </div>
					            <div class="col-md-1">
					            	<div class="col-sm-12">
					              		<label class="col-sm-1 ">&nbsp;</lable>
					              		@if(canAdd('hr_documents'))
										<i class="icon wb-plus-circle" aria-hidden="true" style="font-size: 25px;cursor: pointer;" onclick="add_file('file')"></i>
										@endif
									</div>
								</div>
					      	</div>
					    </div>
					</div>
					<input type="hidden" id="total_files" value="1"/>
				    <input type="hidden" id="experiences_number" name="experiences_number" value="1"/>
				    
					@endif
				    <div id="other_experience"></div>
				    
					</br>
	                <div class="container-fluid">
		                <div class="row">
		                	<div class="col-sm-6">
		                		<div class="col-sm-12">
		                    		<label class="col-sm-2 ">&nbsp;</label>
		                    		@if(canAdd('hr_documents'))
			                        <button class="btn btn-primary" type="submit">ثبت معلومات</button>
			                    	@else
			                    	<p>You dont have permission</p>
			                    	@endif
			                        
			                    </div>
	                    	</div>
	                	</div>
	                </div>
                {!! Form::token() !!}
            </form>
        </div>
    </div>

<script>
$(".datepicker_farsi").persianDatepicker();
function add_file(type)
{
	if(type == 'file')
	{
		var current_total = $('#total_files').val();
		var total_exp = $('#experiences_number').val();
		var total = parseInt(current_total)+parseInt(1);
		var total_exp = parseInt(total_exp)+parseInt(1);
		$('#total_files').val(total);
		$('#experiences_number').val(total_exp);
	}
	else
	{
		var current_total = $('#total_training').val();
		var total_tra = $('#trainings_number').val();
		var total = parseInt(current_total)+parseInt(1);
		var total_tra = parseInt(total_tra)+parseInt(1);
		$('#total_training').val(total);
		$('#trainings_number').val(total_tra);
	}
	
	$.ajax({
		url:'{{URL::route("getMoreExperience")}}',
		data: '&total='+total+'&type='+type,
		type:'POST',
		success:function(r){
			if(type == 'file')
			{
				$('#other_experience').append(r);
			}
			else
			{
				$('#other_training').append(r);
			}
		}
	});
}
function remove_file(no,type)
{
	if(type == 'file')
	{
		$('#files_'+no).remove();
		var current_total = $('#experiences_number').val();
		var total = parseInt(current_total)-parseInt(1);
		$('#experiences_number').val(total);
	}
	else
	{
		$('#trainings_'+no).remove();
		var current_total = $('#trainings_number').val();
		var total = parseInt(current_total)-parseInt(1);
		$('#trainings_number').val(total);
	}
}
function showSoldier()
{
    if($('#soldier').is(':checked'))
    {
    	$('#soldier_det').slideDown();
        
    }
    else
    {
   		$('#soldier_det').slideUp();
    }
}
</script>


