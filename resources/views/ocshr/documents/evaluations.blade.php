@section('head')
{!! HTML::style('/css/tasks/persian_datepicker.css') !!}
{!! HTML::script('/js/tasks/persian_datepicker.js')!!}
@stop
<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix">
		    <h2>
		    	@if(canAdd('hr_documents'))
		    	<a href="javascript:void()" class="btn btn-primary pull-right" data-target="#change_employee" data-toggle="modal">
					<i class="fa fa-plus-circle fa-lg"></i>
				</a>
		        @endif
		    </h2>
		</header>
		<div class="modal-body">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>بست</th>
                        <th>قدم</th>
                        <th>نمره</th>
                        <th>نتیجه</th>
                        <th>تاریخ</th>
                      </tr>
                    </thead>
                    <tbody>
                    @if($evaluations)
                    <?php $i = 1; ?>
                   		@foreach($evaluations AS $row)
                   		<tr>
                    	<td>{!!$i!!}</td>
                    	<td>{!!$row->bast!!}</td>
                    	<td>{!!$row->qadam!!}</td>
                    	<td>{!!$row->score!!}</td>
                    	<td>
                    		@if($row->result==1) {!!'ارتقاع'!!}
                    		@elseif($row->result==2) {!!'حفظ بست'!!}
                    		@elseif($row->result==3) {!!'معرفی به آموزش'!!}
                    		@elseif($row->result==4) {!!'تبدیل'!!}
                    		@else
                    		@endif
                    	</td>
                    	<td>{!!$row->date!!}</td>
                    	</tr>
                   	<?php $i++; ?>
                    	@endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="@if(canAdd('hr_documents')) {!!URL::route('evaluateEmployeeViaAjax')!!} @endif">
			    <input type="hidden" name="employee_id" value="{!!Crypt::encrypt($details->id)!!}">	 
			    <div class="panel-heading">
			      <h5 class="panel-title">ارزیابی ها</h5>
			    </div>
			    	<div class="container-fluid">
	                	<div class="row">
	                		
				      		<div class="col-sm-4">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "emp_bast" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getBastStaticList('employee_rank')!!}
	                                </select>
				      			</div>
				      		</div>
					      	<div class="col-sm-4">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">قدم</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "qadam" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value="1">اول</option>
	                                    <option value="2">دوم</option>
	                                    <option value="3">سوم</option>
	                                    <option value="4">چهارم</option>
	                                    <option value="5">پنجم</option>
	                                </select>
				      			</div>
				      		</div>	
				      		<div class="col-sm-4">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">نمره</label>
	                                <input class="form-control" type="text" name="score">
	                        	</div>
	                        </div>
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		
				      		<div class="col-sm-6">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">نتیجه</label>
	                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
	                                <select name = "result" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>ارتقاع</option>
	                                    <option value='2'>حفظ بست</option>
	                                    <option value='3'>معرفی به آموزش</option>
	                                    <option value='4'>تبدیلی</option>
	                                </select>
	                                
				      			</div>
				      		</div>
					      		
				      		<div class="col-sm-6">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">تاریخ ارزیابی</label>
	                                <input class="form-control datepicker_farsi" readonly type="text" name="date">
	                        	</div>
	                        </div>
	                	</div>
	                </div>
	                
				    <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_documents'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>			            				           
				      	</div>
				    </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="md-overlay"></div><!-- the overlay element -->
<script type="text/javascript">
    $(".datepicker_farsi").persianDatepicker(); 
    function showRank(value)
    {
        if(value == 3)
        {
            //$('#ageer_div').slideUp();
            $('#bast_div').slideUp();
            $('#military_div').slideDown();

        }
        else
        {
            //$('#ageer_div').slideUp();
            $('#military_div').slideUp();
            $('#bast_div').slideDown();
        }

    }              
    function getProvinceDistrict(page,ele,value)
    {

        $.ajax({
            url: page,
            type: 'post',
            data: 'province='+value,
            dataType: 'html',
            success:function(respones){
                $('#'+ele).html(respones);
            }
        });

    }
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
</script>