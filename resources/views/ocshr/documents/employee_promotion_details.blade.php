@section('head')
{!! HTML::style('/css/tasks/persian_datepicker.css') !!}
{!! HTML::script('/js/tasks/persian_datepicker.js')!!}
@stop
<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix">
		    <h2>
		    	@if(canAdd('hr_documents'))
		    	<a href="javascript:void()" class="btn btn-primary pull-right" data-target="#change_employee" data-toggle="modal">
					<i class="fa fa-plus-circle fa-lg"></i>
				</a>
		        @endif
		    </h2>
		</header>
		<div class="modal-body">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        @if($details->employee_type == 2)
                        <th>از درجه</th>
                        <th>به درجه</th>
                        @else
                        <th>از رتبه</th>
                        <th>به رتبه</th>
                        @endif
                        <th>قدم</th>
                        <th>تاریخ</th>
                        <th>عملیه</th>
                      </tr>
                    </thead>
                    <tbody>
                    @if($promotions)
                    <?php $i = 1; ?>
                   		@foreach($promotions AS $row)
                   		<tr>
                    	<td>{!!$i!!}</td>
                    	@if($row->employee_type == 3)
                    	<td>{!!$row->rank_military!!}</td>
                    	<td>{!!$row->to_military!!}</td>
                    	@elseif($row->employee_type == 2)
                    	<td>{!!$row->rank_ajir!!}</td>
                    	<td>{!!$row->to_ajir!!}</td>
                    	@else
                    	<td>{!!$row->rank_name!!}</td>
                    	<td>{!!$row->to_rank!!}</td>
                    	@endif
                    	<td>{!!$row->qadam!!}</td>
                    	<?php 
                    	$date = $row->promotion_date;
                    	if($date!=null)
                    	{
                    		$date = explode('-',$date);
                    		$date = dateToShamsi($date[0],$date[1],$date[2]);
                    	}
                    	?>
                    	<td>{!!$date!!}</td>
                    	<td>
                    		@if(canEdit('hr_documents'))
                    		<a href="javascript:void()" onclick="load_edit_promotion({!!$row->id!!})" data-target="#update_promotion" data-toggle="modal">
								<i class="fa fa-edit fa-lg"></i>
							</a> | 
							@endif
							@if(canDelete('hr_documents'))
							<a href="javascript:void()" onclick="deletePromotion({!!$row->id!!})" class="table-link" style="text-decoration:none;">
								<i class="icon fa-trash-o" style="color:red;" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							@endif
                    	</td>
                    	</tr>
                    <?php $i++; ?>
                    	@endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="@if(canAdd('hr_documents')) {!!URL::route('promoteEmployeeViaAjax')!!} @endif">
			    <input type="hidden" name="employee_id" value="{!!Crypt::encrypt($details->id)!!}">	 
			    <div class="panel-heading">
			      <h5 class="panel-title">معلومات درباره بست مورد نظر</h5>
			    </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-2 ">کارکنان</label>
	                                <select name="employee_type" class="form-control" onchange="showRank(this.value)">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1' <?php echo (Input::old('employee_type')=='1' ? 'selected':''); ?>>مامور</option>
	                                    <option value='2' <?php echo (Input::old('employee_type')=='2' ? 'selected':''); ?>>اجیر</option>
	                                    <option value='3' <?php echo (Input::old('employee_type')=='3' ? 'selected':''); ?>>نظامی</option>
	                                    <option value='4' <?php echo (Input::old('employee_type')=='4' ? 'selected':''); ?>>مامور بالمقطع</option>
	                                    <option value='5' <?php echo (Input::old('employee_type')=='5' ? 'selected':''); ?>>اجیر بالمقطع</option>
	                                </select>
				               	</div>
				               	
				            </div>
				            <div id="bastdiv">
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">رتبه از</label>
		                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
		                                <select name = "emp_rank" id="emp_rank" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('employee_rank',$details->emp_rank)!!}
		                                </select>
		                                
					      			</div>
					      		</div>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">رتبه به</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "emp_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('employee_rank',$details->emp_bast)!!}
		                                </select>
					      			</div>
					      		</div>
				      		</div>
				      		<div id="ajirdiv" style="display:none;">
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">درجه رتبه از</label>
		                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
		                                <select name = "ajir_rank" id="emp_rank" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('ajir_ranks',$details->ajir_rank,'dr','order')!!}
		                                </select>
		                                
					      			</div>
					      		</div>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">درجه رتبه به</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "ajir_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('ajir_ranks',$details->ajir_bast,'dr','order')!!}
		                                </select>
					      			</div>
					      		</div>
				      		</div>
				      		<div id="militarydiv" style="display:none;">
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">رتبه از</label>
		                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
		                                <select name = "military_rank" id="military_rank" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('military_rank',$details->militray_rank)!!}
		                                </select>
					      			</div>
					      		</div>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">رتبه به</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "military_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('military_rank',$details->military_bast)!!}
		                                </select>
					      			</div>
					      		</div>
				      		</div>
				      		<div class="col-sm-3">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">تاریخ ترفیع</label>
	                                <input class="form-control datepicker_farsi" type="text" name="change_date">
	                        	</div>
	                        </div>
	                	</div>
	                </div>
				    <div class="container-fluid">
	                	<div class="row">
	                        <div class="col-sm-3">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">قدم</label>
	                                <input class="form-control" type="text" name="qadam">
	                        	</div>
	                        </div>
	                        <div class="col-sm-2">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">{!!_('year')!!}</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "qadam_year" class="form-control">
	                                    @for($y=0;$y<=10;$y++)
	                                    <option value="{!!$y!!}">{!!$y!!}</option>
	                                    @endfor
	                                </select>
				      			</div>
				      		</div>
				      		<div class="col-sm-2">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">{!!_('month')!!}</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "qadam_month" class="form-control">
	                                    @for($m=0;$m<=11;$m++)
	                                    <option value="{!!$m!!}">{!!$m!!}</option>
	                                    @endfor
	                                </select>
				      			</div>
				      		</div>
				      		<div class="col-sm-2">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">{!!_('day')!!}</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "qadam_day" class="form-control">
	                                    @for($d=0;$d<=30;$d++)
	                                    <option value="{!!$d!!}">{!!$d!!}</option>
	                                    @endfor
	                                </select>
				      			</div>
				      		</div>
	                		<div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_documents'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>			            				           
				      	</div>
				    </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="update_promotion" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">
</div>
<div class="md-overlay"></div><!-- the overlay element -->
<script type="text/javascript">
    $(".datepicker_farsi").persianDatepicker(); 
            
    function load_edit_promotion(id)
    {
		var page = "{!!URL::route('load_promotion_update')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&id='+id,
	        success: function(r){
				$('#update_promotion').html(r);
	        }
	    });

    }
    function deletePromotion(id)
	{
	    var confirmed = confirm("Do you want to delete?");
	    if(confirmed)
	    {
	        $.ajax({
	                url: '{!!URL::route("deleteEmployeePromotion")!!}',
	                data: '&id='+id,
	                type: 'post',
	                
	                success: function(response)
	                {
	                    location.reload();
	                }
	            }
	        );
	    }
	
	}
	function showRank(value)
    {
        if(value == 3)
        {//military
            $('#ajirdiv').slideUp();
            $('#bastdiv').slideUp();
            $('#militarydiv').slideDown();

        }
        else if(value == 2)
        {//ajir
        	$('#ajirdiv').slideDown();
            $('#militarydiv').slideUp();
            $('#bastdiv').slideUp();
        }
        else
        {
            $('#ajirdiv').slideUp();
            $('#militarydiv').slideUp();
            $('#bastdiv').slideDown();
        }

    }
</script>