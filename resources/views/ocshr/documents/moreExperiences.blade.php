@if($type == 'file')
	<div id="files_{!!$total!!}">
	    <hr><div class="container-fluid">
	      	<div class="row">
	      		<div class="col-sm-3">
                	<div class="col-sm-12">
                		<label class="col-sm-12 ">ارگان / موسسه</label>
                		<input class="form-control" type="text" name="experience_company_{!!$total!!}" value="">
                	</div>
                </div>
	      		
	            <div class="col-sm-3">
	            	<div class="col-sm-12">
	              		<label class="col-sm-12 ">عنوان بست</label>
	                  	<input type="text" class="form-control" name="experience_position_{!!$total!!}" />
	               	</div>
	            </div>
	            <div class="col-sm-3">
	            	<div class="col-sm-12">
	              		<label class="col-sm-12 ">بست</lable>
	                  	<input type="text" class="form-control" name="experience_bast_{!!$total!!}" />
	               	</div>
	            </div>
	            <div class="col-sm-3">
	            	<div class="col-sm-12">
	              		<label class="col-sm-12 ">رتبه</lable>
	                  	<input type="text" class="form-control" name="experience_rank_{!!$total!!}" />
	               	</div>
	            </div>
	      	</div>
	    </div>
	    
	    <div class="container-fluid">
	      	<div class="row">
	      		<div class="col-sm-3">
                	<div class="col-sm-12">
                		<label class="col-sm-12 ">از</label>
                		
                        <input class="form-control datepicker_farsi" type="text" name="date_from_{!!$total!!}" value="">
                	</div>
                </div>
                <div class="col-sm-3">
                	<div class="col-sm-12">
                		<label class="col-sm-12 ">تا</label>
                		
                        <input class="form-control datepicker_farsi" type="text" name="date_to_{!!$total!!}" value="">
                	</div>
                </div>
                <div class="col-sm-5">
	            	<div class="col-sm-12">
	              		<label class="col-sm-12 ">دلایل ترک وظیفه</lable>
	                  	<input type="text" class="form-control" name="experience_leave_{!!$total!!}" />
	               	</div>
	            </div>
	            <div class="col-md-1">
	            	<div class="col-sm-12">
	              		<label class="col-sm-1 ">&nbsp;</lable>
						<i class="icon wb-minus-circle" aria-hidden="true" style="font-size: 25px;cursor: pointer;" onclick="remove_file('{!!$total!!}','file')"></i>
					</div>
				</div>
	      	</div>
	    </div>
	</div>
@elseif($type == 'training')
	<div id="trainings_{!!$total!!}">
		<div class="container-fluid">
	      	<div class="row">
	      		<div class="col-sm-3">
	              	<div class="col-sm-12">
	              		<label class="col-sm-12 ">عنوان آموزش</lable>
	                  	<input type="text" class="form-control" name="training_title_{!!$total!!}"  />
	               	</div>
	            </div>
	            <div class="col-sm-3">
	            	<div class="col-sm-12">
	              		<label class="col-sm-12 ">ارگان / موسسه</label>
	                  	<input type="text" class="form-control" name="training_org_{!!$total!!}" />
	               	</div>
	            </div>
	            <div class="col-sm-3">
                	<div class="col-sm-12">
                		<label class="col-sm-12 ">از</label>
                		
                        <input class="form-control datepicker_farsi" readonly type="text" name="training_date_from_{!!$total!!}" value="">
                	</div>
                </div>
                <div class="col-sm-2">
                	<div class="col-sm-12">
                		<label class="col-sm-12 ">تا</label>
                		
                        <input class="form-control datepicker_farsi" readonly type="text" name="training_date_to_{!!$total!!}" value="">
                	</div>
                </div>
                <div class="col-md-1">
	            	<div class="col-sm-12">
	              		<label class="col-sm-1 ">&nbsp;</lable>
						<i class="icon wb-minus-circle" aria-hidden="true" style="font-size: 25px;cursor: pointer;" onclick="remove_file('{!!$total!!}','training')"></i>
					</div>
				</div>
	      	</div>
	    </div>
	</div>
@endif
<script>
$(".datepicker_farsi").persianDatepicker();
</script>