<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="@if(canAdd('hr_documents')) {!!URL::route('promoteEmployeeUpdate')!!} @endif">
			    <input type="hidden" name="id" value="{!!Crypt::encrypt($details->id)!!}">	 
			    <div class="panel-heading">
			      <h5 class="panel-title">معلومات درباره بست مورد نظر</h5>
			    </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<input type="hidden" name="employee_id" value="{!!$details->employee_id!!}"/>
	                		<div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-2 ">کارکنان</label>
	                                <select name="employee_type" class="form-control" onchange="showRank_edit(this.value)">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1' <?php echo ($details->employee_type=='1' ? 'selected':''); ?>>مامور</option>
	                                    <option value='2' <?php echo ($details->employee_type=='2' ? 'selected':''); ?>>اجیر</option>
	                                    <option value='3' <?php echo ($details->employee_type=='3' ? 'selected':''); ?>>نظامی</option>
	                                    <option value='4' <?php echo ($details->employee_type=='4' ? 'selected':''); ?>>مامور بالمقطع</option>
	                                    <option value='5' <?php echo ($details->employee_type=='5' ? 'selected':''); ?>>اجیر بالمقطع</option>
	                                </select>
				               	</div>
				               	
				            </div>
				            <div id="bastDiv" @if($details->employee_type==2 || $details->employee_type==3) style="display:none;" @endif>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">رتبه از</label>
		                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
		                                <select name = "emp_rank" id="emp_rank" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('employee_rank',$details->emp_rank)!!}
		                                </select>
		                                
					      			</div>
					      		</div>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">رتبه به</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "emp_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('employee_rank',$details->emp_bast)!!}
		                                </select>
					      			</div>
					      		</div>
				      		</div>
				      		<div id="ajirDiv" @if($details->employee_type!=2) style="display:none;" @endif>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">درجه رتبه از</label>
		                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
		                                <select name = "ajir_rank" id="emp_rank" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('ajir_ranks',$details->ajir_rank,'dr','order')!!}
		                                </select>
		                                
					      			</div>
					      		</div>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">درجه رتبه به</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "ajir_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('ajir_ranks',$details->ajir_bast,'dr','order')!!}
		                                </select>
					      			</div>
					      		</div>
				      		</div>
				      		<div id="militaryDiv" @if($details->employee_type!=3) style="display:none;" @endif>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">رتبه از</label>
		                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
		                                <select name = "military_rank" id="military_rank" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('military_rank',$details->military_rank)!!}
		                                </select>
					      			</div>
					      		</div>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">رتبه به</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "military_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('military_rank',$details->military_bast)!!}
		                                </select>
		                            
					      			</div>
					      		</div>
				      		</div>
				      		<div class="col-sm-3">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">تاریخ ترفیع</label>
	                        		<?php
	                        		$date = $details->promotion_date;
	                        		if($date !=null)
	                        		{
	                        			$date = explode('-',$date);
	                        			$date = dateToShamsi($date[0],$date[1],$date[2]);
	                        			$date = jalali_format($date);
	                        		}
	                        		?>
	                                <input class="form-control datepicker_farsi" value="{!!$date!!}" readonly type="text" name="change_date">
	                        	</div>
	                        </div>
	                	</div>
	                </div>
				    <div class="container-fluid">
	                	<div class="row">
	                        <div class="col-sm-3">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">قدم</label>
	                                <input class="form-control" value="{!!$details->qadam!!}" type="text" name="qadam">
	                        	</div>
	                        </div>
	                        <div class="col-sm-2">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">{!!_('year')!!}</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "qadam_year" class="form-control">
	                                    @for($y=0;$y<=10;$y++)
	                                    	@if($details->qadam_year==$y)
	                                    	<option value="{!!$y!!}" selected>{!!$y!!}</option>
	                                    	@else
	                                    	<option value="{!!$y!!}">{!!$y!!}</option>
	                                    	@endif
	                                    @endfor
	                                </select>
				      			</div>
				      		</div>
				      		<div class="col-sm-2">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">{!!_('month')!!}</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "qadam_month" class="form-control">
	                                    @for($m=0;$m<=11;$m++)
	                                    	@if($details->qadam_month==$m)
	                                    	<option value="{!!$m!!}" selected>{!!$m!!}</option>
	                                    	@else
	                                    	<option value="{!!$m!!}">{!!$m!!}</option>
	                                    	@endif
	                                    @endfor
	                                </select>
				      			</div>
				      		</div>
				      		<div class="col-sm-2">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">{!!_('day')!!}</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "qadam_day" class="form-control">
	                                    @for($d=0;$d<=30;$d++)
	                                    	@if($details->qadam_day==$d)
	                                    	<option value="{!!$d!!}" selected>{!!$d!!}</option>
	                                    	@else
	                                    	<option value="{!!$d!!}">{!!$d!!}</option>
	                                    	@endif
	                                    @endfor
	                                </select>
				      			</div>
				      		</div>
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_documents'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>			            				           
				      	</div>
				    </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
<script type="text/javascript">
    $(".datepicker_farsi").persianDatepicker(); 
    function showRank_edit(value)
    {
        if(value == 3)
        {//military
            $('#ajirDiv').slideUp();
            $('#bastDiv').slideUp();
            $('#militaryDiv').slideDown();

        }
        else if(value == 2)
        {//ajir
        	$('#ajirDiv').slideDown();
            $('#militaryDiv').slideUp();
            $('#bastDiv').slideUp();
        }
        else
        {
            $('#ajirDiv').slideUp();
            $('#militaryDiv').slideUp();
            $('#bastDiv').slideDown();
        }

    }
</script>