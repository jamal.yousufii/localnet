
<style>
.table-responsive{overflow-x: hidden !important;}
td{
	vertical-align:top;
}
.page_header{
	width:100%;
	text-align: center;
}


.footer-left{
	float:left;
	margin-left: 55px;
	
	
}
.footer-right{
	float: right;
	direction:rtl;
	margin-right: 55px;
	

}
.td_title{
	font-weight: bold;
}
.vertical_text{
  width:10%;
  vertical-align:middle !important;
  -ms-transform:rotate(270deg); /* IE 9 */
  -moz-transform:rotate(270deg); /* Firefox */
  -webkit-transform:rotate(270deg); /* Safari and Chrome */
  -o-transform:rotate(270deg); /* Opera */
  
}
</style>
<div class="row" >
	<div class="main-box">
		<div class="main-box-body clearfix" style="background: #ddd;">
			<h2>
		    	<a href="{!!URL::route('print_cv',$id)!!}" class="btn btn-primary pull-right" target="_blank">
					<i class="fa fa-print fa-lg"></i>
				</a>
		    </h2>
			<div style="border: 1px solid #ddd;width:80%;float:left;padding:8px;margin-left:132px;background:#fff;margin-top:10px;">
				<div class="page_header">
				{!!HTML::image('images/aop-header.png')!!}
				</div>
				<div class="page_content" dir="rtl" style="text-align:center">
					<div class="page_department" style="text-align:center;font-size:18px;">
						ریاست منابع بشری<br>
						خلص سوانح کارمندان<br><br>
					</div>
					<div class="col-lg-6">
						<table border="1px solid" width="100%" height="50px">
							<tr>
								<td colspan="6" class="td_title">معلومات وظیفه فعلی</td>
							</tr>
							<tr>
								<td class="td_title">وظیفه</td>
								<td class="td_title">تاریخ تقرر</td>
								<td class="td_title">محل وظیفه</td>
								
								<td class="td_title">بست</td>
								
								<td class="td_title">رتبه</td>
							</tr>
							<tr>
								@if($details->changed==1)
									<td>{!!$changes->position_title!!}</td>
									<td>{!!$changes->changed_date!!}</td>
									@if($changes->change_type==0)
										<td>{!!$changes->department!!}</td>
									@else
										<td>{!!$changes->ministry!!}</td>
									@endif
									<td>{!!$details->current_pro!!}</td>
									<td>{!!$changes->bast!!}</td>
									<td>{!!$changes->rank!!}</td>
								@else
									@if($details->fired==1)
									<td colspan="2">منفک</td>
									@elseif($details->resigned==1)
									<td colspan="2">استعفا</td>
									@elseif($details->retired==1)
									<td colspan="2">تقاعد</td>
									@elseif($details->position_dr==4)
									<td colspan="2">انتظاربامعاش</td>
									@elseif($details->position_dr==6)
									<td colspan="2">انتظار بدون معاش</td>
									@elseif($details->position_dr==7)
									<td colspan="2">اضافه بست</td>
									@else
									<td>{!!$details->current_position_dr!!}</td>
									<td>{!!$details->first_date_appointment!!}</td>
									@endif
								
									@if($details->tashkil_id>0)
										@if($details->tashkil_sub_dep_id == 82)
										<td>دفتر نماینده فوق العاده رئیس جمهور در امور اصلاحات و حکومتداری خوب</td>
										@else
										<td>{!!$details->dep_name!!}</td>
										@endif
									@else
										@if($details->tashkil_sub_dep_id_old == 82)
										<td>دفتر نماینده فوق العاده رئیس جمهور در امور اصلاحات و حکومتداری خوب</td>
										@else
										<td>{!!$details->dep_name!!}</td>
										@endif
									@endif
								
									<td>{!!$details->bast!!}</td>
									
									<td>{!!$details->emprank!!}</td>
								@endif
							</tr>
						</table>
						<br>
						<table border="1px solid" width="100%" height="350px">
							<tr>
								<td colspan="6" class="td_title" height="25px">سابقه کاری</td>
							</tr>
							<tr>
								<td class="td_title" height="25px">شماره</td>
								<td class="td_title">از تاریخ</td>
								<td class="td_title">تا تاریخ</td>
								<td class="td_title">وظیفه</td>
								<td class="td_title">محل وظیفه</td>
								<td class="td_title" width="20%">علت تبدیلی</td>
								
							</tr>
							@if($experiences)
							<?php $i=1;?>
							@foreach($experiences AS $exp)
							<?php $sdate = $exp->date_from;$edate = $exp->date_to;
                    		if($sdate !=''){$sdate = explode('-',$sdate);$sdate=dateToShamsi($sdate[0],$sdate[1],$sdate[2]);$sdate=jalali_format($sdate);}
                    		if($edate !=''){$edate = explode('-',$edate);$edate=dateToShamsi($edate[0],$edate[1],$edate[2]);$edate=jalali_format($edate);}
                    		?>
								<tr>
									<td>{!!$i!!}</td>
									<td>{!!$sdate!!}</td>
									<td>{!!$edate!!}</td>
									<td>{!!$exp->position!!}</td>
									<td>{!!$exp->organization!!}</td>
									<td>{!!$exp->leave_reason!!}</td>
									
								</tr>
								<?php $i++; ?>
							@endforeach
							@else
								<tr>
									<td colspan="6">ندارد</td>
									
								</tr>
							@endif
						</table>
						<br>
					
						<table border="1px solid" width="100%" height="250px">
							<tr>
								<td colspan="4" class="td_title" height="25px">ترفیعات</td>
							</tr>
							<tr>
								<td class="td_title" height="25px">تاریخ ترفیع</td>
								@if($details->employee_type!=2)
								<td class="td_title">رتبه</td>
								@else
								<td class="td_title">درجه</td>
								@endif
								<td class="td_title">قدم</td>
							</tr>
							@if($promotions)
							@foreach($promotions AS $pro)
							<?php $edate = $pro->promotion_date;
                    		if($edate !=''){$edate = explode('-',$edate);$edate=dateToShamsi($edate[0],$edate[1],$edate[2]);$edate=jalali_format($edate);}
                    		?>
								<tr>
									<td>{!!$edate!!}</td>
									<td>
									@if($pro->employee_type==2)
									{!!$pro->to_ajir!!}
									@elseif($pro->employee_type==3)
									{!!$pro->to_military!!}
									@else
									{!!$pro->to_rank!!}
									@endif
									</td>
									<td>{!!$pro->qadam!!}</td>
								</tr>
							@endforeach
							@else
								<tr>
									<td colspan="4">ندارد</td>
								</tr>
							@endif
						</table>
						<br>
						<table border="1px solid" width="100%" height="150px">
							<tr>
								<td colspan="4" class="td_title" height="25px">آموزش ها</td>
								
							</tr>
							<tr>
								
								<td class="td_title">تاریخ شروع</td>
								<td class="td_title">تاریخ ختم</td>
								<td class="td_title" height="25px">آموزش</td>
								<td class="td_title">محل آموزش</td>
							</tr>
							@if($trainings)
							@foreach($trainings AS $tra)
								<tr>
									<td>{!!$tra->date_from!!}</td>
									<td>{!!$tra->date_to!!}</td>
									<td>{!!$tra->title!!}</td>
									<td>{!!$tra->organization!!}</td>
								</tr>
							@endforeach
						
							@endif
						</table>
					</div>
					<div class="col-lg-6">
						<table border="1px solid" width="100%" height="200px">
							<tr>
								<td colspan="5" class="td_title">شهرت</td>
							</tr>
							<tr>
								<td rowspan="8" width="40%"><img src="/documents/profile_pictures/{!!$details->photo!!}" alt="image" width="100%" height="auto"/></td>
								<td class="td_title" width="20%">کد دیتابیس</td>
								<td colspan="3">{!!$id!!}</td>
							</tr>
							<tr>
								<td class="td_title">نام</td>
								<td colspan="3">{!!$details->name_dr!!}</td>
							</tr>
							<tr>
								<td class="td_title">تخلص</td>
								<td colspan="3">{!!$details->last_name!!}</td>
							</tr>
							<tr>
								<td class="td_title">نام پدر</td>
								<td colspan="3">{!!$details->father_name_dr!!}</td>
							</tr>
							<tr>
								<td class="td_title">نام پدر کلان</td>
								<td colspan="3">{!!$details->grand_father_name!!}</td>
							</tr>
							<tr>
								<td class="td_title">تذکره</td>
								<td colspan="3">{!!$details->id_no!!}</td>
							</tr>
							<tr>
								<td class="td_title">ولایت</td>
								<td colspan="3">{!!$details->original_pro!!}</td>
							</tr>
							
							<tr>
								<td class="td_title">سال تولد</td>
								<td>
								@if($details->birth_year!='')
								{!!$details->birth_year!!}
								@else . @endif
								</td>
								<td class="td_title">عمر</td>
								<td>
								@if($details->birth_year!='')
								<?php $bdate = dateToMiladi($details->birth_year,'01','01');?>
								{!!(date('Y')-$bdate)!!}
								@else . @endif
								</td>
							</tr>
							<tr>
								<td class="td_title">شماره تماس</td>
								<td colspan="4">
									{!!$details->phone!!}
								</td>
							</tr>
							<tr>
								<td class="td_title">ایمیل</td>
								<td colspan="4">
									{!!$details->email!!}
								</td>
							</tr>
							
							<tr>
								<td class="td_title">وضعیت وظیفه</td>
								<td colspan="4">
									@if($details->changed==1){!!'تبدیلی'!!}
									@elseif($details->fired==1){!!'منفکی'!!}
									@elseif($details->resigned==1){!!'استعفا'!!}
									@elseif($details->retired==1){!!'تقاعد'!!}
									@elseif($details->position_dr==1){!!'اصلی'!!}
									@elseif($details->position_dr==2){!!'خدمتی'!!}
									@elseif($details->position_dr==3){!!'تحصیلی'!!}
									@elseif($details->position_dr==4){!!'انتظاربامعاش'!!}
									@elseif($details->position_dr==5){!!'عسکری'!!}
									@elseif($details->position_dr==6){!!'انتظار بدون معاش'!!}
									@elseif($details->position_dr==7){!!'اضافه بست'!!}
									@else {!!''!!}
									@endif
								</td>
							</tr>
						</table>
						<br>
						<?php
						$edu_count=1;
						if(isset($educations))
						{
							$edu_count = count($educations);
						}
						?>
						<table border="1px solid" width="100%" height="150px">
							<tr>
								<td rowspan="{!!$edu_count+1!!}" class="vertical_text">تحصیلات</td>
								<td class="td_title" height="25px">درجه تحصیل</td>
								<td class="td_title">موسسه تحصیلی</td>
								<td class="td_title">محل تحصیل</td>
								<td class="td_title">رشته</td>
								<td class="td_title">سال فراغت</td>
							</tr>
							@if($educations)
								@foreach($educations AS $edu)
								<tr>
									<td>{!!$edu->degree_name!!}</td>
									<td>{!!$edu->education_place!!}</td>
									<td>{!!$edu->location_name!!}</td>
									<td>{!!$edu->education_field!!}</td>
									<td>{!!$edu->graduation_year!!}</td>
								</tr>
							@endforeach
						@endif
						</table>
						<br>
						<!--
						<table border="1px solid" width="100%">
							<tr>
								<td rowspan="2" class="vertical_text">علمی رتبه</td>
								<td class="td_title">دتحصیل درجه</td>
								<td class="td_title">د شونحی نوم</td>
								<td class="td_title">جای</td>
								<td class="td_title">رشته</td>
								<td class="td_title">د فراغت کال</td>
								
							</tr>
							<tr>
								<td>the job</td>
								<td>the place</td>
								<td>the province</td>
								<td>the Bast</td>
								<td>the Qadam</td>
							
							</tr>
							
						</table>
						-->
						<?php $makafat_total = count($makafat); ?>
						<table border="1px solid" width="100%" height="250px">
							<tr>
								<td rowspan="{!!$makafat_total + 2!!}" class="vertical_text">مکافات</td>
								<td class="td_title" height="25px">تاریخ</td>
								<td class="td_title">تقدیرنامه/نشان/مدال</td>
							</tr>
							
							@if($makafat)
							@foreach($makafat AS $mak)
							<?php $hdate = $mak->date;
                    		if($hdate !=''){$hdate = explode('-',$hdate);$hdate=dateToShamsi($hdate[0],$hdate[1],$hdate[2]);$hdate=jalali_format($hdate);}
                    		?>
							<tr>
								<td>{!!$hdate!!}</td>
								<td>
									@if($mak->type==1){!!'تقدیرنامه درجه اول'!!}
									@elseif($mak->type==2){!!'تقدیرنامه درجه دوم'!!}
									@elseif($mak->type==3){!!'تقدیرنامه درجه سوم'!!}
									@elseif($mak->type==4){!!'تقدیرنامه افتخاری'!!}
									@elseif($mak->type==5){!!'تحسین نامه'!!}
									@elseif($mak->type==6){!!'معاش بخششی'!!}
									@elseif($mak->type==7){!!'مدال'!!}
									@elseif($mak->type==8){!!'نشان'!!}
									@endif
								</td>
							</tr>
							@endforeach
							@else
							<tr>
								<td colspan="4">ندارد</td>
							</tr>
							@endif
							
							
						</table>
					
						<?php $punish_total = count($punish); ?>
						<table border="1px solid" width="100%" height="150px">
							<tr>
								<td rowspan="{!!$punish_total + 2!!}" class="vertical_text">مجازات</td>
								
								<td class="td_title" height="25">تاریخ</td>
								<td class="td_title">دلیل</td>
							</tr>
							
							@if($punish)
							@foreach($punish AS $pun)
							<?php $pdate = $pun->date;
                    		if($pdate !=''){$pdate = explode('-',$pdate);$pdate=dateToShamsi($pdate[0],$pdate[1],$pdate[2]);$pdate=jalali_format($pdate);}
                    		?>
							<tr>
								<td>{!!$pdate!!}</td>
								<td>{!!$pun->reason!!}</td>
							</tr>
							@endforeach
							@else
							<tr>
								<td colspan="3">نگردیده</td>
							</tr>
							@endif
						</table>
						<br>
						<table border="1px solid" width="100%">
							<tr>
								<td rowspan="3" class="vertical_text">عسکری</td>
								<td class="td_title">تاریخ شروع</td>
								<td class="td_title">تاریخ ختم</td>
								<td class="td_title">نوعیت</td>
							</tr>
							@if($details->soldier==1)
								<?php $soldier_date_from = $details->soldier_date_from;$soldier_date_to = $details->soldier_date_to;
			                    	  if($soldier_date_from !=''){$soldier_date_from = explode('-',$soldier_date_from);$soldier_date_from=dateToShamsi($soldier_date_from[0],$soldier_date_from[1],$soldier_date_from[2]);$soldier_date_from=jalali_format($soldier_date_from);}
			                    	  if($soldier_date_to !=''){$soldier_date_to = explode('-',$soldier_date_to);$soldier_date_to=dateToShamsi($soldier_date_to[0],$soldier_date_to[1],$soldier_date_to[2]);$soldier_date_to=jalali_format($soldier_date_to);}
			                    ?>
								<tr>
									<td>{!!$soldier_date_from!!}</td>
									<td>{!!$soldier_date_to!!}</td>
									<td>مکلفیت</td>
								</tr>
								@if($details->soldier_date_from_1!='')
									<?php $soldier_date_from_1 = $details->soldier_date_from_1;$soldier_date_to_1 = $details->soldier_date_to_1;
				                    	  if($soldier_date_from_1 !=''){$soldier_date_from_1 = explode('-',$soldier_date_from_1);$soldier_date_from_1=dateToShamsi($soldier_date_from_1[0],$soldier_date_from_1[1],$soldier_date_from_1[2]);$soldier_date_from_1=jalali_format($soldier_date_from_1);}
				                    	  if($soldier_date_to_1 !=''){$soldier_date_to_1 = explode('-',$soldier_date_to_1);$soldier_date_to_1=dateToShamsi($soldier_date_to_1[0],$soldier_date_to_1[1],$soldier_date_to_1[2]);$soldier_date_to_1=jalali_format($soldier_date_to_1);}
				                    ?>
								<tr>
									<td>{!!$soldier_date_from_1!!}</td>
									<td>{!!$soldier_date_to_1!!}</td>
									<td>احتیاط</td>
								</tr>
								@endif
							@else
								<tr>
									<td colspan="4">ننموده</td>
								</tr>
							@endif
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
