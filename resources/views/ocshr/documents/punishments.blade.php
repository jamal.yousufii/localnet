@section('head')
{!! HTML::style('/css/tasks/persian_datepicker.css') !!}
{!! HTML::script('/js/tasks/persian_datepicker.js')!!}
@stop
<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix">
		    <h2>
		    @if(canAdd('hr_documents'))
		    	<a href="javascript:void()" class="btn btn-primary pull-right" data-target="#change_employee" data-toggle="modal">
					<i class="fa fa-plus-circle fa-lg"></i>
				</a>
		    @endif
		    </h2>
		</header>
		<div class="modal-body">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>دلیل</th>
                        <th>تاریخ</th>
                        <th>عملیه</th>
                      </tr>
                    </thead>
                    <tbody>
                    @if($punish)
                    <?php $i = 1; ?>
                   		@foreach($punish AS $row)
                   		<tr>
                    	<td>{!!$i!!}</td>
                    	
                    	<td>
                    	{!!$row->reason!!}
                    	</td>
                    	<td>
                    	<?php $sdate = $row->date;
                    		if($sdate !=''){$sdate = explode('-',$sdate);$sdate=dateToShamsi($sdate[0],$sdate[1],$sdate[2]);$sdate=jalali_format($sdate);}
                    	?>
                    	{!!$sdate!!}</td>
                    	<td>
                    	@if(canEdit('hr_documents'))
                    		<a href="javascript:void()" onclick="load_edit_punish({!!$row->id!!})" data-target="#update_punish" data-toggle="modal">
								<i class="fa fa-edit fa-lg"></i>
							</a> | 
						@endif
						@if(canDelete('hr_documents'))
							<a href="javascript:void()" onclick="deletePunish({!!$row->id!!})" class="table-link" style="text-decoration:none;">
								<i class="icon fa-trash-o" style="color:red;" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
						@endif
                    	</td>
                    	</tr>
                   	<?php $i++; ?>
                    	@endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="@if(canAdd('hr_documents')) {!!URL::route('punishEmployeeViaAjax')!!} @endif">
			    <input type="hidden" name="employee_id" value="{!!Crypt::encrypt($details->id)!!}">	 
			    <div class="panel-heading">
			      <h5 class="panel-title">مجازات</h5>
			    </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		
				      		<div class="col-sm-6">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">دلیل</label>
	                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
	                                <textarea class="form-control" row="5" col="10" name="reason"></textarea>
	                                
				      			</div>
				      		</div>
					      		
				      		<div class="col-sm-6">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">تاریخ</label>
	                                <input class="form-control datepicker_farsi" readonly type="text" name="date">
	                        	</div>
	                        </div>
	                	</div>
	                </div>
	                
				    <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_documents'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>			            				           
				      	</div>
				    </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="update_punish" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">
</div>
<div class="md-overlay"></div><!-- the overlay element -->
<script type="text/javascript">
    $(".datepicker_farsi").persianDatepicker(); 
    function load_edit_punish(id)
    {
		var page = "{!!URL::route('load_punish_update')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&id='+id,
	        success: function(r){
				$('#update_punish').html(r);
	        }
	    });

    }
    function deletePunish(id)
	{
	    var confirmed = confirm("Do you want to delete?");
	    if(confirmed)
	    {
	        $.ajax({
	                url: '{!!URL::route("deleteEmployeePunish")!!}',
	                data: '&id='+id,
	                type: 'post',
	                
	                success: function(response)
	                {
	                    location.reload();
	                }
	            }
	        );
	    }
	
	}
</script>