<html>
	<head>
		<meta charset="UTF-8">
		<style type="text/css">
			* {line-height: 1em;
				padding: 0px;
				margin: 0px;
			}

			.front{
				width: 5.3975cm;
				height:8.5525cm;
				flo/at: left;
				position:relative;
				font-family: 'Bahij Yakout';
				overflow: hidden;

			}

			.back{
				width: 5.3975cm;
				height:8.5425cm;
				flo/at: right;
				position:relative;
				font-family: 'Open Sans';
				overflow: hidden;
			}

	        .front img {
	            width:5.3975cm;
				height:8.5725cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .back img {
	            width:5.3975cm;
				height:8.5725cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .do-print{
	        	font-family: 'Bahij Yakout';
            	font-size: 12pt;
	        }
	        .serial{
	        	position: absolute;
	        	z-index: 10;
	        	font-size: 6pt;
	        	left:0.2934cm;
	        	top:0.366cm;

	        }

	        .front .validaty {
	        	position: absolute;
	        	z-index: 10;
	        	right:0.41cm;
	        	top: 2.3cm;
	        	font-size: 6pt;

	        }
	        .front .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	right:0.41cm;
	        	top: 2.5cm;
	        	font-size: 8pt;
	        	font-weight: bold;
	        	color: red;

	        }

	        .front .photo {
	        	position: absolute;
	        	width: 2.3371cm;
	        	height: 2.9213cm;
	        	top: 1.7832cm;
	        	left: 0.3823cm;
	        	z-index: 10;
	        }

	        .front .position{
	        	/*position: absolute;*/
	        	z-index: 10;
	        	top:0.250cm;
	        	padding-right:0.4325cm;
	        	font-weight: bold;
	        	font-size: 13pt;
	        	color: white;
	        	display: table-cell;
	        	height: 100%;
	        	vertical-align:middle;


	        }
	        .front .box{
	        	position: absolute;
	        	z-index: 100;
	        	/*background-color: red !important;*/
	        	top: 3.5cm;
	        	left: 2.724cm;
	        	height: 1.0651cm;
	        	width: 2.6735cm;
	        	display: table;
	        }
	        .front .name{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.5861cm;
	        	right: 0.4325cm;
	        	font-size: 6pt;

	        }
	        .front .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.8772cm;
	        	right: 0.4325cm;
	        	left: 0.3823cm;
	        	font-size: 9pt;
	        	font-weight: bold;

	        }
	        .front .title{
	        	position: absolu/te;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	right: 0.4325cm;
	        	font-size: 6pt;
	        	display: inline;

	        }
	        .front .title_value{
	        	position: absol/ute;
	        	z-index: 10;
	        	to/p: 4.8772cm;
	        	right: 0.4325cm;
	        	left: 1.6214cm;
	        	width: 3.2582cm;
	        	font-size: 9pt;
	        	font-weight: bold;
	        	display: inline-block;
	        }
	        .front .dep{
	        	position: abso/lute;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	right: 0.4325cm;
	        	font-size: 6pt;
	        	display: inline;

	        }
	        .front .dep_value{
	        	position: absol/ute;
	        	z-index: 10;
	        	t/op: 4.8772cm;
	        	right: 0.4325cm;
	        	left: 1.6214cm;
	        	width: 4.5957cm;
	        	font-size: 9pt;
	        	font-weight: bold;
	        	display: inline-block;
	        }


	        .front .group {
	        	position: absolute;
	        	z-index: 10;
	        	top:5.3481cm;
	        	left: 0.3823cm;
	        	right: 0.4325cm;
	        }
	        .front td {
	        	height: 1.4cm;
	        	vertical-align: middle;
	        }


	        .back .validaty {
	        	position: absolute;
	        	z-index: 10;
	        	left:0.3894cm;
	        	top: 2.3cm;
	        	font-size: 4pt;

	        }
	        .back .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	left:0.3894cm;
	        	top: 2.5cm;
	        	font-size: 6pt;
	        	font-weight: bold;
	        	color: red;

	        }

	        .back .photo {
	        	position: absolute;
	        	width: 2.3371cm;
	        	height: 2.9213cm;
	        	top: 1.7832cm;
	        	right: 0.4556cm;
	        	z-index: 10;
	        }

	        .back .position{
	        	/*position: absolute;*/
	        	z-index: 10;
	        	top:0.250cm;
	        	padding-left:0.3894cm;
	        	font-weight: bold;
	        	font-size: 13pt;
	        	color: white !important;
	        	display: table-cell;
	        	vertical-align:middle;
	        	height: 100%;
	        }
	        .back .box{
	        	position: absolute;
	        	z-index: 100;
	        	/*background-color: red !important;*/
	        	top: 3.5cm;
	        	right: 2.7927cm;
	        	height: 1.0287cm;
	        	width: 2.6029cm;
	        	display: table;
	        }
	        .back .name{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.9194cm;
	        	left: 0.3894cm;
	        	font-size: 4pt;

	        }
	        .back .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 5.0605cm;
	        	right: 0.4556cm;
	        	left: 0.3894cm;
	        	font-size: 8pt;
	        	/*font-weight: bold;*/

	        }
	        .back .title{
	        	position: absolu/te;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	left: 0.3894cm;
	        	font-size: 4pt;
	        	display: inline;

	        }
	        .back .title_value{
	        	position: absol/ute;
	        	z-index: 10;
	        	to/p: 4.8772cm;
	        	right: 0.4556cm;
	        	left: 0.3894cm;
	        	font-size: 8pt;
	        	/*font-weight: bold;*/
	        	display: inline-block;
	        }
	        .back .dep{
	        	position: abso/lute;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	left: 0.3894cm;
	        	font-size: 4pt;
	        	display: inline;

	        }
	        .back .dep_value{
	        	position: absol/ute;
	        	z-index: 10;
	        	t/op: 4.8772cm;
	        	right: 0.4556cm;
	        	left: 0.3894cm;
	        	font-size: 8pt;
	        	/*font-weight: bold;*/
	        	display: inline;
	        }


	        .back .group {
	        	position: absolute;
	        	z-index: 10;
	        	top:5.4146cm;
	        	left: 0.3894cm;
	        	right: 0.4556cm;
	        }

	        .back td {
	        	height: 1.1cm;
	        	vertical-align: middle;
	        }

	        .back .blood{
	        	position: absolute;
	        	z-index: 10;
	        	top: 7.7376cm;
	        	left: 0.3894cm;
	        	font-size: 4pt;

	        }
	        .back .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 7.8875cm;
	        	right: 0.4556cm;
	        	left: 0.3894cm;
	        	font-size: 13pt;
	        	font-weight: bold;
	        	color: red;

	        }

	        .back .email{
	        	position: absolute;
	        	z-index: 10;
	        	top: 8.0051cm;
	        	right: 0.485cm;
	        	
	        	font-size: 5.25pt;
	        	font-weight: bold;

	        }
	        
	        .img-water-mark-front{
				 position:fixed;
				 bottom:16px;
				 left:32px !important;
				 opacity:0.3;
				 z-index:99;
				 width: 128px !important;
				 height: 128px !important;

	        }
	        .img-water-mark-back{
				 position:fixed;
				 bottom:16px;
				 left:32px !important;
				 opacity:0.3;
				 z-index:99;
				 width: 128px !important;
				 height: 128px !important;

	        }
	        
	        .temp{
	        	color: red;
	        	font-weight: bold;
	        	z-index: 1000;
				top: 3cm;
				position: absolute;
	        }
	        .temp_back{
	        	color: red;
	        	font-weight: bold;
	        	z-index: 1000;
				top: 3cm;
				position: absolute;
				font-size: small;
	        }
		</style>

	</head>
	<body>
		<div class="do-print">
	        <div class="row">
	           <div class="col front" dir='rtl'>
	           	<div class='serial'><!-- {!!$row->eid!!} -->
	           		<?php 
	           			$position_ids = array(1,2,3,11);

	           			$dep_id = $row->dep_id;
	           			$dep_code = $dep_id; 
	           			if($dep_id<10)
	           			{
	           				$dep_code = '00'.$dep_id;
	           			}
	           			elseif($dep_id < 100)
	           			{
	           				$dep_code = '0'.$dep_id;
	           			}


	     				$emp_id = $row->eid;
	     				$emp_code = $emp_id;

	     				if($emp_id<10)
	     				{
	     					$emp_code = '000'.$emp_id;
	     				}
	     				elseif($emp_id < 100)
	     				{
	     					$emp_code = '00'.$emp_id;
	     				}
	     				elseif($emp_id <1000)
	     				{
	     					$emp_code = '0'.$emp_id;
	     				}
	           			
	           			echo $dep_code.$emp_code;
	           		?>
	           	</div>
	           	<div class="validaty">د اعتبار موده</div>
	           	<div class="validaty_date">{!!$row->expire!!}</div>
	           	<img src="{!!getResizedPhoto($row->photo)!!}" class="photo">
	           	<!-- {!!HTML::image('documents/profile_pictures/'.$row->photo,'', array('class'=>'photo'))!!} -->
	           	<div class="temp">بالمقطع</div>
	           	<div class="box" style="background-color:#6f6d73;">
	           	<div class="position">{!!$row->position_dr!!}</div>
	           	</div>
	           	<div class="name">نوم</div>
	           	<div class="name_value">{!!$row->name_dr!!} {!!$row->last_name!!}</div>
	           	{!!HTML::image('img/logo-watermark.png','', array('class'=>'img-water-mark-front'))!!}
	           	<div class="group">
	           		
		           	<table cellspacing=0 cellpadding=0><tr><td>
		           		<div class="title">وظیفه</div><br>
		           		<div class="title_value">{!!$row->current_position_dr!!}</div>
		           </td></tr><tr><td>
		           		<div class="dep">اداره</div> <br>
		           		<div class="dep_value">
		           			
		           			<?php 
		           				if(in_array($row->position_id, $position_ids))
		           				{
		           					echo $row->general_department_dr;
		           				}
		           				else
		           				{
		           					echo $row->department_dr;
		           				}
		           			?>

		           		</div>
		           </td></tr></table>
	      	   </div>
	            {!!HTML::image('img/Header-01.png','', array('class'=>'bg-img'))!!}
	           </div>

	           <div class="col back">
	           
	           	<div class="validaty">Validity</div>
	           	<?php
	           	$sdate = explode("-", $row->expire);
				$sy = $sdate[2];
				$sm = $sdate[1];
				$sd = $sdate[0];
				$sdate = dateToMiladi($sy,$sm,$sd);		 
	           	?>
	           	<div class="validaty_date">{!!$sdate!!}</div>
	           	<img src="{!!getResizedPhoto($row->photo)!!}" class="photo">
	           	<!-- {!!HTML::image('documents/profile_pictures/'.$row->photo,'', array('class'=>'photo'))!!} -->
	           	<div class="temp_back">TEMPORARY</div>
	           	<div class="box" style="background-color:#6f6d73;">
	           	<div class="position">{!!$row->position_en!!}</div>
	           	</div>
	           	<div class="name">Full Name</div>
	           	<div class="name_value">{!!$row->name_en!!}</div>
	           	{!!HTML::image('img/logo-watermark.png','', array('class'=>'img-water-mark-back'))!!}
	           	<div class="group">
		           	<table cellspacing=0 cellpadding=0><tr><td>
		           		<div class="title">Title</div><br>
		           		<div class="title_value">{!!$row->current_position_en!!}</div>
		           </td></tr><tr><td>
		           		<div class="dep">Department</div> <br>
		           		<div class="dep_value">
		           			<?php 
		           				if(in_array($row->position_id, $position_ids))
		           				{
		           					echo $row->general_department_en;
		           				}
		           				else
		           				{
		           					echo $row->department_en;
		           				}
		           			?>
		           		</div>
		           </td></tr></table>
	      	   </div>

	      	   <div class="blood">Blood Group</div>
	      	   <div class="blood_value">{!!$row->blood_group!!}</div>
	      	   <div class="email">info@aop.gov.af &nbsp;+93 20 2147900</div>

	            {!!HTML::image('img/Blank-02.png','', array('class'=>'bg-img'))!!}
	           </div>

	        </div>
        </div>
        
	</body>
</html>