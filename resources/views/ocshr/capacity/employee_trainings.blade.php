<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>نام</th>
                        <th>محل آموزش</th>
                        <th>داخلی/خارجی</th>
                        <th>تاریخ شروع</th>
                        <th>تاریخ ختم</th>
                      </tr>
                    </thead>
                    <tbody>
                    @if($details)
                    <?php $i = 1; ?>
                   		@foreach($details AS $row)
                   		<tr>
                    	<td>{!!$i!!}</td>
                    	<td>{!!$row->title!!}</td>
                    	<td>{!!$row->location!!}</td>
                    	<td>{!!$row->type!!}</td>
                    	<td>{!!$row->start_date!!}</td>
                    	<td>{!!$row->end_date!!}</td>
                    
                    	</tr>
                    	<?php $i++; ?>
                    	@endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>
<!-- Modal End -->
<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>

<div class="md-overlay"></div><!-- the overlay element -->
<script>
	function load_change_employee(id)
	{
		var page = "{!!URL::route('load_change_employee')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&id='+id,
	        success: function(r){
				$('#change_employee').html(r);
	        }
	    });
	}
</script>