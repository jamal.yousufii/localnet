@extends('layouts.master')

@section('head')
	{!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
    <title>{!!_('capacity_building')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>کارمندان</span></li>
        </ol>
    </div>
</div>
@if(canAdd('hr_recruitment'))
<header class="main-box-header clearfix">
    
</header>
@endif
<!-- Example Tabs -->
<div class="example-wrap">
	<div class="nav-tabs-horizontal">
	  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
	    <li class="active" role="presentation"><a data-toggle="tab" href="#all" aria-controls="exampleTabsOne"
	      role="tab">تمام کارمندان</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_capacity('trainings')" href="#other_tab" aria-controls="exampleTabsTwo"
	      role="tab">برنامه های آموزشی</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_capacity('trainers')" href="#other_tab" aria-controls="exampleTabsThree"
	      role="tab">آموزش دهندگان</a></li>
	    
	  </ul>
	  <div class="tab-content padding-top-20">
	    <div class="tab-pane active" id="all" role="tabpanel">
	    	<div class="row">
			    <div class="col-lg-12">
			        <div class="main-box">
			            <div class="main-box-body clearfix">
			            <div class="table-responsive">
			                <table class="table table-responsive" id='list'>
			                    <thead>
			                    <tr>
			                        <th>#</th>
			                        <th>نام کامل</th>
			                        <th>ولد</th>
			                        <th>شماره تعینات</th>
			                        <th>جنسیت</th>
			                        <th>سکونت اصلی</th>
			                        
			                        <th>رتبه</th>
			                        <th>بست</th>
			                        <th>تاریخ تقرر</th>
			                        <th>تحصیلات</th>
			                        <th>عملیه</th>
			                    </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>
			            </div>
			        
			            </div>
			        </div>
			    </div>
			</div>
	    </div>
	    <div class="tab-pane" id="other_tab" role="tabpanel">
	    	
	    </div>
	    
	  </div>
	</div>
</div>
<!-- End Example Tabs -->


<div class="modal fade modal-fade-in-scale-up" id="employee_trainings" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>
<div class="modal fade modal-fade-in-scale-up" id="add_training" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">

<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="@if(canAdd('hr_capacity')) {!!URL::route('addTrainingToEmployee')!!} @endif">
			    <input type="hidden" name="employee_id" id="employee_id" value="">	 
			    <div class="panel-heading">
			      <h5 class="panel-title">جزییات</h5>
			    </div>
	                
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">آموزش</label>
	                                <select name="training_id" id="general_department" class="form-control">
	                                        <option value="">انتخاب</option>
	                                    @if($trainings)
	                                    @foreach($trainings AS $dep_item)
	                                    	 <option value='{!!$dep_item->id!!}'>{!!$dep_item->title!!}</option>
	                                        
	                                    @endforeach
	                                    @endif
	                                </select>
	                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	                			</div>
	                		</div>
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_capacity'))
	                					<button class="btn btn-primary" type="submit"><i class="fa fa-refresh fa-lg"></i> ذخیره</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>
				      	</div>
				    </div>
				    
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {
				"order": [[ 1, 'asc' ]],
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getEmployeeCapacityData')!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });

function load_capacity(type)
{
    var page = "{!!URL::route('loadCapacityTrainings')!!}";
    
    $.ajax({
        url: page,
        type: 'post',
        data: '&type='+type,
        //dataType:'HTML',
        success: function(r)
      	{
        	$('#other_tab').html(r);
        }
    });   
}
function load_employee_trainings(id)
{
	var page = "{!!URL::route('load_employee_trainings')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id,
        success: function(r){
			$('#employee_trainings').html(r);
        }
    });
}

</script>
@stop

