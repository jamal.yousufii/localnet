<header class="main-box-header clearfix">
    <h2>
		<a href="javascript:void()" class="btn btn-primary pull-right" data-target="#change_employee" data-toggle="modal">
            <i class="fa fa-plus-circle fa-lg"></i>
        </a>
    </h2>
</header>
<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='fired_list'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>نام</th>
                        <th>محل آموزش</th>
                        <th>داخلی/خارجی</th>
                        <th>تعداد کارمندان</th>
                        <th>تاریخ شروع</th>
                        <th>تاریخ ختم</th>
                        <th>عملیه</th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="@if(canAdd('hr_capacity')) {!!URL::route('postNewTraining')!!} @endif">
			    
			    <div class="panel-heading">
			      <h5 class="panel-title">جزییات برنامه آموزشی </h5>
			    </div>
			    	<div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">داخلی/خارجی</label>
	                                <select class="form-control" name="type" onchange="showExteranls(this.value)">
	                                    
	                                    <option value="0">داخلی</optioin>
	                                    <option value = "1">خارجی</option>
	                                    
	                                </select>
	                			</div>
	                		</div>	
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">عنوان</label>
	                                <input class="form-control" type="text" name="title" value="" required="required">
	                                
				      			</div>
				      		</div>
					      	<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">محل آموزش</label>
	                                <input class="form-control" type="text" name="location" value="">
				      			</div>
				      		</div>	
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">میعاد</label>
	                                <input class="form-control" type="text" name="days_no" value="">
				      			</div>
				      		</div>
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">تاریخ شروع</label>
	                                <input class="form-control datepicker_farsi" type="text" name="start_date" value="">
	                                
				      			</div>
				      		</div>
					      	<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">تاریخ ختم</label>
	                                <input class="form-control datepicker_farsi" type="text" name="end_date" value="">
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">برگزارکننده</label>
	                                <input class="form-control" type="text" name="organizer" value="">
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">تمویل کننده</label>
	                                <input class="form-control" type="text" name="sponser" value="">
				      			</div>
				      		</div>
	                		
	                	</div>
	                </div>
	                <div id="external_div" style="display:none">
		                <div class="container-fluid">
		                	<div class="row">
		                		<div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">نوعیت آموزش</label>
		                                <select class="form-control" name="external_type">
		                                    <option value=''>انتخاب</option>
		                                    <option value="0">اسکالرشیپ</optioin>
		                                    <option value = "1">فلوشیپ</option>
		                                    
		                                </select>
		                			</div>
		                		</div>	
		                		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">تاریخ حکم</label>
		                                <input class="form-control datepicker_farsi" type="text" name="hokm_date" value="">
					      			</div>
					      		</div>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">شماره حکم</label>
		                                <input class="form-control" type="text" name="hokm_no" value="">
					      			</div>
					      		</div>
					      		<div class="col-sm-3">
		                        	<div class="col-sm-12">
		                        		<label class="col-sm-12 ">فایل</label>
		                                <input class="form-control" type="file" name="scan">
		                        	</div>
		                        </div>
		                	</div>
		                </div>
		            </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">تعداد کارمندان</label>
	                                <input class="form-control" type="text" name="seat_no" value="">
				      			</div>
				      		</div>
	                		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_capacity'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>
	                	</div>
	                </div
	               
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="edit_training" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->

<script type="text/javascript">
 $(".datepicker_farsi").persianDatepicker(); 
    $(document).ready(function() {
        $('#fired_list').dataTable(
            {

                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getCapacityTrainingsData')!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });
    function showExteranls(value)
    {
    	if(value == 0)
        {//military
            $('#external_div').slideUp();

        }
        else
        {
            $('#external_div').slideDown();
        }
    }
    function load_training(id)
	{
		var page = "{!!URL::route('load_training')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&id='+id,
	        success: function(r){
				$('#edit_training').html(r);
	        }
	    });
	}
</script>

