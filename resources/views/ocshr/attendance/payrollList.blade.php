@extends('layouts.master')

@section('head')
	
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li><a href="javascript:void()" onclick="history.back()">حاضری کارمندان</a></li>
            <li class="active"><span>راپور معاشات کارمندان</span></li>
        </ol>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix" style="margin-top:10px">
		    <h2>
		    	<a href="{!!URL::route('printPayroll',array($dep_id,$year,$month))!!}" class="btn btn-primary pull-right">
					پرنت اکسل
				</a>
		        
		    </h2>
		</header>
		<div class="modal-body">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table">
                    <thead>
                      	<tr>
	                        <th>#</th>
	                        <th>نام کامل</th>
	                        <th>ولد</th>
	                        <th>وظیفه</th>
	                        <th> معاش</th>
	                        <th>جمله امتیازات</th>
	                        <th>جمله کسرات</th>
	                       
	                        <th>تقاعد</th>
	                        <th>غیرحاضر</th>
	                        <th>مالیات</th>
	                        <th>مبلغ قابل پرداخت</th>
	                        <th>عملیه</th>
	                    </tr>
                    </thead>
                    <tbody>
                    @if($employees)
                    <?php $i = 1; ?>
                   		@foreach($employees AS $row)
                   		<?php
                   		$total_benifits = getEmployeePayrollDetTotal($row->id,$year,$month,'in');//sum of total benifits
                   		$total_ksorat 	= getEmployeePayrollDetTotal($row->id,$year,$month,'out');//sum of ksorat
                   		$salary_wait 	= getEmployeeSalaryWait($row->id,$year,$month);//is he salary wait
                   		
                   		$leaves = getEmployeeLeaveDays($row->id,$year,$month);
                   		$presents = getEmployeePresentDays($row->RFID,$year,$month)+getPresentedDays($row->RFID,$year,$month);
                   		//$holidays = countHolidays($year,$month);//fridays and 5shanba
                   		$paid_days = $leaves + $presents;
                   		$monthDays = countMonthDays($year,$month);
                   		$unpaid_days = $monthDays - ($paid_days);
                   		
                   		$total_salary = $row->extra_salary+$row->main_salary+$row->prof_salary+$row->kadri_salary;
                   		$salary_perday = 0;//emtiazi
                   		$main_salary_perday = 0;//asli
                   		$prof_salary_perday = 0;//maslaki
                   		$kadri_salary_perday = 0;//kadri
                   		$extra_tax = calculateTax($row->extra_salary);
                   		$main_tax = calculateTax($row->main_salary);
                   		$prof_tax = calculateTax($row->prof_salary);
                   		$kadri_tax = calculateTax($row->kadri_salary);
                   		$total_tax = $main_tax+$extra_tax+$prof_tax+$kadri_tax;
                   		if($row->extra_salary!=0)
                   		{
                   			$salary_perday = $row->extra_salary/30;
                   		}
                   		if($row->main_salary!=0)
                   		{
                   			$main_salary_perday = $row->main_salary/30;
                   		}
                   		if($row->prof_salary!=0)
                   		{
                   			$prof_salary_perday = $row->prof_salary/30;
                   		}
                   		if($row->kadri_salary!=0)
                   		{
                   			$kadri_salary_perday = $row->kadri_salary/30;
                   		}
                   		
                   		$absent_deduction = round($unpaid_days*$salary_perday)+round($unpaid_days*$main_salary_perday)+round($unpaid_days*$prof_salary_perday)+round($unpaid_days*$kadri_salary_perday);
                   		
                   		//calculate the amount of salary for paiddays
                   		//$paidDay_salary = ($presents*$salary_perday)+($presents*$main_salary_perday)+($presents*$prof_salary_perday)+($presents*$kadri_salary_perday);
                   		//$absent_deductioin = $total_salary - $paidDay_salary;
                   		?>
                   		<tr>
                    	<td>{!!$i!!}</td>
                    	<td>{!!$row->name!!}</td>
                    	<td>{!!$row->father_name!!}</td>
                    	<td>{!!$row->current_position_dr!!}</td>
                    	<td>{!!$total_salary!!}</td>
                    	<td>{!!$total_benifits!!}</td>
                    	<td>{!!$total_ksorat!!}</td>
                    	
                    	<td>{!!$row->retirment!!}</td>
                    	<td>{!!$absent_deduction!!}</td>
                    	<td>{!!$total_tax!!}</td>
                    	<td>{!!($total_salary+$total_benifits)-($row->retirment+$total_tax+$absent_deduction+$total_ksorat)!!}</td>
                    	<td>
                    	@if($salary_wait)
                    		<a href="javascript:void()" onclick="alert('{!!$salary_wait->desc!!}')">معطل به معاش</a>
	                    @else
	                    	@if($row->RFID!=0)
	                    		<a href="javascript:void()" onclick="edit_payroll({!!$row->id!!},{!!$row->RFID!!})" data-target="#payroll_modal" data-toggle="modal"><i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i></a> 
	                    	@else
	                    		<a href="javascript:void()" onclick="alert('User does not have RFID')">جزییات</a>
	                    	@endif
	                    @endif
                    	</td>
                    	</tr>
                    	<?php $i++; ?>
                    	@endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="payroll_modal" aria-hidden="true" aria-labelledby="leave_modal" role="dialog" tabindex="-1">

</div>
<div class="md-overlay"></div>
@stop
<script>
	function edit_payroll(id,rfid)
	{
		var year = "{!!$year!!}";
		var month = "{!!$month!!}";
		var dep_id= "{!!$dep_id!!}";
		var page = "{!!URL::route('editPayroll')!!}";
		
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&id='+id+'&rfid='+rfid+'&year='+year+'&month='+month+'&dep_id='+dep_id,
	        success: function(r){
				$('#payroll_modal').html(r);
	        }
	    });
	}
</script>