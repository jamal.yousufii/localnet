<div class="modal-dialog" style="width: 70%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" id="time_close_btn" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" id="salaryWait" method="post">
			    <div class="panel-heading">
			      <h5 class="panel-title">ملاحضات</h5>
			    </div>
                <div class="container-fluid">
                	<div class="row">
                		<div class="col-sm-9">
			      			<div class="col-sm-12">
			      				<label class="col-sm-12 "></label>
                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
                                <textarea class="form-control" row="5" col="10" name="desc" required>@if($details){!!$details->desc!!}@endif</textarea>
                                
			      			</div>
			      		</div>
                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
                				@if(canAdd('hr_attendance'))
                					<button class="btn btn-primary" type="button" onclick="submit_salaryWait();$('#time_close_btn').click()"> ذخیره</button>
                				@else
                					<p>You dont have permission</p>
                				@endif
                				<button class="btn btn-danger" type="button" onclick="$('#time_close_btn').click()"> کنسل</button>
                			</div>
                		</div>
                	</div>
                </div>
	            <input type="hidden" name="employee_id" value="{!!$id!!}"/>
	            <input type="hidden" name="year" value="{!!$year!!}"/>
	            <input type="hidden" name="month" value="{!!$month!!}"/>
				{!! Form::token() !!}
			</form>
			</div>
			</div>		
		</div>		                       
	</div>
</div>
<script>
function submit_salaryWait()
{
	var datas = $('#salaryWait').serialize();
	var page = "{!!URL::route('postEmployeeSalaryWait')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: datas,
        success: function(r){
        	
        }
    });
}
</script>