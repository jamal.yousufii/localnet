@extends('layouts.master')

@section('head')
    <title>{!!_('attendance')!!}</title>
@stop
@section('content')
<div class="row">
	<div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>لیست محصلین</span></li>
        </ol>
    </div>
</div>
<?php
if(isset($dep_id)){$dep = $dep_id;}else{$dep=0;};
if(isset($sub_dep_id)){$sub = $sub_dep_id;}else{$sub=0;};
if(isset($status)){$sta = $status;}else{$sta=0;};
if(isset($gender)){$gen = $gender;}else{$gen=0;};
?>
<header class="main-box-header clearfix">
    	<div class="row">
        	<div class="col-lg-12">
        		<div class="container-fluid">
			      	<div class="row">
					    <div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی</label>
                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                    <option value="">انتخاب</option>
                                    @foreach($parentDeps AS $dep_item)
                                    	@if($dep_item->id == $dep)
                                    		<option value='{!!$dep_item->id!!}' selected>{!!$dep_item->name!!}</option>
                                    	@else
                                        	<option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ مربوط</label>
                                <select class="form-control" name="sub_dep" id="sub_dep">
                                    <option value=''>انتخاب</option>
                                    @if(isset($sub_deps))
	                                    @foreach($sub_deps AS $deps)
	                                    	@if($deps->id == $sub)
	                                    		<option value='{!!$deps->id!!}' selected>{!!$deps->name!!}</option>
	                                    	@else
	                                        	<option value='{!!$deps->id!!}'>{!!$deps->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                @endif
                                </select>
                			</div>
                		</div>
                		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">وضعیت</label>
                                <select class="form-control" name="status" id="status">
                                    <option value='0'>انتخاب</option>
                                    <option value='1' @if($sta==1) {!!'selected'!!} @endif>صبح</option>
                                    <option value='2' @if($sta==2) {!!'selected'!!} @endif>بعدازظهر</option>
                                    <option value='3' @if($sta==3) {!!'selected'!!} @endif>پنجشنبه</option>
                                </select>
                			</div>
                		</div>
                		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">جنسیت</label>
                                <select class="form-control" name="gender" id="gender">
                                    <option value='0'>انتخاب</option>
                                    <option value='1' @if($gen==1) {!!'selected'!!} @endif>مرد</option>
                                    <option value='2' @if($gen==2) {!!'selected'!!} @endif>زن</option>
                                </select>
                			</div>
                		</div>
					</div>
					<div class="row">
						
						<div class="col-sm-10">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
								<a href="javascript:void()" onclick="shiftReport()" class="btn btn-primary">راپور</a>
								<a href="javascript:void()" onclick="shiftPrint()" class="btn btn-primary">پرنت اکسل</a>
							</div>
						</div>
						
						<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
								مجموع: {!!$total!!}
							</div>
						</div>
					</div>
				</div>
				
			</form>
			</div>
		</div>
    
</header>
<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix" style="margin-top:10px">
		    
		</header>
		<div class="modal-body">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table" id="list">
                    <thead>
                      	<tr>
	                        <th>#</th>
	                        <th>نام کامل</th>
	                        <th>ولد</th>
	                        <th>ریاست</th>
	                        <th>وظیفه</th>
	                        <th>شیفت</th>
	                        
	                    </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
	$('#list').dataTable(
	    {
	        'sDom': 'lf<"clearfix">tip',
	        "bProcessing": true,
	        "bServerSide": true,
	        "iDisplayLength": 10,
	        "sAjaxSource": "{!!URL::route('getShiftEmployeesData',array($dep,$sub,$sta,$gen))!!}",
	        "language": {
	            "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
	            "zeroRecords": "ریکارد موجود نیست",
	            "info": "نمایش صفحه _PAGE_ از _PAGES_",
	            "infoEmpty": "ریکارد موجود نیست",
	            "search": "جستجو",
	            "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
	        }
	    }
	);
	
});
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function shiftReport()
{
	var general_department = $('#general_department').val();
	if(general_department=='')
	{
		general_department = 0;
	}
	var sub_dep = $('#sub_dep').val();
	if(sub_dep=='')
	{
		sub_dep = 0;
	}
	var status  = $('#status').val();
	var gender  = $('#gender').val();
    window.location = "/hr/getShiftEmployees/"+general_department+"/"+sub_dep+"/"+status+"/"+gender;
    
}
function shiftPrint()
{
	var status = $('#status').val();
	var general_department = $('#general_department').val();
	if(general_department=='')
	{
		general_department = 0;
	}
	var sub_dep = $('#sub_dep').val();
	if(sub_dep=='')
	{
		sub_dep = 0;
	}
	var gender  = $('#gender').val();
    window.location = "/hr/printShiftEmployees/"+general_department+"/"+sub_dep+"/"+status+"/"+gender;
    
}
</script>
@stop