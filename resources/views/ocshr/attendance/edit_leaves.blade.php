@extends('layouts.master')

@section('head')
	
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li><a href="{!!URL::route('leavesManager')!!}">رخصتی کارمندان</a></li>
            <li class="active"><span>{!!$year!!}</span></li>
            <li class="active"><span>{!!getEmployeeFullName($id)!!}</span></li>
        </ol>
        
    </div>
</div>
<header class="main-box-header clearfix">
<div class="row" id="main_content">
	<div class="col-lg-12">
		<header class="main-box-header clearfix">
		    مجموع رخصتی های سال {!!$year!!}: <span>{!!getEmployeeLeaves_total($id,$year,0)!!}</span>
	    	@if(canAddHoliday())
	    	<a href="javascript:void()" class="btn btn-primary pull-right" onclick="$('#main_content').slideUp();$('#all').slideDown();">
				<i class="fa fa-plus-circle fa-lg"></i>
			</a>
		    @endif
		</header>
		<div class="modal-body">
            <div class="example-wrap">
                <div class="example table-responsive">
                  <table class="table">
                    <thead>
                      	<tr>
	                        <th>#</th>
	                        <th>نوعیت رخصتی</th>
	                        <th>از تاریخ</th>
	                        <th>تا تاریخ</th>
	                        <th>تعداد روز </th>
	                        <th>فایل</th>
	                        <th>تفصیلات</th>
	                        <th>عملیه</th>
	                    </tr>
                    </thead>
                    <tbody>
                    <?php $types = array('0'=>'',
                    					 '1'=>'ضروری',
                    					 '2'=>'تفریحی',
                    					 '3'=>'مریضی',
                    					 '4'=>'ولادی',
                    					 '5'=>'عروسی',
                    					 '6'=>'حج',
                    					 '7'=>'دیگر(خدمتی)',
                    					 '8'=>'اضافه رخصتی مریضی',
                    					 '9'=>'دیگر موارد'
                    )
                    
                    ?>
                    @if($details)
                    <?php $i = 1; ?>
                   		@foreach($details AS $row)
                   		<?php $sdate = $row->date_from;$edate = $row->date_to;
                		if($sdate !=''){$sdate = explode('-',$sdate);$sdate=dateToShamsi($sdate[0],$sdate[1],$sdate[2]);$sdate=jalali_format($sdate);}
                		if($edate !=''){$edate = explode('-',$edate);$edate=dateToShamsi($edate[0],$edate[1],$edate[2]);$edate=jalali_format($edate);}
                		?>
                   		<tr>
                    	<td>{!!$i!!}</td>
                    	<td>{!!$types[$row->type]!!}</td>
                    	<td>{!!$sdate!!}</td>
                    	<td>{!!$edate!!}</td>
                    	<td>{!!$row->days_no!!}</td>
              			<td><a href="{!!URL::route('getLeaveDoc',$row->id)!!}">{!!$row->file_name!!}</a></td>
              			<td>{!!$row->desc!!}</td>
                    	<td>
                    		@if(canAddHoliday())
                    		<a href="javascript:void()" onclick="getEditLeaveForm({!!$row->id!!})">
								<i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							<a href="javascript:void()" onclick="delete_leave({!!$row->id!!})">
								<i class="icon fa-close" aria-hidden="true" style="font-size: 16px;"></i>
							</a>
							@endif
                    	</td>
                    	</tr>
                    	<?php $i++; ?>
                    	@endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>
</header>
<div class="row" id="edit" style="display:none;"></div>
<div class="row" id="all" style="display:none;">
<div class="col-lg-12">
    <form class="form-horizontal" id="leaves" role="form" method="post" action="{!!URL::route('postLeaves')!!}" enctype="multipart/form-data">
        <div class="container-fluid">
        	<div class="row">
	      		 <div class="col-sm-4">
                	<div class="col-sm-12">
                		<label class="col-sm-12 ">نوع رخصتی</label>
                        <select class="form-control" name="type" id="type" required onchange="check_leave_validity()">
                            <option value=''>انتخاب</option>
                            <option value='1'>ضروری</option>
                            <option value='2'>تفریحی</option>
                            <option value='3'>مریضی</option>
                            <option value='8'>اضافه رخصتی مریضی</option>
                            
                            <option value='4'>ولادی</option>
                            <!--
                            <option value='5'>عروسی</option>
                            -->
                            <option value='6'>حج</option>
                            
                            <option value='7'>دیگر(خدمتی)</option>
                            <option value='9'>دیگر موارد</option>
                            <option value='10'>شامل کتاب حاضری</option>
                        </select>
                	</div>
                </div>
	      		<div class="col-sm-4">
                	<div class="col-sm-12">
                		<label class="col-sm-12 ">از تاریخ</label>
                        <input class="form-control datepicker_farsi" readonly type="text" name="from_date" id="from_date" required>
                	</div>
                </div>
                <div class="col-sm-4">
                	<div class="col-sm-12">
                		<label class="col-sm-12 ">تا تاریخ</label>
                        <input class="form-control datepicker_farsi" readonly type="text" name="to_date" id="to_date" required onchange="check_leave_validity_date()">
                	</div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
        	<div class="row">
        		<div class="col-sm-4">
            		<div class="col-sm-12">
                		<label class="col-sm-12 ">فورم</label>
                		
                        <input type='file'  name='scan' class="form-control">
                	</div>
                </div>
                <div class="col-sm-8">
            		<div class="col-sm-12">
                		<label class="col-sm-12 ">تفصیلات</label>
                		<textarea class="form-control" row="2" col="10" name="desc"></textarea>
                	</div>
                </div>
        	</div>
        </div>
        <input type="hidden" id="employee_id" name="employee_id" value="{!!$id!!}" />
        <input type="hidden" name="year" id="year" value="{!!$year!!}" />
        <input type="hidden" name="month" id="month" value="{!!$month!!}" />
        
	    <div class="container-fluid">
        	<div class="row">
        		<div class="col-sm-12">
        			<div class="col-sm-6" id="btn_not_valid">
        				
        			</div>
        			<div class="col-sm-6">
        				<label class="col-sm-12">&nbsp;</label>
        				<div id="btn_div" style="display:inline">
        				@if(canAttHoliday())
        					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
        				@else
        					<p>You dont have permission</p>
        				@endif
        				</div>
        				<button class="btn btn-danger" type="button" onclick="$('#main_content').slideDown();$('#all').slideUp()">{!!_('cancel')!!}</button>
        			</div>
        			
        		</div>			            				           
	      	</div>
	    </div>
		{!! Form::token() !!}
	</form>
</div>
</div>

@stop
@section('footer-scripts')

<script type="text/javascript">

    function getEditLeaveForm(id)
    {
    	var year = $('#year').val();
    	var month= $('#month').val();
    	$.ajax({
                url: '{!!URL::route("getEditLeaveForm")!!}',
                data: '&id='+id+'&year='+year+'&month='+month,
                type: 'post',
                
                success: function(response)
                {
                	$('#main_content').slideUp();
                	$('#edit').slideDown();
                	$('#edit').html(response);
                   
                }
            }
        );
    }
    function delete_leave(id)
	{
	    var confirmed = confirm("Do you want to delete?");
	    if(confirmed)
	    {
	        $.ajax({
	                url: '{!!URL::route("deleteEmployeeLeave")!!}',
	                data: '&id='+id,
	                type: 'post',
	                
	                success: function(response)
	                {
	                    location.reload();
	                }
	            }
	        );
	    }
	
	}
	function check_leave_validity()
	{
		var type = $('#type').val();
		var id = $('#employee_id').val();
		var year = $('#year').val();
		$.ajax({
                url: '{!!URL::route("check_leave_validity")!!}',
                data: '&type='+type+'&id='+id+'&year='+year,
                type: 'post',
                
                success: function(r)
                {
                    if(r=='true')
                    {
                    	$('#btn_div').hide();
                    	$('#btn_not_valid').html('<p style="color:red">You can not add more holiday of this type for year '+year+'</p>');
                    }
                    else
                    {
                    	$('#btn_div').show();
                    	$('#btn_not_valid').html('');
                    }
                }
            }
        );
	}
	function check_leave_validity1()
	{
		var type = $('#edit_type').val();
		var id = $('#employee_id').val();
		var year = $('#year').val();
		$.ajax({
                url: '{!!URL::route("check_leave_validity")!!}',
                data: '&type='+type+'&id='+id+'&year='+year,
                type: 'post',
                
                success: function(r)
                {
                    if(r=='true')
                    {
                    	$('#btn_div_edit').hide();
                    	$('#btn_not_valid_edit').html('<p style="color:red">You can not add more holiday of this type for year '+year+'</p>');
                    }
                    else
                    {
                    	$('#btn_div_edit').show();
                    	$('#btn_not_valid_edit').html('');
                    }
                }
            }
        );
	}
	function check_leave_validity_date()
	{
		var type = $('#type').val();
		var id = $('#employee_id').val();
		var date_from = $('#from_date').val();
		var date_to = $('#to_date').val();
		if(date_from=='' || date_to=='')
		{
			$('#to_date').val('');
			$('#btn_div').hide();
			alert('select start date first');
			return;
		}
		else
		{
			$('#btn_div').show();
		}
		if(checkBothDate(date_from,date_to))
		{
			$('#btn_div').hide();
			alert('start date and end date must be at the same month');
			return;
		}
		else
		{
			$('#btn_div').show();
		}
		$.ajax({
                url: '{!!URL::route("check_leave_validity_date")!!}',
                data: '&type='+type+'&id='+id+'&date_from='+date_from+'&date_to='+date_to,
                type: 'post',
                dataType: 'json',
                success: function(r)
                {
                    if(r.result=='true')
                    {
                    	$('#btn_div').hide();
                    	$('#btn_not_valid').html('<p style="color:red">You can not add more holiday, '+r.leaves+' days is used</p>');
                    }
                    else
                    {
                    	$('#btn_div').show();
                    	$('#btn_not_valid').html('');
                    }
                }
            }
        );
	}
	function checkBothDate(date_from,date_to)
	{
		var smonth = date_from.split("-"); 
		var emonth = date_to.split("-"); 
	    if(smonth[1]==emonth[1])
	    {
	    	return false;
	    }
	    else
	    {
	    	return true;
	    }
	}
	function check_leave_validity_date1()
	{
		var type = $('#edit_type').val();
		var id = $('#employee_id').val();
		var date_from = $('#edit_from_date').val();
		var date_to = $('#edit_to_date').val();
		if(date_from=='' || date_to=='')
		{
			$('#edit_to_date').val('');
			$('#btn_div_edit').hide();
			alert('select start date first');
			return;
		}
		else
		{
			$('#btn_div_edit').show();
		}
		if(checkBothDate(date_from,date_to))
		{
			$('#btn_div_edit').hide();
			alert('start date and end date must be at the same month');
			return;
		}
		else
		{
			$('#btn_div_edit').show();
		}
		$.ajax({
                url: '{!!URL::route("check_leave_validity_date")!!}',
                data: '&type='+type+'&id='+id+'&date_from='+date_from+'&date_to='+date_to,
                type: 'post',
                dataType: 'json',
                success: function(r)
                {
                    if(r.result=='true')
                    {
                    	$('#btn_div_edit').hide();
                    	$('#btn_not_valid_edit').html('<p style="color:red">You can not add more holiday, '+r.leaves+' days is used</p>');
                    }
                    else
                    {
                    	$('#btn_div_edit').show();
                    	$('#btn_not_valid_edit').html('');
                    }
                }
            }
        );
	}
</script>
@stop

