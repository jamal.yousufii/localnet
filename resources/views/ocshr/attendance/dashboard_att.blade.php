@extends('layouts.master')

@section('head')
	<title>Dashboard</title>
	
@stop

@section('content')

<div class="modal-body">
	<div class="row">
		<div class="col-md-4">
			<div class="main-box clearfix">
                <header class="main-box-header clearfix">
                    <h2>{!!Auth::user()->first_name.' '.Auth::user()->last_name!!}</span></h2>
                </header>
                <div class="main-box-body clearfix">
                    <?php 
                        //$photo = getProfilePicture(Auth::user()->id);
                        $photo = DB::connection('hr')->table('employees')->where('id',Auth::user()->employee_id)->pluck('photo');
                    ?>
                    <div id="user_profile_picture">
                        <div id="hr_profile">
                        {!! HTML::image('/documents/profile_pictures/'.$photo, '', array('class' => 'profile_pic','style'=>'width:220px;border-radius:10px;')) !!}
                        <!-- <img src="img/samples/scarlet-159.png" alt="" class="profile-img img-responsive center-block"> -->
                       	</div>
                   	</div>
                </div>
	        </div>
	    </div>
	    <div class="col-md-4">
            <div class="main-box clearfix">
                
                <div class="main-box-body clearfix">
                    
                    <div class="profile-details">
                        
                            <label style="font-weight:bold;padding:5px;">Username:</label> <span>{!!Auth::user()->username!!}</span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">Full Name:</label> <span>{!!Auth::user()->first_name.' '.Auth::user()->last_name!!}</span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">Father Name:</label> <span>{!!Auth::user()->father_name!!}</span>
                            
                            <br>
                            <label style="font-weight:bold;padding:5px;">Position:</label> <span>{!!getPositionNameHR(Auth::user()->id)!!}</span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">Department:</label> <span>{!!getDepartmentName(Auth::user()->department_id)!!}</span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">Email:</label> <span>{!!Auth::user()->email!!}</span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">Phone:</label> <span>{!!getEmployeePhone(Auth::user()->id)!!}</span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">User Type:</label> 
                            <span>
                            @if(Auth::user()->position_id==1) Normal User
                            @elseif(Auth::user()->position_id==2) Director
                            @elseif(Auth::user()->position_id==3) Deputy
                            @endif
                            </span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">Status:</label> 
                            <span>
                                @if(Auth::user()->status==1)
                                    <i class="fa fa-check" style="color:#8bc34a;"></i> Active
                                @else
                                    <i class="fa fa-cancel" style="color:red;"></i> Deactivated
                                @endif
                            </span>
                        
                            
                    </div>
                    
                </div>
                
            </div>
        </div>
	    <div class="col-md-4">
	        <div class="main-box clearfix" >
	            <div class="main-box-body clearfix">
	                <div class="profile-details">
	                <?php $date = dateToShamsi(date('Y'),date('m'),date('d')); 
	                $sh_date = explode('-',$date); $year = $sh_date[0];
	                ?>   
	                        <label style="font-weight:bold;padding:5px;">مجموع رخصتی های سال {!!$year!!}:</label> 
	                        <span>{!!getEmployeeLeaves_total(Auth::user()->employee_id,$year,0)!!}</span>
	                        <br>
	                        <label style="font-weight:bold;padding:5px;">مجموع رخصتی های ضروری:</label> 
	                        <span>{!!getEmployeeLeaves(Auth::user()->employee_id,1,$year,0)!!}</span>
	                        <br>
	                        <label style="font-weight:bold;padding:5px;">مجموع رخصتی های تفریحی:</label> 
	                        <span>{!!getEmployeeLeaves(Auth::user()->employee_id,2,$year,0)!!}</span>
	                        <br>
	                        <label style="font-weight:bold;padding:5px;">مجموع رخصتی های مریضی:</label> 
	                        <span>{!!getEmployeeLeaves(Auth::user()->employee_id,3,$year,0)!!}</span>
	                       
	                </div>
	            </div>
	       </div>
	    </div>
	</div>
</div>

@stop





