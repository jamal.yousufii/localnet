@extends('layouts.master')

@section('head')
    <title>{!!_('my_attendance')!!}</title>
<style type="text/css">
.leave_span{
	position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 20px;
}
</style>  
@stop
@section('content')

<header class="main-box-header clearfix" style="margin-top: -30px;">
	<div class="row">
    	<div class="col-lg-12">
    		<div class="container-fluid">
        		<div class="col-sm-4">
        			<div class="col-sm-12" id="img_controller">
        				<div class="icon-box pull-left">
							<img src="/documents/profile_pictures/{!!$photo->photo!!}" alt="Image Not Found!" width="150" hight="224"/>
						</div>
        			</div>
        		</div>
	      		<div class="col-sm-2">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">سال</label>
                        <select name="year" id="year" class="form-control" onchange="bring_employee_images()">
                            <?php 
        					//$month = explode('-',$today);
        					$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');
        					
        					?>
                            @for($i=$year-5;$i<$year+5;$i++)
                            	@if($i==$year)
                                <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
                                @else
                                <option value='{!!$i!!}'>{!!$i!!}</option>
                                @endif
                            @endfor
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
        			</div>
        		</div>
        		<div class="col-sm-2">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">ماه</label>
                        <select name="month" id="month" class="form-control" onchange="bring_employee_images()">
                            @for($j=1;$j<13;$j++)
                            	@if($j==$month)
                                <option value='{!!$j!!}' selected="selected">{!!$month_name[$j]!!}</option>
                                @else
                                <option value='{!!$j!!}'>{!!$month_name[$j]!!}</option>
                                @endif
                            @endfor
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
        			</div>
        		</div>
			    
			</div>
			<div class="container-fluid">
		      	<div class="row">
            		<div class="col-sm-3">
            			{!!$photo->current_position_dr!!}
            		</div>
            		
            		<div class="col-sm-9" style="margin-bottom: 15px;">
            		<h5 style="color: red !important;">
            		لطفا در هنگام پیش نمودن کارت خویش, سه ثانیه بعد از شخص قبلی کارت خود را پیش نمایید تا حاضری شما درست ثبت گردد.
            		</h5>
            		</div>
            	</div>
            </div>
		</div>
	</div>
</header>

<!-- Example Tabs -->
<div class="row">
	<div class="col-lg-12">
	<table width="auto">
	<?php
	$att_time_in = getAttTime($year,$month,'in');
	$att_time_out = getAttTime($year,$month,'out');
	$att_time_in_thu = getAttTime_thu($year,$month,'in');
	$att_time_out_thu = getAttTime_thu($year,$month,'out');
	
	$shift_time = is_in_shift($rfid,$year,$month,1);
	if($shift_time)
	{
		$shift_time_in = $shift_time->time_in;
		$shift_time_out= $shift_time->time_out;
		if($shift_time_in !=null)
		{
			$att_time_in_shift = explode(':',$shift_time_in);
			$shift_time_in = $att_time_in_shift[0].$att_time_in_shift[1].$att_time_in_shift[2];
		}
		if($shift_time_out != null)
		{
			$att_time_out_shift = explode(':',$shift_time_out);
			$shift_time_out = $att_time_out_shift[0].$att_time_out_shift[1].$att_time_out_shift[2];
		}
		
		if($shift_time_in>$att_time_in)
		{
			$att_time_in = $shift_time_in;
		}
		if($shift_time_out<$att_time_out && $shift_time_out != null)
		{
			$att_time_out = $shift_time_out;
		}
	}
	//shifts for thu
	$thu_shift_time = is_in_shift($rfid,$year,$month,2);
	if($thu_shift_time)
	{
		$shift_time_in_thu = $thu_shift_time->time_in;
		$shift_time_out_thu= $thu_shift_time->time_out;
		if($shift_time_in_thu !=null)
		{
			$att_time_in_shift_thu = explode(':',$shift_time_in_thu);
			$shift_time_in_thu = $att_time_in_shift_thu[0].$att_time_in_shift_thu[1].$att_time_in_shift_thu[2];
			$att_time_in_thu = $shift_time_in_thu;
		}
		if($shift_time_out_thu != null)
		{
			$att_time_out_shift_thu = explode(':',$shift_time_out_thu);
			$shift_time_out_thu = $att_time_out_shift_thu[0].$att_time_out_shift_thu[1].$att_time_out_shift_thu[2];
			$att_time_out_thu = $shift_time_out_thu;
		}
	}
	$thu_shift = is_in_thu_shift($rfid,$year,$month);
	
	$sday = att_month_days(0);
	$eday = att_month_days(1);
	if($month==1)
	{
		$from = dateToMiladi($year-1,12,$sday);
	}
	else
	{
		$from = dateToMiladi($year,$month-1,$sday);//the start day of attendance month is 15 of prevouce month
	}
	$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month
	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	$day_counter=1;
	?>
	@foreach($period AS $day)
		<?php
			$the_day = $day->format( "Y-m-d" );
			$dayOfWeek = date('D',strtotime($the_day));
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_monthDay = $period_month.$period_day;
			$shamsi_day = dateToShamsi($period_det[0],$period_det[1],$period_det[2]);
			$imgin_id = '';$imgout_id = '';$signin_time='';$signout_time='';
			$correct = false;$sign_in = false;$sign_out = false;
			$signin_img = '/img/default.jpeg';$signout_img = '/img/default.jpeg';
			$rejected_in = 0;$rejected_out = 0;
			
			$late_status_in = 0;$late_status_out = 0;
			if($dayOfWeek == 'Thu')
			{
				if($thu_shift_time)
				{
					$time_in = $att_time_in_thu;
					$time_out= $att_time_out_thu;
				}
				else
				{
					$time_in = getAttTime_thu($year,$month,'in');
					$time_out = getAttTime_thu($year,$month,'out');
				}
			}
			else
			{
				$time_in = $att_time_in;
				$time_out= $att_time_out;
			}
			//leaves
			$isInLeave = isEmployeeInLeave($photo->id,$the_day,$year,$month);
		?>
		@if($details)
		@foreach($details as $img)
			<?php 
			$path = explode("_",$img->path);
			$time = (int)substr($path[0],-6);//get the time part in path
			$month_day = substr($path[0],-11,4);//get the monthDay part in path
			
			if($period_monthDay == $month_day)
			{
				$late_status = $img->late_status;
				$correct = true;
				if($time <= 120000)
				{
					$sign_in = true;
					$signin_img = "http://10.134.45.19/".$img->path;
					$imgin_id = $img->id;
					$rejected_in = $img->status;
					$signin_time = $time;
					if($time<100000)
					{
						$show_singin_h = substr($time,0,1);
						$show_singin_m = substr($time,1,2);
						$show_singin_s = substr($time,3,2);
					}
					else
					{
						$show_singin_h = substr($time,0,2);
						$show_singin_m = substr($time,2,2);
						$show_singin_s = substr($time,4,2);
					}
					$late_status_in = $img->late_status;
				}
				else
				{
					$sign_out = true;
					$signout_img = "http://10.134.45.19/".$img->path;
					$imgout_id = $img->id;
					$rejected_out = $img->status;
					$signout_time = $time;
					$show_singout_h = substr($time,0,2);
					$show_singout_m = substr($time,2,2);
					$show_singout_s = substr($time,4,2);
					$late_status_out = $img->late_status;
				}
			}
			?>
		@endforeach
		@endif
		<!-- sign in -->
		@if($day_counter == 1 || $day_counter%11==1)
		<tr>
		@endif
		@if($the_day <= date('Y-m-d'))
			
		<?php 
		$holiday = checkHoliday($the_day);
		$urgents = checkUrgentHoliday($the_day);
		?>
		<td style="position: relative;padding-bottom: 2em;padding-right: 1em;">
		<!-- sign in pic -->
			<div style="position: relative;">
				<!-- if there is a pic for this day then send the id to onclick function -->
				<img src="{!!$signin_img!!}" class="att_img" alt="Image Corrupt" height="112" width="90" style="border-top:1px solid;border-left:1px solid;border-right:1px solid"/>
				@if($photo->att_in_date>$the_day)
					<span style="position: absolute;right: 35px;top: 20px;color:red; font-size:15px;background:#FFF;padding:0 10px">شمامل سیستم نمیباشد</span>	
				@else
					@if($isInLeave)
						@if($isInLeave==7)
						<span class="leave_span">خدمتی</span>
						@elseif($isInLeave==9)
						<span class="leave_span">دیگر موارد</span>
						@elseif($isInLeave==1)
						<span class="leave_span">رخصتی ضروری</span>
						@elseif($isInLeave==2)
						<span class="leave_span">رخصتی تفریحی</span>
						@elseif($isInLeave==3)
						<span class="leave_span">رخصتی مریضی</span>
						@elseif($isInLeave==4)
						<span class="leave_span">رخصتی ولادی</span>
						@elseif($isInLeave==5)
						<span class="leave_span">رخصتی عروسی</span>
						@elseif($isInLeave==6)
						<span class="leave_span">رخصتی حج</span>
						@elseif($isInLeave==8)
							<span class="leave_span">اضافه رخصتی مریضی</span>
						@endif
						<span style="position: absolute;right: 5px;top: 80px;color:red; font-size:15px;background:#FFF;padding:0 10px" id="{!!$imgin_id!!}_span">@if($signin_time > $time_in && $late_status==1){!!$show_singin_h!!}:{!!$show_singin_m!!}:{!!$show_singin_s!!}@elseif($rejected_in!=0) Rejected @endif</span>
					@elseif($holiday)
						<span class="leave_span" title="{!!$holiday->desc!!}">رخصتی عمومی</span>
					@elseif($urgents)
						<span style="position: absolute;right: 20px;top: 20px;color:red; font-size:15px;background:#FFF;padding:0 10px" title="{!!$urgents->desc!!}">حالت فوق العاده</span>
					@elseif($dayOfWeek=='Fri')
						<span style="position: absolute;right: 15px;top: 80px;color:red; font-size:15px;background:#FFF;padding:0 10px">جمعه</span>	
					@else
						<span style="position: absolute;right: 5px;top: 80px;color:red; font-size:15px;background:#FFF;padding:0 10px" id="{!!$imgin_id!!}_span">@if($signin_time > $time_in && $late_status_in==1){!!$show_singin_h!!}:{!!$show_singin_m!!}:{!!$show_singin_s!!}@elseif($rejected_in!=0) Rejected @endif</span>
					@endif
				@endif
			</div>
		<!-- sign out pic -->
			<div style="position: relative;">
				<img src="{!!$signout_img!!}" class="att_img" alt="Image Corrupt" height="112" width="90" style="cursor:pointer;border-bottom:1px solid;border-left:1px solid;border-right:1px solid"/>
					<span style="position: absolute;right: 0px;top: 80px;color:red; font-size:15px;background:#FFF;padding:0 10px" id="{!!$imgout_id!!}_span">@if($signout_time < $time_out && $late_status_out==1){!!$show_singout_h!!}:{!!$show_singout_m!!}:{!!$show_singout_s!!}@elseif($rejected_out!=0) Rejected @endif</span>
					@if($dayOfWeek == 'Thu' && $thu_shift)
						<span style="position: absolute;right: 35px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
					@endif
			</div>
			<!--
			($dayOfWeek=='Fri' || $dayOfWeek == 'Thu')
			-->
			@if($dayOfWeek=='Fri')
			<span style="position: absolute;right: 20px;bottom: 1px;color:red">{!!$shamsi_day!!}</span>
			@else
			<span style="position: absolute;right: 20px;bottom: 1px;">{!!$shamsi_day!!}</span>
			@endif
		</td>
		@endif
		@if($day_counter%11==0)
		</tr>
		@endif
		
		<?php $day_counter++;?>
	@endforeach
	</table>
	</div>
</div>
<!-- End Example Tabs -->

@stop
@section('footer-scripts')
<script>
function bring_employee_images()
{
	var rfid = "{!!$enc_rfid!!}";
	var year = $('#year').val();
	var month = $('#month').val();
	
    window.location = "/hr/myAttendance/"+rfid+"/"+year+"/"+month;
    
}
</script>
@stop


