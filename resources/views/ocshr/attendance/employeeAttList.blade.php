@extends('layouts.master')

@section('head')
	{!! HTML::style('/vendor/clockpicker/clockpicker.min.css') !!}
	<style>
	.clockpicker-popover{
		z-index:100000;
	}
	</style>
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
	<div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>حاضری کارمندان</span></li>
        </ol>
        @if(canView('hr_attendance'))
        	<button class="btn btn-primary pull-right" type="button" onclick="getSearchResult('all')"> راپور حاضری</button>
        	
       	@elseif(canAttHoliday())
       		<a href="{!!URL::route('leavesManager')!!}" class="btn btn-primary">مدیریت رخصتی ها</a>
       	@endif
       	@if(canCheckImages())
       		<button class="btn btn-primary pull-right" type="button" onclick="getSearchResult('all')"> راپور حاضری</button>
       	@endif
    </div>
</div>
<header class="main-box-header clearfix">
   		<form class="form-horizontal" role="form" method="post" id="att_report">
    	<div class="row">
        	<div class="col-lg-12">
        	
        		<div class="container-fluid">
			      	<div class="row">
			      		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">سال</label>
                                <select name="year" id="year" class="form-control">
                                    <?php 
                					//$month = explode('-',$today);
                					$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');
                					
                					?>
                                    @for($i=$year-5;$i<$year+5;$i++)
                                    	@if($i==$year)
                                        <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
                                        @else
                                        <option value='{!!$i!!}'>{!!$i!!}</option>
                                        @endif
                                    @endfor
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ماه</label>
                				
                                <select name="month" id="month" class="form-control">
                                       
                                    @for($j=1;$j<13;$j++)
                                    	@if($j==$month)
                                        <option value='{!!$j!!}' selected="selected">{!!$month_name[$j]!!}</option>
                                        @else
                                        <option value='{!!$j!!}'>{!!$month_name[$j]!!}</option>
                                        @endif
                                    @endfor
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
					    <div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی</label>
                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                        <option value="">انتخاب</option>
                                    @foreach($parentDeps AS $dep_item)
                                    	@if($dep_item->id == $dep)
                                    		<option value='{!!$dep_item->id!!}' selected>{!!$dep_item->name!!}</option>
                                    	@else
                                        	<option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ مربوط</label>
                                <select class="form-control" name="sub_dep" id="sub_dep">
                                    <option value=''>انتخاب</option>
                                    @foreach($sub_dep AS $deps)
                                    	@if($deps->id == $dep_id)
                                    		<option value='{!!$deps->id!!}' selected>{!!$deps->name!!}</option>
                                    	@else
                                        	<option value='{!!$deps->id!!}'>{!!$deps->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                			</div>
                		</div>
					    
					</div>
				</div>
			</div>
		</div>
    
</header>

<div class="row" id="all">
	<div class="col-lg-12">
		<header class="main-box-header clearfix" style="margin-top:10px">
		    <h2>
		    @if(!canCheckImages())
		    	<a href="{!!URL::route('getAttCharts',array($dep_id,$year,$month))!!}" class="btn btn-primary pull-right">
					گراف
				</a><div style="display:inline" class="pull-right"> &nbsp; </div>
				<a href="{!!URL::route('printAttReport',array($dep_id,$year,$month))!!}" class="btn btn-primary pull-right">
					پرنت اکسل
				</a><div style="display:inline" class="pull-right"> &nbsp; </div>
		    	<a href="{!!URL::route('getPayrollList',array($dep_id,$year,$month))!!}" class="btn btn-primary pull-right">
					راپور معاشات
				</a>
				<div style="display:inline" class="pull-right"> &nbsp; </div>
		        
		    @endif
		    
		    </h2>
		    
		</header>
</form>
		<div class="modal-body" id="result_div">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table" id="list">
                    <thead>
                      	<tr>
	                        <th>#</th>
	                        <th>نام کامل</th>
	                        <th>ولد</th>
	                        
	                        <th>وظیفه</th>
	                        <th>دیپارتمنت</th>
	                        <th>حاضر</th>
	                        <th>غیرحاضر</th>
	                        <th>رخصتی</th>
	                        <th>عملیه</th>
	                    </tr>
                    </thead>
                    <tbody>
                    	
                    </tbody>
                  </table>
                  
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>

<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>
<div class="modal fade modal-fade-in-scale-up" id="salary_modal" aria-hidden="true" aria-labelledby="salary_modal" role="dialog" tabindex="-1"></div>

<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {
            	
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::route('getEmployeeAttData',array($dep,$dep_id,$year,$month))!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });
	function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function bringRelatedEmployees(id)
    {
    	var year = $('#year').val();
    	var month = $('#month').val()
    	$.ajax({
                url: '{!!URL::route("getRelatedEmployees")!!}',
                data: '&dep_id='+id+'&year='+year+'&month='+month,
                type: 'post',
                
                success: function(response)
                {
                   $('#all').html(response);
                }
            }
        );
    }
    function load_salary_wait(id)
    {
    	var year = $('#year').val();
    	var month = $('#month').val();
    	$.ajax({
                url: '{!!URL::route("getEmployeeSalaryWait")!!}',
                data: '&id='+id+'&year='+year+'&month='+month,
                type: 'post',
                
                success: function(response)
                {
                   $('#salary_modal').html(response);
                }
            }
        );
    }
    function getSearchResult(div)
    {
        $.ajax({
                url: '{!!URL::route("getAttendanceAjax")!!}',
                data: $('#att_report').serialize(),
                type: 'post',
                beforeSend: function(){

                    //$("body").show().css({"opacity": "0.5"});
                    $("#"+div).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {

                    $('#'+div).html(response);
                }
            }
        );

        return false;
        
    }
    
</script>
@stop

