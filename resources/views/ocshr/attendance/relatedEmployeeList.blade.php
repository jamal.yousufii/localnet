<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix" style="margin-top:10px">
		    <h2>
		    	<a href="{!!URL::route('getPayrollList',array($dep_id,$year,$month))!!}" class="btn btn-primary pull-right">
					راپور معاشات
				</a>
		        
		    </h2>
		</header>
		<div class="modal-body">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table">
                    <thead>
                      	<tr>
	                        <th>#</th>
	                        <th>نام کامل</th>
	                        <th>ولد</th>
	                        
	                        <th>حاضر</th>
	                        <th>غیرحاضر</th>
	                        <th>رخصتی</th>
	                        <th>عملیه</th>
	                    </tr>
                    </thead>
                    <tbody>
                    @if($employees)
                    <?php $i = 1; ?>
                   		@foreach($employees AS $row)
                   		<?php $present = getEmployeePresentDays($row->RFID,$year,$month);
                   		$leaves = getEmployeeLeaveDays($row->id,$year,$month);
                   		$holidays = countHolidays($year,$month);
                   		
                   		?>
                   		<tr>
                    	<td>{!!$i!!}</td>
                    	<td>{!!$row->name!!}</td>
                    	<td>{!!$row->father_name!!}</td>
                    	
                    	<td>{!!$present!!}</td>
                    	<td>{!!getEmployeeAbsentDays($present,$year,$month)-$holidays!!}</td>
                    	<td>{!!$leaves!!}</td>
                    	
                    	<td>
                    	@if($row->RFID!='')
                    		<a href="{!!URL::route('approveEmployeeAttendance',array($row->RFID,$year,$month))!!}">تایید حاضری</a> | 
                    		<a href="{!!URL::route('getEmployeeAttendance',array($row->RFID,$year,$month))!!}">بررسی عکسها</a> |
                    	@else
                    		<a href="javascript:void()" onclick="alert('User does not have RFID')">جزییات</a>
                    	@endif
                    	</td>
                    	</tr>
                    	<?php $i++; ?>
                    	@endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="leave_modal" aria-hidden="true" aria-labelledby="leave_modal" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 50%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" id="leaves_close_btn" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" id="leaves" role="form" method="post" >
                <div class="container-fluid">
                	<div class="row">
			      		 <div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">رخصتی با معاش</label>
                                <input class="form-control" type="text" name="paid">
                        	</div>
                        </div>
			      		<div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">رخصتی بدون معاش</label>
                                <input class="form-control" type="text" name="unpaid">
                        	</div>
                        </div>
                        
                    </div>
                </div>
                <input type="hidden" id="employee_id" name="employee_id" value="" />
	            <input type="hidden" name="leave_year" value="{!!$year!!}" />
	            <input type="hidden" name="leave_month" value="{!!$month!!}" />
				    <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_attendance'))
	                					<button class="btn btn-primary" type="button" onclick="submit_leaves({!!$dep_id!!},{!!$year!!},{!!$month!!});$('#leaves_close_btn').click()">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>			            				           
				      	</div>
				    </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="md-overlay"></div>
<script>
	function submit_leaves(dep_id,year,month)
	{
		var datas = $('#leaves').serialize();
		var page = "{!!URL::route('postLeaveDays')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: datas,
	        success: function(r){
	        	$('#leaves_close_btn').click();
				bringRelatedEmployee(dep_id,year,month);
	        }
	    });
	}
	function load_change_employee(id)
	{
		var page = "{!!URL::route('load_change_employee')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&id='+id,
	        success: function(r){
				$('#change_employee').html(r);
	        }
	    });
	}
	function bringRelatedEmployee(id,year,month)
    {
    	$.ajax({
                url: '{!!URL::route("getRelatedEmployees")!!}',
                data: '&dep_id='+id+'&year='+year+'&month='+month,
                type: 'post',
                
                success: function(response)
                {
                   $('#all').html(response);
                }
            }
        );
    }
</script>