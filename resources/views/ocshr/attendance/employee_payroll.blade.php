<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" id="leaves_close_btn" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" id="leaves" role="form" method="post" >
                <div class="container-fluid">
                	<div class="row">
			      		 <div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">رخصتی با معاش</label>
                                <input class="form-control" type="text" name="paid">
                        	</div>
                        </div>
			      		<div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">رخصتی بدون معاش</label>
                                <input class="form-control" type="text" name="unpaid">
                        	</div>
                        </div>
                        
                    </div>
                </div>
                <input type="hidden" id="employee_id" name="employee_id" value="" />
	            <input type="hidden" name="leave_year" value="{!!$year!!}" />
	            <input type="hidden" name="leave_month" value="{!!$month!!}" />
				    <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_attendance'))
	                					<button class="btn btn-primary" type="button" onclick="submit_leaves({!!$dep_id!!},{!!$year!!},{!!$month!!});$('#leaves_close_btn').click()">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>			            				           
				      	</div>
				    </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>