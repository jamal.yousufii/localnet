@extends('layouts.master')

@section('head')
	{!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
	<style>
	.clockpicker-popover{
		z-index:100000;
	}
	</style>
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
	
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>حاضری کارمندان</span></li>
        </ol>
        
    </div>
</div>
<header class="main-box-header clearfix">
    	<div class="row">
        	<div class="col-lg-12">
        	<form class="form-horizontal" role="form" method="post" action="{!!URL::route('generate_att_chart')!!}">
        		<div class="container-fluid">
			      	<div class="row">
			      		<div class="col-sm-2">
		                	<div class="col-sm-12">
		                		<label class="col-sm-12 ">از تاریخ</label>
		                		<input class="form-control datepicker_farsi" readonly type="text" name="from_date" value="{!!old('from_date')!!}">
		                	</div>
		                </div>
		                <div class="col-sm-2">
		                	<div class="col-sm-12">
		                		<label class="col-sm-12 ">تا تاریخ</label>
		                		<input class="form-control datepicker_farsi" readonly value="{!!old('to_date')!!}" type="text" name="to_date">
		                	</div>
		                </div>
					    <div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی</label>
                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                        <option value="">همه</option>
                                    @foreach($parentDeps AS $dep_item)
                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12">ادارۀ مربوط</label>
                			</div>
                			<div class="col-sm-12">
                                <select style="width:100%;" name="sub_dep" id="sub_dep">
                                    <option value=''>همه</option>
                                </select>
                			</div>
                		</div>
                		<div class="col-sm-2">
	                		<div class="col-sm-12">
	                		
		                        <div class="radio-custom radio-primary">
				                  <input type="radio" value="0" name="type" checked />
				                  <label for="inputRadiosUnchecked">{!!_('percentage')!!}</label>
				                </div>
				                <div class="radio-custom radio-primary">
				                  <input type="radio" value="1" name="type" />
				                  <label for="inputRadiosChecked">{!!_('daily')!!}</label>
				                </div>
		                    </div>
                    	</div>
					    <div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12">&nbsp;</label>
                			</div>
                			<div class="col-sm-12">
                                <input type="submit" class="btn btn-primary" value="Get Chart"/>
                			</div>
                		</div>
					</div>
				</div>
				<div class="container-fluid">
			      	<div class="row">
			      		<div class="col-sm-12">
			      			@if (count($errors) > 0)
						    
						    <div class="alert alert-danger" style="margin: 10px 0 20px 0">
						      <ul>
						        @foreach ($errors->all() as $error)
						          <li>{{ $error }}</li>
						        @endforeach
						      </ul>
						    </div>
						    @endif
			      		</div>
			      	</div>
			    </div>
			</form>
			</div>
		</div>
    
</header>
@stop
@section('footer-scripts')
<script type="text/javascript">
    $("#sub_dep").select2();
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
</script>
@stop

