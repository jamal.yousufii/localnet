<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix" style="margin-top:10px">
		    
		    <input type="text" id="emp_search" placeholder="جستجو">
		</header>
		</form>
		<div class="modal-body" id="result_div">
			<div class="example-wrap">
		        <div class="example table-responsive">
		            <table class="table">
		                <thead>
		                    <tr>
			                    <th>#</th>
			                    <th>نام کامل</th>
			                    <th>ولد</th>
			                    <th>وظیفه</th>
			                    <th>دیپارتمنت</th>
			                    <th>حاضر</th>
			                    <th>غیرحاضر</th>
			                    <th>رخصتی</th>
			                    <th>عملیه</th>
			                </tr>
		                </thead>
		                <tbody>
		                    @foreach($rows AS $row)
		                    <?php $presents = getEmployeePresentDays($row->RFID,$year,$month)+getPresentedDays($row->RFID,$year,$month);
		                    if($row->changed==1){$status = 'تبدیلی';}
		                    elseif($row->fired==1){$status = 'منفک';}
		                    elseif($row->resigned==1){$status = 'استعفا';}
		                    elseif($row->retired==1){$status = 'تقاعد';}
		                    elseif($row->position_dr==4){$status = 'انتظاربه معاش';}
		                    else{$status='بدون کارت';}
		                    ?>
		                    	<tr>
		                    		<td>{!!$row->id!!}</td>
		                    		<td>{!!$row->name!!} {!!$row->last_name!!}</td>
		                    		<td>{!!$row->father_name!!}</td>
		                    		<td>{!!$row->position!!}</td>
		                    		<td>{!!$row->department!!}</td>
		                    		<td>
		                    		{!!$presents!!}
		                    		</td>
		                    		<td>
		                    		{!!getEmployeeAbsentDays($presents,$year,$month) - getEmployeeLeaveDays($row->id,$year,$month);!!}
		                    		</td>
		                    		<td>
		                    		{!!getEmployeeLeaveDays($row->id,$year,$month);!!}
		                    		</td>
		                    		<td>
		                    		@if($row->RFID>0)
		                    			@if($row->tashkil_id>0)
		                    			<a href="{!!route('getDirEmployeeImages',array(Crypt::encrypt($row->RFID),$year,$month,Crypt::encrypt($row->general_department),Crypt::encrypt($row->dep_id)))!!}" class="table-link" style="text-decoration:none;">
											بررسی عکسها
										</a>
										@else
										<a href="javascript:void()" class="table-link" style="text-decoration:none;color:red">
											اضافه بست
										</a>
										@endif
		                    		@elseif($row->in_attendance!=0)
	                    				<a href="{!!route('getDirEmployeeImages',array($row->RFID,$year,$month,$row->general_department,$row->dep_id))!!}" class="table-link" style="text-decoration:none;">
											بررسی عکسها
										</a> | 
										<a href="javascript:void()" class="table-link" style="text-decoration:none;color:red">
											{!!$status!!}
										</a>
									@else
										<a href="javascript:void()" class="table-link" style="text-decoration:none;">
											شامل حاضری نمیباشد
										</a>
		                    		@endif
		                    		</td>
		                    	<tr>
		                    	
		                    @endforeach
		                </tbody>
		            </table>
		            <div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
						{!!$rows->render()!!}
					</div>
		        </div>
		    </div>
		</div>
	</div>
</div>

<script>
	$( document ).ready(function() {
		$('.pagination a').on('click', function(event) {
			event.preventDefault();
			if ($(this).attr('href') != '#') {
				//$('#ajaxContent').load($(this).attr('href'));
				
				$.ajax({
		                url: '{!!URL::route("getDirAttendance")!!}',
		                data: $('#att_report').serialize()+"&page="+$(this).text()+"&ajax=1",
		                type: 'post',
		                beforeSend: function(){
		
		                    //$("body").show().css({"opacity": "0.5"});
		                    $('#all').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
		                },
		                success: function(response)
		                {
		
		                    $('#all').html(response);
		                }
		            }
		        );
			
			}
		});
	});
	
    $('#emp_search').keypress(function(event){
	
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
		
	    	var item = $('#emp_search').val();
	    	$.ajax({
	                url: '{!!URL::route("getDirSearchResult")!!}',
	                data: $('#att_report').serialize()+'&item='+item,
	                type: 'post',
	                beforeSend: function(){
	
	                    //$("body").show().css({"opacity": "0.5"});
	                    $("#result_div").html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
	
	                    $('#result_div').html(response);
	                }
	            }
	        );
		}
		event.stopPropagation();
	});
    
</script>

