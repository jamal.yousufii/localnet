@extends('layouts.master')

@section('head')
	{!! HTML::style('/vendor/clockpicker/clockpicker.min.css') !!}
	<style>
	.clockpicker-popover{
		z-index:100000;
	}
	</style>
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	<div class="col-lg-12">
		<form class="form-horizontal" role="form" id="attendanceTime" method="post">
		    <div class="panel-heading">
		      <h5 class="panel-title">علاوه نمودن عکسها</h5>
		    </div>
		    <div class="container-fluid" id="year_time_div">
		    	<div class="row">
		    		<div class="col-sm-6">
		            	<div class="col-sm-12">
			                <label class="col-sm-12 ">تاریخ</label>
                            <input class="form-control datepicker_farsi" onchange="check_date()" readonly type="text" id="the_date" name="the_date" value="{!!Input::old('the_date')!!}">
		              	</div>
		              <!-- End Example Auto Colse -->
		            </div>
		            <div class="col-sm-6" id="loading">
		            </div>
		    	</div>
		    </div>
		    <div class="container-fluid" id="button_div">
		    	<div class="row">
		    		<div class="col-sm-3">
		    			<div class="col-sm-12">
		    				<label class="col-sm-12 ">&nbsp;</label>
		    				<button class="btn btn-primary" type="button" onclick="insertImages()"><i class="fa fa-refresh fa-lg"></i> ذخیره</button>
		    			</div>
		    		</div>
		    	</div>
		    </div>    
			{!! Form::token() !!}
		</form>
	</div>
</div>
@stop
@section('footer-scripts')
{!! HTML::script('/vendor/clockpicker/bootstrap-clockpicker.min.js') !!}
{!! HTML::script('/js/components/clockpicker.min.js') !!}

<script type="text/javascript">
    
function check_date()
{
	var id = $('#the_date').val();
	
    $.ajax({
            url: '{!!URL::route("checkInsertImagesDate")!!}',
            data: '&date='+id,
            type: 'post',
            beforeSend: function(){
                $("#loading").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
            	if(response!='')
            	{
            		$('#button_div').hide();
            	}
            	else
            	{
            		$('#button_div').show();
            	}
            	$('#loading').html(response);
            }
        }
    );
}
function insertImages()
{
	var id = $('#the_date').val();
	
    $.ajax({
            url: '{!!URL::route("insertAttendanceImages")!!}',
            data: '&the_date='+id,
            type: 'post',
            beforeSend: function(){
                $("#loading").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
            	$('#loading').html(response);
            	sendEmails(id);
            }
        }
    );
}
function sendEmails(id)
{
	var page = "{!!URL::route('sendAfternoon_emails')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&the_date='+id,
        success: function(r){
        
        }
    });
}
</script>
@stop