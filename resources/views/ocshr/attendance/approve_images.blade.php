@extends('layouts.master')

@section('head')
    <title>{!!_('recruitment')!!}</title>
   <style type="text/css">
    .att_img:hover {
    opacity: 0.5; filter: alpha(opacity=30);
} 
    </style>
@stop
@section('content')

<header class="main-box-header clearfix">
	<div class="row">
    	<div class="col-lg-12">
    		<div class="container-fluid">
		      	<div class="row">
		      		
            		<div class="col-sm-4" id="img_controller">
            			<div class="col-sm-12">
            				<div class="icon-box pull-left">
			           		@if($previous_rfid)
			           		<?php $link = URL::route('approveNextEmployee',array($rfid,$year,$month,'previous')); ?>
							<a href="javascript:void()" onclick="$('#img_controller').html('Loading...');location.href='{!!$link!!}'" class="btn pull-left">
								<i class="icon ml-chevron_left"></i>
							</a>
							@else
							<i class="btn pull-left icon ml-chevron_left" style="cursor:default"></i>
							@endif
							<img src="/documents/profile_pictures/{!!$photo->photo!!}" alt="profile image" width="150" hight="224"/>
							@if($next_rfid)
							<?php $next_link = URL::route('approveNextEmployee',array($rfid,$year,$month,'next')); ?>
							<a href="javascript:void()" onclick="$('#img_controller').html('Loading...');location.href='{!!$next_link!!}'" class="btn pull-right">
								<i class="icon ml-chevron_right"></i>
							</a>
							@else
							<i class="btn pull-right icon ml-chevron_right" style="cursor:default"></i>
							@endif
						</div>
            			</div>
            		</div>
		      		<div class="col-sm-2">
            			<div class="col-sm-12">
            				<label class="col-sm-12 ">سال</label>
                            <select name="general_department" id="year" class="form-control" readonly>
                                    <?php $curr_year = date('Y')-621;?>
                                @for($i=$curr_year-5;$i<$curr_year+5;$i++)
                                	@if($i==$year)
                                    <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
                                    @else
                                    <option value='{!!$i!!}'>{!!$i!!}</option>
                                    @endif
                                @endfor
                            </select>
                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
            			</div>
            		</div>
            		<div class="col-sm-2">
            			<div class="col-sm-12">
            				<label class="col-sm-12 ">ماه</label>
            				<?php 
            					
            					$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');
            					?>
                            <select name="general_department" id="month" class="form-control" readonly>
                                   
                                @for($j=1;$j<13;$j++)
                                	@if($j==$month)
                                    <option value='{!!$j!!}' selected="selected">{!!$month_name[$j]!!}</option>
                                    @else
                                    <option value='{!!$j!!}'>{!!$month_name[$j]!!}</option>
                                    @endif
                                @endfor
                            </select>
                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
            			</div>
            		</div>
				    
				    
				</div>
			</div>
		</div>
	</div>
</header>

<!-- Example Tabs -->
<div class="row">
	<div class="col-lg-12">
	<table width="100%">
	<?php $day_counter=1; ?>
	@if($details)
		@foreach($details as $img)
			<?php 
			$path = explode("_",$img->path);
			$time = (int)substr($path[0],-6);//get the time part in path
			$month_day = substr($path[0],-11,4);//get the monthDay part in path
			
			$signin_img = "http://10.134.45.19/".$img->path;
			$imgin_id = $img->id;
			$rejected_in = $img->status;
			$signin_time = $time;

			?>
			@if($day_counter == 1 || $day_counter%10==1)
			<tr>
			@endif
			
			<td style="position: relative;padding-bottom: 2em;">
				<div style="position: relative;">
					<img src="{!!$signin_img!!}" class="att_img" onclick="reject_pic({!!$imgin_id!!})" alt="user image" height="112" width="90" style="cursor:pointer;border-top:1px solid;border-left:1px solid;border-right:1px solid"/>
					<span style="position: absolute;left: 10px;top: 50px;color:red; font-size:15px;" id="{!!$imgin_id!!}_span">@if($rejected_in!=0) Rejected @endif</span>
				</div>
			</td>
			
			@if($day_counter%10==0)
			</tr>
			@endif
		<?php $day_counter++;?>
		@endforeach
	@endif
		
		
		
		
	
	</table>
	</div>
</div>
<!-- End Example Tabs -->

@stop
@section('footer-scripts')
<script>
function reject_pic(img_id)
{
	var page = "{!!URL::route('rejectImage')!!}";
	var span = $('#'+img_id+'_span').html();
	$.ajax({
        url:page,
        type:'post',
        data: '&img_id='+img_id+'&type='+span,
        success: function(r){
			
			if(span == '')
			{
				$('#'+img_id+'_span').html('Rejected');
			}
			else
			{
				$('#'+img_id+'_span').html('');
			}
        }
    });
	
}
</script>
@stop


