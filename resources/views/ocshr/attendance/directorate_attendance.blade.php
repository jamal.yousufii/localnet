@extends('layouts.master')

@section('head')
	{!! HTML::style('/vendor/clockpicker/clockpicker.min.css') !!}
	{!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
	<style>
	.clockpicker-popover{
		z-index:100000;
	}
	</style>
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
	<form class="form-horizontal" role="form" id="att_report" method="post">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>حاضری کارمندان</span></li>
        </ol>
        
        <button class="btn btn-primary pull-right" type="button" onclick="getSearchResult('all')"> راپور حاضری</button>
    </div>
</div>
<header class="main-box-header clearfix">
   
    	<div class="row">
        	<div class="col-lg-12">
        	
        		<div class="container-fluid">
			      	<div class="row">
			      		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">سال</label>
                                <select name="year" id="year" class="form-control">
                                    <?php
                                    //default selected values for year and month
                                    $today = dateToShamsi(date('Y'),date('m'),date('d'));
                					$month = explode('-',$today);
                					$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');
                					$year = $month[0];
                					?>
                                    @for($i=$year-5;$i<$year+5;$i++)
                                    	@if($i==$year)
                                        <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
                                        @else
                                        <option value='{!!$i!!}'>{!!$i!!}</option>
                                        @endif
                                    @endfor
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ماه</label>
                				
                                <select name="month" id="month" class="form-control">
                                       
                                    @for($j=1;$j<13;$j++)
                                    	@if($j==$month[1])
                                        <option value='{!!$j!!}' selected="selected">{!!$month_name[$j]!!}</option>
                                        @else
                                        <option value='{!!$j!!}'>{!!$month_name[$j]!!}</option>
                                        @endif
                                    @endfor
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
					    
                		@if($details->position_id==2)
                		<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی</label>
                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                        
                                    @foreach($parentDeps AS $dep_item)
                                    	@if($dep_item->id == $details->gen_dep)
                                        <option value='{!!$dep_item->id!!}' selected>{!!$dep_item->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 control-label">ادارۀ مربوط</label>
                			</div>
                			<div class="col-sm-12">
                                <select style="width:100%;" name="sub_dep" id="sub_dep">
                                    
                                    @foreach($sub_dep AS $sub)
                                    	@if($sub->id == $details->sub_dep)
                                        <option value='{!!$sub->id!!}' selected>{!!$sub->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                			</div>
                		</div>
					    @else
					    <div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی</label>
                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                        
                                    @foreach($parentDeps AS $dep_item)
                                    	@if($dep_item->id == $details->sub_dep)
                                        <option value='{!!$dep_item->id!!}' selected>{!!$dep_item->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
					    <div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 control-label">ادارۀ مربوط</label>
                			</div>
                			<div class="col-sm-12">
                                <select style="width:100%;" name="sub_dep" id="sub_dep">
                                    <option value="0" selected>{!!_('all')!!}</option>
                                    @foreach($sub_dep AS $sub)
                                    	
                                        <option value='{!!$sub->id!!}'>{!!$sub->name!!}</option>
                                        
                                    @endforeach
                                </select>
                			</div>
                		</div>
                		@endif
					</div>
				</div>
				
			</form>
			</div>
		</div>
    
</header>

<!-- Example Tabs -->
<div id="all">
</div>



@stop
@section('footer-scripts')
{!! HTML::script('/vendor/clockpicker/bootstrap-clockpicker.min.js') !!}
{!! HTML::script('/js/components/clockpicker.min.js') !!}
<script type="text/javascript">
    $("#sub_dep").select2();
	function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function bringRelatedEmployees(id)
    {
    	var year = $('#year').val();
    	var month = $('#month').val();
    	$.ajax({
                url: '{!!URL::route("getRelatedEmployees")!!}',
                data: '&dep_id='+id+'&year='+year+'&month='+month,
                type: 'post',
                
                success: function(response)
                {
                   $('#all').html(response);
                }
            }
        );
    }
    
	function getSearchResult(div)
    {
        $.ajax({
                url: '{!!URL::route("getDirAttendance")!!}',
                data: $('#att_report').serialize(),
                type: 'post',
                beforeSend: function(){

                    //$("body").show().css({"opacity": "0.5"});
                    $("#"+div).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {

                    $('#'+div).html(response);
                }
            }
        );

        return false;
        
    }
</script>
@stop

