<div class="example-wrap">
    <div class="example table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>نام کامل</th>
                    <th>ولد</th>
                    <th>وظیفه</th>
                    <th>دیپارتمنت</th>
                    <th>حاضر</th>
                    <th>غیرحاضر</th>
                    <th>رخصتی</th>
                    <th>عملیه</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rows AS $row)
                <?php $presents = getEmployeePresentDays($row->RFID,$year,$month)+getPresentedDays($row->RFID,$year,$month);
                if($row->changed==1){$status = 'تبدیلی';}
                elseif($row->fired==1){$status = 'منفک';}
                elseif($row->resigned==1){$status = 'استعفا';}
                elseif($row->retired==1){$status = 'تقاعد';}
                elseif($row->position_dr==4){$status = 'انتظاربه معاش';}
                else{$status='بدون کارت';}
                ?>
                	<tr>
                		<td>{!!$row->id!!}</td>
                		<td>{!!$row->name!!} {!!$row->last_name!!}</td>
                		<td>{!!$row->father_name!!}</td>
                		<td>{!!$row->position!!}</td>
                		<td>{!!$row->department!!}</td>
                		<td>
                		{!!$presents!!}
                		</td>
                		<td>
                		{!!getEmployeeAbsentDays($presents,$year,$month) - getEmployeeLeaveDays($row->id,$year,$month);!!}
                		</td>
                		<td>
                		{!!getEmployeeLeaveDays($row->id,$year,$month);!!}
                		</td>
                		<td>
                		@if($row->RFID>0)
                			@if(canCheckImages())
                				@if($row->tashkil_id>0)
                				<a href="{!!route('getEmployeeAttendanceCheck',array($row->RFID,$year,$month,$row->general_department,$row->dep_id))!!}" class="table-link" style="text-decoration:none;">
									بررسی عکسها
								</a>
								@else
								<a href="javascript:void()" class="table-link" style="text-decoration:none;color:red">
									اضافه بست
								</a>
								@endif
                			@else
                				@if($row->tashkil_id>0)
                				<a href="javascript:void()" onclick="load_salary_wait('.$row->id.');" class="table-link" style="text-decoration:none;" data-target="#salary_modal" data-toggle="modal">
									معطل به معاش
								</a> |
								
								@else
								<a href="javascript:void()" class="table-link" style="text-decoration:none;color:red">
									اضافه بست
								</a> | 
								@endif
								<a href="{!!route('getEmployeeAttendance',array($row->RFID,$year,$month,$row->general_department,$row->dep_id))!!}" class="table-link" style="text-decoration:none;">
									بررسی عکسها
								</a>
                			@endif
                		@elseif($row->in_attendance!=0)
                			@if(canCheckImages())
                				<a href="{!!route('getEmployeeAttendanceCheck',array($row->RFID,$year,$month,$row->general_department,$row->dep_id))!!}" class="table-link" style="text-decoration:none;">
									بررسی عکسها
								</a> | 
								
                			@else
								<a href="{!!route('getEmployeeAttendance',array(0,$year,$month,$row->general_department,$row->dep_id,$row->in_attendance))!!}" target="_blank" class="table-link" style="text-decoration:none;">
									بررسی عکسها
								</a> | 
								
							@endif
							<a href="javascript:void()" class="table-link" style="text-decoration:none;color:red">
								{!!$status!!}
							</a>
						@else
							<a href="javascript:void()" class="table-link" style="text-decoration:none;">
								شامل حاضری نمیباشد
							</a>
                		@endif
                		</td>
                	<tr>
                	
                @endforeach
            </tbody>
        </table>
        
    </div>
</div>