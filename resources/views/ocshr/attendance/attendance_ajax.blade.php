<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix" style="margin-top:10px">
		    <h2>
		    @if(!canCheckImages())
		    	<a href="{!!URL::route('getAttCharts',array($dep_id,$year,$month))!!}" class="btn btn-primary pull-right">
					گراف
				</a><div style="display:inline" class="pull-right"> &nbsp; </div>
				<a href="{!!URL::route('printAttReport',array($dep_id,$year,$month))!!}" class="btn btn-primary pull-right">
					پرنت اکسل
				</a><div style="display:inline" class="pull-right"> &nbsp; </div>
		    	<a href="{!!URL::route('getPayrollList',array($dep_id,$year,$month))!!}" class="btn btn-primary pull-right">
					راپور معاشات
				</a>
				<div style="display:inline" class="pull-right"> &nbsp; </div>
		        
		    @endif
		    </h2>
		    <input type="text" id="emp_search" placeholder="جستجو">
		</header>
		</form>
		<div class="modal-body" id="result_div">
			<div class="example-wrap">
		        <div class="example table-responsive">
		            <table class="table">
		                <thead>
		                    <tr>
			                    <th>#</th>
			                    <th>نام کامل</th>
			                    <th>ولد</th>
			                    <th>وظیفه</th>
			                    <th>دیپارتمنت</th>
			                    <th>حاضر</th>
			                    <th>غیرحاضر</th>
			                    <th>رخصتی</th>
			                    <th>عملیه</th>
			                </tr>
		                </thead>
		                <tbody>
		                    @foreach($rows AS $row)
		                    <?php $presents = getEmployeePresentDays($row->RFID,$year,$month)+getPresentedDays($row->RFID,$year,$month);
		                    if($row->changed==1){$status = 'تبدیلی';}
		                    elseif($row->fired==1){$status = 'منفک';}
		                    elseif($row->resigned==1){$status = 'استعفا';}
		                    elseif($row->retired==1){$status = 'تقاعد';}
		                    elseif($row->position_dr==4){$status = 'انتظاربه معاش';}
		                    else{$status='بدون کارت';}
		                    ?>
		                    	<tr>
		                    		<td>{!!$row->id!!}</td>
		                    		<td>{!!$row->name!!} {!!$row->last_name!!}</td>
		                    		<td>{!!$row->father_name!!}</td>
		                    		<td>{!!$row->position!!}</td>
		                    		<td>{!!$row->department!!}</td>
		                    		<td>
		                    		{!!$presents!!}
		                    		</td>
		                    		<td>
		                    		{!!getEmployeeAbsentDays($presents,$year,$month) - getEmployeeLeaveDays($row->id,$year,$month);!!}
		                    		</td>
		                    		<td>
		                    		{!!getEmployeeLeaveDays($row->id,$year,$month);!!}
		                    		</td>
		                    		<td>
		                    		@if($row->RFID>0)
		                    			@if(canCheckImages())
		                    				@if($row->tashkil_id>0)
		                    				<a href="{!!route('getEmployeeAttendanceCheck',array($row->RFID,$year,$month,$row->general_department,$row->dep_id))!!}" class="table-link" style="text-decoration:none;">
												بررسی عکسها
											</a>
											@else
											<a href="javascript:void()" class="table-link" style="text-decoration:none;color:red">
												اضافه بست
											</a>
											@endif
		                    			@else
		                    				@if($row->tashkil_id>0)
		                    				<a href="javascript:void()" onclick="load_salary_wait('.$row->id.');" class="table-link" style="text-decoration:none;" data-target="#salary_modal" data-toggle="modal">
												معطل به معاش
											</a> |
											
											@else
											<a href="javascript:void()" class="table-link" style="text-decoration:none;color:red">
												اضافه بست
											</a> | 
											@endif
											<a href="{!!route('getEmployeeAttendance',array($row->RFID,$year,$month,$row->general_department,$row->dep_id))!!}" class="table-link" style="text-decoration:none;">
												بررسی عکسها
											</a>
		                    			@endif
		                    		@elseif($row->in_attendance!=0)
		                    			@if(canCheckImages())
		                    				<a href="{!!route('getEmployeeAttendanceCheck',array($row->RFID,$year,$month,$row->general_department,$row->dep_id))!!}" class="table-link" style="text-decoration:none;">
												بررسی عکسها
											</a> | 
											
		                    			@else
											<a href="{!!route('getEmployeeAttendance',array(0,$year,$month,$row->general_department,$row->dep_id,$row->in_attendance))!!}" target="_blank" class="table-link" style="text-decoration:none;">
												بررسی عکسها
											</a> | 
											
										@endif
										<a href="javascript:void()" class="table-link" style="text-decoration:none;color:red">
											{!!$status!!}
										</a>
									@else
										<a href="javascript:void()" class="table-link" style="text-decoration:none;">
											شامل حاضری نمیباشد
										</a>
		                    		@endif
		                    		</td>
		                    	<tr>
		                    	
		                    @endforeach
		                </tbody>
		            </table>
		            <div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
						{!!$rows->render()!!}
					</div>
		        </div>
		    </div>
		</div>
	</div>
</div>

<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>
<div class="modal fade modal-fade-in-scale-up" id="salary_modal" aria-hidden="true" aria-labelledby="salary_modal" role="dialog" tabindex="-1"></div>

<div class="md-overlay"></div><!-- the overlay element -->

<script>
	$( document ).ready(function() {
		$('.pagination a').on('click', function(event) {
			event.preventDefault();
			if ($(this).attr('href') != '#') {
				//$('#ajaxContent').load($(this).attr('href'));
				
				$.ajax({
		                url: '{!!URL::route("getAttendanceAjax")!!}',
		                data: $('#att_report').serialize()+"&page="+$(this).text()+"&ajax=1",
		                type: 'post',
		                beforeSend: function(){
		
		                    //$("body").show().css({"opacity": "0.5"});
		                    $('#all').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
		                },
		                success: function(response)
		                {
		
		                    $('#all').html(response);
		                }
		            }
		        );
			
			}
		});
	});
	function load_salary_wait(id)
    {
    	var year = $('#year').val();
    	var month = $('#month').val();
    	$.ajax({
                url: '{!!URL::route("getEmployeeSalaryWait")!!}',
                data: '&id='+id+'&year='+year+'&month='+month,
                type: 'post',
                
                success: function(response)
                {
                   $('#salary_modal').html(response);
                }
            }
        );
    }
    $('#emp_search').keypress(function(event){
	
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
		
	    	var item = $('#emp_search').val();
	    	$.ajax({
	                url: '{!!URL::route("getSearchResult")!!}',
	                data: $('#att_report').serialize()+'&item='+item,
	                type: 'post',
	                beforeSend: function(){
	
	                    //$("body").show().css({"opacity": "0.5"});
	                    $("#result_div").html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
	
	                    $('#result_div').html(response);
	                }
	            }
	        );
		}
		event.stopPropagation();
	});
    
</script>

