@extends('layouts.master')

@section('head')
	<title>Dashboard</title>
	
@stop

@section('content')

<div class="row">
	<div id="chart3" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
</div>
<?php
$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');

$present = getDailyAtt(1,$dep_id);
$absent = getDailyAbsent(1,$dep_id);
$date = explode('-',$date);
$year = $date[0];
$month = $month_name[$date[1]];
$day = $date[2];
 
?>
@stop
@section('footer-scripts')

{!! HTML::script('/js/highcharts/highcharts.js') !!}
{!! HTML::script('/js/highcharts/highcharts-3d.js') !!}
{!! HTML::script('/js/highcharts/exporting.js') !!}


<script type="text/javascript">

	$(function () {
    $('#chart3').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: '{!!$dep->name!!}({!!$day!!}-{!!$month!!}-{!!$year!!})'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.y}</b>',
                    style: {color: "#D2691E"}
                },
                showInLegend: true
            }
        },
        series: [{
            type: 'pie',
            name: 'Percentage',
            data: [
                ['حاضر', <?=$present?>],
                {
                	sliced: true,
                	selected: true,
                	name: "غیرحاضر",
                	y: <?=$absent?>
                }
            ]
        }]
    });
});
</script>
@stop




