<script>
$(document).ready(function() {
$(".datepicker_farsi").persianDatepicker();
});
</script>
<div class="col-lg-12">
    <form class="form-horizontal" role="form" method="post" action="{!!URL::route('updateLeaves')!!}" enctype="multipart/form-data">
        <div class="container-fluid">
        	<div class="row">
	      		 <div class="col-sm-4">
                	<div class="col-sm-12">
                		<label class="col-sm-12 ">نوع رخصتی</label>
                        <select class="form-control" name="type" id="edit_type" onchange="check_leave_validity1()">
                            <option value=''>انتخاب</option>
                            <option value='1' @if($details->type==1){!!'selected'!!} @endif>ضروری</option>
                            <option value='2' @if($details->type==2){!!'selected'!!} @endif>تفریحی</option>
                            <option value='3' @if($details->type==3){!!'selected'!!} @endif>مریضی</option>
                            <option value='8' @if($details->type==8){!!'selected'!!} @endif>اضافه رخصتی مریضی</option>
                            
                            <option value='7' @if($details->type==7){!!'selected'!!} @endif>دیگر(خدمتی)</option>
                            <option value='9' @if($details->type==9){!!'selected'!!} @endif>دیگر موارد</option>
                        </select>
                	</div>
                </div>
	      		<div class="col-sm-4">
                	<div class="col-sm-12">
                		<label class="col-sm-12 ">از تاریخ</label>
                		<?php if($details->date_from != null){$sdate = explode('-',$details->date_from);} ?>
                        <input class="form-control datepicker_farsi" readonly value="@if($details->date_from!=null){!!jalali_format(dateToShamsi($sdate[0],$sdate[1],$sdate[2]))!!}@endif" type="text" name="from_date" id="edit_from_date" required>
                	</div>
                </div>
                <div class="col-sm-4">
                	<div class="col-sm-12">
                		<label class="col-sm-12 ">تا تاریخ</label>
                		<?php if($details->date_to != null){$edate = explode('-',$details->date_to);} ?>
                        <input class="form-control datepicker_farsi" readonly value="@if($details->date_to!=null){!!jalali_format(dateToShamsi($edate[0],$edate[1],$edate[2]))!!}@endif" type="text" name="to_date" id="edit_to_date" required onchange="check_leave_validity_date1()">
                	</div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
        	<div class="row">
        		<div class="col-sm-4">
            		<div class="col-sm-12">
                		<label class="col-sm-12 ">فورم</label>
                		
                        <input type='file'  name='scan' class="form-control">
                	</div>
                </div>
                <div class="col-sm-4">
            		<div class="col-sm-12">
                		<label class="col-sm-12 ">تفصیلات</label>
                		<textarea class="form-control" row="2" col="10" name="desc">{!!$details->desc!!}</textarea>
                	</div>
                </div>
                @if($details->file_name !=null)
                <div class="col-sm-4">
            		<div class="col-sm-12" id="doc_{!!$details->id!!}">
                		<label class="col-sm-12 ">فورم ضمیمه شده</label>
                		<a href="{!!URL::route('getLeaveDoc',$details->id)!!}">{!!$details->file_name!!}</a>
                		@if(canDelete('hr_attendance'))
                		<a href="javascript:void()" onclick="removeLeaveFile('{!!$details->id!!}');" class="table-link danger">
                            <i class="fa fa-trash-o" style='color:red;'></i>
                        </a>
                        @endif
                		<input type="hidden" name="filename" value="{!!$details->file_name!!}" />
                	</div>
                </div>
                @endif
        	</div>
        </div>
        <input type="hidden" name="id" value="{!!$id!!}" />
        <input type="hidden" name="employee_id" id="employee_id" value="{!!$details->employee_id!!}" />
        <input type="hidden" name="year" id="year" value="{!!$year!!}" />
        <input type="hidden" name="month" value="{!!$month!!}" />
	    <div class="container-fluid">
        	<div class="row">
        		<div class="col-sm-12">
        			<div class="col-sm-6" id="btn_not_valid_edit">
        				
        			</div>
        			<div class="col-sm-6">
        				<label class="col-sm-12">&nbsp;</label>
        				<div id="btn_div_edit" style="display:inline">
        				@if(canAttHoliday())
        					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
        				@else
        					<p>You dont have permission</p>
        				@endif
        				</div>
        				<button class="btn btn-danger" type="button" onclick="$('#main_content').slideDown();$('#edit').slideUp()">{!!_('cancel')!!}</button>
        			</div>
        			
        		</div>			            				           
	      	</div>
	    </div>
		{!! Form::token() !!}
	</form>
</div>
<script type="text/javascript">

function removeLeaveFile(doc_id)
{
    var confirmed = confirm("Do you want to remove this file?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeLeaveFile")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                beforeSend: function(){
                    $("#doc_"+doc_id).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#doc_'+doc_id).html(response);
                }
            }
        );
    }
}
	
	
}
</script>