@extends('layouts.master')

@section('head')
	{!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
	<div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::route('hrHomePage')!!}">لوحه معلومات</a></li>
            <li><a href="{!!URL::route('getAttendance')!!}">حاضری کارمندان</a></li>
            <li class="active"><span>رخصتی های عمومی</span></li>
        </ol>
    </div>
</div>

</script>
<header class="main-box-header clearfix">
	<div class="row">
		<div class="col-lg-12">
		    <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postHolidays')!!}">
			    <div class="panel-heading">
			      <h5 class="panel-title">رخصتی های عمومی</h5>
			    </div>
		        <div class="container-fluid">
		        	<div class="row">
		        		<div class="col-sm-4">
		        			<div class="col-sm-12">
		        				<label class="col-sm-12 ">سال</label>
		                        <select name="leave_year" id="leave_year" class="form-control">
		                            <?php
		                            //default selected values for year and month
		                            $today = dateToShamsi(date('Y'),date('m'),date('d'));
		        					$month = explode('-',$today);
		        					$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');
		        					$year = $month[0];
		        					?>
		                            @for($i=$year-5;$i<$year+5;$i++)
		                            	@if($i==$year)
		                                <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
		                                @else
		                                <option value='{!!$i!!}'>{!!$i!!}</option>
		                                @endif
		                            @endfor
		                        </select>
		                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		        			</div>
		        		</div>
		        		<div class="col-sm-4">
		        			<div class="col-sm-12">
		        				<label class="col-sm-12 ">ماه</label>
		        				
		                        <select name="leave_month" id="leave_month" class="form-control" onchange="bringHolidays()" required>
		                        	<option value="">انتخاب</option>  
		                            @for($j=1;$j<13;$j++)
		                                <option value='{!!$j!!}'>{!!$month_name[$j]!!}</option>
		                            @endfor
		                        </select>
		                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		        			</div>
		        		</div>
		        		<div class="col-sm-4">
		                	<div class="col-sm-12">
		                		<label class="col-sm-12 ">&nbsp;</label>
		                        <input type="button" class="btn btn-success" onclick="show_holiday_dates()" value="علاوه نمودن"/>
		                		<input type="hidden" name="holiday_number" id="holiday_number" value="0"/>
		                		<input type="hidden" name="h_number" id="h_number" value="0"/>
		                	</div>
		                </div>
		                
		        	</div>
		        </div>
		        <div id="edit"></div>
		        
		        <div class="container-fluid">
		        	<div class="row">
		        		<div class="col-sm-1">
		        			<div class="col-sm-12">
		        				<label class="col-sm-12 ">&nbsp;</label>
		        				@if(canAdd('hr_attendance'))
		        					<button class="btn btn-primary" type="submit"><i class="fa fa-refresh fa-lg"></i> ذخیره</button>
		        				@else
		        					<p>You dont have permission</p>
		        				@endif
		        			</div>
		        		</div>
		        	</div>
		        </div>
				{!! Form::token() !!}
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
		    <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postEmergencyHolidays')!!}">
			    <div class="panel-heading">
			      <h5 class="panel-title">رخصتی های اضطراری</h5>
			    </div>
		        <div class="container-fluid">
		        	<div class="row">
		        		<div class="col-sm-4">
		        			<div class="col-sm-12">
		        				<label class="col-sm-12 ">سال</label>
		                        <select name="emergency_year" id="emergency_year" class="form-control">
		                            <?php
		                            //default selected values for year and month
		                            $today = dateToShamsi(date('Y'),date('m'),date('d'));
		        					$month = explode('-',$today);
		        					$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');
		        					$year = $month[0];
		        					?>
		                            @for($i=$year-5;$i<$year+5;$i++)
		                            	@if($i==$year)
		                                <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
		                                @else
		                                <option value='{!!$i!!}'>{!!$i!!}</option>
		                                @endif
		                            @endfor
		                        </select>
		                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		        			</div>
		        		</div>
		        		<div class="col-sm-4">
		        			<div class="col-sm-12">
		        				<label class="col-sm-12 ">ماه</label>
		        				
		                        <select name="emergency_month" id="emergency_month" class="form-control" onchange="bringEmergencyHolidays()" required>
		                        	<option value="">انتخاب</option>  
		                            @for($j=1;$j<13;$j++)
		                                <option value='{!!$j!!}'>{!!$month_name[$j]!!}</option>
		                            @endfor
		                        </select>
		                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		        			</div>
		        		</div>
		        		<div class="col-sm-4">
		                	<div class="col-sm-12">
		                		<label class="col-sm-12 ">&nbsp;</label>
		                        <input type="button" class="btn btn-success" onclick="show_emergency_dates()" value="علاوه نمودن"/>
		                		<input type="hidden" name="emergency_number" id="emergency_number" value="0"/>
		                		<input type="hidden" name="e_number" id="e_number" value="0"/>
		                	</div>
		                </div>
		                
		        	</div>
		        </div>
		        <div id="emergency_edit"></div>
		       
		        <div class="container-fluid">
		        	<div class="row">
		        		<div class="col-sm-1">
		        			<div class="col-sm-12">
		        				<label class="col-sm-12 ">&nbsp;</label>
		        				@if(canAdd('hr_attendance'))
		        					<button class="btn btn-primary" type="submit"><i class="fa fa-refresh fa-lg"></i> ذخیره</button>
		        				@else
		        					<p>You dont have permission</p>
		        				@endif
		        			</div>
		        		</div>
		        	</div>
		        </div>
				{!! Form::token() !!}
			</form>
		</div>
	</div>
</header>

@stop
@section('footer-scripts')

<script type="text/javascript">
    
	function bringHolidays()
	{
		$('#edit').html('Loading ... ');
		var year = $('#leave_year').val();
		var month= $('#leave_month').val();
		
		var page = "{!!URL::route('bringMonthHolidays')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        dataType: 'json',
	        data: '&year='+year+'&month='+month,
	        success: function(r){
	        	$('#edit').html(r.view);
	        	$('#holiday_number').val(r.days);
	        	$('#h_number').val(r.days);
	        }
	    });
	}
	
	function showDates()
	{
		$("#edit").empty(); 
		$("#datesCol1").children().hide(); 
		$("#datesCol2").children().hide(); 
		var no = $('#holiday_number').val();
		if(no!=0)
		{
			for(i=1;i<=no;i++)
			{
				$('#h_'+i).show();
				$("#date_"+i).prop('required',true);
				$("#date_"+i).prop('disabled',false);
			}
		}
	}
	function bringEmergencyHolidays()
	{
		$('#emergency_edit').html('Loading ... ');
		var year = $('#emergency_year').val();
		var month= $('#emergency_month').val();
		
		var page = "{!!URL::route('bringMonthEmergencyHolidays')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        dataType: 'json',
	        data: '&year='+year+'&month='+month,
	        success: function(r){
	        	$('#emergency_edit').html(r.view);
	        	$('#emergency_number').val(r.days);
	        	$('#e_number').val(r.days);
	        }
	    });
	}
	
	function show_emergency_dates()
	{
		var no = $('#emergency_number').val();//for loop
		var count = $('#e_number').val();//for number of dates
		var total = parseInt(no)+parseInt(1);
		var count = parseInt(count)+parseInt(1);
		$('#emergency_number').val(total);
		$('#e_number').val(count);
		$.ajax({
			url:'{{URL::route("getMoreEmergencyDate")}}',
			data: '&total='+total+'&type=e',
			type:'POST',
			success:function(r){
				$('#emergency_edit').append(r);
				$(".datepicker_farsi").persianDatepicker();
			}
		});
	}
	function remove_emergency_date(no)
	{
		var count = $('#e_number').val();//for number of dates
		var count = parseInt(count)-parseInt(1);
		$('#e_number').val(count);
		$('#e_'+no).remove();
	}
	function show_holiday_dates()
	{
		var no = $('#holiday_number').val();//for loop
		var count = $('#h_number').val();//for number of dates
		var total = parseInt(no)+parseInt(1);
		var count = parseInt(count)+parseInt(1);
		$('#holiday_number').val(total);
		$('#h_number').val(count);
		$.ajax({
			url:'{{URL::route("getMoreEmergencyDate")}}',
			data: '&total='+total+'&type=h',
			type:'POST',
			success:function(r){
				$('#edit').append(r);
				$(".datepicker_farsi").persianDatepicker();
			}
		});
	}
	function remove_holiday_date(no)
	{
		var count = $('#h_number').val();//for number of dates
		var count = parseInt(count)-parseInt(1);
		$('#h_number').val(count);
		$('#h_'+no).remove();
	}
</script>
@stop

