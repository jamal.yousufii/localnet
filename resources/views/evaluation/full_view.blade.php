@extends('layouts.master')

@section('head')
    <title>{!!_('نمایش کامل سند')!!}</title>

    <style>
    .td-header{
    	font-weight: 400;
		color: #526069;
    }
    </style>
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAllReceivedDocs')!!}">{!!_('evaluation')!!}</a></li>
    <li class="active"><span>نمایش</span></li>
</ol>
<div class="row" dir="rtl">
    <div class="col-lg-12">
    	
      	<div class="row">
      		<div class="col-sm-12">
	           <div class="panel-heading">
			      <h5 class="panel-title">مشخصات سند</h5>
			    </div>
			</div>
		    
        </div>
	    
        <div class="example table-responsive">
            <table class="table" style="direction: rtl;float: right;">
                <thead>
                  <tr>
                    <td align="right" class="td-header">نوعیت سند</td>
                    <td align="right" class="td-header" >شماره سند</td>
                    <td align="right" class="td-header">سال</td>
                    <td align="right" class="td-header">تاریخ سند</td>
                    <td align="right" class="td-header">نوعیت اداره</td>
                    <td align="right" class="td-header">ریاست مربوطه</td>
                    <td align="right" class="td-header">موضوع</td>
                  </tr>
                </thead>
                <tbody>
                  <tr style="background-color: #eee">
                    <td>
                    	@if($details->doc_type==1) مکتوب
                        @elseif($details->doc_type==2) پیشنهاد
                        @elseif($details->doc_type==3) هدایت
                        @else نامعلوم
                        @endif
                    </td>
                    <td>{!!$details->doc_no!!}</td>
                    <td>{!!$details->year!!}</td>
                    <td>
                    	@if($details->doc_date!='')
                		<?php
                		$sdate = $details->doc_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                        {!!$sdate!!}
                        @else
             			{!!$details->doc_date_string!!}
             			@endif
             		</td>
                    <td>
                    	@if($details->dep_type==1) داخلی
                        @elseif($details->dep_type==2) بیرونی
                        @else نامعلوم
                        @endif
                    </td>
                    <td>
                    	@if($details->dep_type==1)
                    		@foreach($parentDeps AS $sub_item)
                                	@if($details->sub_dep==$sub_item->id)
                                <?php $dep = $sub_item->name;break; ?>
                                @else
                                <?php $dep=''; ?>
                                @endif
                            @endforeach
                            {!!$dep!!}
                    	@else
                    		@foreach($ministrires AS $ministry)
                        		@if($details->ministry==$ministry->id)
                                <?php $ministry_name = $ministry->name_dr;break; ?>
                                @else
                                <?php $ministry_name=''; ?>
                                @endif
                            @endforeach
                            {!!$ministry_name!!}
                    	@endif
                    </td>
                    <td>{!!$details->desc!!}</td>
                  </tr>
                  
                </tbody>
            </table>
        </div>
    </div>
	<div class="col-lg-12">		    
       	<hr style="border-top: 1px solid #121314 !important">
       	
      	<div class="row">
      		<div class="col-sm-12">
	           <div class="panel-heading">
			      <h5 class="panel-title">مشخصات ذوات پیشنهاد شده برای مکافات</h5>
			    </div>
			</div>
		    
        </div>
		<div class="example table-responsive">
            <table class="table" style="direction: rtl;float: right;">
                <thead>
                  <tr>
                    <td align="right" class="td-header">اتباع</td>
                    <td align="right" class="td-header" >نام</td>
                    <td align="right" class="td-header">نام پدر</td>
                    <td align="right" class="td-header">نوعیت وظیفه</td>
                    <td align="right" class="td-header">بست</td>
                    <td align="right" class="td-header">عنوان وظیفه</td>
                    <td align="right" class="td-header">نوعیت پهشنهاد شده</td>
                    <td align="right" class="td-header">مورد پیشنهاد شده</td>
                  </tr>
                </thead>
                <tbody>
                  <tr style="background-color: #eee">
                    <td>
                    	@if($emp->nationality==1) افغان
                        @elseif($emp->nationality==2) خارجی
                        @else نامعلوم
                        @endif
                    </td>
                    <td>{!!$emp->name!!}</td>
                    <td>{!!$emp->f_name!!}</td>
                    <td>
                    	@if($emp->emp_type==1) ملکی
                        @elseif($emp->emp_type==2) نظامی
                        @else نامعلوم
                        @endif
             		</td>
                    <td>
                    	{!!$emp->bast!!}
                    </td>
                    <td>
                    	@if($emp->emp_type==1) ملکی
                        @elseif($emp->emp_type==2) نظامی
                        @else نامعلوم
                        @endif
                    </td>
                    <td>
                    	@if($emp->suggested_type==1) مدال
                    	@elseif($emp->suggested_type==2) نشان
                        @elseif($emp->suggested_type==3) تحسین نامه
                        @elseif($emp->suggested_type==4) تقدیرنامه
                        @else نامعلوم
                        @endif
                    </td>
                    <td>
                    	<?php $subitems = getItems($emp->suggested_type);$sugg_item = 'نامعلوم';?>
                    	@if($subitems)
                    		@foreach($subitems AS $sub)
                    			@if($sub->id == $emp->suggested_type_item)
                    			<?php $sugg_item = $sub->name_dr; ?>
                    	
                    			@endif
                    		@endforeach
                    	@endif
                    	{!!$sugg_item!!}
                    </td>
                  </tr>
                  
                </tbody>
            </table>
        </div>	   
	</div>
	<div class="col-lg-12">		    
       	<hr/>
    	
      	<div class="row">
      		<div class="col-sm-12">
	           <div class="panel-heading">
			      <h5 class="panel-title">اجرات ریاست نظارت و ارزیابی</h5>
			    </div>
			</div>
		    
        </div>
        @if($emp->status==1)
        <?php $details = $approved;?>
		<div class="example table-responsive">
            <table class="table" style="direction: rtl;float: right;">
                <thead>
                  <tr>
                    <td align="right" class="td-header">نوعیت مکافات منظور شده</td>
                    <td align="right" class="td-header" >مورد مکافات منظور شده</td>
                    <td align="right" class="td-header">مناسبت</td>
                    
                  </tr>
                </thead>
                <tbody>
                  <tr style="background-color: #eee">
                    <td>
                    	@if($details->approved_type==1) مدال
                    	@elseif($details->approved_type==2) نشانه
                        @elseif($details->approved_type==3) تحسین نامه
                        @elseif($details->approved_type==4) تقدیرنامه
                        @elseif($details->approved_type==0) هیچکدام
                        @endif
                    </td>
                    <td>
                    	<?php $approved_item='نامعلوم'; ?>
                    	@if($items)
                    		@foreach($items AS $item)
                    			@if($item->id == $details->approved_type_item)
                    			<?php $approved_item = $item->name_dr;break; ?>
                    			@endif
                    		@endforeach
                    		
                    	@endif
                    	{!!$approved_item!!}
                    </td>
                    <td>
                    	@if($details->title==1) نهم حوت
                        @elseif($details->title==2) هشت مارچ
                        @elseif($details->title==3) بیست و هشت اسد
                        @elseif($details->title==4) روز معلم
                        @elseif($details->title==0) هیچکدام
                        @endif
                    </td>
                    
                  </tr>
                  
                </tbody>
            </table>
            <table class="table" style="direction: rtl;float: right;">
                <thead>
                  <tr>
                    <td align="right" class="td-header">تاریخ حکم</td>
                    <td align="right" class="td-header" >شماره حکم</td>
                    <td align="right" class="td-header">تاریخ صادره</td>
                    <td align="right" class="td-header">شماره صادره</td>
                    <td align="right" class="td-header">تاریخ فرمان</td>
                    <td align="right" class="td-header">شماره فرمان</td>
                    
                  </tr>
                </thead>
                <tbody>
                  <tr style="background-color: #eee">
                    <td>
                    	@if($details->hokm_date!='')
                		<?php $sdate = $details->hokm_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                        {!!$sdate!!}
                        @else
                        {!!$details->hokm_date_string!!}
                        @endif
                    </td>
                    <td>
                    	{!!$details->hokm_no!!}
                    </td>
                    <td>
                    	@if($details->issued_date!='')
                		<?php $sdate = $details->issued_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>{!!$sdate!!}
                		@else
                		{!!$details->issued_date_string!!}
                		@endif
                    </td>
                    <td>
                    	{!!$details->issued_no!!}
                    </td>
                    <td>
                    	@if($details->farman_date!='')
                		<?php $sdate = $details->farman_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                		{!!$sdate!!}
                		@else
                		{!!$details->farman_date_string!!}
                		@endif
                    </td>
                    <td>
                    	{!!$details->farman_no!!}
                    </td>
                    
                    
                  </tr>
                  
                </tbody>
            </table>
            <table class="table" style="direction: rtl;float: right;">
                <thead>
                  <tr>
                    <td align="right" class="td-header">مرسل</td>
                    <td align="right" class="td-header" >مرسل الیه</td>
                    <td align="right" class="td-header">چگونگی تفویض</td>
                    
                  </tr>
                </thead>
                <tbody>
                  <tr style="background-color: #eee">
                    <td>
                    	@if($approved_details->dep_type==1)
                    		
                                @foreach($parentDeps AS $sub_item)
	                                @if($sub_item->id == $approved_details->sender_sub_dep)
                                		{!!$sub_item->name!!}
                                	@endif
                            	@endforeach
                            
                    	@else
                    		@foreach($ministrires AS $ministry)
                                @if($ministry->id == $approved_details->sender_ministry)
                                	{!!$ministry->name_dr!!}
                                @endif
                            @endforeach
                    	@endif
                    </td>
                    <td>
                    	@if($approved_details->sent_to_dep_type==1)
                    		
                                @foreach($parentDeps AS $sub_item)
	                                @if($sub_item->id == $approved_details->sent_to_sub_dep)
                                		{!!$sub_item->name!!}
                                	@endif
                            	@endforeach
                            
                        @else
                    		@if($send_tos)
                            	@foreach($ministrires AS $ministry)
                            		@foreach($send_tos AS $sent)
                                    	@if($sent->dep_id==$ministry->id)
                                    	{{ $ministry->name_dr }}
                                    	@endif
                                    @endforeach
                                    
                                @endforeach
                             @endif
                             {{-- {!!$sender!!} --}}
                    	@endif
                    </td>
                    
                    <td>
                    	@if($details->how==1) رئیس جمهور اسلامی افغانستان
                        @elseif($details->how==2) وزارت/اداره مربوط
                        @elseif($details->how==0) تفویض نشده
                        
                        @endif
                    </td>
                    
                  </tr>
                  
                </tbody>
            </table>
        </div>
        
        <div class="container-fluid" >
	      	<div class="row">
	            @if($details->file_name!=null)
	            <div class="col-sm-6" id="attached_file_div">
            		<div class="col-sm-12">
                		<label class="col-sm-12 ">&nbsp;</label>
                		<a href="{!!URL::route('downloadDoc',$details->id)!!}">{!!$details->file_name!!}</a>
                		@if(canDelete('evaluation_receivedDocs'))
                		<a href="javascript:void()" onclick="removeReportFile('{!!$details->id!!}');" class="table-link danger">
                            <i class="fa fa-trash-o" style='color:red;'></i>
                        </a>	
                        @endif
                	</div>
               </div>
               @endif
               	
	        </div>
	    </div>  
	    @endif	 
	</div>		   
</div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">
$("#general_department").select2();
$("#sub_dep").select2();
$("#ministry").select2();
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function bring_items(no)
{
	var type = $('#suggested_type_'+no).val();
	
    $.ajax({
            url: '{!!URL::route("bring_items")!!}',
            data: '&type='+type,
            type: 'post',
            beforeSend: function(){
                $("#suggested_type_item_"+no).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#suggested_type_item_'+no).html(response);
            }
        }
    );
}
function showServiceType(value)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#internal_div').slideUp();
        $('#external_div').slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#external_div').slideUp();
        $('#internal_div').slideDown();
    }

}
function add_emp()
{
	$('#add_finding_btn').hide();
	$('#loading_div').show();
	var current_total = $('#total_emp').val();
	//var total_findings = $('#total_findings').val();
	var total = parseInt(current_total)+parseInt(1);
	//var total_findings = parseInt(total_findings)+parseInt(1);
	$('#total_emp').val(total);
	//$('#total_findings').val(total_findings);
	$.ajax({
		url:'{{URL::route("getMoreEmployees")}}',
		data: '&total='+total,
		type:'POST',
		success:function(r){
			$('#emp_1').prepend(r);
			$('#loading_div').hide();
			$('#add_finding_btn').show();
		}
	});
}
function remove_emp(no)
{
	$('#emp_'+no).remove();
}
function removeReportFile(doc_id)
{
    var confirmed = confirm("Do you want to remove this file?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeFile")!!}',
                data: '&doc_id='+doc_id+'&type=received_docs',
                type: 'post',
                beforeSend: function(){
                    $("#errors").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#errors').html(response);
                    $('#attached_file_div').remove();
                    $('#attach_div').show();
                }
            }
        );
    }

}
</script>

@stop

