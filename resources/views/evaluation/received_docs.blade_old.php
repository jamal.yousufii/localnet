@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('received_docs_list')!!}</title>
    {!! HTML::style('/css/font.css') !!}

@stop
@section('content')

    @if(Session::has('success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('success')!!}
		</div>

    @elseif(Session::has('fail'))
    	<div class="alert alert-danger alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('fail')!!}
		</div>
    @endif
    <div class="row" style="margin-top:-30px">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div>
                	<center>
                	<h4>
                    جمهوری اسلامی افغانستان</br>
                    ریاست عمومی دفتر مقام عالی ریاست جمهوری</br>
                    معاونیت پالیسی,نظارت و بررسی</br>
                    ریاست نظارت و ارزیابی
                	</h4></center>
                </div>
                
            </div>
        </div>
    </div>
    
    <div class="container-fluid">
    	<div class="row">
    		<div class="col-sm-3"></div>
    		<div class="col-sm-2">
            	<div class="col-sm-12">
            		@if(canAdd('evaluation_receivedDocs'))
					<header class="main-box-header clearfix">
					    <h2>
					    	<a href="{!!URL::route('addNewDoc')!!}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle fa-lg"></i>ثبت سند جدید</a>
					    </h2>
					</header>
					@endif
            	</div>
            </div>
    		<div class="col-sm-4">
            	<div class="col-sm-12">
            		
                    <select class="form-control" id="year" onchange="bring_data(this.value)">
                        @for($i=1380;$i<=$sh_year;$i++)
                        <option value='{!!$i!!}' @if($i==$year) {!!'selected'!!} @endif>{!!$i!!}</option>
                        @endfor
                    </select>
               	</div>
               	
            </div>
    		<div class="col-sm-2"></div>
            
    	</div>
    </div>    
        <div style="padding:15px" class="table-responsive">
            <table class="table table-bordered table-responsive" id="depList">
                <thead>
                  <tr>
                    <th>شماره</th>
                    <th>نوعیت سند</th>
                    <th>اداره مربوطه</th>
                    <th>شماره سند</th>
                    
                    <th>موضوع</th>
                    <th>تاریخ سند</th>
                    <th>عملیه</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>

            
        </div>
<div class="modal fade modal-fade-in-scale-up" id="reject_modal" aria-hidden="true" aria-labelledby="reject_modal" role="dialog" tabindex="-1"></div>
<div class="md-overlay"></div><!-- the overlay element -->

@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#depList').dataTable(
        {
			"order": [[ 1, 'desc' ]],
            "sDom": 'lfr<"clearfix">tip',
            "bProcessing": true,
            "bServerSide": true,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getAllDocs',array($year))!!}"

        }
    );
});
function bring_data(id)
{
	window.location.href = "getAllReceivedDocs/"+id;
}

</script>

@stop


