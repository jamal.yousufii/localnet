@extends('layouts.master')

@section('head')
    <title>{!!_('edit_employee')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAuditReports')!!}">{!!_('evaluation')!!}</a></li>
    <li class="active"><span>{!!_('edit_employee')!!}</span></li>
</ol>
    <div class="row">
        <form class="form-horizontal" role="form" id="edit_doc_form" method="post">
            <div class="container-fluid">
                <div class="row">
                    
                    <div class="col-sm-3">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">نوعیت سند<span style="color:red">*</span>
                            </label>
                            <select name="doc_type" required class="form-control">
                                
                                <option value='1' @if($details->doc_type==1) selected @endif>مکتوب</option>
                                <option value='2' @if($details->doc_type==2) selected @endif>پیشنهاد</option>
                                <option value='3' @if($details->doc_type==3) selected @endif>هدایت</option>
                                
                            </select>
                        </div>
                        
                    </div>
                    <div class="col-sm-3">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">وزارت/اداره<span style="color:red">*</span></label>
                        </div>
                        <div class="col-sm-12">
                            <select style="width:100%" name="ministry" class="form-control">
                                <option value="">انتخاب</option>
                                @foreach($ministrires AS $ministry)
                                    @if($details->ministry==$ministry->id)
                                    <option value='{!!$ministry->id!!}' selected>{!!$ministry->name_dr!!}</option>
                                    @else
                                    <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
                                    @endif
                                @endforeach
                            </select>
                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">شماره سند
                            </label>
                            <input class="form-control" type="text" name="doc_no" value="{!!$details->doc_no!!}">
                        </div>
                    </div>
                
                    <div class="col-sm-2">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">تاریخ سند
                            
                            </label>
                            <?php $sdate = $details->doc_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                            <input class="form-control datepicker_farsi" readonly type="text" name="date" value='<?php if($details->doc_date !=null){echo jalali_format($sdate);}?>'>
                            <span style="color:red">{!!$errors->first('date')!!}
                            @if($details->doc_date==null)
                                {!!$details->doc_date_string!!}
                            @endif
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">سال
                            
                            </label>
                            <select class="form-control" name="year" required>
                                
                                @for($i=1380;$i<=$year;$i++)
                                <option value='{!!$i!!}' @if($i==$details->year) {!!'selected'!!} @endif>{!!$i!!}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
            </div>
             
            <div class="container-fluid" >
                <div class="row">
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">موضوع</label>
                            <input class="form-control" type="text" name="desc" value="{!!$details->desc!!}">
                        </div>
                    </div>
                    @if($details->file_name!=null)
                        <div class="col-sm-6" id="attached_file_div">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">&nbsp;</label>
                                <a href="{!!URL::route('downloadDocument',$details->id)!!}">{!!$details->file_name!!}</a>
                                @if(canDelete('evaluation_receivedDocs'))
                                <a href="javascript:void()" onclick="removeReportFile('{!!$details->id!!}');" class="table-link danger">
                                    <i class="fa fa-trash-o" style='color:red;'></i>
                                </a>    
                                @endif
                            </div>
                       </div>
                    
                   @endif
                   <div class="col-sm-4" @if($details->file_name!=null) style="display:none" @endif id="attach_div">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">فایل ضمیمه</label>
                            
                            <input type='file'  name='attach' class="form-control">
                        </div>
                   </div>
                   <div class="col-sm-2" style="display:none;float:right" id="doc_loading_div">
                        <label class="col-sm-12">&nbsp;</label>
                        <button class="btn btn-success" type="button">Updating</button>
                    </div>
                   <div class="col-sm-2" id="edit_doc_btn">
                        <div class="col-sm-12">
                            <label class="col-sm-12">&nbsp;</label>
                            @if(canEdit('evaluation_receivedDocs'))
                                <button class="btn btn-success" type="button" onclick="update_doc();">ثبت تغییرات سند</button>
                            @else
                                <p>You dont have Edit permission</p>
                            @endif
                            
                        </div>
                    </div>
               </div>
           </div>
           <input type="hidden" name="doc_id" value="{!!$emp->doc_id!!}"/>
        </form>
        <hr style="border-top: 1px solid #121314 !important">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" id="edit_emp_form" method="post">
			    
                <div class="container-fluid">
                	<div class="row">
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-2 ">اتباع<span style="color:red">*</span>
			            		</label>
                                <select name="nationality" required class="form-control">
                                   	<option value='1' @if($emp->nationality==1) selected @endif>افغان</option>
                                    <option value='2' @if($emp->nationality==2) selected @endif>خارجی</option>
                                    
                                </select>
			               	</div>
			               	
			            </div>
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">نام مکمل<span style="color:red">*</span>
                        		
                        		</label>
                                <input class="form-control" required type="text" name="name" value="{!!$emp->name!!}">
                                
                        	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">نام پدر<span style="color:red">*</span>
                        		
                        		</label>
                                <input class="form-control" type="text" name="f_name" value="{!!$emp->f_name!!}">
                                
                        	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">نوعیت وظیفه<span style="color:red">*</span></label>
                                <select name="job" class="form-control" onchange="showRank(this.value)">
                                	<option value="">یک گزینه را انتخاب کنید</option>
                                	<option value='1' @if($emp->emp_type==1) selected @endif>ملکی</option>
                                    <option value='2' @if($emp->emp_type==2) selected @endif>نظامی</option>
                                    
                                </select>
			               	</div>
			            </div>
                	</div>
                	<div class="row">
                		<div id="bast_div" @if($emp->emp_type==2) style="display:none" @endif>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "emp_bast" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getBastStaticList('employee_rank',$emp->emp_bast)!!}
	                                </select>
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">رتبه</label>
	                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
	                                <select name = "emp_rank" id="emp_rank" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getStaticDropdown('employee_rank',$emp->emp_rank)!!}
	                                </select>
				      			</div>
				      		</div>
				      	</div>
					      	
			      		<div id="military_div" @if($emp->emp_type==1) style="display:none" @endif>
			      			<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست نظامی</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "military_bast" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getStaticDropdown('military_rank',$emp->military_bast)!!}
                                    </select>
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">رتبه</label>
	                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
	                                <select name = "military_rank" id="military_rank" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getStaticDropdown('military_rank',$emp->military_rank)!!}
	                                </select>
				      			</div>
				      		</div>
			      		</div>
			      		<div class="col-sm-3">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">عنوان وظیفه
                        		
                        		</label>
                                <input class="form-control" type="text" name="job_title" value="{!!$emp->job_title!!}">
                                
                        	</div>
			            </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">وزارت/اداره مربوطه</label>
                            </div>
                            <div class="col-sm-12">
                                <select style="width:100%" name="emp_ministry" class="form-control">
                                    <option value="">انتخاب</option>
                                    @foreach($ministrires AS $ministry)
                                        @if($emp->ministry==$ministry->id)
                                        <option value='{!!$ministry->id!!}' selected>{!!$ministry->name_dr!!}</option>
                                        @else
                                        <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
			      		<div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">نوعیت مکافات پیشنهاد شده</label>
                                <select name="suggested_type" id="suggested_type" class="form-control" onchange="bring_items()">
                                	<option value="">یک گزینه را انتخاب کنید</option>
                                	<option value='1' @if($emp->suggested_type==1) selected @endif>مدال</option>
                                	<option value='2' @if($emp->suggested_type==2) selected @endif>نشان</option>
                                    <option value='3' @if($emp->suggested_type==3) selected @endif>تحسین نامه</option>
                                    <option value='4' @if($emp->suggested_type==4) selected @endif>تقدیرنامه</option>
                                    
                                </select>
			               	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">مورد مکافات پیشنهاد شده</label>
                                <select name="suggested_type_item" id="suggested_type_item" class="form-control">
                                	<option value="">یک گزینه را انتخاب کنید</option>
                                	
                                	@foreach($items AS $item)
                                		@if($item->id == $emp->suggested_type_item)
                                		<option value="{!!$item->id!!}" selected>{!!$item->name_dr!!}</option>
                                		@else
                                		<option value="{!!$item->id!!}">{!!$item->name_dr!!}</option>
                                		@endif
                                	@endforeach
                                </select>
			               	</div>
			            </div>
                        <div class="col-sm-3" style="display:none;float:right" id="emp_loading_div">
                            <label class="col-sm-12">&nbsp;</label>
                            <button class="btn btn-success" type="button">Updating</button>
                        </div>
			            <div class="col-sm-3" id="edit_emp_btn">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12">&nbsp;</label>
		                        @if(canEdit('evaluation_receivedDocs'))
		                        	<button class="btn btn-primary" type="button" onclick="update_emp();">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have Edit permission</p>
		                    	@endif
		                        
		                    </div>
                    	</div>
                	</div>
               </div>
               <input type="hidden" name="id" value="{!!$id!!}"/>
               
               
               
			 	<div class="container_fluid">
			 		<div class="row">
			 			<div class="col-sm-12">
			 			@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
			 			</div>
			 		</div>
			 	</div>
                
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">

function bring_items()
{
	var type = $('#suggested_type').val();
	
    $.ajax({
            url: '{!!URL::route("bring_items")!!}',
            data: '&type='+type,
            type: 'post',
            beforeSend: function(){
                $("#suggested_type_item").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#suggested_type_item').html(response);
            }
        }
    );
}
function showRank(value)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#bast_div').slideUp();
        $('#military_div').slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#military_div').slideUp();
        $('#bast_div').slideDown();
    }

}
function update_emp()
{
    $('#edit_emp_btn').hide();
    $('#emp_loading_div').show();
	$.ajax({
            url: '{!!URL::route("postEditEmployee")!!}',
            data: $('#edit_emp_form').serialize(),
            type: 'post',
            
            success: function(response)
            {
                $('#emp_loading_div').hide();
                $('#edit_emp_btn').show();
                alert('Updated');
            }
        }
    );
}
function update_doc()
{
    $('#edit_doc_btn').hide();
    $('#doc_loading_div').show();
    $.ajax({
            url: '{!!URL::route("postDocViaAjax")!!}',
            data: $('#edit_doc_form').serialize(),
            type: 'post',
            
            success: function(response)
            {
                $('#doc_loading_div').hide();
                $('#edit_doc_btn').show();
                alert('Updated');
            }
        }
    );
}
</script>

@stop

