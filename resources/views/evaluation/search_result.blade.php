<style>
    .scroll-table {
    width: auto;
    overflow-x: auto;
    white-space: nowrap;
}
</style>
@if(count($result)>0)

<div class="col-sm-2 pull-left">
    مجموع: {!!count($total)!!}
</div>
<?php echo $result->render(); ?>

<div class="col-lg-12" id="result_ajax">
    <div class="main-box">
        <div class="main-box-body clearfix">
            <div style="padding:15px;height: 520px;" class="table-responsive">
	            <table class="table table-bordered table-responsive scroll-table" id="depList">
	                <thead>
						<tr>
							<td></td>
							<td colspan="7" style="background-color: #39ac39;text-align: center;font-weight: bold;border: 1px solid orange;color: black;">مشخصات سند</td>
							<td colspan="9" style="background-color: #79d279;text-align: center;font-weight: bold;border: 1px solid orange;color: black;">مشخصات ذوات پیشنهاد شده</td>
							<td colspan="14" style="background-color: #b3e6b3;text-align: center;font-weight: bold;border: 1px solid orange;color: black;">اجراآت ریاست نظارت وارزیابی</td>
							<td></td>
						</tr>
	                  <tr>
	                    <th>#</th>
	                    <th style="font-weight: bold;">نوعیت سند</th>
	                    <th style="font-weight: bold;">شماره سند</th>
	                    <th style="font-weight: bold;">تاریخ سند</th>
	                    <th style="font-weight: bold;">اداره مربوطه</th>
	                    <th style="font-weight: bold;">موضوع</th>
	                    <th style="font-weight: bold;">سال</th>
	                    <th style="font-weight: bold;">فایل</th>

	                    <th style="font-weight: bold;">نام مکمل</th>
	                    <th style="font-weight: bold;">نام پدر</th>
	                    <th style="font-weight: bold;">اتباع</th>
	                    <th style="font-weight: bold;">عنوان وظیفه</th>
	                    <th style="font-weight: bold;">نوعیت وظیفه</th>
	                    <th style="font-weight: bold;">بست</th>
	                    <th style="font-weight: bold;">وزارت/اداره مربوطه</th>
	                    <th style="font-weight: bold;">نوعیت پیشنهاد شده</th>
	                    <th style="font-weight: bold;">مورد پیشنهاد شده</th>

	                    <th style="font-weight: bold;">شماره حکم</th>
	                    <th style="font-weight: bold;">تاریخ حکم</th>
	                    <th style="font-weight: bold;">شماره فرمان</th>
	                    <th style="font-weight: bold;">تاریخ فرمان</th>
	                    <th style="font-weight: bold;">شماره صادره</th>
	                    <th style="font-weight: bold;">تاریخ صادره</th>
	                    <th style="font-weight: bold;">نوعیت منظور شده</th>
	                    <th style="font-weight: bold;">مورد منظور شده</th>
	                    <th style="font-weight: bold;">مناسبت</th>
	                    <th style="font-weight: bold;">مرسل</th>
	                    <th style="font-weight: bold;">مرسل الیه</th>
	                    <th style="font-weight: bold;">چگونگی تفویض</th>
	                    <th style="font-weight: bold;">موضوع</th>
	                    <th style="font-weight: bold;">فایل</th>
	                    <th colspan="3" style="font-weight: bold;">عملیه</th>
	                    
	                  </tr>
	                </thead>
	
	                <tbody>
	                
	                <?php $i = 1;?>
	                	@foreach($result as $row)
	                	
	                	<tr>
	                	<td>{!!$i!!}</td>
	                	<td>{!!$row->doc_type!!}</td>
	                	<td>{!!$row->doc_no!!}</td>
						<?php $sdate = $row->doc_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);} ?>
	                	<td>{!!$sdate!!}</td>
	                	<td>{!!$row->dep!!}</td>
	                	<td>{!!$row->desc!!}</td>
	                	<td>{!!$row->year!!}</td>
	                	<td>{!!$row->doc_file!!}</td>

	                	<td>{!!$row->name!!}</td>
	                	<td>{!!$row->f_name!!}</td>
	                	<td>{!!$row->nationality!!}</td>
	                	<td>{!!$row->job_title!!}</td>
	                	<td>{!!$row->emptype!!}</td>
	                	<td>{!!$row->bast!!}</td>
	                	<td>{!!$row->emp_dir!!}</td>
	                	<td>{!!$row->suggested_type!!}</td>
	                	<td>{!!$row->suggested_type_item!!}</td>

	                	<td>@if(isset($row->hokm_no)) {!!$row->hokm_no!!} @endif</td>
	                	<?php
	                	if(isset($row->hokm_date))
	                	{
	                		$sdate = $row->hokm_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}
	                	}else{$sdate='';} ?>
	                	<td>{!!$sdate!!}</td>
	                	<td>@if(isset($row->farman_no)) {!!$row->farman_no!!} @endif</td>
	                	<?php 
	                	if(isset($row->farman_date))
	                	{
	                		$sdate = $row->farman_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}
	                	}else{$sdate='';} ?>
	                	<td>{!!$sdate!!}</td>
	                	<td>@if(isset($row->issued_no)) {!!$row->issued_no!!} @endif</td>
	                	<?php 
	                	if(isset($row->issued_date))
	                	{
	                		$sdate = $row->issued_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}
	                	}else{$sdate='';} ?>
	                	<td>{!!$sdate!!}</td>
	                	<td>@if(isset($row->approved_type)) {!!$row->approved_type!!} @endif</td>
	                	<td>@if(isset($row->approved_type_item)) {!!$row->approved_type_item!!} @endif</td>
	                	<td>
	                	@if(isset($row->title))
	                		@if($row->title==1) نهم حوت
	                        @elseif($row->title==2) هشت مارچ
	                        @elseif($row->title==3) بیست و هشت اسد
	                        @elseif($row->title==4) روز معلم
	                        @elseif($row->title==0) هیچکدام
	                        @endif
	                    @endif
	                	</td>
	                	<td>ریاست نظارت و رزیابی</td>
	                	<td>{!!$row->elayhe!!}</td>
	                	<td>@if(isset($row->how)) {!!$row->how!!} @endif</td>
	                	<td>@if(isset($row->subject)) {!!$row->subject!!} @endif</td>
	                	<td>@if(isset($row->app_file)) {!!$row->app_file!!} @endif</td>
	                	<td>
	                	<?php $options=''; 
	                	$options.= '<a href="'.route('docFullView',array($row->doc_id,$row->id)).'" target="_blank" title="نمایش"><i class="fa fa-eye"></i></a> | ';
	                	$options.= '<a href="'.route('depEmpEdit',$row->id).'" target="_blank" title="ویرایش کارمند"><i class="fa fa-edit"></i></a> | ';
	                	if(canDelete('evaluation_receivedDocs'))
						{
							if($row->status==0)
							{
								$options.='<a href="'.route('postEmployeeDelete',$row->id).'" onclick="javascript:return confirm(\'Are you sure ???\')" title="حذف"><i class="fa fa-trash-o"></i></a> |';
							}
							elseif($row->status==1)
							{//cant be deleted as it is approved or rejected
								$options.='<a href="javascript:void()" onclick="alert(\'you cant delete this employee since it is approved\')" title="اجرا شده"><i class="fa fa-trash-o"></i></a> |';
							}
							elseif($row->status==2)
							{//cant be deleted as it is approved or rejected
								$options.='<a href="javascript:void()" onclick="alert(\'you cant delete this employee since it is rejected\')" title="اجرا نشده"><i class="fa fa-trash-o"></i></a> |';
							}
						}		
						if($row->status==0)
						{
							$options.='<a href="'.route('approve_doc',$row->id).'" target="_blank" title="تایید"><i class="fa fa-check"></i></a>';
						}
						elseif($row->status==1)
						{//approved
							$options.='<a href="'.route('approve_doc',$row->id).'" target="_blank" title="تایید شده"><i class="fa fa-check-square-o"></i></a>';
						}
						else
						{//rejected
							//$options.='<a href="'.route('loadRejectDoc',$row->id).'" target="_blank" title="رد شده"><i class="fa fa-times-circle"></i></a>';
						}
						echo $options;
						?>
	                	</td>
                		
	                	</tr>
	                	<?php $i++;?>
	                	@endforeach
	                	<?php 
	                	
	                	echo "<script>
							$('ul.pager li a').attr('href','javascript:void()');
							$('ul.pager li.disabled').html('<a rel=\"previous\">«</a>');
							$('ul.pager li a').click(function(){
								window.scrollTo(0, 2000);
								if($(this).attr('rel')=='next')
								{
									$('ul.pager li').removeClass('disabled');

									var pageno = $('#pageno').val();
									var total = parseInt(pageno)+parseInt(1);
									$('#pageno').val(total);
									do_search_ajax(total);
								}
								else
								{
									var pageno = $('#pageno').val();
									var total = parseInt(pageno)-parseInt(1);
									if(total<1)
									{
										$('ul.pager li:first').addClass('disabled');
										$('#pageno').val(0);
									}
									else
									{
										$('#pageno').val(total);
									}
									do_search_ajax(total);
								}
							});
	                	</script>";
	                	?>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<input type="hidden" id="pageno" value="0">
@else
<div><center>ریکاردی پیدا نشد</center>
<button class="btn btn-primary pull-right" type="button" onclick="do_search(0)"> جستجو</button>
</div>
@endif
