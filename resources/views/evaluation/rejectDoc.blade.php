@extends('layouts.master')

@section('head')
    <title>{!!_('register_new_document')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAllDocEmployees')!!}">{!!_('evaluation')!!}</a></li>
    <li class="active"><span>{!!_('reject_form')!!}</span></li>
</ol>
<div class="row">
    <div class="col-lg-12">
        <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postRejectDoc')!!}" enctype="multipart/form-data">
		    <div class="panel-heading">
		      <h5 class="panel-title">جزییات</h5>
		    </div>
     
            <div class="container-fluid">
            	<div class="row">
            		<div class="col-sm-3">
		            	<div class="col-sm-12">
                    		<label class="col-sm-12 ">شماره صادره
                    		<span style="color:red">*</span>
                    		</label>
                            <input class="form-control" required type="text" name="issued_no" value="{!!Input::old('issued_no')!!}">
                    	</div>
		            </div>
            	
		            <div class="col-sm-3">
		            	<div class="col-sm-12">
                    		<label class="col-sm-12 ">تاریخ صادره</label>
                            <input class="form-control datepicker_farsi" readonly type="text" name="issued_date" value="{!!Input::old('issued_date')!!}">
                    	</div>
		            </div>
		            <div class="col-sm-3">
            			<div class="col-sm-12">
            				<label class="col-sm-12 ">مرسل الیه</label>
            			</div>
            			<div class="col-sm-12">
                            <select name="ministry" id="send_to" required class="form-control" style="width:100%" data-plugin="select2">
                                <option value="">انتخاب</option>
                            	@foreach($ministrires AS $ministry)
                                    <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
                                @endforeach
                            </select>
                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
            			</div>
            		</div>
            		<div class="col-sm-3">
			      			<div class="col-sm-12">
	            				<label class="col-sm-12 ">مرسل</label>
	            			</div>
	            			<div class="col-sm-12">
	            				
	                            <select style="width:100%" name="general_department" id="general_department" class="form-control">
	                                <option value="">ریاست نظارت و ارزیابی</option>
	                            </select>
	            			</div>
	            		</div>
            	</div>
            </div>
            
		    <div class="container-fluid" >
		      	<div class="row">
		      		<div class="col-sm-6">
		            	<div class="col-sm-12">
		              		<label class="col-sm-12 ">موضوع</label>
                            <input class="form-control" type="text" name="subject" value="{!!Input::old('subject')!!}">
		               	</div>
		            </div>
                   	<div class="col-sm-6">
                		<div class="col-sm-12">
                    		<label class="col-sm-12 ">فایل ضمیمه</label>
                    		
                            <input type='file'  name='attach' class="form-control">
                    	</div>
                   </div>
		            
		        </div>
		    </div>
		   <br/>
           <input type="hidden" name="id" value="{!!$id!!}">
           <div class="container-fluid" >
		      	<div class="row">
                   <div class="col-sm-6">
                		<div class="col-sm-12">
                    		<label class="col-sm-2 ">&nbsp;</label>
	                        @if(canAdd('evaluation_receivedDocs'))
	                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
	                    	@else
	                    		<p>You dont have permission</p>
	                    	@endif
	                        <button onclick="history.back()" class="btn btn-danger" type="button">برگشت</button>
	                    </div>
                	</div>
                	
		      	</div>
		    </div>
		 	<div class="container_fluid">
		 		<div class="row">
		 			<div class="col-sm-12">
		 			@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
		 			</div>
		 		</div>
		 	</div>
            
            {!! Form::token() !!}
        </form>
    </div>
</div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script>
$("#general_department").select2();
$("#sub_dep").select2();
$("#send_to").select2();
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function showServiceType(value)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#internal_div').slideUp();
        $('#external_div').slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#external_div').slideUp();
        $('#internal_div').slideDown();
    }

}
</script>
@stop