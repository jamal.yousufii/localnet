@extends('layouts.master')

@section('head')
    <title>{!!_('register_new_document')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAllReceivedDocs')!!}">{!!_('evaluation')!!}</a></li>
    <li class="active"><span>علاوه نمودن سند جدید</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postDoc')!!}" enctype="multipart/form-data">
			    <div class="panel-heading">
			      <h5 class="panel-title">مشخصات سند</h5>
			    </div>
                <div class="container-fluid">
                	<div class="row">
                		
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12">نوعیت سند<span style="color:red">*</span>
			            		</label>
                                <select name="doc_type" class="form-control">
                                    <option value=''>یک گزینه را انتخاب کنید</option>
                                    <option value='1' <?php echo (Input::old('doc_type')=='1' ? 'selected':''); ?>>مکتوب</option>
                                    <option value='2' <?php echo (Input::old('doc_type')=='2' ? 'selected':''); ?>>پیشنهاد</option>
                                    <option value='3' <?php echo (Input::old('doc_type')=='3' ? 'selected':''); ?>>هدایت</option>
                                    
                                </select>
			               	</div>
			               	
			            </div>
			            <div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">وزارت/اداره<span style="color:red">*</span></label>
                			</div>
                			<div class="col-sm-12">
                                <select style="width:100%" name="ministry" id="ministry" class="form-control">
                                    <option value="">انتخاب</option>
                                	@foreach($ministrires AS $ministry)
                                        <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
                                    @endforeach
                                </select>
                			</div>
                		</div>
                		<div class="col-sm-2">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">شماره سند <span style="color:red">*</span>
                        		</label>
                                <input class="form-control" type="text" name="doc_no" value="{!!Input::old('doc_no')!!}">
                        	</div>
			            </div>
                		<div class="col-sm-2">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ سند <span style="color:red">*</span>
                        		
                        		</label>
                                <input class="form-control datepicker_farsi" readonly type="text" name="date" value="{!!Input::old('date')!!}">
                                <span style="color:red">{!!$errors->first('date')!!}</span>
                        	</div>
			            </div>
			            <div class="col-sm-2">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">سال <span style="color:red">*</span>
                        		
                        		</label>
                                <select class="form-control" name="year" required>
                                	
			                        @for($i=1380;$i<=$year;$i++)
			                        <option value='{!!$i!!}' @if($i==$year) {!!'selected'!!} @endif>{!!$i!!}</option>
			                        @endfor
			                    </select>
                        	</div>
			            </div>
			            
                	</div>
                </div>
	            </br>
				<div class="container-fluid" >
			      	<div class="row">
	            		<div class="col-sm-6">
	                		<div class="col-sm-12">
	                			<label class="col-sm-12 ">موضوع <span style="color:red">*</span></label>
	                            <input class="form-control" type="text" name="desc" value="{!!Input::old('desc')!!}">
	                        </div>
	                    </div>
                		<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">فایل ضمیمه</label>
	                    		
	                            <input type='file'  name='attach' class="form-control">
	                    	</div>
	                   </div>
                	</div>
               	</div>
               	
			    
	           <hr style="border-top: 1px solid #121314 !important">
	           <div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
				           <div class="panel-heading">
						      <h5 class="panel-title">ذوات پیشنهاد شده</h5>
						    </div>
						</div>
					    <div class="col-sm-6">
		                	<div class="col-sm-1" style="display:none;float:right" id="loading_div">
							    <label class="col-sm-12">&nbsp;</label>
							    <button class="btn btn-primary" style="float:right" type="button">LOADING</button>
						    </div>
			            	<div class="col-sm-11" id="add_finding_btn">
				            	<label class="col-sm-12">&nbsp;</label>
						        <button class="btn btn-primary" style="float:right" onclick="add_emp()" type="button"> + </button>
						    </div>
						    
			            </div>
			        </div>
			   </div>
			   <div class="container-fluid" id="emp_1">
                	<div class="row">
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12">اتباع <span style="color:red">*</span>
			            		</label>
                                <select name="nationality_1" class="form-control">
                                   	<option value='1' <?php echo (Input::old('nationality')=='1' ? 'selected':''); ?>>افغان</option>
                                    <option value='2' <?php echo (Input::old('nationality')=='2' ? 'selected':''); ?>>خارجی</option>
                                    
                                </select>
			               	</div>
			               	
			            </div>
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">نام مکمل<span style="color:red">*</span>
                        		
                        		</label>
                                <input class="form-control" required type="text" name="name_1" value="{!!Input::old('name_1')!!}">
                                
                        	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">نام پدر <span style="color:red">*</span>
                        		
                        		</label>
                                <input class="form-control" type="text" name="f_name_1" value="{!!Input::old('f_name_1')!!}">
                                
                        	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">نوعیت وظیفه <span style="color:red">*</span></label>
                                <select name="job_1" class="form-control" onchange="showRank(this.value)">
                                	<option value="">انتخاب</option>
                                	<option value='1' <?php echo (Input::old('job_1')=='1' ? 'selected':''); ?>>ملکی</option>
                                    <option value='2' <?php echo (Input::old('job_1')=='2' ? 'selected':''); ?>>نظامی</option>
                                    
                                </select>
			               	</div>
			            </div>
                	</div>
                	<div class="row">
                		<div id="bast_div">
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست <span style="color:red">*</span></label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "emp_bast_1" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getBastStaticList('employee_rank')!!}
	                                </select>
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">رتبه <span style="color:red">*</span></label>
	                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
	                                <select name = "emp_rank_1" id="emp_rank" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getStaticDropdown('employee_rank')!!}
	                                </select>
				      			</div>
				      		</div>
				      	</div>
					      	
			      		<div id="military_div" style="display:none;">
			      			<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست نظامی <span style="color:red">*</span></label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "military_bast_1" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getStaticDropdown('military_rank',Input::old('emp_bast'))!!}
                                    </select>
				      			</div>
				      		</div>
			      			<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">رتبه <span style="color:red">*</span></label>
	                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
	                                <select name = "military_rank_1" id="military_rank" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getStaticDropdown('military_rank')!!}
	                                </select>
				      			</div>
				      		</div>
			      		</div>
			      		<div class="col-sm-3">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">عنوان وظیفه <span style="color:red">*</span>
                        		
                        		</label>
                                <input class="form-control" type="text" name="job_title_1" value="">
                                
                        	</div>
			            </div>
			            <div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">وزارت/اداره مربوط<span style="color:red">*</span></label>
                			</div>
                			<div class="col-sm-12">
                                <select style="width:100%" name="emp_ministry_1" class="form-control">
                                    <option value="">انتخاب</option>
                                	@foreach($ministrires AS $ministry)
                                        <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
                                    @endforeach
                                </select>
                			</div>
                		</div>
			      		<div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">نوعیت مکافات پیشنهاد شده <span style="color:red">*</span></label>
                                <select name="suggested_type_1" required id="suggested_type_1" class="form-control" onchange="bring_items(1)">
                                	<option value="">یک گزینه را انتخاب کنید</option>
                                	<option value='1' <?php echo (Input::old('suggested_type_1')=='1' ? 'selected':''); ?>>مدال</option>
                                	<option value='2' <?php echo (Input::old('suggested_type_1')=='2' ? 'selected':''); ?>>نشان</option>
                                    <option value='3' <?php echo (Input::old('suggested_type_1')=='3' ? 'selected':''); ?>>تحسین نامه</option>
                                    <option value='4' <?php echo (Input::old('suggested_type_1')=='4' ? 'selected':''); ?>>تقدیرنامه</option>
                                    
                                </select>
			               	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">مورد مکافات پیشنهاد شده <span style="color:red">*</span></label>
                                <select name="suggested_type_item_1" required id="suggested_type_item_1" class="form-control">
                                	<option value="">یک گزینه را انتخاب کنید</option>
                                </select>
			               	</div>
			            </div>
				        <div class="col-sm-3">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12">&nbsp;</label>
		                        @if(canAdd('evaluation_receivedDocs'))
		                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">برگشت</button>
		                    </div>
                    	</div>
                	</div>
                	
               </div>
               
               <input type="hidden" id="total_emp" name="total_emp" value="1"/>
	           
			 	<div class="container_fluid">
			 		<div class="row">
			 			<div class="col-sm-12">
			 			@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
			 			</div>
			 		</div>
			 	</div>
                
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">
$("#general_department").select2();
$("#sub_dep").select2();
$("#ministry").select2();
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function bring_items(no)
{
	var type = $('#suggested_type_'+no).val();
	
    $.ajax({
            url: '{!!URL::route("bring_items")!!}',
            data: '&type='+type,
            type: 'post',
            beforeSend: function(){
                $("#suggested_type_item_"+no).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#suggested_type_item_'+no).html(response);
            }
        }
    );
}
function showServiceType(value)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#internal_div').slideUp();
        $('#external_div').slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#external_div').slideUp();
        $('#internal_div').slideDown();
    }

}
function showRank(value)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#bast_div').slideUp();
        $('#military_div').slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#military_div').slideUp();
        $('#bast_div').slideDown();
    }

}
function add_emp()
{
	$('#add_finding_btn').hide();
	$('#loading_div').show();
	var current_total = $('#total_emp').val();
	//var total_findings = $('#total_findings').val();
	var total = parseInt(current_total)+parseInt(1);
	//var total_findings = parseInt(total_findings)+parseInt(1);
	$('#total_emp').val(total);
	//$('#total_findings').val(total_findings);
	$.ajax({
		url:'{{URL::route("getMoreEmployees")}}',
		data: '&total='+total,
		type:'POST',
		success:function(r){
			$('#emp_1').prepend(r);
			$('#loading_div').hide();
			$('#add_finding_btn').show();
		}
	});
}
function remove_emp(no)
{
	$('#emp_'+no).remove();
}
</script>

@stop

