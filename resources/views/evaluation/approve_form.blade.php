@extends('layouts.master')

@section('head')
    <title>{!!_('approve_form')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAllDocEmployees')!!}">{!!_('evaluation')!!}</a></li>
    <li class="active"><span>{!!_('approve_form')!!}</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postApproveDoc')!!}" enctype="multipart/form-data">
			    <div class="panel-heading">
			      <h5 class="panel-title">فورم تایید</h5>
			    </div>
                <div class="container-fluid">
                	<div class="row">
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">نوعیت مکافات منظور شده
			              		<span style="color:red">*</span>
			              		</label>
                                <select name="approved_type" id="approved_type" class="form-control" required onchange="bring_items()">
                                	<option value="">یک گزینه را انتخاب کنید</option>
                                    <?php $counter = 0; ?>
                                    @foreach (Config::get('myConfig.evaluation.eval_type.dr') as $item)
										<option value="{{$counter}}" <?=$emp->suggested_type==$counter? 'selected': '' ?> >{{$item}}</option>
									  <?php $counter++; ?>
									@endforeach
                                </select>
			               	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">مورد مکافات منظور شده
			              		<span style="color:red">*</span>
			              		</label>
                                <select name="approved_type_item" id="approved_type_item" required class="form-control">
									<option value="">یک گزینه را انتخاب کنید</option>
									<?php $items = getItems($emp->suggested_type); ?>
                                	@foreach($items AS $item)
                                		@if($item->id == $emp->suggested_type_item)
                                		<option value="{!!$item->id!!}" selected>{!!$item->name_dr!!}</option>
                                		@else
                                		<option value="{!!$item->id!!}">{!!$item->name_dr!!}</option>
                                		@endif
                                	@endforeach
                                </select>
			               	</div>
			            </div>
                		<div class="col-sm-6">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">مناسبت
			              		<span style="color:red">*</span>
			              		</label>
	                            <select name="title" class="form-control">
	                            	<option value="">یک گزینه را انتخاب کنید</option>
	                            	<option value='1' <?php echo (Input::old('title')=='1' ? 'selected':''); ?>>نهم حوت</option>
	                                <option value='2' <?php echo (Input::old('title')=='2' ? 'selected':''); ?>>هشت مارچ</option>
	                                <option value='3' <?php echo (Input::old('title')=='3' ? 'selected':''); ?>>بیست و هشت اسد</option>
	                                <option value='4' <?php echo (Input::old('title')=='4' ? 'selected':''); ?>>روز جهانی معلم</option>
	                                <option value='5' <?php echo (Input::old('title')=='5' ? 'selected':''); ?>>روز آغاز سال تعلیمی جدید</option>
	                                <option value='6' <?php echo (Input::old('title')=='6' ? 'selected':''); ?>>روز مطبوعات</option>
	                                <option value='7' <?php echo (Input::old('title')=='7' ? 'selected':''); ?>>روز گرامیداشت از نزول قرآن عظیم الشان</option>
	                                <option value='8' <?php echo (Input::old('title')=='8' ? 'selected':''); ?>>روز جهانی المپیک</option>
	                                <option value='9' <?php echo (Input::old('title')=='9' ? 'selected':''); ?>>روز استرداد استقلال افغانستان</option>
	                                
	                                <option value='10' <?php echo (Input::old('title')=='10' ? 'selected':''); ?>>روز هنر</option>
	                                <option value='11' <?php echo (Input::old('title')=='11' ? 'selected':''); ?>>میلاد النبی</option>
	                                <option value='0' <?php echo (Input::old('title')=='0' ? 'selected':''); ?>>هیچکدام</option>
	                                
	                            </select>
			               	</div>
			            </div>
                	</div>
                </div>
	            </br>
				
               	<div class="container-fluid">
	            	<div class="row">
	            		<div class="col-sm-6">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">شماره حکم
	                    		</label>
	                            <input class="form-control" type="text" name="hokm_no" value="{!!Input::old('hokm_no')!!}">
	                    	</div>
			            </div>
	            	
			            <div class="col-sm-6">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">تاریخ حکم
	                    		
	                    		</label>
	                            <input class="form-control datepicker_farsi" readonly type="text" name="hokm_date" value="{!!Input::old('hokm_date')!!}">
	                            
	                    	</div>
			            </div>
	            	</div>
	            </div>
			    <div class="container-fluid" style="display:none" id="farman_div">
	            	<div class="row">
	            		<div class="col-sm-6">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">شماره فرمان
	                    		</label>
	                            <input class="form-control" type="text" name="farman_no" value="{!!Input::old('farman_no')!!}">
	                    	</div>
			            </div>
	            	
			            <div class="col-sm-6">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">تاریخ فرمان
	                    		
	                    		</label>
	                            <input class="form-control datepicker_farsi" readonly type="text" name="farman_date" value="{!!Input::old('farman_date')!!}">
	                            
	                    	</div>
			            </div>
	            	</div>
	            </div>
	        	<div class="container-fluid">
	            	<div class="row">
	            		<div class="col-sm-3">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">شماره صادره
	                    		</label>
	                            <input class="form-control" type="text" name="issued_no" value="{!!Input::old('issued_no')!!}">
	                    	</div>
			            </div>
	            	
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">تاریخ صادره</label>
	                            <input class="form-control datepicker_farsi" readonly type="text" name="issued_date" value="{!!Input::old('issued_date')!!}">
	                    	</div>
			            </div>
			            <div class="col-sm-6">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">چگونگی تفویض
			              		<span style="color:red">*</span>
		            				</label>
	                            <select name="how" class="form-control">
	                            	<option value="">یک گزینه را انتخاب کنید</option>
	                            	<option value='1' <?php echo (Input::old('how')=='1' ? 'selected':''); ?>>رئیس جمهور اسلامی افغانستان</option>
	                                <option value='2' <?php echo (Input::old('how')=='2' ? 'selected':''); ?>>وزارت/اداره مربوط</option>
	                                <option value="0" <?php echo (Input::old('how')=='0' ? 'selected':''); ?>>تفویض نشده</option>
	                            </select>
			               	</div>
			            </div>
	            	</div>
	            </div>
	            <div class="container-fluid" >
			      	<div class="row">
	            		<div id="sender_external_div">
		        			<div class="col-sm-3">
		            			<div class="col-sm-12">
		            				<label class="col-sm-12 ">مرسل الیه</label>
		            			</div>
		            			<div class="col-sm-12">
		                            <select name="ministry" id="send_to" class="form-control" style="width:100%" data-plugin="select2">
		                                <option value="">انتخاب</option>
		                            	@foreach($ministrires AS $ministry)
		                                    <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
		                                @endforeach
		                                <option value="0">هیچکدام</option>
		                            </select>
		                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		            			</div>
		            		</div>
		        		</div>
	            		
			            <div id="internal_div">
				      		<div class="col-sm-3">
		            			<div class="col-sm-12">
		            				<label class="col-sm-12 ">مرسل
		            				
		            				</label>
		            			</div>
		            			<div class="col-sm-12">
		                            <select name="general_department" class="form-control">
		                                <option value="">ریاست نظارت و ارزیابی</option>		                                
		                            </select>
		            			</div>
		            		</div>
		            		
	            		</div>
	            		
			      	</div>
			   </div>
			   
			   <div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">موضوع</label>
	                            <input class="form-control" type="text" name="subject" value="{{$doc_details->desc}}">
			               	</div>
			            </div>
			      		
			        </div>
			    </div>
			    <div class="container-fluid" >
			      	<div class="row">
	                   	<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">فایل ضمیمه</label>
	                    		
	                            <input type='file'  name='attach' class="form-control">
	                    	</div>
	                   </div>
			            <div class="col-sm-3">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
	                    	</div>
	                    	<div class="col-sm-12" id="sub_div">
		                        @if(canAdd('evaluation_receivedDocs'))
		                        	<button class="btn btn-primary" type="submit" onclick="$('#sub_div').hide();">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
		                    </div>
	                	</div>
			        </div>
			    </div>
	            <br/>
	           <input type="hidden" name="id" value="{!!$id!!}">
	           
			 	<div class="container_fluid">
			 		<div class="row">
			 			<div class="col-sm-12">
			 			@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
			 			</div>
			 		</div>
			 	</div>
                
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">
$("#general_department").select2();
$("#sub_dep").select2();
$("#send_to").select2();
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function bring_items()
{
	var type = $('#approved_type').val();
	if(type==1 || type==2)
	{
		$('#farman_div').show();
	}
	else
	{
		$('#farman_div').hide();
	}
    $.ajax({
            url: '{!!URL::route("bring_items")!!}',
            data: '&type='+type,
            type: 'post',
            beforeSend: function(){
                $("#approved_type_item").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#approved_type_item').html(response);
            }
        }
    );
}
function showServiceType(value)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#internal_div').slideUp();
        $('#external_div').slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#external_div').slideUp();
        $('#internal_div').slideDown();
    }

}
function showSenderType(value)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#sender_internal_div').slideUp();
        $('#sender_external_div').slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#sender_external_div').slideUp();
        $('#sender_internal_div').slideDown();
    }

}
function add_emp()
{
	$('#add_finding_btn').hide();
	$('#loading_div').show();
	var current_total = $('#total_emp').val();
	//var total_findings = $('#total_findings').val();
	var total = parseInt(current_total)+parseInt(1);
	//var total_findings = parseInt(total_findings)+parseInt(1);
	$('#total_emp').val(total);
	//$('#total_findings').val(total_findings);
	$.ajax({
		url:'{{URL::route("getMoreEmployees")}}',
		data: '&total='+total,
		type:'POST',
		success:function(r){
			$('#emp_1').prepend(r);
			$('#loading_div').hide();
			$('#add_finding_btn').show();
		}
	});
}
function remove_emp(no)
{
	$('#emp_'+no).remove();
}
</script>

@stop

