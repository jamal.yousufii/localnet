@extends('layouts.master')

@section('head')
	{!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
	{!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
    <title>{!!_('advanced_search')!!}</title>
@stop
@section('content')
<div class="row col-md-12" style="margin-top:-30px">
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix">
            <center>
                <h4>
                د افغانستان اسلامی جمهوریت</br>
                دجمهوری ریاست دلوړ مقام د دفتر لوی ریاست</br>
                د پالیسی ، څارنی او بررسی معاونیت</br>
                د څارنی او ارزونی ریاست
                </h4>
            </center>   
        </div>
    </div>
    <div class="col-md-2 noprint">
        {!! HTML::image('/img/logo.jpg', 'LOGO', array('width' => '130','height' => '125')) !!}
        
    </div>
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix">
           
                <center>
                <h4 style="font-family: 'B Nazanin';">
                جمهوری اسلامی افغانستان</br>
                ریاست عمومی دفتر مقام عالی ریاست جمهوری</br>
                معاونیت پالیسی,نظارت و بررسی</br>
                ریاست نظارت و ارزیابی
                </h4></center>
            
            
        </div>
    </div>
</div>
<div class="row noprint" style="margin: 10px;">
<h4 class="pull-right" style="font-weight: bold;font-size: 25px;font-family: 'B Nazanin';">گزارش عمومی</h4>
</div>
<hr class="noprint" />
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
</div>
<div class="tab-pane" id="search" role="tabpanel">

	<form class="form-horizontal" role="form" method="post" id="search_form" action="{!!URL::route('getEvaluationExcel')!!}"> 
    	<div class="row" id="all_search_div">
        	<div class="col-lg-12">
        		<div class="container-fluid">
			      	<div class="row">
			      		<div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 " style="font-weight: bold;">سال</label>
                                <select class="form-control" name="year">
                                	<option value="">همه</option>
			                        @for($i=1380;$i<=$year;$i++)
			                        <option value='{!!$i!!}' @if($i==$year) {!!'selected'!!} @endif>{!!$i!!}</option>
			                        @endfor
			                    </select>
			               	</div>
			            </div>
			      		<div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 " style="font-weight: bold;">نوعیت سند</label>
                                <select name="doc_type" id="doc_type" class="form-control">
                                    <option value='0'>همه</option>
                                    <option value='1' >مکتوب</option>
                                    <option value='2' >پیشنهاد</option>
                                    <option value='3' >هدایت</option>
                                   
                                </select>
			               	</div>
			               	
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 " style="font-weight: bold;">تاریخ سند از</label>
                                <input class="form-control datepicker_farsi" type="text" name="date_from" value="">
			               	</div>
			               	
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 " style="font-weight: bold;">تاریخ سند تا</label>
                                <input class="form-control datepicker_farsi" type="text" name="date_to" value="">
			               	</div>
			               	
			            </div> 
			            
			      		
                		
			            
					</div>
				</div>
				<div class="container-fluid">
			      	<div class="row">
			      		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 " style="font-weight: bold;">شماره سند</label>
                                <input type="text" name="doc_no" class="form-control" value=""/>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
			      		<div id="external_div_dep">
                			<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 " style="font-weight: bold;">وزارت/اداره سند</label>
	                			</div>
	                			<div class="col-sm-12">
	                                <select style="width:100%" name="ministry" id="ministry" class="form-control select2">
	                                    <option value="0">انتخاب</option>
	                                	@foreach($ministrires AS $ministry)
	                                		
	                                    	<option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
	                                    	
	                                    @endforeach
	                                </select>
	                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	                			</div>
	                		</div>
                		</div>
                		<div class="col-sm-6">
                			<div class="col-sm-12">
                				<label class="col-sm-12 " style="font-weight: bold;">موضوع</label>
                                <input type="text" name="subject" class="form-control" value=""/>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
			      		
                	</div>
                </div>
                <hr style="border-top: 1px solid #121314 !important">
			     <div class="container-fluid">
			      	<div class="row">
			      		<div class="col-sm-3">
			              	<div class="col-sm-12">
			              		<label class="col-sm-12 " style="font-weight: bold;">اتباع</lable>
                                <select name="nationality" class="form-control">
                                	<option value='0'>همه</option>
                                   	<option value='1' >افغان</option>
                                    <option value='2' >خارجی</option>
                                    
                                </select>
			               	</div>
			            </div>
			      		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 " style="font-weight: bold;">نام مکمل</label>
                                <input type="text" name="name" id="fullname" class="form-control" value=""/>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 " style="font-weight: bold;">نام پدر</label>
                                <input type="text" name="father_name" class="form-control" value=""/>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 " style="font-weight: bold;">وزارت/اداره مربوطه</label>
                			</div>
                			<div class="col-sm-12">
                                <select style="width:100%" name="emp_ministry" class="form-control select2">
                                    <option value="0">انتخاب</option>
                                	@foreach($ministrires AS $ministry)
                                		
                                    	<option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
                                    	
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
			      		<div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 " style="font-weight: bold;">نوعیت وظیفه</label>
                                <select name="employee_type" id="employee_type" class="form-control" onchange="showRank(this.value)">
                                    <option value='0'>انتخاب</option>
                                    <option value='1' selected="selected">ملکی</option>
                                    <option value='2' >نظامی</option>
                                </select>
			               	</div>
			               	
			            </div>
			            <div id="bast_div">
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 " style="font-weight: bold;">بست</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "emp_bast" id = "emp_bast" class="form-control">
	                                    <option value='0'>انتخاب</option>
	                                    {!!getBastStaticList('employee_rank')!!}
	                                </select>
				      			</div>
				      		</div>
				      	</div>
				      	
			      		<div id="military_div" style="display:none">
			      			<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 " style="font-weight: bold;">بست</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "military_bast" id = "military_bast" class="form-control">
                                        <option value='0'>همه</option>
                                        {!!getStaticDropdown('military_rank')!!}
                                    </select>
				      			</div>
				      		</div>
			      		</div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 " style="font-weight: bold;">نوعیت پیشنهاد شده</label>
                                <select name="suggested_type" id="suggested_type" class="form-control" onchange="bring_items()">
                                	<option value="0">همه</option>
                                	<option value='1' >مدال</option>
                                	<option value='2' >نشان</option>
                                    <option value='3' >تحسین نامه</option>
                                    <option value='4' >تقدیرنامه</option>
                                    
                                </select>
			               	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 " style="font-weight: bold;">مورد پیشنهاد شده</label>
                                <select name="suggested_type_item" id="suggested_type_item" class="form-control">
                                	<option value='0'>همه</option>
                                	
                                </select>
			               	</div>
			            </div>
			            
                		
                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 " style="font-weight: bold;">عنوان وظیفه</label>
                                <input type="text" name="job" class="form-control" value=""/>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
						
                		
                	</div>
                </div>
                <hr style="border-top: 1px solid #121314 !important">
			    <div class="container-fluid" id="approved_items_div">
			    	
			      	<div class="row">
                		<div class="col-sm-2">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 " style="font-weight: bold;">تاریخ حکم از</label>
                                <input class="form-control datepicker_farsi" type="text" name="hokm_date_from" value="">
			               	</div>
			               	
			            </div>
			            <div class="col-sm-2">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 " style="font-weight: bold;">تاریخ حکم تا</label>
                                <input class="form-control datepicker_farsi" type="text" name="hokm_date_to" value="">
			               	</div>
			               	
			            </div> 
			            <div class="col-sm-2">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 " style="font-weight: bold;">تاریخ فرمان از</label>
                                <input class="form-control datepicker_farsi" type="text" name="farman_date_from" value="">
			               	</div>
			               	
			            </div>
			            <div class="col-sm-2">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 " style="font-weight: bold;">تاریخ فرمان تا</label>
                                <input class="form-control datepicker_farsi" type="text" name="farman_date_to" value="">
			               	</div>
			               	
			            </div> 
			            <div class="col-sm-2">
	            			<div class="col-sm-12">
	            				<label class="col-sm-12 " style="font-weight: bold;">شماره حکم</label>
	                            <input type="text" name="hokm_no" class="form-control" value=""/>
	                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	            			</div>
	            		</div>
	            		<div class="col-sm-2">
	            			<div class="col-sm-12">
	            				<label class="col-sm-12 " style="font-weight: bold;">شماره فرمان</label>
	                            <input type="text" name="farman_no" class="form-control" value=""/>
	                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	            			</div>
	            		</div>
			        </div>
			    
			    	<div class="row">
		            	
	            		<div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 " style="font-weight: bold;">نوعیت منظور شده</label>
                                <select name="approved_type" id="approved_type" class="form-control" onchange="bring_items_approved()">
                                	<option value="0">همه</option>
                                	<option value='1'>مدال</option>
                                	<option value='2'>نشان</option>
                                    <option value='3'>تحسین نامه</option>
                                    <option value='4'>تقدیرنامه</option>
                                    
                                </select>
			               	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 " style="font-weight: bold;">مورد منظور شده</label>
                                <select name="approved_type_item" id="approved_type_item" class="form-control">
                                	<option value='0'>همه</option>
                                	
                                </select>
			               	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 " style="font-weight: bold;">مناسبت
			              		</label>
	                            <select name="title" class="form-control">
	                            	<option value="0">همه</option>
	                            	<option value='1'>نهم حوت</option>
	                                <option value='2'>هشت مارچ</option>
	                                <option value='3'>بیست و هشت اسد</option>
	                                <option value='4'>روز جهانی معلم</option>
	                                <option value='5'>روز آغاز سال تعلیمی جدید</option>
	                                <option value='6'>روز مطبوعات</option>
	                                <option value='7'>روز گرامیداشت از نزول قرآن عظیم الشان</option>
	                                <option value='8'>روز جهانی المپیک</option>
	                                <option value='9'>روز استرداد استقلال افغانستان</option>
	                                
	                                <option value='10'>روز هنر</option>
	                                <option value='11'>میلاد النبی</option>
	                                
	 
	                            </select>
			               	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 " style="font-weight: bold;">چگونگی تفویض
		            			</label>
	                            <select name="how" class="form-control" required>
	                            	<option value="">همه</option>
	                            	<option value='1'>رئیس جمهور اسلامی افغانستان</option>
	                                <option value='2'>وزارت/اداره مربوط</option>
	                                <option value='0'>تفویض نشده</option>
	                            </select>
			               	</div>
			            </div>
			        </div>
			        
	            	<div class="row">
	                	<div id="external_div_elay">
                			<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 " style="font-weight: bold;">مرسل الیه</label>
	                			</div>
	                			<div class="col-sm-12">
	                                <select style="width:100%" name="elay_ministry" id="elay_ministry" class="form-control select2">
	                                    <option value="0">انتخاب</option>
	                                	@foreach($ministrires AS $ministry)
	                                		
	                                    	<option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
	                                    	
	                                    @endforeach
	                                </select>
	                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	                			</div>
	                		</div>
                		</div>
                		<div class="col-sm-3">
	            			<div class="col-sm-12">
	            				<label class="col-sm-12 " style="font-weight: bold;">شماره صادره</label>
	                            <input type="text" name="sent_no" class="form-control" value=""/>
	                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	            			</div>
	            		</div>
	            		<div class="col-sm-6">
                			<label class="col-sm-12 ">&nbsp;</label>
                                <a href="javascript:void()" onclick="$('#search_form').submit();" class="btn btn-warning ">
									پرنت اکسل
								</a>
                                <button class="btn btn-primary" type="button" onclick="do_search(0)"> جستجو</button>
                                <button class="btn btn-success pull-right" type="reset"> پاک نمودن فروم</button>
                			
                		</div>
	            	</div>
	            	
	            </div>
			    <div class="container-fluid">
			      	<div class="row">
			      		
			    		
                		<div class="col-sm-2 pull-right">
                			<div class="col-sm-12" id="loader_div" style="display:none">
                				<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>
                			</div>
                			
                		</div>
			    	</div>
			    </div>
			</div>
		</div>
    </form>
    <hr>
	
    <div class="row" id="search_result">
	</div>
</div>
@stop
@section('footer-scripts')
<script>
$(".select2").select2();


function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function showServiceType(value,div)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#internal_div_'+div).slideUp();
        $('#external_div_'+div).slideDown();

    }
    else if(value == 1)
    {
        //$('#ageer_div').slideUp();
        $('#external_div_'+div).slideUp();
        $('#internal_div_'+div).slideDown();
    }
    else
    {
    	$('#external_div_'+div).slideUp();
        $('#internal_div_'+div).slideUp();
    }

}
function showRank(value)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#bast_div').slideUp();
        $('#military_div').slideDown();

    }
    else if(value == 1)
    {
        //$('#ageer_div').slideUp();
        $('#military_div').slideUp();
        $('#bast_div').slideDown();
    }
    else
    {
    	$('#military_div').slideUp();
        $('#bast_div').slideUp();
    }

}
function bring_items()
{
	var type = $('#suggested_type').val();
	
    $.ajax({
            url: '{!!URL::route("bring_items")!!}',
            data: '&type='+type,
            type: 'post',
            beforeSend: function(){
                $("#suggested_type_item").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#suggested_type_item').html(response);
            }
        }
    );
}
function bring_items_approved()
{
	var type = $('#approved_type').val();
	
    $.ajax({
            url: '{!!URL::route("bring_items")!!}',
            data: '&type='+type,
            type: 'post',
            beforeSend: function(){
                $("#approved_type_item").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#approved_type_item').html(response);
            }
        }
    );
}
function bring_approved(type)
{
	if(type == 1)
	{
		$('#approved_items_div').slideDown();
	}
	else
	{
		$('#approved_items_div').slideUp();
	}
}
function do_search(pageno)
{
	$("#loader_div").show();
	//$("#submit_div").hide();
	
    var page = "{!!URL::route('getEvaluationSearch')!!}";
    
    $.ajax({
        url: page,
        type: 'post',
        data: $('#search_form').serialize()+'&pageno='+pageno,
        dataType:'HTML',
        success: function(response)
      	{
      		//$('#all_search_div').slideUp();
      		//$('#search_result').slideDown();
      		$("#loader_div").hide();
        	$('#search_result').html(response);
        }
    });   
}
function do_search_ajax(pageno)
{
	$('#result_ajax').html('Loading...');
	$("#submit_div").hide();
	
    var page = "{!!URL::route('getEvaluationSearch_ajax')!!}";
    
    $.ajax({
        url: page,
        type: 'post',
        data: $('#search_form').serialize()+'&pageno='+pageno,
        dataType:'HTML',
        success: function(response)
      	{
      		//$('#all_search_div').slideUp();
      		//$('#search_result').slideDown();
      		//$("#loader_div").hide();
        	$('#result_ajax').html(response);
        }
    });   
}
</script>
@stop