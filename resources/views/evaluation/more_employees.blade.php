<div id="emp_{!!$total!!}">
	<div class="row">
		<div class="col-sm-3">
    	<div class="col-sm-12">
    		<label class="col-sm-2 ">اتباع
    		</label>
            <select name="nationality_{!!$total!!}" required class="form-control">
               	<option value='1'>افغان</option>
                <option value='2'>خارجی</option>
                
            </select>
       	</div>
       	
    </div>
		<div class="col-sm-3">
    	<div class="col-sm-12">
    		<label class="col-sm-12 ">نام مکمل
    		
    		</label>
            <input class="form-control" type="text" name="name_{!!$total!!}" value="">
            
    	</div>
    </div>
    <div class="col-sm-3">
    	<div class="col-sm-12">
    		<label class="col-sm-12 ">نام پدر
    		
    		</label>
            <input class="form-control" type="text" name="f_name_{!!$total!!}" value="">
            
    	</div>
    </div>
    <div class="col-sm-3">
    	<div class="col-sm-12">
      		<label class="col-sm-12 ">نوعیت وظیفه</label>
            <select name="job_{!!$total!!}" class="form-control" onchange="showRank(this.value)">
            	<option value="">یک گزینه را انتخاب کنید</option>
            	<option value='1'>ملکی</option>
                <option value='2'>نظامی</option>
                
            </select>
       	</div>
    </div>
	</div>
	<div class="row">
		<div id="bast_div">
  		<div class="col-sm-3">
  			<div class="col-sm-12">
  				<label class="col-sm-12 ">بست</label>
                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
                <select name = "emp_bast_{!!$total!!}" class="form-control">
                    <option value=''>انتخاب</option>
                    {!!getBastStaticList('employee_rank')!!}
                </select>
  			</div>
  		</div>
  		<div class="col-sm-3">
  			<div class="col-sm-12">
  				<label class="col-sm-12 ">رتبه</label>
                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
                <select name = "emp_rank_{!!$total!!}" id="emp_rank" class="form-control">
                    <option value=''>انتخاب</option>
                    {!!getStaticDropdown('employee_rank')!!}
                </select>
  			</div>
  		</div>
  	</div>
	      	
		<div id="military_div" style="display:none;">
			<div class="col-sm-3">
    			<div class="col-sm-12">
    				<label class="col-sm-12 ">بست نظامی</label>
                  <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
                  <select name = "military_bast_{!!$total!!}" class="form-control">
                      <option value=''>انتخاب</option>
                      {!!getStaticDropdown('military_rank',Input::old('emp_bast'))!!}
                  </select>
    			</div>
    		</div>
    		<div class="col-sm-3">
    			<div class="col-sm-12">
    				<label class="col-sm-12 ">رتبه</label>
                  <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
                  <select name = "military_rank_{!!$total!!}" id="military_rank" class="form-control">
                      <option value=''>انتخاب</option>
                      {!!getStaticDropdown('military_rank')!!}
                  </select>
    			</div>
    		</div>
		</div>
  	<div class="col-sm-3">
      <div class="col-sm-12">
        <label class="col-sm-12 ">عنوان وظیفه
        
        </label>
            <input class="form-control" type="text" name="job_title_{!!$total!!}" value="">
            
      </div>
    </div>	
    <div class="col-sm-3">
      <div class="col-sm-12">
        <label class="col-sm-12 ">وزارت/اداره مربوط</label>
      </div>
      <div class="col-sm-12">
                <select style="width:100%" name="emp_ministry_{!!$total!!}" class="form-control">
                    <option value="">انتخاب</option>
                  @foreach($ministrires AS $ministry)
                        <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
                    @endforeach
                </select>
      </div>
    </div>
  </div>
  <div class="row">
  		<div class="col-sm-3">
      	<div class="col-sm-12">
        		<label class="col-sm-12 ">نوعیت مکافات پیشنهاد شده</label>
              <select name="suggested_type_{!!$total!!}" id="suggested_type_{!!$total!!}" class="form-control" onchange="bring_items({!!$total!!})">
              	<option value="">یک گزینه را انتخاب کنید</option>
              	<option value='1'>مدال</option>
              	<option value='2'>نشان</option>
                  <option value='3'>تحسین نامه</option>
                  <option value='4'>تقدیرنامه</option>
                  
              </select>
         	</div>
      </div>
      <div class="col-sm-3">
      	<div class="col-sm-12">
        		<label class="col-sm-12 ">مورد مکافات پیشنهاد شده</label>
              <select name="suggested_type_item_{!!$total!!}" id="suggested_type_item_{!!$total!!}" class="form-control">
              	<option value="">یک گزینه را انتخاب کنید</option>
              </select>
         	</div>
      </div>
      <div class="col-sm-1">
      	<div class="col-sm-12">
      		<label class="col-sm-12 ">&nbsp;</label>
      		<button class="btn btn-danger" onclick="remove_emp({!!$total!!})" type="button"> - </button>
      	</div>
      </div>
    </div>
    <hr/>
</div>
