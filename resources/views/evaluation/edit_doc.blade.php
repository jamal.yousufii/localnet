@extends('layouts.master')

@section('head')
    <title>{!!_('edit_document')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
    <style>
    p{
    	font-style: oblique;display:inline;font-size: 14px;font-family: Roboto,sans-serif;
		font-weight: 300;
		line-height: 1.2;
		color: #37474f;
    	text-shadow: rgba(0,0,0,.15) 0 0 1px;	
    }
    label{
    	font-weight: bold;
    }
    </style>
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAllReceivedDocs')!!}">{!!_('evaluation')!!}</a></li>
    <li class="active"><span>{!!_('edit_document')!!}</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postEditDoc')!!}" enctype="multipart/form-data">
			    <div class="panel-heading">
			      <h5 class="panel-title">ویرایش سند</h5>
			    </div>
                <div class="container-fluid">
                	<div class="row">
                		
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 ">نوعیت سند<span style="color:red">*</span>
			            		</label>
                                <select name="doc_type" required class="form-control">
                                    <option value=''>یک گزینه را انتخاب کنید</option>
                                    <option value='1' @if($details->doc_type==1) selected @endif>مکتوب</option>
                                    <option value='2' @if($details->doc_type==2) selected @endif>پیشنهاد</option>
                                    <option value='3' @if($details->doc_type==3) selected @endif>هدایت</option>
                                    
                                </select>
			               	</div>
			               	
			            </div>
            			<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">وزارت/اداره<span style="color:red">*</span></label>
                			</div>
                			<div class="col-sm-12">
                                <select style="width:100%" name="ministry" id="ministry" class="form-control">
                                    <option value="">انتخاب</option>
                                	@foreach($ministrires AS $ministry)
                                		@if($details->ministry==$ministry->id)
                                        <option value='{!!$ministry->id!!}' selected>{!!$ministry->name_dr!!}</option>
                                        @else
                                        <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                			</div>
                		</div>
                		
                		<div class="col-sm-2">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">شماره سند
                        		</label>
                                <input class="form-control" type="text" name="doc_no" value="{!!$details->doc_no!!}">
                        	</div>
			            </div>
                	
			            <div class="col-sm-2">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ سند
                        		
                        		</label>
                        		<?php $sdate = $details->doc_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                                <input class="form-control datepicker_farsi" readonly type="text" name="date" value='<?php if($details->doc_date !=null){echo jalali_format($sdate);}?>'>
                                <span style="color:red">{!!$errors->first('date')!!}
                                @if($details->doc_date==null)
                                	{!!$details->doc_date_string!!}
                                @endif
                                </span>
                        	</div>
			            </div>
			            <div class="col-sm-2">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">سال
                        		
                        		</label>
                                <select class="form-control" name="year" required>
                                	
			                        @for($i=1380;$i<=$year;$i++)
			                        <option value='{!!$i!!}' @if($i==$details->year) {!!'selected'!!} @endif>{!!$i!!}</option>
			                        @endfor
			                    </select>
                        	</div>
			            </div>
                	</div>
                </div>
	            <div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
	                		<div class="col-sm-12">
	                			<label class="col-sm-12 ">موضوع</label>
                                <input class="form-control" type="text" name="desc" value="{!!$details->desc!!}">
                            </div>
                        </div>
	                	@if($details->file_name!=null)
			      			<div class="col-sm-6" id="attached_file_div">
		                		<div class="col-sm-12">
		                    		<label class="col-sm-12 ">&nbsp;</label>
		                    		<a href="{!!URL::route('downloadDocument',$details->id)!!}">{!!$details->file_name!!}</a>
		                    		@if(canDelete('evaluation_receivedDocs'))
		                    		<a href="javascript:void()" onclick="removeReportFile('{!!$details->id!!}');" class="table-link danger">
	                                    <i class="fa fa-trash-o" style='color:red;'></i>
	                                </a>	
	                                @endif
		                    	</div>
		                   </div>
			      		
	                   @endif
	                   <div class="col-sm-6" @if($details->file_name!=null) style="display:none" @endif id="attach_div">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">فایل ضمیمه</label>
	                    		
	                            <input type='file'  name='attach' class="form-control">
	                    	</div>
	                   </div>
	               </div>
	           </div>
	           <hr style="border-top: 1px solid #121314 !important">
	           <div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
				           <div class="panel-heading">
						      <h5 class="panel-title">مشخصات ذوات پیشنهاد شده برای مکافات</h5>
						    </div>
						</div>
					    <div class="col-sm-6">
		                	<div class="col-sm-1" style="display:none;float:right" id="loading_div">
							    <label class="col-sm-12">&nbsp;</label>
							    <button class="btn btn-primary" style="float:right" type="button">LOADING</button>
						    </div>
			            	<div class="col-sm-11" id="add_finding_btn">
				            	<label class="col-sm-12">&nbsp;</label>
						        <button class="btn btn-primary" style="float:right" onclick="add_emp()" type="button"> + </button>
						    </div>
						    
			            </div>
			        </div>
			   </div>
			   <div class="container-fluid" id="emp_div"></div>
               <input type="hidden" id="total_emp" name="total_emp" value="0"/>
			   @if($emps)
			   @foreach($emps AS $emp)
			   
			   <div class="container-fluid">
                	<div class="row">
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-2 ">اتباع
			            		</label>
                                <select id="nationality_{!!$emp->id!!}" required class="form-control">
                                   	<option value='1' <?php echo ($emp->nationality=='1' ? 'selected':''); ?>>افغان</option>
                                    <option value='2' <?php echo ($emp->nationality=='2' ? 'selected':''); ?>>خارجی</option>
                                    
                                </select>
			               	</div>
			               	
			            </div>
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">نام مکمل
                        		
                        		</label>
                                <input class="form-control" type="text" id="name_{!!$emp->id!!}" value="{!!$emp->name!!}">
                                
                        	</div>
			            </div>
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">نام پدر
                        		
                        		</label>
                                <input class="form-control" type="text" id="f_name_{!!$emp->id!!}" value="{!!$emp->f_name!!}">
                                
                        	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">نوعیت وظیفه</label>
                                <select id="job_{!!$emp->id!!}" class="form-control" onchange="showRank(this.value,{!!$emp->id!!})">
                                	<option value="">{!!_('select')!!}</option>
                                	<option value='1' <?php echo ($emp->emp_type=='1' ? 'selected':''); ?>>ملکی</option>
                                    <option value='2' <?php echo ($emp->emp_type=='2' ? 'selected':''); ?>>نظامی</option>
                                    
                                </select>
			               	</div>
			            </div>
			            
                	</div>
                	<div class="row">
                		<div id="bast_div_{!!$emp->id!!}" @if($emp->emp_type==2) style="display:none;" @endif>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست</label>
				                    <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
				                    <select id = "emp_bast_{!!$emp->id!!}" class="form-control">
				                        <option value=''>انتخاب</option>
				                        {!!getBastStaticList('employee_rank',$emp->emp_bast)!!}
				                    </select>
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">رتبه</label>
				                    <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
				                    <select id = "emp_rank_{!!$emp->id!!}" class="form-control">
				                        <option value=''>انتخاب</option>
				                        {!!getStaticDropdown('employee_rank',$emp->emp_rank)!!}
				                    </select>
				      			</div>
				      		</div>
				      	</div>
					      	
				  		<div id="military_div_{!!$emp->id!!}" @if($emp->emp_type==1) style="display:none;" @endif>
				  			<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست نظامی</label>
				                    <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
				                    <select id = "military_bast_{!!$emp->id!!}" class="form-control">
				                        <option value=''>انتخاب</option>
				                        {!!getStaticDropdown('military_rank',$emp->military_bast)!!}
				                    </select>
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">رتبه</label>
				                    <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
				                    <select id = "military_rank_{!!$emp->id!!}" class="form-control">
				                        <option value=''>انتخاب</option>
				                        {!!getStaticDropdown('military_rank',$emp->military_rank)!!}
				                    </select>
				      			</div>
				      		</div>
				  		</div>
				  		<div class="col-sm-3">
				        	<div class="col-sm-12">
				        		<label class="col-sm-12 ">عنوان وظیفه
				        		
				        		</label>
				                <input class="form-control" type="text" id="job_title_{!!$emp->id!!}" value="{!!$emp->job_title!!}">
				                
				        	</div>
				        </div>
				        <div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">وزارت/اداره مربوطه</label>
                			</div>
                			<div class="col-sm-12">
                                <select style="width:100%" id="emp_ministry_{!!$emp->id!!}" class="form-control">
                                    <option value="">انتخاب</option>
                                	@foreach($ministrires AS $ministry)
                                		@if($emp->ministry==$ministry->id)
                                        <option value='{!!$ministry->id!!}' selected>{!!$ministry->name_dr!!}</option>
                                        @else
                                        <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                			</div>
                		</div>
                	</div>
                	<div class="row">
                		<div class="col-sm-3">
				        	<div class="col-sm-12">
				          		<label class="col-sm-12 ">نوعیت مکافات پیشنهاد شده</label>
				                <select id="suggested_type_{!!$emp->id!!}" class="form-control" onchange="bring_items({!!$emp->id!!})">
				                	<option value="">یک گزینه را انتخاب کنید</option>
				                	<option <?php echo ($emp->suggested_type=='1' ? 'selected':''); ?> value='1'>مدال</option>
				                	<option <?php echo ($emp->suggested_type=='2' ? 'selected':''); ?> value='2'>نشان</option>
				                    <option <?php echo ($emp->suggested_type=='3' ? 'selected':''); ?> value='3'>تحسین نامه</option>
				                    <option <?php echo ($emp->suggested_type=='4' ? 'selected':''); ?> value='4'>تقدیرنامه</option>
				                    
				                </select>
				           	</div>
				        </div>
			      		<div class="col-sm-3">
				        	<div class="col-sm-12">
				          		<label class="col-sm-12 ">مورد مکافات</label>
				                <select id="suggested_type_item_{!!$emp->id!!}" class="form-control">
				                	<option value="">یک گزینه را انتخاب کنید</option>
				                <?php $subitems = getItems($emp->suggested_type);?>
		              			@if($subitems)
                            		@foreach($subitems AS $sub)
                            			@if($sub->id == $emp->suggested_type_item)
                            			<option value="{!!$sub->id!!}" selected="selected">{!!$sub->name_dr!!}</option>
                            			@else
                            			<option value="{!!$sub->id!!}">{!!$sub->name_dr!!}</option>
                            			@endif
                            		@endforeach
                            	@endif
				                	
				                </select>
				           	</div>
				        </div>
				        <div class="col-sm-2" style="display:none;float:right" id="emp_loading_div">
						    <label class="col-sm-12">&nbsp;</label>
						    <button class="btn btn-success" type="button">Updating</button>
					    </div>
		            	<div class="col-sm-2" id="edit_emp_btn">
			            	<label class="col-sm-12">&nbsp;</label>
					        <button class="btn btn-success" onclick="update_emp({!!$emp->id!!})" type="button">ثبت تغییرات کارمند</button>
					    </div>
			            
				        
                	</div>
                	<hr/>
                </div>
                @endforeach
                @endif
			  
               
               
               <div class="container-fluid">
                	<div class="row">
               			<div class="col-sm-3">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12">&nbsp;</label>
		                        @if(canAdd('evaluation_receivedDocs'))
		                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">برگشت</button>
		                    </div>
                    	</div>
               		</div>
               </div>
               
               <input type="hidden" name="id" value="{!!$id!!}"/>
	           
			 	<div class="container_fluid">
			 		<div class="row">
			 			<div class="col-sm-12">
			 			@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
			 			</div>
			 		</div>
			 	</div>
                
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">
$("#general_department").select2();
$("#sub_dep").select2();
$("#ministry").select2();
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function bring_items(no)
{
	var type = $('#suggested_type_'+no).val();
	
    $.ajax({
            url: '{!!URL::route("bring_items")!!}',
            data: '&type='+type,
            type: 'post',
            beforeSend: function(){
                $("#suggested_type_item_"+no).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#suggested_type_item_'+no).html(response);
            }
        }
    );
}
function showServiceType(value)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#internal_div').slideUp();
        $('#external_div').slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#external_div').slideUp();
        $('#internal_div').slideDown();
    }

}
function showRank(value,no)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#bast_div_'+no).slideUp();
        $('#military_div_'+no).slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#military_div_'+no).slideUp();
        $('#bast_div_'+no).slideDown();
    }

}
function add_emp()
{
	$('#add_finding_btn').hide();
	$('#loading_div').show();
	var current_total = $('#total_emp').val();
	//var total_findings = $('#total_findings').val();
	var total = parseInt(current_total)+parseInt(1);
	//var total_findings = parseInt(total_findings)+parseInt(1);
	$('#total_emp').val(total);
	//$('#total_findings').val(total_findings);
	$.ajax({
		url:'{{URL::route("getMoreEmployees")}}',
		data: '&total='+total,
		type:'POST',
		success:function(r){
			$('#emp_div').prepend(r);
			$('#loading_div').hide();
			$('#add_finding_btn').show();
		}
	});
}
function update_emp(id)
{
	$('#edit_emp_btn').hide();
	$('#emp_loading_div').show();
	
	var nationality = $('#nationality_'+id).val();
	var name = $('#name_'+id).val();
	var f_name = $('#f_name_'+id).val();
	var job = $('#job_'+id).val();
	var job_title = $('#job_title_'+id).val();
	var suggested_type = $('#suggested_type_'+id).val();
	var suggested_type_item = $('#suggested_type_item_'+id).val();
	var emp_bast = $('#emp_bast_'+id).val();
	var emp_rank = $('#emp_rank_'+id).val();
	var military_bast = $('#militray_bast_'+id).val();
	var military_rank = $('#military_rank_'+id).val();
	var emp_ministry = $('#emp_ministry_'+id).val();

	$.ajax({
		url:'{{URL::route("postEditEmployee")}}',
		data: '&nationality='+nationality+'&name='+name+'&f_name='+f_name+'&job='+job+'&job_title='+job_title+'&suggested_type='+suggested_type+'&suggested_type_item='+suggested_type_item+'&emp_bast='+emp_bast+'&emp_rank='+emp_rank+'&military_bast='+military_bast+'&military_rank='+military_rank+'&id='+id+'&emp_ministry='+emp_ministry,
		type:'POST',
		success:function(r){
			$('#emp_div').prepend(r);
			$('#emp_loading_div').hide();
			$('#edit_emp_btn').show();
			alert('Updated');
		}
	});
}
function remove_emp(no)
{
	$('#emp_'+no).remove();
}
function removeReportFile(doc_id)
{
    var confirmed = confirm("Do you want to remove this file?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeFile")!!}',
                data: '&doc_id='+doc_id+'&type=received_docs',
                type: 'post',
                beforeSend: function(){
                    $("#errors").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#errors').html(response);
                    $('#attached_file_div').remove();
                    $('#attach_div').show();
                }
            }
        );
    }

}
</script>

@stop

