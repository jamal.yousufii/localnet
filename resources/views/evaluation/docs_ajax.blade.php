<div class="row">
	<div class="col-lg-12">
		<div class="modal-body" id="result_div">
			<div class="example-wrap">
		        <div class="example table-responsive">
		            <table class="table">
		                <thead>
		                    <tr>
				                <th>شماره</th>
				                <th>نوعیت سند</th>
				                <th>اداره مربوطه</th>
				                
				                
				                <th>موضوع</th>
				                <th>شماره سند</th>
				                <th>تاریخ سند</th>
				                <th>عملیه</th>
				                
				            </tr>
		                </thead>
		                <tbody>
		                    @foreach($rows AS $row)		         
		                    <tr>
		                    		<td>{!!$row->id!!}</td>
		                    		<td>{!!$row->doc_type!!}</td>
		                    		<td>{!!$row->dep!!}</td>
		                    		<td>{!!$row->desc!!}</td>
		                    		<td>{!!$row->doc_no!!}</td>
		                    		<td>{!!$row->doc_date!!}</td>
		                    		
		                    		<td>		            
		                    			@if(canEdit('evaluation_receivedDocs'))		         
		                    				<a href="{!!route('editDoc',$row->id)!!}" title="ویرایش" target="_blank"><i class="fa fa-edit"></i></a> | 
		                    			@endif
		                    			@if(canDelete('evaluation_receivedDocs'))
		                    				<a href="{!!route('postDocDelete',$row->id)!!}" onclick="javascript:return confirm(\'Are you sure ???\')" title="حذف"><i class="fa fa-trash-o"></i></a> |
		                    			@endif
		                    			<a href="{!!route('viewDoc',$row->id)!!}" title="نمایش"><i class="fa fa-eye"></i></a>
		                    		</td>
		                    <tr>
		                    	
		                    @endforeach
		                </tbody>
		            </table>
		            <div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
						{!!$rows->render()!!}
					</div>
		        </div>
		    </div>
		</div>
	</div>
</div>


<script>
	$( document ).ready(function() {
		$('.pagination a').on('click', function(event) {
			event.preventDefault();
			if ($(this).attr('href') != '#') {
				//$('#ajaxContent').load($(this).attr('href'));
				
				$.ajax({
		                url: '{!!URL::route("search_docs")!!}',
		                data: $('#rec_docs').serialize()+"&page="+$(this).text()+"&ajax=1",
		                type: 'post',
		                beforeSend: function(){
		
		                    //$("body").show().css({"opacity": "0.5"});
		                    $('#all').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
		                },
		                success: function(response)
		                {
		
		                    $('#all').html(response);
		                }
		            }
		        );
			
			}
		});
	});
	
</script>

