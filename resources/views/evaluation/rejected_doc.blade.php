@extends('layouts.master')

@section('head')
    <title>{!!_('edit_rejected_employee')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAllDocEmployees')!!}">{!!_('evaluation')!!}</a></li>
    <li class="active"><span>{!!_('reject_form')!!}</span></li>
</ol>
<div class="row">
    <div class="col-lg-12">
        <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postRejectedDoc')!!}" enctype="multipart/form-data">
		    <div class="panel-heading">
		      <h5 class="panel-title">{!!_('جزییات')!!}</h5>
		    </div>
     
            <div class="container-fluid">
            	<div class="row">
            		<div class="col-sm-3">
		            	<div class="col-sm-12">
                    		<label class="col-sm-12 ">شماره صادره
                    		<span style="color:red">*</span>
                    		</label>
                            <input class="form-control" required type="text" name="issued_no" value="{!!$details->issued_no!!}">
                    	</div>
		            </div>
            	
		            <div class="col-sm-3">
		            	<div class="col-sm-12">
                    		<label class="col-sm-12 ">تاریخ صادره</label>
                    		<?php $sdate = $details->issued_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                            <input class="form-control datepicker_farsi" readonly type="text" name="issued_date" value='<?php if($details->issued_date !=null){echo jalali_format($sdate);}?>'>
                            <span style="color:red">{!!$errors->first('hokm_date')!!}
                                @if($details->issued_date==null)
                                	{!!$details->issued_date_string!!}
                                @endif
                                </span>
                    	</div>
		            </div>
		            <div class="col-sm-3">
            			<div class="col-sm-12">
            				<label class="col-sm-12 ">مرسل الیه</label>
            			</div>
            			<div class="col-sm-12">
                            <select name="ministry" id="send_to" required class="form-control" style="width:100%" data-plugin="select2">
                                <option value="">انتخاب</option>
                            	@if($ministrires)
	                            	@foreach($ministrires AS $ministry)
	                                    @if($ministry->id == $details->ministry)
	                                    <option value='{!!$ministry->id!!}' selected>{!!$ministry->name_dr!!}</option>
	                                    @else
	                                    <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
	                                    @endif
	                                @endforeach
	                            
	                            @endif
                            </select>
                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
            			</div>
            		</div>
            		<div class="col-sm-3">
            			<div class="col-sm-12">
            				<label class="col-sm-12 ">مرسل</label>
            			</div>
            			<div class="col-sm-12">
                            <select class="form-control">
                                <option value="">ریاست نظارت و ارزیابی</option>
                            </select>
                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
            			</div>
            		</div>
            	</div>
            </div>
            
		    <div class="container-fluid" >
		      	<div class="row">
		      		<div class="col-sm-6">
		            	<div class="col-sm-12">
		              		<label class="col-sm-12 ">موضوع</label>
                            <input class="form-control" type="text" name="subject" value="{!!$details->subject!!}">
		               	</div>
		            </div>
                   	@if($details->file_name!=null)
		            <div class="col-sm-6" id="attached_file_div">
                		<div class="col-sm-12">
                    		<label class="col-sm-12 ">&nbsp;</label>
                    		<a href="{!!URL::route('downloadRejectedDoc',$details->id)!!}">{!!$details->file_name!!}</a>
                    		@if(canDelete('evaluation_receivedDocs'))
                    		<a href="javascript:void()" onclick="removeReportFile('{!!$details->id!!}');" class="table-link danger">
                                <i class="fa fa-trash-o" style='color:red;'></i>
                            </a>	
                            @endif
                    	</div>
                   </div>
                   @endif
               		<div class="col-sm-6" @if($details->file_name!=null) style="display:none" @endif id="attach_div">
	            		<div class="col-sm-12">
	                		<label class="col-sm-12 ">فایل ضمیمه</label>
	                        <input type='file'  name='attach' class="form-control">
	                	</div>
               		</div>
		            
		        </div>
		    </div>
		   <br/>
           <input type="hidden" name="id" value="{!!$details->id!!}">
           <div class="container-fluid" >
		      	<div class="row">
                   <div class="col-sm-6">
                		<div class="col-sm-12">
                    		<label class="col-sm-2 ">&nbsp;</label>
	                        @if(canEdit('evaluation_receivedDocs'))
	                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
	                    	@else
	                    		<p>You dont have Edit permission</p>
	                    	@endif
	                        <button onclick="history.back()" class="btn btn-danger" type="button">برگشت</button>
	                    </div>
                	</div>
                	@if(canEdit('evaluation_receivedDocs'))
                	<div class="col-sm-6">
                		<button onclick="undo_rejection()" class="btn btn-danger" type="button">کنسل نمودن</button>
                	</div>
                	@endif
		      	</div>
		    </div>
		 	<div class="container_fluid">
		 		<div class="row">
		 			<div class="col-sm-12">
		 			@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
		 			</div>
		 		</div>
		 	</div>
            
            {!! Form::token() !!}
        </form>
    </div>
</div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script>
$("#general_department").select2();
$("#sub_dep").select2();
$("#send_to").select2();
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}

function undo_rejection()
{
	var id = {!!$details->id!!};
	var emp_id = {!!$details->emp_id!!};
	
	var page = "{!!URL::route('unRejectDoc')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id+'&emp_id='+emp_id,
        success: function(r){
        	location.href="{!!URL::route('getAllDocEmployees')!!}";
        }
    });
}
function removeReportFile(doc_id)
{
    var confirmed = confirm("Do you want to remove this file?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeFile")!!}',
                data: '&doc_id='+doc_id+'&type=rejected_employees',
                type: 'post',
                beforeSend: function(){
                    $("#errors").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#errors').html(response);
                    $('#attached_file_div').remove();
                    $('#attach_div').show();
                }
            }
        );
    }

}
function showServiceType(value)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#internal_div').slideUp();
        $('#external_div').slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#external_div').slideUp();
        $('#internal_div').slideDown();
    }

}
</script>
@stop