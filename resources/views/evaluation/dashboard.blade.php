@extends('layouts.master')

@section('head')
	<title>Dashboard</title>
	
@stop

@section('content')
<div class="row col-md-12" style="margin-top:-30px">
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix">
            <center>
                <h4>
                د افغانستان اسلامی جمهوریت</br>
                دجمهوری ریاست دلوړ مقام د دفتر لوی ریاست</br>
                د پالیسی ، څارنی او بررسی معاونیت</br>
                د څارنی او ارزونی ریاست
                </h4>
            </center>   
        </div>
    </div>
    <div class="col-md-2 noprint">
        {!! HTML::image('/img/logo.jpg', 'LOGO', array('width' => '130','height' => '125')) !!}
        
    </div>
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix">
           
                <center>
                <h4 style="font-family: 'B Nazanin';">
                جمهوری اسلامی افغانستان</br>
                ریاست عمومی دفتر مقام عالی ریاست جمهوری</br>
                معاونیت پالیسی,نظارت و بررسی</br>
                ریاست نظارت و ارزیابی
                </h4></center>
            
            
        </div>
    </div>
</div>
<div class="row noprint" style="margin: 10px;">
<h4 class="pull-right"><span style="font-weight: bold;font-size: 25px;font-family: 'B Nazanin';">گراف احصائیه</span></h4>
<select class="form-control pull-left" style="width: 125px;margin-right: 65px;" id="year" onchange="bring_data(this.value)">
    @for($i=1380;$i<=$sh_year;$i++)
    <option value='{!!$i!!}' @if($i==$year) {!!'selected'!!} @endif>{!!$i!!}</option>
    @endfor
</select>
</div>
<div class="row">
	 
	<div class="col-md-12">
        <div id="chart1" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
        <div id="chart2" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
		<div id="chart4" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
		<div id="chart3" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
	</div>
	
</div>
@stop
@section('footer-scripts')

{!! HTML::script('/js/highcharts/highcharts.js') !!}
{!! HTML::script('/js/highcharts/highcharts-3d.js') !!}
{!! HTML::script('/js/highcharts/exporting.js') !!}
<script>
function bring_data(id)
{
	window.location.href = "/evaluation/evaluationHomePage/"+id;
}
<?php
    $last = count($medals);
    $i = 0;
    $dg='';
    foreach($medals as $a)
    {
        $i++;
        if($a->type_id!='')
        {
        	$dg.= '{name:"'.$medals_items[$a->type_id].'", y:'.$a->total.'}';
        }
        else
        {
        	$dg.= '{name:"NA", y:'.$a->total.'}';
        }
        if($i < $last)
        {
            $dg.= ',';
        }
    }
    
?>
$(function () {
    // Create the chart
    $('#chart1').highcharts({
        chart: {
            type: 'column',
            
        },
        title: {
            text: '<span style="font-weight: bold;font-size: 20px;">مدال ها</span>'
        },
        // subtitle: {
        //     text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'
        // },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
            	innerSize: 50,
                depth: 0,
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            pointFormat: '{series.name}: <b>% {point.percentage:.1f}</b>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [<?=$dg?>]
        }]
    });
});
<?php
    $last = count($signs);
    $i = 0;
    $dg='';
    foreach($signs as $a)
    {
        $i++;
        if($a->type_id!='')
        {
        	$dg.= '{name:"'.$signs_items[$a->type_id].'", y:'.$a->total.'}';
        }
        else
        {
        	$dg.= '{name:"NA", y:'.$a->total.'}';
        }
        if($i < $last)
        {
            $dg.= ',';
        }
    }
    
?>
$(function () {
    // Create the chart
    $('#chart2').highcharts({
        chart: {
            type: 'column',
            
        },
        title: {
            text: '<span style="font-weight: bold;font-size: 20px;">نشان ها</span>'
        },
        // subtitle: {
        //     text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'
        // },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
            	innerSize: 50,
                depth: 0,
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            pointFormat: '{series.name}: <b>% {point.percentage:.1f}</b>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [<?=$dg?>]
        }]
    });
});
<?php
    $last = count($tahsins);
    $i = 0;
    $dg='';
    foreach($tahsins as $a)
    {
        $i++;
        if($a->type_id!='')
        {
        	$dg.= '{name:"'.$tahsins_items[$a->type_id].'", y:'.$a->total.'}';
        }
        else
        {
        	$dg.= '{name:"NA", y:'.$a->total.'}';
        }
        if($i < $last)
        {
            $dg.= ',';
        }
    }
    
?>
$(function () {
    // Create the chart
    $('#chart3').highcharts({
        chart: {
            type: 'column',
            
        },
        title: {
            text: '<span style="font-weight: bold;font-size: 20px;">تحسین نامه ها</span>'
        },
        // subtitle: {
        //     text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'
        // },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
            	innerSize: 50,
                depth: 0,
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            pointFormat: '{series.name}: <b>% {point.percentage:.1f}</b>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [<?=$dg?>]
        }]
    });
});
<?php
    $last = count($taqdirs);
    $i = 0;
    $dg='';
    foreach($taqdirs as $a)
    {
        $i++;
        if($a->type_id!='')
        {
        	$dg.= '{name:"'.$taqdirs_items[$a->type_id].'", y:'.$a->total.'}';
        }
        else
        {
        	$dg.= '{name:"NA", y:'.$a->total.'}';
        }
        if($i < $last)
        {
            $dg.= ',';
        }
    }
    
?>
$(function () {
    // Create the chart
    $('#chart4').highcharts({
        chart: {
            type: 'column',
            
        },
        title: {
            text: '<span style="font-weight: bold;font-size: 20px;">تقدیرنامه ها</span>'
        },
        // subtitle: {
        //     text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'
        // },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
            	innerSize: 50,
                depth: 0,
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            pointFormat: '{series.name}: <b>% {point.percentage:.1f}</b>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [<?=$dg?>]
        }]
    });
});


</script>

@stop




