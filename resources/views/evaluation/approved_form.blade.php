@extends('layouts.master')

@section('head')
    <title>{!!_('edit_approve_form')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAllDocEmployees')!!}">{!!_('evaluation')!!}</a></li>
    <li class="active"><span>{!!_('approve_form')!!}</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postApprovedDoc')!!}" enctype="multipart/form-data">
			    <div class="panel-heading">
			      <h5 class="panel-title">فورم تایید شده</h5>
			    </div>
                <div class="container-fluid">
                	<div class="row">
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">نوعیت مکافات منظور شده
			              		<span style="color:red">*</span>
	                    		</label>
                                <select name="approved_type" id="approved_type" required class="form-control" onchange="bring_items()">
                                	<option value="">{!!_('select')!!}</option>
                                	<option value='1' @if($details->approved_type==1) selected @endif >مدال</option>
                                	<option value='2' @if($details->approved_type==2) selected @endif>نشانه</option>
                                    <option value='3' @if($details->approved_type==3) selected @endif>تحسین نامه</option>
                                    <option value='4' @if($details->approved_type==4) selected @endif>تقدیرنامه</option>
                                    <option value='0' @if($details->approved_type==0) selected @endif>هیچکدام</option>
                                    
                                </select>
			               	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">مورد مکافات منظور شده
			              		<span style="color:red">*</span>
	                    		</label>
                                <select name="approved_type_item" id="approved_type_item" required class="form-control">
                                	<option value="">یک گزینه را انتخاب کنید</option>
                                	@if($items)
                                		@foreach($items AS $item)
                                			@if($item->id == $details->approved_type_item)
                                			<option value="{!!$item->id!!}" selected>{!!$item->name_dr!!}</option>
                                			@else
                                			<option value="{!!$item->id!!}">{!!$item->name_dr!!}</option>
                                			@endif
                                		@endforeach
                                		<option value="0">هیچکدام</option>
                                	@endif
                                </select>
			               	</div>
			            </div>
                		<div class="col-sm-6">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">مناسبت
			              		<span style="color:red">*</span>
	                    		</label>
	                            <select name="title" class="form-control" required>
	                            	<option value="">{!!_('select')!!}</option>
	                            	<option value='1' @if($details->title==1) selected @endif>نهم حوت</option>
	                                <option value='2' @if($details->title==2) selected @endif>هشت مارچ</option>
	                                <option value='3' @if($details->title==3) selected @endif>بیست و هشت اسد</option>
	                                <option value='4' @if($details->title==4) selected @endif>روز جهانی معلم</option>
	                                <option value='5' @if($details->title==5) selected @endif>روز آغاز سال تعلیمی جدید</option>
	                                <option value='6' @if($details->title==6) selected @endif>روز مطبوعات</option>
	                                <option value='7' @if($details->title==7) selected @endif>روز گرامیداشت از نزول قرآن عظیم الشان</option>
	                                <option value='8' @if($details->title==8) selected @endif>روز جهانی المپیک</option>
	                                <option value='9' @if($details->title==9) selected @endif>روز استرداد استقلال افغانستان</option>
	                                <option value='10' @if($details->title==10) selected @endif>روز هنر</option>
	                                <option value='11' @if($details->title==11) selected @endif>میلاد النبی</option>
	                                <option value='0' @if($details->title==0) selected @endif>هیچکدام</option>
	                                
	                            </select>
			               	</div>
			            </div>
                	</div>
                </div>
	            </br>
				
               	<div class="container-fluid">
	            	<div class="row">
	            		<div class="col-sm-6">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">شماره حکم
	                    		</label>
	                            <input class="form-control" type="text" name="hokm_no" value="{!!$details->hokm_no!!}">
	                    	</div>
			            </div>
	            	
			            <div class="col-sm-6">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">تاریخ حکم
	                    		
	                    		</label>
	                    		<?php $sdate = $details->hokm_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
	                            <input class="form-control datepicker_farsi" readonly type="text" name="hokm_date" value='<?php if($details->hokm_date !=null){echo jalali_format($sdate);}?>'>
	                            
	                    	</div>
			            </div>
	            	</div>
	            </div>
			    <div class="container-fluid" @if($details->approved_type>2) style="display:none" @endif id="farman_div">
	            	<div class="row">
	            		<div class="col-sm-6">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">شماره فرمان
	                    		</label>
	                            <input class="form-control" type="text" name="farman_no" value="{!!$details->farman_no!!}">
	                    	</div>
			            </div>
	            	
			            <div class="col-sm-6">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">تاریخ فرمان
	                    		
	                    		</label>
	                    		<?php $sdate = $details->farman_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
	                            <input class="form-control datepicker_farsi" readonly type="text" name="farman_date" value='<?php if($details->farman_date !=null){echo jalali_format($sdate);}?>'>
	                            
	                    	</div>
			            </div>
	            	</div>
	            </div>
	        	<div class="container-fluid">
	            	<div class="row">
	            		<div class="col-sm-3">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">شماره صادره
	                    		<span style="color:red">*</span>
	                    		</label>
	                            <input class="form-control" required type="text" name="issued_no" value="{!!$details->issued_no!!}">
	                    	</div>
			            </div>
	            	
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
	                    		<label class="col-sm-12 ">تاریخ صادره</label>
	                    		<?php $sdate = $details->issued_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
	                            <input class="form-control datepicker_farsi" readonly type="text" name="issued_date" value='<?php if($details->issued_date !=null){echo jalali_format($sdate);}?>'>
	                    	</div>
			            </div>
			            <div class="col-sm-6">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">چگونگی تفویض
			              		<span style="color:red">*</span>
		            			</label>
	                            <select name="how" class="form-control">
	                            	<option value="">یک گزینه را انتخاب کنید</option>
	                            	<option value='1' <?php echo ($details->how =='1' ? 'selected':''); ?>>رئیس جمهور اسلامی افغانستان</option>
	                                <option value='2' <?php echo ($details->how=='2' ? 'selected':''); ?>>وزارت/اداره مربوط</option>
	                                <option value='0' <?php echo ($details->how=='0' ? 'selected':''); ?>>تفویض نشده</option>
	                                
	                            </select>
			               	</div>
			            </div>
	            	</div>
	            </div>
	            <div class="container-fluid" >
			      	<div class="row">
		            	<div id="sender_external_div">
		        			<div class="col-sm-3">
		            			<div class="col-sm-12">
		            				<label class="col-sm-12 "> مرسل الیه<span style="color:red">*</span></label>
		            			</div>
		            			<div class="col-sm-12">
		                            <select name="ministry" id="send_to" class="form-control" style="width:100%" data-plugin="select2">
		                                <option value="">انتخاب</option>
		                            	@foreach($ministrires AS $ministry)
		                            		@if($ministry->id == $details->ministry)
		                                    <option value='{!!$ministry->id!!}' selected>{!!$ministry->name_dr!!}</option>
		                                    @else
		                                    <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
		                                    @endif
		                                @endforeach
			                          
			                            <option value="0">هیچکدام</option>
		                            </select>
		                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		            			</div>
		            		</div>
		        		</div>
	            		
			      	
			      		
			            <div id="internal_div">
				      		
		            		<div class="col-sm-3">
		            			<div class="col-sm-12">
		            				<label class="col-sm-12 ">مرسل
		            				<span style="color:red">*</span>
		            				</label>
		            			</div>
		            			<div class="col-sm-12">
		            				<select style="width:100%" class="form-control" name="sub_dep" id="sub_dep" >
			                            <option value='' selected>ریاست نظارت و ارزیابی</option>		                               
		                            </select>
		                            
		            			</div>
		            		</div>
		            	</div>
	            		
			      	</div>
			   </div>
			   <div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-12">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">موضوع</label>
	                            <input class="form-control" type="text" name="subject" value="{!!$details->subject!!}">
			               	</div>
			            </div>
			      		
			        </div>
			   </div>
			   <div class="container-fluid" >
			      	<div class="row">
			            @if($details->file_name!=null)
			            <div class="col-sm-6" id="attached_file_div">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">&nbsp;</label>
	                    		<a href="{!!URL::route('downloadDoc',$details->id)!!}">{!!$details->file_name!!}</a>
	                    		@if(canDelete('evaluation_receivedDocs'))
	                    		<a href="javascript:void()" onclick="removeReportFile('{!!$details->id!!}');" class="table-link danger">
                                    <i class="fa fa-trash-o" style='color:red;'></i>
                                </a>	
                                @endif
	                    	</div>
	                   </div>
	                   @endif
	                   	<div class="col-sm-6" @if($details->file_name!=null) style="display:none" @endif id="attach_div">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">فایل ضمیمه</label>
	                            <input type='file'  name='attach' class="form-control">
	                    	</div>
	                   	</div>
			           
			        </div>
			    </div>
	            <br/>
	           <input type="hidden" name="id" value="{!!$details->id!!}">
	           <div class="container-fluid" >
			      	<div class="row">
	                   <div class="col-sm-6">
	                		<div class="col-sm-12" id="sub_div">
	                    		<label class="col-sm-2 ">&nbsp;</label>
		                        @if(canEdit('evaluation_receivedDocs'))
		                        	<button class="btn btn-primary" type="submit" onclick="$('#sub_div').hide();">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have Edit permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">برگشت</button>
		                    </div>
	                	</div>
	                	@if(canEdit('evaluation_receivedDocs'))
	                	<div class="col-sm-6">
	                		<button onclick="undo_approve()" class="btn btn-danger" type="button">کنسل نمودن</button>
	                	</div>
	                	@endif
			      	</div>
			    </div>
			 	<div class="container_fluid">
			 		<div class="row">
			 			<div class="col-sm-12">
			 			@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
			 			</div>
			 		</div>
			 	</div>
                
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">
$("#general_department").select2();
$("#sub_dep").select2();
$("#send_to").select2();
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function bring_items()
{
	var type = $('#approved_type').val();
	if(type==1 || type==2)
	{
		$('#farman_div').show();
	}
	else
	{
		$('#farman_div').hide();
	}
    $.ajax({
            url: '{!!URL::route("bring_items")!!}',
            data: '&type='+type,
            type: 'post',
            beforeSend: function(){
                $("#approved_type_item").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#approved_type_item').html(response);
            }
        }
    );
}
function showServiceType(value)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#internal_div').slideUp();
        $('#external_div').slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#external_div').slideUp();
        $('#internal_div').slideDown();
    }

}
function showSenderType(value)
{
    if(value == 2)
    {
        //$('#ageer_div').slideUp();
        $('#sender_internal_div').slideUp();
        $('#sender_external_div').slideDown();

    }
    else
    {
        //$('#ageer_div').slideUp();
        $('#sender_external_div').slideUp();
        $('#sender_internal_div').slideDown();
    }

}
function removeReportFile(doc_id)
{
    var confirmed = confirm("Do you want to remove this file?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeFile")!!}',
                data: '&doc_id='+doc_id+'&type=approved_employees',
                type: 'post',
                beforeSend: function(){
                    $("#errors").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#errors').html(response);
                    $('#attached_file_div').remove();
                    $('#attach_div').show();
                }
            }
        );
    }

}
function undo_approve()
{
	var id = {!!$details->id!!};
	var emp_id = {!!$details->emp_id!!};
	
	var page = "{!!URL::route('unApproveEmp')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id+'&emp_id='+emp_id,
        success: function(r){
        	location.href="{!!URL::route('getAllDocEmployees')!!}";
        }
    });
}
</script>

@stop

