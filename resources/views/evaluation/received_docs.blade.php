@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('received_docs_list')!!}</title>
    {!! HTML::style('/css/font.css') !!}
    <style>
    .scroll-table {
    width: auto;
    overflow-x: auto;
    white-space: nowrap;
}
</style>
@stop
@section('content')
<div class="row col-md-12" style="margin-top:-30px">
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix">
            <center>
                <h4>
                د افغانستان اسلامی جمهوریت</br>
                دجمهوری ریاست دلوړ مقام د دفتر لوی ریاست</br>
                د پالیسی ، څارنی او بررسی معاونیت</br>
                د څارنی او ارزونی ریاست
                </h4>
            </center>   
        </div>
    </div>
    <div class="col-md-2 noprint">
        {!! HTML::image('/img/logo.jpg', 'LOGO', array('width' => '130','height' => '125')) !!}
        
    </div>
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix">
           
                <center>
                <h4 style="font-family: 'B Nazanin';">
                جمهوری اسلامی افغانستان</br>
                ریاست عمومی دفتر مقام عالی ریاست جمهوری</br>
                معاونیت پالیسی,نظارت و بررسی</br>
                ریاست نظارت و ارزیابی
                </h4></center>
            
            
        </div>
    </div>
</div>
<div class="row noprint" style="margin: 10px;">
<h4 class="pull-right"><span style="font-weight: bold;font-size: 25px;font-family: 'B Nazanin';">مشخصات اسناد ثبت شده</span></h4>
@if(canAdd('evaluation_receivedDocs'))
<a href="{!!URL::route('addNewDoc')!!}" class="btn btn-success pull-left"><i class="icon fa-plus" aria-hidden="true"></i>ثبت سند جدید</a>
@endif
</div>
<hr class="noprint" />
    @if(Session::has('success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('success')!!}
		</div>

    @elseif(Session::has('fail'))
    	<div class="alert alert-danger alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('fail')!!}
		</div>
    @endif
    <form class="form-horizontal" action="{!!URL::route('getAllReceivedDocs_post')!!}" role="form" id="rec_docs" method="post">
        <div class="form-group">
        		
                <div class="col-sm-4">
                    <div class="col-sm-12">
                        
                        <select class="form-control" name="year">
                            @for($i=1380;$i<=$sh_year;$i++)
                            <option value='{!!$i!!}' @if($i==$year) {!!'selected'!!} @endif>{!!$i!!}</option>
                            @endfor
                        </select>
                    </div>
                    
                </div>
        		<div class="col-sm-3">
                    <div class="col-sm-12">
                        
                        <select name="doc_type" class="form-control">
                            <option value='0'>نوعیت سند</option>
                            <option value='1' <?php echo ($doc_type=='1' ? 'selected':''); ?>>مکتوب</option>
                            <option value='2' <?php echo ($doc_type=='2' ? 'selected':''); ?>>پیشنهاد</option>
                            <option value='3' <?php echo ($doc_type=='3' ? 'selected':''); ?>>هدایت</option>
                            
                        </select>
                    </div>
                    
                </div>
                <div class="col-sm-3">
                    <div class="col-sm-12">
                        
                        <input class="form-control" type="text" name="doc_no" placeholder="شماره سند" value="0">
                    </div>
                    
                </div>
                <div class="form-group">
                    <div class="col-sm-2">
                        <button class="btn btn-warning" type="submit">جستجو</button>
                    </div>
                </div> 
        </div>
           
    </form>
    <div id="all">
        <div class="col-lg-12">
            <table class="table" style="direction: rtl;float: right;">
                <thead>
                    <tr>
                        <td align="right" class="td-header" style="font-weight: bold;">تعداد مجموع اسناد</td>
                        <td align="right" class="td-header" style="font-weight: bold;">تعداد ذوات تحت اجرا</td>
                        <td align="right" class="td-header" style="font-weight: bold;">تعداد ذوات اجرا شده</td>
                        
                </thead>
                <tbody>
                    <tr style="background-color: #cccc00">
                        <td style="font-weight: bold;">{!!count($doc_total)!!}</td>
                        <td style="font-weight: bold;">{!!count($docEmp_total_not)!!}</td>
                        <td style="font-weight: bold;">{!!count($docEmp_total) - count($docEmp_total_not)!!}</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div style="padding:15px" class="table-responsive scroll-table">
            
            <table class="table table-bordered table-responsive" id="depList">
                <thead>
                  <tr>
                    <th style="font-weight: bold;">شماره</th>
                    <th style="font-weight: bold;">نوعیت سند</th>
                    <th style="font-weight: bold;">شماره سند</th>
                    <th style="font-weight: bold;">تاریخ سند</th>
                    <th style="font-weight: bold;">اداره مربوطه</th>
                    <th style="font-weight: bold;">موضوع</th>
                    <th style="font-weight: bold;">سال</th>   
                    <th style="font-weight: bold;">فایل</th>   
                    <th style="font-weight: bold;">عملیه</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>

            
        </div>
    </div>
<div class="modal fade modal-fade-in-scale-up" id="reject_modal" aria-hidden="true" aria-labelledby="reject_modal" role="dialog" tabindex="-1"></div>
<div class="md-overlay"></div><!-- the overlay element -->

@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#depList').dataTable(
        {
			"order": [[ 1, 'desc' ]],
            "sDom": 'lfr<"clearfix">tip',
            "bProcessing": true,
            "bServerSide": true,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getAllDocs',array($year,$doc_type,$doc_no))!!}"

        }
    );
});

function getSearchResult()
{
    window.location.replace("{!!URL::route('getAllReceivedDocs',array($year,$doc_type,$doc_no))!!}");
    
}
</script>

@stop


