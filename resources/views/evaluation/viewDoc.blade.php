@extends('layouts.master')

@section('head')
    <title>{!!_('view_document')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
    <style type="text/css">
    	label{
    	font-weight: bold;
    }
    </style>
@stop
@section('content')
@if($errors->has('field'))
	<div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif    
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getAuditReports')!!}">{!!_('evaluation')!!}</a></li>
    <li class="active"><span>{!!_('view_document')!!}</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
			    <div class="panel-heading">
			      <h5 class="panel-title" style="font-weight: bold;">مشخصات سند</h5>
			    </div>
                <div class="container-fluid">
                	<div class="row">
                		
                		<div class="col-sm-6">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 ">نوعیت سند
			            		</label>
                                <select name="doc_type" required class="form-control">
                                    <option value=''>{!!_('select_an_item')!!}</option>
                                    <option value='1' @if($details->doc_type==1) selected @endif>{!!_('maktob')!!}</option>
                                    <option value='2' @if($details->doc_type==2) selected @endif>{!!_('pishnahad')!!}</option>
                                    <option value='3' @if($details->doc_type==3) selected @endif>{!!_('hedayat')!!}</option>
                                    
                                </select>
			               	</div>
			               	
			            </div>
                	
			            <div class="col-sm-6">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">نوعیت اداره</label>
                                <select name="type" class="form-control" onchange="showServiceType(this.value)">
                                	<option value="1" @if($details->dep_type==1) selected @endif>داخل اداره</optioin>
                                	<option value="2" @if($details->dep_type==2) selected @endif>خارج از اداره</option>
                                </select>
			               	</div>
			            </div>
                	</div>
                </div>
	            </br>
				<div class="container-fluid" >
			      	<div class="row">
			      		<div id="internal_div" @if($details->dep_type==2) style="display:none" @endif>
	               			
	                		<div class="col-sm-6">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">اداره مربوطه
	                				<span style="color:red">*</span>
	                				</label>
	                				<select class="form-control" name="sub_dep" id="sub_dep" >
	                                	<option value="">انتخاب</option>
	                                    @foreach($parentDeps AS $sub_item)
	                                    	@if($details->sub_dep==$sub_item->id)
	                                        	<option value='{!!$sub_item->id!!}' selected>{!!$sub_item->name!!}</option>
	                                        @else
	                                        	<option value='{!!$sub_item->id!!}'>{!!$sub_item->name!!}</option>
	                                       	@endif
	                                    @endforeach
	                                </select>
	                                
	                			</div>
	                		</div>
	                	</div>
	                	<div id="external_div" @if($details->dep_type==1) style="display:none" @endif >
                			<div class="col-sm-6">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">وزارت/اداره<span style="color:red">*</span></label>
	                                <select name="ministry" class="form-control">
	                                    <option value="">انتخاب</option>
	                                	@foreach($ministrires AS $ministry)
	                                		@if($details->ministry==$ministry->id)
	                                        <option value='{!!$ministry->id!!}' selected>{!!$ministry->name_dr!!}</option>
	                                        @else
	                                        <option value='{!!$ministry->id!!}'>{!!$ministry->name_dr!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>
	                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	                			</div>
	                		</div>
                		</div>
                	</div>
               	</div>
               	<div class="container-fluid">
                	<div class="row">
                		<div class="col-sm-6">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">شماره سند
                        		</label>
                                <input class="form-control" type="text" name="doc_no" value="{!!$details->doc_no!!}">
                        	</div>
			            </div>
                	
			            <div class="col-sm-6">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ سند
                        		
                        		</label>
                        		<?php $sdate = $details->doc_date; if($sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                                <input class="form-control datepicker_farsi" readonly type="text" name="date" value='<?php if($details->doc_date !=null){echo jalali_format($sdate);}?>'>
                                <span style="color:red">{!!$errors->first('date')!!}</span>
                        	</div>
			            </div>
                	</div>
                </div>
			    <div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
	                		<div class="col-sm-12">
	                			<label class="col-sm-12 ">موضوع</label>
                                <input class="form-control" type="text" name="desc" value="{!!$details->desc!!}">
                            </div>
                        </div>
	                	@if($details->file_name!=null)
			      			<div class="col-sm-6" id="attached_file_div">
		                		<div class="col-sm-12">
		                    		<label class="col-sm-12 ">&nbsp;</label>
		                    		<a href="{!!URL::route('downloadDocument',$details->id)!!}">{!!$details->file_name!!}</a>
		                    		@if(canDelete('evaluation_receivedDocs'))
		                    		<a href="javascript:void()" onclick="removeReportFile('{!!$details->id!!}');" class="table-link danger">
	                                    <i class="fa fa-trash-o" style='color:red;'></i>
	                                </a>	
	                                @endif
		                    	</div>
		                   </div>
			      		
	                   @endif
	                   <div class="col-sm-6" @if($details->file_name!=null) style="display:none" @endif id="attach_div">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">فایل ضمیمه</label>
	                    		
	                            <input type='file'  name='attach' class="form-control">
	                    	</div>
	                   </div>
	               </div>
	           </div>
	           <div class="container-fluid" >
			      	<div class="row">
			      		<div class="col-sm-6">
				           <div class="panel-heading">
						      <h5 class="panel-title" style="font-weight: bold;">مشخصات کارمند</h5>
						    </div>
						</div>
					    
			        </div>
			   </div>
			   @if($emps)
			   <?php $emp_count = 0; ?>
			   		@foreach($emps AS $emp)
			   		<?php $emp_count++; ?>
			   <div class="container-fluid" id="emp_{!!$emp_count!!}">
                	<div class="row">
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-2 ">اتباع
			            		</label>
                                <select name="nationality_{!!$emp_count!!}" required class="form-control">
                                   	<option value='1' @if($emp->nationality==1) selected @endif>افغان</option>
                                    <option value='2' @if($emp->nationality==2) selected @endif>خارجی</option>
                                    
                                </select>
			               	</div>
			               	
			            </div>
                		<div class="col-sm-3">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">نام مکمل<span style="color:red">*</span>
                        		
                        		</label>
                                <input class="form-control" required type="text" name="name_{!!$emp_count!!}" value="{!!$emp->name!!}">
                                
                        	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
                        		<label class="col-sm-12 ">نام پدر
                        		
                        		</label>
                                <input class="form-control" type="text" name="f_name_{!!$emp_count!!}" value="{!!$emp->f_name!!}">
                                
                        	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">نوعیت وظیفه</label>
                                <select name="job_{!!$emp_count!!}" class="form-control" onchange="showRank(this.value)">
                                	<option value="">{!!_('select')!!}</option>
                                	<option value='1' @if($emp->emp_type==1) selected @endif>ملکی</option>
                                    <option value='2' @if($emp->emp_type==2) selected @endif>نظامی</option>
                                    
                                </select>
			               	</div>
			            </div>
                	</div>
                	<div class="row">
                		<div id="bast_div" @if($emp->emp_type==2) style="display:none" @endif>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "emp_bast_{!!$emp_count!!}" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getBastStaticList('employee_rank',$emp->emp_bast)!!}
	                                </select>
				      			</div>
				      		</div>
				      	</div>
					      	
			      		<div id="military_div" @if($emp->emp_type==1) style="display:none" @endif>
			      			<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست نظامی</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "military_bast_{!!$emp_count!!}" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getStaticDropdown('military_rank',$emp->military_bast)!!}
                                    </select>
				      			</div>
				      		</div>
			      		</div>
			      		<div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">نوعیت مکافات پیشنهاد شده</label>
                                <select name="suggested_type_{!!$emp_count!!}" id="suggested_type_{!!$emp_count!!}" class="form-control" onchange="bring_items({!!$emp_count!!})">
                                	<option value="">{!!_('select')!!}</option>
									<?php $counter = 0; ?>
                                    @foreach (Config::get('myConfig.evaluation.eval_type.dr') as $item)
										<option value="{{$counter}}" <?=$emp->suggested_type==$counter? 'selected' : ''?> >{{$item}}</option>
									  <?php $counter++; ?>
									@endforeach
                                </select>
			               	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">مورد مکافات پیشنهاد شده</label>
                                <select name="suggested_type_item_{!!$emp_count!!}" id="suggested_type_item_{!!$emp_count!!}" class="form-control">
                                	<option value="">یک گزینه را انتخاب نمایی</option>
                                	<?php $items = getItems($emp->suggested_type); ?>
                                	@foreach($items AS $item)
                                		@if($item->id == $emp->suggested_type_item)
                                		<option value="{!!$item->id!!}" selected>{!!$item->name_dr!!}</option>
                                		@else
                                		<option value="{!!$item->id!!}">{!!$item->name_dr!!}</option>
                                		@endif
                                	@endforeach
                                </select>
			               	</div>
			            </div>
			            <div class="col-sm-3">
	                		<div class="col-sm-12">
	                			<label class="col-sm-12 "></label>
	                			@if($emp->status==1)
		                        <a href="{!!URL::route('approve_doc',$emp->id)!!}" target="_blank" class="btn btn-success" type="button">نمایش اجرات</a>
		                        @elseif($emp->status==2)
		                        <a href="{!!URL::route('loadRejectDoc',$emp->id)!!}" target="_blank" class="btn btn-success" type="button">نمایش اجرات</a>
		                        @else
		                        <a href="javascript:void();" class="btn btn-success" type="button">اجرات نشده</a>
		                        @endif
		                    </div>
                    	</div>
                	</div>
               </div>
               		@endforeach
               		<input type="hidden" id="total_emp" name="total_emp" value="{!!$emp_count!!}"/>
               
               @endif
               
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}
<script type="text/javascript">
function removeReportFile(doc_id)
{
    var confirmed = confirm("Do you want to remove this file?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeFile")!!}',
                data: '&doc_id='+doc_id+'&type=received_docs',
                type: 'post',
                beforeSend: function(){
                    $("#errors").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#errors').html(response);
                    $('#attached_file_div').remove();
                    $('#attach_div').show();
                }
            }
        );
    }

}
</script>
@stop

