@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('received_docs_list')!!}</title>
    {!! HTML::style('/css/font.css') !!}
<style>
    .scroll-table {
    width: auto;
    overflow-x: auto;
    white-space: nowrap;
}

</style>
@stop
@section('content')
<div class="row col-md-12" style="margin-top:-30px">
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix">
            <center>
                <h4>
                د افغانستان اسلامی جمهوریت</br>
                دجمهوری ریاست دلوړ مقام د دفتر لوی ریاست</br>
                د پالیسی ، څارنی او بررسی معاونیت</br>
                د څارنی او ارزونی ریاست
                </h4>
            </center>   
        </div>
    </div>
    <div class="col-md-2 noprint">
        {!! HTML::image('/img/logo.jpg', 'LOGO', array('width' => '130','height' => '125')) !!}
        
    </div>
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix">
           
                <center>
                <h4 style="font-family: 'B Nazanin';">
                جمهوری اسلامی افغانستان</br>
                ریاست عمومی دفتر مقام عالی ریاست جمهوری</br>
                معاونیت پالیسی,نظارت و بررسی</br>
                ریاست نظارت و ارزیابی
                </h4></center>
            
            
        </div>
    </div>
</div>
<div class="row noprint" style="margin: 10px;">
<h4 class="pull-right"><span style="font-weight: bold;font-size: 25px;font-family: 'B Nazanin';">مشخصات ذوات پیشنهاد شده</span></h4>
</div>
<hr class="noprint" />
    @if(Session::has('success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('success')!!}
		</div>

    @elseif(Session::has('fail'))
    	<div class="alert alert-danger alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('fail')!!}
		</div>
    @endif
<form class="form-horizontal" action="{!!URL::route('getAllDocEmployees_post')!!}" role="form" method="post">
   <div class="container-fluid">
    	<div class="row">
    		<div class="col-sm-2">
            	<div class="col-sm-12">
                    <select class="form-control" name="year">
                        @for($i=1380;$i<=$sh_year;$i++)
                        <option value='{!!$i!!}' @if($i==$year) {!!'selected'!!} @endif>{!!$i!!}</option>
                        @endfor
                    </select>
               	</div>
            </div>
            <div class="col-sm-3">
            	<div class="col-sm-12">
                    <select class="form-control" id="type" name="type" onchange="bring_items(1)">
                        <option value="0">نوعیت پیشنهاد شده</option>
                        <option value='1' @if($type==1) {!!'selected'!!} @endif>مدال</option>
                    	<option value='2' @if($type==2) {!!'selected'!!} @endif>نشان</option>
                        <option value='3' @if($type==3) {!!'selected'!!} @endif>تحسین نامه</option>
                        <option value='4' @if($type==4) {!!'selected'!!} @endif>تقدیرنامه</option>
                    </select>
               	</div>
            </div>
            <div class="col-sm-3">
                <div class="col-sm-12">
                    <select id="suggested_type_item_1" name="item" class="form-control">
                        <option value="0">یک گزینه را انتخاب کنید</option>             
                    </select>
                </div>
            </div>
             <div class="col-sm-3">
            	<div class="col-sm-12">
                    <select class="form-control" id="status" name="status">
                        <option value="0">اجرآت</option>
                        <option value="1">اجرا شده</option>
                        <option value="2">اجرا نشده</option>
                    </select>
               	</div>
            </div>
            <div class="col-sm-1">
                <div class="col-sm-12">
                    <button class="btn btn-warning" type="submit">جستجو</button>
                </div>
            </div>
    	</div>
    </div>   
</form>
    <div class="col-lg-12">
        <table class="table" style="direction: rtl;float: right;">
            <thead>
                <tr>
                    <td align="right" class="td-header" style="font-weight: bold;">تعداد ذوات پیشنهاد شده</td>
                    <td align="right" class="td-header" style="font-weight: bold;">تعداد مدال اجرا شده</td>
                    <td align="right" class="td-header" style="font-weight: bold;">تعداد نشان اجرا شده</td>
                    <td align="right" class="td-header" style="font-weight: bold;">تعداد تقدیرنامه های اجرا شده</td>
                    <td align="right" class="td-header" style="font-weight: bold;">تعداد تحسین نامه های اجرا شده</td>
            </thead>
            <tbody>
                <tr style="background-color: #cccc00">
                    <td style="font-weight: bold;">{!!count($docEmp_total)!!}</td>
                    <td style="font-weight: bold;">{!!count($medals)!!}</td>
                    <td style="font-weight: bold;">{!!count($signs)!!}</td>
                    <td style="font-weight: bold;">{!!count($taqdir)!!}</td>
                    <td style="font-weight: bold;">{!!count($tahsin)!!}</td>
                    
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div> 
    <div style="padding:15px" class="table-responsive scroll-table">
        <table class="table table-bordered table-responsive" id="depList">
            <thead>
              <tr >
                <th style="font-weight: bold;">شماره</th>
                <th style="font-weight: bold;">شماره سند</th>
                <th style="font-weight: bold;">نام مکمل</th>
                <th style="font-weight: bold;">نام پدر</th>
                <th style="font-weight: bold;">اتباع</th>
                <th style="font-weight: bold;">نوعیت مکافات پیشنهاد شده</th>
                <th style="font-weight: bold;">مورد مکافات پیشنهاد شده</th>
                <th style="font-weight: bold;">بست</th>
                <th style="font-weight: bold;">رتبه</th>
                <th style="font-weight: bold;">عنوان وظیفه</th>
                
                <th style="font-weight: bold;">اداره مربوط</th>
                
                <th style="font-weight: bold;">چگونگی اجراآت</th>
                
                <th style="font-weight: bold;">عملیه</th>
                
              </tr>
            </thead>

            <tbody>
            </tbody>
        </table>

        
    </div>
<div class="modal fade modal-fade-in-scale-up" id="reject_modal" aria-hidden="true" aria-labelledby="reject_modal" role="dialog" tabindex="-1"></div>
<div class="md-overlay"></div><!-- the overlay element -->

@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#depList').dataTable(
        {
			"order": [[ 12, 'desc' ]],
            "sDom": 'lfr<"clearfix">tip',
            "bProcessing": true,
            "bServerSide": true,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getAllEmployeesData',array($year,$type,$item,$status))!!}"

        }
    );
});
function load_reject_modal(id)
{
	$.ajax({
            url: '{!!URL::route("loadRejectDoc")!!}',
            data: '&id='+id,
            type: 'post',
            
            success: function(response)
            {
               $('#reject_modal').html(response);
            }
        }
    );
}
function bring_data()
{
	var year = $('#year').val();
	var type = $('#type').val();
	window.location.href = "/evaluation/getAllDocEmployees/"+year+"/"+type;
}
function bring_items(no)
{
    var type = $('#type').val();
    
    $.ajax({
            url: '{!!URL::route("bring_items")!!}',
            data: '&type='+type,
            type: 'post',
            beforeSend: function(){
                $("#suggested_type_item_"+no).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#suggested_type_item_'+no).html(response);
            }
        }
    );
}
</script>

@stop


