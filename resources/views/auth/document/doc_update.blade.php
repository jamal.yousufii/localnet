@extends('layouts.master')
@section('head')
    
    <title>Edit Document</title>
@stop

@section('content')

<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Document Edit</a></li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            
            <div class="box-content">
                @foreach($doc_details AS $doc_item)
                <!--<form class="form-horizontal" role="form" method="post" action="{!! URL::route('postCreateDoc') !!}" enctype="multipart/form-data">-->
                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postUpdateDoc',$doc_item->id) !!}" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-4">
                            <input name='name' value='{!!$doc_item->name!!}' class="form-control"  type="text">
                            <input name='tree_id' value='{!!$doc_item->tree_id!!}' class="form-control"  type="hidden">
                            <span style='color:red'>
                            @if($errors->has("name"))
                                {!! $errors->first('name') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">Keyword</label>
                        <div class="col-sm-4">
                            <input value='{!!$doc_item->keyword!!}' name='keyword' class="form-control" type="text">
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Descriptions</label>
                        <div class="col-sm-4">
                            <!--<textarea id="wysiwig_simple" name='desc' id='desc' class="form-control" style="width: 952px; height: 92px;"></textarea>-->
                            <textarea name='desc' id='desc' class="form-control" style="width: 952px; height: 92px;">{!!$doc_item->description!!}</textarea>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Share With</label>
                        <div class="col-sm-4">
                            <select class="js-example-basic-multiple" name='share_with[]' id='share_with' multiple="multiple" style="width: 952px;">
                                <?php 
                                    foreach($users AS $uitem)
                                    {
                                        ?>

                                        <option <?php if(in_array($uitem->id, $shared_users)){ echo "selected = 'selected'"; } ?> value='u_{!!$uitem->id!!}'>{!!$uitem->first_name!!} {!!$uitem->last_name!!} </option>
                                    
                                    <?php
                                    }

                                    foreach($deps AS $ditem)
                                    {
                                        ?>
                                        
                                        echo <option <?php if(in_array($ditem->id, $shared_deps)){ echo "selected = 'selected'"; } ?> value='d_{!!$ditem->id!!}'>{!!$ditem->name!!}</option>
                                    
                                    <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <br>
                    <fieldset><legend>Uploaded Files</legend>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-6">
                                <table class='beauty-table'>
                                    <?php $counter=1; ?>
                                    @foreach($doc_files AS $doc_file)
                                        <tr id='tr_{!!$doc_file->id!!}'>
                                            <td>{!!$counter!!} - <i class="fa fa-file-o"></i> {!!$doc_file->file_name!!}</td>
                                            <td><a href='{!!URL::route("getDownload",$doc_file->id)!!}'><i class="fa fa-download"></i> Download</a></td>
                                            <td><a href='javascript:void()' onclick="removeFile({!!URL::route('deleteDocFile')!!})"><i class="fa fa-trash-o"></i> Remove</a></td>
                                        </tr>
                                    <?php $counter++; ?>
                                    @endforeach
                                </table>
                            </div>
                        </div>

                        
                    </fieldset>
                    <br>
                    <fieldset><legend>Upload New Files</legend>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-sm-4" id='files_div' style='width:465px;'>
                            <input style='width:400px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
                        </div>
                        <span style='color:red'>
                        @if($errors->has("file"))
                            {!! $errors->first('file') !!}
                        @endif
                        </span>

                    </div>
                    </fieldset>

                    {!! Form::token() !!}
                    
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">Cancel</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
@stop
@section('footer-scripts')
<script type="text/javascript">
function removeFile()
{
    var page = "{!!URL::route('deleteDocFile')!!}";
}

$(document).on("change","#files",function(){
    var count = $('#files_div').find('input').length;
    count = count+1;
    $("<div id='div_"+count+"' style='display:inline;'><input style='width:400px;splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div'); 
});

  $("#share_with").select2();

$(document).ready(function() {
    // Create Wysiwig editor for textare
    TinyMCEStart('#wysiwig_simple', null);
    //TinyMCEStart('#wysiwig_full', 'extreme');
});
</script>
@stop
