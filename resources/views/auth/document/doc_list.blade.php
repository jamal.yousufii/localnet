@extends('layouts.master')
@section('head')
	{!! HTML::style('/css/tree/easyui.css') !!}
	{!! HTML::style('/css/tree/icon.css') !!}
	<title>Documents</title>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jquery.easyui.min.js') !!}
@stop

@section('content')
	
	<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		
		<ol class="breadcrumb pull-left">
			<li><a href="#">Dashboard</a></li>
			<li><a href="#">Document List</a></li>
		</ol>
		
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box ui-draggable ui-droppable">
			
			<div class="box-content">

				@if(Session::has('success'))
				<div class='alert alert-success'>{!!Session::get('success')!!}</div>

				@elseif(Session::has('fail'))
				<div class='alert alert-danger'>{!!Session::get('fail')!!}</div>
				 @endif
				 
				<div class="easyui-panel" style="padding:10px;" id='view_tree'>
					<div class="btn-group" role="group" style='padding:5px;'>
					@if(canAdd('auth_document'))
				  		<a href="{!!URL::route('getCreateTree')!!}"><i class="fa fa-2x fa-folder-open"></i> <i class="fa fa-plus"></i> New Folder</a> 
				  	@endif
			   		</div>
			   		<hr>
					<ul class="easyui-tree" data-options="animate:true,lines:true">
						@if($trees)
							@foreach($trees AS $item)
								<li data-options="state:'closed'">
									<span>{!!$item->name!!}
										&nbsp;&nbsp;&nbsp;&nbsp;
										@if(canAdd('auth_document'))
											[ <a href="{!!URL::route('getCreateDoc',$item->id)!!}"><i class="fa fa-cloud-upload"></i> Upload File</a>
											  | <a href="{!!URL::route('getUpdateTree',$item->id)!!}"><i class="fa fa-edit"></i>Rename</a>
											  | <a href="{!!URL::route('getCreateTree',$item->id)!!}"><i class="fa fa-folder"></i> Sub Folder</a> 
											]
										@endif
									</span>

									<!--get tree files-->
									<ul>
										<?php $records = getFileRecords($item->id); ?>
										@foreach($records AS $row)
											<li><span>{!!$row->name!!}
												&nbsp;&nbsp;&nbsp;&nbsp;
												@if(canEdit('auth_document'))<a href="{!!URL::route('getUpdateDoc',$row->id)!!}"><i class="fa fa-edit"></i> Edit</a>@endif
											</span>

												<?php 
												$files = getRecordFiles($row->id);
												?>
												@foreach($files AS $file)
													<ul>
														<li>
															<span>{!!$file->file_name!!}</span>
														</li>
													</ul>
												@endforeach

											</li>
										@endforeach
									</ul>
									<!--get tree files-->


									<?php 
										$subs = getSubTree($item->id);
									?>
									@foreach($subs AS $sub_item)
										<ul>
											<li>
												<span>{!!$sub_item->name!!}</span>
												<!--get tree files-->
												<ul>
													<?php $records = getFileRecords($sub_item->id); ?>
													@foreach($records AS $row)
														<li><span>{!!$row->name!!}</span>

															<?php 
															$files = getRecordFiles($row->id);
															?>
															@foreach($files AS $file)
																<ul>
																	<li>
																		<span>{!!$file->file_name!!}</span>
																	</li>
																</ul>
															@endforeach

														</li>
													@endforeach
												</ul>
												<!--get tree files-->
												<?php 
													$subs = getSubTree($sub_item->id);
												?>
												@foreach($subs AS $sub_item)
													<ul>
														<li>
															<span>{!!$sub_item->name!!}</span>
															<!--get tree files-->
															<ul>
																<?php $records = getFileRecords($sub_item->id); ?>
																@foreach($records AS $row)
																	<li><span>{!!$row->name!!}</span>

																		<?php 
																		$files = getRecordFiles($row->id);
																		?>
																		@foreach($files AS $file)
																			<ul>
																				<li>
																					<span>{!!$file->file_name!!}</span>
																				</li>
																			</ul>
																		@endforeach

																	</li>
																@endforeach
															</ul>
															<!--get tree files-->
														</li>
													</ul>
												@endforeach
											</li>
										</ul>
									@endforeach
								</li>
							@endforeach
						@else
						Tree not created yet, create folder for your tree.
						@endif
					</ul>
				</div>
				
			</div>
		</div>
	</div>
</div>
@stop
