@extends('layouts.master')
@section('head')
    <title>Create Tree</title>
@stop

@section('content')

<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Document Create Tree</a></li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
           
            <div class="box-content">
                
                <!--<form class="form-horizontal" role="form" method="post" action="{!! URL::route('postCreateDoc') !!}" enctype="multipart/form-data">-->
                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postCreateTree') !!}" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Parent Folder</label>
                        <div class="col-sm-4">
                            <select class="form-control" name='parent' id='parent'>
                                @if($parent>0)
                                    <option value='{!!$parent!!}'>{!!getFolderName($parent)!!}</option>
                                @else
                                    <option value=''>Select</option>
                                    @foreach($trees AS $item)
                                        <option value='{!!$item->id!!}'>{!!$item->name!!}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Folder Name</label>
                        <div class="col-sm-4">
                            <input name='name' class="form-control"  type="text">
                            <span style='color:red'>
                            @if($errors->has("name"))
                                {!! $errors->first('name') !!}
                            @endif
                            </span>
                        </div>
                        
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Access Level Type</label>
                        <div class="col-sm-4">
                            <select class="form-control" name='type' id='type'>
                                <option value='1'>Public</option>
                                <option value='2'>Private</option>
                            </select>
                        </div>
                    </div>
                    

                    {!! Form::token() !!}
                    
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">Cancel</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('footer-scripts')
<script type="text/javascript">
  $("#share_with").select2();

$(document).ready(function() {
    // Create Wysiwig editor for textare
    TinyMCEStart('#wysiwig_simple', null);
    //TinyMCEStart('#wysiwig_full', 'extreme');
});
</script>
@stop
