@extends('layouts.master')

@section('head')
	<title>Dashboard</title>

@stop


@section('content')
	
	<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		
		<ol class="breadcrumb pull-left">
			<li><a href="#">Dashboard</a></li>
			<li><a href="#">Role List</a></li>
		</ol>
		
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box ui-draggable ui-droppable">
			
			<div class="box-content">
				
				@if(Session::has('success'))
				<div class='alert alert-success'>{!!Session::get('success')!!}}</div>

				@elseif(Session::has('fail'))
				<div class='alert alert-danger'>{!!Session::get('fail')!!}}</div>
				 @endif
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="role_list">
					<thead>
						<tr>
						   <th>#</th>
						   <th>Role Code</th>
						   <th>Role Name</th>
						   <th>Section Name</th>
						   <th>Module Name</th>
						   <th>Operation</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
				<div class="btn-group" role="group" style='padding:10px;'>
				  @if(canAdd('auth_role'))
				  	<a href="{!!URL::route('getCreateRole')!!}" class="btn btn-primary"><i class="fa fa-plus"></i> New Role</a> 
				  @endif	
			   </div>
			</div>
		</div>
	</div>
</div>
@stop
@section('footer-scripts')
<script type="text/javascript">
$('#role_list').dataTable( 
	{
		"bProcessing": true,
		"bServerSide": true,
		//"iDisplayLength": 2,
		"sAjaxSource": "{!!URL::route('getRoleData',$section_id)!!}}",
		//"aaSorting": [[ 1, "desc" ]],
	}
); 
</script>
@stop
