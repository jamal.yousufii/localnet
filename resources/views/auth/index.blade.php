<!DOCTYPE html>
<html lang="en">
<head>
  <title>AOP MIS Login Page</title>
  <style>
    ul li {list-style-type: square;}
  </style>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<!--===============================================================================================-->  
  <link rel="shortcut icon" href="../../images/favicon.png">
<!--===============================================================================================-->
  {!! HTML::style('login-page/vendor/bootstrap/css/bootstrap.min.css') !!}
<!--===============================================================================================-->
  {!! HTML::style('login-page/fonts/font-awesome-4.7.0/css/font-awesome.min.css') !!}
<!--===============================================================================================-->
  {!! HTML::style('login-page/vendor/animate/animate.css') !!}
<!--===============================================================================================-->  
  {!! HTML::style('login-page/vendor/css-hamburgers/hamburgers.min.css') !!}
<!--===============================================================================================-->
  {!! HTML::style('login-page/vendor/select2/select2.min.css') !!}
<!--===============================================================================================-->
  {!! HTML::style('login-page/css/util.css') !!}
  {!! HTML::style('login-page/css/main.css') !!}
<!--===============================================================================================-->
</head>
<body>
  
  <div class="limiter" >
    <div class="container-login100">
      <div class="wrap-login100">
        <div class="login100-pic js-tilt" data-tilt>
          <img src="{!! asset('login-page/images/login.jpg') !!}" alt="IMG">
        </div>
        
        <form class="login100-form validate-form" method="post" action="{!! URL::route('postLogin') !!}">
        
          <span class="login100-form-title" style="font-family: 'B Nazanin';direction: rtl; font-size: 23px;">
            ورود به سیستم MIS ریاست جمهوری
          </span>

          <div class="wrap-input100 validate-input" data-validate = "username is required">
            <input type="text" class="input100" name="username" placeholder="username">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-user" aria-hidden="true"></i>
            </span>
          </div>

          <div class="wrap-input100 validate-input" data-validate = "Password is required">
            <input type="password" class="input100" id="password" name="password"
          placeholder="Password">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-lock" aria-hidden="true"></i>
            </span>
          </div>
          
          <div class="container-login100-form-btn" >
            <button class="login100-form-btn" style="font-family: 'B Nazanin';">
              ورود به سیستم
            </button>
          </div>
          {{-- Creat new Account or Forgot Password  --}}
          <div class="mt-2" style="border:1px solid blue; text-align:center;">
          <a class="flot-right" href="{{route('create-account')}}">ایجاد اکونت</a> |
              <a href="">تغیر رمز</a>
          </div>
          @if(Session::has("success"))
            <div class="alert alert-success">{!! Session::get('success')!!}</div>
          @elseif(Session::has("fail"))
              <div class="alert" style="color: red;">{!! Session::get('fail')!!}</div>
          @endif
          </br>
        <div style="margin-top:15px" dir="rtl" style="font-family:B Nazanin">
            
            <div style="margin-top:15px;font-weight:bold;font-size:19px">شرایط داشتن اکونت برای چک کردن حاضری</div>
            <ul style="list-style-type: disc">
                <li>
                    داشتن ایمیل شخصی و یا رسمی از شخص تقاضا کننده
                </li>
                <li>
                    جهت داشتن اکونت باید به ایمیل آدرس ذیل اسم، وظیفه، شماره کارت و هم چنان ذکر شود که به اکونت حاضری ضرورت دارید را ارسال بدارید
                </li>
                <li style="margin-top:15px;font-weight:bold;font-size:19px">
                  mis.user@aop.gov.af
                </li>
                <li>
                    در صورت که پاسورد یا رمز ورودی اکونت شما فراموش تان شده باشد از ایمیل شخص خود تان موضوع را به ایمیل متذکره ارسال کنید.
                </li>
            </ul>
        </div>
          <div class="text-center p-t-136">
        
            <p>© AOP {!!date('Y')!!}. All RIGHTS RESERVED.</p>
            <div class="social">
              <a href="javascript:void(0)">
                <i class="icon bd-twitter" aria-hidden="true"></i>
              </a>
              <a href="javascript:void(0)">
                <i class="icon bd-facebook" aria-hidden="true"></i>
              </a>
              <a href="javascript:void(0)">
                <i class="icon bd-dribbble" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  

  
<!--===============================================================================================-->  
  {!! HTML::script('/login-page/vendor/jquery/jquery-3.2.1.min.js') !!}
<!--===============================================================================================-->
  {!! HTML::script('/login-page/vendor/bootstrap/js/popper.js') !!}
  {!! HTML::script('/login-page/vendor/bootstrap/js/bootstrap.min.js') !!}
<!--===============================================================================================-->
  {!! HTML::script('/login-page/vendor/select2/select2.min.js') !!}
<!--===============================================================================================-->
  {!! HTML::script('/login-page/vendor/tilt/tilt.jquery.min.js') !!}
  <script >
    $('.js-tilt').tilt({
      scale: 1.1
    })
  </script>
<!--===============================================================================================-->
  {!! HTML::script('/login-page/js/main.js') !!}
</body>
</html>

