@extends('layouts.master')

@section('head')
    
    <title>Translate and get next</title>
    <style type="text/css">
        .td_progress a{
            text-decoration: none;
            color: black;
        }
        .td_actions{
            text-align: center;
        }
        .pagination_div{
            width: 100%;
            height: 49px;
            text-align: center;
        }

    </style>

@stop

@section('content')
    
<div class="main-box">
    <header class="main-box-header clearfix">
        <h2 class="pull-left">Translate Field</h2>
    </header>
    @if(Session::has('success'))
    <center><div class='alert alert-success' style="width:500px;text-align:center">
        <i class="fa fa-check-circle fa-fw fa-lg"></i>
        {!!Session::get('success')!!}
    </div></center>
    @endif
    
    <div class="main-box-body clearfix">
        <div class="row">

            <div class="col-lg-12">
                <div class="main-box">
                    <header class="main-box-header clearfix">
                       
                            <div class="checkbox-nice">
                                <input id="filter_untranslated" type="checkbox">
                                <!-- <label for="filter_untranslated">
                                    Show all
                                </label> -->
                            </div>
                           
                    </header>
                    
                    <div class="main-box-body clearfix">
                        <form id="trans_form" role="form" class="form-horizontal" action="{!!URL::route('saveAndNext')!!}" method="post">
                            <input name="appId" value="{!!$appId!!}" type="hidden">
                            <input name="appeCode" value="{!!$appCode!!}" type="hidden">
                            <div id="result_div">
                                @foreach($fields AS $item)
                                        <input type="hidden" value="{!!$item->id!!}" name="current">
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Key Field</label>
                                            <div class="col-lg-8">
                                                <textarea class="form-control" type="text" name="key[]" readonly>{!!$item->key!!}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">English</label>
                                            <div class="col-lg-8">
                                                <textarea class="form-control" name="english[{!!$item->key!!}]">{!!$item->english!!}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">دری</label>
                                            <div class="col-lg-8">
                                                <textarea class="form-control" name="dari[{!!$item->key!!}]">{!!$item->dari!!}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">پشتو</label>
                                            <div class="col-lg-8">
                                                <textarea class="form-control" name="pashto[{!!$item->key!!}]">{!!$item->pashto!!}</textarea>
                                            </div>
                                        </div>
                                    
                                @endforeach
                            </div>

                            <div class="form-group">

                                <!-- <div class="col-xs-3">&nbsp;</div>
                                <div class="col-xs-9">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="{!!URL::route('getLocalizeApps')!!}" class="btn btn-danger">Cancel</a>
                                    <a href="{!!URL::route('generatingMoFile',array($appId,$appCode))!!}" class="btn btn-warning">Generate MO File</a>
                                </div> -->
                                <label class="col-lg-5 control-label"> </label>
                                <div class="col-lg-4 icon-box">
                                    <a href="javascript:void()" onclick="saveTranslate('prev')" class="btn pull-left">
                                        <i class="fa fa-chevron-left"></i>
                                    </a>
            
                                    <span class="pull-left" style="margin-left:10px;margin-right:10px;">
                                        <a href="javascript:void()" onclick="saveTranslate('save')" class="btn pull-left btn-primary">
                                            Save
                                        </a>
                                    </span>
                                    <a href="javascript:void()" onclick="saveTranslate('next')" class="btn pull-left">
                                        <i class="fa fa-chevron-right"></i>
                                    </a>
                                </div>
                                        
                            </div>
                        </form>

                        <div class="pagination_div" id="pagination_div">
                            <ul class="pagination">
                                <li><a href="javascript:void()" onclick="getFieldLimit('prev','{!!$item->id!!}',0)"><i class="fa fa-chevron-left"></i></a></li>
                                
                                <?php
                                    $page = 1;

                                    foreach($all AS $row):
                                ?>
                                    
                                    <li><a href="javascript:void()" onclick="getPageForm('{!!$row->id!!}')">{!!$page!!}</a></li>
                                    
                                <?php 
                                    $page++;
                                    endforeach;

                                    $page = $page-1;
                                ?>
                                
                                <li><a href="javascript:void()" onclick="getFieldLimit('next','{!!$row->id!!}','{!!$page!!}')"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@stop

@section('footer-scripts')
<script type="text/javascript">
    function saveTranslate(state)
    {
        $.ajax({
            url     : $("#trans_form").attr('action'),
            type    : $("#trans_form").attr('method'),
            dataType: 'html',
            beforeSend: function()
            {
                $("#result_div").html("LOADING...");
            },
            data    : $("#trans_form").serialize()+"&state="+state,
            success : function( data ) {
                        $("#result_div").html(data);
                      },
            error   : function( xhr, err ) {
                        alert(err);     
                      }
        });    
        return false;
    }
    function getPageForm(current)
    {
        $.ajax({
            url     : "{!!URL::route('getPageForm')!!}",
            type    : "post",
            dataType: 'html',
            beforeSend: function()
            {
                $("#result_div").html("LOADING...");
            },
            data    : "&current="+current+"&appId={!!$appId!!}"+"&appCode={!!$appCode!!}",
            success : function( data ) {
                        $("#result_div").html(data);
                      },
            error   : function( xhr, err ) {
                        alert(err);     
                      }
        });    
        return false;
    }
    function getFieldLimit(state,current,counter)
    {
        $.ajax({
            url     : "{!!URL::route('getFieldLimit')!!}",
            type    : "post",
            dataType: 'html',
            beforeSend: function()
            {
                $("#pagination_div").html("LOADING...");
            },
            data    : "&state="+state+"&current="+current+"&counter="+counter+"&appId={!!$appId!!}"+"&appCode={!!$appCode!!}",
            success : function( data ) {
                        $("#pagination_div").html(data);
                      },
            error   : function( xhr, err ) {
                        alert(err);     
                      }
        });    
        return false;
    }
</script>
@stop



