@extends('layouts.master')

@section('head')
    
    <title>Applications list</title>
    <style type="text/css">
        .td_progress a{
            text-decoration: none;
            color: black;
        }
        .td_actions{
            text-align: center;
        }
    </style>

@stop

@section('content')
    
<div class="main-box">
    <header class="main-box-header clearfix">
        <h2 class="pull-left">All Applications</h2>
    </header>
    
    <div class="main-box-body clearfix">
        <div class="row">

            <div class="col-md-12">
                <table class="table table-condensed table-hover">
                    @foreach($apps AS $item)
                    <tr>
                        <td width="50%" class="td_progress">
                            <a href="{!!URL::route('changeUserModule',$item->code)!!}">
                                <div class="clearfix">
                                        <div class="title pull-left">
                                            {!!$item->name!!}
                                        </div>
                                        <div data-original-title="10% down" class="value pull-right" title="" data-toggle="tooltip">
                                            {!!getTranslatedProgress($item->id)!!}% 
                                        </div>
                                </div>
                                <div class="progress progress-2x progress-striped active">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="{!!getTranslatedProgress($item->id)!!}" aria-valuemin="0" aria-valuemax="100" style="width: {!!getTranslatedProgress($item->id)!!}%">
                                            <span class="sr-only">{!!getTranslatedProgress($item->id)!!}% Complete</span>
                                        </div>
                                </div>
                            </a>
                        </td>
                        <td class="td_actions">
                            <a href="{!!URL::route('scanApplicationFolder',array($item->code,$item->id))!!}" class="btn btn-warning">
                                <i class="fa fa-refresh fa-lg"></i> Scan All Fields
                            </a>
                            <a href="{!!URL::route('translateFields',array($item->code,$item->id))!!}" class="btn btn-warning">
                                <i class="fa fa-edit fa-lg"></i> Translate
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </table>

                
            </div>

        </div>
    </div>
</div>
@stop



