<ul class="pagination">
    <li><a href="javascript:void()" onclick="getFieldLimit('prev','{!!$rows->first()->id!!}',{!!$counter-20!!})"><i class="fa fa-chevron-left"></i></a></li>
    
    <?php
        $page = $counter+1;

        foreach($rows AS $row):
    ?>
        
        <li><a href="javascript:void()" onclick="getPageForm('{!!$row->id!!}')">{!!$page!!}</a></li>
       
    <?php 
        $page++;
        endforeach;

        $page = $page-1;
    ?>
    
    <li><a href="javascript:void()" onclick="getFieldLimit('next','{!!$row->id!!}','{!!$page!!}')"><i class="fa fa-chevron-right"></i></a></li>
</ul>