@extends('layouts.master')

@section('head')
    
    <title>Availible Fields</title>
    <style type="text/css">
        .td_progress a{
            text-decoration: none;
            color: black;
        }
        .td_actions{
            text-align: center;
        }
    </style>

@stop

@section('content')
    
<div class="main-box">
    <header class="main-box-header clearfix">
        <h2 class="pull-left">Form For Localization</h2>
    </header>
    @if(Session::has('success'))
    <center><div class='alert alert-success' style="width:500px;text-align:center">
        <i class="fa fa-check-circle fa-fw fa-lg"></i>
        {!!Session::get('success')!!}
    </div></center>
    @endif
    
    <div class="main-box-body clearfix">
        <div class="row">

            <div class="col-lg-12">
                <div class="main-box">
                    <header class="main-box-header clearfix">
                       
                            <div class="checkbox-nice">
                                <input id="filter_untranslated" type="checkbox">
                                <label for="filter_untranslated">
                                    Show all
                                </label>
                            </div>
                            <hr />
                    </header>
                    
                    <div class="main-box-body clearfix">
                        <form role="form" action="{!!URL::route('updateLocalizedFields')!!}" method="post">
                            <input name="appId" value="{!!$appId!!}" type="hidden">
                            <input name="appCode" value="{!!$appCode!!}" type="hidden">
                            <div class="row">
                                <div class="form-group col-xs-3" style="margin-bottom:0px !important;">
                                    <label>Key Field</label>
                                </div>
                                <div class="form-group col-xs-3" style="margin-bottom:0px !important;">
                                    <label>English</label>
                                </div>
                                <div class="form-group col-xs-3" style="margin-bottom:0px !important;">
                                    <label>دری</label>
                                </div>
                                <div class="form-group col-xs-3" style="margin-bottom:0px !important;">
                                    <label>پشتو</label>
                                </div>
                            </div>
                            @foreach($fields AS $item)
                            <div class="row">
                                @if($item->english != '' && $item->dari != '' && $item->pashto != '')
                                <div class="translated_div" style="display:none;">
                                    <div class="form-group col-xs-3">
                                        <input disabled class="form-control" type="text" name="key[]" value="{!!$item->key!!}" readonly>
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <input disabled class="form-control" name="english[{!!$item->key!!}]" type="text" value="{!!$item->english!!}">
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <input disabled class="form-control" name="dari[{!!$item->key!!}]" type="text" value="{!!$item->dari!!}">
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <input disabled class="form-control" name="pashto[{!!$item->key!!}]" type="text" value="{!!$item->pashto!!}">
                                    </div>
                                </div>
                                @else
                                <div class="all_div">
                                    <div class="form-group col-xs-3">
                                        <input class="form-control" type="text" name="key[]" value="{!!$item->key!!}" readonly>
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <input class="form-control" name="english[{!!$item->key!!}]" type="text" value="{!!$item->english!!}">
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <input class="form-control" name="dari[{!!$item->key!!}]" type="text" value="{!!$item->dari!!}">
                                    </div>
                                    <div class="form-group col-xs-3">
                                        <input class="form-control" name="pashto[{!!$item->key!!}]" type="text" value="{!!$item->pashto!!}">
                                    </div>
                                </div>
                                @endif
                                
                            </div>
                            @endforeach
                            <div class="row">

                                <div class="col-xs-3">&nbsp;</div>
                                <div class="col-xs-9">
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <a href="{!!URL::route('getLocalizeApps')!!}" class="btn btn-danger">Cancel</a>
                                    <a href="{!!URL::route('generatingMoFile',array($appId,$appCode))!!}" class="btn btn-warning">Generate MO File</a>
                                </div>
                            <div>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@stop

@section('footer-scripts')
<script type="text/javascript">
    $('#filter_untranslated').on('change',function(e){
        if(this.checked)
        {
            $('.translated_div').show();
            
           
            var fields = $('.translated_div').find("input");

            for(var i=0;i<fields.length;i++)
            {
                fields[i].disabled = false;
            }

        }
        else
        {
            $('.translated_div').hide();
            //$('.all_div').show();

            var fields_all = $('.all_div').find("input");
            var fields = $('.translated_div').find("input");

            for(var i=0;i<fields.length;i++)
            {
                fields[i].disabled = true;
            }

            // for(var i=0;i<fields_all.length;i++)
            // {
            //     fields_all[i].disabled = false;
            // }
        }
    });
</script>
@stop



