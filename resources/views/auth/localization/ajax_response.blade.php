@foreach($fields AS $item)
    <input type="hidden" value="{!!$item->id!!}" name="current">
    <div class="form-group">
        <label class="col-lg-2 control-label">Key Field</label>
        <div class="col-lg-8">
            <textarea class="form-control" type="text" name="key[]" readonly>{!!$item->key!!}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label">English</label>
        <div class="col-lg-8">
            <textarea class="form-control" name="english[{!!$item->key!!}]">{!!$item->english!!}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label">دری</label>
        <div class="col-lg-8">
            <textarea class="form-control" name="dari[{!!$item->key!!}]">{!!$item->dari!!}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label">پشتو</label>
        <div class="col-lg-8">
            <textarea class="form-control" name="pashto[{!!$item->key!!}]">{!!$item->pashto!!}</textarea>
        </div>
    </div>
    
@endforeach