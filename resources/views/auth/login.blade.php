<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Login Page</title>
    
    {!! HTML::style('/css/template/libs/font-awesome.css') !!}
    {!! HTML::style('/css/template/bootstrap/bootstrap.min.css') !!}
    {!! HTML::style('/css/template/libs/nanoscroller.css') !!}
    {!! HTML::style('/css/template/compiled/theme_styles.css') !!}
    
    <!-- google font libraries -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>

    {!! HTML::style('/css/template/libs/jquery-jvectormap-1.2.2.css') !!}

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body id="login-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="login-box">
                    <div id="login-box-holder">
                        <div class="row">
                            <div class="col-xs-12">
                                <header id="login-header">
                                    <div id="login-logo">
                                        {!! HTML::image('/img/logo.png', 'User Profile Picture', array('class' => 'normal-logo logo-white')) !!}
                                    </div>
                                </header>
                                <div id="login-box-inner">
                                    <form role="form" action="{!! url('/auth/login') !!}" method="post">
                                        <div class="input-group">
                                        	<input type="hidden" name="_token" value="{!!csrf_token()!!}">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input class="form-control" name="username" type="text" placeholder="Username">
                                            <span style='color:red'>
                                            @if($errors->has("username"))
                                                {!! $errors->first('username') !!}
                                            @endif
                                            </span>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                            <input type="password" name="password" class="form-control" placeholder="Password">
                                            <span style='color:red'>
                                            @if($errors->has("password"))
                                                {!! $errors->first('password') !!}
                                            @endif
                                            </span>
                                        </div>
                                        <div id="remember-me-wrapper">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="checkbox-nice">
                                                        <input type="checkbox" id="remember-me" checked="checked" />
                                                        <label for="remember-me">
                                                            Remember me
                                                        </label>
                                                    </div>
                                                </div>
                                                <a href="#" id="login-forget-link" class="col-xs-6">
                                                    Forgot password?
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            @if(Session::has("success"))
                                                <div class="alert alert-success">{!! Session::get('success')!!}</div>
                                            @elseif(Session::has("fail"))
                                                <div class="alert alert-danger">{!! Session::get('fail')!!}</div>
                                            @endif

                                            {!!Form::token()!!}
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-success col-xs-12">Login</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <p class="social-text">Or login with</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <button type="submit" class="btn btn-primary col-xs-12 btn-facebook">
                                                    <i class="fa fa-facebook"></i> facebook
                                                </button>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <button type="submit" class="btn btn-primary col-xs-12 btn-twitter">
                                                    <i class="fa fa-twitter"></i> Twitter
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="login-box-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                Do not have an account? 
                                <a href="registration.html">
                                    Register now
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="config-tool" class="closed">
        <a id="config-tool-cog">
            <i class="fa fa-cog"></i>
        </a>
        
        <div id="config-tool-options">
            <h4>Layout Options</h4>
            <ul>
                <li>
                    <div class="checkbox-nice">
                        <input type="checkbox" id="config-fixed-header" />
                        <label for="config-fixed-header">
                            Fixed Header
                        </label>
                    </div>
                </li>
                <li>
                    <div class="checkbox-nice">
                        <input type="checkbox" id="config-fixed-sidebar" />
                        <label for="config-fixed-sidebar">
                            Fixed Left Menu
                        </label>
                    </div>
                </li>
                <li>
                    <div class="checkbox-nice">
                        <input type="checkbox" id="config-fixed-footer" />
                        <label for="config-fixed-footer">
                            Fixed Footer
                        </label>
                    </div>
                </li>
                <li>
                    <div class="checkbox-nice">
                        <input type="checkbox" id="config-boxed-layout" />
                        <label for="config-boxed-layout">
                            Boxed Layout
                        </label>
                    </div>
                </li>
                <li>
                    <div class="checkbox-nice">
                        <input type="checkbox" id="config-rtl-layout" />
                        <label for="config-rtl-layout">
                            Right-to-Left
                        </label>
                    </div>
                </li>
            </ul>
            <br/>
            <h4>Skin Color</h4>
            <ul id="skin-colors" class="clearfix">
                <li>
                    <a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">
                    </a>
                </li>
                <li>
                    <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">
                    </a>
                </li>
                <li>
                    <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">
                    </a>
                </li>
                <li>
                    <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">
                    </a>
                </li>
                <li>
                    <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">
                    </a>
                </li>
                <li>
                    <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">
                    </a>
                </li>
                <li>
                    <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">
                    </a>
                </li>
                <li>
                    <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">
                    </a>
                </li>
            </ul>
        </div>
    </div>
    
    

    {!! HTML::script('/js/template/demo-skin-changer.js') !!}
    {!! HTML::script('/js/template/jquery.js') !!}
    {!! HTML::script('/js/template/bootstrap.js')!!}
    {!! HTML::script('/js/template/jquery.nanoscroller.min.js')!!}
    {!! HTML::script('/js/template/demo.js')!!}
    {!! HTML::script('/js/template/scripts.js')!!}
    <!-- this page specific inline scripts -->
    
</body>
</html>

