@extends('layouts.master')
@section('head')
	
	{!! HTML::style('/css/charts/jquery.orgchart.css') !!}
	<title>Department</title>

	<style>
		.long-name {
		    font-size: 12px;
		}
		div.orgChart div.node.level0,
		div.orgChart div.node.level2 {
	    background-color: rgb(244, 227, 116);
		}
		.orgchart h2{
			padding: 0px !important;
		}
</style>
@stop


@section('content')
	
	<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		
		<ol class="breadcrumb pull-left">
			<li><a href="#">{!!_('dash_department')!!}</a></li>
			<li><a href="#">Department</a></li>
		</ol>
		
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="box ui-draggable ui-droppable">
			
			<div class="box-content no-padding" style="direction:ltr;">
				
				@if(Session::has('success'))
				<div class='alert alert-success'>{!!Session::get('success')!!}</div>
				@elseif(Session::has('fail'))
				<div class='alert alert-danger'>{!!Session::get('fail')!!}</div>
				 @endif
				 
				<div class="btn-group" role="group">
				  
				  	<a href="{!!URL::route('getAllDepartments')!!}" class="btn btn-primary pull-right"><i class="fa fa-arrow-left"></i> Back</a>
				  
				</div>

				<!-- <ul id="basic-chart-source" class="hide">
				    <li class="long-name">
				        ریاست جمهوری اسلام افغانستان
				        
		                <ul>tr
		                    <li>
		                        Software
		                        <ul>
		                            <li>Peter Gibbons</li>
		                            <li>Michael Bolton</li>
		                            <li class="long-name">Samir Nagheenanajar</li>
		                        </ul>
		                    </li>
		                    <li>
		                        Collation
		                        <ul>
		                            <li>Milton Waddams</li>
		                        </ul>
		                    </li>
		                    <li>
		                        Consultants
		                        <ul>
		                            <li>Bob Slydell</li>
		                            <li>Bob Porter</li>
		                        </ul>
		                    </li>
		                </ul>
				            
				    </li>
				</ul> -->
				
				{!!getOrganizationChart()!!}

				<div id="chart">
				</div>
				
			 </div>

			   
			   
			</div>
		</div>
	</div>
@stop
@section('footer-scripts')
    
    {!! HTML::script('/js/highcharts/org_charts/jquery.orgchart.min.js') !!}
    {!! HTML::script('/js/highcharts/org_charts/jquery.orgchart.js') !!}
    
    <script type="text/javascript">
    	$(function() {
    		$("#basic-chart-source").orgChart({container: $("#chart")});
		});

    </script>
    
@stop
