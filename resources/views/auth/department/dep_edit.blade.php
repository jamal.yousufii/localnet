@extends('layouts.master')
@section('head')
    <title>{!!_('auth_edit_dep')!!}</title>
    <style>
        .select2-container--default {
                width: 372px !important;
        }
    </style>
@stop

@section('content')
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">{!!_('auth_edit_dep')!!}</a></li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            
            <div class="box-content">
                @foreach($details AS $dep_item)
                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postUpdateDepartment',$dep_item->id) !!}">
                    <fieldset>
                        <legend>Department Details</legend>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('auth_name')!!}</label>
                            <div class="col-sm-4">
                                <input value='{!!$dep_item->name!!}' name='name' data-original-title="Tooltip for name" class="form-control" data-toggle="tooltip" data-placement="bottom" type="text">
                                <span style='color:red'>
                                @if($errors->has("name"))
                                    {!! $errors->first('name') !!}
                                @endif
                                </span>
                            </div>
                            <label class="col-sm-2 control-label">Name English</label>
                            <div class="col-sm-4">
                                <input value='{!!$dep_item->name_en!!}' name='name_en' data-original-title="Tooltip for name" class="form-control" data-toggle="tooltip" data-placement="bottom" type="text">
                                <span style='color:red'>
                                @if($errors->has("name_en"))
                                    {!! $errors->first('name_en') !!}
                                @endif
                                </span>
                            </div>           
                            
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Name Pashto</label>
                            <div class="col-sm-4">
                                <input value='{!!$dep_item->name_pa!!}' name='name_pa' data-original-title="Tooltip for name" class="form-control" data-toggle="tooltip" data-placement="bottom" type="text">
                                <span style='color:red'>
                                @if($errors->has("name_pa"))
                                    {!! $errors->first('name_pa') !!}
                                @endif
                                </span>
                            </div>
                            <label class="col-sm-2 control-label">Parent Department</label>
                            <div class="col-sm-4">
                                <select class="select2 form-control" name="parent_dep" id="parent_dep" class="form-control">
                                    <option>Select</option>
                                    @foreach($deps AS $item)
                                        @if($dep_item->id!=$item->id)
                                            @if($dep_item->parent == $item->id)    
                                                <option selected='selected' value='{!!$item->id!!}'>{!!$item->name!!}</option>
                                            @else
                                                <option value='{!!$item->id!!}'>{!!$item->name!!}</option>
                                            @endif
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Department Location</label>
                            <div class="col-sm-10">
                                <textarea name='location' id='location' class="form-control" >{!!$dep_item->location!!}</textarea>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Application for Department</legend>
                    
                        <div class="form-group">
                            
                                <?php $selectedApps = array();?>
                                @foreach($depApps AS $da)
                                <?php array_push($selectedApps,$da->mid); ?>
                                @endforeach
                                    
                                
                            
                            <label class="col-sm-2 control-label">Applicatioon</label>
                            <div class="col-sm-4">
                                <select multiple='multiple' name="dep_app[]" id="dep_app[]" class="form-control">
                                    
                                    @foreach($apps AS $app)
                                        @if(in_array($app->id,$selectedApps))
                                            <option selected='selected' value='{!!$app->id!!}'>{!!$app->name!!}</option>
                                        @else
                                            <option value='{!!$app->id!!}'>{!!$app->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            
                        </div>
                    </fieldset>

                    {!! Form::token() !!}
                    
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">Cancel</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
@stop
