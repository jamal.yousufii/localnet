@extends('layouts.master')
@section('head')
    <title>{!!_('auth_create_dep')!!}</title>

    <style>
        .select2-container--default {
                width: 372px !important;
        }
    </style>
@stop

@section('content')
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="#">{!!_('left_dashboard')!!}</a></li>
            <li><a href="#">{!!_('auth_create_dep')!!}</a></li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            
            <div class="box-content">
                
                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postCreateDepartment') !!}">
                    <fieldset>
                        <legend>{!!_('auth_details_dep')!!}</legend>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('auth_name_dr')!!}</label>
                            <div class="col-sm-4">
                                <input name='name' data-original-title="Tooltip for name" class="form-control" data-toggle="tooltip" data-placement="bottom" type="text">
                                <span style='color:red'>
                                @if($errors->has("name"))
                                    {!! $errors->first('name') !!}
                                @endif
                                </span>
                            </div>
                            <label class="col-sm-2 control-label">{!!_('auth_name_en')!!}</label>
                            <div class="col-sm-4">
                                <input name='name_en' data-original-title="Tooltip for name" class="form-control" data-toggle="tooltip" data-placement="bottom" type="text">
                                <span style='color:red'>
                                @if($errors->has("name_en"))
                                    {!! $errors->first('name_en') !!}
                                @endif
                                </span>
                            </div>

                            
                            
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('auth_name_pa')!!}</label>
                            <div class="col-sm-4">
                            	<textarea name='name_pa' class="form-control" ></textarea>
                                <span style='color:red'>
                                @if($errors->has("name_pa"))
                                    {!! $errors->first('name_pa') !!}
                                @endif
                                </span>
                            </div>

                            <label class="col-sm-2 control-label">{!!_('auth_parent_dep')!!}</label>
                            <div class="col-sm-4">
                                <select class="select2 form-control" name="parent_dep" id="parent_dep" class="form-control">
                                    <option>{!!_('auth_select')!!}</option>
                                    @foreach($deps AS $item)
                                    <option value='{!!$item->id!!}'>{!!$item->name!!}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">{!!_('auth_location_dep')!!}</label>
                            <div class="col-sm-10">
                                <textarea name='location' id='location' class="form-control" ></textarea>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>{!!_('auth_application_dep')!!}</legend>
                    
                        <div class="form-group">
                            
                            <label class="col-sm-2 control-label">{!!_('auth_application')!!}</label>
                            <div class="col-sm-4">
                                <select multiple='multiple' name="dep_app[]" id="dep_app[]" class="form-control">
                                    
                                    @foreach($apps AS $app)
                                    <option value='{!!$app->id!!}'>{!!$app->name!!}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                        </div>
                    </fieldset>
                    

                    {!! Form::token() !!}
                    
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">{!!_('auth_save')!!}</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">{!!_('auth_cancel')!!}</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('footer-scripts')
<script>
    //  $("#parent_dep").select2();
</script>
@stop 
