@extends('layouts.master')
@section('head')
	{!! HTML::style('/css/tree/easyui.css') !!}
	{!! HTML::style('/css/tree/icon.css') !!}

	<title>Department</title>
@stop


@section('content')

<style type="text/css">

#view_tree{
	width: 95% !important;
}
</style>
<div class="row">


        <div class="panel-heading">
          <div class="panel-actions">
          	@if(canAdd('auth_department'))
            <a href="{!!URL::route('getCreateDepartment')!!}" class="btn btn-labeled social-tumblr" style="color:white;">
                    <span class="btn-label"><i class="icon wb-plus" aria-hidden="true"></i></span>Add New Department
            </a>
            @endif

            <a href="{!!URL::route('getOrgChart')!!}" class="btn btn-labeled social-tumblr" style="color:white;">
                    <span class="btn-label"><i class="fa fa-sitemap" aria-hidden="true"></i></span>Organization Chart
            </a>
            
          </div>
          <h3 class="panel-title">Department Tree</h3>
        </div>
        <div class="panel-collapse">

				@if(Session::has('success'))
				<div class='alert alert-success'>{!!Session::get('success')!!}</div>
				@elseif(Session::has('fail'))
				<div class='alert alert-danger'>{!!Session::get('fail')!!}</div>
				 @endif
				<br>
			   <div class="easyui-panel" style="direction:ltr;margin-left:40px;margin-bottom:40px; width:100% !important;" id='view_tree'>

					<ul class="easyui-tree" data-options="animate:true,lines:true">

						{!!getDepartmentTrees()!!}
					</ul>

				</div>

			</div>
        </div>


</div>
@stop
@section('footer-scripts')
    {!! HTML::script('/js/jquery.easyui.min.js') !!}

@stop
