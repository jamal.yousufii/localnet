@extends('layouts.master')
@section('head')
	<title>Department Module</title>
@stop

@section('content')
	
	<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		
		<ol class="breadcrumb pull-left">
			<li><a href="#">Dashboard</a></li>
			<li><a href="{{URL::route('getAllDepartments')}}">Departments</a></li>
			<li><a href="#">Department Application</a></li>
		</ol>
		
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box ui-draggable ui-droppable">
			
			<div class="box-content no-padding">
				@if(Session::has('success'))
				<div class='alert alert-success'>{{Session::get('success')}}</div>

				@elseif(Session::has('fail'))
				<div class='alert alert-danger'>{{Session::get('fail')}}</div>
				 @endif
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
					<thead>
						<tr>
						   <th>#</th>
						   <th>Application Code</th>
						   <th>Application Name</th>
						   <th>Descriptions</th>
						   <th>Operation</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$counter=1;
						?>		  	
						@foreach($records AS $item)
					            <tr>
							<th scope='row'>{{$counter}}</th>
							<td>{{$item->code}}</td>
							<td><a target='_blank' href="{{URL::route('getAllUsers',$item->mid)}}" title="Click to see users">{{ $item->mname }}</a></td>
							<td>{{$item->mdesc}}</td>
							<td><a target='_blank' href="{{URL::route('getUpdateApp',$item->mid)}}"><i class="fa fa-edit"></i> Edit</a> 
							&nbsp;&nbsp;<a href="{{URL::route('getDeleteApp',$item->mid)}}" onclick="javascript:return confirm('Do you want to continue this operation?');"><i class="fa fa-trash-o"></i> Delete</a></td>
						    </tr>
						    <?php 
							$counter++;
						    ?>
			            @endforeach
					</tbody>
				</table>
				
			</div>
		</div>
	</div>
</div>
@stop
