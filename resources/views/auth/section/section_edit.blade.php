@extends('layouts.master')
@section('head')
    <title>Edit Section</title>
@stop

@section('content')
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Section Edit</a></li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            
            <div class="box-content">
                @foreach($details AS $dep_item)
                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postUpdateSection',$dep_item->id) !!}">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Code</label>
                        <div class="col-sm-4">
                            <input value='{!!$dep_item->code!!}' name='code' data-original-title="Tooltip for name" class="form-control" data-toggle="tooltip" data-placement="bottom" type="text">
                            <span style='color:red'>
                            @if($errors->has("code"))
                                {!! $errors->first('code') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">Name English</label>
                        <div class="col-sm-4">
                            
                            <input value='{!!$dep_item->name_en!!}' name='name_en' data-original-title="Tooltip for name" class="form-control" data-toggle="tooltip" data-placement="bottom" type="text">
                            <span style='color:red'>
                            @if($errors->has("name_en"))
                                {!! $errors->first('name_en') !!}
                            @endif
                            </span>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name Pashto</label>
                        <div class="col-sm-4">
                            <input value='{!!$dep_item->name_pa!!}' name='name_pa' class="form-control"  type="text">
                            <span style='color:red'>
                            @if($errors->has("name_pa"))
                                {!! $errors->first('name_pa') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">Name Dari</label>
                        <div class="col-sm-4">
                            <input value='{!!$dep_item->name_dr!!}' name='name_dr' class="form-control" type="text">
                            <span style='color:red'>
                            @if($errors->has("name_dr"))
                                {!! $errors->first('name_dr') !!}
                            @endif
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Application</label>
                        <div class="col-sm-4">
                        <select name="module" id="module" class="form-control">
                            <option>Select</option>
                            @foreach($apps AS $pos)
                                @if($dep_item->module_id==$pos->id)
                                    <option selected='selected' value='{!!$pos->id!!}'>{!!$pos->name!!}</option>

                                @else
                                    <option value='{!!$pos->id!!}'>{!!$pos->name!!}</option>
                                @endif
                            @endforeach
                        </select>
                        <span style='color:red'>
                        @if($errors->has("module"))
                            {!! $errors->first('module') !!}
                        @endif
                        </span>
                        </div>
                        <label class="col-sm-2 control-label">Table Name</label>
                        <div class="col-sm-4">
                            <input value='{!!$dep_item->table_name!!}' name='table' data-original-title="Tooltip for name" class="form-control" data-toggle="tooltip" data-placement="bottom" type="text">
                            <span style='color:red'>
                            @if($errors->has("table"))
                                {!! $errors->first('table') !!}
                            @endif
                            </span>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">URL Route</label>
                        <div class="col-sm-4">
                            <input value='{!!$dep_item->url_route!!}' name='url_route' class="form-control"  type="text">
                            <span style='color:red'>
                            @if($errors->has("url_route"))
                                {!! $errors->first('url_route') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">Font Awsome Class(fa fa-icon)</label>
                        <div class="col-sm-4">
                            <input value='{!!$dep_item->fa_class_icon!!}' name='fa_class_icon' class="form-control" type="text">
                            
                        </div>
                    </div>
                    {!! Form::token() !!}
                    
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">Cancel</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
@stop
