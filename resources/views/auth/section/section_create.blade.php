@extends('layouts.master')
@section('head')
    <title>Create Section</title>
@stop
@section('content')
    
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Section Create</a></li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            
            <div class="box-content">
                
                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postCreateSection') !!}">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Code</label>
                        <div class="col-sm-4">
                            <input name='code' class="form-control"  type="text">
                            <span style='color:red'>
                            @if($errors->has("code"))
                                {!! $errors->first('code') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">Name English</label>
                        <div class="col-sm-4">
                            <input name='name_en' class="form-control" type="text">
                            <span style='color:red'>
                            @if($errors->has("name_en"))
                                {!! $errors->first('name_en') !!}
                            @endif
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name Pashto</label>
                        <div class="col-sm-4">
                            <input name='name_pa' class="form-control"  type="text">
                            <span style='color:red'>
                            @if($errors->has("name_pa"))
                                {!! $errors->first('name_pa') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">Name Dari</label>
                        <div class="col-sm-4">
                            <input name='name_dr' class="form-control" type="text">
                            <span style='color:red'>
                            @if($errors->has("name_dr"))
                                {!! $errors->first('name_dr') !!}
                            @endif
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Application</label>
                        <div class="col-sm-4">
                        <select name="module" id="module" class="form-control">
                            <option>Select</option>
                            @foreach($apps AS $pos)
                            <option value='{!!$pos->id!!}'>{!!$pos->name!!}</option>
                            @endforeach
                        </select>
                        <span style='color:red'>
                        @if($errors->has("module"))
                            {!! $errors->first('module') !!}
                        @endif
                        </span>
                        </div>
                        <label class="col-sm-2 control-label">Table Name</label>
                        <div class="col-sm-4">
                            <input name='table' id='table' class="form-control" type="text">
                            <span style='color:red'>
                            @if($errors->has("table"))
                                {!! $errors->first('table') !!}
                            @endif
                            </span>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">URL Route</label>
                        <div class="col-sm-4">
                            <input name='url_route' class="form-control"  type="text">
                            <span style='color:red'>
                            @if($errors->has("url_route"))
                                {!! $errors->first('url_route') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">Font Awsome Class(fa fa-icon)</label>
                        <div class="col-sm-4">
                            <input name='fa_class_icon' class="form-control" type="text">
                            
                        </div>
                    </div>

                    {!! Form::token() !!}
                    
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">Cancel</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@stop
