@extends('layouts.master')
@section('head')
	<title>Section</title>
@stop

@section('content')
	
<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		
		<ol class="breadcrumb pull-left">
			<li><a href="#">Dashboard</a></li>
			<li><a href="#">Section List</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box ui-draggable ui-droppable">
			
			<div class="box-content">
				@if(Session::has('success'))
				<div class='alert alert-success'>{!!Session::get('success')!!}</div>

				@elseif(Session::has('fail'))
				<div class='alert alert-danger'>{!!Session::get('fail')!!}</div>
				 @endif

				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="section_list">
					<thead>
						<tr>
						   <th>#</th>
						   <th>Section Code</th>
						   <th>Table Name</th>
						   <th>Section Name</th>
						   <th>Application</th>
						   <th>Operation</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
				<div class="btn-group" role="group" style='padding:10px;'>
					@if(canAdd('auth_section'))
				  		<a href="{!!URL::route('getCreateSection')!!}" class="btn btn-primary"><i class="fa fa-plus"></i> New Section</a> 
					@endif	   
			   </div>
			</div>
		</div>
	</div>
</div>
@stop
@section('footer-scripts')
<script type="text/javascript">
$('#section_list').dataTable( 
	{
		"bProcessing": true,
		"bServerSide": true,
		//"iDisplayLength": 2,
		"sAjaxSource": "{!!URL::route('getSectionData',$module_id)!!}",
		"aaSorting": [[ 1, "desc" ]],
		"aoColumns": [
		{ 'sWidth': '60px' },
		{ 'sWidth': '130px', 'sClass': 'center' },
		{ 'sWidth': '180px', 'sClass': 'center' },
		{ 'sWidth': '180px', 'sClass': 'center' },
		{ 'sWidth': '100px', 'sClass': 'center' },
		{ 'sWidth': '250px', 'sClass': 'center' }


		]
	}
); 
</script>
@stop
