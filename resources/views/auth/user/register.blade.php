@extends('layouts.master')
@section('head')
    <title>Register User</title>

    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
    <style type="text/css">
        .profile-img{
            height: 200px !important;
            width: 240px !important;
        }
    </style>
@stop

@section('content')
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">User Registration</a></li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            
            <div class="box-content">

                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postCreate') !!}">
                    <fieldset>
                    <legend>User Details</legend>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Department</label>
                            <div class="col-sm-4">
                                <?php 
                                    $url = URL::route('getUserDepApp');
                                   
                                ?>
                                <select name="department" id="department" style="width:100%;" onchange="bringRelatedWithEmployee('{!!$url!!}','user_module',this.value);" class="form-control">
                                    <option>Select</option>
                                    @foreach($departments AS $item)
                                    <option value='{!!$item->id!!}'>{!!$item->name!!}</option>
                                    @endforeach
                                </select>                
                                <span style='color:red'>
                                @if($errors->has("department"))
                                    {!! $errors->first('department') !!}
                                @endif
                                </span>
                            </div>

                            @if(Auth::user()->is_admin != 1 && isCaseAdmin())
							
							@else
                            <label class="col-sm-2 control-label">Department Users</label>
                            <div class="col-sm-4">
                                <?php $page = URL::route('authGetEmployeeDetails'); ?>
                                <select name='employee' id='employee' style="width:100%;" onchange="getEmployeeDetails('{!!$page!!}')">
                                    <option value=''>Employees...</option>
                                </select>
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">First Name</label>
                            <div class="col-sm-4">
                                <input name='first_name' id='first_name' class="form-control" type="text" "">
                                <span style='color:red'>
                                @if($errors->has("first_name"))
                                    {!! $errors->first('first_name') !!}
                                @endif
                                </span>
                            </div>
                            <label class="col-sm-2 control-label">Name In Eng</label>
                            <div class="col-sm-4">
                                <input name='name_en' id='name_en' class="form-control" type="text" "">
                                <span style='color:red'>
                                @if($errors->has("name_en"))
                                    {!! $errors->first('name_en') !!}
                                @endif
                                </span>
                            </div>
                           
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Last Name</label>
                            <div class="col-sm-4">
                                <input name='last_name' id='last_name' class="form-control" type="text" "">
                                <span style='color:red'>
                                @if($errors->has("last_name"))
                                    {!! $errors->first('last_name') !!}
                                @endif
                                </span>
                            </div>
                            <label class="col-sm-2 control-label">UserID</label>
                            <div class="col-sm-4">
                                <input name='userid' id='userid' class="form-control" type="text" "">
                                <span style='color:red'>
                                @if($errors->has("userid"))
                                    {!! $errors->first('userid') !!} 
                                @endif
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Father Name</label>
                            <div class="col-sm-4">
                                <input name='father_name' id='father_name' class="form-control" type="text" "">
                                <span style='color:red'>
                                @if($errors->has("father_name"))
                                    {!! $errors->first('father_name') !!}
                                @endif
                                </span>
                            </div>
                            <label class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-4">
                                <input name='username' id='username' class="form-control" type="text">
                                <span style='color:red'>
                                @if($errors->has("username"))
                                    {!! $errors->first('username') !!}
                                @endif
                                </span>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Position</label>
                            <div class="col-sm-4">
                                <input type="text" name="position" id="position" class="form-control" "">
                                    
                                <span style='color:red'>
                                @if($errors->has("position"))
                                    {!! $errors->first('position') !!}
                                @endif
                                </span>
                            </div>
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-4">
                                <input name='email' id='email' class="form-control" type="text" "">
                                
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-4">
                                <input name='pass1' id='pass1' class="form-control" type="password">
                                <span style='color:red'>
                                @if($errors->has("pass1"))
                                    {!! $errors->first('pass1') !!}
                                @endif
                                </span>
                            </div>
                            <label class="col-sm-2 control-label">Confirm Password</label>
                            <div class="col-sm-4">
                                <input name='pass2' id='pass2' class="form-control" type="password">
                                <span style='color:red'>
                                @if($errors->has("pass2"))
                                    {!! $errors->first('pass2') !!}
                                @endif
                                </span>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-4">
                                
                                <div class="checkbox">
                                    <label>
                                        <input name='is_admin' id='is_admin' type="checkbox"> Is Admin
                                        <i class="fa fa-square-o small"></i>
                                    </label>
                                </div>
                            
                            </div>
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-4" id="photo">
                                <?php 
                                    $photo = getProfilePicture(Auth::user()->id);
                                ?>
                                
                                {!! HTML::image('/document/profile_pictures/'.$photo, '', array('class' => 'profile-img img-responsive center-block','data-original-title'=>'','data-toggle'=>'tooltip')) !!}
                                <!-- <img src="img/samples/scarlet-159.png" alt="" class="profile-img img-responsive center-block"> -->
                                
                            </div>
                            
                        </div>
                        
                    </fieldset>
                    <fieldset>
                        <legend>User Roles</legend>
                        <div class="form-group">
                            
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-4">
                                
                                <div class="checkbox">
                                    <label>
                                        <input name='is_manager' id='is_manager' type="checkbox"> Is Department Manager
                                        <i class="fa fa-square-o small"></i>
                                    </label>
                                </div>
                            
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <?php 
                                    $ajaxUrl = URL::route('getUserAppSecion');
                            ?>
                            <div id='app_div' style='display:none'>
                                <label class="col-sm-2 control-label">Applications</label>
                                <div class="col-sm-4">
                                    <select style='height:120px;' multiple='multiple' name="user_module[]" id="user_module" onchange="bringMultiRelated('{!!$ajaxUrl!!}','user_module','app_entity');" class="form-control">
                                       
                                    </select>                
                                   
                                </div>
                            </div>
                            <div id='app_entity' style='display:none;'>
                            
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <div id='entity_role' style='display:none;'>
                            
                            </div>
                        </div>


                    </fieldset>
                    
                    {!! Form::token() !!}
                    
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">Cancel</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>

@stop
@section('footer-scripts')
{!! HTML::script('/js/template/select2.min.js')!!}

<script type="text/javascript">
    $("#employee").select2();
    $("#department").select2();

    //get employee details
    function getEmployeeDetails(page)
    {
        var emp = $("#employee").val();
        $.ajax({
            type: 'post',
            data: '&id='+emp,
            url: page,
            dataType: 'json',
            success:function(response){
                
                $("#first_name").val(response.first_name);
                $("#last_name").val(response.last_name);
                $("#father_name").val(response.father_name);
                $("#name_en").val(response.name_en);
                $("#userid").val(response.userid);
                $("#position").val(response.position);
                $("#email").val(response.email);
                $("#photo").html(response.photo);
            }
        });

    }
</script>
@stop
