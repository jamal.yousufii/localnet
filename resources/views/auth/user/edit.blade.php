@extends('layouts.master')
@section('head')
    <title>Edit User</title>
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
    <style type="text/css">
        .profile-img{
            height: 200px !important;
            width: 240px !important;
        }
    </style>
@stop

@section('content')
<div class="row">
    <div id="breadcrumb" class="col-xs-12">

        <ol class="breadcrumb pull-left">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">User Edit</a></li>
        </ol>

    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">

            <div class="box-content">
                @foreach($user_data AS $item)
                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postUpdate',$item->id) !!}">
                    <fieldset>
                    <legend>User Details</legend>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Department</label>
                        <div class="col-sm-4">
                            <?php
                                $url = URL::route('getUserDepApp');
                            ?>
                            <select name="department" id="department" onchange="bringRelatedWithEmployee('{!!$url!!}','user_module',this.value);" class="form-control">
                                @foreach($departments AS $dep)

                                @if($item->department_id == $dep->id)
                                    <option selected='selected' value='{!!$dep->id!!}'>{!!$dep->name!!}</option>
                                @else
                                    <option value='{!!$dep->id!!}'>{!!$dep->name!!}</option>
                                @endif

                                @endforeach
                            </select>
                            <span style='color:red'>
                            @if($errors->has("departmetn"))
                                {!! $errors->first('department') !!}
                            @endif
                            </span>
                        </div>

                        @if(Auth::user()->is_admin != 1 && isCaseAdmin())

						@else
                        <label class="col-sm-2 control-label">Department Users</label>
                        <div class="col-sm-4">
                            <?php $page = URL::route('authGetEmployeeDetails'); ?>
                            <select name='employee' id='employee' style="width:100%;" onchange="getEmployeeDetails('{!!$page!!}')">
                                @foreach($dep_employees AS $emp)
                                    <?php if($item->employee_id == $emp->id){ ?>
                                        <option value='{!!$emp->id!!}' selected="selected">{!!$emp->name!!}</option>
                                    <?php }else{?>
                                        <option value='{!!$emp->id!!}'>{!!$emp->name!!}</option>
                                    <?php }?>
                                @endforeach
                            </select>
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">First Name</label>
                        <div class="col-sm-4">
                            <input value='{!!$item->first_name!!}' name='first_name' id='first_name' class="form-control" type="text" readonly>
                            <span style='color:red'>
                            @if($errors->has("first_name"))
                                {!! $errors->first('first_name') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">Last Name</label>
                        <div class="col-sm-4">
                            <input value='{!!$item->last_name!!}' name='last_name' id='last_name' class="form-control" type="text" readonly>
                            <span style='color:red'>
                            @if($errors->has("last_name"))
                                {!! $errors->first('last_name') !!}
                            @endif
                            </span>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Father Name</label>
                        <div class="col-sm-4">
                            <input value='{!!$item->father_name!!}' name='father_name' id='father_name' class="form-control" type="text" readonly>
                            <span style='color:red'>
                            @if($errors->has("father_name"))
                                {!! $errors->first('father_name') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-4">
                            <input value='{!!$item->username!!}' name='username' id='username' class="form-control" type="text">
                            <span style='color:red'>
                            @if($errors->has("username"))
                                {!! $errors->first('username') !!}
                            @endif
                            </span>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Position</label>
                        <div class="col-sm-4">
                            <input type="text" name="position" id="position" class="form-control" value='{!!$item->position_id!!}' readonly>

                            <span style='color:red'>
                            @if($errors->has("position"))
                                {!! $errors->first('position') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-4">
                            <input value='{!!$item->email!!}' name='email' id='email' class="form-control" type="text" readonly>

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">New Password</label>
                        <div class="col-sm-4">
                            <input name='pass1' id='pass1' class="form-control" type="password">

                        </div>
                        <label class="col-sm-2 control-label">Confirm New Password</label>
                        <div class="col-sm-4">
                            <input name='pass2' id='pass2' class="form-control" type="password">

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">

                            <div class="checkbox">
                                <label>
                                    <input <?php if($item->is_admin==1){ echo "checked='checked'";} ?> name='is_admin' id='is_admin' type="checkbox"> Is Admin
                                    <i class="fa fa-square-o small"></i>
                                </label>
                            </div>

                        </div>
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-4" id="photo">
                            <?php
                                $photo = getProfilePicture($item->employee_id,true);
                            ?>

                            {!! HTML::image('/documents/profile_pictures/'.$photo, '', array('class' => 'profile-img img-responsive center-block','data-original-title'=>'','data-toggle'=>'tooltip')) !!}
                            <!-- <img src="img/samples/scarlet-159.png" alt="" class="profile-img img-responsive center-block"> -->

                        </div>

                    </div>

                </fieldset>
                <fieldset>
                        <legend>User Roles</legend>
                        <div class="form-group">

                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-4">

                                <div class="checkbox">
                                <label>
                                    <input <?php if($item->is_manager==1){ echo "checked='checked'";} ?> name='is_manager' id='is_manager' type="checkbox"> Is Department Manager
                                    <i class="fa fa-square-o small"></i>
                                </label>
                                </div>

                            </div>

                        </div>
                        <div class="form-group">
                            <?php
                                    $ajaxUrl = URL::route('getUserAppSecion');

                            ?>
                            <div id='app_div' style='display:block'>
                                <?php

                                    $selectedModule = array();
                                    foreach ($userModules as $userMod) {
                                        $selectedModule[] = $userMod->module_id;
                                    }
                                ?>
                                <label class="col-sm-2 control-label">Applications</label>
                                <div class="col-sm-4">
                                   <?php
                                    $userid = $item->id;
                                   ?>
                                    <select style="height:120px;" multiple='multiple' name="user_module[]" id="user_module" onchange="bringMultiRelated('{!!$ajaxUrl!!}','user_module','app_entity','&userid={!!$item->id!!}');" class="form-control">
                                        @foreach($modules AS $item)
                                            @if(in_array($item->mid,$selectedModule))
                                                <option selected='selected' value='{!!$item->mid!!}'>{!!$item->mname!!}</option>
                                            @else
                                                <option value='{!!$item->mid!!}'>{!!$item->mname!!}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <?php
                                $page = URL::route('getUserSectionRole');


                                $selectedEntity = array();
                                foreach ($userEntities as $userEnt) {
                                    $selectedEntity[] = $userEnt->entity_id;
                                }

                            ?>
                            <div id='app_entity' style='display:<?php if(count($userModules)>0){echo 'block';}else{echo 'none';}?>'>
                                <label class="col-sm-2 control-label">Sections</label>
                                <div class="col-sm-4">

                                    <select style="height:120px;" multiple="multiple" name="user_entity[]" id="user_entity" onchange="bringMultiRelated('<?=$page?>','user_entity','entity_role','&userid={!!$userid!!}');" class="form-control">
                                         <?php

                                            $moduleId = '';
                                            $moduleName = '';
                                            foreach($entities AS $item)
                                            {
                                                $moduleId = $item->module_id;
                                                $moduleName = getModuleName($item->module_id);
                                                break;
                                            }

                                            echo "<optgroup label='".$moduleName." Sections'>";


                                            foreach($entities AS $item){

                                                if($item->module_id != $moduleId)
                                                {

                                                    echo "</optgroup>";
                                                    echo "<optgroup label='".getModuleName($item->module_id)." Sections'>";
                                                }
                                                $lang = Session::get('lang');
                                                $section_name = "name_".$lang;
                                                $section_name = $item->$section_name;
                                                if(in_array($item->id,$selectedEntity))
                                                {
                                                    echo "<option selected='selected' value='".$item->id."'>".$section_name."</option>";
                                                }
                                                else
                                                {
                                                    echo "<option value='".$item->id."'>".$section_name."</option>";
                                                }

                                                $moduleId = $item->module_id;

                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">

                            <div id='entity_role' style='display:<?php if(count($userEntities)>0){echo 'block';}else{echo 'none';}?>;'>
                                <?php

                                    $selectedRole = array();
                                    foreach ($userRoles as $userRole) {
                                        $selectedRole[] = $userRole->role_id;
                                    }

                                ?>
                                <label class="col-sm-2 control-label">Roles</label>
                                <div class="col-sm-4">
                                    <select style="height:120px;" multiple="multiple" name="user_role[]" id="user_role" class="form-control">
                                        <?php
                                        $entityId = '';
                                        $entityName = '';
                                        foreach($roles AS $item)
                                        {
                                            $entityId = $item->entity_id;
                                            $entityName = getSectionName($item->entity_id);
                                            break;
                                        }

                                        echo "<optgroup label='".$entityName." Roles'>";
                                        foreach($roles AS $item):
                                            if($item->entity_id != $entityId)
                                            {
                                                echo "</optgroup>";
                                                echo "<optgroup label='".getSectionName($item->entity_id)." Roles'>";
                                            }
                                            if(in_array($item->id,$selectedRole))
                                            {
                                                echo "<option selected='selected' value='".$item->id."'>".$item->name."</option>";
                                            }
                                            else
                                            {
                                                echo "<option value='".$item->id."'>".$item->name."</option>";
                                            }
                                            $entityId = $item->entity_id;
                                        endforeach
                                        ?>
                                    </select>
                                </div>

                            </div>
                        </div>


                    </fieldset>


                    {!! Form::token() !!}

                    <div class="form-group">

                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">Cancel</button>

                        </div>

                    </div>

                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>

@stop
@section('footer-scripts')
{!! HTML::script('/js/template/select2.min.js')!!}

<script type="text/javascript">
$("#employee").select2();
$("#department").select2();

    //get employee details
    function getEmployeeDetails(page)
    {
        var emp = $("#employee").val();
        $.ajax({
            type: 'post',
            data: '&id='+emp,
            url: page,
            dataType: 'json',
            success:function(response){

                $("#first_name").val(response.first_name);
                $("#last_name").val(response.last_name);
                $("#father_name").val(response.father_name);
                $("#position").val(response.position);
                $("#email").val(response.email);
                $("#photo").html(response.photo);
            }
        });

    }
</script>
@stop
