<!DOCTYPE html>

<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>Metronic | Login Page - 3</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

		<!--end::Web font -->
		<!--begin::Global Theme Styles -->
		{!! HTML::style('/assets/css/style.bundle.css') !!}
        <!--begin::Web font -->
		<script>
			WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });

            function ConfirmPassword()
            {
                var pass = document.getElementById('password').value;
                var conf = document.getElementById('confirm_password').value;
                if(conf == pass){
                    document.getElementById("msg").style.color = "green";
                    document.getElementById("msg").style.margin = "10px";
                    document.getElementById("msg").style.float = "right";
                    document.getElementById('msg').innerHTML = "* پسورد مطابقت نمود";
                    document.getElementById("btn-submit").disabled = false;
                }else{
                    document.getElementById("msg").style.color = "red";
                    document.getElementById("msg").style.margin = "10px";
                    document.getElementById("msg").style.float = "right";
                    document.getElementById('msg').innerHTML = "* پسورد مطابقت نکرد. لطفآ دوباره کوشش نمایید";
                    document.getElementById("btn-submit").disabled = true;
                }
            }

            function checkLenghtPassword()
            {
                var pass = document.getElementById('password').value;
                var conf = document.getElementById('confirm_password').value;
                if(pass.length<6)
                {
                    document.getElementById("less").style.color = "red";
                    document.getElementById("less").style.margin = "10px";
                    document.getElementById("less").style.float = "right";
                    document.getElementById('less').innerHTML = "* پسورد حداقل باید شش حرف باشد.";
                    document.getElementById("btn-submit").disabled = true;
                }
                else
                {
                     document.getElementById('less').innerHTML = " ";
                }
            }
        </script>

        <style>
           .main_body
            {
                border: 1px solid white;
                background: white;
                margin-top: 20px;
                border-radius: 10px;
            }
            body, html  {
                background: linear-gradient(-70deg, green, black);
                /* Center and scale the image nicely */
                background-size: cover;
                margin: 0;
            }
        </style>
	</head>

	<!-- end::Head -->
     <body>
          <div class="container">
            <div class="row">
               <div class="col-md-10 offset-md-1 main_body">
               <div class=" col-md-6 offset-md-3">
                <form class="m-form m-form--fit m-form--label-align-right" style="direction:rtl;" method="POST" action="{{route('resetPassword')}}">
                    
                    <div class=" col-md-12">
                        <div class="form-group m-form__group row m-form__group_custom m--margin-top-50">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <img src="{!! asset('login-page/images/register.jpg') !!}" alt="IMG">
                                    <br><br>
                                    <h2 class="kt-subheader__title font-weight-bold">تغیر پسورد</h2>
                                </div>
                            </div>
                        </div>
                        <hr>
                        @foreach ($errors->all() as $msg)
                            <div class="alert alert-danger">{{$msg}}</div>
                        @endforeach
                        @if(Session::has("success"))
                            <div class="alert alert-success text-center">{!! Session::get('success')!!}</div>
                        @elseif(Session::has("fail"))
                            <div class="alert alert-danger text-center">{!! Session::get('fail')!!}</div>
                        @endif
          
                        <div class="form-group m-form__group row m-form__group_custom">
                            <div class="col-lg-12">
                                <label for="password" class="float-right font-weight-bold" style="font-size: 1.2rem;">پسورد جدید :</label>
                                <input type="password" class="form-control m-input m-input--air m-input--pill" id="password" name="password" onkeyup="checkLenghtPassword()" placeholder="پسورد">
                                <span id="less"><span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row m-form__group_custom">
                            <div class="col-lg-12">
                                <label for="confirm_password" class="float-right font-weight-bold" style="font-size: 1.2rem;">تائید پسورد :</label>
                                <input type="password" class="form-control m-input m-input--air m-input--pill" id="confirm_password" name="confirm_password" placeholder="تائید پسورد" onkeyup="ConfirmPassword()">
                                <span id="msg"><span>
                            </div>
                        </div>
                        {{-- Hidden Input --}}
                        <input type="hidden" name="emp_id" value="{{Crypt::encrypt($employee->id)}}">
                        <input type="hidden" name="username" value="{{Crypt::encrypt($employee->email)}}">
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button type="submit" class="btn btn-info" id="btn-submit" disabled> تغیر </button>
                                <a class="btn btn-secondary" href="{{route('getLogin')}}">برگشت به صفحه ورودی</a>
                            </div>
                        </div>
                    </div>
                </form>
               </div>
               </div>
            </div>
        </div>  
     </body>
	<!-- end::Body -->
</html>