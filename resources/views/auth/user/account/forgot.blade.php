<!DOCTYPE html>
<html lang="en">
<head>
  <title>AOP MIS Login Page</title>
  <style>
    ul li {list-style-type: square;}
    .return_back:hover {
        border: 1px solid green;
        padding: 10px 22px;
        border-radius: 4px;
    }
  </style>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<!--===============================================================================================-->  
  <link rel="shortcut icon" href="../../images/favicon.png">
<!--===============================================================================================-->
  {!! HTML::style('login-page/vendor/bootstrap/css/bootstrap.min.css') !!}
<!--===============================================================================================-->
  {!! HTML::style('login-page/fonts/font-awesome-4.7.0/css/font-awesome.min.css') !!}
<!--===============================================================================================-->
  {!! HTML::style('login-page/vendor/animate/animate.css') !!}
<!--===============================================================================================-->  
  {!! HTML::style('login-page/vendor/css-hamburgers/hamburgers.min.css') !!}
<!--===============================================================================================-->
  {!! HTML::style('login-page/vendor/select2/select2.min.css') !!}
<!--===============================================================================================-->
  {!! HTML::style('login-page/css/util.css') !!}
  {!! HTML::style('login-page/css/main.css') !!}
<!--===============================================================================================-->
</head>
<body>
  
  <div class="limiter" >
    <div class="container-login100">
      <div class="wrap-login100">
        <div class="login100-pic js-tilt" data-tilt>
          <img src="{!! asset('login-page/images/login.jpg') !!}" alt="IMG">
        </div>
        <form class="login100-form validate-form" method="post" action="{!! URL::route('postChangePassword') !!}">
          <span class="login100-form-title" style="font-family: 'B Nazanin';direction: rtl; font-size: 23px;">
            تغیر پسورد
          </span>

          <div class="wrap-input100 validate-input" data-validate = "ایمیل آدرس ضروری میباشد.">
            <input type="text" class="input100" name="email" placeholder="ایمیل">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-envelope" aria-hidden="true"></i>
            </span>
          </div>
          <div class="container-login100-form-btn" >
            <button class="login100-form-btn" style="font-family: 'B Nazanin';">
            تغیر پسورد
            </button>
          </div>
          <div class="container-login100-form-btn mt-2">
            <a class="return_back" style="font-family: 'B Nazanin'; float:right;" href="{{route('getLogin')}}">برگشت به صفحه ورودی  <i class="fa fa-angle-double-right"></i></a>
          </div>
          @if(Session::has("success"))
                <br><div class="alert alert-success" style="direction: rtl">{!! Session::get('success')!!}</div>
          @elseif(Session::has("fail"))
                <br><div class="alert alert-warning" style="direction: rtl">{!! Session::get('fail')!!}</div>
          @endif
          </br>
          
          <div class="text-center p-t-136">
        
            <p>© AOP {!!date('Y')!!}. All RIGHTS RESERVED.</p>
            <div class="social">
              <a href="javascript:void(0)">
                <i class="icon bd-twitter" aria-hidden="true"></i>
              </a>
              <a href="javascript:void(0)">
                <i class="icon bd-facebook" aria-hidden="true"></i>
              </a>
              <a href="javascript:void(0)">
                <i class="icon bd-dribbble" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  

  
<!--===============================================================================================-->  
  {!! HTML::script('/login-page/vendor/jquery/jquery-3.2.1.min.js') !!}
<!--===============================================================================================-->
  {!! HTML::script('/login-page/vendor/bootstrap/js/bootstrap.min.js') !!}
<!--===============================================================================================-->
  {!! HTML::script('/login-page/vendor/tilt/tilt.jquery.min.js') !!}
  <script >
    $('.js-tilt').tilt({
      scale: 1.1
    })
  </script>
<!--===============================================================================================-->
  {!! HTML::script('/login-page/js/main.js') !!}
</body>
</html>

