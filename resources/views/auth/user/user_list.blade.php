@extends('layouts.master')
@section('head')
	<title>User Management</title>
@stop

@section('content')
<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		
		<ol class="breadcrumb pull-left">
			<li><a href="#">Dashboard</a></li>
			<li><a href="#">User Management</a></li>
		</ol>
		
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box ui-draggable ui-droppable">
			
			<div class="box-content">
				@if(Session::has('success'))
				<div class='alert alert-success'>{{Session::get('success')}}</div>

				@elseif(Session::has('fail'))
				<div class='alert alert-danger'>{{Session::get('fail')}}</div>
				 @endif
				 
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="user_list">
					<thead>
						<tr>
							<th>#</th>
							<th>Full Name</th>
							<th>Father Name</th>
							<th>Username</th>
							<th>Email</th>
							<th>Position</th>
							<th>Department</th>
							<th>Operations</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
				
				<div class="btn-group" role="group" style='padding:10px;'>
				  @if(canAdd('auth_user'))
				  	<a href="{{URL::route('getCreate')}}" class="btn btn-primary"><i class="fa fa-plus"></i> New User</a> 
			   	  @endif
			   </div>
			</div>
		</div>
	</div>
</div>
@stop
@section('footer-scripts')
<script type="text/javascript">
$('#user_list').dataTable( 
	{
		"bProcessing": true,
		"bServerSide": true,
		//"iDisplayLength": 2,
		"sAjaxSource": "{{URL::route('getUserData')}}",
		//"aaSorting": [[ 1, "desc" ]],
		// "aoColumns": [
		// { 'sWidth': '30px' },
		// { 'sWidth': '130px', 'sClass': 'center' },
		// { 'sWidth': '180px', 'sClass': 'center' },
		// { 'sWidth': '100px', 'sClass': 'center' },
		// { 'sWidth': '100px', 'sClass': 'center' },
		// { 'sWidth': '100px', 'sClass': 'center' },
		// { 'sWidth': '100px', 'sClass': 'center' },
		// { 'sWidth': '150px', 'sClass': 'center' }


		// ]
	}
); 
</script>
@stop
	
