@extends('layouts.master')
@section('head')
    <title>Create Application</title>
@stop

@section('content')
    
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Application Create</a></li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            
            <div class="box-content">
                
                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postCreateApp') !!}">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Code</label>
                        <div class="col-sm-4">
                            <input name='code' class="form-control"  type="text">
                            <span style='color:red'>
                            @if($errors->has("code"))
                                {!! $errors->first('code') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-4">
                            <input name='name' class="form-control" type="text">
                            <span style='color:red'>
                            @if($errors->has("name"))
                                {!! $errors->first('name') !!}
                            @endif
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Descriptions</label>
                        <div class="col-sm-4">
                            <textarea name='desc' id='desc' class="form-control" ></textarea>
                        </div>
                        
                    </div>

                    {!! Form::token() !!}
                    
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">Cancel</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@stop
