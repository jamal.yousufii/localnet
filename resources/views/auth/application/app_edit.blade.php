@extends('layouts.master')
@section('head')
    <title>Edit Application</title>
@stop

@section('content')
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Application Edit</a></li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            
            <div class="box-content">
                @foreach($details AS $dep_item)
                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postUpdateApp',$dep_item->id) !!}">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Code</label>
                        <div class="col-sm-4">
                            <input value='{!!$dep_item->code!!}' name='code' data-original-title="Tooltip for name" class="form-control" data-toggle="tooltip" data-placement="bottom" type="text">
                            <span style='color:red'>
                            @if($errors->has("code"))
                                {!! $errors->first('code') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-4">
                            <input value='{!!$dep_item->name!!}' name='name' data-original-title="Tooltip for name" class="form-control" data-toggle="tooltip" data-placement="bottom" type="text">
                            <span style='color:red'>
                            @if($errors->has("name"))
                                {!! $errors->first('name') !!}
                            @endif
                            </span>
                        </div>
                        
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Descriptions</label>
                        <div class="col-sm-4">
                            <textarea name='desc' id='desc' class="form-control" >{!!$dep_item->description!!}</textarea>
                        </div>
                        
                    </div>

                    {!! Form::token() !!}
                    
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">Cancel</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
@stop
