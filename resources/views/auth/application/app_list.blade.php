@extends('layouts.master')
@section('head')
	<title>Applications</title>
@stop

@section('content')
	
	<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		
		<ol class="breadcrumb pull-left">
			<li><a href="#">{!!_('left_dashboard')!!}</a></li>
			<li><a href="#">Application List</a></li>
		</ol>
		
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box ui-draggable ui-droppable">
			
			<div class="box-content">
				@if(Session::has('success'))
				<div class='alert alert-success'>{!!Session::get('success')!!}</div>

				@elseif(Session::has('fail'))
				<div class='alert alert-danger'>{!!Session::get('fail')!!}</div>
				 @endif
				<table class="table table-striped table-condensed table-bordered" id="app_list">
					<thead>
						<tr>
						   <th>#</th>
						   <th>Application Code</th>
						   <th>Application Name</th>
						   <th>Descriptions</th>
						   <th>Operation</th>
						   
						</tr>
					</thead>
					<tbody>
							<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							
							</tr> 
					</tbody>
				</table>
				<div class="btn-group" role="group" style='padding:10px;'>
					@if(canAdd('auth_application'))
				  		<a href="{!!URL::route('getCreateApp')!!}" class="btn btn-primary"><i class="fa fa-plus"></i> New Application</a> 
				  	@endif
			   </div>
			</div>
		</div>
	</div>
</div>
@stop
@section('footer-scripts')
<script type="text/javascript">
$('#app_list').dataTable( 
	{
		"bProcessing": true,
		"bServerSide": true,
		//"iDisplayLength": 2,
		"sAjaxSource": "{!!URL::route('getAllAppData')!!}",
		"aaSorting": [[ 1, "desc" ]]
	}
); 
</script>
@stop
