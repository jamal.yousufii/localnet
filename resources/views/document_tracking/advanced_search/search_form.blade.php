@extends('layouts.master')

@section('head')
	@parent
	<title>گزارش دهی اجراآت اسناد وزارتها/ ادارات</title>
    <style type="text/css">
        a{cursor:pointer;}
        .fixed{width: 11%;}
        table th { font-size: 16px;font-weight: bold !important;text-align: center; }
        table tr { color: #000 !important; }
        table tr td last-child { border-right: 1px; }
        #content {white-space: nowrap; overflow-y: auto; overflow-x: scroll; margin: 10px; direction: rtl;}
    </style>
@stop

@section('content')

@if(Session::has('success'))
<div class="alert alert-success alert-dismissible" role="alert">
  <button class="close" aria-label="Close" data-dismiss="alert" type="button"> <span aria-hidden="true">×</span> </button>
  {!!Session::get('success')!!} </div>
@elseif(Session::has('fail'))
<div class="alert alert-danger alert-dismissible" role="alert">
  <button class="close" aria-label="Close" data-dismiss="alert" type="button"> <span aria-hidden="true">×</span> </button>
  {!!Session::get('fail')!!} </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <h3 id="form_title">بخش گزارش دهی اجراآت اسناد واصله وزارت ها/ ادارات</h3>
        </div>
        <hr style="border: 1px dashed #b6b6b6" />
    </div>
    <div style="margin: 20px">
        <form role="form" id="search_form" class="form-horizontal" method="post" action="{!!URL::route('exportIncomingIssuedDocsToExcel')!!}">

            <div class="form-group">

                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">نوعیت سند</label>
                    </div>
                    <div class="col-sm-12">
                        <select class="form-control" name="doc_type">
                          <option value="">نوعیت سند را انتخاب کنید</option>
                          {!!getMEDocumentType();!!}
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="col-sm-12">
                      <label class="col-sm-12 ">شماره سند</label>
                    </div>
                    <div class="col-sm-12">
                        <input type="number" class="form-control" name="doc_number" placeholder="شماره سند" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="col-sm-12">
                      <label class="col-sm-12 ">تاریخ سند</label>
                    </div>
                    <div class="col-sm-12">
                        <input type="text" class="form-control datepicker_farsi" name="doc_date" placeholder="تاریخ سند" readonly="readonly" />
                    </div>
                </div>
                <div id="pendings">
                    <div class="col-sm-3">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">شماره وارده</label>
                        </div>
                        <div class="col-sm-12">
                            <select name="incoming_number" id="incoming_number" class="form-control" style="width:100%" data-plugin="select2">
                                <option value="">انتخاب شماده وارده</option>
                                {!!getAllIncomingNumber()!!}
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <div class="form-group">

                <div class="col-sm-3">
                    <div class="col-sm-12">
                      <label class="col-sm-12 ">تاریخ وارده</label>
                    </div>
                    <div class="col-sm-12">
                        <input type="text" class="form-control datepicker_farsi" name="incoming_date" placeholder="تاریخ وارده" readonly="readonly" />
                    </div>
                </div>  
                <div class="col-sm-3">
                    <div class="col-sm-12">
                      <label class="col-sm-12 ">شماره صادره</label>
                    </div>
                    <div class="col-sm-12">
                      <input type="number" class="form-control" name="issue_number" value="{!!old('issue_number')!!}" placeholder="شماره صادره" />
                    </div>
                </div>    
                <div class="col-sm-3">
                    <div class="col-sm-12">
                      <label class="col-sm-12 ">تاریخ صادره</label>
                    </div>
                    <div class="col-sm-12">
                      <input type="text" class="form-control datepicker_farsi" name="issue_date" value="{!!old('issue_date')!!}" placeholder="تاریخ صادره" readonly="readonly" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="col-sm-12">
                      <label class="col-sm-12 ">کاپی سوم</label>
                    </div>
                    <div class="col-sm-12">
                      <input type="number" class="form-control" name="third_copy" value="{!!old('third_copy')!!}" placeholder="{!!_('کاپی سوم')!!}" />
                    </div>
                </div>

            </div>

            <div class="form-group"> 

                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">مرسل</label>
                    </div>
                    <div class="col-sm-12">
                        <select class="form-control" name="sender" id="sender" style="width: 100%">
                            <option value="">انتخاب مرسل</option>
                            {!!getOrganizations()!!}    
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">مرسل الیه</label>
                    </div>
                    <div class="col-sm-12">
                        <select class="form-control" name="sent_to">
                            <option value="">انتخاب مرسل الیه</option>
                            <option value="{{getUserDepartment(Auth::user()->id)->department}}">{{getDepartmentName(getUserDepartment(Auth::user()->id)->department,getLanguage())}}</option>
                        </select>
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">موضوع</label>
                    </div>
                    <div class="col-sm-12">
                        <textarea cols="20" rows="2" class="form-control" name="subject" placeholder="موضوع"></textarea>
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">کاپی به مراجع</label>
                    </div>
                    <div class="col-sm-12">
                        <select class="form-control" name="reference_copy" id="reference_copy" style="width: 100%">
                            <option value="">انتخاب کاپی به مراجع</option>
                            {!!getOrganizations()!!}    
                        </select>
                    </div>
                </div>

            </div>

            <div class="form-group">

                <div class="col-sm-3">
                    <div class="col-sm-12">
                      <label class="col-sm-12 ">تعداد اوراق</label>
                    </div>
                    <div class="col-sm-12">
                      <input type="number" class="form-control" name="number_of_papers" placeholder="تعداد اوراق" />
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="col-sm-12">
                      <label class="col-sm-12 ">کارتن مربوطه</label>
                    </div>
                    <div class="col-sm-12">
                      <input type="text" class="form-control" name="related_carton" value="{!!old('related_carton')!!}" placeholder="کارتن مربوطه" />
                    </div>
                </div> 
                @if(isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs') || isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
                <div class="col-sm-3">
                    <div class="col-sm-12">
                      <label class="col-sm-12 ">کارشناس مؤظف</label>
                    </div>
                    <div class="col-sm-12">
                        <select class="form-control" name="assignee" id="assignee" style="width: 100%">
                            <option value="">انتخاب کارشناس</option>
                            {!!getMEEXperts()!!}
                        </select>
                    </div>
                </div> 
                @endif
                <div class="col-sm-3">
                    <div class="col-sm-12">
                      <label class="col-sm-12 ">عملیات</label>
                    </div>
                    <div class="col-sm-12">
                        <select class="form-control" name="operations">
                          <option value="">انتخاب</option>
                          <option value="1">حفظ شده</option>
                          <option value="2">صادر شده</option>
                        </select>
                    </div>
                </div> 
            </div>
            {!! Form::token() !!}
            <div class="form-group">
                <div class="col-sm-3">
                    <div class="col-sm-12">
                      <label class="col-sm-12 ">تعداد اسناد جهت نمایش در صفحه</label>
                    </div>
                    <div class="col-sm-12">
                        <select class="form-control" name="number_of_records_per_page">
                            <option value="">تعداد اسناد جهت نمایش در صفحه</option>
                            <option value="3">3</option>
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="30">30</option>
                        </select>
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="col-sm-12">
                      <label class="col-sm-12 ">انتخاب سال</label>
                    </div>
                    <div class="col-sm-12">
                        <select class="form-control" name="year" id="year" style="width: 100%">
                            {!!getJalaliYears();!!}
                        </select>
                    </div>
                </div> 
                <div class="col-sm-6" style="margin-top: 27px;">
                    <button class="btn btn-warning" id="search_documents" type="submit">
                        <span>
                            <i class="fa fa-search"></i>
                        </span>
                        &nbsp;جستجو
                    </button>
                    &nbsp;<input type="reset" value="پاک کردن" id="searchclear" class="btn btn-danger"/>
                    &nbsp;&nbsp;
                    <button type="submit" class="btn btn-success" id="print_to_excel" >چاپ به صفحه اکسل</button>
                </div>
            </div>
        </form>
    </div>
    <hr style="border: 1px dashed;">
    <div id="search_result"></div>

</div>

@stop
@section('footer-scripts')
{!! HTML::script('/js/autocomplete/jquery-ui.js')!!}

<script type="text/javascript">

    $("#incoming_number").select2();
    $("#sender").select2();
    $("#assignee").select2();
    $("#reference_copy").select2();
    $("#year").select2();

    $(function(){
        $("#searchclear").click(function(){
          $("#incoming_number").select2('val', '');
          $("#sender").select2('val', '');
          $("#assignee").select2('val', '');
          $("#reference_copy").select2('val', '');
          $("#year").select2('val', '');
        });
    });

    $('#search_documents').click(function(){
        var datastring = $('#search_form').serialize();
        $.ajax({
            type: 'POST',
            url: '{!!URL::route("postDocSearch")!!}',
            data: datastring,
            beforeSend: function(){
                $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#search_result').html(response);
            }
        });
        return false;
    });

    $("#year").on("change", function(){
        var year = $("#year").val();
        $.ajax({
            type: 'POST',
            url: '{!!URL::route("getYearlyIncomingNumbers")!!}',
            data: "year="+year,
            beforeSend: function(){
                $('#pendings').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#pendings').html(response);
            }
        });
        return false;
    });

</script>


@stop