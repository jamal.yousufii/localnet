
<span style="font-weight: bold;font-size: 20px">مجموع اسناد واصله وزارت ها/ ادارات : {!!getTotalOfIncomingDocs(@$year)!!} </span>
<span style="margin-right: 2em;font-weight: bold;font-size: 20px">مجموع اسناد تحت اجراء : {!!getTotalOfDocsBasedOnParameters(0,@$year)!!} </span>
<span style="margin-right: 2em;font-weight: bold;font-size: 20px">مجموع اسناد اجراء شده : {!!getTotalOfDocsBasedOnParameters(1,@$year)!!} </span>
<div id="content" style="margin-top: 12px">
	<table class="table table-bordered table-responsive">
		<thead>
	      <tr>
	        <th>شماره</th>
	        <th>نوعیت سند</th>
	        <th>چگونگی اجراآت</th>
	        <th>شماره سند</th>
	        <th>تاریخ سند</th>
	        <th>مرسل</th>
	        <th>مرسل الیه</th>
	        <th>شماره وارده</th>
	        <th>تاریخ وارده</th>
	        <th>موضوع</th>
	        <th>تعداد اوراق</th>
	        <th>معیاد اجراء</th>
	        <th>کارشناس مؤظف</th>
	        <th>توضیحات کارشناس</th>
	        <th>وضعیت سند</th>
	        <!-- <th>{!!_('assignee')!!}</th> -->
	        <!-- <th>{!!_('assignee_description')!!}</th> -->

	        <th colspan="3" class="noprint">عملیات</th>
	        
	      </tr>
	    </thead>

	    <tbody>
	    	@if(!empty($records))
	    	<?php $counter = $records->firstItem(); ?>
	            @foreach($records AS $item)
	            	<?php
	            		$record_id = Crypt::encrypt($item->id);
	            		$assignee = getAssigneeName(getAssignee($item->id));
	            		$assignee_description = getAssigneeDescription($item->id);
	            		$assignee_description = wordwrap($assignee_description, 150, "<br />");
	            		$ministries = wordwrap($item->ministries, 70, "<br />");
	            		$subject = wordwrap($item->subject, 150, "<br />"); 
	            		if($item->execution_date != "0000-00-00" && strtotime(date('y-m-d')) > strtotime($item->execution_date) && $item->doc_status == 0)
	            			$bg_color = "background:red";
            			elseif(isMEDirector('document_tracking_incoming_docs','m&e_director_incoming_docs') && checkGregorianEmtpyDate($item->execution_date) == "")
            				$bg_color = "background:yellow";
            			elseif(isMEExpert('document_tracking_incoming_docs','m&e_expert_incoming_docs') && $assignee_description == "")
            				$bg_color = "background:yellow";
            			else
	            			$bg_color = "";
	            	?>
            		<tr class="remove_record{!!$item->id!!}" style={!!$bg_color!!}>
		                <td>{!!$counter!!}</td>
		                @if(getMEDocumentTypeName($item->doc_type) != "")
		                <td>{!!getMEDocumentTypeName($item->doc_type)!!}</td>
		                @else
		                <td></td>
		                @endif
		                @if($item->execution_type == 1)
		                <td><?= "عادی"?></td>
		                @elseif($item->execution_type == 2)
		                <td><?= "تفصیلی"?></td>
		                @else
		                <td></td>
		                @endif
		                <td>{!!$item->doc_number!!}</td>
		                <td>{!! dmy_format(toJalali($item->doc_date))!!}</td>
		                <td>{!!$ministries!!}</td>
		                <td>{!!getDepartmentName($item->sent_to,Session::get('lang'))!!}</td>
		                <td>{!!$item->incoming_number!!}</td>
		                <td>{!!dmy_format(toJalali($item->incoming_date))!!}</td>
		                <td>{!!$subject!!}</td>
		                <td>{!!$item->number_of_papers!!}</td>
		                <td>{!!dmy_format(toJalali($item->execution_date))!!}</td>
		                @if(!empty(getAssignee($item->id)))
		                <td>
		                	{!!$assignee!!}
			            </td>
			            @else
			            <td></td>
			            @endif
		                <td>{!!$assignee_description!!}</td>
		                @if($item->doc_status == 0)
		                <td>تحت اجراء</td>
		                @elseif($item->doc_status == 1)
		                <td>اجراء</td>
		                @endif
		                <?php $record_id = Crypt::encrypt($item->id);?>
	                	@if($item->doc_status != 1 && isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
	                		<!-- if the document is not completed by director or deputy director and expert, then do not allow the executive manager to save or issue the document -->
	                		@if($item->execution_type!="" && $item->execution_date!="0000-00-00" && getAssignee($item->id) != "" && getAssigneeDescription($item->id) != "")
	                			<td align='center' class="noprint"><a href="{!!URL::route('saveDoc',$record_id)!!}" title="ذخیره کردن این سند"><span class='fa icon-export'><span style="font-weight: bold;font-size: 17px;color:green">حفظ کردن سند</span></span></a></td>
	                		@else
	                		<td><span style="font-weight: bold;font-size: 15px">سند نا تکمیل میباشد</span></td>
	                		@endif
	                	@elseif(getOperation($item->id) == 1)
	                	<td><span style="font-weight: bold;font-size: 15px">حفظ شده</span></td>
	                	@elseif(getOperation($item->id) == 2)
	                	<td><span style="font-weight: bold;font-size: 15px">صادر شده</span></td>
	                	@else
	                	<td><span style="font-weight: bold;font-size: 15px">تحت اجراء</span></td>
	                	@endif
		                <td align='center' class="noprint"><a href="{!!URL::route('editIncomingDoc',$record_id)!!}" title="اصلاح ساختن این سند"><span class='fa fa-edit'></span></a></td>
		                @if(isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
		                	<td align='center' class="noprint"><a onclick="delteDocument(this.id)" id="{!!$item->id!!}" title='حذف کردن این سند'><span class='fa fa-trash'></span></a></td>
		                @endif
	                </tr>
	                <?php $counter++; ?>
	            @endforeach
	        @else
	        <div style="padding: 10px" class="noprint">
	        	<tr><td colspan="20" style="color:red">موردی پیدا نشد !</td></tr>
	        </div>
	    	@endif
	    </tbody>
	</table>
</div>
<script type="text/javascript">

	$(document).ready(function() {
    	$('td.text').each(function() {
	        var td = $(this);
	        var cs = td.text().length;
	        
	        if(cs>50)
	        {
	        	var shown = td.text().substring(0, 50);
	        	td.text(shown+'...');
	        }
	    });
	});

</script>