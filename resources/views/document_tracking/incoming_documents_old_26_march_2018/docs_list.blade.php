@extends('layouts.master')

@section('head')
    @parent
    <title>اسناد وزارت ها/ ادارات</title>
    <style type="text/css">
      a
      {
        cursor: pointer;
      }
      #content {white-space: nowrap; overflow-y: auto; overflow-x: scroll; direction: rtl;}
      table th { font-size: 16px;font-weight: bold !important;text-align: center; }
      table tr { color: #000 !important; }
    </style>
    {!! HTML::style('/css/print.css', array('media' => 'print')) !!}
    
@stop


@section('content')


  @if (count($errors) > 0)
  <div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
    <strong>Whoops !</strong>There were some problems with your input, please check it and try again. <br><br>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if(Session::has('success'))
  <div class='alert alert-success noprint'>{{Session::get('success')}}</div>

  @elseif(Session::has('fail'))
  <div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
   @endif
  <div class="row col-md-12" style="margin-top:-30px;margin-bottom: 30px">
    <div class="col-md-5 noprint" style="margin-top: 10px">
        <div id="content-header" class="clearfix">
            <div>
                <center>
                <h4>
                د افغانستان اسلامی جمهوریت</br>
                دجمهوری ریاست دلوړ مقام د دفتر لوی ریاست</br>
                د پالیسی ، څارنی او بررسی معاونیت</br>
                د څارنی او ارزونی ریاست
                </h4></center>
            </div>
            
        </div>
    </div>
    <div class="col-md-2 noprint">
        {!! HTML::image('/img/logo.jpg', 'LOGO', array('width' => '130','height' => '125')) !!}
        
    </div>
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix" style="margin-top: 10px">
            <div>
                <center>
                <h4>
                جمهوری اسلامی افغانستان</br>
                ریاست عمومی دفتر مقام عالی ریاست جمهوری</br>
                معاونیت پالیسی, نظارت و بررسی</br>
                ریاست نظارت و ارزیابی
                </h4></center>
            </div>
            
        </div>
    </div>
  </div>
  <div class="row noprint" style="margin: 10px;">
    <h4 class="pull-right"><span style="font-weight: bold;font-size: 25px;">بخش اسناد واصله وزارت ها/ ادارات و توضیحات کارشناس</span></h4>
    @if(isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
    <!-- <a id="click" data-target="#exampleModalPrimary" data-toggle="modal" class="btn btn-success pull-left"><i class="icon fa-plus" aria-hidden="true"></i> اضافه کردن سند جدید</a> -->
    <a href="{!!URL::route('loadIncomingDocForm')!!}" class="btn btn-success pull-left"><i class="icon fa-plus" aria-hidden="true"></i> اضافه کردن سند جدید</a>
    @endif
  </div>
  <hr class="noprint" />
    <div id="deleted_result" class="noprint">
    </div>
    <form role="form" id="search_form" class="form-horizontal noprint" method="post" action="{!!URL::route('exportIncomingDocsToExcel')!!}">
      <div class="form-group">

        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">نوعیت سند را انتخاب کنید</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="doc_type">
              <option value="">نوعیت سند را انتخاب کنید</option>
              {!!getMEDocumentType();!!}
            </select>
          </div>
        </div> 
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">شماره سند</label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="doc_number" placeholder="شماره سند" />
          </div>
        </div>         
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تاریخ سند</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="doc_date" placeholder="تاریخ سند" readonly="readonly" />
          </div>
        </div>         
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">انتخاب مرسل</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="sender" id="sender" style="width: 100%">
              <option value="">انتخاب مرسل</option>
              {!!getOrganizations()!!}    
            </select>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">شماره وارده</label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="incoming_number" placeholder="شماره وارده" />
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تاریخ وارده</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="incoming_date" placeholder="تاریخ وارده" readonly="readonly" />
          </div>
        </div> 

      </div>
      {!!Form::token()!!}
      <div class="form-group">

        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">موضوع</label>
          </div>
          <div class="col-sm-12">
            <textarea class="form-control" name="subject" placeholder="موضوع"></textarea>
          </div>
        </div> 
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تعداد اوراق</label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="number_of_papers" placeholder="تعداد اوراق" />
          </div>
        </div> 
        @if(isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs') || isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">کارشناس</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="assignee" id="assignee" placeholder="کارشناس" style="width: 100%">
              <option value="">انتخاب کارشناس</option>
              {!!getMEExperts()!!}
            </select>
          </div>
        </div> 
        @endif
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">انتخاب وضعیت سند</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="doc_status">
              <option value="">انتخاب وضعیت سند</option>
              <option value="0">تحت اجراء</option>
              <option value="1">اجراء</option>
            </select>
          </div>
        </div> 
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">انتخاب عملیات</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="operations">
              <option value="">انتخاب عملیات</option>
              <option value="1">حفظ شده</option>
              <option value="2">صادر شده</option>
            </select>
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تعداد اسناد جهت نمایش در صفحه</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="number_of_records_per_page">
              <option value="">تعداد اسناد جهت نمایش در صفحه</option>
              <option value="3">3</option>
              <option value="5">5</option>
              <option value="10">10</option>
              <option value="15">15</option>
              <option value="20">20</option>
              <option value="25">25</option>
              <option value="30">30</option>
            </select>
          </div>
        </div> 
      </div>
      <div class="form-group">
        <div class="col-sm-4">
            <button class="btn btn-warning" id="search_docs" type="submit">
                <span>
                    <i class="fa fa-search"></i>
                </span>
                &nbsp;جستجو
            </button>
            &nbsp;<input type="reset" value="پاک کردن" id="searchclear" class="btn btn-danger"/>
            &nbsp;
            <button type="submit" class="btn btn-success" id="print_to_excel" >چاپ به صفحه اکسل</button>
            &nbsp;
            <!-- <a href="#" onclick="window.print()" class="btn btn-primary noprint">Print Page</a> -->
        </div>

      </div>
    </form>
	<hr class="noprint" />
  <span style="font-weight: bold;font-size: 20px">مجموع اسناد واصله وزارت ها/ ادارات : {!!getMEStatsBasedOnParameters('incoming_docs')!!} </span>
  <span style="margin-right: 2em;font-weight: bold;font-size: 20px">مجموع اسناد تحت اجراء : {!!getMEStatsBasedOnParameters('incoming_docs','doc_status','up')!!} </span>
  <span style="margin-right: 2em;font-weight: bold;font-size: 20px">مجموع اسناد اجراء شده : {!!getMEStatsBasedOnParameters('incoming_docs','doc_status',1)!!} </span>
  <div id="search_result" style="margin-top: 10px">
    {{--Bring the list table--}}
        @include('document_tracking.incoming_documents.list_table')
    {{--list table end--}}
    <div class="dataTables_paginate paging_simple_numbers noprint" id="list_paginate">
      @if(!empty($records))
      {!!$records->render()!!}
      @endif
    </div>
  </div>
</div>


@stop

@section('footer-scripts') 
<script type="text/javascript">

  $("#sender").select2();
  $("#doc_type").select2();
  $("#doc_category").select2();
  $("#assignee").select2();

  $(function(){
    $("#searchclear").click(function(){
      $("#sender").select2('val', '');
      $("#doc_type").select2('val', '');
      $("#doc_category").select2('val', '');
      $("#assignee").select2('val', '');
    });
  });

  // $("#document_type").select2();

	$('.pagination a').on('click', function(event) {
    event.preventDefault();
    if ($(this).attr('href') != '#') {
      //$('#ajaxContent').load($(this).attr('href'));
      var dataString = $('#search_form').serialize();
      dataString += "&page="+$(this).text()+"&ajax="+1;
      $.ajax({
          url: '{!!URL::route("getIncomingDocsList")!!}',
          data: dataString,
          type: 'get',
          beforeSend: function(){
              //$("body").show().css({"opacity": "0.5"});
              $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#search_result').html(response);
          }
      }
    );
    
    }
  });

  function delteDocument(record_id)
  {
      if (confirm("آیا موافق هستید ؟ سند صادره که مربوط این سند وارده میشود نیز از سیستم حذف خواهد گردید ")) 
      {
          $.ajax({
            url : '{!!URL::route("deleteIncomingDoc")!!}',
            type : 'post',
            data : {'record_id':record_id,'_token':"{!!csrf_token()!!}"},
            success : function(response)
            {
              $("#deleted_result").html(response);
              $(".remove_record"+record_id).css('background','Crimson');
              $(".remove_record"+record_id).slideUp('6000', function(){
              $(this).remove();
              });

            }
          })
      }
  }

  //get the document list based on form data.
  $('#search_docs').click(function(){
      var datastring = $('#search_form').serialize();
      $.ajax({
          type: 'POST',
          url: '{!!URL::route("searchIncomingDocs")!!}',
          data: datastring,
          beforeSend: function(){
              $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#search_result').html(response);
          }
      });
      return false;
  });

	$(function(){     
        
      // repeat the input fields ===================================
      var max_fields      = 5; //maximum input boxes allowed
      var wrapper         = $(".input_fields_wrap"); //Fields wrapper
      var add_button      = $(".add_field_button"); //Add button ID
     
      var x = 1; //initlal text box count
      $(add_button).click(function(e){ //on add input button click
          e.preventDefault();
          if(x < max_fields){ //max input box allowed
              x++; //text box increment
              $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;display:inline-block;margin:-20px 0 0 5px" title="remove"> X </a></div>'); //add input box
          }
      });

      $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
          $('#add').fadeIn("slow");
          $('#appendedInputButton').fadeIn("slow");
          e.preventDefault(); $(this).parent('div').remove(); x--;
      });

  });

</script> 
@stop