@extends('layouts.master')
@section('content')
  <div class="page-head">
    <h3 dir="rtl">فورمه اضافه کردن سند وارده جدید</h3>
    <hr>
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
    <strong>Whoops !</strong>There were some problems with your input, please check it and try again. <br><br>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if(Session::has('success'))
  <div class='alert alert-success noprint'>{{Session::get('success')}}</div>

  @elseif(Session::has('fail'))
  <div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
  @endif
  <div class="cl-mcont" id="sdu_result" dir="rtl">
    <form class="form-horizontal" role="form" method="post" action="{!!URL::route('saveIncomingDoc')!!}" enctype="multipart/form-data" id="modal_form">
      <div class="form-group">

        <div class="col-sm-3">
          <div class="col-sm-12">
              <label class="col-sm-12 ">نوعیت سند <span style="color: red"> * </span></label>
          </div>
          <div class="col-sm-12">
              <select class="form-control" name="doc_type" required="required">
                  <option value="">نوعیت سند را انتخاب کنید</option>
                  {!!getMEDocumentType();!!}
              </select>
          </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">شماره سند <span style="color: red"> * </span></label>
            </div>
            <div class="col-sm-12">
                <input type="number" class="form-control" name="doc_number" value="{!!old('doc_number')!!}" required="required" />
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">تاریخ سند <span style="color: red"> * </span></label>
            </div>
            <div class="col-sm-12">
                <input type="text" class="form-control datepicker_farsi" name="doc_date" value="{!!old('doc_date')!!}" readonly="readonly" />
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12">
                <label class="col-sm-12 ">شماره وارده <span style="color: red"> * </span></label>
            </div>
            <div class="col-sm-12">
                <input type="number" name="incoming_number" id="incoming_number" class="form-control" required="required" />
            </div>
        </div>

    </div>
    <div class="form-group">

        <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">تاریخ وارده <span style="color: red"> * </span></label>
            </div>
            <div class="col-sm-12">
                <input type="text" class="form-control datepicker_farsi" name="incoming_date" value="{!!old('incoming_date')!!}" readonly="readonly" />
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">مرسل <span style="color: red"> * </span></label>
            </div>
            <div class="col-sm-12">
              <select class="form-control" name="sender" id="sender" style="width: 100%" required="required">
                <option value="0">ندارد</option>
                {!!getOrganizations()!!}    
              </select>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">مرسل الیه <span style="color: red"> * </span></label>
            </div>
            <div class="col-sm-12">
              <select class="form-control" name="sent_to" required="required">
                <option value="ریاست نظارت و ارزیابی">ریاست نظارت و ارزیابی</option>
              </select>
            </div>
        </div> 
        <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">موضوع <span style="color: red"> * </span></label>
            </div>
            <div class="col-sm-12">
                <textarea cols="30" rows="4" class="form-control" name="subject" required="required">{!!old('subject')!!}</textarea>
            </div>
        </div> 
        <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">تعداد اوراق <span style="color: red"> * </span></label>
            </div>
            <div class="col-sm-12">
                <input type="text" class="form-control" name="number_of_papers" value="{!!old('number_of_papers')!!}" required="required" />
            </div>
        </div> 
      </div>
      <hr />
    	{!!Form::token();!!}
    	<div class="form-group" style="margin-left: -18em">
        <label class="col-sm-2 control-label"></label>
        <div class="col-sm-4">
          	<input type="submit" value="ثبت معلومات" class="btn btn-success" id="add_incoming_doc" />
          	<a href="{!!URL::route('getIncomingDocsList')!!}" class="btn btn-warning"> برگشت به لست <i class="fa fa-arrow-left"></i></a>
        </div>
	    </div>
    </form>
  </div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

  $("#sender").select2();

  $(function(){
    $("#searchclear").click(function(){
      $("#sender").select2('val', '');
    });
    $("#add_incoming_doc").submit(function()
    {
      $("#add_incoming_doc").prop('disabled', true);
    });
  });

</script> 

@stop