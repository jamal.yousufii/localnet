
@extends('layouts.master')
@section('head')
    @parent
    <title>احصائیه و گراف</title>
    <style type="text/css">
      span.dashboard { font-weight: bold;font-size: 18px;color: #000;background: white; }
    </style>
@stop

@section('content')
<div style="margin: 20px 2px 20px 2px">
  <div class="page-head">
    <div class="row">
        <h3 style="float:right">بخش احصائیه و گراف</h3>
        <div class="col-sm-6" style="float:left;margin-left: 15em">
            <div class="col-sm-6">
                <label class="col-sm-6">انتخاب سال</label>
                <select class="form-control" name="year" id="year" style="width: 100%">
                    {!!getJalaliYears();!!}
                </select>
            </div>
        </div> 
    </div>
</div>
<div id="search_result">
    <hr style="border: dashed 1px">
    <div align="center">
        <span class="dashboard">مجموع اسناد واصله وزارت ها/ ادارات : {!!getTotalOfIncomingDocs()!!} </span>
        <span class="dashboard" style="margin-right: 3em">مجموع اسناد صادره واصله وزارت ها/ ادارات : {!!getMEIssuedDocumentStats(1)!!} </span>
        <span class="dashboard" style="margin-right: 3em">مجموع اسناد صادره پیگیری : {!!getMEIssuedDocumentStats(2)!!} </span>
        <span class="dashboard" style="margin-right: 3em">مجموع اسناد حفظ شده : {!!getMESavedDocumentStats()!!} </span>
    </div>
    <div align="center" style="margin-top: 1em">
        <span class="dashboard" style="margin-right: 3em">مجموع اسناد تحت اجراء : {!!getTotalOfDocsBasedOnParameters(0)!!} </span>
        <span class="dashboard" style="margin-right: 3em;">مجموع اسناد اجراء شده : {!!getTotalOfDocsBasedOnParameters(1)!!} </span>
    </div>
    <hr style="border: dashed 1px">
    <div class="form-group" style="margin-top: 2em !important;">
        <div class="col-md-6">
          <div id="makatib_graph" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
        </div>
        <div class="col-md-6">
          <div id="peshnehadat_graph" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
        </div>
    </div>
    <hr style="border: 1px dashed" />
    <div class="form-group" style="margin-top: 2em !important;">
        <div class="col-md-6">
          <div id="hedayat_graph" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
        </div>
        <div class="col-md-6">
          <div id="hedayat_report_graph" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
        </div>
    </div>
    <hr style="border: 1px dashed" />
    <div class="form-group" style="margin-top: 2em !important;">
        <div class="col-md-12">
          <div id="other_reports_graph" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
        </div>
    </div>
    <hr style="border: 1px dashed" />
    <div class="form-group">
        <div class="col-md-12">
          <div id="issued_doc_without_incoming_graph" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
        </div>
    </div>
    <hr style="border: 1px dashed" />
    <div class="form-group">
        <div class="col-md-12">
          <div id="total_incoming_docs_graph" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
        </div>
    </div>
</div>
</div>

@stop

@section('footer-scripts') 
{!! HTML::script('/js/highcharts/highcharts.js') !!}
{!! HTML::script('/js/highcharts/highcharts-3d.js') !!}
{!! HTML::script('/js/highcharts/exporting.js') !!}
<script type="text/javascript">

$("#year").select2();

$(function(){
    $("#searchclear").click(function(){
        $("#year").select2('val', '');
    });
});

$(function () {
  // Create the chart
    $('#makatib_graph').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: "گراف مکاتیب"
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ""
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: "{point.y}"
                }
            }
        },

        tooltip: {
          pointFormat: '<span style="color:{point.color}"></span>: <b>{point.y}</b><br/>'
        },

        series: [{
            colorByPoint: true,
            data: [{
                name: "مجموع مکاتیب",
                y: {!!$total_makatib!!},
            },{
                name: "تحت اجراء",
                y: {!!$under_process_makatib!!},
                drilldown: "مجموع مکاتیب"
            },{
                name: "حفظ شده",
                y: {!!$saved_makatib!!},
                drilldown: "مجموع پیشنهادات"
            }, {
                name: "صادر شده",
                y: {!!$issued_makatib!!},
                drilldown: "مجموع هدایات"
            }]
        }]
    });

    // Create the chart
    $('#peshnehadat_graph').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: "گراف پیشنهادات"
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ""
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: "{point.y}"
                }
            }
        },

        tooltip: {
            pointFormat: "<span style='color:{point.color}'></span>: <b>{point.y}</b><br/>"
        },

        series: [{
            colorByPoint: true,
            data: [{
                name: "مجموع پیشنهادات",
                y: {!!$total_peshnehadat!!},
            },{
                name: "تحت اجراء",
                y: {!!$under_process_peshnehadat!!},
            },{
                name: "حفظ شده",
                y: {!!$saved_peshnehadat!!},
            },{
                name: "صادر شده",
                y: {!!$issued_peshnehadat!!},
            }]
        }]
    });

    // Create the chart
    $('#hedayat_graph').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: "گراف هدایات"
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ""
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: "{point.y}"
                }
            }
        },

        tooltip: {
            pointFormat: "<span style='color:{point.color}'></span>: <b>{point.y}</b><br/>"
        },

        series: [{
            colorByPoint: true,
            data: [{
                name: "مجموع هدایات",
                y: {!!$total_hedayaat!!},
            },{
                name: "تحت اجراء",
                y: {!!$under_process_hedayat!!},
            },{
                name: "حفظ شده",
                y: {!!$saved_hedayat!!},
            },{
                name: "صادر شده",
                y: {!!$issued_hedayat!!},
            }]
        }]
    });

    // Create the chart
    $('#hedayat_report_graph').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: "گراف گزارش از تحقق هدایت"
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ""
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: "{point.y}"
                }
            }
        },

        tooltip: {
            pointFormat: "<span style='color:{point.color}'></span>: <b>{point.y}</b><br/>"
        },

        series: [{
            colorByPoint: true,
            data: [{
                name: "مجموع گزارش تحقق هدایت",
                y: {!!$total_hedayat_report!!},
            },{
                name: "تحت اجراء",
                y: {!!$under_process_hedayat_report!!},
            },{
                name: "حفظ شده",
                y: {!!$saved_hedayat_report!!},
            },{
                name: "صادر شده",
                y: {!!$issued_hedayat_report!!},
            }]
        }]
    });

    // Create the chart
    $('#other_reports_graph').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: "گراف سایر گزارشات"
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ""
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: "{point.y}"
                }
            }
        },

        tooltip: {
            pointFormat: "<span style='color:{point.color}'></span>: <b>{point.y}</b><br/>"
        },

        series: [{
            colorByPoint: true,
            data: [{
                name: "مجموع سایر گزارشات",
                y: {!!$total_other_reports!!},
            },{
                name: "تحت اجراء",
                y: {!!$under_process_other_reports!!},
            },{
                name: "حفظ شده",
                y: {!!$saved_other_reports!!},
            },{
                name: "صادر شده",
                y: {!!$issued_other_reports!!},
            }]
        }]
    });

    // Create the chart
    $('#issued_doc_without_incoming_graph').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: "گراف اسناد صادره پیگیری"
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ""
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: "{point.y}"
                }
            }
        },

        tooltip: {
            pointFormat: "<span style='color:{point.color}'></span>: <b>{point.y}</b><br/>"
        },

        series: [{
            colorByPoint: true,
            data: [{
                name: "مجموع صادره",
                y: {!!$total_issued_docs_without_incoming!!},
            },{
                name: "مکتوب",
                y: {!!$issued_doc_maktoob_without_incoming!!},
            },{
                name: "پیشنهاد",
                y: {!!$issued_doc_peshnehad_without_incoming!!},
            },{
                name: "هدایت",
                y: {!!$issued_doc_hedayat_without_incoming!!},
            },{
                name: "گزارش تحقق هدایت",
                y: {!!$issued_doc_hedayat_report_without_incoming!!},
            },{
                name: "سایر گزارشات",
                y: {!!$issued_doc_other_reports_without_incoming!!},
            }]
        }]
    });

    // Create the chart
    $('#total_incoming_docs_graph').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: "گراف مجموع اسناد واصله وزارت ها/ ادارات"
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ""
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: "{point.y}"
                }
            }
        },

        tooltip: {
            pointFormat: "<span style='color:{point.color}'></span>: <b>{point.y}</b><br/>"
        },

        series: [{
            colorByPoint: true,
            data: [{
                name: "مجموع اسناد واصله وزارت ها/ ادارات",
                y: {!!$total_incoming_documents!!},
            },{
                name: "مکتوب",
                y: {!!$maktoob!!},
            },{
                name: "پیشنهاد",
                y: {!!$peshnehad!!},
            },{
                name: "هدایت",
                y: {!!$hedayat!!},
            },{
                name: "گزارش تحقق هدایت",
                y: {!!$hedayat_report!!},
            },{
                name: "سایر گزارشات",
                y: {!!$other_reports!!},
            },{
                name: "مجموع اسناد تفصیلی",
                y: {!!$total_of_descriptive_docs!!},
            },{
                name: "مجموع اسناد عادی",
                y: {!!$total_of_normal_docs!!},
            }]
        }]
    });

    $("#year").on("change", function(){
        var year = $("#year").val();
        $.ajax({
            type: 'POST',
            url: '{!!URL::route("searchDashboardData")!!}',
            data: "year="+year,
            beforeSend: function(){
                $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#search_result').html(response);
            }
        });
        return false;
    });

});
</script> 

@stop