<span style="font-weight: bold;font-size: 20px">مجموع اسناد صادر شده : {!!getMEIssuedDocumentStats(1,@$year)!!} </span>
<span style="margin-right: 3em;font-weight: bold;font-size: 20px">مجموع اسناد حفظ شده : {!!getMESavedDocumentStats(@$year)!!} </span>
<div id="content" style="margin-top: 12px">
	<table class="table table-bordered table-responsive">
		<thead>
	      <tr>
	        <th>شماره</th>
	        <th>شماره وارده</th>
	        <th>نوعیت سند</th>
	        <th>شماره صادره</th>
	        <th>تاریخ صادره</th>
	        <th>کاپی سوم</th>
	        <th>مرسل</th>
	        <th>مرسل الیه</th>
	        <th>موضوع</th>
	        <th>کاپی به مراجع</th>
	        <th>تعداد اوراق</th>
	        <th>کارتن مربوطه</th>
	        <th>کارشناس مؤظف</th>
	        <th>ضمایم</th>
	        <th colspan="3" class="noprint">عملیات</th>
	      </tr>
	    </thead>

	    <tbody>
	    	@if(!empty($records))
	    	<?php $counter = $records->firstItem(); ?>
	            @foreach($records AS $item)
	            	<?php
	            		$record_id = Crypt::encrypt($item->id); 
	            		$copy_to_references = getCopyToReferences($item->id);
	            		$subject = wordwrap($item->subject, 150, "<br />");
	            		$ministries = wordwrap($item->ministries, 70, "<br />");
	            	?>
	                <tr class="remove_record{!!$item->id!!}">
		                <td>{!!$counter!!}</td>
		                <td>{!!$item->incoming_number!!}</td>
		               	@if($item->incoming_doc_id != 0)
			                @if(getMEDocumentTypeName(getRecordFieldBasedOnID('incoming_docs','doc_type',$item->incoming_doc_id)) != "")
			                	<td>{!!getMEDocumentTypeName(getRecordFieldBasedOnID('incoming_docs','doc_type',$item->incoming_doc_id))!!}</td>
			                @else
			                	<td></td>
			                @endif
		                @else
		                	@if(getMEDocumentTypeName($item->issued_doc_type) != "")
		                		<td>{!!getMEDocumentTypeName($item->issued_doc_type)!!}</td>
			                @else
			                	<td></td>
			                @endif
		                @endif
		                <td>{!!$item->issuing_number!!}</td>
		                <td>{!! dmy_format(toJalali($item->issuing_date))!!}</td>
		                <td>{!!$item->third_copy!!}</td>
		                <td>{!!getDepartmentName($item->sender,getLanguage())!!}</td>
		                <td>{!!$ministries!!}</td>
		                <td>{!!$subject!!}</td>
		                @if(!empty($copy_to_references))
		                <td><ul>
			                @foreach($copy_to_references as $refere)
			                	<li>{!!getRecordFieldBasedOnID('ministries','name_dr',$refere->reference_id)!!}</li>
			                @endforeach
		            	</ul></td>
		            	@else
		            	<td></td>
		                @endif
		                <td>{!!$item->number_of_papers!!}</td>
		                <td>{!!$item->related_carton!!}</td>
		                @if($item->incoming_doc_id != 0)
		                <td>{!!getAssigneeName(getAssignee($item->incoming_doc_id))!!}</td>
			            @else
			            <td>{!!getAssigneeName(getIssuedDocAssignee($item->id))!!}</td>
			            @endif
		                <td>
		                	<ul class="widget-todo" style="margin-top:10px;list-style:none">
					            @if(!empty(getDocumentFileName($item->id)))
					                @foreach(getDocumentFileName($item->id) AS $attach)
					                <li class="clearfix" id="li_{!!$attach->id!!}" style="margin: -10px">
					                <?php $file_id = Crypt::encrypt($attach->id); $original_file_name = wordwrap($attach->original_file_name, 50, "<br />"); ?>
					                    <div class="name" style="margin-top:5px">
					                        <label for="todo-1">
					                       		<i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
					                          	<strong>{!!$original_file_name!!}</strong><br />
					                        </label>&nbsp;&nbsp;&nbsp;
					                        <a href="{!!URL::route('getDownloadDocFile',array($file_id))!!}" class="table-link success">
					                            <i class="fa fa-1x fa-cloud-download" style='color:#03a9f4;margin-right: -20px' title="download file"></i>
					                        </a>
					                    </div>                                    
					                </li>
					                @endforeach
					            @else
					                <li><span style='color:red;'>فایل ضمیمه آپلود نشده</span></li>
					            @endif
					        </ul>
					    </td>
		                
		                	<?php $record_id = Crypt::encrypt($item->id); ?>
			                @if($item->operations == 1)
			                <td><span style="font-weight: bold;font-size: 15px;color: green">حفظ شده</span></td>
			                <!-- <td align='center' class="noprint" id="approve_result_{!!$item->id!!}"><a onclick="approveSavedDoc(this.id, '{!!$item->incoming_doc_id!!}')" id="{!!$item->id!!}" title="Approve the saved document"> {!!_('approve')!!}</a></td> -->
				                @if(isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
				                <td align='center' class="noprint"><a href="{!!URL::route('editSavedDoc',$record_id)!!}" title="Edit Saved Document"><span class='fa fa-edit'></span></a></td>
				                @endif
			                @elseif($item->operations == 2)
			                <td><span style="font-weight: bold;font-size: 15px;color: green">صادر شده</span></td>
			                <!-- <td align='center' class="noprint"><span class='fa icon-export' style="color: green">{!!_('issued')!!}</span></td> -->
				                @if(isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
				                <td align='center' class="noprint"><a href="{!!URL::route('editIssuedDoc',$record_id)!!}" title="Edit Issued Document"><span class='fa fa-edit'></span></a></td>
				                @endif
			                @else
			                <td></td>
			                <!-- <td align='center' class="noprint"><span class='fa fa-check-square-o' style='color:green'> {!!_('approved')!!}</span></td> -->
			                @endif
		                	<!-- <td align='center' class="noprint"><a href="{!!URL::route('editSavedDoc',$record_id)!!}" title="Edit Saved Document"><span class='fa fa-edit'></span></a></td> -->
		                	@if(isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
		                	<td align='center' class="noprint"><a onclick="delteDocument(this.id)" id="{!!$item->id!!}" title='Delete Record'><span class='fa fa-trash'></span></a></td>
		                	@endif
	                </tr>
	                <?php $counter++; ?>
	            @endforeach
	        @else
	        <div style="padding: 10px" class="noprint">
	        	<tr><td colspan="20" style="color:red">موردی پیدا نشد !</td></tr>
	        </div>
	    	@endif
	    </tbody>
	</table>
</div>
<script type="text/javascript">

	$(document).ready(function() {
    	$('td.text').each(function() {
	        var td = $(this);
	        var cs = td.text().length;
	        
	        if(cs>50)
	        {
	        	var shown = td.text().substring(0, 50);
	        	td.text(shown+'...');
	        }
	    });
	});

</script>