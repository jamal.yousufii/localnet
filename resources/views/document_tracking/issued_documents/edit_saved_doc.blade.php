
@extends('layouts.master')
@section('content')
<div style="margin: 20px">
  <div class="page-head">
    <h3>{!!_('saved_document_edit_form')!!}</h3>
    <hr >
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
    <strong>Whoops !</strong>There were some problems with your input, please check it and try again. <br><br>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if(Session::has('success'))
  <div class='alert alert-success noprint'>{{Session::get('success')}}</div>

  @elseif(Session::has('fail'))
  <div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
  @endif
  <div class="cl-mcont" id="sdu_result">
    <form class="form-horizontal group-border-dashed" action="{!!URL::route('updateSavedDoc', $record->id)!!}" method="post" style="border-radius: 0px;" enctype="multipart/form-data">
      	
        <div class="form-group"> 

          <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">شماره وارده <span style="color: red"> * </span></label>
            </div>
            <div class="col-sm-12">
              <input type="number" class="form-control" name="incoming_number" value="{!!$record->incoming_number!!}" readonly="readonly" />
            </div>
          </div>
          <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">موضوع <span style="color: red"> * </span></label>
            </div>
            <div class="col-sm-12">
                <textarea cols="20" rows="3" class="form-control" name="subject" required="required">{!!$record->subject!!}</textarea>
            </div> 
          </div>
          <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">کارتن مربوطه <span style="color: red"> * </span></label>
            </div>
            <div class="col-sm-12">
              <input type="text" class="form-control" name="related_carton" required="required" value="{!!$record->related_carton!!}" />
            </div>
          </div> 
        {!!Form::token()!!}
          <div class="col-sm-3">
            <div class="col-sm-12">
              <label class="col-sm-12 ">ضمایم</label>
            </div>
            <div class="col-sm-12">
              <div class="input_fields_wrap">
                  <input type='file' id='files' style="width:87%;display:inline-block" name='files[]' class="form-control" multiple='multiple'>
                  <a class="add_field_button btn" id="add" style="background: green;color:white;display:inline-block;margin-top:-20px;" title="Add another file"> + </a>
              </div>
            </div>
          </div>
      </div>

      <hr style="border: 1px dashed;" />
      <div class="form-group main-box-body clearfix">
          <ul class="widget-todo" style="margin-top:10px;list-style:none">
            <h4>Attachments</h4>
              @if(!empty(getDocumentFileName($record->id)))
                  @foreach(getDocumentFileName($record->id) AS $attach)
          <li class="clearfix" id="li_{!!$attach->id!!}">
            <?php $file_id = Crypt::encrypt($attach->id); ?>
                        <div class="name" style="margin-top:5px">
                          <label for="todo-2">
                                <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
                                <strong>{!!$attach->original_file_name!!}</strong><br />
                            </label>&nbsp;&nbsp;&nbsp;
                <a href="{!!URL::route('getDownloadDocFile',array($file_id))!!}" class="table-link success">
                        <i class="fa fa-2x fa-cloud-download" style='color:#03a9f4;' title="download file"></i>
                      </a>
                      <button type="button" onclick="removeDocFile('{!!$attach->id!!}','{!!$attach->file_name!!}')" class="btn btn-danger" style="margin-top:-20px">
                        <i class="fa fa-trash" title="Remove file"></i>
                      </button>&nbsp;&nbsp;
                          <br />
                  
                        </div>                                    
          </li>
                  @endforeach
              @else
                  <li><span style='color:red;'>فایل ضمیمه آپلود نشده</span></li>
              @endif
          </ul>
              
      </div>

    	<hr />
    	<div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-sm-4" style="text-align: center">
          	<input type="submit" value="ثبت تغییرات" class="btn btn-success"/>
          	<a href="javascript:history.back()" class="btn btn-warning"> برگشت <i class="fa fa-arrow-left"></i></a>
        </div>
	    </div>
    </form>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

  function removeDocFile(file_id,file_name)
  { 
    var result = confirm("Are you sure you want to delete?");
    if (result) 
    {
      $.ajax({

        url : "{!!URL::route('getRemoveDocFile')!!}",
        type: "post",
        data : "file_id="+file_id+"&file_name="+file_name+"&_token="+"<?=csrf_token();?>",
        success : function(response)
        {
          if(response != 0)
          {
            $("#li_"+file_id).css('background','Crimson');
                      $("#li_"+file_id).slideUp('6000', function(){
                        $("#li_"+file_id).remove();
                      });
          }
        }

      });
      return false;
    }
  }

	$(function(){     
        
        // repeat the input fields ===================================
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
       
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;display:inline-block;margin:-20px 0 0 5px" title="remove"> X </a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            $('#add').fadeIn("slow");
            $('#appendedInputButton').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x--;
        });

    });

</script> 

@stop