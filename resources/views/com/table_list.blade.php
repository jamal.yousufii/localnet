
{!! HTML::style('/css/template/libs/dataTables.fixedHeader.css') !!}
{!! HTML::style('/css/template/libs/dataTables.tableTools.css') !!}
  

<div class="row">
	<div class="col-xs-12">
		<div class="box ui-draggable ui-droppable">
			<div class="box-header">
				
			@if(canAdd('com_received_docs'))	
				<div class="btn-group" role="group" style='padding:10px;'>
					<button class="md-trigger btn btn-primary mrg-b-lg" data-modal="modal-13" onclick="$('#add_static').show()">علاوه نمودن</button>		 	   
			   </div>
			@endif
			
			</div>
			<div id="edit_div"></div>
			<div class="row" id="add_static" style="display: none">
			    <div class="col-xs-12 col-sm-12">
			        <div class="box ui-draggable ui-droppable">
			            <div class="box-header">
			                <div class="box-name ui-draggable-handle">
			                    <i class="fa fa-files-o"></i>
			                    <span>علاوه نمودن ریکارد جدید</span>
			                </div>
			                
			                <div class="no-move"></div>
			            </div>
			            <div class="box-content">
			            	<form class="form-horizontal" role="form" method="post" id="insert_table_fields">
			                    <div class="form-group">
			                        <div class="col-sm-4">
			                            <input name='name_en' class="form-control" placeholder="نام انگلیسی" type="text" required="required">			                            
			                        </div>
			                        <div class="col-sm-4">
			                            <input name='name_dr' class="form-control" placeholder="نام دری" type="text">
			                            
			                        </div>
			                        <div class="col-sm-4">
			                            <input name='name_pa' class="form-control" placeholder="نام پشتو" type="text">
			                            
			                        </div>
			                    </div>
			                    <input type="hidden" name="table_name" value="{!!$table->table!!}" />
			                    
			                    {!! Form::token() !!}
                    
			                    <div class="form-group">			                        
			                        <div class="col-sm-9 col-sm-offset-3">
			                            <button type="button" class="btn btn-primary" onclick="save_table('insert_table_fields');load_table({!!$table->id!!})">ذخیره نمودن</button>
			                            <button type="button" class="btn btn-danger" onclick="$('#add_static').hide()">لغو نمودن</button>		                    
			                        </div>    
			                    </div>
			                 </form>
			            </div>
			        </div>
			    </div>
			</div>
			            

			<div class="box-content">
				@if(Session::has('success'))
				<div class='alert alert-success'>{!!Session::get('success')!!}</div>

				@elseif(Session::has('fail'))
				<div class='alert alert-danger'>{!!Session::get('fail')!!}</div>
				 @endif

				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="received_list">
					<thead>
						<tr>
						   <th>شماره مسلسل</th>
						   <th>نام انگلیسی</th>
						   <th>نام دری</th>
						   <th>نام پشتو</th>
						   <th>عملیه ها</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
				
			</div>
		</div>
	</div>
</div>


{!! HTML::script('/js/template/jquery.dataTables.js')!!}
{!! HTML::script('/js/template/dataTables.fixedHeader.js')!!}
{!! HTML::script('/js/template/dataTables.tableTools.js')!!}
{!! HTML::script('/js/template/jquery.dataTables.bootstrap.js')!!}
<script>
	function save_table(form)
	{
		var data = $('#'+form).serializeArray().reduce(function(obj, item) {
	    	obj[item.name] = item.value;
	    	return obj;
		}, {});
    	
		 $.ajax({
		 	url: "{!!route('postCreateStatic')!!}",
		 	data: data,
		 	type: 'POST',
		 	success:function(r){
		 		
		 	}
		 });
	}
	function edit_table(form)
	{
		var data = $('#'+form).serializeArray().reduce(function(obj, item) {
	    	obj[item.name] = item.value;
	    	return obj;
		}, {});
    	
		 $.ajax({
		 	url: "{!!route('postUpdateStatic')!!}",
		 	data: data,
		 	type: 'POST',
		 	success:function(r){
		 		
		 	}
		 });
	}
	function delete_table(id)
	{
		var table = '<?=$table->table?>';
		var table_id = '<?=$table->id?>';
		 $.ajax({
		 	url: "{!!route('getDeleteStatic')!!}",
		 	data: '&id='+id+'&table='+table,
		 	type: 'POST',
		 	success:function(r){
		 		load_table(table_id);
		 	}
		 });
	}
	function load_edit(id)
	{
		var table = '<?=$table->table?>';
		var table_id = '<?=$table->id?>';
		 $.ajax({
		 	url: "{!!route('getUpdateStatic')!!}",
		 	data: '&id='+id+'&table='+table+'&table_id='+table_id,
		 	type: 'POST',
		 	success:function(r){
		 		$('#edit_div').html(r);
		 	}
		 });
	}
</Script>
<script type="text/javascript">

$('#received_list').dataTable( 
	{
		"bProcessing": true,
		"bServerSide": true,
		//"iDisplayLength": 2,
		"sAjaxSource": "<?=URL::to('/com/getData_statictable/'.$table->table.'')?>",
		"aaSorting": [[ 1, "desc" ]],
		"aoColumns": [
		{ 'sWidth': '60px' },
		{ 'sWidth': '130px', 'sClass': 'center' },
		{ 'sWidth': '180px', 'sClass': 'center' },
		{ 'sWidth': '180px', 'sClass': 'center' },
		{ 'sWidth': '250px', 'sClass': 'center' }
		],
		"language": {
            "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
            "zeroRecords": "ریکارد موجود نیست",
            "info": "نمایش صفحه _PAGE_ از _PAGES_",
            "infoEmpty": "ریکارد موجود نیست",
            "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
        }
	}
);

</script>
