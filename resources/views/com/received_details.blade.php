@extends('layouts.master')
@section('head')
    <title>جزییات سند وارده</title>
@stop

@section('content')
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="{!!URL::to('/')!!}">Dashboard</a></li>
            <li><a href="{!!route('getReceived')!!}">Received Documents</a></li>
            <li>Received Document Details</li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            <div class="box-header">
                <div class="box-name ui-draggable-handle">
                    <i class="fa fa-files-o"></i>
                    <span>جزییات سند وارده</span>
                </div>
                
                <div class="no-move"></div>
            </div>
            <div class="main-box-body clearfix">
				<div class="table-responsive">
					<table class="table" width="100%">
						<tbody>
							<tr>
								<td width="20%">نام دوسیه</td>
								<td width="30%">{!!$details->dossier_name!!}</td>
								<td width="20%">کد</td>
								<td width="30%">{!!$details->code!!}</td>
							</tr>
							<tr>
								<td>نوعیت</td>
								<td>
									@foreach($doc_types AS $type)
		                            	@if($type->id == $details->type_id)
		                            		{!!$type->name!!}
		                            	@endif
		                            @endforeach
								</td>
								<td>شماره مکتوب</td>
								<td>{!!$details->number!!}</td>
							</tr>
							<tr>
								<td>تاریخ مکتوب</td>
								<td>{!!$details->date!!}</td>
								<td>ارجعیت موضوع</td>
								<td>
									@if($priority_types)
			                            @foreach($priority_types AS $type)
			                            	@if($type->id == $details->priority_id)
			                            		{!!$type->name!!}
			                            	@endif
			                            @endforeach
			                        @endif
								</td>
							</tr>
							<tr>
								<td>مرجع ارسال کننده</td>
								<td>
									@if($deps)
			                            @foreach($deps AS $dep)
			                            	@if($dep->id == $details->sender_id)
			                            		{!!$dep->name!!}
			                            	@endif
			                            @endforeach
			                        @endif
								</td>
								<td></td>
								<td></td>
							</tr>
							<?php $name_count = 1; ?>
		                    @if(count($names)>0)
		                    <tr><td colspan="4"><table class="table">
		                    	@foreach($names AS $name)
		                    		<tr>
				                    	<td>نام</td>
				                        <td>{!!$name->first_name!!}</td>
					                    <td>موقف</td>    
				                        <td>{!!$name->position!!}</td> 
				                    </tr>
				                    <tr>   
				                        <td>شماره تماس</td>
				                        <td>{!!$name->phone!!}</td>
					                    <td>ایمیل</td>
				                        <td>{!!$name->email!!}</td>					                                            
					                    </div>
					               </div>
				                    <?php $name_count++; ?>
				                @endforeach
				      		</table></td></tr>
		                    @endif
		                    <tr>
		                    	<td>خلص موضوع</td>
		                    	<td>{!!$details->summary!!}</td>
		                    	<td>یادداشت</td>
		                    	<td>{!!$details->note!!}</td>
		                    </tr>
		                    <!--
		                    <tr>
		                    	<td>توضیحات</td>
		                    	<td>{!!$details->desc!!}</td>
		                    	<td>راپور ماهوار</td>
		                    	<td>{!!$details->monthly_report!!}</td>
		                    </tr>
		                    -->
		                    @if(count($attachments)>0)
		                    <tr><td colspan="4"><table class="table">
								@foreach($attachments AS $file)
								<tr><td>
									<a href="{!!route('downloadAttachment_received',$file->file_name)!!}">{!!$file->file_name!!}</a>
								</td></tr>
								@endforeach
							
							</table></td></tr>
							@endif	
						</tbody>
					</table>
				</div>
			</div>
            <div class="box-content">
              
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="button" class="btn btn-danger" onclick="history.back()">برگشت به لیست</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@stop
