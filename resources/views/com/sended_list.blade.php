@extends('layouts.master')
@section('head')
	<title>لیست اسناد صادره</title>
	
	<style>
		.dir{
			direction:auto;
		}
	</style>
@stop

@section('content')
	
	<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		
		<ol class="breadcrumb pull-left">
			<li><a href="{!!URL::to('/')!!}">Dashboard</a></li>
			<li><a href="{!!route('getSended')!!}">Sent Documents</a></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box ui-draggable ui-droppable">
			<div class="box-header">
			@if(canAdd('com_sended_docs'))
				<div class="btn-group" role="group" style='padding:10px;'>
				  	<a href="{!!URL::route('CreateSended_doc')!!}" class="btn btn-primary"><i class="fa fa-plus"></i> علاوه نمودن</a> 	   
			   </div>
			@endif 
			@if(canSearch('com_sended_docs'))
				<div class="btn-group" role="group" style='padding:10px;float: right'>	
				  	<a href="{!!URL::route('getSended_report')!!}" class="btn btn-primary"><i class="fa fa-files-o"></i> راپور</a> 	   
			   </div>
			@endif  
			</div>
			<div class="box-content">
				@if(Session::has('success'))
				<div class='alert alert-success'>{!!Session::get('success')!!}</div>

				@elseif(Session::has('fail'))
				<div class='alert alert-danger'>{!!Session::get('fail')!!}</div>
				 @endif

				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="received_list">
					<thead>
						<tr>
						   <th>شماره مسلسل</th>
						   <th>نام دوسیه</th>
						   <th>کد</th>
						   <th>شماره مکتوب</th>
						   <th>تاریخ مکتوب</th>
						   <th>هدایت رییس جمهور</th>
						   <th>توضیحات</th>
						   <th>به جواب مکتوب</th>
						   <th>عملیه ها</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
				
			</div>
		</div>
	</div>
</div>
@stop
@section('footer-scripts')
<script type="text/javascript">
$('#received_list').dataTable( 
	{
		"bProcessing": true,
		"bServerSide": true,
		//"iDisplayLength": 2,
		"sAjaxSource": "{!!URL::to('/com/getSendData')!!}",
		"aaSorting": [[ 0, "desc" ]],
		
		"language": {
            "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
            "zeroRecords": "ریکارد موجود نیست",
            "info": "نمایش صفحه _PAGE_ از _PAGES_",
            "infoEmpty": "ریکارد موجود نیست",
            "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
       },
       "createdRow": function (row, data, rowIndex) {
	        // Per-cell function to do whatever needed with cells
	        $.each($('td', row), function (colIndex) {
	            // For example, adding data-* attributes to the cell
	            $(this).attr('dir', "auto");
	        });
	   }
	}
); 
$(document).ready(function(){
	var tds = $('#received_list').find('.dir');
	//alert(tds.length);
	for(var i=0;i<tds.length;i++)
	{
		$('.dir').prop('dir','auto');
	}
});

</script>
@stop
