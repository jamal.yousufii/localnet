<div class="row" id="add_static">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            <div class="box-header">
                <div class="box-name ui-draggable-handle">
                    <i class="fa fa-files-o"></i>
                    <span>تجدید نمودن</span>
                </div>
                
                <div class="no-move"></div>
            </div>
            <div class="box-content">
            	<form class="form-horizontal" role="form" method="post" id="edit_table_fields">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <input name='name_en' class="form-control" placeholder="نام انگلیسی" type="text" value="{!!$record->name_en!!}">			                            
                        </div>
                        <div class="col-sm-4">
                            <input name='name_dr' class="form-control" placeholder="نام دری" type="text" value="{!!$record->name_dr!!}">
                            
                        </div>
                        <div class="col-sm-4">
                            <input name='name_pa' class="form-control" placeholder="نام پشتو" type="text" value="{!!$record->name_pa!!}">
                            
                        </div>
                    </div>
                    <input type="hidden" name="table" value="{!!$table!!}" />
                    <input type="hidden" name="id" value="{!!$id!!}" />
                    {!! Form::token() !!}
        
                    <div class="form-group">			                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="button" class="btn btn-primary" onclick="edit_table('edit_table_fields');load_table({!!$table_id!!})">ذخیره نمودن</button>
                            <button type="button" class="btn btn-danger" onclick="$('#add_static').hide()">لغو نمودن</button>		                    
                        </div>    
                    </div>
                 </form>
            </div>
        </div>
    </div>
</div>