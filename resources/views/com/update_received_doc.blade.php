@extends('layouts.master')
@section('head')
{!! HTML::style('/css/schedule_db/persion_datepicker.css') !!}
{!! HTML::style('/css/template/libs/datepicker.css') !!}
{!! HTML::style('/css/autocomplete/jquery-ui.css') !!}
    <title>Edit Received Document</title>
@stop

@section('content')
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="{!!URL::to('/')!!}">Dashboard</a></li>
            <li><a href="{!!route('getReceived')!!}">Received Documents</a></li>
            <li>Edit Received Document</li>
        </ol>        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            <div class="box-header">
                <div class="box-name ui-draggable-handle">
                    <i class="fa fa-files-o"></i>
                    <span>Edit form</span>
                </div>
                
                <div class="no-move"></div>
            </div>
            <div class="box-content">
                
                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postUpdateReceived_doc',$id) !!} " enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">نام دوسیه</label>
                        <div class="col-sm-4">
                            <input name='dossier' dir="auto" class="form-control" required="required" type="text" value="@if(old('dossier')){!!old('dossier')!!}@else{!!$details->dossier_name!!}@endif">
                            <span style='color:red'>
                            @if($errors->has("dossier"))
                                {!! $errors->first('dossier') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">کد</label>
                        <div class="col-sm-4">
                            <input name='code' dir="auto" class="form-control" type="text" value="@if(old('code')){!!old('code')!!}@else{!!$details->code!!}@endif">
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">نوعیت</label>
                        <div class="col-sm-4">
	                        <input name='type' id="type" class="form-control" type="text" value="@if(old('type')){!!old('type')!!}@else{!!$type_name!!}@endif" placeholder="تایپ کنید">
	                        
                        </div>
                        <label class="col-sm-2 control-label">تاریخ میلادی</label>
                        <div class="col-sm-2">
                            <input class="form-control" readonly="readonly" type="text" id="datepickerDate">
                        	
                        </div>
                        <div class="col-sm-2"><input type="button" onclick="change_date()" value="تبدیل به شمسی" /></div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">شماره مکتوب</label>
                        <div class="col-sm-4">
	                        <input name='number' dir="auto" class="form-control" type="text" value="@if(old('number')){!!old('number')!!}@else{!!$details->number!!}@endif">
	                        
                        </div>
                        <label class="col-sm-2 control-label">تاریخ مکتوب</label>
                        <div class="col-sm-4">
                            <input name='date' class="form-control datepicker_farsi" readonly="readonly" required="required" type="text" value="@if(old('date')){!!old('date')!!}@elseif($details->date!=''){!!jalali_format($details->date)!!}@endif" id="shamsi_date">
                            
                        </div>
                        <span style='color:red'>
                        @if($errors->has("date"))
                            {!! $errors->first('date') !!}
                        @endif
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ارجعیت موضوع</label>
                        <div class="col-sm-4">         
                            <?php $pro_opts = array(''=>'یک گزینه را انتخاب کنید')?>
                            @if($priority_types)
	                            @foreach($priority_types AS $type)
	                            	<?php $pro_opts[$type->id] = $type->name;?>
	                            @endforeach
	                        @endif
	                        <?php $pro_type = old('priority');?>
	                        {!!Form::select('priority',$pro_opts,isset($pro_type)?old('priority'):$details->priority_id,['class'=>'form-control','required'=>'required'])!!}
	                        <span style='color:red'>
	                        @if($errors->has("priority"))
	                            {!! $errors->first('priority') !!}
	                        @endif
	                        </span>
                        </div>
                        <label class="col-sm-2 control-label">مرجع ارسال کننده</label>
                        <div class="col-sm-4">
	                        <input name='sender' id="sender" class="form-control" type="text" value="@if(old('sender')){!!old('sender')!!}@else{!!$sender_name!!}@endif" placeholder="تایپ کنید">
	                        
                        </div>
                    </div>
                    <?php $name_count = 1; ?>
                    @if(count($names)>0)
                    	@foreach($names AS $name)
                    		<div id="names_{!!$name_count!!}">
			                    <div class="form-group">
			                    	@if($name_count==1)
			                        <label class="col-sm-2 control-label">اسامی اشخاص</label>
			                        @else
			                        <label class="col-sm-2 control-label"></label>
			                        @endif
			                        <div class="col-sm-4">
				                        <input name='fname[]' class="form-control" type="text" placeholder="اسم" value="{!!$name->first_name!!}">
				                        
			                        </div>
			                        <label class="col-sm-2 control-label"></label>
			                        <div class="col-sm-3">
			                            <input name='position[]' class="form-control" type="text" placeholder="موقف" value="{!!$name->position!!}">
			                            
			                        </div>
			                                           
			                    </div>
			                    <div class="form-group">			                    	
			                        <label class="col-sm-2 control-label"></label>			                        
			                        <div class="col-sm-4">
				                        <input name='phone[]' class="form-control" type="text" placeholder="شماره تماس" value="{!!$name->phone!!}">
				                        
			                        </div>
			                        <label class="col-sm-2 control-label"></label>
			                        <div class="col-sm-3">
			                            <input name='email[]' class="form-control" type="text" placeholder="ایمیل" value="{!!$name->email!!}">
			                            
			                        </div>
			                        <div class="col-sm-1">
			                        	@if($name_count==1)
			                            <input type="button" value="+" class="btn btn-primary" onclick="add_names()"/>
			                            @else
			                            <input type="button" value="-" class="btn btn-danger" onclick="remove_names('names_{!!$name_count!!}')"/>
			                            @endif
			                        </div>                    
			                    </div>
			               </div>
		                    <?php $name_count++; ?>
		                @endforeach
                    @else
                    <div class="form-group">
                        <label class="col-sm-2 control-label">اسامی اشخاص</label>
                        <div class="col-sm-4">
	                        <input name='fname[]' class="form-control" type="text" placeholder="اسم">
	                        
                        </div>
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-3">
                            <input name='position[]' class="form-control" type="text" placeholder="موقف">
                            
                        </div>
                                           
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
	                        <input name='phone[]' class="form-control" type="text" placeholder="شماره تماس">
	                        
                        </div>
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-3">
                            <input name='email[]' class="form-control" type="text" placeholder="ایمیل">
                            
                        </div>
                        <div class="col-sm-1">
                            <input type="button" value="+" class="btn btn-primary" onclick="add_names()"/>
                        </div>                    
                    </div>
                    @endif
                    <input type="hidden" value="{!!$name_count!!}" id="names_value"/>
                    <div class="form-group" id="more_names"></div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">خلص موضوع</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="summary" rows="3">@if(old('summary')){!!old('summary')!!}@else{!!$details->summary!!}@endif</textarea>
						</div>
						<label class="col-sm-2 control-label">یادداشت</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="note" rows="3">@if(old('note')){!!old('note')!!}@else{!!$details->note!!}@endif</textarea>
						</div>
					</div>
					<!--
					<div class="form-group">
						<label class="col-sm-2 control-label">توضیحات</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="desc" rows="3">@if(old('desc')){!!old('desc')!!}@else{!!$details->desc!!}@endif</textarea>
						</div>
						<label class="col-sm-2 control-label">راپور ماهوار</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="report" rows="3">@if(old('report')){!!old('report')!!}@else{!!$details->monthly_report!!}@endif</textarea>
						</div>
					</div>
					-->
					<div class="form-group">
						<label class="col-sm-2 control-label">ضمیمه ها</label>			
						@if(count($attachments)>0)
							@foreach($attachments AS $file)
							<div id="div_file_{!!$file->id!!}">
								<a href="{!!route('downloadAttachment_received',$file->file_name)!!}">{!!$file->file_name!!}</a>&nbsp;<span class="glyphicon glyphicon-remove" style="color: red" title="Delete the file" onclick="if(confirm('Are you sure?')){delete_attachment('{!!$file->id!!}','{!!$file->file_name!!}')}"></span>
							</div>
							@endforeach
							<label class="col-sm-2 control-label"></label>
						@endif	
						
						<div>
							<input type="file" class="multi with-preview" accept="doc|docx|xls|xlsx|pdf|jpg|png" name="files[]"/>
						</div>
								
					</div>
                    {!! Form::token() !!}
                    
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">ذخیره نمودن</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">لغو نمودن</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/schedule_db/persion_datepicker.js')!!}
{!! HTML::script('/js/template/bootstrap-datepicker.js')!!}
{!! HTML::script('/js/template/multifile/jquery.MultiFile.js')!!}
{!! HTML::script('/js/autocomplete/jquery-ui.js')!!}
<script type="text/javascript">
$( "#sender" ).autocomplete({
	source: function(request, response) {
		
		$.ajax({ 
			url:"{!!route('get_sender_list')!!}",
			data: { term: $("#sender").val()},
			dataType: "json",
			type: "POST",
			success: function(data){
				response(data);				
			}
	});
},
minLength: 1
});
$( "#type" ).autocomplete({
	source: function(request, response) {
		
		$.ajax({ 
			url:"{!!route('get_type_list')!!}",
			data: { term: $("#type").val()},
			dataType: "json",
			type: "POST",
			success: function(data){
				response(data);				
			}
	});
},
minLength: 1
});
//datepicker
$('#datepickerDate').datepicker({
  format: 'mm-dd-yyyy'
});

	function delete_attachment(id,name)
	{
		var counter = 1;
		$.ajax({
			url:'{{URL::route("deleteAttachment_received")}}',
			data: '&doc_id='+id+'&name='+name,
			type:'POST',
			success:function(r){
				$('#div_file_'+id).hide();
			}
		});
	}

	function add_names()
	{
		  var ni = document.getElementById('more_names');
		  var numi = document.getElementById('names_value');
		  var num = (document.getElementById('names_value').value -1)+ 2;
		  numi.value = num;
		  
		  var element = '<div id="names_'+num+'"><div class="form-group"><label class="col-sm-2 control-label"></label><div class="col-sm-4"><input name="fname[]" class="form-control" type="text" placeholder="Name"></div><label class="col-sm-2 control-label"></label><div class="col-sm-3"><input name="position[]" class="form-control" type="text" placeholder="position"></div></div><div class="form-group"><label class="col-sm-2 control-label"></label><div class="col-sm-4"><input name="phone[]" class="form-control" type="text" placeholder="Phone"></div><label class="col-sm-2 control-label"></label><div class="col-sm-3"><input name="email[]" class="form-control" type="text" placeholder="Email"></div><div class="col-sm-1"><input type="button" value="-" class="btn btn-danger" onclick="remove_names(\'names_'+num+'\')"/></div></div></div>';
		
		  $('#more_names').append(element);
	}
	
	function remove_names(div)
	{
		var elem = document.getElementById(div);
		elem.parentNode.removeChild(elem);
	}
	function change_date()
	{
		var date = $('#datepickerDate').val();
		if(date==''){alert('یک تاریخ را انتخاب نمایید');return;}
		$.ajax({
			url:"{!!route('change_date')!!}",
			data:'&date='+date,
			type: 'POST',
			success:function(r){
				$('#shamsi_date').val(r);
			}
		});
	}
</Script>
@stop