@extends('layouts.master')
@section('head')
{!! HTML::style('/css/schedule_db/persion_datepicker.css') !!}
{!! HTML::style('/css/template/libs/datepicker.css') !!}
{!! HTML::style('/css/autocomplete/jquery-ui.css') !!}
    <title>علاوه نمودن سند وارده</title>
@stop
@section('content')
    
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="{!!URL::to('/')!!}">Dashboard</a></li>
            <li><a href="{!!route('getReceived')!!}">Received Documents</a></li>
            <li>Create Received Document</li>
        </ol>        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            <div class="box-header">
                <div class="box-name ui-draggable-handle">
                    <i class="fa fa-files-o"></i>
                    <span>علاوه نمودن سند وارده</span>
                </div>
               
                <div class="no-move"></div>
            </div>
            <div class="box-content">
                
                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postCreateReceived_doc') !!}" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">نام دوسیه</label>
                        <div class="col-sm-4">
                            <input name='dossier' dir="auto" class="form-control" value="{!!old('dossier')!!}" type="text" required="required">
                            <span style='color:red'>
                            @if($errors->has("dossier"))
                                {!! $errors->first('dossier') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">کد</label>
                        <div class="col-sm-4">
                            <input name='code' dir="auto" class="form-control" value="{!!old('code')!!}" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">نوعیت</label>
                        <div class="col-sm-4">
	                        <input name='type' id="type" class="form-control" type="text" value="{!!old('type')!!}" placeholder="تایپ کنید">	                        
                        </div>
                        <label class="col-sm-2 control-label">تاریخ میلادی</label>
                        <div class="col-sm-2">
                            <input class="form-control" type="text" readonly="readonly" id="datepickerDate">                       	
                        </div>
                        <div class="col-sm-2"><input type="button" onclick="change_date()" value="تبدیل به شمسی" /></div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">شماره مکتوب</label>
                        <div class="col-sm-4">
	                        <input name='number' dir="auto" class="form-control" value="{!!old('number')!!}" type="text">
                        </div>
                        <label class="col-sm-2 control-label">تاریخ مکتوب</label>
                        <div class="col-sm-4">
                            <input name='date' class="form-control datepicker_farsi" required="required" readonly="readonly" value="{!!old('date')!!}" type="text" id="shamsi_date">
                        	<span style='color:red'>
	                        @if($errors->has("date"))
	                            {!! $errors->first('date') !!}
	                        @endif
	                        </span>
                        </div>                  
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ارجعیت موضوع</label>
                        <div class="col-sm-4">         
                            <?php $pro_opts = array(''=>'یک گزینه را انتخاب کنید')?>
                            @if($priority_types)
	                            @foreach($priority_types AS $type)
	                            	<?php $pro_opts[$type->id] = $type->name;?>
	                            @endforeach
	                        @endif
	                        <?php $pro_type = old('priority');?>
	                        {!!Form::select('priority',$pro_opts,isset($pro_type)?old('priority'):'',['class'=>'form-control','required'=>'required'])!!}
	                        <span style='color:red'>
	                        @if($errors->has("priority"))
	                            {!! $errors->first('priority') !!}
	                        @endif
	                        </span>
                        </div>
                        <label class="col-sm-2 control-label">مرجع ارسال کننده</label>
                        <div class="col-sm-4">
	                        <input name='sender' id="sender" class="form-control" type="text" value="{!!old('sender')!!}" placeholder="تایپ کنید">
	                        
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">اسامی محترمین</label>
                        <div class="col-sm-4">
	                        <input name='fname[]' class="form-control" type="text" placeholder="اسم">
	                        
                        </div>
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-3">
                            <input name='position[]' class="form-control" type="text" placeholder="موقف">
                            
                        </div>
                                            
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
	                        <input name='phone[]' class="form-control" type="text" placeholder="شماره تماس">
	                        
                        </div>
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-3">
                            <input name='email[]' class="form-control" type="text" placeholder="ایمیل">
                            
                        </div>
                        <div class="col-sm-1">
                            <input type="button" value="+" class="btn btn-primary" onclick="add_names()"/>
                        </div>                    
                    </div>
                    <input type="hidden" value="1" id="names_value"/>
                    <div class="form-group" id="more_names"></div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">خلص موضوع</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="summary" rows="3">{!!old('summary')!!}</textarea>
						</div>
						<label class="col-sm-2 control-label">یادداشت</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="note" rows="3">{!!old('note')!!}</textarea>
						</div>
					</div>
					<!--
					<div class="form-group">
						<label class="col-sm-2 control-label">توضیحات</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="desc" rows="3">{!!old('desc')!!}</textarea>
						</div>
						<label class="col-sm-2 control-label">گزارش ماهوار</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="report" rows="3">{!!old('report')!!}</textarea>
						</div>
					</div>
					-->
					<div class="form-group">
						<label class="col-sm-2 control-label">ضمیمه ها</label>
						<div class="col-sm-4">
							<input type="file" class="multi with-preview" accept="doc|docx|xls|xlsx|pdf|jpg|png" name="files[]"/>
						</div>
						<label class="col-sm-2 control-label"></label>
					</div>
					
                    {!! Form::token() !!}
                    
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">ذخیره نمودن</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">لغو نمودن</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/schedule_db/persion_datepicker.js')!!}
{!! HTML::script('/js/template/bootstrap-datepicker.js')!!}
{!! HTML::script('/js/template/multifile/jquery.MultiFile.js')!!}
{!! HTML::script('/js/autocomplete/jquery-ui.js')!!}

<script type="text/javascript">
$( "#sender" ).autocomplete({
	source: function(request, response) {
		
		$.ajax({ 
			url:"{!!route('get_sender_list')!!}",
			data: { term: $("#sender").val()},
			dataType: "json",
			type: "POST",
			success: function(data){
				response(data);				
			}
	});
},
minLength: 1
});
//autocomplete for type
$( "#type" ).autocomplete({
	source: function(request, response) {
		
		$.ajax({ 
			url:"{!!route('get_type_list')!!}",
			data: { term: $("#type").val()},
			dataType: "json",
			type: "POST",
			success: function(data){
				response(data);				
			}
	});
},
minLength: 1
});

//datepicker
$('#datepickerDate').datepicker({
  format: 'mm-dd-yyyy'
});

	function add_names()
	{
		  var ni = document.getElementById('more_names');
		  var numi = document.getElementById('names_value');
		  var num = (document.getElementById('names_value').value -1)+ 2;
		  numi.value = num;
		  
		  var element = '<div id="names_'+num+'"><div class="form-group"><label class="col-sm-2 control-label"></label><div class="col-sm-4"><input name="fname[]" class="form-control" type="text" placeholder="Name"></div><label class="col-sm-2 control-label"></label><div class="col-sm-3"><input name="position[]" class="form-control" type="text" placeholder="position"></div></div><div class="form-group"><label class="col-sm-2 control-label"></label><div class="col-sm-4"><input name="phone[]" class="form-control" type="text" placeholder="Phone"></div><label class="col-sm-2 control-label"></label><div class="col-sm-3"><input name="email[]" class="form-control" type="text" placeholder="Email"></div><div class="col-sm-1"><input type="button" value="-" class="btn btn-danger" onclick="remove_names(\'names_'+num+'\')"/></div></div></div>';
		
		  $('#more_names').append(element);
	}
	
	function remove_names(div)
	{
		var elem = document.getElementById(div);
		elem.parentNode.removeChild(elem);
	}
	function change_date()
	{
		var date = $('#datepickerDate').val();
		if(date==''){alert('یک تاریخ را انتخاب نمایید');return;}
		$.ajax({
			url:"{!!route('change_date')!!}",
			data:'&date='+date,
			type: 'POST',
			success:function(r){
				$('#shamsi_date').val(r);
			}
		});
	}
</Script>
@stop