
<div class="row">
	<div class="col-xs-12">
		<div class="box ui-draggable ui-droppable">
			<div class="box-header">
			
				<div class="btn-group" role="group" style='padding:10px;'>				
				  	<a href="javascript:void()" class="btn btn-primary" onclick="$('#report_form').show();$('#report_result').hide()">برگشت به راپور</a> 	   
			   </div>
			   <div class="btn-group" role="group" style='padding:10px;'>				
				  	<a href="javascript:void()" class="btn btn-primary" onclick="$('#report_form_fields').submit();"> پرنت اکسل</a> 	   
			   </div>
		   </div>
			<div class="box-content">
				@if(Session::has('success'))
				<div class='alert alert-success'>{!!Session::get('success')!!}</div>

				@elseif(Session::has('fail'))
				<div class='alert alert-danger'>{!!Session::get('fail')!!}</div>
				 @endif

				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="received_list">
					<thead>
						<tr>
						   <th>شماره مسلسل</th>
						   <th>نام دوسیه</th>
						   <th>کد</th>
						   <th>شماره مکتوب</th>
						   <th>تاریخ مکتوب</th>
						   <th>هدایت رییس جمهور</th>
						   <th>عملیه ها</th>
						</tr>
					</thead>
					<tbody>
					@if($results)
						@foreach($results AS $item)
						<tr>
							<td>{!!$item->id!!}</td>
							<td dir="auto">{!!$item->dossier_name!!}</td>
							<td>{!!$item->code!!}</td>
							<td>{!!$item->number!!}</td>
							<td>{!!$item->date!!}</td>
							<td dir="auto">{!!$item->president_guide!!}</td>
							<td>
								@if(canEdit('com_sended_docs'))
								<a href="{!!route('getUpdateSended_doc',$item->id)!!}" target="_blank" title="تجدید نمودن"><i class="glyphicon glyphicon-edit"></i></a>
								@endif
								@if(canDelete('com_sended_docs'))
								&nbsp;|&nbsp;<a href="{!!route('getDeleteSended_doc',$item->id)!!}" onclick="javascript:return confirm('آیا میخواهید این ریکارد را حذف نمایید؟');" title="حذف نمودن"><i class="fa fa-trash-o"></i></a>
								@endif
								@if(canView('com_sended_docs'))
								&nbsp;|&nbsp;<a href="{!!route('getDetailsSended_doc',$item->id)!!}" target="_blank" title="نمایش"><i class="glyphicon glyphicon-eye-open"></i></a>
								@endif
							</td>
						</tr>
						@endforeach
					@else
						<tr>
							<td colspan="7">ریکاردی موجود نیست</td>
						</tr>
					@endif	
					</tbody>
				</table>				
			</div>
		</div>
	</div>
</div>

