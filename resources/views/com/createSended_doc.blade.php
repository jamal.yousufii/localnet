@extends('layouts.master')
@section('head')
    <title>علاوه نمودن سند صادره</title>
@stop
@section('content')
{!! HTML::style('/css/template/tagedit.css') !!}
{!! HTML::style('/css/template/custom-tagedit.css') !!}
{!! HTML::style('/css/schedule_db/persion_datepicker.css') !!}
{!! HTML::style('/css/template/libs/datepicker.css') !!}

<style>
	.tagedit-list{
		width:100% !important;
	}
</style>
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="{!!URL::to('/')!!}">Dashboard</a></li>
            <li><a href="{!!route('getSended')!!}">Sent Documents</a></li>
            <li>Create Sent Document</li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            <div class="box-header">
                <div class="box-name ui-draggable-handle">
                    <i class="fa fa-files-o"></i>
                    <span>علاوه نمودن سند صادره</span>
                </div>
                
                <div class="no-move"></div>
            </div>
            <div class="box-content">
                
                <form class="form-horizontal" role="form" method="post" id="function-source" action="{!!URL::route('postCreateSended_doc')!!}" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">نام دوسیه</label>
                        <div class="col-sm-4">
                            <input name='dossier' dir="auto" class="form-control" required="required" value="{!!old('dossier')!!}" type="text">
                            <span style='color:red'>
                            @if($errors->has("dossier"))
                                {!! $errors->first('dossier') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">کد</label>
                        <div class="col-sm-4">
                            <input name='code' dir="auto" class="form-control" value="{!!old('code')!!}" type="text">
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">شماره مکتوب</label>
                        <div class="col-sm-4">
	                        <input name='number' dir="auto" class="form-control" value="{!!old('number')!!}" type="text">
	                        
                        </div>
                        
                    </div>
                    <div class="form-group">
                    	<label class="col-sm-2 control-label">تاریخ مکتوب</label>
                        <div class="col-sm-4">
                            <input name='date' class="form-control datepicker_farsi" required="required" readonly="readonly" value="{!!old('date')!!}" type="text" id="shamsi_date">
                            <span style='color:red'>
                            @if($errors->has("date"))
                                {!! $errors->first('date') !!}
                            @endif
                            </span>
                        </div>
                        <label class="col-sm-2 control-label">تاریخ میلادی</label>
                        <div class="col-sm-2">
                            <input class="form-control" type="text" readonly="readonly" id="datepickerDate">
                            
                        </div>
                        <div class="col-sm-2"><input type="button" onclick="change_date()" value="تبدیل به شمسی" /></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ارجعیت موضوع</label>
                        <div class="col-sm-4">         
                            <?php $pro_opts = array(''=>'یک گزینه را انتخاب کنید')?>
                            @if($priority_types)
	                            @foreach($priority_types AS $type)
	                            	<?php $pro_opts[$type->id] = $type->name;?>
	                            @endforeach
	                        @endif
	                        <?php $pro_type = old('priority');?>
	                        {!!Form::select('priority',$pro_opts,isset($pro_type)?old('priority'):'',['class'=>'form-control','required'=>'required'])!!}
	                        <span style='color:red'>
	                        @if($errors->has("priority"))
	                            {!! $errors->first('priority') !!}
	                        @endif
	                        </span>
                        </div>
                        <label class="col-sm-2 control-label">به جواب مکتوب</label>
                        <div class="col-sm-4">
	                        <input name='answer' dir="auto" class="form-control" value="{!!old('answer')!!}" type="text">
	                        
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">مراجع دریافت و اجراکننده هدایت</label>
                        <div class="col-sm-10">
                        	<input type="text" name="receivers[]" value="" class="tag"/>            
                        </div>                        
                    </div>
                    
                    <div class="form-group">
                    	<label class="col-sm-2 control-label">نتیجه مکتوب پیگیری</label>
                        <div class="col-sm-4">
	                        <select name="result" id="result" class="form-control" onchange="result_reason()">
	                            <option value="">یک گزینه را انتخاب کنید</option>
	                            @if($result_types)
		                            @foreach($result_types AS $result)
		                            	@if($result->id == old('result'))
		                            		<option value='{!!$result->id!!}' selected="selected">{!!$result->name!!}</option>
		                            	@else
		                            		<option value='{!!$result->id!!}'>{!!$result->name!!}</option>
		                            	@endif
		                            @endforeach
		                        @endif
	                        </select>
	                        
                        </div>
                        <label class="col-sm-2 control-label">قیود زمان</label>
                        <div class="col-sm-4">
	                        <input name='deadline' class="form-control datepicker_farsi" readonly="readonly" value="{!!old('deadline')!!}" type="text">
	                        
                        </div>
                        
                    </div>
                    <div class="form-group" <?php if(old('result')==4){ echo 'style="display: block"';}else{echo 'style="display: none"';}?> id="reason_div">
						<label class="col-sm-2 control-label">دلیل اجرات نشده</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="reason" rows="3">{!!old('reason')!!}</textarea>
						</div>
						
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">هدایت رییس جمهور</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="guide" rows="3">{!!old('guide')!!}</textarea>
						</div>
						<label class="col-sm-2 control-label">توضیحات</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="desc" rows="3">{!!old('desc')!!}</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">ضمیمه ها</label>
						<div class="col-sm-4">
							<input type="file" class="multi with-preview" accept="doc|docx|pdf|jpg|png" name="files[]"/>
						</div>
						<label class="col-sm-2 control-label"></label>
					</div>
                    {!! Form::token() !!}
                    
                    <div class="form-group">
                        
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">ذخیره نمودن</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">لغو نمودن</button>
                    
                        </div>
                        
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/template/tagedit/jquery.autoGrowInput.js') !!}
{!! HTML::script('/js/template/tagedit/jquery.tagedit.js') !!}
{!! HTML::script('/js/schedule_db/persion_datepicker.js')!!}
{!! HTML::script('/js/template/multifile/jquery.MultiFile.js')!!}
{!! HTML::script('/js/template/bootstrap-datepicker.js')!!}
<Script>
//datepicker
$('#datepickerDate').datepicker({
  format: 'mm-dd-yyyy'
});
$(function(){
	// Function Source
	$('.tag').tagedit({
		autocompleteURL: "{!!URL::route('getAutocomplete')!!}"
	});	
});
	function result_reason()
	{
		var item = $('#result').val();
		if(item == 4)
		{
			$('#reason_div').show();
		}
		else
		{
			$('#reason_div').hide();
		}
	}
	function change_date()
	{
		var date = $('#datepickerDate').val();
		if(date==''){alert('یک تاریخ را انتخاب نمایید');return;}
		$.ajax({
			url:"{!!route('change_date')!!}",
			data:'&date='+date,
			type: 'POST',
			success:function(r){
				$('#shamsi_date').val(r);
			}
		});
	}
</Script>
@stop