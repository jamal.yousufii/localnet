@extends('layouts.master')
@section('head')
{!! HTML::style('/css/schedule_db/persion_datepicker.css') !!}
{!! HTML::style('/css/autocomplete/jquery-ui.css') !!}
    <title>Received Document Report</title>
@stop
@section('content')
    
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="{!!URL::to('/')!!}">Dashboard</a></li>
            <li><a href="#">Received Document Report</a></li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            <div class="box-header">
                <div class="box-name ui-draggable-handle">
                    <i class="fa fa-files-o"></i>
                    <span>راپور</span>
                </div>               
                <div class="no-move"></div>
            </div>
            <div class="box-content" id="report_form">
                
                <form class="form-horizontal" role="form" method="post" id="report_form_fields" action="{!! URL::route('getReceived_excel') !!}">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">نام دوسیه</label>
                        <div class="col-sm-4">
                            <input name='dossier' dir="auto" class="form-control"  type="text">
                        </div>
                        <label class="col-sm-2 control-label">کد</label>
                        <div class="col-sm-4">
                            <input name='code' class="form-control" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">نوعیت</label>
                        <div class="col-sm-4">
	                        <select name="type" class="form-control">
	                            <option value="">همه</option>
	                            @if($doc_types)
		                            @foreach($doc_types AS $type)
		                            <option value='{!!$type->id!!}'>{!!$type->name!!}</option>
		                            @endforeach
		                        @endif
	                        </select>
                        </div>
                        <label class="col-sm-2 control-label">شماره مکتوب</label>
                        <div class="col-sm-4">
	                        <input name='number' class="form-control" value="{!!old('number')!!}" type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">تاریخ مکتوب از</label>
                        <div class="col-sm-4">
	                        <input name='fdate' class="form-control datepicker_farsi" type="text">
                        </div>
                        <label class="col-sm-2 control-label">تاریخ مکتوب تا</label>
                        <div class="col-sm-4">
                            <input name='tdate' class="form-control datepicker_farsi" type="text">
                        </div>              
                    </div>
					
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ارجعیت موضوع</label>
                        <div class="col-sm-4">
	                        <select name="priority" class="form-control">
	                            <option value="">همه</option>
	                            @if($priority_types)
		                            @foreach($priority_types AS $type)
		                            	@if($type->id == old('priority'))
		                            		<option value='{!!$type->id!!}' selected="selected">{!!$type->name!!}</option>
		                            	@else
		                            		<option value='{!!$type->id!!}'>{!!$type->name!!}</option>
		                            	@endif
		                            @endforeach
		                        @endif
	                        </select>
                        </div>      
                        <label class="col-sm-2 control-label">مرجع ارسال کننده</label>
                        <div class="col-sm-4">
	                        <input name='sender' id="sender" class="form-control" type="text" value="{!!old('sender')!!}" placeholder="تایپ کنید">	                        
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">اسامی محترمین</label>
                        <div class="col-sm-4">
	                        <input name='fname' class="form-control" type="text" placeholder="اسم">	                        
                        </div>
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-3">
                            <input name='position' class="form-control" type="text" placeholder="موقف">
                            
                        </div>                                           
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
	                        <input name='phone' class="form-control" type="text" placeholder="شماره تماس">	                        
                        </div>
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
                            <input name='email' class="form-control" type="text" placeholder="ایمیل">                           
                        </div>                    
                    </div>
                    
					<div class="form-group">
						<label class="col-sm-2 control-label">خلص موضوع</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="summary" rows="3">{!!old('summary')!!}</textarea>
						</div>
						<label class="col-sm-2 control-label">یادداشت</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="note" rows="3">{!!old('note')!!}</textarea>
						</div>
					</div>
					<!--
					<div class="form-group">
						<label class="col-sm-2 control-label">توضیحات</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="desc" rows="3">{!!old('desc')!!}</textarea>
						</div>
						<label class="col-sm-2 control-label">گزارش ماهوار</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="report" rows="3">{!!old('report')!!}</textarea>
						</div>
					</div>
					-->
                    {!! Form::token() !!}
                    
                    <div class="form-group">                      
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="button" class="btn btn-primary" onclick="generate_report()">راپور</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">لغو نمودن</button>                 
                        </div>                       
                    </div>                   
                </form>
            </div>
            <div id="report_result"></div>
        </div>
    </div>
</div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/schedule_db/persion_datepicker.js')!!}
{!! HTML::script('/js/autocomplete/jquery-ui.js')!!}
<script>
$( "#sender" ).autocomplete({
	source: function(request, response) {
		
		$.ajax({ 
			url:"{!!route('get_sender_list')!!}",
			data: { term: $("#sender").val()},
			dataType: "json",
			type: "POST",
			success: function(data){
				response(data);				
			}
	});
},
minLength: 1
});
	function generate_report()
	{
		var data = $('#report_form_fields').serializeArray().reduce(function(obj, item) {
	    	obj[item.name] = item.value;
	    	return obj;
		}, {});
    	
		 $.ajax({
		 	url: "{!! URL::route('postReportReceived_doc') !!}",
		 	data: data,
		 	type: 'POST',
		 	success:function(r){
		 		$('#report_form').hide();
		 		$('#report_result').show();
		 		$('#report_result').html(r);
		 	}
		 });
	}
	function print_excel()
	{
		var data = $('#report_form_fields').serializeArray().reduce(function(obj, item) {
	    	obj[item.name] = item.value;
	    	return obj;
		}, {});
    	
		 $.ajax({
		 	url: "{!! URL::route('getReceived_excel') !!}",
		 	data: data,
		 	type: 'POST',
		 	success:function(r){
		 		//alert(r);
		 	}
		 });
	}
</script>
@stop