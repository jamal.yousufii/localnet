@extends('layouts.master')
@section('head')
    <title>اسناد صادره</title>
@stop
@section('content')

<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="{!!URL::to('/')!!}">Dashboard</a></li>
            <li><a href="{!!route('getSended')!!}">Sent Documents</a></li>
            <li>Sent Document Details</li>
        </ol>       
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            <div class="box-header">
                <div class="box-name ui-draggable-handle">
                    <i class="fa fa-files-o"></i>
                    <span>جزییات سند صادره</span>
                </div>
                
                <div class="no-move"></div>
            </div>
            <div class="main-box-body clearfix">
				<div class="table-responsive">
					<table class="table" width="100%">
						<tbody>
							<tr>
								<td width="20%">نام دوسیه</td>
								<td width="30%">{!!$details->dossier_name!!}</td>
								<td width="20%">کد</td>
								<td width="30%">{!!$details->code!!}</td>
							</tr>
							<tr>
								<td>شماره مکتوب</td>
								<td>{!!$details->number!!}</td>
								<td>تاریخ مکتوب</td>
								<td>{!!$details->date!!}</td>
							</tr>
							<tr>
								<td>ارجعیت موضوع</td>
								<td>
									@if($priority_types)
			                            @foreach($priority_types AS $type)
			                            	@if($type->id == $details->priority_id)
			                            		{!!$type->name!!}
			                            	@endif
			                            @endforeach
			                        @endif
								</td>
								<td>به جواب مکتوب</td>
								<td>{!!$details->answer_to!!}</td>
							</tr>
							<tr>
								<td>مراجع دریافت و اجرا کننده هدایت</td>
								<td>
									@if($receivers)
		                        		@foreach($receivers as $rec)
			                        		{!!$rec->name!!},
			                        	@endforeach
			                        @endif
								</td>
								<td>قیود زمان</td>
								<td>{!!$details->deadline_date!!}</td>
							</tr>
							<tr>
								<td>نتیجه پیگیری</td>
								<td>
									@if($result_types)
			                            @foreach($result_types AS $result)
			                            	@if($result->id == $details->result_id)
			                            		{!!$result->name!!}
			                            	@endif
			                            @endforeach
			                        @endif
								</td>
								<td>@if($details->result_id == 4)دلیل اجرات نشده@endif</td>
								<td>@if($details->result_id == 4){!!$details->result_reason!!}@endif</td>
							</tr>
							<tr>
								<td>هدایت رییس جمهور</td>
								<td>{!!$details->president_guide!!}</td>
								<td>توضیحات</td>
								<td>{!!$details->desc!!}</td>
							</tr>
							@if(count($attachments)>0)
		                    <tr><td colspan="4"><table class="table">
								@foreach($attachments AS $file)
								<tr><td>
									<a href="{!!route('downloadAttachment_received',$file->file_name)!!}">{!!$file->file_name!!}</a>
								</td></tr>
								@endforeach
							
							</table></td></tr>
							@endif	
						</tbody>
					</table>
				</div>
			</div>
            <div class="box-content">
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="button" class="btn btn-danger" onclick="history.back()">برگشت به لیست</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
