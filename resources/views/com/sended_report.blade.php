@extends('layouts.master')
@section('head')
{!! HTML::style('/css/schedule_db/persion_datepicker.css') !!}
{!! HTML::style('/css/template/libs/select2.css') !!}
    <title>Sended Document Report</title>
@stop
@section('content')
    
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="{!!URL::to('/')!!}">Dashboard</a></li>
            <li><a href="#">Sended Document Report</a></li>
        </ol>
        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            <div class="box-header">
                <div class="box-name ui-draggable-handle">
                    <i class="fa fa-files-o"></i>
                    <span>راپور</span>
                </div>
                
                <div class="no-move"></div>
            </div>
            <div class="box-content" id="report_form">     
                <form class="form-horizontal" role="form" method="post" id="report_form_fields" action="{!!URL::route('getSended_excel')!!}">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">نام دوسیه</label>
                        <div class="col-sm-4">
                            <input name='dossier' dir="auto" class="form-control" type="text">
                        </div>
                        <label class="col-sm-2 control-label">کد</label>
                        <div class="col-sm-4">
                            <input name='code' class="form-control" type="text">                         
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">شماره مکتوب</label>
                        <div class="col-sm-4">
	                        <input name='number' class="form-control" type="text">	                        
                        </div>
                        <label class="col-sm-2 control-label">مرجع دریافت و اجراکننده هدایت</label>
                        <div class="col-sm-4 form-group-select2">
                        	<?php $rec_opts = array(''=>'همه')?>
                            @if($receivers)
	                            @foreach($receivers AS $rec)
	                            	<?php $rec_opts[$rec->name] = $rec->name;?>
	                            @endforeach
	                        @endif
	                        <?php $rec_type = old('receivers');?>
	                        {!!Form::select('receivers',$rec_opts,isset($rec_type)?old('receivers'):'',['style'=>'width:300px','id'=>'sel2'])!!}                        
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">تاریخ مکتوب از</label>
                        <div class="col-sm-4">
	                        <input name='fdate' class="form-control datepicker_farsi" type="text">
                        </div>
                        <label class="col-sm-2 control-label">تاریخ مکتوب تا</label>
                        <div class="col-sm-4">
                            <input name='tdate' class="form-control datepicker_farsi" type="text">
                        </div>              
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ارجعیت موضوع</label>
                        <div class="col-sm-4">         
                            <?php $pro_opts = array(''=>'همه')?>
                            @if($priority_types)
	                            @foreach($priority_types AS $type)
	                            	<?php $pro_opts[$type->id] = $type->name;?>
	                            @endforeach
	                        @endif
	                        <?php $pro_type = old('priority');?>
	                        {!!Form::select('priority',$pro_opts,isset($pro_type)?old('priority'):'',['class'=>'form-control'])!!}
	                        <span style='color:red'>
	                        @if($errors->has("priority"))
	                            {!! $errors->first('priority') !!}
	                        @endif
	                        </span>
                        </div>
                        <label class="col-sm-2 control-label">به جواب مکتوب</label>
                        <div class="col-sm-4">
	                        <input name='answer' dir="auto" class="form-control" value="{!!old('answer')!!}" type="text">                 
                        </div>
                    </div>  
                    <div class="form-group">
                    	<label class="col-sm-2 control-label">نتیجه مکتوب پیگیری</label>
                        <div class="col-sm-4">
	                        <select name="result" id="result" class="form-control" onchange="result_reason()">
	                            <option value="">همه</option>
	                            @if($result_types)
		                            @foreach($result_types AS $result)
		                            	@if($result->id == old('result'))
		                            		<option value='{!!$result->id!!}' selected="selected">{!!$result->name!!}</option>
		                            	@else
		                            		<option value='{!!$result->id!!}'>{!!$result->name!!}</option>
		                            	@endif
		                            @endforeach
		                        @endif
	                        </select>
	                        
                        </div>
                        <label class="col-sm-2 control-label">قیود زمان</label>
                        <div class="col-sm-4">
	                        <input name='deadline' class="form-control datepicker_farsi" value="{!!old('deadline')!!}" type="text">                      
                        </div>                        
                    </div>
                    <div class="form-group" id="reason_div">
						<label class="col-sm-2 control-label">دلیل اجرات نشده</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="reason" rows="3">{!!old('reason')!!}</textarea>
						</div>						
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">هدایت رییس جمهور</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="guide" rows="3">{!!old('guide')!!}</textarea>
						</div>
						<label class="col-sm-2 control-label">توضیحات</label>
						<div class="col-sm-4">
							<textarea class="form-control" dir="auto" name="desc" rows="3">{!!old('desc')!!}</textarea>
						</div>
					</div>
                    {!! Form::token() !!}
                    
                    <div class="form-group">                       
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="button" class="btn btn-primary" onclick="generate_report()">راپور</button>
                            <button type="button" class="btn btn-danger" onclick="history.back()">لغو نمودن</button>                   
                        </div>                       
                    </div>                   
                </form>
            </div>
            <div id="report_result"></div>
        </div>
    </div>
</div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/schedule_db/persion_datepicker.js')!!}
{!! HTML::script('/js/template/select2.js')!!}
<Script>
//datepicker
$('#datepickerDate').datepicker({
  format: 'mm-dd-yyyy'
});
//nice select boxes
$('#sel2').select2();
		
$(function(){
	// Function Source
	$('.tag').tagedit({
		autocompleteURL: "{!!URL::route('getAutocomplete')!!}"
	});	
});
function generate_report()
{
	var data = $('#report_form_fields').serializeArray().reduce(function(obj, item) {
    	obj[item.name] = item.value;
    	return obj;
	}, {});
	
	 $.ajax({
	 	url: "{!! URL::route('postReportSended_doc') !!}",
	 	data: data,
	 	type: 'POST',
	 	success:function(r){
	 		$('#report_form').hide();
	 		$('#report_result').show();
	 		$('#report_result').html(r);
	 	}
	 });
}
function print_excel()
{
	var data = $('#report_form_fields').serializeArray().reduce(function(obj, item) {
    	obj[item.name] = item.value;
    	return obj;
	}, {});
	
	 $.ajax({
	 	url: "{!! URL::route('getReceived_excel') !!}",
	 	data: data,
	 	type: 'POST',
	 	success:function(r){
	 		//alert(r);
	 	}
	 });
}
</Script>
@stop