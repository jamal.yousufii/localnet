@extends('layouts.master')
@section('head')
    <title>ارتباطات</title>
@stop
@section('content')
    
<div class="row">
    <div id="breadcrumb" class="col-xs-12">
        
        <ol class="breadcrumb pull-left">
            <li><a href="{!!URL::to('/')!!}">Dashboard</a></li>
            <li><a href="#">Static Tables</a></li>
        </ol>        
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="box ui-draggable ui-droppable">
            <div class="box-header">
                <div class="box-name ui-draggable-handle">
                    <i class="fa fa-files-o"></i>
                    <span>لیست های ثابت</span>
                </div>
               
                <div class="no-move"></div>
            </div>
            <div class="box-content">
                
                <form class="form-horizontal" role="form" method="post" action="{!! URL::route('postCreateReceived_doc') !!}" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">لیست</label>
                        <div class="col-sm-4">
                        	<?php $opts = array(''=>'یک گزینه را انتخاب کنید');?>
                            @if($records)
	                            @foreach($records AS $type)
	                            	<?php $opts[$type->id]=$type->name;?>
	                            @endforeach
	                        @endif
	                        <?php $old = old('type');?>
	                        {!!Form::select('type',$opts,isset($old) ? old('type') : '',['id'=>'type','class'=>"form-control",'onchange'=>'load_table(0)'])!!}
	                        
                        </div>
                        
                    </div>
                    
                </form>
            </div>
            <div id="table_det"></div>
        </div>
    </div>
</div>
@stop
@section('footer-scripts')
<Script>
	function load_table(id)
	{
		 if(id == 0)
		 {
		 	var table = $('#type').val();
		 }
		 else
		 {
		 	var table = id;
		 }
		 $.ajax({
		 	url: "{!!route('getTable_det')!!}",
		 	data: '&id='+table,
		 	type: 'POST',
		 	success:function(r){
		 		$('#table_det').html(r);
		 	}
		 });
	}
</Script>
@stop