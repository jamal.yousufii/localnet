@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('contacts_list')!!}</title>
    <style type="text/css">
        .ltr
        {
            direction: ltr;
            text-align: center;
        }
        a{text-decoration:none}
    	#list_previous a, #list_next a {padding:9px}
       	#list th{
       		text-align:center !important;
       		background: #62A8EA !important;
       		font-weight: bold;
       		color: white !important;
       	}
       	#list td{
       		text-align:center !important;
       	}
    </style>

@stop

@section('content')

@if(Session::has('success'))
    <div class='alert alert-success span6 noprint'>{!!Session::get('success')!!}</div>

@elseif(Session::has('fail'))
    <div class='alert alert-danger span6 noprint'>{!!Session::get('fail')!!}</div>
@endif
@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif


<div class="row" style="opacity: 1;">

    <div class="col-sm-12" style="margin:0;padding:0;overflow-x:hidden;">
      
        <div style="margin:0 auto;width:800px;text-align:center;">{!! HTML::image("img/header.png",'',array('style'=>'max-width:100%;padding:20px')) !!}</div>
            <h4 align="center" style="font-size: 20px;font-weight:bold">شماره های تماس</h4>
            <form id="search_frm" role="form" enctype="multipart/form-data">
                <div class="row" style="margin-left: 15em;padding-top:20px" dir="rtl">
                    
                    <div class="form-group col-sm-1">
                        <a href="javascript:void()" class="btn btn-primary" onclick="getSearchResult('search_result')"><i class="fa fa-search fa-lg"></i> جستجو</a>
                    </div>
                    <div class="form-group col-xs-3" id="sub_dep_div">
                        <select name="sub_dep" id="sub_dap" class="form-control">
                            <option value="">اداره فرعی</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-5">
                        <select name="general_dep" id="parent_dep" class="form-control" onchange="bringRelatedSubDepartment('sub_dep_div',this.value)">
                            <option value="">اداره عمومی</option>
                            @foreach($parentDeps AS $dep_item)
                                <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                            @endforeach
                        </select>
                    </div>
                   
                </div>
                    
            </form>
            <hr style="border: 1px dashed #b6b6b6" />
            <div class="main-box-body clearfix" id="search_result">

                <table class="table table-responsive" id='list' dir="rtl">
                    <thead>
                    <tr>
                        <th>#</th>
                        <!-- <th>دیپارتمنت</th> -->
                        <th>نام مکمل</th>
                        <th>وظیفه</th>
                        <th>سیسکو</th>
                        <th>زیمنس</th>
                        <th>فکس</th>
						@if(canEdit('cons_list'))
                        <th>عملیه</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
        
            </div>
        </div>
    </div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop

@section('footer-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {

                //'sDom': 'Tlfr<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                // "sdom": 'T<"clear">lfrtip',
                // "oTableTools": {
                //     "aButtons": [
                //         {
                //             "sExtends": "print",
                //             "bShowAll": true
                //         }
                //     ]
                // },
                "sAjaxSource": "{!!URL::route('contactsList')!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );
        
    });

    function getSearchResult(div)
    {
        $.ajax({
                url: '{!!URL::route("getContactsSearchResult")!!}',
                data: $('#search_frm').serialize(),
                type: 'post',
                beforeSend: function(){

                    //$("body").show().css({"opacity": "0.5"});
                    $("#"+div).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {

                    $('#'+div).html(response);
                }
            }
        );

        return false;
        
    }

    function bringRelatedSubDepartment(div,id)
    {

        $.ajax({
                url: '{!!URL::route("bringContactsRelatedSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );

        // get the staff list based on the general department.
        // $.ajax({
        //         url: '{!!URL::route("getContactsSearchResult")!!}',
        //         data: '&general_dep='+id,
        //         type: 'post',
        //         beforeSend: function(){
        //             $("#search_result").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
        //         },
        //         success: function(response)
        //         {
        //             $("#search_result").html(response);
        //         }
        //     }
        // );
    }

</script>
@stop
