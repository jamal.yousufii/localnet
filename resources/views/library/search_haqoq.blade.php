<table class="table table-bordered table-responsive" id="datalist1">
                  <thead>
                    <tr>
                      <th>شماره #</th>
                      <th>نام کتاب</th>
                      <th>نوع کتاب</th>
                      <th>شماره مسلسل</th>
                      <th>نام مؤلف</th>
                      <th>الماری</th>
                      <th>توضیحات</th>
                      <th colspan="2">عملیات</th>
                      </tr>  </thead>
                      @if(!empty($rows))
                    <?php $var = 1; ?>
                    @foreach ($rows as $val)
                      <tr>
                        <td>{!! $var !!}</td>
                        <td>{!! $val->book_name !!}</td>
                        <td>{!! $val->cat_name !!}</td>
                        <td>{!! $val->serial_number !!}</td>
                        <td>{!! $val->author_name !!}</td>
                        <td>{!! $val->cabinet_number !!}</td>
                        <td>{!! $val->description !!}</td>
                         <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                        <td><a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a></td>
                      </tr>
                       <?php $var++ ; ?>
                      @endforeach   
                      @else
                      <tr>
                        <td colspan="10" style="color: red"> موردی پیدا نشد !</td>
                      </tr>
                    @endif    
                  </tbody>
                </table>
                