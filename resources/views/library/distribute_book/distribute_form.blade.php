@extends('layouts.master')
@section('content')
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>توزیع کتاب ها</h3></center>
    <ol class="breadcrumb">
     
      <li class="active">توزیع کتاب ها\     </li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
       <div class="content">

          <form class="form-horizontal group-border-dashed" action="{!!URL::route('distribute_insert')!!}" method="post" style="border-radius: 0px;">
              <div class="form-group">
              <label class="col-sm-2 control-label"> نام کتاب </label>
                <div class="col-sm-4">
                   <select class="form-control select2" name="book_id" id="book_name">
                        <option>نام کتاب</option>
                    @foreach($book as $name)
                        <option value="{{$name->id}}">{{$name->book_name}}</option>
                        
                     @endforeach
                   </select>                
                 </div>
                <label class="col-sm-2 control-label"> انتخاب نوع کتاب</label>
                <div class="col-sm-4">
                   <select class="form-control select2" name="" id="book_type">
                        <option>انتخاب نوع کتاب</option>
                    @foreach($categories as $value)
                        <option value="{{$value->id}}">{{$value->cat_name}}</option>
                        
                     @endforeach
                   </select>                
                 </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"> الا تاریخ</label>
                <div class="col-sm-4">
                  <input `type="text" name="to_date" class="form-control datepicker_farsi" value="{{old(' to_date')}}" placeholder="الا تاریخ">
                </div>
                <label class="col-sm-2 control-label">  از تاریخ</label>
                <div class="col-sm-4">
                  <input type="text" name="from_date" class="form-control datepicker_farsi" value="{{old('from_date')}}" placeholder="از تاریخ">
                </div>
              </div>
              <div class="form-group">          
                <label class="col-sm-2 control-label">  توضیحات</label>
                <div class="col-sm-4">
                   <textarea class="form-control" name="description" placeholder=" توضیحات"> </textarea>               
                </div>
                <label class="col-sm-2 control-label">  نام کارمند</label>
                <div class="col-sm-4">
                   <select class="form-control select2" name="customer_id" id="exampleFormControlSelect1">
                       <option value="">نام کارمند </option>
                    @foreach($customer as $name)
                        <option value="{{$name->id}}">{{$name->name}}</option>
                        
                     @endforeach
                   </select>                  
                 </div>
              </div>
            

              {!!Form::token()!!}
              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                </div><label class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                  <input type="submit" value="ثبت" id="add_department" class="btn btn-success"/>
                    <a href="{{route('distribute_book')}}" class="btn btn-danger">برگشت</a>
                </div>
              </div>
            </form>
           </div>
  </div>
</div>
@stop

@section('footer-scripts') 
<script type="text/javascript">
$('.select2').select2();
        jQuery(document).ready(function($) {
          $("#book_type").on('change', function() {
              var id = $(this).val();
              if(id=='')
                id=0;
              console.log(id);
              $.ajax ({
                  type: 'get',
                  url: '{!!URL::route("get_book_type")!!}'+'/'+id,
                  success : function(htmlresponse) {
                    $('#book_name').empty();
                    $("#book_name").css("width","100%");
                    $("#book_name").append("<option value='' >Select book</option>");
                    $.each(htmlresponse, function(id, book) {
                              $("#book_name").append("<option value='"+book.id+"' >"+book.book_name+"</option>");
                            });
                    // $('book_name').select2();


                    }
              });
          });
      });
</script>
@stop