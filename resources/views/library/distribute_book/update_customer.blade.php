@extends('layouts.master')
@section('content')
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>تغیر نمودن معلومات</h3></center>
    <ol class="breadcrumb">
     
      <li class="active">اضافه نمودن اطلاعت  \     </li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <div class="content">

            <form class="form-horizontal group-border-dashed" action="{!!URL::route('update_customer',$customers->id)!!}" method="post" style="border-radius: 0px;">
                <div class="form-group">
                  <label class="col-sm-2 control-label">  نام</label>
                  <div class="col-sm-4">
                    <input type="text" name="name" class="form-control" value="{{$customers->name}}" placeholder="نام" required="">
                  </div>
                  <label class="col-sm-2 control-label">  تخلص</label>
                  <div class="col-sm-4">
                    <input type="text" name="lastname" class="form-control" required="" value="{{$customers->lastname}}" placeholder="تخلص">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"> ایمیل</label>
                  <div class="col-sm-4">
                    <input type="text" name="email" class="form-control" value="{{$customers->email}}" placeholder="ایمیل">
                  </div>
                  <label class="col-sm-2 control-label">  دفتر</label>
                  <div class="col-sm-4">
                    <input type="text" name="office" class="form-control" value="{{$customers->office}}" placeholder="دفتر">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">  موقف</label>
                  <div class="col-sm-4">
                    <input type="text" name="position" class="form-control" value="{{$customers->position}}" placeholder="موقف">
                  </div> 
                   <label class="col-sm-2 control-label"> شماره</label>
                  <div class="col-sm-4">
                    <input type="text" name="number" class="form-control" value="{{$customers->number}}" placeholder="شماره">
                  </div>
               
                </div>
              
                {!!Form::token()!!}
                <div class="form-group">
                  <label class="col-sm-2 control-label"></label>
                  <div class="col-sm-4">
                  </div><label class="col-sm-2 control-label"></label>
                  <div class="col-sm-4">
                    <input type="submit" value="ثبت" id="add_department" class="btn btn-success"/>
                    <a href="{{route('distribute_book')}}" class="btn btn-danger">برگشت</a>
                  </div>
                </div>
              </form>
            </div>
  </div>
</div>
@stop

@section('footer-scripts') 
<script type="text/javascript">

</script> 

@stop