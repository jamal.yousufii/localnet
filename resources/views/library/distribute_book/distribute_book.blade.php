@extends('layouts.master') @section('content')
<style type="text/css">
    ul#ul_tabs {
        margin-top: 40px;
    }

    .modal-content {
        padding: 30px;
    }

    table.table-bordered tbody th,
    table.table-bordered tbody td {
        border-left: 1px solid #ddd !important;
    }

    .dataTables_wrapper .col-sm-6 {
        text-align: left;
    }

    table,
    thead,
    tr,
    th {
        text-align: center;
    }
    .table{
        direction: rtl;
    }

</style>

<div class="container">
    <div class="page-head">
        <h3 style="text-align: center;">توزیع کتاب ها</h3>
    </div>
    <div class="cl-mcont" id="sdu_result">
        <div class="tab-container">
            @if (count($errors) > 0)
            <script type="text/javascript">
                $("ul#ul_tabs li:first-child").removeClass("active");
                $("ul#ul_tabs li:nth-child(2)").addClass("active");
            </script>
            <div class="alert alert-danger" style="margin: 10px 0 20px 0">
                <strong>Whoops!</strong> There were some problems with your input.
                <br>
                <br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                    <span aria-hidden="true">×</span>
                </button>
                {!!Session::get('success')!!}
            </div>
            @elseif(Session::has('fail'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                    <span aria-hidden="true">×</span>
                </button>
                {!!Session::get('fail')!!}
            </div>
            @endif
            <ul class="nav nav-tabs" id="ul_tabs">
                <li class="active"><a href="#tab1" data-toggle="tab">لیست کتاب های توزیع شده</a></li>
                <li><a href="#tab2" data-toggle="tab">معلومات گیرنده</a></li>
                <!-- <li><a href="#tab3" data-toggle="tab">Payment Step</a></li>
        <li><a href="#tab4" data-toggle="tab">Sub Component</a></li> -->
            </ul>
    <div class="tab-content">
        <div class="tab-pane active cont" id="tab1">
            <div class="row">
                <a href="{{route('distribute_form')}}"><button type="button" class="btn btn-success"  style="margin-top: 20px; margin-left: 15px; ">توزیع کتاب </button></a>
            </div>

            <div class="header">
                <h3 style="margin-top:20px;text-align:center"> لست کتاب های توزیع شده</h4>
            </div>
            <div class="content">
                  <div>
                    <table class="table table-bordered table-responsive datatable" id="usersList" >
                      <thead>
                        <tr>
                          <th>شماره #</th>
                          <th>نام گیرینده کتاب</th>
                          <th>تخلص</th>
                          <th>نام کتاب</th>
                          <th>نمبر مسلسل کتاب</th>
                          <th>نمبر تیلفون</th>
                          <th>اداره مربوطه کارمند</th>
                          <th>وظیفه</th>
                          <th>تاریخ گرفتن کتاب</th>
                          <th>تاریخ پس آوردن کتاب</th>
                          <th>تاریخ تسلیمی</th>
                          <th>توضیحات</th>
                          <th>تسلیمی کتاب</th>
                          <th>عملیات</th>
                        </tr>
                      </thead>
                      <tbody>
         ‍‍‍‍           <?php $counter=1;?>
                    @foreach($distribute_book as $value)
                     <tr>
                        <td>{{$counter++}}</td>
                        <td>{{$value->name}}</td>
                        <td>{{$value->lastname}}</td>
                        <td>{{$value->book_name}}</td>
                        <td>{{$value->serial_number}}</td>
                        <td>{{$value->number}}</td>
                        <td>{{$value->office}}</td>
                        <td>{{$value->position}}</td>
                        <td>{!!checkEmptyDate($value->from_date)!!}</td>
                        <td>{!!checkEmptyDate($value->to_date)!!}</td>
                        <td>{!!checkEmptyDate($value->return_date)!!}</td>
                        <td>{!!$value->description!!}</td>
                        <td onclick="my_modal({{$value->id}},'{{checkEmptyDate($value->return_date)}}')">
                        @if(isset($value->return_date))

                              <a href="#"   data-toggle="modal" data-target="#myModal"><span class='label label-success'>تسلیم شده</span></a>

                        @elseif($value->to_date <= date('Y-m-d'))

                            <a href="#"  data-toggle="modal" data-target="#myModal"><span class='label label-danger'> تسلیم نشده</span></a>

                        @else

                          <a href="#"  data-toggle="modal"  data-target="#myModal"><span class='label label-default'> درحال بازگشت</span></a>


                        @endif
                        </td>
                        <td>
                            <a href="{{route('distribute_select_row',$value->id)}}" class="fa fa-edit"></a>
                        </td>
                    </tr>

                    @endforeach
                      </tbody>
                    </table>
                </div>
            </div>

                    <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header" style="text-align: right;">
                    <h4 class="modal-title" id="myModalLabel" >اضافه نمودن تاریخ تسلیمی</h4>
                  <hr>
                  </div>
                  <div class="modal-body">
                    <form class="form-horizontal group-border-dashed" action="{!!URL::route('return_date')!!}" method="post" style="border-radius: 0px;">
                    <div class="form-group">
                      <input type="hidden" class="get_id" name="get_id" >
                      <label class="col-sm-3 control-label">  تاریخ تسلیمی</label>
                      <div class="col-sm-8">
                        <input type="text" name="return_date" id="return_date" class="form-control datepicker_farsi"   placeholder="تاریخ تسلیمی" required="">
                      </div>

                    </div>


                    {!!Form::token()!!}

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
                    <button type="submit" class="btn btn-primary">ثبت شود</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
         </div>
        <div class="tab-pane cont" id="tab2">
          <!--Add New Department -->

          <div class="col col-12">

            <div class="row">
                <!-- Large modal -->
                 <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" style="margin-top: 20px;  margin-left: 15px;">اضافه نمودن </button>

            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
              <h3 style="text-align:center">معلومات گیرنده</h4>
            <div class="content">

                <form class="form-horizontal group-border-dashed" action="{!!URL::route('insert_customer')!!}" method="post" style="border-radius: 0px;">
                    <div class="form-group">
                      <label class="col-sm-2 control-label">  تخلص</label>
                      <div class="col-sm-4">
                        <input type="text" name="lastname" class="form-control" required="" value="{{old('lastname')}}" placeholder="تخلص">
                      </div>
                      <label class="col-sm-2 control-label">  نام</label>
                      <div class="col-sm-4">
                        <input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="نام" required="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label"> ایمیل</label>
                      <div class="col-sm-4">
                        <input type="text" name="email" class="form-control" value="{{old('email')}}" placeholder="ایمیل">
                      </div>
                      <label class="col-sm-2 control-label">  دفتر</label>
                      <div class="col-sm-4">
                        <input type="text" name="office" class="form-control" value="{{old('office')}}" placeholder="دفتر">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label">  موقف</label>
                      <div class="col-sm-4">
                        <input type="text" name="position" class="form-control" value="{{old('position')}}" placeholder="موقف">
                      </div>
                       <label class="col-sm-2 control-label"> شماره</label>
                      <div class="col-sm-4">
                        <input type="text" name="number" class="form-control" value="{{old('number')}}" placeholder="شماره">
                      </div>

                    </div>

                    {!!Form::token()!!}
                    <div class="form-group">
                      <label class="col-sm-2 control-label"></label>
                      <div class="col-sm-4">
                      </div><label class="col-sm-2 control-label"></label>
                      <div class="col-sm-4">
                        <input type="submit" value="ثبت" id="add_department" class="btn btn-success" style="float:right;" />
                        <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>

                      </div>
                    </div>
                  </form>
                 </div>
                </div>
              </div>
            </div>

            </div>
            <div class="header">

              <h3 style="margin-top:20px;text-align:center">لست از کارمندان </h3>
            </div>
                        <hr />
                        <table class="table table-bordered table-responsive datatable" id="usersList" style="direction: rtl;">
                            <thead>
                                <tr>
                                    <th>شماره#</th>
                                    <th>نام</th>
                                    <th>تخلص</th>
                                    <th>ایمیل</th>
                                    <th>دفتر</th>
                                    <th>موقف</th>
                                    <th>شماره موبایل</th>
                                    <th>عملیات</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $counter=1;?>
                                    @foreach($customers as $customer)
                                    <tr>
                                        <td>{{$counter++}}</td>
                                        <td>{{$customer->name}}</td>
                                        <td>{{$customer->lastname}}</td>
                                        <td>{{$customer->email}}</td>
                                        <td>{{$customer->office}}</td>
                                        <td>{{$customer->position}}</td>
                                        <td>{{$customer->number}}</td>
                                        <td>
                                            <a href="{{route('select_customer_row',$customer->id)}}" class="fa fa-edit"></a>
                                        </td>
                                    </tr>

                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop @section('footer-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#ul_tabs a[href="' + activeTab + '"]').tab('show');
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.datatable').DataTable();
    });
</script>
<script type="text/javascript">

function my_modal(id,return_date) {
  // get current maktoob data from
  // pass the data to the modal
  // edit the modal
  // store the updated data into maktoob with id => param id
  // console.log($('#myModal'));
  $('#return_date').val(return_date);
  $('.get_id').val(id);
  // $('#result').val(return_date);
  $('#myModal').on('shown.bs.modal', function () {
    $('#myInput').focus()
  })
}
</script>
@stop
