@extends('layouts.master')
@section('content')
<style type="text/css">
   table.table-bordered tbody th, table.table-bordered tbody td {
   border-left:1px solid #ddd !important;
   } 
   .dataTables_wrapper .col-sm-6{
   text-align: left;
   }
   table, thead ,tr,th{
   text-align: center;
   }
    .nav-tabs>li>a{
      padding:10px 12px !important;
    }
</style>
<div class="container" dir="rtl">
   <div class="page-head">
      <h3>سیستم مدیریت کتابخانه </h3>
   </div>
   <div class="cl-mcont" id="sdu_result">
      <div class="tab-container">
         @if (count($errors) > 0)
         <script type="text/javascript">
            $("ul#ul_tabs li:first-child").removeClass("active");
            $("ul#ul_tabs li:nth-child(2)").addClass("active");
         </script>
         <div class="alert alert-danger" style="margin: 10px 0 20px 0">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
         @if(Session::has('success'))
         <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
         </div>
         @elseif(Session::has('fail'))
         <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
         </div>
         @endif
         <ul class="nav nav-tabs" id="ul_tabs">
            @foreach($categories as $menu)
            <li class=""><a href="#tab{{$menu->id}}" data-toggle="tab">{{$menu->cat_name}}</a></li>
            @endforeach    
            <li><a href="{!!URL::route('high_Search')!!}"> جستجوي پيشرفته </a></li>
            <li><a href="{!!URL::route('distribute_book')!!}"> توزیع کتاب ها</a></li>
         </ul>
         <div class="tab-content">
            <div class="tab-pane active cont" id="tab1">
               <!-- DataTables for Contract Type -->
               <div class="">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها</a>
                        </div>
                        <!--   <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form1">
                             <div class="input-group custom-search-form">
                              <input type="text" class="form-control" id="search_field1" name="record" placeholder="جستجو توسط نام کتاب" required />
                             {!!Form::token();!!}
                               <span class="input-group-btn">
                                  <button class="btn btn-default-sm" id="search_button1"  cat_id="1"><i class="fa fa-search"></i> جستجو</button>
                                </span></div>
                            </form>
                           </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center"> لست کتابهای حقوق/ سیاست</h3>
                     <hr />
                  </div>
                  <div class="content">
                     <div>
                        <table class="table table-bordered table-responsive datatable " id="datalist1">
                           <thead>
                              <tr>
                                 <th>شماره #</th>
                                 <th>نام کتاب</th>
                                 <th>نوع کتاب</th>
                                 <th>شماره مسلسل</th>
                                 <th>نام مؤلف</th>
                                 <th>الماری</th>
                                 <th>توضیحات</th>
                                 <th >عملیات</th>
                              </tr>
                           </thead>
                           <tbody id="firstlist">
                              @if(!empty($data1))
                              <?php $counter =1; ?>
                              @foreach ($data1 as $val)
                              <tr>
                                 <td>{!! $counter++ !!}</td>
                                 <td>{!! $val->book_name !!}</td>
                                 <td>{!! $val->cat_name !!}</td>
                                 <td>{!! $val->serial_number !!}</td>
                                 <td>{!! $val->author_name !!}</td>
                                 <td>{!! $val->cabinet_number !!}</td>
                                 <td>{!! $val->description !!}</td>
                                 <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                                    <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                                 </td>
                              </tr>
                              @endforeach   
                              @endif
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab2">
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها</a>
                        </div>
                        <!--    <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form2">
                             <div class="input-group custom-search-form">
                              <input type="text" class="form-control" id="search_field2" name="record" placeholder="جستجو توسط نام کتاب" required />
                             {!!Form::token();!!}
                               <span class="input-group-btn">
                                  <button class="btn btn-default-sm" id="search_button2"  cat_id="2"><i class="fa fa-search"></i> جستجو</button>
                                </span></div>
                            </form>
                           </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center"> لست کتابهای تاریخ</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist2">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data2))
                        <?php $counter =1; ?>
                        @foreach ($data2 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab3">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها</a>
                        </div>
                        <!--      <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form3">
                             <div class="input-group custom-search-form">
                              <input type="text" class="form-control" id="search_field3" name="record" placeholder="جستجو توسط نام کتاب" required />
                             {!!Form::token();!!}
                               <span class="input-group-btn">
                                  <button class="btn btn-default-sm" id="search_button3"  cat_id="3"><i class="fa fa-search"></i> جستجو</button>
                                </span></div>
                            </form>
                           </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center">لست کتابهای جغرافیه</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist3">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data3))
                        <?php $counter =1; ?>
                        @foreach ($data3 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab4">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها</a>
                        </div>
                        <!--       <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form4">
                             <div class="input-group custom-search-form">
                              <input type="text" class="form-control" id="search_field4" name="record" placeholder="جستجو توسط نام کتاب" required />
                             {!!Form::token();!!}
                               <span class="input-group-btn">
                                  <button class="btn btn-default-sm" id="search_button4"  cat_id="4"><i class="fa fa-search"></i> جستجو</button>
                                </span></div>
                            </form>
                           </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center">لست کتابهای ادبیات</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist4">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data4))
                        <?php $counter =1; ?>
                        @foreach ($data4 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab5">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها</a>
                        </div>
                        <!--   <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form5">
                              <div class="input-group custom-search-form">
                               <input type="text" class="form-control" id="search_field5" name="record" placeholder="جستجو توسط نام کتاب" required />
                              {!!Form::token();!!}
                                <span class="input-group-btn">
                                   <button class="btn btn-default-sm" id="search_button5"  cat_id="5"><i class="fa fa-search"></i> جستجو</button>
                                 </span></div>
                             </form>
                           </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center">لست کتابهای طبی</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist5">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data5))
                        <?php $counter =1; ?>
                        @foreach ($data5 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab6">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها</a>
                        </div>
                        <!--    <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form6">
                             <div class="input-group custom-search-form">
                              <input type="text" class="form-control" id="search_field6" name="record" placeholder="جستجو توسط نام کتاب" required />
                             {!!Form::token();!!}
                               <span class="input-group-btn">
                                  <button class="btn btn-default-sm" id="search_button6"  cat_id="6"><i class="fa fa-search"></i> جستجو</button>
                                </span></div>
                            </form>
                           </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center">لست کتابهای دینی</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist6">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data6))
                        <?php $counter =1; ?>
                        @foreach ($data6 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab7">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها</a>
                        </div>
                        <!--        <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form7">
                              <div class="input-group custom-search-form">
                               <input type="text" class="form-control" id="search_field7" name="record" placeholder="جستجو توسط نام کتاب" required />
                              {!!Form::token();!!}
                                <span class="input-group-btn">
                                   <button class="btn btn-default-sm" id="search_button7"  cat_id="7"><i class="fa fa-search"></i> جستجو</button>
                                 </span></div>
                             </form>
                           </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center">لست کتابهای ریاضیات</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist7">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data7))
                        <?php $counter =1; ?>
                        @foreach ($data7 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab8">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها</a>
                        </div>
                        <!--     <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form8">
                             <div class="input-group custom-search-form">
                              <input type="text" class="form-control" id="search_field8" name="record" placeholder="جستجو توسط نام کتاب" required />
                             {!!Form::token();!!}
                               <span class="input-group-btn">
                                  <button class="btn btn-default-sm" id="search_button8"  cat_id="8"><i class="fa fa-search"></i> جستجو</button>
                                </span></div>
                            </form>
                           </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center">لست کتابهای فلسفه</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist8">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data8))
                        <?php $counter =1; ?>
                        @foreach ($data8 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab9">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها </a>
                        </div>
                        <!--        <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form9">
                              <div class="input-group custom-search-form">
                               <input type="text" class="form-control" id="search_field9" name="record" placeholder="جستجو توسط نام کتاب" required />
                              {!!Form::token();!!}
                                <span class="input-group-btn">
                                   <button class="btn btn-default-sm" id="search_button9"  cat_id="9"><i class="fa fa-search"></i> جستجو</button>
                                 </span></div>
                             </form>
                           </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center"> لست کتابهای روان شناسی</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist9">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data9))
                        <?php $counter =1; ?>
                        @foreach ($data9 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab10">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها </a>
                        </div>
                        <!--       <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form10">
                               <div class="input-group custom-search-form">
                                <input type="text" class="form-control" id="search_field10" name="record" placeholder="جستجو توسط نام کتاب" required />
                               {!!Form::token();!!}
                                 <span class="input-group-btn">
                                    <button class="btn btn-default-sm" id="search_button10"  cat_id="10"><i class="fa fa-search"></i> جستجو</button>
                                  </span></div>
                              </form>
                            </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center">لست کتابهای اقتصاد</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist10">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data10))
                        <?php $counter =1; ?>
                        @foreach ($data10 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab11">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها </a>
                        </div>
                        <!--          <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form1">
                              <div class="input-group custom-search-form">
                               <input type="text" class="form-control" id="search_field11" name="record" placeholder="جستجو توسط نام کتاب" required />
                              {!!Form::token();!!}
                                <span class="input-group-btn">
                                   <button class="btn btn-default-sm" id="search_button11"  cat_id="11"><i class="fa fa-search"></i> جستجو</button>
                                 </span></div>
                             </form>              
                           </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center">لست کتابهای ریفرنس</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist11">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data11))
                        <?php $counter =1; ?>
                        @foreach ($data11 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab12">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها </a>
                        </div>
                        <!--       <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form12">
                              <div class="input-group custom-search-form">
                               <input type="text" class="form-control" id="search_field12" name="record" placeholder="جستجو توسط نام کتاب" required />
                              {!!Form::token();!!}
                                <span class="input-group-btn">
                                   <button class="btn btn-default-sm" id="search_button12"  cat_id="12"><i class="fa fa-search"></i> جستجو</button>
                                 </span></div>
                             </form>
                           </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center">لست کتابهای کلیکسیون</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist12">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data12))
                        <?php $counter =1; ?>
                        @foreach ($data12 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab13">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها </a>
                        </div>
                        <!--        <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form13">
                               <div class="input-group custom-search-form">
                                <input type="text" class="form-control" id="search_field13" name="record" placeholder="جستجو توسط نام کتاب" required />
                               {!!Form::token();!!}
                                 <span class="input-group-btn">
                                    <button class="btn btn-default-sm" id="search_button13"  cat_id="13"><i class="fa fa-search"></i> جستجو</button>
                                  </span></div>
                              </form>
                            </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center">لست کتابهای انگلیسی</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist13">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data13))
                        <?php $counter =1; ?>
                        @foreach ($data13 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="tab-pane cont" id="tab14">
               <!--Add New Department -->
               <div class="col col-12">
                  <div class="header">
                     <br>
                     <div class="row">
                        <div class="pull-left">
                           <a href="{!!URL::route('insertLibraryData')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه نمودن کتاب ها</a>
                        </div>
                        <!--     <div class="col-md-6 pull-right" dir="ltr">
                           <form class="form-horizontal group-border-dashed" id="search_form14">
                              <div class="input-group custom-search-form">
                               <input type="text" class="form-control" id="search_field14" name="record" placeholder="جستجو توسط نام کتاب" required />
                              {!!Form::token();!!}
                                <span class="input-group-btn">
                                   <button class="btn btn-default-sm" id="search_button14"  cat_id="14"><i class="fa fa-search"></i> جستجو</button>
                                 </span></div>
                             </form>
                           </div> -->
                     </div>
                     <h3 style="margin-top:20px;" align="center">لست کتابهای عمومی</h3>
                     <hr />
                  </div>
                  <div>
                     <table class="table table-bordered table-responsive datatable" id="datalist14">
                        <thead>
                           <tr>
                              <th>شماره #</th>
                              <th>نام کتاب</th>
                              <th>نوع کتاب</th>
                              <th>شماره مسلسل</th>
                              <th>نام مؤلف</th>
                              <th>الماری</th>
                              <th>توضیحات</th>
                              <th >عملیات</th>
                           </tr>
                        </thead>
                        @if(!empty($data14))
                        <?php $counter =1; ?>
                        @foreach ($data14 as $val)
                        <tr>
                           <td>{!! $counter++ !!}</td>
                           <td>{!! $val->book_name !!}</td>
                           <td>{!! $val->cat_name !!}</td>
                           <td>{!! $val->serial_number !!}</td>
                           <td>{!! $val->author_name !!}</td>
                           <td>{!! $val->cabinet_number !!}</td>
                           <td>{!! $val->description !!}</td>
                           <td><a href="deleterow/{!! $val->id !!} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a>|
                              <a href="selectrow/{!! $val-> id !!}" class="fa fa-edit"></a>
                           </td>
                        </tr>
                        @endforeach   
                        @endif    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@stop
@section('footer-scripts') 
<script type="text/javascript">
   $(document).ready( function () {
     $('.datatable').DataTable();
   } );
   
</script>
<script type="text/javascript">
   $(document).ready(function(){
       $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
           localStorage.setItem('activeTab', $(e.target).attr('href'));
       });
       var activeTab = localStorage.getItem('activeTab');
       if(activeTab){
           $('#ul_tabs a[href="' + activeTab + '"]').tab('show');
       }
   });
</script>

@stop