@extends('layouts.master')
@section('content')
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>تغیر نمودن معلومات</h3></center>
    <ol class="breadcrumb">
     
      <li class="active">اضافه نمودن اطلاعت  \     </li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed" action="{!!URL::route('updaterow',$data->id)!!}" method="post" style="border-radius: 0px;">

   
          <div class="form-group">
            <label class="col-sm-2 control-label">نام کتاب</label>
            <div class="col-sm-4">
              <input type="text" name="book_name" class="form-control" value="{!! $data->book_name !!}" placeholder="نام کتاب" required>
            </div>
            <label class="col-sm-2 control-label">انتخاب نوع کتاب</label>
            <div class="col-sm-4">
              <select name="book_type" class="form-control">
                <option value="" >انتخاب نوع کتاب</option>
                @foreach($row as $val)
                <option value="<?php echo $val->id; ?>" <?php if ($val->id==$data->book_type) echo "selected"?> >{!!$val->cat_name!!}</option>
               

                @endforeach
              </select>
            </div> 
          </div>
          <div class="form-group">
           <label class="col-sm-2 control-label">  شماره مسلسل</label>
            <div class="col-sm-4">
              <input type="text" name="serial_number" value="{!!$data->serial_number!!}" class="form-control" placeholder="  شماره مسلسل">
            </div>
            <label class="col-sm-2 control-label">نام مؤلف</label>
            <div class="col-sm-4">
              <input type="text" name="author_name" class="form-control" value="{!!$data->author_name!!}" placeholder="نام مؤلف">
            </div>
          </div>

          <div class="form-group">
            
            <label class="col-sm-2 control-label">جزئیات </label>
            <div class="col-sm-4">
            <textarea class="form-control" name="description" placeholder=" موضوع"> {!!$data->description!!}</textarea>
              
            </div>
            <label class="col-sm-2 control-label">شماره الماری</label>
            <div class="col-sm-4">
              <input type="text" name="cabinet_number" class="form-control" value="{!!$data->cabinet_number!!}" placeholder="شماره الماری">
            </div>
          </div><br>
     
          
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <input type="submit" value="Update" id="" class="btn btn-success"/>
              <a href="{!!URL::route('libraryList')!!}"> <input type="button" value="   لیست از اطلاعات 
                  " id="add_department" class="btn btn-danger"/></a>
            </div>
          </div>
        </form><br><br><br><br><br>
  </div>
</div>
@stop

@section('footer-scripts') 
<script type="text/javascript">

</script> 

@stop