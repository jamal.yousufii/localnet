@extends('layouts.master')
@section('content')
<style type="text/css">
   table.table-bordered tbody th, table.table-bordered tbody td {
   border-left:1px solid #ddd !important;
   }
   .dataTables_wrapper .col-sm-6{
   text-align: left;
   }
   </style>
<div class="container" dir="rtl">
    <div class="page-head">
        <center>
            <h3> جستجو پیشرفته</h3>
        </center>
        <ol class="breadcrumb">
            <li class="active">اضافه نمودن اطلاعت \ </li>
        </ol>
    </div>
    <div class="cl-mcont" id="sdu_result">
        <div class="tab-container">
            @if (count($errors) > 0)
            <div class="alert alert-danger" style="margin: 10px 0 20px 0">
                <strong>Whoops!</strong> There were some problems with your input.
                <br>
                <br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
                <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                    <span aria-hidden="true">×</span>
                </button> {!!Session::get('success')!!}
            </div>
            @elseif(Session::has('fail'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                    <span aria-hidden="true">×</span>
                </button> {!!Session::get('fail')!!}
            </div>
            @endif
            <form class="form-horizontal group-border-dashed" action="{{route('export_book')}}" id="form1" method="post" style="border-radius: 0px;">
                <div class="form-group">
                    <label class="col-sm-2 control-label">نام کتاب</label>
                    <div class="col-sm-4">
                        <input type="text" name="book_name" class="form-control" placeholder="نام کتاب">
                    </div>
                    <label class="col-sm-2 control-label">انتخاب نوع کتاب</label>
                    <div class="col-sm-4">
                        <select name="book_type" class="form-control">
                            <option value=" ">انتخاب نوع کتاب</option>
                            @foreach($row as $val)
                            <option value="{!!$val->id!!}">{!!$val->cat_name!!}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">نام مؤلف</label>
                    <div class="col-sm-4">
                        <input type="text" name="author_name" class="form-control" placeholder="نام مؤلف">
                    </div>
                    <label class="col-sm-2 control-label">شماره الماری</label>
                    <div class="col-sm-4">
                        <input type="text" name="cabinet_number" class="form-control" placeholder="شماره الماری">
                    </div>
                </div>
                <br> {!!Form::token()!!}
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                    <div class="col-sm-4">
                        <input type="button" value="  جستجو " id="search" class="btn btn-success" />
                        <a href="{!!URL::route('libraryList')!!}">
                            <input type="button" value="   لیست از اطلاعات  " id="add_department" class="btn btn-danger" />
                        </a>
                         <button type="submit" class="btn btn-primary" onClick="return confirm('Do want to export file ???')" style="padding: 6px 8px;">
                           <span class=" fa-cloud-download" ></span> Excel
                         </button>
                          <button  type="reset" value=" " id="add_department" class="btn btn-info" style="padding: 6px 8px;"><i class="fa fa-eraser fa-lg"></i>&nbsp;پاک شود
                          </button>
                    </div>
                </div>
            </form>
            <h3 style="margin-top:20px;" align="center">لیست از کتاب ها دریافت شده</h3>
            <hr />
        </div>
        <div>
            <table class="table table-bordered table-responsive datatable">
                <thead>
                    <tr>
                        <th>شماره #</th>
                        <th>نام کتاب</th>
                        <th>نوع کتاب</th>
                        <th>شماره مسلسل</th>
                        <th>نام مؤلف</th>
                        <th>الماری</th>
                        <th>توضیحات</th>
                        <th >عملیات</th>
                    </tr>
                </thead>
                <tbody id="search_result">
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop @section('footer-scripts')
<!-- <script type="text/javascript">
   $(document).ready( function () {
     $('.datatable').DataTable();
   } );

</script> -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#search').click(function() {
            var dataString = $('#form1').serialize().trim();

            $.ajax({
                type: 'POST',
                url: '{!!URL::route("getLibraryData")!!}',
                data: dataString,
                success: function(response) {
                    $('#search_result').html(response);
                    $('.datatable').DataTable();

                }
            })

            return false;
        });
    });
</script>
@stop
