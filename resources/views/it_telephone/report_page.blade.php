@extends('layouts.master')
@section('content')
<style type="text/css">
  .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{

    background: none;
    border-bottom-color:#ffffff; 


  }
  
  }
</style>
<div class="container" dir="rtl">
  <div class="page-head">
  <!--  <center><h3>سیستم معلوماتی مدیریت عمومی تلیفون  </h3></center>-->
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
      <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(2)").addClass("active");
      </script>
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      
      <div class="tab-content">
        <div class="tab-pane active cont" id="tab1"> 
          <!-- DataTables for Contract Type -->
          <div class="">
           <div class="header">
            <br>
            <div class="row">
            <!--   <div class="pull-left">
               <a href="{!!URL::route('insert_phone')!!}" style="margin-left: 15px" class="btn btn-success">+ اضافه کردن معلومات جدید</a>
              </div>-->
              <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form" action="">
                 <div class="input-group custom-search-form">
                  <input type="text" class="form-control" id="search_field" name="record" placeholder="Search" required />
                 {!!Form::token();!!}
                   <span class="input-group-btn">
                      <button class="btn btn-default-sm" id="search_button"><i class="fa fa-search"></i> جستجو عمومی </button>
                    </span></div>
                </form>
              </div>
            </div>
          <h3 style="margin-top:20px;" align="center">مدیریت عمومی تلیفون ریاست تکنالوژی معلوماتی معاونیت مالی واداری</h3><hr />
            </div>
            <div class="content">
              <div>
              
           
               <table class="table table-bordered table-responsive" id="datalist" >
                  <thead>
                    <tr>
                    <th>شماره#</th>
                    <th>اسم گیرنده </th>
                    <th>کمپنی مخابراتی</th>
                    <th>سریل نمبر جنس</th>
                    <th>شماره سیم کارت</th>
                    <th>حدود جنس</th>
                    <th>نوع سیم کارت</th>
                    <th>حالت سیم کارت</th>
                    <th>تاریخ ثبت</th>
                    <th>تغیر</th>
                    <th>حذف</th>
                    </thead>
                                         
                    </tr>
                       @if(!empty($all_data))
                       <?php
                        $counter=1;
                        $total_price = 0;
                        ?>
                      @foreach ($all_data as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->equipment_serialno}}</td>
                      <td>{{$val->simcard_number}}</td>
                      <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>
                      <td>@if($val->activation==1)Active @elseif($val->activation == 2)Deactive @endif</td>
                      <td>{!!checkEmptyDate($val->issue_date)!!}</td>
                      <td><a href="edit_equp/{{$val-> id}}" class="fa fa-edit"></a></td>          
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                            
                      </tr> 
                   
                    @endforeach 
                    
                    @else
                    
                    @endif
                 
                     </thead>
                  <tbody>
                  </tbody>
              
                </table>
                
                <a href="{!!URL::route('simcardsList')!!}">
               <input type="button" value=" برگشت به صفحه " id="add_department" class="btn btn-danger"/></a>
              
              </div>
            </div>
          </div>
        </div>
          
    </div>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">
//get the contract type list for all_datatable
$(document).ready(function() {
  
  $("#search_button").click(function(){

    var field_value = $('#search_field').val();
    if(field_value !="")
    $.ajax({
      type : "post",
      url : "{!!URL::route('search_sim')!!}",
      data : {"search_string": field_value, "_token": "<?=csrf_token();?>"},
      success : function(response)
      {
        $("#datalist").html(response);
      }
    });
      return false;
  });
  
});


</script> 

@stop