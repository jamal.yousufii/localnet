
<head>
  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
@extends('layouts.master')
@section('content')
<div class="container" dir="rtl" >
  <!-- <div class="page-head" style="background-color: #EDBB99;text-align: center;">
    <h3>مدیریت عمومی تلیفون ها</h3>
  </div> -->
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
      <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(2)").addClass("active");

      </script>
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
   <!--   style="background-color: #22DDE6;" -->
      <ul class="nav nav-tabs" id="ul_tabs">
        
     <!--   <li  ><a href="#tab10" data-toggle="tab" >مکافات و مجازات</a></li>-->
        
        <!-- <li  ><a href="#tab1" data-toggle="tab">Salam</a></li>
        <li  ><a href="#tab2" data-toggle="tab">Afghan Telecom</a></li>
        <li  ><a href="#tab3" data-toggle="tab">AWCC</a></li>
        <li  ><a href="#tab4" data-toggle="tab">Wasel Telecom</a></li>
        <li  ><a href="#tab5" data-toggle="tab">Etesalat</a></li>      
        <li  ><a href="#tab6" data-toggle="tab">MTN</a></li>      
        <li  ><a href="#tab7" data-toggle="tab">Roshan</a></li> -->
        <!-- <li  ><a href="#tab8" data-toggle="tab">Storage</a></li> -->
      <!--   <li  ><a href="#tab9" data-toggle="tab">Activation</a></li>
        <li  ><a href="#tab10" data-toggle="tab">Search</a></li>
        <li  ><a href="#tab11" data-toggle="tab">USB Dongle</a></li>
        <li  ><a href="#tab12" data-toggle="tab">020 Phone</a></li>
        <li  ><a href="#tab13" data-toggle="tab">Siemens Phone</a></li> -->

      </ul><br><br>
      <div class="tab-content">
        <div class="tab-pane active cont" id="tab1"> 
          <!-- DataTables for Contract Type -->
        
          <div class="">
           <div class="header">
            <br>
            <div class="row">
              <div class="pull-left">
              </div>
              <div class="col-md-6 pull-right" dir="ltr" style="text-align: right;">
                 <h4 >سیم کارت های ذخیره شده</h4>
                <form class="form-horizontal group-border-dashed" id="search_form" action="get" name="radio">
                <!--   -->
               
                </form>


              </div>
            </div>
            <h3 style="margin-top:20px;" align="pull-right"></h3><hr />
            </div>
           
            <div class="content">
              <div>
               <!--  <center> <header id="header1" ><strong style="width: 100%; font-size: 50px;"> این سافتویر امتحانی میباشد </strong><small style=" font-size: 30px;">که توسط زاهدالله خټک ساخته شده </small ></header></center>     -->
             
               <table class="table table-bordered table-responsive" id="datalist2" style="border:0px solid #E67E22;">
             <thead>
                  
                                    
                  <thead>
                    <tr align="center">
                    <th bgcolor="#EDBB99">شماره#</th>
                    <th bgcolor="#EDBB99">اسم گیرنده </th>
                    <th bgcolor="#EDBB99">تخلص</th>
                    <th bgcolor="#EDBB99">کمپنی مخابراتی</th>
                    
                    <th bgcolor="#EDBB99">شماره سیم کارت</th>
                    <!-- <th bgcolor="#EDBB99">حدود جنس</th>
                    <th bgcolor="#EDBB99">نوع سیم کارت</th>-->
                    <th bgcolor="#EDBB99">حالت سیم کارت</th> 
                    <th bgcolor="#EDBB99">تاریخ  ثبت</th>
                    <th bgcolor="#EDBB99">نوع جنس</th>
                    <th bgcolor="#EDBB99">تغیر</th>
                    <th bgcolor="#EDBB99">حذف</th>
                    </thead>
                                         
                    </tr>
                    @if(!empty($storage))
                       
                        <?php $counter=1;
                     
                       ?>
                        
        
                    @foreach ($storage as $val)

                      <td bgcolor="#EDBB99">{{$counter}}</td>
                      <td>{{$val->name}}</td>
                      <td>{{$val->lastname}}</td>
                      <td>{{$val->com_name}}</td>
                      <td>{{$val->simcard_number}}</td>
                      
                      <!-- <td>@if($val->equipment_range==1)Limited @elseif($val->equipment_range == 2)Unlimited @endif</td>
                      <td>@if($val->postpaid==1)PostPaid @elseif($val->postpaid == 2)PrePaid @endif</td>-->
                      <td>@if($val->storage==1)Stock in @elseif($val->storage == 2)Stock out @endif</td> 
                      <td>{!!checkEmptyDate($val->register_date)!!}</td>
                      <td>@if($val->equipment_id==1)SIM Card @elseif($val->equipment_id==2) USB Dongle @elseif($val->equipment_id==3) 020 Phone @endif</td>
                       <td><a href="edit_equp/{{$val-> id}}" class="fa fa-edit"></a></td> 
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>                            
                      </tr> 
                   </tr> <?php $counter++;
                               
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                 
                     </thead>
                  <tbody>
                          <tr>
           
              </tr>
                  </tbody>
            
                </table>
                <a href="{!!URL::route('insert_phone')!!}" class="btn btn-info" ><b>ثبت سیمکارت جدید <b></a>
                  <a href="{!!URL::route('simcardsList')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
                <div class="test-center"> 
             
               </div>
              
              </div>
            </div>
          </div>
        </div>
       
          
    </div>
  </div>
</div>

@stop
