<?php 
namespace App\Http\Controllers\it_telephone;
use App\Http\Controllers\Controller;
use App\models\Application;
use Illuminate\Http\Request;
use App\models\it_telephone\Equipment;
use App\models\it_telephone\Employee_info;
use App\models\it_telephone\Equip_log;
use App\models\it_telephone\Directorat;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Redirect;
use URL;
use Auth;
use Hash;
use DB;
use Input;
use View;
use Crypt;
use Response;
use Excel;
use Validator;


class main_controller extends Controller {

//----------------------------Select Main Page-------------------------------

  public function loadRecordsList(){
   
        return view('it_telephone.list');
  }

//---------------------------Select Sim Card Management 
  function sim_manag(){

    $company=\DB::connection('it_telephone')->table("companys")->get(); 
    $equipt=\DB::connection('it_telephone')->table("equipments")->get(); 
    $equptype=DB::connection('it_telephone')->table("equipment_types")->get(); 
    if($equptype =="PostPaid")
      $postpaid = "1";
    elseif($equptype == "PrePaid")
      $postpaid = "2";
    else
      $postpaid = "";

    $storage=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('storage','=','1')
      ->where('status','=','0')
      ->get();
// return "Salam";
    return view('it_telephone.sim_management',array('company'=>$company,'equipt'=>$equipt,'equptype'=>$equptype,'storage'=>$storage, ));

  }

//---------------------------Select Report Page
public function loadreport(){
  
        
    $company=\DB::connection('it_telephone')->table("companys")->get(); 
    $equipt=\DB::connection('it_telephone')->table("equipments")->get(); 
    $equptype=DB::connection('it_telephone')->table("equipment_types")->get(); 
    // $totale="";

       // $direc=\DB::table("directorates")->get();



    

    if($equptype =="PostPaid")
      $postpaid = "1";
    elseif($equptype == "PrePaid")
      $postpaid = "2";
    else
      $postpaid = "";
    

     
      //  $all_data=Equipment::paginate(6);

 

      $salam=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark','uploads.file_name','employee_infos.issue_date')->groupBy('simcard_number')
      ->leftjoin('uploads','equipments.id','=','uploads.file_id')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','Salam')
      // ->leftjoin('uploads','employee_infos.id','=','uploads.employee_infos_id')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->where('status','=','0')
      ->get();

      $roshan=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark','uploads.file_name','employee_infos.issue_date')->groupBy('simcard_number')
      ->leftjoin('uploads','equipments.id','=','uploads.file_id')->groupBy('file_id')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','Roshan')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
       ->where('status','=','0')
      ->get();

      $awcc=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark','uploads.file_name','employee_infos.issue_date')->groupBy('simcard_number')
      ->leftjoin('uploads','equipments.id','=','uploads.file_id')->groupBy('file_id')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','AWCC')
      
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->where('status','=','0')
      ->get();

      $mtn=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark','uploads.file_name','employee_infos.issue_date')->groupBy('simcard_number')
      ->leftjoin('uploads','equipments.id','=','uploads.file_id')->groupBy('file_id')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','MTN')
      
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->where('status','=','0')
      ->get();

      $etesalat=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark','uploads.file_name','employee_infos.issue_date')->groupBy('simcard_number')
      ->leftjoin('uploads','equipments.id','=','uploads.file_id')->groupBy('file_id')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','Etesalat')
      ->where('status','=','0')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->get();

      $w_telecom=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark','uploads.file_name','employee_infos.issue_date')->groupBy('simcard_number')
      ->leftjoin('uploads','equipments.id','=','uploads.file_id')->groupBy('file_id')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','Wasel Telecom')
      ->where('status','=','0')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->get();

      $a_telecom=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark','uploads.file_name','employee_infos.issue_date','employee_infos.issue_date')->groupBy('simcard_number')
      ->leftjoin('uploads','equipments.id','=','uploads.file_id')->groupBy('file_id')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')->where('com_name','=','Afghan Telecom')
      ->where('status','=','0')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','1')
      ->get();

      $activation=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark','employee_infos.issue_date')->groupBy('simcard_number')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('activation','=','2')
      ->where('status','=','0')
      
      ->get();

      $storage=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark')->groupBy('simcard_number')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->where('storage','=','1')
      ->where('status','=','0')
      ->get();

      $usbdongle=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark','uploads.file_name','employee_infos.issue_date')->groupBy('simcard_number')
      ->leftjoin('uploads','equipments.id','=','uploads.file_id')->groupBy('file_id')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','2')
      ->where('status','=','0')
      ->get();
      $phone020=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark','uploads.file_name','employee_infos.issue_date')->groupBy('simcard_number')
      ->leftjoin('uploads','equipments.id','=','uploads.file_id')->groupBy('file_id')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','3')
      ->where('status','=','0')
      ->get();
      $siemensPhone=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.cardno','employee_infos.name','employee_infos.lastname','employee_infos.position','employee_infos.directorate','employee_infos.file','equipments.remark','uploads.file_name','employee_infos.issue_date')->groupBy('simcard_number')
      ->leftjoin('uploads','equipments.id','=','uploads.file_id')->groupBy('file_id')
      ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->where('activation','=','1')
      ->where('storage','=','2')
      ->where('equipment_id','=','4')
      ->where('status','=','0')
      ->get();



    //   $data['salam']=$salam;
    //   $data['roshan']=$roshan;
    //   $data['a_telecom']=$a_telecom;
    //   $data['company']=$company;
    //   $data['equptype']=$equptype;
    
    // return view('it_telephone.list1',$data);
      return view("it_telephone.list1", array('salam' =>$salam,'roshan' =>$roshan,'a_telecom' =>$a_telecom,'mtn' =>$mtn,'awcc' =>$awcc,'etesalat' =>$etesalat,'w_telecom' =>$w_telecom,'activation' =>$activation,'company'=>$company,'equipt'=>$equipt,'equptype'=>$equptype,'usbdongle'=>$usbdongle,'phone020'=>$phone020, ));
    
}
//dd($_POST);

//------------------------Download file

//--- download file from database---
   //dd($file_name);
  // print_r($file_name);exit;
        //public path for file
//         $file= public_path(). "/uploads/".$file_name;
//         //download file
//         return Response::download($file, $file_name);
// 

public function file_download($id){
     $files=\DB::connection('it_telephone')->table('uploads')->select('uploads.*','equipments.id')
     ->leftjoin('equipments','uploads.file_id','=','equipments.id')
     ->where('file_id',$id)
      ->get();
      return view::make('it_telephone.page_for_down')->with('files',$files);
}

//-----------------------Insert Data--------------------------------------

//---------------Insert Employee Information 
  public function load_emp_form($phone=0,$sim_id=0){
    $data['equipments'] = DB::connection('it_telephone')->table('equipments')->get();
    $data['employee_infos'] = DB::connection('it_telephone')->table('employee_infos')->get();
    $data['directorates'] = DB::connection('it_telephone')->table('directorates')->get();
    $data['phone'] = $phone;
    $data['sim_id'] = $sim_id;
    return view("it_telephone.insert_employee_info",$data);
  }

          
 public function insert_emp_info(Request $request)
 {
    $insert_emp_data=new Employee_info;
    $max=Employee_info::max('id');
    $id =$max + 1;
    $insert_emp_data->cardno=Input::get('cardno');
    $insert_emp_data->name=Input::get('name');
    $insert_emp_data->lastname= Input::get('lastname');
   // $insert_emp_data->gender=Input::get('gender') ;
    $insert_emp_data->address=Input::get('address');
    $insert_emp_data->email_id=Input::get('email_id');
    //$insert_emp_data->office_no=Input::get('office_no');
    $insert_emp_data->directorate=Input::get('directorate');
     $insert_emp_data->department=Input::get('department');
    //$insert_emp_data->position=Input::get('position');
    $insert_emp_data->sim_id=Input::get('sim_id');
    $insert_emp_data->issue_date= toGregorian(gregorian_format(Input::get('issue_date')));
    $insert_emp_data->remark=Input::get('remark');
    $insert_emp_data->save();

    getLog('employee_infos',$id,'inserted record');
    return Redirect::route("phoneSimcardList")->with("success","معلومات معافقانه ثبت  ګردید.");
        }

  //------------- Insert SIM Information 

	public function load_insert_form()
	{

   

     $all_data=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','equipments.remark')
     
     ->leftjoin('companys','equipments.company_id','=','companys.id')
     ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
     ->paginate(10);


     $selectequp=\DB::connection('it_telephone')->table("equipments")->get();
    $company=\DB::connection('it_telephone')->table("companys")->get(); 
     $equptype=DB::connection('it_telephone')->table("equipment_types")->get(); 
     $empinfo=\DB::connection('it_telephone')->table("employee_infos")->orderBy('id','desc')->get();
    return view("it_telephone.insert_sim_info",array('selectequp'=>$selectequp,'company'=>$company,'equptype'=>$equptype,'empinfo'=>$empinfo,'all_data'=>$all_data));
		

	}
	
	public function insert_mo_data(Request $request)
	{	

    $insertdata=new Equipment;
    $max=Equipment::max('id');
    $id =$max + 1;
    //$insertdata->emp_id=Input::get('name');
    $insertdata->company_id=Input::get('company');
    $insertdata->equipment_id= Input::get('equipment');
   // $insertdata->equipment_range=Input::get('equipment_range') ;
    $insertdata->equipment_serialno=Input::get('equipment_serial');
    $insertdata->simcard_number=Input::get('phonno');
    //$insertdata->MB_credit=Input::get('MB_credit');
    $insertdata->storage=Input::get('storage');
    $insertdata->register_date= toGregorian(gregorian_format(Input::get('rig_date')));
    $insertdata->remark=Input::get('remark');
   
    
   // $insertdata->postpaid=Input::get('postpaid');
    // $insertdata->issue_date= toGregorian(gregorian_format(Input::get('issue_date')));
    $insertdata->save();
     getLog('equipments',$id,'inserted record into mo data');

		return Redirect::route("insert_phone")->with("success","معلومات معافقانه ثبت  ګردید.");

		
	}

  //-----------------Insert USB Dongle info

  public function load_dong_form()
  {

  // return "salam";

     $all_data1=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','equipments.remark')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->paginate(10);


     $selectequp1=\DB::connection('it_telephone')->table("equipments")->get();
    $company1=\DB::connection('it_telephone')->table("companys")->get(); 
     $equptype1=DB::connection('it_telephone')->table("equipment_types")->get(); 
     $empinfo1=\DB::connection('it_telephone')->table("employee_infos")->orderBy('id','desc')->get();
return view("it_telephone.insert_dongle_info",array('selectequp1'=>$selectequp1,'company1'=>$company1,'equptype1'=>$equptype1,'empinfo1'=>$empinfo1,'all_data1'=>$all_data1));
    

  }
  
public function insert_dong_data(Request $request)
  { 

    $insertdata1=new Equipment;
    //$insertdata1->emp_id=Input::get('name');
    $insertdata1->company_id=Input::get('company');
    $insertdata1->equipment_id= Input::get('equipment');
   // $insertdata1->equipment_range=Input::get('equipment_range') ;
    $insertdata1->equipment_serialno=Input::get('equipment_serial');
    $insertdata1->simcard_number=Input::get('phonno');
    //$insertdata1->MB_credit=Input::get('MB_credit');
    $insertdata1->storage=Input::get('storage');
    $insertdata1->register_date= toGregorian(gregorian_format(Input::get('rig_date')));
    $insertdata1->remark=Input::get('remark');
   
    
    $insertdata1->save();
 
 return Redirect::route("lod_dong")->with("success","معلومات معافقانه ثبت  ګردید.");

    
  }

  //------------------------Insert 020 info

 public function load_020_form()
  {

  // return "salam";

     $all_data2=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','equipments.remark')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->paginate(10);


     $selectequp2=\DB::connection('it_telephone')->table("equipments")->get();
    $company2=\DB::connection('it_telephone')->table("companys")->get(); 
     $equptype2=DB::connection('it_telephone')->table("equipment_types")->get(); 
     $empinfo2=\DB::connection('it_telephone')->table("employee_infos")->orderBy('id','desc')->get();
 return view("it_telephone.insert_020_info",array('selectequp2'=>$selectequp2,'company2'=>$company2,'equptype2'=>$equptype2,'empinfo2'=>$empinfo2,'all_data2'=>$all_data2));
    

  }
  
 public function insert_020_data(Request $request)
  { 

    $insertdata2=new Equipment;
    $insertdata2->company_id=Input::get('company');
    $insertdata2->equipment_id= Input::get('equipment');
    $insertdata2->equipment_serialno=Input::get('equipment_serial');
    $insertdata2->simcard_number=Input::get('phonno');
    $insertdata2->storage=Input::get('storage');
    $insertdata2->remark=Input::get('remark');
    $insertdata2->register_date= toGregorian(gregorian_format(Input::get('rig_date')));
    $insertdata2->save();
 
 return Redirect::route("lod_020")->with("success","معلومات معافقانه ثبت  ګردید.");

    
  }

  //---------------------------Insert Directorat


 public function lod_dir_form(){
// return "Salam Alikom";

  $all_data3=\DB::connection('it_telephone')->table("directorates")->get();

 return view("it_telephone.insert_directorat",array('all_data3'=>$all_data3));
}

 public function insert_dir_data(Request $request)
  { 

    $insertdata3=new Directorat;
    //$insertdata2->emp_id=Input::get('name');
    $insertdata3->directorate=Input::get('directorate');
    $insertdata3->save();
 
 return back()->with("success","معلومات معافقانه ثبت  ګردید.");

}
  //----------------------------Update Data-----------------------------
  

  public function load_editequp_form(Request $request,$id){
// dd($request);
     $edit_equp = Equipment::find($id);
     $emp_info =\DB::connection('it_telephone')->table("employee_infos")->get();
     $company=\DB::connection('it_telephone')->table("companys")->get(); 
     $equptype=DB::connection('it_telephone')->table("equipment_types")->get(); 
  return view("it_telephone.update_equp",array('edit_equp'=>$edit_equp,'company'=>$company,'equptype'=>$equptype,'emp_info'=>$emp_info));

    }
  
  public function edit_mo_data(Request $request,$id){ 
    $sim_records=Equipment::find($id);
                $data = array(
                 // "id" => Input::get('id'),
                  "company_id" => Input::get('company'),
                  "equipment_id" => Input::get('equipment'),
                  "equipment_range" => Input::get('equipment_range'),
                  "equipment_serialno" => Input::get('equipment_serial'),
                  "simcard_number" =>Input::get('phonno'),
                  "storage" => Input::get('storage'),
                  "activation" => Input::get('activation'),
                  "postpaid" => Input::get('postpaid'),
                  "remark" => Input::get('remark'),
                                  
                  );
                Equip_log::add_log($data);

     // validate the input fields
        $validates = Validator::make(Input::all(), array(
            "company" => "required",
            "equipment"   => "required"
            ));
 
        if($validates->fails())
        {
            return Redirect::route('phoneSimcardList')->withErrors($validates)->withInput();
        }
        else
        {
 
            //check the date type if it's shamsi or miladi.
            // $date = toGregorian(gregorian_format(Input::get('date')));
 
            // get the form data.

            $edit_sim_info=Equipment::find($id);
         
           $edit_sim_info->id=$request->id;
            $edit_sim_info->company_id=$request->company;
            $edit_sim_info->equipment_id= $request->equipment;
            $edit_sim_info->equipment_range=$request->equipment_range;
            $edit_sim_info->equipment_serialno=$request->equipment_serial;
            $edit_sim_info->simcard_number=$request->phonno;
            // $edit_sim_info->MB_credit=$request->MB_credit;
            $edit_sim_info->storage=$request->storage;
            $edit_sim_info->activation=$request->activation;
            // $edit_sim_info->register_date= toGregorian(gregorian_format(Input::get('rig_date')));
            $edit_sim_info->postpaid=$request->postpaid;
            $edit_sim_info->remark=$request->remark;
            $edit_sim_info->save();
            getLog('equipments',$id,'updated record');


            // $edit_sim_info=\DB::connection('it_telephone')->table("equipments")->insert($edit_sim_info);
            if($edit_sim_info){
                // getting all of the post data
                $files = Input::file('files');
                //print_r($files);exit;
               
                $errors = "";
                $auto = 1;
                $file_data = array();
 
                if(Input::hasFile('files'))
                  
                {
                    foreach($files as $file)
                    {
                        // validating each file.
                        $validator = Validator::make(
                            [
                                'file' => $file
                            ],
                            [
                                'file' => 'required|max:10000|mimes:jpg,jpeg,bmp,png,pdf,doc,docx,xls,xlsx,zip'
                            ]
                        );
                        if($validator->passes())
                        {
                            // path is root/uploads
                            $destinationPath ='phone_uploads/';
                            $original_filename = $file->getClientOriginalName();
                            $temp = explode(".", $original_filename);
                            $extension = end($temp);
 
                            $lastid = DB::connection('it_telephone')->table('equipments')->where('id',$id)->pluck('id');
 
 
                            $lastFileId = $lastid;
                            if($auto == 1)
                            $filename = "photo_".$lastFileId.".".$extension;//'attachment_'.date("Y-m-d H:i:s").'_'.$lastFileId.'.'.$extension;
                            else
                            $filename = "photo_".$lastFileId."_".$auto.".".$extension;
                            if(!file_exists($destinationPath)) {
                              File::makeDirectory($destinationPath);
                            }
                            $upload_success = $file->move($destinationPath, $filename);
 
                            if($upload_success)
                            {
                                $data = array(
                                            'file_name'             => $filename,
                                            'file_id'             => $lastid,
                                        );
                                //call the model function to insert the data into upload table.
                            $insertfile=\DB::connection('it_telephone')->table("uploads")->insert($data);
                            }
                            else
                            {
                                // redirect back with errors.
                                return Redirect::route('phoneSimcardList')->withErrors($validator);
                            }
                        }
                        $auto ++;
 
 
                    }
                }
                // get the the log data and insert it into the log table.
 
                return Redirect::route("load_emp",array($request->phonno,$request->id))->with("success","اسناد موافقانه ثبت گردید.");
            }
            else
            {
                // return Redirect::route('edit_equp')->with("fail","An error occured please try again or contact system developer.");
            }
          }
    }
 
   //----------------Update USB dongle

   public function load_editdongle_form($id){

      
     $edit_equp1 = Equipment::find($id);
     $emp_info1 =\DB::connection('it_telephone')->table("employee_infos")->get();
     $company1=\DB::connection('it_telephone')->table("companys")->get(); 
     $equptype1=DB::connection('it_telephone')->table("equipment_types")->get(); 
   return view("it_telephone.update_dongle",array('edit_equp1'=>$edit_equp1,'company1'=>$company1,'equptype1'=>$equptype1,'emp_info1'=>$emp_info1));
    }
  
   public function edit_usb_data(Request $request,$id){ 
// dd($_POST);
    $edit_equp1=Equipment::find($id);
    $edit_equp1->id=$request->id;
    // $edit_equp1->emp_id=$request->name;
    $edit_equp1->company_id=$request->company;
    $edit_equp1->equipment_id= $request->equipment;
  //  $edit_equp1->equipment_range=$request->equipment_range;
    $edit_equp1->equipment_serialno=$request->equipment_serial;
    $edit_equp1->simcard_number=$request->phonno;
    $edit_equp1->storage=$request->storage;
    $edit_equp1->MB_credit=$request->MB_credit;
    $edit_equp1->issue_date= toGregorian(gregorian_format(Input::get('issue_date')));
   // $edit_equp1->postpaid=$request->postpaid;
    $edit_equp1->remark=$request->remark;
    $edit_equp1->save();
    return Redirect::route("load_emp",array($request->phonno,$request->id))->with("success","معلومات معافقانه ثبت  ګردید.");
    }

    //----------------Update 020

    public function load_edit020_form($id){
     $edit_equp2 = Equipment::find($id);
     $emp_info2 =\DB::connection('it_telephone')->table("employee_infos")->get();
     $company2=\DB::connection('it_telephone')->table("companys")->get(); 
     $equptype2=DB::connection('it_telephone')->table("equipment_types")->get(); 
   return view("it_telephone.update_020",array('edit_equp2'=>$edit_equp2,'company2'=>$company2,'equptype2'=>$equptype2,'emp_info2'=>$emp_info2));
    }
  
   public function edit_020_data(Request $request,$id){ 
    $edit_equp2=Equipment::find($id);
    $edit_equp2->id=$request->id;
    // $edit_equp2->emp_id=$request->name;
    $edit_equp2->company_id=$request->company;
    $edit_equp2->equipment_id= $request->equipment;
    $edit_equp2->type_call=$request->type_call;
    $edit_equp2->equipment_serialno=$request->equipment_serial;
    $edit_equp2->simcard_number=$request->phonno;
    $edit_equp2->storage=$request->storage;
    $edit_equp2->activation=$request->activation;
    $edit_equp2->issue_date= toGregorian(gregorian_format(Input::get('issue_date')));
   // $edit_equp2->postpaid=$request->postpaid;
    $edit_equp2->remark=$request->remark;
    $edit_equp2->save();
    return Redirect::route("load_emp",array($request->phonno,$request->id))->with("success","معلومات معافقانه ثبت  ګردید.");
    }

   //--------------------Delete Record-------------

     function delete_equp(Request $request,$id){
     $equip_log=Equipment::find($id);
      $equip_log->status=1;
      $equip_log->save();
    return Redirect::route("phoneSimcardList")->with("success","معلومات معافقانه ثبت  ګردید.");
    }
    
    //------------------------------------Search Data---------------------
    public function search_sim(){
      $company_id=Input::get('company');
      $SIMCard=Input::get('SIMCard');
      $all_data=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','equipment_types.equipment_name','employee_infos.remark','employee_infos.issue_date')
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
      ->whereRaw("(company_id=".$company_id." AND equipment_id =".$SIMCard.")")
      ->get();
    return view("it_telephone.search_for_print",array('all_data'=>$all_data));
    }
    public function search_engin(Request $request){
      //dd($_POST);
       
        $company_id=Input::get('company');
        $equipment_range=Input::get('equipment_range');
        $postpaid=Input::get('postpaid');
        $storage=Input::get('storage');
        $equipment_id=Input::get('equipment_type');
        $all_data=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','employee_infos.name','employee_infos.lastname','employee_infos.file','equipment_types.equipment_name','employee_infos.issue_date')
             ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id','uploads.file_id')
             ->where('status','=','0')
             ->leftjoin('companys','equipments.company_id','=','companys.id')
             ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')  
             ->leftjoin('uploads','equipments.id','=','uploads.file_id')->groupBy('file_id');
                if($company_id != "")
                {
                  $all_data->where("company_id", $company_id);
                }
                if($equipment_range!="")
                {
                  $all_data->where("equipment_range", $equipment_range);
                }
                if($postpaid!="")
                {
                  $all_data->where("postpaid",$postpaid);
                }
                if($storage!="")
                {
                  $all_data->where("storage",$storage);
                }
                if($equipment_id!="")
                {
                  $all_data->where("equipment_id",$equipment_id);
                }
                // ->whereRaw("(company_id=".$company_id." AND equipment_id =".$SIMCard.")")
                $data['all_data'] = $all_data->orderBy('com_name','asc')->get();
             
                return view("it_telephone.search_engin", $data);
               
        }


    //-----------------------Live Search---------------------
    public function search_sim_live(){

     $search_string=trim(Input::get('search_string'));
    if($search_string == "Limited")
      $equipment_range = "1";
    elseif($search_string == "Unlimited")
      $equipment_range = "2";
    else
      $equipment_range = "";

    if($search_string =="PostPaid")
      $postpaid = "1";
    elseif($search_string == "PrePaid")
      $postpaid = "2";
    else
      $postpaid = "";
    if($search_string == "Active")
      $activation="1";
    elseif($search_string == "Deactive")
      $activation="2";
    else
      $activation="";
 

       $rows=\DB::connection('it_telephone')->table('equipments')
       ->select('equipments.*','equipments.activation','companys.com_name','equipment_types.equipment_name','employee_infos.remark')
      
      ->leftjoin('companys','equipments.company_id','=','companys.id')
      ->leftjoin('equipment_types','equipments.equipment_id','=','equipment_types.id')
       ->where('companys.com_name', 'like', '%'.$search_string.'%')
       ->orwhere('equipments.equipment_serialno', 'like', '%'.$search_string.'%')
       ->orwhere('equipments.simcard_number','like','%'.$search_string.'%')
       ->orwhere('equipments.equipment_range',$equipment_range)
       ->orwhere('equipments.postpaid',$postpaid)
       ->orwhere('employee_infos.name','like','%'.$search_string.'%')
       ->orwhere('employee_infos.directorate','like','%'.$search_string.'%')
       ->orwhere('equipments.activation',$activation)
       ->get();
      $data['rows'] = $rows;
       return view::make('it_telephone.search_live',$data); 
  } 
  public function exportExcel(){

    $company_id=Input::get('company');
     $equipment_range=Input::get('equipment_range');
     $postpaid=Input::get('postpaid');
      $storage=Input::get('storage');

       $all_data=\DB::connection('it_telephone')->table('equipments')->select('equipments.*','companys.com_name','employee_infos.name','employee_infos.issue_date')
        ->leftjoin('employee_infos','equipments.id', '=', 'employee_infos.sim_id')
        ->leftjoin('companys','equipments.company_id','=','companys.id');  
            if($company_id != "")
            {
              $all_data->where("company_id", $company_id);
            }
            if($equipment_range!="")
            {
              $all_data->where("equipment_range", $equipment_range);
            }
            if($postpaid!="")
            {
              $all_data->where("postpaid",$postpaid);
            }
            if($storage!="")
            {
              $all_data->where("storage",$storage);
            }
            // ->whereRaw("(company_id=".$company_id." AND equipment_id =".$SIMCard.")")
            $data= $all_data->orderBy('com_name','asc')->get();
         

            $curr_date = date('Y-m-d');
            Excel::load('excel_template/it_telephone.xlsx', function($file) use($data){
            //Excel::create('Filename', function($file) use($data){        
            $file->sheet($file->getActiveSheetIndex(1), function($sheet) use($data){   
                $row = 3;
                //$sheet->setFreeze('A3');
               
                foreach($data AS $item)
                {
                    $sheet->getStyle('A2:I' . $sheet->getHighestRow())->getAlignment()->setWrapText(true);

                      if($item->equipment_range ==1 ){ 
                        $equipment_range = "Limited";
                      }else{ 

                      $equipment_range ="Unlimited";
                      }

                      if($item->postpaid ==1){ 
                        $postpaid = "PostPaid";
                      }else{ 

                      $postpaid ="PrePaid";
                      }

                      if($item->activation ==1){ 
                        $activation = "Active";
                      }else{ 

                      $activation ="Deactive";
                      }


                    $sheet->setHeight($row, 20);
                    $sheet->setCellValue('A'.$row.'',checkEmptyDate($item->issue_date));
                    $sheet->setCellValue('B'.$row.'',$activation);
                    $sheet->setCellValue('C'.$row.'',$postpaid);
                    $sheet->setCellValue('D'.$row.'',$equipment_range);
                    $sheet->setCellValue('E'.$row.'',$item->simcard_number);
                    $sheet->setCellValue('F'.$row.'',$item->equipment_serialno);
                    $sheet->setCellValue('G'.$row.'',$item->com_name);
                    $sheet->setCellValue('H'.$row.'',$item->name);
                    $sheet->setCellValue('I'.$row.'',$row-2);
                    //$sheet->setCellValue('H'.$row.'',strip_tags($item->summary));
                   
                    $row++;
                }
 
                $sheet->setBorder('A3:I'.($row-1).'', 'thin');
               
            });
           
            })->export('xlsx');
    }
  }

    

