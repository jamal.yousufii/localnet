<?php namespace App\models\it_telephone;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;

class Equip_log extends Model {


	protected $table = 'it_telephone.equipment_log';

	public $timestamps = false;



	//protected $filedata=['khlas_mouzo','taqaror_anfaka_safarha','shomara_farman','shamara_hokam','date','jazyat_mouzo','malahaza'];
	public static function add_log($data=array())
	{
		DB::connection('it_telephone')->table('equipment_log')->insert($data);
	}
}
