@extends('layouts.master')
@section('content')
<style type="text/css">
  .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{

    background: none;
    border-bottom-color:#ffffff; 


  }
      .nav-tabs>li>a {
    padding: 10px 0px;
  
  }
  .nav>li>a>img {
    max-width: none;
    width: 220px;
    height: 94px;
  }
</style>
<div class="container" dir="rtl">
  <div class="page-head">
  <!--  <center><h3>سیستم معلوماتی مدیریت عمومی تلیفون  </h3></center>-->
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
      <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(2)").addClass("active");
      </script>
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <ul class="nav nav-tabs" >
        <li style="float: right;"><a href="{!!URL::route('sim_management')!!}" style="height:110px"><img src="{{asset('img/tab2.jpg')}}"></a></li>
        <li style="float: right;" ><a href="{!!URL::route('phoneSimcardList')!!}" style="height:110px"><img src="{{asset('img/tab3.jpg')}}" ></a></li>
        <li style="float: right;" ><a href="{!!URL::route('lod_dong')!!}" style="height:110px"><img src="{{asset('img/tab4.jpg')}}" ></a></li>
        <li style="float: right;" ><a href="{!!URL::route('lod_020')!!}" style="height:110px"><img src="{{asset('img/020.jpg')}}" ></a></li>
       
 
      </ul>

      <body>
      <!--   <div class="col-lg-4 col-sm-4"> -->
        
        <h5><a href="{!!URL::route('lod_dir')!!}" >add New Directorate</a></h5>
     <!--  </div> <div class="permanent" style="text-align: center;"> 
    </div>
 -->
      </body>
      
            </div>
          </div>
        </div>
      

@stop

@section('footer-scripts') 
 

@stop