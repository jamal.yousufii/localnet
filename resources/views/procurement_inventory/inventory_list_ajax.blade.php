<table class="table table-bordered table-responsive" id="inventoryList">
	<thead>
      <tr>
        <th>{!!_('no#')!!}</th>
        <th>{!!_('product_name')!!}</th>
        <th>{!!_('product_type')!!}</th>
        <th>{!!_('department')!!}</th>
        <th>{!!_('status')!!}</th>
        <th>{!!_('stock_in')!!}</th>
        <th>{!!_('stock_out')!!}</th>
        <th>{!!_('person_name')!!}</th>
        <th>{!!_('m7')!!}</th>
        <th>{!!_('fes_5')!!}</th>

        <th colspan="3">{!!_('operations')!!}</th>
        
      </tr>
    </thead>

    <tbody>
    	<?php 
    		if(!empty($records))
    		$counter = $records->firstItem(); 
    	?>
    	@if(!empty($records))
            @foreach($records AS $item)
                <tr class="remove_record{!!$item->id!!}">
	                <td>{!!$counter!!}</td>
	                <td>{!!$item->product_name!!}</td>
	                <td>{!!$item->product_type!!}</td>
	                <td>{!!$item->department!!}</td>
	                @if($item->status == 1)
	                <td>{!!_('new')!!}</td>
	                @elseif($item->status == 2)
	                <td>{!!_('used')!!}</td>
	                @else
	                <td>{!!_('damaged')!!}</td>
	                @endif
	                <td>{!!$item->stock_in!!}</td>
	                <td>{!!$item->stock_out!!}</td>
	                <td>{!!$item->person_name!!}</td>
	                <td>{!!$item->m7!!}</td>
	                <td>{!!$item->fes_5!!}</td>
	                <td align='center'><a href="{!!route('procInventoryEditForm',$item->id)!!}" title='{!!_("edit_record")!!}'><span class='fa fa-edit'></span></a></td>
				    <td align='center'><a onclick="getInventoryDetails('{!!$item->id!!}')" title='{!!_("view_details")!!}'><span class='fa fa-eye'></span></a></td>
			        <td align='center'><a onclick="deleteRecord('{!!$item->id!!}')" title='{!!_("delete_record")!!}'><span class='fa fa-trash'></span></a></td>
                </tr>
                <?php $counter++; ?>
            @endforeach
        @else
        <div style="padding: 10px">
        	<span style="color:red">{!!_('no_records_found_in_the_system')!!}</span>
        </div>
    	@endif
    </tbody>
</table>
<div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
	@if(!empty($records))
	{!!$records->render()!!}
	@endif
</div>
<script type="text/javascript">

$(document).ready(function() {
	
	$('.pagination a').on('click', function(event) {
		event.preventDefault();
		if ($(this).attr('href') != '#') {
			//$('#ajaxContent').load($(this).attr('href'));
			
			$.ajax({
	                url: '{!!URL::route("procInventoryList")!!}',
	                data: {"page":$(this).text(),"ajax":1},
	                type: 'get',
	                beforeSend: function(){
	
	                    //$("body").show().css({"opacity": "0.5"});
	                    $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
	
	                    $('#search_result').html(response);
	                }
	            }
	        );
		
		}
	});
	
	//get the inventory list based on product type onchange of the dropdown.
	$("#search_form").submit(function(){
	
		var dataString = $("#search_form").serialize();
	    $.ajax({
	            url: "{!!URL::route('loadSearchedProcInventories')!!}",
	            data: dataString,
	            type: 'post',
	            beforeSend: function(){
	                $("#search_result").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	            },
	            success: function(response)
	            {
	                $("#search_result").html(response);
	            }
	        }
	    );
		return false;
	});
});
	// Delete Record ajax script;
    function deleteRecord(record_id) 
    {
        var ID = record_id;
        var dataString = "record_id="+ID;
        if(confirm("Are you sure you want to do this action ? THERE IS NO UNDO"))
        {
            $.ajax({
                 type: "POST",
                 url: "{!!URL::route('getDeleteProcInventory')!!}",
                 data: dataString,
                 cache: false,
                 success: function(mydata){
                    $(".remove_record"+ID).css('background','Crimson');
                    $(".remove_record"+ID).slideUp('6000', function(){
                    $(this).remove();
                    //$("#deleted_result").html(mydata);
                    });
                 }
            });
        }
        return false;
    }
</script>