@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('inventory_list')!!}</title>
    <style type="text/css">
        table thead tr th
        {
            text-align: center;
            border-color: #000; 
            text-align: center !important;
        }
        table tbody tr td
        {
            border-color: #000;
            text-align: center !important;
        }
        .span-class
        {
        	font-weight : bold;
        	padding : -50px;
        }
        a{cursor:pointer;}
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
    {!! HTML::style('/css/autocomplete/jquery-ui.css') !!}

@stop

@section('content')

    @if(Session::has('success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('success')!!}
		</div>

    @elseif(Session::has('fail'))
    	<div class="alert alert-danger alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('fail')!!}
		</div>
    @endif

    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div align="center">
                    <h1>{!!_('inventory_list')!!}</h1>
                </div>
            </div>
            <div class="form-group" style="margin:20px 0px 0px -15px">
            	<form id="search_form" class="form-horizontal">
	                <div class="col-sm-4" style="margin-right: -12px">
	                    <select class="form-control" name="department" id="department" style="width:100%">
	                    	<option value=''>{!!_('select_department')!!}</option>
				        	{!!getDepartments()!!}
				        </select>
	                </div>
	                <div class="col-sm-2">
	                    <select class="form-control" name="product_type" id="product_type" style="width:100%">
	                    	<option value=''>{!!_('select_product_type')!!}</option>
				        	{!!getProcInventoryProductTypes()!!}
				        </select>
	                </div>
	                <div class="col-sm-2">
	                    <select class="form-control" name="person_name" id="person_name" style="width:100%">
	                    	<option value=''>{!!_('select_person_name')!!}</option>
				        	{!!getPersonName()!!}
				        </select>
	                </div>
	                <div class="col-sm-3">
	                    <input type="submit" value="{!!_('search')!!}" class="btn btn-warning"/>
	                    &nbsp;<input type="reset" value="{!!_('clear')!!}" class="btn btn-danger" id="clear"/>
	                </div>
	        	</form>
	        	<?php 
			    	$dir = getLangShortcut();
			    	if($dir == "en"){$dir = "pull-right";}else{$dir = "pull-left";}
			    ?>
                <span class="{!!$dir!!}"><a href="{!!URL::route('procInventoryForm')!!}" class="btn btn-success" style="margin-left: 15px;">{!!_('add_new_inventory')!!}</a></span>
            </div>
        </div>
        <hr>
        <div style="padding:15px" class="table-responsive" id="search_result">
            <table class="table table-bordered table-responsive" id="inventoryList">
            	<thead>
		          <tr>
		            <th>{!!_('no#')!!}</th>
		            <th>{!!_('product_name')!!}</th>
		            <th>{!!_('product_type')!!}</th>
		            <th>{!!_('department')!!}</th>
		            <th>{!!_('status')!!}</th>
		            <th>{!!_('stock_in')!!}</th>
		            <th>{!!_('stock_out')!!}</th>
		            <th>{!!_('person_name')!!}</th>
		            <th>{!!_('m7')!!}</th>
		            <th>{!!_('fes_5')!!}</th>
		
		            <th colspan="3">{!!_('operations')!!}</th>
		            
		          </tr>
		        </thead>
		
		        <tbody>
		        	<?php 
		        		if(!empty($records))
		        		$counter = $records->firstItem(); 
		        	?>
		        	@if(!empty($records))
			            @foreach($records AS $item)
			                <tr class="remove_record{!!$item->id!!}">
				                <td>{!!$counter!!}</td>
				                <td>{!!$item->product_name!!}</td>
				                <td>{!!$item->product_type!!}</td>
				                <td>{!!$item->department!!}</td>
				                @if($item->status == 1)
				                <td>{!!_('new')!!}</td>
				                @elseif($item->status == 2)
				                <td>{!!_('used')!!}</td>
				                @else
				                <td>{!!_('damaged')!!}</td>
				                @endif
				                <td>{!!$item->stock_in!!}</td>
				                <td>{!!$item->stock_out!!}</td>
				                <td>{!!$item->person_name!!}</td>
				                <td>{!!$item->m7;!!}</td>
				                <td>{!!$item->fes_5!!}</td>
				                <td align='center'><a href="{!!route('procInventoryEditForm',$item->id)!!}" title='{!!_("edit_record")!!}'><span class='fa fa-edit'></span></a></td>
				                <td align='center'><a onclick="getInventoryDetails('{!!$item->id!!}')" title='{!!_("view_details")!!}'><span class='fa fa-eye'></span></a></td>
			                	<td align='center'><a onclick="deleteRecord('{!!$item->id!!}')" title='{!!_("delete_record")!!}'><span class='fa fa-trash'></span></a></td>
			                </tr>
			                <?php $counter++; ?>
			            @endforeach
		            @else
			        <div style="padding: 10px">
			        	<span style="color:red">{!!_('no_records_found_in_the_system')!!}</span>
			        </div>
			    	@endif
		        </tbody>
            </table>
			<div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
				@if(!empty($records))
				{!!$records->render()!!}
				@endif
			</div>
        </div>
    </div>
    
    <div class="modal fade modal-info" id="exampleModalPrimary" aria-hidden="true" aria-labelledby="exampleModalPrimary" role="dialog" tabindex="-1">
	  <div class="modal-dialog" style="width: 1200px;">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">×</span>
	        </button>
	        <h4 class="modal-title"><i class="fa fa-plus fa-lg"></i> {!!_('inventory_details')!!}</h4>
	      </div>
	      <div id="form_part">
	
	      </div>
	    </div>
	  </div>
	</div>
    
@stop
@section('footer-scripts')

<script type="text/javascript">
$("#product_type").select2();
$('#department').select2();
$('#person_name').select2();
$(document).ready(function() {

	$("#clear").click(function()
	{
		$("#department").select2('val', '');
		$("#person_name").select2('val', '');
	});
	
	$('.pagination a').on('click', function(event) {
		event.preventDefault();
		if ($(this).attr('href') != '#') {
			//$('#ajaxContent').load($(this).attr('href'));
			
			$.ajax({
	                url: '{!!URL::route("procInventoryList")!!}',
	                data: {"page":$(this).text(),"ajax":1},
	                type: 'get',
	                beforeSend: function(){
	
	                    //$("body").show().css({"opacity": "0.5"});
	                    $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
	
	                    $('#search_result').html(response);
	                }
	            }
	        );
		
		}
	});
	
	//get the inventory list based on product type onchange of the dropdown.
	$("#search_form").submit(function(){
	
		var dataString = $("#search_form").serialize();
	    $.ajax({
	            url: "{!!URL::route('loadSearchedProcInventories')!!}",
	            data: dataString,
	            type: 'post',
	            beforeSend: function(){
	                $("#search_result").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	            },
	            success: function(response)
	            {
	                $("#search_result").html(response);
	            }
	        }
	    );
		return false;
	});
});

	function getInventoryDetails(id){
	  $.ajax({
	          url: '{!!URL::route("getProcInventoryDetails")!!}',
	          data: {'id':id,'_token': "<?= csrf_token();?>"},
	          type: 'post',
	          beforeSend: function(){
	
	              //$("body").show().css({"opacity": "0.5"});
	              $('#form_part').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/images/ajax-loader.gif")!!}</span>');
	          },
	          success: function(response)
	          {
	
	              $('#form_part').html(response);
	              $('#exampleModalPrimary').modal("show");
	
	          }
	    });
	}
	// Delete Record ajax script;
    function deleteRecord(record_id) 
    {
        var ID = record_id;
        var dataString = "record_id="+ID;
        if(confirm("آیا مطمئین هستید ؟"))
        {
            $.ajax({
                 type: "POST",
                 url: "{!!URL::route('getDeleteProcInventory')!!}",
                 data: dataString,
                 cache: false,
                 success: function(mydata){
                    $(".remove_record"+ID).css('background','Crimson');
                    $(".remove_record"+ID).slideUp('6000', function(){
                    $(this).remove();
                    //$("#deleted_result").html(mydata);
                    });
                 }
            });
        }
        return false;
    }
</script>

@stop


