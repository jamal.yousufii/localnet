@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('inventory_edit_form')!!}</title>
    <style type="text/css">
        
    </style>
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
    {!! HTML::style('/css/autocomplete/jquery-ui.css') !!}

@stop

@section('content')

	@if (count($errors) > 0)
    <div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
        <strong>Whoops !</strong>There were some problems with your input, please check it and try again. <br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
    </div>
    @endif
    @if(Session::has('success'))
        <div class='alert alert-success noprint'>{{Session::get('success')}}</div>
    @elseif(Session::has('fail'))
        <div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
    @endif
    @if(Session::has('failed'))
        <div class="alert alert-danger">
            <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
        </div>
    @endif
    
    <?php 
	    if(isShamsiDate())
	    {
			$date = checkEmptyDate($inventory->date);
			$m7_date = checkEmptyDate($inventory->m7_date);
	    }
		else
		{
			$date = checkGregorianEmtpyDate($inventory->date);
			$m7_date = checkGregorianEmtpyDate($inventory->m7_date);
		}
			
	?>
<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <h3 id="form_title">{!!_('inventory_edit_form')!!}</h3>
            </div>
        </div>
        <hr style="border: 1px dashed #b6b6b6" />
    </div>

    <div class="container">
        <form role="form" method="post" action="{!! URL::route('postEditProcInventoryData', $inventory->id) !!}" class="form-horizontal">
			
            <div id="first_page">
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('tag_number')!!} </label>
                    <div class="col-sm-2">
                    	<input type="number" class="form-control" name='tag_number' id="tag_number" value="{!!$inventory->tag_number!!}" disabled />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('serial_number')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='serial_number' id="serial_number" value="{!!$inventory->serial_number!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('model')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='model' id="model" value="{!!$inventory->model!!}" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('product_name')!!} </label>
                    <div class="col-sm-2">
                        <input class="form-control" type="text" name="product_name" id="product_name" value="{!!$inventory->product_name;!!}" required/>
                    </div>
                    <div id="product_type_div">
	                    <label class="col-sm-2 control-label">{!!_('product_type')!!} </label>
	                    <div class="col-sm-2">
		                    <select class="form-control" name='product_type' id="product_type" style="width:100% !important;">
		                        <option value=''>---</option>
		                        {!!getProcInventoryProductTypes($inventory->product_type)!!}
		                    </select>
	                    </div>
	                </div>
	                <div id="other_product_div" style="display:none">
	                    <label class="col-sm-2 control-label">{!!_('product_type')!!} </label>
	                    <div class="col-sm-2">
		                    <input type="text" class="form-control" name='other_product' id="other_product" />
	                    </div>
	                </div>
                    <label class="col-sm-2 control-label">{!!_('status')!!} </label>
                    <div class="col-sm-2">
	                    <select class="form-control" name='status' id="status" value="{!!$inventory->status!!}">
	                    	<option value="1" <?php if($inventory->status == 1) echo "selected='selected'";?>>{!!_('new')!!}</option>
	                    	<option value="2" <?php if($inventory->status == 2) echo "selected='selected'";?>>{!!_('used')!!}</option>
	                    	<option value="3" <?php if($inventory->status == 3) echo "selected='selected'";?>>{!!_('damaged')!!}</option>
	                    </select>
                    </div>
                </div>
    			<div class="form-group">
    				<label class="col-sm-2 control-label">{!!_('person_name')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='person_name' id="person_name" value="{!!$inventory->person_name!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('department')!!} </label>
                    <div class="col-sm-6">
	                    <select class="form-control" name='department' id="department" style="width:100%">
	                    	{!!getDepartments($inventory->department)!!}
	                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('stock_in')!!} </label>
                    <div class="col-sm-2">
	                    <input type="number" class="form-control" name='stock_in' id="stock_in" value="{!!$inventory->stock_in!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('stock_out')!!} </label>
                    <div class="col-sm-2">
	                    <input type="number" class="form-control" name='stock_out' id="stock_out" value="{!!$inventory->stock_out!!}" />
                    </div>
                	<label class="col-sm-2 control-label">{!!_('date')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="{!!getDatePickerClass()!!} form-control" name='date' id="date" value="{!!$date!!}" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('masrafi')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='masrafi' id="masrafi" value="{!!$inventory->masrafi!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('item_register_page')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='item_register_page' id="item_register_page" value="{!!$inventory->item_register_page!!}" />
                    </div>
                	<label class="col-sm-2 control-label">{!!_('cost')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='cost' id="cost" value="{!!$inventory->cost!!}" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('total_cost')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='total_cost' id="total_cost" value="{!!$inventory->total_cost!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('m7')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='m7' id="m7" value="{!!$inventory->m7!!}" />
                    </div>
                	<label class="col-sm-2 control-label">{!!_('m7_date')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="{!!getDatePickerClass()!!} form-control" name='m7_date' id="m7_date" value="{!!$m7_date!!}" readonly="readonly" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('fes_5')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='fes_5' id="fes_5" value="{!!$inventory->fes_5!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('remarks')!!} </label>
                    <div class="col-sm-6">
	                    <textarea cols="20" rows="2" class="form-control" name='remarks' id="remarks">{!!$inventory->remarks!!}</textarea>
                    </div>
                </div>
                {!! Form::token() !!}
                <div class="form-group">
                    <div class="col-sm-12" style="margin-top:30px">
	                    <button class="btn btn-success" type="submit">
	                        <span>
	                            <i class="fa fa-save"></i>
	                        </span>
	                        &nbsp;{!!_('save_changes')!!}
	                    </button>
	                    <a href="javascript:history.back()" class="btn btn-danger">
	                        <span>
	                            <i class="fa fa-remove"></i>
	                        </span>
	                        &nbsp;{!!_('cancel')!!}
	                    </a>
	                </div>
                </div>
                <hr style="border: 1px dashed #b6b6b6" />
            </div>
        </form>
    
    </div>

</div>

@stop
@section('footer-scripts')
{!! HTML::script('/js/autocomplete/jquery-ui.js')!!}

<script type="text/javascript">
	$('#department').select2();
	$('#product_type').select2();
	$("#product_type").append("<option value='0'>Other</option>");
	$("#product_type").change(function(){
		if($("#product_type").val() == "0")
		{
			$("#product_type_div").hide();
			//$("#product_type").prop("disabled",true);
			$("#other_product_div").fadeIn('slow');
		}
		else
		{
			$("#other_prduct_div").hide();
			//$("#other_product").prop("disabled",true);
			$("#prduct_type_div").fadeIn('slow');
		}
	});
	function getStockIn(){
		$.ajax({ 
				url:"{!!URL::route('getProcInvStockInByProductType')!!}",
				data: { product_type: $("#product_type").val()},
				type: "POST",
				success: function(data){
					if(data != 0 || data != "")
					{
						$("#stock_in").val(data);
						$("#stock_in").prop('readonly',true);
					}else{
						$("#stock_in").val("");
						$("#stock_in").prop('readonly',false);
					}
				}
		});
	}
	$( "#product_name" ).autocomplete({
	
		source: function(request, response) {
			
			$.ajax({ 
				url:"{!!URL::route('getProcInventoryAutocompleteList', array('table' => 'inventory', 'field' => 'product_name'))!!}",
				data: { term: $("#product_name").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);				
				}
		});
	},
	minLength: 1
	});

    $(function(){


    });

</script>


@stop