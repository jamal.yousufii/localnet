@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('inventory_form')!!}</title>
    <style type="text/css">
        
    </style>
	{!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
@stop

@section('content')
	
	@if (count($errors) > 0)
	<div class="alert alert-danger" style="margin: 10px 0 20px 0">
      <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
	
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
        </div>

    @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
        </div>
    @endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix row">
            <h3 id="form_title">{!!_('inventory_form')!!}</h3>
        	<?php 
				$dir = getLangShortcut();
				if($dir == "en"){$dir = "pull-right";}else{$dir = "pull-left";}
			?>
    		<span class="{!!$dir!!}"><a href="javascript:history.back()" class="btn btn-success" style="margin-left: 100px">{!!_('back')!!}</a></span>
        </div>
        <hr style="border: 1px dashed #b6b6b6" />
    </div>

    <div class="container">
    
        <form role="form" method="post" action="{!! URL::route('postAddProcInventoryData') !!}" class="form-horizontal" enctype="multipart/form-data">
			
            <div id="first_page">
            	<div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('tag_number')!!} </label>
                    <div class="col-sm-2">
                    	<input type="number" class="form-control" name='tag_number' id="tag_number" value="{!!old('tag_number')!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('serial_number')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='serial_number' id="serial_number" value="{!!old('serial_number')!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('model')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='model' id="model" value="{!!old('model')!!}" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('product_name')!!} </label>
                    <div class="col-sm-2">
                        <input class="form-control" type="text" name="product_name" id="product_name" value="{!!old('product_name')!!}" required/>
                    </div>
                    <div id="product_type_div">
	                    <label class="col-sm-2 control-label">{!!_('product_type')!!} </label>
	                    <div class="col-sm-2">
		                    <select class="form-control" name='product_type' id="product_type" value="{!!old('product_type')!!}"  data-plugin="select2" style="width:100% !important;">
		                        <option value=''>---</option>
		                        {!!getProcInventoryProductTypes()!!}
		                    </select>
	                    </div>
	                </div>
	                <div id="other_product_div" style="display:none">
	                    <label class="col-sm-2 control-label">{!!_('product_type')!!} </label>
	                    <div class="col-sm-2">
		                    <input type="text" class="form-control" name='other_product' id="other_product" value="{!!old('product_type')!!}" />
	                    </div>
	                </div>
                    <label class="col-sm-2 control-label">{!!_('status')!!} </label>
                    <div class="col-sm-2">
	                    <select class="form-control" name='status' id="status" value="{!!old('status')!!}">
	                    	<option value="">{!!_('select')!!}</option>
	                    	<option value="1">{!!_('new')!!}</option>
	                    	<option value="2">{!!_('used')!!}</option>
	                    	<option value="3">{!!_('damaged')!!}</option>
	                    </select>
                    </div>
                </div>
    			<div class="form-group">
    				<label class="col-sm-2 control-label">{!!_('person_name')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='person_name' id="person_name" value="{!!old('person_name')!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('department')!!} </label>
                    <div class="col-sm-6">
	                    <select class="form-control" name='department' id="department" style="width:100%">
	                    	<option value="">{!!_('select_department')!!}</option>
	                    	{!!getDepartments()!!}
	                    </select>
                    </div>
                </div>
                <div class="form-group">
                	<label class="col-sm-2 control-label">{!!_('stock_in')!!} </label>
                    <div class="col-sm-2">
	                    <input type="number" class="form-control" name='stock_in' id="stock_in" value="{!!old('stock_in')!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('stock_out')!!} </label>
                    <div class="col-sm-2">
	                    <input type="number" class="form-control" name='stock_out' id="stock_out" value="{!!old('stock_out')!!}" />
                    </div>
                	<label class="col-sm-2 control-label">{!!_('date')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="{!!getDatePickerClass()!!} form-control" name='date' id="date" value="{!!old('date')!!}" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('masrafi')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='masrafi' id="masrafi" value="{!!old('masrafi')!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('item_register_page')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='item_register_page' id="item_register_page" value="{!!old('item_register_page')!!}" />
                    </div>
                	<label class="col-sm-2 control-label">{!!_('cost')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='cost' id="cost" value="{!!old('cost')!!}" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('total_cost')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='total_cost' id="total_cost" value="{!!old('total_cost')!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('m7')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='m7' id="m7" value="{!!old('m7')!!}" />
                    </div>
                	<label class="col-sm-2 control-label">{!!_('m7_date')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="{!!getDatePickerClass()!!} form-control" name='m7_date' id="m7_date" value="{!!old('m7_date')!!}" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{!!_('fes_5')!!} </label>
                    <div class="col-sm-2">
	                    <input type="text" class="form-control" name='fes_5' id="fes_5" value="{!!old('fes_5')!!}" />
                    </div>
                    <label class="col-sm-2 control-label">{!!_('scanned_file')!!}</label>
					<div class="col-sm-2">
						<input type="file" class="form-control" name="file">
					</div>
                    <label class="col-sm-2 control-label">{!!_('remarks')!!} </label>
                    <div class="col-sm-2">
	                    <textarea cols="20" rows="2" class="form-control" name='remarks' id="remarks">{!!old('remarks')!!}</textarea>
                    </div>
                </div>
                {!! Form::token() !!}
                <div class="form-group">
                    <div class="col-sm-12">
                        <hr />
                        <button class="btn btn-primary" id="add_task" type="submit">
                            <span>
                                <i class="fa fa-check"></i>
                            </span>
                            &nbsp;{!!_('save')!!}
                        </button>
                    </div>
                </div>
                <hr style="border: 1px dashed #b6b6b6" />
            </div>
        </form>
    
    </div>

</div>

@stop
@section('footer-scripts')
{!! HTML::script('/js/autocomplete/jquery-ui.js')!!}

<script type="text/javascript">

	$("#department").select2();
	$("#product_type").append("<option value='0'>Other</option>");
	$("#product_type").change(function(){
		if($("#product_type").val() == "0")
		{
			$("#product_type_div").hide();
			//$("#product_type").prop("disabled",true);
			$("#other_product_div").fadeIn('slow');
		}
		else
		{
			$("#other_prduct_div").hide();
			//$("#other_product").prop("disabled",true);
			$("#prduct_type_div").fadeIn('slow');
		}
	});
	function getStockIn(){
		$.ajax({ 
				url:"{!!URL::route('getProcInvStockInByProductType')!!}",
				data: { product_type: $("#product_type").val()},
				type: "POST",
				success: function(data){
					if(data != 0 || data != "")
					{
						$("#stock_in").val(data);
						$("#stock_in").prop('readonly',true);
					}else{
						$("#stock_in").val("");
						$("#stock_in").prop('readonly',false);
					}
				}
		});
	}
	$("#product_name").autocomplete({
	
		source: function(request, response) {
			
			$.ajax({ 
				url:"{!!URL::route('getProcInventoryAutocompleteList', array('table' => 'inventory', 'field' => 'product_name'))!!}",
				data: { term: $("#product_name").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);				
				}
		});
	},
	minLength: 1
	});
	
	$("#person_name").autocomplete({
	
		source: function(request, response) {
			
			$.ajax({ 
				url:"{!!URL::route('getProcInventoryAutocompleteList', array('table' => 'inventory', 'field' => 'person_name'))!!}",
				data: { term: $("#person_name").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);				
				}
		});
	},
	minLength: 1
	});

</script>


@stop