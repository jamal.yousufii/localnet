	<?php 
	    if(isShamsiDate())
	    {
			$date = checkEmptyDate($inventory->date);
			$m7_date = checkEmptyDate($inventory->m7_date);
	    }
		else
		{
			$date = checkGregorianEmtpyDate($inventory->date);
			$m7_date = checkGregorianEmtpyDate($inventory->m7_date);
		}
			
	?>
    <form class="form-horizontal" role="form" method="post">
      	<div class="modal-body">
      
          	<div class="form-group">
	              <label class="col-sm-2 control-label">{!!_('tag_number')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="number" class="form-control" value="{!!$inventory->tag_number!!}" disabled />
	              </div>
	              <label class="col-sm-2 control-label">{!!_('serial_number')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="number" class="form-control" value="{!!$inventory->serial_number!!}" disabled />
	              </div>
	        </div>
	    	<div class="form-group">
	              <label class="col-sm-2 control-label">{!!_('product_name')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="text" class="form-control" value="{!!$inventory->product_name!!}" disabled />
	              </div>
	              <label class="col-sm-2 control-label">{!!_('product_type')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="text" class="form-control" value="{!!getProductTypeName($inventory->product_type)!!}" disabled />
	              </div>
          	</div>
          	<div class="form-group">
	              <label class="col-sm-2 control-label">{!!_('model')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="text" class="form-control" value="{!!$inventory->model!!}" disabled />
	              </div>
	              <label class="col-sm-2 control-label">{!!_('status')!!} : </label>
	              <div class="col-sm-4">
	              	  @if($inventory->status == 1)
	                  <input type="text" class="form-control" value="{!!_('new')!!}" disabled />
	                  @elseif($inventory->status == 2)
	                  <input type="text" class="form-control" value="{!!_('damaged')!!}" disabled />
	                  @else
	                  <input type="text" class="form-control" value="{!!_('used')!!}" disabled />
	                  @endif
	              </div>
          	</div>
          	<div class="form-group">
	              <label class="col-sm-2 control-label">{!!_('stock_in')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="number" class="form-control" value="{!!$inventory->stock_in!!}" disabled />
	              </div>
	              <label class="col-sm-2 control-label">{!!_('stock_out')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="number" class="form-control" value="{!!$inventory->stock_out!!}" disabled />
	              </div>
          	</div>
          	<div class="form-group">
	              <label class="col-sm-2 control-label">{!!_('person_name')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="text" class="form-control" value="{!!$inventory->person_name!!}" disabled />
	              </div>
	              <label class="col-sm-2 control-label">{!!_('date')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="text" class="form-control" value="{!!$date!!}" disabled />
	              </div>
          	</div>
          	<div class="form-group">
	              <label class="col-sm-2 control-label">{!!_('masrafi')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="text" class="form-control" value="{!!$inventory->masrafi!!}" disabled />
	              </div>
	              <label class="col-sm-2 control-label">{!!_('item_register_page')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="text" class="form-control" value="{!!$inventory->item_register_page!!}" disabled />
	              </div>
          	</div>
          	<div class="form-group">
	              <label class="col-sm-2 control-label">{!!_('cost')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="number" class="form-control" value="{!!$inventory->cost!!}" disabled />
	              </div>
	              <label class="col-sm-2 control-label">{!!_('total_cost')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="number" class="form-control" value="{!!$inventory->total_cost!!}" disabled />
	              </div>
          	</div>
          	<div class="form-group">
	              <label class="col-sm-2 control-label">{!!_('m7')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="text" class="form-control" value="{!!$inventory->m7!!}" disabled />
	              </div>
	              <label class="col-sm-2 control-label">{!!_('m7_date')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="text" class="form-control" value="{!!$m7_date!!}" disabled />
	              </div>
          	</div>
          	<div class="form-group">
	              <label class="col-sm-2 control-label">{!!_('fes_5')!!} : </label>
	              <div class="col-sm-4">
	                  <input type="text" class="form-control" value="{!!$inventory->fes_5!!}" disabled />
	              </div>
	              <label class="col-sm-2 control-label">{!!_('remarks')!!} : </label>
	              <div class="col-sm-4">
	                  <textarea class="form-control" value="{!!$inventory->remarks!!}" disabled>{!!$inventory->remarks!!}</textarea>
	              </div>
          	</div>
          	<div class="form-group">
	              <label class="col-sm-2 control-label">{!!_('attachment')!!} : </label>
	              <div class="col-sm-4">
	              @if($file != "")
	                  <img src="../documents/procurement_inventory_attachments/{!!$file->file_name!!}"  width="380px" height="300px" />
	              @else
	              <p style="color:red">{!!_('no_uploaded_file')!!} !</p>
	              @endif
	              </div>
	              <label class="col-sm-2 control-label">{!!_('department')!!} : </label>
	              <div class="col-sm-4">
	                  <textarea class="form-control" value="{!!$inventory->department!!}" disabled>{!!$inventory->department!!}</textarea>
	              </div>
          	</div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
      </div>
    </form>