@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('notification_details')!!}</title>
    <style type="text/css">
        
    </style>

@stop

@section('content')

	<?php
		if($notification->status == "0"){$status = "Emergency";}elseif($notification->status == "1"){$status = "Not Started";}
		elseif($notification->status == "2"){$status = "Started";}elseif($notification->status == "3"){$status = "Finished";}
		elseif($notification->status == "4"){$status = "Feedback";}
		else{$status = "";}
		
		if($notification->approved == 1){$approved = "Approved";}else{$approved = "";}
		if($notification->viewed == 1){$viewed = "Viewed";}else{$viewed = "";}
	?>
	
<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <h3 id="form_title">{!!_('notification_details')!!}</h3>
            </div>
        </div>
        <hr style="border: 1px dashed #b6b6b6" />
    </div>

    <div class="container">

        <form role="form" class="form-horizontal">
			
			<div class="form-group">
                <label class="col-sm-2 control-label">{!!_('type')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" value="{!!$notification->type!!}" readonly/>
                </div>
                <label class="col-sm-2 control-label">{!!_('subject')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" value="{!!$notification->subject!!}" readonly/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('description')!!} : </label>
                <div class="col-sm-4">
                    <textarea cols="30" rows="3" class="form-control" name="description" readonly>{!!$notification->description!!}</textarea>
                </div>
                <label class="col-sm-2 control-label">{!!_('status')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" value="{!!$status!!}" readonly/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('approved')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" value="{!!$approved!!}" readonly/>
                </div>
                <label class="col-sm-2 control-label">{!!_('viewed')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" value="{!!$viewed!!}" readonly/>
                </div>
            </div>
            <hr style="border: 1px dashed #b6b6b6" />
            
        </form>
    
    </div>

</div>

@stop

@section('footer-scripts')
<script type="text/javascript">

</script>


@stop