
<div class="col-lg-12">
    <div id="content-header" class="clearfix">
        <div class="pull-left">
            <h3 id="form_title">{!!_('task_description_revisions')!!}</h3>
        </div>
    </div>
    <hr />
</div>
<?php $i = 1; ?>
@foreach($task_description_update as $item)
	@if($i < count($task_description_update))
        <div class="form-group">
            <label class="col-sm-3 control-label">{!!_('task_description_in_english')!!} : </label>
            <div class="col-sm-3">
                {!!$item->task_description_en!!}
            </div>
            <label class="col-sm-3 control-label">{!!_('task_description_in_dari_or_pashto')!!} : </label>
            <div class="col-sm-3">
                {!!$item->task_description_dr_ps!!}
            </div>
        </div>
    @else
    	<div class="form-group">
        	<input type="hidden" id="case_id" value="{!!$case_id!!}" />
            <label class="col-sm-3 control-label">{!!_('task_description_in_english')!!} : </label>
            <div class="col-sm-3">
                <textarea cols="30" rows="2" class="form-control" id="task_description_en" name="task_description_en">{!!$item->task_description_en!!}</textarea>
            </div>
            <label class="col-sm-3 control-label">{!!_('task_description_in_dari_or_pashto')!!} : </label>
            <div class="col-sm-3">
                <textarea cols="30" rows="2" class="form-control" id="task_description_dr_ps" name="task_description_dr_ps">{!!$item->task_description_dr_ps!!}</textarea>
            </div>
        </div>
    @endif
    <?php $i++;?>
@endforeach
<div class="form-group">
    <div class="col-sm-12">
        <button class="btn btn-primary" type="button" onclick="addDescRevisions('desc_result')">
            <span>
                <i class="fa fa-check"></i>
            </span>
            &nbsp;{!!_('save_description')!!}
        </button>
    </div>
</div>