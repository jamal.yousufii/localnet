@extends('layouts.master')

@section('head')
    @parent
    <title>{!!_('department_management_form')!!}</title>
    <style type="text/css">
        table th, table td{
            text-align: center !important;
        }
    </style>
	{!! HTML::style('/css/autocomplete/jquery-ui.css') !!}
@stop

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
        </div>

    @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
        </div>
    @endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <h3>{!!_('department_management_edit_form')!!}</h3>
            </div>
        </div>
        <hr style="border: 1px solid #eee" />
    </div>

    <div class="container">
    @if($department != null)
		@foreach($department as $item)
	        <form role="form" method="post" action="{!! URL::route('postEditRequestAndDep', array('id' => $item->id)) !!}" class="form-horizontal">
	            
	            <h4 style="padding: 20px">{!!_('edit_department')!!}</h4>
	            <div class="form-group">
	                <label class="col-sm-2 control-label">{!!_('department')!!} : </label>
	                <div class="col-sm-4">
	                    <input class="form-control" type="text" id="department" name="department" value="{!!$item->name!!}" required>
	                </div>
	                <div class="form-group">
	                    @if($errors->has("related_department"))
	                    <div class="col-sm-6" style="padding:10px">
	                        <span style="color: red">{!! $errors->first('related_department') !!}</span>
	                    </div>
	                    @endif
	            	</div>
	            </div>
	            <div class="form-group" style="margin-top: 50px">
	                <div class="col-sm-12">
	                    
	                    <button class="btn btn-primary" type="submit">
	                        <span>
	                            <i class="fa fa-check"></i>
	                        </span>
	                        &nbsp;{!!_('save_changes')!!}
	                    </button>
	                    
	                </div>
	            </div>
	        </form>
	    @endforeach
	@endif
	@if($person != null)
		@foreach($person as $item)
	        <form role="form" method="post" action="{!! URL::route('postEditRequestAndDep', array('id' => $item->id)) !!}" class="form-horizontal">
	            
	            <h4 style="padding: 20px">{!!_('edit_the_person_and_related_department')!!}</h4>
	            <div class="form-group">
	                <label class="col-sm-2 control-label">{!!_('person_name')!!} : </label>
	                <div class="col-sm-4">
	                    <input class="form-control" type="text" id="person" name="name" value="{!!$item->name!!}" required>
	                </div>
	                <label class="col-sm-2 control-label">{!!_('related_department')!!} : </label>
	                <div class="col-sm-4">
	                    <select class="form-control" name="related_department" required>
	                    	{!!getTaskDepartments($item->department);!!}
	                    </select>
	                </div>
	                <div class="form-group">
	            		@if($errors->has("name"))
	                    <div class="col-sm-6" style="padding:10px">
	                        <span style="color: red">{!! $errors->first('name') !!}</span>
	                    </div>
	                    @endif
	                    @if($errors->has("related_department"))
	                    <div class="col-sm-6" style="padding:10px">
	                        <span style="color: red">{!! $errors->first('related_department') !!}</span>
	                    </div>
	                    @endif
	            	</div>
	            </div>
	            <div class="form-group" style="margin-top: 50px">
	                <div class="col-sm-12">
	                    
	                    <button class="btn btn-primary" type="submit">
	                        <span>
	                            <i class="fa fa-check"></i>
	                        </span>
	                        &nbsp;{!!_('save_changes')!!}
	                    </button>
	                    
	                </div>
	            </div>
	        </form>
	    @endforeach
    @endif
    </div>

</div>

@stop

@section('footer-scripts')
{!! HTML::script('/js/autocomplete/jquery-ui.js')!!}

<script type="text/javascript">

    $(function(){     
		$( "#department" ).autocomplete({

			source: function(request, response) {
				
				$.ajax({ 
					url:"{!!URL::route('getAutocompleteList', array('table' => 'department', 'field' => 'name'))!!}",
					data: { term: $("#department").val()},
					dataType: "json",
					type: "POST",
					success: function(data){
						response(data);				
					}
			});
		},
		minLength: 1
		});
		
		$( "#person" ).autocomplete({

			source: function(request, response) {
				
				$.ajax({ 
					url:"{!!URL::route('getAutocompleteList', array('table' => 'person', 'field' => 'name'))!!}",
					data: { term: $("#person").val()},
					dataType: "json",
					type: "POST",
					success: function(data){
						response(data);				
					}
			});
		},
		minLength: 1
		});
    	
    	$( "#related_department" ).autocomplete({

			source: function(request, response) {
				
				$.ajax({ 
					url:"{!!URL::route('getAutocompleteList', array('table' => 'department', 'field' => 'name'))!!}",
					data: { term: $("#related_department").val()},
					dataType: "json",
					type: "POST",
					success: function(data){
						response(data);				
					}
			});
		},
		minLength: 1
		});
    });


</script>

@stop