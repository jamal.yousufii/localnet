@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('main_page')!!}</title>
    <style type="text/css">
        table thead tr th
        {
            text-align: center;
            border-color: #000; 
            text-align: center !important;
        }
        table tbody tr td
        {
            border-color: #000;
            text-align: center !important;
        }
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop


@section('content')

    @if(Session::has('success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('success')!!}
		</div>
	@elseif(Session::has('caseDep_success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('caseDep_success')!!}
		</div>

    @elseif(Session::has('fail'))
    	<div class="alert alert-danger alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('fail')!!}
		</div>
    @endif
    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="pull-left">
                    <h3>{!!_('case_management_main_page')!!}</h3>
                </div>
            </div>
            <hr />
        </div>
        <div class="row">
        	<div class="col-lg-12">
        		<div class="container-fluid">
			        <div style="padding:15px" class="table-responsive col-sm-8">
			        <h4>{!!_('case_management_log')!!}</h4>	
			            <table class="table table-bordered table-responsive" id="logsList">
			                <thead>
			                  <tr>
			                    <th>{!!_('no#')!!}</th>
			                    <th>{!!_('action_by')!!}</th>
			                    <th>{!!_('action_type')!!}</th>
			                    <th>{!!_('action_date')!!}</th>
			                    
			                    <th>{!!_('operation')!!}</th> 
			                    
			                  </tr>
			                </thead>
			
			                <tbody>
			                </tbody>
			            </table>
			
			        </div>
			        <div style="padding:15px" class="col-sm-4" style="margin-left:20px">
			        <h4 style="padding: 20px">{!!_('case_notifications')!!}</h4>
			           	@if(isManager() || isAdmin())
							@foreach($notification as $notify)
								
								@if($notify->table_name == "cases")
								<div class="alert alert-success"><a href='{!!URL::route("viewCase",array($notify->case_id,"visited"))!!}'>Task created</a></div>
								@else
								<div class="alert alert-success"><a href='{!!URL::route("viewCase",array($notify->case_id,"visited"))!!}'>Task progress finished</a></div>
								@endif
								
							@endforeach
						@endif
			        </div>
				</div>
	        </div>
		</div>
 
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#logsList').dataTable(
        {

            "sDom": 'lfr<"clearfix">tip',
            "bProcessing": true,
            "bServerSide": true,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('logsListData')!!}"

        }
    );
});
</script>

@stop


