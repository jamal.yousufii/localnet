@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('notification_form')!!}</title>
    <style type="text/css">
        
    </style>
    {!! HTML::style('/css/autocomplete/jquery-ui.css') !!}

@stop

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
        </div>

    @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
        </div>
    @endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <h3 id="form_title">{!!_('notification_form')!!}</h3>
            </div>
        </div>
        <hr style="border: 1px dashed #b6b6b6" />
    </div>

    <div class="container">

        <form role="form" method="post" action="{!! URL::route('postAddCaseNotification') !!}" class="form-horizontal">
			
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('subject')!!} : </label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" name="subject" value="{!!old('subject')!!}" placeholder="Notification Subject" required/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('description')!!} : </label>
                <div class="col-sm-4">
                    <textarea cols="30" rows="4" class="form-control" name="description"></textarea>
                </div>
                <label class="col-sm-2 control-label">{!!_('urgency')!!} : </label>
                <div class="col-sm-4">
                    <select class="form-control" name="urgency">
                    	<option value="">Select Urgency Type</option>
                    	<option value="1">Normal</option>
                    	<option value="2">Urgent</option>
                    </select>
                </div>
            </div>
            {!! Form::token() !!}
            <div class="form-group">
                <div class="col-sm-12">
                    
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-check"></i>
                        </span>
                        &nbsp;{!!_('save')!!}
                    </button>
                </div>
            </div>
            <hr style="border: 1px dashed #b6b6b6" />
            
        </form>
    
    </div>

</div>

@stop

@section('footer-scripts')
	{!! HTML::script('/js/autocomplete/jquery-ui.js')!!}

<script type="text/javascript">

$( "#requesting_person" ).autocomplete({

	source: function(request, response) {
		
		$.ajax({ 
			url:"{!!URL::route('getAutocompleteList', array('table' => 'person', 'field' => 'name'))!!}",
			data: { term: $("#requesting_person").val()},
			dataType: "json",
			type: "POST",
			success: function(data){
				response(data);				
			}
	});
},
minLength: 1
});
$( "#department" ).autocomplete({

	source: function(request, response) {
		
		$.ajax({ 
			url:"{!!URL::route('getAutocompleteList', array('table' => 'department', 'field' => 'name'))!!}",
			data: { term: $("#department").val()},
			dataType: "json",
			type: "POST",
			success: function(data){
				response(data);				
			}
	});
},
minLength: 1
});
$( "#person_name" ).autocomplete({

	source: function(request, response) {
		
		$.ajax({ 
			url:"{!!URL::route('getAutocompleteList', array('table' => 'person', 'field' => 'name'))!!}",
			data: { term: $("#person_name").val()},
			dataType: "json",
			type: "POST",
			success: function(data){
				response(data);				
			}
	});
},
minLength: 1
});

    $(function(){

        // onchange of the person name field bring it's related department and change the value of the department.
        $("#requesting_person").change(function(){
            var name = $(this).val();
            $.ajax({
                url : "{!!URL::route('getRelatedCaseDepartment')!!}",
                type : 'POST',
                data : "name="+name,
                success : function(response){
                    $("#department").val(response);
                }
            })
        });

        $("#requrring").click(function(){
            
            if($("#requrring").prop("checked")){
                $("#requrring_options_div").fadeIn("slow");
            }
            else{
                $("#requrring_options_div").fadeOut("slow");   
                $(".requrring_options").prop('checked', false);             
            }

        });

        $("#next_page").click(function(){
            $("#first_page").hide("slow");
            $("#second_page").fadeIn("fast");
            $("#form_title").html("{!!_('task_description')!!}");
            return false;
        });

        $("#back").click(function(){
            $("#first_page").fadeIn("slow");
            $("#second_page").hide("fast");
            $("#form_title").html("{!!_('task_form')!!}");
            return false;
        });
        $(".in_house").click(function(){
            if($(".in_house").prop("checked")){
                $("#in_house").fadeIn("slow");
            }
            else{
                $("#in_house").fadeOut("slow");   
            }
            $("#agency").hide("slow");
            $("#external").hide("slow");
        });
        $(".agency").click(function(){
            if($(".agency").prop("checked")){
                $("#agency").fadeIn("slow");
            }
            else{
                $("#agency").fadeOut("slow");   
            }
            $("#in_house").hide("slow");
            $("#external").hide("slow");
        });
        $(".external").click(function(){
            if($(".external").prop("checked")){
                $("#external").fadeIn("slow");
                $("#person_name").prop('required',true);
                $("#related_department").prop('required',true);
            }
            else{
                $("#external").fadeOut("slow");   
                $("#person_name").prop('required',false);
                $("#related_department").prop('required',false);
            }
            $("#agency").hide("slow");
            $("#in_house").hide("slow");
        });

    });

</script>


@stop