@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('task_list')!!}</title>
    <style type="text/css">
        table thead tr th
        {
            text-align: center;
            border-color: #000; 
            text-align: center !important;
        }
        table tbody tr td
        {
            border-color: #000;
            text-align: center !important;
        }
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop


@section('content')

    @if(Session::has('success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('success')!!}
		</div>
	@elseif(Session::has('caseDep_success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('caseDep_success')!!}
		</div>

    @elseif(Session::has('fail'))
    	<div class="alert alert-danger alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('fail')!!}
		</div>
    @endif
    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="pull-left">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{!! URL::route('home') !!}">{!!_('Dashboard')!!}</a>
                        </li>
                        <li class="active">
                            <span>{!!_('all_tasks')!!}</span>
                        </li>
                    </ol>
                    <h1>{!!_('task_list')!!}</h1>
                </div>
            </div>
        </div>
        <div style="padding:15px" class="table-responsive">
            <table class="table table-bordered table-responsive" id="taskList">
                <thead>
                  <tr>
                    <th>{!!_('no#')!!}</th>
                    <th>{!!_('task_title')!!}</th>
                    <th>{!!_('priority')!!}</th>
                    <th>{!!_('name')!!}</th>
                    <th>{!!_('department')!!}</th>
                    <th>{!!_('agency')!!}</th>
                    <th>{!!_('request_level')!!}</th>
                    <th>{!!_('deadline')!!}</th>
                    <!--<th>{!!_('alarm')!!}</th>-->
                    <th>{!!_('category')!!}</th>
                    <th>{!!_('status')!!}</th>
                   	<!--<th>{!!_('assigned_to')!!}</th>-->

                    <th>{!!_('operations')!!}</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>

        </div>
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#taskList').dataTable(
        {

            "sDom": 'lfr<"clearfix">tip',
            "bProcessing": true,
            "bServerSide": true,
            "bDeferRender": true,
            "iDisplayLength": 10,
	        "aaSorting": [[ 0, "desc" ]],
            "sAjaxSource": "{!!URL::route('caseListData')!!}"

        }
    );
});
</script>

@stop


