@extends('layouts.master')

@section('head')
    @parent
    <title>{!!_('department_management_form')!!}</title>
    <style type="text/css">
        table th, table td{
            text-align: center !important;
        }
    </style>
	{!! HTML::style('/css/autocomplete/jquery-ui.css') !!}
@stop

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
        </div>

    @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
        </div>
    @endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <h3>{!!_('management_form')!!}</h3>
            </div> 
        </div>
        <hr style="border: 1px dashed #b6b6b6" />
    </div>

    <div class="container">
		<div class="row">
        	<div class="col-lg-12">
        		<div class="row">
	        		<div class="container-fluid">
				        <form role="form" method="post" action="{!! URL::route('postAddCaseDept') !!}" class="form-horizontal">
			                <div class="col-sm-4">
			                	<!--<label class="col-sm-12">{!!_('add_the_department_and_person_name')!!}</lable>-->
			                    <input class="form-control" type="text" id="department" name="department" placeholder="Write the Department">
			                	
			                </div>
			                <div class="col-sm-8">
			                	<div class="form-group">
				                	<!--<label class="col-sm-12">{!!_('add_the_person_name')!!}</lable>-->
				                	<div class="col-sm-6">
				                    	<input class="form-control" type="text" id="person" name="name" placeholder="Write the person name">
				                    </div>
				                    <div class="col-sm-6">
				                    	<select name="related_department" class="form-control">
				                    		{!!getTaskDepartments();!!}
				                    	</select>
				            		</div>
				            		<div class="form-group">
					            		@if($errors->has("name"))
					                    <div class="col-sm-6" style="padding:10px">
					                        <span style="color: red">{!! $errors->first('name') !!}</span>
					                    </div>
					                    @endif
					                    @if($errors->has("related_department"))
					                    <div class="col-sm-6" style="padding:10px">
					                        <span style="color: red">{!! $errors->first('related_department') !!}</span>
					                    </div>
					                    @endif
					            	</div>
				            	</div>
			                </div>
				    </div>
					    <div class="row">
					    	<div class="container-fluid">
			        			 <div class="col-sm-1">
					                <label class="col-sm-12">&nbsp;</lable>
					                <button class="btn btn-primary" type="submit">
				                        <span>
				                            <i class="fa fa-check"></i>
				                        </span>
				                        &nbsp;{!!_('save')!!}
				                    </button>
					             </div>
			        		</div>
		        		</form>
				    </div>
				</div>
			</div>
		</div>
		<hr style="border: 1px solid #eee" />
        <div class="row">
        	<div class="col-lg-12">
        		<div class="container-fluid">
			        <div style="padding:15px" class="table-responsive col-sm-4" style="border-right: 1px solid #eee">
			        	<h4 style="padding: 20px">{!!_('department_list')!!}</h4>
			            <table class="table table-bordered table-responsive" id="departmentList">
			                <thead>
			                  <tr>
			                    <th>{!!_('no#')!!}</th>
			                    <th>{!!_('name')!!}</th>
			
			                    <th>{!!_('operations')!!}</th>
			                    
			                  </tr>
			                </thead>
			
			                <tbody>
			                </tbody>
			            </table>
			
			        </div>
			        <div style="padding:15px" class="table-responsive col-sm-8">
			        <h4 style="padding: 20px">{!!_('person_list')!!}</h4>
			            <table class="table table-bordered table-responsive" id="personList">
			                <thead>
			                  <tr>
			                    <th>{!!_('no#')!!}</th>
			                    <th>{!!_('name')!!}</th>
			                    <th>{!!_('related_department')!!}</th>

			                    <th>{!!_('operations')!!}</th>
			                    
			                  </tr>
			                </thead>
			
			                <tbody>
			                </tbody>
			            </table>
			
			        </div>
				</div>
	        </div>
		</div>
    
    </div>

</div>

@stop

@section('footer-scripts')
{!! HTML::script('/js/autocomplete/jquery-ui.js')!!}
<script type="text/javascript">

    $(function(){     
		
		$('#departmentList').dataTable(
        {

            "sDom": 'lfr<"clearfix">tip',
            "bProcessing": true,
            "bServerSide": true,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getCaseDeps')!!}"

        }
    	);
    	
    	$( "#department" ).autocomplete({

			source: function(request, response) {
				
				$.ajax({ 
					url:"{!!URL::route('getAutocompleteList', array('table' => 'department', 'field' => 'name'))!!}",
					data: { term: $("#department").val()},
					dataType: "json",
					type: "POST",
					success: function(data){
						response(data);				
					}
			});
		},
		minLength: 1
		});
		
		$( "#person" ).autocomplete({

			source: function(request, response) {
				
				$.ajax({ 
					url:"{!!URL::route('getAutocompleteList', array('table' => 'person', 'field' => 'name'))!!}",
					data: { term: $("#person").val()},
					dataType: "json",
					type: "POST",
					success: function(data){
						response(data);				
					}
			});
		},
		minLength: 1
		});
    	
    	$( "#related_department" ).autocomplete({

			source: function(request, response) {
				
				$.ajax({ 
					url:"{!!URL::route('getAutocompleteList', array('table' => 'department', 'field' => 'name'))!!}",
					data: { term: $("#related_department").val()},
					dataType: "json",
					type: "POST",
					success: function(data){
						response(data);				
					}
			});
		},
		minLength: 1
		});
    	
    	$('#personList').dataTable(
        {

            "sDom": 'lfr<"clearfix">tip',
            "bProcessing": true,
            "bServerSide": true,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getCasePersons')!!}"

        }
    	);

    });


</script>

@stop