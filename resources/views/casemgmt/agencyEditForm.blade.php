@extends('layouts.master')

@section('head')
    @parent
    <title>{!!_('agency_edit_form')!!}</title>
    <style type="text/css">
        table td, table th{
            text-align: center !important;
        }
    </style>

@stop

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
        </div>

    @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
        </div>
    @endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <h3>{!!_('agency_management_edit_form')!!}</h3>
            </div>
        </div>
        <hr style="border: 1px dashed #b6b6b6" />
    </div>

    <div class="container">
	@foreach($records as $item)
        <form role="form" method="post" action="{!! URL::route('postEditCaseAgency', array('id' => $item->id)) !!}" class="form-horizontal">
            
            <h4 style="padding: 20px">{!!_('edit_agency_details')!!}</h4>
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('agency_name_en')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="name_en" value="{!!$item->name_en!!}">
                </div>
                <label class="col-sm-2 control-label">{!!_('agency_name_dr')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="name_dr" value="{!!$item->name_dr!!}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('head_in_english')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="head_en" value="{!!$item->head_en!!}" />
                </div>
                <label class="col-sm-2 control-label">{!!_('head_in_dari')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="head_dr" value="{!!$item->head_dr!!}" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('contact1')!!} : </label>
                <div class="col-sm-2">
                    <input class="form-control" type="number" name="contact1" value="{!!$item->contact1!!}">
                </div>
                <label class="col-sm-2 control-label">{!!_('contact2')!!} : </label>
                <div class="col-sm-2">
                    <input class="form-control" type="number" name="contact2" value="{!!$item->contact2!!}">
                </div>
                <label class="col-sm-2 control-label">{!!_('contact3')!!} : </label>
                <div class="col-sm-2">
                    <input class="form-control" type="number" name="contact3" value="{!!$item->contact3!!}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-check"></i>
                        </span>
                        &nbsp;{!!_('save_changes')!!}
                    </button>
                </div>
            </div>
            <hr style="border: 1px dashed #b6b6b6" />

        </form>
    @endforeach
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     



    });


</script>

@stop