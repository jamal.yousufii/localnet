@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('notification_list')!!}</title>
    <style type="text/css">
        table thead tr th
        {
            text-align: center;
            border-color: #000; 
            text-align: center !important;
        }
        table tbody tr td
        {
            border-color: #000;
            text-align: center !important;
        }
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop


@section('content')

    @if(Session::has('success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('success')!!}
		</div>
	@elseif(Session::has('caseDep_success'))
    	<div class="alert alert-success alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('caseDep_success')!!}
		</div>

    @elseif(Session::has('fail'))
    	<div class="alert alert-danger alert-dismissible" role="alert">
			<button class="close" aria-label="Close" data-dismiss="alert" type="button">
				<span aria-hidden="true">×</span>
			</button>
			{!!Session::get('fail')!!}
		</div>
    @endif
    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="pull-left">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{!! URL::route('home') !!}">{!!_('Dashboard')!!}</a>
                        </li>
                        <li class="active">
                            <span>{!!_('all_notifications')!!}</span>
                        </li>
                    </ol>
                    <h1>{!!_('notification_list')!!}</h1>
                </div>
            </div>
        </div>
        <div style="padding:15px" class="table-responsive">
            <table class="table table-bordered table-responsive" id="taskList">
                <thead>
                  <tr>
                    <th>{!!_('no#')!!}</th>
                    <th>{!!_('type')!!}</th>
                    <th>{!!_('subject')!!}</th>
                    <th>{!!_('description')!!}</th>
                    <th>{!!_('status')!!}</th>
                    <th>{!!_('approval')!!}</th>
                    <th>{!!_('viewed')!!}</th>
                    <th>{!!_('date')!!}</th>
                    
                    <th>{!!_('operation')!!}</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>

        </div>
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#taskList').dataTable(
        {

            "sDom": 'lfr<"clearfix">tip',
            "bProcessing": true,
            "bServerSide": true,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('notificationListData')!!}"

        }
    );
});
</script>

@stop


