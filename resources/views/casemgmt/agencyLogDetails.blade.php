@extends('layouts.master')

@section('head')
    @parent
    <title>{!!_('agency_management_form')!!}</title>
    <style type="text/css">
        table td, table th{
            text-align: center !important;
        }
    </style>

@stop

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
        </div>

    @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
        </div>
    @endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12" style="margin-bottom:10px">
        <div id="content-header" class="clearfix pull-left">
            <div class="pull-left">
                <h3>{!!_('agency_detailed_information')!!}</h3>
            </div>
        </div>
        <div class="pull-right">
			<a href="javascript:history.back()" class="btn btn-warning">
	            <i class="icon wb-arrow-left"></i>
	            &nbsp;
	        	{!!_('go_back')!!}
	        </a>
        </div>
    </div>
    <hr style="border: 1px dashed #b6b6b6" />

    <div class="container">
        <form role="form" method="post" action="{!! URL::route('postAddAgency') !!}" class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('agency_name_en')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="name_en" value="{!!$agency->name_en!!}">
                </div>
                <label class="col-sm-2 control-label">{!!_('agency_name_dr')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="name_dr" value="{!!$agency->name_dr!!}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('head_in_english')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="head_en" value="{!!$agency->head_en!!}" />
                </div>
                <label class="col-sm-2 control-label">{!!_('head_in_dari')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="head_dr" value="{!!$agency->head_dr!!}" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('contact1')!!} : </label>
                <div class="col-sm-2">
                    <input class="form-control" type="number" name="contact1" value="{!!$agency->contact1!!}">
                </div>
                <label class="col-sm-2 control-label">{!!_('contact2')!!} : </label>
                <div class="col-sm-2">
                    <input class="form-control" type="number" name="contact2" value="{!!$agency->contact2!!}">
                </div>
                <label class="col-sm-2 control-label">{!!_('contact3')!!} : </label>
                <div class="col-sm-2">
                    <input class="form-control" type="number" name="contact3" value="{!!$agency->contact3!!}">
                </div>
            </div>
        </form>
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     

    });


</script>

@stop