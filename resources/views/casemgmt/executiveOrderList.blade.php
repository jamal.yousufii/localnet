@extends('layouts.content')

@section('head')
    @parent
    <title>{!!_('executive_order_list')!!}</title>
    <style type="text/css">
        table th, table td{
            text-align: left !important;
        }
        table td:first-child, table td:nth-child(2), table td:last-child{
        	text-align: center !important;
        }
        table th:nth-child(3){
        	width: 30% !important;
        }
        table th:nth-child(5){
        	width: 25% !important;
        }
        #execOrderList tbody tr:hover{
        	background:#eee;
        }
        #execOrderList td{
        	padding: 4px !important;
        	vertical-align:middle;
        }
        
    </style>
@stop

@section('contents')

<div class="col-sm-12" style="margin-top: -60px">
	
    <table class="table table-bordered table-responsive" id="execOrderList">
        <thead>
          <tr>
          
            <th>{!!_('no#')!!}</th>
            <th>{!!_('date')!!}</th>
            <th>{!!_('title')!!}</th>
            <th>{!!_('status')!!}</th>
            <th>{!!_('agency')!!}</th>
            <th>{!!_('deadline')!!}</th>
            <th>{!!_('open')!!}</th>

          </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
</div>

@stop

@section('footer-scripts')
{!!HTML::script("js/jquery_countdown/jquery.countdown.js")!!}

<script type="text/javascript">

    $(function(){   
    	$('#execOrderList').dataTable(
	     	{
	            "sDom": 'lfr<"clearfix">tip',
	            "bProcessing": true,
	            "bServerSide": true,
	            "bDeferRender": true,
	            "iDisplayLength": 10,
	            "aaSorting": [[ 0, "desc" ]],
	            "sAjaxSource": '{!!URL::route("getExecOrderData")!!}'
	        }
	    );
	    
    });
    
 //   $('#execOrderList').on( 'draw.dt', function () {
	//     $('#execOrderList tr').click(function() {
	//         var href = $(this).find(".cases_id").val();
	//         if(href) {
	//             window.location = href;
	//         }
	//     });
	// } );




</script>

@stop