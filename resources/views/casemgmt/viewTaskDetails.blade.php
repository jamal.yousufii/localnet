@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('task_details')!!}</title>
    <style type="text/css">
        table th{
        	text-align: center !important;
        }
    </style>

@stop

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
        </div>

    @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
        </div>
    @endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <h3 id="form_title">{!!_('task_details')!!}</h3>
            </div>
            <div class="pull-right">
            	@if(isManager() || isAdmin() && $task_progress != "" || !$approved_finished_task == null)
		            @if($task_progress->status == 3 && $approved_finished_task == 0)
		            <button class="btn btn-success" id="approveFinishedTask">
		                <span>
		                    <i class="fa fa-check"></i>
		                </span>
		                &nbsp;{!!_('approve_finished_task')!!}
		            </button>
		            @endif
		    	@endif
		    	&nbsp;
				<a href="{!!URL::route('caseList')!!}" class="btn btn-warning">
		            <i class="icon wb-arrow-left"></i>
		            &nbsp;
		        	{!!_('go_back')!!}
		        </a>
		    </div>
        </div>
        <hr style="border: 1px dashed #b6b6b6" />
    </div>
    
    <div class="container">
    <?php
    	$case_id = 0;
    ?>
	@foreach($task_details as $item)
		
		<?php
			$case_id = $item->id;
			if(isShamsiDate()){
				$start_date = checkEmptyDate($item->start_date);
				$deadline = checkEmptyDate($item->deadline);
			}
			else{
				$start_date = checkGregorianEmtpyDate($item->start_date);
				$deadline = checkGregorianEmtpyDate($item->deadline);
			}

		?>
		<form class="form-horizontal">
	        <div class="form-group">
	            <label class="col-sm-2 control-label">{!!_('task_title')!!} </label>
	            <div class="col-sm-3">
	                <input class="form-control" type="text" name="task_title" value="{!!$item->task_title!!}" readonly="" />
	            </div>
	            <label class="col-sm-2 control-label">{!!_('task_description')!!} </label>
	            <div class="col-sm-5">
	                <textarea class="form-control" name="description" readonly="">{!!$item->task_description_en!!}</textarea>
	            </div>
	        </div>
			<div class="form-group">
	            <label class="col-sm-2 control-label">{!!_('requesting_person')!!}</label>
	            <div class="col-sm-3">
	                <input type="text" class="form-control" value="{!!$item->requesting_person!!}" readonly="" />
	            </div>
	            <label class="col-sm-1 control-label">{!!_('department')!!}</label>
	            <div class="col-sm-3">
	                <input type="text" class="form-control" value="{!!$item->department!!}" readonly="" />
	            </div>
	            <label class="col-sm-1 control-label">{!!_('start_date')!!}</label>
	            <div class="col-sm-2">
	                <input type="text" class="form-control" name="start_date" value="{!!$start_date!!}" readonly="" />
	            </div>
	        </div>
			<div class="form-group">
	            <label class="col-sm-2 control-label">{!!_('task_request_level')!!}</label>
	            <div class="col-sm-2">
	                <input type="text" class="form-control" value="{!!$item->task_request_level!!}" readonly="" />
	            </div>
	            <label class="col-sm-2 control-label">{!!_('deadline')!!}</label>
	            <div class="col-sm-2">
	                <input class="form-control" name="deadline" type="text" value="{!!$deadline!!}" readonly="" />
	            </div>
	            <label class="col-sm-2 control-label">{!!_('deadline_option')!!}</label>
	            <div class="col-sm-2">
	                <input type="text" class="form-control" value="{!!$item->deadline_option!!}" name="deadline_option" readonly="" />
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="col-sm-2 control-label">{!!_('task_requrring')!!}</label>
	            <div class="col-sm-2">
	                <input type="text" class="form-control" value="{!!$item->recurring_type!!}" readonly="" />
	            </div>
	            <label class="col-sm-2 control-label">{!!_('alarm')!!}</label>
	            <div class="col-sm-2">
	                <input type="text" class="form-control" value="{!!$item->alarm!!}" readonly="" />
	            </div>
	            <label class="col-sm-2 control-label">{!!_('task_category')!!}</label>
	            <div class="col-sm-2">
	                <input type="text" class="form-control" value="{!!$item->task_category!!}" readonly="" />
	            </div>
	        </div>
	        <div class="form-group">
	            @if($task_assigned_to != "")
	            <label class="col-sm-2 control-label">{!!_('task_assigned_to')!!}</label>
	            <div class="col-sm-4">
	                <input class="form-control" type="text" value="{!!$task_assigned_to!!}" readonly="" />
	            </div>
	        	@endif
	        	@if($name != "" && $department != "")
	        	<label class="col-sm-2 control-label">{!!_('task_assigned_to')!!}</label>
	        	<div class="col-sm-4">
	                <input class="form-control" type="text" value="{!!$name!!}" readonly="" />
	            </div>
	            <div class="col-sm-4">
	                <input class="form-control" type="text" value="{!!$department!!}" readonly="" />
	            </div>
	            @endif
	        </div>
	    </form>
    <hr style="border: 1px dashed #b6b6b6" />
    <form role="form" method="post" action="{!! URL::route('postUpdateTaskProgress', array('id' => $item->id)) !!}" class="form-horizontal" enctype="multipart/form-data">
    	<input class="form-control" type="hidden" name="task_title" value="{!!$item->task_title!!}" />
	    <div class="col-lg-12">
	        <div id="content-header" class="clearfix">
	            <div class="pull-left">
	                <h4>{!!_('task_progress_list')!!}</h4>
	            </div>
	        </div>
	    </div>
	    <div class="form-group">
	    	<div class="col-sm-12" style="padding: 0px 35px 0px 35px">
			    <table class="table table-bordered table-responsive" id="progressList">
		            <thead>
		              <tr>
		                <th>{!!_('no#')!!}</th>
		                <th>{!!_('date')!!}</th>
		                <th>{!!_('progress_level')!!}</th>
		                <th>{!!_('progress_description')!!}</th>
		                <th>{!!_('task_status')!!}</th>
		                <th>{!!_('user')!!}</th>
		                <th>{!!_('progress_attachments')!!}</th>
		                
		              </tr>
		            </thead>
		
		            <tbody>
		            </tbody>
		        </table>
	        </div>
	    </div>
	    <div class="form-group main-box-body clearfix">
            <ul class="widget-todo" style="margin-top:10px;list-style:none">
                @if(count($task_attachments)>0)
                    @foreach($task_attachments AS $attach)
						<li class="clearfix" id="li_{!!$attach->id!!}">
	                        <div class="name">
	                        	@if($attach->audio_name != null && $attach->video_name != null)
	                        		<h4>Task Audio and Video Files</h4>
		                        	<label for="todo-2">
		                                <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
		                                <strong>{!!$attach->audio_name!!}</strong><br />
		                            </label>
								  	&nbsp;&nbsp;
								  	<a href="{!!URL::route('getCaseDownload',array($attach->id,'audio_name'))!!}" class="table-link success">
					              		<i class="fa fa-cloud-download" style='color:#03a9f4;' title="download file"></i>
					              	</a>
		                        	<br />
		                        	<label for="todo-2">
		                                <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
		                                <strong>{!!$attach->video_name!!}</strong><br />
		                            </label>
								  	&nbsp;&nbsp;
								  	<a href="{!!URL::route('getCaseDownload',array($attach->id,'video_name'))!!}" class="table-link success">
					              		<i class="fa fa-cloud-download" style='color:#03a9f4;' title="download file"></i>
					              	</a>
					              	<div class="form-group">
					              		<div class="col-sm-4">
					              			<?php
						              			if($attach->audio_name != ""){
						              				$audioPath="/documents/case_attachments/".$attach->audio_name;	
						              			}
				                        	 ?>
					              			<a class="media {width:320, height:240}" href="<?= $audioPath ?>"></a>
										</div>
										<div class="col-sm-5">
											<?php
												if($attach->video_name != ""){
													$videoPath="/documents/case_attachments/".$attach->video_name;
												}
				                        	 ?>
					              			<a class="media {width:320, height:240}" href="<?= $videoPath ?>"></a>
										</div>
									</div>
	                        	@else
	                                @if($attach->audio_name != null)
	                                <?php $field_name = "audio_name"?>
	                                <h4>Task Audio File</h4>
	                                <label for="todo-2">
	                                <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
	                                </label>
	                                <strong>{!!$attach->audio_name!!}</strong>
	                                &nbsp;&nbsp;
								  	<a href="{!!URL::route('getCaseDownload',array($attach->id,'audio_name'))!!}" class="table-link success">
					              		<i class="fa fa-cloud-download" style='color:#03a9f4;' title="download file"></i>
					              	</a>
					              	<div class="form-group">
										<a class="media {width:320, height:240}" href="<?= @$audioPath ?>"></a> 
									</div>
	                                @elseif($attach->video_name != null)
	                                <?php $field_name = "video_name"?>
	                                <h4>Task Video File</h4>
	                                <label for="todo-2">
	                                <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
	                                </label>
	                                <strong>{!!$attach->video_name!!}</strong>
	                                &nbsp;&nbsp;
								  	<a href="{!!URL::route('getCaseDownload',array($attach->id,'video_name'))!!}" class="table-link success">
					              		<i class="fa fa-cloud-download" style='color:#03a9f4;' title="download file"></i>
					              	</a>
					              	<div class="form-group">
					              		<?php
											if($attach->video_name != ""){
												$videoPath="/documents/case_attachments/".$attach->video_name;
											}
			                        	?>
										<a class="media {width:320, height:240}" href="<?= @$videoPath ?>"></a> 
									</div>
	                                @endif
							  	
	                        	@endif
	                        </div>                                    
						</li>
                    @endforeach
                @else
                    <li><span style='color:red;'>{!!_('no_uploaded_document_!')!!}</span></li>
                @endif
            </ul>
                
        </div>
	    <div class="col-lg-12">
	        <div id="content-header" class="clearfix">
	            <div class="pull-left">
	                <h4>{!!_('change_progress_status')!!}</h4>
	            </div>
	        </div>
	        <hr />
	    </div>
        @if(!$task_progress == null)
	        <div class="form-group">
	        	<div class="col-sm-2">
	            	<label class="control-label">{!!_('task_progress_level')!!} : </label>
	            	<label class="control-label">{!!_('progress_description')!!} : </label>
	           	</div>
	            <div class="col-sm-4">
	                <div class="radio-custom radio-primary">
	                	<input id="progress_level" type="radio" value="25 %" onclick="changeStatus(this)" name="progress_level" <?php if($task_progress->progress_level == "25 %") echo "checked='checked'" ?>>
	                    <label for="progress_level" style="margin-right:20px">{!!_('25_%')!!}</label>
	                    <input id="progress_level" type="radio" value="50 %" onclick="changeStatus(this)" name="progress_level" <?php if($task_progress->progress_level == "50 %") echo "checked='checked'" ?>>
	                    <label for="progress_level" style="margin-right:20px">{!!_('50_%')!!}</label>
	                    <input id="progress_level" type="radio" value="75 %" onclick="changeStatus(this)" name="progress_level" <?php if($task_progress->progress_level == "75 %") echo "checked='checked'" ?>>
	                    <label for="progress_level" style="margin-right:20px">{!!_('75_%')!!}</label>
	                    <input id="progress_level" class="progress_percentage" onclick="changeStatus(this)" type="radio" value="100 %" name="progress_level" <?php if($task_progress->progress_level == "100 %") echo "checked='checked'" ?>>
	                    <label for="progress_level">{!!_('100_%')!!}</label>
	                </div>
		            <div class="col-sm-12" style="margin-top: 20px">
		            	<textarea cols="40" rows="3" name="progress_description" class="form-control"></textarea>
		            </div>
	            </div>
	            <label class="col-sm-2 control-label">{!!_('task_status')!!} : </label>
	            <div class="col-sm-4">
	            	<div class="radio-custom radio-primary">
	                <input id="status" type="radio" value="0" name="status" <?php if($task_progress->status == 0) echo "checked='checked'" ?>>
	                <label for="status" style="margin-right:20px">{!!_('emergency')!!}</label>
	                </div>
	                <div class="radio-custom radio-primary">
	                <input id="status" type="radio" value="1" class="not_started" name="status" <?php if($task_progress->status == 1) echo "checked='checked'" ?>>
	                <label for="status" style="margin-right:20px">{!!_('not_started')!!}</label>
	                </div>
	                <div class="radio-custom radio-primary">
	                <input id="status" type="radio" value="2" name="status" <?php if($task_progress->status == 2) echo "checked='checked'" ?>>
	                <label for="status" style="margin-right:20px">{!!_('started')!!}</label>
	                </div>
	                <div class="radio-custom radio-primary">
	                <input id="status" type="radio" class="finished" value="3" name="status" <?php if($task_progress->status == 3) echo "checked='checked'" ?>>
	                <label for="status" style="margin-right:20px">{!!_('finished')!!}</label>
	                </div>
	                <div class="radio-custom radio-primary">
	                <input id="status" type="radio" value="4" class="feedback" name="status" <?php if($task_progress->status == 4) echo "checked='checked'" ?>>
	                <label for="status">{!!_('feed_back')!!}</label>
	                </div>
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="col-sm-2 control-label">{!!_('attachment')!!} : </label>
	            <div class="col-sm-4">
	            @if($task_progress->progress_level == "100 %" && $task_progress->status == "3")
	            	<input style='width:400px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple' disabled>
	           	@else
	           		<input style='width:400px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
	            @endif
	            </div>
	        </div>
	    @else
	    	<div class="form-group">
	    		<div class="col-sm-2">
	            	<label class="control-label">{!!_('task_progress_level')!!} : </label>
	            	<label class="control-label">{!!_('progress_description')!!} : </label>
	           	</div>
	            <div class="col-sm-4">
	                <div class="radio-custom radio-primary">
	                    <input id="progress_level" type="radio" value="25 %" onclick="changeStatus(this)" name="progress_level">
	                    <label for="progress_level" style="margin-right:20px">{!!_('25_%')!!}</label>
	                    <input id="progress_level" type="radio" value="50 %" onclick="changeStatus(this)" name="progress_level">
	                    <label for="progress_level" style="margin-right:20px">{!!_('50_%')!!}</label>
	                    <input id="progress_level" type="radio" value="75 %" onclick="changeStatus(this)" name="progress_level">
	                    <label for="progress_level" style="margin-right:20px">{!!_('75_%')!!}</label>
	                    <input id="progress_level" class="progress_percentage" onclick="changeStatus(this)" type="radio" value="100 %" name="progress_level">
	                    <label for="progress_level">{!!_('100_%')!!}</label>
	                </div>
		            <div class="col-sm-12" style="margin-top:20px">
		            	<textarea cols="40" rows="3" name="progress_description" class="form-control"></textarea>
		            </div>
	            </div>
	            <label class="col-sm-2 control-label">{!!_('task_status')!!} : </label>
	            <div class="col-sm-4">
	            	<div class="radio-custom radio-primary">
	                <input id="status" type="radio" value="0" name="status">
	                <label for="status" style="margin-right:20px">{!!_('emergency')!!}</label>
	                </div>
	                <div class="radio-custom radio-primary">
	                <input id="status" type="radio" class="not_started" value="1" name="status" checked="checked">
	                <label for="status" style="margin-right:20px">{!!_('not_started')!!}</label>
	                </div>
	                <div class="radio-custom radio-primary">
	                <input id="status" type="radio" value="2" name="status">
	                <label for="status" style="margin-right:20px">{!!_('started')!!}</label>
	                </div>
	                <div class="radio-custom radio-primary">
	                <input id="status" type="radio" class="finished" onclick="changeStatus()" value="3" name="status">
	                <label for="status" style="margin-right:20px">{!!_('finished')!!}</label>
	                </div>
	                <div class="radio-custom radio-primary">
	                <input id="status" type="radio" class="feedback" value="4" name="status">
	                <label for="status">{!!_('feed_back')!!}</label>
	                </div>
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="col-sm-2 control-label">{!!_('attachment')!!} : </label>
	            <div class="col-sm-4">
	            	<input style='width:400px;' type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
	            </div>
	        </div>
        @endif
        <div class="form-group">
        	<span id="approvedResult"></span>
            <div class="col-sm-12" style="margin-top:50px">
                <button class="btn btn-primary" type="submit">
                    <span>
                        <i class="fa fa-check"></i>
                    </span>
                    &nbsp;{!!_('save_progress')!!}
                </button>
            </div>
        </div>
    </form>
    @endforeach         
    </div>

</div>

@stop

@section('footer-scripts')
{!!HTML::script('/js/plugins/jquery_metadata.js')!!}
{!!HTML::script('/js/plugins/jquery_media_plugin.js')!!}
<script type="text/javascript">
	
    $(function(){   
    
    	$('a.media').media();
    	
    	$('#progressList').dataTable(
	     {
	            "sDom": 'lfr<"clearfix">tip',
	            "bProcessing": true,
	            "bServerSide": true,
	            "bDeferRender": true,
	            "iDisplayLength": 10,
	            "sAjaxSource": '{!!URL::route("progressList",array("id" => $case_id))!!}'
	        }
	    );
	    
		var selected = $('input[name=progress_level]:checked').val();
    	
    	if(selected == '100 %')
        {
            //$('.not_started').attr('checked', false);
            $('input[name=progress_level]').attr('disabled', true);
            $('input[name=status]').attr('disabled', true);
            $('.feedback').prop('disabled', false);
            $('input[type=file]').attr('disabled', true);
            $('.finished').prop("checked", true);
        }
        var selected = $('input[name=status]:checked').val();
   
    	if(selected == 3)
        {
            //$('.not_started').attr('checked', false);
            $('input[name=progress_level]').attr('disabled', true);
            $('input[name=status]').attr('disabled', true);
            $('.feedback').prop('disabled', false);
            $('input[type=file]').attr('disabled', true);
            $('.finished').prop("checked", true);
        }
        
    });
    
    //check, uncheck or disable radios based on desired options.
    function changeStatus(e)
    {
    	var selected = $('input[name=status]:checked').val();
   
    	if(selected == 3)
        {
            //$('.not_started').attr('checked', false);
            $('input[name=progress_level]').prop('readonly', true);
            $('input[name=status]').prop('readonly', true);
            $('.feedback').prop('readonly', false);
            $('input[type=file]').prop('readonly', true);
            $('.finished').prop("checked", true);
        }
    	var selected = $('input[name=progress_level]:checked').val();
    	
    	if(selected == '100 %')
        {
            //$('.not_started').attr('checked', false);
            $('input[name=progress_level]').prop('readonly', true);
            $('input[name=status]').prop('readonly', true);
            $('.feedback').prop('readonly', false);
            $('input[type=file]').prop('readonly', true);
            $('.finished').prop("checked", true);
        }
        
    }
    // Add Task Description revisions through ajax script.
    function addDescRevisions(divID)
    {
		
		var task_desc_en = $('#task_description_en').val();
		var task_desc_dr_ps = $('#task_description_dr_ps').val();
		var case_id = $("#case_id").val();
		var dataString = "case_id="+case_id+"&task_desc_en="+task_desc_en+"&task_desc_dr_ps="+task_desc_dr_ps;
		
		$.ajax({
			url: "{!!URL::route('addTaskDescRevisions')!!}",
			type: "POST",
			data: dataString,
			// beforeSend: function(){
   //         	$("#"+divID).html('<span style="align:center;">Processing ...</span>');
   //         },
            success: function(response)
            { 
                $('#'+divID).html(response);
            }
		})

    }
  	$('#approveFinishedTask').click(function(){
  		var case_id = "<?=$case_id?>";
  		var dataString = "case_id="+case_id;
  	
  		$.ajax({
			url: "{!!URL::route('getApproveFinishedTask')!!}",
			type: "POST",
			data: dataString,
			beforeSend: function(){
            	$('#approvedResult').html('<span style="align:center;">Processing ...</span>');
            },
            success: function(response)
            { 
                $('#approveFinishedTask').hide();
                $('#approvedResult').html(response);
            }
		})
  		return false;
  	});

</script>

@stop