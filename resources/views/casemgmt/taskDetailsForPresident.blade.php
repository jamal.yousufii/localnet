@extends('layouts.content')

@section('head')
	@parent
	<title>{!!_('task_details')!!}</title>
    <style type="text/css">
    	table td{
        	text-align: center !important;
        }
        table td:nth-child(4){
        	text-align: left !important;
        }
        table th{
        	text-align: center !important;
        }
    </style>

@stop

@section('contents')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
        </div>

    @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
        </div>
    @endif
<div style="margin-top:-30px">
    <div class="pull-right" style="margin: 0 135px 0 0px">
    	<a href="{!!URL::route('execOrderList')!!}" class="btn btn-success"> Go Back </a>
    </div>

    <div class="container">
    	<div class="col-lg-12">
	        <div id="content-header" class="clearfix">
	            <div class="pull-left">
	                <h4>{!!_('task_details')!!}</h4>
	            </div>
	        </div>
	        <hr style="1px solid #eee" />
	    </div>
    <?php
    	$case_id = 0;
    ?>
	@foreach($task_details as $item)
		
		<?php
			$case_id = $item->id;
			if(isShamsiDate()){
				
				$start_date = checkEmptyDate($item->start_date);
				$deadline = checkEmptyDate($item->deadline);
			}
			else{
				$start_date = checkGregorianEmtpyDate($item->start_date);
				$deadline = checkGregorianEmtpyDate($item->deadline);
			}

		?>
		<form class="form-horizontal">
	        <div class="form-group">
	            <label class="col-sm-2 control-label">{!!_('task_title')!!} : </label>
	            <div class="col-sm-10">
	                <input class="form-control" type="text" name="task_title" value="{!!$item->task_title!!}" readonly="" />
	            </div>
	        </div>
			<div class="form-group">
	            <label class="col-sm-2 control-label">{!!_('start_date')!!} : </label>
	            <div class="col-sm-4">
	                <input type="text" class="form-control" name="start_date" value="{!!$start_date!!}" readonly="" />
	            </div>
	            <label class="col-sm-2 control-label">{!!_('deadline')!!} : </label>
	            <div class="col-sm-4">
	                <input class="form-control" name="deadline" type="text" value="{!!$deadline!!}" readonly="" />
	            </div>
	        </div>
	        <div class="form-group">
	            @if($task_assigned_to != "")
	            <label class="col-sm-2 control-label">{!!_('task_assigned_to')!!} : </label>
	            <div class="col-sm-4">
	                <input class="form-control" type="text" value="{!!$task_assigned_to!!}" readonly="" />
	            </div>
	        	@endif
	        	@if($item->agency_manager != "")
	        	<label class="col-sm-2 control-label">{!!_('agency_manager')!!} : </label>
	        	<div class="col-sm-4">
	                <input class="form-control" type="text" value="{!!$item->agency_manager!!}" readonly="" />
	            </div>
	            @endif
	        </div>
	        <div class="form-group">
	        	<label class="col-sm-2 control-label">{!!_('description')!!} : </label>
	        	<div class="col-sm-6">
	                <textarea class="form-control" type="text" readonly>{!!$item->task_description_en!!}</textarea>
	            </div>
	        </div>
	    </form>
    <hr style="border: 1px dashed #b6b6b6" />
    <form role="form" method="post" action="{!! URL::route('postUpdateTaskProgress', array('id' => $item->id)) !!}" class="form-horizontal" enctype="multipart/form-data">
	    <div class="col-lg-12">
	        <div id="content-header" class="clearfix">
	            <div class="pull-left">
	                <h4>{!!_('task_progress_list')!!}</h4>
	            </div>
	        </div>
	    </div>
	    <div class="form-group">
	    	<div class="col-sm-12" style="padding: 0px 35px 0px 35px">
			    <table class="table table-bordered table-responsive" id="progressList">
		            <thead>
		              <tr>
		                <th>{!!_('no#')!!}</th>
		                <th>{!!_('date')!!}</th>
		                <th>{!!_('progress_level')!!}</th>
		                <th>{!!_('progress_description')!!}</th>
		                <th>{!!_('task_status')!!}</th>
		                <th>{!!_('user')!!}</th>
		                <th>{!!_('progress_attachments')!!}</th>
		              </tr>
		            </thead>
		
		            <tbody>
		            </tbody>
		        </table>
	        </div>
	    </div>
	    <div class="form-group main-box-body clearfix">
            <ul class="widget-todo" style="margin-top:10px;list-style:none">
                @if(count($task_attachments)>0)
                    @foreach($task_attachments AS $attach)
						<li class="clearfix" id="li_{!!$attach->id!!}">
	                        <div class="name">
	                        	@if($attach->audio_name != null && $attach->video_name != null)
	                        		<h4>Task Audio and Video Files</h4>
		                        	<label for="todo-2">
		                                <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
		                                <strong>{!!$attach->audio_name!!}</strong><br />
		                            </label>
								  	&nbsp;&nbsp;
								  	<a href="{!!URL::route('getCaseDownload',array($attach->id,'audio_name'))!!}" class="table-link success">
					              		<i class="fa fa-cloud-download" style='color:#03a9f4;' title="download file"></i>
					              	</a>
		                        	<br />
		                        	<label for="todo-2">
		                                <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
		                                <strong>{!!$attach->video_name!!}</strong><br />
		                            </label>
								  	&nbsp;&nbsp;
								  	<a href="{!!URL::route('getCaseDownload',array($attach->id,'video_name'))!!}" class="table-link success">
					              		<i class="fa fa-cloud-download" style='color:#03a9f4;' title="download file"></i>
					              	</a>
	                        	@else
	                                @if($attach->audio_name != null)
	                                <?php $field_name = "audio_name"?>
	                                <h4>Task Audio File</h4>
	                                <label for="todo-2">
	                                <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
	                                </label>
	                                <strong>{!!$attach->audio_name!!}</strong>
	                                &nbsp;&nbsp;
								  	<a href="{!!URL::route('getCaseDownload',array($attach->id,'audio_name'))!!}" class="table-link success">
					              		<i class="fa fa-cloud-download" style='color:#03a9f4;' title="download file"></i>
					              	</a>
	                                @elseif($attach->video_name != null)
	                                <?php $field_name = "video_name"?>
	                                <h4>Task Video File</h4>
	                                <label for="todo-2">
	                                <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
	                                </label>
	                                <strong>{!!$attach->video_name!!}</strong>
	                                &nbsp;&nbsp;
								  	<a href="{!!URL::route('getCaseDownload',array($attach->id,'video_name'))!!}" class="table-link success">
					              		<i class="fa fa-cloud-download" style='color:#03a9f4;' title="download file"></i>
					              	</a>
	                                @endif
							  	
	                        	@endif
	                        </div>                                    
						</li>
                    @endforeach
                @else
                    <li><span style='color:red;'>{!!_('no_uploaded_document_!')!!}</span></li>
                @endif
            </ul>
                
        </div>
	    
    </form>
    @endforeach         
    </div>

</div>

@stop

@section('footer-scripts')
{!!HTML::script('/js/plugins/jquery_metadata.js')!!}
{!!HTML::script('/js/plugins/jquery_media_plugin.js')!!}
<script type="text/javascript">

    $(function(){   
    	
    	$('a.media').media();
    
    	$('#progressList').dataTable(
	     {
	            "sDom": 'lfr<"clearfix">tip',
	            "bProcessing": true,
	            "bServerSide": true,
	            "bDeferRender": true,
	            "iDisplayLength": 10,
	            "sAjaxSource": '{!!URL::route("progressList",array("id" => $case_id))!!}'
	        }
	    );
	    //$("#progressList th:last-child").hide();
	    $("#progressList td:last-child").hide();
	    
    });

</script>

@stop