@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('executive_order_form')!!}</title>
    <style type="text/css">
        
    </style>

@stop

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
        </div>

    @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
        </div>
    @endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <h3 id="form_title">{!!_('task_executive_order_form')!!}</h3>
            </div>
        </div>
        <hr style="border: 1px dashed #b6b6b6" />
    </div>

    <div class="container">

        <form role="form" method="post" action="{!! URL::route('postAddExec') !!}" class="form-horizontal" enctype="multipart/form-data">
			
            <ul class="nav nav-tabs nav-tabs-line" id="tabs">
                <li class="active">
                    <a href="#audio_recording_tab" data-toggle="tab">Audio Entry</a>
                </li>
                <li>
                    <a href="#text_entry_tab" data-toggle="tab">Text Entry</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="audio_recording_tab" class="tab-pane active">
                    <div style="margin-top:20px">
                        <div class="form-group" style="display:none">
                            <div class="col-sm-3">
                                <select class="form-control">
                                    <option value="record-video">Video</option>
                                    <option value="record-audio">Audio</option>
                                    <option value="record-screen">Screen</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select class="form-control">
                                    <option>WebM</option>
                                    <option disabled>Mp4</option>
                                    <option disabled>WAV</option>
                                    <option disabled>Ogg</option>
                                    <option>Gif</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                        <br><br>
                            <div class="col-sm-2">
                                <button class="btn btn-success" type="button">Start Recording</button>
                            </div>
                            <div class="col-sm-3">
                                <audio controls muted>
                                  Your browser does not support the audio element.
                                </audio>  
                            </div>
                            <br><br>
                            
                        </div>
                    </div>
                        
                    <!-- <div class="form-group">
                        <label class="col-sm-2"></label>
                        <div class="col-sm-4">
                            <button class="btn btn-success span4">Record Audio</button>&nbsp;&nbsp;&nbsp;&nbsp;
                            <button class="btn btn-warning"><i class="fa fa-play"></i> &nbsp;Play</button>
                            <button class="btn btn-danger"><i class="fa fa-trash-o"></i> &nbsp;Delete</button>
                        </div>
                    </div> -->
                </div>
                <div id="text_entry_tab" style="padding-top:20px" class="tab-pane">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{!!_('description')!!} : </label>
                        <div class="col-sm-4">
                            <textarea cols="30" rows="4" class="form-control" name="description"></textarea>
                        </div>
                        <div id="agency_select_div">
	                        <label class="col-sm-2 control-label">{!!_('agencies')!!} : </label>
	                        <div class="col-sm-4">
	                            <select name="agency" id="agency" class="form-control">
	                                {!!getCaseAgencies('', getLangShortcut())!!}
	                                <option value="other">Other</option>
	                            </select>
	                        </div>
	                	</div>
	                	<div style="display:none" id="agency_custom_div">
	            			<label class="col-sm-2 control-label">{!!_('agency')!!} : </label>
		                    <div class="col-sm-4">
		                        <input type="text" class="form-control" id="custom_agency" name="agency" value="{!!old('agency')!!}" />
		                    </div>
	            		</div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">{!!_('priority')!!} : </label>
                        <div class="col-sm-4">
                            <div class="radio-custom radio-primary">
                                <input id="priority" type="radio" value="low" name="priority">
                                <label for="priority" style="margin-right:20px">{!!_('low')!!}</label>
                                <input id="priority" type="radio" value="medium" name="priority">
                                <label for="priority" style="margin-right:20px">{!!_('medium')!!}</label>
                                <input id="priority" type="radio" value="high" name="priority">
                                <label for="priority" style="margin-right:20px">{!!_('high')!!}</label>
                            </div>
                        </div>
                        <div id="deadline_select_div">
	                        <label class="col-sm-2 control-label">{!!_('deadline')!!} : </label>
	                        <div class="col-sm-4">
	                            <select class="form-control" id="deadline" name="deadline">
	                                <option value="">Select</option>
	                                <option value="24 hrs">24 Hours</option>
	                                <option value="3 days">3 Days</option>
	                                <option value="1 week">1 Week</option>
	                                <option value="1 month">1 Month</option>
	                                <option value="custom">Custom</option>
	                            </select>
	                        </div>
	            		</div>
	            		<div style="display:none" id="deadline_date_div">
	            			<label class="col-sm-2 control-label">{!!_('deadline')!!} : </label>
		                    <div class="col-sm-4">
		                        <input type="text" class="{!!getDatePickerClass()!!} form-control" id="deadline_date" name="deadline" value="{!!old('deadline')!!}" readonly="" />
		                    </div>
	            		</div>
                    </div>
                </div>
                
            </div>
            {!! Form::token() !!}
            <div class="form-group">
                <div class="col-sm-12">
                    
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-check"></i>
                        </span>
                        &nbsp;{!!_('save')!!}
                    </button>
                </div>
            </div>
            <hr style="border: 1px dashed #b6b6b6" />
        </form>
    
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){
		
		$("#deadline").change(function(){
			var selectedItem = $("#deadline").val();
			if(selectedItem == "custom")
			{
				$("#deadline_select_div").hide();
				$("#deadline").prop('disabled', true);
				$("#deadline_date_div").fadeIn("slow");
			}
		
		});
		$("#agency").change(function(){
			var selectedItem = $("#agency").val();
			if(selectedItem == "other")
			{
				$("#agency_select_div").hide();
				$("#agency").prop('disabled', true);
				$("#agency_custom_div").fadeIn("slow");
			}
		
		});
    });

</script>


@stop