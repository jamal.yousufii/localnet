@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('case_edit_form')!!}</title>
    <style type="text/css">
        
    </style>
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
	{!! HTML::style('/css/autocomplete/jquery-ui.css') !!}

@stop

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('success')!!}
        </div>

    @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                <span aria-hidden="true">×</span>
            </button>
            {!!Session::get('fail')!!}
        </div>
    @endif
	
<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <h3 id="form_title">{!!_('task_edit_form')!!}</h3>
            </div>
        </div>
        <hr style="border: 1px dashed #b6b6b6" />
    </div>

    <div class="container">
    
	@foreach($task as $item)
		<?php
				
			if(isShamsiDate()){
				
				$start_date = checkEmptyDate($item->start_date);
				$deadline = checkEmptyDate($item->deadline);
			}
			else{
				$start_date = checkGregorianEmtpyDate($item->start_date);
				$deadline = checkGregorianEmtpyDate($item->deadline);
			}
            // $start_date = checkEmptyDate($item->start_date);
            // $deadline = checkEmptyDate($item->deadline);

		?>
        <form role="form" method="post" action="{!! URL::route('postEditCase', array('id' => $item->id)) !!}" class="form-horizontal">
			
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('task_title')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" name="task_title" value="{!!$item->task_title!!}" required/>
                </div>
                <label class="col-sm-2 control-label">{!!_('priority')!!} : </label>
                <div class="col-sm-4">
                    <div class="radio-custom radio-primary">
                        <input id="priority" type="radio" value="1" name="priority" <?php if($item->priority == 1) echo "checked='checked'" ?>>
                        <label for="priority" style="margin-right:20px">{!!_('low')!!}</label>
                        <input id="priority" type="radio" value="2" name="priority" <?php if($item->priority == 2) echo "checked='checked'" ?>>
                        <label for="priority" style="margin-right:20px">{!!_('high')!!}</label>
                	</div>
                </div>
            </div>
			<div class="form-group">
				<label class="col-sm-2 control-label">{!!_('requesting_person')!!} : </label>
                    <div class="col-sm-4">
	                    <select class="form-control" name='requesting_person' id="requesting_person" data-plugin="select2" style="width:100% !important;">
	                        <option value=''>Persons...</option>
	                        {!!getCasePersons($item->requesting_person)!!}
	                    </select>
                    </div>
                <!-- <label class="col-sm-2 control-label">{!!_('requesting_person')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" id="requesting_person" type="text" name="requesting_person"  required/>
                </div> -->
                <label class="col-sm-2 control-label">{!!_('department')!!} : </label>
                <div class="col-sm-4">
                    <select class="form-control" name='department' id="department" data-plugin="select2" style="width:100% !important;" required>
                        {!!getCaseDepartments($item->department)!!}
                    </select>
                </div>
                <!-- <label class="col-sm-2 control-label">{!!_('department')!!} : </label>
                <div class="col-sm-4">
                    <input class="form-control" id="department" type="text" name="department" value="{!!$item->department!!}" required/>
                </div> -->
            </div>
			<div class="form-group">
                <label class="col-sm-2 control-label">{!!_('start_date')!!} : </label>
                <div class="col-sm-4">
                    <input type="text" class="{!!getDatePickerClass()!!} form-control" name="start_date" value="{!!$start_date!!}" readonly="" />
                </div>
                <label class="col-sm-2 control-label">{!!_('task_request_level')!!} : </label>
                <div class="col-sm-4">
                    <select name="task_request_level" class="form-control">
                        {!!getTaskRequestLevel($item->task_request_level)!!}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('deadline')!!} : </label>
                <div class="col-sm-4">
                    <input class="{!!getDatePickerClass()!!} form-control" name="deadline" type="text" value="{!!$deadline!!}" readonly="" />
                </div>
                <label class="col-sm-2 control-label">{!!_('task_requrring')!!} : </label>
                <div class="col-sm-4">
                    <div class="checkbox-custom checkbox-primary">
                    	@if($item->task_requrring == 1)
                        <input id="requrring" type="checkbox" name="task_recurring" checked="checked">
                        <label for="requrring"></label>
                        @else
                        <input id="requrring" type="checkbox" name="task_recurring">
                        <label for="requrring"></label>
                        @endif
                    </div>
                    <div id="requrring_options_div" style="display:none">
	                    <div class="radio-custom radio-primary">
	                    	<input id="requrring_options" class="requrring_options" type="radio" value="daily" name="recurring_type" <?php if($item->recurring_type == "daily") echo "checked='checked'" ?>>
	                        <label for="requrring_options" style="margin-right:20px">{!!_('daily')!!}</label>
	                        <input id="requrring_options" class="requrring_options" type="radio" value="weekly" name="recurring_type" <?php if($item->recurring_type == "weekly") echo "checked='checked'" ?>>
	                        <label for="requrring_options" style="margin-right:20px">{!!_('weekly')!!}</label>
	                        <input id="requrring_options" class="requrring_options" type="radio" value="monthly" name="recurring_type" <?php if($item->recurring_type == "monthly") echo "checked='checked'" ?>>
	                        <label for="requrring_options" style="margin-right:20px">{!!_('monthly')!!}</label>
	                        <input id="requrring_options" class="requrring_options" type="radio" value="annual" name="recurring_type" <?php if($item->recurring_type == "annual") echo "checked='checked'" ?>>
	                        <label for="requrring_options">{!!_('annual')!!}</label>
	                    </div>
	                </div>
	        	</div>
            </div>
            <div class="form-group">
                
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                    <div class="radio-custom radio-primary">
                        <input id="inputRadiosUnchecked" type="radio" value="fix" name="deadline_option" <?php if($item->deadline_option == "fix") echo "checked='checked'" ?>>
                        <label for="inputRadiosUnchecked" style="margin-right:30px">{!!_('fix')!!}</label>
                        <input id="inputRadiosUnchecked" type="radio" value="flexible" name="deadline_option" <?php if($item->deadline_option == "flexible") echo "checked='checked'" ?>>
                        <label for="inputRadiosUnchecked" style="margin-right:30px">{!!_('flexible')!!}</label>
                        <input id="inputRadiosUnchecked" type="radio" value="open" name="deadline_option" <?php if($item->deadline_option == "open") echo "checked='checked'" ?>>
                        <label for="inputRadiosUnchecked">{!!_('open')!!}</label>
                    </div>
                </div>
               	<label class="col-sm-2 control-label">{!!_('alarm')!!} : </label>
                <div class="col-sm-4">
                    <div class="radio-custom radio-primary">
                    	<input id="inputRadiosUnchecked" type="radio" value="1 hour" name="alarm" <?php if($item->alarm == "1 hour") echo "checked='checked'" ?>>
                        <label for="inputRadiosUnchecked" style="margin-right:30px">{!!_('1_hour')!!}</label>
                        <input id="inputRadiosUnchecked" type="radio" value="1 day" name="alarm" <?php if($item->alarm == "1 day") echo "checked='checked'" ?>>
                        <label for="inputRadiosUnchecked" style="margin-right:30px">{!!_('1_day')!!}</label>
                        <input id="inputRadiosUnchecked" type="radio" value="1 week" name="alarm" <?php if($item->alarm == "1 week") echo "checked='checked'" ?>>
                        <label for="inputRadiosUnchecked">{!!_('1_week')!!}</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('task_assigned_to')!!} : </label>
                <div class="col-sm-4">
                    <div class="radio-custom radio-primary">
                        <input id="assigned_task" class="in_house" type="radio" value="in_house" name="assigned_to_option" <?php if($item->assigned_to_option == "in_house") echo "checked='checked'" ?>>
                        <label for="assigned" style="margin-right:30px">{!!_('in_house')!!}</label>
                        <input id="assigned_task" class="agency" type="radio" value="agency" name="assigned_to_option" <?php if($item->assigned_to_option == "agency") echo "checked='checked'" ?>>
                        <label for="assigned" style="margin-right:30px">{!!_('agency')!!}</label>
                        <input id="assigned_task" class="external" type="radio" value="external" name="assigned_to_option" <?php if($item->assigned_to_option == "external") echo "checked='checked'" ?>>
                        <label for="assigned">{!!_('external')!!}</label>
                    </div>
                </div>
                <label class="col-sm-2 control-label">{!!_('task_category')!!} : </label>
                <div class="col-sm-4">
	                <input type="text" class="form-control" name="task_category" id="task_category" value="{!!$item->task_category!!}" />
                    <!-- <select name="task_category" class="form-control">
                        {!!getCategories($item->task_category)!!}
                    </select> -->
                </div>
            </div>
            @if($item->assigned_to_option == "in_house")
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('task_assigned_to_category')!!} : </label>
                <div class="col-sm-4">
                    <select name="assigned_to_category" class="form-control">
                        {!!getCategories($item->task_assigned_to)!!}
                    </select>
                </div>
            </div>
            @else
            <div class="form-group" style="display:none" id="in_house">
                <label class="col-sm-2 control-label">{!!_('task_categories')!!} : </label>
                <div class="col-sm-4">
                    <select name="assigned_to_category" class="form-control">
                        {!!getCategories()!!}
                    </select>
                </div>
            </div>
            @endif
            @if($item->assigned_to_option == "agency")
            <div class="form-group">
                <label class="col-sm-2 control-label">{!!_('task_assigned_to_agency')!!} : </label>
                <div class="col-sm-4">
                    <select name="assigned_to_agency" class="form-control">
                        {!!getCaseAgencies($item->task_assigned_to)!!}
                    </select>
                </div>
            </div>
            @else
            <div class="form-group" style="display:none" id="agency">
                <label class="col-sm-2 control-label">{!!_('task_agencies')!!} : </label>
                <div class="col-sm-4">
                    <select name="assigned_to_agency" class="form-control">
                        {!!getCaseAgencies()!!}
                    </select>
                </div>
            </div>
            @endif
            @if($item->assigned_to_option == "external")
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name='person_name' value="{!!$name!!}" id="person_name" />
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name='related_department' value="{!!$department!!}" id="related_department" />
                </div>
            </div>
            @else
            <div class="form-group" style="display:none" id="external">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" id="person_name" name="person_name" placeholder="Person Name">
                </div>
                <div class="col-sm-4">
                    <select class="form-control" name="related_department">
                    	{!!getTaskDepartments();!!}
                    </select>
                </div>
            </div>
            @endif
			<div class="form-group">
                <label class="col-sm-2 control-label">{!!_('task_description_in_english')!!} : </label>
                <div class="col-sm-4">
                    <textarea cols="30" rows="4" class="form-control" name="task_description_en" required>{!!$item->task_description_en!!}</textarea>
                </div>
                <label class="col-sm-3 control-label">{!!_('task_description_in_dari_or_pashto')!!} : </label>
                <div class="col-sm-3">
                    <textarea cols="30" rows="4" class="form-control" name="task_description_dr_ps">{!!$item->task_description_dr_ps!!}</textarea>
                </div>
            </div>
            {!! Form::token() !!}
            <div class="form-group">
                <div class="col-sm-12" style="margin-top:30px">
                    <button class="btn btn-success" type="submit">
                        <span>
                            <i class="fa fa-save"></i>
                        </span>
                        &nbsp;{!!_('save_changes')!!}
                    </button>
                    <a href="{!! URL::route('caseList') !!}" class="btn btn-danger">
                        <span>
                            <i class="fa fa-remove"></i>
                        </span>
                        &nbsp;{!!_('cancel')!!}
                    </a>
                </div>
            </div>
            <hr style="border: 1px dashed #b6b6b6" />
        </form>
    @endforeach
    </div>

</div>

@stop

@section('footer-scripts')
	{!! HTML::script('/js/autocomplete/jquery-ui.js')!!}

<script type="text/javascript">

$("#requesting_person").select2();
// $("#department").select2();
//$("#person_name").select2();
//$("#related_department").select2();

$( "#person_name" ).autocomplete({

	source: function(request, response) {
		
		$.ajax({ 
			url:"{!!URL::route('getAutocompleteList', array('table' => 'person', 'field' => 'name'))!!}",
			data: { term: $("#person_name").val()},
			dataType: "json",
			type: "POST",
			success: function(data){
				response(data);				
			}
	});
},
minLength: 1
});
$( "#related_department" ).autocomplete({

	source: function(request, response) {
		
		$.ajax({ 
			url:"{!!URL::route('getAutocompleteList', array('table' => 'department', 'field' => 'name'))!!}",
			data: { term: $("#related_department").val()},
			dataType: "json",
			type: "POST",
			success: function(data){
				response(data);				
			}
	});
},
minLength: 1
});
//get the task category autocomplete.
$( "#task_category" ).autocomplete({

	source: function(request, response) {
		
		$.ajax({ 
			url:"{!!URL::route('getAutocompleteList', array('table' => 'category', 'field' => 'name'))!!}",
			data: { term: $("#task_category").val()},
			dataType: "json",
			type: "POST",
			success: function(data){
				response(data);				
			}
	});
},
minLength: 1
});

    $(function(){     
		
		// onchange of the person name field bring it's related department and change the value of the department.
        $("#requesting_person").change(function(){
            var user_id = $(this).val();
            $.ajax({
                url : "{!!URL::route('getRelatedCaseDepartment')!!}",
                type : 'POST',
                data : "user_id="+user_id,
                dataType:"html",
                success : function(response){
                    $("#department").html(response);
                }
            })
        });
		
        // on page load
        if($("#requrring").prop("checked")){
            $("#requrring_options_div").fadeIn("slow");
        }
        else{
            $("#requrring_options_div").fadeOut("slow");   
            $(".requrring_options").prop('checked', false);             
        }
        
        
        // on click
        $("#requrring").click(function(){
            
            if($("#requrring").prop("checked")){
                $("#requrring_options_div").fadeIn("slow");
            }
            else{
                $("#requrring_options_div").fadeOut("slow");   
                $(".requrring_options").prop('checked', false);             
            }

        });

        $("#next_page").click(function(){
            $("#first_page").hide("slow");
            $("#second_page").fadeIn("fast");
            $("#form_title").html("{!!_('task_description')!!}");
            return false;
        });

        $("#back").click(function(){
            $("#first_page").fadeIn("slow");
            $("#second_page").hide("fast");
            $("#form_title").html("{!!_('task_form')!!}");
            return false;
        });
        $(".in_house").click(function(){
            if($(".in_house").prop("checked")){
                $("#in_house").fadeIn("slow");
            }
            else{
                $("#in_house").fadeOut("slow");   
            }
            $("#agency").hide("slow");
            $("#external").hide("slow");
        });
        $(".agency").click(function(){
            if($(".agency").prop("checked")){
                $("#agency").fadeIn("slow");
            }
            else{
                $("#agency").fadeOut("slow");   
            }
            $("#in_house").hide("slow");
            $("#external").hide("slow");
        });
        $(".external").click(function(){
            if($(".external").prop("checked")){
                $("#external").fadeIn("slow");
            }
            else{
                $("#external").fadeOut("slow");   
            }
            $("#agency").hide("slow");
            $("#in_house").hide("slow");
        });

    });

</script>


@stop