@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('contract_edit_form')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
        span.short{
            display: inline-block;
            width: 100px !important;
        }
        td{
            font-size: 0.875em !important;
            padding: 12px 8px !important;
            vertical-align: middle !important;
            font-weight: 300 !important;
        }
    </style>

@stop

@section('content')

@if(Session::has('success'))
    <div class='alert alert-success span6 noprint'>{!!Session::get('success')!!}</div>

@elseif(Session::has('fail'))
    <div class='alert alert-danger span6 noprint'>{!!Session::get('fail')!!}</div>
@endif
@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('contractsList') !!}">{!!_('contract_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('edit_contract')!!}</span>
                    </li>
                </ol>
                <h3>{!!_('contract_edit_form')!!}</h3>
            </div>
        </div>
    </div>
    <div class="container">
    @foreach($contract AS $item)
        <form role="form" method="post" action="{!! URL::route('postEditContract', array('id' => $main_id)) !!}" class="form-horizontal">

            <table class="table pull-right">
	@if(contractRoleCheck('procurement_contracts_list'))
                <tr>
                    <td>{!!_('contract_name')!!}</td>
                    <td>
                        <input class="form-control" type="text" name="contract_name" value="{!!$item->contract_name!!}" />
                    </td>
                    <td>{!!_('contract_no')!!}</td>
                    <td>
                        <input class="form-control" type="number" name="contract_no" value="{!!$item->contract_no!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('start_letter_date')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="start_letter_date" value="{!!checkEmptyDate($item->start_letter_date)!!}" />
                    </td>
                    <td>{!!_('contract_completion_date')!!}</td>
                    <td>
                        <input class="datepicker_farsi form-control" type="text" id="contract_completion_date" name="contract_completion_date" value="{!!checkEmptyDate($item->contract_completion_date)!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('winner_bidder')!!}</td>
                    <td>
                        <input class="form-control" type="text" name="winner_bidder" value="{!!$item->winner_bidder!!}" />
                    </td>
                    <td>{!!_('total_contract_price')!!}</td>
                    <td>
                        <input type="number" class="form-control total_price" name="total_contract_price" value="{!!$item->total_contract_price!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('request')!!}</td>
                    <td>
                        <input type="number" class="form-control" name="request" value="{!!$item->request!!}" />
                    </td>
                    <td>{!!_('request_date')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="request_date" value="{!!checkEmptyDate($item->request_date)!!}" />
                    </td>
                </tr>
                <tr>
                <tr>
                    <td>{!!_('date_of_preparation_of_m3')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_preparation_of_m3" value="{!!checkEmptyDate($item->date_of_preparation_of_m3)!!}" />
                    </td>
                    <td>{!!_('m3_no')!!}</td>
                    <td>
                        <input type="number" class="form-control" name="m3_no" value="{!!$item->m3_no!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('total_m3_price')!!}</td>
                    <td>
                        <input type="number" class="form-control" id="m3_total_amount" name="total_m3_price" value="{!!$item->total_m3_price!!}" />
                    </td>
                    <td>{!!_('date_of_m3')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_m3" value="{!!checkEmptydate($item->date_of_m3)!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('m3_no_to_stock')!!}</td>
                    <td>
                        <input type="number" class="form-control" name="m3_no_to_stock" value="{!!$item->m3_no_to_stock!!}" />
                    </td>
                    <td>{!!_('date_of_m3_to_stock')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_m3_to_stock" value="{!!checkEmptyDate($item->date_of_m3_to_stock)!!}" />
                    </td>
                </tr>
                <tr>
                	<td>{!!_('contract_modification_price')!!}</td>
                    <td>
                        <input type="number" class="form-control" name="contract_modification_price" value="{!!$item->contract_modification_price!!}" />
                    </td>
                    <td>{!!_('total_remained_contract_price')!!}</td>
                    <td>
                        <input type="number" class="form-control total_remaining_price" name="total_remained_contract_price" value="{!!$item->total_remained_contract_price!!}" readonly="" />
                    </td>
                </tr>
                <tr>
                	<td>{!!_('remained_contract_time')!!}</td>
                    <td>
                        <input type="text" class="form-control" id="remained_contract_time" name="remained_contract_time" value="{!!$item->remained_contract_time!!}" readonly="" />
                    </td>
                    <td>{!!_('date_of_contract_termination')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_contract_termination" value="{!!checkEmptyDate($item->date_of_contract_termination)!!}" />
                    </td>
                </tr>
                <tr>
                	<td>{!!_('termination_reason')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" name="termination_reason">{!!$item->termination_reason!!}</textarea>
                    </td>
                    <td>{!!_('remarks')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" name="remarks">{!!$item->remarks!!}</textarea>
                    </td>
                </tr>
			@endif
			@if(contractGoodsRoleCheck('procurement_contracts_list'))

	   			<tr>
                    <td>{!!_('date_of_submission_letter_to_finance_department')!!} :</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_submission_letter_to_finance_department" value="{!!checkEmptyDate($item->date_of_submission_letter_to_finance_department)!!}" />
                    </td>
                    <td>{!!_('m3_number_sent_to_finance')!!}</td>
                    <td>
                        <input type="number" class="form-control" name="m3_number_sent_to_finance" value="{!!$item->m3_number_sent_to_finance!!}" />
                    </td>
                </tr>
                <tr>
                	<td>{!!_('amount_sent_to_finance')!!}</td>
                    <td>
                        <input type="number" class="form-control" name="amount_sent_to_finance" value="{!!$item->amount_sent_to_finance!!}" />
                    </td>
                </tr>

			@endif
			@if(financeContractRoleCheck('procurement_contracts_list'))
				<tr>
                    <td>{!!_('date_of_sent_to_finance')!!} :</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_sent_to_finance" value="{!!checkEmptyDate($item->date_of_sent_to_finance)!!}" />
                    </td>
                </tr>
			@endif
                <input type="hidden" name="procurement_id" value="{!!@$id!!}">
                <input type="hidden" name="field" value="{!!@$field!!}">
                <input type="hidden" id="g_contract_completion_date" value="">
            </table>
            <div class="form-group">
                <div class="col-sm-12">
                    <hr style="border: 1px dashed #b6b6b6" />
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-check"></i>
                        </span>
                        &nbsp;{!!_('submit')!!}
                    </button>
                    <a href="{!! URL::route('contractsList') !!}" class="btn btn-danger">
                        <span>
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>
                        &nbsp;{!!_('cancel')!!}
                    </a>
                </div>
            </div>
        </form>
    @endforeach()
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     
        
        var total_price = parseInt($(".total_price").val());
        var m3_total_amount = $("#m3_total_amount").val();
        if(m3_total_amount > total_price){
            alert('لطفآ قیمت مجموعی م ۳ را از قیمت مجموعی قرار داد کرده زیاد تر ننویسید !');
        }
        else{
            var total_remaining_price = total_price - m3_total_amount;
            $(".total_remaining_price").val(total_remaining_price); 
        }

        $("#contract_completion_date").change(function(){
        var selected_date = $("#contract_completion_date").val(); 

        $.ajax({
            type : 'post',
            url : "{!!URL::route('changeDate')!!}",
            data : "date="+selected_date,
            success : function(response){

                $("#g_contract_completion_date").val(response);
                // find the contract remained time through jquery.
                var contract_completion_date = new Date($('#g_contract_completion_date').val());
                var current_date = new Date("<?=date('Y-m-d');?>");
                var timeDiff = Math.abs(contract_completion_date.getTime() - current_date.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                if(diffDays == 1 || diffDays == 0){
                    diffDays = diffDays+" Day";
                }
                else{
                    diffDays = diffDays+" Days";
                }
                $('#remained_contract_time').val(diffDays);
            }
        });

    });

    });

    $("#m3_total_amount").blur(function(){
        var total_price = parseInt($(".total_price").val());
        var m3_total_amount = $("#m3_total_amount").val();
        if(m3_total_amount > total_price){
            alert('لطفآ قیمت مجموعی م ۳ را از قیمت مجموعی قرار داد کرده زیاد تر ننویسید !');
            $("#m3_total_amount").focus();
        }
        else{
            var total_remaining_price = total_price - m3_total_amount;
            $(".total_remaining_price").val(total_remaining_price); 
        }
    });

</script>

@stop