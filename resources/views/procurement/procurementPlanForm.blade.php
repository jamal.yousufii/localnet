@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('procurement_plan_form')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
        span.short{
            display: inline-block;
            width: 100px !important;
        }
        td{
            font-size: 0.875em !important;
            padding: 12px 8px !important;
            vertical-align: middle !important;
            font-weight: 300 !important;
        }
    </style>

@stop

@section('content')

@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('procurementPlanList') !!}">{!!_('procurement_plan_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('add_procurement_plan')!!}</span>
                    </li>
                </ol>
                <h3>{!!_('procurement_plan_form')!!}</h3>
            </div>
        </div>
    </div>

    <div class="container">

        <form role="form" method="post" action="{!! URL::route('postAddProcurementPlan') !!}" class="form-horizontal">

            <table class="table pull-right">
                <tr>
                    <td>{!!_('sector')!!}</td>
                    <td>
                        <input class="form-control" type="text" name="sector" value="{!!old('sector')!!}" />
                    </td>
                    <td>{!!_('procurement_entity')!!}</td>
                    <td>
                        <input class="form-control" type="text" name="procurement_entity" value="{!!old('procurement_entity')!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('budget_type')!!}</td>
                    <td>
                        <input type="text" class="form-control" name="budget_type">{!!old('budget_type')!!}</textarea>
                    </td>
                    <td>{!!_('budget_code')!!}</td>
                    <td>
                        <input class="form-control" type="number" name="budeget_code" value="{!!old('budeget_code')!!}" />
                    </td>
                </tr>
                <tr>
                	<td>{!!_('year')!!}</td>
                    <td>
                        <input class="form-control" type="text" name="year" value="{!!old('year')!!}" />
                    </td>
                    <td>{!!_('name_of_project_in_arroved_budget_plan')!!}</td>
                    <td>
                        <input class="form-control" type="text" name="name_of_project_in_arroved_budget_plan" value="{!!old('name_of_project_in_arroved_budget_plan')!!}" />
                    </td>
                </tr>
                <tr>
                	<td>{!!_('donor')!!}</td>
                    <td>
                        <input type="text" class="form-control" name="donor" value="{!!old('donor')!!}" />
                    </td>
                	<td>{!!_('procurement_description')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" name="procurement_description">{!!old('procurement_description')!!}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('type_of_contract')!!}</td>
                    <td>
                        <input type="text" class="form-control" name="type_of_contract" value="{!!old('type_of_contract')!!}" />
                    </td>
                    <td>{!!_('procurement_preference_from_national_resources')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="procurement_preference_from_national_resources" value="{!!old('procurement_preference_from_national_resources')!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('contract_number')!!}</td>
                    <td>
                        <input type="number" step="any" class="form-control" name="contract_number" value="{!!old('contract_number')!!}" />
                    </td>
                    <td>{!!_('date_of_budget_agreement_by_donor_or_mof')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_budget_agreement_by_donor_or_mof" value="{!!old('date_of_budget_agreement_by_donor_or_mof')!!}" readonly/>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('procurement_method')!!}</td>
                    <td>
                        <select name="procurement_method" class="form-control" value="{!!old('procurement_method')!!}">
                            {!!getProcMethod();!!}
                        </select>
                    </td>
                    <td>{!!_('estimated_cost_of_the_project_in_afg')!!}</td>
                    <td>
                        <input type="number" step="any" class="form-control" name="estimated_cost_of_the_project_in_afg" value="{!!old('estimated_cost_of_the_project_in_afg')!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('estimated_date_of_project_announcement')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="estimated_date_of_project_announcement" value="{!!old('estimated_date_of_project_announcement')!!}" readonly="readonly"/>
                    </td>
                    <td>{!!_('estimated_date_of_bid_opening')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="	estimated_date_of_bid_opening" value="{!!old('	estimated_date_of_bid_opening')!!}" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('estimated_date_of_procurement_initiation')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="estimated_date_of_procurement_initiation" value="{!!old('estimated_date_of_procurement_initiation')!!}" readonly="readonly"/>
                    </td>
                    <td>{!!_('date_of_bid_evaluation')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_bid_evaluation" value="{!!old('date_of_bid_evaluation')!!}" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_completion_of_the_bid_evaluation')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_completion_of_the_bid_evaluation" value="{!!old('date_of_completion_of_the_bid_evaluation')!!}" readonly />
                    </td>
                    <td>{!!_('date_of_the_evaluation_report_submission')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_the_evaluation_report_submission" value="{!!old('date_of_the_evaluation_report_submission')!!}" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('estimated_date_of_the_public_award')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="estimated_date_of_the_public_award" value="{!!old('estimated_date_of_the_public_award')!!}" readonly />
                    </td>
                    <td>{!!_('estimation_date_of_contract_sign')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="estimation_date_of_contract_sign" value="{!!old('estimation_date_of_contract_sign')!!}" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('guarantee_period_of_works')!!}</td>
                    <td>
                        <input type="text" class="form-control" name="guarantee_period_of_works" value="{!!old('guarantee_period_of_works')!!}" />
                    </td>
                	<td>{!!_('estimation_date_of_contract_completion')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="estimation_date_of_contract_completion" value="{!!old('estimation_date_of_contract_completion')!!}" readonly />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('remarks')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" name="remarks">{!!old('remarks')!!}</textarea>
                    </td>
                </tr>
    
            </table>
            <div class="form-group">
                <div class="col-sm-12">
                    <hr style="border: 1px dashed #b6b6b6" />
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-check"></i>
                        </span>
                        &nbsp;{!!_('submit')!!}
                    </button>
                    <a href="{!! URL::route('procurementPlanList') !!}" class="btn btn-danger">
                        <span>
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>
                        &nbsp;{!!_('cancel')!!}
                    </a>
                </div>
            </div>
        </form>
    
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     
    

    });


</script>

@stop