@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('purchase_details')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
        span.short{
            display: inline-block;
            width: 100px !important;
        }
        td{
            font-size: 0.875em !important;
            padding: 12px 8px !important;
            vertical-align: middle !important;
            font-weight: 300 !important;
        }
    </style>

@stop

@section('content')

@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('goodsPurchaseList') !!}">{!!_('purchase_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('purchase_details')!!}</span>
                    </li>
                </ol>
                <h3>{!!_('purchase_detailed_information')!!}</h3>
            </div>
            <div class="pull-right" style="margin-right:50px">
            	<a href="javascript:history.back()" class="btn btn-success">{!!_('go_back')!!}</a>
            </div>
        </div>
    </div>

    <div class="container">
    @foreach($purchase as $item)
            <table class="table pull-right">
                
                <tr>
                    <td>{!!_('date_of_preparation')!!}</td>
                    <td>
                        <input class="form-control" type="text" value="{!!checkEmptyDate($item->date_of_preparation)!!}" disabled />
                    </td>
                    <td>{!!_('end_user')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->executive_department!!}" disabled/>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('goods_detail')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" disabled>{!!$item->goods_detail!!}</textarea>
                    </td>
                    <td>{!!_('request_no')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->request_no!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_arrival')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_arrival)!!}" disabled/>
                    </td>
                    <td>{!!_('date_of_submission_to_the_purchasing_committee')!!}</td>
                    <td>
                        <input class="form-control" type="text" value="{!!checkEmptyDate($item->date_of_submission_to_the_purchasing_committee)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('purchasing_team')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->purchasing_team!!}" disabled />
                    </td>
                    <td>{!!_('lowest_price')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->lowest_price!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('winner_company')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->winner_company!!}" disabled />
                    </td>
                    <td>{!!_('m3_no')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->m3_no!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_m3')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_m3)!!}" disabled />
                    </td>
                    <td>{!!_('no_of_letter_sent_to_stock')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->no_of_letter_sent_to_stock!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_submission_to_stock')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_submission_to_stock)!!}" disabled />
                    </td>
                    <td>{!!_('date_of_submission_to_the_finance_department')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_submission_to_the_finance_department)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('delay')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkDateDiff($item->delay)!!}" disabled />
                    </td>
                    <td>{!!_('m3_no_sent_to_finance')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->m3_no_sent_to_finance!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('amount_sent_to_finance')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->amount_sent_to_finance!!}" disabled />
                    </td>
                    <td>{!!_('remarks')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" disabled>{!!$item->remarks!!}</textarea>
                    </td>
                </tr>
    
            </table>
    	@endforeach
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     
    

    });


</script>

@stop