@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('procurement_details')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
        span.short{
            display: inline-block;
            width: 100px !important;
        }
        td{
            font-size: 0.875em !important;
            padding: 12px 8px !important;
            vertical-align: middle !important;
            font-weight: 300 !important;
        }
    </style>

@stop

@section('content')

@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('procurementList') !!}">{!!_('procurement_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('procurement_details')!!}</span>
                    </li>
                </ol>
                <h3>{!!_('procurement_detailed_information')!!}</h3>
            </div>
            <div class="pull-right" style="margin-right:50px">
            	<a href="javascript:history.back()" class="btn btn-success">{!!_('go_back')!!}</a>
            </div>
        </div>
    </div>

    <div class="container">
    @foreach($procurement as $item)
        <form role="form" method="post" action="{!! URL::route('postEditProcurement') !!}" class="form-horizontal">

            <table class="table pull-right">
                
                <tr>
                    <td>{!!_('budget_code')!!}</td>
                    <td>
                        <input class="form-control" type="text" value="{!!$item->budget_code!!}" disabled />
                    </td>
                    <td>{!!_('estimation_cost')!!}</td>
                    <td>
                        <input class="form-control" type="text" value="{!!$item->estimation_cost!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('procurement_details')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" disabled>{!!$item->procurement_details!!}</textarea>
                    </td>
                    <td>{!!_('end_user')!!}</td>
                    <td>
                       <input type="text" class="form-control" value="{!!$item->executive_department!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('contract_number')!!}</td>
                    <td>
                        <input class="form-control" type="text" value="{!!$item->contract_number!!}" disabled />
                    </td>
                    <td>{!!_('procurement_type')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->procurement_type!!}" disabled/>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('procurement_method')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->procurement_method!!}" disabled />
                    </td>
                    <td>{!!_('project_submission_date_to_procurement_entity')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->project_submission_date_to_procurement_entity)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_procurement_approval')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_procurement_approval)!!}" disabled/>
                    </td>
                    <td>{!!_('delay')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkDateDiff($item->submission_delay)!!}" disabled/>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('announcement_date')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->announcement_date)!!}" disabled/>
                    </td>
                    <td>{!!_('date_of_pre_qualification_meeting')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_pre_qualification_meeting)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('delay')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkDateDiff($item->announce_delay)!!}" disabled/>
                    </td>
                    <td>{!!_('date_of_bid_opening')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_bid_opening)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('evaluation_completion_date')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->evaluation_completion_date)!!}" disabled/>
                    </td>
                    <td>{!!_('delay')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkDateDiff($item->bid_opening_delay)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_procurement_award')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_procurement_award)!!}" disabled/>
                    </td>
                    <td>{!!_('date_of_npc_approval')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_npc_approval)!!}" disabled/>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('delay')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkDateDiff($item->npc_approval_delay)!!}" disabled/>
                    </td>
                    <td>{!!_('date_of_notice_of_procurement_award')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_notice_of_procurement_award)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('proc_start_letter_date')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->start_letter_date)!!}" disabled />
                    </td>
                    <td>{!!_('delay')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkDateDiff($item->acceptance_letter_delay)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('winner_bidder')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->winner_bidder!!}" disabled/>
                    </td>
                    <td>{!!_('remarks')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" disabled>{!!$item->remarks!!}</textarea>
                    </td>
                </tr>

            </table>
        </form>
    @endforeach
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     
    

    });


</script>

@stop