@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('archived_contract_list')!!}</title>
    <style type="text/css">
        table th{
        	padding: 0px; !important;
        	font-size: 12px !important;
        	font-weight: bold !important;
        	color: #000 !important;
        	text-align: center;
        }
        table td{
        	padding: 0px; !important;
        	font-size: 12px !important;
        	color: #000 !important;
        	text-align: center;
        }table th:last-child, table td:last-child{
        	width: 12% !important;
        	border-right: 1px solid #E4EAEC !important;
        }
        a#ToolTables_contractsList_0{
            padding: 5px 10px !important;
            text-decoration: none;
            font-weight: bold;
            margin-left: 10px;
            float: right;
        }
        a#ToolTables_contractsList_0:hover{
            box-shadow: 0 0 5px #888;
            text-decoration: none;
        }
        .fixed{width: 11%;}
        a{text-decoration:none !important;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop


@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success span6 noprint'>{!!Session::get('success')!!}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger span6 noprint'>{!!Session::get('fail')!!}</div>
    @endif
    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="form-group">
	                <div class="col-sm-3">
	                    <ol class="breadcrumb">
	                        <li>
	                            <a href="{!! URL::route('home') !!}">{!!_('Dashboard')!!}</a>
	                        </li>
	                        <li class="active">
	                            <span>{!!_('archived_contracts')!!}</span>
	                        </li>
	                    </ol>
	                    <h3>{!!_('archived_contract_list')!!}</h3>
	                </div>
	            </div>
	            <div class="form-group">
	            	<div class="col-sm-4">
	            		<h5>{!!_('total_archived_contracts')!!} : <span style="color:green;font-weight:bold">{!!getTotalNumberOfRecords('contract',1)!!}</span></h5>
	            	</div>
	            	<div class="col-sm-3" id="success" style="color:green;"></div>
	            </div>
            </div>
        </div>
        <div style="padding:15px" class="table-responsive">
        	@if(contractGoodsRoleCheck('procurement_contracts_list'))
            <table class="table table-bordered table-responsive" id="contractsList">
                <thead>
                  <tr>
                    <th>{!!_('no#')!!}</th>
                    <th>{!!_('name')!!}</th>
                    <th>{!!_('start_letter_date')!!}</th>
                    <th>{!!_('contract_completion_date')!!}</th>
                    <th>{!!_('m3_no')!!}</th>
                    <th>{!!_('total_m3_price')!!}</th>
                    <th>{!!_('date_of_m3')!!}</th>
                    <th>{!!_('date_of_m3_to_stock')!!}</th>
                    <th>{!!_('date_of_submission_letter_to_finance_department')!!}</th>
                    <th>{!!_('m3_number_sent_to_finance')!!}</th>
                    <th>{!!_('amount_sent_to_finance')!!}</th>

                    <th>{!!_('operations')!!}</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
			@else
			<table class="table table-bordered table-responsive" id="contractsList">
                <thead>
                  <tr>
                    <th>{!!_('no#')!!}</th>
                    <th>{!!_('name')!!}</th>
                    <th>{!!_('start_letter_date')!!}</th>
                    <th>{!!_('contract_completion_date')!!}</th>
                    <th>{!!_('m3_no')!!}</th>
                    <th>{!!_('total_m3_price')!!}</th>
                    <th>{!!_('date_of_m3')!!}</th>
                    <th>{!!_('m3_no_to_stock')!!}</th>
                    <th>{!!_('date_of_m3_to_stock')!!}</th>
                    <th>{!!_('date_of_submission_letter_to_finance_department')!!}</th>

                    <th>{!!_('operations')!!}</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
			@endif
        </div>
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#contractsList').dataTable(
        {

            "sDom": 'Tlfr<"clearfix">tip',
            //"sDom": 'lrf<"clear spacer">Ttip',
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "print",
                        "bShowAll": true
                    }
                ]
            },
            "bProcessing": false,
            "bServerSide": false,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('archivedContractData')!!}"
        }
    );
	$("#contractsList th").css({'font-size':'11px','font-weight':"normal"});
    $("#contractsList td").css({'font-size':'10px'});
    $("#ToolTables_contractsList_0").html("{!!_('print_view')!!}");
    $("#ToolTables_contractsList_0").addClass('btn btn-success');
    $("#ToolTables_contractsList_0").click(function(){
        //$("#contractsList td:last-child").hide();
        //$("#contractsList th:last-child").hide();
        $("#contractsList th").removeClass('sorting');
        $("#contractsList th").css({'font-size':'10px','font-weight':'bold'});
        $("#contractsList td").css({'font-size':'10px'});
    });

});

$(document).keyup(function(e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
        //$("#contractsList td:last-child").show();
	    //$("#contractsList th:last-child").show();
	    $("#contractsList th").addClass('sorting');
	    $("#contractsList th").css({'font-size':'11px','font-weight':'normal'});
	    $("#contractsList td").css({'font-size':'10px'});
    }
});

</script>

@stop


