@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('purchase_list')!!}</title>
    <style type="text/css">
        a#ToolTables_purchaseList_0{
            padding: 5px 10px !important;
            text-decoration: none;
            font-weight: bold;
            margin-left: 10px;
            float: right;
        }
        a#ToolTables_purchaseList_0:hover{
            box-shadow: 0 0 5px #888;
            text-decoration: none;
        }
        table th{
        	padding: 0px; !important;
        	font-size: 12px !important;
        	font-weight: bold !important;
        	color: #000 !important;
        	text-align: center;
        }
        table td{
        	padding: 0px; !important;
        	font-size: 12px !important;
        	color: #000 !important;
        	text-align: center;
        }table th:last-child, table td:last-child{
        	width: 6% !important;
        	border-right: 1px solid #E4EAEC !important;
        }
        .pu
        .fixed{width: 11%;}
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop


@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success span6 noprint'>{!!Session::get('success')!!}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger span6 noprint'>{!!Session::get('fail')!!}</div>
    @endif
    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
	            <div class="form-group">
	                <div class="col-sm-4">
	                    <ol class="breadcrumb">
	                        <li>
	                            <a href="{!! URL::route('home') !!}">{!!_('Dashboard')!!}</a>
	                        </li>
	                        <li class="active">
	                            <span>{!!_('all_purchase_list')!!}</span>
	                        </li>
	                    </ol>
	                    <h3>{!!_('goods_purchase_list')!!}</h3>
	                </div>
	                <div class="col-sm-3">
	                    <h5>{!!_('total_amount_of_lowest_price')!!} : <span style="color:green;font-weight:bold">{!!getTotalAmounts('goods_purchase','lowest_price')!!}</span></h5>
	                </div>
	                <div class="col-sm-3">
	            		<h5>{!!_('total_amount_sent_to_finance')!!} : <span style="color:green;font-weight:bold">{!!getTotalAmounts('goods_purchase','amount_sent_to_finance')!!}</span></h5>
	            	</div>
	            </div>
	            <div class="form-group">
	            	<div class="col-sm-3">
	            		<h5>{!!_('total_number_of_purchases_exist')!!} : <span style="color:green;font-weight:bold">{!!getTotalNumberOfRecords('goods_purchase')!!}</span></h5>
	            	</div>
	            	<div class="col-sm-3">
	            		<h5>{!!_('total_documents_sent_to_mof')!!} : <span style="color:green;font-weight:bold">{!!getTotalAmounts('goods_purchase','date_of_sent_to_finance')!!}</span></h5>
	            	</div>
	            	<?php $lang = getLangShortcut();?>
	            	@if(purchaseRoleCheck('procurement_purchase_list'))
		            <div class="col-sm-1 pull-right" <?php if($lang == "en") echo 'style="margin:10px 55px 0 0"'; else echo 'style="margin:10px 80px 0 0"';?>>
		                <a class="btn btn-success pull-left" href="{!!URL::route('getLoadPurchaseForm')!!}">
		                    <i class="fa fa-plus-circle fa-lg"></i>
		                    {!!_('add_purchase')!!}
		                </a>
		            </div>
		            @endif
	            </div>
	            <div class="form-group">
	            	<div class="form-group" style="margin-top:20px">
		            	
		                <div class="col-sm-3">
		                    <select class="form-control" id="purchasing_team">
					        	{!!getPurchasingTeam()!!}
					        </select>
		                </div>
		            </div>
	            </div>
	    	</div>
        </div>
        
        <div style="padding:15px" class="table-responsive" id="search_result">
            @if(goodsRoleCheck('procurement_purchase_list'))
            <table class="table table-bordered table-responsive" id="purchaseList">
                <thead>
                  <tr>
                    <th>{!!_('no#')!!}</th>
                    <th>{!!_('date_of_preparation')!!}</th>
                    <th>{!!_('end_user')!!}</th>
                    <th>{!!_('goods_detail')!!}</th>
                    <th>{!!_('request_number')!!}</th>
                    <th>{!!_('date_of_submission_to_the_purchasing_committee')!!}</th>
                    <th>{!!_('lowest_price')!!}</th>
                    <th>{!!_('purchasing_team')!!}</th>
                    <th>{!!_('winner')!!}</th>
                    <th>{!!_('m3_no')!!}</th>
                    <th>{!!_('date_of_submission_to_the_finance_department')!!}</th>
                    <th>{!!_('m3_no_sent_to_finance')!!}</th>
                    <th>{!!_('amount_sent_to_finance')!!}</th>

                    <th>{!!_('operations')!!}</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
            @elseif(financeGoodsRoleCheck('procurement_purchase_list'))
            <table class="table table-bordered table-responsive" id="purchaseList">
                <thead>
                  <tr>
                    <th>{!!_('no#')!!}</th>
                    <th>{!!_('m3_no')!!}</th>
                    <th>{!!_('m3_no_sent_to_finance')!!}</th>
                    <th>{!!_('amount_sent_to_finance')!!}</th>
                    <th>{!!_('date_of_sent_to_finance')!!}</th>

                    <th>{!!_('operations')!!}</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
            @else
            <table class="table table-bordered table-responsive" id="purchaseList">
                <thead>
                  <tr>
                    <th>{!!_('no#')!!}</th>
                    <th>{!!_('date_of_preparation')!!}</th>
                    <th>{!!_('end_user')!!}</th>
                    <th>{!!_('goods_detail')!!}</th>
                    <th>{!!_('request_number')!!}</th>
                    <th>{!!_('purchasing_team')!!}</th>
                    <th>{!!_('lowest_price')!!}</th>
                    <th>{!!_('winner')!!}</th>
                    <th>{!!_('m3_no')!!}</th>
                    <th>{!!_('no_of_letter_sent_to_stock')!!}</th>

                    <th>{!!_('operations')!!}</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
            @endif
        </div>
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#purchaseList').dataTable(
        {
        	//"order" : [[1, 'desc']],
            "sDom": 'Tlfr<"clearfix">tip',
            //"sDom": 'lrf<"clear spacer">Ttip',
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "print",
                        "bShowAll": true
                    }
                ]
            },
            "bProcessing": false,
            "bServerSide": false,
            "bDeferRender": true,
            "iDisplayLength": 10,
            // "bSort" : false,
            "sAjaxSource": "{!!URL::route('getAllPurchases')!!}"
        }
    );
	$("#purchaseList th").css({'font-size':'3px','font-weight':'normal'});
    $("#purchaseList td").css({'font-size':'2px'});
    $("#ToolTables_purchaseList_0").html("{!!_('print_view')!!}");
    $("#ToolTables_purchaseList_0").addClass('btn btn-success');
    $("#ToolTables_purchaseList_0").click(function(){
        $("#purchaseList td:last-child").hide();
        $("#purchaseList th:last-child").hide();
        $("#purchaseList th").removeClass('sorting');
        $("#purchaseList th").css({'font-size':'10px','font-weight':'bold'});
        $("#purchaseList td").css({'font-size':'10px'});
    });

});

	$(document).keyup(function(e) {
	    if (e.keyCode == 27) { // escape key maps to keycode `27`
	        $("#purchaseList td:last-child").show();
		    $("#purchaseList th:last-child").show();
		    $("#purchaseList th").addClass('sorting');
		    $("#purchaseList th").css({'font-size':'11px','font-weight':'normal'});
		    $("#purchaseList td").css({'font-size':'10px'});
	    }
	});
	
	//get the purchase details onchange of purchasing team dropdown.
	$("#purchasing_team").change(function(){
	
		var purchasing_team = $(this).val();
	    $.ajax({
	            url: "{!!URL::route('loadPurchaseSearchView')!!}",
	            data: '&purchasing_team='+purchasing_team,
	            type: 'post',
	            beforeSend: function(){
	                $("#search_result").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	            },
	            success: function(response)
	            {
	                $("#search_result").html(response);
	            }
	        }
	    );
	
	});

</script>

@stop


