@extends('layouts.master')

@section('head')
	<title>Dashboard</title>
	
@stop

@section('content')

<div class="container">
  	<!-- <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
    	<li class="active"><a href="#purchase">{!!_('purchase_related_total_amounts_in_graph')!!}</a></li>
    	<li><a href="#contract">{!!_('contracts_related_total_amounts_in_graph')!!}</a></li>
  	</ul>
  	<div class="tab-content padding-top-20">
	    <div class="tab-pane active" id="purchase" role="tabpanel">
	    	<div class="col-md-6">
				<div id="purchase_chart" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
			</div>
	    </div>
	    <div class="tab-pane" id="contract" role="tabpanel">
	    	<div class="col-md-12">
				<div id="contract_chart" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
			</div>
	    </div>
	</div>-->
	<div class="form-group">
		<div class="col-md-6">
			<div id="purchase_chart" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
		</div>
		<div class="col-md-6">
			<div id="contract_chart" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
		</div>
	</div>
</div>
@stop
@section('footer-scripts')

{!! HTML::script('/js/highcharts/highcharts.js') !!}
{!! HTML::script('/js/highcharts/highcharts-3d.js') !!}
{!! HTML::script('/js/highcharts/exporting.js') !!}

<script type="text/javascript">

$(function () {
    // Create the chart
    $('#purchase_chart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: "{!!_('purchase_related_total_amounts')!!}"
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: "{!!_('amounts_in_million')!!}"
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: "{point.y} {!!_('afn')!!}"
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: "<span style='color:{point.color}'>{point.name}</span>: <b>{point.y} {!!_('afn')!!}</b><br/>"
        },

        series: [{
            name: 'Purchase',
            colorByPoint: true,
            data: [{
                name: "{!!_('total_amount_of_lowest_price')!!}",
                y: {!!getTotalAmounts('goods_purchase','lowest_price')!!},
                drilldown: "{!!_('total_amount_of_lowest_price')!!}"
            }, {
                name: "{!!_('total_amount_sent_to_finance')!!}",
                y: {!!getTotalAmounts('goods_purchase','amount_sent_to_finance')!!},
                drilldown: "{!!_('total_amount_sent_to_finance')!!}"
            }]
        }]
    });
    
    // Create the chart
    $('#contract_chart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: "{!!_('contract_related_total_amounts')!!}"
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: "{!!_('amounts_in_million')!!}"
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: "{point.y} {!!_('afn')!!}"
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: "<span style='color:{point.color}'>{point.name}</span>: <b>{point.y} {!!_('afn')!!}</b><br/>"
        },

        series: [{
            name: 'Procurement and Contract',
            colorByPoint: true,
            data: [{
                name: "{!!_('total_amount_of_procurement_estimation_cost')!!}",
                y: {!!getTotalAmounts('procurement','estimation_cost')!!},
                drilldown: "{!!_('total_amount_of_procurement_estimation_cost')!!}"
            }, {
                name: "{!!_('total_amount_of_contract_total_price')!!}",
                y: {!!getTotalAmounts('contract','total_contract_price')!!},
                drilldown: "{!!_('total_amount_of_contract_total_price')!!}"
            }, {
                name: "{!!_('total_amount_sent_to_finance')!!}",
                y: {!!getTotalAmounts('contract','amount_sent_to_finance')!!},
                drilldown: "{!!_('total_amount_sent_to_finance')!!}"
            }]
        }]
    });
});
</script>
@stop
