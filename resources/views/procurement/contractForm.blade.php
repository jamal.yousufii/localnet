@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('contract_form')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
        span.short{
            display: inline-block;
            width: 100px !important;
        }
        td{
            font-size: 0.875em !important;
            padding: 12px 8px !important;
            vertical-align: middle !important;
            font-weight: 300 !important;
        }
    </style>

@stop

@section('content')

@if(Session::has('success'))
    <div class='alert alert-success span6 noprint'>{!!Session::get('success')!!}</div>

@elseif(Session::has('fail'))
    <div class='alert alert-danger span6 noprint'>{!!Session::get('fail')!!}</div>
@endif
@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('contractsList') !!}">{!!_('contracts_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('add_contract')!!}</span>
                    </li>
                </ol>
                <h3>{!!_('contract_form')!!}</h3>
            </div>
        </div>
    </div>
    <div class="container">

        <form role="form" method="post" action="{!! URL::route('postAddContract') !!}" class="form-horizontal">

            <table class="table pull-right">
                <tr>
                    <td>{!!_('contract_name')!!}</td>
                    <td>
                        <input class="form-control" type="text" name="contract_name" value="{!!old('contract_name')!!}"/>
                    </td>
                    <td>{!!_('contract_no')!!}</td>
                    <td>
                        <input class="form-control" type="number" name="contract_no" value="{!!old('contract_no')!!}"/>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('start_letter_date')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="start_letter_date" value="{!!old('start_letter_date')!!}" readonly="readonly"/>
                    </td>
                    <td>{!!_('contract_completion_date')!!}</td>
                    <td>
                        <input class="datepicker_farsi form-control" id="contract_completion_date" type="text" name="contract_completion_date" value="{!!old('contract_completion_date')!!}" readonly="readonly"/>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('winner_bidder')!!}</td>
                    <td>
                        <input class="form-control" type="text" name="winner_bidder" value="{!!old('winner_bidder')!!}"/>
                    </td>
                    @if(@$total_contract_price == '')
                    <td>{!!_('total_contract_price')!!}</td>
                    <td>
                        <input type="number" class="form-control total_price" name="total_contract_price" value="{!!old('total_contract_price')!!}"/>
                    </td>
                    @else
                    <td>{!!_('total_contract_price')!!}</td>
                    <td>
                        <input type="number" class="form-control total_price" name="total_contract_price" value="{!!@$total_remained_contract_price!!}" readonly="" />
                    </td>
                    @endif
                </tr>
                <tr>
                <tr>
                    <td>{!!_('request')!!}</td>
                    <td>
                        <input type="number" class="form-control" name="request" value="{!!old('request')!!}" />
                    </td>
                    <td>{!!_('request_date')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="request_date" value="{!!old('request_date')!!}" readonly="readonly"/>
                    </td>
                </tr>
                <tr>
                <tr>
                    <td>{!!_('date_of_preparation_of_m3')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_preparation_of_m3" value="{!!old('date_of_preparation_of_m3')!!}" readonly="readonly"/>
                    </td>
                    <td>{!!_('m3_no')!!} :</td>
                    <td>
                        <input type="number" class="form-control" name="m3_no" value="{!!old('m3_no')!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('total_m3_price')!!}</td>
                    <td>
                        <input type="number" class="form-control" id="m3_total_amount" name="total_m3_price" value="{!!old('total_m3_price')!!}" />
                    </td>
                    <td>{!!_('date_of_m3')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_m3" value="{!!old('date_of_m3')!!}" readonly="readonly"/>
                    </td>
                </tr>
                <tr>
                <tr>
                    <td>{!!_('m3_no_to_stock')!!}</td>
                    <td>
                        <input type="number" class="form-control" name="m3_no_to_stock" value="{!!old('m3_no_to_stock')!!}" />
                    </td>
                    <td>{!!_('date_of_m3_to_stock')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_m3_to_stock" value="{!!old('date_of_m3_to_stock')!!}" readonly="readonly"/>
                    </td>
                </tr>
                <tr>
                	<td>{!!_('contract_modification_price')!!}</td>
                    <td>
                        <input type="text" class="form-control" name="contract_modification_price" value="{!!old('contract_modification_price')!!}" readonly="" />
                    </td>
                    <td>{!!_('total_remained_contract_price')!!}</td>
                    <td>
                        <input type="number" class="form-control total_remaining_price" name="total_remained_contract_price" value="{!!@$total_remained_contract_price!!}" readonly="" />
                    </td>
                </tr>
                <tr>
                	<td>{!!_('remained_contract_time')!!}</td>
                    <td>
                        <input type="text" class="form-control" id="remained_contract_time" name="remained_contract_time" value="{!!old('remained_contract_time')!!}" readonly="" />
                    </td>
                    <td>{!!_('date_of_contract_termination')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_contract_termination" value="{!!old('date_of_contract_termination')!!}" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                	<td>{!!_('termination_reason')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" name="termination_reason">{!!old('termination_reason')!!}</textarea>
                    </td>
                    <td>{!!_('remarks')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" name="remarks">{!!old('remarks')!!}</textarea>
                    </td>
                </tr>
                <input type="hidden" name="procurement_id" value="{!!@$id!!}">
                <input type="hidden" id="g_contract_completion_date" value="">
            </table>
            <div class="form-group">
                <div class="col-sm-12">
                    <hr style="border: 1px dashed #b6b6b6" />
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-check"></i>
                        </span>
                        &nbsp;{!!_('submit')!!}
                    </button>
                    <a href="{!! URL::route('contractsList') !!}" class="btn btn-danger">
                        <span>
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>
                        &nbsp;{!!_('cancel')!!}
                    </a>
                </div>
            </div>
        </form>
    
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     
        
        var total_price = parseInt($(".total_price").val());
        var m3_total_amount = $("#m3_total_amount").val();
        if(m3_total_amount > total_price){
            alert('لطفآ قیمت مجموعی م ۳ را از قیمت مجموعی قرار داد کرده زیاد تر ننویسید !');
        }
        else{
            var total_remaining_price = total_price - m3_total_amount;
            $(".total_remaining_price").val(total_remaining_price); 
        }

    });

    $("#contract_completion_date").change(function(){
        var selected_date = $("#contract_completion_date").val(); 

        $.ajax({
            type : 'post',
            url : "{!!URL::route('changeDate')!!}",
            data : "date="+selected_date,
            success : function(response){

                $("#g_contract_completion_date").val(response);
                // find the contract remained time through jquery.
                var contract_completion_date = new Date($('#g_contract_completion_date').val());
                var current_date = new Date("<?=date('Y-m-d');?>");
                var timeDiff = Math.abs(contract_completion_date.getTime() - current_date.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
                if(diffDays == 1 || diffDays == 0){
                    diffDays = diffDays+" Day";
                }
                else{
                    diffDays = diffDays+" Days";
                }
                $('#remained_contract_time').val(diffDays);
            }
        });

    });

    $("#m3_total_amount").blur(function(){
        var total_price = parseInt($(".total_price").val());
        var m3_total_amount = $("#m3_total_amount").val();
        if(m3_total_amount > total_price){
            alert('لطفآ قیمت مجموعی م ۳ را از قیمت مجموعی قرار داد کرده زیاد تر ننویسید !');
            $("#m3_total_amount").focus();
        }
        else{
            var total_remaining_price = total_price - m3_total_amount;
            $(".total_remaining_price").val(total_remaining_price); 
        }
    });


</script>

@stop