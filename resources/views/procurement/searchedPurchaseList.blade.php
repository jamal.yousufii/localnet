	
    <div style="padding:15px" class="table-responsive" id="search_result">
        @if(goodsRoleCheck('procurement_purchase_list'))
        <table class="table table-bordered table-responsive" id="purchaseList">
            <thead>
              <tr>
                <th>{!!_('no#')!!}</th>
                <th>{!!_('date_of_preparation')!!}</th>
                <th>{!!_('end_user')!!}</th>
                <th>{!!_('goods_detail')!!}</th>
                <th>{!!_('request_number')!!}</th>
                <th>{!!_('date_of_submission_to_the_purchasing_committee')!!}</th>
                <th>{!!_('lowest_price')!!}</th>
                <th>{!!_('purchasing_team')!!}</th>
                <th>{!!_('winner')!!}</th>
                <th>{!!_('m3_no')!!}</th>
                <th>{!!_('date_of_submission_to_the_finance_department')!!}</th>
                <th>{!!_('m3_no_sent_to_finance')!!}</th>
                <th>{!!_('amount_sent_to_finance')!!}</th>

                <th>{!!_('operations')!!}</th>
                
              </tr>
            </thead>

            <tbody>
            </tbody>
        </table>
        @else
        <table class="table table-bordered table-responsive" id="purchaseList">
            <thead>
              <tr>
                <th>{!!_('no#')!!}</th>
                <th>{!!_('date_of_preparation')!!}</th>
                <th>{!!_('end_user')!!}</th>
                <th>{!!_('goods_detail')!!}</th>
                <th>{!!_('request_number')!!}</th>
                <th>{!!_('purchasing_team')!!}</th>
                <th>{!!_('lowest_price')!!}</th>
                <th>{!!_('winner')!!}</th>
                <th>{!!_('m3_no')!!}</th>
                <th>{!!_('no_of_letter_sent_to_stock')!!}</th>

                <th>{!!_('operations')!!}</th>
                
              </tr>
            </thead>

            <tbody>
            </tbody>
        </table>
        @endif
        @if(purchaseRoleCheck('procurement_purchase_list'))
        <div style="padding-top:60px">
            <a class="btn btn-success pull-left" href="{!!URL::route('getLoadPurchaseForm')!!}">
                <i class="fa fa-plus-circle fa-lg"></i>
                {!!_('add_purchase')!!}
            </a>
        </div>
        @endif
    </div>

<script type="text/javascript">
	$(document).ready(function() {
	    $('#purchaseList').dataTable(
	        {
	        	//"order" : [[1, 'desc']],
	            "sDom": 'Tlfr<"clearfix">tip',
	            //"sDom": 'lrf<"clear spacer">Ttip',
	            "oTableTools": {
	                "aButtons": [
	                    {
	                        "sExtends": "print",
	                        "bShowAll": true
	                    }
	                ]
	            },
	            "bProcessing": false,
	            "bServerSide": false,
	            "bDeferRender": true,
	            "iDisplayLength": 10,
	            // "bSort" : false,
	            "sAjaxSource": "{!!URL::route('getSearchPurchases', $purchasing_team)!!}"
	        }
	    );
		$("#purchaseList th").css({'font-size':'3px','font-weight':'normal'});
	    $("#purchaseList td").css({'font-size':'2px'});
	    $("#ToolTables_purchaseList_0").html("{!!_('print_view')!!}");
	    $("#ToolTables_purchaseList_0").addClass('btn btn-success');
	    $("#ToolTables_purchaseList_0").click(function(){
	        $("#purchaseList td:last-child").hide();
	        $("#purchaseList th:last-child").hide();
	        $("#purchaseList th").removeClass('sorting');
	        $("#purchaseList th").css({'font-size':'10px','font-weight':'bold'});
	        $("#purchaseList td").css({'font-size':'10px'});
	    });
	
	});

	$(document).keyup(function(e) {
	    if (e.keyCode == 27) { // escape key maps to keycode `27`
	        $("#purchaseList td:last-child").show();
		    $("#purchaseList th:last-child").show();
		    $("#purchaseList th").addClass('sorting');
		    $("#purchaseList th").css({'font-size':'11px','font-weight':'normal'});
		    $("#purchaseList td").css({'font-size':'10px'});
	    }
	});
	
	//get the purchase details onchange of purchasing team dropdown.
	$("#purchasing_team").change(function(){
	
		var purchasing_team = $(this).val();
	    $.ajax({
	            url: "{!!URL::route('getSearchPurchases')!!}",
	            data: '&purchasing_team='+purchasing_team,
	            type: 'post',
	            beforeSend: function(){
	                $("#search_result").html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	            },
	            success: function(response)
	            {
	                $("#search_result").html(response);
	            }
	        }
	    );
	
	});

</script>


