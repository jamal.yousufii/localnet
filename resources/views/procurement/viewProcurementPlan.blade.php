@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('procurement_plan_details')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
        span.short{
            display: inline-block;
            width: 100px !important;
        }
        td{
            font-size: 0.875em !important;
            padding: 12px 8px !important;
            vertical-align: middle !important;
            font-weight: 300 !important;
        }
    </style>

@stop

@section('content')

@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('procurementPlanList') !!}">{!!_('procurement_plan_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('procurement_plan_details')!!}</span>
                    </li>
                </ol>
                <h3>{!!_('procurement_plan_detailed_information')!!}</h3>
            </div>
            <div class="pull-right" style="margin-right:50px">
            	<a href="javascript:history.back()" class="btn btn-success">{!!_('go_back')!!}</a>
            </div>
        </div>
    </div>

    <div class="container">
    @foreach($procurement_plan as $item)
        <form role="form" method="post" action="{!! URL::route('postEditProcurement') !!}" class="form-horizontal">

            <table class="table pull-right">
                <tr>
                    <td>{!!_('sector')!!}</td>
                    <td>
                        <input class="form-control" type="text" value="{!!$item->sector!!}" disabled />
                    </td>
                    <td>{!!_('procurement_entity')!!}</td>
                    <td>
                        <input class="form-control" type="text" value="{!!$item->procurement_entity!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('budget_type')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->budget_type!!}" disabled />
                    </td>
                    <td>{!!_('budget_code')!!}</td>
                    <td>
                        <input class="form-control" type="number" value="{!!$item->budget_code!!}" disabled />
                    </td>
                </tr>
                <tr>
                	<td>{!!_('year')!!}</td>
                    <td>
                        <input class="form-control" type="text" value="{!!$item->year!!}" disabled />
                    </td>
                    <td>{!!_('name_of_project_in_approved_budget_plan')!!}</td>
                    <td>
                        <input class="form-control" type="text" value="{!!$item->name_of_project_in_approved_budget_plan!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('donor')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->donor!!}" disabled />
                    </td>
                	<td>{!!_('procurement_description')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" disabled>{!!$item->procurement_description!!}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('type_of_contract')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->type_of_contract!!}" disabled />
                    </td>
                    <td>{!!_('procurement_preference_from_national_resources')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" value="{!!$item->procurement_preference_from_national_resources!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('contract_number')!!}</td>
                    <td>
                        <input type="number" step="any" class="form-control" value="{!!$item->contract_number!!}" disabled />
                    </td>
                    <td>{!!_('date_of_budget_agreement_by_donor_or_mof')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" value="{!!checkEmptyDate($item->date_of_budget_agreement_by_donor_or_mof)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('procurement_method')!!}</td>
                    <td>
                        <select class="form-control" disabled>
                            {!!getProcMethod($item->procurement_method);!!}
                        </select>
                    </td>
                    <td>{!!_('estimated_cost_of_the_project_in_afg')!!}</td>
                    <td>
                        <input type="number" step="any" class="form-control" value="{!!$item->estimated_cost_of_the_project_in_afg!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('estimated_date_of_procurement_initiation')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" value="{!!checkEmptyDate($item->estimated_date_of_procurement_initiation)!!}" disabled />
                    </td>
                    <td>{!!_('estimated_date_of_project_announcement')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" value="{!!checkEmptyDate($item->estimated_date_of_project_announcement)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('estimated_date_of_bid_opening')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" value="{!!checkEmptyDate($item->estimated_date_of_bid_opening)!!}" disabled />
                    </td>
                    <td>{!!_('date_of_bid_evaluation')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" value="{!!checkEmptyDate($item->date_of_bid_evaluation)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_completion_of_the_bid_evaluation')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" value="{!!checkEmptyDate($item->date_of_completion_of_the_bid_evaluation)!!}" disabled />
                    </td>
                    <td>{!!_('date_of_the_evaluation_report_submission')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" value="{!!checkEmptyDate($item->date_of_the_evaluation_report_submission)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('estimated_date_of_the_public_award')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" value="{!!checkEmptyDate($item->estimated_date_of_the_public_award)!!}" disabled />
                    </td>
                    <td>{!!_('estimation_date_of_contract_sign')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" value="{!!checkEmptyDate($item->estimation_date_of_contract_sign)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('guarantee_period_of_works')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->guarantee_period_of_works!!}" disabled />
                    </td>
                	<td>{!!_('estimation_date_of_contract_completion')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" value="{!!checkEmptyDate($item->estimation_date_of_contract_completion)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('remarks')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" disabled>{!!$item->remarks!!}</textarea>
                    </td>
                </tr>
    
            </table>
        </form>
    @endforeach
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     
    

    });


</script>

@stop