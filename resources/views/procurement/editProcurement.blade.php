@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('procurement_edit_form')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
        span.short{
            display: inline-block;
            width: 100px !important;
        }
        td{
            font-size: 0.875em !important;
            padding: 12px 8px !important;
            vertical-align: middle !important;
            font-weight: 300 !important;
        }
    </style>

@stop

@section('content')

@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('procurementList') !!}">{!!_('procurement_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('edit_procurement')!!}</span>
                    </li>
                </ol>
                <h3>{!!_('procurement_edit_form')!!}</h3>
            </div>
        </div>
    </div>

    <div class="container">
    @foreach($procurement as $item)

        <form role="form" method="post" action="{!! URL::route('postEditProcurement', array('id' => $item->id)) !!}" class="form-horizontal">

            <table class="table pull-right">
                
                <tr>
                    <td>{!!_('budget_code')!!}</td>
                    <td>
                        <input class="form-control" type="text" name="budget_code" value="{!!$item->budget_code!!}" />
                    </td>
                    <td>{!!_('estimation_cost')!!}</td>
                    <td>
                        <input class="form-control" type="number" name="estimation_cost" value="{!!$item->estimation_cost!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('procurement_details')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" name="procurement_details">{!!$item->procurement_details!!}</textarea>
                    </td>
                    <td>{!!_('end_user')!!}</td>
                    <td>
                        <select name="end_user" class="form-control">
                            {!!getSubAllDeps($item->end_user);!!}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('contract_number')!!}</td>
                    <td>
                        <input class="form-control" type="number" name="contract_number" value="{!!$item->contract_number!!}" />
                    </td>
                    <td>{!!_('procurement_type')!!}</td>
                    <td>
                        <select name="procurement_type" class="form-control">
                            {!!getProcType($item->procurement_type);!!}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('procurement_method')!!}</td>
                    <td>
                        <select name="procurement_method" class="form-control">
                            {!!getProcMethod($item->procurement_method);!!}
                        </select>
                    </td>
                    <td>{!!_('project_submission_date_to_procurement_entity')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="project_submission_date_to_procurement_entity" value="{!!checkEmptyDate($item->project_submission_date_to_procurement_entity)!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_procurement_approval')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_procurement_approval" value="{!!checkEmptyDate($item->date_of_procurement_approval)!!}" />
                    </td>
                    <td>{!!_('announcement_date')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="announcement_date" value="{!!checkEmptyDate($item->announcement_date)!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_pre_qualification_meeting')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_pre_qualification_meeting" value="{!!checkEmptyDate($item->date_of_pre_qualification_meeting)!!}" />
                    </td>
                    <td>{!!_('date_of_bid_opening')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_bid_opening" value="{!!checkEmptyDate($item->date_of_bid_opening)!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('evaluation_completion_date')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="evaluation_completion_date" value="{!!checkEmptyDate($item->evaluation_completion_date)!!}" />
                    </td>
                    <td>{!!_('date_of_procurement_award')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_procurement_award" value="{!!checkEmptyDate($item->date_of_procurement_award)!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_npc_approval')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_npc_approval" value="{!!checkEmptyDate($item->date_of_npc_approval)!!}" />
                    </td>
                    <td>{!!_('date_of_notice_of_procurement_award')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_notice_of_procurement_award" value="{!!checkEmptyDate($item->date_of_notice_of_procurement_award)!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('proc_start_letter_date')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="start_letter_date" value="{!!checkEmptyDate($item->start_letter_date)!!}" />
                    </td>
                    <td>{!!_('winner_bidder')!!}</td>
                    <td>
                        <input type="text" class="form-control" name="winner_bidder" value="{!!$item->winner_bidder!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('remarks')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" name="remarks">{!!$item->remarks!!}</textarea>
                    </td>
                </tr>
    
            </table>
            <div class="form-group">
                <div class="col-sm-12">
                    <hr style="border: 1px dashed #b6b6b6" />
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-check"></i>
                        </span>
                        &nbsp;{!!_('save_changes')!!}
                    </button>
                    <a href="{!! URL::route('procurementList') !!}" class="btn btn-danger">
                        <span>
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>
                        &nbsp;{!!_('cancel')!!}
                    </a>
                </div>
            </div>
        </form>
    @endforeach
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     
    

    });


</script>

@stop