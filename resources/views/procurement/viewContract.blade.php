@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('contract_details')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
        span.short{
            display: inline-block;
            width: 100px !important;
        }
        td{
            font-size: 0.875em !important;
            padding: 12px 8px !important;
            vertical-align: middle !important;
            font-weight: 300 !important;
        }
    </style>

@stop

@section('content')

@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('contractsList') !!}">{!!_('contracts_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('contract_details')!!}</span>
                    </li>
                </ol>
                <h3>{!!_('view_contract_details')!!}</h3>
            </div>
            <div class="pull-right" style="margin-right:50px">
            	<a href="javascript:history.back()" class="btn btn-success">{!!_('go_back')!!}</a>
            </div>
        </div>
    </div>
    <div class="container">
        @foreach($contract AS $item)
            <table class="table pull-right">
                <tr>
                    <td>{!!_('contract_name')!!}</td>
                    <td>
                        <input class="form-control" type="text" value="{!!$item->contract_name!!}" disabled />
                    </td>
                    <td>{!!_('contract_no')!!}</td>
                    <td>
                        <input class="form-control" type="text" value="{!!$item->contract_no!!}" disabled />
                    </td>
                </tr>
                <tr>
                <tr>
                    <td>{!!_('start_letter_date')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->start_letter_date)!!}" disabled/>
                    </td>
                    <td>{!!_('contract_completion_date')!!}</td>
                    <td>
                        <input class="form-control" type="text" id="contract_completion_date" value="{!!checkEmptyDate($item->contract_completion_date)!!}" disabled/>
                    </td>
                </tr>
                <tr>
                <tr>
                    <td>{!!_('winner_bidder')!!}</td>
                    <td>
                        <input class="form-control" type="text" value="{!!$item->winner_bidder!!}" disabled />
                    </td>
                    <td>{!!_('total_contract_price')!!}</td>
                    <td>
                        <input type="text" class="form-control total_price" value="{!!$item->total_contract_price!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('request')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->request!!}" disabled />
                    </td>
                    <td>{!!_('request_date')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->request_date)!!}" disabled/>
                    </td>
                </tr>
                <tr>
                <tr>
                    <td>{!!_('date_of_preparation_of_m3')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_preparation_of_m3)!!}" disabled/>
                    </td>
                    <td>{!!_('m3_no')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->m3_no!!}" disabled />
                    </td>
                </tr>
                <tr>
                <tr>
                    <td>{!!_('total_m3_price')!!}</td>
                    <td>
                        <input type="text" class="form-control" id="m3_total_amount" value="{!!$item->total_m3_price!!}" disabled />
                    </td>
                    <td>{!!_('date_of_m3')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptydate($item->date_of_m3)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('m3_no_to_stock')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->m3_no_to_stock!!}" disabled />
                    </td>
                    <td>{!!_('date_of_m3_to_stock')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_m3_to_stock)!!}" disabled/>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_submission_letter_to_finance_department')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_submission_letter_to_finance_department)!!}" disabled/>
                    </td>
                    <td>{!!_('delay')!!} :</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkDateDiff($item->delay)!!}" disabled />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('m3_number_sent_to_finance')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->m3_number_sent_to_finance!!}" disabled />
                    </td>
                    <td>{!!_('amount_sent_to_finance')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->amount_sent_to_finance!!}" disabled />
                    </td>
                </tr>
                <tr>
                	<td>{!!_('contract_modification_price')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!$item->contract_modification_price!!}" disabled />
                    </td>
                	<td>{!!_('total_remained_contract_price')!!}</td>
                    <td>
                        <input type="text" class="form-control total_remaining_price" value="{!!$item->total_remained_contract_price!!}" disabled />
                    </td>
                </tr>
                <tr>
                	<td>{!!_('remained_contract_time')!!}</td>
                    <td>
                        <input type="text" class="form-control" id="remained_contract_time" value="{!!checkDateDiff($item->remained_contract_time)!!}" disabled />
                    </td>
                	<td>{!!_('date_of_contract_termination')!!}</td>
                    <td>
                        <input type="text" class="form-control" value="{!!checkEmptyDate($item->date_of_contract_termination)!!}" disabled />
                    </td>
                </tr>
                <tr>
                	<td>{!!_('remained_time_of_performance_security')!!}</td>
                    <td>
                    	@if(checkDateDiff($item->remained_contract_time) != "")
                        <input type="text" class="form-control" value="{!!checkDateDiff($item->remained_contract_time)+28!!} Days" disabled />
                    	@else
                    	<input type="text" class="form-control" value="{!!checkDateDiff($item->remained_contract_time)!!}" disabled />
                    	@endif
                    </td>
                	<td>{!!_('termination_reason')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" disabled>{!!$item->termination_reason!!}</textarea>
                    </td>
                </tr>
                <tr>
                	<td>{!!_('remarks')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" disabled>{!!$item->remarks!!}</textarea>
                    </td>
                </tr>
                <input type="hidden" name="procurement_id" value="{!!@$id!!}">
            </table>
        @endforeach()
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     
    
        var total_price = $(".total_price").val();
        var m3_total_amount = $("#m3_total_amount").val();
        var total_remaining_price = total_price - m3_total_amount;
        $(".total_remaining_price").val(total_remaining_price); 

    });

    $("#m3_total_amount").blur(function(){
        var total_price = $(".total_price").val();
        var m3_total_amount = $("#m3_total_amount").val();
        var total_remaining_price = total_price - m3_total_amount;
        $(".total_remaining_price").val(total_remaining_price); 
    });

</script>

@stop