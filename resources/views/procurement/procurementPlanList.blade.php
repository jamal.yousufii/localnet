@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('procurement_plan_list')!!}</title>
    <style type="text/css">
        table th{
        	padding: 0px; !important;
        	font-size: 12px !important;
        	font-weight: bold !important;
        	color: #000 !important;
        	text-align: center;
        }
        table td{
        	padding: 0px; !important;
        	font-size: 12px !important;
        	color: #000 !important;
        	text-align: center;
        }table th:last-child, table td:last-child{
        	width: 7.1% !important;
        	border-right: 1px solid #E4EAEC !important;
        }
        a#ToolTables_procurementPlanList_0{
            padding: 5px 10px !important;
            text-decoration: none;
            font-weight: bold;
            margin-left: 10px;
            float: right;
        }
        a#ToolTables_procurementPlanList_0:hover{
            box-shadow: 0 0 5px #888;
            text-decoration: none;
        }
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop


@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success span6 noprint'>{!!Session::get('success')!!}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger span6 noprint'>{!!Session::get('fail')!!}</div>
    @endif
    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="form-group">
	                <div class="col-sm-3">
	                    <ol class="breadcrumb">
	                        <li>
	                            <a href="{!! URL::route('home') !!}">{!!_('Dashboard')!!}</a>
	                        </li>
	                        <li class="active">
	                            <span>{!!_('all_procurement_plans')!!}</span>
	                        </li>
	                    </ol>
	                    <h3>{!!_('procurement_plan_list')!!}</h3>
	            	</div>
	            	<div class="form-group">
		            	<div class="form-group" style="margin-top:20px">
			            	<div class="col-sm-4">
			            		<h5>{!!_('total_number_of_procurement_plan_exist')!!} : <span style="color:green;font-weight:bold">{!!getTotalNumberOfRecords('procurement_plan')!!}</span></h5>
			            	</div>
			            </div>
		            </div>
	            	@if(canAdd('procurement_plan_list'))
	            	<div class="col-sm-2 pull-right" style="margin-top:60px">
	            		<a class="btn btn-success pull-left" href="{!!URL::route('getLoadProcurementPlanForm')!!}">
		                    <i class="fa fa-plus-circle fa-lg"></i>
		                    {!!_('add_procurement_plan')!!}
		                </a>
	            	</div>
	            	@endif
                </div>
            </div>
        </div>
        <div style="padding:15px" class="table-responsive">
            <table class="table table-bordered table-responsive" id="procurementPlanList">
                <thead>
                  <tr>
                    <th>{!!_('No#')!!}</th>
                    <th>{!!_('sector')!!}</th>
                    <th>{!!_('procurement_entity')!!}</th>
                    <th>{!!_('budget_type')!!}</th>
                    <th>{!!_('budget_code')!!}</th>
                    <th>{!!_('year')!!}</th>
                    <!-- <th>{!!_('name_of_project_in_approved_budget_plan')!!}</th> -->
                    <th>{!!_('donor')!!}</th>
                    <th>{!!_('procurement_description')!!}</th>
                    <th>{!!_('type_of_contract')!!}</th>
                    <th>{!!_('contract_number')!!}</th>

                    <th>{!!_('operations')!!}</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#procurementPlanList').dataTable(
        {

            "sDom": 'Tlfr<"clearfix">tip',
            //"sDom": 'lrf<"clear spacer">Ttip',
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "print",
                        "bShowAll": true
                    }
                ]
            },
            "bProcessing": false,
            "bServerSide": false,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getAllProcurementPlan')!!}"
        }
    );
	$("#procurementPlanList th").css({'font-size':'11px','font-weight':'normal'});
    $("#procurementPlanList td").css({'font-size':'10px'});
    $("#ToolTables_procurementPlanList_0").html("{!!_('print_view')!!}");
    $("#ToolTables_procurementPlanList_0").addClass('btn btn-success');
    $("#ToolTables_procurementPlanList_0").click(function(){
        $("#procurementPlanList td:last-child").hide();
        $("#procurementPlanList th:last-child").hide();
        $("#procurementPlanList th").removeClass('sorting');
        $("#procurementPlanList th").css({'font-size':'10px','font-weight':'bold'});
        $("#procurementPlanList td").css({'font-size':'10px'});
    });

});
$(document).keyup(function(e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
        $("#procurementPlanList td:last-child").show();
	    $("#procurementPlanList th:last-child").show();
	    $("#procurementPlanList th").addClass('sorting');
	    $("#procurementPlanList th").css({'font-size':'11px','font-weight':'normal'});
	    $("#procurementPlanList td").css({'font-size':'10px'});
    }
});
</script>

@stop


