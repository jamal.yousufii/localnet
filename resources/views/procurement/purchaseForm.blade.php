@extends('layouts.master')

@section('head')
	@parent
	<title>{!!_('purchase_form')!!}</title>
    <style type="text/css">
        td.hide_border{
            border-top: none !important;
        }
        span.short{
            display: inline-block;
            width: 100px !important;
        }
        td{
            font-size: 0.875em !important;
            padding: 12px 8px !important;
            vertical-align: middle !important;
            font-weight: 300 !important;
        }
    </style>

@stop

@section('content')

@if(Session::has('failed'))
    <div class="alert alert-danger">
        <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
    </div>
@endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12">
        <div id="content-header" class="clearfix">
            <div class="pull-left">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!! URL::route('goodsPurchaseList') !!}">{!!_('purchase_list')!!}</a>
                    </li>
                    <li class="active">
                        <span>{!!_('add_purchase')!!}</span>
                    </li>
                </ol>
                <h3>{!!_('goods_purchase_form')!!}</h3>
            </div>
        </div>
    </div>

    <div class="container">

        <form role="form" method="post" action="{!! URL::route('postAddPurchase') !!}" class="form-horizontal">

            <table class="table pull-right">
                
                <tr>
                    <td>{!!_('date_of_preparation')!!}</td>
                    <td>
                        <input class="datepicker_farsi form-control" type="text" name="date_of_preparation" value="{!!old('date_of_preparation')!!}" readonly="" />
                    </td>
                    <td>{!!_('end_user')!!}</td>
                    <td>
                        <select name="end_user" class="form-control">
                            {!!getSubAllDeps();!!}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>{!!_('goods_detail')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" name="goods_detail">{!!old('goods_detail')!!}</textarea>
                    </td>
                    <td>{!!_('request_no')!!}</td>
                    <td>
                        <input type="number" class="form-control" name="request_no" value="{!!old('request_no')!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_arrival')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_arrival" value="{!!old('date_of_arrival')!!}" readonly="readonly"/>
                    </td>
                    <td>{!!_('date_of_submission_to_the_purchasing_committee')!!}</td>
                    <td>
                        <input class="datepicker_farsi form-control" type="text" name="date_of_submission_to_the_purchasing_committee" value="{!!old('date_of_submission_to_the_purchasing_committee')!!}" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td>{!!_('purchasing_team')!!}</td>
                    <td>
                        <input type="text" class="form-control" name="purchasing_team" value="{!!old('purchasing_team')!!}" />
                    </td>
                    <td>{!!_('lowest_price')!!}</td>
                    <td>
                        <input type="number" class="form-control" name="lowest_price" value="{!!old('lowest_price')!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('winner_company')!!}</td>
                    <td>
                        <input type="text" class="form-control" name="winner_company" value="{!!old('winner_company')!!}" />
                    </td>
                    <td>{!!_('m3_no')!!}</td>
                    <td>
                        <input type="number" class="form-control" name="m3_no" value="{!!old('m3_no')!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_m3')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_m3" value="{!!old('date_of_m3')!!}" readonly="readonly"/>
                    </td>
                    <td>{!!_('no_of_letter_sent_to_stock')!!}</td>
                    <td>
                        <input type="number" class="form-control" name="no_of_letter_sent_to_stock" value="{!!old('no_of_letter_sent_to_stock')!!}" />
                    </td>
                </tr>
                <tr>
                    <td>{!!_('date_of_submission_to_stock')!!}</td>
                    <td>
                        <input type="text" class="datepicker_farsi form-control" name="date_of_submission_to_stock" value="{!!old('date_of_submission_to_stock')!!}" readonly="readonly"/>
                    </td>
                    <td>{!!_('remarks')!!}</td>
                    <td>
                        <textarea cols="50" rows="3" class="form-control" name="remarks">{!!old('remarks')!!}</textarea>
                    </td>
                </tr>
    
            </table>
            <div class="form-group">
                <div class="col-sm-12">
                    <hr style="border: 1px dashed #b6b6b6" />
                    <button class="btn btn-primary" type="submit">
                        <span>
                            <i class="fa fa-check"></i>
                        </span>
                        &nbsp;{!!_('submit')!!}
                    </button>
                    <a href="{!! URL::route('goodsPurchaseList') !!}" class="btn btn-danger">
                        <span>
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>
                        &nbsp;{!!_('cancel')!!}
                    </a>
                </div>
            </div>
        </form>
    
    </div>

</div>

@stop

@section('footer-scripts')

<script type="text/javascript">

    $(function(){     
    

    });


</script>

@stop