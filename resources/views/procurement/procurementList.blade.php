@extends('layouts.master')

@section('head')
    @parent
    
    <title>{!!_('procurement_list')!!}</title>
    <style type="text/css">
        table th{
        	padding: 0px; !important;
        	font-size: 12px !important;
        	font-weight: bold !important;
        	color: #000 !important;
        	text-align: center;
        }
        table td{
        	padding: 0px; !important;
        	font-size: 12px !important;
        	color: #000 !important;
        	text-align: center;
        }table th:last-child, table td:last-child{
        	width: 7.1% !important;
        	border-right: 1px solid #E4EAEC !important;
        }
        a#ToolTables_procurementList_0{
            padding: 5px 10px !important;
            text-decoration: none;
            font-weight: bold;
            margin-left: 10px;
            float: right;
        }
        a#ToolTables_procurementList_0:hover{
            box-shadow: 0 0 5px #888;
            text-decoration: none;
        }
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop


@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success span6 noprint'>{!!Session::get('success')!!}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger span6 noprint'>{!!Session::get('fail')!!}</div>
    @endif
    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="form-group">
	                <div class="col-sm-4">
	                    <ol class="breadcrumb">
	                        <li>
	                            <a href="{!! URL::route('home') !!}">{!!_('Dashboard')!!}</a>
	                        </li>
	                        <li class="active">
	                            <span>{!!_('all_procurement')!!}</span>
	                        </li>
	                    </ol>
	                    <h3>{!!_('procurement_list')!!}</h3>
	            	</div>
	            	<div class="form-group">
		            	<div class="form-group" style="margin-top:20px">
			            	<div class="col-sm-4">
			            		<h5>{!!_('total_number_of_procurements_exist')!!} : <span style="color:green;font-weight:bold">{!!getTotalNumberOfRecords('procurement')!!}</span></h5>
			            	</div>
			            	<div class="col-sm-4">
			            		<h5>{!!_('total_archived_procurements')!!} : <span style="color:green;font-weight:bold">{!!getTotalNumberOfRecords('procurement',1)!!}</span></h5>
			            	</div>
			            </div>
		            </div>
                </div>
            </div>
        </div>
        <div style="padding:15px" class="table-responsive">
            <table class="table table-bordered table-responsive" id="procurementList">
                <thead>
                  <tr>
                    <th>{!!_('No#')!!}</th>
                    <th>{!!_('details')!!}</th>
                    <th>{!!_('end_user')!!}</th>
                    <th>{!!_('method')!!}</th>
                    <th>{!!_('announcement')!!}</th>
                    <th>{!!_('pre_qualification_meeting')!!}</th>
                    <th>{!!_('bid_opening')!!}</th>
                    <th>{!!_('evaluation_completion_date')!!}</th>
                    <th>{!!_('procurement_award')!!}</th>
                    <th>{!!_('npc_approval')!!}</th>
                    <th>{!!_('notice_of_procurement_award')!!}</th>
                    <th>{!!_('proc_start_letter_date')!!}</th>
                    <th>{!!_('winnder')!!}</th>

                    <th>{!!_('operations')!!}</th>
                    
                  </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
            <!--@if(canEdit('procurement_proc_list'))
	        <div style="margin-top: 60px">
                <a class="btn btn-success pull-left" href="{!!URL::route('getLoadProcurementForm')!!}">
                    <i class="fa fa-plus-circle fa-lg"></i>
                    {!!_('add_procurement')!!}
                </a>
	        </div>
	        @endif -->
        </div>
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $('#procurementList').dataTable(
        {
            "sDom": 'Tlfr<"clearfix">tip',
            //"sDom": 'lrf<"clear spacer">Ttip',
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "print",
                        "bShowAll": true
                    }
                ]
            },
            "bProcessing": false,
            "bServerSide": false,
            "bDeferRender": true,
            "iDisplayLength": 10,
            "sAjaxSource": "{!!URL::route('getAllProcurement')!!}"
        }
    );
	$("#procurementList th").css({'font-size':'11px','font-weight':'normal'});
    $("#procurementList td").css({'font-size':'10px'});
    $("#ToolTables_procurementList_0").html("{!!_('print_view')!!}");
    $("#ToolTables_procurementList_0").addClass('btn btn-success');
    $("#ToolTables_procurementList_0").click(function(){
        $("#procurementList td:last-child").hide();
        $("#procurementList th:last-child").hide();
        $("#procurementList th").removeClass('sorting');
        $("#procurementList th").css({'font-size':'10px','font-weight':'bold'});
        $("#procurementList td").css({'font-size':'10px'});
    });

});
$(document).keyup(function(e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
        $("#procurementList td:last-child").show();
	    $("#procurementList th:last-child").show();
	    $("#procurementList th").addClass('sorting');
	    $("#procurementList th").css({'font-size':'11px','font-weight':'normal'});
	    $("#procurementList td").css({'font-size':'10px'});
    }
});

	// Delete Record ajax script;
    function archiveContract(record_id) 
    {
        var ID = record_id;
        //alert(record_id);die();
        var dataString = "record_id="+ID;
        if(confirm("Are you sure you want to ARCHIVE this contract ?"))
        {
            $.ajax({
                 type: "POST",
                 url: "{!!URL::route('archiveContract')!!}",
                 data: dataString,
                 cache: false,
                 success: function(mydata){
                 	$("#success").html('Contract Successfully Archived');
                    window.setTimeout(function(){
						window.location.replace("{!! URL::route('procurementList') !!}");}, 1000);
                 }
            });
        }
        return false;
    }

</script>

@stop


