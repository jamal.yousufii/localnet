@extends('layouts.master')
@section('content')

<style type="text/css">
 table,tr, td {
    text-align: start;
 ;
  }
table,thead,tr,th{
  text-align: center;

}
table,thead,tr,th{
  text-align: center;

}
tr:nth-child(even) {background-color: #f2f2f2}
.btn-group *{
  direction: rtl;
  text-align:right;

}
/* .btn-group .dropdown-menu>li>a {
  padding-right: 0;
} */




</style>
<style type="text/css">
#myBtn {
    display: none;
    position: fixed;
    bottom: 10px;
    right: 30px;
    z-index: 99;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 15px;


}
ul#ul_tabs{
  padding: 0px;
}
#ul_tabs>li{
  margin-right: 10px;
}
ul#ul_tabs .tab a{
  padding:10px 10px;
  float: right;
}


#sub_tabs>li.active>a, #sub_tabs>li.active>a:focus, #sub_tabs>li.active>a:hover{
  background-color: #20343e !important;
}
.nav-tabs > li {
  float: right;
}
</style>
<a onclick="topFunction()" id="myBtn">{!! HTML::image('/img/t.png', 'Logo', array('class' => 'normal-logo logo-white', 'width' => '45px')) !!}</a>
<script type="text/javascript">
  window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera
    document.documentElement.scrollTop = 0; // For IE and Firefox
}
</script>
<div class="container" dir="rtl">

  <div class="page-head">
    <h3> مدیریت اجرائیه
 </h3>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
      <script type="text/javascript">
          $("ul#ul_tabs li:first-child").removeClass("active");
          $("ul#ul_tabs li:nth-child(2)").addClass("active");
      </script>
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif

    <!-- Single button -->

      <br>
      <div class="row" style=" padding: 0 0px 15px 0px; margin:0;">
        @if (canAddDepartment('document_management_list'))
          <button class="btn btn-info " type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="float:left;">
            اضافه نمودن ریاست
          </button>
        @endif
        <div class="collapse" id="collapseExample" style="margin-top: 50px;">
          <div class="well">
            <div class="row">
              <form action="{{route('insert_department')}}" method="POST">

                <div class="form-group">
                  <label for="department">ریاست دری</label>
                  <input type="text" name="name" placeholder="ریاست دری"  class="form-control" id="department" required>
                </div>
                <div class="form-group">
                  <label for="pwd">ریاست انگلیسی</label>
                  <input type="text" name="name_en" placeholder="ریاست انگلیسی" class="form-control" id="pwd">
                </div>
                {!!Form::token()!!}

                <button type="submit" class="btn btn-primary">ثبت</button>
              </form>
            </div><!-- /.row -->
          </div>
        </div>
      </div>
      <ul class="nav nav-tabs" id="ul_tabs" style="border-top: 1px solid lightgray; border-radius: ; height: 46px;"  >
         <li class="tab"  id="get_hakom">
          <a  href="#tab1" data-toggle="tab" style=" font-weight: bold;">احکام</a>
        </li>
        <li class="tab" id="farman">
               <a href="#tab2" data-toggle="tab" style="font-weight: bold;">فرامین</a>
        </li>

         <li  class="tab" id="get_maktob">
          <a href="#tab3" data-toggle="tab" ><b style="font-weight: bold;">مکاتب</b></a>
        </li>



       <!--   <li class="tab" id="get_maktab_warada"  >
        <a href="#tab12" data-toggle="tab" style="font-weight: bold;">مکاتب وارده</a>
      </li> -->

         <li class="tab" id="pashnahad" >
          <a href="#tab4" data-toggle="tab" style="font-weight: bold;">پشنهادات</a>
        </li>
‍ ‍‍‍        <li class="tab"  id="istalam">
          <a href="#tab5" data-toggle="tab" style="font-weight: bold;">استعلام ها</a>
        </li>

‍ ‍‍‍        <li class="tab" id="feceno">
          <a href="#tab6" data-toggle="tab" style="font-weight: bold;">ف س ۹</a>
        </li>

        <li class="tab" id="get_copy3" >
         <a href="#tab7" data-toggle="tab"><b style="font-weight: bold;">کاپی سوم</b></a>
        </li>
        <li class="tab" id="mosawaibat" >
         <a href="#tab8" data-toggle="tab"><b style="font-weight: bold;">مصوبات</b></a>
        </li>
<!-- {{-- ‍ ‍‍‍        <li class="tab" id="istalam_hidayati" >
          <a href="#tab13" data-toggle="tab" style="font-weight: bold;">استعلام هدایتی </a>
         </li> --}} -->
‍ ‍‍‍     <!--    <li class="tab" id="maktob_hedayati">
                <a href="#tab7" data-toggle="tab" style="font-weight: bold;">مکتوب هدایتی </a>
              </li> -->
<!-- {{-- ‍ ‍‍‍        <li class="tab" id="pashnahad_hadayati">
          <a href="#tab8" data-toggle="tab" style="font-weight: bold;">پیشنهاد هدایتی</a>
        </li> --}} -->

‍ ‍‍‍

‍ ‍‍‍        <li class="tab" id="tayenat">
          <a href="#tab9" data-toggle="tab" style="font-weight: bold;">تعيينات</a>
        </li>

‍ ‍‍‍        <li class="tab" id="waraq_darkhasti">
          <a href="#tab10" data-toggle="tab" style="font-weight: bold;">ورقه درخواستی</a>
         </li>
‍ ‍‍‍        <li class="tab" id="hadayat">
          <a href="#tab16" data-toggle="tab" style="font-weight: bold;">هدایات</a>
         </li>


        <li class="tab" id="employees">
          <a href="#tab11" data-toggle="tab" style="font-weight: bold;">کارمندان</a>
        </li>

        <li>

          <!-- Single button -->

      <div class="dropdown" style="padding-top:5px;">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <i class="fa fa-search"></i>
          <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="text-align: right; right: 0; margin-top: 5px;">
          <li id="all_doc_search">
          <a href="#tab15" data-toggle="tab" style="font-weight: bold;">جستجو</a>
         </li>
      ‍ ‍‍‍    <li id="search_yearly_doc">
          <a href="#tab12" data-toggle="tab" style="font-weight: bold;">جستجو سالانه</a>
          </li>
        </ul>
      </div>
        </li>
      </ul>
      <br>

         <div class="tab-content" id="data"  >

        <!-- tabs contents -->

         </div>

          </div>
         </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:left;"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel" style="float:right">تغیر در نتیجه</h4>
              </div>
              <div class="modal-body" dir="rtl">
                <form action="{!!URL::route('update_result')!!}" method="post">
                  <div class="form-group">
                    <input type="hidden" class="get_id" name="get_id" >
                    <select name="result"  id="result" class="form-control" style="width: 100%" required >
                         <option value="" >- -  نتیجه   - -</option>
                         <option value="1">اجراه</option>
                         <option value="2">نااجراه </option>
                         <option value="3">حفظیه </option>

                       </select>
                  </div>
                  <div class="form-group">
                    <textarea class="form-control desc" name="result_remark" placeholder=" توضیحات"> </textarea>

                </div>
                {!!Form::token()!!}


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
                <input type="submit" class="btn btn-primary" value="تفیر">
              </div>
              </form>
            </div>
          </div>
        </div>

        {{-- End model --}}

 @stop

@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
    $(".tab").click(function () {
        if(!$(this).hasClass('active'))
        {
            $(".tab.active").removeClass("active");
            $(this).addClass("active");
        }
    });
});
</script>

<!-- TAB ACTIVATION  -->
<script type="text/javascript">
$(document).ready(function() {
  if(window.location.hash == "")
  {
    $("#get_hakom").load( "li", function(evt) {
     $.ajax({
      type:'get',
      url:'{!!URL::route("get_hakom_data_doc")!!}',
      success:function(data){
         $('#data').html(data);


    }
     })

    return false;
   });
  }
});

  $('#ul_tabs a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
  });

// store the currently selected tab in the hash value
$("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
  var id = $(e.target).attr("href").substr(1);
  window.location.hash = id;

  if(id == "tab1")
  {

    $("#get_hakom").load( "li", function(evt) {
     $.ajax({
      type:'get',
      url:'{!!URL::route("get_hakom_data_doc")!!}',
      success:function(data){
         $('#data').html(data);


    }
     })

    return false;
   });
  }else if(id == "tab2"){
  $("#farman").load( "li", function(evt) {
     $.ajax({
      type:'get',
      url:'{!!URL::route("get_farman_data_doc")!!}',
      success:function(data){
         $('#data').html(data);

    }
     })

    return false;
   });
  }else if(id == "tab3")
  {
    $("#get_maktob").load( "li", function(evt) {
     $.ajax({
      type:'get',
      url:'{!!URL::route("get_maktob_data_doc")!!}',
      success:function(data){
         $('#data').html(data);

    }
     })

    return false;
   });

}else if(id == "tab4"){
  $("#pashnahad").load( "li", function(evt) {
     $.ajax({
      type:'get',
      url:'{!!URL::route("get_pashnehad_data_doc")!!}',
      success:function(data){
         $('#data').html(data);

    }
     })

    return false;
   });

  }else if(id == "tab5"){

  $("#istalam").load( "li", function(evt) {
     $.ajax({
        type:'get',
        url:'{!!URL::route("get_istalam_data_doc")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });
  }else if(id == "tab6"){
  $("#feceno").load( "li", function(evt) {
     $.ajax({
        type:'get',
        url:'{!!URL::route("get_feceno_data_doc")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });
  }else if(id == "tab7"){

    $("#get_copy3").load( "li", function(evt) {
       $.ajax({
        type:'get',
        url:'{!!URL::route("get_copy_som_data_doc")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });

  }else if(id == "tab8"){
    $("#mosawaibat").load( "li", function(evt) {
       $.ajax({
          type:'get',
          url:'{!!URL::route("get_mosawibat_doc")!!}',
          success:function(data){
             $('#data').html(data);

        }
         })

        return false;
       });
  }else if(id == "tab9"){
    $("#tayenat").load( "li", function(evt) {
       $.ajax({
          type:'get',
          url:'{!!URL::route("get_tayenat_doc")!!}',
          success:function(data){
             $('#data').html(data);

        }
         })

        return false;
       });
  }else if(id == "tab10"){

  $("#waraq_darkhasti").load( "li", function(evt) {
     $.ajax({
        type:'get',
        url:'{!!URL::route("get_waraq_darkhasti_doc")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });
  }else if(id == "tab16"){

  $("#hadayat").load( "li", function(evt) {
     $.ajax({
        type:'get',
        url:'{!!URL::route("get_hadayat_doc")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });

  }else if(id == "tab11"){
  $("#employees").load( "li", function(evt) {
     $.ajax({
        type:'get',
        url:'{!!URL::route("all_employees")!!}',
        success:function(data){
           $('#data').html(data);

      }
       })

      return false;
     });
  }

});
</script>
 <!-- END OF TAB ACTIVATION  -->

<script type="text/javascript">

   $(document).ready(function() {

  $("#get_hakom").click( "li", function(evt) {
     $.ajax({
            type:'get',
            url:'{!!URL::route("get_hakom_data_doc")!!}',
            success:function(data){
               $('#data').html(data);

          }
           })

          return false;
         });
  });

</script>
<script type="text/javascript">
//  $(window).load(function(){
//   $('#get_maktob').trigger('click');
// });
  $(document).ready(function() {

  $("#get_maktob").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_maktob_data_doc")!!}',
            success:function(data){

               $('#data').html(data);

                      }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#get_copy3").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_copy_som_data_doc")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script>
<!-- <script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#get_maktab_warada").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_maktob_warada_data_doc")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script> -->
 <script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#pashnahad").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_pashnehad_data_doc")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#farman").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_farman_data_doc")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#istalam").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_istalam_data_doc")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#feceno").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_feceno_data_doc")!!}',
            success:function(data){

               $('#data').html(data);              }
           })
          return false;
         });
  });
</script>

<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#waraq_darkhasti").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_waraq_darkhasti_doc")!!}',
            success:function(data){

               $('#data').html(data);
              }
           })
          return false;
         });
  });
</script>



<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#hadayat").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_hadayat_doc")!!}',
            success:function(data){

               $('#data').html(data);
                   }
           })
          return false;
         });
  });
</script>

<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#mosawaibat").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("get_mosawibat_doc")!!}',
            success:function(data){

               $('#data').html(data);
                   }
           })
          return false;
         });
  });
</script>

<!-- search all script -->
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#all_doc_search").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("all_doc_search")!!}',

            success:function(data){

               $('#data').html(data);
                   }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#search_yearly_doc").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("search_yearly_doc")!!}',

            success:function(data){

               $('#data').html(data);

               $( "#fromdate" ).persianDatepicker();
               $( "#todate" ).persianDatepicker();
                $("#department_select").select2();

                   }
           })
          return false;
         });
  });
</script>
<script type="text/javascript">
 //$(window).load(function(){
 //  $('#tab1').trigger('click');
//});
  $(document).ready(function() {

  $("#employees").click( "li", function(evt) {

     $.ajax({
            type:'get',
            url:'{!!URL::route("all_employees")!!}',

            success:function(data){

               $('#data').html(data);
                   }
           })
          return false;
         });
  });

</script>





<script type="text/javascript">
$(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
        $('#ul_tabs a[href="' + activeTab + '"]').tab('show');
    }
});
</script>


<script type="text/javascript">

$('.dropdown-toggle').dropdown()
function my_modal(id,status,description) {
  // get current maktoob data from

  // pass the data to the modal
  // edit the modal
  // store the updated data into maktoob with id => param id
  // console.log($('#myModal'));
  $('.desc').val(description);
  $('.get_id').val(id);
  // $('#result').val(status);
  $('#result').find($('option[value='+status+']')).attr('selected','selected');
  $('#myModal').on('shown.bs.modal', function () {
    $('#myInput').focus()
  })
}
</script>

@stop
