@extends('layouts.print_master')
@section('content')
<style type="text/css">
@media print{
  .no-print ,no-print *{
    display: none !important;
  }
}
</style>

<div class="container" dir="rtl" style="  padding-bottom: 20px; margin-top: -50px;">

              <div class="content" style="text-align: center;   ">

                   @if(!empty($row))
                 @foreach($row as $val)

                  <br><br>
                  <a href="{{asset('uploads_doc/'.getMyDepartmentId().'/'.$val->file_name)}}" download style="color: green" class="no-print"><b>
                  <i class='fa fa-download fa-4x text-success'></i></b></a> &nbsp;  &nbsp; &nbsp;
                  <img src="{{asset('uploads_doc/'.getMyDepartmentId().'/'.$val->file_name)}}"  width= "595px">
                  @endforeach
                  @else
               <td style="color:red ; text-align: center;" colspan="12"> موردی پیدا نشد !</td>
                    @endif

             </div>
                    <!--     <?php // echo "Today is " . checkEmptyDate(date("Y-m-d")) . "<br>"; ?> -->
                    <div class="row">
                      <div class="col-md-12 text-center">
                        <br>
                        <a onclick="javascript:window.print();" id="print-btn"  class="btn btn-success btn-large no-print" >Print
                        <i class="fa fa-print"></i></a>
                        <a class="no-print" " href="{!!URL::route('document_recordsList')!!}">
                        <input type="button" value="لیست اطلاعات" id="add_department" class="btn btn-danger no-print"/></a>
                      </div>
                  </div>
</div>

    @stop

@section('footer-scripts')

@stop
