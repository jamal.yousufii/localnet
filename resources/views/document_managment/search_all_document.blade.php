            @if(!empty($records))
                   <?php $counter = 1; ?>
                 @foreach($records as $val)
                  <tr>
                  <td>{!! $counter  !!}</td>
                  <td>{{$val->number_documents =='0' ? ' ':$val->number_documents}}</td>
                  <td><?php if($val->document_type=='1'){ echo "حکم";}
                  elseif($val->document_type=='2') {echo "مکتوب";}
                  elseif($val->document_type=='3') {echo "پشنهاد";}
                  elseif($val->document_type=='4') {echo "فرمان";}
                  elseif($val->document_type=='5') {echo "استعلام";}
                  elseif($val->document_type=='6') {echo "ف س ۹";}
                  // elseif($val->document_type=='7') {echo "مکتوب هدایتی";}
                  elseif($val->document_type=='8') {echo "پیشنهاد هدایتی";}
                  elseif($val->document_type=='9') {echo "ف س ۹ هدایتی";}
                  elseif($val->document_type=='10') {echo "تعیینات";}
                  // elseif($val->document_type=='11') {echo " مکتوب وارده";}
                  elseif($val->document_type=='12') {echo "استعلام هدایتی";}
                  elseif($val->document_type=='13') {echo "کاپی سوم";}
                  elseif($val->document_type=='14') {echo "ورقه درخواستی";}
                  elseif($val->document_type=='15') {echo "مصوبات";}
                  elseif($val->document_type=='16') {echo "هدایت";}
                   ?></td>
                  <td>{!! $val-> name !!}</td>
                  <td>{!! $val-> head_of_office !!}</td>
                  <td>{!!checkEmptyDate($val->date)!!}</td>
                  <td><?php if($val->type_id==1){ echo "صادره";}elseif($val->type_id==2){echo "وارده";}else{echo "";} ?> </td>

                  <td><a href="{!!URL::route('show_file_doc',$val->id)!!}" target="_blank" ><i class='fa-file-image-o fa-2x text-success'></i></a> </td>
                  <td>{!! $val->dept_name !!}</td>
                  <td>{!! $val->marji !!}</td>
                  <td>{!! $val->description !!}</td>
                  <td>{!! $val->related_to !!}</td>
                  <td>{!! $val->receiver !!}</td>
                  <td >
                     <span data-toggle="tooltip" title="{{$val->result_remark}}" >
                       <span onclick="my_modal({{$val->id}},'{{$val->result}}','{{$val->result_remark}}')"  data-toggle="modal" data-target="#myModal">

                      <?php if($val->result==1){ echo "<span class='label label-success'>اجراء</span>";}if($val->result==2){ echo "<span class='label label-danger'>نا اجراء</span>";}if($val->result==3){ echo "<span class='label label-primary'>حفظیه</span>";} ?>
                      </span>
                    </span>
                  </td>
                  <td>{!!checkEmptyDate($val->result_date)!!}</td>
                  <td>{!!$val->file_address!!}</td>
                  @if(canDelete('document_management_list'))
                  <td><a href="delete_row_doc/{{$val->id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                  @endif
                  @if(canEdit('document_management_list'))
                  <td><a href="<?php if ($val->document_type=='14'){echo "select_waraq_darkhasti_doc"; }elseif($val->document_type=='9' || $val->document_type=='12'){echo "select_doc_hadayat";}else{ echo "select_data_doc";}?>/{{$val->id}}" class="fa fa-edit"></a></td>
                  @endif



                  </tr>
                  <?php $counter++ ; ?>
                   @endforeach
                    @else
                    <tr>          <td colspan="20" style="color: red;text-align: center;"> موردی پیدا نشد !</td>
                    </tr>
                    @endif
