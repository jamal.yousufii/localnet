
                <table  class="table table-bordered table-responsive"  >

                  <thead>
                    <tr>
                      <th>شماره #</th>
                      <th>شماره مصوبات</th>
                      <th>تاریخ</th>
                      <th>فایل مصوبات</th>
                      <th>ریاست مربوطه</th>
                      <th>توضیحات</th>
                      <th>مرجع</th>
                      <th>نتیجه</th>
                      <th>موقیعت فایل</th>
                      <th colspan="2">عملیات</th>
                      </tr>
                 </thead>

               <tbody id="page_data">
                 @if(!empty($rows))
                   <?php $counter = $rows->firstItem(); ?>
                 @foreach($rows as $val)
                  <tr>
                  <td>{!! $counter  !!}</td>
                  <td>{!! $val-> number_documents !!}</td>
                  <td>{!!checkEmptyDate($val->date)!!}</td>
                  <td><a href="{!!URL::route('show_file_doc',$val->id)!!}"  target="_blank"><i class='fa-file-image-o fa-2x text-success'></i></a>
                   </td>
                  <td>{!! $val->dept_name !!}</td>
                  <td>{!! $val->description !!}</td>
                  <td>{!!$val->marji!!}</td>
                  <td >
                     <span data-toggle="tooltip" title="{{$val->result_remark}}" >
                       <span onclick="my_modal({{$val->id}},'{{$val->result}}','{{$val->result_remark}}')"  data-toggle="modal" data-target="#myModal">

                      <?php if($val->result==1){ echo "<span class='label label-success'>اجراء</span>";}if($val->result==2){ echo "<span class='label label-danger'>نا اجراء</span>";}if($val->result==3){ echo "<span class='label label-primary'>حفظیه</span>";} ?>
                      </span>
                    </span>
                  </td>
                  <td>{!!$val->file_address!!}</td>
                    @if(canDelete('document_management_list'))
                        <td><a href="delete_row_doc/{{$val->id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                    @endif
                    @if(canEdit('document_management_list'))
                        <td><a href="select_data_doc/{{$val->id}}" class="fa fa-edit"></a></td>
                    @endif
                  </tr>
                  <?php $counter++ ; ?>
                   @endforeach
                    @else
                    <tr><td align='center' colspan='15'>معلومات در سیستم اضافه نگردیده است</td></tr>
                    @endif
                  </tbody>
                </table>
                <div class="row" style="margin-right:0;">
                  <h5 style="font-weight:bold;">مجموعه اسناد:<span style="color:#00b1b3;"> {{$rows->total()}}</span></h5>
                </div>
                  <div class="text-center">
                  {!! $rows->render() !!}

                </div>


                <script type="text/javascript">
                  $(document).ready(function() {

                  $('.pagination a').on('click', function(event) {
                    event.preventDefault();
                    if ($(this).attr('href') != '#') {
                      //$('#ajaxContent').load($(this).attr('href'));
                      var dataString = $('#search_form').serialize();
                      dataString += "&page="+$(this).text()+"&ajax="+1;
                      $.ajax({
                          url: '{!!URL::route("get_mosawibat_doc")!!}',
                          data: dataString,
                          type: 'get',
                          beforeSend: function(){
                              //$("body").show().css({"opacity": "0.5"});
                              $('#datalist').html('<span style="margin-right:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                          },
                          success: function(response)
                          {
                              $('#datalist').html(response);
                          }
                      }
                    );

                    }
                  });
                    });

                    $(document).ready(function() {
                      $(".pagination a").on("click", function(){
                      $(".pagination").find(".active").removeClass("active");
                      $(this).parent().addClass("active");
                   });
                });

                </script>
