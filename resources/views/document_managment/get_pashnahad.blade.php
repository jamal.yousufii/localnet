        <div class="rows">
                    <ul class="nav nav-tabs" id="sub_tabs" style="border-top: 1px solid lightgray; border-radius: ; height: 46px;"  >


                    <li class="tab" id="pashnahad_sadira">
                      <a href="#tab7" data-toggle="tab" style="font-weight: bold;">پشنهادات صادره </a>
                    </li>
                     <li class="tab" id="peshnahad_warada"  >
                      <a href="#tab12" data-toggle="tab" style="font-weight: bold;">پشنهادات وارده</a>
                    </li>
                    {{-- <li class="tab" id="pashnahad_hadayati">
                      <a href="#tab8" data-toggle="tab" style="font-weight: bold;">پیشنهاد هدایتی</a>
                   </li> --}}
            ‍ ‍‍‍
            ‍ ‍‍‍
                   </ul>

          </div>

          <div class="col col-12" id="sub_menu_data">
          <div class="header">
             <br>
              <div class="row">
                @if(canAdd('document_management_list'))
                  <div class="pull-left">
                    <a href="{!!URL::route('insertForm_document')!!}" style="margin-left: 15px; font-weight: bold;" class="btn btn-success" >+ اضافه نمودن اسناد</a>
                  </div>
               @endif
              <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form3">
                 <div class="input-group custom-search-form">
                  <input type="text" class="form-control" id="search_field3" name="record" placeholder="جستجو" required />
                    {!!Form::token();!!}
                     <span class="input-group-btn">
                      <button class="btn btn-default-sm" id="search_button3" cat_id="3" ><i class="fa fa-search"></i> جستجو</button>
                  </span></div>
                </form>
              </div>
             </div>
            <h3 style="margin-top:20px;" align="center">لیست پشنهادات</h3><hr />
          </div>

        <div>
          <div id="datalist3">

            <table class="table table-bordered table-responsive"  id="">
                  <thead>
                     <tr>
                      <th>شماره #</th>
                      <th>شماره پشنهادات</th>
                      <th>تاریخ</th>
                      <th> نوع</th>
                      <th>فایل پشنهاد</th>
                      <th>ریاست مربوطه</th>
                      <th>توضیحات</th>
                      <th>مرجع</th>
                      <th>نتیجه</th>
                      <th>موقیعت فایل</th>
                      <th colspan="2">عملیات</th>
                      </tr>


                       </thead>
                       <tbody id="page_data">
                 @if(!empty($rows))
                   <?php $counter = 1; ?>
                 @foreach($rows as $val)
                  <tr>
                  <td>{!! $counter  !!}</td>
                  <td>{!! $val-> number_documents !!}</td>
                  <td>{!!checkEmptyDate($val->date)!!}</td>
                  <td><?php if($val->type_id==1){ echo "صادره";}else{echo "وارده";} ?> </td>
                  <td><a href="{!!URL::route('show_file_doc',$val->id)!!}"  target="_blank" ><i class='fa-file-image-o fa-2x text-success'></i></a> </td>
                  <td>{!! $val->dept_name !!}</td>
                  <td>{!! $val->description !!}</td>
                  <td>{!! $val->marji !!}</td>
                  <td >
                     <span data-toggle="tooltip" title="{{$val->result_remark}}" >
                       <span onclick="my_modal({{$val->id}},'{{$val->result}}','{{$val->result_remark}}')"  data-toggle="modal" data-target="#myModal">

                      <?php if($val->result==1){ echo "<span class='label label-success'>اجراء</span>";}if($val->result==2){ echo "<span class='label label-danger'>نا اجراء</span>";}if($val->result==3){ echo "<span class='label label-primary'>حفظیه</span>";} ?>
                      </span>
                    </span>
                  </td>
                  <td>{!!$val->file_address!!}</td>
                    @if(canDelete('document_management_list'))
                        <td><a href="delete_row_doc/{{$val->id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                    @endif
                    @if(canEdit('document_management_list'))
                       <td><a href="select_data_doc/{{$val->id}}" class="fa fa-edit"></a></td>
                    @endif

                  </tr>
                  <?php $counter++ ; ?>
                   @endforeach
                    @else
                    <tr><td align='center' colspan='15'>معلومات در سیستم اضافه نگردیده است</td></tr>
                    @endif
                    </tbody>
                </table>
                <div class="row" style="margin-right:0;">
                  <h5 style="font-weight:bold;">مجموعه اسناد:<span style="color:#00b1b3;"> {{$rows->total()}}</span></h5>
                </div>
                 <div class="text-center">
                  {!! $rows->render() !!}

                </div>
              </div>
                   </div>
                </div>


                 <script type="text/javascript">
                //get the contract type list for datatable
                $(document).ready(function() {

                  $("#search_button3").click(function(){
                    var field_value = $('#search_field3').val();

                       var tab_id= $(this).attr('cat_id');


                    $.ajax({
                      type : "post",
                      url : "{!!URL::route('search_live_doc')!!}",
                      data : {"cat_id":$(this).attr('cat_id'),"field_value":field_value,"_token": "<?=csrf_token();?>"},
                      success : function(response)
                      {
                        $("#datalist3").html(response);
                      }
                    });
                      return false;
                  });

                });


                </script>


              <script type="text/javascript">
                $(document).ready(function() {

                $('.pagination a').on('click', function(event) {
                  event.preventDefault();
                  if ($(this).attr('href') != '#') {
                    //$('#ajaxContent').load($(this).attr('href'));
                    var dataString = $('#search_form').serialize();
                    dataString += "&page="+$(this).text()+"&ajax="+1;
                    $.ajax({
                        url: '{!!URL::route("get_pashnehad_data_doc")!!}',
                        data: dataString,
                        type: 'get',
                        beforeSend: function(){
                            //$("body").show().css({"opacity": "0.5"});
                            $('#datalist3').html('<span style="margin-right:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                        },
                        success: function(response)
                        {
                            $('#datalist3').html(response);
                        }
                    }
                  );

                  }
                });
                  });

                  $(document).ready(function() {
                    $(".pagination a").on("click", function(){
                    $(".pagination").find(".active").removeClass("active");
                    $(this).parent().addClass("active");
                 });
              });
              </script>

              <script type="text/javascript">

                $(document).ready(function() {

                $("#pashnahad_hadayati").click( "li", function(evt) {

                   $.ajax({
                          type:'get',
                          url:'{!!URL::route("get_pashnahad_hadayati_doc")!!}',
                          success:function(data){

                             $('#sub_menu_data').html(data);
                             $("#sub_tabs").find('.active').removeClass('active');
                             $("#pashnahad_hadayati").addClass('active');

                                }
                         })
                        return false;
                       });
                });
              </script>
              <script type="text/javascript">

                $(document).ready(function() {

                $("#pashnahad_sadira").click( "li", function(evt) {

                   $.ajax({
                          type:'get',
                          url:'{!!URL::route("peshnahad_sadira")!!}',
                          success:function(data){

                             $('#sub_menu_data').html(data);
                             $("#sub_tabs").find('.active').removeClass('active');
                             $("#pashnahad_sadira").addClass('active');

                                }
                         })
                        return false;
                       });
                });
              </script>
              <script type="text/javascript">

                $(document).ready(function() {

                $("#peshnahad_warada").click( "li", function(evt) {

                   $.ajax({
                          type:'get',
                          url:'{!!URL::route("pashnahad_warida")!!}',
                          success:function(data){

                             $('#sub_menu_data').html(data);
                             $("#sub_tabs").find('.active').removeClass('active');
                             $("#peshnahad_warada").addClass('active');


                                }
                         })
                        return false;
                       });
                });
              </script>
