
          <div class="col col-12">
          <div class="header">
             <br>
            <h3 style="margin-top:20px;" align="center">لیست کارمندان</h3><hr />
            <div class="col-md-6 pull-right" style="margin-top: -74px; top: 0;margin-bottom: 0; width: 34%;margin-right: -16px;" dir="ltr">
             <form class="form-horizontal group-border-dashed" action="{{URL::route('export_employee')}}" id="search_form4" method="post">
               <div class="input-group custom-search-form">
                <input type="text" class="form-control" id="employee" name="record" placeholder="جستجو"  />
                  {!!Form::token();!!}
                   <span class="input-group-btn">
                    <button class="btn btn-default-sm" id="search" cat_id="4" ><i class="fa fa-search"></i> جستجو</button>
                </span></div>
                <span ><button type="submit" name="" class="btn btn-success" onClick="return confirm('Do want to export file ???')" style="margin-left: -90px; margin-top: -60px;"><span class=" fa-cloud-download" ></span>&nbsp;اکسل</button></span>
              </form>
            </div>
          </div>

        <div>
          <div id="datalist2">

            <table class="table table-bordered table-responsive"  id="">
              <tr style="border-style: none; display:none;" id="search_result"><td colspan="15" style=" color: green;text-align: center;  " ><b>نتیجه به دست آمده از جستجو</b></td></tr>

                     <tr>
                      <th># شماره </th>
                      <th>نام</th>
                      <th> نام پدر</th>
                      <th> وظیفه</th>
                      <th> ایمیل</th>
                      <th> شماره مبایل</th>
                      <th style="border: 1px solid #e4eaec"> سیسکو</th>
                    </tr>
                    <tbody id="page_data">
                    @if(!empty($employee))
                       <?php $counter = 1;  ?>
                     @foreach($employee as $val)
                    <tr>
                      <td>{!! $counter  !!}</td>
                      <td>{!! $val->name_dr !!}</td>
                      <td>{!! $val->father_name_dr !!}</td>
                      <td>{!! $val->current_position_dr !!}</td>
                      <td>{!! $val->email !!}</td>
                      <td>{!! $val->phone !!}</td>
                      <td style="border: 1px solid #e4eaec">{!! $val->ext2 !!}</td>
                    </tr>
                      <?php $counter++ ; ?>
                       @endforeach
                        @else
                        <tr><td align='center' colspan='15'>معلومات در سیستم اضافه نگردیده است</td></tr>
                        @endif
                      </tbody>
                </table>
                <div class="row" style="margin-right:0;">
                  <h5 style="font-weight:bold;">مجموعه کارمندان:<span style="color:#00b1b3;"> {{count($employee)}}</span></h5>
                </div>
                   </div>
                 </div>
                </div>
            <script type="text/javascript">
                  //get the contract type list for datatable
                  $(document).ready(function() {

                    $("#search").click(function(){
                      var field_value = $('#employee').val();

                      $.ajax({
                        type : "post",
                        url : "{!!URL::route('search_employee')!!}",
                        data : {"field_value":field_value,"_token": "<?=csrf_token();?>"},
                        success : function(response)
                        {
                          $('#search_result').show();

                          $("#page_data").html(response);
                        }
                      });
                        return false;
                    });

                  });




                </script>
