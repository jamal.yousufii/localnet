@extends('layouts.master')
@section('content')
<style type="text/css">


</style>
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>بخاطر اضافه نمودن سند از سیستم مدیریت اسناد Follwup استفاده نماید. </h3></center>
   <center><h3>followup.aop.gov.af</h3></center>
   <center><h5>در صورت داشتن مشکل به شماره های ذیل به تماس شوید.    93744078577 /  93749068280</center>

    <?php exit; ?> 
   <center><h3>اضافه نمودن اسناد</h3></center>
    <ol class="breadcrumb">

      <li class="active">اضافه نمودن اسناد  \     </li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>

         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif

      <form class="form-horizontal group-border-dashed" action="{!!URL::route('insert_doc')!!}"  method="post" style="border-radius: 0px;" name="myform" enctype= 'multipart/form-data' >


        <div class="form-group">
          <label class="col-sm-2 control-label"></label>
          <div class="col-sm-4">
            <span style="font-weight: bold;">نوع  </span> <select name="type_doc"  id="type_doc" class="form-control" style="width: 100%"  >
              <option value="" >- -  نوع   - -</option>
              <option value="1" >صادره</option>
              <option value="2" >وارده </option>

            </select>
          </div>
          <label class="col-sm-2 control-label"></label>
          <div class="col-sm-4">
            <span style="font-weight: bold;">نوع اسناد</span>
            <select id="document_type" name="document_type"   class="form-control" style="width: 100%" required>
              <option value="" selected>- -  نوع اسناد  - -</option>
              <option value="1" >حکم</option>
              <option value="4" >فرمان</option>
              <option value="2">مکتوب</option>
              <option value="3">پشنهاد</option>
              <option value="15">مصوبات</option>
              <option value="5">استعلام</option>
              <option value="6">ف س ۹</option>
              <option value="13">کاپی سوم</option>
              <option value="10">تعيينات</option>
              <option value="16">هدایات</option>
              <!--            <option value="7">مکتوب هدایتی</option>
              <option value="8">پیشنهاد هدایتی</option> -->
              {{-- <option value="9">ف س ۹ هدایتی</option> --}}
            </select>
          </div>
        </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"> </label>
            <div class="col-sm-4">
            <span style="font-weight: bold;"> تاریخ</span><input type="text" name="date" id="date"  class="datepicker_farsi form-control" placeholder="تاریخ " required >

            </div>
            <label class="col-sm-2 control-label" ></label>
            <div class="col-sm-4 ">
              <span style="font-weight: bold;">شماره اسناد</span> <input type="number" name="number" class="form-control" placeholder="شماره اسناد" required>
            </div>
          </div>
           <div class="form-group">
             <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">انتخاب ریاست </span>
              <select name="department_id"  id="department_select" class="form-control" style="width: 100%" required>
                <option value="" >- -  انتخاب ریاست  - -</option>
                @foreach($department as $val)
                <option value="{!!$val->id!!}">{!!$val->name!!}</option>
                @endforeach
              </select>
            </div>
              <label class="col-sm-2 control-label"></label>
             <div class="col-sm-4">
            <span style="font-weight: bold;">توضیحات</span> <textarea class="form-control" name="description" placeholder=" توضیحات"> </textarea>

            </div>

          </div>
          <div class="form-group">

            <label class="col-sm-2 control-label"></label>
           <div class="col-sm-4">
          <span style="font-weight: bold;">توضیحات نتیجه</span> <textarea class="form-control" name="result_remark" placeholder=" توضیحات"> </textarea>

          </div>
          <label class="col-sm-2 control-label" ></label>
            <div class="col-sm-4 ">
           <span style="font-weight: bold;">نتیجه</span>
           <select name="result"  id="result" class="form-control" style="width: 100%" required >
                <option value="" >- -  نتیجه   - -</option>
                <option value="1" >اجراه</option>
                <option value="2" >نااجراه </option>
                <option value="3" >حفظیه </option>

              </select>
            </div>
            </div>
        <div class="form-group" id="name_head" style="display:none">

             <label class="col-sm-2 control-label" ></label>
            <div class="col-sm-4 ">
           <span style="font-weight: bold;">مدیریت/آمریت</span> <input type="text" name="head_of_office" class="form-control" placeholder="مدیریت/آمریت" >
            </div>
          <label class="col-sm-2 control-label" ></label>
            <div class="col-sm-4 ">
           <span style="font-weight: bold;">شرکت /نام درخواست کننده</span> <input type="text" name="name" class="form-control" placeholder="شرکت /نام درخواست کننده" >
            </div>
          </div>


          <div class="form-group">

            <label class="col-sm-2 control-label" ></label>
            <div class="col-sm-4 " i>
             <span style="font-weight: bold;">موقیعت فایل </span> <input type="text" name="file_address"  class="form-control" placeholder="موقیعت فایل " >
            </div>
          <label class="col-sm-2 control-label" ></label>
            <div class="col-sm-4 " id="marji">
           <span style="font-weight: bold;">مرجع </span> <input type="text" name="marji"  class="form-control" placeholder="مرجع " >
            </div>
        </div>

        <div class="form-group">


        <label class="col-sm-2 control-label"> </label>
        <div class="col-sm-4">
            <span style="font-weight: bold;">فایل اسناد</span><div class="input_fields_wrap">
                <input type='file' id='files' style="width:87%;display:inline-block" name='files[]' class="form-control" multiple='multiple' >
                <a class="add_field_button btn" id="add" style="background: green;color:white;display:inline-block;" title="Add another file"> + </a>
            </div>
        </div>
        </div>
          {!!Form::token()!!}
         <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
             <input type="submit"  class="btn btn-success" value="ثبت شود"  onclick="validate();" name="myButton" id="myButton">
              <a href="{!!URL::route('document_recordsList')!!}"><input type="button" value="لیست اطلاعات
                  " id="add_department" class="btn btn-danger"/></a>
            </div>
          </div>
        </form><br><br><br><br><br>
  </div></div>
</div>
@stop

@section('footer-scripts')
<script type="text/javascript">
    $(document).ready(function(){


        });

</script>
<script type="text/javascript">
      function validate(){

    var  myButton= document.getElementById('myButton');
    var  myButton= document.getElementById('myButton');

          setTimeout (function(){

            document.getElementById("myButton").disabled = true;
          },0);
        setTimeout (function(){
             document.getElementById("myButton").disabled = false;
          },4000);
    }


</script>
<script type="text/javascript">

  $("#department_select").select2();
  $("#document_type").select2();

  $(function(){

        // repeat the input fields ===================================
        var max_fields      = 50; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;display:inline-block;margin:0px 4px 5px 0px" title="remove"> X </a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            $('#add').fadeIn("slow");
            $('#appendedInputButton').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x--;
        });
    });
// validate document related field
  $(document).ready(function() {
    $('#document_type').change(function() {

      var value = $(this).val();
      switch (value) {
        case '1' :
        case '4' :
           $('#name_head').hide();
           $('#marji').show();
           $('#type_doc').attr('disabled','disabled');
          break;
          case '2':
          case '3':
           $('#name_head').hide();
           $('#marji').show();
          $('#type_doc').removeAttr('disabled');

           break;
           case '5':
           case '6':

            $('#name_head').show();
            $('#marji').hide();
            $('#type_doc').removeAttr('disabled');

             break;
             case '13':
             $('#type_doc').attr('disabled','disabled');
             $('#marji').show();
             $('#name_head').show();


             break;
             case '15':
             $('#name_head').hide();
             $('#marji').show();
             $('#type_doc').removeAttr('disabled');

             break;

        default:

      }
    });
  });
</script>
@stop
