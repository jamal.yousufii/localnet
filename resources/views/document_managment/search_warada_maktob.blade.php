  <table class="table table-bordered table-responsive"  id="datalist10">

                 <thead>
                      <tr style="border-style: none;"><td colspan="15" style=" color: green;text-align: center;" ><b>نتیجه به دست آمده از جستجو</b></td></tr>

                     <tr>
                      <th>شماره #</th>
                      <th>شماره مکاتب وارده</th>
                      <th> شرکت /نام درخواست کننده</th>
                      <th>مدیریت/آمریت</th>
                      <th>تاریخ</th>
                      <th> نوع</th>
                      <th>فایل مکاتب وارده</th>
                      <th>ریاست مربوطه</th>
                      <th>راجع شده </th>
                      <th>توضیحات</th>
                      <th>موقیعت فایل</th>
                      <th colspan="2">عملیات</th>
                      </tr>
                       </thead>
                       <tbody>
                @if(!empty($rows))
                   <?php $counter = 1; ?>
                 @foreach($rows as $val)
                  <tr>
                  <td>{!! $counter  !!}</td>
                  <td>{!! $val-> number_documents !!}</td>
                  <td>{!! $val-> name !!}</td>
                  <td>{!! $val-> head_of_office !!}</td>
                  <td>{!!checkEmptyDate($val->date)!!}</td>
                  <td><?php if($val->type_id==1){ echo "صادره";}else{echo "وارده";} ?> </td>
                  <td><a href="{!!URL::route('show_file_doc',$val->id)!!}" target="_blank"  ><i class='fa-file-image-o fa-2x text-success'></i></a> </td>
                  <td>{!! $val->dept_name !!}</td>
                  <td>{!! $val->related_to !!}</td>
                  <td>{!! $val->description !!}</td>
                  <td>{!!$val->file_address!!}</td>
                  @if(canDelete('document_management_list'))
                      <td><a href="delete_row_doc/{{$val->id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                  @endif
                  @if(canEdit('document_management_list'))
                      <td><a href="select_data_doc/{{$val->id}}" class="fa fa-edit"></a></td>
                  @endif
                  </tr>
                  </tbody>
                  <?php $counter++ ; ?>
                   @endforeach
                    @else                    <tr>
                      <td colspan="20" style="color: red;text-align: center;"> موردی پیدا نشد !</td>
                    </tr>
                    @endif
