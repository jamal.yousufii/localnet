@extends('layouts.master')
@section('content')
<style type="text/css">


</style>
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>تغیرات آوردن در اسناد</h3></center>
    <ol class="breadcrumb">

      <li class="active">تغیرات آوردن در اسناد  \     </li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>

         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed" action="{!!URL::route('updateDoC',$data->id)!!}"  method="post" style="border-radius: 0px;" name="myform" enctype= 'multipart/form-data' >


        <div class="form-group">
          <label class="col-sm-2 control-label"></label>
          <div class="col-sm-4">
            <span style="font-weight: bold;">نوع  </span> <select name="type_doc"  id="type_doc" class="form-control" style="width: 100%" >
              <option value="" >- -  نوع   - -</option>
              <option value="1" <?php if($data->type_id == '1') echo "selected"; ?> >صادره</option>
              <option value="2" <?php if($data->type_id == '2') echo "selected"; ?>>وارده </option>

            </select>
          </div>
          <label class="col-sm-2 control-label"></label>
          <div class="col-sm-4">
            <span style="font-weight: bold;">نوع اسناد</span>
            <select name="document_type" class="form-control" id="document_type" style="width: 100%" required>
              <option value="" >- -  نوع اسناد  - -</option>
              <option value="1" <?php if($data->document_type == '1') echo "selected"; ?>>حکم</option>
              <option value="4" <?php if($data->document_type == '4') echo "selected"; ?>>فرمان</option>
              <option value="2"  <?php if($data->document_type == '2') echo "selected"; ?>>مکتوب</option>
              <option value="3" <?php if($data->document_type == '3') echo "selected"; ?>>پشنهاد</option>
              <option value="15" <?php if($data->document_type == '15') echo "selected";?>>مصوبات</option>
              <option value="5" <?php if($data->document_type == '5') echo "selected"; ?>>استعلام</option>
              <option value="6" <?php if($data->document_type == '6') echo "selected"; ?>>ف س ۹</option>
              <option value="13" <?php if($data->document_type == '13') echo "selected"; ?>>کاپی سوم</option>
              <option value="10" <?php if($data->document_type == '10') echo "selected"; ?>>تعیینات</option>
              <option value="9" <?php if($data->document_type == '9') echo "selected"; ?>>ف س ۹ هدایتی</option>
              <option value="16" <?php if($data->document_type == '16') echo "selected"; ?>>هدایات</option>
            </select>
          </div>
        </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"> </label>
            <div class="col-sm-4">
            <span style="font-weight: bold;"> تاریخ</span><input type="text" value="{!!checkEmptyDate($data->date)!!}" name="date" id="date"  class="datepicker_farsi form-control" placeholder="تاریخ "  required>

            </div>
            <label class="col-sm-2 control-label" ></label>
            <div class="col-sm-4 ">
              <span style="font-weight: bold;">شماره اسناد</span> <input type="number" value="{!! $data->number_documents !!}" name="number" class="form-control" placeholder="شماره اسناد" required>
            </div>
          </div>
           <div class="form-group">
             <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">انتخاب ریاست </span>
               <select name="department_id"  id="department_select" class="form-control" style="width: 100%" required>
                <option value="" >- -  انتخاب ریاست  - -</option>
                @foreach($department as $val)
                <option value="{!!$val->id!!}" <?php if($data->department_id == $val->id) echo "selected"; ?>>{!!$val->name!!}</option>
                @endforeach
              </select>
            </div>
              <label class="col-sm-2 control-label"></label>
             <div class="col-sm-4">
            <span style="font-weight: bold;">توضیحات</span> <textarea class="form-control" name="description" placeholder=" توضیحات">{!! $data->description!!} </textarea>

            </div>

          </div>
          <div class="form-group">

            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
              <span style="font-weight: bold;">توضیحات نتیجه</span> <textarea class="form-control" name="result_remark" placeholder=" توضیحات"> {{$data->result_remark}}</textarea>

            </div>
            <div class="col-sm-4 " style="float: right;">
              <label class="col-sm-2 control-label" ></label>
              <span style="font-weight: bold;">نتیجه</span>
              <select name="result"  id="result" class="form-control" style="width: 100%" required >
                <option value="" >- -  نتیجه   - -</option>
                <option value="1"  <?php if($data->result == '1') echo "selected"; ?> >اجراه</option>
                <option value="2"  <?php if($data->result == '2') echo "selected"; ?> >نااجراه </option>
                <option value="3"   <?php if($data->result == '3') echo "selected"; ?>>حفظیه </option>

              </select>
            </div>
          </div>
        <div class="form-group" id="name_head">
            <label class="col-sm-2 control-label" ></label>
            <div class="col-sm-4 ">
           <span style="font-weight: bold;">مدیریت/آمریت</span> <input type="text"  value="{!! $data ->head_of_office !!}" name="head_of_office" class="form-control" placeholder="مدیریت/آمریت" >
            </div>
          <label class="col-sm-2 control-label" ></label>
            <div class="col-sm-4 ">
           <span style="font-weight: bold;">شرکت /نام درخواست کننده</span> <input type="text" value="{!! $data ->name !!}" name="name" class="form-control" placeholder="شرکت /نام درخواست کننده" >
            </div>
          </div>
       <div class="form-group">
         <label class="col-sm-2 control-label" ></label>
         <div class="col-sm-4 " i>
          <span style="font-weight: bold;">موقیعت فایل </span> <input type="text" name="file_address" value="{{$data->file_address}}" class="form-control" placeholder="موقیعت فایل " >
         </div>
         <label class="col-sm-2 control-label" ></label>
           <div class="col-sm-4 " id="marji">
          <span style="font-weight: bold;">مرجع </span> <input type="text" name="marji" value="{{$data->marji}}" class="form-control" placeholder="مرجع " >
           </div>
        </div>
       <div class="form-group">

         <label class="col-sm-2 control-label"> </label>
         <div class="col-sm-4">
           <span style="font-weight: bold;">فایل اسناد</span><div class="input_fields_wrap">
             <input type='file' id='files' style="width:87%;display:inline-block" name='files[]' class="form-control" multiple='multiple'>
             <a class="add_field_button btn" id="add" style="background: green;color:white;display:inline-block;" title="Add another file"> + </a>
           </div>
           <label class="col-sm-2 control-label" ></label>
           <div class="col-sm-4 " id="">
           </div>
         </div>
       </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
             <input type="submit"  class="btn btn-success" value="ثبت شود"  onclick="validate();" name="myButton" id="myButton">
              <a href="{!!URL::route('document_recordsList')!!}"><input type="button" value="لیست اطلاعات
                  " id="add_department" class="btn btn-danger"/></a>
            </div>
          </div>


            <div class="col-sm-4 " style="float: right; margin-right: 120px; ">
            @foreach($files as $val)
            <div class="row">
              <table>
                <tr class="id{!!$val->file_id!!}" id="{!!$val->file_id!!}">
                  <td>   <a href="{{asset('uploads_doc/'.getMyDepartmentId().'/'.$val->file_name)}}" target='_blank'> <img src="{{asset('uploads_doc/'.getMyDepartmentId().'/'.$val->file_name)}}" height="200px" width="200px" style="margin-right: -120px;">&nbsp;&nbsp;&nbsp;</a>
                  <button class="delete_button" img_id="{!!$val->file_id!!}"><span class="fa fa-trash-o"></span> </button>
                 </td>
               </tr>
             </table>
           </div>
            @endforeach
            </div>
        </div>


          {!!Form::token()!!}

        </form><br><br><br><br><br>
  </div></div>
</div>
@stop

@section('footer-scripts')
<script type="text/javascript">
//get the contract type list for datatable
$(document).ready(function() {
  $(".delete_button").click(function(){
       var img_id= $(this).attr('img_id');
       if(confirm("Are you sure want to delete?")){
    $.ajax({
      type : "post",
      url : "{!!URL::route('delete_image_doc')!!}",
      data : {"img_id":$(this).attr('img_id'),"_token": "<?=csrf_token();?>"},
      success : function(data)
      {

      }
    });
  $(this).parent().fadeOut(300,function(){
          $(this).remove();
        });
      return false;
  }else{
    return false;
  }
});

});
</script>



<script type="text/javascript">

      // validate document related field
        $(document).ready(function() {

          if($('#document_type').val() == '1' || $('#document_type').val() == '4'){

            $('#type_doc').attr('disabled','disabled');
            $('#name_head').hide();
          }else if($('#document_type').val() == '2' || $('#document_type').val() == '3'){
            $('#type_doc').removeAttr('disabled');
            $('#name_head').hide();
          }else if($('#document_type').val() == '5' || $('#document_type').val() == '6'|| $('#document_type').val() == '10'|| $('#document_type').val() == '13'){

            $('#type_doc').removeAttr('disabled');
            $('#name_head').show();
            $('#marji').hide();



          }


          $('#document_type').change(function() {

            var value = $(this).val();
            switch (value) {
              case '1' :
              case '4' :
                 $('#name_head').hide();
                 $('#marji').show();
                 $('#type_doc').attr('disabled','disabled');
                break;
                case '2':
                case '3':
                 $('#name_head').hide();
                 $('#marji').show();
                $('#type_doc').removeAttr('disabled');

                 break;
                 case '5':
                 case '6':

                  $('#name_head').show();
                  $('#marji').hide();
                  $('#type_doc').removeAttr('disabled');

                   break;
                   case '13':
                   $('#type_doc').attr('disabled','disabled');
                   $('#marji').show();
                   $('#name_head').show();


                   break;
                   case '15':
                   $('#name_head').hide();
                   $('#marji').show();
                   $('#type_doc').removeAttr('disabled');

                   break;

              default:

            }
          });
        });
</script>
<script type="text/javascript">
  function validate(){

var  myButton= document.getElementById('myButton');
var  myButton= document.getElementById('myButton');

                        setTimeout (function(){

                          document.getElementById("myButton").disabled = true;
                        },0);
                      setTimeout (function(){
                           document.getElementById("myButton").disabled = false;
                        },4000);
}


</script>

<script type="text/javascript">

  $("#department_select").select2();
  $("#document_type").select2();

  $(function(){

        // repeat the input fields ===================================
        var max_fields      = 8; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;display:inline-block;margin:0px 4px 5px 0px" title="remove"> X </a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            $('#add').fadeIn("slow");
            $('#appendedInputButton').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x--;
        });

    });


</script>
@stop
