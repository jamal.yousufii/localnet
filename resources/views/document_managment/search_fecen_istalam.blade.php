
<table class="table table-bordered table-responsive" id="datalist">

<tr style="border-style: none;"><td colspan="15" style=" color: green;text-align: center;" ><b>نتیجه به دست آمده از جستجو</b></td></tr>


      <tr>
        <th>شماره #</th>
         <th>شماره سند</th>
         <th>شرکت /نام درخواست کننده</th>
         <th>مدیریت/آمریت</th>
         <th>تاریخ</th>
         <th> نوع</th>
         <th>فایل </th>
         <th>ریاست مربوطه</th>
         <th style="border: 1px solid lightgray">توضیحات</th>
         <th>نتیجه</th>
         <th>موقیعت فایل</th>
         <th colspan="2" style="border: 1px solid lightgray">عملیات</th>
         </tr>

                @if(!empty($rows))
                   <?php $counter = 1; ?>
                 @foreach($rows as $val)
                  <tr>
                  <td>{!! $counter  !!}</td>
                  <td>{!! $val-> number_documents !!}</td>
                  <td>{!! $val-> name !!}</td>
                  <td>{!! $val-> head_of_office !!}</td>
                  <td>{!!checkEmptyDate($val->date)!!}</td>
                  <td><?php if($val->type_id==1){ echo "صادره";}else{echo "وارده";} ?> </td>
                  <td><a href="{!!URL::route('show_file_doc',$val->id)!!}" target="_blank" ><i class='fa-file-image-o fa-2x text-success'></i></a> </td>
                  <td>{!! $val->dept_name !!}</td>
                  <td>{!! $val->description !!}</td>
                  <td >
                     <span data-toggle="tooltip" title="{{$val->result_remark}}" >
                       <span onclick="my_modal({{$val->id}},'{{$val->result}}','{{$val->result_remark}}')"  data-toggle="modal" data-target="#myModal">

                      <?php if($val->result==1){ echo "<span class='label label-success'>اجراء</span>";}if($val->result==2){ echo "<span class='label label-danger'>نا اجراء</span>";}if($val->result==3){ echo "<span class='label label-primary'>حفظیه</span>";} ?>
                      </span>
                    </span>
                  </td>
                  <td>{!!$val->file_address!!}</td>
                  @if(canDelete('document_management_list'))
                      <td><a href="delete_row_doc/{{$val->id}}" class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td>
                  @endif
                  @if(canEdit('document_management_list'))
                      <td style="border: 1px solid lightgray"><a href="select_data_doc/{{$val->id}}" class="fa fa-edit"></a></td>
                  @endif


                  </tr>
            <?php $counter++ ; ?>
           @endforeach

        @else
        <tr>
          <td colspan="15" style="color: red;text-align: center;"> موردی پیدا نشد !</td>
        </tr>
      @endif
