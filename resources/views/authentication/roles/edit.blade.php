<div class="row">
  <div class="col-lg-12">
    <div class="m-portlet">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
              <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
              {{ trans('authentication.edit_role') }}
            </h3>
          </div>
        </div>
      </div>
      <!--begin::Form-->
      @if($record)
        <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form-data" id="requestForm" method="post">
          <div class="m-portlet__body">
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.name_dr') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" value="{!!$record->name_dr!!}" name="name_dr" id="name_dr">
                <div class="name_dr error-div" style="display:none;"></div>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.name_pa') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" value="{!!$record->name_pa!!}" name="name_pa" id="name_pa">
                <div class="name_pa error-div" style="display:none;"></div>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.name_en') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" value="{!!$record->name_en!!}" name="name_en" id="name_en">
                <div class="name_en error-div" style="display:none;"></div>
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.code') }} : <span style="color:red;">*</span></label>
                <input class="form-control m-input errorDiv" type="text" value="{!!$record->code!!}" name="code" id="code">
                <div class="code error-div" style="display:none;"></div>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.section') }} : <span style="color:red;">*</span></label>
                <div id="section_id" class="errorDiv">
                  <select class="form-control m-input m-input--air select-2" name="section_id">
                    <option value="">{{ trans('global.select') }}</option>
                    @if($sections)
                      @foreach($sections as $item)
                        @if($item->id==$record->section_id)
                          <option value="{!!$item->id!!}" selected>{!!$item->name!!}</option>
                        @else
                          <option value="{!!$item->id!!}">{!!$item->name!!}</option>
                        @endif
                      @endforeach
                    @endif
                  </select>
                </div>
                <div class="section_id error-div" style="display:none;"></div>
              </div>
              <div class="col-lg-4">
                <label class="title-custom">{{ trans('authentication.description') }} : </label>
                <input class="form-control m-input" type="text" value="{!!$record->description!!}" name="description">
              </div>
            </div>
            <div class="form-group m-form__group row m-form__group_custom">
              <div class="col-lg-12">
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions--solid">
                    <button type="button" onclick="doEditRecord('{{route('roles.update',$enc_id)}}','requestForm','PUT','response_div');" class="btn btn-primary">{{ trans('global.submit') }}</button>
                    <button type="button" onclick="redirectFunction()" class="btn btn-secondary">{{ trans('global.back') }}</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @csrf
        </form>
      @endif
      <!--end::Form-->
    </div>
  </div>
</div>
<script type="text/javascript">
$(".select-2").select2();
</script>
