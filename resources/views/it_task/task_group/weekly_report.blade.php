@extends('layouts.master')

@section('head')
    <title>{!!_('weekly_report')!!}</title>
    {!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
    
@stop
@section('content')
<script>
$(".datepicker_farsi").persianDatepicker(); 
</script>
<div class="page-content">
      <div class="panel">
        <div class="panel-body container-fluid">
			<div class="row row-lg">
				<div class="col-sm-12">
					<div class="example-wrap">
					    <h4 class="example-title">{!!_('weekly_report')!!}</h4>
					    <div class="example">
					      <form method="POST" action="{!!URL::route('generateWeeklyReport_it')!!}">
					        <div class="form-group row">
					        	<div class="col-sm-3">
									<label class="control-label" for="inputBasicLastName">{!!_('start_date')!!}</label>
									<input value="@if(isset($sdate)){!!$sdate!!} @endif" placeholder="{!!_('start_date')!!}" class="form-control datepicker_farsi" type="text" name="start_date" id="start_date" readonly="readonly">
								</div>
					          	<div class="col-sm-3">
									<label class="control-label" for="inputBasicLastName">{!!_('end_date')!!}</label>
									<input value="@if(isset($edate)){!!$edate!!} @endif" placeholder="{!!_('end_date')!!}" class="form-control datepicker_farsi" type="text" name="end_date" id="end_date" readonly="readonly">
								</div>
					          
					          
					        </div>
					        
					        <div class="form-group">
					          <button type="submit" class="btn btn-primary">{!!_('generate_report')!!}</button>
					        </div>
					      </form>
					    </div>
					  </div>
					  <!-- End Example Basic Form -->
					</div>
				</div>
				<div class="row row-lg">
					<div class="col-sm-12">
		              <!-- Example Bordered Table -->
		              <div class="example-wrap">
		              <?php 
		              if(isset($sdate))
		              {
		              	$url = URL::route("weeklyReport_excel_it",array($sdate,$edate));
		              }
		              else
		              {
		              	$url = '';
		              }
		              ?>
		              @if($excel)
		              	<button type="button" class="btn btn-primary" onclick="location.href='{!!$url!!}'";>{!!_('print_to_excel')!!}</button>
		              @endif
		                <div class="example table-responsive">
		                  <table class="table table-bordered" @if(getLangShortcut()!='en') dir="rtl" @endif>
		                    <thead>
		                      <tr class="success">
		                        <th>{!!_('number')!!}</th>
		                        <th>{!!_('title')!!}</th>
		                        <th>{!!_('start_time')!!}</th>
		                        
		                        <th>{!!_('assign_to')!!}</th>
		                        <th>{!!_('summary')!!}</th>
		                        <th>{!!_('Progress')!!}</th>
		                        <th>{!!_('end_time')!!}</th>
		                      </tr>
		                    </thead>
		                    <tbody>
		                    <?php $main_no = 1;$sub_no = 1; ?>
		                      @if(isset($result))
		                      	@foreach($result AS $task)
		                      		<tr class="danger"><td colspan="7">{!!_('main_task')!!}</td></tr>
		                      		<tr>
		                      			<td>{!!$main_no!!}</td>
		                      			<td>{!!$task->title!!}</td>
		                      			<td>{!!$task->created_at!!}</td>
		                      			<td>
		                      			<?php $assignees = getTaskAssignees_it($task->id);?>
		                      			@if($assignees)
		                      				@foreach($assignees AS $assignee)
		                      					{!!getUserFullName_it($assignee->assigned_to)!!},
		                      				@endforeach
		                      			@endif
		                      			</td>
		                      			<td>
		                      			
		                      				{!!$task->description!!}
		                      			
		                      			</td>
		                      			<td>
		                      			<?php $progress = getTaskAllProgress_it($task->id,$sdate,$edate); $end_date = '';?>
			                      			@if($progress)
			                      				@foreach($progress AS $prog)
			                      					{!!$prog->progress_summary!!},
			                      					@if($prog->progress==100)
			                      					<?php $end_date = $prog->created_at; ?>
			                      					@endif
			                      				@endforeach
			                      			@endif
		                      			</td>
		                      			<td>
		                      				{!!$end_date!!}
		                      			</td>
		                      		</tr>
		                      		{!!getSubTask_report($task->id,$main_no,'',$sdate,$edate)!!}
		                      		
		                      		<?php $main_no++; ?>
		                      	@endforeach
		                      @else
		                      	<tr>
		                      		<td colspan="4">{!!_('no_record_fount')!!}</td>
		                      	</tr>
		                      @endif
		                    </tbody>
		                  </table>
		                </div>
		              </div>
		              <!-- End Example Bordered Table -->
		            </div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('footer-scripts')
	{!! HTML::script('/js/tasks.js') !!}   
@stop
