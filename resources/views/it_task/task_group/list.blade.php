@extends('layouts.master')

@section('head')
	<title>Task Group list</title>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box ui-draggable ui-droppable">
			<div class="box-content">
				@if(Session::has('success'))
					<div class='alert alert-success span6'>{{Session::get('success')}}</div>
				@elseif(Session::has('fail'))
					<div class='alert alert-danger span6'>{{Session::get('fail')}}</div>
				@endif
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id='list'>
					<thead>
					  <tr>
					    <th>#</th>
					    <th>{!!_('title')!!}</th>
					    <th>{!!_('description')!!}</th>
					    <th>{!!_('created_by')!!}</th>
					    <th>{!!_('operations')!!}</th>
					  </tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			    <div class="btn-group" role="group" style='padding:10px;'>
				  	<a href="{{URL::route('getTaskGroupCreate_it')}}" class="btn btn-primary"><i class='fa fa-plus'></i> {!!_('new_task_group')!!}</a> 
			    </div>
				
			</div>
		</div>
	</div>
</div>
@stop
@section('footer-scripts')
<script type="text/javascript">
$(document).ready(function() {
$('#list').dataTable( 
	{
		//'info': false,
		//'sDom': 'lTfr<"clearfix">tip',
		"bProcessing": true,
		"bServerSide": true,
		//"iDisplayLength": 2,
		"sAjaxSource": "{{URL::route('getTaskGroupData_it')}}",
		"aaSorting": [[ 1, "desc" ]],
		
	}
); 

});
</script>
@stop


