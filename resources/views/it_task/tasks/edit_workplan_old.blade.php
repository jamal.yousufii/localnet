@extends('layouts.master')

@section('head')
    <title>Edit Task</title>
@stop

@section("content")

<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active"><span>Task Edit</span></li>
        </ol>
        
        <h1>Task<small>Edit</small></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>Create Edit</h2>
            </header>
            
            <div class="main-box-body clearfix">
                @foreach($task_details AS $task_item)
                <form role="form" method="post" action="{{ URL::route('UpdateReport',$task_item->id) }}" class="form-horizontal">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Title :</label>
                        <div class="col-sm-4">
                            <input value='{{$task_item->title}}' class="form-control" type="text" name="title" data-placement="bottom">
                        </div>
                        <label class="col-sm-2 control-label" style="padding-left: 0;font-size: 90%;">Quantity of deliverables :</label>
                        <div class="col-sm-4">
                            <input value='{{$task_item->quantity_of_deliverables}}' class="form-control" type="text" name="quantity_of_deliverables" data-placement="bottom">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Indicator :</label>
                        <div class="col-sm-4">
                            <input value='{{$task_item->indicator}}' class="form-control" type="text" name="indicator" data-placement="bottom">
                        </div>
                        <label class="col-sm-2 control-label">Metric :</label>
                        <div class="col-sm-4">
                            <input value='{{$task_item->metric}}' class="form-control" type="text" name="metric" data-placement="bottom">
                        </div>
                        
                    </div>
                    <div class="form-group">
                        
                        <label class="col-sm-2 control-label">Start date :</label>
                        <div class="col-sm-4">
                            <input value='<?php if($task_item->start_date!="0000-00-00"){echo $task_item->start_date;}?>' class="form-control datepicker_farsi" type="text" name="start_date" data-placement="bottom">
                        </div>
                    
                        <label class="col-sm-2 control-label">End date :</label>
                        <div class="col-sm-4">
                            <input value='<?php if($task_item->end_date!="0000-00-00"){echo $task_item->end_date;}?>' class="form-control datepicker_farsi" type="text" name="end_date" data-placement="bottom">
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Task Group :</label>
                        <div class="col-sm-10">
                            <select class="form-control" name='task_group'>
                                <option>Select</option>
                                @foreach($taskGroup AS $item)
                                    <option <?php if($item->id == $task_item->task_group_id){echo "selected";} ?> value='{{$item->id}}'>{{$item->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-styles">Descriptions</label>
                        <div class="col-sm-10">
                                <textarea class="form-control" rows="2" name="description">{{$task_item->description}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-styles">Expected outcome</label>
                        <div class="col-sm-10">
                                <textarea class="form-control" rows="2" id="wysiwig_full" name="expected_outcome">{{$task_item->expected_outcome}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Assign To</label>
                        <div class="col-sm-10">
                            <div id="users_div" style="display:none;">
                                <select name='assign_with' id='assign_with' style="width:500px;">
                                    <?php 
                                        foreach($users AS $uitem)
                                        {
                                            $selected = "";
                                            if(in_array($uitem->id, $assignees)){$selected='selected';}
                                            echo "<option ".$selected." value='".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div id="managers_div" style="display:inline">
                                <select name='assign_with' id='assign_with1' style="width:500px;">
                                    <option value=''>Select</option>
                                    <?php 
                                        foreach($managers AS $uitem)
                                        {
                                            $selected = "";
                                            if(in_array($uitem->id, $assignees)){$selected='selected';}
                                            echo "<option ".$selected." value='".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            &nbsp;
                            <div class="checkbox-nice checkbox-inline">
                                <input type="checkbox" id="only" onclick="showManagers()">
                                <label for="only">
                                    Show All Subordinates
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div class="checkbox-nice checkbox-inline">
                            <input <?php if($task_item->notify_by_email==1){echo "checked";} ?> value='1' id='by_email' name="by_email" type="checkbox">
                            <label for="by_email">
                                Notify By Email
                            </label>
                        </div>
                        <div class="checkbox-nice checkbox-inline">
                            <input <?php if($task_item->notify_by_sms==1){echo "checked";} ?> value='1' name="by_sms" id='by_sms' type="checkbox">
                            
                            <label for="by_sms">
                                Notify By SMS
                            </label> 
                        </div>
                    
                    </div>
                    {{ Form::token() }}
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-sm-10">
    
                            <button class="btn btn-primary" type="submit">Save</button>
                        
                            <button onclick="history.back()" class="btn btn-danger" type="button">Cancel</button>
                        </div>
                    </div>
                
                </form>
                @endforeach
            </div>
        </div>
    </div>
</div>
@stop
@section('footer-scripts')
{{ HTML::script('/js/template/select2.min.js')}}
{{ HTML::script('/js/template/hogan.js')}}
{{ HTML::script('/js/template/typeahead.min.js')}}
<script type="text/javascript">
      $("#assign_with").select2();
      $("#assign_with1").select2();

        $(document).ready(function() {
        // Create Wysiwig editor for textare
        TinyMCEStart('#wysiwig_simple', null);
        //TinyMCEStart('#wysiwig_full', 'extreme');
        });
    //show only managers
    function showManagers()
    {
        var ch = document.getElementById('only');
        if(ch.checked)
        {
            document.getElementById('managers_div').style.display = "none";
            document.getElementById('users_div').style.display = "inline";
            
        }
        else
        {
            document.getElementById('managers_div').style.display = "inline";
            document.getElementById('users_div').style.display = "none";
            
        }
    }
    </script>
@stop
