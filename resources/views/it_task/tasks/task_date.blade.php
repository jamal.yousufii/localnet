{!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
<script>
	$(".datepicker_farsi").persianDatepicker(); 	
</script>
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
    	@if($type=='task_detail')
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        <span aria-hidden="true">×</span>
		    </button>
		@else
			<button type="button" class="btn btn-danger" onclick="load_task_detail('task_detail',{!!$parent_id!!})">Back</button>
		@endif
      
    </div>
    <div class="modal-body">
      <form role="form" class='form-horizontal' id="change_modal_frm" name="change_modal_frm" style="padding-right:10px;">
            <input type="hidden" name="task_id" id="task_id_modal" value="<?=$report_id?>">  
            <input type="hidden" id="parent_id" value="<?=$parent_id?>">   
            <input type="hidden" id="type" value="<?=$type?>">           
            <div class="form-group">
                <label class="col-sm-2 control-label">Start date :</label>
                <div class="col-sm-4">
                    <input class="form-control datepicker_farsi" value="<?=jalali_format($s_date)?>" type="text" name="start_date" id="start_date_modal" data-placement="bottom">
                </div>
                <label class="col-sm-2 control-label">End date :</label>
                <div class="col-sm-4">
                    <input class="form-control datepicker_farsi" value="<?=jalali_format($e_date)?>" type="text" name="end_date" id="end_date_modal" data-placement="bottom">
                </div>
            </div>              
      </form>
    </div>
    <div class="modal-footer">
      
      <button onclick='changeDate();' type="button" class="btn btn-primary modal-submit">Update</button>
    </div>
  </div>
</div>