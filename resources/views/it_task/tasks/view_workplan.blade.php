
@extends('layouts.master')
@section('head')
	{!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
    {!! HTML::style('/css/tooltipster.css') !!}
    {!! HTML::style('/css/template/libs/nifty-component.css') !!}
	{!! HTML::style('/css/template/libs/jquery.nouislider.css') !!}
	{!! HTML::style('/css/tasks.css') !!}
    {!! HTML::script('/js/jquery.tooltipster.min.js')!!}
    {!! HTML::style('/vendor/select2/select2.css') !!}
    
    <title>View Task Details</title>
<style>
	.select2-dropdown{
		z-index: 100000;
	}
	.no-padding {
		padding:0px;
	}
</style>

{!! HTML::style('/css/conversation.css') !!}


@stop
@section("content")

<div class="row row-lg">
    <div class="col-sm-12">
        <div class="example-wrap">   
            <header class="main-box-header clearfix">
            <?php $approved = true;
	        	$task_creator 	= DB::connection('helpdesk')->table('tasks')->where('id',$task_item->id)->pluck('user_id');
				if($task_item->parent_task == 0)
				{//main task
					if(has_subtask($task_item->id))
					{
						if(!isMainTaskApproved($task_item->id))
						{//main task is not approved, show the buttons
							$approved = false;
						}
					}
					else
					{//no sub task
						if(!isTaskCompleted($task_item->id))
						{//task is not approved, show the buttons
							$approved = false;
						}
					}
				}
				else
				{//sub task
					if(has_subtask($task_item->id))
					{//if the sub task has sub task itself
						if(!isMainTaskApproved($task_item->id))
						{//task is not approved, show the buttons
							$approved = false;
						}
						else
						{//the sub task does not have subtask itself
							if(!isTaskCompleted($task_item->id))
							{//task is not approved, show the buttons
								$approved = false;
							}
						}
					}
					else
					{
						if(!isTaskCompleted($task_item->id))
						{//sub task is not approved, show the buttons
							$approved = false;
						}
					}
				}
			//dont show the buttons if and only if the task is approved as completed
			?>
			@if(!$approved)
                <div class="filter-block pull-right" id="all_buttons">
                    @if(Auth::user()->id == $task_item->user_id)
                        <a href="{!!URL::route('DeleteReport', $task_item->id)!!}" class="btn btn-danger pull-right" onclick="javascript:return confirm('Are you sure you want to DELETE this report and its subtasks ? THERE IS NO UNDO')">
                            <i class="fa fa-trash-o fa-lg"></i> {!!_('delete_task')!!}
                        </a>
                    @endif
                    
		           
                    <a href="{!!URL::route('getReport_it')!!}" class="btn btn-primary pull-right">
                        <i class="fa fa-chevron-left fa-lg"></i> {!!_('back_to_list')!!}
                    </a>
                </div>
            @endif
                
            </header>
            <div class="example" id="all_inputs">
                @if(Session::has('success'))
                    <div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
                @elseif(Session::has('fail'))
                    <div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
                @endif    
                <form role="form" method="post" action="{!! URL::route('UpdateReport_it',$task_item->id) !!}" class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-sm-6">
                        	<label class="control-label" for="inputBasicFirstName">{!!_('title')!!} :</label>
                            <input value='{!!$task_item->title!!}' required="required" class="form-control" type="text" name="title">
                        </div>                           
                		<div class="col-sm-6">
                            <label class="control-label">{!!_('task_group')!!} :</label>
                            <?php 
                                $page = URL::route('isTaskGroupInReport_it');
                            ?>
                            <select class="form-control" name='task_group' id="task_group" onchange="isTaskGroupInReport('{!!$page!!}',this.value)">
                                <option>{!!_('select')!!}</option>
                                @foreach($taskGroup AS $item)
                                    <option <?php if($item->id == $task_item->task_group_id){echo "selected";} ?> value='{!!$item->id!!}'>{!!$item->title!!}</option>
                                @endforeach
                            </select>
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6">    
                            <label class="control-label">{!!_('start_date')!!} :</label>
                            
                            	<?php $sdate = $task_item->start_date; if($sdate!='0000-00-00'){$s_date = explode('-', $task_item->start_date);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                                <input value='<?php if($task_item->start_date != "0000-00-00"){echo jalali_format($sdate);} ?>' class="form-control datepicker_farsi" type="text" name="start_date" data-placement="bottom">
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label">{!!_('end_date')!!} :</label>
                            
                            	<?php $edate = $task_item->end_date; if($edate!='0000-00-00'){$e_date = explode('-', $task_item->end_date);$edate = dateToShamsi($e_date[0], $e_date[1], $e_date[2]);}?>
                                <input value='<?php if($task_item->end_date != "0000-00-00"){echo jalali_format($edate);} ?>' class="form-control datepicker_farsi" type="text" name="end_date" id="end_date" data-placement="bottom">
                                <input type="hidden" id="enddate" value="<?=$task_item->end_date?>" />
                        </div>
                            
                    </div>
                        
                    
                    <div class="form-group">
                    	<div class="col-sm-6">
                            <label class="control-label">{!!_('description')!!} :</label>                            
                             <textarea class="form-control" rows="2" name="description">{!!$task_item->description!!}</textarea>
                         </div>
                         
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6">
                        
                            <label class="control-label">{!!_('assign_to')!!} :</label>
                            
                     
		                		<div id="users_div">
				                    <select name='assign_with1[]' id='assign_with1' style="width:100%;" multiple="multiple" data-plugin="select2">
				                        
				                        <?php 
				                            foreach($users AS $uitem)
				                            {
				                                $selected = "";
				                                if(in_array($uitem->id, $assignees)){$selected='selected';}
				                                echo "<option ".$selected." value='".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
				                            }
				                        ?>
				                    </select>
				                </div>                                                    
                         </div>
                         	                              
                    </div>
                    
                    <div class="form-group">
                    	<div class="col-sm-6">
		             		@if(count($files)>0)
								@foreach($files AS $file)
								<div id="div_file_{!!$file->id!!}">
									<a href="{!!route('downloadAttachmentTasks',$file->file_name)!!}">{!!$file->file_name!!}</a>&nbsp;@if(Auth::user()->id == $task_item->user_id || isAdmin())<span class="icon glyphicon glyphicon-remove" style="color: red" title="Delete the file" onclick="if(confirm('Are you sure?')){delete_attachment('{!!$file->id!!}','{!!$file->file_name!!}')}">X</span>@endif
								</div>
								@endforeach
								
							@endif	
							
		             	</div>
		            </div>
		            
                    {!! Form::token() !!}

                    @if((Auth::user()->id == $task_item->user_id || isAdmin()) && !$approved)
                    <div class="form-group">
                    	<div class="col-sm-6">
                        	<label class="control-label">&nbsp;</label>                              
                            <button class="btn btn-primary" type="submit">{!!_('update')!!}</button>                           
                        </div>
                    </div>
                    @endif
                    
                </form>                              
            </div>
        </div>
    </div>
</div>
@if($task_item->user_id == Auth::user()->id)
<div class="form-group col-md-12">                        
    <!-- Sub task start -->
    <fieldset style="border-color:red !important;">
        <legend>{!!_('sub_tasks')!!}</legend>
        <div class="list-task">
            <ul style="margin:0 0 0 0px;">					                
                <div id="task_id_{!!$task_item->id!!}">
                    @foreach($sub_tasks AS $item)
                    <?php 
                        if($item->end_date != '0000-00-00')
                        {
                            $end_date = $item->end_date;	
                            //convert shamsi to meladi
                            //$end_date = explode("-", $item->end_date);
                         	//$end_date = dateToMiladi($end_date[0],$end_date[1],$end_date[2]);
                     	}
                        else
                        {
                            $end_date = date('Y-m-d');
                   	}
                        
                        $from=date_create($end_date);
                    	$to=date_create(date('Y-m-d'));
                    
                    	$diff=date_diff($to,$from);
                    
                    	$deadline = $diff->format('%R%a');
                    	$d=$deadline;

                    	$label_status = "";
                    	$days;

                        switch (true) {
                            case ($d < 0):
                                $days='Due';
                                break;
                            case ($d == 0):
                                $days='Today';
                                break;
                            case ($d == 1):
                                $days='Tomorrow';
                                break;
                            case ($d > 1 and $d < 7):
                                $days=abs($d).' Days';
                                break;
                            case ($d > 6 and $d < 15):
                                $days='Next Week';
                                break;
                            case ($d > 14 and $d < 22):
                                $days='Two Weeks';
                                break;
                            case ($d > 21 and $d < 29):
                                $days='Three Weeks';
                                break;
                            case ($d > 28 and $d < 56):
                                $days='Next Month';
                                break;
                            case ($d > 55 and $d < 336):
                                $nm=round($d/30);
                                $days=$nm.' Months';
                                break;
                            case ($d > 335):
                                $ny=round($d/365);
                                $days=$ny.' Year(s)';
                                break;
                            default:
                                $days='select date';
                                break;
                        }

                        if($d>0)
                        {
                            $label_status = "label-success";
                        }
                        else if($d == 0)
                        {
                            $label_status = "label-warning";
                        }
                        else
                        {
                            $label_status = "label-danger";
                        }

                        $s_date = $item->start_date;
						$e_date = $item->end_date;
						if($s_date != '0000-00-00')
						{
							$sdate = explode("-", $s_date);
							$sy = $sdate[0];
							$sm = $sdate[1];
							$sd = $sdate[2];
							$s_date = dateToShamsi($sy,$sm,$sd);		
						}
						if($e_date != '0000-00-00')
						{
							$edate = explode("-", $e_date);
							$ey = $edate[0];
							$em = $edate[1];
							$ed = $edate[2];
							$e_date = dateToShamsi($ey,$em,$ed);		
						}
					//get task progress
                    if(has_subtask($item->id))
					{
						$progress = getSubTaskProgress($item->id);
					}
					else
					{
						$progress = getMainTaskProgress($item->id);
					}
                    ?>
                    <li>
                        @if($progress == 100)
						<div class="checkbox-custom checkbox-inline">
							<input type="checkbox" checked disabled>
							<label>&nbsp;</label>
						</div>
						@else
						<div class="checkbox-custom task-check checkbox-inline">
							<input type="checkbox" disabled="">
							<label>&nbsp;</label>
						</div>
						@endif
						<?php $status = get_task_status($item->id); ?>
                        <a href="#task_detail" data-toggle="modal" onclick="load_task_detail('task_detail',{!!$item->id!!})">
							<div class="task_title big_title" style="margin-top:0.3em;">{!!$item->title!!}</div>
						</a>
						@if($item->user_id!=Auth::user()->id && $status != -1)
						<div class="task_title big_title"> 
							@if($status==0)<a href="{!!URL::route('ViewReport',array($item->id))!!}" style="color: #B9B6B6;font-style: italic;font-size: 0.9em">- {!!_('pending')!!} </a>@endif
						</div> 
						@endif
						<?php 
				       	$onclick_date = 'title="'._("You_dont_have_access_to_change_the_date").'" style="cursor:default"';
				       	if(Auth::user()->id == $item->user_id || isAdmin())
						{
							$onclick_date = "onclick=\"$('#task_id_modal').val('".$item->id."');$('#start_date_modal').val('".jalali_format($s_date)."');$('#end_date_modal').val('".jalali_format($e_date)."');\" data-target='#change_date_modal' data-toggle='modal' ";
						}
						?>
						<span id="label_{!!$item->id!!}" style="margin-right: 170px;direction:rtl;float: right;position:relative;">
							<a href="javascript:void()" <?=$onclick_date?>>
								<span id="label_updated_{!!$item->id!!}" class="label {!!$label_status!!} date_middle"> {!!$days!!}</span>
							</a>
						</span>     
                        <div class="pull-right">
                            <div class="assignees" style="display:inline;">
                            <?php 
					       	$onclick = 'href="javascript:void()" title="'._("You_dont_have_access_to_change_assignee").'" style="cursor:default"';
					       	if(Auth::user()->id == $item->user_id || isAdmin())
							{
								$onclick = 'href="#task_detail" onclick="load_task_assignee('.$item->id.')" ';
							}
							$assignees = getTaskAssignees($item->id);
							?>
							@if(count($assignees)>0)
								@if(count($assignees)<=3)
									@foreach($assignees AS $a_item)
									<?php
									    $photo = getProfilePicture($a_item->assigned_to);
									    $tooltip = getUserFullName($a_item->assigned_to);
									?>
									<a data-toggle="modal" <?=$onclick?>>
										{!!HTML::image('/img/'.$photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip'));!!}
									</a>
									@endforeach
								@else
									<span id="more_assignee_<?=$item->id?>" style="cursor: pointer" onmouseover="show_tooltipster(<?=$item->id?>,'<?=URL::route("get_assignee")?>')">
										{!!HTML::image('/img/more.png', '', array('class' => 'project-img-owner'));!!}
									</span>
									<?php $count = 1; ?>
									@foreach($assignees AS $a_item)
										@if($count <= 2)
											<?php
											    $photo = getProfilePicture($a_item->assigned_to);
											    $tooltip = getUserFullName($a_item->assigned_to);
											?>
											<a data-toggle="modal" <?=$onclick?>>
												{!!HTML::image('/img/'.$photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip'));!!}
											</a>
											<?php $count++;?>
										@endif
									@endforeach
								@endif
	                            
	                        @else
								<a data-toggle="modal" <?=$onclick?>>
									{!!HTML::image('/img/default.jpeg', '', array('class' => 'project-img-owner','data-original-title'=>'New Assignee','data-toggle'=>'tooltip'));!!}
								</a>
							@endif
                            </div>
                            <?php 
					       	$onclick_progress = 'title="'._("Since_this_task_has_some_sub_tasks,_you_cant_update_progress_directly").'('.$progress.'% '._("completed)").'" class="progress"';
					       	if(!has_subtask($item->id))
							{
								$onclick_progress = 'onclick="loadModal('.$item->id.')" title="'.$progress.'% '._("completed").'" class="progress md-trigger" data-target="#progress_modal" data-toggle="modal" ';
							}
							?>
						    <div <?=$onclick_progress?> style="display:inline-block;">
						        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{!!$progress!!}" aria-valuemin="0" aria-valuemax="100" style="width: {!!$progress!!}%;">
						            <span class="sr-only">{!!$progress!!}% {!!_('complete')!!}</span>
						        </div>
						    </div>
                        </div>
                        {!!getSubTaskTree($item->id)!!}
                    </li>
                    @endforeach
                </div>						                
                <div class="add-item">
                    <span class="item-plus">+</span>
                    <input onkeypress="saveNewTask(event,'task_id_{!!$task_item->id!!}','{!!$task_item->task_group_id!!}','{!!$task_item->id!!}','task_title_{!!$task_item->id!!}')" style="width:300px;border:0px;" placeholder="{!!_('add_new_sub_task')!!}" type="text" name="title_sub" id="task_title_{!!$task_item->id!!}" style="border:none;">
                </div>						                
            </ul>
        </div>
    </fieldset>
</div>
@endif
<div class="row">
    <div class="form-group col-md-6" style="border-right: 1px solid #eee;margin-top: -20px">
        <div class="col-lg-12">
            <header class="main-box-header clearfix">
                <h4 class="pull-left value red">{!!_('comments')!!}</h4>
            </header>
            <div class="main-box-body clearfix">
                <div class="conversation-wrapper">
                	<?php 
                	$comments = getTaskComments_it($task_item->id); ?>
                    <div class="conversation-content" id="comments_div" @if(count($comments)>3)style="max-height: 220px; overflow-y: scroll;"@endif>
                    	@foreach($comments AS $comment)
                            <div class="conversation-item item-left clearfix">
                                <div class="conversation-user">
                                	<?php
									    $photo = getProfilePicture($comment->user_id);
									    $tooltip = getUserFullName_it($comment->user_id);
									?>
                                    {!!$tooltip!!}
                                    {!! HTML::image('/img/'.$photo,'',array('class' => 'project-img-owner comment','title'=>$tooltip)) !!}
                                </div>
                                <div class="conversation-body" onclick="showCommentDate(<?=$comment->id?>,'comment')" style="cursor:pointer">			                                			                              
                                    <div class="text">
                                        {!!$comment->comment!!}
                                    </div>
                                </div>
                                <div class="time hidden-xs comment_date" style="display: none;margin-left: 60px" id="comment_<?=$comment->id?>">
                                    {!!$comment->created_at!!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <br><br>
                    <div class="conversation-new-message">
                        <form class="comment_form">
                            <input type="hidden" id="comment_task_id" value="{!!$task_item->id!!}">
                            <div class="form-group">
                                <textarea name="comment" id="comment" class="form-control" rows="2" placeholder="{!!_('enter_new_comment')!!}"></textarea>
                            </div>
                            
                            <div class="clearfix">
                                <button type="button" class="btn btn-success pull-right" onclick='postComment()'>{!!_('post_comment')!!}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6" style="border-left: 1px solid #eee;margin-top: -20px">
    	<div class="col-lg-12">					                
            <header class="main-box-header clearfix">
                <h4 class="pull-left value red">{!!_('progress_summary')!!}</h4>
                <span class="pull-right" id="task_completed_alert">
                
                	@if(!$approved && ($task_item->user_id == Auth::user()->id || isAdmin()))
                        <button type="button" class='btn btn-warning mrg-b-lg' onclick="approveCompletedTask('{!!$task_item->id!!}');"><i class="fa fa-check-circle fa-lg"></i> {!!_('approve_as_completed')!!}</button>
                    @endif
                    <?php
                    $completed = true;
                    if(has_subtask($task_item->id))
                    {
                    	if(!isMainTaskCompleted_withSubtasks_it($task_item->id))
                    	{
                    		$completed = false;
                    	}
                    }
                    else
                    {
                    	if(!isTaskPercentage_complete_it($task_item->id))
                    	{
                    		$completed = false;
                    	}
                    }
                    ?>
                    @if($approved)
                    	<div class="alert alert-success">
                            <i class="fa fa-check-circle fa-fw fa-lg"></i>
                            <strong>{!!_('well_done!')!!}</strong> {!!_('task_approved')!!}
                        </div>
                    @elseif($completed)
                    	<div class="alert alert-success">
                            <i class="fa fa-check-circle fa-fw fa-lg"></i>
                            <strong>{!!_('well_done!')!!}</strong> {!!_('task_completed')!!}
                        </div>
                    @endif
                   
                </span>
            </header>
            <div class="main-box-body clearfix">
                <div class="conversation-wrapper">
                	<?php
                	$summary = getTaskSummaryDetails_it($task_item->id);?>
                    <div class="conversation-content" id="progress_summary_div" @if(count($summary)>2)style="max-height: 220px; overflow-y: scroll;"@endif>				                           
                    @foreach($summary AS $summary_item)				                           
                        <div class="conversation-item item-left clearfix">
                            <div class="conversation-user">
                            	<?php
								    $photo = getProfilePicture($summary_item->user_id);
								    $tooltip = getUserFullName_it($summary_item->user_id);
								?>
                                
                                {!! HTML::image('/img/'.$photo,'',array('class' => 'project-img-owner comment','title'=>$tooltip)) !!}
                            </div>
                            <div class="conversation-body" onclick="showCommentDate(<?=$summary_item->id?>,'progress')" style='cursor:pointer'>
                                <div class="text">
                                    {!!$summary_item->progress_summary!!}
                                    
                                    <div class="progress" title="{!!$summary_item->progress!!}% {!!_('completed')!!}">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="{!!$summary_item->progress!!}" aria-valuemin="0" aria-valuemax="100" style="width: {!!$summary_item->progress!!}%;">
                                            <span class="sr-only">{!!$summary_item->progress!!}% {!!_('complete')!!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="time hidden-xs progress_date" style="display: none;margin-left: 60px" id="progress_<?=$summary_item->id?>">
                                {!!$summary_item->created_at!!}
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @if(!has_subtask_it($task_item->id))  
                    	@if(!isTaskPercentage_complete_it($task_item->id))
                            <div class="conversation-new-message">
                                <form id="progress_chagne_modal">
                                    <input type="hidden" name="task_id" value="{!!$task_item->id!!}">
                                    <div class="form-group">
                                        <textarea name="progress_summary" id="progress_summary" class="form-control" rows="2" placeholder="{!!_('enter_new_progress_summary')!!}"></textarea>
                                    </div>
                                    <div class="form-group"	
	                                    <h5><center><span id="progress"></span></center></h5>
									    <div class="slider-basic1" onchange="check_percentage()"></div>
									    <input type="hidden" name="percentage" id="percentage">	
								    </div>			                               
                                    <div class="clearfix">
                                        <button type="button" id="progress_button" class="btn btn-warning pull-right" disabled onclick='postProgress()'>{!!_('update_progress')!!}</button>
                                    </div>
                                </form>
                            </div>
                        @else
                        	<div class="conversation-new-message">
                                <div class="clearfix">				                                    	
                                    {!!_('task_completed')!!}!				                                       
                                </div>
                            </div>
                        @endif
                    @else
                    <div class="conversation-new-message">
                        <div class="clearfix">				                                    	
                            {!!_('Since_this_task_has_some_sub_tasks,_you_cant_update_progress_directly')!!}!				                                       
                        </div>
                    </div>
                    @endif
                </div>
            </div>						             
        </div>						      
    </div>
</div>
<!-- Modal Start -->


<div class="modal fade modal-fade-in-scale-up" id="progress_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog">
	    <div class="md-content">
	        <div class="modal-header">
		      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        <span aria-hidden="true">×</span>
		      </button>
		      <h4 class="modal-title">{!!_('update_task_progress')!!}</h4>
		    </div>
	        <div class="modal-body" id="pop_content1">
	        </div>
	        <div class="modal-footer" id='create_footer'>
	            <button onclick='postProgress()' id="progress_button_pop" disabled type="button" class="btn btn-primary modal-submit">{!!_('save')!!}</button>
	        </div>
	    </div>
	</div>
</div>

<!-- Change date modal start -->
<div class="modal fade modal-fade-in-scale-up" id="change_date_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">×</span>
	      </button>
	      <h4 class="modal-title">{!!_('change_date')!!}</h4>
	    </div>
	    <div class="modal-body">
	      <form role="form" class='form-horizontal' id="change_modal_frm" name="change_modal_frm" style="padding-right:10px;">
	            <input type="hidden" name="task_id" id="task_id_modal" value="0">              
	            <div class="form-group">
	                <label class="col-sm-2 control-label">{!!_('start_date')!!} :</label>
	                <div class="col-sm-4">
	                    <input class="form-control datepicker_farsi" type="text" name="start_date" id="start_date_modal" data-placement="bottom">
	                </div>
	                <label class="col-sm-2 control-label">{!!_('end_date')!!} :</label>
	                <div class="col-sm-4">
	                    <input class="form-control datepicker_farsi" type="text" name="end_date" id="end_date_modal" data-placement="bottom">
	                </div>
	            </div>              
	      </form>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default margin-0" data-dismiss="modal">{!!_('close')!!}</button>
	      <button onclick='changeDate();' type="button" class="btn btn-primary modal-submit">{!!_('update')!!}</button>
	    </div>
	  </div>
	</div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
<div class="modal fade modal-fade-in-scale-up" id="task_detail" aria-hidden="true" aria-labelledby="task_detail" role="dialog" tabindex="-1"></div>

@stop
@section('footer-scripts')
{!! HTML::script('/js/tasks.js') !!}   
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
{!! HTML::script('/js/template/jquery.slimscroll.min.js')!!}
{!! HTML::script('/js/template/jquery.nouislider.js')!!}
@if($task_item->user_id != Auth::user()->id && !isAdmin())
	<script type="text/javascript">
		$("#all_inputs :input").attr("disabled", 'true');
	</script>
@endif
<script type="text/javascript">
    $("#assign_with").select2();
	$("#assign_with1").select2();
	$(".datepicker_farsi").persianDatepicker();
	function load_task_detail(type,id)
   {
    	var page = "{!!URL::route('load_task_detail_it')!!}";
    	$.ajax({
            url:page,
            type:'post',
            data: '&id='+id+'&type='+type,
            success: function(r){
				$('#task_detail').html(r);
            }
        });
    }
    function load_task_assignee(id)
   {
    	var page = "{!!URL::route('load_task_assignee_it')!!}";
    	$.ajax({
            url:page,
            type:'post',
            data: '&id='+id,
            success: function(r){
				$('#task_detail').html(r);
            }
        });
    }
	function task_approve(task_id)
	{
		var page = "{!!URL::route('approveTask_it')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&task_id='+task_id,
	        success: function(response){
	        	location.href="{!!URL::route('getReport_it')!!}";
	        }
	    });
	}
	function task_reject(task_id)
	{
		var comment = $('#reason').val();
		var end_date= $('#end_date').val();//new end date
		var enddate= $('#enddate').val();//current end_date
		
		if(comment == ''){alert('Type your reason to reject!');return;}
		
		var page = "{!!URL::route('rejectTask_it')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&task_id='+task_id+'&comment='+comment+'&end_date='+end_date+'&enddate='+enddate,
	        success: function(response){
	        	location.href="{!!URL::route('getReport_it')!!}";
	        }
	    });
	}
    //is task group included in report or not
    function isTaskGroupInReport(page,selectedTaskGroup)
    {
        $.ajax({
            url:page,
            type:'post',
            dataType:'json',
            data: '&task_group='+selectedTaskGroup,
            success: function(response){

                if(response.condition == 'true')
                {
                    $('#include_in_report').slideDown();
                }
                else
            {
                    $('#include_in_report').slideUp();
                }
            }
        });
    }
    //approve completed task
    function approveCompletedTask(id)
    {
        var r = confirm("Do you want to continue?");

        if(r == true)
        {
            $.ajax({
                url     : "{!!URL::route('approveCompletedTask_it')!!}",
                type    : "post",
                data    : "&task_id="+id,
                dataType: 'json',
                success : function(result){
                    
                    if(result.cond === 'true')
                    {
                        $('#trigger_modal').trigger('click');
                        $('#pop_content2').html(result.data);
                    }
                    else
                    {
                        $("#task_completed_alert").html(result.data);
                    }
                }
            });
        }
        else
      {
            return false;
        }
    }
    
</script>
<script type="text/javascript">
    function loadEditModal(id)
    {   
        $.ajax({
            url     : "{!!URL::route('loadEditSubModal_it')!!}",
            type    : "post",
            data    : "&id="+id,
            success : function(result){
                $('#pop_content').html(result);
                //$("#dialog").dialog("open");
                $('#create_footer').hide();
                $('#edit_footer').show();
            }
        });
    }
    function loadModal(id)
    {   
        $.ajax({
            url     : "{!!URL::route('loadModal_it')!!}",
            type    : "post",
            data    : "&id="+id+"&main="+0,
            success : function(result){
                $('#pop_content1').html(result);
            }
        });
    }
</script>
<script type="text/javascript">

    function postComment()
    {   
        $.ajax({
            url     : "{!!URL::route('postComment_it')!!}",
            type    : "post",
            data    : $("form.comment_form").serialize(),
            success : function(result){
                $('#comments_div').append(result);
                $('#comment').val('');
            }
        });    
        return false;
    }
    function postSubTask()
    {   
        $.ajax({
            url     : "{!!URL::route('postSubTaskCreate_it')!!}",
            type    : "post",
            data    : $("form.form-horizontal").serialize(),
            success : function(result){
                //$('#sub_task_div').html(result);
                location.reload();
            }
        });    
        return false;
    }
    function postSubEdit()
    {
        $.ajax({
            url     : "{!!URL::route('postSubTaskUpdate_it')!!}",
            type    : "post",
            data    : $("form.form-horizontal").serialize(),
            success : function(result){
                //$('#sub_task_div').html(result);
                location.reload();
            }
        });    
        return false;
    }
	function showCommentDate(id,type)
	{
		$('.'+type+'_date').hide();
		$('#'+type+'_'+id).show();
	}

    //min/max slider
      var currentProgress = '{!!$task_item->percentage!!}';

    $('.slider-basic1').noUiSlider({
        range: [0,100],
        start: [0],
        handles: 1,
        connect: 'lower',
        slide: function(){
            var val = Math.round($(this).val());
            $('#progress').text(
                     val+"%"
                );
            $(this).next('input').val(val);
            
        },
        set: function() {
            var val = Math.round($(this).val());
            $('#progress').text(
                     val+"%"
                );
            $(this).next('input').val(val);
        }
    });
    $('.slider-basic1').val(currentProgress, true);
	function delete_attachment(id,name)
	{
		var counter = 1;
		$.ajax({
			url:'{{URL::route("deleteAttachmentTask_it")}}',
			data: '&doc_id='+id+'&name='+name,
			type:'POST',
			success:function(r){
				$('#div_file_'+id).hide();
			}
		});
	}
	function add_file()
	{
		var current_total = $('#total_files').val();
		var total = parseInt(current_total)+parseInt(1);
		$('#total_files').val(total);
		var new_div = '<div id="files_'+total+'"><div class="col-md-11"><input class="form-control" type="file" name="files[]"></div><div class="col-md-1"><i class="icon wb-minus-circle" aria-hidden="true" style="font-size: 30px;cursor: pointer;" onclick="remove_file('+total+')"></i></div></div>';
		$('#all_files').append(new_div);	
	}
	function remove_file(no)
	{
		$('#files_'+no).remove();
	}
</script>

@stop
