
@foreach($task_details AS $task_item)

<form action="" method="post" class='form-horizontal' id="sub_task_frm" name="sub_task_frm">

        <input type="hidden" name="parent_task" value="{{$task_item->parent_task}}">
        <input type="hidden" name="task_group" value="{{$task_item->task_group_id}}">
        <input type="hidden" name="task_id" value="{{$task_item->id}}">
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Title :</label>
            <div class="col-sm-4">
                <input value='{{$task_item->title}}' class="form-control" type="text" name="title" id="title" data-placement="bottom">
            </div>
            <label class="col-sm-2 control-label" style="padding-left: 0;font-size: 90%;">Quantity of deliverables :</label>
            <div class="col-sm-4">
                <input value='{{$task_item->quantity_of_deliverables}}' class="form-control" type="text" name="quantity_of_deliverables" id="quantity_of_deliverables" data-placement="bottom">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Indicator :</label>
            <div class="col-sm-4">
                <input value='{{$task_item->indicator}}' class="form-control" type="text" name="indicator" id="indicator" data-placement="bottom">
            </div>
            <label class="col-sm-2 control-label">Metric :</label>
            <div class="col-sm-4">
                <input value='{{$task_item->metric}}' class="form-control" type="text" name="metric" id="metric" data-placement="bottom">
            </div>
        </div>
        <div class="form-group">
    
            <label class="col-sm-2 control-label">Start date :</label>
            <div class="col-sm-4">
                <input value='<?php if($task_item->start_date!="0000-00-00"){echo $task_item->start_date;}?>' class="form-control datepicker_farsi" type="text" name="start_date" id="start_date" data-placement="bottom">
            </div>
        
            <label class="col-sm-2 control-label">End date :</label>
            <div class="col-sm-4">
                <input value='<?php if($task_item->end_date!="0000-00-00"){echo $task_item->end_date;}?>' class="form-control datepicker_farsi" type="text" name="end_date" id="end_date" data-placement="bottom">
            </div>
            
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-styles">Descriptions</label>
            <div class="col-sm-10">
                    <textarea class="form-control" rows="2" name="description" id="description">{{$task_item->description}}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-styles">Expected outcome</label>
            <div class="col-sm-10">
                    <textarea class="form-control" rows="2" name="expected_outcome" id="expected_outcome">{{$task_item->expected_outcome}}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Assign To</label>
            <div class="col-sm-10">

                <select name='assign_with[]' id='assign_with' style="width:100%">
                    <?php 
                        foreach($users AS $uitem)
                        {
                            $selected = "";
                            if(in_array($uitem->id, $assignees)){$selected='selected';}
                            echo "<option ".$selected." value='".$uitem->id."'>".$uitem->first_name.' '.$uitem->last_name."</option>";
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">&nbsp;</label>
            <div class="checkbox-nice checkbox-inline">
                <input <?php if($task_item->notify_by_email==1){echo "checked";} ?> value='1' id='by_email1' name="by_email" type="checkbox">
                <label for='by_email1'>Notify By Email</label>
            </div>
            <div class="checkbox-nice checkbox-inline">
                <input <?php if($task_item->notify_by_sms==1){echo "checked";} ?> value='1' name="by_sms" id='by_sms1' type="checkbox">
                <label for='by_sms1'>Notify By SMS</label>
            </div>
        </div>
        <h5>Percentage of Main Task: <span id="percent"></span></h5>
        <div class="slider-basic"></div>
        <input type="hidden" name="percentage" value="{{$task_item->percentage}}">
</form>

<script type="text/javascript">
      $("#assign_with").select2();
      //min/max slider
    $('.slider-basic').noUiSlider({
        range: [0,100],
        start: [0],
        handles: 1,
        connect: 'lower',
        slide: function(){
            var val = Math.round($(this).val());
            
            $('#percent').text(
                 val+"%"
            );
            $(this).next('input').val(val);
        },
        set: function() {
            var val = Math.round($(this).val());
            
            $('#percent').text(
                 val+"%"
            );
            $(this).next('input').val(val);
        }
    });
    $('.slider-basic').val('{{$task_item->percentage}}', true);
</script>
@endforeach

      