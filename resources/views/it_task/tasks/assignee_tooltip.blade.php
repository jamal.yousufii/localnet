
@foreach($assignees AS $a_item)
	<?php
	    $photo = getProfilePicture($a_item->assigned_to);
	    $tooltip = getUserFullName($a_item->assigned_to);
	?>
	<div>
		{!!HTML::image('/img/'.$photo, '', array('class' => 'project-img-owner'));!!}
		<span style="padding-top: 6px;display: inline-block">
		&nbsp;<?=$tooltip?>
		</span>
	</div>
	<br/>
@endforeach
<!--
<?php $field = 'more_assignee_'.$id;?>
<a data-toggle="modal" href="javascript:void()" title="close" onclick="$(<?=$field?>).tooltipster('hide');">
	{!!HTML::image('/img/close.jpg', '', array('class' => 'project-img-owner'));!!}
</a>
-->