<form action="" method="post" class='form-horizontal' id="progress_chagne_modal" name="progress_chagne_modal">

    <input type="hidden" name="task_id" value="{!!$task_id!!}">
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-styles">Progress Summary</label>
        <div class="col-sm-10">
            <textarea class="form-control" rows="2" name="progress_summary" id="progresssummary"></textarea>
        </div>
    </div>
    <!--
    <div class="form-group">
        <label class="col-sm-2 control-label">&nbsp;</label>
        <div class="col-sm-10 checkbox-nice">
            <input value='1' type="checkbox" id="in_report" name="in_report" checked="checked">
            <label for="in_report">
                Include in report
            </label>
        </div>
    </div>
	-->
    <h5><center><span id="progress_pop"></span></center></h5>
    <div class="slider-basic1_pop" onchange="check_percentage()"></div>
    <input type="hidden" name="percentage" id="percentage_pop">
</form>

<script type="text/javascript">
      //min/max slider
      var currentProgress = '{!!$progress!!}';

    $('.slider-basic1_pop').noUiSlider({
        range: [0,100],
        start: [0],
        handles: 1,
        connect: 'lower',
        slide: function(){
            var val = Math.round($(this).val());
            $('#progress_pop').text(
                 val+"%"
            );
            $(this).next('input').val(val);
            
        },
        set: function() {
            var val = Math.round($(this).val());
            $('#progress_pop').text(
                     val+"%"
                );
            $(this).next('input').val(val);
            
        }
    });
    $('.slider-basic1_pop').val(currentProgress, true);

    function postProgress()
    { 
    	if($('#progresssummary').val() == '')
		{
			alert('Please add some description');return;
		}  
        $.ajax({
            url     : "{!!URL::route('postProgress')!!}",
            type    : "post",
            data    : $("#progress_chagne_modal").serialize(),
            success : function(result){
                //$('#sub_task_div').html(result);
                location.reload();
            },
            error  : function( xhr, err ){
                alert(err);     
            }
        });    
        return false;
    }
    function check_percentage()
	{
		var percentage = $('#percentage_pop').val();
		$('#progress_button_pop').attr('disabled','true');
		
		if(percentage > {!!$progress!!})
		{
			$('#progress_button_pop').removeAttr('disabled');
		}
	}
</script>


      