
@foreach($object AS $item)
	<?php
	$get_last_id = $item->task_id;
	
	if($item->end_date != '0000-00-00')
	{
		$end_date = $item->end_date;
		//convert shamsi to meladi
		//$end_date = explode("-", $item->end_date);
		//$end_date = dateToMiladi($end_date[0],$end_date[1],$end_date[2]);
	}
	else
	{
		$end_date = date('Y-m-d');
	}
	
	$from=date_create($end_date);
	$to=date_create(date('Y-m-d'));
	
	$diff=date_diff($to,$from);
	
	$deadline = $diff->format('%R%a');
	$d=$deadline;
	
	$label_status = "";
	$days;
	
	switch (true) {
		case ($d < 0):
			$days='Due';
			break;
		case ($d == 0):
			$days='Today';
			break;
		case ($d == 1):
			$days='Tomorrow';
			break;
		case ($d > 1 and $d < 7):
			$days=abs($d).' Days';
			break;
		case ($d > 6 and $d < 15):
			$days='Next Week';
			break;
		case ($d > 14 and $d < 22):
			$days='Two Weeks';
			break;
		case ($d > 21 and $d < 29):
			$days='Three Weeks';
			break;
		case ($d > 28 and $d < 56):
			$days='Next Month';
			break;
		case ($d > 55 and $d < 336):
			$nm=round($d/30);
			$days=$nm.' Months';
			break;
		case ($d > 335):
			$ny=round($d/365);
			$days=$ny.' Year(s)';
			break;
		default:
			$days='select date';
			break;
	}
	
	if($d>0)
	{
	    $label_status = "label-success";
	}
	else if($d == 0)
	{
	    $label_status = "label-warning";
	}
	else
	{
	    $label_status = "label-danger";
	}
	if(has_subtask_it($item->task_id))
	{
		$progress = getSubTaskProgress_it($item->task_id);
	}
	else
	{
		$progress = getMainTaskProgress_it($item->task_id);
	}
	$assignees = getTaskAssignees_it($item->task_id);
	$s_date = $item->start_date;
	$e_date = $item->end_date;
	if($s_date != '0000-00-00')
	{
		$sdate = explode("-", $s_date);
		$sy = $sdate[0];
		$sm = $sdate[1];
		$sd = $sdate[2];
		$s_date = dateToShamsi($sy,$sm,$sd);		
	}
	if($e_date != '0000-00-00')
	{
		$edate = explode("-", $e_date);
		$ey = $edate[0];
		$em = $edate[1];
		$ed = $edate[2];
		$e_date = dateToShamsi($ey,$em,$ed);		
	}
	
	?>
		<li class="{!!$page!!} list_main_task" id="list_{!!$item->task_id!!}">
			<span style="display:inline-block;width:70%;cursor: pointer;" data-target="#task_detail" data-toggle="modal" onclick="load_task_detail('task_detail',{!!$item->task_id!!})">
				@if($progress == 100)
					<div class="checkbox-custom checkbox-inline">
						<input type="checkbox" checked disabled>
						<label>&nbsp;</label>
					</div>
					@else
					<div class="checkbox-custom checkbox-inline" style="margin-top: -1em">
		              <input type="checkbox" disabled />
		              <label>&nbsp;</label>
		            </div>
				@endif
			
				
				<div class="task_title big_title" style="margin-top:0.3em;">{!!$item->title!!}</div>
				
				@if(getTaskProgress4Complete_it(Auth::user()->id,true,$item->task_id)>0)
				<div class="task_title big_title" style="color: #B9B6B6;font-style: italic;font-size: 0.9em"> 
					- {!!_('pending_for_approve')!!}
				</div> 
				@endif
			</span>
			
			<?php 
	       	$onclick_date = 'title="'._("You_dont_have_access_to_change_the_date").'" style="cursor:default"';
	       	
			?>
			<span id="label_{!!$item->task_id!!}" style="margin-right: 170px;direction:rtl;float: right;position:relative;">
				<a href="javascript:void()" <?=$onclick_date?> id="label_updated_{!!$item->task_id!!}">
					<span class="label {!!$label_status!!} date_middle"> {!!$days!!}</span>
				</a>
			</span>
			<div class="pull-right">   		    
		        <div class="assignees" style="display:inline;">
		       	<?php 
		       	$onclick = 'href="javascript:void()" title="'._("You_dont_have_access_to_change_assignee").'" style="cursor:default"';
		       	
				?>
				<!--get task assignees-->
				@if(count($assignees)>0)
					@if(count($assignees)<=3)
						@foreach($assignees AS $a_item)
						<?php
						    $photo = getProfilePicture($a_item->assigned_to);
						    $tooltip = getUserFullName_it($a_item->assigned_to);
						?>
						<a href="javascript:void()" data-toggle="modal" <?=$onclick?>>
							{!!HTML::image('/img/'.$photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip'));!!}
						</a>
						@endforeach
					@else
						<span id="more_assignee_<?=$item->task_id?>" style="cursor: pointer" onmouseover="show_tooltipster(<?=$item->task_id?>,'<?=URL::route("get_assignee")?>')">
							{!!HTML::image('/img/more.png', '', array('class' => 'project-img-owner'));!!}
						</span>
						<?php $count = 1; ?>
						@foreach($assignees AS $a_item)
							@if($count <= 2)
								<?php
								    $photo = getProfilePicture($a_item->assigned_to);
								    $tooltip = getUserFullName_it($a_item->assigned_to);
								?>
								<a href="javascript:void()" data-toggle="modal" <?=$onclick?>>
									{!!HTML::image('/img/'.$photo, '', array('class' => 'project-img-owner','data-original-title'=>$tooltip,'data-toggle'=>'tooltip'));!!}
								</a>
								<?php $count++;?>
							@endif
						@endforeach
					@endif
				@else
					<a href="javascript:void()" data-toggle="modal" <?=$onclick?>>
						{!!HTML::image('/img/default.jpeg', '', array('class' => 'project-img-owner','data-original-title'=>'New Assignee','data-toggle'=>'tooltip'));!!}
					</a>
				@endif
				</div>
				<?php 
		       	$onclick_progress = 'title="'._("You_dont_have_access_to_update_progress").'" class="progress"';
		       	
				?>
			    <div <?=$onclick_progress?> style="display:inline-block;">
			        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{!!$progress!!}" aria-valuemin="0" aria-valuemax="100" style="width: {!!$progress!!}%;">
			            <span class="sr-only">{!!$progress!!}% {!!_('complete')!!}</span>
			        </div>
			    </div>
			</div>
		</li>
	
		{!!getSubTaskTree_it($item->task_id,$mode)!!}
	</li>
@endforeach

<input type="hidden" id="showhidetask" value="true" />
<div class="modal fade modal-fade-in-scale-up" id="task_detail" aria-hidden="true" aria-labelledby="task_detail" role="dialog" tabindex="-1"></div>
