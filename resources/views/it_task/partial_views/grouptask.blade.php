
<div class="task-group-list">
	<h5><a href="{!!URL::route('getReportGroup_it',array($item->group_id,'all'))!!}"><?php if(isset($item->my_title)){echo $item->my_title;}else{echo $item->title;}?></a>
		
	</h5>
	<div id="group_{!!$item->group_id!!}">
		@if($week == date('W') && $year == date('Y'))
		{!!getTaskGroupTask_it($item->group_id,0,$mode)!!}
		@else
		{!!getTaskGroupTask_week_it($item->group_id,0,$mode,$week,$year)!!}
		@endif
	</div>
	<div class="add-item">
		<span class="item-plus">+</span>
	    <input onkeypress="saveNewTask(event,'group_{!!$item->group_id!!}','{!!$item->group_id!!}','','title_{!!$item->group_id!!}')" style="width:300px;border:0px;" placeholder="ایجاد فعالیت جدید" type="text" name="title" id="title_{!!$item->group_id!!}" style="border:none;">
	</div>
</div>
