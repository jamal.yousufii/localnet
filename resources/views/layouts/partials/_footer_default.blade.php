<!-- begin::Footer -->
<footer class="m-grid__item m-footer" @if(session('current_section')=="") style="margin: 0px;" @endif>
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                <span class="m-footer__copyright">	 تهیه شده توسط ریاست تکنالوژی معلوماتی - حق کاپی محفوظ است <a href="" class="m-link">&copy;  <?php echo date("Y"); ?> 	</a></span>
            </div>
        </div>
    </div>
</footer>
<!-- end::Footer -->
