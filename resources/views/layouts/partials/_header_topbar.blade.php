
<!-- BEGIN: Horizontal Menu -->
<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
	
	
</div>
<!-- END: Horizontal Menu -->
<!-- BEGIN: Topbar -->
<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
	<div class="m-stack__item m-topbar__nav-wrapper">
		<ul class="m-topbar__nav m-nav m-nav--inline">
		
			{{-- User Profile Start --}}
			<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
				<a href="#" class="m-nav__link m-dropdown__toggle">
					<span class="m-topbar__welcome"></span>
					<span class="m-topbar__username kt-menu__link-text text-default m-menu__link-text-custom">{{ Auth::user()->name }}</span>&nbsp;&nbsp;
					<span class="m-topbar__userpic">
					<img src="{!!asset('attachments/users/'.Auth::user()->profile_pic)!!}" class="m--img-rounded m--marginless m--img-centered" alt="" />
					</span>
				</a>
				<div class="m-dropdown__wrapper">
					<span class="m-dropdown__arrow m-dropdown__arrow--center m-dropdown__arrow--adjust"></span>
					<div class="m-dropdown__inner">
						<div class="m-dropdown__header m--align-center text-dark">
							<div class="m-card-user m-card-user--skin-dark">
								<div class="m-card-user__pic">
									<img src="{!!asset('attachments/users/'.Auth::user()->profile_pic)!!}" class="m--img-rounded m--marginless" alt="" />
								</div>
								<div class="m-card-user__details">
									<span class="m-card-user__name font-weight-bold m--font-weight-500 text-dark m-menu__link-text-custom">{{ Auth::user()->name }}</span>
									<span class="m-card-user__name text-dark m-menu__link-text-custom">{{ Auth::user()->position }}</span>
								</div>
							</div>
						</div>
						<hr>
						<div class="m-dropdown__body">
							<div class="m-dropdown__content">
								<ul class="m-nav m-nav--skin-light">
									<li class="m-nav__item">
										<a class="m-nav__link" href="" data-toggle="modal" data-target="#myModal">
											<i class="m-nav__link-icon flaticon-profile-1"></i>
											<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-menu__link-text-custom">{{ trans('home.my_profile') }}</span>
											</span>
											</span>
										</a>
									</li>
									<li class="m-nav__item">
										<a class="m-nav__link" href="" data-toggle="modal" data-target="#passwordPopup">
											<i class="m-nav__link-icon flaticon-lock"></i>
											<span class="m-nav__link-title">
											<span class="m-nav__link-wrap">
												<span class="m-menu__link-text-custom">{{ trans('authentication.change_pass') }}</span>
											</span>
											</span>
										</a>
									</li>
									<li class="m-nav__item">
										<a href="{{ route('logout') }}" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
										<span class="m-nav__link-text m-menu__link-text-custom">{{ trans('home.logout') }}</span>
										</a>
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</li>
			{{-- User Profile End --}}
		</ul>
	</div>
</div>
<!-- END: Topbar -->