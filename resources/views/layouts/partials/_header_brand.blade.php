<!-- BEGIN: Brand -->
<div class="m-stack__item m-brand  m-brand--skin-dark ">
	<div class="m-stack m-stack--ver m-stack--general">
		<div class="m-stack__item m-stack__item--middle m-brand__logo">
			<a href="{{ route('home',session('current_mod')) }}" class="m-brand__logo-wrapper">
				<img alt="" src="{!!asset('img/logo_title.png')!!}" width="250px"/>
			</a>
		</div>
		<div class="m-stack__item m-stack__item--middle m-brand__tools">

			<!-- BEGIN: Responsive Aside Left Menu Toggler -->
			@if(session('current_section')!="")
				<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
					<span></span>
				</a>
			@endif
			<!-- END -->

			<!-- BEGIN: Responsive Header Menu Toggler -->
			<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
				<span></span>
			</a>
			<!-- END -->

			<!-- BEGIN: Topbar Toggler -->
			<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
				<i class="flaticon-more"></i>
			</a>

			<!-- END -->
		</div>
	</div>
</div>

<!-- END: Brand -->