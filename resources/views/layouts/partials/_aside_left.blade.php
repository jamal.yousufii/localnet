<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
	<!-- BEGIN: Aside Menu -->
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
		<ul class="m-menu__nav ">
			<li class="m-menu__item  @if(session('current_section')=="") m-menu__item--active @endif" aria-haspopup="true">
				<a href="{{ route('home',session('current_mod')) }}" class="m-menu__link ">
					<span class="m-menu__item-here"></span>
					<i class="m-menu__link-icon flaticon-home-1"></i>
					<span class="m-menu__link-text">{{ trans('home.dashboard') }}</span>
				</a>
			</li>
			<?php $sections = get_module_sections(); ?>
			@if($sections)
				@foreach($sections as $sec)
					@if($sec->code!="survey" && $sec->code!="system")
						<li class="m-menu__item  @if(session('current_section')==$sec->code) m-menu__item--active @endif" aria-haspopup="true">
							<a href="{{ route($sec->url_route) }}" class="m-menu__link ">
								<span class="m-menu__item-here"></span>
								<i class="m-menu__link-icon {{$sec->icon}}"></i>
								<span class="m-menu__link-text">{!! $sec->{'name_'.$lang} !!}</span>
							</a>
						</li>
					@endif
				@endforeach
			@endif 
		</ul>
	</div>
	<!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->