<style>
    .pagination-total{
        font-size: 1.3rem;
        float:right;
    }
    .pagination-pages{
        float:left;
    }
    .page-link {
        border-radius: 10% !important;
    }
    .page-item {
        margin: 2px !important;
        padding: 0px;
    }
    .page-item a , span{
        font-size: 1rem;
    }
    .page-item.active .page-link{
        background-color: #3d3b56 !important;
    }
</style>
<?php  $link_limit = 7; ?> 
@if ($paginator->hasPages())
        <div class="row" >
            <div class="col-sm-6" style="width:20%">
                <span class="pagination-total pagination__desc">
                    Total Record: <span>{{ $paginator->count() }}</span> 
                    From: <span>{{ $paginator->total() }}</span>
                    Page: <span>{{ $paginator->currentPage() }}</span>
                </span>
            </div>  
            <div class="col-sm-6" style="width:80%">
                @if ($paginator->lastPage() > 1)
                    <ul class="pagination">
                        {{-- First Page Link --}}
                        <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                            <a id="{{ $paginator->url(1) }}" href="{{ $paginator->url(1) }}">First</a>
                        </li>
                        {{-- Previous Page Link --}}
                        <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                            <a id="{{ $paginator->currentPage()-1 }}" href="{{ $paginator->currentPage()-1 }}">Previous</a>
                        </li>
                        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                            <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                                <a id="{{$i}}" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                            </li>
                        @endfor
                        {{-- Next Page Link --}}
                        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                            <a id="{{ $paginator->currentPage()+1 }}" href="{{ $paginator->url($paginator->currentPage()+1) }}" >Next</a>
                        </li>
                        {{-- Last Page Link --}}
                        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                            <a id="{{ $paginator->lastPage() }}" href="{{ $paginator->lastPage() }}">Last</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
@endif
