@section('menu')

<div id="nav-col" class="no-print">
	<section id="col-left" class="col-left-nano">
		<div id="col-left-inner" class="col-left-nano-content">
			<div id="user-left-box" class="clearfix hidden-sm hidden-xs dropdown profile2-dropdown">
				<?php 
                    $photo = getProfilePicture(Auth::user()->id);
                ?>
				{!! HTML::image('/img/'.$photo) !!}
				<div class="user-box">
					<span class="name">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							{!!Auth::user()->username!!} 
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a href="user-profile.html"><i class="fa fa-user"></i>Profile</a></li>
							<li><a href="#"><i class="fa fa-cog"></i>Settings</a></li>
							<li><a href="#"><i class="fa fa-envelope-o"></i>Messages</a></li>
							<li><a href="#"><i class="fa fa-power-off"></i>Logout</a></li>
						</ul>
					</span>
					<span class="status">
						<i class="fa fa-circle"></i> Online
					</span>
				</div>
			</div>
			<div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">	
				<ul class="nav nav-pills nav-stacked">
					<!-- <li class="nav-header nav-header-first hidden-sm hidden-xs">
						Navigation
					</li> -->
					<?php if(Route::currentRouteName() == 'home'){ ?>
						<li class='active'>
					<?php }else{  ?>
						<li>
					<?php } ?>

					<a href="{!!URL::route('home')!!}">
						<i class="fa fa-dashboard"></i>
						<span>{!!gettext('left_dashboard')!!}</span>
					</a>

					</li>
					<?php $module_code = getCurrentAppCode(); ?>
					@if($module_code == 'task')
						{!!putTaskLeft()!!}
					@else
						{!!putLeft()!!}
					@endif
				</ul>
			</div>
		</div>
	</section>
	<div id="nav-col-submenu"></div>
</div>
@show