<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

  <!-- Core  -->
  {!! HTML::script('/vendor/jquery/jquery.js') !!}
  {!! HTML::script('/vendor/bootstrap/bootstrap.js') !!}
  
    
  {!! HTML::script('/vendor/animsition/jquery.animsition.js') !!}
  
  {!! HTML::script('/vendor/asscroll/jquery-asScroll.js') !!}
  {!! HTML::script('/vendor/mousewheel/jquery.mousewheel.js') !!}
  {!! HTML::script('/vendor/asscrollable/jquery.asScrollable.all.js') !!}
  {!! HTML::script('/vendor/ashoverscroll/jquery-asHoverScroll.js') !!}
  {!! HTML::style('/css/tasks/persian_datepicker.css') !!}
  {!! HTML::script('/js/tasks/persian_datepicker.js')!!}

  <!-- Stylesheets -->
  {!! HTML::style('css/bootstrap.min.css') !!}
  {!! HTML::style('css/yhdo_style.css') !!}
  {!! HTML::style('css/bootstrap-extend.min.css') !!}
  {!! HTML::style('css/site.min.css') !!}
  {!! HTML::style('css/new-template.css') !!}

  {!! HTML::style('css/template/libs/font-awesome.min.css') !!}

  {!! HTML::style('vendor/animsition/animsition.css') !!}
  {!! HTML::style('vendor/asscrollable/asScrollable.css') !!}
  {!! HTML::style('vendor/switchery/switchery.css') !!}
  {!! HTML::style('vendor/intro-js/introjs.css') !!}
  {!! HTML::style('vendor/slidepanel/slidePanel.css') !!}
  {!! HTML::style('vendor/flag-icon-css/flag-icon.css') !!}
  <!-- Added by Gul Muhammad -->
  {!! HTML::style('/vendor/datatables-bootstrap/dataTables.bootstrap.css') !!}
  {!! HTML::style('/vendor/datatables-fixedheader/dataTables.fixedHeader.css') !!}
  {!! HTML::style('/vendor/datatables-responsive/dataTables.responsive.css') !!}
  {!! HTML::style('/css/template/libs/datepicker.css') !!}
  {!! HTML::style('/js/datepicker/css/jquery-ui.css') !!}
  {!! HTML::style('/js/datepicker/css/datepicker.css') !!}
<!-- Added by Gul Muhammad -->
  
 
  



  <!-- Fonts -->
  {!! HTML::style('fonts/web-icons/web-icons.min.css') !!}
  {!! HTML::style('fonts/brand-icons/brand-icons.min.css') !!}
  {!! HTML::style('fonts/font-awesome/font-awesome.min.css') !!}
  {!! HTML::style('fonts/mfglabs/mfglabs.css') !!}

  <!--[if lt IE 9]>
    {!! HTML::script('/js/vendor/html5shiv/html5shiv.min.js') !!}
    <![endif]-->

  <!--[if lt IE 10]>
    {!! HTML::script('/js/vendor/media-match/media.match.min.js') !!}
    {!! HTML::script('/js/vendor/respond/respond.min.js') !!}
    <![endif]-->

  <!-- Scripts -->
  {!! HTML::script('/vendor/modernizr/modernizr.js') !!}
  {!! HTML::script('/vendor/breakpoints/breakpoints.js') !!}
  <script>
    Breakpoints();
  </script>
  
          

</head>
<body  style ="margin-top:-110px;" class="site-menubar-fold sb-top sb-top-sm" data-auto-menubar="false" onload="hidemore();" >
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  
  <!-- Page -->
  <div class="page animsition" style="margin-top:20px;">
    <div class="page-content">
      <div class="panel">
        <div class="panel-body" style="overflow:hidden;">
          @yield('content')
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->

  <!-- Footer -->
  <footer class="site-footer no-print">
    <span class="site-footer-legal">© 2017 All Rights Reserved. Powered by : MIS - AOP</span>
  </footer>

  <!-- Plugins -->
  {!! HTML::script('/vendor/switchery/switchery.min.js') !!}
  {!! HTML::script('/vendor/intro-js/intro.js') !!}
  {!! HTML::script('/vendor/screenfull/screenfull.js') !!}
  {!! HTML::script('/vendor/slidepanel/jquery-slidePanel.js') !!}
  {!! HTML::script('/vendor/jquery-placeholder/jquery.placeholder.min.js')!!}

  <!-- Scripts -->
  {!! HTML::script('/js/core.js') !!}
  {!! HTML::script('/js/site.js') !!}
  {!! HTML::script('/js/myjs.js') !!}

  {!! HTML::script('/js/sections/menu.js') !!}
  {!! HTML::script('/js/sections/menubar.js') !!}
  {!! HTML::script('/js/sections/sidebar.js') !!}

  {!! HTML::script('/js/configs/config-colors.js') !!}
  {!! HTML::script('/js/configs/config-tour.js') !!}

  {!! HTML::script('/js/components/asscrollable.js') !!}
  {!! HTML::script('/js/components/animsition.js') !!}
  {!! HTML::script('/js/components/slidepanel.js') !!}
  {!! HTML::script('/js/components/switchery.js') !!}
  {!! HTML::script('/vendor/datatables/jquery.dataTables.min.js')!!}
  {!! HTML::script('/js/components/jquery-placeholder.min.js') !!}
  {!! HTML::script('/js/components/material.min.js') !!}
  
  
  {!! HTML::script('/vendor/datatables-fixedheader/dataTables.fixedHeader.js')!!}
  {!! HTML::script('/vendor/datatables-bootstrap/dataTables.bootstrap.js')!!}
  {!! HTML::script('/vendor/datatables-responsive/dataTables.responsive.js')!!}
  {!! HTML::script('/vendor/datatables-tabletools/dataTables.tableTools.js')!!}
  {!! HTML::script('/js/components/datatables.js')!!}
  
  {!! HTML::script('/js/datepicker/js/jquery-ui.js')!!}
  {!! HTML::script('/js/datepicker/js/jquery.ui.datepicker.js')!!}
  
@yield('footer-scripts')

  <script>
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;
      $(document).ready(function() {
        Site.run();
      });
    })(document, window, jQuery);
    $( ".datepicker" ).datepicker({ changeMonth: true, dateFormat: "yy-mm-dd", changeYear: true });
  </script>

</body>

</html>