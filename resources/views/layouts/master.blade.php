<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

  <!-- Core  -->
  {!! HTML::script('/js/vendor/jquery/jquery.js') !!}
  {!! HTML::script('/js/vendor/bootstrap/bootstrap.js') !!}


  {!! HTML::script('/js/vendor/animsition/jquery.animsition.js') !!}

  {!! HTML::script('/js/vendor/asscroll/jquery-asScroll.js') !!}
  {!! HTML::script('/js/vendor/mousewheel/jquery.mousewheel.js') !!}
  {!! HTML::script('/js/vendor/asscrollable/jquery.asScrollable.all.js') !!}
  {!! HTML::script('/js/vendor/ashoverscroll/jquery-asHoverScroll.js') !!}
  {!! HTML::style('/css/tasks/persian_datepicker.css') !!}
  {!! HTML::script('/js/tasks/persian_datepicker.js')!!}

  <!-- Stylesheets -->
  {!! HTML::style('css/bootstrap.min.css') !!}
  {!! HTML::style('css/bootstrap-extend.min.css') !!}
  {!! HTML::style('css/site.min.css') !!}
  {!! HTML::style('css/new-template.css') !!}
  {!! HTML::style('css/template/libs/font-awesome.min.css') !!}
  {!! HTML::style('fonts/web-icons/web-icons.min.css') !!}
  <!--
  {!! HTML::style('fonts/fontawesome-5.1.1/css/fontawesome.min.css') !!}
  -->
  {!! HTML::style('vendor/animsition/animsition.css') !!}
  {!! HTML::style('vendor/asscrollable/asScrollable.css') !!}
  {!! HTML::style('vendor/switchery/switchery.css') !!}
  {!! HTML::style('vendor/intro-js/introjs.css') !!}
  {!! HTML::style('vendor/slidepanel/slidePanel.css') !!}
  {!! HTML::style('vendor/flag-icon-css/flag-icon.css') !!}
  <!-- Added by Gul Muhammad -->
  {!! HTML::style('/vendor/datatables-bootstrap/dataTables.bootstrap.css') !!}
  {!! HTML::style('/vendor/datatables-fixedheader/dataTables.fixedHeader.css') !!}
  {!! HTML::style('/vendor/datatables-responsive/dataTables.responsive.css') !!}
  {!! HTML::style('/css/template/libs/datepicker.css') !!}
  {!! HTML::style('/js/datepicker/css/jquery-ui.css') !!}
  {!! HTML::style('/js/datepicker/css/datepicker.css') !!}
<!-- Added by Gul Muhammad -->
<!-- Added by Mohammad Fawad Naimi -->
  {!! HTML::style('/css/schedule_db/jquery.ui.timepicker.css') !!}
  {!! HTML::style('/css/schedule_db/jquery-ui-1.10.0.custom.min.css') !!}
  {!! HTML::script('/js/schedule_db/jquery.ui.timepicker.js') !!}
<!-- Added by Mohammad Fawad Naimi Select 2 links-->

  {!! HTML::style('/css/font.css') !!}
  <!--
    {!! HTML::style('fonts/fontawesome-5.1.1/css/all.min.css') !!}
  -->
    {!! HTML::style('/vendor/select2/select2.min.css') !!}
    {!! HTML::script('/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/vendor/select2.js')!!}

    <!-- Fonts -->
  {!! HTML::style('fonts/web-icons/web-icons.min.css') !!}
  {!! HTML::style('fonts/brand-icons/brand-icons.min.css') !!}
  {!! HTML::style('fonts/font-awesome/font-awesome.min.css') !!}
  {!! HTML::style('fonts/mfglabs/mfglabs.css') !!}
  @include('assets/css/custome_css')
	<?php
	  if ((LaravelGettext::getLocale()=='ps_AF') or (LaravelGettext::getLocale()=='fa_IR')) {
	  	//cdn.rawgit.com/morteza/bootstrap-rtl/v3.3.4/dist/css/bootstrap-rtl.min.css
	  	//https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.2.0-rc2/css/bootstrap-rtl.min.css
	  ?>
	  	<style>
	  	.custom-logo{
	  		float:left !important;
	  		direction: ltr !important;
	  	}
      .fonts{
        font-family: 'B Nazanin';
      }
	  	</style>
	    {!! HTML::style('css/bootstrap-rtl.css') !!}

	  <?php
	  }
    else
    { ?>
      <style>
      .fonts{
        font-family: 'Times New Roman';
      }
      </style>
    <?php
    }
	  ?>
  <!--[if lt IE 9]>
    {!! HTML::script('/js/vendor/html5shiv/html5shiv.min.js') !!}
    <![endif]-->

  <!--[if lt IE 10]>
    {!! HTML::script('/js/vendor/media-match/media.match.min.js') !!}
    {!! HTML::script('/js/vendor/respond/respond.min.js') !!}
    <![endif]-->

  <!-- Scripts -->
  {!! HTML::script('/js/vendor/modernizr/modernizr.js') !!}
  {!! HTML::script('/js/vendor/breakpoints/breakpoints.js') !!}
  <script>
    Breakpoints();
  </script>

            @yield('head')


</head>
<body class="site-menubar-fold sb-top sb-top-sm fonts" data-auto-menubar="false">

  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

	@include('layouts.top')
  <!-- Page -->
  <div class="page animsition" style="margin-top:20px;">




    <div class="page-content">
      <div class="panel">
        <div class="panel-body" style="overflow:hidden;">
          @yield('content')
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
<div id="attendance_loading" style="display:none;z-index:100000;position: fixed;top: 30%;left: 30%;width: 50%;font-size: 40px;font-family: 'B Nazanin';background-color: white;color: black;">

  <div class="modal-dialog">
    <label class="col-sm-12 ">لطفا منتظر باشید ...</label>
  </div>
</div>
  <!-- Footer -->
  <footer class="site-footer">
    <span class="site-footer-legal">© {!!date('Y')!!} IT-MIS</span>
    <div class="site-footer-right"> by <a href="javascript:void()">AOP Information Technology Directorate - MIS</a>
    </div>
  </footer>


<!-- Profile modal -->
    <div class="modal fade modal-fade-in-scale-up" id="profile_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    	<div class="modal-dialog" style="width:70% !important;">
    		<div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        <span aria-hidden="true">×</span>
		    </button>
            <h4 class="modal-title"><i class="fa fa-user fa-lg"></i> User Profile</h4>
        </div>
        <div class="modal-body">

                <div class="row" id="user-profile">

                    <div class="col-lg-4 col-md-4 col-sm-4" style="height:453px !important;">
                        <div class="main-box clearfix">

                            <header class="main-box-header clearfix">
                                <h2>{!!Auth::user()->first_name.' '.Auth::user()->last_name!!}</span></h2>
                            </header>

                            <div class="main-box-body clearfix">

                               	<input type="radio" name="profile_picture" value='0' onclick="changeUserPhotoType(this.value)" id="hr_id" {!!(Auth::user()->use_custom_photo==0?"checked":"")!!} > <label for="hr_id">HR Photo</label>
                               	&nbsp;
                               	<input type="radio" name="profile_picture" value='1' onclick="changeUserPhotoType(this.value)" id="custom_id" {!!(Auth::user()->use_custom_photo==1?"checked":"")!!} {!!(Auth::user()->photo==''?"disabled":"")!!} > <label for="custom_id">Custom Photo</label>

                                <?php $profile_pic='default.jpeg';
                                    //$photo = getProfilePicture(Auth::user()->id);
                                if(Auth::user()->use_custom_photo==0)
                                {
                                    $photo = DB::connection('hr')->table('employees')->where('id',Auth::user()->employee_id)->pluck('photo');
                                    if(file_exists('documents/profile_pictures/'.$photo ))
                                    {
                                      $image = base64_encode(file_get_contents('documents/profile_pictures/'.$photo ));
                                      $profile_pic = 'data:image/jpg;base64,'.$image;
                                    }
                                }
                                else
                                {
                                  if(file_exists('documents/profile_pictures/'.Auth::user()->photo ))
                                  {
                                    $image = base64_encode(file_get_contents('documents/profile_pictures/'.Auth::user()->photo ));
                                    $profile_pic = 'data:image/jpg;base64,'.$image;
                                  }
                                }
                                ?>
                                <div id="user_profile_picture">
                                  <div id="hr_profile" style="display: {!!(Auth::user()->use_custom_photo==0?'block':'none')!!}">

                                  <img src="{!!$profile_pic!!}" alt="" class="profile_pic" style="width:220px;border-radius:10px;">
                                  </div>
                                  <div id="custom_profile" style="display: {!!(Auth::user()->use_custom_photo==1?'block':'none')!!}">
                                  <img src="{!!$profile_pic!!}" alt="" class="profile_pic" style="width:220px;border-radius:10px;">
                                  </div>
                                </div>

                               <br>
                               <div class="bgColor">
								<form id="uploadForm" enctype="multipart/form-data">
								<!--<div id="targetLayer">No Image</div>-->
								<div id="uploadFormLayer">
								<label>Change your profile picture:</label><br/>
								<input name="files[]" type="file" class="inputFile" id="input_file" />
								{{-- <a href="javascript:void()" class="btn btn-primary btn-xs ladda-button" onclick="updateProfile()">
									Update profile picture
								</a> --}}
							   </form>
							   </div>
							   </div>
							   <script type="text/javascript">
								function updateProfile()
								{
								    var formData = new FormData($("#uploadForm")[0]);

								    if(document.getElementById('input_file').value == "")
								    {
								    	$("#input_file").css("border","1px solid red");
								    	alert("Please select and image!");
								    	return false;
								    }

								    $.ajax({
								        url: "{!!URL::route('updateUserProfilePicture')!!}",
								        type: 'POST',
								        data: formData,
								        //async: false,
								        success: function (data) {
								            $("#user_profile_picture").html(data);
								            $("#input_file").css("border","1px solid green");
								            $("#input_file").val("");
								        },
								        cache: false,
								        contentType: false,
								        processData: false
								    });

								    return false;
								}
								function changeUserPhotoType(val)
								{

								    $.ajax({
								        url: "{!!URL::route('changeUserPhotoType')!!}",
								        type: 'POST',
								        data: "&value="+val,
								        //async: false,
								        success: function (data) {
								        	$("#btn_apply_changes").show();
								        	$("#btn_profile").remove();
								           if(val == 0)
								           {
								           	$("#hr_profile").slideDown();
								           	$("#custom_profile").slideUp();
								           }
								           else
								           {
								           	$("#custom_profile").slideDown();
								           	$("#hr_profile").slideUp();
								           }
								           /*
								           if(confirm("Your changes are done, do you want to reload the page?"))
								           {
								           		window.location.reload();
								           }
								           else
								           {
								           	return false;
								           }
								           */
								        }

								    });


								}
								</script>

                            </div>

                        </div>
                    </div>

                    <div class="col-lg-8 col-md-4 col-sm-4" style="height:453px !important;">
                        <div class="main-box clearfix" style="height:437px !important;">

                            <div class="main-box-body clearfix">

                                <div class="profile-details">

                                        <label style="font-weight:bold;padding:5px;">Username:</label> <span>{!!Auth::user()->username!!}</span>
                                        <br>
                                        <label style="font-weight:bold;padding:5px;">Full Name:</label> <span>{!!Auth::user()->first_name.' '.Auth::user()->last_name!!}</span>
                                        <br>
                                        <label style="font-weight:bold;padding:5px;">Father Name:</label> <span>{!!Auth::user()->father_name!!}</span>

                                        <br>
                                        <label style="font-weight:bold;padding:5px;">Position:</label> <span>{!!getPositionNameHR(Auth::user()->id)!!}</span>
                                        <br>
                                        <label style="font-weight:bold;padding:5px;">Department:</label> <span>{!!getDepartmentName(Auth::user()->department_id)!!}</span>
                                        <br>
                                        <label style="font-weight:bold;padding:5px;">Email:</label> <span>{!!Auth::user()->email!!}</span>
                                        <br>
                                        <label style="font-weight:bold;padding:5px;">Phone:</label> <span>{!!getEmployeePhone(Auth::user()->id)!!}</span>
                                        <br>
                                        <label style="font-weight:bold;padding:5px;">User Type:</label>
                                        <span>
                                        @if(Auth::user()->position_id==1) Normal User
                                        @elseif(Auth::user()->position_id==2) Director
                                        @elseif(Auth::user()->position_id==3) Deputy
                                        @endif
                                        </span>
                                        <br>
                                        <label style="font-weight:bold;padding:5px;">Status:</label>
                                        <span>
                                            @if(Auth::user()->status==1)
                                                <i class="fa fa-check" style="color:#8bc34a;"></i> Active
                                            @else
                                                <i class="fa fa-cancel" style="color:red;"></i> Deactivated
                                            @endif
                                        </span>


                                </div>
                                <br>
                                <div class="profile-message-btn">
                                    <a href="javascript:void()" class="btn btn-warning btn-xs ladda-button" data-target="#change_pass_modal" data-toggle="modal" onclick='$("#modal_close_profile").click();'>

                                        Change Password
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            <style>
               .bgColor label{
				font-weight: bold;
				color: #A0A0A0;
				}
				#targetLayer{
				float:left;
				width:100px;
				height:100px;
				text-align:center;
				line-height:100px;
				font-weight: bold;
				color: #C0C0C0;
				background-color: #F0E8E0;
				overflow:auto;
				}
				#uploadFormLayer{
				float:right;
				padding: 10px;
				}
				.btnSubmit {
				background-color: #3FA849;
				padding:4px;
				border: #3FA849 1px solid;
				color: #FFFFFF;
				}
				.inputFile {
				padding: 3px;
				background-color: #FFFFFF;
				}
            </style>
        </div>
        <div class="modal-footer" id='create_footer'>
            <button id="btn_profile" onclick='$("#modal_close_profile").click();' type="button" class="btn btn-danger modal-submit" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <span id="btn_apply_changes" style="display:none;">
            	<button id="btn_profile_change" onclick='window.location.reload()' type="button" class="btn btn-success modal-submit" data-dismiss="modal"><i class="fa fa-times"></i> Close & Apply Changes</button>
        	</span>
        </div>
    </div>
    </div>
</div>
<!-- end of profile modal -->
<!-- Change date modal start -->
	<div class="modal fade modal-fade-in-scale-up" id="change_pass_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    	<div class="modal-dialog">
    		<div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        <span aria-hidden="true">×</span>
		    </button>
            <h4 class="modal-title"><i class="fa fa-password fa-lg"></i> Change Your Password</h4>
        </div>
        <div class="modal-body">
            <form role="form" class='form-horizontal' id="change_pass_frm" name="change_pass_frm" style="padding-right:10px;">

                <!-- <div class="form-group">
                    <label class="col-sm-4 control-label">Old Password :</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="password" name="old_password" id="old_password">
                    </div>
                </div> -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">New Password :</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="password" name="new_password" id="new_password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Confirm Password :</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="password" name="conf_password" id="conf_password">
                        <span id="conf_pass"></span>
                    </div>
                </div>

          </form>
        </div>
        <div class="modal-footer" id='create_footer'>
            <button id="btn_change_pass" disabled onclick='changePass();$("#modal_close").click();' type="button" class="btn btn-primary modal-submit">Change Password</button>
        </div>
    </div>
    </div>
</div>
<!-- modal start end -->

<!-- Change date model -->

<!-- Change date model end -->



  <!-- Plugins -->
  {!! HTML::script('/js/vendor/switchery/switchery.min.js') !!}
  {!! HTML::script('/js/vendor/intro-js/intro.js') !!}
  {!! HTML::script('/js/vendor/screenfull/screenfull.js') !!}
  {!! HTML::script('/js/vendor/slidepanel/jquery-slidePanel.js') !!}
  {!! HTML::script('/js/vendor/jquery-placeholder/jquery.placeholder.min.js')!!}

  <!-- Scripts -->
  {!! HTML::script('/js/core.js') !!}
  {!! HTML::script('/js/site.js') !!}
  {!! HTML::script('/js/myjs.js') !!}

  {!! HTML::script('/js/sections/menu.js') !!}
  {!! HTML::script('/js/sections/menubar.js') !!}
  {!! HTML::script('/js/sections/sidebar.js') !!}

  {!! HTML::script('/js/configs/config-colors.js') !!}
  {!! HTML::script('/js/configs/config-tour.js') !!}

  {!! HTML::script('/js/components/asscrollable.js') !!}
  {!! HTML::script('/js/components/animsition.js') !!}
  {!! HTML::script('/js/components/slidepanel.js') !!}
  {!! HTML::script('/js/components/switchery.js') !!}
  {!! HTML::script('/vendor/datatables/jquery.dataTables.min.js')!!}
  {!! HTML::script('/js/components/jquery-placeholder.min.js') !!}
  {!! HTML::script('/js/components/material.min.js') !!}


  {!! HTML::script('/vendor/datatables-fixedheader/dataTables.fixedHeader.js')!!}
  {!! HTML::script('/vendor/datatables-bootstrap/dataTables.bootstrap.js')!!}
  {!! HTML::script('/vendor/datatables-responsive/dataTables.responsive.js')!!}
  {!! HTML::script('/vendor/datatables-tabletools/dataTables.tableTools.js')!!}
  {!! HTML::script('/vendor/sweetalert/sweetalert2.js')!!}
  {!! HTML::script('/js/components/datatables.js')!!}

  {!! HTML::script('/js/datepicker/js/jquery-ui.js')!!}
  {!! HTML::script('/js/datepicker/js/jquery.ui.datepicker.js')!!}

  {!! HTML::script('/js/dist/jquery.maskedinput.min.js') !!}
  {!! HTML::script('/js/dist/jquery.maskedinput.min.js') !!}
  {!! HTML::script('/js/tinymce/tinymce.min.js') !!}
  {!! HTML::script('/js/tinymce/init-tinymce.js') !!}
  {!! HTML::script('/js/ckeditor/ckeditor.js') !!}
  @include('assets/js/custom_js') 
@yield('footer-scripts')

  <script>
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;
      $(document).ready(function() {
        Site.run();
      });
    })(document, window, jQuery);
  </script>

  <script>
      $("select.select2").select2();
            $(document).ready(function(){
                $('#conf_password').on('keyup',function(){

                    var conf_pass = $("#conf_password").val();
                    var new_pass = $("#new_password").val();
                    if(conf_pass == new_pass && new_pass != '')
                    {
                        $("#conf_pass").html("<font color='green'><b>Password confirmed!</b></font>");
                        $("#btn_change_pass").removeAttr('disabled');
                    }
                    else
                    {
                        $("#conf_pass").html("<font color='red'><b>Password not matched!</b></font>");
                        //$("#conf_password").val('');
                        $("#conf_password").focus();

                    }

                });
            });
            function loadNewModule(code)
            {
                var page = "{!!URL::route('changeUserModule')!!}";
                $.ajax({
                    url: page,
                    data: '&code='+code,
                    type: 'post',
                    success:function()
                    {
                        window.location.href='{!!URL::route("home")!!}';
                    }
                });
            }
        <?php if(Auth::check()): ?>
            //change start and end date
            function changePass()
            {

                var page = "{!!URL::route('changePassword')!!}";

                $.ajax({
                    url: page,
                    type: 'post',
                    data: $('#change_pass_frm').serialize(),
                    //dataType:'HTML',
                    success: function(response)
                    {
                        alert(response);
                    },
                    error:function(err)
                    {
                        alert(err);
                    }
                });

                return false;

            }
        <?php endif; ?>


        function changeDateSetting(type)
		{

                var page = "{!!URL::route('changeDateSetting')!!}";
                $.ajax({
                    url: page,
                    data: '&date='+type,
                    type: 'post',
                    //dataType: 'json',
                    success:function(r)
                    {
	                    window.location.reload();
                    }
                });
        }
 function loadloader()
  {
    $('.panel-body').css('opacity','0.4');
    $('.panel-body').css('pointer-events','none');
    $('#attendance_loading').show();
  }
        $( ".datepicker" ).datepicker({ changeMonth: true, dateFormat: "yy-mm-dd", changeYear: true });
        </script>


</body>

</html>
