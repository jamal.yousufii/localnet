@if (Session::has('success'))
  <div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show col-lg-12" role="alert">
    <div class="m-alert__icon"><i class="la la-check-square"></i></div>
    <div class="m-alert__text">
      {!! Session::get('success') !!}
      @if(Session::has('att_failed'))<br><span style="color:red">{!! Session::get('att_failed') !!}</span>@endif
    </div>
    <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
  </div>
@elseif (Session::has('fail'))
  <div class="m-alert m-alert--icon m-alert--outline alert alert-warning alert-dismissible fade show col-lg-12" role="alert">
    <div class="m-alert__icon"><i class="la la-warning"></i></div>
    <div class="m-alert__text">{!! Session::get('fail') !!}</div>
    <div class="m-alert__close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"></button></div>
  </div>
@endif