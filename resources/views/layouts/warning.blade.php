@extends('layouts.master')

@section('head')
<title>Access denied</title>
@stop

@section('content')

<div class="row">
	
	<br>
	<div class="row">
		<div class="col-xs-12 text-center page-404">
			<div style='margin:20px;'>
				<h4 class='alert alert-danger'>{{Auth::user()->first_name}} {{Auth::user()->last_name}},
				شما اجازه دسترسی به این بخش را ندارید
				</h3>
			
				<img src="/images/access-denied.jpg" alt="Access denied"/>
				
			</div>
		</div>
	</div>

</div>
@stop

