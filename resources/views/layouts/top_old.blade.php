@section('top_bar')
<?php //echo LaravelGettext::setDomain('dashboard');?>
<style type="text/css">
    /*.normal-logo{
        width: 150px;
        height: 60px;
    }*/
.notification-logo {
	border-radius: 50%;
	background-clip: padding-box;
	display: block;
	float: left;
	height: 40px;
	padding: 3px;
	overflow: hidden;
	width: 40px;
}
.dropdown-menu>li {direction:ltr !important;}
.custom-logo {
	position:absolute;
}

.navbar-right{
	position: absolute;
	right: 8px;
	top: 0px;
}
</style>

  <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" style="direction:ltr;" role="navigation">

    <div class="navbar-container container-fluid" style="min-height: 66px;">
      <!-- Navbar Collapse -->
      <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
      <div class="col-md-1 custom-logo">
    	 {!! HTML::image('/img/logo.png', 'Logo', array('class' => 'normal-logo logo-white')) !!}
      </div>
		  
    <aside id="sidebar_left" class="col-md-8 col-md-offset-1">
		  
      <!-- Start: Sidebar Left Content -->
      <div class="sidebar-left-content nano-content">

        <!-- Start: Sidebar Menu -->
        <ul class="nav sidebar-menu">
          <li class="dashboard">
            <a href="/">
              <span class="glyphicon glyphicon-home"></span>
              <span class="sidebar-title">Dashboard</span>
            </a>
          </li>
		<?php 
		    if(count(Session::get('user_modules'))>0)
		    {
		        $modules = Session::get('user_modules');
		        $moduleNames = Session::get('module_names');
		        if(Request::segment(1) != '')
		        {
		        	?>
		        		<style>.dashboard{background: #465258;}</style>
		        	<?php
		        	setTopAsAppmenu(getCurrentAppCode());
		        }
		        else
		        {
			        for($i=0;$i<count($modules);$i++)
			        {
			
			            ?>
			                <li>
					            <a class="accordion-toggle" href="{!!URL::route('changeUserModule',$modules[$i])!!}">
					              <!--<i class="fa fa-database"></i> -->
					              <span class="sidebar-title">{!!$moduleNames[$modules[$i]]!!}</span>
					              <!--<span class="menu-down fa fa-angle-down"></span>-->
					            </a>
					            <ul class="nav sub-nav">
	
					              
					              {!!getSubMenus($modules[$i])!!}
					              
					            </ul>
	 			            </li>
	
			            <?php
			        }
		        }
		    }
		    else
		    {
		        echo "<option>No Application</option>";
		    }
		?>



        </ul>
        <!-- End: Sidebar Menu -->

      </div>
      <!-- End: Sidebar Left Content -->

    </aside>
    


		  
		  
		  
		  
		  
		  

        <!-- End Navbar Toolbar -->

        <!-- Navbar Toolbar Right -->
        <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
        <!--
          <li class="hidden-float">
            <a class="icon wb-search" data-toggle="collapse" href="#site-navbar-search" role="button">
              <span class="sr-only">Toggle Search</span>
            </a>
          </li>
        -->
		  <li class="dropdown">
            <li class="dropdown hidden-xs">
                @if(isMiladiDate())
                <a class="btn" href="javascript:void()" onclick="changeDateSetting('shamsi')"><i class="fa fa-calendar" aria-hidden="true"></i> Miladi</a>
                @else
                <a class="btn" href="javascript:void()" onclick="changeDateSetting('miladi')">شمسی <i class="fa fa-calendar" aria-hidden="true"></i></a>
                @endif
            </li>
          </li>
          <li class="dropdown">

						<?php
                            $langs = array();
                            foreach (Config::get('laravel-gettext.supported-locales') as $lang) 
                            {
                                if($lang === 'en_US')
                                {
                                    $langs[$lang] = "Eng";
                                }
                                elseif ($lang === 'fa_IR') 
                                {
                                    $langs[$lang] = "دری";
                                }
                                elseif ($lang === 'ps_AF') 
                                {
                                    $langs[$lang] = "پشتو";
                                }
                               
                            }
                            
                        ?>

                        <li class="dropdown hidden-xs">
                            <a class="btn dropdown-toggle" data-toggle="dropdown">
                                {!!$langs[LaravelGettext::getLocale()]!!} 
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                @foreach($langs as $key => $value)
                                    <li class="item"><a href="{{URL::route('changeLang',$key)}}">{!!$value!!}</a></li>
                                @endforeach
                            </ul>
                        </li>


          </li>
          <li class="dropdown">
            <a class="navbar-avatar dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
            data-animation="slide-bottom" role="button">
              <span class="avatar avatar-online">
                <?php 
                    $photo = getProfilePicture(Auth::user()->id);
                    if (!file_exists('/var/www/localnet/public/documents/profile_pictures/small_'.$photo))
                    {
	                    $img = Image::make('/var/www/localnet/public/documents/profile_pictures/'.$photo);
						// crop image
						$img->crop(1278, 1597,897,80)->fit(100,100)->save('/var/www/localnet/public/documents/profile_pictures/small_'.$photo);
						//HTML::image('/documents/profile_pictures/'.$photo, Auth::user()->username)
                    }
                    
                ?>

               @if(Auth::user()->use_custom_photo == 1)
	               {!! HTML::image('/documents/profile_pictures/'.$photo, Auth::user()->username) !!}
	            
	            @else
	            	{!! HTML::image('/documents/profile_pictures/small_'.$photo, Auth::user()->username) !!}
	            @endif
               <b class="caret"></b>

              </span>
            </a>
            <ul class="dropdown-menu" role="menu">
	            <li><a href="javascript:void()" data-toggle="modal" data-target="#profile_modal"><i class="icon wb-user" aria-hidden="true"></i> {!!_('user_profile')!!}</a></li>
	            <li><a href="javascript:void()" data-toggle="modal" data-target="#change_pass_modal"><i class="icon wb-settings"></i> {!!_('changepassword')!!}</a></li>
	            
            </ul>
          </li>
          
          
        <li class="hidden-xxs">
            <a href="{!!URL::route('getLogout')!!}" class="btn">
                <i class="fa fa-power-off"></i>
            </a>
        </li>

        </ul>
        <!-- End Navbar Toolbar Right -->
      </div>
      <!-- End Navbar Collapse -->

      <!-- Site Navbar Seach -->
      <div class="collapse navbar-search-overlap" id="site-navbar-search">
        <form role="search">
          <div class="form-group">
            <div class="input-search">
              <i class="input-search-icon wb-search" aria-hidden="true"></i>
              <input type="text" class="form-control" name="site-search" placeholder="Search...">
              <button type="button" class="input-search-close icon wb-close" data-target="#site-navbar-search"
              data-toggle="collapse" aria-label="Close"></button>
            </div>
          </div>
        </form>
      </div>
      <!-- End Site Navbar Seach -->
    </div>
  </nav>
    
    
    
@show