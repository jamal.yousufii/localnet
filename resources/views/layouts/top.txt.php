@section('top_bar')
{!!LaravelGettext::setDomain('dashboard')!!}
<style type="text/css">
    /*.normal-logo{
        width: 150px;
        height: 60px;
    }*/
</style>
    <header class="navbar no-print" id="header-navbar">
            <div class="container">
                <a href="#" id="logo" class="navbar-brand">
                    
                    
                    {!! HTML::image('/img/logo.png', 'Logo', array('class' => 'normal-logo logo-white')) !!}
                    <!-- <img src="img/logo-black.png" alt="" class="normal-logo logo-black"/>
                    <img src="img/logo-small.png" alt="" class="small-logo hidden-xs hidden-sm hidden"/> -->
                </a>
                
                <div class="clearfix">
                <button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="fa fa-bars"></span>
                </button>
                
                <div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
                    <ul class="nav navbar-nav pull-left">
                        <li>
                            <a class="btn" id="make-small-nav">
                                <i class="fa fa-bars"></i>
                            </a>
                        </li>
                        <li class="dropdown hidden-xs">
                            <a class="btn dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell"></i>
                                <span class="count">{!!getTaskNotifications(true)!!}</span>
                            </a>
                            <ul class="dropdown-menu notifications-list messages-list">
                                <li class="pointer">
                                    <div class="pointer-inner">
                                        <div class="arrow"></div>
                                    </div>
                                </li>
                                <li class="item-header">{!!gettext('have_notify')!!} {!!getTaskNotifications(true)!!} {!!gettext('new_notify')!!}</li>
                                
                                {!!getTaskNotifications()!!}
                                
                                <li class="item-footer">
                                    <a href="#">
                                        {!!gettext('all_notifications')!!}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown hidden-xs">
                            <!-- <a href="" class="btn" title="My Inbox">
                                <i class="fa fa-envelope-o"></i>
                                <span class="count">{!!getUnreadEmails()!!}</span>
                            </a> -->
                            <!-- <ul class="dropdown-menu notifications-list messages-list">
                                <li class="pointer">
                                    <div class="pointer-inner">
                                        <div class="arrow"></div>
                                    </div>
                                </li>
                                <li class="item first-item">
                                    <a href="#">
                                        <img src="img/samples/messages-photo-1.png" alt=""/>
                                        <span class="content">
                                            <span class="content-headline">
                                                George Clooney
                                            </span>
                                            <span class="content-text">
                                                Look, just because I don't be givin' no man a foot massage don't make it 
                                                right for Marsellus to throw...
                                            </span>
                                        </span>
                                        <span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="#">
                                        <img src="img/samples/messages-photo-2.png" alt=""/>
                                        <span class="content">
                                            <span class="content-headline">
                                                Emma Watson
                                            </span>
                                            <span class="content-text">
                                                Look, just because I don't be givin' no man a foot massage don't make it 
                                                right for Marsellus to throw...
                                            </span>
                                        </span>
                                        <span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
                                    </a>
                                </li>
                                <li class="item">
                                    <a href="#">
                                        <img src="img/samples/messages-photo-3.png" alt=""/>
                                        <span class="content">
                                            <span class="content-headline">
                                                Robert Downey Jr.
                                            </span>
                                            <span class="content-text">
                                                Look, just because I don't be givin' no man a foot massage don't make it 
                                                right for Marsellus to throw...
                                            </span>
                                        </span>
                                        <span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
                                    </a>
                                </li>
                                <li class="item-footer">
                                    <a href="#">
                                        View all messages
                                    </a>
                                </li>
                            </ul> -->
                        </li>
                        <li class="dropdown hidden-xs">
                            <a class="btn dropdown-toggle" data-toggle="dropdown">
                                {!!getDefaultModuleName()!!}
                                <i class="fa fa-caret-down"></i>
                            </a>
                            


                            
                            <ul class="dropdown-menu">
                                <?php 
                                    if(count(Session::get('user_modules'))>0)
                                    {
                                        $modules = Session::get('user_modules');
                                        $moduleNames = Session::get('module_names');
                                        for($i=0;$i<count($modules);$i++)
                                        {

                                            ?>
                                                <li class="item">
                                                    <a href="{!!URL::route('changeUserModule',$modules[$i])!!}">
                                                        <i class="fa fa-database"></i> 
                                                        <?=$moduleNames[$modules[$i]]?>
                                                    </a>
                                                </li>
                                            <?php
                                        }
                                    }
                                    else
                                    {
                                        echo "<option>No Application</option>";
                                    }
                                ?>
                                
                            </ul>
                        </li>
                        <?php
                            $langs = array();
                            foreach (Config::get('laravel-gettext.supported-locales') as $lang) 
                            {
                                if($lang === 'en_US')
                                {
                                    $langs[$lang] = "English";
                                }
                                elseif ($lang === 'fa_IR') 
                                {
                                    $langs[$lang] = "دری";
                                }
                                elseif ($lang === 'ps_AF') 
                                {
                                    $langs[$lang] = "پشتو";
                                }
                               
                            }
                            
                        ?>

                        <li class="dropdown hidden-xs">
                            <a class="btn dropdown-toggle" data-toggle="dropdown">
                                {!!$langs[LaravelGettext::getLocale()]!!}
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                @foreach($langs as $key => $value)
                                    <li class="item"><a href="{{URL::route('changeLang',$key)}}">{!!$value!!}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </div>
                
                <div class="nav-no-collapse pull-right" id="header-nav">
                    <ul class="nav navbar-nav pull-right">
                        <!-- <li class="mobile-search">
                            <a class="btn">
                                <i class="fa fa-search"></i>
                            </a>
                            
                            <div class="drowdown-search">
                                <form role="search">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <i class="fa fa-search nav-search-icon"></i>
                                    </div>
                                </form>
                            </div>
                            
                        </li> -->
                        <li class="dropdown profile-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php 
                                    $photo = getProfilePicture(Auth::user()->id);
                                ?>

                               {!! HTML::image('/documents/profile_pictures/'.$photo) !!}
                                <span class="hidden-xs">{!!Auth::user()->username!!}</span> <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="javascript:void()" class="md-trigger" data-modal="profile_modal"><i class="fa fa-user"></i>{!!gettext('top_user_profile')!!}</a></li>
                                <li><a href="javascript:void()" class="md-trigger" data-modal="change_pass_modal"><i class="fa fa-cog"></i>{!!gettext('top_user_changepassword')!!}</a></li>
                                <li><a href="javascript:void()" class="md-trigger" data-modal="change_date_modal"><i class="fa fa-calendar"></i>{!!gettext('date_setting')!!}</a></li>
                                <!-- <li><a href="#"><i class="fa fa-envelope-o"></i>Messages</a></li> -->
                                <li><a href="{!!URL::route('getLogout')!!}"><i class="fa fa-power-off"></i>{!!gettext('top_logout')!!}</a></li>
                            </ul>
                        </li>
                        <li class="hidden-xxs">
                            <a href="{!!URL::route('getLogout')!!}" class="btn">
                                <i class="fa fa-power-off"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                </div>
            </div>
        </header>
    <!--End Header-->
@show
