<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

  <!-- Core  -->
  {!! HTML::script('/js/vendor/jquery/jquery.js') !!}
  {!! HTML::script('/js/vendor/bootstrap/bootstrap.js') !!}
  
    
  {!! HTML::script('/js/vendor/animsition/jquery.animsition.js') !!}
  
  {!! HTML::script('/js/vendor/asscroll/jquery-asScroll.js') !!}
  {!! HTML::script('/js/vendor/mousewheel/jquery.mousewheel.js') !!}
  {!! HTML::script('/js/vendor/asscrollable/jquery.asScrollable.all.js') !!}
  {!! HTML::script('/js/vendor/ashoverscroll/jquery-asHoverScroll.js') !!}
  {!! HTML::style('/css/tasks/persian_datepicker.css') !!}
  {!! HTML::script('/js/tasks/persian_datepicker.js')!!}

  <!-- Stylesheets -->
  {!! HTML::style('css/bootstrap.min.css') !!}
  {!! HTML::style('css/bootstrap-extend.min.css') !!}
  {!! HTML::style('css/site.min.css') !!}
  {!! HTML::style('css/new-template.css') !!}

  {!! HTML::style('css/template/libs/font-awesome.min.css') !!}

  {!! HTML::style('vendor/animsition/animsition.css') !!}
  {!! HTML::style('vendor/asscrollable/asScrollable.css') !!}
  {!! HTML::style('vendor/switchery/switchery.css') !!}
  {!! HTML::style('vendor/intro-js/introjs.css') !!}
  {!! HTML::style('vendor/slidepanel/slidePanel.css') !!}
  {!! HTML::style('vendor/flag-icon-css/flag-icon.css') !!}
  {!! HTML::style('vendor/bootstrap-datepicker/bootstrap-datepicker.min.css') !!}
  <!-- Added by Gul Muhammad -->
  {!! HTML::style('/vendor/datatables-bootstrap/dataTables.bootstrap.css') !!}
  {!! HTML::style('/vendor/datatables-fixedheader/dataTables.fixedHeader.css') !!}
  {!! HTML::style('/vendor/datatables-responsive/dataTables.responsive.css') !!}
  {!! HTML::style('/css/template/libs/datepicker.css') !!}
  {!! HTML::style('/js/datepicker/css/jquery-ui.css') !!}
  {!! HTML::style('/js/datepicker/css/datepicker.css') !!}
<!-- Added by Gul Muhammad -->
  

  <!-- Fonts -->
  {!! HTML::style('fonts/web-icons/web-icons.min.css') !!}
  {!! HTML::style('fonts/brand-icons/brand-icons.min.css') !!}
  {!! HTML::style('fonts/mfglabs/mfglabs.css') !!}

  <!--[if lt IE 9]>
    {!! HTML::script('/js/vendor/html5shiv/html5shiv.min.js') !!}
    <![endif]-->

  <!--[if lt IE 10]>
    {!! HTML::script('/js/vendor/media-match/media.match.min.js') !!}
    {!! HTML::script('/js/vendor/respond/respond.min.js') !!}
    <![endif]-->

  <!-- Scripts -->
  {!! HTML::script('/js/vendor/modernizr/modernizr.js') !!}
  {!! HTML::script('/js/vendor/breakpoints/breakpoints.js') !!}
  <script>
    Breakpoints();
  </script>
  
            @yield('head')

</head>
<body class="site-menubar-fold sb-top sb-top-sm" data-auto-menubar="false">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

  <!-- Page -->
  <div style="margin:10px">
    <!--<div class="page-content">
      <div class="panel" style="border:1px solid #ddd">
        <div class="panel-body" style="overflow:hidden;">
          @yield('contents')
        </div>
      </div>
    </div>-->
    @yield('contents')
  </div>
  <!-- End Page -->

  
<!-- Change date modal start -->
	<div class="modal fade modal-fade-in-scale-up" id="change_pass_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    	<div class="modal-dialog">
    		<div class="modal-content">
    
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        <span aria-hidden="true">×</span>
		    </button>
            <h4 class="modal-title"><i class="fa fa-password fa-lg"></i> Change Your Password</h4>
        </div>
        <div class="modal-body">
            <form role="form" class='form-horizontal' id="change_pass_frm" name="change_pass_frm" style="padding-right:10px;">
                
                <!-- <div class="form-group">
                    <label class="col-sm-4 control-label">Old Password :</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="password" name="old_password" id="old_password">
                    </div>
                </div> -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">New Password :</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="password" name="new_password" id="new_password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Confirm Password :</label>
                    <div class="col-sm-8">
                        <input class="form-control" type="password" name="conf_password" id="conf_password">
                        <span id="conf_pass"></span>
                    </div>
                </div>
                
          </form>
        </div>
        <div class="modal-footer" id='create_footer'>
            <button id="btn_change_pass" disabled onclick='changePass();$("#modal_close").click();' type="button" class="btn btn-primary modal-submit">Change Password</button>
        </div>
    </div>
    </div>
</div>
<!-- modal start end -->

<!-- Change date model -->
    <div class="modal fade modal-fade-in-scale-up" id="change_calendar_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    	<div class="modal-dialog">
    		<div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        <span aria-hidden="true">×</span>
		    </button>
            <h4 class="modal-title"><i class="fa fa-calendar fa-lg"></i> Date Setting</h4>
            
        </div>
        <div class="modal-body">
            
            <div class="form-group" style="margin-left:20px;">
                
                <div class="radio">
                    <input name="date_setting" id="shamsi_date" value="shamsi" type="radio" <?=(isShamsiDate() ? 'checked':'')?>>
                    <label for="shamsi_date">
                        Shamsi Date
                    </label>
                </div>
                <div class="radio">
                    <input name="date_setting" id="miladi_date" value="miladi" type="radio" <?=(isMiladiDate() ? 'checked':'')?>>
                    <label for="miladi_date">
                        Miladi Date
                    </label>
                </div>
                
            </div>
        </div>
        <div class="modal-footer" id='create_footer'>
            <span class="alert alert-success" style="display:none;" id="info_msg"></span>
            
        </div>
    </div>
    </div>
</div>
<!-- Change date model end -->
        


  <!-- Plugins -->
  {!! HTML::script('/js/vendor/switchery/switchery.min.js') !!}
  {!! HTML::script('/js/vendor/intro-js/intro.js') !!}
  {!! HTML::script('/js/vendor/screenfull/screenfull.js') !!}
  {!! HTML::script('/js/vendor/slidepanel/jquery-slidePanel.js') !!}
  {!! HTML::script('/js/vendor/jquery-placeholder/jquery.placeholder.min.js')!!}

  <!-- Scripts -->
  {!! HTML::script('/js/core.js') !!}
  {!! HTML::script('/js/site.js') !!}
  {!! HTML::script('/js/myjs.js') !!}

  {!! HTML::script('/js/sections/menu.js') !!}
  {!! HTML::script('/js/sections/menubar.js') !!}
  {!! HTML::script('/js/sections/sidebar.js') !!}

  {!! HTML::script('/js/configs/config-colors.js') !!}
  {!! HTML::script('/js/configs/config-tour.js') !!}

  {!! HTML::script('/js/components/asscrollable.js') !!}
  {!! HTML::script('/js/components/animsition.js') !!}
  {!! HTML::script('/js/components/slidepanel.js') !!}
  {!! HTML::script('/js/components/switchery.js') !!}
  {!! HTML::script('/js/components/panel.js') !!}
 
  {!! HTML::script('/vendor/datatables/jquery.dataTables.min.js')!!}
  {!! HTML::script('/js/components/jquery-placeholder.min.js') !!}
  {!! HTML::script('/js/components/material.min.js') !!}
  
  
  {!! HTML::script('/vendor/datatables-fixedheader/dataTables.fixedHeader.js')!!}
  {!! HTML::script('/vendor/datatables-bootstrap/dataTables.bootstrap.js')!!}
  {!! HTML::script('/vendor/datatables-responsive/dataTables.responsive.js')!!}
  {!! HTML::script('/vendor/datatables-tabletools/dataTables.tableTools.js')!!}
  {!! HTML::script('/js/components/datatables.js')!!}
  
  
  {!! HTML::script('/js/datepicker/js/jquery-ui.js')!!}
  {!! HTML::script('/js/datepicker/js/jquery.ui.datepicker.js')!!}


  
  
  
@yield('footer-scripts')

  <script>
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;
      $(document).ready(function() {
        Site.run();
      });
    })(document, window, jQuery);
  </script>
  
  <script>
            $(document).ready(function(){
                $('#conf_password').on('blur',function(){
                  
                    var conf_pass = $("#conf_password").val();
                    var new_pass = $("#new_password").val();
                    if(conf_pass == new_pass && new_pass != '')
                    {
                        $("#conf_pass").html("<font color='green'><b>Password confirmed!</b></font>");
                        $("#btn_change_pass").removeAttr('disabled');
                    }
                    else
                    {
                        $("#conf_pass").html("<font color='red'><b>Password not matched!</b></font>");
                        $("#conf_password").val('');
                        $("#conf_password").focus();

                    }

                });
            });
            function loadNewModule(code)
            {
                var page = "{!!URL::route('changeUserModule')!!}";
                $.ajax({
                    url: page,
                    data: '&code='+code,
                    type: 'post',
                    success:function()
                    {
                        window.location.href='{!!URL::route("home")!!}';
                    }
                });
            }
        <?php if(Auth::check()): ?>
            //change start and end date
            function changePass()
            {

                var page = "{!!URL::route('changePassword')!!}";
                
                $.ajax({
                    url: page,
                    type: 'post',
                    data: $('#change_pass_frm').serialize(),
                    //dataType:'HTML',
                    success: function(response)
                    {
                        alert(response);
                    },
                    error:function(err)
                    {
                        alert(err);
                    }
                });

                return false;
                
            }
        <?php endif; ?>


        $(document).ready(function(){

            $("#shamsi_date").on("click",function(){
                var page = "{!!URL::route('changeDateSetting')!!}";
                $.ajax({
                    url: page,
                    data: '&date='+this.value,
                    type: 'post',
                    dataType: 'json',
                    success:function(r)
                    {
                    	window.location.reload();
                        //$("#info_msg").show();
                        //$("#info_msg").html("Date Setting Changed");
                    }
                });
            });
            $("#miladi_date").on("click",function(){
                var page = "{!!URL::route('changeDateSetting')!!}";
                $.ajax({
                    url: page,
                    data: '&date='+this.value,
                    type: 'post',
                    dataType: 'json',
                    success:function(r)
                    {
                    	window.location.reload();
                        //$("#info_msg").show();
                        //$("#info_msg").html("Date Setting Changed");
                    }
                });
            });


        });
        
        $( ".datepicker" ).datepicker({ changeMonth: true, dateFormat: "yy-mm-dd", changeYear: true });
        </script>

</body>

</html>