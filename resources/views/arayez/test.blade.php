@extends('layouts.master')
@section('head')
	@parent
	{{ HTML::style('/css/tree/easyui.css') }}
	{{ HTML::style('/css/tree/icon.css') }}
	
	{{ HTML::script('/js/jquery.easyui.min.js') }}
	
	<title>Department</title>
@stop

@include('layouts.top')
@include('layouts.menu')

@section('top_bar')
@stop


@section('content')
	
	<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		
		<ol class="breadcrumb pull-left">
			<li><a href="#">Dashboard</a></li>
			<li><a href="#">Department</a></li>
		</ol>
		
	</div>
</div>