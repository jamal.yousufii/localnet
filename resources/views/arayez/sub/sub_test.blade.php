@extends('layouts.master')
@section('head')
	@parent
	{{ HTML::style('/css/datatable/jquery.dataTables.css') }}
	{{ HTML::script('/js/datatable/jquery.dataTables.min.js') }}
	<title>Department</title>
@stop

@include('layouts.top')
@include('layouts.menu')

@section('top_bar')
@stop


@section('content')
	
	<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		
		<ol class="breadcrumb pull-left">
			<li><a href="#">Dashboard</a></li>
			<li><a href="#">Department</a></li>
		</ol>
		
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box ui-draggable ui-droppable">
			<div class="box-header">
				<div class="box-name ui-draggable-handle">
					<i class="fa fa-sitemap"></i>
					<span>Department List</span>
				</div>
				
				<div class="no-move"></div>
			</div>
			<div class="box-content no-padding">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-condensed table-bordered" id="articles">
					<thead>
					<tr>
					<th>ID</th>
					<th>Code</th>
					<th>Name</th>
					<th>Description</th>
					<th>Created At</th>
					<th>Updated At</th>
					<th>User</th>
					</tr>
					</thead>
					<tbody>
					
					</tbody>
				</table> 
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#articles').dataTable( 
	{
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "{{URL::action('arayez\Test@getView')}}",
		"aaSorting": [[ 3, "desc" ]],
		"aoColumns": [
		{ 'sWidth': '60px' },
		{ 'sWidth': '130px', 'sClass': 'center' },
		{ 'sWidth': '180px', 'sClass': 'center' },
		{ 'sWidth': '60px', 'sClass': 'center' },
		{ 'sWidth': '90px', 'sClass': 'center' },
		{ 'sWidth': '80px', 'sClass': 'center' },
		{ 'sWidth': '80px', 'sClass': 'center' }
		]
	}
); 
</script>
@stop