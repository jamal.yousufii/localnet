@extends('layouts.master')

@section('head')
    @parent
    
    <title>جستجوی جلسات</title>
    <style type="text/css">
        .fixed{width: 11%;}
    </style>
    {!! HTML::style('/css/font.css') !!}
    {!! HTML::style('/css/print.css', array('media' => 'print')) !!}

@stop


@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success span6 noprint'>{!! Session::get('success') !!}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger span6 noprint'>{!! Session::get('fail') !!}</div>
    @endif
    
    <div dir="rtl" class="noprint">  
    
    	<h3>جلسات را به اساس تاریخ ، نوع و سکتور آن جستجو نمائید</h3>
    	<hr />
    	<form id="searchForm">

	    	<div class="form-group">

                <div class="col-sm-2">
                    <div class="col-sm-12">
	                   <label class="col-sm-12 control-label">تاریخ آغاز</label>
                    </div>
	                <div class="col-sm-12">
	            	  <input class="datepicker_farsi form-control" type="text" name="s_date" value="{!!old('s_date')!!}" readonly="readonly">
                    </div>
	            </div>

                <div class="col-sm-2">
                    <div class="col-sm-12">
	                   <label class="col-sm-12 control-label">تاریخ ختم</label>
                    </div>
	                <div class='col-sm-12'>
	            	  <input class="datepicker_farsi form-control" type="text" name="e_date" value="{!!old('e_date')!!}" readonly="readonly">
                    </div>
	            </div>

	            <div class="col-sm-3">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">نوع جلسه</label>
                    </div>
                    <div class="col-sm-12">
                        <select name="meeting_type" class="form-control" id="meeting_type_list" style="width:100%;">
                            <option value="">از لست انتخاب کنید</option>
                            @foreach($meeting_type AS $item)
                                @if($item->name == old('meeting_type'))
                                    <option value='{!!$item->id!!}' selected="selected">{!!$item->name!!}</option>  
                                @else
                                    <option value='{!!$item->id!!}'>{!!$item->name!!}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="col-sm-12">
		       	      <label class="col-sm-1 control-label">سکتور</label>
                    </div>
	                <div class="col-sm-12">
    		            <select name="sector[]" class="form-control" id="sector" style="width:100%;" multiple="multiple">
    			            @foreach($sectors AS $item)
    			                @if($item->name == old('sector'))
    			                    <option value='{!!$item->id!!}' selected="selected">{!!$item->name!!}</option>  
    			                @else
    			                    <option value='{!!$item->id!!}'>{!!$item->name!!}</option>
    			                @endif
    			            @endforeach
    			        </select>
                    </div>
		       	</div>

		       	<div class="form-group">
	                <div class="col-sm-12">
	                    <hr style="border: 1px dashed #b6b6b6" />
	                    <button class="btn btn-primary" type="submit">
	                        <span>
	                            <i class="fa fa-search"></i>
	                        </span>
	                        &nbsp; جستجو
	                    </button>
		              	<input type="reset" class="btn btn-danger" id="searchclear" value="پاک کردن">  
                          
		              	<!-- <button type="button" class="btn btn-success" onclick="printSearchResultToExcel()" id="print">
	                        <span>
	                            <i class="fa fa-print"></i>
	                        </span>
	                        &nbsp;چاپ اکسل
	                    </button> -->
	                </div>
	            </div>
			
        	</div>
        </form>
    </div>
    <div id="search_result" style="margin:20px">
    </div>
    
@stop
@section('footer-scripts')

<script type="text/javascript">

    $("#meeting_type_list").select2();
    $("#sector").select2();

    $(function(){
        $("#searchclear").click(function(){
          $("#meeting_type_list").select2('val', '');
          $("#sector").select2('val', '');
        });
      });

	function printSearchResultToExcel()
    {
        var dataString = $('#searchForm').serialize();
        $('#print').html('Processing ...');
        //window.location("{!!URL::route('exportToExcel')!!}");
        $.ajax({
            type: "POST",
            url: "{!!URL::route('exportToExcel')!!}",
            data: dataString,
            success: function(mydata){
                //$("#deleted_result").html(mydata);
            }
        });
        return false;
    }

    $("#searchForm").submit(function(){
    	var dataString = $("#searchForm").serialize();
    	$.ajax({
    		url : "{!!URL::route('postSearchMeetings')!!}",	
    		type : "POST",
    		data : dataString,
    		beforeSend: function(){
                $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#search_result').html(response);
            }
        });
        return false;
    });

</script>

@stop


