
    <?php 
        use App\library\jdatetime;
        // now convert it to Jalali to get previous and next months through ajax.
        $jalali_sdate = convertToJalali($s_date);
        $jalali_edate = convertToJalali($e_date);

    ?>
        @if(count(array_filter($weeklyMeetings)) != '')
        <!-- get one week start and end date both in jalali and gregorian using ajax. -->
        <!-- <i id="date_duration"></i> -->
        <input type='hidden' id='s_date' value="{!!$s_date!!}" />
        <input type='hidden' id='e_date' value="{!!$e_date!!}" />

        <table class="table table-bordered" id="list_of_weeklyMeetings">
            <thead>
              <tr>
                <!-- <th>شماره</th> -->
                <!-- <th>تاریخ</th> -->
                <!-- <th>آغاز جلسه</th>
                <th>ختم جلسه</th> -->
                <th>شنبه<br /><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'0')!!}</span> <br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'0')!!}</span></th>
                <th>یکشنبه<br><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'1')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'1')!!}</span></th>
                <th>دوشنبه<br><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'2')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'2')!!}</span></th>
                <th>سه شنبه<br><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'3')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'3')!!}</span></th>
                <th>چهارشنبه<br><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'4')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'4')!!}</span></th>
                <th>پنجشنبه<br><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'5')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'5')!!}</span></th>
                <th>جمعه<br><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'6')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'6')!!}</span></th>
                <!-- <th colspan="2" class="noprint">عملیات</th> -->
                
              </tr>
            </thead>
            <tbody>
                <tr>
                @foreach ($weeklyMeetings as $meeting) 
                    <td class="fixed">
                    @foreach ($meeting as $row) 
                        <div class="meeting_item">{!!$row!!}</div>
                    @endforeach
                    </td>
                @endforeach                
                </tr>
        
            </tbody>
        </table>
        @else

        <!-- <i id="date_duration"></i> -->
        <input type='hidden' id='s_date' value="{!!$s_date!!}" />
        <input type='hidden' id='e_date' value="{!!$e_date!!}" />

        <table class="table table-bordered" id="list_of_weeklyMeetings">
            <thead>
              <tr>
                <!-- <th>شماره</th> -->
                <!-- <th>تاریخ</th>
                <th>آغاز جلسه</th>
                <th>ختم جلسه</th> -->
                <th>شنبه<br><span dir="rtl">{!!$j_day_date!!}</span><br /><span dir="ltr">{!!$g_day_date!!}</span></th>
                <th>یکشنبه<br><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'1')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'1')!!}</span></th>
                <th>دوشنبه<br><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'2')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'2')!!}</span></th>
                <th>سه شنبه<br><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'3')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'3')!!}</span></th>
                <th>چهارشنبه<br><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'4')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'4')!!}</span></th>
                <th>پنجشنبه<br><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'5')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'5')!!}</span></th>
                <th>جمعه<br><span dir="rtl">{!!addDays(gregorian_format($j_start_date),'6')!!}</span><br /> <span dir="ltr">{!!addGregorianDays(gregorian_format($j_start_date),'6')!!}</span></th>
                
              </tr>
            </thead>
            <tbody>
                <tr><td colspan='7'><div class='alert alert-danger span6' style='text-align:center;font-weight: bold;font-size: 16px;'>جلسات برای این هفته در دیتابیس اضافه نشده</div></td></tr>
            </tbody>
        </table>
        @endif

<script type="text/javascript">

    $( document ).ready(function() {

        var start_date = "<?=jalali_format($jalali_sdate);?>";
        var end_date = "<?=jalali_format($jalali_edate);?>";
        // now send the replaced date to distinguish the day of the week in jalali.
        $.ajax({

            type: 'post',
            url: '{!!URL::route("getMonthDifference")!!}',
            data: {start_date: start_date, end_date: end_date},
            success: function(response){
                $("#date_duration").html(response);
                $("#date_duration_print").html(response);
            
            }
        });
    });

</script>
