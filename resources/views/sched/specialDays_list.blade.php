@extends('layouts.master')

@section('head')
    @parent
    
    <title>Meeting list</title>
    <style type="text/css">
        table thead tr th
        {
            text-align: center;
            border-color: #000; 
        }
        table tbody tr td
        {
            border-color: #000;
        }
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}

@stop


@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success span6 noprint' dir="rtl">{!!Session::get('success')!!}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger span6 noprint' dir="rtl">{!!Session::get('fail')!!}</div>
    @endif
    <?php use App\library\jdatetime; ?>
    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="pull-left">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{!! URL::route('home') !!}">Dashboard</a>
                        </li>
                        <li class="active">
                            <span>All Special Days</span>
                        </li>
                    </ol>
                    <h1>Special Days List</h1>
                </div>
            </div>
        </div>

            @if(!$sd_list == '')
            <table class="table table-bordered" dir="rtl">
                <thead>
                  <tr>
                    <th>شماره</th>
                    <th>تاریخ</th>
                    <th>عنوان</th>
                    <th>شرح بیشتر</th>

                    <th colspan="2" class="noprint">عملیات</th>
                    
                  </tr>
                </thead>

                <tbody>
                <?php $auto = 1; ?>
                    @foreach($sd_list as $item)
                    <?php
                        $j_nums = jdatetime::convertNumbers($auto);                        
                    ?>
                        <tr>
                            <td align="center" width='1%'>{!!$j_nums!!}</td>
                            <td width="10%">{!!$item->date!!}</td>
                            <td width="20%">{!!$item->title!!}</td>
                            <td width="30%">{!!$item->description!!}</td>    
                            <td class="noprint" width="6%"><a href="{!!URL::route('LoadSDView',$item->id)!!}"><i class="fa fa-edit"></i> اصلاح</a></td>
                            <td class="noprint" width="6%"><a href="{!!URL::route('deleteSD',$item->id)!!}" onclick="javascript:return confirm('آیا مطمين هستید ؟');"><i class="fa fa-trash-o"></i> حذف</a></td>
                        </tr>
                        <?php $auto++ ?>
                    @endforeach
                    
                </tbody>
            </table>
            @else
            <table class="table table-bordered" dir="rtl">
                <thead>
                  <tr>
                    <th>شماره</th>
                    <th>تاریخ</th>
                    <th>عنوان</th>
                    <th>شرح بیشتر</th>
                    
                    <th colspan="2" class="noprint">عملیات</th>
                    
                  </tr>
                </thead>
                <tbody>
                    <tr><td colspan='6'><div class='alert alert-danger span6' style='text-align:center'>روز خاص در سیستم اضافه نگردیده است !</div></td></tr>
                </tbody>
            </table>
            @endif

        <a href="{!!URL::route('SpecialDays')!!}" class="btn btn-primary noprint">اضافه کردن روز خاص سال</a> 
        <!-- <a href="#" onclick="window.print()" class="btn btn-success noprint">چاپ جلسات امروز</a>  -->
            
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">

</script>

@stop


