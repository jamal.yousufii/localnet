	<?php
		//find the meeting time in which there is no set meeting, if there was then don't create row.
    	$start_of_meetings = date("H:i", strtotime("07:00:00"));
    	$end_of_meetings = date("H:i", strtotime("21:30:00"));
	?>
	@if(!empty($records))
	
		<?php $auto = 1; ?>
		<i>{!!$date!!}</i>
		<input type='hidden' id='date' value="{!!$c_date!!}" />
		<table class="table table-bordered" id="list_of_meetings" dir="rtl">
			<thead>
		    	<tr>
		            <!-- <th class="noprint">شماره</th> -->
		            <th>آغاز جلسه</th>
                    <th>ختم جلسه</th>
                    <th class="noprint">مدت</th>
      
                    <th class="noprint">نوع جلسه</th>
                    <th>موضوع و اشتراک کنندگان</th> 
                    <th class="noprint">سکتور</th> 
                    <th>محل</th>
                    <th>مطبوعات داخل ارگ</th>
                    <th class="noprint">ضمایم</th>
                    
                    <th colspan="2" class="noprint">عملیات</th>
		        
		      	</tr>
		    </thead>
	    	<tbody> 

	    		<?php
	    		// put 15 minutes duration among start and end meeting time and if there is a meeting then subtract it from the start and end
				while($start_of_meetings <= $end_of_meetings)
				{
                    $m_end_time = date("H:i",strtotime("+15 minutes", strtotime($start_of_meetings)));
                    $auto = 1;
                    if(in_array($start_of_meetings, $mSearch))
                    {
                        $item = $mData[$start_of_meetings];
                        $start = date("g:i a", strtotime($item['meeting_start']));
                        $end = date("g:i a", strtotime($item['meeting_end']));
                        // find the duration of the meeting.
                        $start_time = strtotime($start);
                        $end_time = strtotime($end);
                        $duration = round(abs($start_time - $end_time) / 60,2);

                        $date = date("Y-m-d");
                        $timestamp = strtotime($date);
                        $day = date('D', $timestamp);
                        $dayname = getDayName($day);
                        $dayNum = getDayNum($dayname);
                        //convert today's date in jalali.
                        $jalali_date = convertToJalali($date);
                        $m_end_time = date("H:i",strtotime("+$duration minutes", strtotime($start_of_meetings)));
                        $start_of_meetings = $m_end_time;

                    ?>
                        <tr  <?php
                    $m_type=$item->meeting_type;
                   
                    if($m_type=='نماز') echo 'style="background-color:#ccc"';
                    
                    ?>
                    >
                            <td style="font-family:Bnazanin !important" width="7%" class="mtime">{!!$start!!}</td>
                            <td style="font-family:Bnazanin !important" width="7%" class="mtime">{!!$end!!}</td>
                            <td style="font-family:Bnazanin !important" width="8%" class="noprint">{!!$duration. " دقیقه";!!}</td>
                            <td width='10%' class="noprint">{!!$item['meeting_type']!!}</td>
                            <td width='35%' class="msubject">{!!($item['meeting_subject'])!!}</td>
                            <td width="10%" class="noprint">{!!$item['sector']!!}</td>
                            <td width="10%" class="mlocation">{!!$item['location']!!}</td>
                            @if($item['arg_media'] == 1)
                            <td width="2%" align="center" class="margmedia"><input type="checkbox" checked="checked" disabled /></td>
                            @else
                            <td width="2%" class="margmedia"></td>
                            @endif

                            <td width="15%" class="noprint">

                                @if(!empty(getMeetingFileName($item['id'])))
                                    @foreach(getMeetingFileName($item['id']) AS $attach)
                                    <?php $file_id = Crypt::encrypt($attach->id); $original_file_name = wordwrap($attach->original_file_name, 30, "<br />"); ?>
                                        <div class="row" style="margin: 0 1px 0 0" dir="ltr">
                                            <strong style="font-size: 12px;">{!!$original_file_name!!}</strong>&nbsp;
                                            <a href="{!!URL::route('getDownloadMeetingFile',array($file_id))!!}">
                                                {!! HTML::image('/img/download-icon.png','', array('width' => 20, 'height' => 20, 'title' => 'Download')) !!}
                                            </a>
                                        </div>                                    
                                    @endforeach
                                @else
                                    <span style='color:red;text-align: right !important;'>فایل ضمیمه آپلود نشده</span>
                                @endif

                            </td>
                            
                            <td class="noprint" width="3%" align="center"><a href="{!!URL::route('EditMeetings',array($item['id'],'day'))!!}">اصلاح</a></td>
                            
                            @if($item['recurring'] == '1')    
                                <td class="noprint" width="3%"><a href="{!!URL::route('deleteRecurredMeeting',$item['id'])!!}" onclick="javascript:return confirm('آیا مطمئین هستید ؟');">حذف</a></td>
                            @else
                                <td class="noprint" width="3%" align="center"><a href="{!!URL::route('DeleteMeetings',$item['id'])!!}" onclick="javascript:return confirm('آیا مطمئین هستید ؟');">حذف</a></td>
                            @endif
                        </tr>
                        <?php

                    }
                    else
                    {
                        echo "<tr><td width='7%' class='mtime'>".date("g:i a", strtotime($start_of_meetings))."</td>";
                        $start_of_meetings = $m_end_time;
                        echo "<td width='7%' class='mtime'>".date("g:i a", strtotime($m_end_time))."</td><td width='8%' class='noprint'></td><td width='10%' class='noprint'></td><td width='35%' class='msubject'></td>";
                        echo "<td class='noprint' width='10%'></td><td class='mlocation' width='10%'></td><td width='2%'></td><td width='15%' class='noprint'></td>";
                        echo "<td class='noprint' width='3%'></td><td class='noprint' width='3%'></td></tr>";
                    }
                }

                ?>
	
			</tbody>
		</table>
		
	@else
		<i>{!!$date!!}</i>
		<input type='hidden' id='date' value='{!!$c_date!!}' />
		<table class="table table-bordered" dir="rtl" id="list_of_meetings">
            <thead>
            	<tr>
                    <!-- <th class="noprint">شماره</th> -->
                    <th>آغاز جلسه</th>
                    <th>ختم جلسه</th>
                    <th class="noprint">مدت</th>
                    <th class="noprint">نوع جلسه</th>
                    <th>موضوع و اشتراک کنندگان</th> 
                    <th class="noprint">سکتور</th> 
                    <th>محل</th>
                    <th>مطبوعات داخل ارگ</th>
                    <th class="noprint">ضمایم</th>
                    
                    <th colspan="2" class="noprint">عملیات</th>
                
              	</tr>
            </thead>
            <tbody>
            	<?php
            	// display the rows with start and end meeting time if there is no meeting at that particular period of time.
				while($start_of_meetings <= $end_of_meetings)
				{
					$m_end_time = date("H:i",strtotime("+15 minutes", strtotime($start_of_meetings)));
					echo "<tr><td width='7%' class='mtime'>".date("g:i a", strtotime($start_of_meetings))."</td>";
                    $start_of_meetings = $m_end_time;
                    echo "<td width='7%' class='mtime'>".date("g:i a", strtotime($m_end_time))."</td><td width='8%' class='noprint'></td><td width='10%' class='noprint'></td><td width='35%' class='msubject'></td>";
                    echo "<td class='noprint' width='10%'></td><td class='mlocation' width='10%'></td><td width='2%'></td><td width='15%' class='noprint'></td>";
                    echo "<td class='noprint' width='3%'></td><td class='noprint' width='3%'></td></tr>";
				}
				?>
                <tr class="noprint"><td colspan='12'><div class='alert alert-danger span6' style='text-align:center;font-weight: bold;font-size: 16px;'>جلسات برای امروز اضافه نگردیده است</div></td></tr>
            </tbody>
        </table>
		
	@endif