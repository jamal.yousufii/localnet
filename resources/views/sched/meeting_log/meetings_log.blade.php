@extends('layouts.master')

@section('head')
    @parent
    
    <title>Meetings Log</title>
    <style>
        table { direction: rtl; }
        table#list_of_meetings thead tr th
        {
            text-align: center !important;
            border-color: #000; 
            font-weight: bold;
            color: #000;
            border: 1px solid;
            font-size: 15px !important;
        }
        table#list_of_meetings tbody tr td
        {
            border-color: #000;
            border: 1px solid;
            color: #000;
            font-size: 13px !important;
        }
        
        .fixed{width: 11%;}
    </style>
    
@stop


@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success span6 noprint' dir="rtl">{!!Session::get('success')!!}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger span6 noprint' dir="rtl">{!!Session::get('fail')!!}</div>
    @endif
    <div class="row" style="opacity: 1;">
        <h3 style="text-align:center">All Meeting's Log / لاگ یا کنده تمام جلسات </h3>
        <hr />
<!--    <center>
            <img style="display: none;margin:0 auto;" class="title" width="110px" height="120px" src="<?= asset('img/logo.jpg')?>">
        </center>
        <span style="display: none;margin:0 auto;" class="title">جمهوری اسلامی افغانستان</span>
        <span style="display: none;margin:0 auto;" class="title">ریاست تشریفات</span>
        <span style="display: none;margin:0 auto;" class="title">جلسات روزانه جلالتمآب رئیس صاحب جمهور</i></span> -->
        <!-- <span style="display: none;margin:0 auto;" class="title"><i id="week_day"></i>&nbsp;<i id="current_date"></i></span> -->
        <div id="search_result">
            
            {{--Bring the list table--}}
                @include('sched.meeting_log.table')
            {{--list table end--}}
        
            <div class="dataTables_paginate paging_simple_numbers noprint" id="list_paginate">
            @if(!empty($records))
            {!!$records->render()!!}
            @endif
            </div>
        </div>
              <!-- <a href="{!!URL::route('MeetingForm')!!}" class="btn btn-primary noprint">اضافه کردن جلسه جدید</a>  -->
              <!-- <a href="#" onclick="window.print()" class="btn btn-success noprint">چاپ جلسات امروز</a>  -->
            
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">

    $('.pagination a').on('click', function(event) {
		event.preventDefault();
		if ($(this).attr('href') != '#') {
			var dataString = "&page="+$(this).text()+"&ajax="+1;
			$.ajax({
	                url: '{!!URL::route("getMeetingsLog")!!}',
	                data: dataString,
	                type: 'get',
	                beforeSend: function(){
	                    //$("body").show().css({"opacity": "0.5"});
	                    $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
	                    $('#search_result').html(response);
	                }
	            }
	        );
		
		}
	});

    // now send the replaced date to distinguish the day of the week in jalali.
    $.ajax({

        type: 'post',
        url: '{!!URL::route("getJalaliWeekDay")!!}',
        data: 'date='+"{!!date('Y-m-d')!!}",
        success: function(response){
            $("#week_day").html(response);
        
        }
    });

    // now send the replaced date to distinguish the day of the week in jalali.
    $.ajax({

        type: 'post',
        url: '{!!URL::route("getJalaliMonthName")!!}',
        data: 'date='+"{!!date('Y-m-d')!!}",
        success: function(response){
            $("#current_date").html(response);
        
        }
    });

</script>

@stop


