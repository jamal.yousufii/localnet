<?php use App\library\jdatetime; ?>
<table class="table" id="list_of_meetings">
    <thead>
        <tr>
            <!-- <th class="noprint">شماره</th> -->
            <th>آغاز</th>
            <th>ختم</th>
            <th class="noprint">مدت</th>
            <th class="noprint">تاریخ</th>
            <th class="noprint">نوع جلسه</th>
            <th>موضوع و اشتراک کنندگان</th> 
            <th class="noprint">سکتور</th> 
            <th>محل</th>
            <th>مطبوعات داخل ارگ</th>
            <th>نوع تکرار جلسه</th>
            <th>تاریخ ختم تکرار</th>
            <th>به تعویق افتاده</th>
            <th class="noprint">شخص انجام دهنده</th>
            <th class="noprint">عملیه انجام شده</th>
            <th class="noprint">تاریخ و زمان انجام عملیه</th>
                            
        </tr>
    </thead>

    <tbody>
    @if(!empty($records))
        @foreach($records as $item)
        <?php

            $start = date("g:i a", strtotime($item->meeting_start));
            $end = date("g:i a", strtotime($item->meeting_end));
            // find the duration of the meeting.
            $start_time = strtotime($start);
            $end_time = strtotime($end);
            $duration = round(abs($start_time - $end_time) / 60,2);

            $date = date("Y-m-d");
            $timestamp = strtotime($date);
            $day = date('D', $timestamp);
            $dayname = jdatetime::getDayNames($day);
            $dayNum = getDayNum($dayname);
            //convert today's date in jalali.
            $jalali_date = convertToJalali($date);

        ?>
        <tr>
            <td class="mtime">{!!$start!!}</td>
            <td class="mtime">{!!$end!!}</td>
            <td class="noprint">{!!$duration. " دقیقه";!!}</td>
            <td>{!!toJalali($item->date)!!}</td>
            <td class="noprint">{!!$item->meeting_type!!}</td>
            <td class="msubject">{!!nl2br($item->meeting_subject)!!}</td>
            <td class="noprint">{!!$item->sector!!}</td>
            <td class="mlocation">{!!$item->location!!}</td>
            @if($item->arg_media == 1)
            <td align="center" class="margmedia"><input type="checkbox" checked="checked" disabled /></td>
            @else
            <td class="margmedia"></td>
            @endif
            @if($item->recure_type == 1)
            <td>روزانه</td>
            @elseif($item->recure_type == 2)
            <td>هفته وار</td>
            @elseif($item->recure_type == 3)
            <td>بعد از تعداد هفته</td>
            @else
            <td>بدون تکرار</td>
            @endif
            <td>{!!toJalali($item->end_recure)!!}</td>
            <td align="center" class="margmedia"><input type="checkbox" checked="checked" disabled /></td>
            <td>{!!$item->fname!!}  {!!$item->lname!!}</td>
            <td>{!!$item->action!!}</td>
            <td>{!!$item->updated_at!!}</td>
            
        </tr>
        @endforeach
    @else
    <tr>
        <td colspan="25" align="center"><span style="color:red;font-weight:bold"> معلومات در سیستم موجود نمیباشد </span></td>
    </td>
    @endif
    </tbody>
</table>