@extends('layouts.master')

@section('head')
    @parent
    
    <title>Meetings Log</title>
    
@stop


@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success span6 noprint' dir="rtl">{!!Session::get('success')!!}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger span6 noprint' dir="rtl">{!!Session::get('fail')!!}</div>
    @endif

    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="pull-left">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{!! URL::route('home') !!}">Dashboard</a>
                        </li>
                        <li class="active">
                            <span>Meetings Log</span>
                        </li>
                    </ol>
                    <h1>All Meeting's Log</h1>
                </div>
            </div>
        </div>
<!--         <center>
            <img style="display: none;margin:0 auto;" class="title" width="110px" height="120px" src="<?= asset('img/logo.jpg')?>">
        </center>
        <span style="display: none;margin:0 auto;" class="title">جمهوری اسلامی افغانستان</span>
        <span style="display: none;margin:0 auto;" class="title">ریاست تشریفات</span>
        <span style="display: none;margin:0 auto;" class="title">جلسات روزانه جلالتمآب رئیس صاحب جمهور</i></span> -->
        <!-- <span style="display: none;margin:0 auto;" class="title"><i id="week_day"></i>&nbsp;<i id="current_date"></i></span> -->
        
        <table class="table table-bordered dataTable" id="list_of_logs" dir="rtl">
            <thead>
              <tr>
                <th>شماره</th>
                <th>آغاز جلسه</th>
                <th>ختم جلسه</th>
                <!-- <th>مدت</th> -->
                <th>موضوع جلسه</th>
                <th>جلسه همرا با</th>
                <th>محل</th>
                <th>تاریخ</th>
                <th>عمل</th>
                <th>شخص</th>
                <th>وقت و تاریخ عمل</th>
                <!-- <th class="noprint">عملیات</th> -->
                
              </tr>
            </thead>
            <tbody>
        
            </tbody>
        </table>
              <!-- <a href="{!!URL::route('MeetingForm')!!}" class="btn btn-primary noprint">اضافه کردن جلسه جدید</a>  -->
              <!-- <a href="#" onclick="window.print()" class="btn btn-success noprint">چاپ جلسات امروز</a>  -->
            
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list_of_logs').dataTable( 
            {
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 25,
                // "columnDefs": [ {
                // "searchable": false,
                // "orderable": false,
                // "targets": 0
                // } ],
                //"order": [[ 1, 'asc' ]]
                "sAjaxSource": "{!!URL::route('getLogsData')!!}",
                "aaSorting": [[ 0, "desc" ]],
                "aoColumns": [
                { 'sWidth': '10px' },
                { 'sWidth': '60px', 'sClass': 'center' },
                { 'sWidth': '60px', 'sClass': 'center' },
                // { 'sWidth': '40px', 'sClass': 'center' },
                { 'sWidth': '100px', 'sClass': 'center' },
                { 'sWidth': '140px', 'sClass': 'center' },
                { 'sWidth': '80px', 'sClass': 'center' },
                { 'sWidth': '80px', 'sClass': 'center' },
                { 'sWidth': '80px', 'sClass': 'center' },
                { 'sWidth': '80px', 'sClass': 'center' },
                // { 'sWidth': '80px', 'sClass': 'center' },
                { 'sWidth': '120px', 'sClass': 'center noprint' }

                ]
            }
        ); 
    });

    // now send the replaced date to distinguish the day of the week in jalali.
    $.ajax({

        type: 'post',
        url: '{!!URL::route("getJalaliWeekDay")!!}',
        data: 'date='+"{!!date('Y-m-d')!!}",
        success: function(response){
            $("#week_day").html(response);
        
        }
    });

    // now send the replaced date to distinguish the day of the week in jalali.
    $.ajax({

        type: 'post',
        url: '{!!URL::route("getJalaliMonthName")!!}',
        data: 'date='+"{!!date('Y-m-d')!!}",
        success: function(response){
            $("#current_date").html(response);
        
        }
    });

</script>

@stop


