@extends('layouts.master')

@section('head')
    @parent
    
    <title>جلسات روزانه</title>
    <style type="text/css">
        table { direction: rtl; }
        table#list_of_meetings thead tr th
        {
            text-align: center !important;
            border-color: #000; 
            font-weight: bold;
            color: #000;
            border: 3px solid;
        }
        table#list_of_meetings tbody tr td
        {
            border-color: #000;
            border: 2px solid;
            color: #000;
            font-size: 15px !important;
        }

        table#test {
            border: 3px solid;
            /* border-collapse: collapse; */
            /* width: 70%; */
            margin: 0 auto;
        }

        table#test1 thead tr td{
            border: 5px solid;
            /* border-collapse: collapse; */
            /* width: 70%; */
            margin: 0 auto;
        }
        
        
        
        .fixed{width: 11%;}
        /*table.fixed { table-layout:fixed; }
        table.fixed td { overflow: hidden; }*/
    </style>
    {!! HTML::style('/css/font.css') !!}
    {!! HTML::style('/css/print.css', array('media' => 'print')) !!}

@stop


@section('content')

    @if(Session::has('success'))
        <div class='alert alert-success span6 noprint'>{!! Session::get('success') !!}</div>

    @elseif(Session::has('fail'))
        <div class='alert alert-danger span6 noprint'>{!! Session::get('fail') !!}</div>
    @endif

    <?php use App\library\jdatetime; ?>

    <div class="row" style="opacity: 1;">
        <div class="col-lg-12 noprint">
            <div id="content-header" class="clearfix">
                <div class="col-sm-5">
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ URL::route('home') }}">صفحه اصلی</a>
                        </li>
                        <li class="active">
                            <span>جلسات روزانه</span>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-5">
                	<h3>لست جلسات روزانه</h3>
                </div>
            </div>
        </div>
        <?php 
        	//echo "<pre>";print_r($meetings);echo "</pre>";
        	//find the meeting time in which there is no set meeting, if there was then don't create row.
        	$start_of_meetings = date("H:i", strtotime("07:00:00"));
        	$end_of_meetings = date("H:i", strtotime("21:30:00"));
        ?>
<!--         <center>
            <img style="display: none;margin:0 auto;" class="title" width="110px" height="120px" src="<?= asset('img/logo.jpg')?>">
        </center>
        <span style="display: none;margin:0 auto;" class="title">جمهوری اسلامی افغانستان</span>
        <span style="display: none;margin:0 auto;" class="title">ریاست تشریفات</span>
        <span style="display: none;margin:0 auto;" class="title">جلسات روزانه جلالتمآب رئیس صاحب جمهور</i></span> -->
        <!-- <span style="display: none;margin:0 auto;" class="title"><i id="week_day_print"></i>&nbsp;<i id="current_date_print"></i></span> -->

        <!-- <button class="btn btn-info noprint" onclick="prev_next_days('today')">امروز</button> -->
        <div style="text-align:center;" class="noprint">
            <button class="btn btn-info" onclick="prev_next_days('next')"><i class="fa fa-angle-double-left"></i> بعدی </button>  <button class="btn btn-info" onclick="prev_next_days('prev')">قبلی <i class="fa fa-angle-double-right"></i></button>
        </div>
        <input type="hidden" id="c_date" value="{!! date('Y-m-d') !!}" />
        <div id="today_meetings">
            <i><?= $top_dates;?></i>
            @if(!empty($meetings))
            <table class="table table-bordered" id="test">
                <thead>
                  	<tr>
	                    <!-- <th class="noprint">شماره</th> -->
	                    <th>آغاز جلسه</th>
	                    <th>ختم جلسه</th>
	                    <th class="noprint">مدت</th>
                        <th>نوع جلسه</th>
	                    <th>موضوع و اشتراک کنندگان</th> 
	                    <th class="noprint">سکتور</th> 
	                    <th>محل</th>
                        <th class="noprint">مطبوعات داخل ارگ</th>
	                    <th class="noprint">ضمایم</th>
	                    
	                    <th colspan="2" class="noprint">عملیات</th>
                    
                  	</tr>
                </thead>

                <tbody>
                    @foreach($meetings as $item)
                    <?php
                      
                        $start = date("g:i a", strtotime($item->meeting_start));
                        $end = date("g:i a", strtotime($item->meeting_end));
                        // find the duration of the meeting.
                        $start_time = strtotime($start);
                        $end_time = strtotime($end);
                        $duration = round(abs($start_time - $end_time) / 60,2);

                        $date = date("Y-m-d");
                        $timestamp = strtotime($date);
                        $day = date('D', $timestamp);
                        $dayname = jdatetime::getDayNames($day);
                        $dayNum = getDayNum($dayname);
                        //convert today's date in jalali.
                        $jalali_date = convertToJalali($date);

                    ?>
                    <tr  <?php
                    $m_type=$item->meeting_type;
                   
                    if($m_type=='abc') echo 'style="background-color:#ccc"';
                    
                    ?>
                    >
                        <td style="font-family:Bnazanin " width="7%" class="mtime">{!!$start!!}</td>
                        <td style="font-family:Bnazanin !important" width="7%" class="mtime">{!!$end!!}</td>
                        <td style="font-family:Bnazanin !important" width="8%" class="noprint">{!!$duration. " دقیقه";!!} </td>
                        <td width='10%'>{!!$item->meeting_type!!}</td>
                        {{-- <td width='10%'class="noprint">{!!$item->meeting_type!!}</td> --}}
                        <td width='40%' class="msubject">{!!($item->meeting_subject)!!}</td>
                        <td width="10%" class="noprint">{!!$item->sector!!}</td>
                        <td width="5%" class="mlocation">{!!$item->location!!}</td>
                        @if($item->arg_media == 1)
                        <td width="2%" align="center" class="noprint"><input type="checkbox" checked="checked" disabled /></td>
                        @else
                        <td width="2%" class="noprint"></td>
                        @endif
                        <td width="15%" class="noprint">

                            @if(!empty(getMeetingFileName($item->id)))
                                @foreach(getMeetingFileName($item->id) AS $attach)
                                <?php $file_id = Crypt::encrypt($attach->id); $original_file_name = wordwrap($attach->original_file_name, 30, "<br />"); ?>
                                    <div class="row" style="margin: 0 1px 0 0" dir="ltr">
                                        <strong style="font-size: 12px;">{!!$original_file_name!!}</strong>&nbsp;
                                        <a href="{!!URL::route('getDownloadMeetingFile',array($file_id))!!}">
                                            {!! HTML::image('/img/download-icon.png','', array('width' => 20, 'height' => 20, 'title' => 'Download')) !!}
                                        </a>
                                    </div>                                    
                                @endforeach
                            @else
                                <span style='color:red;text-align: right !important;'>فایل ضمیمه آپلود نشده</span>
                            @endif

                        </td>
                        <td class="noprint" width="3%" align="center"><a href="{!!URL::route('EditMeetings',array($item->id,'day'))!!}">اصلاح</a></td>
                        
                        @if($item->recurring == '1')    
                            <td class="noprint" width="3%"><a href="{!!URL::route('deleteRecurredMeeting',$item->id)!!}" onclick="javascript:return confirm('آیا مطمئین هستید ؟');">حذف</a></td>
                        @else
                            <td class="noprint" width="3%" align="center"><a href="{!!URL::route('DeleteMeetings',$item->id)!!}" onclick="javascript:return confirm('آیا مطمئین هستید ؟');">حذف</a></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <table class="table" id="test1" style="">
                <thead>
                    <tr
                    <?php
                    $m_type=$item->meeting_type;
                   
                    if($m_type=='abc') echo 'style="background-color:#ccc"';
                    
                    ?>
                    >
	                    <!-- <th class="noprint">شماره</th> -->
	                    <th style="Background-color:#232">آغاز جلسه</th>
                        <th>ختم جلسه</th>
                        <th class="noprint">مدت</th>
                        <th>نوع جلسه</th>
                        {{-- <th class="noprint">نوع جلسه</th> --}}
                        <th>موضوع و اشتراک کنندگان</th> 
                        <th class="noprint">سکتور</th> 
                        <th>محل</th>
                        <th class="noprint"">مطبوعات داخل ارگ</th>
                        <th class="noprint">ضمایم</th>
                        
                        <th colspan="2" class="noprint">عملیات</th>
                    
                  	</tr>
                </thead>
                <tbody>
                    <tr class="noprint"><td colspan='12'><div class='alert alert-danger span6' style='text-align:center;font-weight: bold;font-size: 16px;'>جلسات برای امروز اضافه نگردیده است</div></td></tr>
                </tbody>
            </table>
            @endif
        </div>
        <div style="margin-bottom: 10px" class="form-group">
            <center><span style="font-family:Bnazanin !important" dir="ltr">تاریخ و زمان : {!!convertToJalali(date("Y-m-d"))!!} - {!!date("H:i:s")!!}</span></center>
            <center>
                <input type="text" name="destination" id="destination" placeholder="لطفآ مرجع گیرنده را بنویسید و انتر نمائید !" style="width: 20%" />
                <p id="destination_paragraph"></p>
            </center>
        </div>
        <div style="margin-top: 10px">
            <a href="{!!URL::route('MeetingForm')!!}" class="btn btn-primary noprint">اضافه کردن جلسه جدید</a> 
            <a href="#" onclick="window.print()" class="btn btn-success noprint">چاپ جلسات</a> 
        </div>
            
    </div>
@stop
@section('footer-scripts')

<script type="text/javascript">

    
    // send the dayHint and bring the meetings based on the dayHint.    
    function prev_next_days(dayHint)
    {

        var current_date = $("#c_date").val();

        $.ajax({

            type: 'post',
            url: '{!!URL::route("GetDailyMeetings")!!}',
            data: 'day='+dayHint+"&date="+current_date,
            success: function(response){
                $("#today_meetings").html(response);
                $("#c_date").val($("#date").val());
            }
        });
    }

    $('#destination').keypress(function(event){     
       // this function looks redundant...?
        if(event.which == 13){
            var value = $("#destination").val();
            $("#destination").hide("fast");
            $("#destination_paragraph").html(value);
        }
    });

</script>

@stop


