@extends('layouts.master')

@section('head')
	@parent
	<title>اصلاح کردن جلسه</title>
@stop

@section('content')

    @if (count($errors) > 0)
    <div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
        <strong>Whoops !</strong>There were some problems with your input, please check it and try again. <br><br>
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
    </div>
    @endif
    @if(Session::has('success'))
        <div class='alert alert-success noprint'>{{Session::get('success')}}</div>
    @elseif(Session::has('fail'))
        <div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
    @endif
    @if(Session::has('failed'))
        <div class="alert alert-danger">
            <h4 dir="rtl">{!! Session::get('failed') !!}</h4>
        </div>
    @endif

<div class="row" style="opacity: 1;">
    <div class="col-lg-12 noprint">
        <div id="content-header" class="clearfix">
            <div class="col-sm-5">
                <ol class="breadcrumb">
                    <li>
                        <a href="{!!URL::route('getMeetings')!!}">جلسات روزانه</a>
                    </li>
                    <li class="active">
                        <span>اصلاح کردن جلسه</span>
                    </li>
                </ol>
            </div>
            <div class="col-sm-6">
                <h3>صفحه اصلاح کردن جلسه</h3>
            </div>
        </div>
        <hr />
    </div>
    <div style="margin: 40px" dir="rtl">

    
        @foreach($record AS $data)

        <?php

            $start_time = DATE("g:i A", STRTOTIME($data->meeting_start));
            // check if the value of the current end time is not zeros;if its then set it to empty;
            if($data->meeting_end == '00:00:00')
            {
                $end_time = '';
            }
            else
            {
                $end_time = DATE("g:i A", STRTOTIME($data->meeting_end));
            }
            // change date format.
            $jalali_date = jalali_format(convertToJalali($data->date));
        ?>
            <form role="form" method="post" action="{!! URL::route('UpdateMeetings', array($id,$hint)) !!}" class="form-horizontal" id="meeting_edit_form">

                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">موضوع و اشتراک کننده گان</label>
                    </div>
                    <div class="col-sm-12">
                        <textarea class="form-control ckeditor" id="messageArea"  name="meeting_subject">{!!$data->meeting_subject!!}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">آغاز جلسه</label>
                        </div>
                        <div class="col-sm-12">
                            <input class="form-control" type="text" id="timepicker2" name="meeting_start" value="{!!$start_time!!}" readonly="readonly" />
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">ختم جلسه</label>
                        </div>
                        <div class="col-sm-12">
                            <input class="form-control" type="text" id="timepicker1" name="meeting_end" value="{!!old ($end_time)!!}" readonly="readonly" />
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">تاریخ جلسه</label>
                        </div>
                        <div class="col-sm-12">
                            <input class="datepicker_farsi form-control" type="text" name="date" id="current_date" value="{!!$jalali_date!!}" readonly="readonly" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                
             
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">محل</label>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_fields_wrap_location" style="margin-top:10px">
                                <select name="location" class="form-control" id="loc_list" style="width:87%;display:inline-block">
                                    @foreach($locations AS $item)
                                        @if($data->location == $item->id)
                                            <option value='{!!$item->id!!}' selected="selected">{!!$item->name!!}</option>  
                                        @else
                                            <option value='{!!$item->id!!}'>{!!$item->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <!-- <a class="add_field_button_location btn" id="add_location" style="background: green;color:white;" title="add"> + </a> -->
                            </div> 
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">نوع جلسه</label>
                        </div>
                        <div class="col-sm-12">
    		             	<select name="meeting_type" class="form-control" id="meeting_type_list">
    		                    @foreach($meeting_type AS $item)
    		                        @if($data->meeting_type == $item->id)
    		                            <option value='{!!$item->id!!}' selected="selected">{!!$item->name!!}</option>  
    		                        @else
    		                            <option value='{!!$item->id!!}'>{!!$item->name!!}</option>
    		                        @endif
    		                    @endforeach
    		                </select>
                        </div>
		            </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">سکتور</label>
                        </div>
                        <div class="col-sm-12">
                            <div class="input_fields_wrap_sector" style="margin-top:10px">
                                <select name="sector" class="form-control" id="sector_list" style="width:87%;display:inline-block">
                                    @foreach($sectors AS $item)
                                        @if($data->sector == $item->id)
                                            <option value='{!!$item->id!!}' selected="selected">{!!$item->name!!}</option>  
                                        @else
                                            <option value='{!!$item->id!!}'>{!!$item->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <!-- <a class="add_field_button_sector btn" id="add_sector" style="background: green;color:white;" title="add"> + </a> -->
                            </div> 
                        </div>
                    </div>
                </div>

		  		<div class="form-group">
		            
                    
                    <div class="col-sm-4">
                        <div class="col-sm-12">
		                    <label class="col-sm-12 control-label">مطبوعات داخل ارگ</label>
                        </div>
		                <div class='col-sm-12'>
    		            	@if($data->arg_media == 1)
    		            	<input type="checkbox" checked="checked" class="form-control" name="arg_media" />
    		            	@else
    		            	<input type="checkbox" class="form-control" name="arg_media" />
    		            	@endif
                        </div>
		            </div>

    				<div class="col-sm-4">
                        <div class="col-sm-12">
                            <label class="col-sm-12">به تعویق انداختن جلسه</label>
                        </div>
                        <div class="col-sm-12">
                        <input type="checkbox" class="form-control" name="delay" <?php if($data->delay != 0) echo "checked='checked'"?> />
                        </div>
                    </div>
		            
    				<div class="col-sm-4">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">تکرار جلسه</label>
                        </div>
                        <div class="col-sm-12">
                        <input type="checkbox" class="form-control" name="recure" id="check" disabled <?php if($data->recurring != 0) echo "checked='checked'"?> />
                        </div>
                    </div>
		            
                </div>
                <div class="form-group">
                    
                    @if($data->recure_type == 1)
                    <div style="margin-top: 15px" class="col-sm-12">
                        <h5 style="color:red">در صورت که تاریخ آغاز و ختم این جلسه با دگر جلسات که تکرار اند تغیر داشته باشد لطفآ تاریخ ختم تکرار را در این جلسه تغیر ندهید.</h5>
                    </div>
                    <div style="margin-top: 15px" id="repeat" class="col-sm-12">
                        <div class="col-sm-3" style="float: right">
                            <input type="radio" name="recure_type" id="day" value="1" checked="checked" /><span style="font-weight:bold"> تکرار روزانه </span>
                        </div>
                    </div>
                    <hr />
                    <div style="margin-top: 15px;float: right" id="daily_recure_end_date" class="col-sm-4">
                        <div class="col-sm-5 pull-right">
                            <label>تاریخ ختم تکرار روزانه</lable>
                        </div>
                        <input type="text" name="daily_recure_end_date" id="daily_end_date" value="{!!jalali_format(convertToJalali($data->end_recure))!!}" class="datepicker_farsi form-control" readonly="readonly" />
                    </div>
                    @elseif($data->recure_type == 2)
                    <div style="margin-top: 15px" class="col-sm-12">
                        <h5>توجه : در صورت تغیر دادن روز های هفته و یا تاریخ ختم تکرار هفته وار از تاریخ جلسه که تغییرات آورده میشود الی جلسه آخیر که تکرار میگردد تغییرات تطبیق میشود</h5>
                    </div>
                    <div style="margin-top: 15px" id="repeat" class="col-sm-12">
                        <div class="col-sm-3" style="float: right">
                            <input type="radio" name="recure_type" id="week" value="2" checked="checked" /><span style="font-weight:bold"> تکرار هفته وار </span>
                        </div>
                    </div>
                    <div style="margin-top: 15px;float:right" id="day_selection" class="form-group">
                        <div class="col-sm-4">
                            <div class="col-sm-12">
                                <label>تاریخ ختم تکرار هفته وار</lable>
                            </div>
                            <div class="col-sm-12">
                                <input type="text" name="weekly_recure_end_date" id="weekly_end_date" value="{!!jalali_format(convertToJalali($data->end_recure))!!}" class="datepicker_farsi form-control" readonly="readonly" />
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="col-sm-12">
                                <label>روز های هفته را انتخاب کنید</lable>
                            </div>
                            <div class="col-sm-12">
                                <select name="weekly_recure_day[]" class="form-control" id="weekly_recure_day" style="width:100%" multiple="multiple">

                                    @foreach($week_days AS $item)
                                        @if(in_array($item->g_weekday_num, $weekly_recure_days))
                                        <option value='{!!$item->g_weekday_num!!}' selected="selected">{!!$item->name!!}</option>
                                        @else
                                        <option value='{!!$item->g_weekday_num!!}'>{!!$item->name!!}</option>
                                        @endif
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>
                    @elseif($data->recure_type == 3)
                    <div style="margin-top: 15px" class="col-sm-12">
                        <h5 style="color:red">در صورت که تاریخ آغاز و ختم این جلسه با دگر جلسات که تکرار اند تغیر داشته باشد لطفآ تاریخ ختم تکرار و یا تعداد هفته را در این جلسه تغیر ندهید.</h5>
                    </div>
                    <div style="margin-top: 15px" id="repeat" class="col-sm-12">
                        <div class="col-sm-3" style="float: right">
                            <input type="radio" name="recure_type" id="week_num" value="3" checked="checked" /><span style="font-weight:bold"> تکرار بعد از هر </span>
                        </div>
                    </div>
                    <div style="margin-top: 15px;float:right" id="after_weeks" class="form-group">
                    <hr />
                        <div class="col-sm-4">
                            <div class="col-sm-12">
                                <label>تاریخ ختم تکرار</label>
                            </div>
                            <div class="col-sm-12">
                                <input type="text" name="after_weeks_end_date" id="after_weeks_end_date" value="{!!jalali_format(convertToJalali($data->end_recure))!!}" class="datepicker_farsi form-control" readonly="readonly" />
                            </div>
                        </div>
                        <div class="col-sm-3"> 
                         تکرار شود الی تاریخ : 
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label>تعداد هفته</label>
                            </div>
                            <div class="col-sm-12">
                                <input type="text" name="after_weeks_recure" id="after_weeks_input" value="{!!$data->num_of_weeks!!}" class="form-control" />
                            </div>
                        </div>
                    </div>
                    @else

                        <div style="display:none;margin-top: 15px" id="repeat" class="col-sm-12">
                            <div class="col-sm-3" style="float: right">
                                <input type="radio" name="recure_type" id="day" value="1"> تکرار روزانه <br />
                                <input type="radio" name="recure_type" id="week" value="2"> تکرار هفته وار <br />
                                <input type="radio" name="recure_type" id="week_num" value="3"> تکرار بعد از هر
                            </div>
                        </div>
                        <div style="display: none;margin-top: 15px;float: right" id="daily_recure_end_date" class="col-sm-3">
                            <input type="text" name="daily_recure_end_date" id="daily_end_date" placeholder="تاریخ ختم تکرار" class="datepicker_farsi form-control" readonly="readonly" />
                        </div>
                        <div style="display:none;margin-top: 15px" id="day_selection" class="col-sm-12">
                            
                            <div class="col-sm-6" style="float:right">
                                <select name="weekly_recure_day[]" class="form-control" id="weekly_recure_day" style="width:100%" multiple="multiple">

                                    @foreach($week_days AS $item)
                                    
                                        <option value='{!!$item->g_weekday_num!!}'>{!!$item->name!!}</option>
                                        
                                    @endforeach

                                </select>
                            </div>
                            <div class="col-sm-3" style="float:right">
                                <input type="text" name="weekly_recure_end_date" id="weekly_end_date" placeholder="تاریخ ختم تکرار" class="datepicker_farsi form-control" readonly="readonly" />
                            </div>
                        </div>

                        <div style="display: none;margin-top: 15px" id="after_weeks" class="col-sm-12">
                            <div class="col-sm-2" style="float:right">
                                <input type="text" name="after_weeks_recure" id="after_weeks_input" placeholder="تعداد هفته" class="form-control" />
                            </div>
                            <div class="col-sm-3" style="float:right"> 
                                هفته تکرار شود الی تاریخ : 
                            </div>
                            <div class="col-sm-3" style="float:right;margin-right: -150px">
                                <input type="text" name="after_weeks_end_date" id="after_weeks_end_date" placeholder="تاریخ ختم" class="datepicker_farsi form-control" readonly="readonly" />
                            </div>
                        </div>

                    @endif
                </div>

                <hr style="border: 1px dashed;" />
                <div class="form-group main-box-body clearfix">
                    <ul class="widget-todo" style="margin-top:10px;list-style:none">
                        <h4>ضمایم</h4>
                        @if(getSchedFileName($data->id))
                            @foreach(getSchedFileName($data->id) AS $attach)
                                <li class="clearfix" id="li_{!!$attach->id!!}">
                                    <?php $file_id = Crypt::encrypt($attach->id); ?>
                                    <div class="name" style="margin-top:5px">
                                        <label for="todo-2">
                                            <i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
                                            <strong>{!!$attach->original_file_name!!}</strong><br />
                                        </label>&nbsp;&nbsp;&nbsp;
                                        <a href="{!!URL::route('getDownloadMeetingFile',array($file_id))!!}" class="table-link success">
                                            <i class="fa fa-2x fa-cloud-download" style='color:#03a9f4;' title="download file"></i>
                                        </a>
                                        <!-- <button type="button" onclick="removeSchedFile('{!!$attach->id!!}','{!!$attach->file_name!!}')" class="btn btn-danger" style="margin-top:-20px">
                                            <i class="fa fa-trash" title="Remove file"></i>
                                        </button>&nbsp;&nbsp; -->
                                        <br />
                                            
                                    </div>                                    
                                </li>
                            @endforeach
                        @else
                            <li><span style='color:red;'>ضمایم آپلود نشده</span></li>
                        @endif
                    </ul>
                        
                </div>

                <div class="form-group">

                    <div class="col-sm-12">
                        <hr style="border: 1px dashed #b6b6b6" />
                        <button class="btn btn-primary" type="submit" id="save_changes" disabled>
                            <span>
                                <i class="fa fa-check"></i>
                            </span>
                            &nbsp; ثبت تغییرات
                        </button>
                        <a href="{!!URL::route('getMeetings')!!}" class="btn btn-danger">
	                        <span>
	                            <i class="fa fa-remove"></i>
	                        </span>
	                        &nbsp; برگشت
	                    </a>
                    </div>

                </div>
            </form>

            <!-- change the date to gregorian to get the week day based on the date. -->
            <?php $g_date = convertToGregorian($data->date); ?>
        @endforeach
        
    </div>

</div>

@stop
@section('footer-scripts')

<script type="text/javascript">

    $("#weekly_recure_day").select2();

    // onchange of the date the weekday is being changed accordingly.
    // $("#current_date").change(function(){
    //     var value = $("#current_date").val();

    //     // now send the replaced date to distinguish the day of the week in jalali.
    //     $.ajax({

    //         type: 'post',
    //         url: '{!!URL::route("getCurrentWeekDay")!!}',
    //         data: 'date='+value,
    //         success: function(response){
    //             //alert(response);
    //             $("#week_day").val(response);
            
    //         }
    //     });

    // });

    function removeSchedFile(file_id,file_name)
    { 
        var result = confirm("{!!_('are_you_sure_?')!!}");
        if (result) 
        {
            $.ajax({

                url : "{!!URL::route('getRemoveSchedFile')!!}",
                type: "post",
                data : "file_id="+file_id+"&file_name="+file_name+"&_token="+"<?=csrf_token();?>",
                success : function(response)
                {
                    if(response != 0)
                    {
                        $("#li_"+file_id).css('background','Crimson');
                        $("#li_"+file_id).slideUp('6000', function(){
                            $("#li_"+file_id).remove();
                        });
                    }
                }

            });
            return false;
        }
    }

    $('#timepicker1').timepicker({
        showPeriod: true,
        onHourShow: OnHourShowCallback,
        onMinuteShow: OnMinuteShowCallback
    });
    function OnHourShowCallback(hour) {
        if ((hour > 20) || (hour < 6)) {
            return false; // not valid
        }
        return true; // valid
    }
    function OnMinuteShowCallback(hour, minute) {
        if ((hour == 20) && (minute >= 60)) { return false; } // not valid
        if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        return true;  // valid
    }

    $('#timepicker2').timepicker({
        showPeriod: true,
        onHourShow: OnHourShowCallback,
        onMinuteShow: OnMinuteShowCallback
    });
    function OnHourShowCallback(hour) {
        if ((hour > 20) || (hour < 6)) {
            return false; // not valid
        }
        return true; // valid
    }
    function OnMinuteShowCallback(hour, minute) {
        if ((hour == 20) && (minute >= 60)) { return false; } // not valid
        if ((hour == 6) && (minute < 30)) { return false; }   // not valid
        return true;  // valid
    }

    $(function(){ 
  
        // repeat the attachment field ===================================
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
         
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;margin-left:2px" title="remove"> X </a></div>'); //add input box
            }
        });
  
        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            $('#add').fadeIn("slow");
            $('#appendedInputButton').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x--;
        });
  
    });

    $(function(){     

        // location dropdown and manual field
        // repeat the input fields ===================================
        var max_location_fields      = 2; //maximum input boxes allowed
        var wrapper_location        = $(".input_fields_wrap_location"); //Fields wrapper
        var add_button_location      = $(".add_field_button_location"); //Add button ID
       
        var x1 = 1; //initlal text box count
        $(add_button_location).click(function(e){ //on add input button click
            e.preventDefault();
            if(x1 < max_location_fields){ //max input box allowed
                x1++; //text box increment
                $(wrapper_location).append('<div class="input-append"><input type="text" style="width:87%;display:inline-block" id="appendedInputButtonLocation" class="form-control" name="manual_loc" placeholder="محل جلسه" value="{!!old("manual_loc")!!}" /><a class="remove_field btn" id="remove_location" style="background: red;color:white;margin-left:2px" title="remove"> X </a></div>'); //add input box
            }
        });
       
        $(wrapper_location).on("click","#remove_location", function(e){ //user click on remove text
            $('#add_location').fadeIn("slow");
            $('#loc_list').fadeIn("slow");
            $("#remove_location").parent('div').remove(); x1--;
        });

        $('#add_location').click(function(){
            $('#add_location').hide();
            $('#loc_list').hide();
            $('#loc_list').val('');
        });
        
        // Sector dropdown and manual field
        // repeat the input fields ===================================
        var max_fields_sector      = 2; //maximum input boxes allowed
        var wrapper_sector         = $(".input_fields_wrap_sector"); //Fields wrapper
        var add_button_sector      = $(".add_field_button_sector"); //Add button ID
       
        var x2 = 1; //initlal text box count
        $(add_button_sector).click(function(e){ //on add input button click
            e.preventDefault();
            if(x2 < max_fields_sector){ //max input box allowed
                x2++; //text box increment
                $(wrapper_sector).append('<div class="input-append"><input type="text" style="width:87%;display:inline-block" id="appendedInputButtonSector" class="form-control" name="manual_sector" placeholder="سکتور را بنویسید" value="{!!old("manual_sector")!!}" /><a class="remove_field_sector btn" id="remove_sector" style="background: red;color:white;margin-left:2px" title="remove"> X </a></div>'); //add input box
            }
        });
       
        $(wrapper_sector).on("click","#remove_sector", function(e){ //user click on remove text
            $('#add_sector').fadeIn("slow");
            $('#sector_list').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x2--;
        });
        $('#add_sector').click(function(){
            $('#add_sector').hide();
            $('#sector_list').hide();
            $('#sector_list').val('');
        });
        
        // Meeting Type dropdown and manual field
        // repeat the input fields ===================================
        var max_fields_meeting_type      = 2; //maximum input boxes allowed
        var wrapper_meeting_type         = $(".input_fields_wrap_meeting_type"); //Fields wrapper
        var add_button_meeting_type      = $(".add_field_button_meeting_type"); //Add button ID
       
        var x3 = 1; //initlal text box count
        $(add_button_meeting_type).click(function(e){ //on add input button click
            e.preventDefault();
            if(x3 < max_fields_meeting_type){ //max input box allowed
                x3++; //text box increment
                $(wrapper_meeting_type).append('<div class="input-append"><input type="text" style="width:87%;display:inline-block" id="appendedInputButtonMeeting_type" class="form-control" name="manual_meeting_type" placeholder="نوع جلسه" value="{!!old("manual_meeting_type")!!}" /><a class="remove_field_meeting_type btn" id="remove_meeting_type" style="background: red;color:white;margin-left:2px" title="remove"> X </a></div>'); //add input box
            }
        });
       
        $(wrapper_meeting_type).on("click","#remove_meeting_type", function(e){ //user click on remove text
            $('#add_meeting_type').fadeIn("slow");
            $('#meeting_type_list').fadeIn("slow");
            $(this).parent('div').remove(); x3--;
        });
        $('#add_meeting_type').click(function(){
            $('#add_meeting_type').hide();
            $('#meeting_type_list').hide();
            $('#meeting_type_list').val('');
        });
    });

$("#save_changes").attr("disabled",true);
$(function(){

    $("#meeting_edit_form").change(function(){
        $("#save_changes").attr("disabled",false);
    });

    $('#check').change(function(){
        if (this.checked) {
            $('#repeat').fadeIn('slow');
        }
        else
        {
            $('#week').prop('checked', false);
            $('#day').prop('checked', false);
            $('#week_num').prop('checked', false);
            $('#repeat').hide();
            $("#day_selection").hide("slow");
            $("#week_day").fadeIn("slow");
            $("#current_date").prop('disabled', false);
            $("#daily_end_date").val("");
            $('#weekly_end_date').val("");
            $('#weekly_recure_day').val("");
            $('#after_weeks_input').val("");
            $('#after_weeks_end_date').val("");
            $("#daily_recure_end_date").hide();
            $('#weekly_end_date').prop("required", false);
            $('#weekly_recure_day').prop("required", false);
            $('#daily_end_date').prop("required", false);
            $('#after_weeks_input').prop('required',false);
            $('#after_weeks_end_date').prop('required',false);
        }
        $('input[id=day]').change(function(){

            $('#daily_recure_end_date').fadeIn('slow');
            $("#weekly_recure_day").val("");
            $("#day_selection").hide();
            $("#after_weeks_input").val("");
            $('#after_weeks_end_date').val("");
            $("#after_weeks").hide();
            $("#current_date").removeClass("datepicker_farsi");
            if (this.checked) {
                $('#daily_end_date').prop('required',true);
                $('#weekly_end_date').prop("required", false);
                $('#weekly_recure_day').prop("required", false);
                $('#after_weeks_input').prop('required',false);
                $('#after_weeks_end_date').prop('required',false);
            }

        });

        $('input[id=week]').change(function(){
        
            $("#day_selection").fadeIn("slow");
            $("#current_date").removeClass("datepicker_farsi");
            $('#daily_end_date').val("");
            $('#daily_recure_end_date').hide();
            $('#after_weeks_input').val("");
            $('#after_weeks_end_date').val("");
            $('#after_weeks').hide();
            if (this.checked) {
                $('#weekly_end_date').prop('required',true);
                $('#weekly_recure_day').prop('required',true);
                $('#daily_end_date').prop("required", false);
                $('#after_weeks_input').prop('required',false);
                $('#after_weeks_end_date').prop('required',false);

            }

        });

        $('input[id=week_num]').change(function(){
        
            $("#after_weeks").fadeIn("slow");
            $('#daily_end_date').val("");
            $('#daily_recure_end_date').hide();
            $('#weekly_recure_day').val("");
            $('#day_selection').hide();
            if (this.checked) {
                $('#after_weeks_input').prop('required',true);
                $('#after_weeks_end_date').prop('required',true);
                $('#daily_end_date').prop("required", false);
                $('#weekly_end_date').prop("required", false);
                $('#weekly_recure_day').prop("required", false);
            }

        });


    });
});

</script>


@stop