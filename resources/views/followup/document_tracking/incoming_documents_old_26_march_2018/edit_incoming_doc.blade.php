
@extends('layouts.master')
@section('content')
<div style="margin: 20px" dir="rtl">
  <div class="page-head">
    <h3>فورمه اصلاح کردن سند</h3>
    <hr >
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
    <strong>Whoops !</strong>There were some problems with your input, please check it and try again. <br><br>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if(Session::has('success'))
  <div class='alert alert-success noprint'>{{Session::get('success')}}</div>

  @elseif(Session::has('fail'))
  <div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
  @endif
  <div class="cl-mcont" id="sdu_result">
    <form class="form-horizontal group-border-dashed" action="{!!URL::route('updateIncomingDoc', $record->id)!!}" method="post" style="border-radius: 0px;" style="direction: rtl !important;">
      	@if(isMEExecutiveManager('document_tracking_incoming_docs', 'm&e_executive_manager_incoming_docs'))
        <div class="form-group">

          <div class="col-sm-3">
              <div class="col-sm-12">
                  <label class="col-sm-12 ">نوعیت سند <span style="color: red"> * </span></label>
              </div>
              <div class="col-sm-12">
                  <select class="form-control" name="doc_type" required="required">
                      <option value="">نوعیت سند را انتخاب کنید</option>
                      {!!getMEDocumentType($record->doc_type);!!}
                  </select>
              </div>
          </div>
          <div class="col-sm-3">
              <div class="col-sm-12">
                <label class="col-sm-12 ">شماره سند <span style="color: red"> * </span></label>
              </div>
              <div class="col-sm-12">
                  <input type="number" class="form-control" name="doc_number" value="{!!$record->doc_number!!}" required="required" />
              </div>
          </div>
          <div class="col-sm-3">
              <div class="col-sm-12">
                <label class="col-sm-12 ">تاریخ سند <span style="color: red"> * </span></label>
              </div>
              <div class="col-sm-12">
                  <input type="text" class="form-control datepicker_farsi" name="doc_date" value="{!!checkEmptyDate($record->doc_date)!!}" readonly="readonly" />
              </div>
          </div>
          <div class="col-sm-3">
              <div class="col-sm-12">
                  <label class="col-sm-12 ">شماره وارده <span style="color: red"> * </span></label>
              </div>
              <div class="col-sm-12">
                  <input type="number" name="incoming_number" class="form-control" required="required" value="{!!$record->incoming_number!!}" />
              </div>
          </div>

      </div>
      <div class="form-group">

          <div class="col-sm-3">
              <div class="col-sm-12">
                <label class="col-sm-12 ">تاریخ وارده <span style="color: red"> * </span></label>
              </div>
              <div class="col-sm-12">
                  <input type="text" class="form-control datepicker_farsi" name="incoming_date" value="{!!checkEmptyDate($record->incoming_date)!!}" readonly="readonly" />
              </div>
          </div>
          <div class="col-sm-3">
              <div class="col-sm-12">
                <label class="col-sm-12 ">مرسل <span style="color: red"> * </span></label>
              </div>
              <div class="col-sm-12">
                <select class="form-control" name="sender" id="sender" style="width: 100%" required="required">
                  <option value="">انتخاب مرسل</option>
                  {!!getOrganizations($record->sender)!!}    
                </select>
              </div>
          </div>
          <div class="col-sm-3">
              <div class="col-sm-12">
                <label class="col-sm-12 ">مرسل الیه <span style="color: red"> * </span></label>
              </div>
              <div class="col-sm-12">
                <select class="form-control" name="sent_to" required="required">
                  <option value="{!!$record->sent_to!!}">{!!$record->sent_to!!}</option>
                </select>
              </div>
          </div> 
          <div class="col-sm-3">
              <div class="col-sm-12">
                <label class="col-sm-12 ">موضوع <span style="color: red"> * </span></label>
              </div>
              <div class="col-sm-12">
                  <textarea cols="30" rows="4" class="form-control" name="subject" required="required">{!!$record->subject!!}</textarea>
              </div>
          </div> 
          <div class="col-sm-3">
              <div class="col-sm-12">
                <label class="col-sm-12 ">تعداد اوراق <span style="color: red"> * </span></label>
              </div>
              <div class="col-sm-12">
                  <input type="text" class="form-control" name="number_of_papers" value="{!!$record->number_of_papers!!}" required="required" />
              </div>
          </div> 

      </div> 
      @endif
      @if(isMEDirector('document_tracking_incoming_docs', 'm&e_director_incoming_docs'))
      <div class="form-group">

        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">چگونگی اجراآت <span style="color: red"> * </span></label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="execution_type" required="required">
              <option value="">...</option>
              <option value="1" <?php if($record->execution_type == 1) echo "selected='selected'"?>>عادی</option>
              <option value="2" <?php if($record->execution_type == 2) echo "selected='selected'"?>>تفصیلی</option>
            </select>
          </div>
        </div> 
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">معیاد اجراء <span style="color: red"> * </span></label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="execution_date" value="{!!checkEmptyDate($record->execution_date)!!}" readonly="readonly" />
          </div>
        </div>
        <div class="col-sm-6">
          <div class="col-sm-12">
            <label class="col-sm-12 ">کارشناس مؤظف <span style="color: red"> * </span></label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="assignee" id="assignee" style="width: 100%" data-plugin="select2" required="required">
              <option value="">...</option>
              {!!getMEExperts($assignee);!!}
            </select>
          </div>
        </div> 

      </div>
      @endif
      @if(isMEExpert('document_tracking_incoming_docs', 'm&e_expert_incoming_docs'))
      <div class="form-group">

        <div class="col-sm-6">
            <div class="col-sm-12">
              <label class="col-sm-12 ">توضیحات کارشناس <span style="color: red"> * </span></label>
            </div>
            <div class="col-sm-12">
                <textarea cols="30" rows="2" class="form-control" name="assignee_description" required="required">{!!$assignee_description!!}</textarea>
            </div>
        </div>

      </div>
      @endif
      <hr style="border: 1px dashed;" />
      {!!Form::token();!!}
      <div class="form-group">
        <label class="col-sm-2 control-label"></label>
        <div class="col-sm-4">
            <input type="submit" value="ثبت تغیرات" class="btn btn-success"/>
            <a href="javascript:history.back()" class="btn btn-warning"> برگشت <i class="fa fa-arrow-left"></i></a>
        </div>
      </div>
    </form>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

  $("#sender").select2();
  $("#incoming_number").select2();
  $("#assignee").select2();

  $(function(){
    $("#searchclear").click(function(){
      $("#sender").select2('val', '');
      $("#incoming_number").select2('val', '');
      $("#assignee").select2('val', '');
    });
  });

</script> 

@stop
