@extends('layouts.master')
@section('content')
<div class="container">
  <div class="page-head">
    <h3>Document Form</h3>
    <hr>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <form class="form-horizontal group-border-dashed" action="{!!URL::route('saveNewDoc')!!}" method="post" style="border-radius: 0px;" enctype="multipart/form-data">
      	<div class="form-group">
          <label class="col-sm-2 control-label">Hokum Number</label>
          <div class="col-sm-4">
              <input type="number" name='hokum_number' class="form-control" value="{!!old('hokum_number')!!}" />
          </div> 
          <label class="col-sm-2 control-label">Peshnehad Number</label>
          <div class="col-sm-4">
              <input type="number" name='peshnehad_number' class="form-control" value="{!!old('peshnehad_number')!!}" />
          </div> 
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Arrival Date</label>
          <div class="col-sm-4">
              <input type="text" name='arrival_date' class="form-control datepicker_farsi" value="{!!old('arrival_date')!!}" readonly="readonly" />
          </div>
          <label class="col-sm-2 control-label">Source Organization</label>
          <div class="col-sm-4">
              <input type="text" name='source' class="form-control" value="{!!old('source')!!}" />
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Document Type</label>
          <div class="col-sm-4">
            <select name="doc_type" id="document_type" class="form-control" style="width: 100%">
              {!!getDocType();!!}
            </select>
          </div>
          <label class="col-sm-2 control-label">Document Category</label>
          <div class="col-sm-4">
            <select name="doc_category" class="form-control" id="document_category" style="width: 100%">
              {!!getDocCategory();!!}
            </select>
          </div>
        </div> 
        <div class="form-group"> 
          <label class="col-sm-2 control-label">Document Summary</label>
          <div class="col-sm-4">
              <textarea name='summary' class="form-control">{!!old('summary')!!}</textarea>
          </div>   
          <label class="col-sm-2 control-label">Document Signed By</label>
          <div class="col-sm-4">
              <input type="text" name='doc_signed_by' class="form-control" value="{!!old('doc_signed_by')!!}" />
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Attachments </label>
  			  <div class="col-sm-4">
              <div class="input_fields_wrap">
                  <input type='file' id='files' style="width:87%;display:inline-block" name='files[]' class="form-control" multiple='multiple'>
                  <a class="add_field_button btn" id="add" style="background: green;color:white;display:inline-block;margin-top:-20px;" title="Add another file"> + </a>
              </div>
          </div>

        </div>
      	{!!Form::token();!!}
      	<hr />
      	<div class="form-group" style="margin-left: -18em">
	        <label class="col-sm-2 control-label"></label>
	        <div class="col-sm-4">
	          	<input type="submit" value="Add Document" class="btn btn-success"/>
	          	<a href="{!!URL::route('getDocsList')!!}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Go to list</a>
	        </div>
	    </div>
    </form>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

  $("#document_category").select2();
  $("#document_type").select2();

	$(function(){     
        
        // repeat the input fields ===================================
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
       
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;display:inline-block;margin:-20px 0 0 5px" title="remove"> X </a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            $('#add').fadeIn("slow");
            $('#appendedInputButton').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x--;
        });

    });

</script> 

@stop