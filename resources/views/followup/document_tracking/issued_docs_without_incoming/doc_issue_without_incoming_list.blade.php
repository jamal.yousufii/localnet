@extends('layouts.master')

@section('head')
    @parent
    <title>اسناد صادره پیگیری</title>
    <style type="text/css">
      a
      {
        cursor: pointer;
      }
      .form-group{ direction: rtl !important; }
      #content {white-space: nowrap; overflow-y: auto; overflow-x: scroll; direction: rtl;}
      table th { font-size: 16px;font-weight: bold !important;text-align: center; }
      table tr { color: #000 !important; }
    </style>
    
@stop


@section('content')


  @if (count($errors) > 0)
  <div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
    <strong>Whoops !</strong>There were some problems with your input, please check it and try again. <br><br>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if(Session::has('success'))
  <div class='alert alert-success noprint'>{{Session::get('success')}}</div>

  @elseif(Session::has('fail'))
  <div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
  @endif
  <div class="row col-md-12" style="margin-top:-30px;margin-bottom: 30px">
    <div class="col-md-5 noprint" style="margin-top: 10px">
        <div id="content-header" class="clearfix">
            <div>
                <center>
                <h4>
                د افغانستان اسلامی جمهوریت</br>
                دجمهوری ریاست دلوړ مقام د دفتر لوی ریاست</br>
                د پالیسی ، څارنی او بررسی معاونیت</br>
                د څارنی او ارزونی ریاست
                </h4></center>
            </div>
            
        </div>
    </div>
    <div class="col-md-2 noprint">
        {!! HTML::image('/img/logo.jpg', 'LOGO', array('width' => '130','height' => '125')) !!}
        
    </div>
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix" style="margin-top: 10px">
            <div>
                <center>
                <h4>
                جمهوری اسلامی افغانستان</br>
                ریاست عمومی دفتر مقام عالی ریاست جمهوری</br>
                معاونیت پالیسی, نظارت و بررسی</br>
                ریاست نظارت و ارزیابی
                </h4></center>
            </div>
            
        </div>
    </div>
  </div>
  <div class="row noprint" style="margin: 10px;">
    <h4 class="pull-right"><span style="font-weight: bold;font-size: 25px;">بخش صادر اسناد و پیگیری</span></h4>
    @if(isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
    <!-- <a id="click" data-target="#exampleModalPrimary" data-toggle="modal" class="btn btn-success pull-right"><i class="icon fa-plus" aria-hidden="true"></i>{!!_('add_new_document')!!}</a> -->
    <a href="{!!URL::route('issueFormWithoutIncoming')!!}" class="btn btn-success pull-left"><i class="icon fa-plus" aria-hidden="true"></i> اضافه کردن سند صادره جدید</a>
    @endif
  </div>
  <hr class="noprint" />
    <div id="deleted_result" class="noprint">
    </div>
    <form role="form" id="search_form" class="form-horizontal noprint" method="post" action="{!!URL::route('exportIssuedDocsToExcel',2)!!}" dir="rtl">
      <div class="form-group">

        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">نوعیت سند</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="doc_type">
              <option value="">نوعیت سند را انتخاب کنید</option>
              {!!getMEDocumentType();!!}
            </select>
          </div>
        </div> 
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">شماره وارده</label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="incoming_number" placeholder="شماره وارده" />
          </div>
        </div> 
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">شماره صادره</label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="issue_number" placeholder="شماره صادره" />
          </div>
        </div>         
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تاریخ صادره</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="issue_date" placeholder="تاریخ صادره" />
          </div>
        </div>         
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">کاپی سوم</label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="third_copy" placeholder="کاپی سوم" style="width: 100%" />
          </div>
        </div>
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">انتخاب مرسل الیه</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" id="sent_to" name="sent_to" style="width: 100%">
              <option value="">انتخاب مرسل الیه</option>
              {!!getOrganizations()!!}    
            </select>
          </div>
        </div>

      </div>
      {!!Form::token()!!}
      <div class="form-group">

        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">موضوع</label>
          </div>
          <div class="col-sm-12">
            <textarea class="form-control" name="subject" placeholder="موضوع"></textarea>
          </div>
        </div> 
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تعداد اوراق</label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="number_of_papers" placeholder="تعداد اوراق" />
          </div>
        </div>  
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">کارتن مربوطه</label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control" name="related_carton" placeholder="کارتن مربوطه" />
          </div>
        </div> 
        @if(isMEDirector('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs') || isMEExecutiveManager('document_tracking_issued_docs', 'm&e_executive_manager_issued_docs'))
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">کارشناس مؤظف</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="assignee" id="assignee" style="width: 100%">
              <option value="">انتخاب کارشناس مؤظف</option>
              {!!getMEExperts();!!}
            </select>
          </div>
        </div>
        @endif 
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تعداد اسناد جهت نمایش در صفحه</label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="number_of_records_per_page">
              <option value="">تعداد اسناد جهت نمایش در صفحه</option>
              <option value="3">3</option>
              <option value="5">5</option>
              <option value="10">10</option>
              <option value="15">15</option>
              <option value="20">20</option>
              <option value="25">25</option>
              <option value="30">30</option>
            </select>
          </div>
        </div> 
        <div class="col-sm-2">
          <div class="col-sm-12">
            <label class="col-sm-12 ">انتخاب سال</label>
          </div>
          <div class="col-sm-12">
              <select class="form-control" name="year" id="year" style="width: 100%">
                {!!getJalaliYears();!!}
              </select>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-4">
            <button class="btn btn-warning" id="search_docs" type="submit">
                <span>
                    <i class="fa fa-search"></i>
                </span>
                &nbsp;جستجو
            </button>
            &nbsp;<input type="reset" value="پاک کردن" id="searchclear" class="btn btn-danger"/>
            &nbsp;
            <button type="submit" class="btn btn-success" id="print_to_excel">چاب به صفحه اکسل</button>
            &nbsp;
            <!-- <a href="#" onclick="window.print()" class="btn btn-primary noprint">Print Page</a> -->
        </div>

      </div>
    </form>
	<hr class="noprint" />
  
  <div id="search_result" style="margin-top: 10px">
    {{--Bring the list table--}}
        @include('document_tracking.issued_docs_without_incoming.list_table')
    {{--list table end--}}
    <div class="dataTables_paginate paging_simple_numbers noprint" id="list_paginate">
      @if(!empty($records))
      {!!$records->render()!!}
      @endif
    </div>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

  $("#sent_to").select2();
  $("#assignee").select2();
  $("#year").select2();

  $(function(){
    $("#searchclear").click(function(){
      $("#sent_to").select2('val', '');
      $("#year").select2('val', '');
      $("#assignee").select2('val', '');
    });
  });

  // $("#document_type").select2();

	$('.pagination a').on('click', function(event) {
    event.preventDefault();
    if ($(this).attr('href') != '#') {
      //$('#ajaxContent').load($(this).attr('href'));
      var dataString = $('#search_form').serialize();
      dataString += "&page="+$(this).text()+"&ajax="+1;
      $.ajax({
          url: '{!!URL::route("issuedDocListWithoutIncoming")!!}',
          data: dataString,
          type: 'get',
          beforeSend: function(){
              //$("body").show().css({"opacity": "0.5"});
              $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#search_result').html(response);
          }
      }
    );
    
    }
  });

  function delteDocument(record_id)
  {
      if (confirm("آیا مطمئین هستید ؟")) 
      {
          $.ajax({
            url : '{!!URL::route("deleteIssuedSavedDocs")!!}',
            type : 'post',
            data : {'record_id':record_id,'_token':"{!!csrf_token()!!}"},
            success : function(response)
            {
              $("#deleted_result").html(response);
              $(".remove_record"+record_id).css('background','Crimson');
              $(".remove_record"+record_id).slideUp('6000', function(){
              $(this).remove();
              //$("#deleted_result").html(mydata);
              });

            }
          })
        //   window.setTimeout(function(){
        //       // Move to a new location or you can do something else
        //     window.location.href = "{!!URL::to('/home')!!}";

        // }, 3000);
      }
  }

  //get the document list based on form data.
  $('#search_docs').click(function(){
      var datastring = $('#search_form').serialize();
      $.ajax({
          type: 'POST',
          url: '{!!URL::route("searchIssuedDocsWithoutIncoming")!!}',
          data: datastring,
          beforeSend: function(){
              $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#search_result').html(response);
          }
      });
      return false;
  });

	$(function(){     
        
      // repeat the input fields ===================================
      var max_fields      = 5; //maximum input boxes allowed
      var wrapper         = $(".input_fields_wrap"); //Fields wrapper
      var add_button      = $(".add_field_button"); //Add button ID
     
      var x = 1; //initlal text box count
      $(add_button).click(function(e){ //on add input button click
          e.preventDefault();
          if(x < max_fields){ //max input box allowed
              x++; //text box increment
              $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;display:inline-block;margin:-20px 0 0 5px" title="remove"> X </a></div>'); //add input box
          }
      });

      $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
          $('#add').fadeIn("slow");
          $('#appendedInputButton').fadeIn("slow");
          e.preventDefault(); $(this).parent('div').remove(); x--;
      });

  });

</script> 
@stop