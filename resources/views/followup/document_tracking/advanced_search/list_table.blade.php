<div style="color: orange;font-weight: bold;font-size: 22px;margin-right: 20px"> نتیجه جستجو </div>
<div id='content'>
	<table class="table table-bordered table-responsive" style="margin: 10px;">
		<thead>
	      <tr>
	        <th>شماره</th>
	        <th>نوعیت سند</th>
	        <th>چگونگی اجراآت</th>
	        <th>شماره سند</th>
	        <th>تاریخ سند</th>
	        <th>مرسل</th>
	        <th>مرسل الیه</th>
	        <th>شماره وارده</th>
	        <th>تاریخ وارده</th>
	        <th>موضوع</th>
	        <th>تعداد اوراق</th>
	        <th>معیاد اجراء</th>
	        <th>وضعیت سند</th>
	        <th>کارشناس مؤظف</th>
	        <th>توضیحات کارشناس</th>
	        <th>عملیات</th>
	        <th>شماره صادره</th>
	        <th>تاریخ صادره</th>
	        <th>کاپی سوم</th>
	        <th>مرسل صادره</th>
	        <th>مرسل الیه صادره</th>
	        <th>موضوع صادره</th>
	        <th>کاپی به مراجع</th>
	        <th>تعداد اوراق صادره</th>
	        <th>ضمایم</th>
	        <th>کارتن مربوطه</th>

	        <!-- <th colspan="2" class="noprint">Operations</th> -->
	        
	      </tr>
	    </thead>

	    <tbody>
	    	@if(!empty($records))
	    	<?php $counter = $records->firstItem(); ?>
	            @foreach($records AS $item)
	            	<?php
	            		$copy_to_references = getCopyToReferences($item->issued_doc_id);
	            		$incoming_subject = wordwrap($item->incoming_subject, 150, "<br />");
	            		$issuing_subject = wordwrap($item->issuing_subject, 150, "<br />");
	            		$assignee_description = getAssigneeDescription($item->incoming_id);
	            		$assignee_description = wordwrap($assignee_description, 150, "<br />");
	            		$incoming_sender = wordwrap($item->incoming_sender, 70, "<br />");
	            		$incoming_sent_to = wordwrap($item->incoming_sent_to, 70, "<br />");
	            		$issuing_sender = wordwrap($item->issuing_sender, 70, "<br />");
	            		$issuing_sent_to = wordwrap($item->issuing_sent_to, 70, "<br />");
	            		//dd($copy_to_references);
	            	?>
	                <tr>
		                <td>{!!$counter!!}</td>
		                @if(getMEDocumentTypeName($item->doc_type) != "")
		                <td>{!!getMEDocumentTypeName($item->doc_type)!!}</td>
		                @else
		                <td></td>
		                @endif
		                @if($item->execution_type == 1)
		                <td><?= "عادی"?></td>
		                @elseif($item->execution_type == 2)
		                <td><?= "تفصیلی"?></td>
		                @else
		                <td></td>
		                @endif
		                <td>{!!$item->doc_number!!}</td>
		                <td>{!! dmy_format(toJalali($item->doc_date))!!}</td>
		                <td>{!!$incoming_sender!!}</td>
		                <td>{!!$incoming_sent_to!!}</td>
		                <td>{!!$item->incoming_number!!}</td>
		                <td>{!!dmy_format(toJalali($item->incoming_date))!!}</td>
		                <td>{!!$incoming_subject!!}</td>
		                <td>{!!$item->incoming_number_of_papers!!}</td>
		                <td>{!!dmy_format(toJalali($item->execution_date))!!}</td>
		                @if($item->doc_status == 0)
		                <td>تحت اجراء</td>
		                @elseif($item->doc_status == 1)
		                <td>اجراء</td>
		                @endif
		                @if(!empty(getAssignee($item->incoming_id)))
		                <td>
		                	{!!getAssigneeName(getAssignee($item->incoming_id))!!}
			            </td>
			            @else
			            <td></td>
			            @endif
		                <td>{!!$assignee_description!!}</td>
		                @if($item->operations == 1)
		                <td>حفظ شده</td>
		                @elseif($item->operations == 2)
		                <td>صادر شده</td>
		                @else
		                <td></td>
		                @endif
		                <td>{!!$item->issuing_number!!}</td>
		                <td>{!! dmy_format(toJalali($item->issuing_date))!!}</td>
		                <td>{!!$item->third_copy!!}</td>
		                <td>{!!$issuing_sender!!}</td>
		                <td>{!!$issuing_sent_to!!}</td>
		                <td>{!!$issuing_subject!!}</td>
		                @if(!empty($copy_to_references))
		                <td><ul>
			                @foreach($copy_to_references as $refere)
			                	<li>{!!getRecordFieldBasedOnID('ministries','name_dr',$refere->reference_id)!!}</li>
			                @endforeach
		            	</ul></td>
		            	@else
		            	<td></td>
		                @endif
		                <td>{!!$item->issuing_number_of_papers!!}</td>
		                <td>
		                	<ul class="widget-todo" style="margin-top:10px;list-style:none">
					            @if(!empty(getDocumentFileName($item->issued_doc_id)))
					                @foreach(getDocumentFileName($item->issued_doc_id) AS $attach)
					                <li class="clearfix" id="li_{!!$attach->id!!}" style="margin: -10px">
					                <?php $file_id = Crypt::encrypt($attach->id); $original_file_name = wordwrap($attach->original_file_name, 50, "<br />"); ?>
					                    <div class="name" style="margin-top:5px">
					                        <label for="todo-1">
					                       		<i class="fa fa-check fa-fw fa-lg" style='color:#03a9f4;'></i>
					                          	<strong>{!!$original_file_name!!}</strong><br />
					                        </label>&nbsp;&nbsp;&nbsp;
					                        <a href="{!!URL::route('getDownloadDocFile',array($file_id))!!}" class="table-link success">
					                            <i class="fa fa-1x fa-cloud-download" style='color:#03a9f4;margin-right: -20px' title="download file"></i>
					                        </a>
					                    </div>                                    
					                </li>
					                @endforeach
					            @else
					                <li><span style='color:red;text-align: right !important;'>فایل ضمیمه آپلود نشده</span></li>
					            @endif
					        </ul>
					    </td>
		                <td>{!!$item->related_carton!!}</td>
	                </tr>
	                <?php $counter++; ?>
	            @endforeach
	        @else
	        <div style="padding: 10px" class="noprint">
	        	<tr><td colspan="30" style="color:red">موردی پیدا نشد !</td></tr>
	        </div>
	    	@endif
	    </tbody>
	</table>
</div>
<script type="text/javascript">

	$(document).ready(function() {
    	$('td.text').each(function() {
	        var td = $(this);
	        var cs = td.text().length;
	        
	        if(cs>50)
	        {
	        	var shown = td.text().substring(0, 50);
	        	td.text(shown+'...');
	        }
	    });
	});

</script>