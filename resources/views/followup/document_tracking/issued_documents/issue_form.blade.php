
@extends('layouts.master')

@section('head')
  <title>فارم اسناد صادره</title>
@stop

@section('content')
<div style="margin: 20px">
  <div class="page-head">
    <h3 dir="rtl">فورمه سند صادره</h3>
    <hr >
  </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger noprint" style="margin: 10px 0 20px 0">
    <strong>Whoops !</strong>There were some problems with your input, please check it and try again. <br><br>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if(Session::has('success'))
  <div class='alert alert-success noprint'>{{Session::get('success')}}</div>

  @elseif(Session::has('fail'))
  <div class='alert alert-danger noprint'>{{Session::get('fail')}}</div>
  @endif
  <div class="cl-mcont" id="sdu_result" dir="rtl">
    <form class="form-horizontal" role="form" method="post" action="{!!URL::route('insertIssuedDoc')!!}" enctype="multipart/form-data" id="modal_form">

      <div class="form-group">

        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">سال</label>
          </div>
          <div class="col-sm-12">
            <select name="year" id="year" class="form-control" style="width:100%" data-plugin="select2">
              {!!getJalaliYears();!!}
            </select>
          </div>
        </div>
        <div id="pendings">
          <div class="col-sm-6">
            <div class="col-sm-12">
              <label class="col-sm-12 ">شماره وارده</label>
            </div>
            <div class="col-sm-12">
              <select name="incoming_ids[]" id="incoming_ids" class="form-control" style="width:100%" multiple="multiple" data-plugin="select2">
                {!!$pendingIncomingNumber!!}
              </select>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">شماره صادره<span style="color:red"> *</span></label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="issue_number" value="{!!old('issue_number')!!}" placeholder="شماره صادره" required="required" />
          </div>
        </div>    

      </div>

      <div class="form-group"> 
        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تاریخ صادره<span style="color:red"> *</span></label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control datepicker_farsi" name="issue_date" value="{!!old('issue_date')!!}" placeholder="تاریخ صادره" readonly="readonly" />
          </div>
        </div>
        <div class="col-sm-4">
          <div class="col-sm-12">
            <label class="col-sm-12 ">کاپی سوم<span style="color:red"> *</span></label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="third_copy" value="{!!old('third_copy')!!}" placeholder="کاپی سوم" required="required" />
          </div>
        </div>
        <div class="col-sm-5">
          <div class="col-sm-12">
            <label class="col-sm-12 ">مرسل<span style="color:red"> *</span></label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="sender" id="sender" style="width: 100%" required="required">
              <option value="ریاست نظارت و ارزیابی">ریاست نظارت و ارزیابی</option>
            </select>
          </div>
        </div>

      </div>

      <div class="form-group">

        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">مرسل الیه<span style="color:red"> *</span></label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="sent_to" id="sent_to" style="width: 100%" required="required">
              <option value="0">ندارد</option>
	            {!!getOrganizations()!!}    
            </select>
          </div>
        </div> 
        <div class="col-sm-4">
          <div class="col-sm-12">
            <label class="col-sm-12 ">موضوع<span style="color:red"> *</span></label>
          </div>
          <div class="col-sm-12">
            <textarea cols="20" rows="3" class="form-control" name="subject" placeholder="موضوع" required="required"></textarea>
          </div>
        </div> 
        <div class="col-sm-5">
          <div class="col-sm-12">
            <label class="col-sm-12 ">کاپی به مراجع<span style="color:red"> *</span></label>
          </div>
          <div class="col-sm-12">
            <select class="form-control" name="reference_copy[]" id="reference_copy" style="width: 100%" required="required" multiple="multiple">
              <option value="0">ندارد</option>
              {!!getOrganizations()!!}    
            </select>
          </div>
        </div>

      </div>

      <div class="form-group">

        <div class="col-sm-3">
          <div class="col-sm-12">
            <label class="col-sm-12 ">تعداد اوراق<span style="color:red"> *</span></label>
          </div>
          <div class="col-sm-12">
            <input type="number" class="form-control" name="number_of_papers" placeholder="تعداد اوراق" required="required" />
          </div>
        </div> 
        <div class="col-sm-4">
          <div class="col-sm-12">
            <label class="col-sm-12 ">کارتن مربوطه<span style="color:red"> *</span></label>
          </div>
          <div class="col-sm-12">
            <input type="text" class="form-control" name="related_carton" value="{!!old('related_carton')!!}" placeholder="کارتن مربوطه" required="required" />
          </div>
        </div> 
        <div class="col-sm-5">
          <div class="col-sm-12">
            <label class="col-sm-12 ">ضمایم</label>
          </div>
          <div class="col-sm-12">
            <div class="input_fields_wrap">
                <input type='file' id='files' style="width:87%;display:inline-block" name='files[]' class="form-control" multiple='multiple'>
                <a class="add_field_button btn" id="add" style="background: green;color:white;display:inline-block;margin-top:-20px;" title="Add another file"> + </a>
            </div>
          </div>
        </div>

      </div>

      {!!Form::token()!!}
      <hr />
      <div class="form-group" style="margin-left: -18em">
        <label class="col-sm-2 control-label"></label>
        <div class="col-sm-4">
            <input type="submit" value="صادر" class="btn btn-success" id="export_doc" />
            <a href="javascript:history.back()" class="btn btn-warning"> برگشت <i class="fa fa-arrow-left"></i></a>
        </div>
      </div>
    </form>
  </div>
</div>

@stop

@section('footer-scripts') 
<script type="text/javascript">

  $("#incoming_ids").select2();
  $("#sender").select2();
  $("#sent_to").select2();
  $("#reference_copy").select2();
  $("#assignee").select2();
  $("#year").select2();

  $(function(){
    $("#searchclear").click(function(){
      $("#incoming_ids").select2('val', '');
      $("#sender").select2('val', '');
      $("#sent_to").select2('val', '');
      $("#reference_copy").select2('val', '');
      $("#assignee").select2('val', '');
      $("#year").select2('val', '');
    });
  });

  $(function(){

    $('#issue_type').on('change', function(){

      var selected_value = $("#issue_type").val();

      if(selected_value == "1"){
        $('#incoming_number').show('slow');
        $('#issue_form').show('slow');
        $('#assignee_div').hide('slow');
        $('#issued_doc_type').prop('required', false);
      }
      else if(selected_value == "2")
      {
        $('#issue_form').show('slow');
        $('#incoming_number').hide('slow');
        $('#assignee_div').show('slow');
        $('#issued_doc_type').prop('required', true);
      }
      else
      {
        $('#incoming_number').hide();
        $('#issue_form').hide();
        $('#assignee_div').hide();
      }
    });

    $("#year").on("change", function(){
        var year = $("#year").val();
        $.ajax({
            type: 'POST',
            url: '{!!URL::route("getPendingsBasedOnYear")!!}',
            data: "year="+year,
            beforeSend: function(){
                $('#pendings').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#pendings').html(response);
            }
        });
        return false;
    });

  });

	$(function(){     
        
      $("#export_doc").submit(function()
      {
        $("#export_doc").prop('disabled', true);
        $("#export_doc").hide("fast");
      });

        // repeat the input fields ===================================
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
       
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-append"><input style="width:87%;display:inline-block" type="file" id="appendedInputButton" name="files[]" class="form-control"><a class="remove_field btn" id="remove" style="background: red;color:white;display:inline-block;margin:-20px 0 0 5px" title="remove"> X </a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            $('#add').fadeIn("slow");
            $('#appendedInputButton').fadeIn("slow");
            e.preventDefault(); $(this).parent('div').remove(); x--;
        });

    });

</script> 

@stop