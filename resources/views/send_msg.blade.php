@extends('layouts.master')
@section('head')
    <title>Create Application</title>
@stop

@section('content')

<div class="box-content">
<form action="{{URL::route('send_msg')}}" method="post" class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-2 control-label">From: </label>
		<div class="col-sm-4">
			<input class="form-control" type="text" name="from" id="from">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">To: </label>
		<div class="col-sm-4">
			<input class="form-control" type="text" name="to" id="to">
		</div>	
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Message: </label>
		<div class="col-sm-8">
			<textarea class="form-control" rows="4" name="message" id="message"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">&nbsp;</label>
		<div class="col-sm-2">
			<button class="btn btn-primary" type="submit"><i class="fa fa-envelope fa-lg"></i> Send Message</button>
		</div>
	</div>

</form>
@stop