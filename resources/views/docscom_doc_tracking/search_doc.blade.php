@extends('layouts.master')
@section('content')

 <style type="text/css">
   th
  {
    background:#EDBB99;
    font-weight: bold;
    text-align: center;
    margin-top: 2px;
  }

   </style>
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>سیستم معلوماتی مدیریت  تعقیب احکام  و اوامر</h3></center>
    <ol class="breadcrumb">
     
      <!-- <li class="active"><h3>اضافه نمودن اطلاعت</h3></li> -->
    </ol>
  </div>
  
  <div class="cl-mcont" id="sdu_result" >
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach


          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <div class="tab-pane cont" id="tab2"> 
          <!--Add New Department -->
          
          <div class="col col-12">

            <div class="header">

            <br>
            
             <div class="col-md-6">
             </div><br><br>
               <form class="form-horizontal group-border-dashed" action="export_excel" method="post" id="search_inge" >
               <div class="form-group">
              <input type="hidden" name="_token" value="{!!csrf_token()!!}">
              <div class="col-sm-3" >
                <label>معاونیت مربوطه</label>
                <select class="form-control" name="dup_name">
                  <option value="">برای انتخاب  معاونیت  </option>
                  @foreach($deputy as $dup)
                 <option value="{{$dup->id}}">{{$dup->dup_name}}</option>
                 @endforeach
                </select>
              </div>
              <div class="col-sm-3" >
                <label>مرجع ارسال کننده </label>
                <select class="form-control" name="sender_name">
                  <option value="">برای انتخاب  مرجع ارسال </option>
                  @foreach($sender as $send)
                 <option value="{{$send->id}}">{{$send->sender_name}}</option>
                 @endforeach
                </select>
              </div>
              <div class="col-sm-3" >
                <label>مرجع اجراکننده </label>
                <select class="form-control" name="exe_name">
                  <option value="">برای انتخاب  مرجع اجراکننده  </option>
                  @foreach($executing as $exec)
                 <option value="{{$exec->id}}">{{$exec->exe_name}}</option>
                 @endforeach
                </select>
              </div>
              <div class="col-sm-3" >
                <label>حالت اجرا</label>
                <select class="form-control" name="condition">
                  <option value="">-----</option>
                  <option value="1">حفظ شده</option>
                  <option value="2">تحت کار</option>
                  <option value="3">ازحیث ارتباط</option>
                </select>
              </div>

              <div class="col-sm-2" align="right">
                 <label style="font-size: 20px;">تاریخ ختم </label>
               <input type="text" class="datepicker_farsi form-control" id="to_date" name="to_date" >
               
               </div>
              <div class="col-sm-2" align="right">
               <label style="font-size: 20px;">تاریخ شروع</label>
               <input type="text"class="datepicker_farsi form-control"id="from_date" name="from_date" >
               </div>
               
              <div class="col-sm-3" align="right">
                <label style="font-size: 20px;">شماره  وارده</label>
              <input type="text"  class="form-control" id="import_no" name="import_no" >
              </div>
              <div class="col-sm-3" align="right">
              <label style="font-size: 20px;">شماره سند</label>
              <input type="text" class="form-control" name="doc_no" >
              </div>
              <div class="col-sm-2" align="right">
              <label style="font-size: 20px;">نمبر حکم </label>
              <input type="text" class="form-control" id="order_no" name="order_no" >
              </div>
              <div class="col-sm-2" align="right">
              <label style="font-size: 20px;">نمبر  صادره</label>
              <input type="text"class="form-control" id="executive_id" name="export_no" >
              </div>
              </div>
              </div>
              {!!Form::token()!!}
              <div class="form-group" >
              <input type="button" value="Search" class="btn btn-success" id="search_ing_submit" style="background-color: #008080" />
              <a href="{!!URL::route('listOfdocument')!!}">
              <input type="button" value=" برگشت به صفحه  " id="add_department" class="btn btn-info"/>
              </a>
              <input type="submit" value="Eexel" id="add_department" class="btn btn-success"/>
              </div>
              </div>
              </form>
              </div>
              <hr />
              <div class="content">
              <table class="table table-bordered table-responsive"style="border:0px solid #E67E22;">
              <thead>
                    <!--  <tr>
                     <th>شماره#</th>
                     <th>نوع  سند</th>
                     <th>نمبر سند</th>
                     <th>تاریخ سند</th>
                     <th>نمبر وارده</th>
                     <th>تاریخ وارده</th>
                     <th>مرجع ارسال کننده</th>
                     <th>معاونیت مربوطه</th>
                     <th>مرجع اجرا کننده </th>
                     <th>موعد اجرا</th>
                     <th>خلص  مطلب</th>
                     <th>نمر حکم</th>
                     <th>تاریخ حکم </th>
                     <th>نمبر صادره </th>
                     <th>تاریخ صادره</th>
                     <th>حفظ/تحت کار/لا اجرا</th>
                     <th>دلایل</th>               
                     <th>دونلود</th>
                     <th>تغیر</th>
                     <th>حذف</th>
                     </tr> -->
               
             </table> 
            </div>
          </div>
        

        </div>
      
                
              </div>
            </div>
          </div>
        </div>
    </div>
@stop

@section('footer-scripts') 
<script type="text/javascript">

  $(document).ready(function() {
    $('#search_ing_submit').click(function(){

      var dataString = $('#search_inge').serialize();
       $.ajax({
            type:'POST',
            url:"{!!URL::route('search_info')!!}",
            data: dataString,
            success:function(response){
               $('.content').html(response);            }
           })

          return false;
         });
  });
    


// menu activation

</script>
@stop