   <style type="text/css">
   th
  {
    background:#EDBB99;
    font-weight: bold;
    text-align: center;
    margin-top: 2px;
  }

   </style>


  <h3 style="margin-top:20px;text-align:center">صفحه راپور دهی</h3>
              <hr />
         
          
          
  <table class="table table-bordered table-responsive"style="border:0px solid #E67E22;">
            <thead>
                     <tr>
                     <th>شماره#</th>
                     <th>نوع  سند</th>
                     <th>نمبر سند</th>
                     <th>تاریخ سند</th>
                     <th>نمبر وارده</th>
                     <th>تاریخ وارده</th>
                     <th>تعداد ایام تاخیر در اجرا سند</th>
                     <th>مرجع ارسال کننده</th>
                     <th>معاونیت مربوطه</th>
                     <th>مرجع اجرا کننده </th>
                     <th>موعد اجرا</th>
                     <th>خلص  مطلب</th>
                     <th>نمر حکم</th>
                     <th>تاریخ حکم </th>
                     <th>نمبر صادره </th>
                     <th>تاریخ صادره</th>
                     <th>حالت اجراات</th>
                     <th>دلایل تاخیر در اجرا سند</th>             
                     <!-- <th>دونلود</th> -->
                     <th>تغیر</th>
                     <th>حذف</th>
                     </tr>
                @if(!empty($rows))
                <?php $counter=1;
                      $totale_records=0;
                      $bg_color="bgcolor"
                ?>
                     @foreach ($rows as $val)
                <?php
                       if($val->condition =="1")
                        $bg_color="color:green";
                       elseif($val->condition =="2")
                        $bg_color="color:black";
                       elseif($val->condition =="3")
                        $bg_color="color:orange";
                       else
                        $bgcolor="color:black";
                ?>
                <?php
                        if($val->import_date != "0000-00-00")
                        {
                          // add 3 days to the rasidat date which would be the expiry date.
                          $date = date_create($val->import_date);
                          $date = date_sub($date,date_interval_create_from_date_string('-'.$val->execution.'day'));
                          $expiry_date = date_format($date,"Y-m-d");
                          // print_r $date;
                          $current_date = date('Y-m-d');


                          //
                          if($current_date > $expiry_date  && $val->condition =="2")
                            $bg_color = "color:red";
                           else
                            $bg_color = "";
                        }
                        else
                          $bg_color = "";
                        ?>

                      <td bgcolor="#EDBB99">{{$counter}}</td>
                      <td style="{!!$bg_color!!}">{{$val->doc_type}} </td>
                      <td style="{!!$bg_color!!}">{{$val->doc_no}}</td>
                      <td style="{!!$bg_color!!}">{!!checkEmptyDate($val->doc_date)!!}</td>
                      <td style="{!!$bg_color!!}">{{$val->import_no}}</td>
                      <td style="{!!$bg_color!!}">{!!checkEmptyDate($val->import_date)!!}</td>

                      <td style="{!!$bg_color!!}">{{ $val->condition == '2' ? $diff = Carbon\Carbon::parse($val->import_date)->diffInDays() : $val->passed_day }}</td>
                      <td style="{!!$bg_color!!}">{{$val->sender_name}}</td>
                      <td style="{!!$bg_color!!}">{{$val->dup_name}}</td>
                      <td style="{!!$bg_color!!}">{{$val->exe_name}}</td>
                      <td style="{!!$bg_color!!}">{{$val->execution}}</td>
                      <td style="{!!$bg_color!!}">{{$val->summary}}</td>
                      <td style="{!!$bg_color!!}">{{$val->order_no}}</td>
                      <td style="{!!$bg_color!!}">{!!checkEmptyDate($val->order_date)!!}</td>
                      <td style="{!!$bg_color!!}">{{$val->export_no}}</td>
                      <td style="{!!$bg_color!!}">{!!checkEmptyDate($val->export_date)!!}</td>
                      <td style="{!!$bg_color!!}">@if($val->condition==1)حفظ شده  @elseif($val->condition==2) تحت کار @elseif($val->condition==3)از حیث ارتباط @endif</td>
                      <td style="{!!$bg_color!!}">{{$val->reasons}}</td>
                      <!-- <td><a href="{!!URL::route('download_doc_File',$val->id)!!}"><button class="btn"><i class="fa fa-download"></i></button></a></td> -->
                      <td><a href="edit_doc/{{$val-> id}}" class="fa fa-edit"></a></td>
                      <td><a href="delete_doc/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')"></a></td> 
                </tr> 
                </tr> <?php $counter++;
                $totale_records++;
                ?> 
                @endforeach 
                @else
                <tr>
                <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                </tr>
                @endif
                <h3 align='right' style="color: green; font-size:16px;font-weight: bold;">مجموع ریکاردها  : {{$totale_records or '0'}}</h3> 
                </thead>
                </table>
               
           