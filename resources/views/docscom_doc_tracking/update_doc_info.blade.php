@extends('layouts.master')
@section('content')
<div class="container" dir="rtl">
  <div class="page-head">
   <center><h3>سیستم معلوماتی مدیریت  تعقیب احکام  و اوامر</h3></center>
    <ol class="breadcrumb">
     
      <li class="active"><h3>اضافه نمودن اطلاعت</h3></li>
    </ol>
  </div>
  
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach


          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif 
      <form class="form-horizontal group-border-dashed" action="{!!URL::route('edit_doc_info',$edit_doc_data->id)!!}" method="post" enctype="multipart/form-data" style="border-radius: 0px;">
      <input type="hidden" name="_token" value="{!!csrf_token()!!}">
       <div class="col-lg-4 col-sm-4">
         <div class="form-group">
         <label >مرجع ارسال کننده</label>
         <select class="form-control" id="sender_id" name="sender_id" >
         <option >--انتخاب--</option>
           @foreach($edit_sender as $sen)
            <option value="{{$sen->id}}"<?php if ( $sen->id == $edit_doc_data->sender_id) echo "selected";?>>{{$sen->sender_name}}</option>
            @endforeach
          </select>
          <label >معاونیت مربوطه</label>
         <select class="form-control" name="deputy_id" >
         <option >--انتخاب--</option>
           @foreach($edit_deputy as $dup)
            <option value="{{$dup->id}}"<?php if ( $dup->id == $edit_doc_data->deputy_id) echo "selected";?>>{{$dup->dup_name}}</option>
            @endforeach
          </select>
           <label >مرجع اجرا کننده </label>
         <select class="form-control" id="executive_id" name="executive_id" >
         <option >--انتخاب--</option>
           @foreach($edit_executing as $exe)
            <option value="{{$exe->id}}"<?php if ( $exe->id == $edit_doc_data->executive_id) echo "selected";?>>{{$exe->exe_name}}</option>
            @endforeach
          </select>
         <label >موعد اجرا</label>
         <input type="text" class="form-control" name="execution" value="{{$edit_doc_data->execution}}"  />
         <label >خلص مطلب</label>
         <input type="text" class="form-control" name="summary" value="{{$edit_doc_data->summary}}"  />
         
         
         </div>
         </div>  

         <div class="col-lg-4 col-sm-4">
         <div class="form-group">
          <label >نمبر حکم</label>
         <input type="text" class="form-control" name="order_no" value="{{$edit_doc_data->order_no}}"  />
          <label >تاریخ  حکم</label>
          <input type="text" name="order_date" class="datepicker_farsi form-control"value ="{!!checkEmptyDate($edit_doc_data->order_date)!!}" >
         <label >نمبر صادره</label>
         <input type="text" class="form-control" name="export_no" value="{{$edit_doc_data->export_no}}"/>
         <label >تاریخ  صادره</label>
          <input type="text" name="export_date" class="datepicker_farsi form-control" value ="{!!checkEmptyDate($edit_doc_data->export_date)!!}"  >
         <label >حالت سند</label>
         <select class="form-control" name="condition" placeholder="مرد/زن" >
          <!-- <option value="{{$edit_doc_data->condition}}">--انتخاب--</option> -->
         <option value="1" @if($edit_doc_data->condition==1) selected @endif>حفظ شده</option>
         <option value="2" @if($edit_doc_data->condition==2) selected @endif>تحت کار</option>
         <option value="3" @if($edit_doc_data->condition==3) selected @endif>ازحیث ارتباط</option>
       </select>
       <label >دلایل تاخیر در اجرا سند</label>
        <input type="text" class="form-control" name="reasons" value="{{$edit_doc_data->reasons}}"> 
        
        </div>
         </div> 
          <div class="col-lg-4 col-sm-4">
         <div class="form-group">
         <label >نوع سند</label>
         <input type="text" class="form-control" name="doc_type" value="{{$edit_doc_data->doc_type}}"  />
         <label >نمبر سند</label>
         <input type="text" class="form-control" name="doc_no" value="{{$edit_doc_data->doc_no}}"/>
         <label >تاریخ  سند</label>
          <input type="text" name="doc_date" class="datepicker_farsi form-control" value ="{!!checkEmptyDate($edit_doc_data->doc_date)!!}">
         <label >نمبروارده</label>
         <input type="text" class="form-control" name="import_no" value="{{$edit_doc_data->import_no}}"   />
         <label >تاریخ  وارده</label>
          <input type="text" name="import_date" class="datepicker_farsi form-control" value ="{!!checkEmptyDate($edit_doc_data->import_date)!!}"  > 
         <!-- <label >کاپی  اسناد </label>
          <input type="file" class="form-control" name="files[]" multiple  />  -->
         </div>
         </div>       
          {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4" style="float: right;">
              <input type="submit" value="Update" id="" class="btn btn-success"/>
              <a  href="{!!URL::route('listOfdocument')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>
              

            </div>
          </div>
        </form>
  </div><hr><br>
     <div class="tab-pane active cont" id="tab1"> 
          <!-- DataTables for Contract Type -->
          <div class="">
           <div class="header">
            <br>
            <div class="row">
             
              <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form" action="">
                 <div class="input-group custom-search-form">
                 
                 {!!Form::token();!!}
                  <!-- <span class="input-group-btn">
                      <button class="btn btn-default-sm" id="search_button"><i class="fa fa-search"></i> جستجو</button>
                    </span>-->
                  </div>
                </form>
              </div>
            </div>
           <!-- <h3 style="margin-top:20px;" align="center">مدیریت عمومی تلیفون ریاست تکنالوژی معلوماتی معاونیت مالی واداری</h3><hr />-->
            </div>
            <div class="content" id="datalist">
              <div>
              
              
               <table class="table table-bordered table-responsive"  >
               
                 
                </table>
                
                
              </div>
            </div>
          </div>
        </div>
    </div>
@stop

@section('footer-scripts') 

@stop