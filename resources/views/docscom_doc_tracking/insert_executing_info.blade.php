@extends('layouts.master')
@section('content')
<div class="container" dir="rtl">
  <div class="page-head">
    <center><h3 align='center' style="color: green; font-size:40px;font-weight: bold;">سیستم معلوماتی مدیریت  تعقیب احکام  و اوامر</h3></center>
    <ol class="breadcrumb">
     
     <li class="active"><h3 align='center' style="color: green; font-size:40px;font-weight: bold;">اضافه نمودن  مرجع اجرا کننده  </h3></li>
    </ol>
  </div>
  <div class="cl-mcont" id="sdu_result">
    <div class="tab-container">
      @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
   
         @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('success')!!}
        </div>
      @elseif(Session::has('fail'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
            <span aria-hidden="true">×</span>
          </button>
          {!!Session::get('fail')!!}
        </div>
      @endif
      <form class="form-horizontal group-border-dashed" action="{!!URL::route('insert_executing')!!}" method="post" style="border-radius: 0px;">
      <input type="hidden" name="_token" value="{!!csrf_token()!!}">
       
      <div>       
      <label  style="color: green; font-size:20px;font-weight: bold;">مرجع اجرا کننده </label>
      </div>
      <input type="text" name="exe_name" class="form-control">        
        {!!Form::token()!!}
          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div style="margin-top: 10px;"class="col-sm-4">
              <input  type="submit" value="ثبت " id="add_department" class="btn btn-success"/>
              <a  href="{!!URL::route('listOfdocument')!!}"> <input type="button" value=" برگشت به صفحه 
                  " id="add_department" class="btn btn-info"/></a>

            </div>
          </div>
      </form>
      </div><hr><br>
      <div class="tab-pane active cont" id="tab1"> 
          <!-- DataTables for Contract Type -->
          <div class="">
           <div class="header">
            <br>
            <div class="row">
             
              <div class="col-md-6 pull-right" dir="ltr">
               <form class="form-horizontal group-border-dashed" id="search_form" action="">
                 <div class="input-group custom-search-form">
                 
                 {!!Form::token();!!}
                  <!-- <span class="input-group-btn">
                      <button class="btn btn-default-sm" id="search_button"><i class="fa fa-search"></i> جستجو</button>
                    </span>-->
                  </div>
                </form>
              </div>
            </div>
          
            </div>
            <div class="content" id="datalist">
              <div>
              
               <h3 align='center' style="color: green; font-size:40px;font-weight: bold;">لست  مرجع اجرا کننده  </h3>
                   <table class="table table-bordered table-responsive"  >
                  <thead>
                    <tr>
                    <th>شماره#</th>
                    <th>اسم م مرجع اجراکننده </th>
                   <!--  <th>تغیر</th>
                    <th>حذف</th> -->
                    </thead>
                                         
                     </tr>
                       @if(!empty($executing))
                     <?php $counter=1;
                       $count=0;
                    ?>               
                       @foreach ($executing as $val)

                      <td>{{$counter}}</td>
                      <td>{{$val->exe_name}}</td>
                      <!-- <td><a href="edit_equp/{{$val-> id}}" class="fa fa-edit"></a></td>          
                      <td><a href="delete_equp/{{$val-> id}} " class="fa fa-trash-o" onClick="return confirm('Do you want to delete?')" ></a></td> -->                            
                      </tr> 
                   </tr> <?php $counter++;
                             $count++;    
                     ?> 
                    @endforeach 
                    
                    @else
                    <tr>
                  <td align='center' colspan='10'>معلومات در سیستم اضافه نگردیده است</td>
                  </tr>
                    @endif
                 
                     </thead>
                  <tbody>
                  </tbody>
                 
                </table>
                
                <div class="test-center"> 
             
               </div>
              
              </div>
            </div>
          </div>
        </div>
    </div>
@stop


@stop