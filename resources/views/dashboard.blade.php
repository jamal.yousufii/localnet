@extends('layouts.master')

@section('head')
  <title>Dashboard</title>
  {!! HTML::style('/css/pages/dashboard.css') !!}

  <style type="text/css">
    .main-box i{
      font-size: 2.6em !important;
      margin-left: -16px !important;
      color: #FFF !important;
      margin-top: -21px !important;
      margin-right: -1px !important;
    }
    .main-box .value{
      text-align: left;
      font-size: 21px;
      font-weight: bold;
    }
    .main-box a{
      color: white;
    }
    .main-box a:hover{
      color: lightgray;
      text-decoration: none;
    }


    .panel-collapse{
      border: 1px solid lightgray;

    }
    .panel {box-shadow:none;}
    .panel-title {padding:8px 11px !important;}
    .panel-actions {right:11px;}
    .panel-body {padding:15px;}
    .panel-actions .panel-action {padding:8px 5px;}
    .panel-title > .icon{margin:0 !important;}


    .maincalendar {position:relative; min-height:800px;}
    .innercalendar {position:absolute; left:0;width:100%}


  </style>
  <style>

  #trash{
    width: 40px;
    height: 40px;
    padding-bottom: 8px;
    float: left;

  }

  #wrap {
    width: 1100px;
    margin: 0 auto;
  }

  #external-events {
    float: left;
    width: 100%;
    padding: 0px 10px;
    border: 1px solid #CCC;
    background: none repeat scroll 0% 0% #EEE;
    text-align: left;
    height: 60px;
  }

  #external-events h4 {
    font-size: 16px;
    margin-top: 0;
    padding-top: 1em;
  }

  #external-events .fc-event {
        margin: 10px 0px;
        cursor: pointer;
        width: 30%;
        padding: 6px;
        margin-left: 73px;

  }

  #external-events p {
    margin: 1.5em 0;
    font-size: 11px;
    color: #666;
  }

  #external-events p input {
    margin: 0;
    vertical-align: middle;
  }

  #calendar {
    float: right;
    width: 100%;
  }

  body .fc {
    font-size: 0.9em;
}


.add_todo input:focus{

  box-shadow: 0 0 5px rgba(81, 203, 238, 1);
  padding: 3px 0px 3px 3px;
  margin: 5px 1px 3px 0px;
  border: 1px solid rgba(81, 203, 238, 1);
  width: 185px !important;

}
.add_todo input{
      border:1px solid white;
      padding: 3px 0px 3px 3px;
      width: 187px !important;

}

.text_limit{
  white-space: nowrap;
    width: 15em;
    overflow: hidden;
    text-overflow: ellipsis;
    display:inline-block;
}
.text_limit a{
  text-decoration: none;
  color: #76838F;
}
.li-item
{
  padding: 7px 15px !important;
  /*background-color:#F3F7F9;*/
  border: 1px solid white;
}
.li-item:hover
{
  padding: 7px 15px !important;
  background-color:#F3F7F9;
  border: 1px solid white;
}

.fc-header tr td{
  border:0px !important;
}
.fc-header{
  border:0px !important;
}
.fc-header .fc-button{
  padding:4px;
  font-size: 14px;
}

.fc-border-separate th,.fc-head th{
  font-weight:bold;
  background: #ddd;
}

a {
    color: #52585f;
}
</style>

@stop

@section('content')
<?php $user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
?>
<div class="row">


  <div class="col-sm-8 maincalendar">
    <div class="widget">
            <div class="widget-header"> <i class="icon-bookmark"></i>
              <center>
              <h2 style="font-family: 'B Nazanin'">{!!$user->name_dr!!} {!!$user->last_name!!}</h2></br>
              <span dir="rtl" style="font-size: 20px;font-family: 'B Nazanin'">
              @if($user_dep_type==2)
              به سیستم MIS اداره امور ریاست جمهوری خوش آمدید
              @else
              به سیستم MIS ریاست جمهوری خوش آمدید
              @endif
              </span>
              </br>
              @if((Auth::user()->position_id==2 || canAttHoliday()) && $dir_leaves>0)
              <div class="panel panel-bordered" style="background-color: #f9f6f1;">
                <div class="panel-heading">
                  <center><h1 class="panel-title" style="font-size: 20px;font-weight: bold;color:#f96868;font-family: 'B Nazanin'">
                  <a href="{!!URL::route('leaveForm_add')!!}" >({!!$dir_leaves!!}) درخواست های رخصتی کارمندان</a>
                  </h1></center>
                </div>
              </div>
              @endif
              


              </center>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
              @if(canCheckAtt())
                <a href="{!!URL::route('getAttendance')!!}" class="shortcut" onclick="load_loading();">
                  <i class="icon wb-time shortcut-icon" aria-hidden="true" style="font-size: 38px;"></i>
                  <span class="shortcut-label">حاضری من</span>
                </a>
              @endif
              @if($apps)
                @foreach($apps AS $app)
                <a href="{!!URL::route('changeUserModule',$app->code)!!}" class="shortcut">
                  <i class="{!!$app->icon!!} shortcut-icon" aria-hidden="true" style="font-size: 38px;"></i>
                  <span class="shortcut-label">{!!$app->name!!}</span>
                </a>
                @endforeach
              @endif
              </div>

            </div>
            @if($user_dep_type==2)

            <div class="panel panel-bordered panel-dark">

              <div class="panel-heading">
                <center><h1 class="panel-title" style="font-size: 32px;font-weight: bold;color:#f96868;font-family: 'B Nazanin'">اطلاعیه ها</h1></center>

              </div>
                <div class="panel-collapse" dir="rtl">
                    <div class="panel-body" style="font-size: 20px;font-family: 'B Nazanin'">
                      <ul class="list-group calendar-list" id="todo_list">
                          @if($notifications)
                          @foreach($notifications as $row)
                          <li class="list-group-item li-item">
                              <i class="wb-check red-600 margin-right-10" aria-hidden="true"></i>
                              {!!$row->content!!}
                          </li>
                          @endforeach
                          @endif


                      </ul>
                  </div>
                </div>
            </div>

            @else

            <div class="panel panel-bordered panel-dark">

              <div class="panel-heading">
                <center><h1 class="panel-title" style="font-size: 32px;font-weight: bold;color:#f96868;font-family: 'B Nazanin'">اطلاعیه ها</h1></center>

              </div>
                <div class="panel-collapse" dir="rtl">
                    <div class="panel-body" style="font-size: 20px;font-family: 'B Nazanin'">
                      <ul class="list-group calendar-list" id="todo_list">

                          @if($notifications)
                          @foreach($notifications as $row)
                          <li class="list-group-item li-item">
                              <i class="wb-check red-600 margin-right-10" aria-hidden="true"></i>
                              {!!$row->content!!}
                          </li>
                          @endforeach
                          @endif

                      </ul>
                  </div>
                </div>
            </div>

            @endif
            <!-- /widget-content -->
          </div>
    </div>


    <div class="col-sm-4">
  <!-- Example Panel Toolbar -->
        <div class="panel panel-bordered panel-dark">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="icon wb-list"></i>
            @if($user_dep_type==2)
             {!!_('welcome_to_AOP_MIS_systems')!!}
            @else
              {!!_('welcome_to_OCS_MIS_systems')!!}
            @endif
             </h3>
          </div>
          <div class="panel-collapse" dir="rtl" style="font-family: 'B Nazanin'">
              <div class="panel-body">
                <ul class="list-group calendar-list" id="todo_list">
                    <li class="list-group-item li-item">
                        <i class="wb-check red-600 margin-right-10" aria-hidden="true"></i>
                        تمام فعالیت های شما در اینجا به شکل اتوماتیک در سیستم ذخیره میگردد.
                    </li>
                    <li class="list-group-item li-item">
                        <i class="wb-check red-600 margin-right-10" aria-hidden="true"></i>
                        در صورت هرگونه سؤاستفاده از این حساب کاربری، مسئولیت به عهده شما میباشد.
                    </li>
                    <li class="list-group-item li-item" style="color:red">
                        <i class="wb-check margin-right-10" style="color:#76838f" aria-hidden="true"></i>
                        بعد از اولین ورود به سیستم، پسورد خود را تغییر دهید.
                    </li>
                </ul>
            </div>
          </div>
      </div>
      <div class="panel panel-bordered panel-dark">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="icon wb-download"></i> فورمها</h3>
          </div>
          <div class="panel-collapse" dir="rtl" style="font-family: 'B Nazanin'">
              <div class="panel-body">
                <ul class="list-group calendar-list" id="todo_list">
                    <li class="list-group-item li-item">
                        <label><a href="{!!URL::route('view_leaveForm')!!}" target="_black">دانلود فورم رخصتی</a></label>
                    </li>
                    <li class="list-group-item li-item">
                        <label><a href="{!!URL::route('download_Form',array('EmailCreationForm.pdf'))!!}">دانلود فورم ایجاد ایمیل رسمی و دومین یوزر</a></label>
                    </li>
                    @if($user_dep_type==2)
                    <li class="list-group-item li-item">
                        <label><a href="{!!URL::route('download_Form',array('EmailChangeForm.pdf'))!!}">دانلود فورم تغییرات در ایمیل رسمی و دومین یوزر</a></label>
                    </li>
                    <li class="list-group-item li-item">
                        <label><a href="{!!URL::route('download_Form',array('transformDomainForm.pdf'))!!}">دانلود فورم انتقال نمودن دومین یوزر</a></label>
                    </li>
                    <li class="list-group-item li-item">
                        <label><a href="{!!URL::route('download_Form',array('shareEmailCreationForm.pdf'))!!}">دانلود فورم ایجاد ایمیل ادرس رسمی مشترک</a></label>
                    </li>
                    <li class="list-group-item li-item">
                        <label><a href="{!!URL::route('download_Form',array('changeCreationForm.pdf'))!!}">دانلود فورم تبدیل نمودن کمپیوتر</a></label>
                    </li>
                    @endif
                    <li class="list-group-item li-item">
                        <label><a href="{!!URL::route('download_Form',array('emailPasswordReset.pdf'))!!}">دانلود فورم تغییر پسورد ایمیل رسمی و دومین یوزر</a></label>
                    </li>
                    @if($user_dep_type==1)
                      <li class="list-group-item li-item">
                        <label><a href="{!!URL::route('download_Form',array('ComputerChangeRequest.pdf'))!!}">Computer Change Request Form</a></label>
                      </li>
                      <li class="list-group-item li-item">
                        <label><a href="{!!URL::route('download_Form',array('domainUserRequest.pdf'))!!}">Domain User Request Form</a></label>
                      </li>
                      <li class="list-group-item li-item">
                        <label><a href="{!!URL::route('download_Form',array('simcard.pdf'))!!}">دانلود فورم سیم کارت</a></label>
                      </li>
                    @endif
                </ul>
            </div>
          </div>
      </div>

  </div>


@stop
<script type="text/javascript">
  function load_loading()
  {
    $('.panel-body').css('opacity','0.4');
    $('.panel-body').css('pointer-events','none');
    $('#attendance_loading').show();
  }
</script>
