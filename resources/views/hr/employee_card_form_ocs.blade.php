@extends('layouts.master')

@section('head')
    <title>Employee Card Details</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
    <link href="{{ asset('crapper/cropper.min.css') }}" rel="stylesheet">
    <style>
    /* create file  */
    .custom-file-input {
        display: none;
    }
    #cropper-image{
      display: none
    }

    .cropper-wrapper{
      display: none;
      height:400px;
      width:800px;
    }
    </style>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active"><span>معلومات کارت کارمند</span></li>
        </ol>
    </div>
</div>

    <div class="row">

        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form class="form-horizontal card-form" role="form" method="post" action="{!!URL::route('postEmployeeCard',$row->id)!!}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">نام و تخلص به دری</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="name_dr" value="{!!$row->name_dr.' '.$row->last_name!!}" disabled>
                                    <span style="color:red">{!!$errors->first('name_dr')!!}</span>
                                </div>

                                <label class="col-sm-2 control-label">نام کامل به انگلیسی</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="name_en" value="{!!$row->name_en!!}">
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">نام پدر به دری</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="father_name_dr" value="{!!$row->father_name_dr!!}" disabled>
                                </div>

                                <label class="col-sm-2 control-label">نام پدر به انگلیسی</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="father_name_en" value="{!!$row->father_name_en!!}">
                                </div>
                            </div>
                        </div> -->

                        <div class="row">
                            <div class="form-group">

                                <label class="col-sm-2 control-label">وظیفه فعلی به دری</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="current_position_dr" value="{!!$row->current_position_dr!!}" disabled>
                                    <span style="color:red">{!!$errors->first('current_position_dr')!!}</span>
                                </div>

                                <label class="col-sm-2 control-label">وظیفه فعلی به انگلیسی</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="current_position_en" value="{!!$row->current_position_en!!}">
                                </div>

                            </div>
                        </div>

                        <!-- <div class="row">
                            <div class="form-group">

                                <label class="col-sm-2 control-label">موقف به دری</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="position_dr" value="{!!$row->position_dr!!}" disabled>
                                    <span style="color:red">{!!$errors->first('position_dr')!!}</span>
                                </div>

                                <label class="col-sm-2 control-label">موقف به انگلیسی</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="position_en" value="{!!$row->position_en!!}">
                                </div>

                            </div>
                        </div> -->

                        <div class="row">
                            <div class="form-group">

                                <label class="col-sm-2 control-label">عنوان کارت</label>
                                <div class="col-sm-4">

                                    <select name = "short_title_id" class="form-control">
                                        <option value=''>انتخاب</option>
                                        @if($row->dep_type==1)

                                        {!!getStaticDropdownBoth('position_short_title',$row->short_title_id)!!}
                                        @else
                                            @if($row->id == 2176)
                                                <option value="2" @if($row->short_title_id==2) selected @endif>B</option>
                                            @else
                                                @if($row->employee_type == 2)
                                                <option value="3" @if($row->short_title_id==3) selected @endif>C</option>
                                                @elseif($row->emp_bast>10)
                                                <option value="1" @if($row->short_title_id==1) selected @endif>A</option>
                                                @else
                                                <option value="1" @if($row->short_title_id==1) selected @endif>A</option>
                                                <option value="2" @if($row->short_title_id==2) selected @endif>B</option>
                                                @endif
                                            @endif
                                        @endif
                                    </select>
                                </div>

                               <label class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-sm-4">
                                    <!-- <input class="form-control" type="text" name="rfid" value="{!!$row->RFID!!}"> -->


                                    <label class="control-label">&nbsp;</label>

                                    <div class="checkbox-nice">
                                        <input <?php echo ($row->ready_to_print == 1?'checked':''); ?> value='1' type="checkbox" id="ready_to_print" name="ready_to_print">
                                        <label for="ready_to_print">
                                            برای چاپ آماده است
                                        </label>
                                    </div>


                                    <!-- <br><br>
                                    <button class="btn btn-default" type="button" id="btn_preview"><i class="fa fa-print fa-lg"></i> چاپ کارت</button>
                                 -->
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">


                              <div class="col-sm-4">
                                <div class="form-group m-form__group cropper-wrapper" style="width:200px;height:300px;">

                                    <img src="#" id="cropper-image" style="" alt=""/>
                                    <input type="hidden" id="crop_x" name="x"/>
                                    <input type="hidden" id="crop_y" name="y"/>
                                    <input type="hidden" id="crop_w" name="w"/>
                                    <input type="hidden" id="crop_h" name="h"/>
                                </div>
                              </div>
                                <div class="col-sm-4">
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;border:1px solid;">
                                      {!! HTML::image('/documents/profile_pictures/'.$row->photo) !!}
                                    </div>
                                    <div>
                                      <span class="btn btn-default btn-file">
                        								<span class="fileinput-new">انتخاب عکس</span>
                        								<span class="fileinput-exists">تغییر عکس</span>
                        								<input type="file" name="profile_pic" id="customFile">
                        							</span>
                                      <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">پاک نمودن عکس</a>
                                    </div>

                                  </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                      <div class="fileinput-new thumbnail" style="width: 5cm; height: 3cm;">
                                        <!-- <img src="http://localhost/auth/public/img/bashir_noori.png" alt="..."> -->
                                        {!! HTML::image('/documents/signatures/'.$row->signature) !!}
                                      </div>
                                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"></div>
                                      <div>
                                        <span class="btn btn-default btn-file"><span class="fileinput-new">انتخاب امضأ</span><span class="fileinput-exists">تغییرات</span><input type="file" name="signature"></span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">حذف</a>
                                      </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <br>


                        {!! Form::token() !!}

                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10">

                                <button class="btn btn-primary" type="submit">ثبت تغییرات کارت</button>

                                <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>


                            </div>
                        </div>
                    </form>

                    <!-- card print preview -->
                </div>
            </div>
        </div>
    </div>


@stop


@section('footer-scripts')
    {!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}
    <script src="{!!asset('crapper/cropper.min.js')!!}" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
       $(document).ready(function () {
         function readURL(input) {
           if (input.files && input.files[0]) {
             var reader = new FileReader();

             reader.onload = function (e) {
               $('.cropper-view-box').html('');
               $('#cropper-image').attr('src', e.target.result);
               $('.cropper-hide').attr('src', e.target.result);
               $('.cropper-wrapper').css('display','block');
               cropper();
               // FormImageCrop.init();
             }

             reader.readAsDataURL(input.files[0]);
           }
         }


         $("#customFile").change(function(){
           readURL(this);
         });
         // image cropper script end
         // 	//cropper
         function cropper() {
           const image = document.getElementById('cropper-image');
           const cropper = new Cropper(image, {
             aspectRatio: 20 / 25,
             autoCropArea: 0,
              strict: false,
              guides: false,
              highlight: false,
              dragCrop: false,
              cropBoxMovable: true,
              cropBoxResizable: false,

             crop(event) {
               $('#crop_x').attr('value', Math.ceil(event.detail.x));
               $('#crop_y').attr('value', Math.ceil(event.detail.y));
               $('#crop_w').attr('value', Math.ceil(event.detail.width));
               $('#crop_h').attr('value', Math.ceil(event.detail.height));
             },
           });
         }

        });
    </script>

<script type="text/javascript">


    $("#ready_to_print").on('change',function(){
        var ready = this.checked;
        var value= 0;
        if(ready)
        {
            value = 1;
        }

        $.ajax({
            url:'{!!URL::route("checkCardReady")!!}',
            type:'post',
            data:'ready='+value+'&id={!!$row->id!!}',
            dataType:'json',
            // success:function(r)
            // {
            //     alert(r);
            // }
        });
    });

    $("#btn_preview").on('click',function(){
        var w = window.open("{!!URL::route('getCardTemplate',$row->id)!!}");
    });

    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringRelatedSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }

</script>

@stop
