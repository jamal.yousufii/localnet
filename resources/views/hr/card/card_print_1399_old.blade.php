<html>
	<head>
		<meta charset="UTF-8">
		<style type="text/css">
		<?php $card_picture = '';  ?> 
			@font-face {
					font-family: bahji_zar;
					<?php if($row->card_type!=7 || $row->card_type!=9 || $row->card_type!=4) {   ?> 
					src:url('/fonts/bahij_zar_regular.ttf');

					<?php } ?> 

				}

				 body * {
				  font-family: bahji_zar;
				}

				.bahji_zar {
				  font-family: bahji_zar;
				}
		
			
			* {
				line-height: 1em;
				padding: 0px;
				margin: 0px;
				padding-bottom: 1px;
				padding-right: 1px;
			}

			.front{
				height: 5.4cm;
    			width: 8.5cm;
				flo/at: left;
				position:relative;
				font-family: bahji_zar;
				overflow: hidden;

			}

			.back{
				height: 5.4cm;
    			width: 8.5cm;
				flo/at: right;
				position:relative;
				font-family: 'B Titr';
				overflow: hidden;
				font-weight: bold;
			}

	        .front img {
	            height:8.5725cm;
				width:5.4cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .back img {
	            height:8.5725cm;
				width:5.4cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .do-print{
	        	font-family: 'B Titr';
            	font-size: 12pt;

	        }
	        .front .serial{
	        	position: absolute;
                z-index: 10;
                top: 5.02cm;
                left: 2.65cm !important;
                font-size: 8.5pt;
                color: white;
                font-weight: bold;

	        }
	        .front .serial_title{
	        	position: absolute;
                z-index: 10;
                top: 5cm;
                left: 3.55cm;
                font-size: 9pt;
                color: white;
                font-weight: bold;

	        }

	        .front .validaty_date_title{
	        	position: absolute;
                z-index: 10;
                left: 1.43cm;
                top: 5cm;
                font-size: 8.5pt;
                color: white !important;
                font-weight: bold;


	        }
	        .front .validaty_date {
                position: absolute;
                z-index: 10;
                left: 0.23cm;
                top: 5cm;
                font-size: 8.5pt;
                color: white;
                font-weight: bold;
            }
	        .front .degree{
	        	position: absolute;
				width: 8.5cm;
				z-index: 10;
				left: 0cm;
				top: 0.65cm;
				font-size: 14px;
				color: white;
				text-align: center;
                font-weight: bold;

	        }
	        .front .afghanistan{
	        	position: absolute;
                width: 8.5cm;
                z-index: 10;
                font-weight: bold;
                left: 0cm;
                top: 0.2cm;
                font-size: 16px;
                color: white;
                text-align: center;

	        }

            .front .photo {
	        	position: absolute;
                width: 2.485cm;
                height: 3.01cm;
                top: 1.46cm;
                left: 0.355cm;
                z-index: 10;
	        }
            

	        .front .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 1.9cm;
    			left: 3cm;
	        	width: 5.3cm;
	        	text-align: center;
	        	font-size: 13pt;
	        	font-weight: bold;
	        }

	        .front .title_value{
	        	position: abs/olute;
	        	z-index: 10;
	        	top: 5.8cm;
	        	text-align: center;
	        	width: 5.3cm;
	        	padding-bottom: 3px;
	        	font-size: 10pt;
	        	font-weight: bold;

	        }

	        .front .dep_value{
	        	position: abs/olute;
	        	z-index: 10;
	        	top: 6.2cm;
	        	text-align: center;
	        	width: 5.3cm;
	        	padding-bottom: 3px;
	        	font-size: 10pt;
	        	font-weight: bold;
	        	display: inline-block;
	        }

	        .front .info
            {
                position: absolute;
                z-index: 10;
                left: 4.9cm;
                top: 4.99cm;
                font-size: 8.5pt;
                font-weight: bold;
	        }
	        .front .blood{
	        	position: absolute;
	        	z-index: 10;
	        	right:0.2cm;
	        	top: 7.6cm;
	        	font-size: 8pt;

	        }
	        .front .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 8cm;
	        	right: 0.4cm;

	        	font-size: 11pt;

	        	color: red;

	        }


	        .front .group {
	        	position: absolute;
	        	z-index: 10;
	            top: 2.6cm;
    			left: 3cm;
	        	text-align: center;
	        	width: 5.3cm;

	        }
	        .back .serial{
                position: absolute;
                z-index: 10;
                top: 5cm;
                right: 4.8cm;
                font-size: 8pt;
                font-family: 'Times New Roman';
                color: white;
	        }
            
            .back .serial_title{
	        	position: absolute;
                z-index: 10;
                top: 4.95cm;
                right: 4.08cm;
                font-size: 8pt;
                color: white;
                font-weight: bold;

	        }

	        .back .validaty_date_title{
	        	position: absolute;
	        	z-index: 10;
	        	right:1.65cm;
	        	top: 4.95cm;
	        	font-size: 8pt;
                font-weight: bold;
                color: white;

	        }
	        .back .validaty_date{
	        	position: absolute;
                z-index: 10;
                right: 0.3cm;
                top: 4.92cm;
                font-size: 8pt;
                font-family: 'Times New Roman';
                color: white;
                font-weight: bold;
	        }
	        .back .degree
			{
	        	    position: absolute;
					width: 8.5cm;
					z-index: 10;
					left: 0cm;
					top: 0.70cm;
					font-size: 12px;
					color: white;
					text-align: center;
                    
	        }

            .back .afghanistan{
	        	position: absolute;
                width: 8.5cm;
                z-index: 10;
                font-weight: bold;
                left: 0cm;
                top: 0.2cm;
                font-size: 16px;
                color: white;
                text-align: center;

	        }

            .back .photo {
                position: absolute;
                width: 2.484cm;
                height: 3.009cm;
                top: 1.60cm;
                left: 5.70cm;
                z-index: 10;
	        }

	        .back .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 1.9cm;
    			left: 0.50cm;
	        	width: 5.2cm;
	        	text-align: center;
	        	font-size: 12pt;
	        	font-weight: b/old;
	        }

	        .back .title_value{
	        	position: abso/lute;
	        	z-index: 10;
	        	font-weight: bold;
	        	text-align: center;
	        	width: 5.2cm;
	        	padding-bottom: 4px;
	        	font-size: 10pt;
	        }

	        .back .dep_value{
	        	position: abs/olute;
	        	z-index: 10;
	        	font-weight: bold;
	        	text-align: center;
	        	width: 5cm;
	        	padding-bottom: 4px;
	        	font-size: 10pt;

	        	display: inline-block;
	        }
	        .back .info{
	        	position: absolute;
                z-index: 10;
                right: 5cm;
                top: 4.95cm;
                font-size: 7pt;
                font-family: 'Times New Roman';
                color: white;
	        }
	        .back .blood{
	        	position: absolute;
	        	z-index: 10;
	        	left:0.2cm;
	        	top: 7.6cm;
	        	font-size: 7pt;
	        }
	        .back .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 7.85cm;
	        	left: 0.6cm;

	        	font-size: 11pt;

	        	color: red;

	        }
	        .back .group {
	        	position: absolute;
	        	z-index: 10;
	        	top: 2.6cm;
    			left: 0.50cm;
	        	text-align: center;
	        	width: 5.2cm;

	        }
			.print_button{
					width: 8.5cm; 
				}

	        <?php
	        if($row->card_type==1)
	        {
				$card_picture = 'ajiran_edareamor';
	        	$card_id = 'head.jpg';$degree = 'A'; ?>

	        	.front .name_value{
		        	color: black;
		        }

		        .front .title_value{
		        	color: black;

		        }
		        .front .dep_value{
		        	color: black;
		        }

		        .front .info{
		        	color: white;

		        }
		        .front .blood{
		        	color: black;

		        }
		        .front .validaty_date_title{
		        	color: black;
		        }
		        .back .name_value{
		        	color: black;
		        }

		        .back .title_value{
		        	color: black;

		        }

		        .back .dep_value{
		        	color: black;
		        }
                .front .photo {
                    width: 2.48cm;
                    height: 3.005cm;
                    top: 1.47cm;
                    left: 0.355cm;
                }
                .back .photo {
                    width: 2.485cm;
                    height: 3.005cm;
                    top: 1.469cm;
                    left: 5.627cm;
				}

                .front .serial {
                    left: 2.80cm;
                }
                .back .serial {
					top: 4.92cm;
                    right: 3cm; 
				}


		     
	        <?php
	    	}
	        elseif($row->card_type==2)
	        {
				$card_picture = 'karmandan_edareamor'; 
	        	$card_id = 'employee.jpg';$degree = 'B'; ?>

	        	.front .name_value{
		        	color: black;
		        }

		        .front .title_value{
		        	color: black;

		        }

		        .front .dep_value{
		        	color: black;
		        }

		        .back .name_value{
		        	color: black;
		        }

		        .back .title_value{
		        	color: black;

		        }

                .front .info{
		        	color: white;

		        }

		        .back .dep_value{
		        	color: black;
		        }
				.back .serial {
					top: 4.92cm;
                    right: 3cm;
				}
				.back .photo {
				    top: 1.484cm;
                    left: 5.61cm;
				}


		    
	        <?php
	    	}
	        elseif($row->card_type==3)
	        {
	        	$card_id = 'ajir.jpg';$degree = 'C'; ?>

                .new-card-front{
                    background-size: 5.4cm 8.5cm !important;
                }
                .front{
                    width: 5.4cm !important;
                    height: 8.5cm !important;
                }
                .new-card-back {
                    background-size: 5.4cm 8.5cm !important;
                }
                .back{
                    width: 5.4cm !important;
                    height: 8.5cm !important;
                }
                .print_button {
                    width: 5.4cm;
                }
                .front .photo {
                    width: 2.49cm;
                    height: 2.82cm;
                    top: 1.23cm;
                    left: 1.415cm;
                    border-top-left-radius: 64% 68%;
                    border-bottom-left-radius: 73% 73%;
                    border-top-right-radius: 74% 67%;
                    border-bottom-right-radius: 67% 71%;
                }
                .front .afghanistan {
                    left: -2.2cm;
                    top: 0.25cm;
                    font-size: 15px;
                    color: #000;
                    text-align: center;
                }
                .front .degree {
                    width: 3.8cm;
                    left: 0cm;
                    top: 0.75cm;
                    font-size: 8px;
                    color: #000;
                }
                .front .name_value {
                    top: 4.7cm;
                    left: 0;
                    color: #ffffff;
                }
                .front .group {
                    top: 5.2cm;
                    left: 0cm;
                    margin-top: 7px;
                }
                .front .title_value {
                    color: #ffffff;
                }
                .front .dep_value {
                    color: #ffffff;
                }
                .front .serial_title {
                    top: 7.23cm;
                    left: 2.64cm;
                    color: #000;
                    font-size: 8pt;
                }
                .front .serial {
                    top: 7.25cm;
                    left: 1.8cm !important;
                    font-size: 8pt;
                    color: #000;
                }
                .front .info {
                    left: 2.7cm;
                    top: 8.1cm;
                    font-size: 5.7pt;
                    width: 100px;
                    color: #ffffff;
                }
                .front .validaty_date_title {
                    left: 1cm;
                    top: 8.1cm;
                    font-size: 6.7pt;
                    color: white !important;
                }
                .front .validaty_date {
                    left: 0.1cm;
                    top: 8.1cm;
                    font-size: 6.7pt;
                    color: white;
                }

                .back .photo {
                    width: 2.5cm;
                    height: 2.81cm;
                    top: 1.23cm;
                    left: 1.415cm;
                    border-top-left-radius: 64% 68%;
                    border-bottom-left-radius: 74% 72%;
                    border-top-right-radius: 76% 70%;
                    border-bottom-right-radius: 68% 73%;
                }
                .back .afghanistan {
                    top: 0.3cm;
                    font-size: 11px;
                    color: #000;
                    text-align: center;
                    width: 131px;
                    left: 62px;
                }
                .back .degree {
                    width: 3.8cm;
                    left: 1.56cm;
                    top: 0.75cm;
                    font-size: 9px;
                    color: #000;
                }
                .back .name_value {
                    top: 4.7cm;
                    left: 0;
                    color: #ffffff;
                }
                .back .group {
                    top: 5.2cm;
                    left: 0cm;
                    margin-top: 7px;
                }
                .back .title_value {
                    color: #ffffff;
                }
                .back .dep_value {
                    color: #ffffff;
                }
                .back .serial_title {
                    top: 7.25cm;
                    right: 2.8cm;
                    color: #000;
                    font-size: 8pt;
                }
                .back .serial {
                    top: 7.22cm;
                    right: 1.7cm !important;
                    font-size: 8pt;
                    color: #000;
                }
                .back .info {
                    right: 2.4cm;
                    top: 8.1cm;
                    font-size: 6pt;
                    width: 110px;
                    color: #ffffff;
                }
                .back .validaty_date_title {
                    right: 1.08cm;
                    top: 8.11cm;
                    font-size: 6pt;
                    color: white !important;
                }
                .back .validaty_date {
                    right: 0.09cm;
                    top: 8.09cm;
                    font-size: 6pt;
                    color: white;
                }

	        <?php
	    	}
			elseif($row->card_type==4)  /* Mawinaet Ajiran  */
	        {
	        	$card_id = 'ajir.jpg';$degree = 'C'; ?>

                .front .name_value, .title_value, .dep_value, .front .serial{
		        	color: black;
		        }
				
                .front .group{
					top: 3.4cm ; 
                    width: 4.63cm;
				}
   			    .front .photo, .back .photo {
					top: 1.90cm 
				}

		        .back .name_value, .dep_value {
		        	color: black;
		        }
				.back .name_value {
					top:  2.6cm; 
				}
				.back .group {
					top: 3.4cm; 
				}
				
                .front .photo {
                    top: 1.713cm;
                    left: 0.46cm;
                    height: 3.015cm;
                    width: 2.484cm;
                }

                .front .afghanistan {
                    left: 2.4cm;
					top: 1.6cm;
					font-size: 14px;
                    color: black !important;
                }

                .front .degree {
					top: 2.1cm; 
					color: black;
                    left: 2.42cm;
    				font-size: 12px !important;
				}

                .front .name_value {
					top: 2.8cm;
                    width: 4.5cm;
					font-size: 16px !important;
				}

                .front .title_value {
                    top: 7.9cm !important;
                    width: 4.5cm;
					font-size: 14px !important; 
                }

                .front .dep_value {
                    top: 6.2cm;   
                    width: 4.5cm;
					font-size: 14px !important; 
                }

                .front .info {
                    font-size: 6.4pt;
                    font-family: 'Times New Roman';
                    color: black;
                    font-weight: bold;
                    top: 5.01cm;
                    left: 5.25cm;
                }

                .front .serial_title {
                   color: black;
				   left: 3.95cm;
				   font-size: 10px;
                }

                .front .serial {
                    left: 2.96cm !important;
					color: black;
					font-size: 10px !important;
                }

                .front .validaty_date_title {
                    font-size: 10px;
					color: black !important;
					left: 1.49cm;
                }

                .front .validaty_date {
                    color: black !important;
					font-size: 10px;
                }

                .back .photo {
                    top: 1.71992cm;
                    left: 5.6cm;
                    width: 2.477cm;
                    height: 3.015cm;
                }
                
                .back .afghanistan {
                    left:  0.1cm;
                    top: 1.6cm;
                    font-size: 9px;
                    color: black;
                    text-align: left;
                }

				.back .name_value {
					font-size: 16px !important; 
				}

                .back .degree {
					top: 2.1cm; 
					left: 0.1cm;
					text-align: left;
					font-size: 9px; 
					color: black; 

				}

                .back .title_value {
                    width: 5cm;
					font-size: 13px !important; 
                    color: black;
                }

                .back .info {
                    right: 5.29cm;
                    color: black;
                    font-size: 6.5pt;
                }

                .back .serial_title {
                    right: 4.3cm;
					font-size: 9px;
					color: black
                }
				.back .dep_value {
					font-size: 13px !important; 
				}
                .back .serial {
                    top: 4.95cm;
					right: 3.4cm;
					font-size: 9px;
					color: black;
                }

                .back .validaty_date_title {
                    right: 1.80cm;
					top: 4.92cm;
					font-size: 9px;
					color: black;
                }

                .back .validaty_date {
                    right: 0.60cm;
					top: 4.92cm;
					font-size: 9px;
					color: black;
                }

	        <?php
	    	}
			elseif($row->card_type==9) /* Mawinat Employee  */
	        {
	        	$card_id = 'ajir.jpg';$degree = 'C'; ?>

	        	.front .name_value, .title_value, .dep_value, .front .serial{
		        	color: black;
		        }

				.front .group{
					top: 3.4cm ; 
                    width: 4.63cm;
				}
   			    .front .photo, .back .photo {
					top: 1.90cm 
				}

		        .back .name_value, .dep_value {
		        	color: black;
		        }
				.back .name_value {
					top:  2.6cm; 
				}
				.back .group {
					top: 3.4cm; 
				}
                .front .photo {
                    top: 1.725cm;
                    left: 0.455cm;
                    height: 3.009cm;
                    width: 2.48cm;
                }

                .front .afghanistan {
                    left: 2.4cm;
                    top: 1.6cm;
                    font-size: 14px;
                    color: white;
                }

                .front .degree {
					top: 2.1cm;
                    color: white;
                    left: 2.42cm;
                    font-size: 12px !important;
				}

                .front .name_value {
					top: 2.8cm;
                    width: 4.5cm;
                    color: white; 
                    font-size: 16px !important; 
				}

                .front .title_value {
                    top: 7.9cm !important;
                    width: 4.5cm;
                    color: white; 
                    font-size: 14px !important; 
                }

                .front .dep_value {
                    top: 6.2cm;   
                    width: 4.5cm;
                    color: white; 
                    font-size: 14px !important; 
                }

                .front .info {
                    font-size: 6.4pt;
                    font-family: 'Times New Roman';
                    color: white;
                    font-weight: bold;
                    top: 5.02cm;
                    left: 5.25cm;
                }

                .front .serial_title {
					left: 3.95cm;
					color: white; 
					font-size: 10px;
                }

                .front .serial {
                    left: 2.96cm !important;
					color: white;
					font-size: 10px !important;
                }

                .front .validaty_date_title {
                    font-size: 10px;
					color: white !important;
					left: 1.49cm;
                }

                .front .validaty_date {
                    color: white !important;
					font-size: 10px; 
                }

                .back .photo {
                    top: 1.598cm;
                    left: 5.695cm;
                    width: 2.48cm;
                    height: 3.005cm;
                }
                
                .back .afghanistan {
                    left:  0.1cm;
                    top: 1.6cm;
                    font-size: 9px;
                    color: white;
                    text-align: left;
                }

                .back .name_value {
                    color: white; 
                    font-size: 16px !important; 
                }

                .back .dep_value {
                    color: white;
                    font-size: 14px; 
                    font-size: 13px !important; 
                }
               
                .back .degree {
					top: 2.1cm; 
					left: 0.1cm;
					text-align: left;
					font-size: 9px; 
					color: white; 

				}
                .back .title_value {
                    width: 5cm;
                    color: white;
                    font-size: 13px !important; 
                }

                .back .info {
                    right: 5.29cm;
                    color: white;
                    font-size: 6.5pt;
                }

                .back .serial_title {
                    right: 4.3cm;
					font-size: 9px;
                    color: white;
                }

                .back .serial {
                    top: 4.95cm;
					right: 3.4cm;
					font-size: 9px;
                    color: white;
                }

                .back .validaty_date_title {
                    right: 1.80cm;
					top: 4.92cm;
					font-size: 9px;
                    color: white;
                }

                .back .validaty_date {
                    right: 0.60cm;
					top: 4.92cm;
					font-size: 9px;
                    color: white;
                }

	        <?php
	    	}
			elseif($row->card_type==5)
	        {
	        	$card_id = 'ajir.jpg';$degree = 'C'; ?>

	        	.front .name_value, .title_value, .dep_value, .info{
		        	color: white;
		        }

		        .back .name_value, .title_value, .dep_value{
		        	color: white;
		        }
				.front .photo {
                    width: 2.478cm;
                    height: 3.005cm;
                    top: 1.455cm;
                    left: 0.355cm;
                }
                .back .photo {
                    width: 2.482cm;
                    height: 3.01cm;
                    top: 1.4606cm;
                    left: 5.606cm;
				}
                .front .serial {
                    left: 2.80cm;
                }
                .back .serial {
					top: 4.92cm;
                    right: 3cm; 
				}

                <?php
            }
            elseif($row->card_type==7) /* Mawinat awal  Director  */
	        {
                $card_id = 'karmandan_alirotba_mawaneat.jpg';$degree = 'A'; ?>

                .back .name_value {
					top:  2.8cm;
                    color: white; 
				}
                .front .group{
					top: 3.4cm ; 
                    width: 4.63cm;
				}
                .back .group {
                    top: 3.4cm;
                }
                .front .photo {
                    top: 1.82cm;
                    left: 0.468cm;
                    height: 3.01cm;
                    width: 2.485cm;
                }

                .front .afghanistan {
                    left: 2.4cm;
                    top: 1.6cm;
                    font-size: 14px !important;
                    color: white;
                }

                .front .degree {
					top: 2.1cm; 
					color: white; 
					left: 2.42cm;
					font-size: 12px !important; 
				}

                .front .name_value {
					top: 2.8cm;
                    width: 4.5cm;
                    color: white;
                    font-size: 16px !important; 
				}

                .front .title_value {
                    top: 7.5cm !important;
                    left: 2.4cm;
                    width: 4.5cm;
                    color: white;
                    font-size: 14px !important; 
                }

                .front .dep_value {
                    top: 6.2cm;
                    width: 4.5cm;
                    color: white;
                    font-size: 14px !important; 
                }

                .front .info {
                    font-size: 6.4pt;
                    font-family: 'Times New Roman';
                    color: white;
                    font-weight: bold;
                    top: 5.02cm;
                    left: 5.25cm;
                }

                .front .serial_title {
                    color: white;
                    left: 3.95cm;
					font-size: 10px;
                }

                .front .serial {
                    left: 2.96cm !important;
					font-size: 10px !important;
                    color: white;
                }

                .front .validaty_date_title {
                    font-size: 10px;
					left: 1.49cm;
                    color: white !important;
                }

                .front .validaty_date {
                    color: white !important;
					font-size: 10px; 
                }

                .back .photo{
                    top: 1.6352cm;
                    left: 5.61cm;
                    width: 2.487cm;
                    height: 3.012cm;
                }

                .back .name_value {
                    font-size: 16px !important; 
                }
                
                .back .afghanistan {
                    left:  0.1cm;
                    top: 1.6cm;
                    font-size: 9px;
                    color: white;
                    text-align: left;
                }

                .back .degree {
					top: 2.1cm; 
					left: 0.1cm;
					text-align: left;
					font-size: 9px; 
					color: white; 

				}

                .back .title_value {
                    width: 5cm;
                    color: white;
                    font-size: 13px !important; 
                }

                .back .info {
                    right: 5.29cm;
                    color: white;
                    font-size: 6.5pt;
                }

                .back .serial_title {
                    right: 4.3cm;
					font-size: 9px;
                    color: white;
                }

                .back .serial {
                   top: 4.95cm;
				   right: 3.4cm;
				   font-size: 9px;
                   color: white;
                }

                .back .validaty_date_title {
                    right: 1.80cm;
					top: 4.92cm;
					font-size: 9px;
                    color: white;
                }

                .back .validaty_date {
                   right: 0.60cm;
					top: 4.92cm;
					font-size: 9px;
                    color: white;
                }
                .back .dep_value {
                    color: white;
                }

            <?php
            }
            elseif($row->card_type==8)
	        {
	        	$card_id = 'employee.jpg';$degree = 'B'; ?>
                .new-card-front{
                    background-size: 5.4cm 8.5cm !important;
                }
                .front{
                    width: 5.4cm !important;
                    height: 8.5cm !important;
                }
                .new-card-back {
                    background-size: 5.4cm 8.5cm !important;
                }
                .back{
                    width: 5.4cm !important;
                    height: 8.5cm !important;
                }
                .print_button {
                    width: 5.4cm;
                }
                .front .photo {
                    width: 2.486cm;
                    height: 2.8cm;
                    top: 1.23cm;
                    left: 1.415cm;
                    border-top-left-radius: 65% 68%;
                    border-bottom-left-radius: 71% 71%;
                    border-top-right-radius: 69% 72%;
                    border-bottom-right-radius: 67% 66%;
                }
                .front .afghanistan {
                    left: -2.3cm;
                    top: 0.25cm;
                    font-size: 16px;
                    color: #000;
                    text-align: center;
                }
                .front .degree {
                    width: 3.8cm;
                    left: 0cm;
                    top: 0.75cm;
                    font-size: 10px;
                    color: #000;
                }
                .front .name_value {
                    top: 4.7cm;
                    left: 0;
                    color: #ffffff;
                }
                .front .group {
                    top: 5.2cm;
                    left: 0cm;
                    margin-top: 7px;
                }
                .front .title_value {
                    color: #ffffff;
                }
                .front .dep_value {
                    color: #ffffff;
                }
                .front .serial_title {
                    top: 7.23cm;
                    left: 2.64cm;
                    color: #000;
                    font-size: 8pt;
                }
                .front .serial {
                    top: 7.25cm;
                    left: 1.8cm !important;
                    font-size: 8pt;
                    color: #000;
                }
                .front .info {
                    left: 2.7cm;
                    top: 8.1cm;
                    font-size: 6pt;
                    width: 100px;
                    color: #ffffff;
                }
                .front .validaty_date_title {
                    left: 1cm;
                    top: 8.1cm;
                    font-size: 6.7pt;
                    color: white !important;
                }
                .front .validaty_date {
                    left: 0.1cm;
                    top: 8.1cm;
                    font-size: 6.7pt;
                    color: white;
                }

                .back .photo {
                    width: 2.49cm;
                    height: 2.8cm;
                    top: 1.23cm;
                    left: 1.415cm;
                    border-top-left-radius: 64% 68%;
                    border-bottom-left-radius: 71% 72%;
                    border-top-right-radius: 70% 73%;
                    border-bottom-right-radius: 71% 65%;
                }
                .back .afghanistan {
                    top: 0.3cm;
                    font-size: 11px;
                    color: #000;
                    text-align: center;
                    width: 131px;
                    left: 62px;
                }
                .back .degree {
                    width: 3.8cm;
                    left: 1.56cm;
                    top: 0.75cm;
                    font-size: 9px;
                    color: #000;
                }
                .back .name_value {
                    top: 4.7cm;
                    left: 0;
                    color: #ffffff;
                }
                .back .group {
                    top: 5.2cm;
                    left: 0cm;
                    margin-top: 7px;
                }
                .back .title_value {
                    color: #ffffff;
                }
                .back .dep_value {
                    color: #ffffff;
                }
                .back .serial_title {
                    top: 7.25cm;
                    right: 2.8cm;
                    color: #000;
                    font-size: 8pt;
                }
                .back .serial {
                    top: 7.22cm;
                    right: 1.7cm !important;
                    font-size: 8pt;
                    color: #000;
                }
                .back .info {
                    right: 2.4cm;
                    top: 8.1cm;
                    font-size: 6pt;
                    width: 110px;
                    color: #ffffff;
                }
                .back .validaty_date_title {
                    right: 1.08cm;
                    top: 8.11cm;
                    font-size: 6pt;
                    color: white !important;
                }
                .back .validaty_date {
                    right: 0.09cm;
                    top: 8.09cm;
                    font-size: 6pt;
                    color: white;
                }
	        <?php
	    	}
	        else
	        {
	        	$card_id = 'employee.jpg';$degree = 'B';
	        }
	        ?>
	        .new-card-front {
			  background-image: url('{!! asset('img/aop_cards/1399_simple/'.$row->card_picture.'_front.jpeg') !!}');
			  background-size: 8.5cm 5.4cm;
			  background-repeat: no-repeat;

			}
			.new-card-back {
			  background-image: url('{!! asset('img/aop_cards/1399_simple/'.$row->card_picture.'_back.jpeg') !!}');
			  background-size: 8.5cm 5.4cm;
			  background-repeat: no-repeat; 

			}
		</style>

	</head>
	<?php
	$img = Image::make(file_get_contents('documents/profile_pictures/'.$row->photo ));

		$img->encode('jpg');
		$type = 'jpg';
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);

	?>
	<body>
		<div class="do-print">
	        <div class="row" style="margin-left:1px;">
	           <div class="col front new-card-front" dir='rtl'>
	           	<!-- {!!$row->eid!!} -->
	           		<?php
	           			//$position_ids = array(1,2,3,11);
	           			//$position_ids = array(11,3,17,18);
	           			$position_ids = array(11,17,18);

	           			$dep_id = $row->sudep_id;
	           			$dep_code = $dep_id;
	           			if($dep_id<10)
	           			{
	           				$dep_code = '00'.$dep_id;
	           			}
	           			elseif($dep_id < 100)
	           			{
	           				$dep_code = '0'.$dep_id;
	           			}


	     				$emp_id = $row->eid;
	     				$emp_code = $emp_id;

	     				if($emp_id<10)
	     				{
	     					$emp_code = '000'.$emp_id;
	     				}
	     				elseif($emp_id < 100)
	     				{
	     					$emp_code = '00'.$emp_id;
	     				}
	     				elseif($emp_id <1000)
	     				{
	     					$emp_code = '0'.$emp_id;
	     				}

	           		?>

	           	<img src="{!!$base64!!}" class="photo">
	           	<div class="name_value">{!!$row->name_dr!!} {!!$row->last_name!!}</div>
	           	<div class="group">
		           	<table cellspacing=0 cellpadding=0>

		           		<tr>
		           			<td>
		           				<div class="title_value">{!!$row->current_position_dr!!}</div>

		           			</td>
		           		</tr>

		           		<tr>
		           			<td>
		           				<div class="dep_value">

		           			<?php
		           			/*
		           				if(in_array($row->position_id, $position_ids))
		           				{
		           					if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58)
		           					{//frist and second deputy
		           						echo $row->department_dr;
		           					}
		           					else
		           					{
		           						echo $row->general_department_dr;
		           					}
		           				}
		           				else
		           				{
		           					echo $row->department_dr;
		           				}
							   */
							if($dep_id != 92 && $dep_id !=93)    
		           			 echo $row->department_dr;
		           			//echo $row->position_id;
		           			?>

		           				</div>

		           			</td>
		           		</tr>
		           	</table>
	      	   	</div>
                <div class="afghanistan">دافغانستان اسلامی جمهوریت</div>	 
				<div class="degree">{{Config::get('myConfig.card_title.dr.'.$row->card_title)}}</div>	 
                <div class='serial'>{!!$dep_code!!}{!!$emp_code!!}</div>
                <div class='serial_title'>شماره کارت</div>
                <?php 
                if($row->card_type==3 OR $row->card_type==4 OR $row->card_type==7 OR $row->card_type==8 OR $row->card_type==9)
                {
                ?>
                    <div class="validaty_date_title">تاریخ انقضا‌ء:</div>
                <?php 
                }else{
                ?>
                    <div class="validaty_date_title">تاریخ انقضا</div>
                <?php 
                }
                ?>
	           	<div class="validaty_date">1400/12/29</div>
	           	<div class="info">info@aop.gov.af +93202143608</div>
	           </div>

	           	<div class="col back new-card-back">
	           		<img src="{!!$base64!!}" class="photo">

		       		<div class="name_value">{!!$row->name_en!!}</div>
		       		<div class="group">
			           	<table cellspacing=0 cellpadding="0" style="line-height: 1em;">
			           		<tr>
			           			<td>
			           				<div class="title_value">{!!$row->current_position_en!!}</div>

			           			</td>
			           		</tr>
			           		<tr>
			           			<td>
			           				<div class="dep_value">
			           			<?php
			           			/*
			           				if(in_array($row->position_id, $position_ids))
			           				{
			           					if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58)
			           					{//frist and second deputy
			           						echo $row->department_en;
			           					}
			           					else
			           					{
			           						echo $row->general_department_en;
			           					}
			           				}
			           				else
			           				{
			           					echo $row->department_en;
			           				}
								   */
							if($dep_id != 92 && $dep_id !=93)  	   
			           			echo $row->department_en;
			           			//echo $row->department_en;
			           			?>
			           				</div>
			           			</td>
			           		</tr>
			           	</table>
                    </div>
                    {{-- <div class="afghanistan">دافغانستان اسلامی جمهوریت</div>
					<div class="degree">{{Config::get('myConfig.card_title.en.'.$row->card_title)}}</div>	
	           		<div class='serial'>{!!$dep_code!!}{!!$emp_code!!}</div>
		           	<div class="info">info@aop.gov.af +93202143606</div>
		           	<div class="validaty_date_title">Expiry Date</div>
		           	<div class="validaty_date">19/03/2022</div>
                    <div class="degree">{!!$degree!!}</div> --}}

                <div class="afghanistan">Islamic Republic of Afghanistan</div>                    	 
				<div class="degree">{{Config::get('myConfig.card_title.en.'.$row->card_title)}}</div>	 
                <div class='serial'>{!!$dep_code!!}{!!$emp_code!!}</div>
                <div class='serial_title'>ID Card</div>
                <?php 
                if($row->card_type==3 OR $row->card_type==4 OR $row->card_type==7 OR $row->card_type==8 OR $row->card_type==9)
                {
                ?>
                    <div class="validaty_date_title">Expiry Date:</div>
                <?php 
                }else{
                ?>
                    <div class="validaty_date_title">Expiry Date</div>
                <?php 
                }
                ?>
	           	<div class="validaty_date">19/03/2022</div>
	           	<div class="info">info@aop.gov.af +93202143608</div>
                       

	           </div>

	        </div>
        </div>
	</body>
</html>
