@extends('layouts.master')

@section('head')
    <title>Employee Card Details</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
    
    <style type="text/css">
        @media print{

            .col-sm-4 input, .col-sm-4 select{
                border: 0px;
            }

            .col-sm-4{

                border-bottom: 1px solid black;

            }
            .photo{
                /*position:fixed;*/
                /*left:90%;*/
                /*bottom:80%;*/
                border: none;
                /*float: right;*/
                right: 8px;
                border-bottom: 0px !important;
            }

            .col-lg-12{
                border: 0px;
            }


            .main-box-header{
                padding: 10px;
                bottom: 10px;
                font-weight: bold;
            }

            #content-wrapper{
                height: 500px !important;
                overflow: hidden;
                min-height: auto ;
            }

            .row label{
                font-weight: bold;
            }

        }
    </style>
@stop
@section('content')


    <div class="row">
        
        <div class="col-lg-12">
            <div class="main-box">
                <header class="main-box-header clearfix">
                    <h2 align="center">جزییات کارت</h2>
                    <br><br>
                </header>
                
                <div class="main-box-body clearfix">
                    <form class="form-horizontal card-form" role="form" method="post" action="{!!URL::route('postEmployeeCard',$row->id)!!}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">نام و تخلص به دری</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="name_dr" value="{!!$row->name_dr.' '.$row->last_name!!}" disabled>
                                    <span style="color:red">{!!$errors->first('name_dr')!!}</span>
                                </div>               

                                <label class="col-sm-2 control-label">نام کامل به انگلیسی</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="name_en" value="{!!$row->name_en!!}" disabled>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group">

                                <label class="col-sm-2 control-label">وظیفه فعلی به دری</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="current_position_dr" value="{!!$row->current_position_dr!!}" disabled>
                                    <span style="color:red">{!!$errors->first('current_position_dr')!!}</span>
                                </div>                    

                                <label class="col-sm-2 control-label">وظیفه فعلی به انگلیسی</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="current_position_en" value="{!!$row->current_position_en!!}" disabled>
                                </div>

                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group">

                                <label class="col-sm-2 control-label">عنوان کارت</label>
                                <div class="col-sm-4">
                                    
                                    <select name = "short_title_id" class="form-control" disabled>
                                        <option value=''>انتخاب</option>
                                        {!!getStaticDropdownBoth('position_short_title',$row->short_title_id)!!}
                                    </select>
                                </div>

                               <label class="col-sm-2 control-label">اداره به انگلیسی</label>
                                <div class="col-sm-4">
                                    <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep_div',this.value)" disabled>
                                        <!-- <option value="">انتخاب</option> -->
                                        @foreach($parentDeps AS $dep_item)
                                            <option <?php echo ($row->department==$dep_item->id ? 'selected':'');?> value='{!!$dep_item->id!!}'>{!!$dep_item->name_en!!}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                
                                <label class="col-sm-2 control-label">گروپ خون</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="current_position_en" value="{!!$row->b_group!!}" disabled>
                                </div>

                                <label class="col-sm-2 control-label">اداره به دری</label>
                                <div class="col-sm-4">
                                    <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep_div',this.value)" disabled>
                                        <!-- <option value="">انتخاب</option> -->
                                        @foreach($parentDeps AS $dep_item)
                                            <option <?php echo ($row->department==$dep_item->id ? 'selected':'');?> value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                                <label class="col-sm-2 control-label">امضأ</label>
                                <div class="col-sm-4" style="border:0px;">
                                    <div style="border:1px solid black;width:5cm;height:3cm;">
                                        
                                    </div>
                                </div>

                                <label class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-sm-4 photo" style="border:0px;float:right">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                      <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;border:0px;">
                                        <!-- <img src="http://localhost/auth/public/img/bashir_noori.png" alt="..."> -->
                                        {!! HTML::image('/documents/profile_pictures/'.$row->photo) !!}
                                      </div>
                                    </div>              

                                </div>
                        </div>
                        
                        <div class="form-group no-print">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10">
        
                                <button class="btn btn-primary" type="button" onclick="window.print()"><i class="fa fa-print fa-lg"></i> چاپ</button>
                            
                                <button onclick="history.back()" class="btn btn-danger" type="button">برگشت</button>

                            
                            </div>
                        </div>

                    </form>

                    <!-- card print preview -->
                </div>
            </div>
        </div>
    </div>


@stop

@section('footer-scripts')
    {!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

@stop

