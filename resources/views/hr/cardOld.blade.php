<html>
	<head>
		<meta charset="UTF-8">
        <style type="text/css">
           <?php 
             $color = getColumn('position_short_title',array('id' => $row->pshort_id),'color'); 
             if($color)
             {
                 $color = $color; 
             }
             else 
             {
                 $color = '#000'; 
             }
           ?>
			@media screen, print{
			* {line-height: 1em;
				padding: 0px;
				margin: 0px;
			}

			.front{
				width: 5.3975cm;
				height:8.5525cm;
				flo/at: left;
				position:relative;
				font-family: 'Bahij Yakout';
				overflow: hidden;
				left: 5px;

			}

			.back{
				width: 5.3975cm;
				height:8.5425cm;
				flo/at: right;
				position:relative;
				font-family: 'Open Sans' !important;
				overflow: hidden;
				left: 5px; 
			}

	        .front img {
	            width:5.3975cm;
				height:8.5725cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .back img {
	            width:5.3975cm;
				height:8.5725cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .do-print{
	        	font-family: 'Bahij Yakout';
            	font-size: 12pt;
	        }
	        .serial{
	        	position: absolute;
	        	z-index: 10;
	        	font-size: 6pt;
	        	left:0.2934cm;
	        	top:0.366cm;

	        }

	        .front .validaty {
	        	position: absolute;
	        	z-index: 10;
	        	right:0.41cm;
	        	top: 2.3cm;
	        	font-size: 6pt;

	        }
	        .front .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	right:0.41cm;
	        	top: 2.5cm;
	        	font-size: 8pt;
	        	font-weight: bold;
	        	color: red !important;

	        }

	        .front .photo {
	        	position: absolute;
	        	width: 2.3371cm;
	        	height: 2.9213cm;
	        	top: 1.7832cm;
	        	left: 0.3823cm;
	        	z-index: 10;
	        	border: solid 1px;
	        }

	        .front .position{
	        	/*position: absolute;*/
	        	z-index: 10;
	        	top:0.250cm;
	        	padding-right:0.4325cm;
	        	font-weight: bold;
	        	font-size: 13pt;
	        	color: white !important; 
	        	display: table-cell;
	        	height: 100%;
	        	vertical-align:middle;


	        }
	        .front .box{
	        	position: absolute;
	        	z-index: 100;
	        	background-color: <?=$color;?> !important;
	        	top: 3.2354cm;
	        	left: 2.77cm;
	        	height: 1.0651cm;
	        	width: 2.6735cm;
	        	display: table;
	        }
	        .front .name{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.5861cm;
	        	right: 0.4325cm;
	        	font-size: 6pt;

	        }
	        .front .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.8772cm;
	        	right: 0.4325cm;
	        	left: 0.3823cm;
	        	font-size: 9pt;
	        	font-weight: bold;

	        }
	        .front .title{
	        	position: absolu/te;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	right: 0.4325cm;
	        	font-size: 6pt;
	        	display: inline;

	        }
	        .front .title_value{
	        	position: absol/ute;
	        	z-index: 10;
	        	to/p: 4.8772cm;
	        	right: 0.4325cm;
	        	left: 1.6214cm;
	        	width: 3.2582cm;
	        	font-size: 9pt;
	        	font-weight: bold;
	        	display: inline-block;
	        }
	        .front .dep{
	        	position: abso/lute;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	right: 0.4325cm;
	        	font-size: 6pt;
	        	display: inline;

	        }
	        .front .dep_value{
	        	position: absol/ute;
	        	z-index: 10;
	        	t/op: 4.8772cm;
	        	right: 0.4325cm;
	        	left: 1.6214cm;
	        	width: 4.5957cm;
	        	font-size: 9pt;
	        	font-weight: bold;
	        	display: inline-block;
	        }


	        .front .group {
	        	position: absolute;
	        	z-index: 10;
	        	top:5.3481cm;
	        	left: 0.3823cm;
	        	right: 0.4325cm;
	        }
	        .front td {
	        	height: 1.4cm;
	        	vertical-align: middle;
	        }


	        .back .validaty {
	        	position: absolute;
	        	z-index: 10;
	        	left:0.3894cm;
	        	top: 2.3cm;
	        	font-size: 4pt;

	        }
	        .back .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	left:0.3894cm;
	        	top: 2.5cm;
	        	font-size: 6pt;
	        	font-weight: bold;
	        	color: red !important;

	        }

	        .back .photo {
	        	position: absolute;
	        	width: 2.3371cm;
	        	height: 2.9213cm;
	        	top: 1.7832cm;
	        	right: 0.4556cm;
	        	z-index: 10;
	        	border: solid 1px;
	        }

	        .back .position{
	        	/*position: absolute;*/
	        	z-index: 10;
	        	top:0.250cm;
	        	padding-left:0.3894cm;
	        	font-weight: bold;
	        	font-size: 13pt;
	        	color: white !important;
	        	display: table-cell;
	        	vertical-align:middle;
	        	height: 100%;
	        }
	        .back .box{
	        	position: absolute;
	        	z-index: 100;
	        	background-color: <?=$color;?> !important;
	        	top: 3.2535cm;
	        	right: 2.85cm;
	        	height: 1.0287cm;
	        	width: 2.6029cm;
	        	display: table;
	        }
	        .back .name{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.9194cm;
	        	left: 0.3894cm;
	        	font-size: 4pt;

	        }
	        .back .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 5.0605cm;
	        	right: 0.4556cm;
	        	left: 0.3894cm;
	        	font-size: 8pt;
	        	/*font-weight: bold;*/

	        }
	        .back .title{
	        	position: absolu/te;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	left: 0.3894cm;
	        	font-size: 4pt;
	        	display: inline;

	        }
	        .back .title_value{
	        	position: absol/ute;
	        	z-index: 10;
	        	to/p: 4.8772cm;
	        	right: 0.4556cm;
	        	left: 0.3894cm;
	        	font-size: 8pt;
	        	/*font-weight: bold;*/
	        	display: inline-block;
	        }
	        .back .dep{
	        	position: abso/lute;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	left: 0.3894cm;
	        	font-size: 4pt;
	        	display: inline;

	        }
	        .back .dep_value{
	        	position: absol/ute;
	        	z-index: 10;
	        	t/op: 4.8772cm;
	        	right: 0.4556cm;
	        	left: 0.3894cm;
	        	font-size: 8pt;
	        	/*font-weight: bold;*/
	        	display: inline;
	        }


	        .back .group {
	        	position: absolute;
	        	z-index: 10;
	        	top:5.4146cm;
	        	left: 0.3894cm;
	        	right: 0.4556cm;
	        }

	        .back td {
	        	height: 1.1cm;
	        	vertical-align: middle;
	        }

	        .back .blood{
	        	position: absolute;
	        	z-index: 10;
	        	top: 7.7376cm;
	        	left: 0.3894cm;
	        	font-size: 4pt;

	        }
	        .back .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 7.8875cm;
	        	right: 0.4556cm;
	        	left: 0.3894cm;
	        	font-size: 13pt;
	        	font-weight: bold;
	        	color: red;

	        }

	        .back .email{
	        	position: absolute;
	        	z-index: 10;
	        	top: 8.0051cm;
	        	right: 0.485cm;
	        	
	        	font-size: 5.25pt;
	        	font-weight: bold;

	        }
	        
	        .img-water-mark-front{
				 position:fixed;
				 bottom:16px;
				 left:32px !important;
				 opacity:0.3;
				 z-index:99;
				 width: 128px !important;
				 height: 128px !important;

	        }
	        .img-water-mark-back{
				 position:fixed;
				 bottom:16px;
				 left:32px !important;
				 opacity:0.3;
				 z-index:99;
				 width: 128px !important;
				 height: 128px !important;

	        }
		}
			
		</style>

    </head>
    <?php

	   $img = Image::make(file_get_contents('documents/profile_pictures/'.$row->photo ));
       $img->encode('jpg');
       $type = 'jpg';
       $base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);

	?>
	<body>
		<div class="do-print">
	        <div class="row">
	           <div class="col front" dir='rtl'>
	           	<div class='serial'><!-- {!!$row->eid!!} -->
	           		<?php 
	           			//$position_ids = array(1,2,3,11);
	           			$position_ids = array(11,3);
	           			$dep_id = $row->dep_id;
	           			$dep_code = $dep_id; 
	           			if($dep_id<10)
	           			{
	           				$dep_code = '00'.$dep_id;
	           			}
	           			elseif($dep_id < 100)
	           			{
	           				$dep_code = '0'.$dep_id;
	           			}


	     				$emp_id = $row->eid;
	     				$emp_code = $emp_id;

	     				if($emp_id<10)
	     				{
	     					$emp_code = '000'.$emp_id;
	     				}
	     				elseif($emp_id < 100)
	     				{
	     					$emp_code = '00'.$emp_id;
	     				}
	     				elseif($emp_id <1000)
	     				{
	     					$emp_code = '0'.$emp_id;
	     				}
	           			
	           			echo $dep_code.$emp_code;
	           		?>
	           	</div>
	           	<div class="validaty">د اعتبار موده</div>
	           	<div class="validaty_date">۲۹ حوت ۱۳۹۸</div>
	           	<img src="{!!$base64!!}" class="photo">
	           	<!-- {!!HTML::image('documents/profile_pictures/'.$row->photo,'', array('class'=>'photo'))!!} -->
	           	@if($row->employee_type==2)
	           	<div class="box">
	           	<div class="position">اجیر</div>
	           	</div>
	           	@else
	           	<div class="box">
	           	<div class="position">کارمند</div>
	           	</div>
	           	@endif
	           	
	           	<div class="name">نوم</div>
	           	<div class="name_value">{!!$row->name_dr!!} {!!$row->last_name!!}</div>
	           	<img src='/img/logo-watermark.png' class="img-water-mark-front">
	           
	           	<div class="group">
	           		
		           	<table cellspacing=0 cellpadding=0><tr><td>
		           		<div class="title">وظیفه</div><br>
		           		<div class="title_value">{!!$row->current_position_dr!!}</div>
		           </td></tr><tr><td>
		           		<div class="dep">اداره</div> <br>
		           		<div class="dep_value">
		           			
		           			<?php 
		           			
		           				if(in_array($row->position_id, $position_ids))
		           				{
		           					echo $row->general_department_dr;
		           				}
		           				else
		           				{
		           					echo $row->department_dr;
		           				}
		           			
		           			//echo $row->position_id;
		           			?>

		           		</div>
		           </td></tr></table>
	      	   </div>
	            {!!HTML::image('img/Header-01.png','', array('class'=>'bg-img'))!!}
               </div>
                @if(!$print)
                  <div style="width: 5.5cm;">
                        <a href="{!!route('getCardTemplatePrint',array($row->eid,Crypt::encrypt('wahid_janabi')))!!}" style="width: 5.5cm;" class="btn btn-primary">Print</a>
                   </div>
                @endif   
	           <div class="col back">
	           
	           	<div class="validaty">Validity</div>
	           	<div class="validaty_date">19 MAR 2020</div>
	           	<img src="{!!$base64!!}" class="photo">
	           	@if($row->employee_type==2)
	           	<div class="box" >
	           	<div class="position">SUPPORT STAFF</div>
	           	</div>
	           	@else
	           	<div class="box">
	           	<div class="position">STAFF</div>
	           	</div>
	           	@endif
	           	<div class="name">Full Name</div>
	           	<div class="name_value">{!!$row->name_en!!}</div>
	           	{!!HTML::image('img/logo-watermark.png','', array('class'=>'img-water-mark-back'))!!}
	           	<div class="group">
		           	<table cellspacing=0 cellpadding=0><tr><td>
		           		<div class="title">Title</div><br>
		           		<div class="title_value">{!!$row->current_position_en!!}</div>
		           </td></tr><tr><td>
		           		<div class="dep">Department</div> <br>
		           		<div class="dep_value">
		           			<?php 
		           			
		           				if(in_array($row->position_id, $position_ids))
		           				{
		           					echo $row->general_department_en;
		           				}
		           				else
		           				{
		           					echo $row->department_en;
		           				}
		           			
		           			//echo $row->department_en;
		           			?>
		           		</div>
		           </td></tr></table>
	      	   </div>

	      	   
	      	   <div class="email">info@aop.gov.af &nbsp;+93 20 2147900</div>

	            {!!HTML::image('img/Blank-02.png','', array('class'=>'bg-img'))!!}
	           </div>

	        </div>
        </div>

        {!! HTML::script('/js/vendor/jquery/jquery.js') !!}
        {!! HTML::script('/js/vendor/bootstrap/bootstrap.js') !!}
        {!! HTML::style('css/bootstrap.min.css') !!}
@if(!$print)        
<script>
            document.addEventListener("keydown", keyDownTextField, false);
            function keyDownTextField(e) {
            var keyCode = e.keyCode;
            //list all CTRL + key combinations you want to disable
            var forbiddenKeys = new Array('p');
            var key;
            var isCtrl;
            if(window.event)
            {
            key = window.event.keyCode; //IE
            if(window.event.ctrlKey)
            isCtrl = true;
            else
            isCtrl = false;
            }
            else
            {
            key = e.which; //firefox
            if(e.ctrlKey)
            isCtrl = true;
            else
            isCtrl = false;
            }
            //if ctrl is pressed check if other key is in forbidenKeys array
            if(isCtrl)
            {
                for(i=0; i<forbiddenKeys.length; i++)
                {
                    //case-insensitive comparation
                    if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
                    {
                        alert('use print button!');

                        //window.close();
                        return false;
                    }
                }
            }
            return true;
            }

</script>
@endif
	</body>
</html>