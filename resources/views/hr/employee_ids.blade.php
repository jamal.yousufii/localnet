@extends('layouts.master')

@section('head')
    <title>All Employees</title>
    {!! HTML::style('/css/template/libs/nifty-component.css') !!}
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">لوحه معلومات</a></li>
            <li><span>کارمندان</span></li>
            <li class="active"><span>کارت هویت کارمندان</span></li>
        </ol>
    </div>
</div>

<div class="row">
@if(Session::has('success'))
	<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif
    <div class="col-lg-12">
        <div class="container-fluid">
          <form id="search_form"> 
            <div class="row">
                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">ادارۀ عمومی</label>
                        <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                <option value="">انتخاب</option>
                            @foreach($parentDeps AS $dep_item)
                                <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                            @endforeach
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="col-sm-12">
                        <label class="col-sm-12 control-label">ادارۀ مربوط</label>
                    </div>
                    <div class="col-sm-12">
                        <select style="width:100%;" class="form-control" name="sub_dep" id="sub_dep">
                            <option value='0'>همه</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="col-sm-12">
                        <label class="col-sm-12 control-label" for="">از تاریخ</label>
                    </div>
                    <input type="text" name="from_date" value="" class="form-control datepicker_farsi pdp-el" placeholder="تاریخ مقرری" pdp-id="pdp-2721823">
                </div>
                <div class="col-sm-2">
                    <div class="col-sm-12">
                        <label class="col-sm-12 control-label" for="">الی تاریخ</label>
                    </div>
                    <input type="text" name="to_date" value="" class="form-control datepicker_farsi pdp-el" placeholder="تاریخ مقرری" pdp-id="pdp-2721823">
                </div>
                <div class="col-sm-2">
                    <div class="col-sm-12">
                        <label class="col-sm-12 control-label">کارت</label>
                    </div>
                    <select name="card_issued" id="" class="form-control">
                            <option value="default">انتخاب گزینه</option>
                            <option value="with_card">دارنده کارت</option>
                            <option value="no_card">بدون</option>
                    </select>
                </div>
            </div>
            <div class="row mb-30 pl-30" style="padding-left:23px;">
                <div class="col-sm-1 mt-25">
                  <button class="btn btn-primary pull-right" type="button" onclick="getSearchResult('all')">جستجو</button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="col-lg-12" id="all">
        <div class="main-box">

            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='list'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>نام کامل</th>
                        <th>ولد</th>
                        <th>شماره تعینات</th>
                        <th>جنسیت</th>
                        <th>سکونت اصلی</th>
                        <th>رتبه</th>
                        <th>تاریخ تقرر</th>
                        <th>نوعیت کارکن</th>
                        <th>عملیه</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="rfid_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Enter RFID</h4>
            </div>
            <div class="modal-body">
                <form class="comment_form" method="post" action="{!!URL::route('saveRFID')!!}" class="form-horizontal">

                    <input type="hidden" name="record_id" id = "record_id">
                    <div class="form-group">
                        <input type="text" class="form-control" name="RFID" placehloder="Enter card RFID...">
                    </div>

                    <button type="submit" type="button" class="btn btn-primary">
                        <i class="fa fa-save fa-lg"></i> Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="reprint_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">دلیل برای باز نمودن کارت</h4>
            </div>
            <div class="modal-body">
                <form class="comment_form" method="post" action="{!!URL::route('releaseCard')!!}" class="form-horizontal">

                    <input type="hidden" name="card_id" id = "card_id">
                    <div class="form-group">
                      <input type="text" class="form-control" name="reprint_reason" placehloder="Reason...">

                    </div>

                    <button type="submit" type="button" class="btn btn-primary">
                        <i class="fa fa-save fa-lg"></i> Save</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')
<script type="text/javascript">
$("#sub_dep").select2();
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    $(document).ready(function() {
        $('#list').dataTable(
            {
                "fnDrawCallback" : function() {
                    $(document).trigger('doc:updated');
                },
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getEmployeeCardData')!!}",
                "aaSorting": [[ 1, "desc" ]],
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });

function sendBack(id)
{

    if(confirm("Are you sure?"))
    {
        $.ajax({
            url:'{!!URL::route("checkCardReady")!!}',
            type:'post',
            data:'ready=0'+'&id='+id,
            //dataType:'json',
            success:function(r)
            {
                location.reload();
            }
        });
    }
    else
    {
        return false;
    }

}
function getSearchResult(div)
{
    var form_data = $('#search_form').serialize(); 
    var dep = $('#general_department').val();
    if(dep == '')
    {
        alert('Please select a department');return;
    }
    $.ajax({
            url: '{!!URL::route("getEmployeeIDs_search")!!}',
            data: form_data,
            type: 'post',
            beforeSend: function(){

                //$("body").show().css({"opacity": "0.5"});
                $("#"+div).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {

                $('#'+div).html(response);
            }
        }
    );

    return false;

}
</script>
@stop
