<style>
thead {display: table-header-group;}
</style>
<h2 style="font-weight:bold;text-align:center;" id="search_title">لیست کارمندان - {!!getDepartmentName($department)!!}</h2>
<br>
<table class="table table-bordered" id='list'>
    <thead>
    <tr>
        <th width='5%'>شماره</th>
        <th width='12%'>نام و تخلص</th>
        <th width='12%'>عنوان وظیفه</th>
        <th>Fullname</th>
        <th>Position Title</th>
    </tr>
    </thead>
    <tbody>
        <?php $counter = 1;?>
        @foreach($rows AS $item)
        <tr>
            <td>{!!$counter!!}</td>
            <td>{!!$item->name_dr!!}</td>
            <td>{!!$item->current_position_dr!!}</td>
            <td> </td>
            <td> </td>
            
        </tr>
        <?php $counter++; ?>
        @endforeach
    </tbody>
</table>
