<div class="main-box">

    <div class="main-box-body clearfix">
        <div class="table-responsive">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>نام کامل</th>
                    <th>ولد</th>
                    <th>شماره تعینات</th>
                    <th>جنسیت</th>
                    <th>سکونت اصلی</th>
                    <th>رتبه</th>
                    <th>تاریخ تقرر</th>
                    <th>نوعیت کارکن</th>
                    <th>عملیه</th>
                </tr>
                </thead>
                <tbody>
                @if($records)
                    @foreach($records AS $row)
                        <tr>
                            <td>{!!$row->id!!}</td>
                            <td>{!!$row->name!!}</td>
                            <td>{!!$row->father_name!!}</td>
                            <td>{!!$row->number_tayenat!!}</td>
                            <td>{!!$row->gender!!}</td>
                            <td>{!!$row->original_province!!}</td>
                            <td>{!!$row->rank!!}</td>
                            <td>{!!$row->emp_date!!}</td>
                            <td>{!!trans('static.employee_type_'.$row->employee_type)!!}</td>
                            <td><?php
                            $options = '';
                            if(Auth::user()->id==174)
                						{
                							$options.='<a href="javascript:void()" class="table-link" data-target="#reprint_modal" data-toggle="modal" onclick="$(\'#card_id\').val('.$row->id.')">
                													<span class="fa-stack" title="Release Card for Reprint">
                														<span class="icon wb-replay"></span>
                													</span>
                												</a>';
                						}
                            if(canUpdateCard('hr_cards'))
                            {
                                if(Auth::user()->id!=203)
                                {
                                $options.= '<a href="'.route('getEmployeeCardDetails',$row->id).'" class="table-link" style="text-decoration:none;" title="update">
                                            <i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
                                        </a>
                                        | ';
                                }
                                $options.=
                                        ' <a href="'.route('forPrint',$row->id).'" class="table-link" style="text-decoration:none;" title="Details">
                                            <i class="icon fa-eye" aria-hidden="true" style="font-size: 16px;"></i>
                                        </a> |
                                        <a href="javascript:void()" class="table-link" data-target="#rfid_modal" data-toggle="modal" onclick="$(\'#record_id\').val('.$row->id.')">
                                        <span class="fa-stack" title="Enter RFID">
                                            <span class="icon wb-edit"></span>
                                        </span>
                                    </a>';
                            }
                            if(canPrintCard('hr_cards'))
                            {
                                if($row->employee_type == 4 || $row->employee_type == 5 || $row->employee_type == 6)
                                {//temporary employees

                                    $options.= '<a target="_blank" href="'.route('getCardTemplateTemp',$row->id).'" class="table-link" style="text-decoration:none;" title="print">
                                            <i class="icon fa-print" aria-hidden="true" style="font-size: 16px;"></i>
                                        </a>
                                        | ';
                                }
                                else
                                {

                                    $options.= '<a target="_blank" href="'.route('getCardTemplate',$row->id).'" class="table-link" style="text-decoration:none;" title="print">
                                            <i class="icon fa-print" aria-hidden="true" style="font-size: 16px;"></i>
                                        </a>
                                         |
                                        <a target="_blank" href="'.route('getCardTemplate_view',$row->id).'" class="table-link" style="text-decoration:none;" title="view card">
                                            <i class="icon fa-eye" aria-hidden="true" style="font-size: 16px;"></i>
                                        </a>';
                                }
                            }
                            echo $options;
                            ?>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>

    </div>
</div>
