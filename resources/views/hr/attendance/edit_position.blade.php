<div class="modal-dialog" style="width: 70%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" id="time_close_btn" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" id="salaryWait" method="post">
			    
                <div class="container-fluid">
                	<div class="row">
                	<div class="col-sm-12">
                		<div class="col-sm-3">
                			<label class="col-sm-12 "> </label>
                		</div>
                		<div class="col-sm-9">
			      			<span>{!!$details->current_position_dr!!}</span>
			      		</div>
			      	</div>
                	<div class="col-sm-12">
                		<div class="col-sm-9">
			      			<div class="col-sm-12">
			      				<label class="col-sm-12 ">وظیفه انگلیسی</label>
                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
                                <textarea class="form-control" row="3" col="10" name="name_en" id="name_en" required>@if($details){!!$details->current_position_en!!}@endif</textarea>
                                
			      			</div>
			      		</div>
                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
                				
                				<button class="btn btn-primary" type="button" onclick="submit_salaryWait();$('#time_close_btn').click()"> ذخیره</button>
                			
                				<button class="btn btn-danger" type="button" onclick="$('#time_close_btn').click()"> کنسل</button>
                			</div>
                		</div>
                	</div>
                	</div>
                </div>
	            <input type="hidden" id="employee_id" value="{!!$id!!}"/>
	            
				{!! Form::token() !!}
			</form>
			</div>
			</div>		
		</div>		                       
	</div>
</div>
<script>
function submit_salaryWait()
{
	var name_en = $('#name_en').val();
	var employee_id = $('#employee_id').val();
	var page = "{!!URL::route('postEmployeePosition')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&name_en='+name_en+'&employee_id='+employee_id,
        success: function(r){
        	
        }
    });
}
</script>