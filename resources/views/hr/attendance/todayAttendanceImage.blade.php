@extends('layouts.master')

@section('head')
	
	</style>
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	<form class="form-horizontal" role="form" method="post" action="{!!URL::route('getRelatedEmployees')!!}">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>حاضری کارمندان</span></li>
        </ol>
    </div>
</div>
<header class="main-box-header clearfix">
   
    	<div class="row">
        	<div class="col-lg-12">
        	
        		<div class="container-fluid">
			      	<div class="row">
						<div class="col-sm-2">
                			<div class="col-sm-12">
								<label for="from_date">از تاریخ:</label>
								<input type="text" name="from_date" id="from_date" value="{{$from_date}}" class="form-control datepicker_farsi" placeholder="شروع تاریخ">
							</div>
						</div>
						<div class="col-sm-2">
                			<div class="col-sm-12">
								<label for="to_date">الی تاریخ:</label>
								<input type="text" name="to_date" id="to_date" value="{{$to_date}}" class="form-control datepicker_farsi" placeholder="ختم تاریخ">
							</div>
                        </div>
                        <div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">وضعیت</label>
                                <select class="form-control" name="status" id="status">
                                    <option value='0'>انتخاب</option>
                                    <option value='1' @if($status==1) {!!'selected'!!} @endif>حاضر</option>
									<option value='2' @if($status==2) {!!'selected'!!} @endif>غیرحاضر</option>
									<option value="show_img" @if($status=='show_img') {!!'selected'!!} @endif>تصاویر غیر حاضران</option>
                                </select>
                			</div>
                		</div>
					</div>
					<div class="row">
						<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
								<a href="javascript:void()" onclick="dailyAttendanceReport()" class="btn btn-primary"> راپور حاضری روزانه</a>
							</div>
						</div>
						<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
								<a href="javascript:void()" onclick="dailyAttendancePrint()" class="btn btn-primary">پرنت اکسل</a>
							</div>
						</div>
						<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
								<a href="javascript:void()" onclick="dailyAttChart()" class="btn btn-primary">گراف</a>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
				        	<label class="col-sm-12 ">Present: {!!getDailyAtt($dep,$sub_dep,$type)!!}</label>
				        	</div>
				        </div>
				        <div class="col-sm-2">
				        	<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
				        		<label class="col-sm-12 ">Absent: {!!getDailyAbsent($dep,$sub_dep,$type)!!}</label>
				        	</div>
				        </div>
					</div>
				</div>
				
			</form>
			</div>
		</div>
    
</header>

<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix" style="margin-top:10px">
		    
		</header>
		<div class="modal-body">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table" id="list">
                    <thead>
                      	<tr>
	                        <th>#</th>
	                        <th>حاضری</th>
	                        <th>ماشین حاضری</th>
	                        <th>کود ماشین حاضری</th>
	                        <th>زمان حاضری</th>
	                        <th with="20">تاریخ</th>
	                        <th with="20">عکس</th>
	                    </tr>
                    </thead>
                    <tbody>
                        @if ($images)
                         <?php $counter = 1; ?>
                            @foreach ($images as $item)
                               <tr>
                                 <td>{{$counter++}}</td>
                                 <td>{{$item->status}}</td>
                                 <td>{{$item->device_name}}</td>
                                 <td>{{$item->device_id}}</td>
                                 <td>{{$item->time}}</td>
                                 <td>{{$item->date}}</td>
                                 <td>
                                    <img height="70" style="cursor: pointer" width="70" onclick="imageModal('image_{{$counter}}')" id="image_{{$counter}}" src="{{att_image_source_ip()."/".$item->path}}" alt="">
                                 </td>
                               </tr> 
                            @endforeach
                        @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>

<!-- The Modal -->
<div id="image_modal" class="modal" style="
    margin-top: 117px;
    height: 50%;
    width: 50%;
}">
  <span class="close">&times;</span>
  <img class="modal-content" id="modal_img">
  <div id="caption"></div>
</div>

@stop
@section('footer-scripts')

<script type="text/javascript">

    function imageModal(img_id)
    {   
        // Get the modal
        var modal = document.getElementById("image_modal");
        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById(img_id);
        var modalImg = document.getElementById("modal_img");
        var captionText = document.getElementById("caption");
        img.onclick = function()
        {
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }
    }

    function closeModal()
    {
        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close_image_modal")[0];

        // When the user clicks on <span> (x), close the modal
         span.onclick = function() { 
         modal.style.display = "none";
        }
    }

	function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function dailyAttendanceReport()
	{
		var general_department = $('#general_department').val();
		if(general_department=='')
		{
			general_department = 0;
		}
		var sub_dep = $('#sub_dep').val();
		if(sub_dep=='')
		{
			sub_dep = 0;
		}
		var status  = $('#status').val();
		var type  = $('#type').val();
		var from_date = $('#from_date').val(); 
		var to_date   = $('#to_date').val(); 

	    window.location = "/hr/getTodayAttendances/"+general_department+"/"+sub_dep+"/"+status+"/"+type+"/"+from_date+"/"+to_date;
	    
	}
	function dailyAttendancePrint()
	{
		var status = $('#status').val();
		var general_department = $('#general_department').val();
		if(general_department=='')
		{
			general_department = 0;
		}
		var sub_dep = $('#sub_dep').val();
		if(sub_dep=='')
		{
			sub_dep = 0;
		}
		var type = $('#type').val();
	    window.location = "/hr/printDailyAtt/"+general_department+"/"+sub_dep+"/"+status+"/"+type;
	    
	}
	function dailyAttChart()
	{
		var general_department = $('#general_department').val();
		if(general_department=='')
		{
			alert('لطفا یک اداره عمومی را انتخاب نمایید');return;
		}
		var sub_dep = $('#sub_dep').val();
		if(sub_dep=='')
		{
			alert('لطفا اداره مربوطه را انتخاب نمایید');return;
		}
		
	    window.location = "/hr/printDailyAttChart/"+sub_dep;
	    
	}
</script>
@stop

