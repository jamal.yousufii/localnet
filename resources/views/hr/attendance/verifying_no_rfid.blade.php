@extends('layouts.master')

@section('head')
    <title>{!!_('attendance')!!}</title>
   <style type="text/css">
    .att_img:hover {
    opacity: 0.5; filter: alpha(opacity=30);
}
    </style>
@stop
@section('content')

<header class="main-box-header clearfix">
	<div class="row">
    	<div class="col-lg-12">
    		<div class="container-fluid">
        		<div class="col-sm-4">
        			<div class="col-sm-12" id="img_controller">
        				<div class="icon-box pull-left">
			           		<?php
	        				$image = base64_encode(file_get_contents('documents/profile_pictures/'.$photo->photo ));
							$profile_pic = 'data:image/jpg;base64,'.$image;
							?>
							<img src="{!!$profile_pic!!}" alt="Image Not Found!" width="150" hight="224"/>

						</div>
        			</div>
        		</div>
	      		<div class="col-sm-2">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">سال</label>
                        <select name="year" id="year" class="form-control" onchange="bring_employee_images()">
                            <?php
        					//$month = explode('-',$today);
        					$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');

        					?>
                            @for($i=$year-5;$i<$year+5;$i++)
                            	@if($i==$year)
                                <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
                                @else
                                <option value='{!!$i!!}'>{!!$i!!}</option>
                                @endif
                            @endfor
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
        			</div>
        		</div>
        		<div class="col-sm-2">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">ماه</label>

                        <select name="month" id="month" class="form-control" onchange="bring_employee_images()">
                            @for($j=1;$j<13;$j++)
                            	<?php
                            	$m = $j;
                            	if($j<10) $m='0'.$j;
                            	?>
                            	@if($m==$month)
                                <option value='{!!$m!!}' selected="selected">{!!$month_name[$j]!!}</option>
                                @else
                                <option value='{!!$m!!}'>{!!$month_name[$j]!!}</option>
                                @endif
                            @endfor
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
        			</div>
        		</div>

        		<div class="col-sm-1">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">&nbsp;</label>
        				<a href="{!!URL::route('getRelatedEmployees_get',array($dep,$sub_dep,$year,$month))!!}" class="btn btn-primary">برگشت به لیست</a>
        			</div>
        		</div>

			</div>
			<div class="container-fluid">
		      	<div class="row">
            		<div class="col-sm-3">
            			{!!$photo->current_position_dr!!}
            		</div>
            	</div>
            </div>
		</div>
	</div>
</header>

<!-- Example Tabs -->
<div class="row">
	<div class="col-lg-12">
	<table width="100%">
	<?php
	$img_source_date = att_img_source_date();
    $img_source_ip = att_image_source_ip();
	$sday = att_month_days(0);
	$eday = att_month_days(1);
    
    $from = calculate_attendence_date($sday,$month,$year,'start_date');
	$to = calculate_attendence_date($eday,$month,$year,'end_date');
	$from_shamsi = convertDateAndTime($from,'to_shamsi');
    $from_shamsi = explode('-',$from_shamsi);
    //shift for those who is completely off on thu
	$thu_shift = is_in_university_shift($rfid,$from_shamsi[0],$from_shamsi[1],0); // $from_shamsi index 0 is year index 1 is month
	$sun_shift = is_in_university_shift($rfid,$from_shamsi[0],$from_shamsi[1],3); // $from_shamsi index 0 is year index 1 is month
    $sat_shift = is_in_university_shift($rfid,$from_shamsi[0],$from_shamsi[1],4); // $from_shamsi index 0 is year index 1 is month 
    $night_shift = false;

	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	$day_counter=1;
	?>
	@foreach($period AS $day)
		<?php
			$the_day = $day->format( "Y-m-d" );
			$dayOfWeek = date('D',strtotime($the_day));
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_monthDay = $period_month.$period_day;
			$shamsi_day = dateToShamsi($period_det[0],$period_det[1],$period_det[2]);
			$imgin_id = '';$imgout_id = '';$signin_time='';$signout_time='';
			$correct = false;$sign_in = false;$sign_out = false;
			$signin_img = '/img/default.jpeg';$signout_img = '/img/default.jpeg';
			$rejected_in = 0;$rejected_out = 0;
			$late_status = 0;

			//leaves
			$isInLeave = isEmployeeInLeave($photo->id,$the_day,$year,$month);
		?>
		@if($details)
        <?php $night_shift_in = false;?>
		@foreach($details as $img)
			<?php
			$path = explode("_",$img->path);
            $time = (int)substr($path[0],-6);//get the time part in path
            $month_day = (!empty($path[0]) ? substr($path[0],-11,4) : 0);//get the monthDay part in path
			if($period_monthDay == $month_day)
			{
				$correct = true;
				if($night_shift)
				{
					if(!$$month_day)
					{
						$night_shift_in = true;
						$sign_in = true;
						if($year.$month < $img_source_date)
						{
							$signin_img = "/att_img/upload".$img->path;
						}
						else
						{
							$signin_img = $img_source_ip."/".$img->path;
						}

						$imgin_id = $img->id;
						$rejected_in = $img->status;

						$late_status_in = $img->late_status;
					}
					else
					{
						$sign_out = true;
						if($year.$month < $img_source_date)
						{
							$signout_img = "/att_img/upload".$img->path;
						}
						else
						{
							$signout_img = $img_source_ip."/".$img->path;
						}
						//$signout_img = "http://10.134.45.19/".$img->path;

						$imgout_id = $img->id;
						$rejected_out = $img->status;

						$late_status_out = $img->late_status;
					}
				}
				else
				{
					if($time <= 120000)
					{
						$sign_in = true;
						if($year.$month < $img_source_date)
						{
							$signin_img = "/att_img/upload".$img->path;
						}
						else
						{
							$signin_img = $img_source_ip."/".$img->path;
						}

						$imgin_id = $img->id;
						$rejected_in = $img->status;

						$late_status_in = $img->late_status;
					}
					else
					{
						$sign_out = true;
						if($year.$month < $img_source_date)
						{
							$signout_img = "/att_img/upload".$img->path;
						}
						else
						{
							$signout_img = $img_source_ip."/".$img->path;
						}
						//$signout_img = "http://10.134.45.19/".$img->path;

						$imgout_id = $img->id;
						$rejected_out = $img->status;

						$late_status_out = $img->late_status;
					}
				}
			}
			?>
		@endforeach
		@endif
		<!-- sign in -->
		@if($day_counter == 1 || $day_counter%11==1)
		<tr>
		@endif
		@if($the_day <= date('Y-m-d'))

		<?php
		$holiday = checkHoliday($the_day);
		$urgents = checkUrgentHoliday($the_day);
		?>
		<td style="position: relative;padding-bottom: 2em;">
		<!-- sign in pic -->
			<div style="position: relative;">
				<!-- if there is a pic for this day then send the id to onclick function -->
				<img src="{!!$signin_img!!}" class="att_img" @if($rejected_in==0) onclick="reject_pic(@if($imgin_id!=''){!!$imgin_id!!}@else 'in' @endif,'{!!$the_day!!}','')" @else style="opacity: 0.5" @endif alt="Image Corrupt" height="112" width="90" style="cursor:pointer;border-top:1px solid;border-left:1px solid;border-right:1px solid"/>

				@if($photo->att_out_date<=$the_day)
				<?php
					if($photo->fired==1) $out_title = 'منفک';
					elseif($photo->retired==1) $out_title = 'تقاعد';
					elseif($photo->resigned==1) $out_title = 'استعفا';
					elseif($photo->changed==1) $out_title = 'تبدیل';
					else $out_title = 'حاضری کتابچه';
				?>
					<span style="position: absolute;right: 35px;top: 20px;color:red; font-size:15px;background:#FFF;padding:0 10px">{!!$out_title!!}</span>
				@else
					@if($isInLeave)
						@if($isInLeave==7)
						<span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">خدمتی</a></span>
						@elseif($isInLeave==9)
						<span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">دیگر موارد</a></span>
						@elseif($isInLeave==1)
						<span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی ضروری</a></span>
						@elseif($isInLeave==2)
						<span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی تفریحی</a></span>
						@elseif($isInLeave==3)
						<span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی مریضی</a></span>
						@elseif($isInLeave==4)
						<span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی ولادی</a></span>
						@elseif($isInLeave==5)
						<span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی عروسی</a></span>
						@elseif($isInLeave==6)
						<span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی حج</a></span>
						@elseif($isInLeave==8)
						<span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">اضافه رخصتی مریضی</a></span>
						@endif
						<span style="position: absolute;right: 35px;top: 10px;color:red; font-size:17px;padding:0 10px" id="{!!$imgin_id!!}_span">@if($rejected_in!=0) عکس قابل قبول نمیباشد @endif</span>
					@elseif($holiday)
						<span style="position: absolute;right: 35px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">رخصتی عمومی</span>
					@elseif($urgents)
						<span style="position: absolute;right: 35px;top: 20px;color:red; font-size:15px;background:#FFF;padding:0 10px">حالت فوق العاده</span>
					@elseif($dayOfWeek=='Fri')
						<span style="position: absolute;right: 35px;top: 20px;color:red; font-size:15px;background:#FFF;padding:0 10px">جمعه</span>
					@else

						<span style="position: absolute;right: 35px;top: 10px;color:red; font-size:17px;padding:0 10px" id="{!!$imgin_id!!}_span">@if($rejected_in!=0) عکس قابل قبول نمیباشد @endif</span>
					@endif
				@endif
			</div>
		<!-- sign out pic -->
			<div style="position: relative;">
				<img src="{!!$signout_img!!}" class="att_img" @if($rejected_out==0) onclick="reject_pic(@if($imgout_id!=''){!!$imgout_id!!}@else 'out' @endif,'{!!$the_day!!}','')" @else style="opacity: 0.5" @endif alt="No Image" height="112" width="90" style="cursor:pointer;border-bottom:1px solid;border-left:1px solid;border-right:1px solid"/>

				<span style="position: absolute;right: 35px;top: 10px;color:red; font-size:19px;padding:0 10px" id="{!!$imgout_id!!}_span">@if($rejected_out!=0) عکس قابل قبول نمیباشد @endif</span>
				@if($dayOfWeek == 'Thu' && $thu_shift)
					<span style="position: absolute;right: 35px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
				@elseif($dayOfWeek == 'Sun' && $sun_shift)
						<span style="position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
				@elseif($dayOfWeek == 'Sat' && $sat_shift)
						<span style="position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
				@endif
			</div>

			@if($dayOfWeek=='Fri')
			<span style="position: absolute;right: 20px;bottom: 1px;color:red">{!!$shamsi_day!!}</span>
			@else
				<div id="{!!$the_day!!}_date">
					<span style="position: absolute;right: 20px;bottom: 1px;">{!!$shamsi_day!!}</span>
				</div>
			@endif
		</td>
		@endif
		@if($day_counter%11==0)
		</tr>
		@endif

		<?php $day_counter++;?>
	@endforeach
	</table>
	</div>
</div>
<!-- End Example Tabs -->

@stop
@section('footer-scripts')
<script>
function reject_pic(img_id,theday,late='')
{
	var emp_id = "{!!$rfid!!}";
	if(img_id=='in' || img_id == 'out')
	{//there is no pic, then present the default iamge
		return;
		var page = "{!!URL::route('presentImage')!!}";
		var span = $('#'+theday+'_'+img_id).html();
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&date='+theday+'&type='+img_id+'&present='+span+'&emp_id='+emp_id,
	        success: function(r)
	        {
				if(span != 'Present')
				{
					if(img_id=='in')
					{
						$('#'+theday+'_date span').css('color','greenyellow');
					}
					else if(img_id=='out')
					{
						$('#'+theday+'_date span').css('color','yellowgreen');
					}
					else
					{
						$('#'+theday+'_date span').css('color','dark');
					}
					$('#'+theday+'_'+img_id).html('Present');
				}
				else
				{
					$('#'+theday+'_date span').css('color','black');
					$('#'+theday+'_'+img_id).html('');
				}
	        }
	    });
	}
	else
	{//there is a pic, then reject it
		var page = "{!!URL::route('rejectImage')!!}";
		var span = $('#'+img_id+'_span').html();
		var late_status = $('#'+theday+'_'+late).html();

		$.ajax({
	        url:page,
	        type:'post',
	        data: '&img_id='+img_id+'&type='+span+'&late='+late+'&date='+theday+'&emp_id='+emp_id+'&late_status='+late_status,
	        success: function(r){
				if(late !='')
				{
					if(late_status != 'Present')
					{
						$('#'+img_id+'_span').html('');
						$('#'+theday+'_'+late).html('Present');
					}
					else
					{
						$('#'+theday+'_'+late).html('');
						$('#'+img_id+'_span').html('Late');
					}
				}
				else
				{
					if(span == '')
					{
						$('#'+img_id+'_span').html('Rejected');
					}
					else
					{
						$('#'+img_id+'_span').html('');
					}
				}
	        }
	    });
	}
}
function bring_employee_images()
{
	var rfid = "{!!$rfid!!}";
	var year = $('#year').val();
	var month = $('#month').val();
	var dep = "{!!$dep!!}";
	var sub_dep = "{!!$sub_dep!!}";

    window.location = "/hr/getEmployeeAttendance/0/"+year+"/"+month+"/"+dep+"/"+sub_dep+"/"+rfid;

}
</script>
@stop
