@extends('layouts.master')

@section('head')
    <title>کشیدن کارمند از حاضری</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
	<form class="form-horizontal" role="form" id="att_report" method="post">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>حاضری کارمندان</span></li>
        </ol>
        @if(Auth::user()->id == 715 || Auth::user()->id == 1286 || Auth::user()->id == 450 || Auth::user()->id == 113)
        <!-- jawadi roles -->
        	<a href="{!!URL::route('putImagesToHr')!!}" onclick="load_loading();" class="btn btn-primary">علاوه نمودن عکس ها</a>

        	<a href="{!!URL::route('notifyManager')!!}" class="btn btn-primary">اطلاعیه ها</a>
        	<button class="btn btn-primary" type="button" onclick="getCheckImages_ajir('all')">حاضری اجیران</button>
        @endif
        @if(Auth::user()->id == 241)
        <a href="{!!URL::route('putImagesToHr')!!}" onclick="load_loading();" class="btn btn-primary">علاوه نمودن عکس ها</a>
        @endif
        @if(Auth::user()->id == 201)
        <!-- farkhona roles -->
        	<a href="{!!URL::route('putImagesToHr')!!}" onclick="load_loading();" class="btn btn-primary">علاوه نمودن عکس ها</a>
        	<a href="{!!URL::route('notifyManager')!!}" class="btn btn-primary">اطلاعیه ها</a>
        	<a href="{!!URL::route('getAllInShifts')!!}" class="btn btn-primary">بخش محصلین</a>
	        <a href="{!!URL::route('getAllAttEmps')!!}" class="btn btn-primary">کارمندان شامل حاضری الکترونیکی</a>
        @endif
        @if(canAttHoliday())
       		<a href="{!!URL::route('leavesManager')!!}" class="btn btn-primary">مدیریت رخصتی ها</a>

       	@endif
       	@if(canAddHoliday())
       		<a href="{!!URL::route('holidaysManager')!!}" class="btn btn-primary">رخصتی های عمومی</a>
       	@endif
       	@if(canAttTodayAtt())
       		<a href="javascript:void()" class="btn btn-primary" data-target="#today_att" data-toggle="modal">علاوه نمودن حاضری امروز</a>
	        <a href="javascript:void()" onclick="dailyAttendanceReport()" class="btn btn-primary"> راپور حاضری روزانه</a>
       	@endif
        @if(canAdd('hr_attendance'))
        	<button class="btn btn-primary pull-right" type="button" onclick="getSearchResult('all')"> راپور حاضری</button>
	        <button class="btn btn-primary" type="button" onclick="getCheckImages('all')">بررسی عکسها</button>

    		<a href="javascript:void()" onclick="dailyAttendance()" class="btn btn-primary">اکسل روزانه</a>
	        @if(Auth::user()->id == 113 || Auth::user()->id == 174)
	        <!-- fahim roles
	        <a href="javascript:void()" class="btn btn-primary" data-target="#change_employee" data-toggle="modal">تایم حاضری</a>
	        -->
	        <a href="{!!URL::route('notifyManager')!!}" class="btn btn-primary">اطلاعیه ها</a>
	        <a href="{!!URL::route('putImagesToHr')!!}" onclick="load_loading();" class="btn btn-primary">علاوه نمودن عکس ها</a>
	        @endif
	        <a href="{!!URL::route('getAllInShifts')!!}" class="btn btn-primary">بخش محصلین</a>
	        <a href="{!!URL::route('getAllAttEmps')!!}" class="btn btn-primary">کارمندان شامل حاضری الکترونیکی</a>
	    @elseif(canCheckImages())
       		<button class="btn btn-primary pull-right" type="button" onclick="getSearchResult('all')"> راپور حاضری</button>
       		<button class="btn btn-primary" type="button" onclick="getCheckImages('all')">بررسی عکسها</button>
		   @endif
		   
		  
			<a href="{!!URL::route('nullRFID')!!}" class="btn btn-primary">NULL RFID</a>
			
	
    </div>
</div>

<header class="main-box-header clearfix">
    <form action="">
    <div class="row">
        <div class="col-lg-12">
            <div class="container-fluid">
                <div class="row">
                        <div class="col-sm-4">
                            <div class="col-sm-12">
                                <br>
                                <label class="col-sm-12 ">ادارۀ عمومی</label>
                                    <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                            <option value="">انتخاب</option>
                                        @foreach($parentDeps AS $dep_item)
                                            <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                                <div class="col-sm-12">
                                    <br>
                                    <label class="col-sm-12">ادارۀ مربوط</label>
                                </div>
                                <div class="col-sm-12">
                                    <select class="form-control" name="sub_dep" id="sub_dep">
                                        <option value='0'>همه</option>
                                    </select>
                             </div>
                        </div>

                        <div class="col-sm-3">
                             <br>
                            <label class="col-sm-12 "></label>
                            <br>
                            <input type="text" class="form-control" placeholder=" RFID ,جستجوی نام, تخلص, نام پدر">
                        </div>   
                    <br><br><button class="btn btn-primary" type="submit">جستجو</button>
                </div>
            </div>
        </div>
    </div>
</form>
</header>

<div class="row">
        <div class="col-lg-12">
            <div class="modal-body">
                  <div class="example-wrap">
                  
                    <div class="example table-responsive">
                      <table class="table" id="list">
                        <thead>
                              <tr>
                                <th>#</th>
                                <th>نام کامل</th>
                                <th>ولد</th>
                                <th>ریاست</th>
                                <th>وظیفه</th>
                                <th>RFID</th>
                                <th>Actions</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!-- End Example Basic -->
                </div>
        </div>
    </div>
@stop
@section('footer-scripts')
{!! HTML::script('/vendor/clockpicker/bootstrap-clockpicker.min.js') !!}
{!! HTML::script('/js/components/clockpicker.min.js') !!}
<script type="text/javascript">
    $("#sub_dep").select2();

	function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }


    $(document).ready(function() {
        $('#list').dataTable(
            {
                "fnDrawCallback" : function() {
                    $(document).trigger('doc:updated');
                },
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getEmployeeCardData')!!}",
                "aaSorting": [[ 1, "desc" ]],
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });
 

   
</script>