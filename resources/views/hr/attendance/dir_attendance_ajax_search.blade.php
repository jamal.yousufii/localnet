<div class="row">
    <div class="col-lg-12">
        
        </form>
        <div class="modal-body" id="result_div">
            <div class="example-wrap">
                <div class="example table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>نام کامل</th>
                                <th>ولد</th>
                                <th>وظیفه</th>
                                <th>دیپارتمنت</th>
                                
                                <th>عملیه</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows AS $row)
                            <?php 
                            if($row->mawqif_employee==8){$status = 'تبدیلی';}
                            elseif($row->mawqif_employee==6){$status = 'منفک';}
                            elseif($row->mawqif_employee==9){$status = 'استعفا';}
                            elseif($row->mawqif_employee==5){$status = 'تقاعد';}
                            elseif($row->mawqif_employee==2){$status = 'انتظاربه معاش';}
                            else{$status='بدون کارت';}
                            ?>
                                <tr>
                                    <td>{!!$row->id!!}</td>
                                    <td>{!!$row->name!!} {!!$row->last_name!!}</td>
                                    <td>{!!$row->father_name!!}</td>
                                    <td>{!!$row->position!!}</td>
                                    <td>{!!$row->department!!}</td>
                                    
                                    <td>
                                    @if($row->RFID>0)
                                        @if($row->tashkil_id>0)
                                        <a href="{!!route('getDirEmployeeImages',array(Crypt::encrypt($row->RFID),$year,$month,Crypt::encrypt($row->general_department),Crypt::encrypt($row->dep_id)))!!}" class="table-link" style="text-decoration:none;">
                                            بررسی عکسها
                                        </a>
                                        @else
                                        <a href="javascript:void()" class="table-link" style="text-decoration:none;color:red">
                                            اضافه بست
                                        </a>
                                        @endif
                                    @elseif($row->in_attendance!=0)
                                        <a href="{!!route('getDirEmployeeImages',array($row->RFID,$year,$month,$row->general_department,$row->dep_id))!!}" class="table-link" style="text-decoration:none;">
                                            بررسی عکسها
                                        </a> | 
                                        <a href="javascript:void()" class="table-link" style="text-decoration:none;color:red">
                                            {!!$status!!}
                                        </a>
                                    @else
                                        <a href="javascript:void()" class="table-link" style="text-decoration:none;">
                                            شامل حاضری نمیباشد
                                        </a>
                                    @endif
                                    </td>
                                <tr>
                                
                            @endforeach
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
