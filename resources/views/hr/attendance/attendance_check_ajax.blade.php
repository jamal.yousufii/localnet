<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix" style="margin-top:10px">
		    <h2>
            @if(!canCheckImages())
		    	<a href="{!!URL::route('getAttCharts',array($dep_id,$year,$month))!!}" class="btn btn-primary pull-right">
					گراف
				</a><div style="display:inline" class="pull-right"> &nbsp; </div>
				@if(getUserDepType(Auth::user()->id)->dep_type == 1)
					@if($check==2)
					<a href="{!!URL::route('printAttReport_ajir',array($dep_id,$year,$month,$date_type))!!}" class="btn btn-primary pull-right">
						راپور حاضری اجیران
					</a><div style="display:inline" class="pull-right"> &nbsp; </div>
					@else
					<a href="{!!URL::route('printAttReport',array($dep_id,$year,$month,$date_type))!!}" class="btn btn-primary pull-right">
						راپور حاضری
					</a><div style="display:inline" class="pull-right"> &nbsp; </div>
					<a href="{!!URL::route('printDailyAttReport',array($dep_id,$year,$month))!!}" class="btn btn-primary pull-right">
						راپور حاضری روزانه
					</a><div style="display:inline" class="pull-right"> &nbsp; </div>
					@endif
				@else
					<a href="{!!URL::route('printAttReport_aop',array($dep_id,$year,$month))!!}" class="btn btn-primary pull-right">
						راپور حاضری
					</a><div style="display:inline" class="pull-right"> &nbsp; </div>
				@endif

				@if($check==2)
		        <a href="javascript:void();" onclick="do_print_ajir();" class="btn btn-primary pull-right">
					راپور غیرحاضری اجیران
				</a><div style="display:inline" class="pull-right"> &nbsp; </div>
				<a href="{!!URL::route('printExtraReport_ajir',array($dep_id,$year,$month,$date_type))!!}" class="btn btn-primary pull-right">
						راپور اضافه کاری اجیران
				</a><div style="display:inline" class="pull-right"> &nbsp; </div>
				@else
				<a href="javascript:void();" onclick="do_print();" class="btn btn-primary pull-right">
					پرنت اکسل غیرحاضری
				</a><div style="display:inline" class="pull-right"> &nbsp; </div>
				<a href="{!!URL::route('getPayrollList',array($dep_id,$year,$month))!!}" class="btn btn-primary pull-right">
					راپور معاشات
				</a>
				<div style="display:inline" class="pull-right"> &nbsp; </div>
				@endif
		    @endif
		    </h2>
		    <input type="text" id="emp_search" class="col-sm-6" placeholder="جستجوی نام, تخلص, نام پدر">
		</header>
		</form>
		<div class="modal-body" id="result_div">
			<div class="example-wrap">
		        <div class="example table-responsive">
		            <table class="table">
		                <thead>
		                    <tr>
			                    <th>#</th>
			                    <th>نام کامل</th>
			                    <th>ولد</th>
			                    <th>وظیفه</th>
			                    <th>دیپارتمنت</th>

			                    <th>عملیه</th>
			                </tr>
		                </thead>
		                <tbody>
		                    @foreach($rows AS $row)
		                    <?php
		                    if($row->mawqif_employee==8){$status = 'تبدیلی';}
		                    elseif($row->mawqif_employee==6){$status = 'منفک';}
		                    elseif($row->mawqif_employee==9){$status = 'استعفا';}
		                    elseif($row->mawqif_employee==5){$status = 'تقاعد';}
		                    elseif($row->mawqif_employee==2){$status = 'انتظاربه معاش';}
		                    else{$status='بدون کارت';}
		                    ?>
		                    	<tr>
		                    		<td>{!!$row->id!!}</td>
		                    		<td>{!!$row->name!!} {!!$row->last_name!!}</td>
		                    		<td>{!!$row->father_name!!}</td>
		                    		<td>{!!$row->position!!}</td>
		                    		<td>{!!$row->department!!}</td>
		                    		<td>
		                    		   @if($row->RFID>0)
		                    			 @if(canCheckImages())
		                    				@if($row->tashkil_id>0)
							                   <a href="{!!route('getEmployeeAttendanceCheck',array($row->RFID,$year,$month,$row->general_department,$row->dep_id))!!}" target="_blank" class="table-link" style="text-decoration:none;">
                                                    بررسی عکسها
												</a>
											@else
                                                <a href="javascript:void()" class="table-link" style="text-decoration:none;color:red">
                                                    اضافه بست
                                                </a>
                                            @endif
		                    		     @else
                                            <a href="{!!route('getEmployeeAttendance',array($row->RFID,$year,$month,$row->general_department,$row->dep_id))!!}" class="table-link" target="_blank" style="text-decoration:none;">
                                                بررسی عکسها
                                            </a>
		                    		    @endif
                                      @elseif($row->in_attendance!=0)
                                            @if(canCheckImages())
                                                <a href="{!!route('getEmployeeAttendanceCheck',array(0,$year,$month,$row->general_department,$row->dep_id,$row->in_attendance))!!}" target="_blank" class="table-link" style="text-decoration:none;">
                                                    بررسی عکسها
                                                </a> |
                                            @else
                                                <a href="{!!route('getEmployeeAttendance',array(0,$year,$month,$row->general_department,$row->dep_id,$row->in_attendance))!!}" target="_blank" target="_blank" class="table-link" style="text-decoration:none;">
                                                    بررسی عکسها
                                                </a> |
                                            @endif
                                                <a href="javascript:void()" class="table-link" style="text-decoration:none;color:red">
                                                    {!!$status!!}
                                                </a>
                                            @else
                                            <a href="javascript:void()" class="table-link" style="text-decoration:none;">
                                                شامل حاضری نمیباشد
                                            </a>
                                    @endif
                                    @if(canAttHoliday())
                                        <a href="{!!route('getEmployeeLeaves',array($row->id,$year,0))!!}" class="table-link" target="_blank" style="text-decoration:none;">
                                                | مدیریت رخصتی
                                        </a>
                                    @endif
                                  </td>
                                <tr>

		                    @endforeach
		                </tbody>
		            </table>
		            <div class="dataTables_paginate paging_simple_numbers" id="list_paginate">
						{!!$rows->render()!!}
					</div>
		        </div>
		    </div>
		</div>
	</div>
</div>

<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>
<div class="modal fade modal-fade-in-scale-up" id="salary_modal" aria-hidden="true" aria-labelledby="salary_modal" role="dialog" tabindex="-1"></div>

<div class="md-overlay"></div><!-- the overlay element -->

<script>
	$( document ).ready(function() {
		$('.pagination a').on('click', function(event) {
			event.preventDefault();
			if ($(this).attr('href') != '#') {
				//$('#ajaxContent').load($(this).attr('href'));

				$.ajax({
		                url: '{!!URL::route("getAttendanceAjax")!!}',
		                data: $('#att_report').serialize()+"&page="+$(this).text()+"&ajax=1&check=1",
		                type: 'post',
		                beforeSend: function(){

		                    //$("body").show().css({"opacity": "0.5"});
		                    $('#all').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
		                },
		                success: function(response)
		                {

		                    $('#all').html(response);
		                }
		            }
		        );

			}
		});
	});

    $('#emp_search').keypress(function(event){

		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){

	    	var item = $('#emp_search').val();
	    	$.ajax({
	                url: '{!!URL::route("getSearchResult")!!}',
	                data: $('#att_report').serialize()+'&item='+item+'&check=1',
	                type: 'post',
	                beforeSend: function(){

	                    //$("body").show().css({"opacity": "0.5"});
	                    $("#result_div").html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {

	                    $('#result_div').html(response);
	                }
	            }
	        );
		}
		event.stopPropagation();
	});
function do_print()
{
	if($('#sub_dep').val()==0)
	{
		alert('اداره مربوطه را انتخاب نمایید');
		return;
	}

  window.location = "{!!URL::route('printAbsentReport',array($dep_id,$year,$month))!!}";

}
function do_print_ajir()
{
	if($('#sub_dep').val()==0)
	{
		alert('اداره مربوطه را انتخاب نمایید');
		return;
	}

    window.location = "{!!URL::route('printAbsentReport_ajir',array($dep_id,$year,$month))!!}";

}
</script>
