@extends('layouts.master')

@section('head')
    <title>leave form</title>
@stop
@section('content')

@if(getUserDepType(Auth::user()->id)->dep_type==1)  
<div class="row col-md-12" style="margin-top:-30px">
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix">
            <center>
                <h4>
                د افغانستان اسلامی جمهوریت</br>
                دجمهوری ریاست دلوړ مقام د دفتر لوی ریاست</br>
                د منابع او اداره معاونیت</br>
                د بشری منابع ریاست
                </h4>
            </center>   
        </div>
    </div>
    <div class="col-md-2 noprint">
        {!! HTML::image('/img/logo.jpg', 'LOGO', array('width' => '130','height' => '125')) !!}
        
    </div>
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix">
           
                <center>
                <h4 style="font-family: 'B Nazanin';">
                جمهوری اسلامی افغانستان</br>
                ریاست عمومی دفتر مقام عالی ریاست جمهوری</br>
                معاونیت منابع و اداره</br>
                ریاست منابع بشری
                </h4></center>
            
            
        </div>
    </div>
</div>
@else
<div class="row col-md-12" style="margin-top:-30px">
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix">
            <center>
                <h4>
                د افغانستان اسلامی جمهوریت</br>
                دجمهوری ریاست دلوړ مقام د دفتر لوی ریاست</br>
                د بشری منابع ریاست
                </h4>
            </center>   
        </div>
    </div>
    <div class="col-md-2 noprint">
        {!! HTML::image('/img/logo.jpg', 'LOGO', array('width' => '130','height' => '125')) !!}
        
    </div>
    <div class="col-md-5 noprint">
        <div id="content-header" class="clearfix">
           
                <center>
                <h4 style="font-family: 'B Nazanin';">
                جمهوری اسلامی افغانستان</br>
                ریاست عمومی ادراه امور ریاست جمهوری</br>
                ریاست منابع بشری
                </h4></center>
            
            
        </div>
    </div>
</div>
@endif

<header class="main-box-header clearfix">
<div class="row" id="main_content">
    <div class="col-lg-12">
        @if(canCheckAtt() || canAttHoliday())
        <header class="main-box-header clearfix">
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <a href="javascript:void()" class="btn btn-primary pull-right" onclick="$('#main_content').slideUp();$('#all').slideDown();">
                        <i class="fa fa-plus-circle fa-lg"></i>
                    </a>
                </div>
            </div>
        </header>
        @endif
        @if (count($errors) > 0)
        <div class="alert alert-danger" style="margin: 10px 0 20px 0">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <div class="modal-body">
            <div class="example-wrap">
                <div class="example table-responsive">
                  <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>نوعیت رخصتی</th>
                            <th>از تاریخ</th>
                            <th>تا تاریخ</th>
                            <th>تعداد روز </th>
                            @if(Auth::user()->position_id==2 || canAttHoliday())
                            <th>اسم</th>
                            @else
                            <th>وضعیت</th>
                            @endif
                            <th>تاریخ ثبت</th>
                            <th>عملیه</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $types = array('0'=>'',
                                         '1'=>'ضروری',
                                         '2'=>'تفریحی',
                                         '3'=>'مریضی',
                                         '4'=>'ولادی',
                                         '5'=>'عروسی',
                                         '6'=>'حج',
                                         '7'=>'دیگر(خدمتی)',
                                         '8'=>'اضافه رخصتی مریضی',
                                         '9'=>'دیگر موارد',
                                         '10'=>'شامل کتاب حاضری'
                    );
                    
                    ?>
                    @if($details)
                    <?php $i = 1; ?>
                        @foreach($details AS $row)
                        <?php $sdate = $row->date_from;$edate = $row->date_to;
                        if($sdate !=''){$sdate = explode('-',$sdate);$sdate=dateToShamsi($sdate[0],$sdate[1],$sdate[2]);$sdate=jalali_format($sdate);}
                        if($edate !=''){$edate = explode('-',$edate);$edate=dateToShamsi($edate[0],$edate[1],$edate[2]);$edate=jalali_format($edate);}
                        ?>
                        <tr>
                            <td>{!!$i!!}</td>
                            <td>{!!$types[$row->type]!!}</td>
                            <td>{!!$sdate!!}</td>
                            <td>{!!$edate!!}</td>
                            <td>{!!$row->days_no!!}</td>
                              
                            <td>
                            @if(canCheckAtt())
                                @if($row->dir_approved==0)
                                    <button class="btn btn-primary">تایید نشده توسط ریاست</button>
                                @elseif($row->dir_approved==2)
                                    <button class="btn btn-danger">رد شده توسط ریاست</button>
                                @else
                                    @if($row->hr_approved==0)
                                        <button class="btn btn-warning">تایید نشده توسط حاضری</button>
                                    @else
                                        <button class="btn btn-success">تایید شده {{convertDateAndTime($row->hr_approved_at,'to_shamsi')}}</button>
                                    @endif
                                @endif
                                
                            @else
                                {!!$row->name_dr!!} {!!$row->last_name!!} - {{$row->dep_name}}
                            @endif
                            </td>
                            <td>{{convertDateAndTime($row->created_at,'to_shamsi')}}</td>
                            <td id="btndiv">
                            @if(canAttHoliday() || Auth::user()->position_id==2)
                                @if(Auth::user()->position_id==2)
                                    <button class="btn btn-danger" onclick="reject_leave({!!$row->id!!},2)">رد</button> |
                                    <button class="btn btn-success" onclick="reject_leave({!!$row->id!!},1)">تایید</button>
                                @else
                                    <button class="btn btn-success" onclick="approve_leave({!!$row->id!!},3)">تایید</button>
                                @endif
                                
                            @else
                                @if($row->dir_approved==0)
                                
                                <a href="javascript:void()" onclick="delete_leave({!!$row->id!!})">
                                    <i class="icon fa-close" aria-hidden="true" style="font-size: 16px;"></i>
                                </a>
                                @elseif($row->dir_approved==2)

                                @elseif($row->dir_approved == 1 OR $row->hr_approved==0)
                                    <button class="btn btn-primary">تحت پروسس</button>
                                @endif
                            @endif
                            </td>
                        
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
    </div>
</div>
</header>
<div class="row" id="all" style="display:none;">
    <div class="col-lg-12">
        <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postLeaves_byUser')!!}" enctype="multipart/form-data">
            <div class="panel-heading">
              <center><h5 class="panel-title">فورم رخصتی کارکنان</h5></center>
            </div>
        
            <div class="container-fluid">
                <div class="row">
                    
                    <div class="col-sm-3">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">نوع رخصتی</label>
                            <select class="form-control" name="type" id="type" required onchange="check_leave_validity()">
                                <option value=''>انتخاب</option>
                                <option value='1' @if(old('type')==1) selected @endif>ضروری</option>
                                <option value='2' @if(old('type')==2) selected @endif>تفریحی</option>
                                <option value='3' @if(old('type')==3) selected @endif>مریضی</option>
                                @if(getEmployeeSex(Auth::user()->id)=='F')
                                <option value='4' @if(old('type')==4) selected @endif>ولادی</option>
                                @endif
                                <option value='6' @if(old('type')==6) selected @endif>حج</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">از تاریخ</label>
                            <input class="form-control datepicker_farsi" value="{!!old('from_date')!!}" readonly type="text" name="from_date" id="from_date" required>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">تا تاریخ</label>
                            <input class="form-control datepicker_farsi" value="{!!old('to_date')!!}" readonly type="text" name="to_date" id="to_date" required onchange="check_leave_validity_date()">
                        </div>
                    </div>
                </div>
            </div>
            </br>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6" id="btn_not_valid">
                    
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <label class="col-sm-2 ">&nbsp;</label>
                            <div id="btn_div" style="display: inline;">
                                <button class="btn btn-primary" type="submit">ثبت معلومات</button>
                            </div>
                            <button onclick="$('#all').slideUp();$('#main_content').slideDown();" class="btn btn-danger" type="button">کنسل</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::token() !!}
        </form>
    </div>
</div>
    
    
@stop

@section('footer-scripts')

<script type="text/javascript">
function check_leave_validity()
{
    var type = $('#type').val();
    if(type=='')
    {
        $('#btn_div').hide();
        return;
    }
    $.ajax({
            url: '{!!URL::route("check_leave_validity_byUser")!!}',
            data: '&type='+type,
            type: 'post',
            
            success: function(r)
            {
                if(r=='true')
                {
                    $('#btn_div').hide();
                    $('#btn_not_valid').html('<p style="color:red">رخصتی های شما تکمیل گردیده است</p>');
                }
                else if(r=='tafrihi')
                {
                    $('#btn_div').hide();
                    $('#btn_not_valid').html('<p style="color:red">شما یازده ماه تقرری را تکمیل ننموده اید, مستحق رخصتی تفریحی نمیباشید</p>');
                }
                else
                {
                    $('#btn_div').show();
                    $('#btn_not_valid').html('');
                }
            }
        }
    );
}
function check_leave_validity_date()
{
    var type = $('#type').val();
    var id = 0;
    var date_from = $('#from_date').val();
    var date_to = $('#to_date').val();
    if(type=='')
    {
        $('#to_date').val('');
        $('#btn_div').hide();
        alert('لطفا از بخش نوع رخصتی یک مورد را انتخاب نمایید');
        return;
    }
    else
    {
        $('#btn_div').show();
    }
    if(date_from=='' || date_to=='')
    {
        $('#to_date').val('');
        $('#btn_div').hide();
        alert('لطفا تاریخ شروع را انتخاب نمایید');
        return;
    }
    if(checkBothDate(date_from,date_to))
    {
        $('#btn_div').hide();
        alert('تاریخهای شروع و ختم باید در عین ماه باشند.');
        $('#btn_not_valid').html('<p style="color:red">در صورتی که تاریخ ختم رخصتی در ماه بعد قرار دارد. تاریخ ختم را آخرین روز همین ماه انتخاب نموده. سپس برای متباقی روزها در ماه بعد فورم جدید ثبت نمایید</p>');
        return;
    }
    else
    {
        $('#btn_div').show();
        $('#btn_not_valid').html('');
    }
    if(date_from > date_to)
    {
        $('#to_date').val('');
        $('#btn_div').hide();
        alert('تاریخ شروع نباید بزرگتر از تاریخ ختم باشد');
        return;
    }
    else
    {
        $('#btn_div').show();
    }
    
    $.ajax({
            url: '{!!URL::route("check_leave_validity_date")!!}',
            data: '&type='+type+'&id='+id+'&date_from='+date_from+'&date_to='+date_to,
            type: 'post',
            dataType: 'json',
            success: function(r)
            {
                if(r.result=='true')
                {
                    $('#btn_div').hide();
                    $('#btn_not_valid').html('<p style="color:red">رخصتی درخواست شده بیشتر از حد تعیین شده میباشد, '+r.leaves+' روز از این نوع رخصتی استفاده شده است.</p>');
                }
                else if(r.result=='offday')
                {
                    $('#btn_div').hide();
                    $('#btn_not_valid').html('<p style="color:red">روز انتخاب شده رخصتی میباشد.</p>');
                }
                else if(r.result=='extraLeave')
                {
                    $('#btn_div').hide();
                    $('#btn_not_valid').html('<p style="color:red">بدلیل اینکه قبل از تاریخ انتخاب شده رخصتی گرفته اید,اجازه رخصتی عادی ندارید!</p>');
                }
                else if(r.result=='absent')
                {
                    $('#btn_div').show();
                    $('#btn_not_valid').html('<p style="color:red">بدلیل موجودیت روزهای رخصتی در بین غیرحاضری شما, اگر روزهای رخصت را هم فورم نیندازید, روزهای رخصت هم غیرحاضر محسوب خواهید شد.</p>');
                }
                else
                {
                    $('#btn_div').show();
                    $('#btn_not_valid').html('');
                }
            }
        }
    );
}
function checkBothDate(date_from,date_to)
{
    var smonth = date_from.split("-"); 
    var emonth = date_to.split("-"); 
    if(smonth[1]==emonth[1])
    {
        return false;
    }
    else
    {
        return true;
    }
}
function reject_leave(id,type)
{
    var confirmed = confirm("آیا مطمئن هستید؟");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("process_leave_dir")!!}',
                data: '&id='+id+'&type='+type,
                type: 'post',
                
                success: function(r)
                {
                    $('#btndiv').html(r);
                }
            }
        );
    }
}
function approve_leave(id,type)
{
    var confirmed = confirm("لطفا در صورت موجود بودن اصل فورم تایید نمایید");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("process_leave_dir")!!}',
                data: '&id='+id+'&type='+type,
                type: 'post',
                
                success: function(r)
                {
                    $('#btndiv').html(r);
                }
            }
        );
    }
}
function delete_leave(id)
{
    var confirmed = confirm("مطمین هستید؟");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("delete_leave_by_user")!!}',
                data: '&id='+id,
                type: 'post',
                
                success: function(r)
                {
                    $('#btndiv').html(r);
                }
            }
        );
    }
}

</script>

@stop