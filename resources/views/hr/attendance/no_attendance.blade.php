@extends('layouts.master')

@section('head')
    <title>{!!_('my_attendance')!!}</title>
<style type="text/css">
.leave_span{
	position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 20px;
}
</style>  
@stop
@section('content')

<header class="main-box-header clearfix" style="margin-top: -30px;">
	<div class="row">
    	<div class="col-lg-12">
    		<div class="container-fluid">
        		<div class="col-sm-4">
        			<div class="col-sm-12" id="img_controller">
        				<div class="icon-box pull-left">
							<img src="/documents/profile_pictures/{!!$photo->photo!!}" alt="Image Not Found!" width="150" hight="224"/>
						</div>
        			</div>
        		</div>
			</div>
			<div class="container-fluid">
		      	<div class="row">
            		<div class="col-sm-3">
            			{!!$photo->current_position_dr!!}
            		</div>
            		
            		<div class="col-sm-9" style="margin-bottom: 15px;">
            		<h4 style="color: red !important;font-family: 'B Nazanin';">
            		اکونت شما غیر فعال گردیده است.
            		</h4>
            		</div>
            	</div>
            </div>
		</div>
	</div>
</header>

@stop
@section('footer-scripts')

@stop


