@extends('layouts.master')

@section('head')
	
	</style>
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	<form class="form-horizontal" role="form" method="post" action="{!!URL::route('getRelatedEmployees')!!}">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>حاضری کارمندان</span></li>
        </ol>
    </div>
</div>
<header class="main-box-header clearfix">
   
    	<div class="row">
        	<div class="col-lg-12">
        	
        		<div class="container-fluid">
			      	<div class="row">
					    <div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی</label>
                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                    <option value="">انتخاب</option>
                                    @foreach($parentDeps AS $dep_item)
                                    	@if($dep_item->id == $dep)
                                    		<option value='{!!$dep_item->id!!}' selected>{!!$dep_item->name!!}</option>
                                    	@else
                                        	<option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ مربوط</label>
                                <select class="form-control" name="sub_dep" id="sub_dep">
                                    <option value=''>انتخاب</option>
                                    @foreach($sub_deps AS $deps)
                                    	@if($deps->id == $sub_dep)
                                    		<option value='{!!$deps->id!!}' selected>{!!$deps->name!!}</option>
                                    	@else
                                        	<option value='{!!$deps->id!!}'>{!!$deps->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                			</div>
						</div>
						<div class="col-sm-2">
                			<div class="col-sm-12">
								<label for="from_date">از تاریخ:</label>
								<input type="text" name="from_date" id="from_date" value="{{$from_date}}" class="form-control datepicker_farsi" placeholder="شروع تاریخ">
							</div>
						</div>
						<div class="col-sm-2">
                			<div class="col-sm-12">
								<label for="to_date">الی تاریخ:</label>
								<input type="text" name="to_date" id="to_date" value="{{$to_date}}" class="form-control datepicker_farsi" placeholder="ختم تاریخ">
							</div>
						</div>
					</div>
					<div class="row" style="margin-top: 15px;">
						<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">وضعیت</label>
                                <select class="form-control" name="status" id="status">
                                    <option value='0'>انتخاب</option>
                                    <option value='1' @if($status==1) {!!'selected'!!} @endif>حاضر</option>
									<option value='2' @if($status==2) {!!'selected'!!} @endif>غیرحاضر</option>
									<option value="show_img">تصاویر غیر حاضران</option>
                                </select>
                			</div>
                		</div>
                		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">کارکن</label>
                                <select class="form-control" name="type" id="type">
                                    <option value='0'>انتخاب</option>
                                    <option value='1' @if($type==1) {!!'selected'!!} @endif>مامور</option>
                                    <option value='2' @if($type==2) {!!'selected'!!} @endif>اجیر</option>
                                </select>
                			</div>
                		</div>
					</div>
					<div class="row">
						<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
								<a href="javascript:void()" onclick="dailyAttendanceReport()" class="btn btn-primary"> راپور حاضری روزانه</a>
							</div>
						</div>
						<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
								<a href="javascript:void()" onclick="dailyAttendancePrint()" class="btn btn-primary">پرنت اکسل</a>
							</div>
						</div>
						<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
								<a href="javascript:void()" onclick="dailyAttChart()" class="btn btn-primary">گراف</a>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
				        	<label class="col-sm-12 ">Present: {!!getDailyAtt($dep,$sub_dep,$type)!!}</label>
				        	</div>
				        </div>
				        <div class="col-sm-2">
				        	<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
				        		<label class="col-sm-12 ">Absent: {!!getDailyAbsent($dep,$sub_dep,$type)!!}</label>
				        	</div>
				        </div>
					</div>
				</div>
				
			</form>
			</div>
		</div>
    
</header>

<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix" style="margin-top:10px">
		    
		</header>
		<div class="modal-body">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table" id="list">
                    <thead>
                      	<tr>
	                        <th>#</th>
	                        <th>نام کامل</th>
	                        <th>ولد</th>
	                        <th>وظیفه</th>
	                        <th>دیپارتمنت</th>
	                        <th>حاضری</th>
	                        <th>زمان حاضری</th>
	                        <th with="20">تاریخ</th>
                            <th>ماشین حاضری</th>
	                       
	                    </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>

@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::route('getTodayAttData',array($dep,$sub_dep,$status,$type,$from_date,$to_date))!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });
	function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function dailyAttendanceReport()
	{
		var general_department = $('#general_department').val();
		if(general_department=='')
		{
			general_department = 0;
		}
		var sub_dep = $('#sub_dep').val();
		if(sub_dep=='')
		{
			sub_dep = 0;
		}
		var status  = $('#status').val();
		var type  = $('#type').val();
		var from_date = $('#from_date').val(); 
		var to_date   = $('#to_date').val(); 

	    window.location = "/hr/getTodayAttendances/"+general_department+"/"+sub_dep+"/"+status+"/"+type+"/"+from_date+"/"+to_date;
	    
	}
	function dailyAttendancePrint()
	{
		var status = $('#status').val();
		var general_department = $('#general_department').val();
		if(general_department=='')
		{
			general_department = 0;
		}
		var sub_dep = $('#sub_dep').val();
		if(sub_dep=='')
		{
			sub_dep = 0;
		}
		var type = $('#type').val();
	    window.location = "/hr/printDailyAtt/"+general_department+"/"+sub_dep+"/"+status+"/"+type;
	    
	}
	function dailyAttChart()
	{
		var general_department = $('#general_department').val();
		if(general_department=='')
		{
			alert('لطفا یک اداره عمومی را انتخاب نمایید');return;
		}
		var sub_dep = $('#sub_dep').val();
		if(sub_dep=='')
		{
			alert('لطفا اداره مربوطه را انتخاب نمایید');return;
		}
		
	    window.location = "/hr/printDailyAttChart/"+sub_dep;
	    
	}
</script>
@stop

