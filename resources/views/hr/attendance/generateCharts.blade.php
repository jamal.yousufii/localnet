@extends('layouts.master')

@section('head')
	{!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
	
    <title>{!!_('attendance_chart')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
	
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>حاضری کارمندان</span></li>
        </ol>
        
    </div>
</div>
<header class="main-box-header clearfix">
    	<div class="row">
        	<div class="col-lg-12">
        	<form class="form-horizontal" role="form" method="post" action="{!!URL::route('generate_att_chart')!!}">
        		<div class="container-fluid">
			      	<div class="row">
			      		<div class="col-sm-2">
		                	<div class="col-sm-12">
		                		<label class="col-sm-12 ">از تاریخ</label>
		                		<input class="form-control datepicker_farsi" readonly value="{!!$s_date!!}" type="text" name="from_date">
		                	</div>
		                </div>
		                <div class="col-sm-2">
		                	<div class="col-sm-12">
		                		<label class="col-sm-12 ">تا تاریخ</label>
		                		<input class="form-control datepicker_farsi" readonly value="{!!$e_date!!}" type="text" name="to_date">
		                	</div>
		                </div>
					    <div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی</label>
                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                    <option value="">همه</option>
                                    <?php $parentDeps=getDepartmentWhereIn();?>
                                    @foreach($parentDeps AS $dep_item)
                                    	@if($dep_item->id==$gen_dep)
                                        <option value='{!!$dep_item->id!!}' selected>{!!$dep_item->name!!}</option>
                                        @else
                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
										@endif	
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12">ادارۀ مربوط</label>
                			</div>
                			<div class="col-sm-12">
                                <select style="width:100%;" name="sub_dep" id="sub_dep">
                                	<option value="">همه</option>
                                <?php $sub_deps = getRelatedSubDepartment($gen_dep);?> 
                                    @foreach($sub_deps AS $sub)
                                    	@if($sub->id==$sub_dep)
                                        <option value='{!!$sub->id!!}' selected>{!!$sub->name!!}</option>
                                        @else
                                        <option value='{!!$sub->id!!}'>{!!$sub->name!!}</option>
										@endif	
                                    @endforeach
                                </select>
                			</div>
                		</div>
                		<div class="col-sm-2">
	                		<div class="col-sm-12">
	                		
		                        <div class="radio-custom radio-primary">
				                  <input type="radio" value="0" name="type" @if($type==0){!! 'checked' !!}@endif />
				                  <label for="inputRadiosUnchecked">{!!_('percentage')!!}</label>
				                </div>
				                <div class="radio-custom radio-primary">
				                  <input type="radio" value="1" name="type" @if($type==1){!! 'checked' !!}@endif />
				                  <label for="inputRadiosChecked">{!!_('daily')!!}</label>
				                </div>
		                    </div>
                    	</div>
					    @if($sub_dep=='')
					    <div class="col-sm-2">
			      			<div class="col-sm-12">
                				<input type="submit" class="btn btn-primary" value="Get Chart"/>
                			</div>
                			
			      		</div>
			      		@endif
					</div>
				</div>
				@if($sub_dep!='')
				<br/>
				<div class="container-fluid" style="float:right">
			      	<div class="row">
			      		
			      		<div class="col-sm-9">
			      			<div class="col-sm-12">
                				<label class="col-sm-12">تعداد کارمندان {!!$direc!!}: @if($total){!!$total->total!!}@else{!!0!!}@endif</label>
                			</div>
                			
			      		</div>
			      		<div class="col-sm-3">
			      			<div class="col-sm-12">
                				<input type="submit" class="btn btn-primary" value="Get Chart"/>
                			</div>
                			
			      		</div>
			      	</div>
			    </div>
			    @endif
			    <div class="container-fluid">
			      	<div class="row">
			      		<div class="col-sm-12">
			      			@if (count($errors) > 0)
						    
						    <div class="alert alert-danger" style="margin: 10px 0 20px 0">
						      <ul>
						        @foreach ($errors->all() as $error)
						          <li>{{ $error }}</li>
						        @endforeach
						      </ul>
						    </div>
						    @endif
			      		</div>
			      	</div>
			    </div>
			</form>
			</div>
		</div>
    
</header>
<div class="col-md-12">
	@if($sub_dep!='')
	<div id="chart" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
	@else
	@for($i=0;$i<count($all_charts);$i++)
	
	<div id="chart{!!$i!!}" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
	@endfor
	@endif
</div>
@stop
@section('footer-scripts')
{!! HTML::script('/js/highcharts/highcharts.js') !!}
{!! HTML::script('/js/highcharts/highcharts-3d.js') !!}
{!! HTML::script('/js/highcharts/exporting.js') !!}
<script type="text/javascript">
    $("#sub_dep").select2();
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
@if($sub_dep!='')
$(function () {
    // Create the chart
    $('#chart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Attendance Chart'
        },
        
        xAxis: {
            categories: [
                <?=$display?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Employees Number'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
            	innerSize: 50,
                depth: 0,
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },
        series: [{
            name: 'Present',
            data: [<?=$present_totals?>]
		
        }, {
            name: 'Absent',
            data: [<?=$absent_totals?>]

        }
        ]
    });
});
@else
@for($i=0;$i<count($all_charts);$i++)
$(function () {
    // Create the chart
    $('#chart{!!$i!!}').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: "<?=$all_charts[$i]['dir']?>(@if($all_charts[$i]['dir_total']){!!$all_charts[$i]['dir_total']->total!!}@else 0 @endif)"
        },
        
        xAxis: {
            categories: [
                <?=$all_charts[$i]['display']?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Employees Number'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
            	innerSize: 50,
                depth: 0,
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },
        series: [{
            name: 'Present',
            data: [<?=$all_charts[$i]['present_totals']?>]
		
        }, {
            name: 'Absent',
            data: [<?=$all_charts[$i]['absent_totals']?>]

        }
        ]
    });
});
@endfor
@endif
</script>
@stop

