@extends('layouts.master')

@section('head')
    <title>{!!_('attendance')!!}</title>

@stop
@section('content')

<header class="main-box-header clearfix">
	<div class="row">
    	<div class="col-lg-12">
    		<div class="container-fluid">
        		<div class="col-sm-4">
        			<div class="col-sm-12" id="img_controller">
        				<div class="icon-box pull-left">
			           		@if($previous_rfid)
			           		<?php $link = URL::route('verifyNextEmployee_check',array($rfid,$year,$month,'previous',$dep,$sub_dep)); ?>
							<a href="javascript:void()" onclick="$('#img_controller').html('Loading...');location.href='{!!$link!!}'" class="btn pull-left">
								<i class="icon ml-chevron_left"></i>
							</a>
							@else
							<i class="btn pull-left icon ml-chevron_left" style="cursor:default"></i>
							@endif
							<?php
                                //Check if Image exist  
                                $imgwidth  = 80;
                                if(file_exists(public_path() . '/documents/profile_pictures/'.$photo->photo) && $photo->photo!='')
                                {
                                    $data = getimagesize('documents/profile_pictures/'.$photo->photo);
                                    $image = base64_encode(file_get_contents('documents/profile_pictures/'.$photo->photo));
                                    $width = $data[0];
                                    $height = $data[1];
                                    if($width>2200)
                                        $imgwidth  = 150;
                                }
                                else {
                                    $image = base64_encode(file_get_contents('documents/profile_pictures/default.jpeg'));
                                }
                                $profile_pic = 'data:image/jpg;base64,'.$image;							
							
								?>
							<img src="{!!$profile_pic!!}" alt="Image Not Found!" width="150" hight="224"/>
							@if($next_rfid)
							<?php $next_link = URL::route('verifyNextEmployee_check',array($rfid,$year,$month,'next',$dep,$sub_dep)); ?>
							<a href="javascript:void()" onclick="$('#img_controller').html('Loading...');location.href='{!!$next_link!!}'" class="btn pull-right">
								<i class="icon ml-chevron_right"></i>
							</a>
							@else
							<i class="btn pull-right icon ml-chevron_right" style="cursor:default"></i>
							@endif
						</div>
        			</div>
        		</div>
	      		<div class="col-sm-2">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">سال</label>
                        <select name="year" id="year" class="form-control" onchange="bring_employee_images()">
                            <?php
        					//$month = explode('-',$today);
        					$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');

        					?>
                            @for($i=$year-5;$i<$year+5;$i++)
                            	@if($i==$year)
                                <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
                                @else
                                <option value='{!!$i!!}'>{!!$i!!}</option>
                                @endif
                            @endfor
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
        			</div>
        		</div>
        		<div class="col-sm-2">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">ماه</label>

                        <select name="month" id="month" class="form-control" onchange="bring_employee_images()">

                            @for($j=1;$j<13;$j++)
                            	<?php
                            	$m = $j;
                            	if($j<10) $m='0'.$j;
                            	?>
                            	@if($m==$month)
                                <option value='{!!$m!!}' selected="selected">{!!$month_name[$j]!!}</option>
                                @else
                                <option value='{!!$m!!}'>{!!$month_name[$j]!!}</option>
                                @endif
                            @endfor
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
        			</div>
        		</div>
			    <div class="col-sm-3">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">کارمندان</label>
                        <select id="emp_rfid" class="form-control" onchange="bring_employee_images()">
                                <option value="">انتخاب</option>
                            @foreach($employees AS $emp)
                            	@if($emp->RFID!=0)
                                	@if($emp->RFID == $rfid)
                                		<option value='{!!$emp->RFID!!}' selected>{!!$emp->name!!} {!!$emp->last_name!!}</option>
                                	@else
                                    	<option value='{!!$emp->RFID!!}'>{!!$emp->name!!} {!!$emp->last_name!!}</option>
                                    @endif
                                 @endif
                            @endforeach
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
        			</div>
        		</div>


			</div>
			<div class="container-fluid">
		      	<div class="row">
            		<div class="col-sm-3">
            			{!!$photo->current_position_dr!!}
            		</div>

            	</div>
            </div>
		</div>
	</div>
</header>

<!-- Example Tabs -->
<div class="row">
	<div class="col-lg-12">
	<table width="100%">
	<?php
	$img_source_date = att_img_source_date();
    $img_source_ip = att_image_source_ip();
	$thu_shift = is_in_thu_shift($rfid,$year,$month);
	$sun_shift = is_in_thu_shift($rfid,$year,$month,3);
    $sat_shift = is_in_thu_shift($rfid,$year,$month,4);
    $night_shift = is_in_night_shift($rfid);
    $quarantin_shift = is_in_quarantine_shift($rfid,'evening');
	$sday = att_month_days(0);
	$eday = att_month_days(1);
    
    $from = calculate_attendence_date($sday,$month,$year,'start_date');
	$to = calculate_attendence_date($eday,$month,$year,'end_date');
	$from_shamsi = convertDateAndTime($from,'to_shamsi');
    $from_shamsi = explode('-',$from_shamsi);
    //shift for those who is completely off on thu
	$thu_shift = is_in_university_shift($rfid,$from_shamsi[0],$from_shamsi[1],0); // $from_shamsi index 0 is year index 1 is month
	$sun_shift = is_in_university_shift($rfid,$from_shamsi[0],$from_shamsi[1],3); // $from_shamsi index 0 is year index 1 is month
    $sat_shift = is_in_university_shift($rfid,$from_shamsi[0],$from_shamsi[1],4); // $from_shamsi index 0 is year index 1 is month 

	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	$day_counter=1;
	?>
	@foreach($period AS $day)
		<?php
			$the_day = $day->format( "Y-m-d" );
			$dayOfWeek = date('D',strtotime($the_day));
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_monthDay = $period_month.$period_day;
			$shamsi_day = dateToShamsi($period_det[0],$period_det[1],$period_det[2]);
			$imgin_id = '';$imgout_id = '';
			$correct = false;$sign_in = false;$sign_out = false;
			$signin_img = '/img/default.jpeg';$signout_img = '/img/default.jpeg';
			$rejected_in = 0;$rejected_out = 0;


			//leaves
			$isInLeave = isEmployeeInLeave_today($photo->id,$the_day);
		?>
		@if($details)
		<?php $night_shift_in = false; ?>
		@foreach($details as $img)
			<?php
			$path = explode("_",$img->path);
			$time = (int)substr($path[0],-6);//get the time part in path
			$month_day = substr($path[0],-11,4);//get the monthDay part in path
			$$month_day = $night_shift_in;

			if($period_monthDay == $month_day)
			{
				$correct = true;
				if($night_shift)
				{
					if(!$$month_day)
					{
						$night_shift_in = true;
						$sign_in = true;
						if($year.$month < $img_source_date)
						{
							$signin_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signin_img = '/img/default.jpeg';
							}
							else 
							{
								$signin_img = $img_source_ip."/".$img->path;
							}
						}

						$imgin_id = $img->id;
						$rejected_in = $img->status;

						$late_status_in = $img->late_status;
					}
					else
					{
						$sign_out = true;
						if($year.$month < $img_source_date)
						{
							$signout_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signout_img = '/img/default.jpeg';
							}
							else 
							{
								$signout_img = $img_source_ip."/".$img->path;
							}
						}
						//$signout_img = "http://10.134.45.19/".$img->path;

						$imgout_id = $img->id;
						$rejected_out = $img->status;

						$late_status_out = $img->late_status;
					}
                }
                elseif($quarantin_shift && is_in_quarin_date($the_day))
				{
					if(!$$month_day)
					{
						$night_shift_in = true;
						$sign_in = true;
						if($year.$month < $img_source_date)
						{
							$signin_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signin_img = '/img/default.jpeg';
							}
							else 
							{
								$signin_img = $img_source_ip."/".$img->path;
							}
						}

						$imgin_id = $img->id;
						$rejected_in = $img->status;

						$late_status_in = $img->late_status;
					}
					else
					{
						$sign_out = true;
						if($year.$month < $img_source_date)
						{
							$signout_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signout_img = '/img/default.jpeg';
							}
							else 
							{
								$signout_img = $img_source_ip."/".$img->path;
							}
						}
						//$signout_img = "http://10.134.45.19/".$img->path;

						$imgout_id = $img->id;
						$rejected_out = $img->status;

						$late_status_out = $img->late_status;
					}
				}
				else
				{
					if($time <= 120000)
					{
						$sign_in = true;
						if($year.$month < $img_source_date)
						{
							$signin_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signin_img = '/img/default.jpeg';
							}
							else 
							{
								$signin_img = $img_source_ip."/".$img->path;
							}
						}

						$imgin_id = $img->id;
						$rejected_in = $img->status;

						$late_status_in = $img->late_status;
					}
					else
					{
						$sign_out = true;
						if($year.$month < $img_source_date)
						{
							$signout_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signout_img = '/img/default.jpeg';
							}
							else 
							{
								$signout_img = $img_source_ip."/".$img->path;
							}
						}
						//$signout_img = "http://10.134.45.19/".$img->path;

						$imgout_id = $img->id;
						$rejected_out = $img->status;

						$late_status_out = $img->late_status;
					}
				}
			}
			if($sign_in && $sign_out)
			{
				break;
			}
			?>
		@endforeach
		@endif
		<!-- sign in -->
		@if($day_counter == 1 || $day_counter%11==1)
		<tr>
		@endif
		@if($the_day <= date('Y-m-d'))

		<?php
            $holiday = checkHoliday($the_day);
            $urgents = checkUrgentHoliday($the_day);
            $sign_in_other_label = false; 
            $sign_out_other_label = false; 
		?>
		<td style="position: relative;padding-bottom: 2em;">
		<!-- sign in pic -->
			<div style="position: relative;">
				<!-- if there is a pic for this day then send the id to onclick function -->
				<img src="{!!$signin_img!!}" class="att_img" @if($rejected_in!=0) style="opacity: 0.5" @endif alt="Image Corrupt" height="112" width="90" style="border-top:1px solid;border-left:1px solid;border-right:1px solid"/>
				@if($photo->att_in_date>$the_day)
                    <?php $sign_in_other_label = true; ?>    
                    <span style="position: absolute;right: 35px;top: 20px;color:red; font-size:15px;background:#FFF;padding:0 10px">شامل سیستم نمیباشد</span>
				@else
					@if($isInLeave)
						@if($isInLeave->type==7)
                            <span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">خدمتی</a></span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==9)
                            <span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">دیگر موارد</a></span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==1)
                            <span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی ضروری</a></span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==2)
                            <span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی تفریحی</a></span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==3)
                            <span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی مریضی</a></span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==4)
                            <span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی ولادی</a></span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==5)
                            <span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی عروسی</a></span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==6)
                            <span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی حج</a></span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==8)
                            <span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">اضافه رخصتی مریضی</a></span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==10)
                            <span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">شامل کتاب</a></span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==11)
                            <span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">اضافه رخصتی تفریحی</a></span>
                            <?php $sign_in_other_label = true; ?>
						@endif
						    <span style="position: absolute;right: 35px;top: 10px;color:red; font-size:17px;padding:0 10px" id="{!!$imgin_id!!}_span">@if($rejected_in!=0) عکس قابل قبول نمیباشد @endif</span>
					@elseif($holiday)
                        <span style="position: absolute;right: 20px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px" title="{!!$holiday->desc!!}">رخصتی عمومی</span>
                        <?php $sign_in_other_label = true; ?>
					@elseif($urgents)
                        <span style="position: absolute;right: 20px;top: 20px;color:red; font-size:15px;background:#FFF;padding:0 10px" title="{!!$urgents->desc!!}">حالت فوق العاده</span>
                        <?php $sign_in_other_label = true; ?>
					@elseif($dayOfWeek=='Fri')
						<span style="position: absolute;right: 35px;top: 20px;color:red; font-size:15px;background:#FFF;padding:0 10px">جمعه</span>
                        <span style="position: absolute;right: 35px;top: 10px;color:red; font-size:17px;padding:0 10px" id="{!!$imgin_id!!}_span">@if($rejected_in!=0) عکس قابل قبول نمیباشد @endif</span>
                        <?php $sign_in_other_label = true; ?>
                    @elseif($img_source_ip=='not_ping_able' && !$sign_in_other_label && !($dayOfWeek == 'Thu' && check_day_is_in_shfit($thu_shift,$shamsi_day) || $dayOfWeek == 'Sun' && check_day_is_in_shfit($sun_shift,$shamsi_day) || $dayOfWeek == 'Sat' && check_day_is_in_shfit($sat_shift,$shamsi_day)))
					  <span style="position: absolute;right: 4px;top: 20px;font-size:15px;background:#FFF;padding:0 10px">@if($sign_in) <span style="color:green;">حاضر</span> @else <span style="color:red;"> غیر حاضر </span> @endif</span>    
					@endif
				@endif
			</div>
		<!-- sign out pic -->
			<div style="position: relative;">
				<img src="{!!$signout_img!!}" class="att_img" @if($rejected_out!=0) style="opacity: 0.5" @endif alt="Image Corrupt" height="112" width="90" style="border-bottom:1px solid;border-left:1px solid;border-right:1px solid"/>

				<span style="position: absolute;right: 35px;top: 10px;color:red; font-size:19px;padding:0 10px" id="{!!$imgout_id!!}_span">@if($rejected_out!=0) عکس قابل قبول نمیباشد @endif</span>
				@if($dayOfWeek == 'Thu' && check_day_is_in_shfit($thu_shift,$shamsi_day))
                    <span style="position: absolute;right: 35px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
                    <?php $sign_in_other_label = true; ?>
				@elseif($dayOfWeek == 'Sun' && check_day_is_in_shfit($sun_shift,$shamsi_day))
                    <span style="position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
                    <?php $sign_in_other_label = true; ?>
				@elseif($dayOfWeek == 'Sat' && check_day_is_in_shfit($sat_shift,$shamsi_day))
                    <span style="position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
                    <?php $sign_in_other_label = true; ?>
                {{-- If Image Server is not pingable and day is not friday  --}}
                @elseif($img_source_ip=='not_ping_able' && !($sign_out_other_label || $sign_in_other_label))
                    <span style="position: absolute;right: 4px;top: 20px;font-size:15px;background:#FFF;padding:0 10px">@if($sign_out) <span style="color:green;">حاضر</span> @else <span style="color:red;"> غیر حاضر </span> @endif</span>
					    
				@endif
			</div>
			<!--
			($dayOfWeek=='Fri' || $dayOfWeek == 'Thu')
			-->
			@if($dayOfWeek=='Fri')
			<span style="position: absolute;right: 20px;bottom: 1px;color:red">{!!$shamsi_day!!}</span>
			@else
			<span style="position: absolute;right: 20px;bottom: 1px;">{!!$shamsi_day!!}</span>
			@endif
		</td>
		@endif
		@if($day_counter%11==0)
		</tr>
		@endif

		<?php $day_counter++;?>
	@endforeach
	</table>
	</div>
</div>
<!-- End Example Tabs -->

@stop
@section('footer-scripts')
<script>

function bring_employee_images()
{
	if($('#emp_rfid').val()!="")
	{
		var rfid = $('#emp_rfid').val();
	}
	else
	{
		var rfid = "{!!$rfid!!}";
	}
	var year = $('#year').val();
	var month = $('#month').val();
	var dep = "{!!$dep!!}";
	var sub_dep = "{!!$sub_dep!!}";

    window.location = "/hr/getEmployeeAttendanceCheck/"+rfid+"/"+year+"/"+month+"/"+dep+"/"+sub_dep;

}
</script>
@stop
