@extends('layouts.master')

@section('head')
	{!! HTML::style('/vendor/clockpicker/clockpicker.min.css') !!}
	{!! HTML::style('/vendor/select2/select2.css') !!}
  {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
  {!! HTML::script('/js/components/select2.js')!!}
	<style>
	.clockpicker-popover{
		z-index:100000;
	}
	.custom_date {
		display: none;
	}

	</style>
	<script type="text/javascript">
		$("#sub_dep").tabIndex=4;

	</script>
    <title>{!!_('attendance')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
	<form class="form-horizontal" role="form" id="att_report" method="post">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>حاضری کارمندان</span></li>
        </ol>
        @if(Auth::user()->id == 715 || Auth::user()->id == 1286 || Auth::user()->id == 450 || Auth::user()->id == 113 || Auth::user()->id == 2038)
        <!-- jawadi roles -->
          @if((Auth::user()->id != 1286 || Auth::user()->id != 450) || Auth::user()->id == 2038)
        	<a href="{!!URL::route('putImagesToHr')!!}" onclick="load_loading();" class="btn btn-primary">علاوه نمودن عکس ها</a>
          @endif
        	<a href="{!!URL::route('notifyManager')!!}" class="btn btn-primary">اطلاعیه ها</a>
        	
        @endif
        @if(Auth::user()->id == 241)
        <a href="{!!URL::route('putImagesToHr')!!}" onclick="load_loading();" class="btn btn-primary">علاوه نمودن عکس ها</a>
        @endif
        @if(Auth::user()->id == 201)
        <!-- farkhona roles -->
        	<a href="{!!URL::route('putImagesToHr')!!}" onclick="load_loading();" class="btn btn-primary">علاوه نمودن عکس ها</a>
        	<a href="{!!URL::route('notifyManager')!!}" class="btn btn-primary">اطلاعیه ها</a>
        	<a href="{!!URL::route('getAllInShifts')!!}" class="btn btn-primary">بخش محصلین</a>
	        <a href="{!!URL::route('getAllAttEmps')!!}" class="btn btn-primary">کارمندان شامل حاضری الکترونیکی</a>
        @endif
        @if(canAttHoliday())
       		<a href="{!!URL::route('leavesManager')!!}" class="btn btn-primary">مدیریت رخصتی ها</a>

       	@endif
       	@if(canAddHoliday() && !hasRule('hr_attendance','procurement_holiday'))
       		<a href="{!!URL::route('holidaysManager')!!}" class="btn btn-primary">رخصتی های عمومی</a>
       	@endif
       	@if(canAttTodayAtt())
       		<a href="javascript:void()" class="btn btn-primary" data-target="#today_att" data-toggle="modal">علاوه نمودن حاضری امروز</a>
	        <a href="javascript:void()" onclick="dailyAttendanceReport()" class="btn btn-primary"> راپور حاضری روزانه</a>
       	@endif
        @if(canAdd('hr_attendance'))
        	<button class="btn btn-primary pull-right" type="button" onclick="getSearchResult('all')"> راپور حاضری</button>
	        <button class="btn btn-primary" type="button" onclick="getCheckImages('all')">بررسی عکسها</button>
			<button class="btn btn-primary" type="button" onclick="getCheckImages_ajir('all')">حاضری اجیران</button>
    		<a href="javascript:void()" onclick="dailyAttendance()" class="btn btn-primary">اکسل روزانه</a>
	        @if(Auth::user()->id == 113 || Auth::user()->id == 174)
	        <!-- fahim roles
	        <a href="javascript:void()" class="btn btn-primary" data-target="#change_employee" data-toggle="modal">تایم حاضری</a>
	        -->
	        <a href="{!!URL::route('notifyManager')!!}" class="btn btn-primary">اطلاعیه ها</a>
	        <a href="{!!URL::route('putImagesToHr')!!}" onclick="load_loading();" class="btn btn-primary">علاوه نمودن عکس ها</a>
	        @endif
	        <a href="{!!URL::route('getAllInShifts')!!}" class="btn btn-primary">بخش محصلین</a>
	        <a href="{!!URL::route('getAllAttEmps')!!}" class="btn btn-primary">کارمندان شامل حاضری الکترونیکی</a>
	    @elseif(canCheckImages())
       		<button class="btn btn-primary pull-right" type="button" onclick="getSearchResult('all')"> راپور حاضری</button>
            <button class="btn btn-primary" type="button" onclick="getCheckImages('all')">بررسی عکسها</button>
            <button class="btn btn-primary" type="button" onclick="getCheckImages_ajir('all')">حاضری اجیران</button>
       	@endif
    </div>
</div>
@if(canAdd('hr_attendance') || canCheckImages())
<header class="main-box-header clearfix">

    	<div class="row">
        	<div class="col-lg-12">

        		<div class="container-fluid">
			      	<div class="row">
								<div class="col-sm-2 col-md-1">
								    <div class="custom-control custom-radio">
								        <input type="radio" id="customRadio1" checked onclick="changeSearchType('monthly_date')" name="date_type" value="0" class="custom-control-input">
								        <label class="custom-control-label" for="customRadio1">ماهوار</label>
								    </div>
								    <div class="custom-control custom-radio">
								        <input type="radio" id="customRadio2" onclick="changeSearchType('custom_date')" name="date_type" value="1" class="custom-control-input">
								        <label class="custom-control-label" for="customRadio2">تاریخ دلخوا</label>
								    </div>
								</div>
								<div class="custom_date">
									 <div class="col-sm-2">
									 		<label for="from_date">از تاریخ:</label>
											<input type="text" name="from_date" id="from_date" value="" class="form-control datepicker_farsi" placeholder="شروع تاریخ">
									 </div>
									 <div class="col-sm-2">
									 		<label for="to_date">الی تاریخ:</label>
											<input type="text" name="to_date" id="to_date" value="" class="form-control datepicker_farsi" placeholder="ختم تاریخ">
									 </div>
								</div>
							<div class="monthly_date">
								<div class="col-sm-2">
											<div class="col-sm-12">
												<label class="col-sm-12 ">سال</label>
																<select name="year" id="year" class="form-control">
																		<?php
																		//default selected values for year and month
																		$today = dateToShamsi(date('Y'),date('m'),date('d'));
													$month = explode('-',$today);
													$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');
													$year = $month[0];
													?>
																		@for($i=$year-5;$i<$year+5;$i++)
																			@if($i==$year)
																				<option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
																				@else
																				<option value='{!!$i!!}'>{!!$i!!}</option>
																				@endif
																		@endfor
																</select>
																		<!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
											</div>
										</div>
										<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ماه</label>

                                <select name="month" id="month" class="form-control">

                                    @for($j=1;$j<13;$j++)
                                    	<?php
                                    	$m = $j;
                                    	if($j<10) $m='0'.$j;
                                    	?>
                                    	@if($m==$month[1])
                                        <option value='{!!$m!!}' selected="selected">{!!$month_name[$j]!!}</option>
                                        @else
                                        <option value='{!!$m!!}'>{!!$month_name[$j]!!}</option>
                                        @endif
                                    @endfor
                                </select>
                                {{-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> --}}
                			</div>
                		</div>
							</div>

					    <div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی</label>
                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                    @if(hasRule('hr_attendance','filter_general_dept'))
                                      <option value="1000000">اداره عمومی را انتخاب نماید.</option>
                                    @else
                                      <option value="">اداره عمومی را انتخاب نماید.</option>
                                    @endif
                                    @foreach($parentDeps AS $dep_item)
                                      <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                    @endforeach
                                </select>
                                    {{-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> --}}
                			</div>
                		</div>
                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 control-label">ادارۀ مربوط</label>
                			</div>
                			<div class="col-sm-12">
                                <select style="width:100%;" name="sub_dep" id="sub_dep">
                                    <option value='0'>همه</option>
                                </select>
                			</div>
                		</div>

					</div>
				</div>

			</form>
			</div>
		</div>

</header>
@endif
<!-- Example Tabs -->
<div id="all">
</div>
<!-- End Example Tabs -->
<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">

	<div class="modal-dialog" style="width: 50%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" id="time_close_btn" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>

			<div class="modal-body">
				<div class="row">
	        	<div class="col-lg-12">
	            <form class="form-horizontal" role="form" id="attendanceTime" method="post">
				    <div class="panel-heading">
				      <h5 class="panel-title">تایم حاضری</h5>
				    </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-6">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">سال</label>
	                                <select name="time_year" id="time_year" class="form-control">
	                                    <?php
	                                    //default selected values for year and month
	                                    $today = dateToShamsi(date('Y'),date('m'),date('d'));
	                					$month = explode('-',$today);
	                					$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');
	                					$year = $month[0];
	                					?>
	                                    @for($i=$year-5;$i<$year+5;$i++)
	                                    	@if($i==$year)
	                                        <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
	                                        @else
	                                        <option value='{!!$i!!}'>{!!$i!!}</option>
	                                        @endif
	                                    @endfor
	                                </select>
	                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	                			</div>
	                		</div>
	                		<div class="col-sm-6">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ماه</label>

	                                <select name="time_month" id="time_month" class="form-control" onchange="bringTime()">

	                                    @for($j=1;$j<13;$j++)
	                                    	@if($j==$month[1])
	                                        <option value='{!!$j!!}' selected="selected">{!!$month_name[$j]!!}</option>
	                                        @else
	                                        <option value='{!!$j!!}'>{!!$month_name[$j]!!}</option>
	                                        @endif
	                                    @endfor
	                                </select>
	                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	                			</div>
	                		</div>
	                	</div>
	                </div>
	                <div class="container-fluid" id="year_time_div">
	                	<div class="row">
	                		<div class="col-sm-6">
				            	<div class="col-sm-12">
					                <label class="col-sm-12 ">شروع</label>
					                <div class="example">
					                  <div class="input-group">
					                    <span class="input-group-addon">
					                      <span class="wb-time"></span>
					                    </span>
					                    <input type="text" name="time_in" id="time_in" value="@if(getAttendanceTime($year,$month[1])){!!getAttendanceTime($year,$month[1])->time_in!!}@endif" class="timepicker form-control" data-plugin="clockpicker" data-autoclose="true">
					                  </div>
					                </div>
				              	</div>
				              <!-- End Example Auto Colse -->
				            </div>

	                		<div class="col-sm-6">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ختم</label>
	                                <div class="example">
					                  <div class="input-group">
					                    <span class="input-group-addon">
					                      <span class="wb-time"></span>
					                    </span>
					                    <input type="text" name="time_out" id="time_out" value="@if(getAttendanceTime($year,$month[1])){!!getAttendanceTime($year,$month[1])->time_out!!}@endif" class="timepicker form-control" data-plugin="clockpicker" data-autoclose="true">
					                  </div>
					                </div>
	                			</div>
	                		</div>

	                	</div>
	                </div>
	                <div class="panel-heading">
				      <h5 class="panel-title">پنجشنبه ها</h5>
				    </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-6">
				            	<div class="col-sm-12">
					                <label class="col-sm-12 ">شروع</label>
					                <div class="example">
					                  <div class="input-group">
					                    <span class="input-group-addon">
					                      <span class="wb-time"></span>
					                    </span>
					                    <input type="text" name="time_in_thu" id="time_in_thu" value="@if(getAttendanceTime_thu($year,$month[1])){!!getAttendanceTime_thu($year,$month[1])->time_in!!}@endif" class="timepicker form-control" data-plugin="clockpicker" data-autoclose="true">
					                  </div>
					                </div>
				              	</div>
				              <!-- End Example Auto Colse -->
				            </div>

	                		<div class="col-sm-6">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ختم</label>
	                                <div class="example">
					                  <div class="input-group">
					                    <span class="input-group-addon">
					                      <span class="wb-time"></span>
					                    </span>
					                    <input type="text" name="time_out_thu" id="time_out_thu" value="@if(getAttendanceTime_thu($year,$month[1])){!!getAttendanceTime_thu($year,$month[1])->time_out!!}@endif" class="timepicker form-control" data-plugin="clockpicker" data-autoclose="true">
					                  </div>
					                </div>
	                			</div>
	                		</div>

	                	</div>
	                </div>
		            <div class="container-fluid">
	                	<div class="row">
				    		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_attendance'))
	                					<button class="btn btn-primary" type="button" onclick="submit_time();"><i class="fa fa-refresh fa-lg"></i> ذخیره</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>
	                	</div>
	                </div>
					{!! Form::token() !!}
				</form>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal-fade-in-scale-up" id="today_att" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">

	<div class="modal-dialog" style="width: 50%">
		<div class="modal-content">
			<div class="modal-header" style="display:none;">
				<button type="button" id="today_close_btn" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>

			<div class="modal-body">
                <div class="container-fluid" >
                   <div class="row" id="result_div">
                   </div>
                </div>
				<div class="row">
		        	<div class="col-lg-12">
					    <div class="panel-heading" style="display:none" id="msg_div">
					      <h5 class="panel-title">در حال ذخیره حاضری امروز ... لطفا منتظر باشید.</h5>
					    </div>
					    <div class="panel-heading" style="display:none" id="success_div">
					      <h5 class="panel-title">حاضری موفقانه ذخیره شد</h5>
                        </div>
					    <div class="container-fluid" id="button_div">
		                	<div class="row">
					    		<div class="col-sm-12">
		                			<div class="col-sm-12" style="text-align: center">
                                        <p>بخاطر ذخیره سازی حاضری به شکل موقعت تاریخ را انتخاب کنید</p>
                                    </div>
                                    <div class="col-sm-12">
                                        <input type="text" name="from_date" id="from_date" value="" class="form-control datepicker_farsi" placeholder="تاریخ را انتخاب کنید">
                                    </div>
                                    <div class="col-sml-12">
                                        <button style="margin-top: 20px;" class="btn btn-primary pull-right" type="button" onclick="$('#today_close_btn').click();" ><i class="fa fa-close"></i> بستن</button>
		                				<button style="margin-top: 20px;" class="btn btn-primary" type="button" onclick="insertAttendance_today()"><i class="fa fa-refresh fa-lg"></i> ذخیره</button>
                                    </div>
		                		</div>
		                	</div>
		                </div>
		                <div class="container-fluid" style="display:none" id="success_button_div">
		                	<div class="row">
					    		<div class="col-sm-12">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">&nbsp;</label>
		                				<button class="btn btn-primary" type="button" onclick="$('#today_close_btn').click();"><i class="fa fa-refresh fa-lg"></i> بستن</button>
		                			</div>
		                		</div>
		                	</div>
		                </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="md-overlay"></div>
@stop
@section('footer-scripts')
{!! HTML::script('/vendor/clockpicker/bootstrap-clockpicker.min.js') !!}
{!! HTML::script('/js/components/clockpicker.min.js') !!}
<script type="text/javascript">
    $("#sub_dep").select2();

	function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function bringRelatedEmployees(id)
    {
    	var year = $('#year').val();
    	var month = $('#month').val();
    	$.ajax({
                url: '{!!URL::route("getRelatedEmployees")!!}',
                data: '&dep_id='+id+'&year='+year+'&month='+month,
                type: 'post',

                success: function(response)
                {
                   $('#all').html(response);
                }
            }
        );
    }

    function submit_time()
	{
		if($('#time_in').val()=='' || $('#time_in').val()=='00:00:00')
		{
			alert('please set the time in');return;
		}
		if($('#time_out').val()=='' || $('#time_out').val()=='00:00:00')
		{
			alert('please set the time out');return;
		}
		var datas = $('#attendanceTime').serialize();
		var page = "{!!URL::route('postAttendanceTime')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: datas,
	        success: function(r){
	        	$('#time_close_btn').click();
	        }
	    });
	}

	function insertAttendance_today()
	{
		$('#button_div').hide();
		$('#msg_div').show();
        var parmas = 'from_date='+$('#from_date').val(); 
		var page = "{!!URL::route('insertAttendanceImages_today')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: parmas,
	        success: function(result)
            {
	        	$('#msg_div').hide();
				$('#success_button_div').show();
                $('#result_div').html(result);
				//sendAbsentEmails();
	        }
	    });
	}
	function sendAbsentEmails()
	{
		var page = "{!!URL::route('sendAbsent_emails')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: '',
	        success: function(r){

	        }
	    });
	}
	function dailyAttendance()
	{
		var year = $('#year').val();
		var month = $('#month').val();
		var general_department = $('#general_department').val();
		if(general_department=='')
		{
			general_department = 0;
		}
		var sub_dep = $('#sub_dep').val();
		if(sub_dep=='')
		{
			sub_dep = 0;
		}

	    window.location = "getAllAttendances/"+year+"/"+month+"/"+general_department+"/"+sub_dep;

	}
	function dailyAttendanceReport()
	{
		var general_department = $('#general_department').val();
		if(general_department=='')
		{
			alert('لطفا یک اداره عمومی را انتخاب نمایید');return;
		}
		var sub_dep = $('#sub_dep').val();
        var from_date = $('#from_date').val(); 
        var to_date   = $('#to_date').val(); 

	    window.location = "getTodayAttendances/"+general_department+"/"+sub_dep+"/"+0+"/"+0+"/"+from_date+"/"+to_date;

	}
	function bringTime()
	{
		var year = $('#time_year').val();
		var month= $('#time_month').val();
		var page = "{!!URL::route('bringYearTime')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        dataType: 'json',
	        data: '&year='+year+'&month='+month,
	        success: function(r){
	        	$('#time_in').val(r.time_in);
	        	$('#time_out').val(r.time_out);
	        	$('#time_in_thu').val(r.time_in_thu);
	        	$('#time_out_thu').val(r.time_out_thu);
	        }
	    });
	}
	function getSearchResult(div)
    {
    	if($('#general_department').val()=='')
    	{
    		alert('Please select a department');return;
    	}
        $.ajax({
                url: '{!!URL::route("getAttendanceAjax")!!}',
                data: $('#att_report').serialize()+"&check=0",
                type: 'post',
                beforeSend: function(){

                    //$("body").show().css({"opacity": "0.5"});
                    $("#"+div).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {

                    $('#'+div).html(response);
                }
            }
        );

        return false;

    }
    function getCheckImages(div)
    {
        $.ajax({
                url: '{!!URL::route("getAttendanceAjax")!!}',
                data: $('#att_report').serialize()+"&check=1",
                type: 'post',
                beforeSend: function(){

                    //$("body").show().css({"opacity": "0.5"});
                    $("#"+div).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {

                    $('#'+div).html(response);
                }
            }
        );

        return false;

    }
    function getCheckImages_ajir(div)
    {
        $.ajax({
                url: '{!!URL::route("getAttendanceAjax")!!}',
                data: $('#att_report').serialize()+"&check=2",
                type: 'post',
                beforeSend: function(){

                    //$("body").show().css({"opacity": "0.5"});
                    $("#"+div).html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {

                    $('#'+div).html(response);
                }
            }
        );

        return false;

    }
	function load_loading()
	  {
	    $('.panel-body').css('opacity','0.4');
	    $('.panel-body').css('pointer-events','none');
	    $('#attendance_loading').show();
	  }

		// change display of search type
		function changeSearchType(type)
		{
			if(type=='monthly_date')
			{
				$('div.monthly_date').css('display','block');
				$('div.custom_date').css('display','none');
			}
			else if(type=='custom_date'){
				$('div.monthly_date').css('display','none');
				$('div.custom_date').css('display','block');
			}
		}
</script>
@stop
