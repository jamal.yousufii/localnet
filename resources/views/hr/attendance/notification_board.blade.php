@extends('layouts.master')

@section('head')
    <title>مدیریت اطلاعیه ها</title>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">لوحه معلومات</a></li>
            <li class="active"><span>اطلاعیه ها</span></li>
        </ol>

        
        
    </div> 
</div>

<div class="row">
@if(Session::has('success'))
	<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif
<a href="javascript:void()" class="table-link" style="float: right;" data-target="#rfid_modal" data-toggle="modal">
    <span class="fa-stack" title="New Notification">
        <span class="icon fa-plus"></span>
    </span>
</a>
    <div class="col-lg-12">
        <div class="main-box">
            
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='list'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>محتوی</th>
                        <th>تاریخ</th>
                        <th>عملیه</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($records)
                        @foreach($records AS $row)
                            <tr>
                                <td>{!!$row->id!!}</td>
                                <td id="content_{!!$row->id!!}">{!!$row->content!!}</td>
                                <td id="content_{!!$row->id!!}">{!!$row->created_at!!}</td>
                                <td id="Notification_operation_{!!$row->id!!}">
                                    <a href="javascript:void()" onclick="load_position_edit({!!$row->id!!});" class="table-link" style="text-decoration:none;" data-target="#edit_modal" data-toggle="modal">
                                        <i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
                                    </a> | 
                                    @if($row->status==0)
                                    <a href="javascript:void()" onclick="publish_notificationn({!!$row->id!!},1);" class="table-link" style="text-decoration:none;">
                                        <i class="icon fa-bullhorn" aria-hidden="true" style="font-size: 16px;" title="published"></i>
                                    </a>
                                    @else
                                    <a href="javascript:void()" onclick="publish_notificationn({!!$row->id!!},0);" class="table-link" style="text-decoration:none;">
                                        <i class="icon fa-ban" aria-hidden="true" style="font-size: 16px;" title="un published"></i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else

                    @endif
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="rfid_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Content</h4>
            </div>
            <div class="modal-body">
                <form class="comment_form" method="post" action="{!!URL::route('saveNewNotify')!!}" class="form-horizontal">
                    <div class="form-group">
                        <textarea class="form-control" name="content"></textarea>
                    </div>
                
                    <button type="submit" type="button" class="btn btn-primary">
                        <i class="fa fa-save fa-lg"></i> Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="edit_modal" aria-hidden="true" aria-labelledby="edit_modal" role="dialog" tabindex="-1"></div>

<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')
<script type="text/javascript">
function load_position_edit(id)
{
    $.ajax({
            url: '{!!URL::route("editNotifyModal")!!}',
            data: '&id='+id,
            type: 'post',
            
            success: function(response)
            {
               $('#edit_modal').html(response);
            }
        }
    );
}
function publish_notificationn(id,type)
{
    if(confirm('مطمئن هستید؟'))
    {
        $.ajax({
                url: '{!!URL::route("publishNotify")!!}',
                data: '&id='+id+'&type='+type,
                type: 'post',
                
                success: function(response)
                {
                   $('#Notification_operation_'+id).html(response);
                }
            }
        );
    }
}

</script>
@stop

