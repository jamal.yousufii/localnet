@extends('layouts.print_master')
@section('content')
<style type="text/css">
@media print{
  .no-print ,no-print *{
    display: none !important;
  }
}
</style>
<?php 
    $leave_form_type = (getUserDepType(Auth::user()->id)->dep_type==1 ? 'ocs_leave_form.jpg' : 'aop_leave_form.jpg');
    $image = base64_encode(file_get_contents('documents/hr_attachments/'.$leave_form_type));
    $profile_pic = 'data:image/jpg;base64,'.$image;
?>
<div class="container" dir="rtl">
  <div class="content" style="text-align: center;   ">
    <br><br>
    <a href="{!!URL::route('downloadLeaveForm')!!}" style="color: green" class="no-print"><b>
    <i class='fa fa-download fa-4x text-success'></i></b></a> &nbsp;  &nbsp; &nbsp;
    
    <img src="{!!$profile_pic!!}" alt="Image Not Found!" style="width:595px;border-radius:10px;" />
  </div>
</div>
                       
    @stop

@section('footer-scripts') 

@stop