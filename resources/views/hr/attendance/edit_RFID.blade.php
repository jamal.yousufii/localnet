@extends('layouts.master')

@section('head')
    <title>Employee Card Details</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
    <link href="{{ asset('crapper/cropper.min.css') }}" rel="stylesheet">
    <style>
    /* create file  */
    .custom-file-input {
        display: none;
    }
    #cropper-image{
      display: none
    }

    .cropper-wrapper{
      display: none;
      height:400px;
      width:800px;
    }
    </style>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active"><span>معلومات کارت کارمند</span></li>
        </ol>
    </div>
</div>

    <div class="row">

        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form class="form-horizontal card-form" role="form" method="post" action="" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">نام و تخلص به دری</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="name_dr" value="" disabled>
                                    <span style="color:red">{!!$errors->first('name_dr')!!}</span>
                                </div>

                                <label class="col-sm-2 control-label">وظیفه فعلی به دری</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="current_position_dr" value="" disabled>
                                    <span style="color:red"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="name_dr" value="" disabled>
                                    <span style="color:red">{!!$errors->first('name_dr')!!}</span>
                                </div>

                                <label class="col-sm-2 control-label">وظیفه فعلی به دری</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="current_position_dr" value="" disabled>
                                    <span style="color:red"></span>
                                </div>
                            </div>
                        </div>
                       

                        <div class="row">
                            <div class="form-group">

                                <label class="col-sm-2 control-label">Old RFID</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="current_position_dr" value="" disabled >
                                    <span style="color:red"></span>
                                </div>

                                <label class="col-sm-2 control-label">Archive RFID</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="current_position_en" value="">
                                </div>

                            </div>
                        </div>


                        {!! Form::token() !!}

                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10">

                                <button class="btn btn-primary" type="submit">ثبت تغییرات کارت</button>

                                <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>


                            </div>
                        </div>
                    </form>

                    <!-- card print preview -->
                </div>
            </div>
        </div>
    </div>


@stop


@section('footer-scripts')
    {!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}
    <script src="{!!asset('crapper/cropper.min.js')!!}" type="text/javascript"></script>


<script type="text/javascript">


   

    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringRelatedSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }

</script>

@stop
