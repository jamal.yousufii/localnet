@extends('layouts.master')

@section('head')
    <title>{!!_('hr_localization')!!}</title>
@stop
@section('content')
<div class="row">
	<div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>لیست وظایف کارمندان</span></li>
        </ol>
    </div>
</div>
<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix" style="margin-top:10px">
		    
		</header>
		<div class="modal-body">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table" id="list">
                    <thead>
                      	<tr>
	                        <th>#</th>
	                        <th>نام کامل</th>
	                        <th>ولد</th>
	                        <th>ریاست</th>
	                        <th>وظیفه دری</th>
	                        <th>وظیفه انگلیسی</th>
	                        <th>عملیه</th>
	                    </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="salary_modal" aria-hidden="true" aria-labelledby="salary_modal" role="dialog" tabindex="-1"></div>

<div class="md-overlay"></div><!-- the overlay element -->

@stop
@section('footer-scripts')

<script type="text/javascript">
$(document).ready(function() {
	$('#list').dataTable(
	    {
	        'sDom': 'lf<"clearfix">tip',
	        "bProcessing": true,
	        "bServerSide": true,
	        "iDisplayLength": 10,
	        "sAjaxSource": "{!!URL::route('getAllPositionsData')!!}",
	        "language": {
	            "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
	            "zeroRecords": "ریکارد موجود نیست",
	            "info": "نمایش صفحه _PAGE_ از _PAGES_",
	            "infoEmpty": "ریکارد موجود نیست",
	            "search": "جستجو",
	            "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
	        }
	    }
	);
	
});
function load_position_edit(id)
{
	$.ajax({
            url: '{!!URL::route("editPositionModal")!!}',
            data: '&id='+id,
            type: 'post',
            
            success: function(response)
            {
               $('#salary_modal').html(response);
            }
        }
    );
}
</script>
@stop