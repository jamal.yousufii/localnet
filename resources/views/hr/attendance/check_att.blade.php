@extends('layouts.master')

@section('head')
    <title>{!!_('my_attendance')!!}</title>
<style type="text/css">
.leave_span{
	position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 20px;
}
</style>
@stop
@section('content')

<header class="main-box-header clearfix" style="margin-top: -30px;">
	<div class="row">
    	<div class="col-lg-12">
    		<div class="container-fluid">
        		<div class="col-sm-4">
        			<div class="col-sm-12" id="img_controller">
        				<div class="icon-box pull-left">
						<?php
							//Check if Image exist  
							$imgwidth  = 80; 
							if(file_exists(public_path().'/documents/profile_pictures/'.$photo->photo) && $photo->photo!='')
							{
								$data = getimagesize((public_path().'/documents/profile_pictures/'.$photo->photo));
								$image = base64_encode(file_get_contents('documents/profile_pictures/'.$photo->photo));
								$width = $data[0];
								$height = $data[1];
								if($width>2200)
									$imgwidth  = 150;
							}
							else {
								$image = base64_encode(file_get_contents('documents/profile_pictures/default.jpeg'));
							}
							$profile_pic = 'data:image/jpg;base64,'.$image;							
						 ?>	
							<img src="{!!$profile_pic!!}" alt="Image Not Found!" width="150" hight="224"/>
						</div>
        			</div>
        		</div>
	      		<div class="col-sm-2">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">سال</label>
                        <select name="year" id="year" class="form-control" onchange="bring_employee_images()">
                            <?php
        					//$month = explode('-',$today);
        					$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');
        					$selected_year = $year;
        					if($month==13)
        					{
        						$selected_year = $year+1;
        					}

        					?>
                            @for($i=$year-5;$i<$year+5;$i++)
                            	@if($i==$selected_year)
                                <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
                                @else
                                <option value='{!!$i!!}'>{!!$i!!}</option>
                                @endif
                            @endfor
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
        			</div>
        		</div>
        		<div class="col-sm-2">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">ماه</label>
                        <select name="month" id="month" class="form-control" onchange="bring_employee_images()">
                            @for($j=1;$j<13;$j++)
                            	<?php
                            	$m = $j;
                            	if($j<10) $m='0'.$j;
                            	?>
                            	@if($m==$month)
                                <option value='{!!$m!!}' selected="selected">{!!$month_name[$j]!!}</option>
                                @else
                                <option value='{!!$m!!}'>{!!$month_name[$j]!!}</option>
                                @endif
                            @endfor
                        </select>
                        {{-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> --}}
        			</div>
        		</div>
        		{{--
			    <div class="col-sm-4">
			    	<div class="col-sm-12">
        				<label class="col-sm-12 " style="color: red !important;font-family: 'B Nazanin';">تعداد روزهای غیرحاضر در این ماه:</label>

                        <input class="form-control" type="text" value="{!!$absent!!}" readonly="readonly">
        			</div>

        		</div>
        		--}}
			</div>
			<div class="container-fluid">
		      	<div class="row">
            		<div class="col-sm-3">
            			{!!$photo->current_position_dr!!}
            		</div>

            		<div class="col-sm-9" style="margin-bottom: 15px;">
            		<h4 style="color: red !important;font-family: 'B Nazanin';">
            		لطفا در هنگام پیش نمودن کارت خویش, سه ثانیه بعد از شخص قبلی کارت خود را پیش نمایید تا حاضری شما درست ثبت گردد
            		</h4>
            		</div>
            	</div>
            </div>
		</div>
	</div>
</header>

<!-- Example Tabs -->
<div class="row">
	<div class="col-lg-12">
	<table width="auto">
	<?php
    $img_source_date = att_img_source_date();
    $img_source_ip = att_image_source_ip();
    $night_shift = is_in_night_shift($rfid);
     // Quarantin shift 
    $quarantin_shift = is_in_quarantine_shift($rfid,'evening');
	$sday = att_month_days(0);
	$eday = att_month_days(1);
    
    $from = calculate_attendence_date($sday,$month,$year,'start_date');
	$to = calculate_attendence_date($eday,$month,$year,'end_date');
	$from_shamsi = convertDateAndTime($from,'to_shamsi');
    $from_shamsi = explode('-',$from_shamsi);
    //shift for those who is completely off on thu
	$thu_shift = is_in_university_shift($rfid,$from_shamsi[0],$from_shamsi[1],0); // $from_shamsi index 0 is year index 1 is month
	$sun_shift = is_in_university_shift($rfid,$from_shamsi[0],$from_shamsi[1],3); // $from_shamsi index 0 is year index 1 is month
    $sat_shift = is_in_university_shift($rfid,$from_shamsi[0],$from_shamsi[1],4); // $from_shamsi index 0 is year index 1 is month 
    
	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	$day_counter=1;
	?>
	@foreach($period AS $day)
		<?php
			$the_day = $day->format( "Y-m-d" );
			$dayOfWeek = date('D',strtotime($the_day));
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
			$period_monthDay = $period_month.$period_day;
			$shamsi_day = dateToShamsi($period_det[0],$period_det[1],$period_det[2]);
			$imgin_id = '';$imgout_id = '';
			$sign_in = false;$sign_out = false;$counter=0;
			$signin_img = '/img/default.jpeg';$signout_img = '/img/default.jpeg';
			$rejected_in = 0;$rejected_out = 0;

			//leaves
			$isInLeave = isEmployeeInLeave_today($photo->id,$the_day);
		?>
		@if($details)
		<?php $night_shift_in = false; ?>
		@foreach($details as $img)
			<?php $counter++;
			$path = explode("_",$img->path);
			$time = (int)substr($path[0],-6);//get the time part in path
			$month_day = substr($path[0],-11,4);//get the monthDay part in path
			$$month_day = $night_shift_in;

			if($period_monthDay == $month_day)
			{
				//$late_status = $img->late_status;
				//$correct = true;
				if($night_shift)
				{
					if(!$$month_day)
					{
						$night_shift_in = true;
						$sign_in = true;
						if($year.$month < $img_source_date)
						{
							$signin_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signin_img = '/img/default.jpeg';
							}
							else 
							{
								$signin_img = $img_source_ip."/".$img->path;
							}
                        }
                        
                        $signin_img = encodeBasePath($signin_img); 
						$imgin_id = $img->id;
						$rejected_in = $img->status;

						//$late_status_in = $img->late_status;
					}
					else
					{
						$sign_out = true;
						if($year.$month < $img_source_date)
						{
							$signout_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signout_img = '/img/default.jpeg';
							}
							else 
							{
								$signout_img = $img_source_ip."/".$img->path;
							}
                        }
                        
                        $signout_img = encodeBasePath($signout_img); 
						$imgout_id = $img->id;
						$rejected_out = $img->status;

						//$late_status_out = $img->late_status;
					}
                }
                elseif($quarantin_shift && is_in_quarin_date($the_day))
				{
					if(!$$month_day)
					{
						$night_shift_in = true;
						$sign_in = true;
						if($year.$month < $img_source_date)
						{
							$signin_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signin_img = '/img/default.jpeg';
							}
							else 
							{
								$signin_img = $img_source_ip."/".$img->path;
							}
                        }
                        
                        $signin_img = encodeBasePath($signin_img);
						$imgin_id = $img->id;
						$rejected_in = $img->status;

						$late_status_in = $img->late_status;
					}
					else
					{
						$sign_out = true;
						if($year.$month < $img_source_date)
						{
							$signout_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signout_img = '/img/default.jpeg';
							}
							else 
							{
								$signout_img = $img_source_ip."/".$img->path;
							}
                        }
                        
                        $signout_img = encodeBasePath($signout_img);
						$imgout_id = $img->id;
						$rejected_out = $img->status;

						$late_status_out = $img->late_status;
					}
				}
				else
				{
					if($time <= 120000)
					{
						$sign_in = true;
						if($year.$month < $img_source_date)
						{
							$signin_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signin_img = '/img/default.jpeg';
							}
							else 
							{
								$signin_img = $img_source_ip."/".$img->path;
							}

                        }
                       
                        $signin_img = encodeBasePath($signin_img); 
						$imgin_id = $img->id;
						$rejected_in = $img->status;
						//$late_status_in = $img->late_status;
					}
					else
					{
						$sign_out = true;
						if($year.$month < $img_source_date)
						{
							$signout_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signout_img = '/img/default.jpeg';
							}
							else 
							{
								$signout_img = $img_source_ip."/".$img->path;
							}
                        }
                        
                        $signout_img = encodeBasePath($signout_img);;
						$imgout_id = $img->id;
						$rejected_out = $img->status;
						//$late_status_out = $img->late_status;
					}
				}

			}
			if($sign_in && $sign_out)
			{
				break;
			}
            ?>
		@endforeach

		@endif
		<!-- sign in -->
		@if($day_counter == 1 || $day_counter%11==1)
		<tr>
		@endif
		@if($the_day <= date('Y-m-d'))

		<?php
		$holiday = checkHoliday($the_day);
        $urgents = checkUrgentHoliday($the_day);
        $sign_in_other_label = false; 
		$sign_out_other_label = false; 
		?>
		<td style="position: relative;padding-bottom: 2em;padding-right: 1em;width: -moz-min-content;">
		<!-- sign in pic -->
            <div style="position: relative;">
				<!-- if there is a pic for this day then send the id to onclick function -->
				<img src="{!!$signin_img!!}" class="att_img" @if($rejected_in!=0) style="opacity: 0.5" @endif alt="عکس قابل نمایش نمیباشد" height="112" width="90" style="border-top:1px solid;border-left:1px solid;border-right:1px solid"/>
				@if($photo->att_in_date>$the_day)
					<span style="position: absolute;right: 35px;top: 20px;color:red; font-size:15px;background:#FFF;padding:0 10px">شامل سیستم نمیباشد</span>
				@else
					@if($isInLeave)
						@if($isInLeave->type==7)
                            <span class="leave_span">خدمتی</span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==9)
                            <span class="leave_span">دیگر موارد</span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==1)
                            <span class="leave_span">رخصتی ضروری</span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==2)
                            <span class="leave_span">رخصتی تفریحی</span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==3)
                            <span class="leave_span">رخصتی مریضی</span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==4)
                            <span class="leave_span">رخصتی ولادی</span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==5)
                            <span class="leave_span">رخصتی عروسی</span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==6)
                            <span class="leave_span">رخصتی حج</span>
                            <?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==8)
                            <span class="leave_span">اضافه رخصتی مریضی</span>
                            <?php $sign_in_other_label = true; ?>
                        @elseif($isInLeave->type==11)
                            <span class="leave_span">اضافه رخصتی تفریحی</a></span>
                            <?php $sign_in_other_label = true; ?>
						@endif

					@elseif($holiday)
                            <span class="leave_span" title="{!!$holiday->desc!!}">رخصتی عمومی</span>
                            <?php $sign_in_other_label = true; ?>
					@elseif($urgents)
						<span style="position: absolute;right: 20px;top: 20px;color:red; font-size:15px;background:#FFF;padding:0 10px" title="{!!$urgents->desc!!}">حالت فوق العاده</span>
					@elseif($dayOfWeek=='Fri')
                        <span style="position: absolute;right: 15px;top: 80px;color:red; font-size:15px;background:#FFF;padding:0 10px">جمعه</span>
                        <?php $sign_in_other_label = true; ?>
                    @elseif($img_source_ip=='not_ping_able' && !$sign_in_other_label && !($dayOfWeek == 'Thu' && $thu_shift) || ($dayOfWeek == 'Sun' && $sun_shift) || ($dayOfWeek == 'Sat' && $sat_shift))
					  <span style="position: absolute;right: 4px;top: 20px;font-size:15px;background:#FFF;padding:0 10px">@if($sign_in) <span style="color:green;">حاضر</span> @else <span style="color:red;"> غیر حاضر </span> @endif</span>
					@endif
					<span style="position: absolute;right: 5px;top: 10px;color:red; font-size:17px;padding:0 10px" id="{!!$imgin_id!!}_span" title="این عکس توسط مسئول حاضری رد شده است">@if($rejected_in!=0) عکس قابل قبول نمیباشد @endif</span>
				@endif
			</div>
		<!-- sign out pic -->
			<div style="position: relative;">
				<img src="{!!$signout_img!!}" class="att_img" @if($rejected_out!=0) style="opacity: 0.5" @endif alt="عکس قابل نمایش نمیباشد" height="112" width="90" style="border-bottom:1px solid;border-left:1px solid;border-right:1px solid"/>
					<span style="position: absolute;right: 0px;top: 10px;color:red; font-size:19px;padding:0 10px" id="{!!$imgout_id!!}_span" title="این عکس توسط مسئول حاضری رد شده است">@if($rejected_out!=0) عکس قابل قبول نمیباشد @endif</span>
					@if($dayOfWeek == 'Thu' && check_day_is_in_shfit($thu_shift,$shamsi_day))
						<span style="position: absolute;right: 35px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
					@elseif($dayOfWeek == 'Sun' && check_day_is_in_shfit($sun_shift,$shamsi_day))
						<span style="position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
					@elseif($dayOfWeek == 'Sat' && check_day_is_in_shfit($sat_shift ,$shamsi_day))
                      <span style="position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
                    {{-- If Image Server is not pingable and day is not friday  --}}
					@elseif($img_source_ip=='not_ping_able' && !($sign_out_other_label || $sign_in_other_label))
					   <span style="position: absolute;right: 4px;top: 20px;font-size:15px;background:#FFF;padding:0 10px">@if($sign_out) <span style="color:green;">حاضر</span> @else <span style="color:red;"> غیر حاضر </span> @endif</span>    
					@endif
			</div>
			<!--
			($dayOfWeek=='Fri' || $dayOfWeek == 'Thu')
			-->
			@if($dayOfWeek=='Fri')
			<span style="position: absolute;right: 20px;bottom: 1px;color:red">{!!$shamsi_day!!}</span>
			@else
			<span style="position: absolute;right: 20px;bottom: 1px;">{!!$shamsi_day!!}</span>
			@endif
		</td>
		@endif
		@if($day_counter%11==0)
		</tr>
		@endif

		<?php $day_counter++;?>
	@endforeach
	</table>
	</div>
</div>
<!-- End Example Tabs -->

@stop
@section('footer-scripts')
<script>
function bring_employee_images()
{
	//loader
	$('.panel-body').css('opacity','0.4');
    $('.panel-body').css('pointer-events','none');
    $('#attendance_loading').show();

	var rfid = "{!!$enc_rfid!!}";
	var year = $('#year').val();
	var month = $('#month').val();

    window.location = "/hr/myAttendance/"+rfid+"/"+year+"/"+month;

}
</script>
@stop
