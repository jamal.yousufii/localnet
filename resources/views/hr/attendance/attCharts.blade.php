@extends('layouts.master')

@section('head')
	<title>Dashboard</title>
	
@stop

@section('content')

<div class="row">
	
		<div id="chart2" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>

</div>
@stop
@section('footer-scripts')

{!! HTML::script('/js/highcharts/highcharts.js') !!}
{!! HTML::script('/js/highcharts/highcharts-3d.js') !!}
{!! HTML::script('/js/highcharts/exporting.js') !!}

<?php 

$month_days = countMonthDays1($year,$month);
$days="";
$present = '';
$absent='';

for($i=1;$i<=$month_days;$i++){
	$days .= $i.",";
	$dep_present = getDepPresentDays($dep_id,$year,$month,$i);
	$present .= $dep_present.",";
	$absent .= $dep_employees-$dep_present.",";
}

$days = substr($days,0,-1);
$present = substr($present,0,-1);
$absent = substr($absent,0,-1);

$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');
            					
?>

<script type="text/javascript">
    
    $(function () {
    $('#chart2').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '<?=$dep->name?>(<?=$month_name[$month]?> <?=$year?>)'
        },
        xAxis: {
            categories: [<?=$days?>]
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            column: {
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.y}</b>',
                    style: {color: "#D2691E"}
                }
            }
        },
        series: [{
            name: 'Present',
            data: [<?=$present?>]
        }, {
            name: 'Absent',
            data: [<?=$absent?>]
        }]
    });
});
</script>
@stop




