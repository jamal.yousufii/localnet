<div class="modal-dialog" style="width: 70%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" id="time_close_btn" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" id="salaryWait" method="post">
			    
                <div class="container-fluid">
                	<div class="row">
                	<div class="col-sm-12">
                		<div class="col-sm-3">
                			<label class="col-sm-12 "> </label>
                		</div>
                		
			      	</div>
                	<div class="col-sm-12">
                		<div class="col-sm-9">
			      			<div class="col-sm-12">
			      				<label class="col-sm-12 ">محتوی</label>
                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
                                <textarea class="form-control" row="6" col="10" id="content" required>@if($details){!!$details->content!!}@endif</textarea>
                                
			      			</div>
			      		</div>
                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
                				
                				<button class="btn btn-primary" type="button" onclick="submit_notifyEdit();$('#time_close_btn').click()"> ذخیره</button>
                			
                				<button class="btn btn-danger" type="button" onclick="$('#time_close_btn').click()"> کنسل</button>
                			</div>
                		</div>
                	</div>
                	</div>
                </div>
	            <input type="hidden" id="id" value="{!!$id!!}"/>
	            
				{!! Form::token() !!}
			</form>
			</div>
			</div>		
		</div>		                       
	</div>
</div>
<script>
function submit_notifyEdit()
{
	var content = $('#content').val();
	var id = $('#id').val();
	var page = "{!!URL::route('postNotifyEdit')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&content='+content+'&id='+id,
        success: function(r){
        	$('#content_'+id).html(r);
        }
    });
}
</script>