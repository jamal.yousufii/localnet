@extends('layouts.master')

@section('head')
    <title>{!!_('attendance')!!}</title>
   <style type="text/css">
    .att_img:hover {
    opacity: 0.5; filter: alpha(opacity=30);
}
.leave_span{
	position: absolute;right: 0px;top: 65px;color:green; font-size:15px;background:#FFF;padding:0 20px;
}
.holiday_span{
	position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 20px;
}
.panel-body{
	padding-top: 0px !important; 
}
    </style>
@stop
@section('content')

<header class="main-box-header clearfix">
	<div class="row">
    	<div class="col-lg-12">
    		<div class="container-fluid">
        		<div class="col-sm-4">
        			<div class="col-sm-12" id="img_controller">
        				<div class="icon-box pull-left">
			           		@if($previous_rfid)
			           		<?php $link = URL::route('verifyNextEmployee',array($rfid,$year,$month,'previous',$dep,$sub_dep)); ?>
							<a href="javascript:void()" onclick="$('#img_controller').html('Loading...');location.href='{!!$link!!}'" class="btn pull-left">
								<i class="icon ml-chevron_left"></i>
							</a>
							@else
							<i class="btn pull-left icon ml-chevron_left" style="cursor:default"></i>
							@endif
                            <?php
                                //Check if Image exist  
                                $imgwidth  = 80;
                                if(file_exists(public_path() . '/documents/profile_pictures/'.$photo->photo) && $photo->photo!='')
                                {
                                    $data = getimagesize('documents/profile_pictures/'.$photo->photo);
                                    $image = base64_encode(file_get_contents('documents/profile_pictures/'.$photo->photo));
                                    $width = $data[0];
                                    $height = $data[1];
                                    if($width>2200)
                                        $imgwidth  = 150;
                                }
                                else {
                                    $image = base64_encode(file_get_contents('documents/profile_pictures/default.jpeg'));
                                }
                                $profile_pic = 'data:image/jpg;base64,'.$image;							
							
								?>
								<img src="{!!$profile_pic!!}" alt="Image Not Found!" width="{!!$imgwidth!!}" hight="224"/>
    							@if($next_rfid)
								<?php $next_link = URL::route('verifyNextEmployee',array($rfid,$year,$month,'next',$dep,$sub_dep)); ?>
								<a href="javascript:void()" onclick="$('#img_controller').html('Loading...');location.href='{!!$next_link!!}'" class="btn pull-right">
									<i class="icon ml-chevron_right"></i>
								</a>
								@else
								<i class="btn pull-right icon ml-chevron_right" style="cursor:default"></i>
								@endif
						</div>
        			</div>
        		</div>
	      		<div class="col-sm-2 pt-20">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">سال</label>
                        <select name="year" id="year" class="form-control" onchange="bring_employee_images()">
                            <?php
        					//$month = explode('-',$today);
        					$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');

        					?>
                            @for($i=$year-5;$i<$year+5;$i++)
                            	@if($i==$year)
                                <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
                                @else
                                <option value='{!!$i!!}'>{!!$i!!}</option>
                                @endif
                            @endfor
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
        			</div>
        		</div>
        		<div class="col-sm-2 pt-20">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">ماه</label>

                        <select name="month" id="month" class="form-control" onchange="bring_employee_images()">
                            @for($j=1;$j<13;$j++)
                            	<?php
                            	$m = $j;
                            	if($j<10) $m='0'.$j;
                            	?>
                            	@if($m==$month)
                                <option value='{!!$m!!}' selected="selected">{!!$month_name[$j]!!}</option>
                                @else
                                <option value='{!!$m!!}'>{!!$month_name[$j]!!}</option>
                                @endif
                            @endfor
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
        			</div>
        		</div>
			    <div class="col-sm-3 pt-20">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">کارمندان</label>
                        <select id="emp_rfid" class="form-control" onchange="bring_employee_images()">
                                <option value="">انتخاب</option>
                            @foreach($employees AS $emp)
                            	@if($emp->RFID!=0)
                                	@if($emp->RFID == $rfid)
                                		<option value='{!!$emp->RFID!!}' selected>{!!$emp->name!!} {!!$emp->last_name!!}</option>
                                	@else
                                    	<option value='{!!$emp->RFID!!}'>{!!$emp->name!!} {!!$emp->last_name!!}</option>
                                    @endif
                                 @endif
                            @endforeach
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
        			</div>
        		</div>
        		<div class="col-sm-1 pt-20">
        			<div class="col-sm-12">
        				<label class="col-sm-12 ">&nbsp;</label>
        				<a href="{!!URL::route('getRelatedEmployees_get',array($dep,$sub_dep,$year,$month))!!}" class="btn btn-primary">برگشت به لیست</a>
        			</div>
        		</div>

			</div>
			<div class="container-fluid">
		      	<div class="row">
            		<div class="col-sm-3">
            			{!!$photo->current_position_dr!!}
            		</div>
            	</div>
            </div>
		</div>
	</div>
</header>

<!-- Example Tabs -->
<div class="row">
	<div class="col-lg-12">
	<table width="auto">
	<?php
	//shift for those who is completely off on thu
    $night_shift = is_in_night_shift($rfid);
    $quarantin_shift = is_in_quarantine_shift($rfid,'evening');
	$sday = att_month_days(0);
    $eday = att_month_days(1);
    // date which tak image from backup not server 
    $img_source_date = att_img_source_date();
    // ip of Image server 
    $img_source_ip = att_image_source_ip();
    $from = calculate_attendence_date($sday,$month,$year,'start_date');
	$to = calculate_attendence_date($eday,$month,$year,'end_date');
	$from_shamsi_date = convertDateAndTime($from,'to_shamsi');
    $from_shamsi = explode('-',$from_shamsi_date);
    //shift for those who is completely off on thu
    $thu_shift = is_in_university_shift($rfid,$from_shamsi[0],$from_shamsi[1],0,'compelete_month_date'); // $from_shamsi index 0 is year index 1 is month
	// ddd($thu_shift);
	$sun_shift = is_in_university_shift($rfid,$from_shamsi[0],$from_shamsi[1],3,'compelete_month_date'); // $from_shamsi index 0 is year index 1 is month
	$sat_shift = is_in_university_shift($rfid,$from_shamsi[0],$from_shamsi[1],4,'compelete_month_date'); // $from_shamsi index 0 is year index 1 is month 
     
	//$to = dateToMiladi($year,$month,$eday);//the end day of attendance month is 15 of current month
	$begin = new DateTime($from);
	$end = new DateTime($to);
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
    $day_counter=1;
	?>
	@foreach($period AS $day)
		<?php
            $the_day = $day->format( "Y-m-d" );
			$dayOfWeek = date('D',strtotime($the_day));
			$period_det = explode('-',$the_day);
			$period_day = $period_det[2];
			$period_month = $period_det[1];
            $period_monthDay = $period_month.$period_day;
            $shamsi_day = convertToShamsi($period_det[0],$period_det[1],$period_det[2]);
			$imgin_id = '';$imgout_id = '';
            $correct = false;
            $sign_in = false;
            $sign_out = false;
			$signin_img = asset('img/default.jpeg'); $signout_img =asset('img/default.jpeg');
			$rejected_in = 0;$rejected_out = 0;
			//leaves
            $isInLeave = isEmployeeInLeave_today($photo->id,$the_day);

		?>
		@if($details)
		 <?php $night_shift_in = false; ?>
		 @foreach($details as $img)
			<?php
			$path = explode("_",$img->path);
			$time = (int)substr($path[0],-6);//get the time part in path
            $month_day = substr($path[0],-11,4);//get the monthDay part in path
            $$month_day = $night_shift_in;
            
			if($period_monthDay == $month_day)
			{
				$correct = true;
				if($night_shift) // Night shift, show first picture as sign in shift last picture as sign out 
				{
					if(!$$month_day)
					{
						$night_shift_in = true;
						$sign_in = true;
						if($year.$month < $img_source_date)
						{
							$signin_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signin_img =asset('img/default.jpeg');
							}
							else 
							{
								$signin_img = $img_source_ip."/".$img->path;
							}
						}

						$imgin_id = $img->id;
						$rejected_in = $img->status;

						$late_status_in = $img->late_status;
					}
					else
					{
						$sign_out = true;
						if($year.$month < $img_source_date)
						{
							$signout_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signout_img =asset('img/default.jpeg');
							}
							else 
							{
								$signout_img = $img_source_ip."/".$img->path;
							}
						}
						//$signout_img = "http://10.134.45.19/".$img->path;

						$imgout_id = $img->id;
						$rejected_out = $img->status;

						$late_status_out = $img->late_status;
					}
                }
                elseif($quarantin_shift && is_in_quarin_date($the_day)) // Quarintin shift, evening person first picture as sign in last picture as signout 
				{
					if(!$$month_day)
					{
						$night_shift_in = true;
						$sign_in = true;
						if($year.$month < $img_source_date)
						{
							$signin_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signin_img =asset('img/default.jpeg');
							}
							else 
							{
								$signin_img = $img_source_ip."/".$img->path;
							}
						}

						$imgin_id = $img->id;
						$rejected_in = $img->status;

						$late_status_in = $img->late_status;
					}
					else
					{
						$sign_out = true;
						if($year.$month < $img_source_date)
						{
							$signout_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signout_img =asset('img/default.jpeg');
							}
							else 
							{
								$signout_img = $img_source_ip."/".$img->path;
							}
						}
						//$signout_img = "http://10.134.45.19/".$img->path;

						$imgout_id = $img->id;
						$rejected_out = $img->status;

						$late_status_out = $img->late_status;
					}
				}
				else
				{
					if($time <= 120000)
					{
						$sign_in = true;
						if($year.$month < $img_source_date)
						{
							$signin_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signin_img =asset('img/default.jpeg');
							}
							else 
							{
								$signin_img = $img_source_ip."/".$img->path;
							}
						}

						$imgin_id = $img->id;
						$rejected_in = $img->status;

						//$late_status_in = $img->late_status;
					}
					else
					{
						$sign_out = true;
						if($year.$month < $img_source_date)
						{
							$signout_img = "/att_img/upload".$img->path;
						}
						else
						{
							if($img_source_ip=='not_ping_able') //Server is not pingalbe 
							{
								$signout_img =asset('img/default.jpeg');
							}
							else 
							{
								$signout_img = $img_source_ip."/".$img->path;
							}
						}
						//$signout_img = "http://10.134.45.19/".$img->path;

						$imgout_id = $img->id;
						$rejected_out = $img->status;
						//$late_status_out = $img->late_status;
					}
				}
			}
			if($sign_in && $sign_out)
			{
				break;
			}
			?>
		 @endforeach
		@endif
		<!-- sign in -->
		@if($day_counter == 1 || $day_counter%11==1)
		<tr>
		@endif
		@if($the_day <= date('Y-m-d'))
		<?php
		$holiday = checkHoliday($the_day);
		$urgents = checkUrgentHoliday($the_day);
		$sign_in_other_label = false; 
		$sign_out_other_label = false; 
		?>
		<td style="position: relative;padding-bottom: 2em;padding-right: 1em;width: -moz-min-content;">
		<!-- sign in pic -->
			<div style="position: relative;">
				<!-- if there is a pic for this day then send the id to onclick function -->
				<img src="{!!$signin_img!!}" class="att_img" @if($rejected_in==0) onclick="reject_pic(@if($imgin_id!=''){!!$imgin_id!!}@else 'in' @endif,'{!!$the_day!!}','')" @else style="opacity: 0.5" @endif alt="عکس قابل نمایش نمیباشد" height="110" width="90" style="cursor:pointer;border-top:1px solid;border-left:1px solid;border-right:1px solid;"/>

				@if($photo->att_in_date>$the_day)
					<span style="position: absolute;right: 35px;top: 20px;color:red; font-size:15px;background:#FFF;padding:0 10px">شامل سیستم نمیباشد</span> <?php $sign_in_other_label = true; ?> 
				@else
					@if($isInLeave)
						@if($isInLeave->type==7)
							<span class="leave_span"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">خدمتی</a></span> 
							<?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==9)
							<span class="leave_span"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">دیگر موارد</a></span> 
							<?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==1)
							<span class="leave_span"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی ضروری</a></span> 
							<?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==2)
							<span class="leave_span"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی تفریحی</a></span>
							<?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==3)
							<span class="leave_span"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی مریضی</a></span>
							<?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==4)
							<span class="leave_span"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی ولادی</a></span>
							<?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==5)
							<span class="leave_span"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی عروسی</a></span>
							<?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==6)
							<span class="leave_span"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">رخصتی حج</a></span>
							<?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==8)
							<span class="leave_span"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">اضافه رخصتی مریضی</a></span>
							<?php $sign_in_other_label = true; ?>
						@elseif($isInLeave->type==10)
							<span class="leave_span">شامل کتاب</span>
							<?php $sign_in_other_label = true; ?>
                        @elseif($isInLeave->type==11)
							<span class="leave_span"><a href="{!!URL::route('getEmployeeLeaves',array($photo->id,$year,$month))!!}" target="_blank">اضافه رخصتی تفریحی</a></span>
							<?php $sign_in_other_label = true; ?>
						@endif
						 	<span style="position: absolute;right: 5px;top: 10px;color:red; font-size:17px;padding:0 10px" id="{!!$imgin_id!!}_span">@if($rejected_in!=0) عکس قابل قبول نمیباشد <?php $sign_in_other_label = true; ?> @endif</span>
					@elseif($holiday)
						<span class="holiday_span" title="{!!$holiday->desc!!}">رخصتی عمومی</span>
						<?php $sign_in_other_label = true; ?>
					@elseif($urgents)
						<span style="position: absolute;right: 0px;top: 20px;color:red; font-size:15px;background:#FFF;padding:0 10px" title="{!!$urgents->desc!!}">حالت فوق العاده</span>
						<?php $sign_in_other_label = true; ?>
					@elseif($dayOfWeek=='Fri')
						<span style="position: absolute;right: 15px;top: 80px;color:red; font-size:15px;background:#FFF;padding:0 10px">جمعه</span>
						<?php $sign_in_other_label = true; ?>
					@elseif($img_source_ip=='not_ping_able' && !$sign_in_other_label && !($dayOfWeek == 'Thu' && check_day_is_in_shfit($thu_shift,$shamsi_day) || $dayOfWeek == 'Sun' && check_day_is_in_shfit($sun_shift,$shamsi_day) || $dayOfWeek == 'Sat' && check_day_is_in_shfit($sat_shift,$shamsi_day)))
					  <span style="position: absolute;right: 4px;top: 20px;font-size:15px;background:#FFF;padding:0 10px">@if($sign_in) <span style="color:green;">حاضر</span> @else <span style="color:red;"> غیر حاضر </span> @endif</span>
					@else
						<span style="position: absolute;right: 5px;top: 10px;color:red; font-size:17px;padding:0 10px" id="{!!$imgin_id!!}_span">@if($rejected_in!=0) عکس قابل قبول نمیباشد @endif</span>
					@endif
					{{-- if image server is not pingable, check image in the table present or Obsent, Should not be in UNI shifts --}}
					
				@endif
            </div>
            <?php 
              
            ?>
		<!-- sign out pic -->
			<div style="position: relative;">
				<img src="{!!$signout_img!!}" class="att_img" @if($rejected_out==0) onclick="reject_pic(@if($imgout_id!=''){!!$imgout_id!!}@else 'out' @endif,'{!!$the_day!!}','')" @else style="opacity: 0.5" @endif alt="عکس قابل نمایش نمیباشد" height="110" width="90" style="cursor:pointer;border-bottom:1px solid;border-left:1px solid;border-right:1px solid;"/>
					<span style="position: absolute;right: 0px;top: 10px;color:red; font-size:19px;padding:0 10px" id="{!!$imgout_id!!}_span">@if($rejected_out!=0) عکس قابل قبول نمیباشد @endif</span>
					@if($dayOfWeek == 'Thu' && check_day_is_in_shfit($thu_shift,$shamsi_day))
						<span style="position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
						<?php $sign_out_other_label = true; ?>
					@elseif($dayOfWeek == 'Sun' && check_day_is_in_shfit($sun_shift,$shamsi_day))
						<span style="position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
						<?php $sign_out_other_label = true; ?>
					@elseif($dayOfWeek == 'Sat' && check_day_is_in_shfit($sat_shift,$shamsi_day))
						<span style="position: absolute;right: 0px;top: 20px;color:green; font-size:15px;background:#FFF;padding:0 10px">university</span>
						<?php $sign_out_other_label = true; ?>
					{{-- If Image Server is not pingable and day is not friday  --}}
					@elseif($img_source_ip=='not_ping_able' && !($sign_out_other_label || $sign_in_other_label))
						 <span style="position: absolute;right: 4px;top: 20px;font-size:15px;background:#FFF;padding:0 10px">@if($sign_out) <span style="color:green;">حاضر</span> @else <span style="color:red;"> غیر حاضر </span> @endif</span>
					@endif
					
			</div>

			@if($dayOfWeek=='Fri')
			<span style="position: absolute;right: 20px;bottom: 1px;color:red">{!!$shamsi_day!!}</span>
			@else
				<div id="{!!$the_day!!}_date">
					<span style="position: absolute;right: 20px;bottom: 1px;">{!!$shamsi_day!!}</span>
				</div>
			@endif
		</td>
		@endif
		@if($day_counter%11==0)
		</tr>
		@endif

		<?php $day_counter++;?>
	@endforeach
	</table>
	</div>
</div>
<!-- End Example Tabs -->

@stop
@section('footer-scripts')
<script>
function reject_pic(img_id,theday,late='')
{
	var emp_id = "{!!$rfid!!}";
	if(img_id=='in' || img_id == 'out')
	{//there is no pic, then present the default image
		return;
		var page = "{!!URL::route('presentImage')!!}";
		var span = $('#'+theday+'_'+img_id).html();
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&date='+theday+'&type='+img_id+'&present='+span+'&emp_id='+emp_id,
	        success: function(r)
	        {
				if(span != 'Present')
				{
					if(img_id=='in')
					{
						$('#'+theday+'_date span').css('color','greenyellow');
					}
					else if(img_id=='out')
					{
						$('#'+theday+'_date span').css('color','yellowgreen');
					}
					else
					{
						$('#'+theday+'_date span').css('color','dark');
					}
					$('#'+theday+'_'+img_id).html('Present');
				}
				else
				{
					$('#'+theday+'_date span').css('color','black');
					$('#'+theday+'_'+img_id).html('');
				}
	        }
	    });
	}
	else
	{//there is a pic, then reject it
		var page = "{!!URL::route('rejectImage')!!}";
		var span = $('#'+img_id+'_span').html();
		var late_status = $('#'+theday+'_'+late).html();

		$.ajax({
	        url:page,
	        type:'post',
	        data: '&img_id='+img_id+'&type='+span+'&late='+late+'&date='+theday+'&emp_id='+emp_id+'&late_status='+late_status,
	        success: function(r){
				if(late !='')
				{
					if(late_status != 'Present')
					{
						$('#'+img_id+'_span').html('');
						$('#'+theday+'_'+late).html('Present');
					}
					else
					{
						$('#'+theday+'_'+late).html('');
						$('#'+img_id+'_span').html('Late');
					}
				}
				else
				{
					if(span == '')
					{
						$('#'+img_id+'_span').html('Rejected');
					}
					else
					{
						$('#'+img_id+'_span').html('');
					}
				}
	        }
	    });
	}
}
function bring_employee_images()
{
	if($('#emp_rfid').val()!="")
	{
		var rfid = $('#emp_rfid').val();
	}
	else
	{
		var rfid = "{!!$rfid!!}";
	}

	var year = $('#year').val();
	var month = $('#month').val();
	var dep = "{!!$dep!!}";
	var sub_dep = "{!!$sub_dep!!}";

    window.location = "/hr/getEmployeeAttendance/"+rfid+"/"+year+"/"+month+"/"+dep+"/"+sub_dep;

}
</script>
@stop
