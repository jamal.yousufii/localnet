@extends('layouts.master')

@section('head')

    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">

	<form class="form-horizontal" role="form" method="post" action="{!!URL::route('getLeavesEmployees')!!}">
    <div class="col-lg-12">
      <div class="col-sm-9">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li><span>رخصتی کارمندان</span></li>
            <li class="active"><span>نتیجه جستجو</span></li>
        </ol>
      </div>
      <div class="col-sm-3">
        <button class="btn btn-primary" type="submit" title="Report"><i class="fa fa-search">جستجو </i></button>
        <button class="btn btn-primary pull-right" type="button" onclick="do_print();" title="Print Excel"><i class="fa fa-print"> راپور رخصتی سالانه</i></button>
      </div>
    </div>
</div>
<header class="main-box-header clearfix">
	<div class="row">
    	<div class="col-lg-12">

    		<div class="container-fluid">
		      	<div class="row">
		      		<div class="col-sm-2">
            			<div class="col-sm-12">
            				<label class="col-sm-12 ">سال</label>
                            <select name="year" id="year" class="form-control">
                                <?php
            					//$month = explode('-',$today);
            					$month_name = array(1=>'حمل',2=>'ثور',3=>'جوزا',4=>'سرطان',5=>'اسد',6=>'سنبله',7=>'میزان',8=>'عقرب',9=>'قوس',10=>'جدی',11=>'دلو',12=>'حوت');

            					?>
                                @for($i=$year-5;$i<$year+5;$i++)
                                	@if($i==$year)
                                    <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
                                    @else
                                    <option value='{!!$i!!}'>{!!$i!!}</option>
                                    @endif
                                @endfor
                            </select>
                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
            			</div>
            		</div>
            		<div class="col-sm-2">
            			<div class="col-sm-12">
            				<label class="col-sm-12 ">ماه</label>

                            <select name="month" id="month" class="form-control">
                                <option value="0">انتخاب</option>
                                @for($j=1;$j<13;$j++)
                                	@if($j==$month)
                                    <option value='{!!$j!!}' selected="selected">{!!$month_name[$j]!!}</option>
                                    @else
                                    <option value='{!!$j!!}'>{!!$month_name[$j]!!}</option>
                                    @endif
                                @endfor
                            </select>
                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
            			</div>
            		</div>
				        <div class="col-sm-4">
            			<div class="col-sm-12">
            				<label class="col-sm-12 ">ادارۀ عمومی</label>
                            <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                <option value="">انتخاب</option>
                                @foreach($parentDeps AS $dep_item)
                                	@if($dep_item->id == $dep)
                                		<option value='{!!$dep_item->id!!}' selected>{!!$dep_item->name!!}</option>
                                	@else
                                    	<option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                    @endif
                                @endforeach
                            </select>
                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
            			</div>
            		</div>
            		<div class="col-sm-4">
            			<div class="col-sm-12">
            				<label class="col-sm-12 ">ادارۀ مربوط</label>
                        </div>
                        <div class="col-sm-12">
                            <select class="form-control" style="width:100%;" name="sub_dep" id="sub_dep">
                            	<option value="">انتخاب</option>
                                @foreach($sub_deps AS $deps)
                                	@if($deps->id == $sub_dep)
                                		<option value='{!!$deps->id!!}' selected>{!!$deps->name!!}</option>
                                	@else
                                    	<option value='{!!$deps->id!!}'>{!!$deps->name!!}</option>
                                    @endif
                                @endforeach
                            </select>
            			</div>
            		</div>

				</div>
			</div>

		</form>
		</div>
	</div>
</header>

<div class="row">
	<div class="col-lg-12">
		<header class="main-box-header clearfix" style="margin-top:10px">

		</header>
		<div class="modal-body">
              <div class="example-wrap">

                <div class="example table-responsive">
                  <table class="table" id="list">
                    <thead>
                      	<tr>
	                        <th>#</th>
	                        <th>نام کامل</th>
	                        <th>ولد</th>
	                        <th>وظیفه</th>
	                        <th>وضعیت</th>
	                        <th>ضروری</th>
	                        <th>تفریحی</th>
	                        <th>مریضی</th>
	                        <th>ولادی</th>
	                        <th>عروسی</th>
	                        <th>حج</th>
	                        <th>اضافه مریضی</th>
	                        <th>دیگر</th>
	                        <th>مجموع</th>
	                        <th>عملیه</th>
	                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>
@stop
@section('footer-scripts')

<script type="text/javascript">
$("#sub_dep").select2();
$(document).ready(function() {
	$('#list').dataTable(
	    {
	        'sDom': 'lf<"clearfix">tip',
	        "bProcessing": true,
	        "bServerSide": true,
	        "iDisplayLength": 10,
	        "sAjaxSource": "{!!URL::route('getLeavesEmployeesData',array($dep,$sub_dep,$year,$month))!!}",
	        "language": {
	            "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
	            "zeroRecords": "ریکارد موجود نیست",
	            "info": "نمایش صفحه _PAGE_ از _PAGES_",
	            "infoEmpty": "ریکارد موجود نیست",
	            "search": "جستجو",
	            "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
	        }
	    }
	);

});
function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function do_print()
{
    var dep = {{$dep}};
    var sub_dep = {{$sub_dep}};
    var year = {{$year}};
    var month = {{$month}};

    window.location = "/hr/printEmployeeLeaves_dep/"+dep+"/"+sub_dep+"/"+year+"/"+month;

}
</script>
@stop
