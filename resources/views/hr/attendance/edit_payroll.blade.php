
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postPayroll')!!}">
			    <input type="hidden" name="employee_id" value="{!!$emp_id!!}">
			    <input type="hidden" name="year" value="{!!$year!!}">
			    <input type="hidden" name="month" value="{!!$month!!}">
			    <input type="hidden" name="dep_id" value="{!!$dep_id!!}">
			    <div class="panel-heading">
			      <h5 class="panel-title">استحقاق معاش</h5>
			    </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">اصل معاش</label>
	                                <input class="form-control" readonly type="text" value="@if($salaries){!!$salaries->main_salary!!}@endif">
	                			</div>
	                		</div>
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">مسلکی</label>
	                                <input class="form-control" readonly type="text" value="@if($salaries){!!$salaries->prof_salary!!}@endif">
	                			</div>
	                		</div>
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">کدری</label>
	                                <input class="form-control" readonly type="text" value="@if($salaries){!!$salaries->kadri_salary!!}@endif">
	                			</div>
	                		</div>
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">امتیازی</label>
	                                <input class="form-control" readonly type="text" value="@if($salaries){!!$salaries->extra_salary!!}@endif">
	                			</div>
	                		</div>
	                		
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ماکولات</label>
	                                <input class="form-control" type="text" name="makolat" value="@if($details){!!$details->makolat!!}@endif">
	                                
	                			</div>
	                		</div>
	                		<div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">کرایه موتر</label>
	                                <input class="form-control" type="text" name="car_rent" value="@if($details){!!$details->car_rent!!}@endif">
	                                
	                			</div>
	                		</div>
	                		<div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">PRR</label>
	                                <input class="form-control" type="text" name="prr" value="@if($details){!!$details->prr!!}@endif">
	                                
	                			</div>
	                		</div>
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">خطریه</label>
	                                <input class="form-control" type="text" name="khatar" value="@if($details){!!$details->khatar!!}@endif">
	                                
	                			</div>
	                		</div>
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">سایر امتیازات</label>
	                                <input class="form-control" type="text" name="other_benifit" value="@if($details){!!$details->other_benifit!!}@endif">
	                                
	                			</div>
	                		</div>
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-12">
	                		<hr>
	                		</div>
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                			<?php
	                			$total_tax = 0;
	                			if($salaries)
	                			{
		                			$extra_tax = calculateTax($salaries->extra_salary);
			                   		$main_tax = calculateTax($salaries->main_salary);
			                   		$prof_tax = calculateTax($salaries->prof_salary);
			                   		$kadri_tax = calculateTax($salaries->kadri_salary);
			                   		$total_tax = $main_tax+$extra_tax+$prof_tax+$kadri_tax;
	                			}
	                			?>
	                				<label class="col-sm-12 ">مالیات</label>
	                                <input class="form-control" type="text" readonly value="{!!$total_tax!!}">
	                                
	                			</div>
	                		</div>
	                		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">تقاعد</label>
	                                <input class="form-control" type="text" readonly value="@if($salaries){!!$salaries->retirment!!}@endif">
	                                
	                			</div>
	                		</div>
	                		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                			<?php
	                			$salary_perday = 0;//emtiazi
		                   		$main_salary_perday = 0;//asli
		                   		$prof_salary_perday = 0;//maslaki
		                   		$kadri_salary_perday = 0;//kadri
	                			$leaves = getEmployeeLeaveDays($emp_id,$year,$month);
		                   		$presents = getEmployeePresentDays($rfid,$year,$month);
		                   		$holidays = countHolidays($year,$month);//fridays and 5shanba
		                   		$paid_days = $leaves + $presents + $holidays;
		                   		$monthDays = countMonthDays($year,$month);
		                   		$unpaid_days = $monthDays - ($paid_days);
		                   		if($salaries)
		                   		{
		                			if($salaries->extra_salary!=0)
			                   		{
			                   			$salary_perday = $salaries->extra_salary/30;
			                   		}
			                   		if($salaries->main_salary!=0)
			                   		{
			                   			$main_salary_perday = $salaries->main_salary/30;
			                   		}
			                   		if($salaries->prof_salary!=0)
			                   		{
			                   			$prof_salary_perday = $salaries->prof_salary/30;
			                   		}
			                   		if($salaries->kadri_salary!=0)
			                   		{
			                   			$kadri_salary_perday = $salaries->kadri_salary/30;
			                   		}
		                   		}
		                   		$absent_deduction = round($unpaid_days*$salary_perday)+round($unpaid_days*$main_salary_perday)+round($unpaid_days*$prof_salary_perday)+round($unpaid_days*$kadri_salary_perday);
                   		
	                			?>
	                				<label class="col-sm-12 ">غیرحاضر</label>
	                                <input class="form-control" type="text" readonly value="{!!$absent_deduction!!}">
	                                
	                			</div>
	                		</div>
	                			                  	                				            				         
				      	</div>
				    </div>
				    <div class="container-fluid">
	                	<div class="row">
	                		
	                		<div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ربع معاش</label>
	                                <input class="form-control" type="text" name="robh" value="@if($details){!!$details->robh!!}@endif">
	                                
	                			</div>
	                		</div>
	                		<div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">تضمین</label>
	                                <input class="form-control" type="text" name="tazmin" value="@if($details){!!$details->tazmin!!}@endif">
	                                
	                			</div>
	                		</div>
	                		<div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ماکولات</label>
	                                <input class="form-control" type="text" name="makolat_ksor" value="@if($details){!!$details->makolat_ksor!!}@endif">
	                                
	                			</div>
	                		</div>
	                		<div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">کمیشن بانکی</label>
	                                <input class="form-control" type="text" name="bank" value="@if($details){!!$details->bank!!}@endif">
	                                
	                			</div>
	                		</div>
	                		<div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">بازگشتی</label>
	                                <input class="form-control" type="text" name="returns" value="@if($details){!!$details->returns!!}@endif">
	                                
	                			</div>
	                		</div>
	                		<div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">سایر کسرات</label>
	                                <input class="form-control" type="text" name="other_ksor" value="@if($details){!!$details->other_ksor!!}@endif">
	                                
	                			</div>
	                		</div>	                  	                				            				         
				      	</div>
				    </div>
				    <div class="container-fluid">
	                	<div class="row">
				    		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				<button class="btn btn-primary" type="submit"> ذخیره</button>
	                			</div>
	                		</div>
	                	</div>
	                </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
