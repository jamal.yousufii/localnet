
@if($details)
	<?php $i=1;?>
	@foreach($details AS $row)
	<div class="col-sm-12" id="h_{!!$i!!}">
		<div class="col-sm-4">
        	<div class="col-sm-12">
	    		<label class="col-sm-12 ">تاریخ</label>
	    		<?php if($row->date != null){$sdate = explode('-',$row->date);} ?>
	    		<input class="form-control datepicker_farsi" value="@if($row->date!=null){!!jalali_format(dateToShamsi($sdate[0],$sdate[1],$sdate[2]))!!}@endif" required readonly type="text" name="date_<?=$i?>">
	    	</div>
        </div>
        <div class="col-sm-6">
        	<div class="col-sm-12">
        		<label class="col-sm-12 ">توضیحات</label>
        		<input class="form-control" type="text" value="{!!$row->desc!!}" name="desc_<?=$i?>">
        	</div>
        </div>
        <div class="col-sm-2">
        	<div class="col-sm-12">
        		<label class="col-sm-12 ">&nbsp;</label>
        		<input class="btn btn-danger" type="button" value=" - " onclick="remove_holiday_date(<?=$i?>)">
        	</div>
        </div>
    </div>
	
    <?php $i++;?>
	@endforeach
@endif
<script type="text/javascript">
    $(".datepicker_farsi").persianDatepicker();
    
</script>