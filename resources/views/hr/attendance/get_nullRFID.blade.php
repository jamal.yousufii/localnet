@extends('layouts.master')

@section('head')
    <title>All Employees</title>
    {!! HTML::style('/css/template/libs/nifty-component.css') !!}
@stop
@section('content')
<div class="row">
@if(Session::has('success'))
	<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
@elseif(Session::has('fail'))
	<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
	<form class="form-horizontal" role="form" id="att_report" method="post">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>NULL RIFID</span></li>
        </ol>
<br><br>
    <div class="col-lg-12">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4">
                    <div class="col-sm-12">
                        <label class="col-sm-12 ">ادارۀ عمومی</label>
                        <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                <option value="">انتخاب</option>
                            @foreach($parentDeps AS $dep_item)
                                <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                            @endforeach
                        </select>
                            <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="col-sm-12">
                        <label class="col-sm-12 control-label">ادارۀ مربوط</label>
                    </div>
                    <div class="col-sm-12">
                        <select style="width:100%;" class="form-control" name="sub_dep" id="sub_dep">
                            <option value='0'>همه</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                <button class="btn btn-primary pull-right" type="button" onclick="getSearchResult('all')">جستجو</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12" id="all">
        <div class="main-box">

            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='list'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>نام کامل</th>
                        <th>ولد</th>
                        <th>شماره تعینات</th>
                        <th>جنسیت</th>
                        <th>سکونت اصلی</th>
                        <th>RFID</th>
                        <th>عملیه</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            </div>
        </div>
    </div>
</div>



<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')
<script type="text/javascript">
$("#sub_dep").select2();
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    $(document).ready(function() {
        $('#list').dataTable(
            {
                "fnDrawCallback" : function() {
                    $(document).trigger('doc:updated');
                },
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getEmployeeRFIDData')!!}",
                "aaSorting": [[ 1, "desc" ]],
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });



</script>
@stop
