<div class="main-box">

    <div class="main-box-body clearfix">
        <div class="table-responsive">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>نام کامل</th>
                    <th>ولد</th>
                    <th>شماره تعینات</th>
                    <th>جنسیت</th>
                    <th>سکونت اصلی</th>
                    <th>رتبه</th>
                    <th>تاریخ تقرر</th>
                    <th>نوعیت کارکن</th>
                    <th>عملیه</th>
                </tr>
                </thead>
                <tbody>
                @if($records)
                    @foreach($records AS $row)
                        <tr>
                            <td>{!!$row->id!!}</td>
                            <td>{!!$row->name!!}</td>
                            <td>{!!$row->father_name!!}</td>
                            <td>{!!$row->number_tayenat!!}</td>
                            <td>{!!$row->gender!!}</td>
                            <td>{!!$row->original_province!!}</td>
                            <td>{!!$row->rank!!}</td>
                            <td>{!!$row->emp_date!!}</td>
                            <td>{!!trans('static.employee_type_'.$row->employee_type)!!}</td>
                            <td>Test</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>

    </div>
</div>
