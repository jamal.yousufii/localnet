@extends('layouts.master')

@section('head')
    <title>Employee Card Details</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
    <link href="{{ asset('crapper/cropper.min.css') }}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
    <style>
    /* create file  */
    .custom-file-input {
        display: none;
    }
    #cropper-image{
      display: none
    }

    .cropper-wrapper{
      display: none;
      height:400px;
      width:800px;
    }

    form {
    max-width: 900px;
    display: block;
    margin: 0 auto;
    }
    
    </style>
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active"><span>معلومات RFID کارمند</span></li>
        </ol>
    </div>
</div>

    <div class="row">

        <div class="col-lg-12">
            <div class="main-box">
                <div class="main-box-body clearfix">
                    <form class="form-horizontal card-form" role="form" method="post" action="{!!URL::route('postEmployeeCard',$row->id)!!}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">نام و تخلص به دری</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="name_dr" value="{!!$row->name_dr.' '.$row->last_name!!}" disabled>
                                    <span style="color:red">{!!$errors->first('name_dr')!!}</span>
                                </div>
                                <label class="col-sm-2 control-label">نام کامل به انگلیسی</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="name_en" value="{!!$row->name_en!!}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">وظیفه فعلی </label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="current_position_dr" value="{!!$row->current_position_dr!!}" disabled>
                                    <span style="color:red">{!!$errors->first('current_position_dr')!!}</span>
                                </div>
                                <label class="col-sm-2 control-label">اداره به دری</label>
                                <div class="col-sm-4">
                                    <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep_div',this.value)" disabled>
                                        <!-- <option value="">انتخاب</option> -->
                                        @foreach($parentDeps AS $dep_item)
                                            <option <?php echo ($row->department==$dep_item->id ? 'selected':'');?> value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">RFID</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="rfid" value="{!!$row->id!!}" disabled>
                                    <span style="color:red">{!!$errors->first('rfid')!!}</span>
                                </div>
                                <label class="col-sm-2 control-label">تاریخ</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" id="date_id" name="date">
                                </div>
                            </div>
                        </div>

                        
                            <div class="row">
                                <div class="form-group"  type id="otherFieldGroupDiv">
                                    <label class="col-sm-2 control-label"   for="currentImagePath">عکس صبح</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="text" name="imagepth" id="currentImagePath1" value="" ><br>
                                        <img src="http://10.10.0.201//OCN6183060823/2019/0626/080914_8358555.jpg" class="rounded" alt="Cinque Terre" width="200" height="200"> 
                                    </div>
                                    <label class="col-sm-2 control-label" for="chnageImagePath">عکس شام</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="text" id="chnageImagePath1" name="changePath"><br>
                                        <img src="http://10.10.0.201//OCN6183060823/2019/0626/080914_8358555.jpg" class="rounded" alt="Cinque Terre" width="200" height="200">
                                    </div>
                                </div>
                             </div>
                        <br>
                        {!! Form::token() !!}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10">
                                <button class="btn btn-primary" type="submit">ثبت تغییرات کارت</button>
                                <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
                            </div>
                        </div>
                    </form>

                    <!-- card print preview -->
                </div>
            </div>
        </div>
    </div>


@stop


@section('footer-scripts')
    {!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}
    <script src="{!!asset('crapper/cropper.min.js')!!}" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
       $(document).ready(function () {
         function readURL(input) {
           if (input.files && input.files[0]) {
             var reader = new FileReader();

             reader.onload = function (e) {
               $('.cropper-view-box').html('');
               $('#cropper-image').attr('src', e.target.result);
               $('.cropper-hide').attr('src', e.target.result);
               $('.cropper-wrapper').css('display','block');
               cropper();
               // FormImageCrop.init();
             }

             reader.readAsDataURL(input.files[0]);
           }
         }


         $("#customFile").change(function(){
           readURL(this);
         });
         // image cropper script end
         // 	//cropper
         function cropper() {
           const image = document.getElementById('cropper-image');
           const cropper = new Cropper(image, {
             aspectRatio: 25 / 35,
             autoCropArea: 0,
              strict: false,
              guides: false,
              highlight: false,
              dragCrop: false,
              cropBoxMovable: true,
              cropBoxResizable: false,

             crop(event) {
               $('#crop_x').attr('value', Math.ceil(event.detail.x));
               $('#crop_y').attr('value', Math.ceil(event.detail.y));
               $('#crop_w').attr('value', Math.ceil(event.detail.width));
               $('#crop_h').attr('value', Math.ceil(event.detail.height));
             },
           });
         }

        });
    </script>

<script type="text/javascript">


    $("#ready_to_print").on('change',function(){
        var ready = this.checked;
        var value= 0;
        if(ready)
        {
            value = 1;
        }

        $.ajax({
            url:'{!!URL::route("checkCardReady")!!}',
            type:'post',
            data:'ready='+value+'&id={!!$row->id!!}',
            dataType:'json',
            // success:function(r)
            // {
            //     alert(r);
            // }
        });
    });

    $("#btn_preview").on('click',function(){
        var w = window.open("{!!URL::route('getCardTemplate',$row->id)!!}");
    });

    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringRelatedSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }

    // Tayyeb
    $('#otherFieldGroupDiv').hide();
    $("#date_id").change(function() {
        if($(this).val()==''){
            $('#otherFieldGroupDiv').hide();
        }else{    
        $('#otherFieldGroupDiv').show();
        $('#currentImagePath').attr('required', '');
        $('#currentImagePath1').attr('data-error', 'This field is required.');
        $('#chnageImagePath').attr('required', '');
        $('#chnageImagePath1').attr('data-error', 'This field is required.');
        }
    });
    $("#seeAnotherFieldGroup").trigger("change");



</script>

@stop

