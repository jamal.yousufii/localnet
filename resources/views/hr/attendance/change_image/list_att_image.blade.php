@extends('layouts.master')

@section('head')

{!! HTML::style('/css/tasks/persian_datepicker.css') !!}
{!! HTML::script('/js/tasks/persian_datepicker.js')!!}
{!! HTML::style('/vendor/select2/select2.css') !!}
{!! HTML::script('/js/vendor/select2/select2.min.js')!!}
{!! HTML::script('/js/components/select2.js')!!}
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>عکس ها </span></li>
        </ol>
    </div>
</div>

<!-- Example Tabs -->
<div class="example-wrap">
	<div class="nav-tabs-horizontal">
	  <div class="tab-content padding-top-20">
	    <div class="tab-pane active" id="all" role="tabpanel">
	    	<div class="row">
			    <div class="col-lg-12">
			        <div class="main-box">
			            <div class="main-box-body clearfix">
			            <div class="table-responsive">
			                <table class="table table-responsive" id='list'>
			                    <thead>
			                    <tr>
			                        <th>#</th>
			                        <th>نام کامل</th>
			                        <th>ولد</th>
			                        <th>وظیفه</th>
			                        <th>ریاست</th>
			                        <th>عملیه</th>
			                    </tr>
			                    </thead>
			                    <tbody>
			                    </tbody>
			                </table>
			            </div>
			        
			            </div>
			        </div>
			    </div>
			</div>
	    </div> 
	  </div>
	</div>
</div>
<div class="md-overlay"></div>
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {
				"order": [[ 1, 'asc' ]],
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getEmployeeAttImageData')!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });

function load_employees(div)
{
    var page = "{!!URL::route('loadEmployees')!!}";
    
    $.ajax({
        url: page,
        type: 'post',
        data: '&type='+div,
        //dataType:'HTML',
        success: function(r)
      	{
        	$('#other').html(r);
        }
    });   
}

</script>
@stop
