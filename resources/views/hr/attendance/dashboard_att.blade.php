@extends('layouts.master')

@section('head')
	<title>Dashboard</title>
	
@stop

@section('content')

<div class="modal-body">
	<div class="row">
		<div class="col-md-4">
			<div class="main-box clearfix">
                <header class="main-box-header clearfix">
                    <h2>{!!Auth::user()->first_name.' '.Auth::user()->last_name!!}</span></h2>
                </header>
                <div class="main-box-body clearfix">
                    <?php 
                        //$photo = getProfilePicture(Auth::user()->id);
                        $photo = DB::connection('hr')->table('employees')->where('id',Auth::user()->employee_id)->pluck('photo');
                        if(file_exists(public_path() . '/documents/profile_pictures/'.$photo) && $photo!='')
                        {
                             $image = base64_encode(file_get_contents('documents/profile_pictures/'.$photo ));
                        }
                        else {
                             $image = base64_encode(file_get_contents('documents/profile_pictures/default.jpeg'));
                        }
                        $profile_pic = 'data:image/jpg;base64,'.$image;
                    ?>
                    <div id="user_profile_picture">
                        <div id="hr_profile">
                        
                        <img src="{!!$profile_pic!!}" alt="" class="profile_pic" style="width:220px;border-radius:10px;padding:1px;border:1px solid #021a40;">
                       	</div>
                   	</div>
                </div>
	        </div>
	    </div>
	    <div class="col-md-4">
            <div class="main-box clearfix">
                
                <div class="main-box-body clearfix">
                    
                    <div class="profile-details">
                        
                            <label style="font-weight:bold;padding:5px;">Username:</label> <span>{!!Auth::user()->username!!}</span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">Full Name:</label> <span>{!!Auth::user()->first_name.' '.Auth::user()->last_name!!}</span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">Father Name:</label> <span>{!!Auth::user()->father_name!!}</span>
                            
                            <br>
                            <label style="font-weight:bold;padding:5px;">Position:</label> <span>{!!getPositionNameHR(Auth::user()->id)!!}</span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">Department:</label> <span>{!!getDepartmentName(Auth::user()->department_id)!!}</span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">Email:</label> <span>{!!Auth::user()->email!!}</span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">Phone:</label> <span>{!!getEmployeePhone(Auth::user()->id)!!}</span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">User Type:</label> 
                            <span>
                            @if(Auth::user()->position_id==1) Normal User
                            @elseif(Auth::user()->position_id==2) Director
                            @elseif(Auth::user()->position_id==3) Deputy
                            @endif
                            </span>
                            <br>
                            <label style="font-weight:bold;padding:5px;">Status:</label> 
                            <span>
                                @if(Auth::user()->status==1)
                                    <i class="fa fa-check" style="color:#8bc34a;"></i> Active
                                @else
                                    <i class="fa fa-cancel" style="color:red;"></i> Deactivated
                                @endif
                            </span>
                        
                            
                    </div>
                    
                </div>
                
            </div>
        </div>
	    <div class="col-md-4">
	        <div class="main-box clearfix" >
	            <div class="main-box-body clearfix">
	                <div class="profile-details">
	                <?php $date = dateToShamsi(date('Y'),date('m'),date('d')); 
	                $sh_date = explode('-',$date); $year = $sh_date[0];
                    $total = getEmployeeLeaves_total(Auth::user()->employee_id,$year,0);
	                ?>   
	                        <label style="font-weight:bold;padding:5px;">مجموع رخصتی های سال {!!$year!!}:</label> 
	                        <span style="color: red;">{!!$total!!}</span>
	                        <br>
	                        <label style="font-weight:bold;padding:5px;">مجموع رخصتی های ضروری(۱۰):</label> 
	                        <span style="color: red;">{!!getEmployeeLeaves(Auth::user()->employee_id,1,$year,0)!!}</span>
	                        <br>
	                        <label style="font-weight:bold;padding:5px;">مجموع رخصتی های تفریحی(۲۰):</label> 
	                        <span style="color: red;">{!!getEmployeeLeaves(Auth::user()->employee_id,2,$year,0)!!}</span>
	                        <br>
	                        <label style="font-weight:bold;padding:5px;">مجموع رخصتی های مریضی(۲۰):</label> 
	                        <span style="color: red;">{!!getEmployeeLeaves(Auth::user()->employee_id,3,$year,0)!!}</span>
                            <br>
                            @if(getUserDepType(Auth::user()->id)->dep_type==2)
    	                        <label style="font-weight:bold;padding:5px;font-size: 20px;font-family: 'B Nazanin';"><a href="{!!URL::route('view_leaveForm')!!}" target="_black">دانلود فورم رخصتی</a></label> 
                                {!! HTML::image('/documents/hr_attachments/leave_form.jpg', '', array('class' => 'img-thumbnail','width'=>100,'height'=>100)) !!}
                                <br>
                                
                                @if($total>=50)
                                <label style="font-weight:bold;padding:5px;font-size: 20px;font-family: 'B Nazanin';"><a href="javascript:void()" style="color: red !important;" onclick="alert('رخصتی های شما تکمیل گردیده است!!!!')">
                                
                                شما درخواست رخصتی نمیتوانید
                                </a></label>
                                @else
                                <label style="font-weight:bold;padding:5px;font-size: 20px;font-family: 'B Nazanin';"><a href="{!!URL::route('leaveForm_add')!!}">
                                درخواست رخصتی
                                </a></label>
                                @endif
                                
                            @else
                                <!--
                                @if($total>=50)
                                <label style="font-weight:bold;padding:5px;font-size: 20px;font-family: 'B Nazanin';"><a href="javascript:void()" style="color: red !important;" onclick="alert('رخصتی های شما تکمیل گردیده است!!!!')">
                                
                                شما درخواست رخصتی نمیتوانید
                                </a></label>
                                @else
                                <label style="font-weight:bold;padding:5px;font-size: 20px;font-family: 'B Nazanin';"><a href="{!!URL::route('leaveForm_add')!!}">
                                درخواست رخصتی
                                </a></label>
                                @endif
                                --> 
                            @endif
	                </div>
	            </div>
	       </div>
	    </div>
	</div>
    <div class="row">
        
        <div class="col-sm-12" style="margin-bottom: 15px;direction: rtl;">
            <h4 style="color: red !important;font-family: 'B Nazanin';">
            کارمندانی که یازده ماه مکمل از تقررشان سپری شده باشد مستحق ۲۰ روز رخصتی تفریحی میباشند.
            </h4>
        </div>
        
    </div>
</div>

@stop





