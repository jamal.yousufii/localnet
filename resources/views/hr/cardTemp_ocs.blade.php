<html>
	<head>
		<meta charset="UTF-8">
		<style type="text/css">
			* {line-height: 1em;
				padding: 0px;
				margin: 0px;
				padding-bottom: 1px;
				padding-right: 1px;
			}

			.front{
				height: 8.5525cm;
				width:5.4cm;
				flo/at: left;
				position:relative;
				font-family: 'Bahij Zar';

				overflow: hidden;

			}

			.back{
				height: 8.5525cm;
				width:5.4cm;
				flo/at: right;
				position:relative;
				font-family: 'Times New Roman';
				overflow: hidden;
			}

	        .front img {
	            height:8.5725cm;
				width:5.4cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .back img {
	            height:8.5725cm;
				width:5.4cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .do-print{
	        	font-family: 'B Nazanin';
            	font-size: 12pt;
            	color: white;
	        }
	        .front .serial{
	        	position: absolute;
	        	z-index: 10;
	        	font-size: 7pt;
	        	right:1.6cm;
	        	top:7.45cm;
	        	color: black;

	        }

	        .front .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	left:1.6cm;
	        	top: 8.15cm;
	        	font-size: 7pt;

	        	color: white;

	        }

	        .front .photo {
	        	position: absolute;
	        	width: 1.7cm;
	        	height: 1.86cm;
	        	top: 3.47cm;
	        	left: 0.5cm;
	        	z-index: 10;
	        }

	        .front .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 5.3cm;

	        	width: 5.4cm;
	        	text-align: center;
	        	font-size: 12pt;
	        	font-weight: bold;
	        	color: black;
	        }

	        .front .title_value{
	        	position: abs/olute;
	        	z-index: 10;
	        	top: 5.8cm;
	        	text-align: center;
	        	width: 5.3cm;
	        	padding-bottom: 3px;
	        	font-size: 9pt;
	        	font-weight: bold;
	        	color: black;

	        }

	        .front .dep_value{
	        	position: abs/olute;
	        	z-index: 10;
	        	top: 6.2cm;
	        	text-align: center;
	        	width: 5.3cm;
	        	padding-bottom: 3px;
	        	font-size: 8pt;
	        	font-weight: bold;
	        	display: inline-block;
	        	color: black;
	        }

	        .front .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 7.7cm;
	        	right: 1.6cm;

	        	font-size: 9pt;

	        	color: black;

	        }

	        .front .group {
	        	position: absolute;
	        	z-index: 10;
	        	top: 5.8cm;
	        	text-align: center;

	        }
	        .back .serial{
	        	position: absolute;
	        	z-index: 10;
	        	font-size: 7pt;
	        	left:2.2cm;
	        	top:7.45cm;
	        	color: black;
	        }

	        .back .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	right:1.27cm;
	        	top:8.2cm;
	        	font-size: 7pt;

	        	color: white;

	        }


	        .back .photo {
	        	position: absolute;
	        	width: 1.7cm;
	        	height: 1.86cm;
	        	top: 3.46cm;
	        	right: 0.32cm;
	        	z-index: 10;
	        }

	        .back .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 5.3cm;
	        	width: 5.4cm;
	        	text-align: center;
	        	font-size: 10pt;
	        	font-weight: bold;
	        	color: black;

	        }

	        .back .title_value{
	        	position: abso/lute;
	        	z-index: 10;

	        	text-align: center;
	        	width: 5.3cm;
	        	padding-bottom: 4px;
	        	font-size: 9pt;

	        	color: black;

	        }

	        .back .dep_value{
	        	position: abs/olute;
	        	z-index: 10;

	        	text-align: center;
	        	width: 5.3cm;
	        	padding-bottom: 4px;
	        	font-size: 8pt;

	        	display: inline-block;
	        	color: black;
	        }

	        .back .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 7.7cm;
	        	left: 2.2cm;

	        	font-size: 10pt;

	        	color: black;

	        }
	        .back .group {
	        	position: absolute;
	        	z-index: 10;
	        	top: 5.7cm;
	        	text-align: center;

	        }
	        <?php
	        if($row->position_id==4 || $row->position_id == 5)
	        	$card_id = 'head.jpg';
	        elseif($row->position_id==6)
	        	$card_id = 'employee.jpg';
	        elseif($row->position_id==3)
	        	$card_id = 'director.jpg';
	        elseif($row->position_id==8)
	        	$card_id = 'deputy.jpg';
	        elseif($row->position_id==7)
	        	$card_id = 'ajir.jpg';
	        else
	        	$card_id = 'employee.jpg';
	        ?>
	        .new-card-front {
			  background-image: url('{!! asset('img/ocs_card_front.jpg') !!}');
			  background-size: 5.5cm 8.5525cm;


			}
			.new-card-back {
			  background-image: url('{!! asset('img/ocs_card_back.jpg') !!}');
			  background-size: 5.6cm 8.5525cm;
			  height: 8.5525cm;

			}
		</style>

	</head>
	<?php
		//$img = Image::make(file_get_contents('documents/profile_pictures/'.$row->photo ))->crop(1478, 1797);
		$img = Image::make(file_get_contents('documents/profile_pictures/'.$row->photo ));
		$img->encode('jpg');
		$type = 'jpg';
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);

	?>
	<body>
		<div class="do-print">
	        <div class="row">
	           <div class="col front new-card-front" dir='rtl'>
	           	<!-- {!!$row->eid!!} -->
	           		<?php
	           			//$position_ids = array(1,2,3,11);
	           			//$position_ids = array(11,3,17,18);
	           			$position_ids = array(11,17,18);

	           			$dep_id = $row->sudep_id;
	           			$dep_code = $dep_id;
	           			if($dep_id<10)
	           			{
	           				$dep_code = '00'.$dep_id;
	           			}
	           			elseif($dep_id < 100)
	           			{
	           				$dep_code = '0'.$dep_id;
	           			}


	     				$emp_id = $row->eid;
	     				$emp_code = $emp_id;

	     				if($emp_id<10)
	     				{
	     					$emp_code = '000'.$emp_id;
	     				}
	     				elseif($emp_id < 100)
	     				{
	     					$emp_code = '00'.$emp_id;
	     				}
	     				elseif($emp_id <1000)
	     				{
	     					$emp_code = '0'.$emp_id;
	     				}

	           		?>

	           	<img src="{!!$base64!!}" class="photo">
	           	<div class="name_value">{!!$row->name_dr!!} {!!$row->last_name!!}</div>
	           	<div class="group">
		           	<table cellspacing=0 cellpadding=0>

		           		<tr>
		           			<td>
		           				<div class="title_value">{!!$row->current_position_dr!!}</div>

		           			</td>
		           		</tr>

		           		<tr>
		           			<td>
		           				<div class="dep_value">

		           			<?php

		           				if(in_array($row->position_id, $position_ids))
		           				{
		           					if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58)
		           					{//frist and second deputy
		           						echo $row->department_dr;
		           					}
		           					else
		           					{
		           						echo $row->general_department_dr;
		           					}
		           				}
		           				else
		           				{
		           					echo $row->department_dr;
		           				}

		           			//echo $row->position_id;
		           			?>

		           				</div>

		           			</td>
		           		</tr>
		           	</table>
	      	   	</div>

	           	<div class='serial'>: {!!$dep_code!!}{!!$emp_code!!}</div>
	           	<div class="validaty_date">  ۳۰ قوس ۱۳۹۹ </div>
	           	<div class="blood_value">: {!!$row->blood_group!!}</div>
	           </div>

	           	<div class="col back new-card-back">
	           		<img src="{!!$base64!!}" class="photo">

		       		<div class="name_value">{!!$row->name_en!!}</div>
		       		<div class="group">
			           	<table cellspacing=0 cellpadding="0" style="line-height: 1em;">
			           		<tr>
			           			<td>
			           				<div class="title_value">{!!$row->current_position_en!!}</div>

			           			</td>
			           		</tr>
			           		<tr>
			           			<td>
			           				<div class="dep_value">
			           			<?php

			           				if(in_array($row->position_id, $position_ids))
			           				{
			           					if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58)
			           					{//frist and second deputy
			           						echo $row->department_en;
			           					}
			           					else
			           					{
			           						echo $row->general_department_en;
			           					}
			           				}
			           				else
			           				{
			           					echo $row->department_en;
			           				}

			           			//echo $row->department_en;
			           			?>
			           				</div>
			           			</td>
			           		</tr>
			           	</table>
	      	   		</div>


	           		<div class='serial'>: {!!$dep_code!!}{!!$emp_code!!}</div>
		           	<div class="blood_value">: {!!$row->blood_group!!}</div>
		           	<div class="validaty_date">: 20 Dec 2020</div>

	           </div>

	        </div>
        </div>


	</body>
</html>
