@extends('layouts.master')

@section('head')
	<title>Dashboard</title>
	
@stop

@section('content')

<div class="row">
	<div class="col-md-6">
		<div id="chart1" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
	</div>
	
	<div class="col-md-6">
		<div id="chart3" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
	</div>
	<div class="col-md-12">
		<div id="chart2" style="min-width: 300px; height: 400px; margin: 0 auto;padding:20px"></div>
	</div>
	
</div>
@stop
@section('footer-scripts')

{!! HTML::script('/js/highcharts/highcharts.js') !!}
{!! HTML::script('/js/highcharts/highcharts-3d.js') !!}
{!! HTML::script('/js/highcharts/exporting.js') !!}

<script type="text/javascript">
	<?php
        $last = count($emp_degrees);
        $i = 0;
        $dg='';
        foreach($emp_degrees as $a)
        {
            $i++;
            if($a->education_degree!='')
            {
            	$dg.= '{name:"'.$degrees[$a->education_degree].'", y:'.$a->total.'}';
            }
            else
            {
            	$dg.= '{name:"NA", y:'.$a->total.'}';
            }
            if($i < $last)
            {
                $dg.= ',';
            }
        }
        
    ?>
   
    // Build the chart
    $('#chart1').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'سطح تحصیلات کارمندان'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
        },
        plotOptions: {
            pie: {
            	
                depth: 45,
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>:% {point.percentage:.1f} '
                }
            }
        },
     
        series: [{
            name: 'Number',
            data: [
                <?=$dg;?>
            ]
        }]
    });
    
    $(function () {
    // Create the chart
    $('#chart2').highcharts({
        chart: {
            type: 'column',
            
        },
        title: {
            text: 'آمار کلی کارمندان'
        },
        // subtitle: {
        //     text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'
        // },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
            	innerSize: 50,
                depth: 0,
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            pointFormat: '{series.name}: <b>% {point.percentage:.1f}</b>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'مامور',
                y: <?=$types[1]?>
            }, {
                name: 'اجیر',
                y: <?=$types[2]?>
            }, {
                name: 'نظامی',
                y: <?=$types[3]?>
            }, {
                name: 'هیچکدام',
                y: <?=$types[0]?>
            }]
        }]
    });
});

	$(function () {
    $('#chart3').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'کارمندان به اساس جنسیت'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>:% {point.percentage:.1f}'
                }
            }
        },
        series: [{
        	innerSize: 100,
            type: 'pie',
            name: 'Number',
            data: [
            	
                ['زن', <?=$gender['F']?>],
                {
                	sliced: true,
                	selected: true,
                	name: "مرد",
                	y: <?=$gender['M']?>
                }
            ]
        }]
    });
});
</script>
@stop




