<div class="container-fluid" id="targetDiv_{!!$number!!}">
    <div class="row">
        <div class="col-sm-2">
            <div class="col-sm-12">
                <label class="col-sm-12 ">سویه تحصیلی</label>
                <select name = "education_degree[]" class="form-control">
                    <option value=''>انتخاب</option>
                    {!!getStaticDropdown('education_degree',0)!!}
                </select>
            </div>
        </div>

        <div class="col-sm-2">
            <div class="col-sm-12">
                <label class="col-sm-12 ">رشته تحصیلی</label>
                <input class="form-control" type="text" name="education_field[]" value="">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="col-sm-12">
                <label class="col-sm-12 ">موسسه تحصیلی</lable>
                <input type="text" class="form-control" name="education_place[]" value=""/>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="col-sm-12">
                <label class="col-sm-12 ">محل تحصیل</label>
                <select name = "edu_location[]" class="form-control">
                    <option value=''>انتخاب</option>
                    {!!getStaticDropdown('countries',0)!!}
                </select>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="col-sm-12">
                <label class="col-sm-12 ">سال فراغت</label>
                <?php
                        $current_year = date('Y')-621;

                ?>
                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
                <select name = "graduation_year[]" class="form-control">
                    <option value=''>انتخاب</option>
                    <?php

                    for($i=$current_year-84;$i<=$current_year;$i++)
                    {
                        echo "<option value='".$i."'>".$i."</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="col-sm-12" style="margin-top: 23px">
                <input type="button" class="btn btn-danger" id="btn_{!!$number!!}"  value="-" onclick="remove_moreAttachments('targetDiv_{!!$number!!}','{{$number}}')">
            </div>
        </div>
    </div>
</div>