<div class="row">
	<div class="col-lg-12">
		@if(canAdd('hr_recruitment'))
		<header class="main-box-header clearfix">
		    <h2>
		    	<a href="javascript:void()" onclick="load_service_employee({!!$id!!})" class="btn btn-primary pull-right" data-target="#change_employee" data-toggle="modal">
					<i class="fa fa-plus-circle fa-lg"></i>
				</a>
		        
		    </h2>
		</header>
		@endif
		<div class="modal-body">
              <div class="example-wrap">
              
                <div class="example table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>عنوان بست</th>
                        <th>اداره عمومی</th>
                        <th>اداره مربوطه</th>
                        <th>تاریخ</th>
                      </tr>
                    </thead>
                    <tbody>
                    @if($details)
                    <?php $i = 1; ?>
                   		@foreach($details AS $row)
                   		<tr>
                    	<td>{!!$i!!}</td>
                    	<td>{!!$row->position_title!!}</td>
                    	<td>{!!$row->dep_name!!}</td>
                    	<td>{!!$row->dep_name!!}</td>
                    	<td>{!!$row->service_date!!}</td>
                    	</tr>
                    <?php $i++; ?>
                    	@endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>
<!-- Modal End -->
<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>

<div class="md-overlay"></div><!-- the overlay element -->
<script>
function load_service_employee(id)
{
	var page = "{!!URL::route('load_service_employee')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id,
        success: function(r){
			$('#change_employee').html(r);
        }
    });
}
</script>