
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('tanqisDate')!!}" enctype="multipart/form-data">
			 
	            <div class="panel-heading">
			      <h5 class="panel-title">جزییات</h5>
			    </div>
			    <div class="container-fluid">
			    	<div class="row">
			    		<div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ</label>
                        		<?php $sdate = $row->tanqis_date; if($sdate!='0000-00-00' && $sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                                <input class="form-control datepicker_farsi" readonly type="text" name="date" value='<?php if($row->tanqis_date!="0000-00-00" && $row->tanqis_date !=null){echo jalali_format($sdate);}?>'>
                        	</div>
                        </div>
                        <div class="col-sm-4">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">شماره حکم</label>
                        		<input class="form-control" type="text" name="number" value='<?=$row->tanqis_no?>'>
                        	</div>
                        </div>
                        <div class="col-sm-4">
			      			<div class="col-sm-12">
			      				<label class="col-sm-12 ">موقف کارکن</label>
                                <select name="mawqif_employee" class="form-control" required="required">
                                    <option value=''>انتخاب</option>
                                    <option value='2' <?php echo ($row->mawqif_employee=='2' ? 'selected':''); ?>>انتظار بامعاش</option>
                                    <option value='7' <?php echo ($row->mawqif_employee=='7' ? 'selected':''); ?>>اضافه بست</option>
                                    <option value='7' <?php echo ($row->mawqif_employee=='10' ? 'selected':''); ?>>انتظار بدون معاش</option>
                                    
                                </select>
                               
			      			</div>
			      		</div>	
			    	</div>
			    </div>
			    
			    <input type="hidden" value="{!!$id!!}" name="employee_id">
	           </br>
               <div class="container-fluid">
	                <div class="row">
	                	<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
	                    		@if(canAdd('hr_recruitment'))
		                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        
		                    </div>
                    	</div>
                	</div>
                </div>
                {!! Form::token() !!}
            </form>
        </div>
    </div>
<script>
$(".datepicker_farsi").persianDatepicker();

</script>
