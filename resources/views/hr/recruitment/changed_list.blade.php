
<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='changed_list'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>نام کامل</th>
                        <th>نام پدر</th>
                        <th>شماره تعینات</th>
                        
                        <th>سکونت اصلی</th>
                        
                        <th>رتبه</th>
                        <th>بست</th>
                        <th>تاریخ تقرر</th>
                        <th>عملیه</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="change_employee_det" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>

<div class="md-overlay"></div><!-- the overlay element -->

<script type="text/javascript">
    $(document).ready(function() {
        $('#changed_list').dataTable(
            {

                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getChangedEmployeeData')!!}"
            }
        );

    });
function load_change_detail(id)
{
	var page = "{!!URL::route('load_change_detail')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id,
        success: function(r){
			$('#change_employee_det').html(r);
        }
    });
}
</script>

