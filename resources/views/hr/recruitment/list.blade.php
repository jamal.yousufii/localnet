@extends('layouts.master')

@section('head')
	{!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
    <title>{!!_('recruitment')!!}</title>

@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>کارمندان</span></li>
        </ol>
    </div>
</div>
@if(canAdd('hr_recruitment') && Auth::user()->id!=413)
<header class="main-box-header clearfix">
    <h2>
    	<a href="{!!URL::route('addNewEmployee')!!}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle fa-lg"></i> کارمند جدید</a>
    </h2>
</header>
@endif
<!-- Example Tabs -->
<div class="example-wrap">
	<div class="nav-tabs-horizontal">
	  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
	    <li @if($active=='all'){!!'class="active"'!!}@endif role="presentation"><a data-toggle="tab" href="#all" aria-controls="exampleTabsOne"
	      role="tab">تمام کارمندان</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('changed')" href="#changed" aria-controls="exampleTabsTwo"
	      role="tab">تبدیلی ها</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('khedmati')" href="#khedmati" aria-controls="exampleTabsThree"
	      role="tab">خدمتی ها</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('fired')" href="#fired" aria-controls="exampleTabsFour"
	      role="tab">منفکی ها</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('resign')" href="#resign" aria-controls="exampleTabsFour"
	      role="tab">استعفا</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('retire')" href="#retire" aria-controls="exampleTabsFour"
	      role="tab">متقاعدین</a></li>

	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('extra_bast')" href="#extra_bast" aria-controls="exampleTabsFour"
	      role="tab">انتظار به معاش</a></li>

	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('tanqis')" href="#tanqis" aria-controls="exampleTabsFour"
	      role="tab">اضافه بست</a></li>
	    <li role="presentation"><a data-toggle="tab" onclick="load_employees('contracts')" href="#contracts" aria-controls="exampleTabsFour"
	      role="tab">قراردادی ها</a></li>

	    <li @if($active=='search'){!!'class="active"'!!}@endif role="presentation"><a data-toggle="tab" href="#search" aria-controls="exampleTabsFour"
	      role="tab">جستجوی پیشرفته </a></li>
	  </ul>
	  <div class="tab-content padding-top-20">
	    <div class="tab-pane @if($active=='all'){!!'active'!!}@endif" id="all" role="tabpanel">
	    	<div class="row">
			    <div class="col-lg-12">
			        <div class="main-box">
			        <form role="form" id="search_form" class="form-horizontal noprint" method="post">
				      <div class="form-group">
				        <div class="col-sm-2">
				          <div class="col-sm-12">
				            <label class="col-sm-12 ">شماره</label>
				          </div>
				          <div class="col-sm-12">
				            <input type="number" class="form-control" name="id_number" placeholder="شماره" />
				          </div>
				        </div>


				        <div class="col-sm-2">
				          <div class="col-sm-12">
				            <label class="col-sm-12 ">نام</label>
				          </div>
				          <div class="col-sm-12">
				            <input type="text" class="form-control" name="full_name" placeholder="نام" />
				          </div>
				        </div>
				        <div class="col-sm-2">
				          <div class="col-sm-12">
				            <label class="col-sm-12 ">نام پدر</label>
				          </div>
				          <div class="col-sm-12">
				            <input type="text" class="form-control" name="father_name" placeholder="نام پدر" />
				          </div>
				        </div>
				        <div class="col-sm-2">
				          <div class="col-sm-12">
				            <label class="col-sm-12 ">تخلص</label>
				          </div>
				          <div class="col-sm-12">
				            <input type="text" class="form-control" name="last_name" placeholder="تخلص" />
				          </div>
				        </div>
				       	<div class="col-sm-4" style="margin-top: 27px">
				            <button class="btn btn-warning" id="search_docs" type="button">
				                <span>
				                    <i class="fa fa-search"></i>
				                </span>
				                &nbsp;جستجو
				            </button>
				            &nbsp;<input type="reset" value="پاک کردن" id="searchclear" class="btn btn-danger"/>

				        </div>

				      </div>
				      {!!Form::token()!!}

				    </form>
			            <div class="main-box-body clearfix" id="searchresult">
			            <div class="table-responsive">
			                <table class="table table-responsive">
			                    <thead>
			                    <tr>
			                        <th>#</th>
			                        <th>نام کامل</th>
			                        <th>ولد</th>
			                        <th>شماره تعینات</th>
			                        <th>دیپارتمنت</th>
			                        <th>سکونت اصلی</th>


			                        <th>بست</th>
			                        <th>تاریخ تقرر</th>
			                        <th>عملیه</th>
			                    </tr>
			                    </thead>
			                    <tbody>
			                    @if($records)
			                    	@foreach($records AS $rec)
			                    	<tr>
			                    	<td>{!!$rec->id!!}</td>
			                    	<td>{!!$rec->fullname!!}</td>
			                    	<td>{!!$rec->father_name!!}</td>
			                    	<td>{!!$rec->number_tayenat!!}</td>
			                    	<td>{!!$rec->department!!}</td>
			                    	<td>{!!$rec->original_province!!}</td>
			                    	<td>{!!$rec->bast!!}</td>
			                    	<td>{!!$rec->emp_date!!}</td>
			                    	<td>
			                    	@if(canEdit('hr_recruitment') || canEdit('hr_documents') || canView('hr_documents'))
										<a href="{!!URL::route('getDetailsEmployee',Crypt::encrypt($rec->id))!!}" class="table-link" style="text-decoration:none;" title="Edit"><i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
										</a>
									@endif
			                    	</td>
			                    	</tr>
			                    	@endforeach
			                    @endif
			                    </tbody>
			                </table>
			            </div>
			        	<div class="dataTables_paginate paging_simple_numbers noprint" id="list_paginate">
					      @if(!empty($records))
					      {!!$records->render()!!}
					      @endif
					    </div>
			            </div>
			        </div>
			    </div>
			</div>
	    </div>
	    <div class="tab-pane" id="changed" role="tabpanel">
	    	لیست تبدیلی ها
	    </div>
	    <div class="tab-pane" id="khedmati" role="tabpanel">
	    	لیست خدمتی ها
	    </div>
	    <div class="tab-pane" id="fired" role="tabpanel">
	    	لیست منفکی ها
	    </div>
	    <div class="tab-pane" id="resign" role="tabpanel">
	    	لیست استعفا
	    </div>
	    <div class="tab-pane" id="retire" role="tabpanel">
	    	لیست تقاعدی ها
	    </div>
	    <div class="tab-pane" id="extra_bast" role="tabpanel">
	    	انتظار به معاش
	    </div>
	    <div class="tab-pane" id="tanqis" role="tabpanel">
	    	اضافه بست
	    </div>
	    <div class="tab-pane" id="contracts" role="tabpanel">

	    </div>
	    <div class="tab-pane @if($active=='search'){!!'active'!!}@endif" id="search" role="tabpanel">
	    	<form class="form-horizontal" role="form" method="post" id="advancedSearch_form" action="{!!URL::route('printEmployees')!!}">
		    	<div class="row">
		        	<div class="col-lg-12">
		        		<div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">نام مکمل</label>
		                                <input type="text" name="name" id="fullname" class="form-control" value=""/>
		                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		                			</div>
		                		</div>
							    <div class="col-sm-3">
		                			<label class="col-sm-12 ">ادارۀ عمومی</label>
                                    <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
										@if(!hasRule('hr_attendance','filter_general_dept'))
										 <option value="0">انتخاب</option>
										@endif 
                                        @foreach($parentDeps AS $dep_item)
                                            <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endforeach
                                    </select>
		                		</div>
		                		<div class="col-sm-3">
                                    <div class="col-sm-12">
                                        <label class="col-sm-12 control-label">ادارۀ مربوط</label>
                                    </div>
		                			<div class="col-sm-12">
		                                <select class="form-control select2 col-sm-12" name="sub_dep" id="sub_dep">
                                            <option value='0'>انتخاب</option>

		                                </select>
		                			</div>
		                		</div>
								<div class="col-sm-3">
					            	<div class="col-sm-12">
					            		<label class="col-sm-12 ">نوعیت اداره</label>
		                                <select name="officetype" class="form-control">
		                                    <option value='0'>همه</option>
		                                    <option value='1'>ریاست دفتر</option>
		                                    <option value='2'>اداره امور</option>
		                                    
		                                </select>
					               	</div>

					            </div>
							</div>
						</div>
						<div class="container-fluid">
					      	<div class="row">

					      		<div class="col-sm-3">
					            	<div class="col-sm-12">
					            		<label class="col-sm-2 ">کارکنان</label>
		                                <select name="employee_type" id="employee_type" class="form-control" onchange="showRank(this.value)">
		                                    <option value='0'>انتخاب</option>
		                                    <option value='1'>مامور</option>
		                                    <option value='2'>اجیر</option>
		                                    <option value='3'>نظامی</option>
		                                    <option value='4'>مامور بالمقطع</option>
		                                    <option value='5'>اجیر بالمقطع</option>
		                                </select>
					               	</div>

					            </div>
					            <div id="bast_div">
						      		<div class="col-sm-3">
						      			<div class="col-sm-12">
						      				<label class="col-sm-12 ">بست</label>
			                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
			                                <select name = "emp_bast" id = "emp_bast" class="form-control">
			                                    <option value='0'>انتخاب</option>
			                                    {!!getBastStaticList('employee_rank')!!}
			                                </select>
						      			</div>
						      		</div>
						      		<div class="col-sm-3">
						      			<div class="col-sm-12">
						      				<label class="col-sm-12 ">رتبه</label>
			                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
			                                <select name = "emp_rank" id="emp_rank" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    {!!getStaticDropdown('employee_rank')!!}
			                                </select>
						      			</div>
						      		</div>
						      	</div>
						      	<div id="ajir_div" style="display:none;">
							      	<div class="col-sm-3">
						      			<div class="col-sm-12">
						      				<label class="col-sm-12 ">درجه اجیر</label>
			                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
			                                <select name = "ajir_bast" id = "ajir_bast" class="form-control">
			                                    <option value='0'>انتخاب</option>
			                                    {!!getBastStaticList('employee_rank')!!}
			                                </select>
						      			</div>
						      		</div>
						      		<div class="col-sm-3">
						      			<div class="col-sm-12">
						      				<label class="col-sm-12 ">رتبه</label>
			                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
			                                <select name = "ajir_rank" id="emp_rank" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    {!!getStaticDropdown('employee_rank')!!}
			                                </select>
						      			</div>
						      		</div>
					      		</div>
					      		<div id="military_div" style="display:none;">
					      			<div class="col-sm-3">
						      			<div class="col-sm-12">
						      				<label class="col-sm-12 ">بست</label>
			                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
			                                <select name = "military_bast" name = "military_bast" class="form-control">
		                                        <option value='0'>انتخاب</option>
		                                        {!!getStaticDropdown('military_rank',Input::old('emp_bast'))!!}
		                                    </select>
						      			</div>
						      		</div>
						      		<div class="col-sm-3">
						      			<div class="col-sm-12">
						      				<label class="col-sm-12 ">رتبه</label>
			                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
			                                <select name = "military_rank" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    {!!getStaticDropdown('military_rank')!!}
			                                </select>
						      			</div>
						      		</div>
					      		</div>
					      		<div class="col-sm-3">
					            	<div class="col-sm-12">
					            		<label class="col-sm-2 ">نوعیت</label>
		                                <select name="type" id="type" class="form-control">
		                                	<option value='0'>انتخاب</option>
		                                    <option value='1'>برحال</option>
		                                    <option value='2'>انتظار بامعاش</option>
		                                    <option value='3'>بالمقطع</option>
		                                    <option value='4'>درجریان تحصیل</option>
		                                    <option value='5'>تقاعد</option>
		                                    <option value='6'>منفک</option>
		                                    <option value='7'>اضافه بست</option>
	                                        <option value='8'>تبدیلی</option>
		                                    <option value='9'>استعفا</option>
		                                </select>
					               	</div>
					            </div>


		                	</div>
		                </div>
		                <div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-3">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">سال تولد</lable>
		                                <?php
		                                     $current_year = date('Y')-621;
		                                ?>
		                                <select name = "birth_year[]" id = "birth_year" class="form-control select2" style="width:100%;" multiple="multiple" data-plugin="select2">
		                                    <?php
		                                        for($i=$current_year-84;$i<=$current_year-10;$i++)
		                                        {
		                                            echo "<option>".$i."</option>";
		                                        }
		                                    ?>
		                                </select>
					               	</div>
					            </div>
					            <div class="col-sm-3">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">سویه تحصیلی</lable>
		                                <select name = "education_degree[]" class="form-control select2" style="width:100%;" multiple="multiple" data-plugin="select2">
		                                    {!!getStaticDropdown('education_degree')!!}
		                                </select>
					               	</div>
					            </div>
					      		<div class="col-sm-3">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12">ولایت</lable>
                                         <select class="form-control select2" name="original_province" id="original_province">
                                               <option value=''>انتخاب</option>
                                                @foreach(getAllProvinces('dr') AS $pro)
                                                   <option value='{!!$pro->id!!}'>{!!$pro->name!!}</option>
                                                @endforeach
                                         </select>   
					               	</div>
					            </div>
					            <div class="col-sm-3">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">قوم</lable>
                                        <select class="form-control select2" name="nationality" id="nationality">
                                             <option value=''>انتخاب</option>
	                                         {!!getStaticDropdown('ethnicity',0)!!}
                                         </select>       
					               	</div>
					            </div>
					        </div>
					    </div>
                        <div class="container-fluid">
					      	<div class="row">
		                		<div class="col-sm-3">
					            	<div class="col-sm-12">
					            		<label class="col-sm-12 ">تاریخ ابتدای تقرر</label>
		                                <input type="text" name="first_job_date" class="form-control datepicker_farsi">
					               	</div>
					            </div>
		                		<div class="col-sm-2">
					            	<div class="col-sm-12">
					            		<label class="col-sm-2 ">جنسیت</label>
		                                <select name="gender" id="gender" class="form-control">
		                                    <option value='0'>انتخاب</option>
		                                    <option value='1'>مرد</option>
		                                    <option value='2'>زن</option>
		                                </select>
					               	</div>
					            </div>
					             <div class="col-sm-2">
							    	<label class="col-sm-12 ">&nbsp;</label>
					    			<div class="col-sm-12 checkbox-custom checkbox-primary checkbox-inline">
				                        <input value='1' name="docs" id="docs" type="checkbox">
				                        <label for="subordinates">
				                           اسناد ناتکمیل
				                        </label>
				                    </div>
                                </div>
                                <div class="col-sm-3">
					            	<div class="col-sm-12">
					            		<label class="col-sm-12 ">شماره تماس</label>
		                                <input type="text" name="phone" class="form-control">
					               	</div>
					            </div>
                                <div class="col-sm-1">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">&nbsp;</label>
		                                <button class="btn btn-primary pull-right" id="advancedSearch_docs" type="button"> جستجو</button>
		                			</div>
		                		</div>
                                <div class="col-sm-1">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">&nbsp;</label>
		                                <input type="reset" value="پاک کردن" id="searchclear" class="btn btn-danger"/>
		                			</div>
		                		</div>
                              </div>
                        </div>
					    <div class="container-fluid">
					      	<div class="row">
                               @if(hasRule('hr_recruitment','print_ehsaya_report'))
		                		<div class="col-sm-1">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">&nbsp;</label>
		                                <button type="submit" class="btn btn-success" name="print" value="statistic" >احصاییه</button>
		                			</div>
								</div>
								@endif
								@if(hasRule('hr_recruitment','tarfe_report'))
		                		<div class="col-lg-2">
		                			<div class="col-sm-10">
		                				<label class="col-sm-12 ">&nbsp;</label>
		                                <button type="submit" class="btn btn-success" name="print" value="tarfea" >جدول ترفیع</button>
		                			</div>
								</div>
								@endif
								@if(hasRule('hr_recruitment','search_print'))
		                		<div class="col-sm-2">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">&nbsp;</label>
										<button type="submit" class="btn btn-success" style="display:inline-block" name="print" value="excel" >چاپ به صفحه اکسل</button>
		                			</div>
		                		</div>
		                	   @endif
		                	</div>
		                </div>
					</div>
				</div>
		    </form>
		    <hr>
		    <div class="row">
			    <div class="col-lg-12">
			        <div class="main-box">
			            <div class="main-box-body clearfix" id="search_result_list">

			            </div>
			        </div>
			    </div>
			</div>
	    </div>
	  </div>
	</div>
</div>
<!-- End Example Tabs -->


<!-- fire modal start -->
<div class="modal fade modal-fade-in-scale-up" id="change_date_modal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <div class="modal-header">
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">×</span>
	      </button>
	      <h4 class="modal-title">تاریخ منفکی</h4>
	    </div>
	    <div class="modal-body">
	      <form role="form" class='form-horizontal' id="change_modal_frm" name="change_modal_frm" style="padding-right:10px;">
	            <input type="hidden" name="employee_id" id="employee_id" value="0">
	            <div class="form-group">
	                <label class="col-sm-2 control-label">تاریخ :</label>
	                <div class="col-sm-4">
	                    <input class="form-control datepicker_farsi" type="text" name="date" data-placement="bottom">
	                </div>

	            </div>
	      </form>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn btn-default margin-0" data-dismiss="modal">{!!_('close')!!}</button>
	      @if(canAdd('hr_recruitment'))
	      <button onclick='fireDate();' type="button" class="btn btn-primary modal-submit">منفک</button>
	      @else
	      <p>You dont have permission</p>
	      @endif
	    </div>
	  </div>
	</div>
</div>
<!-- Modal End -->
<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>

<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">
$(".select2").select2();

$('.pagination a').on('click', function(event) {
    event.preventDefault();
    if ($(this).attr('href') != '#') {
      //$('#ajaxContent').load($(this).attr('href'));
      var dataString = $('#search_form').serialize();
      dataString += "&page="+$(this).text()+"&ajax="+1;
      $.ajax({
          url: '{!!URL::route("getRecruitment")!!}',
          data: dataString,
          type: 'get',
          beforeSend: function(){
              //$("body").show().css({"opacity": "0.5"});
              $('#searchresult').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#searchresult').html(response);
          }
      }
    );

    }
  });
$('#search_docs').click(function(){
      var datastring = $('#search_form').serialize();
      $.ajax({
          type: 'POST',
          url: '{!!URL::route("getRecruitment_search")!!}',
          data: datastring+"&ajax="+1,
          beforeSend: function(){
              $('#search_result').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#searchresult').html(response);
          }
      });
      return false;
  });
//advanced search
$('#advancedSearch_docs').click(function(){
      var datastring = $('#advancedSearch_form').serialize();
      $.ajax({
          type: 'POST',
          url: '{!!URL::route("getRecruitmentSearch")!!}',
          data: datastring+"&ajax="+1,
          beforeSend: function(){
              $('#search_result_list').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
          },
          success: function(response)
          {
              $('#search_result_list').html(response);
          }
      });
      return false;
  });

function fireDate()
{
    var page = "{!!URL::route('fireDate')!!}";

    $.ajax({
        url: page,
        type: 'post',
        data: $('#change_modal_frm').serialize(),
        dataType:'HTML',
        success: function(response)
      	{
        	$('.close').click();
        }
    });
}
function load_employees(div)
{
    var page = "{!!URL::route('loadEmployees')!!}";

    $.ajax({
        url: page,
        type: 'post',
        data: '&type='+div,
        //dataType:'HTML',
        success: function(r)
      	{
        	$('#'+div).html(r);
        }
    });
}
function load_change_employee(id)
{
	var page = "{!!URL::route('load_change_employee')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id,
        success: function(r){
			$('#change_employee').html(r);
        }
    });
}
function load_promotion_employee(id)
{
	var page = "{!!URL::route('load_promotion_employee')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id,
        success: function(r){
			$('#change_employee').html(r);
        }
    });
}
function load_service_employee(id)
{
	var page = "{!!URL::route('load_service_employee')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id,
        success: function(r){
			$('#change_employee').html(r);
        }
    });
}

function bringRelatedSubDepartment(div,id)
{
    $.ajax({
            url: '{!!URL::route("bringSubDepartment")!!}',
            data: '&dep_id='+id,
            type: 'post',
            beforeSend: function(){
                $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
            },
            success: function(response)
            {
                $('#'+div).html(response);
            }
        }
    );
}
function showRank(value)
{
    if(value == 3)
    {//military
        $('#ajir_div').slideUp();
        $('#bast_div').slideUp();
        $('#military_div').slideDown();

    }
    else if(value == 2)
    {//ajir
    	$('#ajir_div').slideDown();
        $('#military_div').slideUp();
        $('#bast_div').slideUp();
    }
    else
    {
        $('#ajir_div').slideUp();
        $('#military_div').slideUp();
        $('#bast_div').slideDown();
    }

}
</script>
@stop
