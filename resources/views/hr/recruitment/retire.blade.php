
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('retireDate')!!}" enctype="multipart/form-data">
			 
	            <div class="panel-heading">
			      <h5 class="panel-title">جزییات</h5>
			    </div>
			    <div class="container-fluid">
			    	<div class="row">
			    		<div class="col-sm-6">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تاریخ تقاعد</label>
                        		<?php $sdate = $row->retire_date; if($sdate!='0000-00-00' && $sdate !=null){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
                                <input class="form-control datepicker_farsi" readonly type="text" name="date" value='<?php if($row->retire_date!="0000-00-00" && $row->retire_date !=null){echo jalali_format($sdate);}?>'>
                        	</div>
                        </div>
                        <div class="col-sm-6">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">شماره حکم</label>
                        		<input class="form-control" type="text" name="number" value='<?=$row->retire_no?>'>
                        	</div>
                        </div>
			    	</div>
			    </div>
			    <div class="container-fluid">
	                <div class="row">
	                	<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">ضمیمه</label>
	                    		
	                            <input type='file'  name='retire' class="form-control">
	                    	</div>
	                   </div>
	                   @if($attachment)
	                   <div class="col-sm-6" id="doc_{!!$attachment->id!!}">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-12 ">&nbsp;</label>
	                    		<a href="{!!URL::route('getEmployeeDownload',$attachment->id)!!}">{!!$attachment->file_name!!}</a>
	                    		@if(canDelete('hr_recruitment'))
	                    		<a href="javascript:void()" onclick="removeEmployeeFile('{!!$attachment->id!!}');" class="table-link danger">
                                    <i class="fa fa-trash-o" style='color:red;'></i>
                                </a>	
                                @endif
	                    	</div>
	                   </div>
	                   @endif
	               	</div>
	           </div>
			    <input type="hidden" value="{!!$id!!}" name="employee_id">
	           </br>
               <div class="container-fluid">
	                <div class="row">
	                	<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
	                    		@if(canAdd('hr_documents'))
		                        	<button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                        
		                    </div>
                    	</div>
                	</div>
                </div>
                {!! Form::token() !!}
            </form>
        </div>
    </div>
<script>
$(".datepicker_farsi").persianDatepicker();
function removeEmployeeFile(doc_id)
{
    var confirmed = confirm("Do you want to remove this file?");
    if(confirmed)
    {
        $.ajax({
                url: '{!!URL::route("removeEmployeeFile")!!}',
                data: '&doc_id='+doc_id,
                type: 'post',
                beforeSend: function(){
                    $("#doc_"+doc_id).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#doc_'+doc_id).html(response);
                }
            }
        );
    }

}
</script>
