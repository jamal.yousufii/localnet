@extends('layouts.master')

@section('head')
    <title>{!!_('register_new_employee')!!}</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
@stop
@section('content')
@if (count($errors) > 0)
<div class="alert alert-danger" style="margin: 10px 0 20px 0">
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
@if($errors->has('field'))
    <div class='alert alert-danger span6'>{!!$errors->first('field')!!}</div>
@elseif(Session::has('fail'))
    <div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
@endif
<ol class="breadcrumb">
    <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
    <li><a href="{!!URL::route('getRecruitment')!!}">{!!_('employees')!!}</a></li>
    <li class="active"><span>ثبت معلومات کارمند</span></li>
</ol>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" id="newemployeeform" role="form" method="post" action="{!!URL::route('postNewEmployee')!!}">
                <div class="panel-heading">
                  <h5 class="panel-title">معلومات درباره بست مورد نظر</h5>
                </div>
            <div id="exist_div">
                <div class="container-fluid">
                    <div class="row">
                        <?php
                        $sh_date = dateToShamsi(date('Y'),date('m'),date('d'));
                        $year = explode('-', $sh_date);
                        $year = $year[0]+1;
                        ?>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">سال <span style="color:red;">*</span></label>
                                <select id="tashkil_year" required class="form-control">
                                    @for($i=1396;$i<=$year;$i++)
                                        @if($i==$year)
                                        <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
                                        @else
                                        <option value='{!!$i!!}'>{!!$i!!}</option>
                                        @endif
                                    @endfor
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">ادارۀ عمومی <span style="color:red;">*</span></label>
                                <select name="general_department" id="general_department" required class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                        <option value="">انتخاب</option>
                                    @foreach($parentDeps AS $dep_item)

                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>

                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12">ادارۀ مربوط <span style="color:red;">*</span></label>
                            </div>
                            <div class="col-sm-12">
                                <select class="form-control" style="width:100%;" name="sub_dep" id="sub_dep" required onchange="getRelatedBasts(this.value)">
                                    <option value="">انتخاب</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">عنوان وظیفه <span style="color:red;">*</span></label>
                                <select name="tashkil" class="form-control" id="tashkil" onchange="bringTashkilDet(this.value)">
                                    <option value=''>انتخاب</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">شماره تعینات</label>
                                <input class="form-control" type="text" name="number_tayenat" id="number_tayenat" value="{!!Input::old('number_tayenat')!!}" readonly="readonly">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">بست</label>
                                <input class="form-control" type="text" id="tashkil_bast" name="tashkil_bast" readonly>

                            </div>
                        </div>



                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">موقف بست</label>
                                <select name = "position_dr" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>اصلی</option>
                                        <option value='2'>خدمتی</option>
                                        <option value='3'>تحصیلی</option>
                                        <option value='4'>انتظار معاش</option>
                                        <option value='5'>عسکری</option>
                                    </select>
                                <span style="color:red">{!!$errors->first('position_dr')!!}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



                <div class="container-fluid">
                    <div class="row">

                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">کارکنان <span style="color:red;">*</span></label>
                                <select name="employee_type" class="form-control" onchange="showRank(this.value)">
                                    <option value=''>انتخاب</option>
                                    <option value='1' <?php echo (Input::old('employee_type')=='1' ? 'selected':''); ?>>مامور</option>
                                    <option value='2' <?php echo (Input::old('employee_type')=='2' ? 'selected':''); ?>>اجیر</option>
                                    <option value='3' <?php echo (Input::old('employee_type')=='3' ? 'selected':''); ?>>نظامی</option>
                                    <option value='4' <?php echo (Input::old('employee_type')=='4' ? 'selected':''); ?>>مامور بالمقطع</option>
                                    <option value='5' <?php echo (Input::old('employee_type')=='5' ? 'selected':''); ?>>اجیر بالمقطع</option>
                                    <option value='6' <?php echo (Input::old('employee_type')=='6' ? 'selected':''); ?>>خدمتی</option>

                                </select>
                            </div>

                        </div>



                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">نوع تقرر</label>
                                <select name = "free_competition" class="form-control">
                                    <option value=''>انتخاب</option>
                                    <option value='جدیدالتقرر'>
                                        جدیدالتقرر
                                    </option>
                                    <option value='سابق'>
                                        سابق
                                    </option>
                                    <option value='تبدیلی'>
                                        تبدیلی
                                    </option>

                                </select>
                                <span style="color:red">{!!$errors->first('free_compitition')!!}</span>
                            </div>
                        </div>
                         <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">موقف کارکن</label>
                                <select name="mawqif_employee" class="form-control" disabled="disabled">
                                    <option value=''>انتخاب</option>
                                    <option value='1' selected="selected">برحال</option>
                                    <option value='2' >انتظار بامعاش</option>
                                    <option value='3' >بالمقطع</option>
                                    <option value='4' >درجریان تحصیل</option>
                                    <option value='5' >وغیره</option>
                                </select>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid" >
                    <div class="row">
                        <!--
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">رتبه علمی</label>
                                <input class="form-control" type="text" name="education_rank" value="{!!Input::old('education_rank')!!}">

                            </div>
                        </div>
                        -->

                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">تاریخ درخواست</label>
                                <input class="form-control datepicker_farsi" readonly type="text" name="first_appointment_date" value="{!!Input::old('first_appointment_date')!!}">
                                <span style="color:red">{!!$errors->first('first_appointment_date')!!}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">وظیفه فعلی</label>
                                <input class="form-control" type="text" name="current_position_dr" value="{!!Input::old('current_position_dr')!!}">
                                <span style="color:red">{!!$errors->first('current_position_dr')!!}</span>
                            </div>
                        </div>
                        <!--
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">تاریخ اخرین ترفع</label>
                                <input class="form-control datepicker_farsi" type="text" name="last_appointment_date" value="{!!Input::old('last_appointment_date')!!}">
                            </div>
                        </div>
                        -->
                    </div>
                </div>

            <div id="contract" style="display:none">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">ادارۀ عمومی <span style="color:red;">*</span></label>
                                <select name="general_department1" id="general_department1" class="form-control" onchange="bringRelatedSubDepartment('sub_dep1',this.value)">
                                        <option value="">انتخاب</option>
                                    @foreach($parentDeps AS $dep_item)

                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>

                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">ادارۀ مربوط <span style="color:red;">*</span></label>
                                <select class="form-control" name="sub_dep1" id="sub_dep1">
                                    <option value="">انتخاب</option>

                                </select>

                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">تاریخ ختم قرارداد <span style="color:red;">*</span></label>
                                <input class="form-control datepicker_farsi" readonly type="text" name="last_date" value="{!!Input::old('last_date')!!}">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
                <div class="panel-heading">
                  <h5 class="panel-title">معلومات عمومی در مورد کاندید</h5>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">نام <span style="color:red;">*</span></label>
                                <input class="form-control" type="text" required name="name_dr" value="{!!Input::old('name_dr')!!}">
                                <span style="color:red">{!!$errors->first('name_dr')!!}</span>

                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">تخلص</lable>
                                <input type="text" class="form-control" name="last_name" />

                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">نام پدر <span style="color:red;">*</span></lable>
                                <input class="form-control" type="text" required name="father_name_dr" value="{!!Input::old('father_name_dr')!!}">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">پدر کلان <span style="color:red;">*</span></lable>
                                <input type="text" class="form-control" required name="grand_father_name" value="{!!Input::old('grand_father_name')!!}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">نمبر تذکره</label>
                                <input class="form-control" type="text" name="id_no" value="{!!Input::old('id_no')!!}">
                                <span style="color:red">{!!$errors->first('id_no')!!}</span>

                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">جلد</lable>
                                <input type="text" class="form-control" name="id_jild" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">صفحه</lable>
                                <input class="form-control" type="text" name="id_page" value="{!!Input::old('id_page')!!}">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">ثبت</lable>
                                <input type="text" class="form-control" name="id_sabt" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">قوم <span style="color:red;">*</span></label>
                                <select name = "nationality" class="form-control">
                                    <option value=''>انتخاب</option>
                                    {!!getStaticDropdown('ethnicity')!!}
                                </select>

                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">جنسیت <span style="color:red;">*</span></label>

                                <select name="gender" required="required" class="form-control">
                                    <option value=''>انتخاب</option>
                                    <option value='M' <?php echo (Input::old('gender')=='M' ? 'selected':''); ?>>مرد</option>
                                    <option value='F' <?php echo (Input::old('gender')=='F' ? 'selected':''); ?>>زن</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">گروپ خون <span style="color:red;">*</span></label>
                                <select name = "blood_group" id="blood_group" class="form-control">
                                    <option value=''>انتخاب</option>
                                    {!!getStaticDropdown('blood_group',Input::old('blood_group'))!!}
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">سال تولد</lable>
                                <?php
                                     $current_year = date('Y')-621;

                                ?>
                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
                                <select name = "birth_year" class="form-control">
                                    <option value=''>انتخاب</option>
                                    <?php

                                        for($i=$current_year-84;$i<=$current_year-10;$i++)
                                        {

                                            echo "<option>".$i."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">&nbsp;</label>
                                <h5>سکونت اصلی</h5>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">ولایت</label>
                                <?php $page=URL::route('getProvinceDistrict'); ?>
                                <select class="form-control" name="original_province" id="original_province" onchange="getProvinceDistrict('{!!$page!!}','districts',this.value);">
                                    <option value=''>انتخاب</option>
                                    @foreach($provinces AS $pro)
                                        <option value='{!!$pro->id!!}'>{!!$pro->name!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">ولسوالی</lable>
                                <select name = "district" id="districts" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">ناحیه</lable>
                                <input type="text" class="form-control" name="village" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                        <div class="col-sm-12">
                                <label class="col-sm-12 ">&nbsp;</label>
                                <h5>سکونت فعلی</h5>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">ولایت</label>
                                <?php $page=URL::route('getProvinceDistrict'); ?>
                                <select class="form-control" name="current_province" id="current_province" onchange="getProvinceDistrict('{!!$page!!}','current_districts',this.value);">
                                    <option value=''>انتخاب</option>
                                    @foreach($provinces AS $pro)
                                        <option value='{!!$pro->id!!}'>{!!$pro->name!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">ولسوالی</lable>
                                <select name = "current_district" id="current_districts" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">ناحیه</lable>
                                <input type="text" class="form-control" name="current_village" />
                            </div>
                        </div>

                    </div>
                </div>


                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">تیلفون</label>
                                <input class="form-control" type="text" name="phone" value="{!!Input::old('phone')!!}">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">آدرس الکترونیکی</label>
                                <input type="text" name="email" class="form-control">
                            </div>
                        </div>


                    </div>
                </div>
                <!--
                <div class="panel-heading">
                  <h5 class="panel-title">معلومات در مورد تجارب کاری</h5>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">ارگان/موسسه</lable>
                                <input type="text" class="form-control" name="experience_company_1" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">عنوان بست</label>
                                <input type="text" class="form-control" name="experience_position_1" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">بست/رتبه</lable>
                                <input type="text" class="form-control" name="experience_bast_1" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">دلایل ترک وظیفه</lable>
                                <input type="text" class="form-control" name="experience_leave_1" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">&nbsp;</lable>
                                <input type="text" class="form-control" name="experience_company_2" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">&nbsp;</label>
                                <input type="text" class="form-control" name="experience_position_2" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">&nbsp;</lable>
                                <input type="text" class="form-control" name="experience_bast_2" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">&nbsp;</lable>
                                <input type="text" class="form-control" name="experience_leave_2" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">&nbsp;</lable>
                                <input type="text" class="form-control" name="experience_company_3" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">&nbsp;</label>
                                <input type="text" class="form-control" name="experience_position_3" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">&nbsp;</lable>
                                <input type="text" class="form-control" name="experience_bast_3" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">&nbsp;</lable>
                                <input type="text" class="form-control" name="experience_leave_3" />
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="panel-heading">
                      <h5 class="panel-title">فهم و توانایی در لسان ها</h5>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">زبان</lable>
                                    <input type="text" class="form-control" name="lang_1" value="da" placeholder="دری" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">تحریر</label>
                                    <select name = "writing_1" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">تکلم</lable>
                                    <select name = "speaking_1" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">خوانش</lable>
                                    <select name = "reading_1" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">تایپینگ</lable>
                                    <select name = "typing_1" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <input type="text" class="form-control" name="lang_2" value="pa" placeholder="پشتو" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</label>
                                    <select name = "writing_2" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <select name = "speaking_2" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <select name = "reading_2" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <select name = "typing_2" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <input type="text" class="form-control" name="lang_3" value="en" placeholder="انگلیسی" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</label>
                                    <select name = "writing_3" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <select name = "speaking_3" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <select name = "reading_3" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <select name = "typing_3" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <input type="text" class="form-control" name="lang_4" value="other" placeholder="دیگر" />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</label>
                                    <select name = "writing_4" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <select name = "speaking_4" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <select name = "reading_4" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <select name = "typing_4" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1'>عالی</option>
                                        <option value='2'>خوب</option>
                                        <option value='3'>متوسط</option>
                                        <option value='4'>ابتدایی</option>
                                        <option value='5'>هیچکدام</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-heading">
                      <h5 class="panel-title">مشخصات و مراجع معلومات اشخاص بر حال وظیفه</h5>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">اسم و تخلص</lable>
                                    <input type="text" class="form-control" name="garantee_name_1"  />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">وظیفه</label>
                                    <input type="text" class="form-control" name="garantee_job_1" />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">اداره مربوطه</lable>
                                    <input type="text" class="form-control" name="garantee_company_1" />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">تیلفون</lable>
                                    <input type="text" class="form-control" name="garantee_phone_1" />
                                </div>
                            </div>
                             <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">آدرس الکترونیکی</lable>
                                    <input type="text" class="form-control" name="garantee_email_1" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <input type="text" class="form-control" name="garantee_name_2" />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</label>
                                    <input type="text" class="form-control" name="garantee_job_2" />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <input type="text" class="form-control" name="garantee_company_2" />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <input type="text" class="form-control" name="garantee_phone_2" />
                                </div>
                            </div>
                             <div class="col-sm-2">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">&nbsp;</lable>
                                    <input type="text" class="form-control" name="garantee_email_2" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="col-sm-12">
                                    <label class="col-sm-12 ">آپلود ضمیمه</label>
                                    <input type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
                                </div>
                            </div>
                        </div>
                    </div>

               -->

                <div class="container-fluid">
                    <div class="row mt-2">
                        <div class="col-sm-6">
                            <div class="col-sm-12" id="btndivs">
                                <label class="col-sm-2 ">&nbsp;</label>
                                <button class="btn btn-primary" type="button" id="submitbtn">ثبت معلومات</button>

                                <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::token() !!}
            </form>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}
{!! HTML::script('/js/jquery/jquery.validation.min.js') !!}

<script type="text/javascript">
$("#sub_dep").select2();
$('#submitbtn').click(function(e){
    $('#newemployeeform').validate();
    $('.error').css({"color": "red"});
    if($('#newemployeeform').valid())
    {
        $('#btndivs').html('loading...');
        $("#newemployeeform").submit();
    }
    else
    {
        e.preventDefault();
    }

});
    $(document).on("change","#files",function(){
        var count = $('#files_div').find('input').length;
        count = count+1;
        $("<div id='div_"+count+"' style='display:inline;'><input style='splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div');
    });


    function changeDiv(value)
    {
        if(value==4)
        {
            $('#vacant_div').slideDown()
            $('#exist_div').slideUp()
        }
        else
        {
            $('#vacant_div').slideUp()
            $('#exist_div').slideDown()
        }
    }
    function showRank(value)
    {
        if(value == 4 || value == 5 || value == 6)
        {//contract
            $('#exist_div').slideUp();
            $('#contract').slideDown();
            $('#general_department1').prop("required",true);
            $('#sub_dep1').prop("required",true);
            $('#general_department').prop("required",false);
            $('#sub_dep').prop("required",false);
        }
        else
        {//exist div
            $('#general_department1').prop("required",false);
            $('#sub_dep1').prop("required",false);
            $('#general_department').prop("required",true);
            $('#sub_dep').prop("required",true);

            $('#contract').slideUp();
            $('#exist_div').slideDown();

            if(value == 3)
            {//military
                $('#ajir_div').slideUp();
                $('#bast_div').slideUp();
                $('#military_div').slideDown();

            }
            else if(value == 2)
            {//ajir
                $('#ajir_div').slideDown();
                $('#military_div').slideUp();
                $('#bast_div').slideUp();
            }
            else
            {
                $('#ajir_div').slideUp();
                $('#military_div').slideUp();
                $('#bast_div').slideDown();
            }
        }
    }
    function getProvinceDistrict(page,ele,value)
    {

        $.ajax({
            url: page,
            type: 'post',
            data: 'province='+value,
            dataType: 'html',
            success:function(respones){
                $('#'+ele).html(respones);
            }
        });

    }
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function bringTashkilDet(value)
    {
        $.ajax({
            url: '{!!URL::route("bringTashkilDetViaAjax")!!}',
            type: 'post',
            data: '&id='+value,
            dataType: 'json',
            success:function(respones){
                $('#tashkil_bast').val(respones.bast);
                $('#number_tayenat').val(respones.number);
               //$('#tashkil_sub_dep').val(respones.sub_dep);
            }
        });
    }
    function getRelatedBasts(value)
    {
        var year = $('#tashkil_year').val();
        if($('#vacant').val()==4)
        {
            var vacant = $('#vacant').val();
            $.ajax({
                url: '{!!URL::route("bringExistBastViaAjax")!!}',
                type: 'post',
                data: 'id='+value+'&vacant='+vacant,
                dataType: 'html',
                success:function(respones){
                    $('#tashkil').html(respones);
                }
            });
        }
        else
        {
            $.ajax({
                url: '{!!URL::route("bringRelatedBastViaAjax")!!}',
                type: 'post',
                data: 'id='+value+'&type=0'+'&year='+year,
                dataType: 'html',
                success:function(respones){
                    $('#tashkil').html(respones);
                }
            });
        }
    }
</script>

@stop
