<div class="table-responsive">
    <table class="table table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>نام کامل</th>
            <th>ولد</th>
            <th>شماره تعینات</th>
            <th>وظیفه</th>
            <th>دیپارتمنت</th>
            <th>بست</th>            
            <th>تاریخ تقرر</th>
            <th>تلفن</th>
            <th>تحصیلات</th>
            <th>وضعیت</th>
            <th>عملیه</th>
        </tr>
        </thead>
        <tbody>
        @if($records)
            @foreach($unique_record AS $rec)
              <?php  $mawqif_employee = $rec['mawqif_employee']; ?>
                <tr>
                <td>{!!$rec['id']!!}</td>
                <td>{!!$rec['fullname']!!}</td>
                <td>{!!$rec['father_name']!!}</td>
                <td>{!!$rec['number_tayenat']!!}</td>
                <td>{!!$rec['current_position_dr']!!}</td>
                <td>{!!$rec['department']!!}</td>
                <td>{!!$rec['bast']!!}</td>
                <td>{!!$rec['emp_date']!!}</td>
                <td>{!!$rec['phone']!!}</td>
                <td>{{$rec['education']}}</td>
                <td>
                    @if(empty(Config::get("static.emp_status.$lang.$mawqif_employee")))
                    نامعلوم
                    @else
                        {{Config::get("static.emp_status.$lang.$mawqif_employee")}}
                    @endif    
                </td>
                <td>
                    @if(canEdit('hr_recruitment') || canEdit('hr_documents') || hasRule('hr_recruitment','hr_recruitment_edit'))
                        <a href="{!!URL::route('getDetailsEmployee',Crypt::encrypt($rec['id']))!!}" target="_blank" class="table-link" style="text-decoration:none;" title="Edit"><i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
                        </a>
                    @endif
                </td>
                </tr>
        	@endforeach
        @endif
        </tbody>
    </table>
</div>
<div class="dataTables_paginate paging_simple_numbers noprint" id="list_paginate">
  @if(!empty($records))
   @include('layouts.pagination', ['paginator' => $records])
  @endif
</div>
<script type="text/javascript">

$(document).ready(function() {
	
	$('.pagination a').on('click', function(event) {
		event.preventDefault();
		if ($(this).attr('href') != '#') {
			//$('#ajaxContent').load($(this).attr('href'));
			var dataString = $('#advancedSearch_form').serialize();
			dataString += "&page="+$(this).attr('id')+"&ajax="+1;
			$.ajax({
	                url: '{!!URL::route("getRecruitmentSearch")!!}',
	                data: dataString,
	                type: 'post',
	                beforeSend: function(){
	                    //$("body").show().css({"opacity": "0.5"});
	                    $('#search_result_list').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
	                    $('#search_result_list').html(response);
	                }
	            }
	        );
		
		}
	});

});

</script>