@if (count($errors) > 0)
<div class="alert alert-danger" style="margin: 10px 0 20px 0">
  <ul>
    @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('updateEmployeeContract',$id)!!}" enctype="multipart/form-data">
			    <input type="hidden" name="tashkil_id" value="{!!$row->tashkil_id!!}"/>
			    <div class="panel-heading col-sm-9">
			      <h5 class="panel-title">معلومات درباره بست مورد نظر</h5>

			    </div>
                <div class="col-sm-3">
                    {!!HTML::image('documents/profile_pictures/'.$row->photo,'user image',array('width'=>'150', 'hight'=>'100'))!!}
                </div>
			    <div id="exist_div" @if($row->employee_type==4 || $row->employee_type==5 || $row->employee_type==6) style="display:none" @else style="display:block" @endif>


                <div class="container-fluid">
                	<div class="row">
                    <div class="col-sm-3">
                        <div class="col-sm-12">
                            <label class="col-sm-12 ">سال <span style="color:red;">*</span></label>
                            <select id="tashkil_year" required class="form-control">
                                <?php
                                $the_date = dateToShamsi(date('Y'),date('m'),date('d'));
                                $s_date = explode('-',$the_date);
                                $cyear = $s_date[0]+1;
                                ?>
                                @for($i=1396;$i<=$cyear;$i++)
                                    @if($i==$year)
                                    <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
                                    @else
                                    <option value='{!!$i!!}'>{!!$i!!}</option>
                                    @endif
                                @endfor
                            </select>
                                <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                        </div>
                    </div>

                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی <span style="color:red;">*</span></label>
                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                    <option value="">انتخاب</option>
                                    @foreach($parentDeps AS $dep_item)
                                    	@if($dep_id == $dep_item->id)
                                        <option value='{!!$dep_item->id!!}' selected>{!!$dep_item->name!!}</option>
                                        @else
                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-3">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ مربوط <span style="color:red;">*</span></label>
                				<select class="form-control" name="sub_dep" id="sub_dep" onchange="getRelatedBasts(this.value)">
                                	<option value="">انتخاب</option>
                                	@if($deps)
                                    @foreach($deps AS $sub)
                                    	@if($sub_dep_id == $sub->id)
                                        <option value='{!!$sub->id!!}' selected>{!!$sub->name!!}</option>
                                        @else
                                        <option value='{!!$sub->id!!}'>{!!$sub->name!!}</option>
                                        @endif
                                    @endforeach
                                    @endif
                                </select>

                			</div>
                		</div>

                		@if(Auth::user()->id==113 || Auth::user()->id==179 || Auth::user()->id==175)
                		<div class="col-sm-1">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
                				<button onclick="saveEmployeeNewTashkil(<?=$id?>)" class="btn btn-primary" type="button">ثبت تعیینات جدید کارمند</button>
                			</div>
                		</div>
                		@endif

                	</div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                      <div class="col-sm-3">
                  			<div class="col-sm-12">
                  				<label class="col-sm-12 ">عنوان وظیفه <span style="color:red;">*</span></label>
                  				<select name="tashkil" class="form-control" id="tashkil" onchange="bringTashkilDet(this.value)">
                                      <option value=''>انتخاب</option>
                                      @if($tashkils)
                                      	@foreach($tashkils AS $tashkil)
                                      		@if($row->tashkil_id == $tashkil->id)
                                      			<option value="{!!$tashkil->id!!}" selected>{!!$tashkil->name!!}-{!!$tashkil->tainat!!}</option>
                                      		@else
                                      			<option value="{!!$tashkil->id!!}">{!!$tashkil->name!!}-{!!$tashkil->tainat!!}</option>
                                      		@endif
                                      	@endforeach
                                      @endif
                                  </select>
                  			</div>
                  		</div>

                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">شماره تعینات</label>
                                <input class="form-control" type="text" name="number_tayenat" id="number_tayenat" value="{!!$row->tainat!!}" readonly>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">بست</label>
                                <input class="form-control" type="text" id="tashkil_bast" value="{!!$row->bast!!}" readonly>

                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">موقف بست</label>
                                <select name = "position_dr" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1' <?php echo ($row->position_dr=='1' ? 'selected':''); ?>>اصلی</option>
                                        <option value='2' <?php echo ($row->position_dr=='2' ? 'selected':''); ?>>خدمتی</option>
                                        <option value='3' <?php echo ($row->position_dr=='3' ? 'selected':''); ?>>تحصیلی</option>
                                        <option value='4' <?php echo ($row->position_dr=='4' ? 'selected':''); ?>>انتظاربا معاش</option>
                                        <option value='5' <?php echo ($row->position_dr=='5' ? 'selected':''); ?>>عسکری</option>
                                        <option value='6' <?php echo ($row->position_dr=='6' ? 'selected':''); ?>>انتظار بدون معاش</option>
                                        <option value='7' <?php echo ($row->position_dr=='7' ? 'selected':''); ?>>اضافه بست</option>
                                    </select>
                                <span style="color:red">{!!$errors->first('position_dr')!!}</span>
                            </div>
                        </div>

                    </div>
                </div>
                </div>
			    <div class="container-fluid">
                	<div class="row">

                		<div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-4">کارکنان <span style="color:red;">*</span></label>
                                <select name="employee_type" class="form-control" onchange="showRank(this.value)">
                                    <option value=''>انتخاب</option>
                                    <option value='1' <?php echo ($row->employee_type=='1' ? 'selected':''); ?>>مامور</option>
                                    <option value='2' <?php echo ($row->employee_type=='2' ? 'selected':''); ?>>اجیر</option>
                                    <option value='3' <?php echo ($row->employee_type=='3' ? 'selected':''); ?>>نظامی</option>
                                    <option value='4' <?php echo ($row->employee_type=='4' ? 'selected':''); ?>>مامور بالمقطع</option>
                                    <option value='5' <?php echo ($row->employee_type=='5' ? 'selected':''); ?>>اجیر بالمقطع</option>
                                    <option value='6' <?php echo ($row->employee_type=='6' ? 'selected':''); ?>>خدمتی</option>
                                </select>
			               	</div>

			            </div>
                		<div id="bast_div" @if($row->employee_type==2 || $row->employee_type==3) style="display:none;" @endif>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">رتبه</label>
	                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
	                                <select name = "emp_rank" id="emp_rank" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getStaticDropdown('employee_rank',$row->emp_rank)!!}
	                                </select>
	                                <span style="color:red">{!!$errors->first('emp_rank')!!}</span>
				      			</div>
				      		</div>

			      		</div>
			      		<div id="ajir_div" @if($row->employee_type!=2) style="display:none;" @endif>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">درجه اجیر</label>
	                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
	                                <select name = "ajeer_rank" id="emp_rank" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getStaticDropdown('employee_rank',$row->emp_rank)!!}
	                                </select>
	                                <span style="color:red">{!!$errors->first('emp_rank')!!}</span>
				      			</div>
				      		</div>

			      		</div>
			      		<div id="military_div" @if($row->employee_type!=3) style="display:none;" @endif>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">رتبه</label>
	                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
	                                <select name = "military_rank" id="military_rank" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getStaticDropdown('military_rank',$row->emp_rank)!!}
	                                </select>
	                                <span style="color:red">{!!$errors->first('emp_rank')!!}</span>
				      			</div>
				      		</div>

			      		</div>


			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			            		<label class="col-sm-12 ">نوع تقرر</label>
                                <select name = "free_compitition" class="form-control">
                                    <option value=''>انتخاب</option>
                                    @if($row->free_compitition=='جدیدالتقرر')
                                    <option selected value='جدیدالتقرر'>
                                    	جدیدالتقرر
                                    </option>
                                    <option value='سابق'>
                                    	سابق
                                    </option>
                                    <option value='تبدیلی'>
                                    	تبدیلی
                                    </option>
                                    @elseif($row->free_compitition=='سابق')
                                    <option value='جدیدالتقرر'>
                                    	جدیدالتقرر
                                    </option>
                                    <option selected value='سابق'>
                                    	سابق
                                    </option>
                                    <option value='تبدیلی'>
                                    	تبدیلی
                                    </option>
                                    @elseif($row->free_compitition=='تبدیلی')
                                    <option value='جدیدالتقرر'>
                                    	جدیدالتقرر
                                    </option>
                                    <option value='سابق'>
                                    	سابق
                                    </option>
                                    <option selected value='تبدیلی'>
                                    	تبدیلی
                                    </option>
                                    @else
                                    <option value='جدیدالتقرر'>
                                    	جدیدالتقرر
                                    </option>
                                    <option value='سابق'>
                                    	سابق
                                    </option>
                                    <option value='تبدیلی'>
                                    	تبدیلی
                                    </option>
                                    @endif
                                </select>
                                <span style="color:red">{!!$errors->first('free_compitition')!!}</span>
			            	</div>
			            </div>
                	</div>
                </div>

					<div class="container-fluid" >
				      	<div class="row">
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">موقف کارکن</label>
	                                <select name="mawqif_employee" class="form-control" disabled="disabled">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1' <?php echo ($row->mawqif_employee=='1' ? 'selected':''); ?>>برحال</option>
	                                    <option value='2' <?php echo ($row->mawqif_employee=='2' ? 'selected':''); ?>>انتظار بامعاش</option>
	                                    <option value='3' <?php echo ($row->mawqif_employee=='3' ? 'selected':''); ?>>بالمقطع</option>
	                                    <option value='4' <?php echo ($row->mawqif_employee=='4' ? 'selected':''); ?>>درجریان تحصیل</option>
	                                    <option value='5' <?php echo ($row->mawqif_employee=='5' ? 'selected':''); ?>>تقاعد</option>
	                                    <option value='6' <?php echo ($row->mawqif_employee=='6' ? 'selected':''); ?>>منفک</option>
	                                    <option value='7' <?php echo ($row->mawqif_employee=='7' ? 'selected':''); ?>>اضافه بست</option>
                                        <option value='8' <?php echo ($row->mawqif_employee=='8' ? 'selected':''); ?>>تبدیلی</option>
	                                    <option value='9' <?php echo ($row->mawqif_employee=='9' ? 'selected':''); ?>>استعفا</option>

	                                </select>

				      			</div>
				      		</div>
				      		<div class="col-sm-3">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">تاریخ درخواست</label>
	                        		<?php $sdate = $row->first_date_appointment;?>
	                                <input class="form-control datepicker_farsi" readonly type="text" name="first_appointment_date" value="<?php if($row->first_date_appointment !=""){echo $sdate;}?>">
	                                <span style="color:red">{!!$errors->first('first_appointment_date')!!}</span>
	                        	</div>
	                        </div>
	                        <div class="col-sm-6">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">وظیفه فعلی</label>
	                                <input class="form-control" type="text" name="current_position_dr" id="current_position_dr" value="{!!$row->current_position_dr!!}">
	                                <span style="color:red">{!!$errors->first('current_position_dr')!!}</span>
	                			</div>
	                		</div>

				      	</div>
				    </div>
				    <div id="contract" @if($row->employee_type==4 || $row->employee_type==5) style="display:block" @else style="display:none" @endif>
				    	<div class="container-fluid">
		                	<div class="row">
                        <div class="col-sm-3">
                            <div class="col-sm-12">
                                <label class="col-sm-12 ">سال <span style="color:red;">*</span></label>
                                <select id="tashkil_year" required class="form-control">
                                    <?php
                                    $the_date = dateToShamsi(date('Y'),date('m'),date('d'));
                                    $s_date = explode('-',$the_date);
                                    $cyear = $s_date[0]+1;
                                    ?>
                                    @for($i=1396;$i<=$cyear;$i++)
                                        @if($i==$year)
                                        <option value='{!!$i!!}' selected="selected">{!!$i!!}</option>
                                        @else
                                        <option value='{!!$i!!}'>{!!$i!!}</option>
                                        @endif
                                    @endfor
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                            </div>
                        </div>
		                		<div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">ادارۀ عمومی <span style="color:red;">*</span></label>
		                                <select name="general_department1" id="general_department1" class="form-control" onchange="bringRelatedSubDepartment('sub_dep1',this.value)">
		                                        <option value="">انتخاب</option>
		                                    @foreach($parentDeps AS $dep_item)
			                                    @if($dep_id == $dep_item->id)
		                                        <option value='{!!$dep_item->id!!}' selected>{!!$dep_item->name!!}</option>
		                                        @else
		                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
			                                    @endif
		                                    @endforeach
		                                </select>
		                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
		                			</div>
		                		</div>
		                		<div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">ادارۀ مربوط <span style="color:red;">*</span></label>
		                				<select class="form-control" name="sub_dep1" id="sub_dep1">
		                                	<option value="">انتخاب</option>
		                                    @if($deps)
		                                    @foreach($deps AS $sub)
		                                    	@if($sub_dep_id == $sub->id)
		                                        <option value='{!!$sub->id!!}' selected>{!!$sub->name!!}</option>
		                                        @else
		                                        <option value='{!!$sub->id!!}'>{!!$sub->name!!}</option>
		                                        @endif
		                                    @endforeach
		                                    @endif
		                                </select>

		                			</div>
		                		</div>
		                		<div class="col-sm-3">
		                        	<div class="col-sm-12">
		                        		<label class="col-sm-12 ">تاریخ ختم قرارداد <span style="color:red;">*</span></label>
                                        <?php $edate = $row->contract_expire_date;?>
		                                <input class="form-control datepicker_farsi" readonly type="text" name="last_date" value="<?php if($row->contract_expire_date !=""){echo $edate;}?>">
		                        	</div>
		                        </div>

		                	</div>
		                </div>
					</div>
				    <div class="panel-heading">
				      <h5 class="panel-title">معلومات عمومی در مورد کاندید</h5>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">نام <span style="color:red;">*</span></label>
				            		<input class="form-control" type="text" name="name_dr" value="{!!$row->name_dr!!}">
	                                <span style="color:red">{!!$errors->first('name_dr')!!}</span>

				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">تخلص</lable>
				                  	<input type="text" class="form-control" name="last_name" value="{!!$row->last_name!!}" />

				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">نام پدر <span style="color:red;">*</span></lable>
				                  	<input class="form-control" type="text" name="father_name_dr" value="{!!$row->father_name_dr!!}">
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">پدر کلان <span style="color:red;">*</span></lable>
				            		<input type="text" class="form-control" name="grand_father_name" value="{!!$row->grand_father_name!!}"/>
				               	</div>
				            </div>
				      	</div>
				    </div>
					<div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">نمبر تذکره</label>
				            		<input class="form-control" type="text" name="id_no" value="{!!$row->id_no!!}">
	                                <span style="color:red">{!!$errors->first('id_no')!!}</span>

				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">جلد</lable>
				                  	<input type="text" class="form-control" name="id_jild" value="{!!$row->id_jild!!}"/>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">صفحه</lable>
				                  	<input class="form-control" type="text" name="id_page" value="{!!$row->id_page!!}">
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">ثبت</lable>
				            		<input type="text" class="form-control" name="id_sabt" value="{!!$row->id_sabt!!}"/>
				               	</div>
				            </div>
				      	</div>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">قوم <span style="color:red;">*</span></label>
				            		<select name = "nationality" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getStaticDropdown('ethnicity',$row->nationality)!!}
	                                </select>

				               	</div>
				            </div>

				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">جنسیت <span style="color:red;">*</span></label>

	                                <select name="gender" required="required" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='M' <?php echo ($row->gender=='M' ? 'selected':''); ?>>مرد</option>
	                                    <option value='F' <?php echo ($row->gender=='F' ? 'selected':''); ?>>زن</option>
	                                </select>

				            	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				            		<label class="col-sm-12 ">گروپ خون <span style="color:red;">*</span></label>
	                                <select name = "blood_group" id="blood_group" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    {!!getStaticDropdown('blood_group',$row->blood_group)!!}
	                                </select>
				            	</div>
				            </div>
				            <div class="col-sm-3">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">سال تولد</lable>
	                                <?php
	                                     $current_year = date('Y')-621;

	                                ?>
	                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
	                                <select name = "birth_year" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <?php

	                                        for($i=$current_year-84;$i<=$current_year-10;$i++)
	                                        {
	                                        	if($i == $row->birth_year)
												{
	                                            	echo "<option value='".$i."' selected='selected'>".$i."</option>";
												}
												else
												{
													echo "<option value='".$i."'>".$i."</option>";
												}
	                                   }
	                                    ?>
	                                </select>
				               	</div>
				            </div>
				      	</div>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">&nbsp;</label>
				      				<h5>سکونت اصلی</h5>
				      			</div>
				      		</div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ولایت</label>
				                  	<?php $page=URL::route('getProvinceDistrict'); ?>
	                                <select class="form-control" name="original_province" id="original_province" onchange="getProvinceDistrict('{!!$page!!}','districts',this.value);">
	                                    <option value=''>انتخاب</option>
	                                    @foreach($provinces AS $pro)
	                                    	@if($pro->id == $row->original_province)
	                                        	<option value='{!!$pro->id!!}' selected="selected">{!!$pro->name!!}</option>
	                                        @else
	                                        	<option value='{!!$pro->id!!}'>{!!$pro->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ولسوالی</lable>
				              		<select name = "district" id="districts" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    @foreach($org_districts AS $org_dist)
	                                    	@if($org_dist->id == $row->district)
	                                        	<option value='{!!$org_dist->id!!}' selected="selected">{!!$org_dist->name!!}</option>
	                                        @else
	                                        	<option value='{!!$org_dist->id!!}'>{!!$org_dist->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>

				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ناحیه</lable>
				                  	<input type="text" class="form-control" name="village" value="{!!$row->village!!}" />
				               	</div>
				            </div>
				      	</div>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				      		<div class="col-sm-12">
				      				<label class="col-sm-12 ">&nbsp;</label>
				      				<h5>سکونت فعلی</h5>
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ولایت</label>
				                  	<?php $page=URL::route('getProvinceDistrict'); ?>
	                                <select class="form-control" name="current_province" id="current_province" onchange="getProvinceDistrict('{!!$page!!}','current_districts',this.value);">
	                                    <option value=''>انتخاب</option>
	                                    @foreach($provinces AS $pro)
	                                        @if($pro->id == $row->current_province)
	                                        	<option value='{!!$pro->id!!}' selected="selected">{!!$pro->name!!}</option>
	                                        @else
	                                        	<option value='{!!$pro->id!!}'>{!!$pro->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ولسوالی</lable>
				              		<select name = "current_district" id="current_districts" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    @foreach($cur_districts AS $cur_dist)
	                                    	@if($cur_dist->id == $row->current_district)
	                                        	<option value='{!!$cur_dist->id!!}' selected="selected">{!!$cur_dist->name!!}</option>
	                                        @else
	                                        	<option value='{!!$cur_dist->id!!}'>{!!$cur_dist->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>

				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ناحیه</lable>
				                  	<input type="text" class="form-control" name="current_village" value="{!!$row->current_village!!}" />
				               	</div>
				            </div>

				      	</div>
				    </div>
				    <?php $edu_count =0;?>
				    @if(canEdit('hr_documents'))
				    @if($educations)
				    	@foreach($educations as $edu)
				    	<?php $edu_count++;?>
						    <div class="container-fluid">
						      	<div class="row">
						      		<div class="col-sm-2">
						              	<div class="col-sm-12">
						              		<label class="col-sm-12 ">سویه تحصیلی</label>
						            		<select name = "education_degree_{!!$edu_count!!}" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    {!!getStaticDropdown('education_degree',$edu->education_id)!!}
			                                </select>
						               	</div>
						            </div>

						            <div class="col-sm-2">
						            	<div class="col-sm-12">
						            		<label class="col-sm-12 ">رشته تحصیلی</label>
			                                <input class="form-control" type="text" name="education_field_{!!$edu_count!!}" value="{!!$edu->education_field!!}">
						            	</div>
						            </div>
						            <div class="col-sm-3">
						            	<div class="col-sm-12">
						              		<label class="col-sm-12 ">موسسه تحصیلی</lable>
						                  	<input type="text" class="form-control" name="education_place_{!!$edu_count!!}" value="{!!$edu->education_place!!}"/>
						               	</div>
						            </div>
						            <div class="col-sm-2">
						            	<div class="col-sm-12">
						            		<label class="col-sm-12 ">محل تحصیل</label>
			                                <select name = "edu_location_{!!$edu_count!!}" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    {!!getStaticDropdown('countries',$edu->edu_location)!!}
			                                </select>
						            	</div>
						            </div>
						            <div class="col-sm-2">
						            	<div class="col-sm-12">
						              		<label class="col-sm-12 ">سال فراغت</label>
						                  	<?php
			                                     $current_year = date('Y')-621;

			                                ?>
			                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
			                                <select name = "graduation_year_{!!$edu_count!!}" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    <?php

			                                        for($i=$current_year-84;$i<=$current_year;$i++)
			                                        {
			                                            if($i == $edu->graduation_year)
														{
			                                            	echo "<option value='".$i."' selected='selected'>".$i."</option>";
														}
														else
														{
															echo "<option value='".$i."'>".$i."</option>";
														}
			                                   }
			                                    ?>
			                                </select>
						               	</div>
						            </div>
						      	</div>
						    </div>
						@endforeach
						@if($edu_count<2)
							<div class="container-fluid">
						      	<div class="row">
						      		<div class="col-sm-2">
						              	<div class="col-sm-12">
						              		<label class="col-sm-12 ">سویه تحصیلی</label>
						            		<select name = "education_degree_2" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    {!!getStaticDropdown('education_degree',0)!!}
			                                </select>
						               	</div>
						            </div>

						            <div class="col-sm-2">
						            	<div class="col-sm-12">
						            		<label class="col-sm-12 ">رشته تحصیلی</label>
			                                <input class="form-control" type="text" name="education_field_2" value="">
						            	</div>
						            </div>
						            <div class="col-sm-3">
						            	<div class="col-sm-12">
						              		<label class="col-sm-12 ">موسسه تحصیلی</lable>
						                  	<input type="text" class="form-control" name="education_place_2" value=""/>
						               	</div>
						            </div>
						            <div class="col-sm-2">
						            	<div class="col-sm-12">
						            		<label class="col-sm-12 ">محل تحصیل</label>
			                                <select name = "edu_location_2" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    {!!getStaticDropdown('countries',0)!!}
			                                </select>
						            	</div>
						            </div>
						            <div class="col-sm-2">
						            	<div class="col-sm-12">
						              		<label class="col-sm-12 ">سال فراغت</label>
						                  	<?php
			                                     $current_year = date('Y')-621;

			                                ?>
			                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
			                                <select name = "graduation_year_2" class="form-control">
			                                    <option value=''>انتخاب</option>
			                                    <?php

		                                        for($i=$current_year-84;$i<=$current_year;$i++)
		                                        {
		                                            echo "<option value='".$i."'>".$i."</option>";
			                                   	}
			                                    ?>
			                                </select>
						               	</div>
						            </div>
						      	</div>
						    </div>
						@endif
				    @else
				    	<div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-2">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">سویه تحصیلی</label>
					            		<select name = "education_degree_1" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('education_degree',0)!!}
		                                </select>
					               	</div>
					            </div>

					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					            		<label class="col-sm-12 ">رشته تحصیلی</label>
		                                <input class="form-control" type="text" name="education_field_1" value="">
					            	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">موسسه تحصیلی</lable>
					                  	<input type="text" class="form-control" name="education_place_1" value=""/>
					               	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					            		<label class="col-sm-12 ">محل تحصیل</label>
		                                <select name = "edu_location_1" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('countries',0)!!}
		                                </select>
					            	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">سال فراغت</label>
					                  	<?php
		                                     $current_year = date('Y')-621;

		                                ?>
		                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
		                                <select name = "graduation_year_1" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <?php

	                                        for($i=$current_year-84;$i<=$current_year;$i++)
	                                        {
	                                            echo "<option value='".$i."'>".$i."</option>";
		                                   	}
		                                    ?>
		                                </select>
					               	</div>
					            </div>
					      	</div>
					    </div>
					    <div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-2">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">سویه تحصیلی</label>
					            		<select name = "education_degree_2" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('education_degree',0)!!}
		                                </select>
					               	</div>
					            </div>

					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					            		<label class="col-sm-12 ">رشته تحصیلی</label>
		                                <input class="form-control" type="text" name="education_field_2" value="">
					            	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">موسسه تحصیلی</lable>
					                  	<input type="text" class="form-control" name="education_place_2" value=""/>
					               	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					            		<label class="col-sm-12 ">محل تحصیل</label>
		                                <select name = "edu_location_2" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getStaticDropdown('countries',0)!!}
		                                </select>
					            	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">سال فراغت</label>
					                  	<?php
		                                     $current_year = date('Y')-621;

		                                ?>
		                                <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
		                                <select name = "graduation_year_2" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <?php

	                                        for($i=$current_year-84;$i<=$current_year;$i++)
	                                        {
	                                            echo "<option value='".$i."'>".$i."</option>";
		                                   	}
		                                    ?>
		                                </select>
					               	</div>
					            </div>
					      	</div>
					    </div>
				    @endif
				    @endif
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-6">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">تیلفون</label>
				            		<input class="form-control" type="text" name="phone" value="{!!$row->phone!!}">
				               	</div>
				            </div>

				            <div class="col-sm-6">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">آدرس الکترونیکی</label>
				                  	<input type="text" name="email" class="form-control" value="{!!$row->email!!}">
				               	</div>
				            </div>


				      	</div>
				    </div>
                    <!--
					<div class="panel-heading">
				      <h5 class="panel-title">معلومات در مورد تجارب کاری</h5>
				    </div>
				    @if($experiences)
				    <?php $i=1;$j=3; ?>
				    	@foreach($experiences AS $exp)
				    	<div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-3">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">ارگان/موسسه</lable>
					                  	<input type="text" value="{!!$exp->organization!!}" class="form-control" name="experience_company_{!!$i!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">عنوان بست</label>
					                  	<input type="text" value="{!!$exp->position!!}" class="form-control" name="experience_position_{!!$i!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">بست/رتبه</lable>
					                  	<input type="text" value="{!!$exp->bast!!}" class="form-control" name="experience_bast_{!!$i!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">دلایل ترک وظیفه</lable>
					                  	<input type="text" value="{!!$exp->leave_reason!!}" class="form-control" name="experience_leave_{!!$i!!}" />
					               	</div>
					            </div>
					      	</div>
					    </div>
				    	<?php $i++;$j--;?>
				    	@endforeach
				    	@for($no=1;$no<=$j;$no++)
				   		<div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-3">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">ارگان/موسسه</lable>
					                  	<input type="text" class="form-control" name="experience_company_{!!$i!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">عنوان بست</label>
					                  	<input type="text" class="form-control" name="experience_position_{!!$i!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">بست/رتبه</lable>
					                  	<input type="text" class="form-control" name="experience_bast_{!!$i!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">دلایل ترک وظیفه</lable>
					                  	<input type="text" class="form-control" name="experience_leave_{!!$i!!}" />
					               	</div>
					            </div>
					      	</div>
					    </div>
					    <?php $i++;?>
				   		@endfor
				   	@else


				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ارگان/موسسه</lable>
				                  	<input type="text" class="form-control" name="experience_company_1" />
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">عنوان بست</label>
				                  	<input type="text" class="form-control" name="experience_position_1" />
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">بست/رتبه</lable>
				                  	<input type="text" class="form-control" name="experience_bast_1" />
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">دلایل ترک وظیفه</lable>
				                  	<input type="text" class="form-control" name="experience_leave_1" />
				               	</div>
				            </div>
				      	</div>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="experience_company_2" />
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</label>
				                  	<input type="text" class="form-control" name="experience_position_2" />
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="experience_bast_2" />
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="experience_leave_2" />
				               	</div>
				            </div>
				      	</div>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="experience_company_3" />
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</label>
				                  	<input type="text" class="form-control" name="experience_position_3" />
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="experience_bast_3" />
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="experience_leave_3" />
				               	</div>
				            </div>
				      	</div>
				    </div>
				    @endif
				    <div class="panel-heading">
				      <h5 class="panel-title">فهم و توانایی در لسان ها</h5>
				    </div>
				    @if($langs)
				    <?php $i=1;$j=4; ?>
				    	@foreach($langs AS $lang)
				    	<div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-2">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">زبان</lable>
					                  	<input type="text" class="form-control" name="lang_{!!$i!!}" value="{!!$lang->language!!}" placeholder="دری" readonly="readonly" />
					               	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">تحریر</label>
					                  	<select name = "writing_{!!$i!!}" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <option value='1' <?php echo ($lang->writing=='1' ? 'selected':''); ?>>عالی</option>
		                                    <option value='2' <?php echo ($lang->writing=='2' ? 'selected':''); ?>>خوب</option>
		                                    <option value='3' <?php echo ($lang->writing=='3' ? 'selected':''); ?>>متوسط</option>
		                                    <option value='4' <?php echo ($lang->writing=='4' ? 'selected':''); ?>>ابتدایی</option>
		                                    <option value='5' <?php echo ($lang->writing=='5' ? 'selected':''); ?>>هیچکدام</option>
		                                </select>
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">تکلم</lable>
					                  	<select name = "speaking_{!!$i!!}" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <option value='1' <?php echo ($lang->speaking=='1' ? 'selected':''); ?>>عالی</option>
		                                    <option value='2' <?php echo ($lang->speaking=='2' ? 'selected':''); ?>>خوب</option>
		                                    <option value='3' <?php echo ($lang->speaking=='3' ? 'selected':''); ?>>متوسط</option>
		                                    <option value='4' <?php echo ($lang->speaking=='4' ? 'selected':''); ?>>ابتدایی</option>
		                                    <option value='5' <?php echo ($lang->speaking=='5' ? 'selected':''); ?>>هیچکدام</option>
		                                </select>
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">خوانش</lable>
					                  	<select name = "reading_{!!$i!!}" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <option value='1' <?php echo ($lang->reading=='1' ? 'selected':''); ?>>عالی</option>
		                                    <option value='2' <?php echo ($lang->reading=='2' ? 'selected':''); ?>>خوب</option>
		                                    <option value='3' <?php echo ($lang->reading=='3' ? 'selected':''); ?>>متوسط</option>
		                                    <option value='4' <?php echo ($lang->reading=='4' ? 'selected':''); ?>>ابتدایی</option>
		                                    <option value='5' <?php echo ($lang->reading=='5' ? 'selected':''); ?>>هیچکدام</option>
		                                </select>
					               	</div>
					            </div>
					             <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">تایپینگ</lable>
					                  	<select name = "typing_{!!$i!!}" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <option value='1' <?php echo ($lang->typing=='1' ? 'selected':''); ?>>عالی</option>
		                                    <option value='2' <?php echo ($lang->typing=='2' ? 'selected':''); ?>>خوب</option>
		                                    <option value='3' <?php echo ($lang->typing=='3' ? 'selected':''); ?>>متوسط</option>
		                                    <option value='4' <?php echo ($lang->typing=='4' ? 'selected':''); ?>>ابتدایی</option>
		                                    <option value='5' <?php echo ($lang->typing=='5' ? 'selected':''); ?>>هیچکدام</option>
		                                </select>
					               	</div>
					            </div>
					      	</div>
					    </div>
					    <?php $i++;$j--;?>
				    	@endforeach
				    	@for($no=1;$no<=$j;$no++)
				    	<div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-2">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">زبان</lable>
					                  	<input type="text" class="form-control" name="lang_{!!$i!!}" value="da" placeholder="دری" readonly="readonly" />
					               	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">تحریر</label>
					                  	<select name = "writing_{!!$i!!}" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <option value='1'>عالی</option>
		                                    <option value='2'>خوب</option>
		                                    <option value='3'>متوسط</option>
		                                    <option value='4'>ابتدایی</option>
		                                    <option value='5'>هیچکدام</option>
		                                </select>
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">تکلم</lable>
					                  	<select name = "speaking_{!!$i!!}" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <option value='1'>عالی</option>
		                                    <option value='2'>خوب</option>
		                                    <option value='3'>متوسط</option>
		                                    <option value='4'>ابتدایی</option>
		                                    <option value='5'>هیچکدام</option>
		                                </select>
					               	</div>
					            </div>
					            <div class="col-sm-3">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">خوانش</lable>
					                  	<select name = "reading_{!!$i!!}" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <option value='1'>عالی</option>
		                                    <option value='2'>خوب</option>
		                                    <option value='3'>متوسط</option>
		                                    <option value='4'>ابتدایی</option>
		                                    <option value='5'>هیچکدام</option>
		                                </select>
					               	</div>
					            </div>
					             <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">تایپینگ</lable>
					                  	<select name = "typing_{!!$i!!}" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    <option value='1'>عالی</option>
		                                    <option value='2'>خوب</option>
		                                    <option value='3'>متوسط</option>
		                                    <option value='4'>ابتدایی</option>
		                                    <option value='5'>هیچکدام</option>
		                                </select>
					               	</div>
					            </div>
					      	</div>
					    </div>
				    	@endfor
				    @else
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-2">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">زبان</lable>
				                  	<input type="text" class="form-control" name="lang_1" value="da" placeholder="دری" readonly="readonly" />
				               	</div>
				            </div>
				            <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">تحریر</label>
				                  	<select name = "writing_1" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">تکلم</lable>
				                  	<select name = "speaking_1" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">خوانش</lable>
				                  	<select name = "reading_1" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				             <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">تایپینگ</lable>
				                  	<select name = "typing_1" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				      	</div>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-2">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="lang_2" value="pa" placeholder="پشتو" readonly="readonly" />
				               	</div>
				            </div>
				            <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</label>
				                  	<select name = "writing_2" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<select name = "speaking_2" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<select name = "reading_2" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				             <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<select name = "typing_2" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				      	</div>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-2">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="lang_3" value="en" placeholder="انگلیسی" readonly="readonly" />
				               	</div>
				            </div>
				            <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</label>
				                  	<select name = "writing_3" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<select name = "speaking_3" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<select name = "reading_3" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				             <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<select name = "typing_3" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				      	</div>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-2">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="lang_4" value="other" placeholder="دیگر" />
				               	</div>
				            </div>
				            <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</label>
				                  	<select name = "writing_4" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<select name = "speaking_4" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<select name = "reading_4" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				             <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<select name = "typing_4" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1'>عالی</option>
	                                    <option value='2'>خوب</option>
	                                    <option value='3'>متوسط</option>
	                                    <option value='4'>ابتدایی</option>
	                                    <option value='5'>هیچکدام</option>
	                                </select>
				               	</div>
				            </div>
				      	</div>
				    </div>
				    @endif
				    <div class="panel-heading">
				      <h5 class="panel-title">مشخصات و مراجع معلومات اشخاص بر حال وظیفه</h5>
				    </div>
				    @if($garantee)
				    <?php $g=1;$j=2; ?>
				    	@foreach($garantee AS $gr)
				    	<div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-4">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">اسم و تخلص</lable>
					                  	<input type="text" value="{!!$gr->name!!}" class="form-control" name="garantee_name_{!!$g!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">وظیفه</label>
					                  	<input type="text" class="form-control" value="{!!$gr->job!!}" name="garantee_job_{!!$g!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">اداره مربوطه</lable>
					                  	<input type="text" class="form-control" value="{!!$gr->organization!!}" name="garantee_company_{!!$g!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">تیلفون</lable>
					                  	<input type="text" class="form-control" value="{!!$gr->phone!!}" name="garantee_phone_{!!$g!!}" />
					               	</div>
					            </div>
					             <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">آدرس الکترونیکی</lable>
					                  	<input type="text" class="form-control" value="{!!$gr->email!!}" name="garantee_email_{!!$g!!}" />
					               	</div>
					            </div>
					      	</div>
					    </div>

				    	<?php $g++;$j--;?>
				    	@endforeach
				    	@for($no=1;$no<=$j;$no++)
				   		<div class="container-fluid">
					      	<div class="row">
					      		<div class="col-sm-4">
					              	<div class="col-sm-12">
					              		<label class="col-sm-12 ">اسم و تخلص</lable>
					                  	<input type="text" class="form-control" name="garantee_name_{!!$g!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">وظیفه</label>
					                  	<input type="text" class="form-control" name="garantee_job_{!!$g!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">اداره مربوطه</lable>
					                  	<input type="text" class="form-control" name="garantee_company_{!!$g!!}" />
					               	</div>
					            </div>
					            <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">تیلفون</lable>
					                  	<input type="text" class="form-control" name="garantee_phone_{!!$g!!}" />
					               	</div>
					            </div>
					             <div class="col-sm-2">
					            	<div class="col-sm-12">
					              		<label class="col-sm-12 ">آدرس الکترونیکی</lable>
					                  	<input type="text" class="form-control" name="garantee_email_{!!$g!!}" />
					               	</div>
					            </div>
					      	</div>
					    </div>
					    <?php $g++;?>
				   		@endfor
				   	@else
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-4">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">اسم و تخلص</lable>
				                  	<input type="text" class="form-control" name="garantee_name_1"  />
				               	</div>
				            </div>
				            <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">وظیفه</label>
				                  	<input type="text" class="form-control" name="garantee_job_1" />
				               	</div>
				            </div>
				            <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">اداره مربوطه</lable>
				                  	<input type="text" class="form-control" name="garantee_company_1" />
				               	</div>
				            </div>
				            <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">تیلفون</lable>
				                  	<input type="text" class="form-control" name="garantee_phone_1" />
				               	</div>
				            </div>
				             <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">آدرس الکترونیکی</lable>
				                  	<input type="text" class="form-control" name="garantee_email_1" />
				               	</div>
				            </div>
				      	</div>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-4">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="garantee_name_2" />
				               	</div>
				            </div>
				            <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</label>
				                  	<input type="text" class="form-control" name="garantee_job_2" />
				               	</div>
				            </div>
				            <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="garantee_company_2" />
				               	</div>
				            </div>
				            <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="garantee_phone_2" />
				               	</div>
				            </div>
				             <div class="col-sm-2">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">&nbsp;</lable>
				                  	<input type="text" class="form-control" name="garantee_email_2" />
				               	</div>
				            </div>
				      	</div>
				    </div>
					@endif
            	</div>

	            -->
				<br/>
                <div class="container-fluid">
	                <div class="row">
	                	<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
	                    		@if(canEdit('hr_recruitment'))
		                        <button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    	<p>You dont have permission</p>
		                    	@endif
		                        <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
		                    </div>
                    	</div>
                	</div>
                </div>
                {!! Form::token() !!}
            </form>
        </div>
    </div>

<script type="text/javascript">
$(".datepicker_farsi").persianDatepicker();
    $(document).on("change","#files",function(){
        var count = $('#files_div').find('input').length;
        count = count+1;
        $("<div id='div_"+count+"' style='display:inline;'><input style='splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div');
    });
    function getRelatedBasts(value)
    {
    	var type = '{!!$row->tashkil_id!!}';
      var year = $('#tashkil_year').val();
    	$.ajax({
            url: '{!!URL::route("bringRelatedBastViaAjax")!!}',
            type: 'post',
            data: 'id='+value+'&type='+type+'&year='+year,
            dataType: 'html',
            success:function(respones){
                $('#tashkil').html(respones);
            }
        });
    }
	function bringTashkilDet(value)
    {
        $.ajax({
            url: '{!!URL::route("bringTashkilDetViaAjax")!!}',
            type: 'post',
            data: '&id='+value,
            dataType: 'json',
            success:function(respones){
                $('#tashkil_bast').val(respones.bast);
                $('#number_tayenat').val(respones.number);
               //$('#tashkil_sub_dep').val(respones.sub_dep);
            }
        });
    }
    function changeDiv(value)
    {
        if(value==1)
        {
            $('#vacant_div').slideDown()
            $('#exist_div').slideUp()
        }
        else
        {
            $('#vacant_div').slideUp()
            $('#exist_div').slideDown()
        }
    }

    function getProvinceDistrict(page,ele,value)
    {

        $.ajax({
            url: page,
            type: 'post',
            data: 'province='+value,
            dataType: 'html',
            success:function(respones){
                $('#'+ele).html(respones);
            }
        });

    }
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function saveEmployeeNewTashkil(id)
    {
    	var no 		= $('#number_tayenat').val();
    	var dep 	= $('#general_department').val();
    	var sub_dep = $('#sub_dep').val();
    	var tashkil = $('#tashkil').val();
    	var position= $('#current_position_dr').val();
        $.ajax({
                url: '{!!URL::route("saveEmployeeNewTashkil")!!}',
                data: '&no='+no+'&dep='+dep+'&sub_dep='+sub_dep+'&tashkil='+tashkil+'&id='+id+'&position='+position,
                type: 'post',

                success: function(r)
                {
                    alert('updated');return;
                }
            }
        );
    }
    function showRank(value)
    {
    	if(value == 4 || value == 5 || value == 6)
        {//contract
            $('#exist_div').slideUp();
            $('#contract').slideDown();
            $('#general_department1').prop("required",true);
        	$('#sub_dep1').prop("required",true);
        	$('#general_department').prop("required",false);
        	$('#sub_dep').prop("required",false);
        }
        else
        {//exist div
        	$('#general_department1').prop("required",false);
        	$('#sub_dep1').prop("required",false);
        	$('#general_department').prop("required",true);
        	$('#sub_dep').prop("required",true);

        	$('#contract').slideUp();
            $('#exist_div').slideDown();

	        if(value == 3)
	        {//military
	            $('#ajir_div').slideUp();
	            $('#bast_div').slideUp();
	            $('#military_div').slideDown();

	        }
	        else if(value == 2)
	        {//ajir
	        	$('#ajir_div').slideDown();
	            $('#military_div').slideUp();
	            $('#bast_div').slideUp();
	        }
	        else
	        {
	            $('#ajir_div').slideUp();
	            $('#military_div').slideUp();
	            $('#bast_div').slideDown();
	        }
        }
    }
</script>
