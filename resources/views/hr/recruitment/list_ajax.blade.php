<div class="table-responsive">
    <table class="table table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>نام کامل</th>
            <th>ولد</th>
            <th>شماره تعینات</th>
            <th>دیپارتمنت</th>
            <th>سکونت اصلی</th>
            
            
            <th>بست</th>
            <th>تاریخ تقرر</th>
            <th>عملیه</th>
        </tr>
        </thead>
        <tbody>
        @if($records)
        	@foreach($records AS $rec)
        	<tr>
        	<td>{!!$rec->id!!}</td>
        	<td>{!!$rec->fullname!!}</td>
        	<td>{!!$rec->father_name!!}</td>
        	<td>{!!$rec->number_tayenat!!}</td>
        	<td>{!!$rec->department!!}</td>
        	<td>{!!$rec->original_province!!}</td>
        	<td>{!!$rec->bast!!}</td>
        	<td>{!!$rec->emp_date!!}</td>
        	<td>
        		@if(canEdit('hr_recruitment') || canEdit('hr_documents') || canView('hr_documents'))
					<a href="{!!URL::route('getDetailsEmployee',Crypt::encrypt($rec->id))!!}" class="table-link" style="text-decoration:none;" title="Edit"><i class="icon fa-edit" aria-hidden="true" style="font-size: 16px;"></i>
					</a>
				@endif
        	</td>
        	</tr>
        	@endforeach
        @endif
        </tbody>
    </table>
</div>
<div class="dataTables_paginate paging_simple_numbers noprint" id="list_paginate">
  @if(!empty($records))
  {!!$records->render()!!}
  @endif
</div>
<script type="text/javascript">

$(document).ready(function() {
	
	$('.pagination a').on('click', function(event) {
		event.preventDefault();
		if ($(this).attr('href') != '#') {
			//$('#ajaxContent').load($(this).attr('href'));
			var dataString = $('#search_form').serialize();
			dataString += "&page="+$(this).text()+"&ajax="+1;
			$.ajax({
	                url: '{!!URL::route("getRecruitment")!!}',
	                data: dataString,
	                type: 'get',
	                beforeSend: function(){
	                    //$("body").show().css({"opacity": "0.5"});
	                    $('#searchresult').html('<span style="float:center;margin-left:500px;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
	                },
	                success: function(response)
	                {
	                    $('#searchresult').html(response);
	                }
	            }
	        );
		
		}
	});

});

</script>