
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postEmployeeSalary',$employee_id)!!}" >
			 
	            <div class="panel-heading">
			      <h5 class="panel-title">جزییات معاش</h5>
			      <div style='display:none;' class="alert alert-danger" id='barcode_validate_div'></div>
			    </div>
			    <div class="container-fluid">
			    	<div class="row">
			    		<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">معاش اصلی</label>
                                <input class="form-control barcode_field" type="text" name="main_salary" value="@if($row) {!!$row->main_salary!!} @else 0 @endif">
                                
                			</div>
                		</div>
                        <div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">معاش امتیازی</label>
                                <input class="form-control barcode_field" type="text" name="salary" value="@if($row) {!!$row->extra_salary!!} @else 0 @endif">
                                
                			</div>
                		</div>
                		<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">معاش مسلکی</label>
                                <input class="form-control barcode_field" type="text" name="profesional_salary" value="@if($row) {!!$row->prof_salary!!} @else 0 @endif">
                                
                			</div>
                		</div>
                	</div>
			    </div>
			    <div class="container-fluid">
			    	<div class="row">
			    		<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">معاش کدری</label>
                                <input class="form-control barcode_field" type="text" name="kadri_salary" value="@if($row) {!!$row->kadri_salary!!} @else 0 @endif">
                                
                			</div>
                		</div>
                		<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">درجه</label>
                                <select name="grade" class="form-control">
                                <?php $selectedA = '';$selectedB = '';$selectedC = ''; ?>
                                @if($row)
                                	@if($row->grade == 'A') {!!$selectedA = 'selected'!!} @endif 
                                	@if($row->grade == 'B') {!!$selectedB = 'selected'!!} @endif
                                	@if($row->grade == 'C') {!!$selectedC = 'selected'!!} @endif
                                @endif
                                	<option value="">انتخاب</option>
                                	<option value="A" {!!$selectedA!!}>A</option>
                                	<option value="B" {!!$selectedB!!}>B</option>
                                	<option value="C" {!!$selectedC!!}>C</option>
                                </select>
                			</div>
                		</div>
                		<div class="col-sm-4">
	                		<div class="col-sm-6">
	                    		<label class="col-sm-2 ">&nbsp;</label>
	                    		@if(canAdd('hr_documents'))
		                        	<button class="btn btn-primary form-control" type="submit">ثبت معلومات</button>
		                    	@else
		                    		<p>You dont have permission</p>
		                    	@endif
		                       
		                    </div>
                    	</div>
			    	</div>
			    </div>
	           
                {!! Form::token() !!}
            </form>
        </div>
    </div>
<script type="text/javascript">

$(document).ready(function() {
    $(".barcode_field").on("keypress", function(event) {

        var englishAlphabetDigitsAndWhiteSpace = /[0-9 -]/g;
       
        var key = String.fromCharCode(event.which);
        
       if (event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 37 || event.keyCode == 39 || englishAlphabetDigitsAndWhiteSpace.test(key)) {
            $("#barcode_validate_div").hide();
            return true;
        }

         $("#barcode_validate_div").show();
         $("#barcode_validate_div").html("Only English Numbers please!");

        return false;
    });

    $('.barcode_field').on("paste",function(e)
    {
        e.preventDefault();
    });
});
</script>


