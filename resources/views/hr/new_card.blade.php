<html>
	<head>
		<meta charset="UTF-8">
		<style type="text/css">
			* {line-height: 1.2em;
				padding: 0px;
				margin: 0px;
			}

			.front{
				height: 5.4cm;
				width:8.5525cm;
				flo/at: left;
				position:relative;
				font-family: 'B Nazanin';

				overflow: hidden;

			}

			.back{
				height: 5.4cm;
				width:8.5525cm;
				flo/at: right;
				position:relative;
				font-family: 'Times New Roman';
				overflow: hidden;
			}

	        .front img {
	            height:5.4cm;
				width:8.5725cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .back img {
	            height:5.4cm;
				width:8.5725cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .do-print{
	        	font-family: 'B Nazanin';
            	font-size: 12pt;
            	color: white;
	        }
	        .front .serial{
	        	position: absolute;
	        	z-index: 10;
	        	font-size: 7pt;
	        	right:0.38cm;
	        	top:4.9cm;
	        	

	        }
	        .front .phone{
	        	position: absolute;
	        	z-index: 10;
	        	font-size: 7pt;
	        	right:2.7cm;
	        	top:4.9cm;
	        	direction: ltr;
	        }
	        .front .group {
	        	position: absolute;
	        	z-index: 10;
	        	top:2.6cm;
	        	
	        	right: 3cm;
	        }
	        .front td {
	        	height: auto;
	        	vertical-align: middle;
	        }
	        .front .validaty {
	        	position: absolute;
	        	z-index: 10;
	        	left:1.7cm;
	        	top: 4.9cm;
	        	font-size: 8pt;
	        	font-weight: bold;
	        	color: #BDA44A;

	        }
	        .front .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	left:0.3cm;
	        	top: 4.9cm;
	        	font-size: 8pt;
	        	font-weight: bold;
	        	color: white;

	        }
	        
	        .front .photo {
	        	position: absolute;
	        	width: 2.3371cm;
	        	height: 2.9213cm;
	        	top: 1.5cm;
	        	right: 0.3cm;
	        	z-index: 10;
	        }

	        .front .name{
	        	position: absolute;
	        	z-index: 10;
	        	top: 1.7cm;
	        	right: 3cm;
	        	font-size: 8pt;
	        	font-weight: bold;
	        	color: #BDA44A;

	        }
	        .front .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 2cm;
	        	right: 3cm;
	        	
	        	font-size: 10pt;
	        	font-weight: bold;

	        }
	        .front .title{
	        	position: absolu/te;
	        	z-index: 10;
	        	to/p: 1.5861cm;
	        	right: 0.4325cm;
	        	font-size: 8pt;
	        	display: inline;
	        	font-weight: bold;
	        	color: #BDA44A;

	        }
	        .front .title_value{
	        	position: absolu/te;
	        	z-index: 10;
	        	top: 0.9cm;
	        	right: 0cm;
	        	width: 5cm;
	        	padding-bottom: 3px;
	        	font-size: 10pt;
	        	font-weight: bold;
	        	color: white;
	        	
	        }
	        .front .dep{
	        	position: abso/lute;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	right: 0.4325cm;
	        	font-size: 8pt;
	        	display: inline;
	        	font-weight: bold;
	        	color: #BDA44A;

	        }
	        .front .dep_value{
	        	position: absolu/te;
	        	z-index: 10;
	        	top: 3.7cm;
	        	right: 3cm;
	        	width: 5cm;
	        	padding-bottom: 3px;
	        	font-size: 10pt;
	        	font-weight: bold;
	        	display: inline-block;
	        	color: white;
	        }

	        .front .blood{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.5cm;
	        	right: 0.3894cm;
	        	font-size: 8pt;
	        	font-weight: bold;
	        	color: #BDA44A;

	        }
	        .front .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.5cm;
	        	right: 1.6cm;
	        	
	        	font-size: 10pt;
	        	font-weight: bold;
	        	color: white;

	        }

	        .back .group {
	        	position: absolute;
	        	z-index: 10;
	        	top:2.6cm;
	        	
	        	left: 3cm;
	        }
	        .back td {
	        	height: auto;
	        	vertical-align: middle;

	        }
	        .back .serial{
	        	position: absolute;
	        	z-index: 10;
	        	font-size: 7pt;
	        	left:0.38cm;
	        	top:4.9cm;

	        }
	        .back .phone{
	        	position: absolute;
	        	z-index: 10;
	        	font-size: 7pt;
	        	left:2.7cm;
	        	top:4.9cm;

	        }
	        .back .validaty {
	        	position: absolute;
	        	z-index: 10;
	        	right:1.7cm;
	        	top:4.9cm;
	        	font-size: 8pt;
	        	font-weight: bold;
	        	color: #BDA44A;

	        }
	        .back .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	right:0.3cm;
	        	top:4.9cm;
	        	font-size: 8pt;
	        	font-weight: bold;
	        	color: white;

	        }
	        
			
	        .back .photo {
	        	position: absolute;
	        	width: 2.3371cm;
	        	height: 2.9213cm;
	        	top: 1.5cm;
	        	left: 0.38cm;
	        	z-index: 10;
	        }

	        .back .name{
	        	position: absolute;
	        	z-index: 10;
	        	top: 1.7cm;
	        	left: 3cm;
	        	font-size: 8pt;
	        	font-weight: bold;
	        	color: #BDA44A;

	        }
	        .back .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 2cm;
	        	
	        	left: 3cm;
	        	font-size: 10pt;
	        	font-weight: bold;
	        	color: white;

	        }
	        
	        .back .title{
	        	
	        	z-index: 10;
	        	to/p: 1.5861cm;
	        	left: 0.4325cm;
	        	font-size: 8pt;
	        	display: inline;
	        	font-weight: bold;
	        	color: #BDA44A;

	        }
	        .back .title_value{
	        	position: absolu/te;
	        	z-index: 10;
	        	top: 0.9cm;
	        	left: 0cm;
	        	width: 5cm;
	        	padding-bottom: 4px;
	        	font-size: 10pt;
	        	font-weight: bold;
	        	color: white;
	        	
	        }
	        .back .dep{
	        	
	        	z-index: 10;
	        	
	        	left: 0.4325cm;
	        	font-size: 8pt;
	        	display: inline;
	        	font-weight: bold;
	        	color: #BDA44A;

	        }
	        .back .dep_value{
	        	position: absolu/te;
	        	z-index: 10;
	        	top: 3.7cm;
	        	left: 3cm;
	        	width: 5cm;
	        	padding-bottom: 4px;
	        	font-size: 10pt;
	        	font-weight: bold;
	        	display: inline-block;
	        	color: white;
	        }
	        .back .blood{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.5cm;
	        	left: 0.3894cm;
	        	font-size: 8pt;
	        	font-weight: bold;
	        	color: #BDA44A;

	        }
	        .back .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.45cm;
	        	left: 2.2cm;
	        	
	        	font-size: 10pt;
	        	font-weight: bold;
	        	color: white;

	        }

	        

	        .new-card-front {
			  background-image: url('{!! asset('img/new_card_front.png') !!}');
			  background-size: 8.5525cm 5.5cm;
			  
			  
			}
			.new-card-back {
			  background-image: url('{!! asset('img/new_card_back.png') !!}');
			  background-size: 8.5525cm 5.6cm;
			  height: 5.6cm;
			  
			}
		</style>

	</head>
	<body>
		<div class="do-print">
	        <div class="row">
	           <div class="col front new-card-front" dir='rtl'>
	           	<div class='serial'><!-- {!!$row->eid!!} -->
	           		<?php 
	           			//$position_ids = array(1,2,3,11);
	           			$position_ids = array(11,3,17,18);

	           			$dep_id = $row->sudep_id;
	           			$dep_code = $dep_id; 
	           			if($dep_id<10)
	           			{
	           				$dep_code = '00'.$dep_id;
	           			}
	           			elseif($dep_id < 100)
	           			{
	           				$dep_code = '0'.$dep_id;
	           			}


	     				$emp_id = $row->eid;
	     				$emp_code = $emp_id;

	     				if($emp_id<10)
	     				{
	     					$emp_code = '000'.$emp_id;
	     				}
	     				elseif($emp_id < 100)
	     				{
	     					$emp_code = '00'.$emp_id;
	     				}
	     				elseif($emp_id <1000)
	     				{
	     					$emp_code = '0'.$emp_id;
	     				}
	           			
	           			echo $dep_code.$emp_code;
	           		?>
	           	</div>
	           	<div class='phone'>+93 20 2147009</div>
	           	<img src="{!!getResizedPhoto($row->photo)!!}" class="photo">
	           	<!-- {!!HTML::image('documents/profile_pictures/'.$row->photo,'', array('class'=>'photo'))!!} -->
	           	<div class="validaty">تاریخ انقضا:</div>
	           	<div class="validaty_date">۳۰/۰۹/۱۳۹۷</div>
	           	<div class="name">اسم</div>
	           	<div class="name_value">{!!$row->name_dr!!} {!!$row->last_name!!}</div>
	           	<div class="group">
		           	<table cellspacing=0 cellpadding=0>
		           		<tr>
		           			<td>
		           				<div class="title">وظیفه</div><br>
		           				
		           			</td>
		           		</tr>
		           		<tr>
		           			<td>
		           				<div class="title_value">{!!$row->current_position_dr!!}</div>
		           				
		           			</td>
		           		</tr>
		           		<tr>
		           			<td>
		           				<div class="dep">ریاست</div> <br>
		           				
		           			</td>
		           		</tr>
		           		<tr>
		           			<td>
		           				<div class="dep_value">
		           			
		           			<?php 
		           			
		           				if(in_array($row->position_id, $position_ids))
		           				{
		           					if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58)
		           					{//frist and second deputy
		           						echo $row->department_dr;
		           					}
		           					else
		           					{
		           						echo $row->general_department_dr;
		           					}
		           				}
		           				else
		           				{
		           					echo $row->department_dr;
		           				}
		           			
		           			//echo $row->position_id;
		           			?>

		           				</div>
		           				
		           			</td>
		           		</tr>
		           	</table>
	      	   </div>
	           	
	           	<div class="blood">گروپ خون:</div>
	           	<div class="blood_value">{!!$row->blood_group!!}</div>
	           </div>

	           <div class="col back new-card-back">
	           
	       		<div class='serial'>{!!$dep_code!!}{!!$emp_code!!}</div>
	       		<div class='phone'>+93 20 2147009</div>
	           	<div class="validaty">Expiry Date:</div>
	           	<div class="validaty_date">21/12/2018</div>
	           	
	           	<img src="{!!getResizedPhoto($row->photo)!!}" class="photo">
	           	<!-- {!!HTML::image('documents/profile_pictures/'.$row->photo,'', array('class'=>'photo'))!!} -->
	           	<div class="name">Name</div>
	           	<div class="name_value">{!!$row->name_en!!}</div>
	           	<div class="group">
		           	<table cellspacing=0 cellpadding=0>
		           		<tr>
		           			<td>
		           				<div class="title">Title</div>
		           				
		           			</td>
		           		</tr>
		           		<tr>
		           			<td>
		           				<div class="title_value">{!!$row->current_position_en!!}</div>
		           				
		           			</td>
		           		</tr>
		           		<tr>
		           			<td>
		           				<div class="dep">Dirctorate</div>
		           				
		           			</td>
		           		</tr>
		           		<tr>
		           			<td>
		           				<div class="dep_value">
		           			<?php 
		           			
		           				if(in_array($row->position_id, $position_ids))
		           				{
		           					if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58)
		           					{//frist and second deputy
		           						echo $row->department_en;
		           					}
		           					else
		           					{
		           						echo $row->general_department_en;
		           					}
		           				}
		           				else
		           				{
		           					echo $row->department_en;
		           				}
		           			
		           			//echo $row->department_en;
		           			?>
		           				</div>
		           			</td>
		           		</tr>
		           	</table>
	      	   </div>
	      	   <div class="blood">Blood Group:</div>
	      	   <div class="blood_value">{!!$row->blood_group!!}</div>
	      	   
	            
	           </div>

	        </div>
        </div>
        
	</body>
</html>