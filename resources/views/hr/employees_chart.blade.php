@extends('layouts.master')

@section('head')
	<title>Dashboard</title>
@stop

@section('content')


<div class="col-xs-12">
    <h2 style="text-center">Employee Attendance by charts</h2>
</div>

        <div class="col-xs-4">
                <label class="col-sm-12 ">ادارۀ عمومی</label>
                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                        <option value="">همه</option>
                    @foreach($parentDeps AS $dep_item)
                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                    @endforeach
                </select>
        </div>
        <div class="col-xs-4">
                <div class="col-sm-12">
                    <label class="col-sm-12">ادارۀ مربوط</label>
                </div>
                <div class="col-sm-12">
                    <select style="width:100%;" name="sub_dep" id="sub_dep">
                        <option value=''>همه</option>
                    </select>
                </div>
            </div>
        <div class="col-xs-4">
            <h4>ادارۀ مربوط</h4>
            <select class="form-control" name="select2" id="select2">
            <option value="1">Banana</option>
            <option value="1">Apple</option>
            <option value="1">Orange</option>
            <option value="2">Wolf</option>
            <option value="2">Fox</option>
            <option value="2">Bear</option>
            <option value="3">Eagle</option>
            <option value="3">Hawk</option>
            <option value="4">BWM<option>
        </select>
        </div>
        <div class="col-xs-4">
                <h4>نوعیت چارت</h4>
                <select class="form-control" name="select3" id="select3">
                    <option value="1">Box chart</option>
                    <option value="2">two chart</option>
                    <option value="3">This chart</option>
                </select>
        </div>



 @section('footer-scripts')
 <script type="text/javascript">
     $("#sub_dep").select2();
     function bringRelatedSubDepartment(div,id)
     {
         $.ajax({
                 url: '{!!URL::route("bringSubDepartment")!!}',
                 data: '&dep_id='+id,
                 type: 'post',
                 beforeSend: function(){
                     $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                 },
                 success: function(response)
                 {
                     $('#'+div).html(response);
                 }
             }
         );
     }
var $select1 = $( '#select1' ),
		$select2 = $( '#select2' ),
    $options = $select2.find( 'option' );
    
$select1.on( 'change', function() {
	$select2.html( $options.filter( '[value="' + this.value + '"]' ) );
} ).trigger( 'change' );
});

</script>
@stop
@stop




