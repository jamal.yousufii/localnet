@if ($status==0)
<div class="alert alert-danger show" role="alert">
    <strong>قابل توجو:</strong> {{$message}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@else
<div class="alert alert-success show" role="alert">
    <strong>موفقیت:</strong> {{$message}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif