@extends('layouts.master')

@section('head')
    <title>Register New Employee</title>
    {!! HTML::style('/css/jasny_css/jasny-bootstrap.css') !!}
@stop
@section('content')
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">{!!_('dashboard')!!}</a></li>
            <li class="active"><span>ثبت معلومات کارمند</span></li>
        </ol>
    </div>
</div>

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box">
                <header class="main-box-header clearfix">
                    <h2>ثبت معلومات کارمند</h2>
                </header>
                
                <div class="main-box-body clearfix">
                    <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postEmployee')!!}" enctype="multipart/form-data">
                        
                        <div class="row">
                            <div class="form-group">

                                <label class="col-sm-2 control-label">وضعیت بست</label>
                                <div class="col-sm-4">
                                    <select name="vacant" class="form-control" onchange='changeDiv(this.value)'>
                                        
                                        <option value='1' <?php echo (Input::old('vacant')=='1' ? 'selected':''); ?>>کمبود</option>
                                        <option value='2' selected <?php echo (Input::old('vacant')=='2' ? 'selected':''); ?>>موجود</option>
                                        <option value='3' <?php echo (Input::old('vacant')=='3' ? 'selected':''); ?>>ریزرف</option>
                                    </select>
                                </div>
                                <label class="col-sm-2 control-label">شماره تعینات</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="number_tayenat" value="{!!Input::old('number_tayenat')!!}">
                                </div>
                            </div>
                        </div>

                    <script type="text/javascript">
                        function changeDiv(value)
                        {
                            if(value==1)
                            {
                                $('#vacant_div').slideDown()
                                $('#exist_div').slideUp()
                            }
                            else
                            {
                                $('#vacant_div').slideUp()
                                $('#exist_div').slideDown()
                            }
                        }
                    </script>

                    <div id="exist_div">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">نام و تخلص</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="name_dr" value="{!!Input::old('name_dr')!!}">
                                    <span style="color:red">{!!$errors->first('name_dr')!!}</span>
                                </div>

                                <label class="col-sm-2 control-label">ولد</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="father_name_dr" value="{!!Input::old('father_name_dr')!!}">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">سکونت اصلی</label>
                                <div class="col-sm-4">
                                    <?php $page=URL::route('getProvinceDistrict'); ?>
                                    <select class="form-control" name="original_province" id="original_province" onchange="getProvinceDistrict('{!!$page!!}','districts',this.value);">
                                        <option value=''>انتخاب</option>
                                        @foreach($provinces AS $pro)
                                            <option value='{!!$pro->id!!}'>{!!$pro->name!!}</option>
                                        @endforeach
                                    </select>
                                    <!-- <input class="form-control" type="text" name="original_province" value="{!!Input::old('original_province')!!}"> -->
                                </div>
                                <label class="col-sm-2 control-label">ولسوالی</label>
                                <div class="col-sm-4">
                                    <select name = "district" id="districts" class="form-control">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">جنسیت</label>
                                <div class="col-sm-4">
                                    <select name="gender" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='M' <?php echo (Input::old('gender')=='M' ? 'selected':''); ?>>مرد</option>
                                        <option value='F' <?php echo (Input::old('gender')=='F' ? 'selected':''); ?>>زن</option>
                                    </select>
                                </div>
                                <label class="col-sm-2 control-label">ملیت</label>
                                <div class="col-sm-4">
                                    <!-- <input class="form-control" type="text" name="nationality" value="{!!Input::old('nationality')!!}"> -->
                                    <select name = "nationality" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getStaticDropdown('ethnicity')!!}
                                    </select>
                                </div>

                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">درجه تحصیل</label>
                                <div class="col-sm-4">
                                    <select name = "education_degree" id="education_degree" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getStaticDropdown('education_degree',Input::old('education_degree'))!!}
                                    </select>
                                </div>

                                <label class="col-sm-2 control-label">رشته تحصیلی</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="education_field" value="{!!Input::old('education_field')!!}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">گروپ خون</label>
                                <div class="col-sm-4">
                                    
                                    <select name = "blood_group" id="blood_group" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getStaticDropdown('blood_group',Input::old('blood_group'))!!}
                                    </select>
                                </div>

                                <label class="col-sm-2 control-label">سال تولد</label>
                                <div class="col-sm-4">
                                    <?php 
                                         $current_year = date('Y')-621;
                                         
                                    ?>
                                    <!-- <input class="form-control" type="text" name="birth_year" value="{!!Input::old('birth_year')!!}"> -->
                                    <select name = "birth_year" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <?php 
                                           
                                            for($i=$current_year-84;$i<=$current_year-10;$i++)
                                            {
                                                
                                                echo "<option>".$i."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">تاریخ اول تقرر</label>
                                <div class="col-sm-4">
                                    <input class="form-control datepicker_farsi" type="text" name="first_appointment_date" value="{!!Input::old('first_appointment_date')!!}">
                                </div>

                                <label class="col-sm-2 control-label">تاریخ اخرین ترفع</label>
                                <div class="col-sm-4">
                                    <input class="form-control datepicker_farsi" type="text" name="last_appointment_date" value="{!!Input::old('last_appointment_date')!!}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">نوع تقرر</label>
                                <div class="col-sm-4">
                                    <!-- <select name="free_competition" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='0' <?php echo (Input::old('free_competition')=='0' ? 'selected':''); ?>>بلی</option>
                                        <option value='1' <?php echo (Input::old('free_competition')=='1' ? 'selected':''); ?>>نخیر</option>
                                    </select> -->
                                    <input class="form-control" type="text" name="free_compitition" value="{!!Input::old('free_competition')!!}">
                                    <span style="color:red">{!!$errors->first('free_compitition')!!}</span>
                                </div>

                                <label class="col-sm-2 control-label">کارکنان</label>
                                <div class="col-sm-4">
                                    
                                    <select name="employee_type" class="form-control" onchange="showRank(this.value)">
                                        <option value=''>انتخاب</option>
                                        <option value='1' <?php echo (Input::old('employee_type')=='1' ? 'selected':''); ?>>مامور</option>
                                        <option value='2' <?php echo (Input::old('employee_type')=='2' ? 'selected':''); ?>>اجیر</option>
                                        <option value='3' <?php echo (Input::old('employee_type')=='3' ? 'selected':''); ?>>نظامی</option>
                                        <option value='4' <?php echo (Input::old('employee_type')=='4' ? 'selected':''); ?>>مامور بالمقطع</option>
                                        <option value='5' <?php echo (Input::old('employee_type')=='5' ? 'selected':''); ?>>اجیر بالمقطع</option>
                                    </select>
                                </div>
                                <script type="text/javascript">
                                    function showRank(value)
                                    {
                                        if(value == 2)
                                        {
                                            $('#ageer_div').slideDown();
                                            $('#bast_div').slideUp();
                                            $('#military_div').slideUp();
                                        }
                                        else if(value == 3)
                                        {
                                            $('#ageer_div').slideUp();
                                            $('#bast_div').slideUp();
                                            $('#military_div').slideDown();

                                        }
                                        else
                                        {
                                            $('#ageer_div').slideUp();
                                            $('#military_div').slideUp();
                                            $('#bast_div').slideDown();
                                        }

                                    }
                                </script>
                            </div>
                        </div>
                       
                        <div class="row" id="military_div" style="display:none;">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">رتبه</label>
                                <div class="col-sm-4">
                                    <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
                                    <select name = "military_rank" id="military_rank" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getStaticDropdown('military_rank',Input::old('emp_rank'))!!}
                                    </select>
                                    <span style="color:red">{!!$errors->first('emp_rank')!!}</span>
                                </div>

                                <label class="col-sm-2 control-label">بست</label>
                                <div class="col-sm-4">
                                    <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
                                    <select name = "military_bast" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getStaticDropdown('military_rank',Input::old('emp_bast'))!!}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="bast_div">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">رتبه</label>
                                <div class="col-sm-4">
                                    <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
                                    <select name = "emp_rank" id="emp_rank" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getStaticDropdown('employee_rank',Input::old('emp_rank'))!!}
                                    </select>
                                    <span style="color:red">{!!$errors->first('emp_rank')!!}</span>
                                </div>

                                <label class="col-sm-2 control-label">بست</label>
                                <div class="col-sm-4">
                                    <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
                                    <select name = "emp_bast" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getBastStaticList('employee_rank',Input::old('emp_bast'))!!}
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="ageer_div" style="display:none;">
                            <div class="form-group">

                                <label class="col-sm-2 control-label">درجه اجیر</label>
                                <div class="col-sm-4">
                                    <!-- <input class="form-control" type="text" name="ageer_rank" value="{!!Input::old('ageer_rank')!!}"> -->
                                    <select name = "ageer_rank" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getStaticDropdown('employee_rank',Input::old('ageer_rank'))!!}
                                    </select>
                                </div>

                                <label class="col-sm-2 control-label">بست اجیر</label>
                                <div class="col-sm-4">
                                    <!-- <input class="form-control" type="text" name="ageer_bast" value="{!!Input::old('ageer_bast')!!}"> -->
                                    <select name = "ageer_bast" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getBastStaticList('employee_rank',Input::old('ageer_bast'))!!}
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">

                                <label class="col-sm-2 control-label"> تقرر دربست فعلی</label>
                                <div class="col-sm-4">
                                    <input class="form-control datepicker_farsi" type="text" name="appointment_current_position" value="{!!Input::old('appointment_current_position')!!}">
                                </div>

                                <label class="col-sm-2 control-label">شماره تماس</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="phone" value="{!!Input::old('phone')!!}">
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">

                                <label class="col-sm-2 control-label">وظیفه فعلی</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="current_position_dr" value="{!!Input::old('current_position_dr')!!}">
                                    <span style="color:red">{!!$errors->first('current_position_dr')!!}</span>
                                </div>

                                <label class="col-sm-2 control-label">موقف بست</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="position_dr" value="{!!Input::old('position_dr')!!}">
                                    <span style="color:red">{!!$errors->first('position_dr')!!}</span>
                                </div>

                            </div>
                        </div>

                        
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">ادارۀ عمومی</label>
                                <div class="col-sm-4">
                                    <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                        <option value="">انتخاب</option>
                                        @foreach($parentDeps AS $dep_item)
                                            <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endforeach
                                    </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                                </div>

                                <label class="col-sm-2 control-label">ادارۀ مربوط</label>
                                <div class="col-sm-4" id="sub_dep_div">
                                    <!-- <input class="form-control" type="text" name="department_dr" value="{!!Input::old('department_dr')!!}"> -->
                                    
                                    <select class="form-control" name="sub_dep" id="sub_dep">
                                        <option value=''>انتخاب</option>
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">

                                <label class="col-sm-2 control-label">رتبه علمی</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="education_rank" value="{!!Input::old('education_rank')!!}">
                                </div>

                                <label class="col-sm-2 control-label">موقف کارکن</label>
                                <div class="col-sm-4">
                                    <select name="mawqif_employee" class="form-control">
                                        <option value=''>انتخاب</option>
                                        <option value='1' <?php echo (Input::old('mawqif_employee')=='1' ? 'selected':''); ?>>برحال</option>
                                        <option value='2' <?php echo (Input::old('mawqif_employee')=='2' ? 'selected':''); ?>>انتظار بامعاش</option>
                                        <option value='3' <?php echo (Input::old('mawqif_employee')=='3' ? 'selected':''); ?>>بالمقطع</option>
                                        <option value='4' <?php echo (Input::old('mawqif_employee')=='4' ? 'selected':''); ?>>درجریان تحصیل</option>
                                        <option value='5' <?php echo (Input::old('mawqif_employee')=='5' ? 'selected':''); ?>>وغیره</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">لایحه وظایف</label>
                                <div class="col-sm-10">
                                    <!-- <textarea rows="4" name="job_description" class="form-control"></textarea> -->
                                    <div class="alert alert-info"><i class="fa fa-info fa-lg"></i> لایحه وظایف را می توانید به فایل ورد درج نموده و از طریق آپلود ضمیمه آپلود نمائید.</div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">ایمیل</label>
                                <div class="col-sm-4">
                                    <input type="text" name="email" class="form-control">
                                </div>

                                
                                <label class="col-sm-2 control-label">آپلود ضمیمه</label>
                                <div class="col-sm-4" id='files_div'>
                                    <input type='file' id='files'  name='files[]' class="form-control" multiple='multiple'>
                                </div>
                                

                            </div>
                        </div>
 
                    </div>

                    <div id="vacant_div" style="display:none">
                        <div class="row">
                            <div class="form-group">

                                <label class="col-sm-2 control-label">عنوان بست</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="current_position_dr1" value="{!!Input::old('current_position_dr1')!!}">
                                    <span style="color:red">{!!$errors->first('current_position_dr1')!!}</span>
                                </div>

                                <label class="col-sm-2 control-label">بست</label>
                                <div class="col-sm-4">
                                    <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
                                    <select name = "emp_bast1" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getBastStaticList('employee_rank',Input::old('emp_bast1'))!!}
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">درجه اجیر</label>
                                <div class="col-sm-4">
                                    <!-- <input class="form-control" type="text" name="ageer_rank" value="{!!Input::old('ageer_rank')!!}"> -->
                                    <select name = "ageer_rank1" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getStaticDropdown('employee_rank',Input::old('ageer_rank'))!!}
                                    </select>
                                </div>

                                <label class="col-sm-2 control-label">بست اجیر</label>
                                <div class="col-sm-4">
                                    <!-- <input class="form-control" type="text" name="ageer_bast" value="{!!Input::old('ageer_bast')!!}"> -->
                                    <select name = "ageer_bast1" class="form-control">
                                        <option value=''>انتخاب</option>
                                        {!!getBastStaticList('employee_rank',Input::old('ageer_bast'))!!}
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">ادارۀ عمومی</label>
                                <div class="col-sm-4">
                                    <select name="general_department1" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep1',this.value)">
                                        <option value="">انتخاب</option>
                                        @foreach($parentDeps AS $dep_item)
                                            <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endforeach
                                    </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                                </div>

                                <label class="col-sm-2 control-label">ادارۀ مربوط</label>
                                <div class="col-sm-4">
                                    <!-- <input class="form-control" type="text" name="department_dr" value="{!!Input::old('department_dr')!!}"> -->
                                    
                                    <select class="form-control" name="sub_dep1" id="sub_dep1">
                                        <option value=''>انتخاب</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-4">
                                <div class="row form-group">
                                    <div class="col-sm-12">
                                        <div class="checkbox-nice">
                                            <input value='1' type="checkbox" id="create_account" name="create_account">
                                            <label for="create_account">
                                                ایجاد پروفایل کاربر
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>              
                        </div>

                        {!! Form::token() !!}

                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10">
        
                                <button class="btn btn-primary" type="submit">ثبت معلومات</button>
                            
                                <button onclick="history.back()" class="btn btn-danger" type="button">لغو</button>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer-scripts')
{!! HTML::script('/js/jasny_js/jasny-bootstrap.js') !!}

<script type="text/javascript">
    $(document).on("change","#files",function(){
        var count = $('#files_div').find('input').length;
        count = count+1;
        $("<div id='div_"+count+"' style='display:inline;'><input style='splay:inline;' type='file' class='form-control' id='files'  name='files[]' multiple='multiple' /><a style='float:right;margin-top:-33px;' class=\"btn btn-default btn-app-sm\" href='javascript:void()' onclick='$(\"#div_"+count+"\").remove();'><i class=\"fa fa-minus-circle\" style='color:red'></i></a></div>").appendTo('#files_div');
    });


    function getProvinceDistrict(page,ele,value)
    {

        $.ajax({
            url: page,
            type: 'post',
            data: 'province='+value,
            dataType: 'html',
            success:function(respones){
                $('#'+ele).html(respones);
            }
        });

    }
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
</script>

@stop

