<html>
	<head>
		<meta charset="UTF-8">
		<style type="text/css">
			* {line-height: 1em;
				padding: 0px;
				margin: 0px;
			}

			.front{
				width: 5.3975cm;
				height:8.5525cm;
				flo/at: left;
				position:relative;
				font-family: 'B Nazanin';
				overflow: hidden;

			}

			.back{
				width: 5.3975cm;
				height:8.5425cm;
				fl/oat: right;
				position:relative;
				font-family: 'Times New Roman';
				overflow: hidden;
			}

	        .front img {
	            width:5.3975cm;
				height:8.5725cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .back img {
	            width:5.3975cm;
				height:8.5725cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .do-print{
	        	font-family: 'B Nazanin';
            	font-size: 12pt;
	        }
	        .serial{
	        	position: absolute;
	        	z-index: 10;
	        	font-size: 6pt;
	        	left:0.2934cm;
	        	top:0.366cm;
	        	font-family: 'Times New Roman';

	        }

	        .front .validaty {
	        	position: absolute;
	        	z-index: 10;
	        	right:0.41cm;
	        	top: 2.6cm;
	        	font-size: 6pt;

	        }
	        .front .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	right:0.41cm;
	        	top: 2.9cm;
	        	font-size: 7pt;
	        	font-weight: bold;
	        	color: red;

	        }
	        .front .issue {
	        	position: absolute;
	        	z-index: 10;
	        	right:0.41cm;
	        	top: 1.9cm;
	        	font-size: 6pt;

	        }
	        .front .issue_date{
	        	position: absolute;
				z-index: 10;
				right: 0.41cm;
				top: 2.2cm;
				font-size: 7pt;
				font-weight: bold;
				color: red;

	        }

	        .front .photo {
	        	position: absolute;
	        	width: 2.3371cm;
	        	height: 2.9213cm;
	        	top: 1.7832cm;
	        	left: 0.3823cm;
	        	z-index: 10;
	        }

	        .front .position{
	        	/*position: absolute;*/
	        	z-index: 10;
	        	top:0.250cm;
	        	padding-right:0.4325cm;
	        	font-weight: bold;
	        	font-size: 13pt;
	        	color: white;
	        	display: table-cell;
	        	height: 100%;
	        	vertical-align:middle;


	        }
	        .front .box{
	        	position: absolute;
	        	z-index: 100;
	        	/*background-color: red !important;*/
	        	top: 3.2354cm;
	        	left: 2.724cm;
	        	height: 1.0651cm;
	        	width: 2.6735cm;
	        	display: table;
	        }
	        .front .name{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.5861cm;
	        	right: 0.4325cm;
	        	font-size: 6pt;

	        }
	        .front .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.8772cm;
	        	right: 0.4325cm;
	        	left: 0.3823cm;
	        	font-size: 9pt;
	        	font-weight: bold;

	        }
	        .front .title{
	        	position: absolu/te;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	right: 0.4325cm;
	        	font-size: 6pt;
	        	display: inline;

	        }
	        .front .title_value{
	        	position: absol/ute;
	        	z-index: 10;
	        	to/p: 4.8772cm;
	        	right: 0.4325cm;
	        	left: 1.6214cm;
	        	width: 3.2582cm;
	        	font-size: 9pt;
	        	font-weight: bold;
	        	display: inline-block;
	        }
	        .front .dep{
	        	position: abso/lute;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	right: 0.4325cm;
	        	font-size: 6pt;
	        	display: inline;

	        }
	        .front .dep_value{
	        	position: absol/ute;
	        	z-index: 10;
	        	t/op: 4.8772cm;
	        	right: 0.4325cm;
	        	left: 1.6214cm;
	        	width: 4.5957cm;
	        	font-size: 9pt;
	        	font-weight: bold;
	        	display: inline-block;
	        }


	        .front .group {
	        	position: absolute;
	        	z-index: 10;
	        	top:5.3481cm;
	        	left: 0.3823cm;
	        	right: 0.4325cm;
	        }
	        .front td {
	        	height: 1.4cm;
	        	vertical-align: middle;
	        }


	        .back .validaty {
	        	position: absolute;
	        	z-index: 10;
	        	left:0.3894cm;
	        	top: 2.7cm;
	        	font-size: 4pt;

	        }
	        .back .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	left:0.3894cm;
	        	top: 2.9cm;
	        	font-size: 6pt;
	        	font-weight: bold;
	        	color: red;

	        }
	        
			.back .issue {
	        	position: absolute;
	        	z-index: 10;
	        	left:0.3894cm;
	        	top: 1.9cm;
	        	font-size: 4pt;

	        }
	        .back .issue_date{
	        	position: absolute;
	        	z-index: 10;
	        	left:0.3894cm;
	        	top: 2.1cm;
	        	font-size: 6pt;
	        	font-weight: bold;
	        	color: red;

	        }
	        
	        .back .photo {
	        	position: absolute;
	        	width: 2.3371cm;
	        	height: 2.9213cm;
	        	top: 1.7832cm;
	        	right: 0.4556cm;
	        	z-index: 10;
	        }

	        .back .position{
	        	/*position: absolute;*/
	        	z-index: 10;
	        	top:0.250cm;
	        	padding-left:0.3894cm;
	        	font-weight: bold;
	        	font-size: 13pt;
	        	color: white !important;
	        	display: table-cell;
	        	vertical-align:middle;
	        	height: 100%;
	        }
	        .back .box{
	        	position: absolute;
	        	z-index: 100;
	        	/*background-color: red !important;*/
	        	top: 3.2535cm;
	        	right: 2.7927cm;
	        	height: 1.0287cm;
	        	width: 2.6029cm;
	        	display: table;
	        }
	        .back .name{
	        	position: absolute;
	        	z-index: 10;
	        	top: 4.9194cm;
	        	left: 0.3894cm;
	        	font-size: 4pt;

	        }
	        .back .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 5.0605cm;
	        	right: 0.4556cm;
	        	left: 0.3894cm;
	        	font-size: 8pt;
	        	/*font-weight: bold;*/

	        }
	        .back .title{
	        	position: absolu/te;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	left: 0.3894cm;
	        	font-size: 4pt;
	        	display: inline;

	        }
	        .back .title_value{
	        	position: absol/ute;
	        	z-index: 10;
	        	to/p: 4.8772cm;
	        	right: 0.4556cm;
	        	left: 0.3894cm;
	        	font-size: 8pt;
	        	/*font-weight: bold;*/
	        	display: inline-block;
	        }
	        .back .dep{
	        	position: abso/lute;
	        	z-index: 10;
	        	to/p: 4.5861cm;
	        	left: 0.3894cm;
	        	font-size: 4pt;
	        	display: inline;

	        }
	        .back .dep_value{
	        	position: absol/ute;
	        	z-index: 10;
	        	t/op: 4.8772cm;
	        	right: 0.4556cm;
	        	left: 0.3894cm;
	        	font-size: 8pt;
	        	/*font-weight: bold;*/
	        	display: inline;
	        }


	        .back .group {
	        	position: absolute;
	        	z-index: 10;
	        	top:5.4146cm;
	        	left: 0.3894cm;
	        	right: 0.4556cm;
	        }

	        .back td {
	        	height: 1.1cm;
	        	vertical-align: middle;
	        }

	        .back .blood{
	        	position: absolute;
	        	z-index: 10;
	        	top: 7.7376cm;
	        	left: 0.3894cm;
	        	font-size: 4pt;

	        }
	        .back .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 7.8875cm;
	        	right: 0.4556cm;
	        	left: 0.3894cm;
	        	font-size: 13pt;
	        	font-weight: bold;
	        	color: red;

	        }

	        .back .email{
	        	position: absolute;
	        	z-index: 10;
	        	top: 8.0051cm;
	        	right: 0.485cm;
	        	
	        	font-size: 5.25pt;
	        	font-weight: bold;

	        }
	        
	        .img-water-mark-front{
				 position:fixed;
				 bottom:16px;
				 left:32px !important;
				 opacity:0.3;
				 z-index:99;
				 width: 128px !important;
				 height: 128px !important;

	        }
	        .img-water-mark-back{
				 position:fixed;
				 bottom:16px;
				 left:32px !important;
				 opacity:0.3;
				 z-index:99;
				 width: 128px !important;
				 height: 128px !important;

	        }

		</style>

	</head>
	<body>
		<div class="do-print">
	        <div class="row">
	           <div class="col front" dir='rtl'>
	           	<div class='serial'><!-- {!!$row->eid!!} -->
	           		<?php 
	           			//$position_ids = array(1,2,3,11);
	           			$position_ids = array(11,3,17,18);

	           			$dep_id = $row->dep_id;
	           			$dep_code = $dep_id; 
	           			if($dep_id<10)
	           			{
	           				$dep_code = '00'.$dep_id;
	           			}
	           			elseif($dep_id < 100)
	           			{
	           				$dep_code = '0'.$dep_id;
	           			}


	     				$emp_id = $row->eid;
	     				$emp_code = $emp_id;

	     				if($emp_id<10)
	     				{
	     					$emp_code = '000'.$emp_id;
	     				}
	     				elseif($emp_id < 100)
	     				{
	     					$emp_code = '00'.$emp_id;
	     				}
	     				elseif($emp_id <1000)
	     				{
	     					$emp_code = '0'.$emp_id;
	     				}
	           			
	           			echo $dep_code.$emp_code;
	           			
	           			$current_date = date("Y-m-d");
	           			$c_date = explode('-',$current_date);
	           			$c_date = dateToShamsi($c_date[0],$c_date[1],$c_date[2]);
	           			
	           			$c_date = explode("-",$c_date);
	           			$jalaliMonthName=getJalaliMonthName($c_date[1]);
	           			$shamsi_date = $c_date[2]." ".$jalaliMonthName." ".$c_date[0];
	           			
	           			$miladi_date = date("j M Y");
	           		?>
	           	</div>
	           	<div class="issue">د صدور نیټه</div>
	           	<div class="issue_date">{!!$shamsi_date!!}</div>

	           	<div class="validaty">د اعتبار موده</div>
	           	<div class="validaty_date">11 جوزا 1397</div>
	           	<img src="{!!getResizedPhoto($row->photo)!!}" class="photo">
	           	<!-- {!!HTML::image('documents/profile_pictures/'.$row->photo,'', array('class'=>'photo'))!!} -->
	           	<div class="box" style="background-color:<?=$row->color?>;">
	           	<div class="position">{!!$row->position_dr!!}</div>
	           	</div>
	           	<div class="name">نوم</div>
	           	<div class="name_value">{!!$row->name_dr!!} {!!$row->last_name!!}</div>
	           	{!!HTML::image('img/logo-watermark.png','', array('class'=>'img-water-mark-front'))!!}
	           	<div class="group">
	           		
		           	<table cellspacing=0 cellpadding=0>
		           		<tr>
		           			<td>
		           				<div class="title">وظیفه</div><br>
		           				<div class="title_value">{!!$row->current_position_dr!!}</div>
		           			</td>
		           		</tr>
		           		<tr>
		           			<td>
		           				<div class="dep">اداره</div> <br>
		           				<div class="dep_value">
		           			
		           			<?php 
		           			
		           				if(in_array($row->position_id, $position_ids))
		           				{
		           					if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58 || $row->advisor==1)
		           					{//frist and second deputy
		           						echo $row->department_dr;
		           					}
		           					else
		           					{
		           						echo $row->general_department_dr;
		           					}
		           				}
		           				else
		           				{
		           					echo $row->department_dr;
		           				}
		           			
		           			//echo $row->position_id;
		           			?>

		           				</div>
		           			</td>
		           		</tr>
		           	</table>
	      	   </div>
	            {!!HTML::image('img/Header-01.png','', array('class'=>'bg-img'))!!}
	           </div>


	        </div>
        </div>
        
	</body>
</html>