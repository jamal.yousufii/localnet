<html>
	<head>
		<meta charset="UTF-8">
		<style type="text/css">
		<?php $card_picture = '' ?> 
			@font-face {
					font-family: bahji_zar;
					   src:url('/fonts/bahij_zar_regular.ttf');

				}

				 body * {
				  font-family: bahji_zar;
				}

				.bahji_zar {
				  font-family: bahji_zar;
				}
		
			
			* {
				line-height: 1em;
				padding: 0px;
				margin: 0px;
				padding-bottom: 1px;
				padding-right: 1px;
			}

			.front{
				height: 5.4cm;
    			width: 8.5cm;
				flo/at: left;
				position:relative;
				font-family: bahji_zar;
				overflow: hidden;

			}

			.back{
				height: 5.4cm;
    			width: 8.5cm;
				flo/at: right;
				position:relative;
				font-family: 'B Titr';
				overflow: hidden;
				font-weight: bold;
			}

	        .front img {
	            height:8.5725cm;
				width:5.4cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .back img {
	            height:8.5725cm;
				width:5.4cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .do-print{
	        	font-family: 'B Titr';
            	font-size: 12pt;

	        }
	        .front .serial{
	        	position: absolute;
                z-index: 10;
                top: 5.02cm;
                left: 3.05cm !important;
                font-size: 8pt;
                color: white;

	        }
	        .front .serial_title{
	        	position: absolute;
                z-index: 10;
                top: 5cm;
                left: 3.9cm;
                font-size: 8pt;
                color: white;

	        }

	        .front .validaty_date_title{
	        	position: absolute;
                z-index: 10;
                left: 1.70cm;
                top: 5cm;
                font-size: 8pt;
                color: white !important;


	        }
	        .front .validaty_date {
                position: absolute;
                z-index: 10;
                left: 0.5cm;
                top: 5cm;
                font-size: 8pt;
                color: white;
            }
	        .front .degree{
	        	position: absolute;
				width: 8.5cm;
				z-index: 10;
				left: 0cm;
				top: 0.6cm;
				font-size: 12px;
				color: white;
				text-align: center;

	        }
	        .front .afghanistan{
	        	position: absolute;
                width: 8.5cm;
                z-index: 10;
                font-weight: bold;
                left: 0cm;
                top: 0.1cm;
                font-size: 12px;
                color: white;
                text-align: center;

	        }

	        .front .photo {
	        	position: absolute;
	        	width: 2.3cm;
                height: 2.85cm;
	        	top: 1.65cm;
    			left: 0.5cm;
	        	z-index: 10;
	        }
	        .front .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 1.9cm;
    			left: 3cm;
	        	width: 5.3cm;
	        	text-align: center;
	        	font-size: 13pt;
	        	font-weight: bold;
	        }

	        .front .title_value{
	        	position: abs/olute;
	        	z-index: 10;
	        	top: 5.8cm;
	        	text-align: center;
	        	width: 5.3cm;
	        	padding-bottom: 3px;
	        	font-size: 10pt;
	        	font-weight: bold;

	        }

	        .front .dep_value{
	        	position: abs/olute;
	        	z-index: 10;
	        	top: 6.2cm;
	        	text-align: center;
	        	width: 5.3cm;
	        	padding-bottom: 3px;
	        	font-size: 10pt;
	        	font-weight: bold;
	        	display: inline-block;
	        }

	        .front .info
            {
                position: absolute;
                z-index: 10;
                left: 5.15cm;
                top: 4.99cm;
                font-size: 8pt;

	        }
	        .front .blood{
	        	position: absolute;
	        	z-index: 10;
	        	right:0.2cm;
	        	top: 7.6cm;
	        	font-size: 8pt;

	        }
	        .front .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 8cm;
	        	right: 0.4cm;

	        	font-size: 11pt;

	        	color: red;

	        }


	        .front .group {
	        	position: absolute;
	        	z-index: 10;
	            top: 2.6cm;
    			left: 3cm;
	        	text-align: center;
	        	width: 5.3cm;

	        }
	        .back .serial{
	        	position: absolute;
	        	z-index: 10;
	        	top: 5cm;
    			left: 4.8cm;
	        	font-size: 7pt;
	        	font-family: 'Times New Roman';
	        	color: white;
	        }

	        .back .validaty_date_title{
	        	position: absolute;
	        	z-index: 10;
	        	left:1.1cm;
	        	top: 7.6cm;
	        	font-size: 8pt;


	        }
	        .back .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	right:1cm;
	        	top: 7.5cm;
	        	font-size: 10pt;
	        	font-family: 'Times New Roman';
	        	color: red;

	        }
	        .back .degree
			{
	        	    position: absolute;
					width: 8.5cm;
					z-index: 10;
					left: 0cm;
					top: 0.70cm;
					font-size: 12px;
					color: white;
					text-align: center;
	        }


	        .back .photo {
	        	position: absolute;
	            width: 2.3cm;
    			height: 2.85cm;
	        	top: 1.60cm;
    			left: 5.70cm;
	        	z-index: 10;
	        }

	        .back .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 1.9cm;
    			left: 0.50cm;
	        	width: 5.2cm;
	        	text-align: center;
	        	font-size: 12pt;
	        	font-weight: b/old;
	        }

	        .back .title_value{
	        	position: abso/lute;
	        	z-index: 10;
	        	font-weight: bold;
	        	text-align: center;
	        	width: 5.2cm;
	        	padding-bottom: 4px;
	        	font-size: 10pt;
	        }

	        .back .dep_value{
	        	position: abs/olute;
	        	z-index: 10;
	        	font-weight: bold;
	        	text-align: center;
	        	width: 5.2cm;
	        	padding-bottom: 4px;
	        	font-size: 10pt;

	        	display: inline-block;
	        }
	        .back .info{
	        	position: absolute;
	        	z-index: 10;
	        	right:1cm;
	        	top: 7.85cm;
	        	font-size: 7pt;
	        	font-family: 'Times New Roman';
	        }
	        .back .blood{
	        	position: absolute;
	        	z-index: 10;
	        	left:0.2cm;
	        	top: 7.6cm;
	        	font-size: 7pt;
	        }
	        .back .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 7.85cm;
	        	left: 0.6cm;

	        	font-size: 11pt;

	        	color: red;

	        }
	        .back .group {
	        	position: absolute;
	        	z-index: 10;
	        	top: 2.6cm;
    			left: 0.50cm;
	        	text-align: center;
	        	width: 5.2cm;

	        }
			.print_button{
					width: 8.5cm; 
				}

	        <?php
	        if($row->card_type==1)
	        {
				$card_picture = 'ajiran_edareamor';
	        	$card_id = 'head.jpg';$degree = 'A'; ?>

	        	.front .name_value{
		        	color: black;
		        }

		        .front .title_value{
		        	color: black;

		        }
		        .front .dep_value{
		        	color: black;
		        }

		        .front .info{
		        	color: white;

		        }
		        .front .blood{
		        	color: black;

		        }
		        .front .validaty_date_title{
		        	color: black;
		        }
		        .back .name_value{
		        	color: black;
		        }

		        .back .title_value{
		        	color: black;

		        }

		        .back .dep_value{
		        	color: black;
		        }
                .front .serial {
					left: 2.75cm;
				}


		     
	        <?php
	    	}
	        elseif($row->card_type==2)
	        {
				$card_picture = 'karmandan_edareamor'; 
	        	$card_id = 'employee.jpg';$degree = 'B'; ?>

	        	.front .name_value{
		        	color: black;
		        }

		        .front .title_value{
		        	color: black;

		        }

		        .front .dep_value{
		        	color: black;
		        }

		        .back .name_value{
		        	color: black;
		        }

		        .back .title_value{
		        	color: black;

		        }

		        .back .dep_value{
		        	color: black;
		        }
				.back .serial {
					top: 4.99cm;
    				left: 4.9cm;
				}
				.back .photo {
				  top: 1.65cm;
    			  left: 5.70cm;
				}


		    
	        <?php
	    	}
	        elseif($row->position_id==3)
	        {
	        	$card_id = 'ajir.jpg';$degree = 'C'; ?>

	        	.front .name_value{
		        	color: black;
		        }

		        .front .title_value{
		        	color: black;

		        }

		        .front .dep_value{
		        	color: black;
		        }

		        .front .info{
		        	color: black;

		        }
		        .front .blood{
		        	color: black;

		        }
		        .front .validaty_date_title{
		        	color: black;
		        }
		        .back .name_value{
		        	color: black;
		        }

		        .back .title_value{
		        	color: black;

		        }

		        .back .dep_value{
		        	color: black;
		        }

		        .back .info{
		        	color: black;

		        }
		        .back .blood{
		        	color: black;

		        }
		        .back .validaty_date_title{
		        	color: black;
		        }
	        <?php
	    	}
			elseif($row->card_type==4)
	        {
	        	$card_id = 'ajir.jpg';$degree = 'C'; ?>

	        	.front .name_value, .title_value, .dep_value, .front .serial{
		        	color: black;
		        }
				.front .degree {
					top: 1.9cm; 
					color: black; 
					left: 2.9cm;
					font: 13px; 
					text-align: right !important; 
				}

				.front .name_value {
					top: 2.6cm; 
				}

				.front .group{
					top: 3.3cm ; 
				}
				.front .photo, .back .photo {
					top: 1.90cm 
				}

		        .back .name_value, .title_value, .dep_value, .back .serial{
		        	color: black;
		        }
				.back .name_value {
					top:  2.6cm; 
				}
				.back .group {
					top: 3.1cm; 
				}
				.back .degree {
					top: 1.9cm; 
					left: 0.3cm;
					text-align: left;
					font-size: 10px; 
					color: black; 

				}

	        <?php
	    	}
			elseif($row->card_type==9)
	        {
	        	$card_id = 'ajir.jpg';$degree = 'C'; ?>

	        	.front .name_value, .title_value, .dep_value, .front .serial{
		        	color: black;
		        }

				.front .degree {
					top: 1.9cm; 
					color: black; 
					left: 2.9cm;
					font: 13px; 
					text-align: right !important; 
				}

				.front .name_value {
					top: 2.6cm; 
				}

				.front .group{
					top: 3.3cm ; 
				}
				
		        .back .name_value, .title_value, .dep_value, .back .serial{
		        	color: black;
		        }
				.back .name_value {
					top:  2.6cm; 
				}
				.back .group {
					top: 3.1cm; 
				}
				.back .degree {
					top: 1.9cm; 
					left: 0.3cm;
					text-align: left;
					font-size: 10px; 
					color: black; 

				}

	        <?php
	    	}
			elseif($row->card_type==5)
	        {
	        	$card_id = 'ajir.jpg';$degree = 'C'; ?>

	        	.front .name_value, .title_value, .dep_value, .info{
		        	color: white;
		        }

		        .back .name_value, .title_value, .dep_value{
		        	color: white;
		        }
				.back .serial {
					top: 4.8cm; 
				}
                .front .photo {
                    top: 1.70cm; 
                }
                .front .serial {
                    left: 2.80cm;
                }

	        <?php
	    	}
	        else
	        {
	        	$card_id = 'employee.jpg';$degree = 'B';
	        }
	        ?>
	        .new-card-front {
			  background-image: url('{!! asset('img/aop_cards/1399_simple/'.$row->card_picture.'_front.jpeg') !!}');
			  background-size: 8.5cm 5.4cm;
			  background-repeat: no-repeat;

			}
			.new-card-back {
			  background-image: url('{!! asset('img/aop_cards/1399_simple/'.$row->card_picture.'_back.jpeg') !!}');
			  background-size: 8.5cm 5.4cm;
			  background-repeat: no-repeat; 

			}
		</style>

	</head>
	<?php

	$img = Image::make(file_get_contents('documents/profile_pictures/'.$row->photo ));

		$img->encode('jpg');
		$type = 'jpg';
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);

	?>
	<body>
		<div class="do-print">
	        <div class="row" style="margin-left:1px;">
	           <div class="col front new-card-front">
	           	<!-- {!!$row->eid!!} -->
	           		<?php
	           			//$position_ids = array(1,2,3,11);
	           			//$position_ids = array(11,3,17,18);
	           			$position_ids = array(11,17,18);

	           			$dep_id = $row->sudep_id;
	           			$dep_code = $dep_id;
	           			if($dep_id<10)
	           			{
	           				$dep_code = '00'.$dep_id;
	           			}
	           			elseif($dep_id < 100)
	           			{
	           				$dep_code = '0'.$dep_id;
	           			}


	     				$emp_id = $row->eid;
	     				$emp_code = $emp_id;

	     				if($emp_id<10)
	     				{
	     					$emp_code = '000'.$emp_id;
	     				}
	     				elseif($emp_id < 100)
	     				{
	     					$emp_code = '00'.$emp_id;
	     				}
	     				elseif($emp_id <1000)
	     				{
	     					$emp_code = '0'.$emp_id;
	     				}

	           		?>

                <img src="{!!$base64!!}" class="photo">
                   
	           	<div class="name_value">{!!$row->name_dr!!} {!!$row->last_name!!}</div>
	           	<div class="group">
		           	<table cellspacing=0 cellpadding=0>

		           		<tr>
		           			<td>
		           				<div class="title_value">{!!$row->current_position_dr!!}</div>

		           			</td>
		           		</tr>

		           		<tr>
		           			<td>
		           				<div class="dep_value">

		           			<?php
		           			/*
		           				if(in_array($row->position_id, $position_ids))
		           				{
		           					if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58)
		           					{//frist and second deputy
		           						echo $row->department_dr;
		           					}
		           					else
		           					{
		           						echo $row->general_department_dr;
		           					}
		           				}
		           				else
		           				{
		           					echo $row->department_dr;
		           				}
		           			*/
		           			echo $row->department_dr;
		           			//echo $row->position_id;
		           			?>

		           				</div>

		           			</td>
		           		</tr>
		           	</table>
	      	   	</div>
				<div class="afghanistan">دافغانستان اسلامی جمهوریت</div>	 
				<div class="degree">{{Config::get('myConfig.card_title.dr.'.$row->card_title)}}</div>	 
                <div class='serial'>{!!$dep_code!!}{!!$emp_code!!}</div>
                <div class='serial_title'>شماره کارت</div>
                <div class="validaty_date_title">تاریخ انقضا</div>
	           	<div class="validaty_date">1400/12/29</div>
	           	<div class="info">info@aop.gov.af +93202147356</div>

	           </div>
                <div style="width: 5.5cm;">
                  <a href="{!!route('getCardTemplatePrint',$row->eid)!!}" class="btn btn-primary print_button">Print</a>
                </div>
	           	<div class="col back new-card-back">
	           		<img src="{!!$base64!!}" class="photo">

		       		<div class="name_value">{!!$row->name_en!!}</div>
		       		<div class="group">
			           	<table cellspacing=0 cellpadding="0" style="line-height: 1em;">
			           		<tr>
			           			<td>
			           				<div class="title_value">{!!$row->current_position_en!!}</div>

			           			</td>
			           		</tr>
			           		<tr>
			           			<td>
			           				<div class="dep_value">
			           			<?php
			           			/*
			           				if(in_array($row->position_id, $position_ids))
			           				{
			           					if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58)
			           					{//frist and second deputy
			           						echo $row->department_en;
			           					}
			           					else
			           					{
			           						echo $row->general_department_en;
			           					}
			           				}
			           				else
			           				{
			           					echo $row->department_en;
			           				}
			           			*/
			           			echo $row->department_en;
			           			//echo $row->department_en;
			           			?>
			           				</div>
			           			</td>
			           		</tr>
			           	</table>
					</div>
					<div class="degree">{{Config::get('myConfig.card_title.en.'.$row->card_title)}}</div>	 
	           		<div class='serial'>{!!$dep_code!!}{!!$emp_code!!}</div>

	           </div>

	        </div>
        </div>
{!! HTML::script('/js/vendor/jquery/jquery.js') !!}
{!! HTML::script('/js/vendor/bootstrap/bootstrap.js') !!}
{!! HTML::style('css/bootstrap.min.css') !!}
<script>
document.addEventListener("keydown", keyDownTextField, false);
function keyDownTextField(e) {
var keyCode = e.keyCode;
//list all CTRL + key combinations you want to disable
var forbiddenKeys = new Array('p');
var key;
var isCtrl;
if(window.event)
{
key = window.event.keyCode; //IE
if(window.event.ctrlKey)
isCtrl = true;
else
isCtrl = false;
}
else
{
key = e.which; //firefox
if(e.ctrlKey)
isCtrl = true;
else
isCtrl = false;
}
//if ctrl is pressed check if other key is in forbidenKeys array
if(isCtrl)
{
	for(i=0; i<forbiddenKeys.length; i++)
	{
		//case-insensitive comparation
		if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
		{
			alert('use print button!');

			//window.close();
			return false;
		}
	}
}
return true;
}

</script>
</body>
</html>
