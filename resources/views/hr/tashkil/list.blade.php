@extends('layouts.master')

@section('head')
    <title>{!!_('all_tashkil')!!}</title>
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
    
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>تشکیلات</span></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <div class="col-sm-12">
                	<div class="col-sm-4">
                    <select name="employee_type" class="form-control" onchange="bring_tashkilat(this.value)">
                        @for($i=1395;$i<=date('Y')-621;$i++)
                        <option value='{!!$i!!}' @if($i==$year) {!!'selected'!!} @endif>{!!$i!!}</option>
                        @endfor
                    </select>
                    </div>
                	@if(canAdd('hr_tashkil'))
                	<div class="col-sm-4">
                    <a href="javascript:void()" class="btn btn-primary pull-right" data-target="#change_employee" data-toggle="modal">
                    	<i class="fa fa-plus-circle fa-lg"></i>علاوه نمودن بست جدید</a>
                   	</div>
                    @endif
                    <div class="col-sm-4">
                    <a href="{!!URL::route('searchTashkil')!!}" class="btn btn-primary">
                    	جستجو</a>
                    </div>
                    
                </div>
            </header>

            <div class="main-box-body clearfix">
            <div class="table-responsive">
                {!!$table->render()!!}
  				{!!$table->script()!!}
            </div>
        
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="update_tashkil" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>
<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="@if(canAdd('hr_tashkil')) {!!URL::route('postNewBast')!!} @endif" enctype="multipart/form-data">
			    
			    <div class="panel-heading">
			      <h5 class="panel-title">جزییات بست</h5>
			    </div>
			    	<div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">سال</label>
	                                <select class="form-control" name="year" required>
	                                    @for($i=1395;$i<=date('Y')-621;$i++)
				                        <option value='{!!$i!!}' selected>{!!$i!!}</option>
				                        @endfor
	                                </select>
	                			</div>
	                		</div>
	                		<div class="col-sm-4">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">شماره تعینات</label>
	                                <input class="form-control" placeholder="seprate with ','" value="" type="text" name="tainat" required>
	                        	</div>
	                        </div>
	                		<div class="col-sm-2">
				            	<div class="col-sm-12">
				            		<label class="col-sm-2 ">کارکنان</label>
	                                <select name="employee_type" class="form-control" onchange="showRank(this.value)">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1' <?php echo (Input::old('employee_type')=='1' ? 'selected':''); ?>>مامور</option>
	                                    <option value='2' <?php echo (Input::old('employee_type')=='2' ? 'selected':''); ?>>اجیر</option>
	                                    <option value='3' <?php echo (Input::old('employee_type')=='3' ? 'selected':''); ?>>نظامی</option>
	                                    <option value='4' <?php echo (Input::old('employee_type')=='4' ? 'selected':''); ?>>مامور بالمقطع</option>
	                                    <option value='5' <?php echo (Input::old('employee_type')=='5' ? 'selected':''); ?>>اجیر بالمقطع</option>
	                                </select>
				               	</div>
				               	
				            </div>
				            <div id="bast_div">
					      		<div class="col-sm-2">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">بست</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "emp_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getBastStaticList('employee_rank')!!}
		                                </select>
					      			</div>
					      		</div>
					      	</div>
					      	<div id="ajir_div" style="display:none;">
						      	<div class="col-sm-2">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">درجه اجیر</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "ajir_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getBastStaticList('employee_rank')!!}
		                                </select>
					      			</div>
					      		</div>	
				      		</div>
				      		<div id="military_div" style="display:none;">
				      			<div class="col-sm-2">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">بست</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "military_bast" class="form-control">
	                                        <option value=''>انتخاب</option>
	                                        {!!getStaticDropdown('military_rank',Input::old('emp_bast'))!!}
	                                    </select>
					      			</div>
					      		</div>
				      		</div>
				      		
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	        				<div class="col-sm-4">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">عنوان وظیفه</label>
	                                <input class="form-control" type="text" name="title" value="">
				      			</div>
				      		</div>
	                		
				      		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ادارۀ عمومی</label>
	                                <select name="general_department" id="general_department" required class="form-control" onchange="bringRelatedSubDepartment('subdep',this.value)">
	                                        <option value="">انتخاب</option>
	                                    <?php $parentDeps = getMainDepartments();?>
	                                    @foreach($parentDeps AS $dep_item)
	                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>	                                        
	                                    @endforeach
	                                </select>
	                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	                			</div>
	                		</div>
	                		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12">ادارۀ مربوط</label>
	                			</div>
	                			<div class="col-sm-12">
	                                <select style="width:100%;" class="form-control" name="sub_dep" id="subdep" required>
	                                    <option value=''>انتخاب</option>
	                                </select>
	                			</div>
	                		</div>	
	                		
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-4">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">تعداد بست</label>
	                                <input class="form-control" value="1" type="text" name="bast_number" required>
	                        	</div>
	                        </div>
	                		<div class="col-sm-4">
		                		<div class="col-sm-12">
		                    		<label class="col-sm-12 ">لایحه وظایف</label>
		                    		
		                            <input type='file'  name='scan' class="form-control">
		                    	</div>
		                   </div>
		                   
	                        
	                		<div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_tashkil'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>
	                	</div>
	                </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
@stop
@section('footer-scripts')

<script type="text/javascript">
    
    function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function showRank(value)
    {
        if(value == 3)
        {//military
            $('#ajir_div').slideUp();
            $('#bast_div').slideUp();
            $('#military_div').slideDown();

        }
        else if(value == 2)
        {//ajir
        	$('#ajir_div').slideDown();
            $('#military_div').slideUp();
            $('#bast_div').slideUp();
        }
        else
        {
            $('#ajir_div').slideUp();
            $('#military_div').slideUp();
            $('#bast_div').slideDown();
        }

    }  
    function load_tashkil_det(id)
	{
		var page = "{!!URL::route('load_tashkil_det')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&id='+id,
	        success: function(r){
				$('#update_tashkil').html(r);
	        }
	    });
	}
	function bring_tashkilat(id)
	{
		window.location.href = "/hr/getTashkils/"+id;
	}
	function deleteTashkil(id)
	{
	    var confirmed = confirm("Do you want to delete?");
	    if(confirmed)
	    {
	        $.ajax({
	                url: '{!!URL::route("deleteTashkil")!!}',
	                data: '&id='+id,
	                type: 'post',
	                
	                success: function(r)
	                {
	                    location.reload();
	                }
	            }
	        );
	    }
	
	}
</script>
@stop

