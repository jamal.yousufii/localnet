@extends('layouts.master')

@section('head')
	</style>
    <title>{!!_('tashkilat')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif

</div>
<header class="main-box-header clearfix">
   	<form class="form-horizontal" role="form" method="post" action="{!!URL::route('getTashkilSearch')!!}">
    	<div class="row">
        	<div class="col-lg-12">

        		<div class="container-fluid">
			      	<div class="row">
					    <div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی</label>
                                <select name="general_department" id="general_department" required="required" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
									@if(hasRule('hr_attendance','filter_general_dept'))
									  <option value="">انتخاب</option>
									@else  
									<option value="0">انتخاب</option>
									@endif
                                    @foreach($parentDeps AS $dep_item)
                                    	@if($dep_item->id == $dep_id)
                                    		<option value='{!!$dep_item->id!!}' selected>{!!$dep_item->name!!}</option>
                                    	@else
                                        	<option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ مربوط</label>
                                <select class="form-control" name="sub_dep" id="sub_dep">
                                    <option value=''>انتخاب</option>
                                    @foreach($sub_deps AS $deps)
                                    	@if($deps->id == $sub_dep_id)
                                    		<option value='{!!$deps->id!!}' selected>{!!$deps->name!!}</option>
                                    	@else
                                        	<option value='{!!$deps->id!!}'>{!!$deps->name!!}</option>
                                        @endif
                                    @endforeach
                                </select>
                			</div>
                		</div>

					    <div class="col-sm-4">
			      			<div class="col-sm-12">
			      				<label class="col-sm-12 ">عنوان وظیفه</label>
                                <input class="form-control" type="text" name="title" value="@if($title!==0){!!$title!!}@endif">
			      			</div>
			      		</div>
					</div>
				</div>
				<div class="container-fluid">
			      	<div class="row">
			      		<div class="col-sm-2">
			            	<div class="col-sm-12">
			            		<label class="col-sm-2 ">کارکنان</label>
                                <select name="employee_type" class="form-control" onchange="showRank(this.value)">
                                    <option value='0'>انتخاب</option>
                                    <option value='1' <?php echo ($employee_type=='1' ? 'selected':''); ?>>مامور</option>
                                    <option value='2' <?php echo ($employee_type=='2' ? 'selected':''); ?>>اجیر</option>
                                    <option value='3' <?php echo ($employee_type=='3' ? 'selected':''); ?>>نظامی</option>
                                    <option value='4' <?php echo ($employee_type=='4' ? 'selected':''); ?>>مامور بالمقطع</option>
                                    <option value='5' <?php echo ($employee_type=='5' ? 'selected':''); ?>>اجیر بالمقطع</option>
                                </select>
			               	</div>

			            </div>
			            <div id="bast_div">
				      		<div class="col-sm-2">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "emp_bast" class="form-control">
	                                    <option value='0'>انتخاب</option>
	                                    {!!getBastStaticList('employee_rank')!!}
	                                </select>
				      			</div>
				      		</div>
				      	</div>
				      	<div id="ajir_div" style="display:none;">
					      	<div class="col-sm-2">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">درجه اجیر</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "ajir_bast" class="form-control">
	                                    <option value='0'>انتخاب</option>
	                                    {!!getBastStaticList('employee_rank')!!}
	                                </select>
				      			</div>
				      		</div>
			      		</div>
			      		<div id="military_div" style="display:none;">
			      			<div class="col-sm-2">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "military_bast" class="form-control">
                                        <option value='0'>انتخاب</option>
                                        {!!getStaticDropdown('military_rank',Input::old('emp_bast'))!!}
                                    </select>
				      			</div>
				      		</div>
			      		</div>
			      		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">وضعیت</label>
                                <select class="form-control" name="type">
                                    <option value='2'>انتخاب</option>
                                    <option value='1' <?php if($type==1){echo 'selected';}?> >موجود</option>
                                    <option value='0' <?php if($type==0){echo 'selected';}?>>کمبود</option>
                                </select>
                			</div>
                		</div>
                		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">سال</label>
                                <select class="form-control" name="year">
                                    @for($i=1395;$i<=date('Y')-621;$i++)
			                        <option value='{!!$i!!}' @if($i==$year) {!!'selected'!!} @endif>{!!$i!!}</option>
			                        @endfor
                                </select>
                			</div>
                		</div>
					    <div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
                                <button class="btn btn-primary pull-right" name="search" value="search" type="submit"> راپور</button>
                			</div>
                		</div>
                		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
                                <button class="btn btn-primary pull-right" name="print" type="submit" value="print"> پرنت اکسل</button>
                			</div>
                		</div>
			      	</div>
			    </div>
			</div>
		</div>
    </form>
</header>

<div class="row">
	<div class="col-lg-12">


		<div class="modal-body">
              <div class="example-wrap">

                <div class="example table-responsive">
                  <table class="table" id="list">
                    <thead>
                      	<tr>
	                        <th>#</th>
	                        <th>بست</th>
	                        <th>تعینات</th>

	                        <th>وظیفه</th>
	                        <th>دیپارتمنت</th>
	                        <th>اداره عمومی</th>
	                        <th>سال</th>
	                        <th>عملیه ها</th>
	                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
              <!-- End Example Basic -->
            </div>
	</div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="update_tashkil" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>
<div class="modal fade modal-fade-in-scale-up" id="change_employee" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		</div>

		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="@if(canAdd('hr_tashkil')) {!!URL::route('postNewBast')!!} @endif" enctype="multipart/form-data">

			    <div class="panel-heading">
			      <h5 class="panel-title">جزییات بست</h5>
			    </div>
			    	<div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-4">
				            	<div class="col-sm-12">
				            		<label class="col-sm-2 ">کارکنان</label>
	                                <select name="employee_type" class="form-control" onchange="showRank(this.value)">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1' <?php echo (Input::old('employee_type')=='1' ? 'selected':''); ?>>مامور</option>
	                                    <option value='2' <?php echo (Input::old('employee_type')=='2' ? 'selected':''); ?>>اجیر</option>
	                                    <option value='3' <?php echo (Input::old('employee_type')=='3' ? 'selected':''); ?>>نظامی</option>
	                                    <option value='4' <?php echo (Input::old('employee_type')=='4' ? 'selected':''); ?>>مامور بالمقطع</option>
	                                    <option value='5' <?php echo (Input::old('employee_type')=='5' ? 'selected':''); ?>>اجیر بالمقطع</option>
	                                </select>
				               	</div>

				            </div>
				            <div id="bast_div">
					      		<div class="col-sm-4">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">بست</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "emp_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getBastStaticList('employee_rank')!!}
		                                </select>
					      			</div>
					      		</div>
					      	</div>
					      	<div id="ajir_div" style="display:none;">
						      	<div class="col-sm-4">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">درجه اجیر</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "ajir_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getBastStaticList('employee_rank')!!}
		                                </select>
					      			</div>
					      		</div>
				      		</div>
				      		<div id="military_div" style="display:none;">
				      			<div class="col-sm-4">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">بست</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "military_bast" class="form-control">
	                                        <option value=''>انتخاب</option>
	                                        {!!getStaticDropdown('military_rank',Input::old('emp_bast'))!!}
	                                    </select>
					      			</div>
					      		</div>
				      		</div>
				      		<div class="col-sm-4">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">عنوان وظیفه</label>
	                                <input class="form-control" type="text" name="title" value="">
				      			</div>
				      		</div>
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">

	                		<div class="col-sm-4">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">تعداد بست</label>
	                                <input class="form-control" value="1" type="text" name="bast_number" required>
	                        	</div>
	                        </div>
				      		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ادارۀ عمومی</label>
	                                <select name="general_department" id="general_department" required class="form-control" onchange="bringRelatedSubDepartment('subdep',this.value)">
	                                        <option value="">انتخاب</option>
	                                    @foreach($parentDeps AS $dep_item)

	                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>

	                                    @endforeach
	                                </select>
	                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	                			</div>
	                		</div>
	                		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ادارۀ مربوط</label>
	                                <select class="form-control" name="sub_dep" id="subdep" required>
	                                    <option value=''>انتخاب</option>
	                                </select>
	                			</div>
	                		</div>

	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-4">
		                		<div class="col-sm-12">
		                    		<label class="col-sm-12 ">لایحه وظایف</label>

		                            <input type='file'  name='scan' class="form-control">
		                    	</div>
		                   </div>
		                   <div class="col-sm-4">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">شماره تعینات</label>
	                                <input class="form-control" value="" type="text" name="tainat" required>
	                        	</div>
	                        </div>
	                		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_tashkil'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>
	                	</div>
	                </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->
<?php
$title = preg_replace('/\s+/', '-', $title);
?>
@stop
@section('footer-scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $('#list').dataTable(
            {
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::route('getTashkilSearchData',array($dep_id,$sub_dep_id,$type,$bast,$title,$employee_type,$year))!!}"
            }
        );

    });
	function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function load_tashkil_det(id)
	{
		var page = "{!!URL::route('load_tashkil_det')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&id='+id,
	        success: function(r){
				$('#update_tashkil').html(r);
	        }
	    });
	}
	function deleteTashkil(id)
	{
	    var confirmed = confirm("Do you want to delete?");
	    if(confirmed)
	    {
	        $.ajax({
	                url: '{!!URL::route("deleteTashkil")!!}',
	                data: '&id='+id,
	                type: 'post',

	                success: function(response)
	                {
	                    location.reload();
	                }
	            }
	        );
	    }

	}

	// Get employee details by Tashkil ID
	function getEmpDetailsByTashkil(tashkil_id)
	{
			 $.ajax({
								url: '{!!URL::route("get_emp_details_by_tashkil")!!}',
								data: '&tashkil_id='+tashkil_id,
								type: 'post',

								success: function(response)
								{
										$('#tashkil_'+tashkil_id).prop('title',response);
								}
						}
				);
	}
</script>
@stop
