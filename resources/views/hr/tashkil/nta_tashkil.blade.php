<div class="container-fluid">
    <div class="row" >
        <div class="col-sm-4">
            <div class="col-sm-12">
                <label class="col-sm-12 ">سال</label>
                <select class="form-control" name="year" required>
                    @for($i=1395;$i<=date('Y')-621;$i++)
                    <option value='{!!$i!!}' selected>{!!$i!!}</option>
                    @endfor
                </select>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="col-sm-12">
                <label class="col-sm-12 ">شماره تعینات</label>
                <input class="form-control" placeholder="seprate with ','" value="" type="text" name="tainat" required>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="col-sm-12">
                <label class="col-sm-2 ">کتگوری</label>
                <?php $counter=0 ?> 
                <select name="nta_category" class="form-control">
                    <option value=''>انتخاب</option>
                    @foreach (Config::get('myConfig.nta_category.'.$lang) as $category)
                        <option value="{{$counter}}">{{$category}}</option>
                        <?php $counter++ ?> 
                    @endforeach
                </select>
               </div>
               
        </div>
        <div id="bast_div">
              <div class="col-sm-2">
                  <div class="col-sm-12">
                      <label class="col-sm-12 ">قدم</label>
                    <select name = "nta_qadam" class="form-control">
                        <option value=''>انتخاب</option>
                        @foreach (Config::get('myConfig.nta_qadam') as $category)
                            <option value="{{$counter}}">{{$category}}</option>
                            <?php $counter++ ?> 
                        @endforeach
                    </select>
                  </div>
              </div>
          </div>
          <div id="ajir_div" style="display:none;">
              <div class="col-sm-2">
                  <div class="col-sm-12">
                      <label class="col-sm-12 ">درجه اجیر</label>
                    <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
                    <select name = "ajir_bast" class="form-control">
                        <option value=''>انتخاب</option>
                        {!!getBastStaticList('employee_rank')!!}
                    </select>
                  </div>
              </div>	
          </div>
          <div id="military_div" style="display:none;">
              <div class="col-sm-2">
                  <div class="col-sm-12">
                      <label class="col-sm-12 ">بست</label>
                    <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
                    <select name = "military_bast" class="form-control">
                        <option value=''>انتخاب</option>
                        {!!getStaticDropdown('military_rank',Input::old('emp_bast'))!!}
                    </select>
                  </div>
              </div>
          </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4">
              <div class="col-sm-12">
                  <label class="col-sm-12 ">عنوان وظیفه</label>
                <input class="form-control" type="text" name="title" value="">
              </div>
          </div>
        
          <div class="col-sm-4">
            <div class="col-sm-12">
                <label class="col-sm-12 ">ادارۀ عمومی</label>
                <select name="general_department" id="general_department" required class="form-control" onchange="bringRelatedSubDepartment('subdep',this.value)">
                        <option value="">انتخاب</option>
                    <?php $parentDeps = getMainDepartments();?>
                    @foreach($parentDeps AS $dep_item)
                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>	                                        
                    @endforeach
                </select>
                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
            </div>
        </div>
        <div class="col-sm-4">
            <div class="col-sm-12">
                <label class="col-sm-12">ادارۀ مربوط</label>
            </div>
            <div class="col-sm-12">
                <select style="width:100%;" class="form-control" name="sub_dep" id="subdep" required>
                    <option value=''>انتخاب</option>
                </select>
            </div>
        </div>	
        
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4">
            <div class="col-sm-12">
                <label class="col-sm-12 ">تعداد بست</label>
                <input class="form-control" value="1" type="text" name="bast_number" required>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="col-sm-12">
                <label class="col-sm-12 ">لایحه وظایف</label>
                
                <input type='file'  name='scan' class="form-control">
            </div>
       </div>
    </div>
</div>