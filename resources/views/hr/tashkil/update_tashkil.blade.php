<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" id="close_btn" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" id="updateBast_form" method="post" action="@if(canAdd('hr_tashkil')) {!!URL::route('updateBast')!!} @endif" enctype="multipart/form-data">
			    
			    <div class="panel-heading">
			      <h5 class="panel-title">جزییات بست</h5>
			    </div>
			    	<div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-4">
				            	<div class="col-sm-12">
				            		<label class="col-sm-2 ">کارکنان</label>
	                                <select name="employee_type" class="form-control" onchange="showRank(this.value)">
	                                    <option value=''>انتخاب</option>
	                                    <option value='1' <?php echo ($details->employee_type=='1' ? 'selected':''); ?>>مامور</option>
	                                    <option value='2' <?php echo ($details->employee_type=='2' ? 'selected':''); ?>>اجیر</option>
	                                    <option value='3' <?php echo ($details->employee_type=='3' ? 'selected':''); ?>>نظامی</option>
	                                    <option value='4' <?php echo ($details->employee_type=='4' ? 'selected':''); ?>>مامور بالمقطع</option>
	                                    <option value='5' <?php echo ($details->employee_type=='5' ? 'selected':''); ?>>اجیر بالمقطع</option>
	                                </select>
				               	</div>
				               	
				            </div>
				            <div id="bast_div_t" @if($details->employee_type==2 || $details->employee_type==3) style="display:none;" @endif>
					      		<div class="col-sm-4">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">بست</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "emp_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getBastStaticList('employee_rank',$details->bast)!!}
		                                </select>
					      			</div>
					      		</div>
					      	</div>
					      	<div id="ajir_div_t" @if($details->employee_type!=2) style="display:none;" @endif>
						      	<div class="col-sm-4">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">درجه اجیر</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "ajir_bast" class="form-control">
		                                    <option value=''>انتخاب</option>
		                                    {!!getBastStaticList('employee_rank',$details->bast)!!}
		                                </select>
					      			</div>
					      		</div>	
				      		</div>
				      		<div id="military_div_t" @if($details->employee_type!=3) style="display:none;" @endif>
				      			<div class="col-sm-4">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">بست</label>
		                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
		                                <select name = "military_bast" class="form-control">
	                                        <option value=''>انتخاب</option>
	                                        {!!getStaticDropdown('military_rank',$details->bast)!!}
	                                    </select>
					      			</div>
					      		</div>
				      		</div>
				      		<div class="col-sm-4">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">عنوان وظیفه</label>
	                                <input class="form-control" type="text" name="title" value="{!!$details->title!!}">
				      			</div>
				      		</div>
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
				      		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ادارۀ عمومی</label>
	                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('subdep',this.value)">
	                                        <option value="">انتخاب</option>
	                                    @foreach($parentDeps AS $dep_item)
	                                    	@if($details->dep_id == $dep_item->id)
	                                    		<option value='{!!$dep_item->id!!}' selected>{!!$dep_item->name!!}</option>
	                                        
	                                        @else
	                                        	<option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>
	                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	                			</div>
	                		</div>
	                		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">ادارۀ مربوط</label>
	                                <select class="form-control" name="sub_dep" id="subdep">
	                                    <option value=''>انتخاب</option>
	                                    @foreach($sud_dep AS $dep)
	                                    	@if($details->sub_dep_id == $dep->id)
	                                    		<option value='{!!$dep->id!!}' selected>{!!$dep->name!!}</option>
	                                        
	                                        @else
	                                        	<option value='{!!$dep->id!!}'>{!!$dep->name!!}</option>
	                                        @endif
	                                    @endforeach
	                                </select>
	                			</div>
	                		</div>	
	                		<div class="col-sm-4">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">شماره تعینات</label>
	                                <input class="form-control" value="{!!$details->tainat!!}" type="text" name="tainat">
	                        	</div>
	                        </div>
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<!--
	                		<div class="col-sm-8">
		                		<div class="col-sm-12">
		                    		<label class="col-sm-12 ">لایحه وظایف</label>
		                    		
		                            <input type='file'  name='scan' class="form-control">
		                    	</div>
		                   	</div>
		                   	-->
		                   <div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">سال</label>
	                                <select class="form-control" name="year" required>
	                                    @for($i=1395;$i<=date('Y')-621;$i++)
				                        <option value='{!!$i!!}' @if($i==$details->year) {!!'selected'!!} @endif>{!!$i!!}</option>
				                        @endfor
	                                </select>
	                			</div>
	                		</div>
	                		<div class="col-sm-2">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_tashkil'))
	                					<button class="btn btn-primary" type="button" onclick="do_update()">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>
	                	</div>
	                </div>
	                <input type="hidden" name="bast_id" value="{!!$id!!}"/>
	                <input type="hidden" name="scan_file" value="{!!$details->job_desc!!}"/>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
<script>
function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function showRank(value)
    {
        if(value == 3)
        {//military
            $('#ajir_div_t').slideUp();
            $('#bast_div_t').slideUp();
            $('#military_div_t').slideDown();

        }
        else if(value == 2)
        {//ajir
        	$('#ajir_div_t').slideDown();
            $('#military_div_t').slideUp();
            $('#bast_div_t').slideUp();
        }
        else
        {
            $('#ajir_div_t').slideUp();
            $('#military_div_t').slideUp();
            $('#bast_div_t').slideDown();
        }

    }  
function do_update()
{
	var page = "{!!URL::route('updateBast')!!}";
    
    $.ajax({
        url: page,
        type: 'post',
        data: $('#updateBast_form').serialize(),
        dataType:'HTML',
        success: function(response)
      	{
      		$('#close_btn').click();
      		alert('updated');
        	//$('#search_result').html(response);
        }
    });   
}
</script>