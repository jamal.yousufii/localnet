@extends('layouts.master')

@section('head')
    {!! HTML::style('/vendor/select2/select2.css') !!}
    {!! HTML::script('/js/vendor/select2/select2.min.js')!!}
    {!! HTML::script('/js/components/select2.js')!!}
	{!! HTML::style('/css/farsi_date/pwt-datepicker.css') !!}
    <title>{!!_('recruitment')!!}</title>
@stop
@section('content')
<div class="row">
	@if(Session::has('success'))
		<div class='alert alert-success span6'>{!!Session::get('success')!!}</div>
	@elseif(Session::has('fail'))
		<div class='alert alert-danger span6'>{!!Session::get('fail')!!}</div>
	@endif

    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="{!!URL::to('/')!!}">لوحه معلومات</a></li>
            <li class="active"><span>تشکیلات</span></li>
        </ol>
    </div>
</div>
<header class="main-box-header clearfix">
  <form class="form-horizontal" role="form" method="post" action="{!!URL::route('getTashkilSearch')!!}">
    	<div class="row">
        	<div class="col-lg-12">
        		<div class="container-fluid">
			      	<div class="row">

					    <div class="col-sm-4">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">ادارۀ عمومی</label>
                        <?php $parentDeps = getMainDepartments();?>
                                <select name="general_department" id="general_department" class="form-control" onchange="bringRelatedSubDepartment('sub_dep',this.value)">
                                        <option value="0">انتخاب</option>
                                    @foreach($parentDeps AS $dep_item)
                                        <option value='{!!$dep_item->id!!}'>{!!$dep_item->name!!}</option>
                                    @endforeach
                                </select>
                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
                			</div>
                		</div>
                		<div class="col-sm-4">
                            <label class="col-sm-12 ">ادارۀ مربوط</label>
                			<div class="col-sm-12">

                                <select class="form-control" name="sub_dep" id="sub_dep" style="width:100%;">
                                    <option value=''>انتخاب</option>
                                </select>
                			</div>
                		</div>
                		<div class="col-sm-4">
			      			<div class="col-sm-12">
			      				<label class="col-sm-12 ">عنوان وظیفه</label>
                                <input class="form-control" type="text" name="title" value="">
			      			</div>
			      		</div>

					</div>
				</div>
				<div class="container-fluid">
			      	<div class="row">
			      		<div class="col-sm-4">
			            	<div class="col-sm-12">
			            		<label class="col-sm-2 ">کارکنان</label>
                                <select name="employee_type" class="form-control" onchange="showRank(this.value)">
                                    <option value='0'>انتخاب</option>
                                    <option value='1' <?php echo (Input::old('employee_type')=='1' ? 'selected':''); ?>>مامور</option>
                                    <option value='2' <?php echo (Input::old('employee_type')=='2' ? 'selected':''); ?>>اجیر</option>
                                    <option value='3' <?php echo (Input::old('employee_type')=='3' ? 'selected':''); ?>>نظامی</option>
                                    <option value='4' <?php echo (Input::old('employee_type')=='4' ? 'selected':''); ?>>مامور بالمقطع</option>
                                    <option value='5' <?php echo (Input::old('employee_type')=='5' ? 'selected':''); ?>>اجیر بالمقطع</option>
                                </select>
			               	</div>

			            </div>
			            <div id="bast_div">
				      		<div class="col-sm-2">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "emp_bast" class="form-control">
	                                    <option value='0'>انتخاب</option>
	                                    {!!getBastStaticList('employee_rank')!!}
	                                </select>
				      			</div>
				      		</div>
				      	</div>
				      	<div id="ajir_div" style="display:none;">
					      	<div class="col-sm-2">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">درجه اجیر</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "ajir_bast" class="form-control">
	                                    <option value='0'>انتخاب</option>
	                                    {!!getBastStaticList('employee_rank')!!}
	                                </select>
				      			</div>
				      		</div>
			      		</div>
			      		<div id="military_div" style="display:none;">
			      			<div class="col-sm-2">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">بست</label>
	                                <!-- <input class="form-control" type="text" name="emp_bast" value="{!!Input::old('emp_bast')!!}"> -->
	                                <select name = "military_bast" class="form-control">
                                        <option value='0'>انتخاب</option>
                                        {!!getStaticDropdown('military_rank',Input::old('emp_bast'))!!}
                                    </select>
				      			</div>
				      		</div>
			      		</div>
			      		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">وضعیت</label>
                                <select class="form-control" name="type">
                                    <option value='2'>انتخاب</option>
                                    <option value='1'>موجود</option>
                                    <option value='0'>کمبود</option>
                                </select>
                			</div>
                		</div>
                		<div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">سال</label>
                                <select class="form-control" name="year">
                                    @for($i=1395;$i<=date('Y')-621;$i++)
                                        <option value='{!!$i!!}' selected>{!!$i!!}</option>
                                    @endfor
                                </select>
                			</div>
                		</div>
					    <div class="col-sm-2">
                			<div class="col-sm-12">
                				<label class="col-sm-12 ">&nbsp;</label>
                                <button class="btn btn-primary pull-right" name="search" value="search" type="submit"> راپور</button>
                			</div>
                		</div>
			      	</div>
			    </div>
			</div>
		</div>
    </form>
</header>

<!-- Example Tabs -->
<div id="all">
</div>
<!-- End Example Tabs -->

@stop
@section('footer-scripts')

<script type="text/javascript">
    $("#sub_dep").select2();
	function bringRelatedSubDepartment(div,id)
    {
        $.ajax({
                url: '{!!URL::route("bringSubDepartment")!!}',
                data: '&dep_id='+id,
                type: 'post',
                beforeSend: function(){
                    $("#"+div).html('<span style="float:center;">{!!HTML::image("/img/ajax-loader.gif")!!}</span>');
                },
                success: function(response)
                {
                    $('#'+div).html(response);
                }
            }
        );
    }
    function showRank(value)
    {
        if(value == 3)
        {//military
            $('#ajir_div').slideUp();
            $('#bast_div').slideUp();
            $('#military_div').slideDown();

        }
        else if(value == 2)
        {//ajir
        	$('#ajir_div').slideDown();
            $('#military_div').slideUp();
            $('#bast_div').slideUp();
        }
        else
        {
            $('#ajir_div').slideUp();
            $('#military_div').slideUp();
            $('#bast_div').slideDown();
        }

    }
</script>
@stop
