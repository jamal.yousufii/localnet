<html>
	<head>
		<meta charset="UTF-8">
		<style type="text/css">
			* {line-height: 1em;
				padding: 0px;
				margin: 0px;
				padding-bottom: 1px;
				padding-right: 1px;
			}

			.front{
				height: 8.5525cm;
				width:5.4cm;
				flo/at: left;
				position:relative;
				font-family: 'B Titr';

				overflow: hidden;

			}

			.back{
				height: 8.5525cm;
				width:5.4cm;
				flo/at: right;
				position:relative;
				font-family: 'B Titr';
				overflow: hidden;
				font-weight: bold;
			}

	        .front img {
	            height:8.5725cm;
				width:5.4cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .back img {
	            height:8.5725cm;
				width:5.4cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .do-print{
	        	font-family: 'B Titr';
            	font-size: 12pt;

	        }
	        .front .serial{
	        	position: absolute;
	        	font-family: 'B Titr';
	        	z-index: 10;
	        	top: 1.7cm;
	        	left: 0.35cm;

	        	font-size: 7pt;

	        	color: red;

	        }

	        .front .validaty_date_title{
	        	position: absolute;
	        	z-index: 10;
	        	right:0.5cm;
	        	top: 1.7cm;
	        	font-size: 7.17pt;
	        	color:white;

	        }
	        .front .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	left:1.7cm;
	        	top: 1.7cm;
	        	font-size: 9pt;

	        	color: red;

	        }


	        .front .photo {
	        	position: absolute;
	        	width: 2.68cm;
	        	height: 2.94cm;
	        	top: 2.3cm;
	        	right: 1.34cm;
	        	z-index: 10;
	        }
	        .front .title{
	        	position: absolute;
	        	z-index: 10;
	        	top: 5.3cm;
	        	color: black;
	        	width: 5.3cm;
	        	text-align: center;
	        	font-size: 14pt;
	        	font-weight: bold;
	        	color:white;
	        }
	        .front .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 6cm;
	        	color: black;
	        	width: 5.3cm;
	        	text-align: center;
	        	font-size: 12pt;
	        	font-weight: bold;
	        }

	        .front .title_value{
	        	position: abs/olute;
	        	z-index: 10;
	        	top: 5.8cm;
	        	text-align: center;
	        	width: 5.3cm;
	        	padding-bottom: 3px;
	        	font-size: 10pt;
	        	font-weight: bold;

	        }

	        .front .dep_value{
	        	position: abs/olute;
	        	z-index: 10;
	        	top: 6.2cm;
	        	text-align: center;
	        	width: 5.3cm;
	        	padding-bottom: 3px;
	        	font-size: 8pt;
	        	font-weight: bold;
	        	display: inline-block;
	        }

	        .front .info{
	        	position: absolute;
	        	z-index: 10;
	        	left:1cm;
	        	top: 8.05cm;
	        	font-size: 7pt;

	        }
	        .front .blood{
	        	position: absolute;
	        	z-index: 10;
	        	right:0.2cm;
	        	top: 7.6cm;
	        	font-size: 8pt;

	        }
	        .front .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 8cm;
	        	right: 0.4cm;

	        	font-size: 11pt;

	        	color: red;

	        }


	        .front .group {
	        	position: absolute;
	        	z-index: 10;
	        	top: 6.5cm;
	        	text-align: center;
	        	width: 5.3cm;

	        }
	        .back .serial{
	        	position: absolute;
	        	z-index: 10;
	        	top: 1.7cm;
	        	right:0.2cm;

	        	font-size: 7pt;
	        	font-family: 'Times New Roman';
	        	color: red;
	        }

	        .back .validaty_date_title{
	        	position: absolute;
	        	z-index: 10;
	        	left:0.4cm;
	        	top: 1.7cm;
	        	font-size: 7.17pt;
	        	color:white;

	        }
	        .back .validaty_date{
	        	position: absolute;
	        	z-index: 10;
	        	left:2.2cm;
	        	top: 1.65cm;
	        	font-size: 9pt;
	        	font-family: 'Times New Roman';
	        	color: red;

	        }


	        .back .photo {
	        	position: absolute;
	        	width: 2.74cm;
	        	height: 2.96cm;
	        	top: 2.28cm;
	        	left: 1.4cm;
	        	z-index: 10;
	        }

	        .back .name_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 6.1cm;
	        	width: 5.2cm;
	        	text-align: center;
	        	font-size: 10pt;
	        	font-weight: b/old;
	        }
	        .back .title{
	        	position: absolute;
	        	z-index: 10;
	        	top: 5.4cm;
	        	left: 2cm;

	        	font-size: 12pt;
	        	font-weight: bold;
	        	color:white;
	        }

	        .back .title_value{
	        	position: abso/lute;
	        	z-index: 10;
	        	font-weight: bold;
	        	text-align: center;
	        	width: 5.2cm;
	        	padding-bottom: 4px;
	        	font-size: 9pt;
	        }

	        .back .dep_value{
	        	position: abs/olute;
	        	z-index: 10;
	        	font-weight: bold;
	        	text-align: center;
	        	width: 5.2cm;
	        	padding-bottom: 4px;
	        	font-size: 7.8pt;

	        	display: inline-block;
	        }
	        .back .info{
	        	position: absolute;
	        	z-index: 10;
	        	right:1cm;
	        	top: 8cm;
	        	font-size: 7pt;
	        	font-family: 'Times New Roman';
	        }
	        .back .blood{
	        	position: absolute;
	        	z-index: 10;
	        	left:0.2cm;
	        	top: 7.6cm;
	        	font-size: 7pt;
	        }
	        .back .blood_value{
	        	position: absolute;
	        	z-index: 10;
	        	top: 7.85cm;
	        	left: 0.6cm;

	        	font-size: 11pt;

	        	color: red;

	        }
	        .back .group {
	        	position: absolute;
	        	z-index: 10;
	        	top: 6.9cm;
	        	text-align: center;
	        	width: 5.2cm;


	        }
	        <?php
	        if($row->position_id==1)
	        {
	        	$card_id = 'head.jpg';$degree = 'A'; ?>
	        	.back .validaty_date{
	        	color: white;
	        	}
	        	.front .validaty_date{
	        	color: white;
	        	}
	        	.new-card-front {
				  background-image: url('{!! asset('img/deputy_cards/front_head.jpg') !!}');
				  background-size: 5.5cm 8.5525cm;

			    }
			    .new-card-back {
				  background-image: url('{!! asset('img/deputy_cards/back_head.jpg') !!}');
				  background-size: 5.6cm 8.5525cm;
				  height: 8.5525cm;

				}
	        <?php
	    	}
	        elseif($row->position_id==2)
	        {
	        	$card_id = 'employee.jpg';$degree = 'B'; ?>
	        	.back .validaty_date{
	        	color: red;
	        	}
	        	.front .validaty_date{
	        	color: red;
	        	}
	        	.new-card-front {
				  background-image: url('{!! asset('img/deputy_cards/front_employee.jpg') !!}');
				  background-size: 5.5cm 8.5525cm;

			    }
	        	.new-card-back {
				  background-image: url('{!! asset('img/deputy_cards/back_employee.jpg') !!}');
				  background-size: 5.6cm 8.5525cm;
				  height: 8.5525cm;

				}
	        <?php
	    	}
	        elseif($row->position_id==3)
	        {
	        	$card_id = 'ajir.jpg';$degree = 'C'; ?>
	        	.back .validaty_date{
	        	color: red;
	        	}
	        	.front .validaty_date{
	        	color: red;
	        	}
	        	.new-card-front {
				  background-image: url('{!! asset('img/deputy_cards/front_ajir.jpg') !!}');
				  background-size: 5.5cm 8.5525cm;

			    }
	        	.new-card-back {
				  background-image: url('{!! asset('img/deputy_cards/back_ajir.jpg') !!}');
				  background-size: 5.6cm 8.5525cm;
				  height: 8.5525cm;

				}
	        <?php
	    	}
	        else
	        {
	        	$card_id = 'ajir.jpg';$degree = 'C';?>
	        	.back .validaty_date{
	        	color: red;
	        	}
	        	.front .validaty_date{
	        	color: red;
	        	}
	        	.new-card-front {
				  background-image: url('{!! asset('img/deputy_cards/front_ajir.jpg') !!}');
				  background-size: 5.5cm 8.5525cm;

			    }
			    .new-card-back {
				  background-image: url('{!! asset('img/deputy_cards/back_ajir.jpg') !!}');
				  background-size: 5.6cm 8.5525cm;
				  height: 8.5525cm;

				}
	        <?php
	        }

	        ?>


			}
			.new-card-back {
			  background-image: url('{!! asset('img/deputy_cards/back_'.$card_id.'') !!}');
			  background-size: 5.6cm 8.5525cm;
			  height: 8.5525cm;

			}
		</style>

	</head>
	<?php
	$img = Image::make(file_get_contents('documents/profile_pictures/'.$row->photo ));

		$img->encode('jpg');
		$type = 'jpg';
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);

	?>
	<body>
		<div class="do-print">
	        <div class="row">
	           <div class="col front new-card-front" dir='rtl'>
	           	<!-- {!!$row->eid!!} -->
	           		<?php
	           			//$position_ids = array(1,2,3,11);
	           			//$position_ids = array(11,3,17,18);
	           			$position_ids = array(11,17,18);

	           			$dep_id = $row->sudep_id;
	           			$dep_code = $dep_id;
	           			if($dep_id<10)
	           			{
	           				$dep_code = '00'.$dep_id;
	           			}
	           			elseif($dep_id < 100)
	           			{
	           				$dep_code = '0'.$dep_id;
	           			}


	     				$emp_id = $row->eid;
	     				$emp_code = $emp_id;

	     				if($emp_id<10)
	     				{
	     					$emp_code = '000'.$emp_id;
	     				}
	     				elseif($emp_id < 100)
	     				{
	     					$emp_code = '00'.$emp_id;
	     				}
	     				elseif($emp_id <1000)
	     				{
	     					$emp_code = '0'.$emp_id;
	     				}

	           		?>

	           	<img src="{!!$base64!!}" class="photo">
	           	<div class="name_value">{!!$row->name_dr!!} {!!$row->last_name!!}</div>
	           	<div class="group">
		           	<table cellspacing=0 cellpadding=0>

		           		<tr>
		           			<td>
		           				<div class="title_value">{!!$row->current_position_dr!!}</div>

		           			</td>
		           		</tr>

		           		<tr>
		           			<td>
		           				<div class="dep_value">

		           			<?php
		           			/*
		           				if(in_array($row->position_id, $position_ids))
		           				{
		           					if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58)
		           					{//frist and second deputy
		           						echo $row->department_dr;
		           					}
		           					else
		           					{
		           						echo $row->general_department_dr;
		           					}
		           				}
		           				else
		           				{
		           					echo $row->department_dr;
		           				}
		           			*/
		           			echo $row->department_dr;
		           			//echo $row->position_id;
		           			?>

		           				</div>

		           			</td>
		           		</tr>
		           	</table>
	      	   	</div>

	           	<div class='serial'>{!!$dep_code!!}{!!$emp_code!!}</div>
	           	<div class="validaty_date_title">تاریخ انقضاء</div>

	           	<div class="validaty_date">۱۳۹۸/۱۲/۲۹</div>

	           	<div class="info">info@aop.gov.af +93202147356</div>
	           </div>
						 <div style="width: 5.5cm;">
						 	<a href="{!!route('getCardTemplatePrint',$row->eid)!!}" style="width: 5.5cm;" class="btn btn-primary">Print</a>
						 </div>
	           	<div class="col back new-card-back">
	           		<img src="{!!$base64!!}" class="photo">

		       		<div class="name_value">{!!$row->name_en!!}</div>
		       		<div class="group">
			           	<table cellspacing=0 cellpadding="0" style="line-height: 1em;">
			           		<tr>
			           			<td>
			           				<div class="title_value">{!!$row->current_position_en!!}</div>

			           			</td>
			           		</tr>
			           		<tr>
			           			<td>
			           				<div class="dep_value">
			           			<?php
			           			/*
			           				if(in_array($row->position_id, $position_ids))
			           				{
			           					if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58)
			           					{//frist and second deputy
			           						echo $row->department_en;
			           					}
			           					else
			           					{
			           						echo $row->general_department_en;
			           					}
			           				}
			           				else
			           				{
			           					echo $row->department_en;
			           				}
			           			*/
			           			echo $row->department_en;
			           			//echo $row->department_en;
			           			?>
			           				</div>
			           			</td>
			           		</tr>
			           	</table>
	      	   		</div>


	           		<div class='serial'>{!!$dep_code!!}{!!$emp_code!!}</div>
		           	<div class="info">info@aop.gov.af +93202147356</div>
		           	<div class="validaty_date_title">Expiry Date</div>

		           	<div class="validaty_date">19/03/2020</div>

	           </div>

	        </div>
        </div>
{!! HTML::script('/js/vendor/jquery/jquery.js') !!}
{!! HTML::script('/js/vendor/bootstrap/bootstrap.js') !!}
{!! HTML::style('css/bootstrap.min.css') !!}
<script>
document.addEventListener("keydown", keyDownTextField, false);
function keyDownTextField(e) {
var keyCode = e.keyCode;
//list all CTRL + key combinations you want to disable
var forbiddenKeys = new Array('p');
var key;
var isCtrl;
if(window.event)
{
key = window.event.keyCode; //IE
if(window.event.ctrlKey)
isCtrl = true;
else
isCtrl = false;
}
else
{
key = e.which; //firefox
if(e.ctrlKey)
isCtrl = true;
else
isCtrl = false;
}
//if ctrl is pressed check if other key is in forbidenKeys array
if(isCtrl)
{
	for(i=0; i<forbiddenKeys.length; i++)
	{
		//case-insensitive comparation
		if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
		{
			alert('use print button!');

			window.close();
			return false;
		}
	}
}
return true;
}

</script>
	</body>
</html>
