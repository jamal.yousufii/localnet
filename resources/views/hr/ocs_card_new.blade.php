<html>
	<head>
		<meta charset="UTF-8">
		<style type="text/css">
			@font-face {
                font-family: "Baran";
                src: url('{!! asset('css/fonts/Baran.ttf') !!}');
            }

			* {line-height: 1em;
				padding: 0px;
				margin: 0px;
				padding-bottom: 1px;
				padding-right: 1px;
			}

			html{
                font-family: Baran !important;
            }

			.front{
				height: 5.4cm;
                width: 8.5525cm;
				position:relative;
				overflow: hidden;

			}

			.back{
				height: 5.4cm;
                width: 8.5525cm;
				position:relative;
				font-family: 'Times New Roman';
				overflow: hidden;
			}

	        .front img {
	            height: 5.4cm;
                width: 8.5525cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .back img {
	            height: 5.4cm;
                width: 8.5525cm;
	            z-index: 1;
	            position: absolute;
	        }

	        .do-print{
	        	font-family: 'Baran';
            	font-size: 12pt;
            	color: white;
	        }

	        .front .validaty_date{
	        	position: absolute;
                z-index: 10;
                right: 1.73cm;
                top: 5.04cm;
                font-size: 6pt;
                color: white;
	        }

	        .front .photo {
                position: absolute;
                width: 2.7cm;
                height: 2.83cm;
                top: 2.005cm;
                left: 5.5cm;
                z-index: 10;
            }

	        .front .name_value {
                position: absolute;
                z-index: 10;
                top: 2.2cm;
                width: 5cm;
                text-align: right;
                font-size: 10pt;
                right: 3.7cm;
                font-weight: bold;
                color: black;
            }

			.front .title_value {
                position: absolute;
                z-index: 10;
                top: 3.1cm;
                text-align: right;
                width: 3.4cm;
                font-size: 7.5pt;
                color: black;
                right: 3.86cm;
            }

	        .front .dep_value{
	        	position: absolute;
                z-index: 10;
                top: 3.78cm;
                text-align: right;
                width: 3.4cm;
                font-size: 7.5pt;
                color: black;
                right: 3.85cm;
	        }

	        .front .blood_value{
	        	position: absolute;
                z-index: 10;
                font-size: 6pt;
                right: 5.3cm;
                top: 4.4cm;
                color: black;
	        }

	        .front .group {
	        	position: absolute;
	        	z-index: 10;
	        	top: 5.8cm;
	        	text-align: center;
	        }

	        .front .serial {
                position: absolute;
                z-index: 10;
                font-size: 6pt;
                right: 4cm;
                top: 4.4cm;
                color: black;
            }


            .back .serial {
                position: absolute;
                z-index: 10;
                font-size: 6pt;
                left: 3.12cm;
                top: 4.4cm;
                color: black;
            }

	        .back .validaty_date{
                position: absolute;
                z-index: 10;
                right: 1.13cm;
                top: 5.13cm;
                font-size: 6pt;
                color: white;
	        }

	        .back .photo {
                position: absolute;
                width: 2.6cm;
                height: 2.87cm;
                top: 2.05cm;
                right: 5.53cm;
                z-index: 10;
            }

	        .back .name_value {
                position: absolute;
                z-index: 10;
                top: 2.25cm;
                width: 5cm;
                text-align: left;
                font-size: 10pt;
                right: 0.47cm;
                font-weight: bold;
                color: black;
            }

	        .back .title_value {
                position: absolute;
                z-index: 10;
                top: 3.1cm;
                text-align: left;
                width: 4.2cm;
                font-size: 6.5pt;
                color: black;
                left: 3.1cm;
            }

	        .back .dep_value{
                position: absolute;
                z-index: 10;
                top: 3.78cm;
                text-align: left;
                width: 4.3cm;
                font-size: 6.5pt;
                color: black;
                left: 3.1cm;
	        }

	        .back .blood_value{
                position: absolute;
                z-index: 10;
                font-size: 6pt;
                left: 4.9cm;
                top: 4.4cm;
                color: black;
	        }

	        .back .group {
	        	position: absolute;
	        	z-index: 10;
	        	top: 5.7cm;
	        	text-align: center;

	        }
	        <?php
	        if($row->position_id==4 || $row->position_id == 5)
	        	$card_id = 'head.jpg';
	        elseif($row->position_id==6)
	        	$card_id = 'employee.jpg';
	        elseif($row->position_id==3)
	        	$card_id = 'director.jpg';
	        elseif($row->position_id==8)
	        	$card_id = 'deputy.jpg';
	        elseif($row->position_id==7)
	        	$card_id = 'ajir.jpg';
	        else
	        	$card_id = 'employee.jpg';
	        ?>
	        .new-card-front {
			  background-image: url('{!! asset('img/ocs_cards/cards/ocs_front.jpeg') !!}');
			  background-size: 8.5525cm 5.5cm;


			}
			.new-card-back {
			  background-image: url('{!! asset('img/ocs_cards/cards/ocs_back.jpeg') !!}');
			  background-size: 8.5525cm 5.6cm;

			}
		</style>

	</head>
    <?php
        //$img = Image::make(file_get_contents('documents/profile_pictures/'.$row->photo ))->crop(1478, 1797);
        if(empty($row->photo))
          exit('عکس کارمند شما اپلود نشده است دوباره عکس همکار تان را اپلود نماید.'); 
          
        $img = Image::make(file_get_contents('documents/profile_pictures/'.$row->photo ));
		$img->encode('jpg');
		$type = 'jpg';
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);

	?>
	<body>
		<div class="do-print">
	        <div class="row">
	           	<div class="col front new-card-front" dir='rtl'>
	           		<!-- {!!$row->eid!!} -->
	           		<?php
	           			//$position_ids = array(1,2,3,11);
                           //$position_ids = array(11,3,17,18);
                        // if bazras then department should be edara baz ras 
                        $general_dept = array(536,573); 
                        $position_ids = array(11,17,18);
                        $department   = array(564);
                           

	           			$dep_id = $row->sudep_id;
	           			$dep_code = $dep_id;
	           			if($dep_id<10)
	           			{
	           				$dep_code = '00'.$dep_id;
	           			}
	           			elseif($dep_id < 100)
	           			{
	           				$dep_code = '0'.$dep_id;
	           			}

	     				$emp_id = $row->eid;
	     				$emp_code = $emp_id;

	     				if($emp_id<10)
	     				{
	     					$emp_code = '000'.$emp_id;
	     				}
	     				elseif($emp_id < 100)
	     				{
	     					$emp_code = '00'.$emp_id;
	     				}
	     				elseif($emp_id <1000)
	     				{
	     					$emp_code = '0'.$emp_id;
	     				}

	           		?>

	           	<img src="{!!$base64!!}" class="photo">
	           	<div class="name_value">{!!$row->name_dr!!} {!!$row->last_name!!}</div>
                <div class="title_value">{!!$row->current_position_dr!!}</div>
                <div class="dep_value">
                    <?php
                        if(in_array($row->general_department, $general_dept))
                        {
                             echo $row->general_department_dr;
                        }
                        //Print General Department as Department in some department in in $department 
                        elseif(in_array($row->sudep_id, $department))
                        {
                            echo $row->general_department_dr;
                        }
                        else 
                        {
                            if(in_array($row->position_id, $position_ids))
                            {
                                if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58)
                                {
                                    echo $row->department_dr;
                                }
                                else
                                {
                                    echo $row->general_department_dr;
                                }
                            }
                            else
                            {
                                echo $row->department_dr;
                            }    
                        }
						
                    ?>
                </div>
                <div class='serial'>{!!$dep_code!!}{!!$emp_code!!}</div>               
                <div class="validaty_date">۲۹ حوت ۱۴۰۰</div>
                <div class="blood_value">{!!$row->blood_group!!}</div>
	           	</div>
                <div style="width: 8.5525cm;">
                    <?php 
                        $enc_empid = Crypt::encrypt($emp_id);
                        $enc_dep_id = Crypt::encrypt($dep_id);
                        $user_dep_type = getUserDepType(Auth::user()->id)->dep_type;
                        if ($user_dep_type==1) { 
                    ?>
                            <a href="{{'http://itsurvey.aop.gov.af/cardTemplatePrint/'.$enc_empid.'/'.$enc_dep_id}}" target="_blank" style="width: 8.5525cm;" class="btn btn-primary">Print</a>
                    <?php 
                        }
                        else {
                    ?>
                            <a href="{!!route('getCardTemplatePrint',$row->eid)!!}" style="width: 8.5525cm;" class="btn btn-primary">Print</a>
                    <?php 
                        }
                    ?>
					
				</div>
	           	<div class="col back new-card-back">
	           		<img src="{!!$base64!!}" class="photo">
                    <div class="name_value">{!!$row->name_en!!}</div>
                    <div class="title_value">{!!$row->current_position_en!!}</div>
                    <div class="dep_value">
                        <?php
                         if(in_array($row->general_department, $general_dept))
                         {
                            echo $row->general_department_en;
                         }
                         //Print General Department as Department in some department in in $department 
                        elseif(in_array($row->sudep_id, $department))
                        {
                            echo $row->general_department_en;
                        }
                         else 
                         {
                            if(in_array($row->position_id, $position_ids))
                            {
                                if($row->sudep_id==92 || $row->sudep_id==93 || $row->sudep_id==58)
                                {
                                echo $row->department_en;
                                }
                                else
                                {
                                    echo $row->general_department_en;
                                }
                            }
                            else
                            {
                                echo $row->department_en;
                            }
                        }
                        ?>
                    </div>  
	           		<div class='serial'>{!!$dep_code!!}{!!$emp_code!!}</div>
                    <div class="blood_value">{!!$row->blood_group!!}</div>
                    <div class="validaty_date"> 20 Mar 2022 </div>
               </div>
	        </div>
        </div>
	{!! HTML::script('/js/vendor/jquery/jquery.js') !!}
	{!! HTML::script('/js/vendor/bootstrap/bootstrap.js') !!}
	{!! HTML::style('css/bootstrap.min.css') !!}
	<script>
	document.addEventListener("keydown", keyDownTextField, false);
	function keyDownTextField(e) {
	var keyCode = e.keyCode;
	//list all CTRL + key combinations you want to disable
	var forbiddenKeys = new Array('p');
	var key;
	var isCtrl;
	if(window.event)
	{
	key = window.event.keyCode; //IE
	if(window.event.ctrlKey)
	isCtrl = true;
	else
	isCtrl = false;
	}
	else
	{
	key = e.which; //firefox
	if(e.ctrlKey)
	isCtrl = true;
	else
	isCtrl = false;
	}
	//if ctrl is pressed check if other key is in forbidenKeys array
	if(isCtrl)
	{
		for(i=0; i<forbiddenKeys.length; i++)
		{
			//case-insensitive comparation
			if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
			{
				alert('use print button!');

				//window.close();
				return false;
			}
		}
	}
	return true;
	}

	</script>
	</body>
</html>
