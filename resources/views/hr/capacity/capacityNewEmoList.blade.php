
<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='newlist'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>نام کامل</th>
                        <th>ولد</th>
                        <th>شماره تعینات</th>
                        <th>جنسیت</th>
                        <th>سکونت اصلی</th>
                        
                        <th>رتبه</th>
                        <th>بست</th>
                        <th>تاریخ تقرر</th>
                        <th>تحصیلات</th>
                        <th>عملیه</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="newemployee_trainings" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1"></div>
<div class="modal fade modal-fade-in-scale-up" id="newadd_training" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">

<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="@if(canAdd('hr_capacity')) {!!URL::route('addTrainingToEmployee')!!} @endif">
			    <input type="hidden" name="employee_id" id="employee_id" value="">	 
			    <div class="panel-heading">
			      <h5 class="panel-title">جزییات</h5>
			    </div>
	                
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">آموزش</label>
	                                <select name="training_id" id="general_department" class="form-control">
	                                        <option value="" selected="selected">انتخاب</option>
	                                    @if($trainings)
	                                    @foreach($trainings AS $dep_item)
	                                    	 <option value='{!!$dep_item->id!!}'>{!!$dep_item->title!!}</option>
	                                        
	                                    @endforeach
	                                    @endif
	                                </select>
	                                    <!-- <input class="form-control" type="text" name="general_department_dr" value="{!!Input::old('general_department_dr')!!}"> -->
	                			</div>
	                		</div>
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_capacity'))
	                					<button class="btn btn-primary" type="submit"><i class="fa fa-refresh fa-lg"></i> ذخیره</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>
				      	</div>
				    </div>
				    
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<div class="md-overlay"></div><!-- the overlay element -->

<script type="text/javascript">
    $(document).ready(function() {
        $('#newlist').dataTable(
            {
				"order": [[ 1, 'asc' ]],
                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getNewEmployeeCapacityData')!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });

function load_capacity(type)
{
    var page = "{!!URL::route('loadCapacityTrainings')!!}";
    
    $.ajax({
        url: page,
        type: 'post',
        data: '&type='+type,
        //dataType:'HTML',
        success: function(r)
      	{
        	$('#other_tab').html(r);
        }
    });   
}
function load_employee_trainings(id)
{
	var page = "{!!URL::route('load_employee_trainings')!!}";
	$.ajax({
        url:page,
        type:'post',
        data: '&id='+id,
        success: function(r){
			$('#employee_trainings').html(r);
        }
    });
}
function employee_oriented(id)
{
	if(confirm('Are you sure?'))
	{
		var page = "{!!URL::route('employee_oriented')!!}";
		$.ajax({
	        url:page,
	        type:'post',
	        data: '&id='+id,
	        success: function(r){
				location.reload();
	        }
	    });
	}
}

</script>

