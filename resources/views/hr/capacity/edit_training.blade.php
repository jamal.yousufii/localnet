<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="@if(canEdit('hr_capacity')) {!!URL::route('editTraining')!!} @endif">
			    <?php
					$s_date = $details->start_date;
					$e_date = $details->end_date;
					if(isShamsiDate()){
						
						$s_date = checkEmptyDate($s_date);
						$e_date = checkEmptyDate($e_date);
					}
					else{
						if($details->start_date != '')
						{
							$sdate = explode("-", $details->start_date);
							$sy = $sdate[0];
							$sm = $sdate[1];
							$sd = $sdate[2];
							$s_date = $sm."/".$sd."/".$sy;	
						}
						
						if($details->end_date != '')
						{
							$edate = explode("-", $details->end_date);
							$ey = $edate[0];
							$em = $edate[1];
							$ed = $edate[2];
							$e_date = $em."/".$ed."/".$ey;
						}
						
					}

				?>
			    <div class="panel-heading">
			      <h5 class="panel-title">جزییات برنامه آموزشی </h5>
			    </div>
			    	<div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">داخلی/خارجی</label>
	                                <select class="form-control" name="type" onchange="showExteranls(this.value)">
	                                    <option value="0" <?php if($details->type==0){echo "selected";}?>>داخلی</optioin>
	                                    <option value = "1" <?php if($details->type==1){echo "selected";}?>>خارجی</option>
	                                </select>
	                			</div>
	                		</div>	
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">عنوان</label>
	                                <input class="form-control" type="text" name="title" value="{!!$details->title!!}" required="required">
	                                
				      			</div>
				      		</div>
					      	<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">محل آموزش</label>
	                                <input class="form-control" type="text" name="location" value="{!!$details->location!!}">
				      			</div>
				      		</div>	
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">میعاد</label>
	                                <input class="form-control" type="text" name="days_no" value="{!!$details->days_no!!}">
				      			</div>
				      		</div>
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">تاریخ شروع</label>
				      				
	                                <input class="form-control {!!getDatePickerClass()!!}" type="text" name="start_date" value="{!!$s_date!!}">
	                                
				      			</div>
				      		</div>
					      	<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">تاریخ ختم</label>
				      				
	                                <input class="form-control {!!getDatePickerClass()!!}" type="text" name="end_date" value="{!!$e_date!!}">
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">برگزارکننده</label>
	                                <input class="form-control" type="text" name="organizer" value="{!!$details->organizer!!}">
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">تمویل کننده</label>
	                                <input class="form-control" type="text" name="sponser" value="{!!$details->sponser!!}">
				      			</div>
				      		</div>
	                		
	                	</div>
	                </div>
	                <div id="external_div_edit" @if($details->type==0) style="display:none" @endif>
		                <div class="container-fluid">
		                	<div class="row">
		                		<div class="col-sm-3">
		                			<div class="col-sm-12">
		                				<label class="col-sm-12 ">نوعیت آموزش</label>
		                                <select class="form-control" name="external_type">
		                                    <option value=''>انتخاب</option>
		                                    <option value="0" <?php if($details->external_type==0){echo "selected";}?>>اسکالرشیپ</optioin>
		                                    <option value = "1" <?php if($details->external_type==1){echo "selected";}?>>فلوشیپ</option>
		                                    
		                                </select>
		                			</div>
		                		</div>	
		                		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">تاریخ حکم</label>
					      				<?php $hdate = $details->hokm_date;
		                        		if($hdate !=''){$hdate = explode('-',$hdate);$hdate=dateToShamsi($hdate[0],$hdate[1],$hdate[2]);$hdate=jalali_format($hdate);}
		                        		?>
		                                <input class="form-control datepicker_farsi" type="text" name="hokm_date" value="{!!$hdate!!}">
					      			</div>
					      		</div>
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">شماره حکم</label>
		                                <input class="form-control" type="text" name="hokm_no" value="{!!$details->hokm_no!!}">
					      			</div>
					      		</div>
					      		@if($details->file_name!='')
					      		<div class="col-sm-3">
					      			<div class="col-sm-12">
					      				<label class="col-sm-12 ">فایل ضمیمه</label>
		                                {!!$details->file_name!!}
					      			</div>
					      		</div>
					      		@endif
					      		
		                	</div>
		                </div>
		            </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">تعداد کارمندان</label>
	                                <input class="form-control" type="text" name="seat_no" value="{!!$details->seat_no!!}">
				      			</div>
				      		</div>
				      		<input type="hidden" name="id" value="{!!$id!!}"/>
	                		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_capacity'))
	                					<button class="btn btn-primary" type="submit">{!!_('update')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>
	                	</div>
	                </div
	               
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
<script type="text/javascript">
$(".datepicker_farsi").persianDatepicker(); 
$(".datepicker").datepicker(); 
function showExteranls(value)
{
	if(value == 0)
    {//military
        $('#external_div_edit').slideUp();

    }
    else
    {
        $('#external_div_edit').slideDown();
    }
}
 </script>