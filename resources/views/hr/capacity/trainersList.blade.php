<header class="main-box-header clearfix">
    <h2>
		<a href="javascript:void()" class="btn btn-primary pull-right" data-target="#add_trainer" data-toggle="modal">
            <i class="fa fa-plus-circle fa-lg"></i>
        </a>
    </h2>
</header>
<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            
            <div class="main-box-body clearfix">
            <div class="table-responsive">
                <table class="table table-responsive" id='fired_list'>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>نام</th>
                        <th>تخلص</th>
                        <th>مرجع</th>
                        <th>تخصص</th>
                        <th>عملیه</th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-fade-in-scale-up" id="add_trainer" aria-hidden="true" aria-labelledby="change_employee" role="dialog" tabindex="-1">
<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="@if(canAdd('hr_capacity')) {!!URL::route('postNewTrainer')!!} @endif">
			    
			    <div class="panel-heading">
			      <h5 class="panel-title">جزییات</h5>
			    </div>
			    	<div class="container-fluid">
	                	<div class="row">
	                		
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">نام</label>
	                                <input class="form-control" type="text" name="name" value="">
	                                
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">تخلص</label>
	                                <input class="form-control" type="text" name="last_name" value="">
	                                
				      			</div>
				      		</div>
					      	<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">مرجع</label>
	                                <input class="form-control" type="text" name="org" value="">
				      			</div>
				      		</div>	
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">تخصص</label>
	                                <input class="form-control" type="text" name="especiality" value="">
				      			</div>
				      		</div>
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">شماره تماس</label>
	                                <input class="form-control" type="text" name="phone" value="">
	                                
				      			</div>
				      		</div>
				      		<div class="col-sm-3">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">ایمیل</label>
	                                <input class="form-control" type="text" name="email" value="">
	                                
				      			</div>
				      		</div>
					      	<div class="col-sm-6">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">تجربه تدریس</label>
	                                <input class="form-control" type="text" name="experience" value="">
				      			</div>
				      		</div>	
				      		
	                	</div>
	                </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-4">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_capacity'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>
	                	</div>
	                </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
</div>
<!-- Modal End -->
<div class="md-overlay"></div><!-- the overlay element -->

<script type="text/javascript">
 $(".datepicker_farsi").persianDatepicker(); 
    $(document).ready(function() {
        $('#fired_list').dataTable(
            {

                'sDom': 'lf<"clearfix">tip',
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "sAjaxSource": "{!!URL::to('/hr/getCapacityTrainersData')!!}",
                "language": {
                    "lengthMenu": "نمایش _MENU_ ریکارد در هر صفحه",
                    "zeroRecords": "ریکارد موجود نیست",
                    "info": "نمایش صفحه _PAGE_ از _PAGES_",
                    "infoEmpty": "ریکارد موجود نیست",
                    "search": "جستجو",
                    "infoFiltered": "(filtered از _MAX_ مجموع ریکارد)"
                }
            }
        );

    });
</script>

