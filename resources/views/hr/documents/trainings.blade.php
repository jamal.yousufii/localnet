
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postEmployeeTrainings',$row->id)!!}" enctype="multipart/form-data">
			    <div class="panel-heading">
			      <h5 class="panel-title">آموزش ها</h5>
			    </div>
			    @if($trainings)
			    <?php $j=0;$trainings_number=0; ?>
			    @foreach($trainings AS $train)
			    <?php $j++;?>
			    <div id="trainings_{!!$j!!}">
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-3">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">عنوان آموزش</lable>
				                  	<input type="text" class="form-control" name="training_title_{!!$j!!}" required="required" value="{!!$train->title!!}" />
				               	</div>
				            </div>
				            <div class="col-sm-3">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">ارگان / موسسه</label>
				                  	<input type="text" class="form-control" name="training_org_{!!$j!!}" value="{!!$train->organization!!}" />
				               	</div>
				            </div>
				            <div class="col-sm-3">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">از</label>
	                        		<?php $sdate = $train->date_from; if($sdate!=''){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
	                                <input class="form-control datepicker_farsi" readonly type="text" name="training_date_from_{!!$j!!}" value="<?php if($train->date_from!=""){echo jalali_format($sdate);}?>">
	                        	</div>
	                        </div>
	                        <div class="col-sm-2">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">تا</label>
	                        		<?php $edate = $train->date_to; if($edate!=''){$e_date = explode('-', $edate);$edate = dateToShamsi($e_date[0], $e_date[1], $e_date[2]);}?>
	                                <input class="form-control datepicker_farsi" readonly type="text" name="training_date_to_{!!$j!!}" value="<?php if($train->date_to!=""){echo jalali_format($edate);}?>">
	                        	</div>
	                        </div>
	                        <div class="col-md-1">
				            	<div class="col-sm-12">
				              		<label class="col-sm-1 ">&nbsp;</lable>
				              		@if($j==1)
				              			@if(canAdd('hr_documents'))
											<i class="icon wb-plus-circle" aria-hidden="true" style="font-size: 25px;cursor: pointer;" onclick="add_file('training')"></i>
										@endif
									@else
										@if(canDelete('hr_documents'))
											<i class="icon wb-minus-circle" aria-hidden="true" style="font-size: 25px;cursor: pointer;" onclick="remove_file({!!$j!!},'training')"></i>
										@endif
									@endif
								</div>
							</div>
				      	</div>
				    </div>
			    </div>
			    <?php $trainings_number++;?>
			    @endforeach
			    <input type="hidden" id="total_training" value="{!!$j!!}"/>
			    <input type="hidden" id="trainings_number" name="trainings_number" value="{!!$trainings_number!!}"/>
			    
			    @else
			    <div id="trainings_1">
			    <div class="container-fluid">
			      	<div class="row">
			      		<div class="col-sm-3">
			              	<div class="col-sm-12">
			              		<label class="col-sm-12 ">عنوان آموزش</lable>
			                  	<input type="text" class="form-control" name="training_title_1" required="required"  />
			               	</div>
			            </div>
			            <div class="col-sm-3">
			            	<div class="col-sm-12">
			              		<label class="col-sm-12 ">ارگان / موسسه</label>
			                  	<input type="text" class="form-control" name="training_org_1" />
			               	</div>
			            </div>
			            <div class="col-sm-3">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">از</label>
                        		
                                <input class="form-control datepicker_farsi" readonly type="text" name="training_date_from_1" value="">
                        	</div>
                        </div>
                        <div class="col-sm-2">
                        	<div class="col-sm-12">
                        		<label class="col-sm-12 ">تا</label>
                        		
                                <input class="form-control datepicker_farsi" readonly type="text" name="training_date_to_1" value="">
                        	</div>
                        </div>
                        <div class="col-md-1">
			            	<div class="col-sm-12">
			              		<label class="col-sm-1 ">&nbsp;</lable>
			              		@if(canAdd('hr_documents'))
									<i class="icon wb-plus-circle" aria-hidden="true" style="font-size: 25px;cursor: pointer;" onclick="add_file('training')"></i>
								@endif
							</div>
						</div>
			      	</div>
			    </div>
			    </div>
			    <input type="hidden" id="total_training" value="1"/>
			    <input type="hidden" id="trainings_number" name="trainings_number" value="1"/>
			    
			    @endif
			    <div id="other_training"></div>
			    
				</br>
                <div class="container-fluid">
	                <div class="row">
	                	<div class="col-sm-6">
	                		<div class="col-sm-12">
	                    		<label class="col-sm-2 ">&nbsp;</label>
	                    		@if(canAdd('hr_documents'))
		                        <button class="btn btn-primary" type="submit">ثبت معلومات</button>
		                    	@else
		                    	<p>You dont have permission</p>
		                    	@endif
		                        
		                    </div>
                    	</div>
                	</div>
                </div>
                {!! Form::token() !!}
                <div class="panel-heading">
			      <h5 class="panel-title">آموزش های مربوط اداره</h5>
			    </div>
                <?php $j=0;$trainings_number=0; ?>
                @foreach($internal_trainings AS $train)
			    <?php $j++;?>
			    <div id="trainings_{!!$j!!}">
				    <div class="container-fluid">
				      	<div class="row">
				      		<div class="col-sm-4">
				              	<div class="col-sm-12">
				              		<label class="col-sm-12 ">عنوان آموزش</lable>
				                  	<input type="text" class="form-control" readonly value="{!!$train->title!!}" />
				               	</div>
				            </div>
				            <div class="col-sm-4">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">محل آموزش</label>
				                  	<input type="text" class="form-control" readonly value="{!!$train->location!!}" />
				               	</div>
				            </div>
				            <div class="col-sm-4">
				            	<div class="col-sm-12">
				              		<label class="col-sm-12 ">برگزار کننده</label>
				                  	<input type="text" class="form-control" readonly value="{!!$train->organizer!!}" />
				               	</div>
				            </div>
				        </div>
				    </div>
				    <div class="container-fluid">
				      	<div class="row">
				            <div class="col-sm-4">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">از</label>
	                        		<?php $sdate = $train->start_date; if($sdate!=''){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
	                                <input class="form-control" readonly type="text" value="<?php if($train->start_date!=""){echo jalali_format($sdate);}?>">
	                        	</div>
	                        </div>
	                        <div class="col-sm-4">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">تا</label>
	                        		<?php $edate = $train->end_date; if($edate!=''){$e_date = explode('-', $edate);$edate = dateToShamsi($e_date[0], $e_date[1], $e_date[2]);}?>
	                                <input class="form-control" readonly type="text" value="<?php if($train->end_date!=""){echo jalali_format($edate);}?>">
	                        	</div>
	                        </div>
	                       
				      	</div>
				    </div>
			    </div>
			    <?php $trainings_number++;?>
			    @endforeach
            </form>
        </div>
    </div>

<script>
$(".datepicker_farsi").persianDatepicker();
function add_file(type)
{
	if(type == 'file')
	{
		var current_total = $('#total_files').val();
		var total = parseInt(current_total)+parseInt(1);
		$('#total_files').val(total);
		$('#experiences_number').val(total);
	}
	else
	{
		var current_total = $('#total_training').val();
		var total = parseInt(current_total)+parseInt(1);
		$('#total_training').val(total);
		$('#trainings_number').val(total);
	}
	
	$.ajax({
		url:'{{URL::route("getMoreExperience")}}',
		data: '&total='+total+'&type='+type,
		type:'POST',
		success:function(r){
			if(type == 'file')
			{
				$('#other_experience').append(r);
			}
			else
			{
				$('#other_training').append(r);
			}
		}
	});
}
function remove_file(no,type)
{
	if(type == 'file')
	{
		$('#files_'+no).remove();
		var current_total = $('#experiences_number').val();
		var total = parseInt(current_total)-parseInt(1);
		$('#experiences_number').val(total);
	}
	else
	{
		$('#trainings_'+no).remove();
		var current_total = $('#trainings_number').val();
		var total = parseInt(current_total)-parseInt(1);
		$('#trainings_number').val(total);
	}
}

</script>


