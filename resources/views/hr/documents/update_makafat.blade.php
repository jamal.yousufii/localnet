<div class="modal-dialog" style="width: 90%">
	<div class="modal-content">
		<div class="modal-header">			
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
		</div>
		   
		<div class="modal-body">
			<div class="row">
        	<div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="@if(canAdd('hr_documents')) {!!URL::route('makafatEmployeeUpdate')!!} @endif">
			    <input type="hidden" name="id" value="{!!Crypt::encrypt($details->id)!!}">	 
			    <input type="hidden" name="employee_id" value="{!!$details->employee_id!!}">	 
			    <div class="panel-heading">
			      <h5 class="panel-title">مکافات</h5>
			    </div>
	                <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-4">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">نوع</label>
	                                <select name = "type" class="form-control">
	                                    <option value=''>انتخاب</option>
	                                    <option value="1" <?php echo ($details->type=='1' ? 'selected':''); ?>>تقدیرنامه درجه اول</option>
	                                    <option value="2" <?php echo ($details->type=='2' ? 'selected':''); ?>>تقدیرنامه درجه دوم</option>
	                                    <option value="3" <?php echo ($details->type=='3' ? 'selected':''); ?>>تقدیرنامه درجه سوم</option>
	                                    <option value="4" <?php echo ($details->type=='4' ? 'selected':''); ?>>تقدیرنامه افتخاری</option>
	                                    <option value="5" <?php echo ($details->type=='5' ? 'selected':''); ?>>تحسین نامه</option>
	                                    <option value="6" <?php echo ($details->type=='6' ? 'selected':''); ?>>معاش بخششی</option>
	                                    <option value="7" <?php echo ($details->type=='7' ? 'selected':''); ?>>مدال</option>
	                                    <option value="8" <?php echo ($details->type=='8' ? 'selected':''); ?>>نشان</option>
	                                    <option value="8" <?php echo ($details->type=='8' ? 'selected':''); ?>>نشان</option>
	                                    <option value="9" <?php echo ($details->type=='9' ? 'selected':''); ?>>تقدیرنامه</option>
	                                    <option value="10" <?php echo ($details->type=='10' ? 'selected':''); ?>>ستاره</option>
	                                </select>
	                        	</div>
	                        </div>
				      		<div class="col-sm-4">
				      			<div class="col-sm-12">
				      				<label class="col-sm-12 ">نتیجه</label>
	                                <!-- <input class="form-control" type="text" name="emp_rank" value="{!!Input::old('emp_rank')!!}"> -->
	                                <textarea class="form-control" row="5" col="10" name="result">{!!$details->result!!}</textarea>
	                                
				      			</div>
				      		</div>
					      		
				      		<div class="col-sm-4">
	                        	<div class="col-sm-12">
	                        		<label class="col-sm-12 ">تاریخ</label>
	                        		<?php $date = $details->date;
	                        		if($date !=''){$date = explode('-',$date);$date=dateToShamsi($date[0],$date[1],$date[2]);$date=jalali_format($date);}
	                        		?>
	                                <input class="form-control datepicker_farsi" readonly type="text" name="date" value="{!!$date!!}">
	                        	</div>
	                        </div>
	                	</div>
	                </div>
	                
				    <div class="container-fluid">
	                	<div class="row">
	                		<div class="col-sm-3">
	                			<div class="col-sm-12">
	                				<label class="col-sm-12 ">&nbsp;</label>
	                				@if(canAdd('hr_documents'))
	                					<button class="btn btn-primary" type="submit">{!!_('save')!!}</button>
	                				@else
	                					<p>You dont have permission</p>
	                				@endif
	                			</div>
	                		</div>			            				           
				      	</div>
				    </div>
				{!! Form::token() !!}
				</form>
				</div>
			</div>		
		</div>		                       
	</div>
</div>
<script>
$(".datepicker_farsi").persianDatepicker(); 
</script>