
{!! HTML::style('css/bootstrap.min.css') !!}
  {!! HTML::style('css/bootstrap-extend.min.css') !!}

  {!! HTML::style('css/new-template.css') !!}
  {!! HTML::style('css/noori.css') !!}
<style>
@page{
	margin:10px;
}
@media print{
body{ background-color:#FFFFFF; background-image:none; color:#000000 }
#ad{ display:none;}
#leftbar{ display:none;}
#contentarea{ width:99%;}

}
td, th {
    text-align: center;
    vertical-align:top;
}
.page_header{
	width:100%;
	text-align: center;
	height: 11%;
}
.page_header img
{
	height: 100px;
}


.footer-left{
	float:left;
	margin-left: 55px;


}
.footer-right{
	float: right;
	direction:rtl;
	margin-right: 55px;


}
.td_title{
	font-weight: bold;
}
.vertical_text{
  width:10%;
  vertical-align:middle !important;
  -ms-transform:rotate(270deg); /* IE 9 */
  -moz-transform:rotate(270deg); /* Firefox */
  -webkit-transform:rotate(270deg); /* Safari and Chrome */
  -o-transform:rotate(270deg); /* Opera */

}
table tr td{
	font-size: 12px !important;
}
tr.noBorder td {
	border-bottom-style:hidden !important;
	border-left-style:hidden !important;
	border-right-style:hidden !important;
}
</style>
<div id="ad"></div>
<div id="leftbar"></div>
<div class="main-box-body clearfix" id="contentarea" style="background: #ddd;">

	<div style="border: 0px solid #ddd;width:100%;float:left;background:#fff;">
		<div class="page_header">
		@if($details->dep_type==2)
			<img src="/images/aop-header.png" alt="logo" width="65%"/>
		@else
			<img src="/images/ocs-header.jpg" alt="logo" width="65%"/>
		@endif
		</div>
		<div class="page_content" dir="rtl">
			<div class="page_department" style="text-align:center;font-size:15px;">
				ریاست منابع بشری<br>
				خلص سوانح کارمندان<br>
			</div>
			<div style="margin-right: 40px;">
			تاریخ:
			<?php $date = dateToShamsi(date('Y'),date('m'),date('d'));
			echo $date;
			 ?>
			</div>
			<table border="1px solid" width="100%">
			<tr>
				<td>
				<table border="1px solid" width="100%" height="200px">
					<tr>
						<td colspan="5" class="td_title">شهرت</td>
					</tr>
					<tr>
                    <?php
                    $base64 = '';
					if($details->photo!='')
                    {//crop(width,hight,X,Y)
                      $photo  = (file_exists(public_path().'/documents/profile_pictures/'.$details->photo) ? $details->photo : 'default.jpeg'); 
                      if($pic_size=='old' && $photo!='default.jpeg')
                      {  
                            if($id == 2024)
                            {
                                $img = Image::make(file_get_contents('documents/profile_pictures/'.$photo ))->crop(1578, 1597,1000);
                            }
                            elseif($id == 4111)
                            {
                                $img = Image::make(file_get_contents('documents/profile_pictures/'.$photo ))->crop(700,700);
                            }
                            elseif($id == 3267)
                            {
                                $img = Image::make(file_get_contents('documents/profile_pictures/'.$photo ))->crop(2000,2000);
                            }
                            elseif($id == 617)
                            {
                                $img = Image::make(file_get_contents('documents/profile_pictures/'.$photo ))->crop(578, 597);
                            }
                            elseif($id == 3863)
                            {
                                $img = Image::make(file_get_contents('documents/profile_pictures/'.$photo ))->crop(1358, 1407,0);
                            }
                            elseif($details->dep_type==2)
                            {
                                $img = Image::make(file_get_contents('documents/profile_pictures/'.$photo ))->crop(2000, 2000);
                            }
                            else
                            {
                                $img = Image::make(file_get_contents('documents/profile_pictures/'.$photo ));
                            }
                       }
                       else
                       {
                          $img = Image::make(file_get_contents('documents/profile_pictures/'.$photo )); 
                       }
						$img->encode('jpg');
						$type = 'jpg';
						$base64 = 'data:image/' . $type . ';base64,' . base64_encode($img);
					}


					?>
						<td rowspan="8" width="35%"><img src="{!!$base64!!}" alt="photo not available" width="95%"/></td>
						<td class="td_title" width="20%">کد دیتابیس</td>
						<td colspan="3">{!!$id!!}</td>
					</tr>
					<tr>
						<td class="td_title">نام</td>
						<td colspan="3">{!!$details->name_dr!!}</td>
					</tr>
					<tr>
						<td class="td_title">تخلص</td>
						<td colspan="3">{!!$details->last_name!!}</td>
					</tr>
					<tr>
						<td class="td_title">نام پدر</td>
						<td colspan="3">{!!$details->father_name_dr!!}</td>
					</tr>
					<tr>
						<td class="td_title">نام پدر کلان</td>
						<td colspan="3">{!!$details->grand_father_name!!}</td>
					</tr>
					<tr>
						<td class="td_title">تذکره</td>
						<td colspan="3">{!!$details->id_no!!}</td>
					</tr>
					<tr>
						<td class="td_title">ولایت</td>
						<td colspan="3">{!!$details->original_pro!!}</td>
					</tr>

					<tr>
						<td class="td_title">سال تولد</td>
						<td>
						@if($details->birth_year!='')
						{!!$details->birth_year!!}
						@else . @endif
						</td>
						<td class="td_title">عمر</td>
						<td>
						@if($details->birth_year!='')
						<?php 
						$sh_date = dateToShamsi(date('Y'),date('m'),date('d'));
						$year = explode('-', $sh_date);
						$year = $year[0];
						?>
						{!!($year-$details->birth_year)!!}
						@else . @endif
						</td>
					</tr>
					<tr>
						<td class="td_title">شماره تماس</td>
						<td colspan="4" dir="ltr">
							{!!$details->phone!!}
						</td>
					</tr>
					<tr>
						<td class="td_title">ایمیل</td>
						<td colspan="4">
							{!!$details->email!!}
						</td>
					</tr>

					<tr>
						<td class="td_title">وضعیت وظیفه</td>
						<td colspan="4">
							@if($details->changed==1){!!'تبدیلی'!!}
							@elseif($details->fired==1){!!'منفکی'!!}
							@elseif($details->resigned==1){!!'استعفا'!!}
							@elseif($details->retired==1){!!'تقاعد'!!}
							@elseif($details->position_dr==1){!!'اصلی'!!}
							@elseif($details->position_dr==2){!!'خدمتی'!!}
							@elseif($details->position_dr==3){!!'تحصیلی'!!}
							@elseif($details->position_dr==4){!!'انتظاربامعاش'!!}
							@elseif($details->position_dr==5){!!'عسکری'!!}
							@elseif($details->position_dr==6){!!'انتظار بدون معاش'!!}
							@elseif($details->position_dr==7){!!'اضافه بست'!!}
							@else {!!''!!}
							@endif
						</td>
					</tr>
				</table>
				<br>
				<?php
				$edu_count=1;
				if(isset($educations))
				{
					$edu_count = count($educations);
				}
				?>
				<table border="1px solid" width="100%" height="150px">
					<tr>
						<td rowspan="{!!$edu_count+1!!}" class="vertical_text">تحصیلات</td>
						<td class="td_title" height="25px">درجه تحصیل</td>
						<td class="td_title">موسسه تحصیلی</td>
						<td class="td_title">محل تحصیل</td>
						<td class="td_title">رشته</td>
						<td class="td_title">سال فراغت</td>

					</tr>
					@if($educations)
						@foreach($educations AS $edu)
							<tr>
								<td>{!!$edu->degree_name!!}</td>
								<td>{!!$edu->education_place!!}</td>
								<td>{!!$edu->location_name!!}</td>
								<td>{!!$edu->education_field!!}</td>
								<td>{!!$edu->graduation_year!!}</td>
							</tr>
						@endforeach
					@endif
				</table>
				<br>
				<?php $makafat_total = count($makafat); ?>
				<table border="1px solid" width="100%" height="250px">
					<tr>
						<td rowspan="{!!$makafat_total + 2!!}" class="vertical_text">مکافات</td>
						<td class="td_title" height="25px">تاریخ</td>
						<td class="td_title">تقدیرنامه/نشان/مدال</td>
					</tr>

					@if($makafat)
					@foreach($makafat AS $mak)
					<?php $hdate = $mak->date;
            		if($hdate !=''){$hdate = explode('-',$hdate);$hdate=dateToShamsi($hdate[0],$hdate[1],$hdate[2]);$hdate=jalali_format($hdate);}
            		?>
					<tr>
						<td>{!!$hdate!!}</td>
						<td>
							{{Config::get('myConfig.makafat_type.'.$lang.'.'.$mak->type)}}
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="4">ندارد</td>
					</tr>
					@endif
				</table>
				<?php $punish_total = count($punish); ?>
				<table border="1px solid" width="100%" height="150px">
					<tr>
						<td rowspan="{!!$punish_total + 2!!}" class="vertical_text">مجازات</td>

						<td class="td_title" height="25">تاریخ</td>
						<td class="td_title">دلیل</td>
					</tr>

					@if($punish)
					@foreach($punish AS $pun)
					<?php $pdate = $pun->date;
            		if($pdate !=''){$pdate = explode('-',$pdate);$pdate=dateToShamsi($pdate[0],$pdate[1],$pdate[2]);$pdate=jalali_format($pdate);}
            		?>
					<tr>
						<td>{!!$pdate!!}</td>
						<td>{!!$pun->reason!!}</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="3">نگردیده</td>
					</tr>
					@endif
				</table>
				<br>
				<table border="1px solid" width="100%">
					<tr>
						<td rowspan="3" class="vertical_text">عسکری</td>
						<td class="td_title">تاریخ شروع</td>
						<td class="td_title">تاریخ ختم</td>
						<td class="td_title">نوعیت</td>
					</tr>
					@if($details->soldier==1)
						<?php $soldier_date_from = $details->soldier_date_from;$soldier_date_to = $details->soldier_date_to;
	                    	  if($soldier_date_from !=''){$soldier_date_from = explode('-',$soldier_date_from);$soldier_date_from=dateToShamsi($soldier_date_from[0],$soldier_date_from[1],$soldier_date_from[2]);$soldier_date_from=jalali_format($soldier_date_from);}
	                    	  if($soldier_date_to !=''){$soldier_date_to = explode('-',$soldier_date_to);$soldier_date_to=dateToShamsi($soldier_date_to[0],$soldier_date_to[1],$soldier_date_to[2]);$soldier_date_to=jalali_format($soldier_date_to);}
	                    ?>
						<tr>
							<td>{!!$soldier_date_from!!}</td>
							<td>{!!$soldier_date_to!!}</td>
							<td>مکلفیت</td>
						</tr>
						@if($details->soldier_date_from_1!='')
							<?php $soldier_date_from_1 = $details->soldier_date_from_1;$soldier_date_to_1 = $details->soldier_date_to_1;
		                    	  if($soldier_date_from_1 !=''){$soldier_date_from_1 = explode('-',$soldier_date_from_1);$soldier_date_from_1=dateToShamsi($soldier_date_from_1[0],$soldier_date_from_1[1],$soldier_date_from_1[2]);$soldier_date_from_1=jalali_format($soldier_date_from_1);}
		                    	  if($soldier_date_to_1 !=''){$soldier_date_to_1 = explode('-',$soldier_date_to_1);$soldier_date_to_1=dateToShamsi($soldier_date_to_1[0],$soldier_date_to_1[1],$soldier_date_to_1[2]);$soldier_date_to_1=jalali_format($soldier_date_to_1);}
		                    ?>
						<tr>
							<td>{!!$soldier_date_from_1!!}</td>
							<td>{!!$soldier_date_to_1!!}</td>
							<td>احتیاط</td>
						</tr>
						@endif
					@else
						<tr>
							<td colspan="4">ننموده</td>
						</tr>
					@endif
				</table>
			</td>
			<td width="50%" valign="top">
				<table border="1px solid" width="100%" height="50px">
					<tr>
						<td colspan="6" class="td_title">معلومات وظیفه فعلی</td>
					</tr>
					<tr>
						<td class="td_title">وظیفه</td>
						<td class="td_title">تاریخ تقرر</td>
						<td class="td_title">محل وظیفه</td>

						<td class="td_title">بست</td>

						<td class="td_title">رتبه</td>
					</tr>
					    <?php $mawqif_employee = array(5,6,7,8,9) ?> 
							<tr>
                                @if(in_array($details->mawqif_employee,$mawqif_employee)) <!--Resigned then make the current job empty  --> 
                                   <tr>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td>{{$details->bast}}</td>
									   <td>{!!$details->emprank!!}</td>
                                   </tr> 
                                {{-- Entizar ba mash: get the last experience bas because tashkil id get 0 --}}
                                @elseif($details->mawqif_employee==2) 
                                   <tr>
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                       <td>{{$last_experiences->bast}}</td>
									   <td>{!!$details->emprank!!}</td>
                                   </tr> 
                                @elseif($details->changed==2)
								<?php $sdate = $changes->changed_date;
								if($changes->changed_date !=''){$sdate = explode('-',$changes->changed_date);$sdate=dateToShamsi($sdate[0],$sdate[1],$sdate[2]);$sdate=jalali_format($sdate);} ?>
									<td>{!!$changes->position_title!!}</td>
									<td>{!!$sdate!!}</td>
									@if($changes->change_type==0)
										<td>{!!$changes->department!!}</td>
									@else
										<td>{!!$changes->ministry!!}</td>
									@endif
									<td>{!!$changes->bast!!}</td>
									<td>{!!$changes->rank!!}</td>
                                @else
                                    @if(!empty(Config::get("myConfig.mawqif_employee.dr.$lang.$details->mawqif_employee")))
							          <td>{{Config::get("myConfig.mawqif_employee.$lang.$details->mawqif_employee")}}</td>
                                    @else 
									<td>{!!$details->current_position_dr!!}</td>

									<td>{!!$details->first_date_appointment!!}</td>
									@endif

									@if($details->tashkil_id>0)
										@if($details->tashkil_sub_dep_id == 82)
										<td>دفتر نماینده فوق العاده رئیس جمهور در امور اصلاحات و حکومتداری خوب</td>
										@else
										<td>{!!$details->dep_name!!}</td>
										@endif
									@else
										@if($details->tashkil_sub_dep_id_old == 82)
										<td>دفتر نماینده فوق العاده رئیس جمهور در امور اصلاحات و حکومتداری خوب</td>
										@else
										<td>{!!$details->dep_name!!}</td>
										@endif
									@endif

									<td>{!!$details->bast!!}</td>

									<td>{!!$details->emprank!!}</td>
								@endif
							</tr>

				</table>
				<br>
				<table border="1px solid" width="100%" height="350px">
					<tr>
						<td colspan="7" class="td_title" height="25px">سابقه کاری</td>

					</tr>
					<tr>
						<td class="td_title" height="25px">شماره</td>
						<td class="td_title" width="15%">از تاریخ</td>
						<td class="td_title" width="15%">تا تاریخ</td>
						<td class="td_title">وظیفه</td>
                        <td class="td_title">ارگان مربوطه</td>
                        @if($details->dep_type==1) 
                         <td class="td_title">بست</td>
                        @endif
						<td class="td_title">علت تبدیلی</td>
					</tr>
					@if($experiences)
					<?php $i=1;?>
					@foreach($experiences AS $exp)
					<?php $sdate = $exp->date_from;$edate = $exp->date_to;
            		if($sdate !=''){$sdate = explode('-',$sdate);$sdate=dateToShamsi($sdate[0],$sdate[1],$sdate[2]);$sdate=jalali_format($sdate);}
            		if($edate !=''){$edate = explode('-',$edate);$edate=dateToShamsi($edate[0],$edate[1],$edate[2]);$edate=jalali_format($edate);}
            		?>
						<tr>
							<td>{!!$i!!}</td>
							<td>{!!$sdate!!}</td>
							<td>{!!$edate!!}</td>
							<td>{!!$exp->position!!}</td>
                            <td>{!!$exp->organization!!}</td>
                             @if($details->dep_type==1) 
                             <td>{!!$exp->bast!!}</td>
                             @endif
							<td>{!!$exp->leave_reason!!}</td>

						</tr>
						<?php $i++; ?>
					@endforeach
					@else
						<tr>
							<td colspan="6">ندارد</td>

						</tr>
					@endif
				</table>
				<br>

				<table border="1px solid" width="100%" height="150px">
					<tr>
						<td colspan="4" class="td_title" height="25px">ترفیعات</td>

					</tr>
					<tr>
						<td class="td_title" height="25px" width="15%">تاریخ ترفیع</td>
						@if($details->employee_type!=2)
						<td class="td_title">رتبه</td>
						@else
						<td class="td_title">درجه</td>
						@endif
						<td class="td_title">قدم</td>
					</tr>
					@if($promotions)
					@foreach($promotions AS $pro)
					<?php $edate = $pro->promotion_date;
            		if($edate !=''){$edate = explode('-',$edate);$edate=dateToShamsi($edate[0],$edate[1],$edate[2]);$edate=jalali_format($edate);}
            		?>
						<tr>
							<td>{!!$edate!!}</td>
							<td>
							@if($pro->employee_type==2)
							{!!$pro->to_ajir!!}
							@elseif($pro->employee_type==3)
							{!!$pro->to_military!!}
							@else
							{!!$pro->to_rank!!}
							@endif
							</td>
							<td>{!!$pro->qadam!!}</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="4">ندارد</td>
						</tr>
					@endif
				</table>
				<br>
				<table border="1px solid" width="100%" height="150px">
					<tr>
						<td colspan="4" class="td_title" height="25px">آموزش ها</td>

					</tr>
					<tr>
						<td class="td_title">تاریخ شروع</td>
						<td class="td_title">تاریخ ختم</td>
						<td class="td_title" height="25px">آموزش</td>
						<td class="td_title">محل آموزش</td>
					</tr>

					@if($trainings)
					@foreach($trainings AS $tra)
						<tr>
							<td>{!!convertDate($tra->date_from,'to_shamsi','not_default')!!}</td>
							<td>{!!convertDate($tra->date_to,'to_shamsi','not_default')!!}</td>
							<td>{!!$tra->title!!}</td>
							<td>{!!$tra->organization!!}</td>
						</tr>
					@endforeach

					@endif
					@if($internal_trainings)
					@foreach($internal_trainings AS $tra)
					<?php $edate = $tra->end_date;
            		if($edate !=''){$edate = explode('-',$edate);$edate=dateToShamsi($edate[0],$edate[1],$edate[2]);$edate=jalali_format($edate);}
            			$sdate = $tra->start_date;
            		if($sdate !=''){$sdate = explode('-',$sdate);$sdate=dateToShamsi($sdate[0],$sdate[1],$sdate[2]);$sdate=jalali_format($sdate);}
            		?>

						<tr>
							<td>{!!$sdate!!}</td>
							<td>{!!$edate!!}</td>
							<td>{!!$tra->title!!}</td>
							<td>{!!$tra->organizer!!}</td>
						</tr>
					@endforeach

					@endif
				</table>
			</td>
			</tr>

			</table>
		</div>
		<br>

		<div>

			<div class="footer-left">
				رئیس منابع بشری  ـــــــــــــــــــــــــــــــ
			</div>
			<div class="footer-right">
				آمر ارزیابی اجرآت کارکنان و سوانح  ـــــــــــــــــــــــــــــــ
			</div>
		</div>
	</div>
</div>
