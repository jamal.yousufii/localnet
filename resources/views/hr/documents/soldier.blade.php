
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" method="post" action="{!!URL::route('postEmployeeSoldier',$row->id)!!}" enctype="multipart/form-data">
			    
					
				    <div class="panel-heading">
				      <h5 class="panel-title">عسکری</h5>
				    </div>
				    
				    <div class="container-fluid">
				    	<div class="row">
				    		<div class="col-sm-6">
				    			<div class="col-sm-12 checkbox-custom checkbox-primary checkbox-inline">
				       	
			                        <input onclick="showSoldier()" value='1' name="soldier" id="soldier" type="checkbox" @if($row->soldier == 1) checked @endif>
			                        <label for="subordinates">
			                           عسکری
			                        </label> 
			                    </div>
			                </div>
			            </div>
					</div>
				    <div class="container-fluid" id="soldier_det" @if($row->soldier!=1) style="display:none" @endif>
				      	<div class="row">
				      		<div class="col-sm-2">
				    			<div class="col-sm-12 checkbox-custom checkbox-primary checkbox-inline">
				       				
			                        <input onclick="showSoldier_types()" value='1' name="mokalafiat" id="mokalafiat" type="checkbox" @if($row->soldier_type_1 == 1) checked @endif>
			                        <label for="subordinates">
			                           مکلفیت
			                        </label> 
			                        
			                    </div>
			                </div>
			                <div id="moklfiat_div" @if($row->soldier_type_1 != 1) style="display:none" @endif>
					            <div class="col-sm-4">
		                        	<div class="col-sm-12">
		                        		<label class="col-sm-12 ">از</label>
		                        		<?php $sdate = $row->soldier_date_from; if($sdate!=''){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
		                                <input class="form-control datepicker_farsi" type="text" name="soldier_date_from" value="<?php if($row->soldier_date_from!=""){echo jalali_format($sdate);}?>">
		                        	</div>
		                        </div>
		                        <div class="col-sm-4">
		                        	<div class="col-sm-12">
		                        		<label class="col-sm-12 ">تا</label>
		                        		<?php $edate = $row->soldier_date_to; if($edate!=''){$e_date = explode('-', $edate);$edate = dateToShamsi($e_date[0], $e_date[1], $e_date[2]);}?>
		                                <input class="form-control datepicker_farsi" type="text" name="soldier_date_to" value="<?php if($row->soldier_date_to!=""){echo jalali_format($edate);}?>">
		                        	</div>
		                        </div>
	                        </div>
				      	</div>
				      	<div class="row">
				      		<div class="col-sm-2">
				    			<div class="col-sm-12 checkbox-custom checkbox-primary checkbox-inline">
				       				
			                        <input onclick="showSoldier_types()" value='1' name="ehtiat" id="ehtiat" type="checkbox" @if($row->soldier_type_2 == 1) checked @endif>
			                        <label for="subordinates">
			                           احتیاط
			                        </label>
			                    </div>
			                </div>
			                <div id="ehtiat_div" @if($row->soldier_type_2 != 1) style="display:none" @endif>
					            <div class="col-sm-4">
		                        	<div class="col-sm-12">
		                        		<label class="col-sm-12 ">از</label>
		                        		<?php $sdate = $row->soldier_date_from_1; if($sdate!=''){$s_date = explode('-', $sdate);$sdate = dateToShamsi($s_date[0], $s_date[1], $s_date[2]);}?>
		                                <input class="form-control datepicker_farsi" type="text" name="soldier_date_from_1" value="<?php if($row->soldier_date_from_1!=""){echo jalali_format($sdate);}?>">
		                        	</div>
		                        </div>
		                        <div class="col-sm-4">
		                        	<div class="col-sm-12">
		                        		<label class="col-sm-12 ">تا</label>
		                        		<?php $edate = $row->soldier_date_to_1; if($edate!=''){$e_date = explode('-', $edate);$edate = dateToShamsi($e_date[0], $e_date[1], $e_date[2]);}?>
		                                <input class="form-control datepicker_farsi" type="text" name="soldier_date_to_1" value="<?php if($row->soldier_date_to_1!=""){echo jalali_format($edate);}?>">
		                        	</div>
		                        </div>
	                        </div>
	                        
				      	</div>
				    </div>
					</br>
	                <div class="container-fluid">
		                <div class="row">
		                	<div class="col-sm-6">
		                		<div class="col-sm-12">
		                    		<label class="col-sm-2 ">&nbsp;</label>
		                    		@if(canAdd('hr_documents'))
			                        <button class="btn btn-primary" type="submit">ثبت معلومات</button>
			                    	@else
			                    	<p>You dont have permission</p>
			                    	@endif
			                        
			                    </div>
	                    	</div>
	                	</div>
	                </div>
                {!! Form::token() !!}
            </form>
        </div>
    </div>

<script>
$(".datepicker_farsi").persianDatepicker();
function showSoldier()
{
    if($('#soldier').is(':checked'))
    {
    	$('#soldier_det').slideDown();
        
    }
    else
    {
   		$('#soldier_det').slideUp();
    }
}
function showSoldier_types()
{
    if($('#mokalafiat').is(':checked'))
    {
    	$('#moklfiat_div').slideDown();
        
    }
    else
    {
   		$('#moklfiat_div').slideUp();
    }
    if($('#ehtiat').is(':checked'))
    {
    	$('#ehtiat_div').slideDown();
        
    }
    else
    {
   		$('#ehtiat_div').slideUp();
    }
}
</script>


