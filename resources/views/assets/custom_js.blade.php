<!-- Custom Scripts -->
<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

function chooseFile(id) {
    var id = $("#" + id);
    var fileName = id[0].files[0].name;
    id.parent().find('label').html(fileName);
}

function readImg(input)
{
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#user-img').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
      $("#apply_changes").css("display", "inline");
  }
}

function callToPressBtn(input)
{
  $("#"+input).click();
}

function applyChanges()
{
  var formData = new FormData($("#docForm")[0]);
  $.ajax({
      url   : '{{ route("uploadPic") }}',
      data  : formData,
      processData : false,
      contentType : false,
      type  : 'post',
      success: function(response)
      {
        $('#msg-profile').html(response);
        $("#apply_changes").css("display", "none");
      }
  });
}

function ShowHide()
{
  if($('#password').attr("type") == "text"){
    $('#password').attr('type', 'password');
    $('#icon-pass').addClass( "la-eye" );
    $('#icon-pass').removeClass( "la-eye-slash" );
  }else if($('#password').attr("type") == "password"){
    $('#password').attr('type', 'text');
    $('#icon-pass').removeClass( "la-eye" );
    $('#icon-pass').addClass( "la-eye-slash" );
  }
}

function ShowHideConf()
{
  if($('#conf_password').attr("type") == "text"){
    $('#conf_password').attr('type', 'password');
    $('#icon-passConf').addClass( "la-eye" );
    $('#icon-passConf').removeClass( "la-eye-slash" );
  }else if($('#conf_password').attr("type") == "password"){
    $('#conf_password').attr('type', 'text');
    $('#icon-passConf').removeClass( "la-eye" );
    $('#icon-passConf').addClass( "la-eye-slash" );
  }
}

function ConfirmPassword()
{
  var pass = $('#password').val();
  var conf = $('#conf_password').val();
  if(conf == pass){
    $("#msg").css("color", "green");
    $('#msg').html("{{ trans('global.pass_match') }}");
  }else{
    $("#msg").css("color", "red");
    $('#msg').html("{{ trans('global.pass_not_match') }}");
  }
}

function editPassword()
{
  var pass = $('#password').val();
  var conf = $('#conf_password').val();
  if(conf != pass){
    $('#msg').html("{{ trans('global.pass_not_match') }}");
    $("#conf_password").css("border", "1px solid red");
    $("#msg").css("color", "red");
    return;
  }
  var formData = new FormData($("#changePassword")[0]);
  $.ajax({
      url   : '{{ route("editPassword") }}',
      data  : formData,
      processData: false,
      contentType : false,
      type  : 'post',
      success: function(response)
      {
        $('#btn-password').click();
        swal({
            text: response,
            icon: "success",
            type: "",
            confirmButtonText:  "<span>{{trans('global.back')}}</span>",
            confirmButtonClass: "btn btn-success m-btn btn-sm m-btn--icon",
            showCancelButton:   0,
            cancelButtonText:   "<span><span>{{trans('global.no')}}</span></span>",
            cancelButtonClass:  "btn btn-secondary btn-sm m-btn--icon"
        }).then(function (e) {
            e.value && redirectFunction();
        });
      }
  });
}

// Add Records
function addRecord(url,params,method,response_div)
{
  serverRequest(url,params,method,response_div);
}

// Edit Records
function editRecord(url,params,method,response_div,is_modal)
{
  serverRequest(url,params,method,response_div,is_modal);
}

// Call data from server side
function serverRequest(url,params,method,response_div,is_modal=false)
{
  $.ajax({
    url: url,
    data:params,
    type:method,
    beforeSend: function()
    {
      if(is_modal){
         $('#'+response_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('img/loader.gif')!!}" /></div>');
      }else {
        $(".m-page-loader.m-page-loader--base").css("display","block");
      }
    },
    success: function(response)
    {
      $(".m-page-loader.m-page-loader--base").css("display","none");
      $('#'+response_div).html(response);
      $(".datePicker").css('width','100%');
      @if($lang=="en")
        $(".datePicker").attr('type', 'date');
      @else
        $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
      @endif
      $('.select-2').select2();
      tinymce.remove();
      tinymce.init({
        selector:'textarea.tinymce',
        directionality : 'rtl',
        setup: function (editor) {
          editor.on('change', function () {
              tinymce.triggerSave();
          });
        }
      });
    },
    error: function (request, status, error) {
      $(".m-page-loader.m-page-loader--base").css("display","none");
      json = $.parseJSON(request.responseText);
      $.each(json.errors, function(key, value){

        $('.'+key).show();
        $('.'+key).html('<span class="text-danger">'+value+'</span>');
        $('#'+key).css('border-bottom','1px solid #dc3545');
      });
    },
    cache: false,
    processData: false
  })
}

// Store Records
function storeRecord(url,form_id,method,response_div)
{
  if(validation(form_id))
  {
    var params = new FormData($('#'+form_id)[0]);
    serverRequestResponse(url,params,method,response_div,redirectFunction);
  }
}

// Pass result to the function form server side
function serverRequestResponse(url,params,method,response_div,redirectFunction,is_modal=false)
{
  $('.error-div').html('');
  $('.errorDiv').css('border-bottom','');
  $.ajax({
    url: url,
    data:params,
    type:method,
    beforeSend: function()
    {
      if(is_modal){
         $('#'+response_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('img/loader.gif')!!}" /></div>');
      }else {
        $(".m-page-loader.m-page-loader--base").css("display","block");
      }
    },
    success: function(response)
    {
      $(".m-page-loader.m-page-loader--base").css("display","none");
      redirectFunction(response,response_div);
    },
    error: function (request, status, error) {
      $(".m-page-loader.m-page-loader--base").css("display","none");
      json = $.parseJSON(request.responseText);
      $.each(json.errors, function(key, value){
        $('.'+key).show();
        $('.'+key).html('<span class="text-danger">'+value+'</span>');
        $('#'+key).css('border-bottom','1px solid #dc3545');
      });
    },
    cache: false,
    processData: false,
    contentType: false,
  })
}

// Update Records
function doEditRecord(url,form_id,method,response_div)
{
  if(validation(form_id))
  {
    var params = $('#'+form_id).serialize();
    serverRequestResponseEdit(url,params,method,response_div,redirectFunction);
  }
}

// View Records
function viewRecord(url,params,method,response_div)
{
  serverRequest(url,params,method,response_div);
}

// Redirect to index function
function redirectFunction(response)
{
   location.reload();
}

// Paginate Records
$(document).ready(function()
{
  $('.pagination a').on('click', function(event) {
		event.preventDefault();
		if ($(this).attr('href') != '#') {
      // Get current URL route
      document.cookie = "no="+$(this).text();
      var dataString = '';
			dataString += "&page="+$(this).attr('id')+"&ajax="+1;
			$.ajax({
              url:  '{{ url()->current() }}',
              data: dataString,
              type: 'get',
              beforeSend: function(){
                $('#searchresult').html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('img/loader.gif')!!}" /></div>');
              },
              success: function(response)
              {
                $('#searchresult').html(response);
              }
          }
      );
		}
	});
});



more = 1;
function add_more(div,url,total)
{
  if(more==1 && total>0){
    more = parseInt(total) + 1;
  }else{
    $('#'+div+'_btn_'+more).hide();
    more = parseInt(more) + 1;
  }

  $.ajax({
    url: url,
    data: {
      "_method" : 'POST',
      "number"  : more,
    },
    type:'POST',
    success:function(response){
      $('#'+div).prepend(response);
      $(".datePicker").css('width','100%');
      @if ($lang=="en")
        $(".datePicker").attr('type', 'date');
      @else
        $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
      @endif
    }
  });
}

function confirmRemoveMore(div,number,btn)
{
  swal({
      text: "{{trans('global.conf_msg')}}",
      icon: "success",
      type: "question",
      confirmButtonText: "<span>{{trans('global.yes')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn btn-sm m-btn--icon",
      showCancelButton: !0,
      cancelButtonText: "<span><span>{{trans('global.no')}}</span></span>",
      cancelButtonClass: "btn btn-secondary btn-sm m-btn--icon"
  }).then(function (e) {
      e.value && remove_more(div,number,btn);
  });
}

function remove_more(div,number,btn)
{
  more = number-1;
  $('#'+btn+'_'+more).show();
  $('#'+div).remove();
}

function destroyFile(url,method,params,response_div)
{
  swal({
      text: "{{trans('global.conf_msg')}}",
      icon: "success",
      type: "question",
      confirmButtonText: "<span>{{trans('global.yes')}}</span>",
      confirmButtonClass: "btn btn-danger m-btn btn-sm m-btn--icon",
      showCancelButton: !0,
      cancelButtonText: "<span><span>{{trans('global.no')}}</span></span>",
      cancelButtonClass: "btn btn-secondary btn-sm m-btn--icon"
  }).then(function (e) {
      e.value && deleteFile(url,method,params,response_div);
  });
}

// Update Records
function doEditRecord(url,form_id,method,response_div)
{
  if(validation(form_id))
  {
    var params = $('#'+form_id).serialize();
    serverRequestResponseEdit(url,params,method,response_div,redirectFunction);
  }
}

// Pass result to the update function form server side
function serverRequestResponseEdit(url,params,method,response_div='content',redirectFunction)
{
  $('.error-div').html('');
  $('.errorDiv').css('border-bottom','');
  $.ajax({
    url: url,
    data:params,
    type:method,
    beforeSend: function()
    {
      $(".m-page-loader.m-page-loader--base").css("display","block");
    },
    success: function(response)
    {
      $(".m-page-loader.m-page-loader--base").css("display","none");
      redirectFunction(response);
    },
    error: function (request, status, error) {
      $(".m-page-loader.m-page-loader--base").css("display","none");
      json = $.parseJSON(request.responseText);
      $.each(json.errors, function(key, value){
        $('.'+key).show();
        $('.'+key).html('<span class="text-danger">'+value+'</span>');
        $('#'+key).css('border-bottom','1px solid #dc3545');
      });
    },
    cache: false,
    processData: false,
  })
}

function validation(form_id)
{
  var form = document.getElementById(form_id);
  for (i=0;i<form.length;i++)
  {
    var tempobj = form.elements[i];
    if(tempobj.id)
    {
      if(($('#'+tempobj.id).hasClass('required') && $.trim(tempobj.value) == ''))
      {
        tempobj.focus();
        if($('#'+tempobj.id).attr("type") == "number" || $('#'+tempobj.id).attr("type") == "text" || $('#'+tempobj.id).attr("type") == "date"){
          $('#'+tempobj.id).css('border-bottom','1px solid #FF0000');
        }else{
          $('#div_'+tempobj.id).css('border-bottom','1px solid #FF0000');
        }
        $('.'+tempobj.id).css('display','inline');
        $('.'+tempobj.id).html('<span class="text-danger">{{trans("validation.required")}}</span>');
        return false;
      }
      else
      {
        $('#'+tempobj.id).css('border','');
        $('.'+tempobj.id).html('');
      }
    }
  }
  return true;
}

function validate(url,form_id,method)
{
  var form = document.getElementById(form_id);
  for (i=0;i<form.length;i++)
  {
    var tempobj = form.elements[i];
    if(tempobj.id)
    {
      if (tempobj.id.substring(0,4) == 'req_' && $.trim(tempobj.value) == '' )
      {
        tempobj.focus();
        if($('#'+tempobj.id).attr("type") == "number" || $('#'+tempobj.id).attr("type") == "text"){
          $('.'+tempobj.id).css('border-top','1px solid #FF0000');
          $('.'+tempobj.id).html('<span class="text-danger">{{trans("validation.required")}}</span>');
        }else {
          $('.'+tempobj.id).css('border-top','1px solid #FF0000');
          $('.'+tempobj.id).html('<span class="text-danger">{{trans("validation.required")}}</span>');
        }
        return false;
      }
      else
      {
        $('.'+tempobj.id).css('border','');
        $('.'+tempobj.id).html('');
      }
    }
  }
  storeRecord(url,form_id,method);
}


function bringModules(id,url)
{
  $.ajax({
      url: url,
      data: {
          "_method" : 'POST',
          "id"      : id,
      },
      type: 'post',
      success: function(response)
      {
        $("#app_div").removeAttr("style");
        $("#applications").html(response);
      }
  });
}

function bringSections(url)
{
  var modules = $("#applications").val();
  $.ajax({
      url: url,
      data: {
        "_method" : 'POST',
        "modules" : modules,
      },
      type: 'post',
      success: function(response)
      {
        $("#section_div").removeAttr("style");
        $("#sectins").html(response);
      }
  });
}

function bringRoles(url)
{
  var sections = $("#sectins").val();
  $.ajax({
      url: url,
      data: {
          "_method"  : 'POST',
          "sections" : sections,
      },
      type: 'post',
      success: function(response)
      {
        $("#role_div").removeAttr("style");
        $("#roles").html(response);
      }
  });
}


// Store Records
function storeData(url,form_id,method,response_div,responseFunction,is_modal)
{
  if(validation(form_id))
  {
    var params = new FormData($('#'+form_id)[0]);
    serverRequestResponse(url,params,method,response_div,responseFunction,is_modal);
  }
}


$(".select-2").select2({ width: '100%' });

// Filter Records
function filterRecords(url,method,response_div)
{
  item = $('#keyWord').val();
  $.ajax({
      url : url,
      data: {
          "item" : item
      },
      type: method,
      beforeSend: function(){
        $("#"+response_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('img/loader.gif')!!}" /></div>');
      },
      success: function(response)
      {
        $("#"+response_div).html(response);
      }
  });
}

</script>
@stack('custom-js')
