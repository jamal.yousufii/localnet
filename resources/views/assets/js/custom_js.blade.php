
<!-- Custom Scripts -->
<script type="text/javascript">
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

// Add Records
function addRecord(url,params,method,response_div)
{
  serverRequest(url,params,method,response_div);
}

// Call data from server side
function serverRequest(url,params,method,response_div,is_modal=false)
{
  $.ajax({
    url: url,
    data:params,
    type:method,
    beforeSend: function()
    {
      if(is_modal){
         $('#'+response_div).html('<div class="col text-center" style="width:100%"><img alt="" src="{!!asset('img/ajax-loader.gif')!!}" /></div>');
      }else {
        $(".m-page-loader.m-page-loader--base").css("display","block");
      }
    },
    success: function(response)
    {
      $(".m-page-loader.m-page-loader--base").css("display","none");
      $('#'+response_div).html(response);
      $(".datePicker").css('width','100%');
      @if(session('lang')=="en")
        $(".datePicker").attr('type', 'date');
      @else
        $(".datePicker").persianDatepicker({cellWidth: 38, cellHeight: 28, fontSize: 11});
      @endif
      $('.select-2').select2();
      tinymce.remove();
      tinymce.init({
        selector:'textarea.tinymce',
        directionality : 'rtl',
        setup: function (editor) {
          editor.on('change', function () {
              tinymce.triggerSave();
          });
        }
      });
    },
    error: function (request, status, error) {
      $(".m-page-loader.m-page-loader--base").css("display","none");
      json = $.parseJSON(request.responseText);
      $.each(json.errors, function(key, value){

        $('.'+key).show();
        $('.'+key).html('<span class="text-danger">'+value+'</span>');
        $('#'+key).css('border-bottom','1px solid #dc3545');
      });
    },
    cache: false,
    processData: false
  })
}


// Pass result to the function form server side
function serverRequestResponse(url,params,method,response_div,redirectFunction,is_modal=false,btnID='submitBtn')
{
  $('.error-div').html('');
  $('.errorDiv').css('border-bottom','');
  $.ajax({
    url: url,
    data:params,
    type:method,
    beforeSend: function()
    {
      // disable submit button
      $('#'+btnID).attr('disabled','disabled');
      if(is_modal){
        //  $('#'+response_div).html('<div class="col text-center loader_div" style="width:100%"><img alt="" src="{!!asset('img/ajax-loader.gif')!!}" /></div>');
      }else {
        $(".m-page-loader.m-page-loader--base").css("display","block");
      }
    },
    success: function(response)
    {
      // enable submit button
      $('#'+btnID).removeAttr('disabled');
      $(".m-page-loader.m-page-loader--base").css("display","none");
      redirectFunction(response,response_div);
    },
    error: function (request, status, error) {
      // enable submit button
      $('#'+btnID).removeAttr('disabled');
      $(".m-page-loader.m-page-loader--base").css("display","none");
      json = $.parseJSON(request.responseText);
      $.each(json.errors, function(key, value){
        $('.'+key).show();
        $('.'+key).html('<span class="text-danger">'+value+'</span>');
        $('#'+key).css('border-bottom','1px solid #dc3545');
      });
    },
    cache: false,
    processData: false,
  })
}

/** 
 * @Author: Jamal Yousufi 
 * @Date: 2020-11-30 10:56:07 
 * @Desc:  Remove Element based on parent  
 */
  function removeElement(elem_id) 
  {
    if (confirm("آیا شما موافق هستید؟"))
    {
     $('#'+elem_id).slideUp(1000); 
    }
  }


counter = 1;
function add_moreAttachments(route, total)
{
    if (counter <= 4)
    {
        if (counter == 1 && total > 0) {
            counter = parseInt(total) + 1;
        } else {
            $('#btn_'+counter).hide();
            counter = parseInt(counter) + 1;
        }
        $.ajax({
            url: route,
            data: {
                "_method": 'POST',
                "number" : counter,
                "_token" : "{{ csrf_token() }}",
            },
            type: 'POST',
            success: function (response) {
                $('#more_education').append(response);
            }
        });
    }
}

function remove_moreAttachments(div, number)
{
    if (confirm("آیا شما موافق هستید؟"))
    {
        counter = number - 1;
        $('#btn_' + counter).show();
        $('#'+ div).slideUp(1000);
    }
}

</script>
