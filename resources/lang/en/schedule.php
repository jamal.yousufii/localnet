<?php
return [
    'schedules'         => "Bill of Quantity Schedule",
    'schedule_add'      => "Add Schedule",
    'start_date'        => "Start Date",
    'end_date'          => "End Date",
    'actual_start_date' => "Exact Start Date",
    'actual_end_date'   => "Exact End Date",
    'owner_date'        => "Determined Date",
    'contractor_date'   => "Completed Work Date",

  ];
?>
