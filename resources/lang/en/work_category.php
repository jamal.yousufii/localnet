<?php
return [
  'list' 	                    => "Work Category List",
  'category_name'   		      => "Work Category Name",
  'category_code' 		        => "Work Category Code",
  'add_new_category' 	        => "Add Work Category",
  'search' 		                => "Search based on Work Category Name",
  'view_category'     		    => "View Work Category",
  'edit_category'     		    => "Edit Work Category",
  'edit'     		              => "Edit",
  'view'     	    	          => "View",
  'delete'             		    => "Delete",
];
?>
