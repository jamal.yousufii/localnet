<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Change Static Vavlue of Database to words
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/
  "employee_type_1" => "مامور",
  "employee_type_2" => "اجیر",
  "employee_type_3" => "نظامی",
  "employee_type_4" => "مامور بالمقطع",
  "employee_type_5" => "اجیر بالمقطع",
  "employee_type_6" => "خدمتی",
];
