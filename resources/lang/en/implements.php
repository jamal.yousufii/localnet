<?php
return [
  'implements' 		    => "Checklists",
  'list' 	            => "List of Checklists",
  'edit' 		          => "Edit Checklist",
  'add' 		          => "Add Checklist",
  'view' 		          => "View Checklist",
  'attachment_type'   => "File Type",
  'attachment_name' 	=> "Name",
  'description' 	    => "Description",
];
?>
