<?php
return [
  'login' 		      => "Login to System",
  'email' 		      => "Email",
  'password' 		    => "Password",
  'forgot_password' => "Forgot Password",
  'enter_email' 		=> "Please enter your Email to reset your Password",
  'new_password' 		=> "Reset",
  'cancel' 		      => "Cancel",
  'pmis' 		        => "Project management information system (PMIS)",
];
?>
