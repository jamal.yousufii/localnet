<?php
return [
 
    'adjustments'                   => "تعدیلات",
 
    'req_adjustments'               => "درخواست تعدیلات",
    'add_req_adjustment'            => "ثبت درخواست تعدیلات",
    'edit_req_adjustment'           => "تجدید درخواست تعدیلات",
    'list_req_adjustment'           => "لیست درخواست تعدیلات",
    'view_req_adjustment'           => "نمایش درخواست تعدیلات",
    'request_number'                => "شماره درخواست",
    'request_date'                  => "تاریخ درخواست",
    'subject'                       => "موضوع",
    'description'                   => "متن درخواست",
    'list_projects'                 => 'لیست پروژه ها',
    'project_location'              => 'موقعیت پروژه',
    'section'                       => "بخش تعدیل",
    'documents'                     => "اسناد تخنیکی",
    'billQuantity'                  => "احجام کاری",
    'time'                          => "زمان",
    'project'                       => "پروژه",
    'location'                      => "موقعیت پروژه",
    
    'update_status'                 => "تجدید حالت",
    'view_status' 	                => "نمایش حالت",
    'approval_number' 		        => "شماره حکم",
    'approval_date' 		        => "تاریخ حکم",
    'approval_description'          => "متن حکم",
    'status' 		                => "حالت",
    'approved' 		                => "تاييد شده",
    'rejected' 		                => "رد شده",
    'pending' 		                => "منتظره",
    'req_approve' 	                => "تایید",
    'req_reject'                    => "رد",
    'approved_msg' 	                => "درخواست تعدیل توسط شما تایید گردید.",
    'rejected_msg'                  => "درخواست تعدیل توسط شما رد گردید.",
    'document_urn'                  => "شماره مسلسل سند",

]
?>