<?php
return [
    'contractor_project' 	=> "Projects",
    'projects' 		        => "List of Projects",
    'project_name' 		    => "Project Name",  
    'contract_start_date'   => "Contract Start Date",
    'contract_end_date' 	=> "Contract End Date",
    'contract_code'         => "Contract Code",
    'search' 		        => "Search by Project Name",
];
?>