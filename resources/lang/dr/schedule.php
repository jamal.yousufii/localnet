<?php
return [
    'schedules'         => "تقسیم اوقات احجام کاری",
    'schedule_add'      => "علاوه نمودن زمان کاری",
    'start_date'        => "تاریخ آغاز",
    'end_date'          => "تاریخ ختم",
    'actual_start_date' => "تاریخ دقیق آغاز کار",
    'actual_end_date'   => "تاریخ دقیق ختم کار",
    'owner_date'        => "تاریخ تعین شده",
    'contractor_date'   => "تاریخ تکمیل کار",

  ];
?>
