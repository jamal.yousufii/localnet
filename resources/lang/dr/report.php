<?php
return [
  'select' 	          => "لطفآ یک بخش را اتنخاب نمایید",
  'hide' 	            => "پنهان کردن",
  'show' 	            => "نمایش دادن",
  'req_dep' 	        => "شعبه درخواست کننده",
  'from' 	            => "از",
  'to' 	              => "الی",
  'search' 	          => "جستجو",
  'project_progress_report'    => 'راپور پیشرفت پروژه ها',
  'project_progress_form'      => 'فورم جستجو پیشرفت راپور',
  'activity_progress'          => 'پیشرفت کار',
  'activity_reamin'            => 'کار باقی مانده',
  'activity_is_compelete'      => 'تکمیل شده ؟',
  'summary_project_progress'      => 'خلاصه پیشرفت پروژه',
  'project_progress'      => 'پیشرفت کار',
  'project_work_remain'      => 'کار باقی مانده',
  'work_done_money'      => 'مقدار پول به اساس کار انجام شده',
  'work_remain_money'      => 'مقدار پول به اسال کار باقی مانده',
  'proj_prog_money'      => 'مقدار پول به اساس کار',
  'extra_work'      => 'اضافه کاری',
  'extra_work_money'      => 'پول اضافه کاری مقدار',

  ];
?>
