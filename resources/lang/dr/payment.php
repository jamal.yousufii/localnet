<?php

return [
    'request'        => 'درخواست',
    'description'    => 'تشریحات',
    'payment_list'   => 'لیست درخواست پرداختی پروژه',
    'prequest_summary'   => 'خلاصه درخواست پرداختی',
    'contracted_money'   => 'مجموع پول قرارداد',
    'act_total_money'           => 'مجموع پول فعالیت ها', 
    'activity_no'           => 'تعداد فعالیت های درخواستی', 
    'reqtotal_money'            => 'مجموع پول فعالیت درخواستی', 
    'veriance'                     =>'تفاوت',
    'pre_request'                     =>'درخواست قبلی به فیصدی',
    'pre_request_money'                    =>'مجموع پول درخواست قبلی',
    'reqtotal_percentage'                     =>' مجموع درخواستی به فیصدی',
    'remained_money' => 'پول باقی مانده',
    'remained_percent' => 'پول باقی مانده به فیصدی',
    'to_request' => 'پول درخواستی',
    'pre_payment' => 'پرداختی قبلی',
    'to_recieve' => 'پول قابل دریافت',
    'store_payment' => 'ثبت پرداختی',
    'store_payment_request' => 'ثبت درخواست پرداختی',
    'request_number' => 'شماره درخواست',
    'request_number_note' => 'شماره درخواست ضروری میباشد',
    'request_date' => 'تاریخ درخواست',
    'request_date_note' => 'تاریخ درخواست ضروری میباشد',
    'payment_view' => 'نمایش درخواست پرداختی',
    'request_detail' => 'جزییات درخواست',
    'payment_act_detls' => 'جزییات فعالیت های درخواست',

]

?>