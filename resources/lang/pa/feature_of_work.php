<?php
return [
  'list' 	                => "لیست مشخصات کار",
  'feature_name'   		    => "نام مشخصات کار",
  'feature_description'   => "شرح مشخصات کار",
  'add_new_feature' 	    => "ثبت مشخصات کار",
  'search' 		            => "جستجو بر اساس نام مشخصات کار",
  'view_feature'     	    => "نمایش مشخصات کار",
  'edit_feature'     	    => "تجدید مشخصات کار",
  'edit'     		          => "تجدید",
  'view'     	    	      => "نمایش",
  'delete'                => "حذف",
];
?>
