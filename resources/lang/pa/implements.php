<?php
return [
  'implements' 		    => "چک لیست ها",
  'list' 	            => "لیست چک لیست ها",
  'edit' 		          => "تجدید چک لیست",
  'add' 		          => "ثبت چک لیست",
  'view' 		          => "نمایش چک لیست",
  'attachment_type'   => "نوعیت ضمیمه",
  'attachment_name' 	=> "نام",
  'description' 	    => "تشریحات",
];
?>
