<?php
return [
  'inspection_name_dr' 	       => "نام بازدید کاری به دری",
  'add_new_inspection' 	       => "ثبت بازدید کاری ",
  'edit'     		               => "تجدید",
  'view'     	    	           => "نمایش تحلیل و تجزیه خطرات",
  'delete'             	       => "حذف",
  'add_hazard_analysis'        => "علاوه نمودن تحلیل و تجزیه خطرات",
  'add_hazard_analysis_sub'    => "علاوه نمودن شرح تحلیل و تجزیه خطرات",
  'description'                => "شرح",
  'hazard_analysis'            => "تحلیل و تجزیه خطرات", 
  'hazard_analysis_category'   => "کتگوری اصلی", 
  'hazard_inspection'          => "سفارش ـ آموزش",
  'hazard_equipment'           => "سفارش",
  'hazard_training'            => "آموزش",
  'hazard_te_add'              => "علاوه نمودن سفارش و آموزش",
  'hazard_te_view'             => " نمایش سفارش و آموزش",
];
?>
