<?php

return [

	/*
	|--------------------------------------------------------------------------
	| PDO Fetch Style
	|--------------------------------------------------------------------------
	|
	| By default, database results will be returned as instances of the PHP
	| stdClass object; however, you may desire to retrieve records in an
	| array format for simplicity. Here you can tweak the fetch style.
	|
	*/

	'fetch' => PDO::FETCH_CLASS,

	/*
	|--------------------------------------------------------------------------
	| Default Database Connection Name
	|--------------------------------------------------------------------------
	|
	| Here you may specify which of the database connections below you wish
	| to use as your default connection for all database work. Of course
	| you may use many connections at once using the Database library.
	|
	*/

	'default' => 'mysql',

	/*
	|--------------------------------------------------------------------------
	| Database Connections
	|--------------------------------------------------------------------------
	|
	| Here are each of the database connections setup for your application.
	| Of course, examples of configuring each database platform that is
	| supported by Laravel is shown below to make development simple.
	|
	|
	| All database work in Laravel is done through the PHP PDO facilities
	| so make sure you have the driver for your particular database of
	| choice installed on your machine before you begin development.
	|
	*/

	'connections' => [

		'sqlite' => [
			'driver'   => 'sqlite',
			'database' => storage_path().'/database.sqlite',
			'prefix'   => '',
		],

		'mysql' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'auth',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'workplan' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'workplan',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'helpdesk' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'helpdesk',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'hr' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'hr',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'audit' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'audit',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],

		'attendance' => [
			'driver'    => 'mysql',
			'host'      => '10.134.45.19',//'10.5.1.80',
			'port'		=> '3306',
			'database'  => 'adms_db',
			'username'  => 'root',
			'password'  => '',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'evaluation' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'evaluation',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'transport' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'transport',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'library' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'library_system',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'docscom' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'docscom',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'procurement' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'procurement',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'procurement_inventory' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'procurement_inventory',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'inventory' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'inventory',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'const_maintenance' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'construction_maintenance',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'sched' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'schedule',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'specification' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'specification',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'executive_management' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'executive_management',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'document_managment' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'document_managment',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'document_tracking' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'm_e_document_tracking',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'docs_archive' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'docs_archive',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'services' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'services',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'icom' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'icom',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'assets_mgmt' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'assets_management',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'it_telephone' => [
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'it_telephone',
			'username'  => 'root',
			'password'  => 'mynewpassword',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
			'strict'    => false,
		],
		'pgsql' => [
			'driver'   => 'pgsql',
			'host'     => env('DB_HOST', 'localhost'),
			'database' => env('DB_DATABASE', 'forge'),
			'username' => env('DB_USERNAME', 'forge'),
			'password' => env('DB_PASSWORD', ''),
			'charset'  => 'utf8',
			'prefix'   => '',
			'schema'   => 'public',
		],

		'sqlsrv' => [
			'driver'   => 'sqlsrv',
			'host'     => env('DB_HOST', 'localhost'),
			'database' => env('DB_DATABASE', 'forge'),
			'username' => env('DB_USERNAME', 'forge'),
			'password' => env('DB_PASSWORD', ''),
			'prefix'   => '',
		],

	],

	/*
	|--------------------------------------------------------------------------
	| Migration Repository Table
	|--------------------------------------------------------------------------
	|
	| This table keeps track of all the migrations that have already run for
	| your application. Using this information, we can determine which of
	| the migrations on disk haven't actually been run in the database.
	|
	*/

	'migrations' => 'migrations',

	/*
	|--------------------------------------------------------------------------
	| Redis Databases
	|--------------------------------------------------------------------------
	|
	| Redis is an open source, fast, and advanced key-value store that also
	| provides a richer set of commands than a typical key-value systems
	| such as APC or Memcached. Laravel makes it easy to dig right in.
	|
	*/

	'redis' => [

		'cluster' => false,

		'default' => [
			'host'     => '127.0.0.1',
			'port'     => 6379,
			'database' => 0,
		],

	],

];
